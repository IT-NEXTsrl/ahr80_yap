* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_mam                                                        *
*              Movimenti matricole                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_323]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-02                                                      *
* Last revis.: 2015-12-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsmd_mam")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsmd_mam")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsmd_mam")
  return

* --- Class definition
define class tgsmd_mam as StdPCForm
  Width  = 571
  Height = 321
  Top    = 10
  Left   = 1
  cComment = "Movimenti matricole"
  cPrg = "gsmd_mam"
  HelpContextID=137737367
  add object cnt as tcgsmd_mam
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsmd_mam as PCContext
  w_MTSERIAL = space(10)
  w_MTROWNUM = 0
  w_MTNUMRIF = 0
  w_MTKEYSAL = space(20)
  w_MTCODMAT = space(40)
  w_MTCODLOT = space(20)
  w_MTCODUBI = space(20)
  w_MTSERRIF = space(10)
  w_MTROWRIF = 0
  w_CODART = space(20)
  w_CODICE = space(41)
  w_DESART = space(40)
  w_UNIMIS1 = space(3)
  w_QTAUM1 = 0
  w_MGUBIC = space(1)
  w_FLLOTT = space(1)
  w_RIGA = 0
  w_MTMAGCAR = space(5)
  w_MTFLSCAR = space(1)
  w_MTMAGSCA = space(5)
  w_MTFLCARI = space(1)
  w_CODMAG = space(5)
  w_ESIRIS = space(1)
  w_KEYSAL = space(20)
  w_MTRIFNUM = 0
  w_MT__FLAG = space(1)
  w_MT_SALDO = 0
  w_CODFOR = space(15)
  w_ARTLOT = space(20)
  w_VARLOT = space(20)
  w_STALOT = space(1)
  w_LOTZOOM = space(1)
  w_MAGUBI = space(5)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_FLRISE = space(1)
  w_NUMRIF = 0
  w_MOVMAT = space(1)
  w_DATREG = space(5)
  w_ULTPROG = 0
  w_SECODUBI = space(20)
  w_SECODLOT = space(20)
  w_CODLOTTES = space(20)
  w_CODUBITES = space(20)
  w_ARTLOTTES = space(20)
  w_FLPRG = space(1)
  w_ROWORD = 0
  w_TOTALE = 0
  w_FLSTAT = space(1)
  w_MTROWLOT = 0
  w_NOMSG = space(1)
  w_TIPCON = space(10)
  w_CODCON = space(15)
  w_CODUBI = space(20)
  w_MTRIFSTO = 0
  w_ARCLAMAT = space(5)
  w_CMFLAUTO = space(1)
  w_COMCAR = space(15)
  w_COMSCA = space(15)
  w_MTCODCOM = space(15)
  w_ARSALCOM = space(1)
  proc Save(i_oFrom)
    this.w_MTSERIAL = i_oFrom.w_MTSERIAL
    this.w_MTROWNUM = i_oFrom.w_MTROWNUM
    this.w_MTNUMRIF = i_oFrom.w_MTNUMRIF
    this.w_MTKEYSAL = i_oFrom.w_MTKEYSAL
    this.w_MTCODMAT = i_oFrom.w_MTCODMAT
    this.w_MTCODLOT = i_oFrom.w_MTCODLOT
    this.w_MTCODUBI = i_oFrom.w_MTCODUBI
    this.w_MTSERRIF = i_oFrom.w_MTSERRIF
    this.w_MTROWRIF = i_oFrom.w_MTROWRIF
    this.w_CODART = i_oFrom.w_CODART
    this.w_CODICE = i_oFrom.w_CODICE
    this.w_DESART = i_oFrom.w_DESART
    this.w_UNIMIS1 = i_oFrom.w_UNIMIS1
    this.w_QTAUM1 = i_oFrom.w_QTAUM1
    this.w_MGUBIC = i_oFrom.w_MGUBIC
    this.w_FLLOTT = i_oFrom.w_FLLOTT
    this.w_RIGA = i_oFrom.w_RIGA
    this.w_MTMAGCAR = i_oFrom.w_MTMAGCAR
    this.w_MTFLSCAR = i_oFrom.w_MTFLSCAR
    this.w_MTMAGSCA = i_oFrom.w_MTMAGSCA
    this.w_MTFLCARI = i_oFrom.w_MTFLCARI
    this.w_CODMAG = i_oFrom.w_CODMAG
    this.w_ESIRIS = i_oFrom.w_ESIRIS
    this.w_KEYSAL = i_oFrom.w_KEYSAL
    this.w_MTRIFNUM = i_oFrom.w_MTRIFNUM
    this.w_MT__FLAG = i_oFrom.w_MT__FLAG
    this.w_MT_SALDO = i_oFrom.w_MT_SALDO
    this.w_CODFOR = i_oFrom.w_CODFOR
    this.w_ARTLOT = i_oFrom.w_ARTLOT
    this.w_VARLOT = i_oFrom.w_VARLOT
    this.w_STALOT = i_oFrom.w_STALOT
    this.w_LOTZOOM = i_oFrom.w_LOTZOOM
    this.w_MAGUBI = i_oFrom.w_MAGUBI
    this.w_SERRIF = i_oFrom.w_SERRIF
    this.w_ROWRIF = i_oFrom.w_ROWRIF
    this.w_FLRISE = i_oFrom.w_FLRISE
    this.w_NUMRIF = i_oFrom.w_NUMRIF
    this.w_MOVMAT = i_oFrom.w_MOVMAT
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_ULTPROG = i_oFrom.w_ULTPROG
    this.w_SECODUBI = i_oFrom.w_SECODUBI
    this.w_SECODLOT = i_oFrom.w_SECODLOT
    this.w_CODLOTTES = i_oFrom.w_CODLOTTES
    this.w_CODUBITES = i_oFrom.w_CODUBITES
    this.w_ARTLOTTES = i_oFrom.w_ARTLOTTES
    this.w_FLPRG = i_oFrom.w_FLPRG
    this.w_ROWORD = i_oFrom.w_ROWORD
    this.w_TOTALE = i_oFrom.w_TOTALE
    this.w_FLSTAT = i_oFrom.w_FLSTAT
    this.w_MTROWLOT = i_oFrom.w_MTROWLOT
    this.w_NOMSG = i_oFrom.w_NOMSG
    this.w_TIPCON = i_oFrom.w_TIPCON
    this.w_CODCON = i_oFrom.w_CODCON
    this.w_CODUBI = i_oFrom.w_CODUBI
    this.w_MTRIFSTO = i_oFrom.w_MTRIFSTO
    this.w_ARCLAMAT = i_oFrom.w_ARCLAMAT
    this.w_CMFLAUTO = i_oFrom.w_CMFLAUTO
    this.w_COMCAR = i_oFrom.w_COMCAR
    this.w_COMSCA = i_oFrom.w_COMSCA
    this.w_MTCODCOM = i_oFrom.w_MTCODCOM
    this.w_ARSALCOM = i_oFrom.w_ARSALCOM
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MTSERIAL = this.w_MTSERIAL
    i_oTo.w_MTROWNUM = this.w_MTROWNUM
    i_oTo.w_MTNUMRIF = this.w_MTNUMRIF
    i_oTo.w_MTKEYSAL = this.w_MTKEYSAL
    i_oTo.w_MTCODMAT = this.w_MTCODMAT
    i_oTo.w_MTCODLOT = this.w_MTCODLOT
    i_oTo.w_MTCODUBI = this.w_MTCODUBI
    i_oTo.w_MTSERRIF = this.w_MTSERRIF
    i_oTo.w_MTROWRIF = this.w_MTROWRIF
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_CODICE = this.w_CODICE
    i_oTo.w_DESART = this.w_DESART
    i_oTo.w_UNIMIS1 = this.w_UNIMIS1
    i_oTo.w_QTAUM1 = this.w_QTAUM1
    i_oTo.w_MGUBIC = this.w_MGUBIC
    i_oTo.w_FLLOTT = this.w_FLLOTT
    i_oTo.w_RIGA = this.w_RIGA
    i_oTo.w_MTMAGCAR = this.w_MTMAGCAR
    i_oTo.w_MTFLSCAR = this.w_MTFLSCAR
    i_oTo.w_MTMAGSCA = this.w_MTMAGSCA
    i_oTo.w_MTFLCARI = this.w_MTFLCARI
    i_oTo.w_CODMAG = this.w_CODMAG
    i_oTo.w_ESIRIS = this.w_ESIRIS
    i_oTo.w_KEYSAL = this.w_KEYSAL
    i_oTo.w_MTRIFNUM = this.w_MTRIFNUM
    i_oTo.w_MT__FLAG = this.w_MT__FLAG
    i_oTo.w_MT_SALDO = this.w_MT_SALDO
    i_oTo.w_CODFOR = this.w_CODFOR
    i_oTo.w_ARTLOT = this.w_ARTLOT
    i_oTo.w_VARLOT = this.w_VARLOT
    i_oTo.w_STALOT = this.w_STALOT
    i_oTo.w_LOTZOOM = this.w_LOTZOOM
    i_oTo.w_MAGUBI = this.w_MAGUBI
    i_oTo.w_SERRIF = this.w_SERRIF
    i_oTo.w_ROWRIF = this.w_ROWRIF
    i_oTo.w_FLRISE = this.w_FLRISE
    i_oTo.w_NUMRIF = this.w_NUMRIF
    i_oTo.w_MOVMAT = this.w_MOVMAT
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_ULTPROG = this.w_ULTPROG
    i_oTo.w_SECODUBI = this.w_SECODUBI
    i_oTo.w_SECODLOT = this.w_SECODLOT
    i_oTo.w_CODLOTTES = this.w_CODLOTTES
    i_oTo.w_CODUBITES = this.w_CODUBITES
    i_oTo.w_ARTLOTTES = this.w_ARTLOTTES
    i_oTo.w_FLPRG = this.w_FLPRG
    i_oTo.w_ROWORD = this.w_ROWORD
    i_oTo.w_TOTALE = this.w_TOTALE
    i_oTo.w_FLSTAT = this.w_FLSTAT
    i_oTo.w_MTROWLOT = this.w_MTROWLOT
    i_oTo.w_NOMSG = this.w_NOMSG
    i_oTo.w_TIPCON = this.w_TIPCON
    i_oTo.w_CODCON = this.w_CODCON
    i_oTo.w_CODUBI = this.w_CODUBI
    i_oTo.w_MTRIFSTO = this.w_MTRIFSTO
    i_oTo.w_ARCLAMAT = this.w_ARCLAMAT
    i_oTo.w_CMFLAUTO = this.w_CMFLAUTO
    i_oTo.w_COMCAR = this.w_COMCAR
    i_oTo.w_COMSCA = this.w_COMSCA
    i_oTo.w_MTCODCOM = this.w_MTCODCOM
    i_oTo.w_ARSALCOM = this.w_ARSALCOM
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsmd_mam as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 571
  Height = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-29"
  HelpContextID=137737367
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=61

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  AGG_MATR_IDX = 0
  MATRICOL_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  CMT_MAST_IDX = 0
  MOVIMATR_IDX = 0
  SALLOTUBI_IDX = 0
  cFile = "AGG_MATR"
  cKeySelect = "MTSERIAL,MTROWNUM,MTNUMRIF,MTROWLOT"
  cKeyWhere  = "MTSERIAL=this.w_MTSERIAL and MTROWNUM=this.w_MTROWNUM and MTNUMRIF=this.w_MTNUMRIF and MTROWLOT=this.w_MTROWLOT"
  cKeyDetail  = "MTSERIAL=this.w_MTSERIAL and MTROWNUM=this.w_MTROWNUM and MTNUMRIF=this.w_MTNUMRIF and MTKEYSAL=this.w_MTKEYSAL and MTCODMAT=this.w_MTCODMAT and MTROWLOT=this.w_MTROWLOT"
  cKeyWhereODBC = '"MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';
      +'+" and MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)';
      +'+" and MTROWLOT="+cp_ToStrODBC(this.w_MTROWLOT)';

  cKeyDetailWhereODBC = '"MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';
      +'+" and MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)';
      +'+" and MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)';
      +'+" and MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT)';
      +'+" and MTROWLOT="+cp_ToStrODBC(this.w_MTROWLOT)';

  cKeyWhereODBCqualified = '"AGG_MATR.MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and AGG_MATR.MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';
      +'+" and AGG_MATR.MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)';
      +'+" and AGG_MATR.MTROWLOT="+cp_ToStrODBC(this.w_MTROWLOT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'AGG_MATR.MTCODLOT,AGG_MATR.MTCODUBI,AGG_MATR.MTCODMAT'
  cPrg = "gsmd_mam"
  cComment = "Movimenti matricole"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MTSERIAL = space(10)
  w_MTROWNUM = 0
  w_MTNUMRIF = 0
  w_MTKEYSAL = space(20)
  w_MTCODMAT = space(40)
  o_MTCODMAT = space(40)
  w_MTCODLOT = space(20)
  o_MTCODLOT = space(20)
  w_MTCODUBI = space(20)
  w_MTSERRIF = space(10)
  w_MTROWRIF = 0
  w_CODART = space(20)
  w_CODICE = space(41)
  w_DESART = space(40)
  w_UNIMIS1 = space(3)
  w_QTAUM1 = 0
  w_MGUBIC = space(1)
  w_FLLOTT = space(1)
  w_RIGA = 0
  w_MTMAGCAR = space(5)
  w_MTFLSCAR = space(1)
  w_MTMAGSCA = space(5)
  w_MTFLCARI = space(1)
  w_CODMAG = space(5)
  w_ESIRIS = space(1)
  w_KEYSAL = space(20)
  w_MTRIFNUM = 0
  w_MT__FLAG = space(1)
  w_MT_SALDO = 0
  w_CODFOR = space(15)
  w_ARTLOT = space(20)
  w_VARLOT = space(20)
  w_STALOT = space(1)
  w_LOTZOOM = .F.
  w_MAGUBI = space(5)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_FLRISE = space(1)
  w_NUMRIF = 0
  w_MOVMAT = .F.
  w_DATREG = space(5)
  w_ULTPROG = 0
  w_SECODUBI = space(20)
  w_SECODLOT = space(20)
  w_CODLOTTES = space(20)
  w_CODUBITES = space(20)
  w_ARTLOTTES = space(20)
  w_FLPRG = space(1)
  w_ROWORD = 0
  w_TOTALE = 0
  w_FLSTAT = space(1)
  w_MTROWLOT = 0
  w_NOMSG = .F.
  w_TIPCON = space(10)
  w_CODCON = space(15)
  w_CODUBI = space(20)
  w_MTRIFSTO = 0
  w_ARCLAMAT = space(5)
  w_CMFLAUTO = space(1)
  w_COMCAR = space(15)
  w_COMSCA = space(15)
  w_MTCODCOM = space(15)
  w_ARSALCOM = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsmd_mam
  Procedure InitSon
  	* Valorizzo le variabili che arrivano dal padre
  	With This
  		.w_MTMAGCAR=Space(5)
  		.w_MTMAGSCA=Space(5)
  		.w_MTFLSCAR=' '
  		.w_MTFLCARI=' '
  		* Scarico da evasione
  		.w_SERRIF=Space(10)
  		.w_ROWRIF=0
  		.w_FLRISE=' '
  		.w_NUMRIF=0
  		.w_CODLOTTES=Space(20)
  		.w_CODUBITES=Space(20)
  		.w_COMCAR=Space(15)
  		.w_COMSCA=Space(15)
  		.w_MTCODCOM=Space(15)
      
  		If Upper(.oParentObject.Class)='TCGSMD_MSP'
  			* Documenti
  			.w_CODICE=.oParentObject.oParentObject.w_MVCODICE
  			.w_DESART=.oParentObject.oParentObject.w_MVDESART
  			.w_CODART=.oParentObject.oParentObject.w_MVCODART
  			.w_QTAUM1=.oParentObject.w_SPQTAUM1
  			.w_UNIMIS1=.oParentObject.oParentObject.w_UNMIS1
  			.w_KEYSAL=.oParentObject.oParentObject.w_MVCODART
  			.w_DATREG=.oParentObject.oParentObject.w_MVDATREG
  			.w_CODLOTTES=.oParentObject.w_SPCODLOT
  			.w_COMCAR=.oParentObject.w_MVCODCOM
  			.w_COMSCA=.oParentObject.w_MVCODCOM
        
  			* Determino Magazzino di Scarico
  			If .oParentObject.oParentObject.w_MVFLCASC='-' Or .oParentObject.oParentObject.w_MVFLRISE='+' Or .oParentObject.oParentObject.w_MVF2CASC='-' Or .oParentObject.oParentObject.w_MVF2RISE='+'
  				.w_MTMAGSCA=Iif(.oParentObject.oParentObject.w_MVFLCASC='-' Or .oParentObject.oParentObject.w_MVFLRISE='+',.oParentObject.oParentObject.w_MVCODMAG,.oParentObject.oParentObject.w_MVCODMAT)
  				.w_MTFLSCAR=Iif(.oParentObject.oParentObject.w_MVFLCASC='-' Or .oParentObject.oParentObject.w_MVF2CASC='-','E','R')
  				.w_CODUBI=Iif(.oParentObject.oParentObject.w_MVFLCASC='-' Or .oParentObject.oParentObject.w_MVFLRISE='+',.oParentObject.w_SPCODUBI,.oParentObject.w_SPCODUB2)
  				.w_CODUBITES=.w_CODUBI
  			Endif
  
  			* Determino Magazzino di Carico
  			If .oParentObject.oParentObject.w_MVFLCASC='+' Or .oParentObject.oParentObject.w_MVFLRISE='-' Or .oParentObject.oParentObject.w_MVF2CASC='+' Or .oParentObject.oParentObject.w_MVF2RISE='-'
  				.w_MTMAGCAR=Iif(.oParentObject.oParentObject.w_MVFLCASC='+' Or .oParentObject.oParentObject.w_MVFLRISE='-',.oParentObject.oParentObject.w_MVCODMAG,.oParentObject.oParentObject.w_MVCODMAT)
  				.w_MTFLCARI=Iif(.oParentObject.oParentObject.w_MVFLCASC='+' Or .oParentObject.oParentObject.w_MVF2CASC='+','E','R')
  				.w_CODUBI=Iif(.oParentObject.oParentObject.w_MVFLCASC='+' Or .oParentObject.oParentObject.w_MVFLRISE='-',.oParentObject.w_SPCODUBI,.oParentObject.w_SPCODUB2)
  				.w_CODUBITES=Iif(Not Empty(.w_CODUBITES),.w_CODUBITES,.w_CODUBI)
  			Endif
  
  			* Magazzino di Riferimento (se presente magazzino di scarico)
  			.w_CODMAG = Iif( Not Empty( .w_MTMAGSCA) , .w_MTMAGSCA , .w_MTMAGCAR )
  
  			* Eventuale codice Fornitore
  			* solo se carico merce e documento con intestatario
  			If .w_CODMAG=.w_MTMAGCAR And .oParentObject.oParentObject.w_MVTIPCON='F'
  				.w_CODFOR=.oParentObject.oParentObject.w_MVCODCON
  			Endif
  
  			* Magazzino di Riferimento ubicazioni (fa fede il magazzino di carico se presente)
  			If Not Empty( .w_MTMAGCAR)
  				.w_MAGUBI = Iif( .w_MTMAGCAR = .oParentObject.oParentObject.w_MVCODMAT , .oParentObject.oParentObject.w_MVCODMAT , .oParentObject.oParentObject.w_MVCODMAG )
  			Else
  				.w_MAGUBI = Iif( .w_MTMAGSCA = .oParentObject.oParentObject.w_MVCODMAT , .oParentObject.oParentObject.w_MVCODMAT , .oParentObject.oParentObject.w_MVCODMAG )
  			Endif
  
  			* Magazzino di riferimento gestito ad Ubicazioni
  			.w_MGUBIC = Iif( .w_MAGUBI=.oParentObject.oParentObject.w_MVCODMAG , .oParentObject.oParentObject.w_FLUBIC,.oParentObject.w_F2UBIC )
  
  			* Scarico da evasione
  			.w_FLRISE=.oParentObject.oParentObject.w_FLRRIF
  
  			If .w_FLRISE='+'
  				.w_SERRIF=.oParentObject.oParentObject.w_MVSERRIF
  				.w_ROWRIF=.oParentObject.oParentObject.w_MVROWRIF
  				.w_NUMRIF=.oParentObject.oParentObject.w_MVNUMRIF
  			Endif
  
  			* Flag articolo gestito a lotti
  			.w_FLLOTT=.oParentObject.oParentObject.w_FLLOTT
  
        * Flag articolo gestito a commessa
        .w_ARSALCOM=.oParentObject.w_ARSALCOM
        
  			* Classe Matricola
  			.w_ARCLAMAT=.oParentObject.w_ARCLAMAT
  			.w_CMFLAUTO=LookTab("CMT_MAST","CMFLAUTO","CMCODICE",.w_ARCLAMAT)
  
  			* tipo di operazione (Esistenza/Riservato)
  			.w_ESIRIS = Iif( Not Empty( .w_MTFLSCAR) , .w_MTFLSCAR , .w_MTFLCARI )
  
  			.w_MTKEYSAL=.w_KEYSAL
  
  			* Numero di riga
  			.w_ROWORD=.oParentObject.oParentObject.w_CPROWORD
  			* Sbianco i dati di testata
  		Else
  			* Matricole senza Attivazione Lotti\Ubicazioni
  			.w_CODICE=.oParentObject.w_MVCODICE
  			.w_DESART=.oParentObject.w_MVDESART
  			.w_CODART=.oParentObject.w_MVCODART
  			.w_QTAUM1=.oParentObject.w_MVQTAUM1
  			.w_UNIMIS1=.oParentObject.w_UNMIS1
  			.w_KEYSAL=.oParentObject.w_MVCODART
  			.w_DATREG=.oParentObject.w_MVDATREG
  			.w_CODLOTTES=.oParentObject.w_MVCODLOT
  			.w_CODUBITES=.oParentObject.w_MVCODUBI
  			.w_COMCAR=.oParentObject.w_MVCODCOM
  			.w_COMSCA=.oParentObject.w_MVCODCOM
  
  			* Determino Magazzino di Carico
  			If .oParentObject.w_MVFLCASC='+' Or .oParentObject.w_MVFLRISE='-' Or .oParentObject.w_MVF2CASC='+' Or .oParentObject.w_MVF2RISE='-'
  				.w_MTMAGCAR=Iif(.oParentObject.w_MVFLCASC='+' Or .oParentObject.w_MVFLRISE='-',.oParentObject.w_MVCODMAG,.oParentObject.w_MVCODMAT)
  				.w_MTFLCARI=Iif(.oParentObject.w_MVFLCASC='+' Or .oParentObject.w_MVF2CASC='+','E','R')
  			Endif
  
  			* Determino Magazzino di Scarico
  			If .oParentObject.w_MVFLCASC='-' Or .oParentObject.w_MVFLRISE='+' Or .oParentObject.w_MVF2CASC='-' Or .oParentObject.w_MVF2RISE='+'
  				.w_MTMAGSCA=Iif(.oParentObject.w_MVFLCASC='-' Or .oParentObject.w_MVFLRISE='+',.oParentObject.w_MVCODMAG,.oParentObject.oParentObject.w_MVCODMAT)
  				.w_MTFLSCAR=Iif(.oParentObject.w_MVFLCASC='-' Or .oParentObject.w_MVF2CASC='-','E','R')
  			Endif
  
  			* Magazzino di Riferimento (se presente magazzino di scarico)
  			.w_CODMAG = Iif( Not Empty( .w_MTMAGSCA) , .w_MTMAGSCA , .w_MTMAGCAR )
  
  			* Eventuale codice Fornitore
  			* solo se carico merce e documento con intestatario
  			If .w_CODMAG=.w_MTMAGCAR And .oParentObject.w_MVTIPCON='F'
  				.w_CODFOR=.oParentObject.w_MVCODCON
  			Endif
  
  			* Magazzino di Riferimento ubicazioni (fa fede il magazzino di carico se presente)
  			If Not Empty( .w_MTMAGCAR)
  				.w_MAGUBI = Iif( .w_MTMAGCAR = .oParentObject.w_MVCODMAT , .oParentObject.w_MVCODMAT , .oParentObject.w_MVCODMAG )
  			Else
  				.w_MAGUBI = Iif( .w_MTMAGSCA = .oParentObject.w_MVCODMAT , .oParentObject.w_MVCODMAT , .oParentObject.w_MVCODMAG )
  			Endif
  
  			* Magazzino di riferimento gestito ad Ubicazioni
  			.w_MGUBIC = Iif( .w_MAGUBI=.oParentObject.w_MVCODMAG , .oParentObject.w_FLUBIC,.oParentObject.w_F2UBIC )
  
  			* Scarico da evasione
  			.w_FLRISE=.oParentObject.w_FLRRIF
  
  			If .w_FLRISE='+'
  				.w_SERRIF=.oParentObject.w_MVSERRIF
  				.w_ROWRIF=.oParentObject.w_MVROWRIF
  				.w_NUMRIF=.oParentObject.w_MVNUMRIF
  			Endif
  
  			* Flag articolo gestito a lotti
  			.w_FLLOTT=.oParentObject.w_FLLOTT
  
        * Flag articolo gestito a commessa
        .w_ARSALCOM=.oParentObject.w_ARSALCOM
        
  			* Classe Matricola
  			.w_ARCLAMAT=.oParentObject.w_ARCLAMAT
  			.w_CMFLAUTO=LookTab("CMT_MAST","CMFLAUTO","CMCODICE",.w_ARCLAMAT)
  
  			* tipo di operazione (Esistenza/Riservato)
  			.w_ESIRIS = Iif( Not Empty( .w_MTFLSCAR) , .w_MTFLSCAR , .w_MTFLCARI )
  			.w_MTKEYSAL=.w_KEYSAL
  			* Numero di riga
  			.w_ROWORD=.oParentObject.w_CPROWORD
  			* Sbianco i dati di testata
  		Endif
  		.SetControlsValue()
  		* Mostro / Nascondo i Controls
  		.mHideControls()
  	Endwith
  Endproc
  
  Proc F6()
  	* --- disabilita F6 per righe non cancellabili
  	* --- Es. matricola movimentata in seguito
  	If Inlist(This.cFunction,'Edit','Load')
  		If This.w_MT_SALDO<>0
  			Ah_errormsg("Impossibile cancellare una riga successivamente movimentata",'!',"")
  		Else
  			DoDefault()
  		Endif
  	Endif
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_mamPag1","gsmd_mam",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='MATRICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='UBICAZIO'
    this.cWorkTables[4]='CMT_MAST'
    this.cWorkTables[5]='MOVIMATR'
    this.cWorkTables[6]='SALLOTUBI'
    this.cWorkTables[7]='AGG_MATR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AGG_MATR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AGG_MATR_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsmd_mam'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsmd_mam
    This.InitSon()
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from AGG_MATR where MTSERIAL=KeySet.MTSERIAL
    *                            and MTROWNUM=KeySet.MTROWNUM
    *                            and MTNUMRIF=KeySet.MTNUMRIF
    *                            and MTKEYSAL=KeySet.MTKEYSAL
    *                            and MTCODMAT=KeySet.MTCODMAT
    *                            and MTROWLOT=KeySet.MTROWLOT
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.AGG_MATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_MATR_IDX,2],this.bLoadRecFilter,this.AGG_MATR_IDX,"gsmd_mam")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AGG_MATR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AGG_MATR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AGG_MATR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MTSERIAL',this.w_MTSERIAL  ,'MTROWNUM',this.w_MTROWNUM  ,'MTNUMRIF',this.w_MTNUMRIF  ,'MTROWLOT',this.w_MTROWLOT  )
      select * from (i_cTable) AGG_MATR where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODICE = space(41)
        .w_DESART = space(40)
        .w_UNIMIS1 = space(3)
        .w_QTAUM1 = 0
        .w_MOVMAT = .t.
        .w_ULTPROG = 0
        .w_SECODUBI = space(20)
        .w_SECODLOT = space(20)
        .w_CODLOTTES = space(20)
        .w_CODUBITES = space(20)
        .w_ARTLOTTES = space(20)
        .w_TOTALE = 0
        .w_FLSTAT = space(1)
        .w_CODUBI = space(20)
        .w_ARCLAMAT = space(5)
        .w_CMFLAUTO = space(1)
        .w_MTSERIAL = NVL(MTSERIAL,space(10))
        .w_MTROWNUM = NVL(MTROWNUM,0)
        .w_MTNUMRIF = NVL(MTNUMRIF,0)
        .w_CODART = .w_CODART
        .w_MGUBIC = .w_MGUBIC
        .w_FLLOTT = .w_FLLOTT
        .w_MTMAGCAR = NVL(MTMAGCAR,space(5))
        .w_MTFLSCAR = NVL(MTFLSCAR,space(1))
        .w_MTMAGSCA = NVL(MTMAGSCA,space(5))
        .w_MTFLCARI = NVL(MTFLCARI,space(1))
        .w_CODMAG = .w_CODMAG
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .w_KEYSAL
        .w_CODFOR = .w_CODFOR
        .w_MAGUBI = .w_MAGUBI
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .w_DATREG = .w_DATREG
          .link_1_30('Load')
          .link_1_31('Load')
        .w_FLPRG = 'L'
        .w_ROWORD = .w_ROWORD
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .w_MTROWLOT = NVL(MTROWLOT,0)
        .w_TIPCON = this.oParentObject .w_STIPCON
        .w_CODCON = this.oParentObject .w_SCODCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
        .w_ARSALCOM = .w_ARSALCOM
        cp_LoadRecExtFlds(this,'AGG_MATR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_ARTLOT = space(20)
          .w_VARLOT = space(20)
          .w_STALOT = space(1)
          .w_MTKEYSAL = NVL(MTKEYSAL,space(20))
          .w_MTCODMAT = NVL(MTCODMAT,space(40))
          .w_MTCODLOT = NVL(MTCODLOT,space(20))
          .link_2_3('Load')
          .w_MTCODUBI = NVL(MTCODUBI,space(20))
          * evitabile
          *.link_2_4('Load')
          .w_MTSERRIF = NVL(MTSERRIF,space(10))
          .w_MTROWRIF = NVL(MTROWRIF,0)
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
          .w_MTRIFNUM = NVL(MTRIFNUM,0)
          * evitabile
          *.link_2_8('Load')
          .w_MT__FLAG = NVL(MT__FLAG,space(1))
          .w_MT_SALDO = NVL(MT_SALDO,0)
        .w_LOTZOOM = .F.
        .w_NOMSG = .T.
          .w_MTRIFSTO = NVL(MTRIFSTO,0)
          * evitabile
          *.link_1_49('Load')
          .w_MTCODCOM = NVL(MTCODCOM,space(15))
          select (this.cTrsName)
          append blank
          replace MTKEYSAL with .w_MTKEYSAL
          replace MTCODMAT with .w_MTCODMAT
          replace MTSERRIF with .w_MTSERRIF
          replace MTROWRIF with .w_MTROWRIF
          replace MTRIFNUM with .w_MTRIFNUM
          replace MT__FLAG with .w_MT__FLAG
          replace MTRIFSTO with .w_MTRIFSTO
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALE = .w_TOTALE+.w_RIGA
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CODART = .w_CODART
        .w_MGUBIC = .w_MGUBIC
        .w_FLLOTT = .w_FLLOTT
        .w_CODMAG = .w_CODMAG
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .w_KEYSAL
        .w_CODFOR = .w_CODFOR
        .w_MAGUBI = .w_MAGUBI
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .w_DATREG = .w_DATREG
        .w_FLPRG = 'L'
        .w_ROWORD = .w_ROWORD
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .w_NOMSG = .T.
        .w_TIPCON = this.oParentObject .w_STIPCON
        .w_CODCON = this.oParentObject .w_SCODCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
        .w_ARSALCOM = .w_ARSALCOM
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MTSERIAL=space(10)
      .w_MTROWNUM=0
      .w_MTNUMRIF=0
      .w_MTKEYSAL=space(20)
      .w_MTCODMAT=space(40)
      .w_MTCODLOT=space(20)
      .w_MTCODUBI=space(20)
      .w_MTSERRIF=space(10)
      .w_MTROWRIF=0
      .w_CODART=space(20)
      .w_CODICE=space(41)
      .w_DESART=space(40)
      .w_UNIMIS1=space(3)
      .w_QTAUM1=0
      .w_MGUBIC=space(1)
      .w_FLLOTT=space(1)
      .w_RIGA=0
      .w_MTMAGCAR=space(5)
      .w_MTFLSCAR=space(1)
      .w_MTMAGSCA=space(5)
      .w_MTFLCARI=space(1)
      .w_CODMAG=space(5)
      .w_ESIRIS=space(1)
      .w_KEYSAL=space(20)
      .w_MTRIFNUM=0
      .w_MT__FLAG=space(1)
      .w_MT_SALDO=0
      .w_CODFOR=space(15)
      .w_ARTLOT=space(20)
      .w_VARLOT=space(20)
      .w_STALOT=space(1)
      .w_LOTZOOM=.f.
      .w_MAGUBI=space(5)
      .w_SERRIF=space(10)
      .w_ROWRIF=0
      .w_FLRISE=space(1)
      .w_NUMRIF=0
      .w_MOVMAT=.f.
      .w_DATREG=space(5)
      .w_ULTPROG=0
      .w_SECODUBI=space(20)
      .w_SECODLOT=space(20)
      .w_CODLOTTES=space(20)
      .w_CODUBITES=space(20)
      .w_ARTLOTTES=space(20)
      .w_FLPRG=space(1)
      .w_ROWORD=0
      .w_TOTALE=0
      .w_FLSTAT=space(1)
      .w_MTROWLOT=0
      .w_NOMSG=.f.
      .w_TIPCON=space(10)
      .w_CODCON=space(15)
      .w_CODUBI=space(20)
      .w_MTRIFSTO=0
      .w_ARCLAMAT=space(5)
      .w_CMFLAUTO=space(1)
      .w_COMCAR=space(15)
      .w_COMSCA=space(15)
      .w_MTCODCOM=space(15)
      .w_ARSALCOM=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_MTKEYSAL = .w_KEYSAL
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_MTCODLOT))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MTCODUBI))
         .link_2_4('Full')
        endif
        .DoRTCalc(8,8,.f.)
        .w_MTROWRIF = 0
        .w_CODART = .w_CODART
        .DoRTCalc(11,14,.f.)
        .w_MGUBIC = .w_MGUBIC
        .w_FLLOTT = .w_FLLOTT
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
        .DoRTCalc(18,21,.f.)
        .w_CODMAG = .w_CODMAG
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .w_KEYSAL
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_MTRIFNUM))
         .link_2_8('Full')
        endif
        .w_MT__FLAG = ' '
        .DoRTCalc(27,27,.f.)
        .w_CODFOR = .w_CODFOR
        .DoRTCalc(29,31,.f.)
        .w_LOTZOOM = .F.
        .w_MAGUBI = .w_MAGUBI
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .w_MOVMAT = .t.
        .w_DATREG = .w_DATREG
        .DoRTCalc(40,43,.f.)
        if not(empty(.w_CODLOTTES))
         .link_1_30('Full')
        endif
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_CODUBITES))
         .link_1_31('Full')
        endif
        .DoRTCalc(45,45,.f.)
        .w_FLPRG = 'L'
        .w_ROWORD = .w_ROWORD
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .DoRTCalc(48,50,.f.)
        .w_NOMSG = .T.
        .w_TIPCON = this.oParentObject .w_STIPCON
        .w_CODCON = this.oParentObject .w_SCODCON
        .DoRTCalc(54,55,.f.)
        if not(empty(.w_MTRIFSTO))
         .link_1_49('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(56,57,.f.)
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
        .w_MTCODCOM = .w_MTCODCOM
        .w_ARSALCOM = .w_ARSALCOM
      endif
    endwith
    cp_BlankRecExtFlds(this,'AGG_MATR')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsmd_mam
    * Dopo l'inserzione della riga rileggo tutto
    This.InitSon()
    * per aggiornare w_MTKEYSAL (sul transitorio)
    This.TrsFromWork()
    
    * verifico se lanciare il caricamento
    If Not Empty(This.w_MTMAGCAR) or Not Empty(This.w_MTMAGSCA)
    	* sono in un caricamento verifico se riga aggiunta
    	Local L_area,L_Srv
    	L_Area=Select()
    	Select(This.oParentObject.cTrsName)
    	L_Srv=I_Srv
    	Select (L_Area)
    	* se riga in append e attivo flag caricmaento rapido
    	* sulla causale
    	If L_Srv='A' And This.oParentObject.w_MTCARI='S'
    		This.NotifyEvent('CarRapido')
    	Endif
    Endif
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- gsmd_mam
    this.InitSon()
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_36.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_42.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AGG_MATR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AGG_MATR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTSERIAL,"MTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTROWNUM,"MTROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTNUMRIF,"MTNUMRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTMAGCAR,"MTMAGCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTFLSCAR,"MTFLSCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTMAGSCA,"MTMAGSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTFLCARI,"MTFLCARI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTROWLOT,"MTROWLOT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MTCODMAT C(40);
      ,MTKEYSAL C(20);
      ,MTCODMAT C(40);
      ,MTSERRIF C(10);
      ,MTROWRIF N(4);
      ,MTRIFNUM N(4);
      ,MT__FLAG C(1);
      ,MTRIFSTO N(4);
      ,t_MTKEYSAL C(20);
      ,t_MTCODLOT C(20);
      ,t_MTCODUBI C(20);
      ,t_MTSERRIF C(10);
      ,t_MTROWRIF N(4);
      ,t_RIGA N(10);
      ,t_MTRIFNUM N(4);
      ,t_MT__FLAG C(1);
      ,t_MT_SALDO N(1);
      ,t_ARTLOT C(20);
      ,t_VARLOT C(20);
      ,t_STALOT C(1);
      ,t_LOTZOOM L(1);
      ,t_NOMSG L(1);
      ,t_MTRIFSTO N(4);
      ,t_MTCODCOM C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsmd_mambodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.controlsource=this.cTrsName+'.t_MTCODMAT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AGG_MATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_MATR_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AGG_MATR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_MATR_IDX,2])
      *
      * insert into AGG_MATR
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AGG_MATR')
        i_extval=cp_InsertValODBCExtFlds(this,'AGG_MATR')
        i_cFldBody=" "+;
                  "(MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTCODMAT"+;
                  ",MTCODLOT,MTCODUBI,MTSERRIF,MTROWRIF,MTMAGCAR"+;
                  ",MTFLSCAR,MTMAGSCA,MTFLCARI,MTRIFNUM,MT__FLAG"+;
                  ",MT_SALDO,MTROWLOT,MTRIFSTO,MTCODCOM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MTSERIAL)+","+cp_ToStrODBC(this.w_MTROWNUM)+","+cp_ToStrODBC(this.w_MTNUMRIF)+","+cp_ToStrODBC(this.w_MTKEYSAL)+","+cp_ToStrODBC(this.w_MTCODMAT)+;
             ","+cp_ToStrODBCNull(this.w_MTCODLOT)+","+cp_ToStrODBCNull(this.w_MTCODUBI)+","+cp_ToStrODBC(this.w_MTSERRIF)+","+cp_ToStrODBC(this.w_MTROWRIF)+","+cp_ToStrODBC(this.w_MTMAGCAR)+;
             ","+cp_ToStrODBC(this.w_MTFLSCAR)+","+cp_ToStrODBC(this.w_MTMAGSCA)+","+cp_ToStrODBC(this.w_MTFLCARI)+","+cp_ToStrODBCNull(this.w_MTRIFNUM)+","+cp_ToStrODBC(this.w_MT__FLAG)+;
             ","+cp_ToStrODBC(this.w_MT_SALDO)+","+cp_ToStrODBC(this.w_MTROWLOT)+","+cp_ToStrODBCNull(this.w_MTRIFSTO)+","+cp_ToStrODBC(this.w_MTCODCOM)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AGG_MATR')
        i_extval=cp_InsertValVFPExtFlds(this,'AGG_MATR')
        cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_MTSERIAL,'MTROWNUM',this.w_MTROWNUM,'MTNUMRIF',this.w_MTNUMRIF,'MTKEYSAL',this.w_MTKEYSAL,'MTCODMAT',this.w_MTCODMAT,'MTROWLOT',this.w_MTROWLOT)
        INSERT INTO (i_cTable) (;
                   MTSERIAL;
                  ,MTROWNUM;
                  ,MTNUMRIF;
                  ,MTKEYSAL;
                  ,MTCODMAT;
                  ,MTCODLOT;
                  ,MTCODUBI;
                  ,MTSERRIF;
                  ,MTROWRIF;
                  ,MTMAGCAR;
                  ,MTFLSCAR;
                  ,MTMAGSCA;
                  ,MTFLCARI;
                  ,MTRIFNUM;
                  ,MT__FLAG;
                  ,MT_SALDO;
                  ,MTROWLOT;
                  ,MTRIFSTO;
                  ,MTCODCOM;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MTSERIAL;
                  ,this.w_MTROWNUM;
                  ,this.w_MTNUMRIF;
                  ,this.w_MTKEYSAL;
                  ,this.w_MTCODMAT;
                  ,this.w_MTCODLOT;
                  ,this.w_MTCODUBI;
                  ,this.w_MTSERRIF;
                  ,this.w_MTROWRIF;
                  ,this.w_MTMAGCAR;
                  ,this.w_MTFLSCAR;
                  ,this.w_MTMAGSCA;
                  ,this.w_MTFLCARI;
                  ,this.w_MTRIFNUM;
                  ,this.w_MT__FLAG;
                  ,this.w_MT_SALDO;
                  ,this.w_MTROWLOT;
                  ,this.w_MTRIFSTO;
                  ,this.w_MTCODCOM;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.AGG_MATR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_MATR_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_MTRIFSTO<>t_MTRIFSTO
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_MTCODMAT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'AGG_MATR')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " MTMAGCAR="+cp_ToStrODBC(this.w_MTMAGCAR)+;
                 ",MTFLSCAR="+cp_ToStrODBC(this.w_MTFLSCAR)+;
                 ",MTMAGSCA="+cp_ToStrODBC(this.w_MTMAGSCA)+;
                 ",MTFLCARI="+cp_ToStrODBC(this.w_MTFLCARI)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                 " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'AGG_MATR')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  MTMAGCAR=this.w_MTMAGCAR;
                 ,MTFLSCAR=this.w_MTFLSCAR;
                 ,MTMAGSCA=this.w_MTMAGSCA;
                 ,MTFLCARI=this.w_MTFLCARI;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and MTKEYSAL=&i_TN.->MTKEYSAL;
                      and MTCODMAT=&i_TN.->MTCODMAT;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_MTRIFSTO<>t_MTRIFSTO
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_MTCODMAT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MTKEYSAL=&i_TN.->MTKEYSAL;
                            and MTCODMAT=&i_TN.->MTCODMAT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace MTKEYSAL with this.w_MTKEYSAL
              replace MTCODMAT with this.w_MTCODMAT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update AGG_MATR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'AGG_MATR')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MTCODLOT="+cp_ToStrODBCNull(this.w_MTCODLOT)+;
                     ",MTCODUBI="+cp_ToStrODBCNull(this.w_MTCODUBI)+;
                     ",MTSERRIF="+cp_ToStrODBC(this.w_MTSERRIF)+;
                     ",MTROWRIF="+cp_ToStrODBC(this.w_MTROWRIF)+;
                     ",MTMAGCAR="+cp_ToStrODBC(this.w_MTMAGCAR)+;
                     ",MTFLSCAR="+cp_ToStrODBC(this.w_MTFLSCAR)+;
                     ",MTMAGSCA="+cp_ToStrODBC(this.w_MTMAGSCA)+;
                     ",MTFLCARI="+cp_ToStrODBC(this.w_MTFLCARI)+;
                     ",MTRIFNUM="+cp_ToStrODBCNull(this.w_MTRIFNUM)+;
                     ",MT__FLAG="+cp_ToStrODBC(this.w_MT__FLAG)+;
                     ",MT_SALDO="+cp_ToStrODBC(this.w_MT_SALDO)+;
                     ",MTRIFSTO="+cp_ToStrODBCNull(this.w_MTRIFSTO)+;
                     ",MTCODCOM="+cp_ToStrODBC(this.w_MTCODCOM)+;
                     ",MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)+;
                     ",MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and MTKEYSAL="+cp_ToStrODBC(MTKEYSAL)+;
                             " and MTCODMAT="+cp_ToStrODBC(MTCODMAT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'AGG_MATR')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MTCODLOT=this.w_MTCODLOT;
                     ,MTCODUBI=this.w_MTCODUBI;
                     ,MTSERRIF=this.w_MTSERRIF;
                     ,MTROWRIF=this.w_MTROWRIF;
                     ,MTMAGCAR=this.w_MTMAGCAR;
                     ,MTFLSCAR=this.w_MTFLSCAR;
                     ,MTMAGSCA=this.w_MTMAGSCA;
                     ,MTFLCARI=this.w_MTFLCARI;
                     ,MTRIFNUM=this.w_MTRIFNUM;
                     ,MT__FLAG=this.w_MT__FLAG;
                     ,MT_SALDO=this.w_MT_SALDO;
                     ,MTRIFSTO=this.w_MTRIFSTO;
                     ,MTCODCOM=this.w_MTCODCOM;
                     ,MTKEYSAL=this.w_MTKEYSAL;
                     ,MTCODMAT=this.w_MTCODMAT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and MTKEYSAL=&i_TN.->MTKEYSAL;
                                      and MTCODMAT=&i_TN.->MTCODMAT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsmd_mam
    Wait Clear
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.MOVIMATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MT__FLAG,space(1))==this.w_MT__FLAG;
              and NVL(&i_cF..MTRIFNUM,0)==this.w_MTRIFNUM;
              and NVL(&i_cF..MTSERRIF,space(10))==this.w_MTSERRIF;
              and NVL(&i_cF..MTROWRIF,0)==this.w_MTROWRIF;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MT__FLAG,space(1)),'MT_SALDO','',1,'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MTRIFNUM,0)
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" MT_SALDO="+i_cOp1+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE MTNUMRIF="+cp_ToStrODBC(NVL(&i_cF..MTRIFNUM,0));
             +" AND MTSERIAL="+cp_ToStrODBC(NVL(&i_cF..MTSERRIF,space(10)));
             +" AND MTROWNUM="+cp_ToStrODBC(NVL(&i_cF..MTROWRIF,0));
             +" AND MTKEYSAL="+cp_ToStrODBC(NVL(&i_cF..MTKEYSAL,space(20)));
             +" AND MTCODMAT="+cp_ToStrODBC(NVL(&i_cF..MTCODMAT,space(40)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MT__FLAG,'MT_SALDO','1',1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'MTSERIAL',&i_cF..MTSERRIF;
                 ,'MTROWNUM',&i_cF..MTROWRIF;
                 ,'MTKEYSAL',&i_cF..MTKEYSAL;
                 ,'MTCODMAT',&i_cF..MTCODMAT;
                 ,'MTNUMRIF',&i_cF..MTRIFNUM)
      UPDATE (i_cTable) SET ;
           MT_SALDO=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.SALLOTUBI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALLOTUBI_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MT__FLAG,space(1))==this.w_MT__FLAG;
              and NVL(&i_cF..MTRIFSTO,0)==this.w_MTRIFSTO;
              and NVL(&i_cF..MTSERRIF,space(10))==this.w_MTSERRIF;
              and NVL(&i_cF..MTROWRIF,0)==this.w_MTROWRIF;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MT__FLAG,space(1)),'SU_SALDO','',1,'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MTRIFSTO,0)
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SU_SALDO="+i_cOp1+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SUNUMRIF="+cp_ToStrODBC(NVL(&i_cF..MTRIFSTO,0));
             +" AND SUSERRIF="+cp_ToStrODBC(NVL(&i_cF..MTSERRIF,space(10)));
             +" AND SUROWRIF="+cp_ToStrODBC(NVL(&i_cF..MTROWRIF,0));
             +" AND SUCODICE="+cp_ToStrODBC(NVL(&i_cF..MTKEYSAL,space(20)));
             +" AND SUCODMAT="+cp_ToStrODBC(NVL(&i_cF..MTCODMAT,space(40)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MT__FLAG,'SU_SALDO','1',1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SUSERRIF',&i_cF..MTSERRIF;
                 ,'SUROWRIF',&i_cF..MTROWRIF;
                 ,'SUCODICE',&i_cF..MTKEYSAL;
                 ,'SUCODMAT',&i_cF..MTCODMAT;
                 ,'SUNUMRIF',&i_cF..MTRIFSTO)
      UPDATE (i_cTable) SET ;
           SU_SALDO=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.MOVIMATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MT__FLAG,space(1))==this.w_MT__FLAG;
              and NVL(&i_cF..MTRIFNUM,0)==this.w_MTRIFNUM;
              and NVL(&i_cF..MTSERRIF,space(10))==this.w_MTSERRIF;
              and NVL(&i_cF..MTROWRIF,0)==this.w_MTROWRIF;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;

      i_cOp1=cp_SetTrsOp(this.w_MT__FLAG,'MT_SALDO','1',1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MTRIFNUM)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" MT_SALDO="+i_cOp1  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE MTNUMRIF="+cp_ToStrODBC(this.w_MTRIFNUM);
           +" AND MTSERIAL="+cp_ToStrODBC(this.w_MTSERRIF);
           +" AND MTROWNUM="+cp_ToStrODBC(this.w_MTROWRIF);
           +" AND MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL);
           +" AND MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MT__FLAG,'MT_SALDO','1',1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'MTSERIAL',this.w_MTSERRIF;
                 ,'MTROWNUM',this.w_MTROWRIF;
                 ,'MTKEYSAL',this.w_MTKEYSAL;
                 ,'MTCODMAT',this.w_MTCODMAT;
                 ,'MTNUMRIF',this.w_MTRIFNUM)
      UPDATE (i_cTable) SET;
           MT_SALDO=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.SALLOTUBI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALLOTUBI_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MT__FLAG,space(1))==this.w_MT__FLAG;
              and NVL(&i_cF..MTRIFSTO,0)==this.w_MTRIFSTO;
              and NVL(&i_cF..MTSERRIF,space(10))==this.w_MTSERRIF;
              and NVL(&i_cF..MTROWRIF,0)==this.w_MTROWRIF;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;

      i_cOp1=cp_SetTrsOp(this.w_MT__FLAG,'SU_SALDO','1',1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MTRIFSTO)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SU_SALDO="+i_cOp1  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SUNUMRIF="+cp_ToStrODBC(this.w_MTRIFSTO);
           +" AND SUSERRIF="+cp_ToStrODBC(this.w_MTSERRIF);
           +" AND SUROWRIF="+cp_ToStrODBC(this.w_MTROWRIF);
           +" AND SUCODICE="+cp_ToStrODBC(this.w_MTKEYSAL);
           +" AND SUCODMAT="+cp_ToStrODBC(this.w_MTCODMAT);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MT__FLAG,'SU_SALDO','1',1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SUSERRIF',this.w_MTSERRIF;
                 ,'SUROWRIF',this.w_MTROWRIF;
                 ,'SUCODICE',this.w_MTKEYSAL;
                 ,'SUCODMAT',this.w_MTCODMAT;
                 ,'SUNUMRIF',this.w_MTRIFSTO)
      UPDATE (i_cTable) SET;
           SU_SALDO=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AGG_MATR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_MATR_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MTCODMAT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete AGG_MATR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MTKEYSAL=&i_TN.->MTKEYSAL;
                              and MTCODMAT=&i_TN.->MTCODMAT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MTCODMAT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AGG_MATR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_MATR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
          .link_2_3('Full')
          .link_2_4('Full')
        .DoRTCalc(8,9,.t.)
          .w_CODART = .w_CODART
        .DoRTCalc(11,14,.t.)
          .w_MGUBIC = .w_MGUBIC
          .w_FLLOTT = .w_FLLOTT
        if .o_MTCODMAT<>.w_MTCODMAT
          .w_TOTALE = .w_TOTALE-.w_riga
          .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
          .w_TOTALE = .w_TOTALE+.w_riga
        endif
        .DoRTCalc(18,21,.t.)
          .w_CODMAG = .w_CODMAG
          .w_ESIRIS = .w_ESIRIS
          .w_KEYSAL = .w_KEYSAL
          .link_2_8('Full')
        .DoRTCalc(26,27,.t.)
          .w_CODFOR = .w_CODFOR
        .DoRTCalc(29,31,.t.)
        if .o_MTCODLOT<>.w_MTCODLOT
          .w_LOTZOOM = .F.
        endif
          .w_MAGUBI = .w_MAGUBI
          .w_SERRIF = .w_SERRIF
          .w_ROWRIF = .w_ROWRIF
          .w_FLRISE = .w_FLRISE
          .w_NUMRIF = .w_NUMRIF
        .DoRTCalc(38,38,.t.)
          .w_DATREG = .w_DATREG
        .DoRTCalc(40,42,.t.)
          .link_1_30('Full')
          .link_1_31('Full')
        .DoRTCalc(45,45,.t.)
          .w_FLPRG = 'L'
          .w_ROWORD = .w_ROWORD
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .DoRTCalc(48,50,.t.)
          .w_NOMSG = .T.
          .w_TIPCON = this.oParentObject .w_STIPCON
          .w_CODCON = this.oParentObject .w_SCODCON
        .DoRTCalc(54,54,.t.)
          .link_1_49('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(56,57,.t.)
          .w_COMCAR = .w_COMCAR
          .w_COMSCA = .w_COMSCA
          .w_MTCODCOM = .w_MTCODCOM
          .w_ARSALCOM = .w_ARSALCOM
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MTKEYSAL with this.w_MTKEYSAL
      replace t_MTCODLOT with this.w_MTCODLOT
      replace t_MTCODUBI with this.w_MTCODUBI
      replace t_MTSERRIF with this.w_MTSERRIF
      replace t_MTROWRIF with this.w_MTROWRIF
      replace t_RIGA with this.w_RIGA
      replace t_MTRIFNUM with this.w_MTRIFNUM
      replace t_MT__FLAG with this.w_MT__FLAG
      replace t_MT_SALDO with this.w_MT_SALDO
      replace t_ARTLOT with this.w_ARTLOT
      replace t_VARLOT with this.w_VARLOT
      replace t_STALOT with this.w_STALOT
      replace t_LOTZOOM with this.w_LOTZOOM
      replace t_NOMSG with this.w_NOMSG
      replace t_MTRIFSTO with this.w_MTRIFSTO
      replace t_MTCODCOM with this.w_MTCODCOM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMTMAGCAR_1_13.visible=!this.oPgFrm.Page1.oPag.oMTMAGCAR_1_13.mHide()
    this.oPgFrm.Page1.oPag.oMTFLSCAR_1_14.visible=!this.oPgFrm.Page1.oPag.oMTFLSCAR_1_14.mHide()
    this.oPgFrm.Page1.oPag.oMTMAGSCA_1_15.visible=!this.oPgFrm.Page1.oPag.oMTMAGSCA_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oMTFLCARI_1_18.visible=!this.oPgFrm.Page1.oPag.oMTFLCARI_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MTCODLOT
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE,LOCODART,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_MTCODLOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_MTCODLOT)
            select LOCODICE,LOCODART,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
      this.w_FLSTAT = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MTCODLOT = space(20)
      endif
      this.w_ARTLOT = space(20)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MTCODUBI
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_MTCODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGUBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MAGUBI;
                       ,'UBCODICE',this.w_MTCODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MTCODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MTRIFNUM
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOVIMATR_IDX,3]
    i_lTable = "MOVIMATR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2], .t., this.MOVIMATR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTRIFNUM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTRIFNUM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTSERIAL,MTROWNUM,MTKEYSAL,MTCODMAT,MTNUMRIF";
                   +" from "+i_cTable+" "+i_lTable+" where MTNUMRIF="+cp_ToStrODBC(this.w_MTRIFNUM);
                   +" and MTSERIAL="+cp_ToStrODBC(this.w_MTSERRIF);
                   +" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWRIF);
                   +" and MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL);
                   +" and MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTSERIAL',this.w_MTSERRIF;
                       ,'MTROWNUM',this.w_MTROWRIF;
                       ,'MTKEYSAL',this.w_MTKEYSAL;
                       ,'MTCODMAT',this.w_MTCODMAT;
                       ,'MTNUMRIF',this.w_MTRIFNUM)
            select MTSERIAL,MTROWNUM,MTKEYSAL,MTCODMAT,MTNUMRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTRIFNUM = NVL(_Link_.MTNUMRIF,0)
    else
      if i_cCtrl<>'Load'
        this.w_MTRIFNUM = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOVIMATR_IDX,2])+'\'+cp_ToStr(_Link_.MTSERIAL,1)+'\'+cp_ToStr(_Link_.MTROWNUM,1)+'\'+cp_ToStr(_Link_.MTKEYSAL,1)+'\'+cp_ToStr(_Link_.MTCODMAT,1)+'\'+cp_ToStr(_Link_.MTNUMRIF,1)
      cp_ShowWarn(i_cKey,this.MOVIMATR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTRIFNUM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLOTTES
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLOTTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLOTTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE,LOCODART";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_CODLOTTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_CODLOTTES)
            select LOCODICE,LOCODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLOTTES = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOTTES = NVL(_Link_.LOCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODLOTTES = space(20)
      endif
      this.w_ARTLOTTES = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLOTTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBITES
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBITES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBITES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBITES);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGUBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MAGUBI;
                       ,'UBCODICE',this.w_CODUBITES)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBITES = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBITES = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBITES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MTRIFSTO
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALLOTUBI_IDX,3]
    i_lTable = "SALLOTUBI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALLOTUBI_IDX,2], .t., this.SALLOTUBI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALLOTUBI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTRIFSTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTRIFSTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SUSERRIF,SUROWRIF,SUCODICE,SUCODMAT,SUNUMRIF";
                   +" from "+i_cTable+" "+i_lTable+" where SUNUMRIF="+cp_ToStrODBC(this.w_MTRIFSTO);
                   +" and SUSERRIF="+cp_ToStrODBC(this.w_MTSERRIF);
                   +" and SUROWRIF="+cp_ToStrODBC(this.w_MTROWRIF);
                   +" and SUCODICE="+cp_ToStrODBC(this.w_MTKEYSAL);
                   +" and SUCODMAT="+cp_ToStrODBC(this.w_MTCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SUSERRIF',this.w_MTSERRIF;
                       ,'SUROWRIF',this.w_MTROWRIF;
                       ,'SUCODICE',this.w_MTKEYSAL;
                       ,'SUCODMAT',this.w_MTCODMAT;
                       ,'SUNUMRIF',this.w_MTRIFSTO)
            select SUSERRIF,SUROWRIF,SUCODICE,SUCODMAT,SUNUMRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTRIFSTO = NVL(_Link_.SUNUMRIF,0)
    else
      if i_cCtrl<>'Load'
        this.w_MTRIFSTO = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALLOTUBI_IDX,2])+'\'+cp_ToStr(_Link_.SUSERRIF,1)+'\'+cp_ToStr(_Link_.SUROWRIF,1)+'\'+cp_ToStr(_Link_.SUCODICE,1)+'\'+cp_ToStr(_Link_.SUCODMAT,1)+'\'+cp_ToStr(_Link_.SUNUMRIF,1)
      cp_ShowWarn(i_cKey,this.SALLOTUBI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTRIFSTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oUNIMIS1_1_8.value==this.w_UNIMIS1)
      this.oPgFrm.Page1.oPag.oUNIMIS1_1_8.value=this.w_UNIMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAUM1_1_10.value==this.w_QTAUM1)
      this.oPgFrm.Page1.oPag.oQTAUM1_1_10.value=this.w_QTAUM1
    endif
    if not(this.oPgFrm.Page1.oPag.oMTMAGCAR_1_13.value==this.w_MTMAGCAR)
      this.oPgFrm.Page1.oPag.oMTMAGCAR_1_13.value=this.w_MTMAGCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oMTFLSCAR_1_14.RadioValue()==this.w_MTFLSCAR)
      this.oPgFrm.Page1.oPag.oMTFLSCAR_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMTMAGSCA_1_15.value==this.w_MTMAGSCA)
      this.oPgFrm.Page1.oPag.oMTMAGSCA_1_15.value=this.w_MTMAGSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oMTFLCARI_1_18.RadioValue()==this.w_MTFLCARI)
      this.oPgFrm.Page1.oPag.oMTFLCARI_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oROWORD_1_34.value==this.w_ROWORD)
      this.oPgFrm.Page1.oPag.oROWORD_1_34.value=this.w_ROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_2.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_2.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value==this.w_MTCODMAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value=this.w_MTCODMAT
      replace t_MTCODMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value
    endif
    cp_SetControlsValueExtFlds(this,'AGG_MATR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsmd_mam
        * Valorizzo con w_TOTALE la variabile w_TOTMAT
        * del padre (solo se a video)
        IF this.bOnScreen
            Local L_Area
            L_area=Select()
            Select(This.oParentObject.cTrsName)
            Replace t_TOTMAT with this.w_TOTALE
            This.oParentObject.WorkFromTrs()
            Select(L_Area)
        endif
      
        IF (.w_QTAUM1<>.w_TOTALE) and i_Bres And .bOnScreen
         if Not ah_YESNO("Il numero di matricole movimentate deve coincidere con la quantit� di riga non sar� possibile confermare la registrazione. Confermi ugualmente?")
           i_Bres=.f.
         endif
        endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_MTCODMAT))
        * --- Area Manuale = Check Row
        * --- gsmd_mam
        * Controllo che la matricola inserita non sia gia presente
        * nel dettaglio
        Local L_Posizione,L_Area,L_CodMat,L_Trovato,L_oldcodmat
        
        L_Area = Select()
        
        Select (this.cTrsName)
        L_Posizione=Recno(this.cTrsName)
        L_CodMat= t_MTCODMAT
        L_oldcodmat = MTCODMAT
        
        if L_Posizione<>0 And Not Empty(L_CodMat)
        	
        	Go Top
        	
        	LOCATE FOR t_MTCODMAT = L_CodMat And Not Deleted()
        	
        	L_Trovato=Found()
        	
        	if L_Trovato And Recno()<>L_Posizione
        		i_bRes = .f.
        		i_bnoChk = .f.
        		i_cErrorMsg = Ah_MsgFormat("Matricola gi� utilizzata nel dettaglio")
        	ELse
        		if L_Trovato
        			Continue
        			if Found() And Recno()<>L_Posizione
        				i_bRes = .f.
        				i_bnoChk = .f.
        				i_cErrorMsg = Ah_MsgFormat("Matricola gi� utilizzata nel dettaglio")
        			endif
        		Endif
        	Endif
        
        	* mi riposiziono nella riga di partenza
        	Select (this.cTrsName)
        	Go L_Posizione
        endif
        			
        * mi rimetto nella vecchia area
        Select (L_Area)
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MTCODMAT = this.w_MTCODMAT
    this.o_MTCODLOT = this.w_MTCODLOT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MTCODMAT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MTKEYSAL=space(20)
      .w_MTCODMAT=space(40)
      .w_MTCODLOT=space(20)
      .w_MTCODUBI=space(20)
      .w_MTSERRIF=space(10)
      .w_MTROWRIF=0
      .w_RIGA=0
      .w_MTRIFNUM=0
      .w_MT__FLAG=space(1)
      .w_MT_SALDO=0
      .w_ARTLOT=space(20)
      .w_VARLOT=space(20)
      .w_STALOT=space(1)
      .w_LOTZOOM=.f.
      .w_NOMSG=.f.
      .w_MTRIFSTO=0
      .w_MTCODCOM=space(15)
      .DoRTCalc(1,3,.f.)
        .w_MTKEYSAL = .w_KEYSAL
      .DoRTCalc(5,6,.f.)
      if not(empty(.w_MTCODLOT))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_MTCODUBI))
        .link_2_4('Full')
      endif
      .DoRTCalc(8,8,.f.)
        .w_MTROWRIF = 0
      .DoRTCalc(10,16,.f.)
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
      .DoRTCalc(18,25,.f.)
      if not(empty(.w_MTRIFNUM))
        .link_2_8('Full')
      endif
        .w_MT__FLAG = ' '
      .DoRTCalc(27,31,.f.)
        .w_LOTZOOM = .F.
      .DoRTCalc(33,50,.f.)
        .w_NOMSG = .T.
      .DoRTCalc(52,55,.f.)
      if not(empty(.w_MTRIFSTO))
        .link_1_49('Full')
      endif
      .DoRTCalc(56,59,.f.)
        .w_MTCODCOM = .w_MTCODCOM
    endwith
    this.DoRTCalc(61,61,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MTKEYSAL = t_MTKEYSAL
    this.w_MTCODMAT = t_MTCODMAT
    this.w_MTCODLOT = t_MTCODLOT
    this.w_MTCODUBI = t_MTCODUBI
    this.w_MTSERRIF = t_MTSERRIF
    this.w_MTROWRIF = t_MTROWRIF
    this.w_RIGA = t_RIGA
    this.w_MTRIFNUM = t_MTRIFNUM
    this.w_MT__FLAG = t_MT__FLAG
    this.w_MT_SALDO = t_MT_SALDO
    this.w_ARTLOT = t_ARTLOT
    this.w_VARLOT = t_VARLOT
    this.w_STALOT = t_STALOT
    this.w_LOTZOOM = t_LOTZOOM
    this.w_NOMSG = t_NOMSG
    this.w_MTRIFSTO = t_MTRIFSTO
    this.w_MTCODCOM = t_MTCODCOM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MTKEYSAL with this.w_MTKEYSAL
    replace t_MTCODMAT with this.w_MTCODMAT
    replace t_MTCODLOT with this.w_MTCODLOT
    replace t_MTCODUBI with this.w_MTCODUBI
    replace t_MTSERRIF with this.w_MTSERRIF
    replace t_MTROWRIF with this.w_MTROWRIF
    replace t_RIGA with this.w_RIGA
    replace t_MTRIFNUM with this.w_MTRIFNUM
    replace t_MT__FLAG with this.w_MT__FLAG
    replace t_MT_SALDO with this.w_MT_SALDO
    replace t_ARTLOT with this.w_ARTLOT
    replace t_VARLOT with this.w_VARLOT
    replace t_STALOT with this.w_STALOT
    replace t_LOTZOOM with this.w_LOTZOOM
    replace t_NOMSG with this.w_NOMSG
    replace t_MTRIFSTO with this.w_MTRIFSTO
    replace t_MTCODCOM with this.w_MTCODCOM
    if i_srv='A'
      replace MTKEYSAL with this.w_MTKEYSAL
      replace MTCODMAT with this.w_MTCODMAT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_riga
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsmd_mamPag1 as StdContainer
  Width  = 567
  height = 321
  stdWidth  = 567
  stdheight = 321
  resizeXpos=226
  resizeYpos=192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUNIMIS1_1_8 as StdField with uid="CXTKOINJOE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_UNIMIS1", cQueryName = "UNIMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 266889146,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=267, Top=5, InputMask=replicate('X',3)

  add object oQTAUM1_1_10 as StdField with uid="RTHDOFNRAY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_QTAUM1", cQueryName = "QTAUM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� movimentata nella prima unit� di misura",;
    HelpContextID = 241114630,;
   bGlobalFont=.t.,;
    Height=21, Width=85, Left=343, Top=5, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  add object oMTMAGCAR_1_13 as StdField with uid="MERVSHUWOA",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MTMAGCAR", cQueryName = "MTMAGCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di carico",;
    HelpContextID = 1319400,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=74, Top=27, InputMask=replicate('X',5)

  func oMTMAGCAR_1_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_MTMAGCAR))
    endwith
    endif
  endfunc


  add object oMTFLSCAR_1_14 as StdCombo with uid="SZORIXUVXK",rtseq=19,rtrep=.f.,left=138,top=5,width=89,height=21, enabled=.f.;
    , ToolTipText = "Tipo di scarico";
    , HelpContextID = 256479720;
    , cFormVar="w_MTFLSCAR",RowSource=""+"Riservato,"+"Esistenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMTFLSCAR_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MTFLSCAR,&i_cF..t_MTFLSCAR),this.value)
    return(iif(xVal =1,'R',;
    iif(xVal =2,'E',;
    space(1))))
  endfunc
  func oMTFLSCAR_1_14.GetRadio()
    this.Parent.oContained.w_MTFLSCAR = this.RadioValue()
    return .t.
  endfunc

  func oMTFLSCAR_1_14.ToRadio()
    this.Parent.oContained.w_MTFLSCAR=trim(this.Parent.oContained.w_MTFLSCAR)
    return(;
      iif(this.Parent.oContained.w_MTFLSCAR=='R',1,;
      iif(this.Parent.oContained.w_MTFLSCAR=='E',2,;
      0)))
  endfunc

  func oMTFLSCAR_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMTFLSCAR_1_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_MTMAGSCA))
    endwith
    endif
  endfunc

  add object oMTMAGSCA_1_15 as StdField with uid="VUEYCRWZPJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MTMAGSCA", cQueryName = "MTMAGSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 1319417,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=74, Top=5, InputMask=replicate('X',5)

  func oMTMAGSCA_1_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_MTMAGSCA))
    endwith
    endif
  endfunc


  add object oMTFLCARI_1_18 as StdCombo with uid="BGKPGJQOGV",rtseq=21,rtrep=.f.,left=138,top=27,width=89,height=21, enabled=.f.;
    , ToolTipText = "Tipo di carico";
    , HelpContextID = 38375921;
    , cFormVar="w_MTFLCARI",RowSource=""+"Riservato,"+"Esistenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMTFLCARI_1_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MTFLCARI,&i_cF..t_MTFLCARI),this.value)
    return(iif(xVal =1,'R',;
    iif(xVal =2,'E',;
    space(1))))
  endfunc
  func oMTFLCARI_1_18.GetRadio()
    this.Parent.oContained.w_MTFLCARI = this.RadioValue()
    return .t.
  endfunc

  func oMTFLCARI_1_18.ToRadio()
    this.Parent.oContained.w_MTFLCARI=trim(this.Parent.oContained.w_MTFLCARI)
    return(;
      iif(this.Parent.oContained.w_MTFLCARI=='R',1,;
      iif(this.Parent.oContained.w_MTFLCARI=='E',2,;
      0)))
  endfunc

  func oMTFLCARI_1_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMTFLCARI_1_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_MTMAGCAR))
    endwith
    endif
  endfunc

  add object oROWORD_1_34 as StdField with uid="LXQBQSVLVB",rtseq=47,rtrep=.f.,;
    cFormVar = "w_ROWORD", cQueryName = "ROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240486122,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=153, Top=271


  add object oBtn_1_36 as StdButton with uid="IAEEBJPUON",left=7, top=271, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per attivare inserimento rapido";
    , HelpContextID = 2737626;
    , TabStop=.f.,Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        .NotifyEvent("CarRapido")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TOTALE>=.w_QTAUM1)
    endwith
   endif
  endfunc


  add object oObj_1_38 as cp_runprogram with uid="RIFVIWSAID",left=4, top=398, width=188,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('CARRAPIDO',w_ARCLAMAT,w_ARSALCOM,w_CMFLAUTO)",;
    cEvent = "CarRapido",;
    nPag=1;
    , ToolTipText = "lancia il caricamento rapido";
    , HelpContextID = 261169997


  add object oObj_1_39 as cp_runprogram with uid="AWQPAKNIED",left=192, top=344, width=188,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('UPDATEROW',w_ARCLAMAT,w_ARSALCOM,w_CMFLAUTO)",;
    cEvent = "Update row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la modifica della matricola";
    , HelpContextID = 261169997


  add object oObj_1_40 as cp_runprogram with uid="ACNKDSSMKQ",left=3, top=342, width=188,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('DELETEROW',w_ARCLAMAT,w_ARSALCOM,w_CMFLAUTO)",;
    cEvent = "Delete row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la cancellazione della matricola";
    , HelpContextID = 261169997


  add object oObj_1_41 as cp_runprogram with uid="VECODULZZQ",left=4, top=371, width=188,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('INSERTROW',w_ARCLAMAT,w_ARSALCOM,w_CMFLAUTO)",;
    cEvent = "Insert row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la cancellazione della matricola";
    , HelpContextID = 261169997


  add object oObj_1_42 as cp_runprogram with uid="DHTIGXOYWX",left=4, top=423, width=188,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('CARMAT',w_ARCLAMAT,w_ARSALCOM,w_CMFLAUTO)",;
    cEvent = "CarMat",;
    nPag=1;
    , ToolTipText = "lancia il caricamento dettaglio";
    , HelpContextID = 261169997


  add object oObj_1_43 as cp_runprogram with uid="ZRZPFWLUDD",left=383, top=345, width=225,height=24,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('CHANGED',w_ARCLAMAT,w_ARSALCOM,w_CMFLAUTO)",;
    cEvent = "w_MTCODMAT Changed",;
    nPag=1;
    , ToolTipText = "Al cambio della matricola effettua tutti i controlli pi� eventuali letture";
    , HelpContextID = 261169997


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=56, width=555,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=1,Field1="MTCODMAT",Label1="Matricola",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 83042694

  add object oStr_1_7 as StdString with uid="MZWIFWFMHD",Visible=.t., Left=233, Top=8,;
    Alignment=1, Width=27, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="OSJFPVSUNF",Visible=.t., Left=306, Top=8,;
    Alignment=1, Width=35, Height=18,;
    Caption="Qt�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ICCPJRRUUQ",Visible=.t., Left=13, Top=30,;
    Alignment=1, Width=56, Height=18,;
    Caption="Carico:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (Empty(.w_MTMAGCAR))
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="HOXNHFDEYL",Visible=.t., Left=8, Top=7,;
    Alignment=1, Width=61, Height=16,;
    Caption="Scarico:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (Empty(.w_MTMAGSCA))
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="OWVFYBIINW",Visible=.t., Left=113, Top=271,;
    Alignment=1, Width=36, Height=18,;
    Caption="Riga:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=75,;
    width=551+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=76,width=550+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_2 as StdField with uid="DBXZGNSDJC",rtseq=48,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    ToolTipText = "Numero totale di matricole movimentate",;
    HelpContextID = 230930122,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=463, Top=271

  add object oStr_3_1 as StdString with uid="PTKMNIEUIA",Visible=.t., Left=280, Top=271,;
    Alignment=1, Width=177, Height=18,;
    Caption="Totale matricole movimentate:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsmd_mamBodyRow as CPBodyRowCnt
  Width=541
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMTCODMAT_2_2 as StdTrsField with uid="SPOHKTCFXW",rtseq=5,rtrep=.t.,;
    cFormVar="w_MTCODMAT",value=space(40),isprimarykey=.t.,;
    HelpContextID = 104251878,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=538, Left=-2, Top=0, InputMask=replicate('X',40), bHasZoom = .t. 

  func oMTCODMAT_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_MTRIFNUM)
        bRes2=.link_2_8('Full')
      endif
      if .not. empty(.w_MTRIFSTO)
        bRes2=.link_1_49('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMTCODMAT_2_2.mZoom
      with this.Parent.oContained
        GSMD_BGM(this.Parent.oContained,"ZOOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  add object oLast as LastKeyMover
  * ---
  func oMTCODMAT_2_2.When()
    return(.t.)
  proc oMTCODMAT_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMTCODMAT_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_mam','AGG_MATR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MTSERIAL=AGG_MATR.MTSERIAL";
  +" and "+i_cAliasName2+".MTROWNUM=AGG_MATR.MTROWNUM";
  +" and "+i_cAliasName2+".MTNUMRIF=AGG_MATR.MTNUMRIF";
  +" and "+i_cAliasName2+".MTROWLOT=AGG_MATR.MTROWLOT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
