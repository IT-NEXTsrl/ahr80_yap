* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_spl                                                        *
*              Stampa picking lotti                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_68]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-25                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_spl",oParentObject))

* --- Class definition
define class tgsmd_spl as StdForm
  Top    = 16
  Left   = 9

  * --- Standard Properties
  Width  = 589
  Height = 363
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=127251607
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  LOTTIART_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  MAGAZZIN_IDX = 0
  UNIT_LOG_IDX = 0
  cPrg = "gsmd_spl"
  cComment = "Stampa picking lotti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODMAG = space(5)
  w_CODFAM = space(5)
  w_CODGRU = space(5)
  w_CODCAT = space(5)
  w_CODMAR = space(5)
  w_CODART = space(20)
  o_CODART = space(20)
  w_DESART = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLOTT = space(1)
  w_LOTTO = space(20)
  o_LOTTO = space(20)
  w_DATCRE = ctod('  /  /  ')
  w_DATSCA = ctod('  /  /  ')
  w_STATO = space(1)
  w_SCADENZA = space(1)
  o_SCADENZA = space(1)
  w_GIORNI = 0
  w_FLUNILOG = space(1)
  o_FLUNILOG = space(1)
  w_UNILOGINI = space(18)
  w_UNILOGFIN = space(18)
  w_FLARSTO = space(1)
  w_DESFAM = space(35)
  w_DESGRU = space(35)
  w_DESCAT = space(35)
  w_DESMAR = space(35)
  w_OGGI = ctod('  /  /  ')
  w_DESMAG = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_splPag1","gsmd_spl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODMAG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='UNIT_LOG'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODMAG=space(5)
      .w_CODFAM=space(5)
      .w_CODGRU=space(5)
      .w_CODCAT=space(5)
      .w_CODMAR=space(5)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLOTT=space(1)
      .w_LOTTO=space(20)
      .w_DATCRE=ctod("  /  /  ")
      .w_DATSCA=ctod("  /  /  ")
      .w_STATO=space(1)
      .w_SCADENZA=space(1)
      .w_GIORNI=0
      .w_FLUNILOG=space(1)
      .w_UNILOGINI=space(18)
      .w_UNILOGFIN=space(18)
      .w_FLARSTO=space(1)
      .w_DESFAM=space(35)
      .w_DESGRU=space(35)
      .w_DESCAT=space(35)
      .w_DESMAR=space(35)
      .w_OGGI=ctod("  /  /  ")
      .w_DESMAG=space(30)
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODMAG))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFAM))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODGRU))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODCAT))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODMAR))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODART))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(9,10,.f.)
        .w_LOTTO = SPACE(20)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_LOTTO))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,13,.f.)
        .w_STATO = IIF(EMPTY(.w_LOTTO),'D',.w_STATO)
        .w_SCADENZA = "T"
        .w_GIORNI = IIF(.w_SCADENZA = "S",1,0)
        .w_FLUNILOG = 'T'
        .w_UNILOGINI = space(18)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_UNILOGINI))
          .link_1_18('Full')
        endif
        .w_UNILOGFIN = space(18)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_UNILOGFIN))
          .link_1_19('Full')
        endif
        .w_FLARSTO = 'N'
          .DoRTCalc(21,24,.f.)
        .w_OGGI = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
    endwith
    this.DoRTCalc(26,26,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
        if .o_CODART<>.w_CODART
            .w_LOTTO = SPACE(20)
          .link_1_11('Full')
        endif
        .DoRTCalc(12,13,.t.)
        if .o_LOTTO<>.w_LOTTO
            .w_STATO = IIF(EMPTY(.w_LOTTO),'D',.w_STATO)
        endif
        .DoRTCalc(15,15,.t.)
        if .o_SCADENZA<>.w_SCADENZA
            .w_GIORNI = IIF(.w_SCADENZA = "S",1,0)
        endif
        .DoRTCalc(17,17,.t.)
        if .o_FLUNILOG<>.w_FLUNILOG
            .w_UNILOGINI = space(18)
          .link_1_18('Full')
        endif
        if .o_FLUNILOG<>.w_FLUNILOG
            .w_UNILOGFIN = space(18)
          .link_1_19('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLOTTO_1_11.enabled = this.oPgFrm.Page1.oPag.oLOTTO_1_11.mCond()
    this.oPgFrm.Page1.oPag.oSTATO_1_14.enabled = this.oPgFrm.Page1.oPag.oSTATO_1_14.mCond()
    this.oPgFrm.Page1.oPag.oSCADENZA_1_15.enabled = this.oPgFrm.Page1.oPag.oSCADENZA_1_15.mCond()
    this.oPgFrm.Page1.oPag.oUNILOGINI_1_18.enabled = this.oPgFrm.Page1.oPag.oUNILOGINI_1_18.mCond()
    this.oPgFrm.Page1.oPag.oUNILOGFIN_1_19.enabled = this.oPgFrm.Page1.oPag.oUNILOGFIN_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oGIORNI_1_16.visible=!this.oPgFrm.Page1.oPag.oGIORNI_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_1'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( FADESCRI like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%")+cp_TransWhereFldName('FADESCRI',trim(this.w_CODFAM))+")";

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_CODFAM)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_2'),i_cWhere,'GSAR_AFA',"Elenco famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( GMDESCRI like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%")+cp_TransWhereFldName('GMDESCRI',trim(this.w_CODGRU))+")";

            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GMDESCRI like "+cp_ToStr(trim(this.w_CODGRU)+"%");

            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCODGRU_1_3'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( OMDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%")+cp_TransWhereFldName('OMDESCRI',trim(this.w_CODCAT))+")";

            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OMDESCRI like "+cp_ToStr(trim(this.w_CODCAT)+"%");

            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_1_4'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_CODMAR)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_1_5'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_6'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSMD_ALO.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLOTT = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLOTT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLOTT$ 'SC'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente, obsoleto, oppure non gestito a lotti")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLOTT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTO
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTTO)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOCODICE',trim(this.w_LOTTO))
          select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTTO)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTTO) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTTO_1_11'),i_cWhere,'GSMD_ALO',"Lotti",'GSMD_SPL.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTTO);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_LOTTO)
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTO = NVL(_Link_.LOCODICE,space(20))
      this.w_DATCRE = NVL(cp_ToDate(_Link_.LODATCRE),ctod("  /  /  "))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
      this.w_STATO = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTO = space(20)
      endif
      this.w_DATCRE = ctod("  /  /  ")
      this.w_DATSCA = ctod("  /  /  ")
      this.w_STATO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNILOGINI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNILOGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_UNILOGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_UNILOGINI))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UNILOGINI)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UNILOGINI) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oUNILOGINI_1_18'),i_cWhere,'',"Unit� logistiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNILOGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_UNILOGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_UNILOGINI)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNILOGINI = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_UNILOGINI = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_UNILOGINI <= .w_UNILOGFIN or empty(.w_UNILOGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione")
        endif
        this.w_UNILOGINI = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNILOGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNILOGFIN
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNILOGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_UNILOGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_UNILOGFIN))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UNILOGFIN)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UNILOGFIN) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oUNILOGFIN_1_19'),i_cWhere,'',"Unit� logistiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNILOGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_UNILOGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_UNILOGFIN)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNILOGFIN = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_UNILOGFIN = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_UNILOGINI <= .w_UNILOGFIN or empty(.w_UNILOGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione")
        endif
        this.w_UNILOGFIN = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNILOGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_1.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_1.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_2.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_2.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_3.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_3.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_4.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_4.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAR_1_5.value==this.w_CODMAR)
      this.oPgFrm.Page1.oPag.oCODMAR_1_5.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_6.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_6.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_7.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_7.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTO_1_11.value==this.w_LOTTO)
      this.oPgFrm.Page1.oPag.oLOTTO_1_11.value=this.w_LOTTO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCRE_1_12.value==this.w_DATCRE)
      this.oPgFrm.Page1.oPag.oDATCRE_1_12.value=this.w_DATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCA_1_13.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oDATSCA_1_13.value=this.w_DATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_14.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCADENZA_1_15.RadioValue()==this.w_SCADENZA)
      this.oPgFrm.Page1.oPag.oSCADENZA_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIORNI_1_16.value==this.w_GIORNI)
      this.oPgFrm.Page1.oPag.oGIORNI_1_16.value=this.w_GIORNI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLUNILOG_1_17.RadioValue()==this.w_FLUNILOG)
      this.oPgFrm.Page1.oPag.oFLUNILOG_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUNILOGINI_1_18.value==this.w_UNILOGINI)
      this.oPgFrm.Page1.oPag.oUNILOGINI_1_18.value=this.w_UNILOGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oUNILOGFIN_1_19.value==this.w_UNILOGFIN)
      this.oPgFrm.Page1.oPag.oUNILOGFIN_1_19.value=this.w_UNILOGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLARSTO_1_20.RadioValue()==this.w_FLARSTO)
      this.oPgFrm.Page1.oPag.oFLARSTO_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_27.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_27.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_28.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_28.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_29.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_29.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAR_1_30.value==this.w_DESMAR)
      this.oPgFrm.Page1.oPag.oDESMAR_1_30.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_37.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_37.value=this.w_DESMAG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLOTT$ 'SC')  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente, obsoleto, oppure non gestito a lotti")
          case   ((empty(.w_GIORNI)) or not(.w_GIORNI >0))  and not(.w_SCADENZA <> "S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGIORNI_1_16.SetFocus()
            i_bnoObbl = !empty(.w_GIORNI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un numero di giorni positivo")
          case   not(.w_UNILOGINI <= .w_UNILOGFIN or empty(.w_UNILOGFIN))  and (.w_FLUNILOG='D')  and not(empty(.w_UNILOGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUNILOGINI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione")
          case   not(.w_UNILOGINI <= .w_UNILOGFIN or empty(.w_UNILOGFIN))  and (.w_FLUNILOG='D')  and not(empty(.w_UNILOGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUNILOGFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    this.o_LOTTO = this.w_LOTTO
    this.o_SCADENZA = this.w_SCADENZA
    this.o_FLUNILOG = this.w_FLUNILOG
    return

enddefine

* --- Define pages as container
define class tgsmd_splPag1 as StdContainer
  Width  = 585
  height = 363
  stdWidth  = 585
  stdheight = 363
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODMAG_1_1 as StdField with uid="LQULXXYLAQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino selezionato",;
    HelpContextID = 218675162,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=130, Top=8, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oCODFAM_1_2 as StdField with uid="ZWRVWUDXAW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della famiglia",;
    HelpContextID = 118470618,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=130, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Elenco famiglie",'',this.parent.oContained
  endproc
  proc oCODFAM_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oCODGRU_1_3 as StdField with uid="GHRDBKLYUR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del gruppo merceologico",;
    HelpContextID = 234797018,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=130, Top=56, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCODGRU_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oCODGRU_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_CODGRU
     i_obj.ecpSave()
  endproc

  add object oCODCAT_1_4 as StdField with uid="LHAHLEODHE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria omogenea",;
    HelpContextID = 1226714,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=130, Top=80, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCODMAR_1_5 as StdField with uid="ZYLGRLNXGQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della marca",;
    HelpContextID = 34125786,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=130, Top=104, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object oCODART_1_6 as StdField with uid="CFQDOGWTUR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente, obsoleto, oppure non gestito a lotti",;
    ToolTipText = "Codice articolo da stampare",;
    HelpContextID = 251967450,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=130, Top=131, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
      if .not. empty(.w_LOTTO)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODART_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSMD_ALO.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_7 as StdField with uid="VGAIKLUVZE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 251908554,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=283, Top=131, InputMask=replicate('X',40)

  add object oLOTTO_1_11 as StdField with uid="HOXNUBYCWB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_LOTTO", cQueryName = "LOTTO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto selezionato",;
    HelpContextID = 215959734,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=130, Top=156, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTTO"

  func oLOTTO_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODART))
    endwith
   endif
  endfunc

  func oLOTTO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTTO_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTTO_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTTO_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Lotti",'GSMD_SPL.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oLOTTO_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODART
     i_obj.w_LOCODICE=this.parent.oContained.w_LOTTO
     i_obj.ecpSave()
  endproc

  add object oDATCRE_1_12 as StdField with uid="ICMNDWSKOQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATCRE", cQueryName = "DATCRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 234997194,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=357, Top=156

  add object oDATSCA_1_13 as StdField with uid="SEYUFIPHBL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATSCA", cQueryName = "DATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 220084790,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=505, Top=156


  add object oSTATO_1_14 as StdCombo with uid="OYVZGJNQVT",value=4,rtseq=14,rtrep=.f.,left=130,top=181,width=100,height=21;
    , ToolTipText = "Status del lotto";
    , HelpContextID = 215883302;
    , cFormVar="w_STATO",RowSource=""+"Disponibile,"+"Sospeso,"+"Esaurito,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_14.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oSTATO_1_14.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_14.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='D',1,;
      iif(this.Parent.oContained.w_STATO=='S',2,;
      iif(this.Parent.oContained.w_STATO=='E',3,;
      iif(this.Parent.oContained.w_STATO=='',4,;
      0))))
  endfunc

  func oSTATO_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_LOTTO))
    endwith
   endif
  endfunc


  add object oSCADENZA_1_15 as StdCombo with uid="EENNFTAFDS",rtseq=15,rtrep=.f.,left=333,top=182,width=98,height=21;
    , ToolTipText = "Stampa lotti in scadenza, oppure tutti";
    , HelpContextID = 170790247;
    , cFormVar="w_SCADENZA",RowSource=""+"In scadenza,"+"Scaduti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSCADENZA_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSCADENZA_1_15.GetRadio()
    this.Parent.oContained.w_SCADENZA = this.RadioValue()
    return .t.
  endfunc

  func oSCADENZA_1_15.SetRadio()
    this.Parent.oContained.w_SCADENZA=trim(this.Parent.oContained.w_SCADENZA)
    this.value = ;
      iif(this.Parent.oContained.w_SCADENZA=='S',1,;
      iif(this.Parent.oContained.w_SCADENZA=='N',2,;
      iif(this.Parent.oContained.w_SCADENZA=='T',3,;
      0)))
  endfunc

  func oSCADENZA_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_LOTTO))
    endwith
   endif
  endfunc

  add object oGIORNI_1_16 as StdField with uid="WMBCAOGAIX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GIORNI", cQueryName = "GIORNI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un numero di giorni positivo",;
    ToolTipText = "Lotti scadenti entro giorni..",;
    HelpContextID = 171117978,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=505, Top=178, cSayPict='"999"', cGetPict='"999"'

  func oGIORNI_1_16.mHide()
    with this.Parent.oContained
      return (.w_SCADENZA <> "S")
    endwith
  endfunc

  func oGIORNI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GIORNI >0)
    endwith
    return bRes
  endfunc


  add object oFLUNILOG_1_17 as StdCombo with uid="BMZONYGOGL",rtseq=17,rtrep=.f.,left=130,top=205,width=100,height=21;
    , ToolTipText = "Filtra solo documenti, mov. magaz. o tutti";
    , HelpContextID = 142169501;
    , cFormVar="w_FLUNILOG",RowSource=""+"Documenti,"+"Mov. magaz.,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLUNILOG_1_17.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLUNILOG_1_17.GetRadio()
    this.Parent.oContained.w_FLUNILOG = this.RadioValue()
    return .t.
  endfunc

  func oFLUNILOG_1_17.SetRadio()
    this.Parent.oContained.w_FLUNILOG=trim(this.Parent.oContained.w_FLUNILOG)
    this.value = ;
      iif(this.Parent.oContained.w_FLUNILOG=='D',1,;
      iif(this.Parent.oContained.w_FLUNILOG=='M',2,;
      iif(this.Parent.oContained.w_FLUNILOG=='T',3,;
      0)))
  endfunc

  add object oUNILOGINI_1_18 as StdField with uid="CLRLZBEIKM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_UNILOGINI", cQueryName = "UNILOGINI",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione",;
    ToolTipText = "Unit� logistica di inizio selezione",;
    HelpContextID = 64396580,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=130, Top=229, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", oKey_1_1="UL__SSCC", oKey_1_2="this.w_UNILOGINI"

  func oUNILOGINI_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLUNILOG='D')
    endwith
   endif
  endfunc

  func oUNILOGINI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oUNILOGINI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUNILOGINI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIT_LOG','*','UL__SSCC',cp_AbsName(this.parent,'oUNILOGINI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� logistiche",'',this.parent.oContained
  endproc

  add object oUNILOGFIN_1_19 as StdField with uid="HXBCXAVSOR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_UNILOGFIN", cQueryName = "UNILOGFIN",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione",;
    ToolTipText = "Unit� logistica di fine selezione",;
    HelpContextID = 204038801,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=130, Top=257, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", oKey_1_1="UL__SSCC", oKey_1_2="this.w_UNILOGFIN"

  func oUNILOGFIN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLUNILOG='D')
    endwith
   endif
  endfunc

  func oUNILOGFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oUNILOGFIN_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUNILOGFIN_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIT_LOG','*','UL__SSCC',cp_AbsName(this.parent,'oUNILOGFIN_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� logistiche",'',this.parent.oContained
  endproc

  add object oFLARSTO_1_20 as StdCheck with uid="JWFJSBNSHL",rtseq=20,rtrep=.f.,left=331, top=259, caption="Movimenti storicizzati",;
    ToolTipText = "Se attivo la stampa considera anche i movim. di magazzino e i documenti storicizzati",;
    HelpContextID = 249817770,;
    cFormVar="w_FLARSTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLARSTO_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLARSTO_1_20.GetRadio()
    this.Parent.oContained.w_FLARSTO = this.RadioValue()
    return .t.
  endfunc

  func oFLARSTO_1_20.SetRadio()
    this.Parent.oContained.w_FLARSTO=trim(this.Parent.oContained.w_FLARSTO)
    this.value = ;
      iif(this.Parent.oContained.w_FLARSTO=='S',1,;
      0)
  endfunc

  add object oDESFAM_1_27 as StdField with uid="SPMCVKBKOF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 118411722,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=196, Top=32, InputMask=replicate('X',35)

  add object oDESGRU_1_28 as StdField with uid="CFYYVOSFIV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 234738122,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=196, Top=56, InputMask=replicate('X',35)

  add object oDESCAT_1_29 as StdField with uid="NODDAITHOX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1167818,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=196, Top=80, InputMask=replicate('X',35)

  add object oDESMAR_1_30 as StdField with uid="QTSUZDFJJS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34066890,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=196, Top=104, InputMask=replicate('X',35)

  add object oDESMAG_1_37 as StdField with uid="JWEHYDIVPB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 218616266,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=196, Top=8, InputMask=replicate('X',30)


  add object oObj_1_39 as cp_outputCombo with uid="ZLERXIVPWT",left=130, top=285, width=443,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 231621658


  add object oBtn_1_40 as StdButton with uid="GHEVIDFOUV",left=478, top=311, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 267829722;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="GNIBZHELYY",left=529, top=311, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 134569030;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_21 as StdString with uid="INRZPNFMLG",Visible=.t., Left=4, Top=131,;
    Alignment=1, Width=123, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="NBNTRZHWWX",Visible=.t., Left=4, Top=156,;
    Alignment=1, Width=123, Height=15,;
    Caption="Codice lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ZFJAHPFQKF",Visible=.t., Left=287, Top=156,;
    Alignment=1, Width=66, Height=18,;
    Caption="Creato il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JERJEZNWHL",Visible=.t., Left=433, Top=156,;
    Alignment=1, Width=69, Height=18,;
    Caption="Scade il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="BLKTNZLXQT",Visible=.t., Left=4, Top=181,;
    Alignment=1, Width=123, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="FFRASAGOHG",Visible=.t., Left=4, Top=279,;
    Alignment=1, Width=123, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="QXTSUYBMKV",Visible=.t., Left=4, Top=32,;
    Alignment=1, Width=123, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="QMGOJLAWZQ",Visible=.t., Left=4, Top=56,;
    Alignment=1, Width=123, Height=15,;
    Caption="Gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="EPSUAQSQPN",Visible=.t., Left=4, Top=80,;
    Alignment=1, Width=123, Height=15,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="VUZNOWJVAG",Visible=.t., Left=4, Top=104,;
    Alignment=1, Width=123, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="JVEQXXULUD",Visible=.t., Left=413, Top=182,;
    Alignment=1, Width=89, Height=18,;
    Caption="Entro gg:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_SCADENZA <> "S")
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="ZFOEKLYSJC",Visible=.t., Left=4, Top=8,;
    Alignment=1, Width=123, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="VUGOWIAJLF",Visible=.t., Left=4, Top=229,;
    Alignment=1, Width=123, Height=18,;
    Caption="Da unit� log.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="BOBTZDHUXS",Visible=.t., Left=4, Top=257,;
    Alignment=1, Width=123, Height=18,;
    Caption="A unit� log.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="VVJSXHFMRW",Visible=.t., Left=4, Top=205,;
    Alignment=1, Width=123, Height=18,;
    Caption="Filtro unit� log.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_spl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
