* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_asm                                                        *
*              Saldi commessa lotti/ubicazioni                                 *
*                                                                              *
*      Author: Zucchetti Spa - AT                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-28                                                      *
* Last revis.: 2015-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_asm"))

* --- Class definition
define class tgsmd_asm as StdForm
  Top    = -2
  Left   = 4

  * --- Standard Properties
  Width  = 563
  Height = 250+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-19"
  HelpContextID=158708887
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  SALOTCOM_IDX = 0
  MAGAZZIN_IDX = 0
  UBICAZIO_IDX = 0
  LOTTIART_IDX = 0
  ART_ICOL_IDX = 0
  CAN_TIER_IDX = 0
  cFile = "SALOTCOM"
  cKeySelect = "SMCODMAG,SMCODUBI,SMCODART,SMCODCAN,SMCODLOT"
  cKeyWhere  = "SMCODMAG=this.w_SMCODMAG and SMCODUBI=this.w_SMCODUBI and SMCODART=this.w_SMCODART and SMCODCAN=this.w_SMCODCAN and SMCODLOT=this.w_SMCODLOT"
  cKeyWhereODBC = '"SMCODMAG="+cp_ToStrODBC(this.w_SMCODMAG)';
      +'+" and SMCODUBI="+cp_ToStrODBC(this.w_SMCODUBI)';
      +'+" and SMCODART="+cp_ToStrODBC(this.w_SMCODART)';
      +'+" and SMCODCAN="+cp_ToStrODBC(this.w_SMCODCAN)';
      +'+" and SMCODLOT="+cp_ToStrODBC(this.w_SMCODLOT)';

  cKeyWhereODBCqualified = '"SALOTCOM.SMCODMAG="+cp_ToStrODBC(this.w_SMCODMAG)';
      +'+" and SALOTCOM.SMCODUBI="+cp_ToStrODBC(this.w_SMCODUBI)';
      +'+" and SALOTCOM.SMCODART="+cp_ToStrODBC(this.w_SMCODART)';
      +'+" and SALOTCOM.SMCODCAN="+cp_ToStrODBC(this.w_SMCODCAN)';
      +'+" and SALOTCOM.SMCODLOT="+cp_ToStrODBC(this.w_SMCODLOT)';

  cPrg = "gsmd_asm"
  cComment = "Saldi commessa lotti/ubicazioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPOPE = space(4)
  w_SMCODMAG = space(5)
  o_SMCODMAG = space(5)
  w_SMCODUBI = space(20)
  w_CODUBI = space(10)
  o_CODUBI = space(10)
  w_DESMAG = space(30)
  w_SMCODART = space(20)
  o_SMCODART = space(20)
  w_DESART = space(40)
  w_SMCODCAN = space(15)
  w_SMCODLOT = space(20)
  w_CODLOT = space(20)
  o_CODLOT = space(20)
  w_TIPART = space(2)
  w_SMQTAPER = 0
  w_SMQTRPER = 0
  w_DISPOPER = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESUBI = space(40)
  w_FLUBIC = space(1)
  w_FLLOTT = space(1)
  w_SMQTAPRO = 0
  w_SMQTRPRO = 0
  w_DESCAN = space(30)
  w_FLCOMM = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SALOTCOM','gsmd_asm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_asmPag1","gsmd_asm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Saldi")
      .Pages(1).HelpContextID = 7396134
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSMCODMAG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='UBICAZIO'
    this.cWorkTables[3]='LOTTIART'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='SALOTCOM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SALOTCOM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SALOTCOM_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SMCODMAG = NVL(SMCODMAG,space(5))
      .w_SMCODUBI = NVL(SMCODUBI,space(20))
      .w_SMCODART = NVL(SMCODART,space(20))
      .w_SMCODCAN = NVL(SMCODCAN,space(15))
      .w_SMCODLOT = NVL(SMCODLOT,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_8_joined
    link_1_8_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SALOTCOM where SMCODMAG=KeySet.SMCODMAG
    *                            and SMCODUBI=KeySet.SMCODUBI
    *                            and SMCODART=KeySet.SMCODART
    *                            and SMCODCAN=KeySet.SMCODCAN
    *                            and SMCODLOT=KeySet.SMCODLOT
    *
    i_nConn = i_TableProp[this.SALOTCOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALOTCOM_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SALOTCOM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SALOTCOM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SALOTCOM '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SMCODMAG',this.w_SMCODMAG  ,'SMCODUBI',this.w_SMCODUBI  ,'SMCODART',this.w_SMCODART  ,'SMCODCAN',this.w_SMCODCAN  ,'SMCODLOT',this.w_SMCODLOT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESMAG = space(30)
        .w_DESART = space(40)
        .w_TIPART = space(2)
        .w_OBTEST = i_INIDAT
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESUBI = space(40)
        .w_FLUBIC = space(1)
        .w_FLLOTT = space(1)
        .w_DESCAN = space(30)
        .w_FLCOMM = 'S'
        .w_TIPOPE = this.cFunction
        .w_SMCODMAG = NVL(SMCODMAG,space(5))
          if link_1_2_joined
            this.w_SMCODMAG = NVL(MGCODMAG102,NVL(this.w_SMCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG102,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO102),ctod("  /  /  "))
            this.w_FLUBIC = NVL(MGFLUBIC102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_SMCODUBI = NVL(SMCODUBI,space(20))
        .w_CODUBI = IIF (.cFunction<>'Query', SPACE(20),.w_SMCODUBI) 
          .link_1_4('Load')
        .w_SMCODART = NVL(SMCODART,space(20))
          if link_1_6_joined
            this.w_SMCODART = NVL(ARCODART106,NVL(this.w_SMCODART,space(20)))
            this.w_DESART = NVL(ARDESART106,space(40))
            this.w_TIPART = NVL(ARTIPART106,space(2))
            this.w_DATOBSO = NVL(cp_ToDate(ARDTOBSO106),ctod("  /  /  "))
            this.w_FLLOTT = NVL(ARFLLOTT106,space(1))
            this.w_FLCOMM = NVL(ARSALCOM106,space(1))
          else
          .link_1_6('Load')
          endif
        .w_SMCODCAN = NVL(SMCODCAN,space(15))
          if link_1_8_joined
            this.w_SMCODCAN = NVL(CNCODCAN108,NVL(this.w_SMCODCAN,space(15)))
            this.w_DESCAN = NVL(CNDESCAN108,space(30))
          else
          .link_1_8('Load')
          endif
        .w_SMCODLOT = NVL(SMCODLOT,space(20))
        .w_CODLOT = IIF (.cFunction<>'Query', SPACE(20),.w_SMCODLOT) 
          .link_1_10('Load')
        .w_SMQTAPER = NVL(SMQTAPER,0)
        .w_SMQTRPER = NVL(SMQTRPER,0)
        .w_DISPOPER = .w_SMQTAPER-.w_SMQTRPER
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_SMQTAPRO = NVL(SMQTAPRO,0)
        .w_SMQTRPRO = NVL(SMQTRPRO,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'SALOTCOM')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsmd_asm
    this.w_CODUBI=this.w_SMCODUBI
    this.w_CODLOT=this.w_SMCODLOT
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOPE = space(4)
      .w_SMCODMAG = space(5)
      .w_SMCODUBI = space(20)
      .w_CODUBI = space(10)
      .w_DESMAG = space(30)
      .w_SMCODART = space(20)
      .w_DESART = space(40)
      .w_SMCODCAN = space(15)
      .w_SMCODLOT = space(20)
      .w_CODLOT = space(20)
      .w_TIPART = space(2)
      .w_SMQTAPER = 0
      .w_SMQTRPER = 0
      .w_DISPOPER = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESUBI = space(40)
      .w_FLUBIC = space(1)
      .w_FLLOTT = space(1)
      .w_SMQTAPRO = 0
      .w_SMQTRPRO = 0
      .w_DESCAN = space(30)
      .w_FLCOMM = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_TIPOPE = this.cFunction
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_SMCODMAG))
          .link_1_2('Full')
          endif
        .w_SMCODUBI = IIF (.cFunction<>'Query', .w_CODUBI,.w_SMCODUBI)
        .w_CODUBI = IIF (.cFunction<>'Query', SPACE(20),.w_SMCODUBI) 
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CODUBI))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,6,.f.)
          if not(empty(.w_SMCODART))
          .link_1_6('Full')
          endif
        .DoRTCalc(7,8,.f.)
          if not(empty(.w_SMCODCAN))
          .link_1_8('Full')
          endif
        .w_SMCODLOT = IIF (.cFunction<>'Query', .w_CODLOT , .w_SMCODLOT)
        .w_CODLOT = IIF (.cFunction<>'Query', SPACE(20),.w_SMCODLOT) 
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_CODLOT))
          .link_1_10('Full')
          endif
          .DoRTCalc(11,13,.f.)
        .w_DISPOPER = .w_SMQTAPER-.w_SMQTRPER
        .w_OBTEST = i_INIDAT
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
          .DoRTCalc(16,22,.f.)
        .w_FLCOMM = 'S'
      endif
    endwith
    cp_BlankRecExtFlds(this,'SALOTCOM')
    this.DoRTCalc(24,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSMCODMAG_1_2.enabled = i_bVal
      .Page1.oPag.oSMCODUBI_1_3.enabled = i_bVal
      .Page1.oPag.oCODUBI_1_4.enabled = i_bVal
      .Page1.oPag.oSMCODART_1_6.enabled = i_bVal
      .Page1.oPag.oSMCODCAN_1_8.enabled = i_bVal
      .Page1.oPag.oSMCODLOT_1_9.enabled = i_bVal
      .Page1.oPag.oCODLOT_1_10.enabled = i_bVal
      .Page1.oPag.oSMQTAPER_1_12.enabled = i_bVal
      .Page1.oPag.oSMQTRPER_1_13.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSMCODMAG_1_2.enabled = .f.
        .Page1.oPag.oSMCODUBI_1_3.enabled = .f.
        .Page1.oPag.oSMCODART_1_6.enabled = .f.
        .Page1.oPag.oSMCODCAN_1_8.enabled = .f.
        .Page1.oPag.oSMCODLOT_1_9.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSMCODMAG_1_2.enabled = .t.
        .Page1.oPag.oSMCODUBI_1_3.enabled = .t.
        .Page1.oPag.oSMCODART_1_6.enabled = .t.
        .Page1.oPag.oSMCODCAN_1_8.enabled = .t.
        .Page1.oPag.oSMCODLOT_1_9.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SALOTCOM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SALOTCOM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMCODMAG,"SMCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMCODUBI,"SMCODUBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMCODART,"SMCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMCODCAN,"SMCODCAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMCODLOT,"SMCODLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMQTAPER,"SMQTAPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMQTRPER,"SMQTRPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMQTAPRO,"SMQTAPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SMQTRPRO,"SMQTRPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SALOTCOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALOTCOM_IDX,2])
    i_lTable = "SALOTCOM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SALOTCOM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SALOTCOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALOTCOM_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SALOTCOM_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SALOTCOM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SALOTCOM')
        i_extval=cp_InsertValODBCExtFlds(this,'SALOTCOM')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SMCODMAG,SMCODUBI,SMCODART,SMCODCAN,SMCODLOT"+;
                  ",SMQTAPER,SMQTRPER,SMQTAPRO,SMQTRPRO,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_SMCODMAG)+;
                  ","+cp_ToStrODBC(this.w_SMCODUBI)+;
                  ","+cp_ToStrODBCNull(this.w_SMCODART)+;
                  ","+cp_ToStrODBCNull(this.w_SMCODCAN)+;
                  ","+cp_ToStrODBC(this.w_SMCODLOT)+;
                  ","+cp_ToStrODBC(this.w_SMQTAPER)+;
                  ","+cp_ToStrODBC(this.w_SMQTRPER)+;
                  ","+cp_ToStrODBC(this.w_SMQTAPRO)+;
                  ","+cp_ToStrODBC(this.w_SMQTRPRO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SALOTCOM')
        i_extval=cp_InsertValVFPExtFlds(this,'SALOTCOM')
        cp_CheckDeletedKey(i_cTable,0,'SMCODMAG',this.w_SMCODMAG,'SMCODUBI',this.w_SMCODUBI,'SMCODART',this.w_SMCODART,'SMCODCAN',this.w_SMCODCAN,'SMCODLOT',this.w_SMCODLOT)
        INSERT INTO (i_cTable);
              (SMCODMAG,SMCODUBI,SMCODART,SMCODCAN,SMCODLOT,SMQTAPER,SMQTRPER,SMQTAPRO,SMQTRPRO,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SMCODMAG;
                  ,this.w_SMCODUBI;
                  ,this.w_SMCODART;
                  ,this.w_SMCODCAN;
                  ,this.w_SMCODLOT;
                  ,this.w_SMQTAPER;
                  ,this.w_SMQTRPER;
                  ,this.w_SMQTAPRO;
                  ,this.w_SMQTRPRO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SALOTCOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALOTCOM_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SALOTCOM_IDX,i_nConn)
      *
      * update SALOTCOM
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SALOTCOM')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SMQTAPER="+cp_ToStrODBC(this.w_SMQTAPER)+;
             ",SMQTRPER="+cp_ToStrODBC(this.w_SMQTRPER)+;
             ",SMQTAPRO="+cp_ToStrODBC(this.w_SMQTAPRO)+;
             ",SMQTRPRO="+cp_ToStrODBC(this.w_SMQTRPRO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SALOTCOM')
        i_cWhere = cp_PKFox(i_cTable  ,'SMCODMAG',this.w_SMCODMAG  ,'SMCODUBI',this.w_SMCODUBI  ,'SMCODART',this.w_SMCODART  ,'SMCODCAN',this.w_SMCODCAN  ,'SMCODLOT',this.w_SMCODLOT  )
        UPDATE (i_cTable) SET;
              SMQTAPER=this.w_SMQTAPER;
             ,SMQTRPER=this.w_SMQTRPER;
             ,SMQTAPRO=this.w_SMQTAPRO;
             ,SMQTRPRO=this.w_SMQTRPRO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SALOTCOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALOTCOM_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SALOTCOM_IDX,i_nConn)
      *
      * delete SALOTCOM
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SMCODMAG',this.w_SMCODMAG  ,'SMCODUBI',this.w_SMCODUBI  ,'SMCODART',this.w_SMCODART  ,'SMCODCAN',this.w_SMCODCAN  ,'SMCODLOT',this.w_SMCODLOT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SALOTCOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALOTCOM_IDX,2])
    if i_bUpd
      with this
            .w_TIPOPE = this.cFunction
        .DoRTCalc(2,2,.t.)
        if .o_SMCODMAG<>.w_SMCODMAG.or. .o_CODUBI<>.w_CODUBI
            .w_SMCODUBI = IIF (.cFunction<>'Query', .w_CODUBI,.w_SMCODUBI)
        endif
        if .o_SMCODMAG<>.w_SMCODMAG
            .w_CODUBI = IIF (.cFunction<>'Query', SPACE(20),.w_SMCODUBI) 
          .link_1_4('Full')
        endif
        .DoRTCalc(5,8,.t.)
        if .o_SMCODART<>.w_SMCODART.or. .o_CODLOT<>.w_CODLOT
            .w_SMCODLOT = IIF (.cFunction<>'Query', .w_CODLOT , .w_SMCODLOT)
        endif
        if .o_SMCODART<>.w_SMCODART
            .w_CODLOT = IIF (.cFunction<>'Query', SPACE(20),.w_SMCODLOT) 
          .link_1_10('Full')
        endif
        .DoRTCalc(11,13,.t.)
            .w_DISPOPER = .w_SMQTAPER-.w_SMQTRPER
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODUBI_1_4.enabled = this.oPgFrm.Page1.oPag.oCODUBI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSMCODART_1_6.enabled = this.oPgFrm.Page1.oPag.oSMCODART_1_6.mCond()
    this.oPgFrm.Page1.oPag.oSMCODCAN_1_8.enabled = this.oPgFrm.Page1.oPag.oSMCODCAN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCODLOT_1_10.enabled = this.oPgFrm.Page1.oPag.oCODLOT_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSMCODUBI_1_3.visible=!this.oPgFrm.Page1.oPag.oSMCODUBI_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCODUBI_1_4.visible=!this.oPgFrm.Page1.oPag.oCODUBI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oSMCODLOT_1_9.visible=!this.oPgFrm.Page1.oPag.oSMCODLOT_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCODLOT_1_10.visible=!this.oPgFrm.Page1.oPag.oCODLOT_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SMCODMAG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_SMCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_SMCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_SMCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_SMCODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SMCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oSMCODMAG_1_2'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_SMCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_SMCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SMCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.MGCODMAG as MGCODMAG102"+ ",link_1_2.MGDESMAG as MGDESMAG102"+ ",link_1_2.MGDTOBSO as MGDTOBSO102"+ ",link_1_2.MGFLUBIC as MGFLUBIC102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on SALOTCOM.SMCODMAG=link_1_2.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and SALOTCOM.SMCODMAG=link_1_2.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_SMCODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_SMCODMAG;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODMAG,UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_4'),i_cWhere,'GSMD_MUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SMCODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_SMCODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_SMCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_SMCODMAG;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(10))
      this.w_DESUBI = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(10)
      endif
      this.w_DESUBI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMCODART
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SMCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT,ARSALCOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SMCODART))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT,ARSALCOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_SMCODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT,ARSALCOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_SMCODART)+"%");

            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SMCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSMCODART_1_6'),i_cWhere,'GSMA_AAR',"Articoli",'GSMA1ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT,ARSALCOM";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT,ARSALCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SMCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SMCODART)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_FLCOMM = NVL(_Link_.ARSALCOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SMCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPART = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLLOTT = space(1)
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND NVL (.w_FLLOTT , '  ')<>'N' AND .w_FLCOMM='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo incongruente oppure obsoleto")
        endif
        this.w_SMCODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPART = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLLOTT = space(1)
        this.w_FLCOMM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.ARCODART as ARCODART106"+ ",link_1_6.ARDESART as ARDESART106"+ ",link_1_6.ARTIPART as ARTIPART106"+ ",link_1_6.ARDTOBSO as ARDTOBSO106"+ ",link_1_6.ARFLLOTT as ARFLLOTT106"+ ",link_1_6.ARSALCOM as ARSALCOM106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on SALOTCOM.SMCODART=link_1_6.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and SALOTCOM.SMCODART=link_1_6.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SMCODCAN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMCODCAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_SMCODCAN)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_SMCODCAN))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMCODCAN)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMCODCAN) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oSMCODCAN_1_8'),i_cWhere,'GSAR_ACN',"COMMESSE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMCODCAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_SMCODCAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_SMCODCAN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMCODCAN = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_SMCODCAN = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMCODCAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.CNCODCAN as CNCODCAN108"+ ",link_1_8.CNDESCAN as CNDESCAN108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on SALOTCOM.SMCODCAN=link_1_8.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and SALOTCOM.SMCODCAN=link_1_8.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODLOT
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_CODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_SMCODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_SMCODART;
                     ,'LOCODICE',trim(this.w_CODLOT))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oCODLOT_1_10'),i_cWhere,'GSMD_ALO',"Lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SMCODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_SMCODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_CODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_SMCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_SMCODART;
                       ,'LOCODICE',this.w_CODLOT)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLOT = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODLOT = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSMCODMAG_1_2.value==this.w_SMCODMAG)
      this.oPgFrm.Page1.oPag.oSMCODMAG_1_2.value=this.w_SMCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSMCODUBI_1_3.value==this.w_SMCODUBI)
      this.oPgFrm.Page1.oPag.oSMCODUBI_1_3.value=this.w_SMCODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_4.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_4.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_5.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_5.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSMCODART_1_6.value==this.w_SMCODART)
      this.oPgFrm.Page1.oPag.oSMCODART_1_6.value=this.w_SMCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_7.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_7.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oSMCODCAN_1_8.value==this.w_SMCODCAN)
      this.oPgFrm.Page1.oPag.oSMCODCAN_1_8.value=this.w_SMCODCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oSMCODLOT_1_9.value==this.w_SMCODLOT)
      this.oPgFrm.Page1.oPag.oSMCODLOT_1_9.value=this.w_SMCODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLOT_1_10.value==this.w_CODLOT)
      this.oPgFrm.Page1.oPag.oCODLOT_1_10.value=this.w_CODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oSMQTAPER_1_12.value==this.w_SMQTAPER)
      this.oPgFrm.Page1.oPag.oSMQTAPER_1_12.value=this.w_SMQTAPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSMQTRPER_1_13.value==this.w_SMQTRPER)
      this.oPgFrm.Page1.oPag.oSMQTRPER_1_13.value=this.w_SMQTRPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPOPER_1_14.value==this.w_DISPOPER)
      this.oPgFrm.Page1.oPag.oDISPOPER_1_14.value=this.w_DISPOPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUBI_1_25.value==this.w_DESUBI)
      this.oPgFrm.Page1.oPag.oDESUBI_1_25.value=this.w_DESUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oSMQTAPRO_1_29.value==this.w_SMQTAPRO)
      this.oPgFrm.Page1.oPag.oSMQTAPRO_1_29.value=this.w_SMQTAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSMQTRPRO_1_30.value==this.w_SMQTRPRO)
      this.oPgFrm.Page1.oPag.oSMQTRPRO_1_30.value=this.w_SMQTRPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_36.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_36.value=this.w_DESCAN
    endif
    cp_SetControlsValueExtFlds(this,'SALOTCOM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SMCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSMCODMAG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_SMCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_SMCODART)) or not((.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND NVL (.w_FLLOTT , '  ')<>'N' AND .w_FLCOMM='S'))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSMCODART_1_6.SetFocus()
            i_bnoObbl = !empty(.w_SMCODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo incongruente oppure obsoleto")
          case   (empty(.w_SMCODCAN))  and (.cFunction<>'Edit' And .w_FLCOMM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSMCODCAN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_SMCODCAN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsmd_asm
      If i_bRes And !(g_PERUBI='S' Or g_PERLOT='S')
       		i_bRes = .f.
       		i_bnoChk = .f.		
          i_cErrorMsg =Ah_MsgFormat("Gestione lotti ed ubicazioni non attivi a livello di azienda")
      Else
          If i_bRes And Empty(.w_SMCODART + .w_SMCODUBI)
       	  	i_bRes = .f.
       	  	i_bnoChk = .f.		
            i_cErrorMsg = Ah_MsgFormat("Codice lotto o codice ubicazione non impostato")
          Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SMCODMAG = this.w_SMCODMAG
    this.o_CODUBI = this.w_CODUBI
    this.o_SMCODART = this.w_SMCODART
    this.o_CODLOT = this.w_CODLOT
    return

enddefine

* --- Define pages as container
define class tgsmd_asmPag1 as StdContainer
  Width  = 559
  height = 250
  stdWidth  = 559
  stdheight = 250
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSMCODMAG_1_2 as StdField with uid="NADSKCYPRP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SMCODMAG", cQueryName = "SMCODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 83282067,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=96, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_SMCODMAG"

  func oSMCODMAG_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if .not. empty(.w_CODUBI)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSMCODMAG_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMCODMAG_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oSMCODMAG_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oSMCODMAG_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_SMCODMAG
     i_obj.ecpSave()
  endproc

  add object oSMCODUBI_1_3 as StdField with uid="JBCXRJMLVY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SMCODUBI", cQueryName = "SMCODMAG,SMCODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 217499793,;
   bGlobalFont=.t.,;
    Height=21, Width=158, Left=96, Top=36, InputMask=replicate('X',20)

  func oSMCODUBI_1_3.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND  .cFunction<>'Filter' )
    endwith
  endfunc

  add object oCODUBI_1_4 as StdField with uid="SXTGAQFOQS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice ubicazione",;
    HelpContextID = 152090586,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=36, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_SMCODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBI"

  func oCODUBI_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit' And g_PERUBI = 'S' And .w_FLUBIC ='S')
    endwith
   endif
  endfunc

  func oCODUBI_1_4.mHide()
    with this.Parent.oContained
      return (NOT (.cFunction<>'Query' AND  .cFunction<>'Filter' ))
    endwith
  endfunc

  func oCODUBI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_SMCODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_SMCODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_MUB',"Ubicazioni",'',this.parent.oContained
  endproc
  proc oCODUBI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_SMCODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_5 as StdField with uid="VTWFBBMJMA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 187158986,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=171, Top=9, InputMask=replicate('X',30)

  add object oSMCODART_1_6 as StdField with uid="DQHWJQQANT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SMCODART", cQueryName = "SMCODMAG,SMCODUBI,SMCODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo incongruente oppure obsoleto",;
    ToolTipText = "Codice articolo",;
    HelpContextID = 16173190,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=63, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_SMCODART"

  func oSMCODART_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oSMCODART_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
      if .not. empty(.w_CODLOT)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSMCODART_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMCODART_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSMCODART_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Articoli",'GSMA1ADI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oSMCODART_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SMCODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_7 as StdField with uid="PPOWDSBJGJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 220451274,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=263, Top=63, InputMask=replicate('X',40)

  add object oSMCODCAN_1_8 as StdField with uid="SSGBERVKJA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SMCODCAN", cQueryName = "SMCODMAG,SMCODUBI,SMCODART,SMCODCAN",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 251054220,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=90, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_SMCODCAN"

  func oSMCODCAN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit' And .w_FLCOMM='S')
    endwith
   endif
  endfunc

  func oSMCODCAN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMCODCAN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMCODCAN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oSMCODCAN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"COMMESSE",'',this.parent.oContained
  endproc
  proc oSMCODCAN_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_SMCODCAN
     i_obj.ecpSave()
  endproc

  add object oSMCODLOT_1_9 as StdField with uid="MUMIBQMNQZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SMCODLOT", cQueryName = "SMCODMAG,SMCODUBI,SMCODART,SMCODCAN,SMCODLOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 100059270,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=117, InputMask=replicate('X',20)

  func oSMCODLOT_1_9.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND  .cFunction<>'Filter' )
    endwith
  endfunc

  add object oCODLOT_1_10 as StdField with uid="SQAWMUBGXK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODLOT", cQueryName = "CODLOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice ubicazione",;
    HelpContextID = 222935002,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=117, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_SMCODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_CODLOT"

  func oCODLOT_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit' And g_PERLOT = 'S' And (.w_FLLOTT = 'S'  OR  .w_FLLOTT = 'C' ) )
    endwith
   endif
  endfunc

  func oCODLOT_1_10.mHide()
    with this.Parent.oContained
      return (NOT (.cFunction<>'Query' AND  .cFunction<>'Filter' ))
    endwith
  endfunc

  func oCODLOT_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLOT_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLOT_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_SMCODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_SMCODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oCODLOT_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Lotti",'',this.parent.oContained
  endproc
  proc oCODLOT_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_SMCODART
     i_obj.w_LOCODICE=this.parent.oContained.w_CODLOT
     i_obj.ecpSave()
  endproc

  add object oSMQTAPER_1_12 as StdField with uid="HBHFIZDNUI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SMQTAPER", cQueryName = "SMQTAPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza",;
    HelpContextID = 35711112,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=96, Top=170, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oSMQTRPER_1_13 as StdField with uid="WLUPBGAOVV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SMQTRPER", cQueryName = "SMQTRPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą riservata",;
    HelpContextID = 17885320,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=96, Top=194, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oDISPOPER_1_14 as StdField with uid="UZFQQDECWD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DISPOPER", cQueryName = "DISPOPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Differenza fra l'esistenza e la quantitą riservata",;
    HelpContextID = 21286264,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=96, Top=226, cSayPict="v_PQ(17)", cGetPict="v_GQ(17)"


  add object oObj_1_23 as cp_runprogram with uid="UJOBPIBCFS",left=-3, top=265, width=169,height=19,;
    caption='GSMD_BMC',;
   bGlobalFont=.t.,;
    prg="GSMD_BMC('LOTCOM')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 28236969

  add object oDESUBI_1_25 as StdField with uid="ZEZWMNSFIM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESUBI", cQueryName = "DESUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 152031690,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=263, Top=36, InputMask=replicate('X',40)

  add object oSMQTAPRO_1_29 as StdField with uid="LVHUPBAFPS",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SMQTAPRO", cQueryName = "SMQTAPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza movimenti fuori linea",;
    HelpContextID = 35711115,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=297, Top=170, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oSMQTRPRO_1_30 as StdField with uid="JEJLUSICFE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SMQTRPRO", cQueryName = "SMQTRPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą riservata fuori linea",;
    HelpContextID = 17885323,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=297, Top=194, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oDESCAN_1_36 as StdField with uid="BEBTOHVSHC",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 70373834,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=263, Top=90, InputMask=replicate('X',30)

  add object oStr_1_15 as StdString with uid="ZYBQELVOXG",Visible=.t., Left=7, Top=63,;
    Alignment=1, Width=83, Height=15,;
    Caption="Articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="GGRLWGCGFS",Visible=.t., Left=7, Top=9,;
    Alignment=1, Width=83, Height=15,;
    Caption="Magazzino:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="MKHWSLYRMX",Visible=.t., Left=5, Top=194,;
    Alignment=1, Width=85, Height=18,;
    Caption="Riservato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="PKVTUNPKFC",Visible=.t., Left=5, Top=170,;
    Alignment=1, Width=85, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YRDDOINMWV",Visible=.t., Left=5, Top=226,;
    Alignment=1, Width=85, Height=15,;
    Caption="Disponibilitą:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="EUHKXZIIXD",Visible=.t., Left=7, Top=36,;
    Alignment=1, Width=83, Height=15,;
    Caption="Ubicazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="EWZCWQUAAA",Visible=.t., Left=13, Top=147,;
    Alignment=0, Width=88, Height=15,;
    Caption="Saldi in linea"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ZFUAPVOXAT",Visible=.t., Left=215, Top=194,;
    Alignment=1, Width=79, Height=18,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="YLIFYMXWWI",Visible=.t., Left=215, Top=170,;
    Alignment=1, Width=79, Height=18,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="KIMQMNUBME",Visible=.t., Left=261, Top=147,;
    Alignment=0, Width=99, Height=18,;
    Caption="Saldi fuori linea"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="JFJKLBGWZL",Visible=.t., Left=7, Top=117,;
    Alignment=1, Width=83, Height=15,;
    Caption="Lotto:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="VSFYAUAIGM",Visible=.t., Left=22, Top=90,;
    Alignment=1, Width=68, Height=15,;
    Caption="Commessa:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_19 as StdBox with uid="YJGBELFMFO",left=1, top=164, width=535,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_asm','SALOTCOM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SMCODMAG=SALOTCOM.SMCODMAG";
  +" and "+i_cAliasName2+".SMCODUBI=SALOTCOM.SMCODUBI";
  +" and "+i_cAliasName2+".SMCODART=SALOTCOM.SMCODART";
  +" and "+i_cAliasName2+".SMCODCAN=SALOTCOM.SMCODCAN";
  +" and "+i_cAliasName2+".SMCODLOT=SALOTCOM.SMCODLOT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
