* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_amt                                                        *
*              Matricole                                                       *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_63]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-02                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_amt"))

* --- Class definition
define class tgsmd_amt as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 560
  Height = 129+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-05"
  HelpContextID=210389865
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  MATRICOL_IDX = 0
  CMT_DETT_IDX = 0
  ART_ICOL_IDX = 0
  CMT_MAST_IDX = 0
  cFile = "MATRICOL"
  cKeySelect = "AMCODICE,AMKEYSAL"
  cKeyWhere  = "AMCODICE=this.w_AMCODICE and AMKEYSAL=this.w_AMKEYSAL"
  cKeyWhereODBC = '"AMCODICE="+cp_ToStrODBC(this.w_AMCODICE)';
      +'+" and AMKEYSAL="+cp_ToStrODBC(this.w_AMKEYSAL)';

  cKeyWhereODBCqualified = '"MATRICOL.AMCODICE="+cp_ToStrODBC(this.w_AMCODICE)';
      +'+" and MATRICOL.AMKEYSAL="+cp_ToStrODBC(this.w_AMKEYSAL)';

  cPrg = "gsmd_amt"
  cComment = "Matricole"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AMCODICE = space(40)
  w_AMCODART = space(20)
  o_AMCODART = space(20)
  w_AMCLAMAT = space(5)
  o_AMCLAMAT = space(5)
  w_AMDATCRE = ctod('  /  /  ')
  w_AMDATSCA = ctod('  /  /  ')
  w_GESMAT = space(1)
  w_DESART = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CMDESCRI = space(30)
  w_AMKEYSAL = space(20)
  w_AM_PROGR = 0
  w_AM_PRENO = 0
  w_AM_PRODU = 0
  w_UNIMAT = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MATRICOL','gsmd_amt')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_amtPag1","gsmd_amt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Matricola")
      .Pages(1).HelpContextID = 226451778
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAMCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CMT_DETT'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CMT_MAST'
    this.cWorkTables[4]='MATRICOL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MATRICOL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MATRICOL_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AMCODICE = NVL(AMCODICE,space(40))
      .w_AMKEYSAL = NVL(AMKEYSAL,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MATRICOL where AMCODICE=KeySet.AMCODICE
    *                            and AMKEYSAL=KeySet.AMKEYSAL
    *
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MATRICOL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MATRICOL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MATRICOL '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AMCODICE',this.w_AMCODICE  ,'AMKEYSAL',this.w_AMKEYSAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_GESMAT = space(1)
        .w_DESART = space(40)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_CMDESCRI = space(30)
        .w_UNIMAT = space(1)
        .w_AMCODICE = NVL(AMCODICE,space(40))
        .w_AMCODART = NVL(AMCODART,space(20))
          if link_1_2_joined
            this.w_AMCODART = NVL(ARCODART102,NVL(this.w_AMCODART,space(20)))
            this.w_DESART = NVL(ARDESART102,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ARDTOBSO102),ctod("  /  /  "))
            this.w_GESMAT = NVL(ARGESMAT102,space(1))
            this.w_AMCLAMAT = NVL(ARCLAMAT102,space(5))
          else
          .link_1_2('Load')
          endif
        .w_AMCLAMAT = NVL(AMCLAMAT,space(5))
          if link_1_3_joined
            this.w_AMCLAMAT = NVL(CMCODICE103,NVL(this.w_AMCLAMAT,space(5)))
            this.w_CMDESCRI = NVL(CMDESCRI103,space(30))
            this.w_UNIMAT = NVL(CMUNIMAT103,space(1))
          else
          .link_1_3('Load')
          endif
        .w_AMDATCRE = NVL(cp_ToDate(AMDATCRE),ctod("  /  /  "))
        .w_AMDATSCA = NVL(cp_ToDate(AMDATSCA),ctod("  /  /  "))
        .w_AMKEYSAL = NVL(AMKEYSAL,space(20))
        .w_AM_PROGR = NVL(AM_PROGR,0)
        .w_AM_PRENO = NVL(AM_PRENO,0)
        .w_AM_PRODU = NVL(AM_PRODU,0)
        cp_LoadRecExtFlds(this,'MATRICOL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AMCODICE = space(40)
      .w_AMCODART = space(20)
      .w_AMCLAMAT = space(5)
      .w_AMDATCRE = ctod("  /  /  ")
      .w_AMDATSCA = ctod("  /  /  ")
      .w_GESMAT = space(1)
      .w_DESART = space(40)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_CMDESCRI = space(30)
      .w_AMKEYSAL = space(20)
      .w_AM_PROGR = 0
      .w_AM_PRENO = 0
      .w_AM_PRODU = 0
      .w_UNIMAT = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
          if not(empty(.w_AMCODART))
          .link_1_2('Full')
          endif
        .w_AMCLAMAT = .w_AMCLAMAT
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_AMCLAMAT))
          .link_1_3('Full')
          endif
        .w_AMDATCRE = i_Datsys
          .DoRTCalc(5,7,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(9,10,.f.)
        .w_AMKEYSAL = .w_AMCODART
      endif
    endwith
    cp_BlankRecExtFlds(this,'MATRICOL')
    this.DoRTCalc(12,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAMCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oAMCODART_1_2.enabled = i_bVal
      .Page1.oPag.oAMCLAMAT_1_3.enabled = i_bVal
      .Page1.oPag.oAMDATCRE_1_4.enabled = i_bVal
      .Page1.oPag.oAMDATSCA_1_6.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oAMCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAMCODICE_1_1.enabled = .t.
        .Page1.oPag.oAMCODART_1_2.enabled = .t.
        .Page1.oPag.oAMCLAMAT_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MATRICOL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMCODICE,"AMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMCODART,"AMCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMCLAMAT,"AMCLAMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMDATCRE,"AMDATCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMDATSCA,"AMDATSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMKEYSAL,"AMKEYSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_PROGR,"AM_PROGR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_PRENO,"AM_PRENO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_PRODU,"AM_PRODU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    i_lTable = "MATRICOL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MATRICOL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsmd_smt with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MATRICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MATRICOL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MATRICOL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MATRICOL')
        i_extval=cp_InsertValODBCExtFlds(this,'MATRICOL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AMCODICE,AMCODART,AMCLAMAT,AMDATCRE,AMDATSCA"+;
                  ",AMKEYSAL,AM_PROGR,AM_PRENO,AM_PRODU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AMCODICE)+;
                  ","+cp_ToStrODBCNull(this.w_AMCODART)+;
                  ","+cp_ToStrODBCNull(this.w_AMCLAMAT)+;
                  ","+cp_ToStrODBC(this.w_AMDATCRE)+;
                  ","+cp_ToStrODBC(this.w_AMDATSCA)+;
                  ","+cp_ToStrODBC(this.w_AMKEYSAL)+;
                  ","+cp_ToStrODBC(this.w_AM_PROGR)+;
                  ","+cp_ToStrODBC(this.w_AM_PRENO)+;
                  ","+cp_ToStrODBC(this.w_AM_PRODU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MATRICOL')
        i_extval=cp_InsertValVFPExtFlds(this,'MATRICOL')
        cp_CheckDeletedKey(i_cTable,0,'AMCODICE',this.w_AMCODICE,'AMKEYSAL',this.w_AMKEYSAL)
        INSERT INTO (i_cTable);
              (AMCODICE,AMCODART,AMCLAMAT,AMDATCRE,AMDATSCA,AMKEYSAL,AM_PROGR,AM_PRENO,AM_PRODU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AMCODICE;
                  ,this.w_AMCODART;
                  ,this.w_AMCLAMAT;
                  ,this.w_AMDATCRE;
                  ,this.w_AMDATSCA;
                  ,this.w_AMKEYSAL;
                  ,this.w_AM_PROGR;
                  ,this.w_AM_PRENO;
                  ,this.w_AM_PRODU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MATRICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MATRICOL_IDX,i_nConn)
      *
      * update MATRICOL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MATRICOL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AMCODART="+cp_ToStrODBCNull(this.w_AMCODART)+;
             ",AMCLAMAT="+cp_ToStrODBCNull(this.w_AMCLAMAT)+;
             ",AMDATCRE="+cp_ToStrODBC(this.w_AMDATCRE)+;
             ",AMDATSCA="+cp_ToStrODBC(this.w_AMDATSCA)+;
             ",AM_PROGR="+cp_ToStrODBC(this.w_AM_PROGR)+;
             ",AM_PRENO="+cp_ToStrODBC(this.w_AM_PRENO)+;
             ",AM_PRODU="+cp_ToStrODBC(this.w_AM_PRODU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MATRICOL')
        i_cWhere = cp_PKFox(i_cTable  ,'AMCODICE',this.w_AMCODICE  ,'AMKEYSAL',this.w_AMKEYSAL  )
        UPDATE (i_cTable) SET;
              AMCODART=this.w_AMCODART;
             ,AMCLAMAT=this.w_AMCLAMAT;
             ,AMDATCRE=this.w_AMDATCRE;
             ,AMDATSCA=this.w_AMDATSCA;
             ,AM_PROGR=this.w_AM_PROGR;
             ,AM_PRENO=this.w_AM_PRENO;
             ,AM_PRODU=this.w_AM_PRODU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MATRICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MATRICOL_IDX,i_nConn)
      *
      * delete MATRICOL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AMCODICE',this.w_AMCODICE  ,'AMKEYSAL',this.w_AMKEYSAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_AMCLAMAT<>.w_AMCLAMAT
            .w_AMCLAMAT = .w_AMCLAMAT
          .link_1_3('Full')
        endif
        .DoRTCalc(4,10,.t.)
        if .o_AMCODART<>.w_AMCODART
            .w_AMKEYSAL = .w_AMCODART
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAMCODART_1_2.enabled = this.oPgFrm.Page1.oPag.oAMCODART_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oAM_PRENO_1_19.visible=!this.oPgFrm.Page1.oPag.oAM_PRENO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oAM_PRODU_1_20.visible=!this.oPgFrm.Page1.oPag.oAM_PRODU_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AMCODART
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AMCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_AMCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGESMAT,ARCLAMAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_AMCODART))
          select ARCODART,ARDESART,ARDTOBSO,ARGESMAT,ARCLAMAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AMCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_AMCODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGESMAT,ARCLAMAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_AMCODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARGESMAT,ARCLAMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AMCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oAMCODART_1_2'),i_cWhere,'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGESMAT,ARCLAMAT";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARGESMAT,ARCLAMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AMCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGESMAT,ARCLAMAT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_AMCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_AMCODART)
            select ARCODART,ARDESART,ARDTOBSO,ARGESMAT,ARCLAMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AMCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_GESMAT = NVL(_Link_.ARGESMAT,space(1))
      this.w_AMCLAMAT = NVL(_Link_.ARCLAMAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_AMCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_GESMAT = space(1)
      this.w_AMCLAMAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_GESMAT='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente obsoleto o non gestito a matricole")
        endif
        this.w_AMCODART = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_GESMAT = space(1)
        this.w_AMCLAMAT = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AMCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.ARCODART as ARCODART102"+ ",link_1_2.ARDESART as ARDESART102"+ ",link_1_2.ARDTOBSO as ARDTOBSO102"+ ",link_1_2.ARGESMAT as ARGESMAT102"+ ",link_1_2.ARCLAMAT as ARCLAMAT102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on MATRICOL.AMCODART=link_1_2.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and MATRICOL.AMCODART=link_1_2.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AMCLAMAT
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AMCLAMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ACM',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_AMCLAMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMUNIMAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_AMCLAMAT))
          select CMCODICE,CMDESCRI,CMUNIMAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AMCLAMAT)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AMCLAMAT) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oAMCLAMAT_1_3'),i_cWhere,'GSMD_ACM',"Classi matricole",'GSMDMACM.CMT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMUNIMAT";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMUNIMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AMCLAMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMUNIMAT";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_AMCLAMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_AMCLAMAT)
            select CMCODICE,CMDESCRI,CMUNIMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AMCLAMAT = NVL(_Link_.CMCODICE,space(5))
      this.w_CMDESCRI = NVL(_Link_.CMDESCRI,space(30))
      this.w_UNIMAT = NVL(_Link_.CMUNIMAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AMCLAMAT = space(5)
      endif
      this.w_CMDESCRI = space(30)
      this.w_UNIMAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AMCLAMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CMT_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.CMCODICE as CMCODICE103"+ ",link_1_3.CMDESCRI as CMDESCRI103"+ ",link_1_3.CMUNIMAT as CMUNIMAT103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on MATRICOL.AMCLAMAT=link_1_3.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and MATRICOL.AMCLAMAT=link_1_3.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAMCODICE_1_1.value==this.w_AMCODICE)
      this.oPgFrm.Page1.oPag.oAMCODICE_1_1.value=this.w_AMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oAMCODART_1_2.value==this.w_AMCODART)
      this.oPgFrm.Page1.oPag.oAMCODART_1_2.value=this.w_AMCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oAMCLAMAT_1_3.value==this.w_AMCLAMAT)
      this.oPgFrm.Page1.oPag.oAMCLAMAT_1_3.value=this.w_AMCLAMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oAMDATCRE_1_4.value==this.w_AMDATCRE)
      this.oPgFrm.Page1.oPag.oAMDATCRE_1_4.value=this.w_AMDATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oAMDATSCA_1_6.value==this.w_AMDATSCA)
      this.oPgFrm.Page1.oPag.oAMDATSCA_1_6.value=this.w_AMDATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_12.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_12.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDESCRI_1_16.value==this.w_CMDESCRI)
      this.oPgFrm.Page1.oPag.oCMDESCRI_1_16.value=this.w_CMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_PRENO_1_19.RadioValue()==this.w_AM_PRENO)
      this.oPgFrm.Page1.oPag.oAM_PRENO_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_PRODU_1_20.RadioValue()==this.w_AM_PRODU)
      this.oPgFrm.Page1.oPag.oAM_PRODU_1_20.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'MATRICOL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AMCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAMCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_AMCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AMCODART)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_GESMAT='S'))  and (.cFunction ='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAMCODART_1_2.SetFocus()
            i_bnoObbl = !empty(.w_AMCODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente obsoleto o non gestito a matricole")
          case   (empty(.w_AMCLAMAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAMCLAMAT_1_3.SetFocus()
            i_bnoObbl = !empty(.w_AMCLAMAT)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsmd_amt
      * Valorizzo la variante
      *Controllo Univocit� Matricola solo se g_UNIMAT = 'C'(per Classe), ='M'(per Matricola)
      if i_bRes=.t. and .cFunction = 'Load' and g_UNIMAT <> 'A'
         * --- Controlli Univocita' Matricola
          WAIT WINDOW 'Controllo Univocita...' NOWAIT
          WAIT CLEAR
          if UNIVOMAT(.w_AMCODICE, .w_UNIMAT,.w_AMKEYSAL, .w_AMCLAMAT)
            ah_ErrorMsg("Matricola gi� esistente %0Impossibile confermare")
            i_bRes=.f.
          endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AMCODART = this.w_AMCODART
    this.o_AMCLAMAT = this.w_AMCLAMAT
    return

  func CanEdit()
    local i_res
    i_res=CHKDUMAT(this.w_AMCODICE,this.w_AMCLAMAT,this.w_AMKEYSAL)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile modificare. Matricola movimentata"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=CHKDUMAT(this.w_AMCODICE,this.w_AMCLAMAT,this.w_AMKEYSAL)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile cancellare. Matricola movimentata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsmd_amtPag1 as StdContainer
  Width  = 556
  height = 129
  stdWidth  = 556
  stdheight = 129
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAMCODICE_1_1 as StdField with uid="GVVTGHLRAU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AMCODICE", cQueryName = "AMCODICE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 17380939,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=93, Top=14, InputMask=replicate('X',40)

  add object oAMCODART_1_2 as StdField with uid="AXMPDDVDFF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AMCODART", cQueryName = "AMCODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente obsoleto o non gestito a matricole",;
    ToolTipText = "Codice articolo associato",;
    HelpContextID = 151598682,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=93, Top=43, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_AMCODART"

  func oAMCODART_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction ='Load')
    endwith
   endif
  endfunc

  func oAMCODART_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oAMCODART_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAMCODART_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oAMCODART_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oAMCODART_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_AMCODART
     i_obj.ecpSave()
  endproc

  add object oAMCLAMAT_1_3 as StdField with uid="VUTLLGUIZZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AMCLAMAT", cQueryName = "AMCLAMAT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe matricola",;
    HelpContextID = 81147482,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=93, Top=73, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CMT_MAST", cZoomOnZoom="GSMD_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_AMCLAMAT"

  func oAMCLAMAT_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oAMCLAMAT_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAMCLAMAT_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oAMCLAMAT_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ACM',"Classi matricole",'GSMDMACM.CMT_MAST_VZM',this.parent.oContained
  endproc
  proc oAMCLAMAT_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_AMCLAMAT
     i_obj.ecpSave()
  endproc

  add object oAMDATCRE_1_4 as StdField with uid="FHERTWKBCK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AMDATCRE", cQueryName = "AMDATCRE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data creazione",;
    HelpContextID = 201016907,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=93, Top=102

  add object oAMDATSCA_1_6 as StdField with uid="UNMBNSXAZA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_AMDATSCA", cQueryName = "AMDATSCA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza",;
    HelpContextID = 201016903,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=295, Top=102


  add object oBtn_1_7 as StdButton with uid="GXJLNKUQQK",left=496, top=81, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizzazione tracciabilit� matricola";
    , HelpContextID = 251568481;
    , caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSMD_BLT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load')
     endwith
    endif
  endfunc

  add object oDESART_1_12 as StdField with uid="PEPJNBNRHO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 215756342,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=255, Top=43, InputMask=replicate('X',40)

  add object oCMDESCRI_1_16 as StdField with uid="HEZNMZAFQD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CMDESCRI", cQueryName = "CMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 200230511,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=157, Top=73, InputMask=replicate('X',30)

  add object oAM_PRENO_1_19 as StdCheck with uid="WUZTCMKGPA",rtseq=13,rtrep=.f.,left=392, top=103, caption="Prenotata", enabled=.f.,;
    ToolTipText = "Se attivo, matricola prenotata da dichiarazione di produzione",;
    HelpContextID = 34867627,;
    cFormVar="w_AM_PRENO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAM_PRENO_1_19.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oAM_PRENO_1_19.GetRadio()
    this.Parent.oContained.w_AM_PRENO = this.RadioValue()
    return .t.
  endfunc

  func oAM_PRENO_1_19.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AM_PRENO==1,1,;
      0)
  endfunc

  func oAM_PRENO_1_19.mHide()
    with this.Parent.oContained
      return (g_PROD<>'S')
    endwith
  endfunc

  add object oAM_PRODU_1_20 as StdCheck with uid="ZRNZNCFMNV",rtseq=14,rtrep=.f.,left=392, top=72, caption="Produzione", enabled=.f.,;
    ToolTipText = "Se attivo, matricola dichiarata in produzione",;
    HelpContextID = 132904539,;
    cFormVar="w_AM_PRODU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAM_PRODU_1_20.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oAM_PRODU_1_20.GetRadio()
    this.Parent.oContained.w_AM_PRODU = this.RadioValue()
    return .t.
  endfunc

  func oAM_PRODU_1_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AM_PRODU==1,1,;
      0)
  endfunc

  func oAM_PRODU_1_20.mHide()
    with this.Parent.oContained
      return (g_PROD<>'S')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="JRLHXDFPHN",Visible=.t., Left=27, Top=18,;
    Alignment=1, Width=63, Height=18,;
    Caption="Matricola:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="XJWWCOFRXL",Visible=.t., Left=31, Top=77,;
    Alignment=1, Width=59, Height=18,;
    Caption="Classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ZGJDKWMIPM",Visible=.t., Left=26, Top=106,;
    Alignment=1, Width=64, Height=18,;
    Caption="Creato il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="QCPUXOJUXD",Visible=.t., Left=228, Top=106,;
    Alignment=1, Width=63, Height=18,;
    Caption="Scade il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="STMFUBVCZX",Visible=.t., Left=43, Top=47,;
    Alignment=1, Width=47, Height=18,;
    Caption="Articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_amt','MATRICOL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AMCODICE=MATRICOL.AMCODICE";
  +" and "+i_cAliasName2+".AMKEYSAL=MATRICOL.AMKEYSAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
