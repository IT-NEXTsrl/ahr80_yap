* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bzu                                                        *
*              Seleziona ubicazione da inseri                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-01-15                                                      *
* Last revis.: 2016-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bzu",oParentObject,m.pEXEC)
return(i_retval)

define class tgsmd_bzu as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_CODMAG = space(5)
  w_CODUBI = space(20)
  w_QTAESI = 0
  w_MLCODICE = space(20)
  w_CODART = space(20)
  w_FLLOTT = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona l' Ubicazione da Inserire (da GSMA_MVM, GSVE_MDV,GSAC_MDV, GSMD_MLU)
    WITH this.oParentObject
    do case
      case this.oParentObject.w_FLPRG="M"
        * --- Siamo entrati dai Movimenti di Magazzino
        this.w_CODART = .w_MMCODART
        this.w_CODUBI = IIF(this.pEXEC="A",.w_MMCODUBI,.w_MMCODUB2)
        this.w_FLLOTT = .w_MMFLLOTT
        this.w_CODMAG = IIF(this.pEXEC="A",.w_MMCODMAG,.w_MMCODMAT)
      case this.oParentObject.w_FLPRG="D"
        * --- Siamo entrati dai Documenti
        this.w_CODART = .w_MVCODART
        this.w_CODUBI = IIF(this.pEXEC="A",.w_MVCODUBI,.w_MVCODUB2)
        this.w_FLLOTT = .w_MVFLLOTT
        this.w_CODMAG = IIF(this.pEXEC="A",.w_MVCODMAG,.w_MVCODMAT)
      case this.oParentObject.w_FLPRG="A"
        * --- Siamo entrati dai Documenti
        this.w_CODART = .w_CODART
        this.w_CODUBI = IIF(this.pEXEC="A",.w_SPCODUBI,.w_SPCODUB2)
        this.w_FLLOTT = .w_SFLLOTT
        this.w_CODMAG = IIF(this.pEXEC="A",.w_SLMAG,.w_SLMAT)
      case this.oParentObject.w_FLPRG="P"
        * --- Siamo entrati dall'inserimento penna
        this.w_CODART = .w_CODART
        this.w_CODUBI = IIF(this.pEXEC="A",.w_GPCODUBI,.w_GPCODUB2)
        this.w_FLLOTT = .w_MxFLLOTT
        this.w_CODMAG = IIF(this.pEXEC="A",.w_GPCODMAG,.w_GPCODMA2 )
      case this.oParentObject.w_FLPRG="V"
        * --- Siamo entrati dal POS
        this.w_CODART = .w_MDCODART
        this.w_CODUBI = .w_MDCODUBI
        this.w_FLLOTT = .w_MDFLLOTT
        this.w_CODMAG = .w_MDCODMAG
      case this.oParentObject.w_FLPRG="L"
        this.w_CODART = .w_CODART
        this.w_CODUBI = .w_CLCODUBI
        this.w_FLLOTT = .w_FLLOTT
        this.w_CODMAG = .w_CLCODMAG
    endcase
    ENDWITH
    this.w_MLCODICE = SPACE(20)
    this.w_QTAESI = 0
    * --- Seleziona Viste del Form
    if this.w_FLLOTT="+"
      * --- Carica il Lotto
      vx_exec("..\MADV\EXE\QUERY\GSMD_QUC.VZM",this)
    else
      * --- Scarica il Lotto
      vx_exec("..\MADV\EXE\QUERY\GSMD_QUS.VZM",this)
    endif
    if NOT EMPTY(NVL(this.w_CODUBI," "))
      do case
        case this.oParentObject.w_FLPRG="M"
          if this.pEXEC="A"
            this.oParentObject.w_MMCODUBI = this.w_CODUBI
          else
            this.oParentObject.w_MMCODUB2 = this.w_CODUBI
          endif
        case this.oParentObject.w_FLPRG="D"
          if this.pEXEC="A"
            this.oParentObject.w_MVCODUBI = this.w_CODUBI
          else
            this.oParentObject.w_MVCODUB2 = this.w_CODUBI
          endif
        case this.oParentObject.w_FLPRG="A"
          if this.pEXEC="A"
            this.oParentObject.w_SPCODUBI = this.w_CODUBI
          else
            this.oParentObject.w_SPCODUB2 = this.w_CODUBI
          endif
        case this.oParentObject.w_FLPRG="P"
          if this.pEXEC="A"
            this.oParentObject.w_GPCODUBI = this.w_CODUBI
          else
            this.oParentObject.w_GPCODUB2 = this.w_CODUBI
          endif
        case this.oParentObject.w_FLPRG="V"
          this.oParentObject.w_MDCODUBI = this.w_CODUBI
        case this.oParentObject.w_FLPRG="L"
          this.oParentObject.w_CLCODUBI = this.w_CODUBI
      endcase
      * --- Mi dice se il dato proviene da uno Zoom o da inserimento manuale
      this.oParentObject.w_UBIZOOM = .T.
      * --- Notifica Riga Variata
      SELECT (this.oParentObject.cTrsName)
      if I_SRV=" " AND NOT DELETED()
        REPLACE i_SRV WITH "U"
      endif
    else
      this.oParentObject.w_UBIZOOM = .F.
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
