* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_scl                                                        *
*              Stampa classe matricola/lotto                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-05                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_scl",oParentObject))

* --- Class definition
define class tgsmd_scl as StdForm
  Top    = 30
  Left   = 37

  * --- Standard Properties
  Width  = 597
  Height = 147
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=177583255
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  CMT_MAST_IDX = 0
  cPrg = "gsmd_scl"
  cComment = "Stampa classe matricola/lotto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CLASSE = space(5)
  w_CMFLAUTO = space(1)
  w_CMDESCRI = space(30)
  w_TIPCLA = space(1)
  w_CLASSE = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_sclPag1","gsmd_scl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLASSE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CMT_MAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CLASSE=space(5)
      .w_CMFLAUTO=space(1)
      .w_CMDESCRI=space(30)
      .w_TIPCLA=space(1)
      .w_CLASSE=space(5)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CLASSE))
          .link_1_1('Full')
        endif
        .w_CMFLAUTO = 'A'
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
          .DoRTCalc(3,3,.f.)
        .w_TIPCLA = 'M'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CLASSE))
          .link_1_12('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
  return

  proc Calculate_DINGQKZDBO()
    with this
          * --- Reset classe
          .w_CLASSE = ''
          .w_CMDESCRI = ''
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCLASSE_1_1.enabled = this.oPgFrm.Page1.oPag.oCLASSE_1_1.mCond()
    this.oPgFrm.Page1.oPag.oCLASSE_1_12.enabled = this.oPgFrm.Page1.oPag.oCLASSE_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCLASSE_1_1.visible=!this.oPgFrm.Page1.oPag.oCLASSE_1_1.mHide()
    this.oPgFrm.Page1.oPag.oCLASSE_1_12.visible=!this.oPgFrm.Page1.oPag.oCLASSE_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
        if lower(cEvent)==lower("w_TIPCLA Changed")
          .Calculate_DINGQKZDBO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLASSE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASSE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CLASSE)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CLASSE))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASSE)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASSE) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oCLASSE_1_1'),i_cWhere,'',"Elenco classi",'GSMDMACM.CMT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASSE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CLASSE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CLASSE)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASSE = NVL(_Link_.CMCODICE,space(5))
      this.w_CMDESCRI = NVL(_Link_.CMDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CLASSE = space(5)
      endif
      this.w_CMDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASSE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASSE
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASSE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CLASSE)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CLASSE))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASSE)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASSE) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oCLASSE_1_12'),i_cWhere,'',"Elenco classi",'GSMDLACM.CMT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASSE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CLASSE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CLASSE)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASSE = NVL(_Link_.CMCODICE,space(5))
      this.w_CMDESCRI = NVL(_Link_.CMDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CLASSE = space(5)
      endif
      this.w_CMDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASSE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCLASSE_1_1.value==this.w_CLASSE)
      this.oPgFrm.Page1.oPag.oCLASSE_1_1.value=this.w_CLASSE
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLAUTO_1_2.RadioValue()==this.w_CMFLAUTO)
      this.oPgFrm.Page1.oPag.oCMFLAUTO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDESCRI_1_8.value==this.w_CMDESCRI)
      this.oPgFrm.Page1.oPag.oCMDESCRI_1_8.value=this.w_CMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLA_1_10.RadioValue()==this.w_TIPCLA)
      this.oPgFrm.Page1.oPag.oTIPCLA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASSE_1_12.value==this.w_CLASSE)
      this.oPgFrm.Page1.oPag.oCLASSE_1_12.value=this.w_CLASSE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsmd_sclPag1 as StdContainer
  Width  = 593
  height = 147
  stdWidth  = 593
  stdheight = 147
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLASSE_1_1 as StdField with uid="SNOGMKOZOO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CLASSE", cQueryName = "CLASSE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe matricole",;
    HelpContextID = 85792038,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=97, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CMT_MAST", oKey_1_1="CMCODICE", oKey_1_2="this.w_CLASSE"

  func oCLASSE_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLA='M')
    endwith
   endif
  endfunc

  func oCLASSE_1_1.mHide()
    with this.Parent.oContained
      return (.w_TIPCLA='L')
    endwith
  endfunc

  func oCLASSE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASSE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASSE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oCLASSE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco classi",'GSMDMACM.CMT_MAST_VZM',this.parent.oContained
  endproc


  add object oCMFLAUTO_1_2 as StdCombo with uid="MEIXNSYKGZ",rtseq=2,rtrep=.f.,left=469,top=14,width=90,height=21;
    , HelpContextID = 201955723;
    , cFormVar="w_CMFLAUTO",RowSource=""+"Automatico,"+"Manuale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLAUTO_1_2.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oCMFLAUTO_1_2.GetRadio()
    this.Parent.oContained.w_CMFLAUTO = this.RadioValue()
    return .t.
  endfunc

  func oCMFLAUTO_1_2.SetRadio()
    this.Parent.oContained.w_CMFLAUTO=trim(this.Parent.oContained.w_CMFLAUTO)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLAUTO=='A',1,;
      iif(this.Parent.oContained.w_CMFLAUTO=='M',2,;
      0))
  endfunc


  add object oObj_1_3 as cp_outputCombo with uid="ZLERXIVPWT",left=97, top=73, width=489,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181290010


  add object oBtn_1_4 as StdButton with uid="HHUWXEDCXQ",left=486, top=98, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217498074;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="YAISLJNHUN",left=538, top=98, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184900678;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCMDESCRI_1_8 as StdField with uid="HEZNMZAFQD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CMDESCRI", cQueryName = "CMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 217102737,;
   bGlobalFont=.t.,;
    Height=21, Width=228, Left=160, Top=42, InputMask=replicate('X',30)

  add object oTIPCLA_1_10 as StdRadio with uid="NQLNXLZUZZ",rtseq=4,rtrep=.f.,left=97, top=14, width=140,height=23;
    , cFormVar="w_TIPCLA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTIPCLA_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Matricola"
      this.Buttons(1).HelpContextID = 10355510
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Matricola","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Lotto"
      this.Buttons(2).HelpContextID = 10355510
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Lotto","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTIPCLA_1_10.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'L',;
    space(1))))
  endfunc
  func oTIPCLA_1_10.GetRadio()
    this.Parent.oContained.w_TIPCLA = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLA_1_10.SetRadio()
    this.Parent.oContained.w_TIPCLA=trim(this.Parent.oContained.w_TIPCLA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLA=='M',1,;
      iif(this.Parent.oContained.w_TIPCLA=='L',2,;
      0))
  endfunc

  add object oCLASSE_1_12 as StdField with uid="VQYVWKLIJD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CLASSE", cQueryName = "CLASSE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe matricole",;
    HelpContextID = 85792038,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=97, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CMT_MAST", oKey_1_1="CMCODICE", oKey_1_2="this.w_CLASSE"

  func oCLASSE_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLA='L')
    endwith
   endif
  endfunc

  func oCLASSE_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPCLA='M')
    endwith
  endfunc

  func oCLASSE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASSE_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASSE_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oCLASSE_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco classi",'GSMDLACM.CMT_MAST_VZM',this.parent.oContained
  endproc

  add object oStr_1_6 as StdString with uid="EYUCCHJANJ",Visible=.t., Left=7, Top=73,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="LKQRCDADBR",Visible=.t., Left=34, Top=42,;
    Alignment=1, Width=59, Height=18,;
    Caption="Classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="YVOXARTJAU",Visible=.t., Left=352, Top=17,;
    Alignment=0, Width=114, Height=18,;
    Caption="Tipo aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="IIWUYJRUVV",Visible=.t., Left=23, Top=17,;
    Alignment=1, Width=70, Height=18,;
    Caption="Tipo classe:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_scl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
