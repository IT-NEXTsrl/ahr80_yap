* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_kcr                                                        *
*              Inserimento rapido matricole                                    *
*                                                                              *
*      Author: Zucchetti S.p.A                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_403]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-14                                                      *
* Last revis.: 2015-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_kcr",oParentObject))

* --- Class definition
define class tgsmd_kcr as StdForm
  Top    = -1
  Left   = 0

  * --- Standard Properties
  Width  = 732
  Height = 482
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-18"
  HelpContextID=99240809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  CMT_MAST_IDX = 0
  UBICAZIO_IDX = 0
  LOTTIART_IDX = 0
  MAGAZZIN_IDX = 0
  cPrg = "gsmd_kcr"
  cComment = "Inserimento rapido matricole"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODICE = space(41)
  w_CODART = space(20)
  w_MTKEYSAL = space(20)
  w_DESART = space(40)
  w_OBTEST = space(10)
  w_CODMAG = space(5)
  w_MAGSCA = space(5)
  w_SECODLOT = space(20)
  w_CODLOT = space(20)
  w_SECODUBI = space(20)
  w_MTCODLOT = space(20)
  o_MTCODLOT = space(20)
  w_CODUBI = space(20)
  w_CODUBISCA = space(20)
  w_UNIMIS1 = space(3)
  w_QTAUM1 = 0
  w_PRODUZ = .F.
  w_QTACA1 = 0
  w_NPROG = 0
  w_ESIRIS = space(1)
  w_FILTROLIKE = space(41)
  w_ARTLOT = space(20)
  w_FLLOTT = space(1)
  w_MGUBIC = space(1)
  w_CONTA = 0
  w_CONTACAR = 0
  w_CODFOR = space(15)
  w_MTCODMAT = space(40)
  w_STALOT = space(1)
  w_LOTZOOM = .F.
  w_MOVMAT = .F.
  w_DATREG = ctod('  /  /  ')
  w_MTMAGCAR = space(5)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_NUMRIF = 0
  w_MTFLCARI = space(1)
  w_MTMAGSCA = space(5)
  w_MTFLSCAR = space(1)
  w_ULTPROG = 0
  w_MAGUBI = space(5)
  w_MAGUBI1 = space(5)
  w_MGUBICAR = space(1)
  w_MGUBICA2 = space(1)
  w_FLPRG = space(1)
  w_AM_PRODU = 0
  w_MGUBISCA = space(1)
  w_FLAUTO = space(1)
  w_QTASC1 = 0
  w_FLGSCA = space(10)
  w_COMSCA = space(15)
  w_COMCAR = space(15)
  w_ZoomCar = .NULL.
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_kcrPag1","gsmd_kcr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNPROG_1_23
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomCar = this.oPgFrm.Pages(1).oPag.ZoomCar
    this.w_ZoomSel = this.oPgFrm.Pages(1).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomCar = .NULL.
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CMT_MAST'
    this.cWorkTables[4]='UBICAZIO'
    this.cWorkTables[5]='LOTTIART'
    this.cWorkTables[6]='MAGAZZIN'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE=space(41)
      .w_CODART=space(20)
      .w_MTKEYSAL=space(20)
      .w_DESART=space(40)
      .w_OBTEST=space(10)
      .w_CODMAG=space(5)
      .w_MAGSCA=space(5)
      .w_SECODLOT=space(20)
      .w_CODLOT=space(20)
      .w_SECODUBI=space(20)
      .w_MTCODLOT=space(20)
      .w_CODUBI=space(20)
      .w_CODUBISCA=space(20)
      .w_UNIMIS1=space(3)
      .w_QTAUM1=0
      .w_PRODUZ=.f.
      .w_QTACA1=0
      .w_NPROG=0
      .w_ESIRIS=space(1)
      .w_FILTROLIKE=space(41)
      .w_ARTLOT=space(20)
      .w_FLLOTT=space(1)
      .w_MGUBIC=space(1)
      .w_CONTA=0
      .w_CONTACAR=0
      .w_CODFOR=space(15)
      .w_MTCODMAT=space(40)
      .w_STALOT=space(1)
      .w_LOTZOOM=.f.
      .w_MOVMAT=.f.
      .w_DATREG=ctod("  /  /  ")
      .w_MTMAGCAR=space(5)
      .w_SERRIF=space(10)
      .w_ROWRIF=0
      .w_NUMRIF=0
      .w_MTFLCARI=space(1)
      .w_MTMAGSCA=space(5)
      .w_MTFLSCAR=space(1)
      .w_ULTPROG=0
      .w_MAGUBI=space(5)
      .w_MAGUBI1=space(5)
      .w_MGUBICAR=space(1)
      .w_MGUBICA2=space(1)
      .w_FLPRG=space(1)
      .w_AM_PRODU=0
      .w_MGUBISCA=space(1)
      .w_FLAUTO=space(1)
      .w_QTASC1=0
      .w_FLGSCA=space(10)
      .w_COMSCA=space(15)
      .w_COMCAR=space(15)
      .w_CODICE=oParentObject.w_CODICE
      .w_CODART=oParentObject.w_CODART
      .w_MTKEYSAL=oParentObject.w_MTKEYSAL
      .w_DESART=oParentObject.w_DESART
      .w_CODMAG=oParentObject.w_CODMAG
      .w_MAGSCA=oParentObject.w_MAGSCA
      .w_CODLOT=oParentObject.w_CODLOT
      .w_UNIMIS1=oParentObject.w_UNIMIS1
      .w_QTAUM1=oParentObject.w_QTAUM1
      .w_QTACA1=oParentObject.w_QTACA1
      .w_ESIRIS=oParentObject.w_ESIRIS
      .w_FLLOTT=oParentObject.w_FLLOTT
      .w_MGUBIC=oParentObject.w_MGUBIC
      .w_CODFOR=oParentObject.w_CODFOR
      .w_MTMAGCAR=oParentObject.w_MTMAGCAR
      .w_SERRIF=oParentObject.w_SERRIF
      .w_ROWRIF=oParentObject.w_ROWRIF
      .w_NUMRIF=oParentObject.w_NUMRIF
      .w_MTFLCARI=oParentObject.w_MTFLCARI
      .w_MTMAGSCA=oParentObject.w_MTMAGSCA
      .w_MTFLSCAR=oParentObject.w_MTFLSCAR
      .w_MAGUBI=oParentObject.w_MAGUBI
      .w_MAGUBI1=oParentObject.w_MAGUBI1
      .w_MGUBICAR=oParentObject.w_MGUBICAR
      .w_MGUBICA2=oParentObject.w_MGUBICA2
      .w_AM_PRODU=oParentObject.w_AM_PRODU
      .w_MGUBISCA=oParentObject.w_MGUBISCA
      .w_FLAUTO=oParentObject.w_FLAUTO
      .w_QTASC1=oParentObject.w_QTASC1
      .w_FLGSCA=oParentObject.w_FLGSCA
      .w_COMSCA=oParentObject.w_COMSCA
      .w_COMCAR=oParentObject.w_COMCAR
          .DoRTCalc(1,4,.f.)
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
          .DoRTCalc(6,7,.f.)
        .w_SECODLOT = .w_CODLOT
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SECODLOT))
          .link_1_9('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_SECODUBI = IIF( TYPE('.oParentObject.oParentObject.w_CODUBITES')='U','',.oParentObject.oParentObject.w_CODUBITES)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_SECODUBI))
          .link_1_11('Full')
        endif
        .w_MTCODLOT = IIF( TYPE('.oParentObject.oParentObject.w_CODLOTTES')='U','',.oParentObject.oParentObject.w_CODLOTTES)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MTCODLOT))
          .link_1_12('Full')
        endif
        .w_CODUBI = IIF( TYPE('.oParentObject.oParentObject.w_CODUBI')='U','',.oParentObject.oParentObject.w_CODUBI)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODUBI))
          .link_1_13('Full')
        endif
        .w_CODUBISCA = IIF( TYPE('.oParentObject.oParentObject.w_CODUB2TES')='U','',.oParentObject.oParentObject.w_CODUB2TES)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODUBISCA))
          .link_1_14('Full')
        endif
          .DoRTCalc(14,15,.f.)
        .w_PRODUZ = not empty(.w_MAGSCA) and .w_QTASC1>0
          .DoRTCalc(17,17,.f.)
        .w_NPROG = iif(.w_PRODUZ and .w_QTACA1>0, .w_QTACA1, .w_QTAUM1)
      .oPgFrm.Page1.oPag.ZoomCar.Calculate()
      .oPgFrm.Page1.oPag.ZoomSel.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
          .DoRTCalc(19,28,.f.)
        .w_LOTZOOM = .F.
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
          .DoRTCalc(30,43,.f.)
        .w_FLPRG = 'I'
    endwith
    this.DoRTCalc(45,51,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_76.enabled = this.oPgFrm.Page1.oPag.oBtn_1_76.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODICE=.w_CODICE
      .oParentObject.w_CODART=.w_CODART
      .oParentObject.w_MTKEYSAL=.w_MTKEYSAL
      .oParentObject.w_DESART=.w_DESART
      .oParentObject.w_CODMAG=.w_CODMAG
      .oParentObject.w_MAGSCA=.w_MAGSCA
      .oParentObject.w_CODLOT=.w_CODLOT
      .oParentObject.w_UNIMIS1=.w_UNIMIS1
      .oParentObject.w_QTAUM1=.w_QTAUM1
      .oParentObject.w_QTACA1=.w_QTACA1
      .oParentObject.w_ESIRIS=.w_ESIRIS
      .oParentObject.w_FLLOTT=.w_FLLOTT
      .oParentObject.w_MGUBIC=.w_MGUBIC
      .oParentObject.w_CODFOR=.w_CODFOR
      .oParentObject.w_MTMAGCAR=.w_MTMAGCAR
      .oParentObject.w_SERRIF=.w_SERRIF
      .oParentObject.w_ROWRIF=.w_ROWRIF
      .oParentObject.w_NUMRIF=.w_NUMRIF
      .oParentObject.w_MTFLCARI=.w_MTFLCARI
      .oParentObject.w_MTMAGSCA=.w_MTMAGSCA
      .oParentObject.w_MTFLSCAR=.w_MTFLSCAR
      .oParentObject.w_MAGUBI=.w_MAGUBI
      .oParentObject.w_MAGUBI1=.w_MAGUBI1
      .oParentObject.w_MGUBICAR=.w_MGUBICAR
      .oParentObject.w_MGUBICA2=.w_MGUBICA2
      .oParentObject.w_AM_PRODU=.w_AM_PRODU
      .oParentObject.w_MGUBISCA=.w_MGUBISCA
      .oParentObject.w_FLAUTO=.w_FLAUTO
      .oParentObject.w_QTASC1=.w_QTASC1
      .oParentObject.w_FLGSCA=.w_FLGSCA
      .oParentObject.w_COMSCA=.w_COMSCA
      .oParentObject.w_COMCAR=.w_COMCAR
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .DoRTCalc(1,7,.t.)
          .link_1_9('Full')
        .DoRTCalc(9,9,.t.)
          .link_1_11('Full')
          .link_1_12('Full')
          .link_1_13('Full')
          .link_1_14('Full')
        .oPgFrm.Page1.oPag.ZoomCar.Calculate()
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .DoRTCalc(14,28,.t.)
        if .o_MTCODLOT<>.w_MTCODLOT
            .w_LOTZOOM = .F.
        endif
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .DoRTCalc(30,43,.t.)
            .w_FLPRG = 'I'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(45,51,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.ZoomCar.Calculate()
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSECODLOT_1_9.visible=!this.oPgFrm.Page1.oPag.oSECODLOT_1_9.mHide()
    this.oPgFrm.Page1.oPag.oSECODUBI_1_11.visible=!this.oPgFrm.Page1.oPag.oSECODUBI_1_11.mHide()
    this.oPgFrm.Page1.oPag.oMTCODLOT_1_12.visible=!this.oPgFrm.Page1.oPag.oMTCODLOT_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCODUBI_1_13.visible=!this.oPgFrm.Page1.oPag.oCODUBI_1_13.mHide()
    this.oPgFrm.Page1.oPag.oCODUBISCA_1_14.visible=!this.oPgFrm.Page1.oPag.oCODUBISCA_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_27.visible=!this.oPgFrm.Page1.oPag.oBtn_1_27.mHide()
    this.oPgFrm.Page1.oPag.oMTMAGCAR_1_48.visible=!this.oPgFrm.Page1.oPag.oMTMAGCAR_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oMTFLCARI_1_55.visible=!this.oPgFrm.Page1.oPag.oMTFLCARI_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oMTMAGSCA_1_57.visible=!this.oPgFrm.Page1.oPag.oMTMAGSCA_1_57.mHide()
    this.oPgFrm.Page1.oPag.oMTFLSCAR_1_58.visible=!this.oPgFrm.Page1.oPag.oMTFLSCAR_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_76.visible=!this.oPgFrm.Page1.oPag.oBtn_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_82.visible=!this.oPgFrm.Page1.oPag.oStr_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomCar.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SECODLOT
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SECODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SECODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_SECODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_SECODLOT)
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SECODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
      this.w_STALOT = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SECODLOT = space(20)
      endif
      this.w_ARTLOT = space(20)
      this.w_STALOT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTLOT=.w_CODART
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o di un altro articolo")
        endif
        this.w_SECODLOT = space(20)
        this.w_ARTLOT = space(20)
        this.w_STALOT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SECODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SECODUBI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SECODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SECODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_SECODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CODMAG;
                       ,'UBCODICE',this.w_SECODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SECODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_SECODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SECODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MTCODLOT
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_MTCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_MTCODLOT)
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
      this.w_STALOT = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MTCODLOT = space(20)
      endif
      this.w_ARTLOT = space(20)
      this.w_STALOT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTLOT=.w_CODART
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o di un altro articolo")
        endif
        this.w_MTCODLOT = space(20)
        this.w_ARTLOT = space(20)
        this.w_STALOT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGUBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MAGUBI;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBISCA
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBISCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBISCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBISCA);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MAGUBI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MAGUBI1;
                       ,'UBCODICE',this.w_CODUBISCA)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBISCA = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBISCA = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBISCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESART_1_4.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_4.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODLOT_1_9.value==this.w_SECODLOT)
      this.oPgFrm.Page1.oPag.oSECODLOT_1_9.value=this.w_SECODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODUBI_1_11.value==this.w_SECODUBI)
      this.oPgFrm.Page1.oPag.oSECODUBI_1_11.value=this.w_SECODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oMTCODLOT_1_12.value==this.w_MTCODLOT)
      this.oPgFrm.Page1.oPag.oMTCODLOT_1_12.value=this.w_MTCODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_13.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_13.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBISCA_1_14.value==this.w_CODUBISCA)
      this.oPgFrm.Page1.oPag.oCODUBISCA_1_14.value=this.w_CODUBISCA
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS1_1_18.value==this.w_UNIMIS1)
      this.oPgFrm.Page1.oPag.oUNIMIS1_1_18.value=this.w_UNIMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAUM1_1_20.value==this.w_QTAUM1)
      this.oPgFrm.Page1.oPag.oQTAUM1_1_20.value=this.w_QTAUM1
    endif
    if not(this.oPgFrm.Page1.oPag.oNPROG_1_23.value==this.w_NPROG)
      this.oPgFrm.Page1.oPag.oNPROG_1_23.value=this.w_NPROG
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTA_1_38.value==this.w_CONTA)
      this.oPgFrm.Page1.oPag.oCONTA_1_38.value=this.w_CONTA
    endif
    if not(this.oPgFrm.Page1.oPag.oMTMAGCAR_1_48.value==this.w_MTMAGCAR)
      this.oPgFrm.Page1.oPag.oMTMAGCAR_1_48.value=this.w_MTMAGCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oMTFLCARI_1_55.RadioValue()==this.w_MTFLCARI)
      this.oPgFrm.Page1.oPag.oMTFLCARI_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMTMAGSCA_1_57.value==this.w_MTMAGSCA)
      this.oPgFrm.Page1.oPag.oMTMAGSCA_1_57.value=this.w_MTMAGSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oMTFLSCAR_1_58.RadioValue()==this.w_MTFLSCAR)
      this.oPgFrm.Page1.oPag.oMTFLSCAR_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oULTPROG_1_60.value==this.w_ULTPROG)
      this.oPgFrm.Page1.oPag.oULTPROG_1_60.value=this.w_ULTPROG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ARTLOT=.w_CODART)  and not(.w_CODMAG<>.w_MTMAGSCA)  and not(empty(.w_SECODLOT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECODLOT_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lotto inesistente o di un altro articolo")
          case   not(.w_ARTLOT=.w_CODART)  and not(.w_CODMAG<>.w_MTMAGCAR And Empty(.w_MTMAGCAR))  and not(empty(.w_MTCODLOT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMTCODLOT_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lotto inesistente o di un altro articolo")
          case   (empty(.w_QTAUM1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oQTAUM1_1_20.SetFocus()
            i_bnoObbl = !empty(.w_QTAUM1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NPROG >= 0 and .w_NPROG <= .w_QTAUM1-.w_CONTA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNPROG_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ULTPROG >= 0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oULTPROG_1_60.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MTCODLOT = this.w_MTCODLOT
    return

enddefine

* --- Define pages as container
define class tgsmd_kcrPag1 as StdContainer
  Width  = 728
  height = 482
  stdWidth  = 728
  stdheight = 482
  resizeXpos=367
  resizeYpos=195
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESART_1_4 as StdField with uid="ZLXWOVIZXZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209965514,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=71, Top=3, InputMask=replicate('X',40)


  add object oObj_1_6 as cp_runprogram with uid="IKTKTRDRQJ",left=222, top=494, width=220,height=24,;
    caption='GSVE_BCR',;
   bGlobalFont=.t.,;
    prg="GSMD_BCR('C')",;
    cEvent = "ZOOMSEL row checked",;
    nPag=1;
    , HelpContextID = 38825144

  add object oSECODLOT_1_9 as StdField with uid="XLHJCQUWSZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SECODLOT", cQueryName = "SECODLOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Lotto inesistente o di un altro articolo",;
    ToolTipText = "Codice lotto selezionato per la ricerca",;
    HelpContextID = 89575558,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=146, Left=202, Top=37, InputMask=replicate('X',20), bHasZoom = .t. , ForeColor=RGB(128,128,0), cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_SECODLOT"

  func oSECODLOT_1_9.mHide()
    with this.Parent.oContained
      return (.w_CODMAG<>.w_MTMAGSCA)
    endwith
  endfunc

  func oSECODLOT_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  proc oSECODLOT_1_9.mZoom
  endproc

  add object oSECODUBI_1_11 as StdField with uid="YHFZZSLLDN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SECODUBI", cQueryName = "SECODUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione selezionata per la ricerca",;
    HelpContextID = 61419375,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=146, Left=351, Top=37, InputMask=replicate('X',20), ForeColor=RGB(128,128,0), cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_SECODUBI"

  func oSECODUBI_1_11.mHide()
    with this.Parent.oContained
      return (.w_CODMAG<>.w_MTMAGSCA)
    endwith
  endfunc

  func oSECODUBI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMTCODLOT_1_12 as StdField with uid="OTTOSNRIVX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MTCODLOT", cQueryName = "MTCODLOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Lotto inesistente o di un altro articolo",;
    ToolTipText = "Codice lotto selezionato per l'inserimento della matricola",;
    HelpContextID = 89571814,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=202, Top=63, InputMask=replicate('X',20), bHasZoom = .t. , ForeColor=RGB(0,0,255), cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_MTCODLOT"

  func oMTCODLOT_1_12.mHide()
    with this.Parent.oContained
      return (.w_CODMAG<>.w_MTMAGCAR And Empty(.w_MTMAGCAR))
    endwith
  endfunc

  func oMTCODLOT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  proc oMTCODLOT_1_12.mZoom
  endproc

  add object oCODUBI_1_13 as StdField with uid="MHUQLNNIPJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione selezionata per l'inserimento della matricola",;
    HelpContextID = 141604826,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=351, Top=63, InputMask=replicate('X',20), ForeColor=RGB(0,0,255), cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MAGUBI", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBI"

  func oCODUBI_1_13.mHide()
    with this.Parent.oContained
      return (.w_CODMAG<>.w_MTMAGCAR  And Empty(.w_MTMAGCAR))
    endwith
  endfunc

  func oCODUBI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODUBISCA_1_14 as StdField with uid="VZNNFUCYRU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODUBISCA", cQueryName = "CODUBISCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione magazzino scarti selezionata per l'inserimento della matricola",;
    HelpContextID = 126831737,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=503, Top=63, InputMask=replicate('X',20), ForeColor=RGB(0,0,255), cLinkFile="UBICAZIO", cZoomOnZoom="GSAR_AUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MAGUBI1", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBISCA"

  func oCODUBISCA_1_14.mHide()
    with this.Parent.oContained
      return (Empty(.w_MAGSCA) or .w_MAGSCA=.w_MTMAGCAR)
    endwith
  endfunc

  func oCODUBISCA_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oUNIMIS1_1_18 as StdField with uid="GRGGGLKVDJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_UNIMIS1", cQueryName = "UNIMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 235431866,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=506, Top=4, InputMask=replicate('X',3)

  add object oQTAUM1_1_20 as StdField with uid="PWZTAHGMOE",rtseq=15,rtrep=.f.,;
    cFormVar = "w_QTAUM1", cQueryName = "QTAUM1",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� movimentata nella prima unit� di misura",;
    HelpContextID = 264299002,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=587, Top=4, cSayPict="v_pv(28)", cGetPict='"99999999"'

  add object oNPROG_1_23 as StdField with uid="RFHTMKRDST",rtseq=18,rtrep=.f.,;
    cFormVar = "w_NPROG", cQueryName = "NPROG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero matricole selezionate",;
    HelpContextID = 19256874,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=259, Top=444, cSayPict='"999999999999"', cGetPict='"999999999999"'

  func oNPROG_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NPROG >= 0 and .w_NPROG <= .w_QTAUM1-.w_CONTA)
    endwith
    return bRes
  endfunc


  add object oBtn_1_24 as StdButton with uid="BUJGGYAQUX",left=413, top=444, width=24,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare le matricole";
    , HelpContextID = 99039786;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSMD_BCR(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_NPROG>0)
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="TNGHLKHEBJ",left=532, top=444, width=24,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per deselezionare le matricole";
    , HelpContextID = 99039786;
  , bGlobalFont=.t.

    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSMD_BCR(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CONTA>0)
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="QNLSAIHLGU",left=639, top=121, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 86803386;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSVE_BCR(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CODMAG<>.w_MTMAGCAR)
     endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="EPMPENVLUD",left=668, top=4, width=48,height=45,;
    CpPicture="bmp\genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per caricare nuove matricole";
    , HelpContextID = 224264602;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSMD_BCR(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CODMAG<>.w_MTMAGCAR or .w_FLAUTO<>'A')
     endwith
    endif
  endfunc


  add object ZoomCar as cp_zoombox with uid="AWXTJLPKWG",left=7, top=271, width=706,height=156,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MATRICOL",cZoomFile="GSMD1KCR",bOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bAdvOptions=.t.,cMenuFile="",cZoomOnZoom="GSMD_AMT",bRetriveAllRows=.f.,;
    nPag=1;
    , HelpContextID = 78756838


  add object ZoomSel as cp_szoombox with uid="CZSVCBIDGT",left=7, top=99, width=707,height=164,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MATRICOL",cZoomFile="GSMD_KCR",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="GSMD_AMT",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 78756838


  add object oBtn_1_30 as StdButton with uid="HZUGTMEYRQ",left=617, top=432, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare movimenti matricole";
    , HelpContextID = 99212058;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSMD_BCR(this.Parent.oContained,"L")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CONTA>0)
      endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="FRSAGQFIZA",left=668, top=432, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91923386;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_37 as cp_runprogram with uid="HZTIPOIXTU",left=446, top=494, width=221,height=24,;
    caption='GSVE_BCR',;
   bGlobalFont=.t.,;
    prg="GSMD_BCR('U')",;
    cEvent = "ZOOMSEL row unchecked",;
    nPag=1;
    , HelpContextID = 38825144

  add object oCONTA_1_38 as StdField with uid="RCAQEGOCXS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CONTA", cQueryName = "CONTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25237466,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=92, Top=444, cSayPict='"999999"', cGetPict='"999999"'


  add object oObj_1_47 as cp_runprogram with uid="BPCRIWNKUU",left=222, top=518, width=118,height=24,;
    caption='GSVE_BCR',;
   bGlobalFont=.t.,;
    prg="GSMD_BCR('R')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 38825144

  add object oMTMAGCAR_1_48 as StdField with uid="MKSVILDVZA",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MTMAGCAR", cQueryName = "MTMAGCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 238297576,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=71, Top=63, InputMask=replicate('X',5)

  func oMTMAGCAR_1_48.mHide()
    with this.Parent.oContained
      return (Empty(.w_MTMAGCAR))
    endwith
  endfunc


  add object oMTFLCARI_1_55 as StdCombo with uid="XXMJJTTNAC",rtseq=36,rtrep=.f.,left=123,top=63,width=76,height=21, enabled=.f.;
    , ToolTipText = "Tipo di carico";
    , HelpContextID = 6918641;
    , cFormVar="w_MTFLCARI",RowSource=""+"Riservato,"+"Esistenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMTFLCARI_1_55.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oMTFLCARI_1_55.GetRadio()
    this.Parent.oContained.w_MTFLCARI = this.RadioValue()
    return .t.
  endfunc

  func oMTFLCARI_1_55.SetRadio()
    this.Parent.oContained.w_MTFLCARI=trim(this.Parent.oContained.w_MTFLCARI)
    this.value = ;
      iif(this.Parent.oContained.w_MTFLCARI=='R',1,;
      iif(this.Parent.oContained.w_MTFLCARI=='E',2,;
      0))
  endfunc

  func oMTFLCARI_1_55.mHide()
    with this.Parent.oContained
      return (Empty(.w_MTMAGCAR))
    endwith
  endfunc

  add object oMTMAGSCA_1_57 as StdField with uid="RLPRITDPHT",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MTMAGSCA", cQueryName = "MTMAGSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 30137863,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=71, Top=37, InputMask=replicate('X',5)

  func oMTMAGSCA_1_57.mHide()
    with this.Parent.oContained
      return (Empty(.w_MTMAGSCA))
    endwith
  endfunc


  add object oMTFLSCAR_1_58 as StdCombo with uid="ZVJGZFXVVU",rtseq=38,rtrep=.f.,left=122,top=36,width=76,height=21, enabled=.f.;
    , ToolTipText = "Tipo di carico";
    , HelpContextID = 225022440;
    , cFormVar="w_MTFLSCAR",RowSource=""+"Riservato,"+"Esistenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMTFLSCAR_1_58.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oMTFLSCAR_1_58.GetRadio()
    this.Parent.oContained.w_MTFLSCAR = this.RadioValue()
    return .t.
  endfunc

  func oMTFLSCAR_1_58.SetRadio()
    this.Parent.oContained.w_MTFLSCAR=trim(this.Parent.oContained.w_MTFLSCAR)
    this.value = ;
      iif(this.Parent.oContained.w_MTFLSCAR=='R',1,;
      iif(this.Parent.oContained.w_MTFLSCAR=='E',2,;
      0))
  endfunc

  func oMTFLSCAR_1_58.mHide()
    with this.Parent.oContained
      return (Empty(.w_MTMAGSCA))
    endwith
  endfunc

  add object oULTPROG_1_60 as StdField with uid="NQOKSCDMNH",rtseq=39,rtrep=.f.,;
    cFormVar = "w_ULTPROG", cQueryName = "ULTPROG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Visualizza solo le matricole con progressivo maggiore o uguale a questo",;
    HelpContextID = 244008518,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=553, Top=87, cSayPict='"999999999999"', cGetPict='"999999999999"'

  func oULTPROG_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ULTPROG >= 0)
    endwith
    return bRes
  endfunc


  add object oBtn_1_62 as StdButton with uid="IWDUZKSZIR",left=668, top=52, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 77681430;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      with this.Parent.oContained
        GSMD_BCR(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_76 as StdButton with uid="VTZYOFFXNN",left=15, top=432, width=48,height=45,;
    CpPicture="bmp\lotti.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per creare un nuovo lotto con i parametri impostati";
    , HelpContextID = 243177290;
    , TabStop=.f.,Caption='\<Lotto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_76.Click()
      with this.Parent.oContained
        do GSMA_BKL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_76.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not ( g_PERLOT='S' AND .w_FLLOTT='S'  And .w_ESIRIS='E' And Not Empty(.w_MTMAGCAR) ))
     endwith
    endif
  endfunc

  add object oStr_1_15 as StdString with uid="PVZWAFKICF",Visible=.t., Left=202, Top=24,;
    Alignment=0, Width=31, Height=15,;
    Caption="Lotto"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_CODMAG<>.w_MTMAGSCA)
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="KEDEFKDRHD",Visible=.t., Left=351, Top=23,;
    Alignment=0, Width=61, Height=17,;
    Caption="Ubicazione"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_CODMAG<>.w_MTMAGSCA)
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="ROOUJJSJCM",Visible=.t., Left=475, Top=7,;
    Alignment=1, Width=27, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TTSQTOOQQT",Visible=.t., Left=550, Top=7,;
    Alignment=1, Width=35, Height=20,;
    Caption="Qt�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KEPFNBBYWH",Visible=.t., Left=87, Top=427,;
    Alignment=0, Width=113, Height=18,;
    Caption="Totale selezionate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="VUMMUAMHTQ",Visible=.t., Left=7, Top=89,;
    Alignment=0, Width=300, Height=18,;
    Caption="Matricole da selezionare per movimento di carico"    , forecolor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (Empty( .w_MTMAGCAR ))
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="ZWQKCCJHIP",Visible=.t., Left=7, Top=89,;
    Alignment=0, Width=314, Height=18,;
    Caption="Matricole da selezionare per movimento di scarico"    , forecolor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (Not Empty( .w_MTMAGCAR ))
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="BUGFQWKMXZ",Visible=.t., Left=11, Top=65,;
    Alignment=1, Width=56, Height=18,;
    Caption="Carico:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (Empty(.w_MTMAGCAR))
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="EQXKJFWQDI",Visible=.t., Left=6, Top=39,;
    Alignment=1, Width=61, Height=18,;
    Caption="Scarico:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (Empty(.w_MTMAGSCA))
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="YDPVSAQQJT",Visible=.t., Left=351, Top=89,;
    Alignment=1, Width=198, Height=18,;
    Caption="Progressivo di riferimento ricerca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="OGJJZWNSXX",Visible=.t., Left=9, Top=262,;
    Alignment=0, Width=127, Height=18,;
    Caption="Matricole selezionate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_65 as StdString with uid="QZWLKFSNTD",Visible=.t., Left=224, Top=427,;
    Alignment=0, Width=117, Height=18,;
    Caption="Selezione matricole"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_67 as StdString with uid="TVVIZVMLKA",Visible=.t., Left=225, Top=448,;
    Alignment=1, Width=33, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="JCCRXKENQK",Visible=.t., Left=341, Top=448,;
    Alignment=1, Width=67, Height=18,;
    Caption="Seleziona"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="TOBAIUYCIC",Visible=.t., Left=445, Top=448,;
    Alignment=1, Width=82, Height=18,;
    Caption="Deseleziona"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="KBFKZCCBAM",Visible=.t., Left=24, Top=5,;
    Alignment=1, Width=43, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="JKQHVWJWNU",Visible=.t., Left=202, Top=48,;
    Alignment=0, Width=31, Height=15,;
    Caption="Lotto"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (!Empty(.w_MTMAGSCA))
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="BKDIHPILEP",Visible=.t., Left=351, Top=47,;
    Alignment=0, Width=61, Height=17,;
    Caption="Ubicazione"  ;
  , bGlobalFont=.t.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (!Empty(.w_MTMAGSCA))
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="PSTTLWWAZD",Visible=.t., Left=503, Top=47,;
    Alignment=0, Width=155, Height=17,;
    Caption="Ubicazione magaz. scarti"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (Empty(.w_MAGSCA) or .w_MAGSCA=.w_MTMAGCAR)
    endwith
  endfunc

  add object oStr_1_82 as StdString with uid="PIYXCDXDVP",Visible=.t., Left=336, Top=427,;
    Alignment=0, Width=61, Height=18,;
    Caption="conformi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_82.mHide()
    with this.Parent.oContained
      return (not .w_PRODUZ or .w_CONTACAR>=.w_QTACA1)
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="JYOYGCYEJC",Visible=.t., Left=336, Top=427,;
    Alignment=0, Width=78, Height=18,;
    Caption="non conformi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (not .w_PRODUZ or .w_CONTACAR<.w_QTACA1)
    endwith
  endfunc

  add object oBox_1_66 as StdBox with uid="OIDSMVCXDZ",left=87, top=440, width=483,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_kcr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
