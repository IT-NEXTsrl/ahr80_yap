* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bbk                                                        *
*              Codice SSCC                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-27                                                      *
* Last revis.: 2004-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bbk",oParentObject)
return(i_retval)

define class tgsmd_bbk as StdBatch
  * --- Local variables
  w_NUMINI = space(6)
  w_NUMFIN = space(6)
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_SERIE1 = space(2)
  w_SERIE2 = space(2)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Quantit� Movimentata (da GSMD_AUL)
    * --- Dichiaro e sbianco i filtri
    * --- Select from GSMD1AUL
    do vq_exec with 'GSMD1AUL',this,'_Curs_GSMD1AUL','',.f.,.t.
    if used('_Curs_GSMD1AUL')
      select _Curs_GSMD1AUL
      locate for 1=1
      do while not(eof())
      * --- Assegno la Qt� movimentata nei documenti con Packing list attivo
      this.oParentObject.w_QTAMOV = QTAMOV
      this.oParentObject.w_NUMCOL = MVNUMCOL
        select _Curs_GSMD1AUL
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSMD1AUL')
      use in _Curs_GSMD1AUL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
