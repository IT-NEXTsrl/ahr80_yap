* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: itn_bccarpar                                                    *
*              Caricamento parametri e configurazioni                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2018-11-18                                                      *
* Last revis.: 2018-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("titn_bccarpar",oParentObject)
return(i_retval)

define class titn_bccarpar as StdBatch
  * --- Local variables
  w_CHKRECORD = 0
  * --- WorkFile variables
  ITNPARCONF_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa routine verifica se presente una configurazione per l'azienda in cui si sta lavorando.
    *     Se non presente la crea vuota.
    *     Dopo di che carica la configurazione.
    this.w_CHKRECORD = 0
    * --- Select from ITNPARCONF
    i_nConn=i_TableProp[this.ITNPARCONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ITNPARCONF_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ITNCODAZI  from "+i_cTable+" ITNPARCONF ";
           ,"_Curs_ITNPARCONF")
    else
      select ITNCODAZI from (i_cTable);
        into cursor _Curs_ITNPARCONF
    endif
    if used('_Curs_ITNPARCONF')
      select _Curs_ITNPARCONF
      locate for 1=1
      if not(eof())
      do while not(eof())
      this.w_CHKRECORD = this.w_CHKRECORD + 1
        select _Curs_ITNPARCONF
        continue
      enddo
      else
        this.w_CHKRECORD = 0
        select _Curs_ITNPARCONF
      endif
      use
    endif
    if this.w_CHKRECORD = 0
      * --- Insert into ITNPARCONF
      i_nConn=i_TableProp[this.ITNPARCONF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ITNPARCONF_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ITNPARCONF_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ITNCODAZI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(i_CODAZI),'ITNPARCONF','ITNCODAZI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ITNCODAZI',i_CODAZI)
        insert into (i_cTable) (ITNCODAZI &i_ccchkf. );
           values (;
             i_CODAZI;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    this.oparentobject.ecpfilter
    this.oparentobject.w_ITNCODAZI = i_CODAZI
    this.oparentobject.ecpsave
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ITNPARCONF'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ITNPARCONF')
      use in _Curs_ITNPARCONF
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
