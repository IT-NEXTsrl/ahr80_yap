* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_ara                                                        *
*              Rating                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-05                                                      *
* Last revis.: 2012-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_ara"))

* --- Class definition
define class tgsdf_ara as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 498
  Height = 153+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-27"
  HelpContextID=126497897
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  DORATING_IDX = 0
  cFile = "DORATING"
  cKeySelect = "RACODICE"
  cKeyWhere  = "RACODICE=this.w_RACODICE"
  cKeyWhereODBC = '"RACODICE="+cp_ToStrODBC(this.w_RACODICE)';

  cKeyWhereODBCqualified = '"DORATING.RACODICE="+cp_ToStrODBC(this.w_RACODICE)';

  cPrg = "gsdf_ara"
  cComment = "Rating"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RACODICE = space(2)
  w_RADESCRI = space(50)
  w_RASCADUT = space(2)
  w_RAGIORNI = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DORATING','gsdf_ara')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_araPag1","gsdf_ara",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Rating")
      .Pages(1).HelpContextID = 154766826
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRACODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DORATING'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DORATING_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DORATING_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RACODICE = NVL(RACODICE,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DORATING where RACODICE=KeySet.RACODICE
    *
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DORATING')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DORATING.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DORATING '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RACODICE',this.w_RACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RACODICE = NVL(RACODICE,space(2))
        .w_RADESCRI = NVL(RADESCRI,space(50))
        .w_RASCADUT = NVL(RASCADUT,space(2))
          * evitabile
          *.link_1_6('Load')
        .w_RAGIORNI = NVL(RAGIORNI,0)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        cp_LoadRecExtFlds(this,'DORATING')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RACODICE = space(2)
      .w_RADESCRI = space(50)
      .w_RASCADUT = space(2)
      .w_RAGIORNI = 0
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_RASCADUT))
          .link_1_6('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DORATING')
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRACODICE_1_2.enabled = i_bVal
      .Page1.oPag.oRADESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oRASCADUT_1_6.enabled = i_bVal
      .Page1.oPag.oRAGIORNI_1_8.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRACODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRACODICE_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DORATING',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RACODICE,"RACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RADESCRI,"RADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RASCADUT,"RASCADUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RAGIORNI,"RAGIORNI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    i_lTable = "DORATING"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DORATING_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DORATING_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DORATING_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DORATING
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DORATING')
        i_extval=cp_InsertValODBCExtFlds(this,'DORATING')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RACODICE,RADESCRI,RASCADUT,RAGIORNI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RACODICE)+;
                  ","+cp_ToStrODBC(this.w_RADESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_RASCADUT)+;
                  ","+cp_ToStrODBC(this.w_RAGIORNI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DORATING')
        i_extval=cp_InsertValVFPExtFlds(this,'DORATING')
        cp_CheckDeletedKey(i_cTable,0,'RACODICE',this.w_RACODICE)
        INSERT INTO (i_cTable);
              (RACODICE,RADESCRI,RASCADUT,RAGIORNI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RACODICE;
                  ,this.w_RADESCRI;
                  ,this.w_RASCADUT;
                  ,this.w_RAGIORNI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DORATING_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DORATING_IDX,i_nConn)
      *
      * update DORATING
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DORATING')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RADESCRI="+cp_ToStrODBC(this.w_RADESCRI)+;
             ",RASCADUT="+cp_ToStrODBCNull(this.w_RASCADUT)+;
             ",RAGIORNI="+cp_ToStrODBC(this.w_RAGIORNI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DORATING')
        i_cWhere = cp_PKFox(i_cTable  ,'RACODICE',this.w_RACODICE  )
        UPDATE (i_cTable) SET;
              RADESCRI=this.w_RADESCRI;
             ,RASCADUT=this.w_RASCADUT;
             ,RAGIORNI=this.w_RAGIORNI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DORATING_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DORATING_IDX,i_nConn)
      *
      * delete DORATING
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RACODICE',this.w_RACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RASCADUT
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_lTable = "DORATING"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2], .t., this.DORATING_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RASCADUT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsdf_Ara',True,'DORATING')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RACODICE like "+cp_ToStrODBC(trim(this.w_RASCADUT)+"%");

          i_ret=cp_SQL(i_nConn,"select RACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RACODICE',trim(this.w_RASCADUT))
          select RACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RASCADUT)==trim(_Link_.RACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RASCADUT) and !this.bDontReportError
            deferred_cp_zoom('DORATING','*','RACODICE',cp_AbsName(oSource.parent,'oRASCADUT_1_6'),i_cWhere,'Gsdf_Ara',"Rating",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',oSource.xKey(1))
            select RACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RASCADUT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(this.w_RASCADUT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',this.w_RASCADUT)
            select RACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RASCADUT = NVL(_Link_.RACODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_RASCADUT = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])+'\'+cp_ToStr(_Link_.RACODICE,1)
      cp_ShowWarn(i_cKey,this.DORATING_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RASCADUT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRACODICE_1_2.value==this.w_RACODICE)
      this.oPgFrm.Page1.oPag.oRACODICE_1_2.value=this.w_RACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oRADESCRI_1_4.value==this.w_RADESCRI)
      this.oPgFrm.Page1.oPag.oRADESCRI_1_4.value=this.w_RADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRASCADUT_1_6.value==this.w_RASCADUT)
      this.oPgFrm.Page1.oPag.oRASCADUT_1_6.value=this.w_RASCADUT
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGIORNI_1_8.value==this.w_RAGIORNI)
      this.oPgFrm.Page1.oPag.oRAGIORNI_1_8.value=this.w_RAGIORNI
    endif
    cp_SetControlsValueExtFlds(this,'DORATING')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdf_araPag1 as StdContainer
  Width  = 494
  height = 153
  stdWidth  = 494
  stdheight = 153
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRACODICE_1_2 as StdField with uid="RDSVZOZLMW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RACODICE", cQueryName = "RACODICE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Rating",;
    HelpContextID = 101270107,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=118, Top=15, InputMask=replicate('X',2)

  add object oRADESCRI_1_4 as StdField with uid="REPUSIDHDZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RADESCRI", cQueryName = "RADESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 252751265,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=118, Top=48, InputMask=replicate('X',50)

  add object oRASCADUT_1_6 as StdField with uid="NZVIOHLKJP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RASCADUT", cQueryName = "RASCADUT",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 13517418,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=118, Top=79, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="DORATING", cZoomOnZoom="Gsdf_Ara", oKey_1_1="RACODICE", oKey_1_2="this.w_RASCADUT"

  func oRASCADUT_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oRASCADUT_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRASCADUT_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DORATING','*','RACODICE',cp_AbsName(this.parent,'oRASCADUT_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsdf_Ara',"Rating",'',this.parent.oContained
  endproc
  proc oRASCADUT_1_6.mZoomOnZoom
    local i_obj
    i_obj=Gsdf_Ara()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RACODICE=this.parent.oContained.w_RASCADUT
     i_obj.ecpSave()
  endproc

  add object oRAGIORNI_1_8 as StdField with uid="IEXHIKHLPY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RAGIORNI", cQueryName = "RAGIORNI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Scaduto da giorni",;
    HelpContextID = 5012897,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=230, Top=79, cSayPict='"999"', cGetPict='"999"'


  add object oObj_1_9 as cp_runprogram with uid="QQLMSUMQRR",left=-2, top=166, width=236,height=19,;
    caption='GSDF_BDM',;
   bGlobalFont=.t.,;
    prg="GSDF_BDM('RATINGCANC')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 11559859

  add object oStr_1_1 as StdString with uid="UEHDKOXGJS",Visible=.t., Left=51, Top=17,;
    Alignment=1, Width=64, Height=18,;
    Caption="Rating:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="LGTLRJVSQA",Visible=.t., Left=6, Top=49,;
    Alignment=1, Width=109, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="JGVFATJTAI",Visible=.t., Left=9, Top=83,;
    Alignment=1, Width=106, Height=18,;
    Caption="Rating scaduto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QCRZKDEILJ",Visible=.t., Left=166, Top=83,;
    Alignment=1, Width=62, Height=18,;
    Caption="da giorni:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_ara','DORATING','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RACODICE=DORATING.RACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
