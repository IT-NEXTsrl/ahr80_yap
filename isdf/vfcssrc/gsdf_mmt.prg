* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_mmt                                                        *
*              Movimenti DocFinance importati                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-15                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_mmt"))

* --- Class definition
define class tgsdf_mmt as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 760
  Height = 473+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-14"
  HelpContextID=197801065
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=36

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DOCFINIM_IDX = 0
  cFile = "DOCFINIM"
  cKeySelect = "DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME,DFDATREG,DFPIANOC,DFCONTRO,DFCHIAVE"
  cKeyWhere  = "DFCODAZI=this.w_DFCODAZI and DFNUMERA=this.w_DFNUMERA and DF__ANNO=this.w_DF__ANNO and DFMOVIME=this.w_DFMOVIME and DFDATREG=this.w_DFDATREG and DFPIANOC=this.w_DFPIANOC and DFCONTRO=this.w_DFCONTRO and DFCHIAVE=this.w_DFCHIAVE"
  cKeyDetail  = "DFCODAZI=this.w_DFCODAZI and DFNUMERA=this.w_DFNUMERA and DF__ANNO=this.w_DF__ANNO and DFMOVIME=this.w_DFMOVIME and DFDATREG=this.w_DFDATREG and DFPIANOC=this.w_DFPIANOC and DFCONTRO=this.w_DFCONTRO and DFCHIAVE=this.w_DFCHIAVE"
  cKeyWhereODBC = '"DFCODAZI="+cp_ToStrODBC(this.w_DFCODAZI)';
      +'+" and DFNUMERA="+cp_ToStrODBC(this.w_DFNUMERA)';
      +'+" and DF__ANNO="+cp_ToStrODBC(this.w_DF__ANNO)';
      +'+" and DFMOVIME="+cp_ToStrODBC(this.w_DFMOVIME)';
      +'+" and DFDATREG="+cp_ToStrODBC(this.w_DFDATREG,"D")';
      +'+" and DFPIANOC="+cp_ToStrODBC(this.w_DFPIANOC)';
      +'+" and DFCONTRO="+cp_ToStrODBC(this.w_DFCONTRO)';
      +'+" and DFCHIAVE="+cp_ToStrODBC(this.w_DFCHIAVE)';

  cKeyDetailWhereODBC = '"DFCODAZI="+cp_ToStrODBC(this.w_DFCODAZI)';
      +'+" and DFNUMERA="+cp_ToStrODBC(this.w_DFNUMERA)';
      +'+" and DF__ANNO="+cp_ToStrODBC(this.w_DF__ANNO)';
      +'+" and DFMOVIME="+cp_ToStrODBC(this.w_DFMOVIME)';
      +'+" and DFDATREG="+cp_ToStrODBC(this.w_DFDATREG,"D")';
      +'+" and DFPIANOC="+cp_ToStrODBC(this.w_DFPIANOC)';
      +'+" and DFCONTRO="+cp_ToStrODBC(this.w_DFCONTRO)';
      +'+" and DFCHIAVE="+cp_ToStrODBC(this.w_DFCHIAVE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DOCFINIM.DFCODAZI="+cp_ToStrODBC(this.w_DFCODAZI)';
      +'+" and DOCFINIM.DFNUMERA="+cp_ToStrODBC(this.w_DFNUMERA)';
      +'+" and DOCFINIM.DF__ANNO="+cp_ToStrODBC(this.w_DF__ANNO)';
      +'+" and DOCFINIM.DFMOVIME="+cp_ToStrODBC(this.w_DFMOVIME)';
      +'+" and DOCFINIM.DFDATREG="+cp_ToStrODBC(this.w_DFDATREG,"D")';
      +'+" and DOCFINIM.DFPIANOC="+cp_ToStrODBC(this.w_DFPIANOC)';
      +'+" and DOCFINIM.DFCONTRO="+cp_ToStrODBC(this.w_DFCONTRO)';
      +'+" and DOCFINIM.DFCHIAVE="+cp_ToStrODBC(this.w_DFCHIAVE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DOCFINIM.CPROWNUM '
  cPrg = "gsdf_mmt"
  cComment = "Movimenti DocFinance importati"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  cAutoZoom = 'GSDF_MMT'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFCODAZI = space(10)
  w_DFNUMERA = space(4)
  w_DF__ANNO = space(4)
  w_DFMOVIME = space(10)
  w_DFDATREG = ctod('  /  /  ')
  w_DFCODCAU = space(4)
  w_DFTIPCON = space(1)
  w_DFCODICE = space(15)
  w_DFCODVAC = space(4)
  w_DFIMPVAL = 0
  w_DFCODVAL = space(5)
  w_DFIMPORT = 0
  w_DFDATVAL = ctod('  /  /  ')
  w_DFNUMPAR = 0
  w_DFCODBUN = space(6)
  w_DFPTTIPO = space(1)
  w_DFDATSCA = ctod('  /  /  ')
  w_DFMODPAG = space(10)
  w_DFPTSERI = space(10)
  w_DFPTROW = 0
  w_DFCPROW = 0
  w_DFPIANOC = space(16)
  w_DFCONTRO = space(22)
  w_DFCHIAVE = space(6)
  w_DFDATIMP = ctod('  /  /  ')
  w_DFERROR = space(1)
  w_DFFLGMOV = space(1)
  w_DFNUMEFF = 0
  w_DFDESOP = space(40)
  w_DF__NOTE = space(40)
  w_DF_BANCA = space(8)
  w_DFCODRBN = space(4)
  w_DFERRLOG = space(0)
  w_DFNOMFIL = space(0)
  w_DFSERTES = space(10)
  w_DFFLGDIS = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsdf_mmt
  Procedure ecpedit 
  IF NOT EMPTY (this.w_DFSERTES)
  ah_errormsg( "La modifica del movimento DocFinance non � possibile poich� � stato generato il movimento di tesoreria.",48,0)
  else
  dodefault()
  endif
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DOCFINIM','gsdf_mmt')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_mmtPag1","gsdf_mmt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimento DocFinance")
      .Pages(1).HelpContextID = 41571416
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDFCODAZI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DOCFINIM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOCFINIM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOCFINIM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_DFCODAZI = NVL(DFCODAZI,space(10))
      .w_DFNUMERA = NVL(DFNUMERA,space(4))
      .w_DF__ANNO = NVL(DF__ANNO,space(4))
      .w_DFMOVIME = NVL(DFMOVIME,space(10))
      .w_DFDATREG = NVL(DFDATREG,ctod("  /  /  "))
      .w_DFPIANOC = NVL(DFPIANOC,space(16))
      .w_DFCONTRO = NVL(DFCONTRO,space(22))
      .w_DFCHIAVE = NVL(DFCHIAVE,space(6))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DOCFINIM where DFCODAZI=KeySet.DFCODAZI
    *                            and DFNUMERA=KeySet.DFNUMERA
    *                            and DF__ANNO=KeySet.DF__ANNO
    *                            and DFMOVIME=KeySet.DFMOVIME
    *                            and DFDATREG=KeySet.DFDATREG
    *                            and DFPIANOC=KeySet.DFPIANOC
    *                            and DFCONTRO=KeySet.DFCONTRO
    *                            and DFCHIAVE=KeySet.DFCHIAVE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DOCFINIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINIM_IDX,2],this.bLoadRecFilter,this.DOCFINIM_IDX,"gsdf_mmt")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOCFINIM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOCFINIM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOCFINIM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFCODAZI',this.w_DFCODAZI  ,'DFNUMERA',this.w_DFNUMERA  ,'DF__ANNO',this.w_DF__ANNO  ,'DFMOVIME',this.w_DFMOVIME  ,'DFDATREG',this.w_DFDATREG  ,'DFPIANOC',this.w_DFPIANOC  ,'DFCONTRO',this.w_DFCONTRO  ,'DFCHIAVE',this.w_DFCHIAVE  )
      select * from (i_cTable) DOCFINIM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DFCODAZI = NVL(DFCODAZI,space(10))
        .w_DFNUMERA = NVL(DFNUMERA,space(4))
        .w_DF__ANNO = NVL(DF__ANNO,space(4))
        .w_DFMOVIME = NVL(DFMOVIME,space(10))
        .w_DFDATREG = NVL(cp_ToDate(DFDATREG),ctod("  /  /  "))
        .w_DFCODCAU = NVL(DFCODCAU,space(4))
        .w_DFPIANOC = NVL(DFPIANOC,space(16))
        .w_DFCONTRO = NVL(DFCONTRO,space(22))
        .w_DFCHIAVE = NVL(DFCHIAVE,space(6))
        .w_DFDATIMP = NVL(cp_ToDate(DFDATIMP),ctod("  /  /  "))
        .w_DFERROR = NVL(DFERROR,space(1))
        .w_DFFLGMOV = NVL(DFFLGMOV,space(1))
        .w_DFNOMFIL = NVL(DFNOMFIL,space(0))
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .w_DFSERTES = NVL(DFSERTES,space(10))
        .w_DFFLGDIS = NVL(DFFLGDIS,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DOCFINIM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DFTIPCON = NVL(DFTIPCON,space(1))
          .w_DFCODICE = NVL(DFCODICE,space(15))
          .w_DFCODVAC = NVL(DFCODVAC,space(4))
          .w_DFIMPVAL = NVL(DFIMPVAL,0)
          .w_DFCODVAL = NVL(DFCODVAL,space(5))
          .w_DFIMPORT = NVL(DFIMPORT,0)
          .w_DFDATVAL = NVL(cp_ToDate(DFDATVAL),ctod("  /  /  "))
          .w_DFNUMPAR = NVL(DFNUMPAR,0)
          .w_DFCODBUN = NVL(DFCODBUN,space(6))
          .w_DFPTTIPO = NVL(DFPTTIPO,space(1))
          .w_DFDATSCA = NVL(cp_ToDate(DFDATSCA),ctod("  /  /  "))
          .w_DFMODPAG = NVL(DFMODPAG,space(10))
          .w_DFPTSERI = NVL(DFPTSERI,space(10))
          .w_DFPTROW = NVL(DFPTROW,0)
          .w_DFCPROW = NVL(DFCPROW,0)
          .w_DFNUMEFF = NVL(DFNUMEFF,0)
          .w_DFDESOP = NVL(DFDESOP,space(40))
          .w_DF__NOTE = NVL(DF__NOTE,space(40))
          .w_DF_BANCA = NVL(DF_BANCA,space(8))
          .w_DFCODRBN = NVL(DFCODRBN,space(4))
          .w_DFERRLOG = NVL(DFERRLOG,space(0))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_29.enabled = .oPgFrm.Page1.oPag.oBtn_1_29.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_30.enabled = .oPgFrm.Page1.oPag.oBtn_1_30.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DFCODAZI=space(10)
      .w_DFNUMERA=space(4)
      .w_DF__ANNO=space(4)
      .w_DFMOVIME=space(10)
      .w_DFDATREG=ctod("  /  /  ")
      .w_DFCODCAU=space(4)
      .w_DFTIPCON=space(1)
      .w_DFCODICE=space(15)
      .w_DFCODVAC=space(4)
      .w_DFIMPVAL=0
      .w_DFCODVAL=space(5)
      .w_DFIMPORT=0
      .w_DFDATVAL=ctod("  /  /  ")
      .w_DFNUMPAR=0
      .w_DFCODBUN=space(6)
      .w_DFPTTIPO=space(1)
      .w_DFDATSCA=ctod("  /  /  ")
      .w_DFMODPAG=space(10)
      .w_DFPTSERI=space(10)
      .w_DFPTROW=0
      .w_DFCPROW=0
      .w_DFPIANOC=space(16)
      .w_DFCONTRO=space(22)
      .w_DFCHIAVE=space(6)
      .w_DFDATIMP=ctod("  /  /  ")
      .w_DFERROR=space(1)
      .w_DFFLGMOV=space(1)
      .w_DFNUMEFF=0
      .w_DFDESOP=space(40)
      .w_DF__NOTE=space(40)
      .w_DF_BANCA=space(8)
      .w_DFCODRBN=space(4)
      .w_DFERRLOG=space(0)
      .w_DFNOMFIL=space(0)
      .w_DFSERTES=space(10)
      .w_DFFLGDIS=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOCFINIM')
    this.DoRTCalc(1,36,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDFCODAZI_1_1.enabled = i_bVal
      .Page1.oPag.oDFNUMERA_1_3.enabled = i_bVal
      .Page1.oPag.oDF__ANNO_1_5.enabled = i_bVal
      .Page1.oPag.oDFMOVIME_1_7.enabled = i_bVal
      .Page1.oPag.oDFDATREG_1_9.enabled = i_bVal
      .Page1.oPag.oDFCODCAU_1_10.enabled = i_bVal
      .Page1.oPag.oDFPTTIPO_2_10.enabled = i_bVal
      .Page1.oPag.oDFDATSCA_2_11.enabled = i_bVal
      .Page1.oPag.oDFMODPAG_2_12.enabled = i_bVal
      .Page1.oPag.oDFPTSERI_2_13.enabled = i_bVal
      .Page1.oPag.oDFPTROW_2_14.enabled = i_bVal
      .Page1.oPag.oDFCPROW_2_15.enabled = i_bVal
      .Page1.oPag.oDFPIANOC_1_12.enabled = i_bVal
      .Page1.oPag.oDFCONTRO_1_13.enabled = i_bVal
      .Page1.oPag.oDFCHIAVE_1_15.enabled = i_bVal
      .Page1.oPag.oDFDATIMP_1_17.enabled = i_bVal
      .Page1.oPag.oDFERROR_1_20.enabled = i_bVal
      .Page1.oPag.oDFFLGMOV_1_21.enabled = i_bVal
      .Page1.oPag.oDFNUMEFF_2_21.enabled = i_bVal
      .Page1.oPag.oDFDESOP_2_24.enabled = i_bVal
      .Page1.oPag.oDF__NOTE_2_26.enabled = i_bVal
      .Page1.oPag.oDF_BANCA_2_28.enabled = i_bVal
      .Page1.oPag.oDFCODRBN_2_30.enabled = i_bVal
      .Page1.oPag.oDFERRLOG_2_31.enabled = i_bVal
      .Page1.oPag.oDFNOMFIL_1_22.enabled = i_bVal
      .Page1.oPag.oBtn_2_22.enabled = i_bVal
      .Page1.oPag.oBtn_1_29.enabled = .Page1.oPag.oBtn_1_29.mCond()
      .Page1.oPag.oBtn_1_30.enabled = .Page1.oPag.oBtn_1_30.mCond()
      .Page1.oPag.oObj_1_24.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDFCODAZI_1_1.enabled = .f.
        .Page1.oPag.oDFNUMERA_1_3.enabled = .f.
        .Page1.oPag.oDF__ANNO_1_5.enabled = .f.
        .Page1.oPag.oDFMOVIME_1_7.enabled = .f.
        .Page1.oPag.oDFDATREG_1_9.enabled = .f.
        .Page1.oPag.oDFPIANOC_1_12.enabled = .f.
        .Page1.oPag.oDFCONTRO_1_13.enabled = .f.
        .Page1.oPag.oDFCHIAVE_1_15.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDFCODAZI_1_1.enabled = .t.
        .Page1.oPag.oDFNUMERA_1_3.enabled = .t.
        .Page1.oPag.oDF__ANNO_1_5.enabled = .t.
        .Page1.oPag.oDFMOVIME_1_7.enabled = .t.
        .Page1.oPag.oDFDATREG_1_9.enabled = .t.
        .Page1.oPag.oDFPIANOC_1_12.enabled = .t.
        .Page1.oPag.oDFCONTRO_1_13.enabled = .t.
        .Page1.oPag.oDFCHIAVE_1_15.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DOCFINIM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOCFINIM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODAZI,"DFCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFNUMERA,"DFNUMERA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DF__ANNO,"DF__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFMOVIME,"DFMOVIME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFDATREG,"DFDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODCAU,"DFCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFPIANOC,"DFPIANOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCONTRO,"DFCONTRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCHIAVE,"DFCHIAVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFDATIMP,"DFDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFERROR,"DFERROR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFFLGMOV,"DFFLGMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFNOMFIL,"DFNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFSERTES,"DFSERTES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFFLGDIS,"DFFLGDIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOCFINIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINIM_IDX,2])
    i_lTable = "DOCFINIM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DOCFINIM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DFTIPCON C(1);
      ,t_DFCODICE C(15);
      ,t_DFCODVAC C(4);
      ,t_DFIMPVAL N(18,4);
      ,t_DFCODVAL C(5);
      ,t_DFIMPORT N(18,4);
      ,t_DFDATVAL D(8);
      ,t_DFNUMPAR N(6);
      ,t_DFPTTIPO N(3);
      ,t_DFDATSCA D(8);
      ,t_DFMODPAG C(10);
      ,t_DFPTSERI C(10);
      ,t_DFPTROW N(4);
      ,t_DFCPROW N(4);
      ,t_DFNUMEFF N(6);
      ,t_DFDESOP C(40);
      ,t_DF__NOTE C(40);
      ,t_DF_BANCA C(8);
      ,t_DFCODRBN C(4);
      ,t_DFERRLOG M(10);
      ,CPROWNUM N(10);
      ,t_DFCODBUN C(6);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsdf_mmtbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPCON_2_1.controlsource=this.cTrsName+'.t_DFTIPCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODICE_2_2.controlsource=this.cTrsName+'.t_DFCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODVAC_2_3.controlsource=this.cTrsName+'.t_DFCODVAC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPVAL_2_4.controlsource=this.cTrsName+'.t_DFIMPVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODVAL_2_5.controlsource=this.cTrsName+'.t_DFCODVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPORT_2_6.controlsource=this.cTrsName+'.t_DFIMPORT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFDATVAL_2_7.controlsource=this.cTrsName+'.t_DFDATVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFNUMPAR_2_8.controlsource=this.cTrsName+'.t_DFNUMPAR'
    this.oPgFRm.Page1.oPag.oDFPTTIPO_2_10.controlsource=this.cTrsName+'.t_DFPTTIPO'
    this.oPgFRm.Page1.oPag.oDFDATSCA_2_11.controlsource=this.cTrsName+'.t_DFDATSCA'
    this.oPgFRm.Page1.oPag.oDFMODPAG_2_12.controlsource=this.cTrsName+'.t_DFMODPAG'
    this.oPgFRm.Page1.oPag.oDFPTSERI_2_13.controlsource=this.cTrsName+'.t_DFPTSERI'
    this.oPgFRm.Page1.oPag.oDFPTROW_2_14.controlsource=this.cTrsName+'.t_DFPTROW'
    this.oPgFRm.Page1.oPag.oDFCPROW_2_15.controlsource=this.cTrsName+'.t_DFCPROW'
    this.oPgFRm.Page1.oPag.oDFNUMEFF_2_21.controlsource=this.cTrsName+'.t_DFNUMEFF'
    this.oPgFRm.Page1.oPag.oDFDESOP_2_24.controlsource=this.cTrsName+'.t_DFDESOP'
    this.oPgFRm.Page1.oPag.oDF__NOTE_2_26.controlsource=this.cTrsName+'.t_DF__NOTE'
    this.oPgFRm.Page1.oPag.oDF_BANCA_2_28.controlsource=this.cTrsName+'.t_DF_BANCA'
    this.oPgFRm.Page1.oPag.oDFCODRBN_2_30.controlsource=this.cTrsName+'.t_DFCODRBN'
    this.oPgFRm.Page1.oPag.oDFERRLOG_2_31.controlsource=this.cTrsName+'.t_DFERRLOG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(38)
    this.AddVLine(163)
    this.AddVLine(212)
    this.AddVLine(358)
    this.AddVLine(411)
    this.AddVLine(555)
    this.AddVLine(628)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPCON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOCFINIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINIM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOCFINIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINIM_IDX,2])
      *
      * insert into DOCFINIM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOCFINIM')
        i_extval=cp_InsertValODBCExtFlds(this,'DOCFINIM')
        i_cFldBody=" "+;
                  "(DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME,DFDATREG"+;
                  ",DFCODCAU,DFTIPCON,DFCODICE,DFCODVAC,DFIMPVAL"+;
                  ",DFCODVAL,DFIMPORT,DFDATVAL,DFNUMPAR,DFCODBUN"+;
                  ",DFPTTIPO,DFDATSCA,DFMODPAG,DFPTSERI,DFPTROW"+;
                  ",DFCPROW,DFPIANOC,DFCONTRO,DFCHIAVE,DFDATIMP"+;
                  ",DFERROR,DFFLGMOV,DFNUMEFF,DFDESOP,DF__NOTE"+;
                  ",DF_BANCA,DFCODRBN,DFERRLOG,DFNOMFIL,DFSERTES"+;
                  ",DFFLGDIS,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DFCODAZI)+","+cp_ToStrODBC(this.w_DFNUMERA)+","+cp_ToStrODBC(this.w_DF__ANNO)+","+cp_ToStrODBC(this.w_DFMOVIME)+","+cp_ToStrODBC(this.w_DFDATREG)+;
             ","+cp_ToStrODBC(this.w_DFCODCAU)+","+cp_ToStrODBC(this.w_DFTIPCON)+","+cp_ToStrODBC(this.w_DFCODICE)+","+cp_ToStrODBC(this.w_DFCODVAC)+","+cp_ToStrODBC(this.w_DFIMPVAL)+;
             ","+cp_ToStrODBC(this.w_DFCODVAL)+","+cp_ToStrODBC(this.w_DFIMPORT)+","+cp_ToStrODBC(this.w_DFDATVAL)+","+cp_ToStrODBC(this.w_DFNUMPAR)+","+cp_ToStrODBC(this.w_DFCODBUN)+;
             ","+cp_ToStrODBC(this.w_DFPTTIPO)+","+cp_ToStrODBC(this.w_DFDATSCA)+","+cp_ToStrODBC(this.w_DFMODPAG)+","+cp_ToStrODBC(this.w_DFPTSERI)+","+cp_ToStrODBC(this.w_DFPTROW)+;
             ","+cp_ToStrODBC(this.w_DFCPROW)+","+cp_ToStrODBC(this.w_DFPIANOC)+","+cp_ToStrODBC(this.w_DFCONTRO)+","+cp_ToStrODBC(this.w_DFCHIAVE)+","+cp_ToStrODBC(this.w_DFDATIMP)+;
             ","+cp_ToStrODBC(this.w_DFERROR)+","+cp_ToStrODBC(this.w_DFFLGMOV)+","+cp_ToStrODBC(this.w_DFNUMEFF)+","+cp_ToStrODBC(this.w_DFDESOP)+","+cp_ToStrODBC(this.w_DF__NOTE)+;
             ","+cp_ToStrODBC(this.w_DF_BANCA)+","+cp_ToStrODBC(this.w_DFCODRBN)+","+cp_ToStrODBC(this.w_DFERRLOG)+","+cp_ToStrODBC(this.w_DFNOMFIL)+","+cp_ToStrODBC(this.w_DFSERTES)+;
             ","+cp_ToStrODBC(this.w_DFFLGDIS)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOCFINIM')
        i_extval=cp_InsertValVFPExtFlds(this,'DOCFINIM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DFCODAZI',this.w_DFCODAZI,'DFNUMERA',this.w_DFNUMERA,'DF__ANNO',this.w_DF__ANNO,'DFMOVIME',this.w_DFMOVIME,'DFDATREG',this.w_DFDATREG,'DFPIANOC',this.w_DFPIANOC,'DFCONTRO',this.w_DFCONTRO,'DFCHIAVE',this.w_DFCHIAVE)
        INSERT INTO (i_cTable) (;
                   DFCODAZI;
                  ,DFNUMERA;
                  ,DF__ANNO;
                  ,DFMOVIME;
                  ,DFDATREG;
                  ,DFCODCAU;
                  ,DFTIPCON;
                  ,DFCODICE;
                  ,DFCODVAC;
                  ,DFIMPVAL;
                  ,DFCODVAL;
                  ,DFIMPORT;
                  ,DFDATVAL;
                  ,DFNUMPAR;
                  ,DFCODBUN;
                  ,DFPTTIPO;
                  ,DFDATSCA;
                  ,DFMODPAG;
                  ,DFPTSERI;
                  ,DFPTROW;
                  ,DFCPROW;
                  ,DFPIANOC;
                  ,DFCONTRO;
                  ,DFCHIAVE;
                  ,DFDATIMP;
                  ,DFERROR;
                  ,DFFLGMOV;
                  ,DFNUMEFF;
                  ,DFDESOP;
                  ,DF__NOTE;
                  ,DF_BANCA;
                  ,DFCODRBN;
                  ,DFERRLOG;
                  ,DFNOMFIL;
                  ,DFSERTES;
                  ,DFFLGDIS;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DFCODAZI;
                  ,this.w_DFNUMERA;
                  ,this.w_DF__ANNO;
                  ,this.w_DFMOVIME;
                  ,this.w_DFDATREG;
                  ,this.w_DFCODCAU;
                  ,this.w_DFTIPCON;
                  ,this.w_DFCODICE;
                  ,this.w_DFCODVAC;
                  ,this.w_DFIMPVAL;
                  ,this.w_DFCODVAL;
                  ,this.w_DFIMPORT;
                  ,this.w_DFDATVAL;
                  ,this.w_DFNUMPAR;
                  ,this.w_DFCODBUN;
                  ,this.w_DFPTTIPO;
                  ,this.w_DFDATSCA;
                  ,this.w_DFMODPAG;
                  ,this.w_DFPTSERI;
                  ,this.w_DFPTROW;
                  ,this.w_DFCPROW;
                  ,this.w_DFPIANOC;
                  ,this.w_DFCONTRO;
                  ,this.w_DFCHIAVE;
                  ,this.w_DFDATIMP;
                  ,this.w_DFERROR;
                  ,this.w_DFFLGMOV;
                  ,this.w_DFNUMEFF;
                  ,this.w_DFDESOP;
                  ,this.w_DF__NOTE;
                  ,this.w_DF_BANCA;
                  ,this.w_DFCODRBN;
                  ,this.w_DFERRLOG;
                  ,this.w_DFNOMFIL;
                  ,this.w_DFSERTES;
                  ,this.w_DFFLGDIS;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.DOCFINIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINIM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DF_BANCA))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DOCFINIM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " DFCODCAU="+cp_ToStrODBC(this.w_DFCODCAU)+;
                 ",DFDATIMP="+cp_ToStrODBC(this.w_DFDATIMP)+;
                 ",DFERROR="+cp_ToStrODBC(this.w_DFERROR)+;
                 ",DFFLGMOV="+cp_ToStrODBC(this.w_DFFLGMOV)+;
                 ",DFNOMFIL="+cp_ToStrODBC(this.w_DFNOMFIL)+;
                 ",DFSERTES="+cp_ToStrODBC(this.w_DFSERTES)+;
                 ",DFFLGDIS="+cp_ToStrODBC(this.w_DFFLGDIS)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DOCFINIM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  DFCODCAU=this.w_DFCODCAU;
                 ,DFDATIMP=this.w_DFDATIMP;
                 ,DFERROR=this.w_DFERROR;
                 ,DFFLGMOV=this.w_DFFLGMOV;
                 ,DFNOMFIL=this.w_DFNOMFIL;
                 ,DFSERTES=this.w_DFSERTES;
                 ,DFFLGDIS=this.w_DFFLGDIS;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DF_BANCA))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DOCFINIM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DOCFINIM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DFCODCAU="+cp_ToStrODBC(this.w_DFCODCAU)+;
                     ",DFTIPCON="+cp_ToStrODBC(this.w_DFTIPCON)+;
                     ",DFCODICE="+cp_ToStrODBC(this.w_DFCODICE)+;
                     ",DFCODVAC="+cp_ToStrODBC(this.w_DFCODVAC)+;
                     ",DFIMPVAL="+cp_ToStrODBC(this.w_DFIMPVAL)+;
                     ",DFCODVAL="+cp_ToStrODBC(this.w_DFCODVAL)+;
                     ",DFIMPORT="+cp_ToStrODBC(this.w_DFIMPORT)+;
                     ",DFDATVAL="+cp_ToStrODBC(this.w_DFDATVAL)+;
                     ",DFNUMPAR="+cp_ToStrODBC(this.w_DFNUMPAR)+;
                     ",DFCODBUN="+cp_ToStrODBC(this.w_DFCODBUN)+;
                     ",DFPTTIPO="+cp_ToStrODBC(this.w_DFPTTIPO)+;
                     ",DFDATSCA="+cp_ToStrODBC(this.w_DFDATSCA)+;
                     ",DFMODPAG="+cp_ToStrODBC(this.w_DFMODPAG)+;
                     ",DFPTSERI="+cp_ToStrODBC(this.w_DFPTSERI)+;
                     ",DFPTROW="+cp_ToStrODBC(this.w_DFPTROW)+;
                     ",DFCPROW="+cp_ToStrODBC(this.w_DFCPROW)+;
                     ",DFDATIMP="+cp_ToStrODBC(this.w_DFDATIMP)+;
                     ",DFERROR="+cp_ToStrODBC(this.w_DFERROR)+;
                     ",DFFLGMOV="+cp_ToStrODBC(this.w_DFFLGMOV)+;
                     ",DFNUMEFF="+cp_ToStrODBC(this.w_DFNUMEFF)+;
                     ",DFDESOP="+cp_ToStrODBC(this.w_DFDESOP)+;
                     ",DF__NOTE="+cp_ToStrODBC(this.w_DF__NOTE)+;
                     ",DF_BANCA="+cp_ToStrODBC(this.w_DF_BANCA)+;
                     ",DFCODRBN="+cp_ToStrODBC(this.w_DFCODRBN)+;
                     ",DFERRLOG="+cp_ToStrODBC(this.w_DFERRLOG)+;
                     ",DFNOMFIL="+cp_ToStrODBC(this.w_DFNOMFIL)+;
                     ",DFSERTES="+cp_ToStrODBC(this.w_DFSERTES)+;
                     ",DFFLGDIS="+cp_ToStrODBC(this.w_DFFLGDIS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DOCFINIM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DFCODCAU=this.w_DFCODCAU;
                     ,DFTIPCON=this.w_DFTIPCON;
                     ,DFCODICE=this.w_DFCODICE;
                     ,DFCODVAC=this.w_DFCODVAC;
                     ,DFIMPVAL=this.w_DFIMPVAL;
                     ,DFCODVAL=this.w_DFCODVAL;
                     ,DFIMPORT=this.w_DFIMPORT;
                     ,DFDATVAL=this.w_DFDATVAL;
                     ,DFNUMPAR=this.w_DFNUMPAR;
                     ,DFCODBUN=this.w_DFCODBUN;
                     ,DFPTTIPO=this.w_DFPTTIPO;
                     ,DFDATSCA=this.w_DFDATSCA;
                     ,DFMODPAG=this.w_DFMODPAG;
                     ,DFPTSERI=this.w_DFPTSERI;
                     ,DFPTROW=this.w_DFPTROW;
                     ,DFCPROW=this.w_DFCPROW;
                     ,DFDATIMP=this.w_DFDATIMP;
                     ,DFERROR=this.w_DFERROR;
                     ,DFFLGMOV=this.w_DFFLGMOV;
                     ,DFNUMEFF=this.w_DFNUMEFF;
                     ,DFDESOP=this.w_DFDESOP;
                     ,DF__NOTE=this.w_DF__NOTE;
                     ,DF_BANCA=this.w_DF_BANCA;
                     ,DFCODRBN=this.w_DFCODRBN;
                     ,DFERRLOG=this.w_DFERRLOG;
                     ,DFNOMFIL=this.w_DFNOMFIL;
                     ,DFSERTES=this.w_DFSERTES;
                     ,DFFLGDIS=this.w_DFFLGDIS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOCFINIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINIM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DF_BANCA))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DOCFINIM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DF_BANCA))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOCFINIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINIM_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,36,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DFCODBUN with this.w_DFCODBUN
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDFFLGMOV_1_21.enabled = this.oPgFrm.Page1.oPag.oDFFLGMOV_1_21.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oDFDATSCA_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDFDATSCA_2_11.mCond()
    this.oPgFrm.Page1.oPag.oDFMODPAG_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDFMODPAG_2_12.mCond()
    this.oPgFrm.Page1.oPag.oDFPTSERI_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDFPTSERI_2_13.mCond()
    this.oPgFrm.Page1.oPag.oDFPTROW_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDFPTROW_2_14.mCond()
    this.oPgFrm.Page1.oPag.oDFCPROW_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDFCPROW_2_15.mCond()
    this.oPgFrm.Page1.oPag.oDFNUMEFF_2_21.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDFNUMEFF_2_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_22.enabled =this.oPgFrm.Page1.oPag.oBtn_2_22.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBtn_2_22.visible=!this.oPgFrm.Page1.oPag.oBtn_2_22.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDFCODAZI_1_1.value==this.w_DFCODAZI)
      this.oPgFrm.Page1.oPag.oDFCODAZI_1_1.value=this.w_DFCODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oDFNUMERA_1_3.value==this.w_DFNUMERA)
      this.oPgFrm.Page1.oPag.oDFNUMERA_1_3.value=this.w_DFNUMERA
    endif
    if not(this.oPgFrm.Page1.oPag.oDF__ANNO_1_5.value==this.w_DF__ANNO)
      this.oPgFrm.Page1.oPag.oDF__ANNO_1_5.value=this.w_DF__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDFMOVIME_1_7.value==this.w_DFMOVIME)
      this.oPgFrm.Page1.oPag.oDFMOVIME_1_7.value=this.w_DFMOVIME
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDATREG_1_9.value==this.w_DFDATREG)
      this.oPgFrm.Page1.oPag.oDFDATREG_1_9.value=this.w_DFDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCODCAU_1_10.value==this.w_DFCODCAU)
      this.oPgFrm.Page1.oPag.oDFCODCAU_1_10.value=this.w_DFCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDFPTTIPO_2_10.RadioValue()==this.w_DFPTTIPO)
      this.oPgFrm.Page1.oPag.oDFPTTIPO_2_10.SetRadio()
      replace t_DFPTTIPO with this.oPgFrm.Page1.oPag.oDFPTTIPO_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDATSCA_2_11.value==this.w_DFDATSCA)
      this.oPgFrm.Page1.oPag.oDFDATSCA_2_11.value=this.w_DFDATSCA
      replace t_DFDATSCA with this.oPgFrm.Page1.oPag.oDFDATSCA_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFMODPAG_2_12.value==this.w_DFMODPAG)
      this.oPgFrm.Page1.oPag.oDFMODPAG_2_12.value=this.w_DFMODPAG
      replace t_DFMODPAG with this.oPgFrm.Page1.oPag.oDFMODPAG_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFPTSERI_2_13.value==this.w_DFPTSERI)
      this.oPgFrm.Page1.oPag.oDFPTSERI_2_13.value=this.w_DFPTSERI
      replace t_DFPTSERI with this.oPgFrm.Page1.oPag.oDFPTSERI_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFPTROW_2_14.value==this.w_DFPTROW)
      this.oPgFrm.Page1.oPag.oDFPTROW_2_14.value=this.w_DFPTROW
      replace t_DFPTROW with this.oPgFrm.Page1.oPag.oDFPTROW_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCPROW_2_15.value==this.w_DFCPROW)
      this.oPgFrm.Page1.oPag.oDFCPROW_2_15.value=this.w_DFCPROW
      replace t_DFCPROW with this.oPgFrm.Page1.oPag.oDFCPROW_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFPIANOC_1_12.value==this.w_DFPIANOC)
      this.oPgFrm.Page1.oPag.oDFPIANOC_1_12.value=this.w_DFPIANOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCONTRO_1_13.value==this.w_DFCONTRO)
      this.oPgFrm.Page1.oPag.oDFCONTRO_1_13.value=this.w_DFCONTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCHIAVE_1_15.value==this.w_DFCHIAVE)
      this.oPgFrm.Page1.oPag.oDFCHIAVE_1_15.value=this.w_DFCHIAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDATIMP_1_17.value==this.w_DFDATIMP)
      this.oPgFrm.Page1.oPag.oDFDATIMP_1_17.value=this.w_DFDATIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDFERROR_1_20.RadioValue()==this.w_DFERROR)
      this.oPgFrm.Page1.oPag.oDFERROR_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFFLGMOV_1_21.RadioValue()==this.w_DFFLGMOV)
      this.oPgFrm.Page1.oPag.oDFFLGMOV_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFNUMEFF_2_21.value==this.w_DFNUMEFF)
      this.oPgFrm.Page1.oPag.oDFNUMEFF_2_21.value=this.w_DFNUMEFF
      replace t_DFNUMEFF with this.oPgFrm.Page1.oPag.oDFNUMEFF_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDESOP_2_24.value==this.w_DFDESOP)
      this.oPgFrm.Page1.oPag.oDFDESOP_2_24.value=this.w_DFDESOP
      replace t_DFDESOP with this.oPgFrm.Page1.oPag.oDFDESOP_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDF__NOTE_2_26.value==this.w_DF__NOTE)
      this.oPgFrm.Page1.oPag.oDF__NOTE_2_26.value=this.w_DF__NOTE
      replace t_DF__NOTE with this.oPgFrm.Page1.oPag.oDF__NOTE_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDF_BANCA_2_28.value==this.w_DF_BANCA)
      this.oPgFrm.Page1.oPag.oDF_BANCA_2_28.value=this.w_DF_BANCA
      replace t_DF_BANCA with this.oPgFrm.Page1.oPag.oDF_BANCA_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCODRBN_2_30.value==this.w_DFCODRBN)
      this.oPgFrm.Page1.oPag.oDFCODRBN_2_30.value=this.w_DFCODRBN
      replace t_DFCODRBN with this.oPgFrm.Page1.oPag.oDFCODRBN_2_30.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFERRLOG_2_31.value==this.w_DFERRLOG)
      this.oPgFrm.Page1.oPag.oDFERRLOG_2_31.value=this.w_DFERRLOG
      replace t_DFERRLOG with this.oPgFrm.Page1.oPag.oDFERRLOG_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDFNOMFIL_1_22.value==this.w_DFNOMFIL)
      this.oPgFrm.Page1.oPag.oDFNOMFIL_1_22.value=this.w_DFNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPCON_2_1.value==this.w_DFTIPCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPCON_2_1.value=this.w_DFTIPCON
      replace t_DFTIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPCON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODICE_2_2.value==this.w_DFCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODICE_2_2.value=this.w_DFCODICE
      replace t_DFCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODVAC_2_3.value==this.w_DFCODVAC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODVAC_2_3.value=this.w_DFCODVAC
      replace t_DFCODVAC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODVAC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPVAL_2_4.value==this.w_DFIMPVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPVAL_2_4.value=this.w_DFIMPVAL
      replace t_DFIMPVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPVAL_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODVAL_2_5.value==this.w_DFCODVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODVAL_2_5.value=this.w_DFCODVAL
      replace t_DFCODVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODVAL_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPORT_2_6.value==this.w_DFIMPORT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPORT_2_6.value=this.w_DFIMPORT
      replace t_DFIMPORT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPORT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFDATVAL_2_7.value==this.w_DFDATVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFDATVAL_2_7.value=this.w_DFDATVAL
      replace t_DFDATVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFDATVAL_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFNUMPAR_2_8.value==this.w_DFNUMPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFNUMPAR_2_8.value=this.w_DFNUMPAR
      replace t_DFNUMPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFNUMPAR_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'DOCFINIM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(EMPTY (.w_DFSERTES))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("La modifica del movimento DocFinance non � possibile poich� � stato generato il movimento di tesoreria"))
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_DF_BANCA)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_DF_BANCA))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DF_BANCA)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DFTIPCON=space(1)
      .w_DFCODICE=space(15)
      .w_DFCODVAC=space(4)
      .w_DFIMPVAL=0
      .w_DFCODVAL=space(5)
      .w_DFIMPORT=0
      .w_DFDATVAL=ctod("  /  /  ")
      .w_DFNUMPAR=0
      .w_DFCODBUN=space(6)
      .w_DFPTTIPO=space(1)
      .w_DFDATSCA=ctod("  /  /  ")
      .w_DFMODPAG=space(10)
      .w_DFPTSERI=space(10)
      .w_DFPTROW=0
      .w_DFCPROW=0
      .w_DFNUMEFF=0
      .w_DFDESOP=space(40)
      .w_DF__NOTE=space(40)
      .w_DF_BANCA=space(8)
      .w_DFCODRBN=space(4)
      .w_DFERRLOG=space(0)
    endwith
    this.DoRTCalc(1,36,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DFTIPCON = t_DFTIPCON
    this.w_DFCODICE = t_DFCODICE
    this.w_DFCODVAC = t_DFCODVAC
    this.w_DFIMPVAL = t_DFIMPVAL
    this.w_DFCODVAL = t_DFCODVAL
    this.w_DFIMPORT = t_DFIMPORT
    this.w_DFDATVAL = t_DFDATVAL
    this.w_DFNUMPAR = t_DFNUMPAR
    this.w_DFCODBUN = t_DFCODBUN
    this.w_DFPTTIPO = this.oPgFrm.Page1.oPag.oDFPTTIPO_2_10.RadioValue(.t.)
    this.w_DFDATSCA = t_DFDATSCA
    this.w_DFMODPAG = t_DFMODPAG
    this.w_DFPTSERI = t_DFPTSERI
    this.w_DFPTROW = t_DFPTROW
    this.w_DFCPROW = t_DFCPROW
    this.w_DFNUMEFF = t_DFNUMEFF
    this.w_DFDESOP = t_DFDESOP
    this.w_DF__NOTE = t_DF__NOTE
    this.w_DF_BANCA = t_DF_BANCA
    this.w_DFCODRBN = t_DFCODRBN
    this.w_DFERRLOG = t_DFERRLOG
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DFTIPCON with this.w_DFTIPCON
    replace t_DFCODICE with this.w_DFCODICE
    replace t_DFCODVAC with this.w_DFCODVAC
    replace t_DFIMPVAL with this.w_DFIMPVAL
    replace t_DFCODVAL with this.w_DFCODVAL
    replace t_DFIMPORT with this.w_DFIMPORT
    replace t_DFDATVAL with this.w_DFDATVAL
    replace t_DFNUMPAR with this.w_DFNUMPAR
    replace t_DFCODBUN with this.w_DFCODBUN
    replace t_DFPTTIPO with this.oPgFrm.Page1.oPag.oDFPTTIPO_2_10.ToRadio()
    replace t_DFDATSCA with this.w_DFDATSCA
    replace t_DFMODPAG with this.w_DFMODPAG
    replace t_DFPTSERI with this.w_DFPTSERI
    replace t_DFPTROW with this.w_DFPTROW
    replace t_DFCPROW with this.w_DFCPROW
    replace t_DFNUMEFF with this.w_DFNUMEFF
    replace t_DFDESOP with this.w_DFDESOP
    replace t_DF__NOTE with this.w_DF__NOTE
    replace t_DF_BANCA with this.w_DF_BANCA
    replace t_DFCODRBN with this.w_DFCODRBN
    replace t_DFERRLOG with this.w_DFERRLOG
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsdf_mmtPag1 as StdContainer
  Width  = 756
  height = 473
  stdWidth  = 756
  stdheight = 473
  resizeXpos=493
  resizeYpos=234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFCODAZI_1_1 as StdField with uid="OIWOYDRJMH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DFCODAZI", cQueryName = "DFCODAZI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 104249729,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=78, Top=7, InputMask=replicate('X',10)

  add object oDFNUMERA_1_3 as StdField with uid="KCEXUEQPAI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DFNUMERA", cQueryName = "DFCODAZI,DFNUMERA",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 241170039,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=260, Top=7, InputMask=replicate('X',4)

  add object oDF__ANNO_1_5 as StdField with uid="BMFDEUGJQN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DF__ANNO", cQueryName = "DFCODAZI,DFNUMERA,DF__ANNO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 156563835,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=373, Top=7, InputMask=replicate('X',4)

  add object oDFMOVIME_1_7 as StdField with uid="HUQXXRABYY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DFMOVIME", cQueryName = "DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 219552133,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=517, Top=7, InputMask=replicate('X',10)

  add object oDFDATREG_1_9 as StdField with uid="XRLIARKFIH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DFDATREG", cQueryName = "DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME,DFDATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 196826749,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=680, Top=7

  add object oDFCODCAU_1_10 as StdField with uid="FOKDSBKPUO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DFCODCAU", cQueryName = "DFCODCAU",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 197740171,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=80, Top=40, InputMask=replicate('X',4)

  add object oDFPIANOC_1_12 as StdField with uid="QQHRLKDXGE",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DFPIANOC", cQueryName = "DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME,DFDATREG,DFPIANOC",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 158067079,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=260, Top=40, InputMask=replicate('X',16)

  add object oDFCONTRO_1_13 as StdField with uid="XZLPJDIGBS",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DFCONTRO", cQueryName = "DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME,DFDATREG,DFPIANOC,DFCONTRO",;
    bObbl = .f. , nPag = 1, value=space(22), bMultilanguage =  .f.,;
    HelpContextID = 225003141,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=517, Top=40, InputMask=replicate('X',22)

  add object oDFCHIAVE_1_15 as StdField with uid="GFKMYVJKRJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DFCHIAVE", cQueryName = "DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME,DFDATREG,DFPIANOC,DFCONTRO,DFCHIAVE",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 168969851,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=80, Top=79, InputMask=replicate('X',6)

  add object oDFDATIMP_1_17 as StdField with uid="TCFCAHYHGD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DFDATIMP", cQueryName = "DFDATIMP",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 222603642,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=260, Top=79

  add object oDFERROR_1_20 as StdCheck with uid="BRRUFPQCKF",rtseq=26,rtrep=.f.,left=349, top=78, caption="Movimento errato",;
    ToolTipText = "Il tracciato contiene errori",;
    HelpContextID = 145516086,;
    cFormVar="w_DFERROR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFERROR_1_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFERROR,&i_cF..t_DFERROR),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oDFERROR_1_20.GetRadio()
    this.Parent.oContained.w_DFERROR = this.RadioValue()
    return .t.
  endfunc

  func oDFERROR_1_20.ToRadio()
    this.Parent.oContained.w_DFERROR=trim(this.Parent.oContained.w_DFERROR)
    return(;
      iif(this.Parent.oContained.w_DFERROR=='S',1,;
      0))
  endfunc

  func oDFERROR_1_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDFFLGMOV_1_21 as StdCheck with uid="AIAVFXRPHI",rtseq=27,rtrep=.f.,left=484, top=80, caption="Movimento generato",;
    ToolTipText = "Movimento generato",;
    HelpContextID = 168397172,;
    cFormVar="w_DFFLGMOV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFFLGMOV_1_21.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFFLGMOV,&i_cF..t_DFFLGMOV),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oDFFLGMOV_1_21.GetRadio()
    this.Parent.oContained.w_DFFLGMOV = this.RadioValue()
    return .t.
  endfunc

  func oDFFLGMOV_1_21.ToRadio()
    this.Parent.oContained.w_DFFLGMOV=trim(this.Parent.oContained.w_DFFLGMOV)
    return(;
      iif(this.Parent.oContained.w_DFFLGMOV=='S',1,;
      0))
  endfunc

  func oDFFLGMOV_1_21.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDFFLGMOV_1_21.mCond()
    with this.Parent.oContained
      return (EMPTY (.w_DFSERTES))
    endwith
  endfunc

  add object oDFNOMFIL_1_22 as StdMemo with uid="LDGKZEGIKG",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DFNOMFIL", cQueryName = "DFNOMFIL",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 10881406,;
   bGlobalFont=.t.,;
    Height=47, Width=371, Left=260, Top=122, readonly = .t. , enabled = .t. 


  add object oObj_1_24 as cp_runprogram with uid="IKLYXHHNHT",left=-6, top=548, width=230,height=24,;
    caption='Cancella movimento di tesoreria',;
   bGlobalFont=.t.,;
    prg="GSDF_BDM ('CANC')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 226460214


  add object oBtn_1_29 as StdButton with uid="YQEFECFQJU",left=691, top=125, width=49,height=45,;
    CpPicture="BMP\EVASIONE.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza la distinta generata";
    , HelpContextID = 155281815;
    , Caption='\<Distinta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSDF_BDM (this.Parent.oContained,"MOV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_DFSERTES) OR .w_DFFLGDIS<>'D'  )
    endwith
   endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="UZGWHZWDJZ",left=691, top=125, width=49,height=45,;
    CpPicture="BMP\EVASIONE.BMP", caption="", nPag=1;
    , ToolTipText = "visualizza la registrazione contabile generata";
    , HelpContextID = 146397562;
    , Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSDF_BDM (this.Parent.oContained,"MOV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_DFSERTES) OR .w_DFFLGDIS='D' )
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=178, width=744,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="DFTIPCON",Label1="Tipo",Field2="DFCODICE",Label2="Codice conto",Field3="DFCODVAC",Label3="Val mov.",Field4="DFIMPVAL",Label4="Importo in valuta",Field5="DFCODVAL",Label5="Val contr.",Field6="DFIMPORT",Label6="Controvalore",Field7="DFDATVAL",Label7="Data valuta",Field8="DFNUMPAR",Label8="N par/dis",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118289786

  add object oStr_1_2 as StdString with uid="NPQVBHGPID",Visible=.t., Left=8, Top=9,;
    Alignment=1, Width=68, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="CYLGZEHLHP",Visible=.t., Left=174, Top=9,;
    Alignment=1, Width=85, Height=18,;
    Caption="Numeratore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="MMLPMUVDDI",Visible=.t., Left=326, Top=9,;
    Alignment=1, Width=46, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="NAWRJOBDKL",Visible=.t., Left=423, Top=9,;
    Alignment=1, Width=93, Height=18,;
    Caption="Numero mov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="YNXCVELTAI",Visible=.t., Left=604, Top=9,;
    Alignment=1, Width=75, Height=18,;
    Caption="Data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="HMOZRRUGXK",Visible=.t., Left=435, Top=42,;
    Alignment=1, Width=81, Height=18,;
    Caption="Controvalore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="IOPBHSJLKY",Visible=.t., Left=163, Top=42,;
    Alignment=1, Width=96, Height=18,;
    Caption="Piano dei conti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NBWMOOKUIB",Visible=.t., Left=158, Top=81,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data importazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="FYIKPJPEQR",Visible=.t., Left=145, Top=125,;
    Alignment=1, Width=112, Height=18,;
    Caption="Nome del file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VTJSFVMZMX",Visible=.t., Left=26, Top=42,;
    Alignment=1, Width=50, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="KZEMRNKZFV",Visible=.t., Left=630, Top=86,;
    Alignment=0, Width=80, Height=17,;
    Caption="Distinta"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_DFFLGDIS<>'D')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="MECYIOJHEO",Visible=.t., Left=630, Top=86,;
    Alignment=0, Width=117, Height=17,;
    Caption="Movimento di primanota"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_DFFLGDIS='D' )
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="DSDNXBITTE",Visible=.t., Left=113, Top=320,;
    Alignment=1, Width=64, Height=18,;
    Caption="Tipo partita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="AIRRVARBGR",Visible=.t., Left=322, Top=320,;
    Alignment=1, Width=85, Height=18,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="YAGOGXACUN",Visible=.t., Left=512, Top=320,;
    Alignment=1, Width=115, Height=18,;
    Caption="Modalit� pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ZHUHMFZEMV",Visible=.t., Left=-5, Top=79,;
    Alignment=1, Width=81, Height=18,;
    Caption="Num. prog.:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=197,;
    width=741+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=198,width=740+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDFPTTIPO_2_10.Refresh()
      this.Parent.oDFDATSCA_2_11.Refresh()
      this.Parent.oDFMODPAG_2_12.Refresh()
      this.Parent.oDFPTSERI_2_13.Refresh()
      this.Parent.oDFPTROW_2_14.Refresh()
      this.Parent.oDFCPROW_2_15.Refresh()
      this.Parent.oDFNUMEFF_2_21.Refresh()
      this.Parent.oDFDESOP_2_24.Refresh()
      this.Parent.oDF__NOTE_2_26.Refresh()
      this.Parent.oDF_BANCA_2_28.Refresh()
      this.Parent.oDFCODRBN_2_30.Refresh()
      this.Parent.oDFERRLOG_2_31.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDFPTTIPO_2_10 as StdTrsCombo with uid="VTKZRLSNRK",rtrep=.t.,;
    cFormVar="w_DFPTTIPO", RowSource=""+"In distinta,"+"Chiave,"+"Raggruppate,"+"Nessuna" , ;
    HelpContextID = 47126149,;
    Height=20, Width=117, Left=178, Top=322,;
    cTotal="", cQueryName = "DFPTTIPO",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.


  func oDFPTTIPO_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFPTTIPO,&i_cF..t_DFPTTIPO),this.value)
    return(iif(xVal =1,'D',;
    iif(xVal =2,'C',;
    iif(xVal =3,'R',;
    iif(xVal =4,' ',;
    space(1))))))
  endfunc
  func oDFPTTIPO_2_10.GetRadio()
    this.Parent.oContained.w_DFPTTIPO = this.RadioValue()
    return .t.
  endfunc

  func oDFPTTIPO_2_10.ToRadio()
    this.Parent.oContained.w_DFPTTIPO=trim(this.Parent.oContained.w_DFPTTIPO)
    return(;
      iif(this.Parent.oContained.w_DFPTTIPO=='D',1,;
      iif(this.Parent.oContained.w_DFPTTIPO=='C',2,;
      iif(this.Parent.oContained.w_DFPTTIPO=='R',3,;
      iif(this.Parent.oContained.w_DFPTTIPO=='',4,;
      0)))))
  endfunc

  func oDFPTTIPO_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDFDATSCA_2_11 as StdTrsField with uid="ANLVWNDSYF",rtseq=17,rtrep=.t.,;
    cFormVar="w_DFDATSCA",value=ctod("  /  /  "),;
    HelpContextID = 213603959,;
    cTotal="", bFixedPos=.t., cQueryName = "DFDATSCA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=409, Top=319

  func oDFDATSCA_2_11.mCond()
    with this.Parent.oContained
      return (.w_DFPTTIPO='R')
    endwith
  endfunc

  add object oDFMODPAG_2_12 as StdTrsField with uid="WXXVWXIZFF",rtseq=18,rtrep=.t.,;
    cFormVar="w_DFMODPAG",value=space(10),;
    HelpContextID = 147449469,;
    cTotal="", bFixedPos=.t., cQueryName = "DFMODPAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=627, Top=319, InputMask=replicate('X',10)

  func oDFMODPAG_2_12.mCond()
    with this.Parent.oContained
      return (.w_DFPTTIPO='R')
    endwith
  endfunc

  add object oDFPTSERI_2_13 as StdTrsField with uid="PICZVKOQGE",rtseq=19,rtrep=.t.,;
    cFormVar="w_DFPTSERI",value=space(10),;
    ToolTipText = "Seriale della partita saldata",;
    HelpContextID = 247404159,;
    cTotal="", bFixedPos=.t., cQueryName = "DFPTSERI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=178, Top=345, InputMask=replicate('X',10)

  func oDFPTSERI_2_13.mCond()
    with this.Parent.oContained
      return (.w_DFPTTIPO<>'R')
    endwith
  endfunc

  add object oDFPTROW_2_14 as StdTrsField with uid="SAXIJAOTPX",rtseq=20,rtrep=.t.,;
    cFormVar="w_DFPTROW",value=0,;
    ToolTipText = "Ptroword della partita saldata",;
    HelpContextID = 145692214,;
    cTotal="", bFixedPos=.t., cQueryName = "DFPTROW",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=330, Top=345, cSayPict=["9999"], cGetPict=["9999"]

  func oDFPTROW_2_14.mCond()
    with this.Parent.oContained
      return (.w_DFPTROW<>-20 AND .w_DFPTTIPO<>'R')
    endwith
  endfunc

  add object oDFCPROW_2_15 as StdTrsField with uid="WBHOIHJTIC",rtseq=21,rtrep=.t.,;
    cFormVar="w_DFCPROW",value=0,;
    ToolTipText = "Cprownum della partita saldata",;
    HelpContextID = 145376822,;
    cTotal="", bFixedPos=.t., cQueryName = "DFCPROW",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=449, Top=345, cSayPict=["9999"], cGetPict=["9999"]

  func oDFCPROW_2_15.mCond()
    with this.Parent.oContained
      return (.w_DFPTROW<>-20 AND .w_DFPTTIPO<>'R')
    endwith
  endfunc

  add object oDFNUMEFF_2_21 as StdTrsField with uid="GGTFWYGHUW",rtseq=28,rtrep=.t.,;
    cFormVar="w_DFNUMEFF",value=0,;
    ToolTipText = "Numero effetto delle partite saldate",;
    HelpContextID = 241170044,;
    cTotal="", bFixedPos=.t., cQueryName = "DFNUMEFF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=655, Top=345, cSayPict=["999999"], cGetPict=["999999"]

  func oDFNUMEFF_2_21.mCond()
    with this.Parent.oContained
      return (.w_DFPTROW=-20 AND .w_DFPTTIPO<>'R')
    endwith
  endfunc

  add object oBtn_2_22 as StdButton with uid="JBWRSPMQXH",width=23,height=25,;
   left=722, top=328,;
    CpPicture="bmp\ABBINA.bmp", caption="", nPag=2;
    , ToolTipText = "Abbina partite / scadenze";
    , HelpContextID = 101341329;
    , caption='...';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_22.Click()
      with this.Parent.oContained
        GSDF_BDM  (this.Parent.oContained , "ABBINA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_22.mCond()
    with this.Parent.oContained
      return (.cFunction='Edit' and NOT EMPTY(.w_DFPTTIPO) )
    endwith
  endfunc

  func oBtn_2_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.t.)
    endwith
   endif
  endfunc

  add object oDFDESOP_2_24 as StdTrsField with uid="UYBIXXHARK",rtseq=29,rtrep=.t.,;
    cFormVar="w_DFDESOP",value=space(40),;
    ToolTipText = "Descrizione operazione",;
    HelpContextID = 145708598,;
    cTotal="", bFixedPos=.t., cQueryName = "DFDESOP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=105, Top=384, InputMask=replicate('X',40)

  add object oDF__NOTE_2_26 as StdTrsField with uid="NOEUMVXEKW",rtseq=30,rtrep=.t.,;
    cFormVar="w_DF__NOTE",value=space(40),;
    ToolTipText = "Note",;
    HelpContextID = 142280315,;
    cTotal="", bFixedPos=.t., cQueryName = "DF__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=105, Top=411, InputMask=replicate('X',40)

  add object oDF_BANCA_2_28 as StdTrsField with uid="OZFUJUOWFN",rtseq=31,rtrep=.t.,;
    cFormVar="w_DF_BANCA",value=space(8),;
    ToolTipText = "Banca",;
    HelpContextID = 109971063,;
    cTotal="", bFixedPos=.t., cQueryName = "DF_BANCA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=105, Top=436, InputMask=replicate('X',8)

  add object oDFCODRBN_2_30 as StdTrsField with uid="JFHZBXVPQP",rtseq=32,rtrep=.t.,;
    cFormVar="w_DFCODRBN",value=space(4),;
    ToolTipText = "Codice RBN della banca ",;
    HelpContextID = 180962948,;
    cTotal="", bFixedPos=.t., cQueryName = "DFCODRBN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=270, Top=436, InputMask=replicate('X',4)

  add object oDFERRLOG_2_31 as StdTrsMemo with uid="VEPPQTGYBQ",rtseq=33,rtrep=.t.,;
    cFormVar="w_DFERRLOG",value=space(0),;
    ToolTipText = "Log errori",;
    HelpContextID = 173250947,;
    cTotal="", bFixedPos=.t., cQueryName = "DFERRLOG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=80, Width=347, Left=406, Top=384

  add object oStr_2_16 as StdString with uid="LBHXDYWNEQ",Visible=.t., Left=132, Top=345,;
    Alignment=1, Width=44, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="DANGZWPSEO",Visible=.t., Left=262, Top=345,;
    Alignment=1, Width=66, Height=18,;
    Caption="Ptroword:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="XUBDUAGYCH",Visible=.t., Left=381, Top=345,;
    Alignment=1, Width=66, Height=18,;
    Caption="Cprownum:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="XAWHAGKHVO",Visible=.t., Left=579, Top=345,;
    Alignment=1, Width=74, Height=18,;
    Caption="Num. effetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="LMTLJZRTDG",Visible=.t., Left=65, Top=411,;
    Alignment=1, Width=38, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="FPRAHSGSFW",Visible=.t., Left=12, Top=384,;
    Alignment=1, Width=91, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="GOQPVDCNNT",Visible=.t., Left=405, Top=371,;
    Alignment=1, Width=53, Height=18,;
    Caption="Log errori"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="CDYDSNZUDV",Visible=.t., Left=51, Top=436,;
    Alignment=1, Width=52, Height=18,;
    Caption="Banca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="AWYAKBFQOC",Visible=.t., Left=192, Top=436,;
    Alignment=1, Width=76, Height=18,;
    Caption="Codice RBN:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsdf_mmtBodyRow as CPBodyRowCnt
  Width=731
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDFTIPCON_2_1 as StdTrsField with uid="TSXNYYAOYS",rtseq=7,rtrep=.t.,;
    cFormVar="w_DFTIPCON",value=space(1),;
    ToolTipText = "Tipo conto (C=cliente, F=fornitore, G=conto generico, -=conto generico )",;
    HelpContextID = 58435964,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=25, Left=-2, Top=0, InputMask=replicate('X',1)

  add object oDFCODICE_2_2 as StdTrsField with uid="FRMESBZMAT",rtseq=8,rtrep=.t.,;
    cFormVar="w_DFCODICE",value=space(15),;
    ToolTipText = "Codice conto",;
    HelpContextID = 29967995,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=31, Top=0, InputMask=replicate('X',15)

  add object oDFCODVAC_2_3 as StdTrsField with uid="AOUJTAAEJH",rtseq=9,rtrep=.t.,;
    cFormVar="w_DFCODVAC",value=space(4),;
    ToolTipText = "Valuta del movimento",;
    HelpContextID = 248071801,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=155, Top=0, InputMask=replicate('X',4)

  add object oDFIMPVAL_2_4 as StdTrsField with uid="WSVIQADCIH",rtseq=10,rtrep=.t.,;
    cFormVar="w_DFIMPVAL",value=0,;
    ToolTipText = "Importo nella valuta del movimento",;
    HelpContextID = 260548226,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=206, Top=0, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oDFCODVAL_2_5 as StdTrsField with uid="WWCHOOLNQF",rtseq=11,rtrep=.t.,;
    cFormVar="w_DFCODVAL",value=space(5),;
    ToolTipText = "Valuta di conto",;
    HelpContextID = 248071810,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=350, Top=0, InputMask=replicate('X',5)

  add object oDFIMPORT_2_6 as StdTrsField with uid="CVUPAHIVIO",rtseq=12,rtrep=.t.,;
    cFormVar="w_DFIMPORT",value=0,;
    ToolTipText = "Importo nella valuta di conto",;
    HelpContextID = 143107722,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=403, Top=0, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oDFDATVAL_2_7 as StdTrsField with uid="QCHWHBOYEM",rtseq=13,rtrep=.t.,;
    cFormVar="w_DFDATVAL",value=ctod("  /  /  "),;
    ToolTipText = "Data valuta",;
    HelpContextID = 263935618,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=547, Top=0

  add object oDFNUMPAR_2_8 as StdTrsField with uid="RVNRPUORRU",rtseq=14,rtrep=.t.,;
    cFormVar="w_DFNUMPAR",value=0,;
    ToolTipText = "Numero partita / distinta",;
    HelpContextID = 157283976,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=107, Left=619, Top=0, cSayPict=["999999"], cGetPict=["999999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDFTIPCON_2_1.When()
    return(.t.)
  proc oDFTIPCON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDFTIPCON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_mmt','DOCFINIM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFCODAZI=DOCFINIM.DFCODAZI";
  +" and "+i_cAliasName2+".DFNUMERA=DOCFINIM.DFNUMERA";
  +" and "+i_cAliasName2+".DF__ANNO=DOCFINIM.DF__ANNO";
  +" and "+i_cAliasName2+".DFMOVIME=DOCFINIM.DFMOVIME";
  +" and "+i_cAliasName2+".DFDATREG=DOCFINIM.DFDATREG";
  +" and "+i_cAliasName2+".DFPIANOC=DOCFINIM.DFPIANOC";
  +" and "+i_cAliasName2+".DFCONTRO=DOCFINIM.DFCONTRO";
  +" and "+i_cAliasName2+".DFCHIAVE=DOCFINIM.DFCHIAVE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
