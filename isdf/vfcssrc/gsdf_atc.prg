* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_atc                                                        *
*              Trascodifica causali DocFinance                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-03                                                      *
* Last revis.: 2012-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_atc"))

* --- Class definition
define class tgsdf_atc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 532
  Height = 149+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-19"
  HelpContextID=92943465
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  DOCTRCAU_IDX = 0
  CAU_DIST_IDX = 0
  CAU_CONT_IDX = 0
  cFile = "DOCTRCAU"
  cKeySelect = "TRCAUDOC"
  cKeyWhere  = "TRCAUDOC=this.w_TRCAUDOC"
  cKeyWhereODBC = '"TRCAUDOC="+cp_ToStrODBC(this.w_TRCAUDOC)';

  cKeyWhereODBCqualified = '"DOCTRCAU.TRCAUDOC="+cp_ToStrODBC(this.w_TRCAUDOC)';

  cPrg = "gsdf_atc"
  cComment = "Trascodifica causali DocFinance"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TRCAUDOC = space(4)
  w_TRTIPCAU = space(1)
  o_TRTIPCAU = space(1)
  w_TRCAUDIS = space(5)
  w_TRCAUCON = space(5)
  w_CADESCAU = space(35)
  w_CCDESCRI = space(35)
  w_RESCHK = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DOCTRCAU','gsdf_atc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_atcPag1","gsdf_atc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Trascodifica causali DocFinance")
      .Pages(1).HelpContextID = 163616409
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRCAUDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAU_DIST'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='DOCTRCAU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOCTRCAU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOCTRCAU_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TRCAUDOC = NVL(TRCAUDOC,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DOCTRCAU where TRCAUDOC=KeySet.TRCAUDOC
    *
    i_nConn = i_TableProp[this.DOCTRCAU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCTRCAU_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOCTRCAU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOCTRCAU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOCTRCAU '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCAUDOC',this.w_TRCAUDOC  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CADESCAU = space(35)
        .w_CCDESCRI = space(35)
        .w_RESCHK = 0
        .w_TRCAUDOC = NVL(TRCAUDOC,space(4))
        .w_TRTIPCAU = NVL(TRTIPCAU,space(1))
        .w_TRCAUDIS = NVL(TRCAUDIS,space(5))
          if link_1_5_joined
            this.w_TRCAUDIS = NVL(CACODICE105,NVL(this.w_TRCAUDIS,space(5)))
            this.w_CADESCAU = NVL(CADESCAU105,space(35))
          else
          .link_1_5('Load')
          endif
        .w_TRCAUCON = NVL(TRCAUCON,space(5))
          if link_1_7_joined
            this.w_TRCAUCON = NVL(CCCODICE107,NVL(this.w_TRCAUCON,space(5)))
            this.w_CCDESCRI = NVL(CCDESCRI107,space(35))
          else
          .link_1_7('Load')
          endif
        cp_LoadRecExtFlds(this,'DOCTRCAU')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TRCAUDOC = space(4)
      .w_TRTIPCAU = space(1)
      .w_TRCAUDIS = space(5)
      .w_TRCAUCON = space(5)
      .w_CADESCAU = space(35)
      .w_CCDESCRI = space(35)
      .w_RESCHK = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_TRTIPCAU = 'C'
        .w_TRCAUDIS = Space(5)
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_TRCAUDIS))
          .link_1_5('Full')
          endif
        .w_TRCAUCON = Space(5)
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_TRCAUCON))
          .link_1_7('Full')
          endif
          .DoRTCalc(5,6,.f.)
        .w_RESCHK = 0
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOCTRCAU')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTRCAUDOC_1_1.enabled = i_bVal
      .Page1.oPag.oTRTIPCAU_1_3.enabled = i_bVal
      .Page1.oPag.oTRCAUDIS_1_5.enabled = i_bVal
      .Page1.oPag.oTRCAUCON_1_7.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTRCAUDOC_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTRCAUDOC_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DOCTRCAU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOCTRCAU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCAUDOC,"TRCAUDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRTIPCAU,"TRTIPCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCAUDIS,"TRCAUDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCAUCON,"TRCAUCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOCTRCAU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCTRCAU_IDX,2])
    i_lTable = "DOCTRCAU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DOCTRCAU_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOCTRCAU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCTRCAU_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DOCTRCAU_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DOCTRCAU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOCTRCAU')
        i_extval=cp_InsertValODBCExtFlds(this,'DOCTRCAU')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TRCAUDOC,TRTIPCAU,TRCAUDIS,TRCAUCON "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TRCAUDOC)+;
                  ","+cp_ToStrODBC(this.w_TRTIPCAU)+;
                  ","+cp_ToStrODBCNull(this.w_TRCAUDIS)+;
                  ","+cp_ToStrODBCNull(this.w_TRCAUCON)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOCTRCAU')
        i_extval=cp_InsertValVFPExtFlds(this,'DOCTRCAU')
        cp_CheckDeletedKey(i_cTable,0,'TRCAUDOC',this.w_TRCAUDOC)
        INSERT INTO (i_cTable);
              (TRCAUDOC,TRTIPCAU,TRCAUDIS,TRCAUCON  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TRCAUDOC;
                  ,this.w_TRTIPCAU;
                  ,this.w_TRCAUDIS;
                  ,this.w_TRCAUCON;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DOCTRCAU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCTRCAU_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DOCTRCAU_IDX,i_nConn)
      *
      * update DOCTRCAU
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DOCTRCAU')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TRTIPCAU="+cp_ToStrODBC(this.w_TRTIPCAU)+;
             ",TRCAUDIS="+cp_ToStrODBCNull(this.w_TRCAUDIS)+;
             ",TRCAUCON="+cp_ToStrODBCNull(this.w_TRCAUCON)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DOCTRCAU')
        i_cWhere = cp_PKFox(i_cTable  ,'TRCAUDOC',this.w_TRCAUDOC  )
        UPDATE (i_cTable) SET;
              TRTIPCAU=this.w_TRTIPCAU;
             ,TRCAUDIS=this.w_TRCAUDIS;
             ,TRCAUCON=this.w_TRCAUCON;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOCTRCAU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCTRCAU_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DOCTRCAU_IDX,i_nConn)
      *
      * delete DOCTRCAU
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TRCAUDOC',this.w_TRCAUDOC  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOCTRCAU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCTRCAU_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_Trtipcau<>.w_Trtipcau
            .w_TRCAUDIS = Space(5)
          .link_1_5('Full')
        endif
        if .o_Trtipcau<>.w_Trtipcau
            .w_TRCAUCON = Space(5)
          .link_1_7('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_ITRQQGYOLH()
    with this
          * --- Checkform (gsdf_bdm - Tracau)
          GSDF_BDM(this;
              ,'TRACAU';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTRCAUDIS_1_5.visible=!this.oPgFrm.Page1.oPag.oTRCAUDIS_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oTRCAUCON_1_7.visible=!this.oPgFrm.Page1.oPag.oTRCAUCON_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCADESCAU_1_9.visible=!this.oPgFrm.Page1.oPag.oCADESCAU_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCCDESCRI_1_10.visible=!this.oPgFrm.Page1.oPag.oCCDESCRI_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Controlli")
          .Calculate_ITRQQGYOLH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRCAUDIS
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
    i_lTable = "CAU_DIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2], .t., this.CAU_DIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCAUDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACD',True,'CAU_DIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_TRCAUDIS)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_TRCAUDIS))
          select CACODICE,CADESCAU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCAUDIS)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCAUDIS) and !this.bDontReportError
            deferred_cp_zoom('CAU_DIST','*','CACODICE',cp_AbsName(oSource.parent,'oTRCAUDIS_1_5'),i_cWhere,'GSTE_ACD',"Causali distinte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCAUDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_TRCAUDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_TRCAUDIS)
            select CACODICE,CADESCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCAUDIS = NVL(_Link_.CACODICE,space(5))
      this.w_CADESCAU = NVL(_Link_.CADESCAU,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TRCAUDIS = space(5)
      endif
      this.w_CADESCAU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_DIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCAUDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_DIST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.CACODICE as CACODICE105"+ ",link_1_5.CADESCAU as CADESCAU105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on DOCTRCAU.TRCAUDIS=link_1_5.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and DOCTRCAU.TRCAUDIS=link_1_5.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TRCAUCON
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_TRCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_TRCAUCON))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oTRCAUCON_1_7'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_TRCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_TRCAUCON)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TRCAUCON = space(5)
      endif
      this.w_CCDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.CCCODICE as CCCODICE107"+ ",link_1_7.CCDESCRI as CCDESCRI107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on DOCTRCAU.TRCAUCON=link_1_7.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and DOCTRCAU.TRCAUCON=link_1_7.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRCAUDOC_1_1.value==this.w_TRCAUDOC)
      this.oPgFrm.Page1.oPag.oTRCAUDOC_1_1.value=this.w_TRCAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTRTIPCAU_1_3.RadioValue()==this.w_TRTIPCAU)
      this.oPgFrm.Page1.oPag.oTRTIPCAU_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCAUDIS_1_5.value==this.w_TRCAUDIS)
      this.oPgFrm.Page1.oPag.oTRCAUDIS_1_5.value=this.w_TRCAUDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCAUCON_1_7.value==this.w_TRCAUCON)
      this.oPgFrm.Page1.oPag.oTRCAUCON_1_7.value=this.w_TRCAUCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCAU_1_9.value==this.w_CADESCAU)
      this.oPgFrm.Page1.oPag.oCADESCAU_1_9.value=this.w_CADESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_10.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_10.value=this.w_CCDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'DOCTRCAU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TRCAUDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCAUDOC_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TRCAUDOC)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsdf_atc
      if i_bRes=.t.
         .w_RESCHK=0
           .NotifyEvent('Controlli')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TRTIPCAU = this.w_TRTIPCAU
    return

enddefine

* --- Define pages as container
define class tgsdf_atcPag1 as StdContainer
  Width  = 528
  height = 149
  stdWidth  = 528
  stdheight = 149
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCAUDOC_1_1 as StdField with uid="ETGWKHQAZQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCAUDOC", cQueryName = "TRCAUDOC",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Causale di DocFinance",;
    HelpContextID = 200584327,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=175, Top=19, InputMask=replicate('X',4)


  add object oTRTIPCAU_1_3 as StdCombo with uid="WTGDJJGPQZ",rtseq=2,rtrep=.f.,left=175,top=52,width=117,height=22;
    , ToolTipText = "Indentifica il tipo di causale";
    , HelpContextID = 46424971;
    , cFormVar="w_TRTIPCAU",RowSource=""+"Contabile,"+"Distinta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRTIPCAU_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oTRTIPCAU_1_3.GetRadio()
    this.Parent.oContained.w_TRTIPCAU = this.RadioValue()
    return .t.
  endfunc

  func oTRTIPCAU_1_3.SetRadio()
    this.Parent.oContained.w_TRTIPCAU=trim(this.Parent.oContained.w_TRTIPCAU)
    this.value = ;
      iif(this.Parent.oContained.w_TRTIPCAU=='C',1,;
      iif(this.Parent.oContained.w_TRTIPCAU=='D',2,;
      0))
  endfunc

  add object oTRCAUDIS_1_5 as StdField with uid="SWOQGLEGQK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TRCAUDIS", cQueryName = "TRCAUDIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Accoglie la causale distinta",;
    HelpContextID = 200584311,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=175, Top=88, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_DIST", cZoomOnZoom="GSTE_ACD", oKey_1_1="CACODICE", oKey_1_2="this.w_TRCAUDIS"

  func oTRCAUDIS_1_5.mHide()
    with this.Parent.oContained
      return (.w_Trtipcau='C')
    endwith
  endfunc

  func oTRCAUDIS_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCAUDIS_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCAUDIS_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_DIST','*','CACODICE',cp_AbsName(this.parent,'oTRCAUDIS_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACD',"Causali distinte",'',this.parent.oContained
  endproc
  proc oTRCAUDIS_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_TRCAUDIS
     i_obj.ecpSave()
  endproc

  add object oTRCAUCON_1_7 as StdField with uid="EPWWLFKGXW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TRCAUCON", cQueryName = "TRCAUCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Accoglie la causale contabile",;
    HelpContextID = 217361532,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=175, Top=88, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_TRCAUCON"

  func oTRCAUCON_1_7.mHide()
    with this.Parent.oContained
      return (.w_Trtipcau='D')
    endwith
  endfunc

  func oTRCAUCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCAUCON_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCAUCON_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oTRCAUCON_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oTRCAUCON_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_TRCAUCON
     i_obj.ecpSave()
  endproc

  add object oCADESCAU_1_9 as StdField with uid="QEAQOJAQAH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CADESCAU", cQueryName = "CADESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 49238395,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=246, Top=88, InputMask=replicate('X',35)

  func oCADESCAU_1_9.mHide()
    with this.Parent.oContained
      return (.w_Trtipcau='C')
    endwith
  endfunc

  add object oCCDESCRI_1_10 as StdField with uid="OURFEXEKSF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 49238895,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=246, Top=88, InputMask=replicate('X',35)

  func oCCDESCRI_1_10.mHide()
    with this.Parent.oContained
      return (.w_Trtipcau='D')
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="LMZZSVFKDJ",Visible=.t., Left=6, Top=22,;
    Alignment=1, Width=164, Height=18,;
    Caption="Causale DocFinance:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="GQXWHEPMEQ",Visible=.t., Left=69, Top=54,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipologia causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="QCQLLAYYWI",Visible=.t., Left=46, Top=91,;
    Alignment=1, Width=124, Height=18,;
    Caption="Causale distinta:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_Trtipcau='C')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="DDUYVMTSHH",Visible=.t., Left=38, Top=91,;
    Alignment=1, Width=132, Height=18,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_Trtipcau='D')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_atc','DOCTRCAU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCAUDOC=DOCTRCAU.TRCAUDOC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
