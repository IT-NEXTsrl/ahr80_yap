* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_bes                                                        *
*              Generazione tracciati DocFinance                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-06-06                                                      *
* Last revis.: 2017-03-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdf_bes",oParentObject)
return(i_retval)

define class tgsdf_bes as StdBatch
  * --- Local variables
  w_Scaini = ctod("  /  /  ")
  w_Scafin = ctod("  /  /  ")
  w_Tipo = space(1)
  w_Codcon1 = space(15)
  w_Flrd1 = space(2)
  w_Flrb1 = space(2)
  w_Flbo1 = space(2)
  w_Flri1 = space(2)
  w_Flca1 = space(2)
  w_Flma1 = space(2)
  w_Abicod = space(5)
  w_Codban = space(15)
  w_Codzon = space(3)
  w_Consup = space(15)
  w_Catcom = space(3)
  w_Tipsca = space(1)
  w_Lcodeff = space(15)
  w_Datdocini = ctod("  /  /  ")
  w_Datdocfin = ctod("  /  /  ")
  w_Alfdocfin = space(10)
  w_Alfdocini = space(10)
  w_Numdocfin = 0
  w_Numdocini = 0
  w_Ltippar = space(1)
  w_Filrow = 0
  w_TipoPagamento = space(2)
  w_Newrec = space(10)
  w_Oldrec = space(10)
  w_Ptserial = space(10)
  w_Ptroword = 0
  w_Cprownum = 0
  w_Progre = 0
  w_Tipdis = space(2)
  w_FilePdc = space(200)
  w_Aztraazi = space(10)
  w_Codpdc = space(16)
  w_Flagpdc = space(1)
  w_Handle = 0
  w_Andescri = space(80)
  w_NomDescri = space(6)
  w_Anindiri = space(40)
  w_An___Cap = space(9)
  w_Anlocali = space(40)
  w_Anprovin = space(2)
  w_Nacodiso = space(4)
  w_Bansbtes = space(8)
  w_Barbntes = space(4)
  w_Annumcor = space(12)
  w_Anrating = space(2)
  w_Linea = space(4)
  w_Angiorit = space(3)
  w_Anvocfin = space(6)
  w_An_Email = space(80)
  w_Baflstra = space(1)
  w_CliAbi = space(5)
  w_CliCab = space(5)
  w_Cccinabi = space(1)
  w_Bapaseur = space(2)
  w_Cccincab = space(2)
  w_Cccodbic = space(11)
  w_Cccoiban = space(34)
  w_Badesban = space(35)
  w_Baindiri = space(35)
  w_Clifis = space(16)
  w_Anpariva = space(11)
  w_Record = space(120)
  w_Separa = space(2)
  w_ErrorEsportazione = .f.
  w_FileScadenziario = space(200)
  w_Pagca = space(2)
  w_Pagrd = space(2)
  w_Pagbo = space(2)
  w_Pagma = space(2)
  w_Pagrb = space(2)
  w_Pagri = space(2)
  w_Pagno = space(2)
  w_Codcon = space(15)
  w_Codval = space(3)
  w_Ddoini = ctod("  /  /  ")
  w_Ddofin = ctod("  /  /  ")
  w_Ndoini = 0
  w_Ndofin = 0
  w_Adoini = space(10)
  w_Adofin = space(10)
  w_Datpar = ctod("  /  /  ")
  w_Flgsos = space(1)
  w_Flgnso = space(1)
  w_Numpar = space(14)
  w_Valuta = space(3)
  w_Tipcon = space(1)
  w_Bannos = space(15)
  w_Banapp = space(10)
  w_Rowscad = 0
  w_Flsald = space(1)
  w_Codage = space(5)
  w_Regini = ctod("  /  /  ")
  w_Regfin = ctod("  /  /  ")
  w_TipoEla = space(10)
  w_Tippag = space(2)
  w_Valore = 0
  w_CodIsoValuta = space(5)
  w_ControlloValute = space(10)
  w_oErrorLog = .NULL.
  w_CambioApertura = 0
  w_Ptdatsca = ctod("  /  /  ")
  w_Rascadut = space(2)
  w_Ragiorni = 0
  w_Controvalore = 0
  w_DataScadenza = space(8)
  w_DataValuta = space(8)
  w_FlagSospeso = space(1)
  w_Provenienza = space(2)
  w_Sezione = space(6)
  w_Ptnumeff = 0
  w_NumDocOrigine = space(12)
  w_ChiaveCoge = space(26)
  w_NumRec = 0
  w_DataDocOrigine = space(8)
  w_Datsca = ctod("  /  /  ")
  w_Modpag = space(10)
  w_Numeff = 0
  w_Note = space(80)
  w_Despar = space(1)
  w_Ptdescri = space(50)
  w_TipoCodice = space(1)
  w_CodiceCliente = space(16)
  w_Tipcli = space(1)
  w_Tipfor = space(1)
  w_Tipgen = space(1)
  w_Codcli = space(15)
  w_Codfor = space(15)
  w_Ndocin = 0
  w_Ndocfi = 0
  w_Adocin = space(10)
  w_Adocfi = space(10)
  w_Ddocin = ctod("  /  /  ")
  w_Ddocfi = ctod("  /  /  ")
  w_Datini = ctod("  /  /  ")
  w_Datfin = ctod("  /  /  ")
  w_Filatt = space(10)
  w_FileMovimenti = space(200)
  w_TipoRec = space(2)
  w_Numeratore = space(4)
  w_CauCoge = space(4)
  w_IdOperaz = space(4)
  w_DataReg = space(8)
  w_DataOpe = space(8)
  w_PdcRbn = space(16)
  w_NumRif = space(10)
  w_ImpDivNeg = 0
  w_DivNeg = space(5)
  w_ImpMov = 0
  w_TipoDoc = space(2)
  w_Numdoc = space(6)
  w_NumRifCoge = space(26)
  w_Nummov = space(10)
  w_ProgrNumMov = space(10)
  w_Pnserial = space(10)
  w_DataEsecuzione = ctod("  /  /  ")
  w_MATIPSEQ = space(5)
  w_TIPMAN = space(0)
  * --- WorkFile variables
  DOCFINPA_idx=0
  TMP_PART2_idx=0
  TMP_PART3_idx=0
  PAR_TITE_idx=0
  PRO_NUME_idx=0
  ZOOMPART_idx=0
  VALUTE_idx=0
  DOCPNTRA_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la generazione dei tracciati del piano dei conti e dello 
    *     scadenziario per DocFinance
    this.w_Handle = 0
    this.w_Record = space(120)
    this.w_Separa = chr(13)+chr(10)
    this.w_ErrorEsportazione = .f.
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oErrorLog=createobject("AH_ErrorLog")
    this.oParentObject.w_Msg = ""
    this.oParentobject.oPgFrm.ActivePage=2
    ADDMSG("Fase 1: inizializzazione e controlli %1",this,chr(13))
    if Empty(this.oParentObject.w_Dftraazi)
      ah_ErrorMsg("Codice azienda di trascodifica inesistente")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_Pdc="S" And (JustExt(Upper(this.oParentObject.w_Nomefile))<>"TXT" Or Empty(this.oParentObject.w_Nomefile))
      ah_ErrorMsg("Nome file piano dei conti inesistente oppure con estensione diversa da txt")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_Scadenziario="S" And (JustExt(Upper(this.oParentObject.w_NomefileScad))<>"TXT" Or Empty(this.oParentObject.w_NomefileScad))
      ah_ErrorMsg("Nome file scadenzario inesistente oppure con estensione diversa da txt")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_Expcorri="S" And (JustExt(Upper(this.oParentObject.w_NomefileMov))<>"TXT" Or Empty(this.oParentObject.w_NomefileMov))
      ah_ErrorMsg("Nome file movimenti inesistente oppure con estensione diversa da txt")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_Pdc="S" Or this.oParentObject.w_Scadenziario="S" Or this.oParentObject.w_Expcorri="S"
      if ! Directory(this.oParentObject.w_Pathfile) Or Empty(this.oParentObject.w_Pathfile)
        ah_ErrorMsg("Path di esportazione inesistente")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_Pdc<>"S" And this.oParentObject.w_Scadenziario<>"S" And this.oParentObject.w_Expcorri<>"S"
      ah_ErrorMsg("Non � stata selezionata alcuna tipologia di esportazione")
      i_retcode = 'stop'
      return
    endif
    if Empty(this.oParentObject.w_Dfratsca)
      ah_ErrorMsg("Non � stato impostato il default rating a scadere")
      i_retcode = 'stop'
      return
    endif
    if Empty(this.oParentObject.w_Dfratsdt)
      ah_ErrorMsg("Non � stato impostato il default rating scaduto")
      i_retcode = 'stop'
      return
    endif
    if Empty(this.oParentObject.w_Dfvocfcl)
      ah_ErrorMsg("Non � stato impostato il default voce finanziaria per i clienti")
      i_retcode = 'stop'
      return
    endif
    if Empty(this.oParentObject.w_Dfvocffo)
      ah_ErrorMsg("Non � stato impostato il default voce finanziaria per i fornitori")
      i_retcode = 'stop'
      return
    endif
    if Empty(this.oParentObject.w_Dfvocfge) And this.oParentObject.w_Scadenziario="S"
      ah_ErrorMsg("Non � stato impostato il default voce finanziaria per i conti generici")
      i_retcode = 'stop'
      return
    endif
    if Empty(this.oParentObject.w_Dfvoceff) And this.oParentObject.w_Scadenziario="S"
      ah_ErrorMsg("Non � stata impostata la voce finanziaria effetti")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_Pdc="S"
      ADDMSG("Fase 2: Esportazione piano dei conti %1",this,chr(13))
      ADDMSG("%1Inizio fase di esportazione piano dei conti%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_Scadenziario="S"
      if this.oParentObject.w_Pdc<>"S"
        ADDMSG("Fase 2: Esportazione scadenzario %1",this,chr(13))
      else
        ADDMSG("Fase 3: Esportazione scadenzario %1",this,chr(13))
      endif
      ADDMSG("%1Numerazione effetti%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
      if this.oParentObject.w_Dfdis_Ri="RI" or this.oParentObject.w_Dfdis_Ra="RB"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_Expcorri="S" And ! this.w_ErrorEsportazione
      do case
        case (this.oParentObject.w_Pdc="S" And this.oParentObject.w_Scadenziario<>"S") Or (this.oParentObject.w_Pdc<>"S" And this.oParentObject.w_Scadenziario="S")
          ADDMSG("Fase 3: Esportazione movimenti %1",this,chr(13))
        case this.oParentObject.w_Pdc="S" And this.oParentObject.w_Scadenziario="S"
          ADDMSG("Fase 4: Esportazione movimenti %1",this,chr(13))
        case this.oParentObject.w_Pdc<>"S" And this.oParentObject.w_Scadenziario<>"S"
          ADDMSG("Fase 2: Esportazione movimenti %1",this,chr(13))
      endcase
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if ! this.w_ErrorEsportazione
      ah_ErrorMsg("Esportazione eseguita con successo")
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FileScadenziario = alltrim(this.oParentObject.w_Pathfile)+alltrim(this.oParentObject.w_NomefileScad)
    Public erroHAND 
 erroHAND=0
    ON ERROR erroHAND= -1
    this.w_Handle = fcreate(this.w_FileScadenziario, 0)
    ON ERROR 
    if this.w_Handle = -1 OR erroHAND=-1
      Ah_Errormsg("Non � possibile esportare i dati",48)
      i_retcode = 'stop'
      return
    endif
    ADDMSG("%1Esportazione scadenze%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
    this.w_Pagca = "CA"
    this.w_Pagrd = "RD"
    this.w_Pagbo = "BO"
    this.w_Pagma = "MA"
    this.w_Pagrb = "RB"
    this.w_Pagri = "RI"
    this.w_Pagno = "XX"
    this.w_Codcon = Space(15)
    this.w_Codval = Space(3)
    this.w_Scaini = Cp_CharToDate("  -  -    ")
    this.w_Scafin = Cp_CharToDate("  -  -    ")
    this.w_Datpar = Cp_CharToDate("  -  -    ")
    this.w_Ddoini = Cp_CharToDate("  -  -    ")
    this.w_Ddofin = Cp_CharToDate("  -  -    ")
    this.w_Adoini = Space(10)
    this.w_Adofin = Space(10)
    this.w_Ndoini = 0
    this.w_Ndofin = 0
    this.w_Flgsos = Space(1)
    this.w_Flgnso = Space(1)
    this.w_Numpar = Space(14)
    this.w_Tipcon = Space(1)
    this.w_Valuta = Space(3)
    this.w_Bannos = Space(15)
    this.w_Banapp = Space(10)
    this.w_Rowscad = 0
    this.w_Flsald = "R"
    this.w_Codage = Space(5)
    this.w_Regini = Cp_CharToDate("  -  -    ")
    this.w_Regfin = Cp_CharToDate("  -  -    ")
    this.w_ControlloValute = ""
    this.w_Tipdis = "SD"
    * --- Determino le partite aperte
    * --- Create temporary table TMP_PART3
    i_nIdx=cp_AddTableDef('TMP_PART3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_NAZCIUUBYP[5]
    indexes_NAZCIUUBYP[1]='NUMPAR'
    indexes_NAZCIUUBYP[2]='DATSCA'
    indexes_NAZCIUUBYP[3]='TIPCON'
    indexes_NAZCIUUBYP[4]='CODCON'
    indexes_NAZCIUUBYP[5]='CODVAL'
    vq_exec('Gstetkms',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_NAZCIUUBYP,.f.)
    this.TMP_PART3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMP_PART2
    i_nIdx=cp_AddTableDef('TMP_PART2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_YFOENLKDAG[5]
    indexes_YFOENLKDAG[1]='NUMPAR'
    indexes_YFOENLKDAG[2]='DATSCA'
    indexes_YFOENLKDAG[3]='TIPCON'
    indexes_YFOENLKDAG[4]='CODCON'
    indexes_YFOENLKDAG[5]='CODVAL'
    vq_exec('GSDF0kms',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_YFOENLKDAG,.f.)
    this.TMP_PART2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_Tipcon = "C"
    ADDMSG("%1Determina partite aperte per i clienti%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
    * --- Nel caso sia stato attivato il flag "Partite di Acconto" devo lanciare la routine
    *     Gste_Bpa con parametri differenti
    this.w_TipoEla = iif(this.oParentObject.w_Dfespacc="S","GSCG_BSA","GSDF_BES")
    Gste_Bpa (this, this.w_Scaini , this.w_Scafin , this.w_Tipcon , this.w_Codcon ,this.w_Codcon , this.w_Codval , "", "RB","BO","RD","RI","MA","CA", this.w_TipoEla,"N",this.w_Datpar)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Create temporary table ZOOMPART
    i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\Isdf\Exe\Query\Gsdf22bes',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.ZOOMPART_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_Tipcon = "F"
    ADDMSG("%1Determina partite aperte per i fornitori%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
    Gste_Bpa (this, this.w_Scaini , this.w_Scafin , this.w_Tipcon , this.w_Codcon ,this.w_Codcon , this.w_Codval , "", "RB","BO","RD","RI","MA","CA", this.w_TipoEla,"N",this.w_Datpar)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into ZOOMPART
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\Isdf\Exe\Query\Gsdf22Bes",this.ZOOMPART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_Tipcon = "G"
    ADDMSG("%1Determina partite aperte per i conti contabili%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
    Gste_Bpa (this, this.w_Scaini , this.w_Scafin , this.w_Tipcon , this.w_Codcon ,this.w_Codcon , this.w_Codval , "", "RB","BO","RD","RI","MA","CA", this.w_TipoEla,"N",this.w_Datpar)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into ZOOMPART
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\Isdf\Exe\Query\Gsdf26Bes",this.ZOOMPART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Conta ragruppate
    * --- Write into TMP_PART2
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_PART2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART2_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="Datsca,Tipcon,Codcon,Modpag,Ptnumeff"
      do vq_exec with 'GSDFDBRZ',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART2_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_PART2.Datsca = _t2.Datsca";
              +" and "+"TMP_PART2.Tipcon = _t2.Tipcon";
              +" and "+"TMP_PART2.Codcon = _t2.Codcon";
              +" and "+"TMP_PART2.Modpag = _t2.Modpag";
              +" and "+"TMP_PART2.Ptnumeff = _t2.Ptnumeff";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COUNTEFF = _t2.COUNTEFF";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_PART2.Datsca = _t2.Datsca";
              +" and "+"TMP_PART2.Tipcon = _t2.Tipcon";
              +" and "+"TMP_PART2.Codcon = _t2.Codcon";
              +" and "+"TMP_PART2.Modpag = _t2.Modpag";
              +" and "+"TMP_PART2.Ptnumeff = _t2.Ptnumeff";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 set ";
          +"TMP_PART2.COUNTEFF = _t2.COUNTEFF";
          +Iif(Empty(i_ccchkf),"",",TMP_PART2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_PART2.Datsca = t2.Datsca";
              +" and "+"TMP_PART2.Tipcon = t2.Tipcon";
              +" and "+"TMP_PART2.Codcon = t2.Codcon";
              +" and "+"TMP_PART2.Modpag = t2.Modpag";
              +" and "+"TMP_PART2.Ptnumeff = t2.Ptnumeff";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set (";
          +"COUNTEFF";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.COUNTEFF";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_PART2.Datsca = _t2.Datsca";
              +" and "+"TMP_PART2.Tipcon = _t2.Tipcon";
              +" and "+"TMP_PART2.Codcon = _t2.Codcon";
              +" and "+"TMP_PART2.Modpag = _t2.Modpag";
              +" and "+"TMP_PART2.Ptnumeff = _t2.Ptnumeff";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set ";
          +"COUNTEFF = _t2.COUNTEFF";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".Datsca = "+i_cQueryTable+".Datsca";
              +" and "+i_cTable+".Tipcon = "+i_cQueryTable+".Tipcon";
              +" and "+i_cTable+".Codcon = "+i_cQueryTable+".Codcon";
              +" and "+i_cTable+".Modpag = "+i_cQueryTable+".Modpag";
              +" and "+i_cTable+".Ptnumeff = "+i_cQueryTable+".Ptnumeff";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COUNTEFF = (select COUNTEFF from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna i dati dele partite presenti inun dettaglio Mandati
    * --- Write into TMP_PART2
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_PART2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART2_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSDF_BRZ',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART2_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MAXCODICE = _t2.MAXCODICE";
          +",MINCODICE = _t2.MINCODICE";
          +",TIPSEQ = _t2.TIPSEQ";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 set ";
          +"TMP_PART2.MAXCODICE = _t2.MAXCODICE";
          +",TMP_PART2.MINCODICE = _t2.MINCODICE";
          +",TMP_PART2.TIPSEQ = _t2.TIPSEQ";
          +Iif(Empty(i_ccchkf),"",",TMP_PART2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_PART2.PTSERIAL = t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set (";
          +"MAXCODICE,";
          +"MINCODICE,";
          +"TIPSEQ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MAXCODICE,";
          +"t2.MINCODICE,";
          +"t2.TIPSEQ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set ";
          +"MAXCODICE = _t2.MAXCODICE";
          +",MINCODICE = _t2.MINCODICE";
          +",TIPSEQ = _t2.TIPSEQ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MAXCODICE = (select MAXCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MINCODICE = (select MINCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TIPSEQ = (select TIPSEQ from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Per le partite ragruppate devo prendere le informazioni dell ultimo mandato valido (per tutte le partite ragruppate deve esserci la stessa informazione)
    * --- Write into TMP_PART2
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_PART2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART2_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="Datsca,Tipcon,Codcon,Modpag,Ptnumeff"
      do vq_exec with 'GSDFABRZ',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART2_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_PART2.Datsca = _t2.Datsca";
              +" and "+"TMP_PART2.Tipcon = _t2.Tipcon";
              +" and "+"TMP_PART2.Codcon = _t2.Codcon";
              +" and "+"TMP_PART2.Modpag = _t2.Modpag";
              +" and "+"TMP_PART2.Ptnumeff = _t2.Ptnumeff";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MAXCODICE = _t2.MAXCODICE";
          +",MINCODICE = _t2.MINCODICE";
          +",TIPSEQ = _t2.TIPSEQ";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_PART2.Datsca = _t2.Datsca";
              +" and "+"TMP_PART2.Tipcon = _t2.Tipcon";
              +" and "+"TMP_PART2.Codcon = _t2.Codcon";
              +" and "+"TMP_PART2.Modpag = _t2.Modpag";
              +" and "+"TMP_PART2.Ptnumeff = _t2.Ptnumeff";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 set ";
          +"TMP_PART2.MAXCODICE = _t2.MAXCODICE";
          +",TMP_PART2.MINCODICE = _t2.MINCODICE";
          +",TMP_PART2.TIPSEQ = _t2.TIPSEQ";
          +Iif(Empty(i_ccchkf),"",",TMP_PART2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_PART2.Datsca = t2.Datsca";
              +" and "+"TMP_PART2.Tipcon = t2.Tipcon";
              +" and "+"TMP_PART2.Codcon = t2.Codcon";
              +" and "+"TMP_PART2.Modpag = t2.Modpag";
              +" and "+"TMP_PART2.Ptnumeff = t2.Ptnumeff";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set (";
          +"MAXCODICE,";
          +"MINCODICE,";
          +"TIPSEQ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MAXCODICE,";
          +"t2.MINCODICE,";
          +"t2.TIPSEQ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_PART2.Datsca = _t2.Datsca";
              +" and "+"TMP_PART2.Tipcon = _t2.Tipcon";
              +" and "+"TMP_PART2.Codcon = _t2.Codcon";
              +" and "+"TMP_PART2.Modpag = _t2.Modpag";
              +" and "+"TMP_PART2.Ptnumeff = _t2.Ptnumeff";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set ";
          +"MAXCODICE = _t2.MAXCODICE";
          +",MINCODICE = _t2.MINCODICE";
          +",TIPSEQ = _t2.TIPSEQ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".Datsca = "+i_cQueryTable+".Datsca";
              +" and "+i_cTable+".Tipcon = "+i_cQueryTable+".Tipcon";
              +" and "+i_cTable+".Codcon = "+i_cQueryTable+".Codcon";
              +" and "+i_cTable+".Modpag = "+i_cQueryTable+".Modpag";
              +" and "+i_cTable+".Ptnumeff = "+i_cQueryTable+".Ptnumeff";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MAXCODICE = (select MAXCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MINCODICE = (select MINCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TIPSEQ = (select TIPSEQ from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna i dati dele partite presenti in un Mandato Intestato
    * --- Write into TMP_PART2
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_PART2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART2_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSDF2BRZ',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART2_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MAXCODICE = _t2.MAXCODICE";
          +",MINCODICE = _t2.MINCODICE";
          +",TIPSEQ = _t2.TIPSEQ";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 set ";
          +"TMP_PART2.MAXCODICE = _t2.MAXCODICE";
          +",TMP_PART2.MINCODICE = _t2.MINCODICE";
          +",TMP_PART2.TIPSEQ = _t2.TIPSEQ";
          +Iif(Empty(i_ccchkf),"",",TMP_PART2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_PART2.PTSERIAL = t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set (";
          +"MAXCODICE,";
          +"MINCODICE,";
          +"TIPSEQ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MAXCODICE,";
          +"t2.MINCODICE,";
          +"t2.TIPSEQ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set ";
          +"MAXCODICE = _t2.MAXCODICE";
          +",MINCODICE = _t2.MINCODICE";
          +",TIPSEQ = _t2.TIPSEQ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MAXCODICE = (select MAXCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MINCODICE = (select MINCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TIPSEQ = (select TIPSEQ from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna i dati dele partite presenti in un Mandato Generico
    * --- Write into TMP_PART2
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_PART2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART2_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSDF3BRZ',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART2_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MAXCODICE = _t2.MAXCODICE";
          +",MINCODICE = _t2.MINCODICE";
          +",TIPSEQ = _t2.TIPSEQ";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2, "+i_cQueryTable+" _t2 set ";
          +"TMP_PART2.MAXCODICE = _t2.MAXCODICE";
          +",TMP_PART2.MINCODICE = _t2.MINCODICE";
          +",TMP_PART2.TIPSEQ = _t2.TIPSEQ";
          +Iif(Empty(i_ccchkf),"",",TMP_PART2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_PART2.PTSERIAL = t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set (";
          +"MAXCODICE,";
          +"MINCODICE,";
          +"TIPSEQ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MAXCODICE,";
          +"t2.MINCODICE,";
          +"t2.TIPSEQ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_PART2.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_PART2.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_PART2.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART2 set ";
          +"MAXCODICE = _t2.MAXCODICE";
          +",MINCODICE = _t2.MINCODICE";
          +",TIPSEQ = _t2.TIPSEQ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MAXCODICE = (select MAXCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MINCODICE = (select MINCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TIPSEQ = (select TIPSEQ from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimina le partite di saldo sospese generate dalle ritenute solo se � stato
    *     attivato il modulo "Ritenute"
    if g_RITE="S"
      * --- Delete from TMP_PART2
      i_nConn=i_TableProp[this.TMP_PART2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART2_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with 'QUERY\GSDF4BRZ',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Elimino tutte le scadenze associate a clienti e fornitori
    *     con il check attivo "Escludi nell'esportazione per DocFinance"
    *     Elimino tutte le partite sospese che sono associate a movimenti ritenute
    vq_exec("..\Isdf\Exe\Query\Gsdf23bes.VQR", this, "PARTITE")
    * --- Cosa fare
    *     Come gestire le partite raggruppate, stesso numero effetto. Devo valorizzare
    *     solo un record
    *     Riporta descrizione partita
    *     Esportazione conti generici
    Gste_Bcp(this,"A","PARTITE"," " , , , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Nel cursore NoRagr vengono inserite tutte le scadenze che hanno il numero
    *     effetto vuoto e non devono essere state inserite in una distinta che non
    *     impegna partite, dato che per esportarle la distinta deve essere stampata
    Select Datsca,Tipcon,Codcon,(Impdar-Impave) as Saldo,; 
 Codval,Modpag,Ptserial,Ptroword,Cprownum,; 
 Flsosp,Caoape,Tippag,Andescri,Vacodiso,Angiorit,Anrating,Rascadut,; 
 Ragiorni,Ptdatape,Bansbtes,Barbntes,Anvocfin,Baflbest,; 
 Bacodabi,Bacodcab,Bapaseur,Badesban,; 
 Baindiri,Cccinabi,Cccincab,Cccodbic,Cccoiban,Provenienza,Tiposcad,; 
 Serialedistinta,Nvl(Ptnumeff,0) as Ptnumeff,1 as NumRec,; 
 Numdoc,Alfdoc,Datdoc,Ptnumcor,Ancodfis,Space(80) as Note,; 
 Andespar,Desrig,Anidridy,Antiidri,Anibarid,"S" as Ordina,DFCODICE AS DFCODICE,DFCODPAG as DFCODPAG,MAXCODICE as MAXCODICE,; 
 MATIPSDD as MATIPSDD, MADATSOT as MADATSOT,MATIPMAN as MATIPMAN,; 
 MANOMFIL as MANOMFIL,TIPSEQ as TIPSEQ,MATIPSEQ as MATIPSEQ; 
 from Partite where Nvl(Ptnumeff,0)=0 And Tiposcad $ "RC" into cursor NoRagr
    * --- Nel cursore Ragr vengono inserite tutte le scadenze che hanno il numero
    *     effetto valorizzato
    Select Datsca,Tipcon,Codcon,Sum(Impdar-Impave) as Saldo,; 
 Max(Codval) as Codval,Modpag,Max(Ptserial) as Ptserial,; 
 Max(Ptroword) as Ptroword,Max(Cprownum) as Cprownum,; 
 Max(Flsosp) as Flsosp,Max(Caoape) as Caoape,; 
 Max(Tippag) as Tippag,Max(Andescri) as Andescri,; 
 Max(Vacodiso) as Vacodiso,Max(Angiorit) as Angiorit,; 
 Max(Anrating) as Anrating,Max(Rascadut) as Rascadut,; 
 Max(Ragiorni) as Ragiorni,Max(Ptdatape) as Ptdatape,; 
 Max(Bansbtes) as Bansbtes,Max(Barbntes) as Barbntes,; 
 Max(Anvocfin) as Anvocfin,Max(Baflbest) as Baflbest,; 
 Max(Bacodabi) as Bacodabi,Max(Bacodcab) as Bacodcab,; 
 Max(Bapaseur) as Bapaseur,Max(Badesban) as Badesban,; 
 Max(Baindiri) as Baindiri,Max(Cccinabi) as Cccinabi,; 
 Max(Cccincab) as Cccincab,Max(Cccodbic) as Cccodbic,; 
 Max(Cccoiban) as Cccoiban,Max(Provenienza) as Provenienza,; 
 Max(Tiposcad) as Tiposcad,Max(Serialedistinta) as Serialedistinta,; 
 Nvl(Ptnumeff,0) as Ptnumeff,Count(*) as NumRec,; 
 Max(Numdoc) as Numdoc,Max(Alfdoc) as Alfdoc,Max(Datdoc) as Datdoc,; 
 Max(Ptnumcor) as Ptnumcor,Max(Ancodfis) as Ancodfis,; 
 Space(80) as Note,Max(Andespar) as Andespar,Max(Desrig) as Desrig,; 
 Max(Anidridy) as Anidridy,Max(Antiidri) as Antiidri,Max(Anibarid) as Anibarid,; 
 "E" as Ordina, MAX(DFCODICE) AS DFCODICE , MAX(DFCODPAG) as DFCODPAG, Max(MAXCODICE) as MAXCODICE,Max(MATIPSDD) as MATIPSDD,; 
 Max(MADATSOT) as MADATSOT,Max(MATIPMAN) as MATIPMAN,; 
 Max(MANOMFIL) as MANOMFIL,Max(TIPSEQ) as TIPSEQ,Max(MATIPSEQ) as MATIPSEQ; 
 from Partite where Nvl(Ptnumeff,0)<>0; 
 group by Datsca,Tipcon,Codcon,Modpag,Ptnumeff into cursor Ragr 
    Cur = WrCursor("Ragr")
    Select Ragr 
 Go Top 
 Scan for Ragr.NumRec>1
    this.w_Datsca = Cp_Todate(Ragr.Datsca)
    this.w_Modpag = Ragr.Modpag
    this.w_Numeff = Ragr.Ptnumeff
    this.w_Tipo = Ragr.Tipcon
    this.w_Codcon = Ragr.Codcon
    Select Partite 
 Go Top 
 Locate For this.w_Datsca=Partite.Datsca and this.w_Modpag=Partite.Modpag and; 
 this.w_Numeff=Partite.Ptnumeff and this.w_Tipo=Partite.Tipcon and ; 
 this.w_Codcon=Partite.Codcon
    this.w_Note = ""
    do while found ()
      this.w_Note = this.w_Note+"Doc:"+Alltrim(Str(Nvl(Partite.Numdoc,0),15,0))
      this.w_Note = this.w_Note+iif( Empty(Nvl(Partite.Alfdoc,space(2))) , "", "/"+Alltrim(Nvl(Partite.Alfdoc,Space(2))))
      this.w_Note = this.w_Note+"-"+left(Dtoc(Cp_Todate(Partite.Datdoc)),2) + substr(Dtoc(Cp_Todate(Partite.Datdoc)),4,2) + right(Dtoc(Cp_Todate(Partite.Datdoc)),4)
      if Nvl(Partite.Andespar,"N")="S"
        this.w_Note = this.w_Note+iif(Not Empty(Nvl(Partite.Desrig,"")),"-"+Alltrim(Partite.Desrig),"")
      endif
      continue
    enddo
    Select Ragr
    Replace Note with this.w_Note
    Endscan
    use in select ("Partite")
    * --- Il cursore Doc_Rate contiene l'elenco dei documenti estrapolati tramite
    *     le query del Cash Flow
    this.w_Tipcli = "C"
    this.w_Tipfor = "F"
    this.w_Tipgen = " "
    this.w_Codcli = Space(15)
    this.w_Codfor = Space(15)
    this.w_Ndocin = 0
    this.w_Ndocfi = 0
    this.w_Adocin = Space(10)
    this.w_Adocfi = Space(10)
    this.w_Codval = Space(3)
    this.w_Ddocin = Cp_CharToDate("  -  -    ")
    this.w_Ddocfi = Cp_CharToDate("  -  -    ")
    this.w_Datini = i_Inidat
    this.w_Datfin = i_Findat
    this.w_Filatt = Space(10)
    this.w_Pagrd = "RD"
    this.w_Pagbo = "BO"
    this.w_Pagrb = "RB"
    this.w_Pagri = "RI"
    this.w_Pagca = "CA"
    this.w_Pagma = "MA"
    ADDMSG("%1Elabora scadenza da cash flow%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
    * --- Effettuo un controllo sul campo "Calcolo rate documenti" per lanciare 
    *     query differenti
    if this.oParentObject.w_DFCASFLO="S"
      Vq_Exec("..\Isdf\exe\query\gsdf25bes.VQR",this,"Doc_Rate")
    else
      Vq_Exec("..\Isdf\exe\query\gsdf28bes.VQR",this,"Doc_Rate")
    endif
    Select * from NoRagr; 
 union all ; 
 Select * from Ragr ; 
 union all ; 
 Select * , space(15) as MAXCODICE, " " as MATIPSDD,; 
 ctod("  /  /  ") as MADATSOT, " " as MATIPMAN,; 
 space(15) as MANOMFIL,space(4) as TIPSEQ,space(4) as MATIPSEQ from Doc_Rate; 
 order by 49,5,1,2,3 into cursor Partite
    use in select ("NoRagr")
    use in select ("Ragr")
    use in select ("Doc_Rate")
    Select Partite
    Go Top
    Scan
    this.w_Tippag = Partite.Tippag
    this.w_Valore = Partite.Saldo
    this.w_Valore = iif(this.w_Valore>0,"+","-")+right(repl("0",11)+alltrim(Strtran(Str(Abs(this.w_Valore),11,3),".",",")),15)
    this.w_CodIsoValuta = Nvl(Partite.Vacodiso,Space(3))
    this.w_CodIsoValuta = left( Alltrim(this.w_CodIsoValuta) + space(5) ,5)
    if Empty(this.w_CodIsoValuta)
      this.w_Codval = Partite.Codval
      if ! this.w_Codval $ this.w_ControlloValute
        this.w_oErrorLog.AddMsgLog("Codice ISO della valuta %1 non inserito", this.w_Codval)     
        this.w_ControlloValute = this.w_ControlloValute+","+Alltrim(this.w_Codval)
      endif
    endif
    this.w_CambioApertura = right(repl("0",5)+alltrim(Strtran(Str(Partite.Caoape,5,3),".",",")),9)
    this.w_Codval = g_Perval
    this.w_Angiorit = Nvl(Partite.Angiorit,0)
    this.w_Ptdatsca = Cp_Todate(Partite.Datsca)+this.w_Angiorit
    this.w_Anrating = Nvl(Partite.Anrating,Space(2))
    if Empty(this.w_Anrating)
      this.w_Anrating = iif(this.w_Ptdatsca>=i_Datsys,this.oParentObject.w_Dfratsca,this.oParentObject.w_Dfratsdt)
    else
      this.w_Rascadut = Nvl(Partite.Rascadut,Space(2))
      this.w_Ragiorni = Nvl(Partite.Ragiorni,0)
      if Not Empty(this.w_Rascadut)
        this.w_Anrating = iif((this.w_Ptdatsca+this.w_Ragiorni)>=i_Datsys,this.w_Anrating,this.w_Rascadut)
      endif
    endif
    this.w_Anrating = left( Alltrim(this.w_Anrating) + space(2) , 2)
    * --- Controllo se la valuta sulla scadenza e diversa dalla valuta di conto
    if this.w_Codval<>Partite.Codval
      this.w_Controvalore = Cp_Round(Val2mon(Partite.Saldo,Partite.Caoape,g_Caoval,Partite.Ptdatape, this.w_Codval ),g_Perpvl)
      this.w_Controvalore = iif(this.w_Controvalore>0,"+","-")+right(repl("0",12)+alltrim(Strtran(Str(Abs(this.w_Controvalore),12,2),".",",")),15)
    else
      this.w_Controvalore = iif(Partite.Saldo>0,"+","-")+right(repl("0",12)+alltrim(Strtran(Str(Abs(Partite.Saldo),12,2),".",",")),15)
    endif
    this.w_DataScadenza = Dtoc(Cp_Todate(Partite.Datsca))
    this.w_DataScadenza = left(this.w_DataScadenza,2) + substr(this.w_DataScadenza,4,2) + right(this.w_DataScadenza,4)
    this.w_DataValuta = Dtoc(this.w_Ptdatsca)
    this.w_DataValuta = left(this.w_DataValuta,2) + substr(this.w_DataValuta,4,2) + right(this.w_DataValuta,4)
    this.w_Bansbtes = left( Alltrim(Nvl(Partite.Bansbtes,space(8))) + space(8) , 8)
    this.w_Barbntes = left( Alltrim(Nvl(Partite.Barbntes,space(4))) + space(4) , 4)
    this.w_FlagSospeso = Nvl(Partite.Flsosp," ")
    this.w_Anvocfin = Nvl(Partite.Anvocfin,Space(6))
    if Empty(this.w_Anvocfin)
      this.w_Anvocfin = iif(Partite.Tipcon="C",this.oParentObject.w_Dfvocfcl,iif(Partite.Tipcon="F",this.oParentObject.w_Dfvocffo,this.oParentObject.w_Dfvocfge))
    endif
    this.w_Anvocfin = left( Alltrim(this.w_Anvocfin) + space(6) ,6)
    this.w_Codpdc = iif(Partite.Tipcon="G","-",Partite.Tipcon)+Alltrim(Partite.Codcon)
    this.w_Codpdc = left( this.w_Codpdc+Space(16),16)
    this.w_Andescri = Partite.Andescri
    this.w_Andescri = left( Alltrim(this.w_Andescri) + space(40) , 40)
    this.w_Baflstra = Nvl(Partite.Baflbest,"N")
    if this.w_Baflstra="S"
      this.w_CliAbi = Space(5)
      this.w_CliCab = Space(5)
      this.w_Cccinabi = Space(1)
      this.w_Bapaseur = Space(2)
      this.w_Cccincab = space(2)
      this.w_Annumcor = space(12)
      this.w_Cccodbic = left( Alltrim(Nvl(Partite.Cccodbic,space(11))) + space(11) , 11)
      this.w_Cccoiban = left( Alltrim(Nvl(Partite.Cccoiban,space(34))) + space(34) , 34)
      this.w_Badesban = left( Alltrim(Nvl(Partite.Badesban,space(35))) + space(35) , 35)
      this.w_Baindiri = left( Alltrim(Nvl(Partite.Baindiri,space(35))) + space(35) , 35)
    else
      if this.oParentObject.w_Dfcorrsc="S"
        this.w_Annumcor = iif(Empty(Nvl(Ptnumcor,Space(12))),Space(12),right(replicate("0", 12)+alltrim(Nvl(Ptnumcor,space(12))),12))
      else
        this.w_Annumcor = left( alltrim(Nvl(Ptnumcor,space(12))) + space(12) , 12)
      endif
      this.w_CliAbi = left( Alltrim(Nvl(Partite.Bacodabi,space(5))) + space(5) , 5)
      this.w_CliCab = left( Alltrim(Nvl(Partite.Bacodcab,space(5))) + space(5) , 5)
      this.w_Cccinabi = Nvl(Partite.Cccinabi,Space(1))
      this.w_Bapaseur = iif(Partite.Tipcon="G",Space(2),iif(empty(Nvl(Partite.Bapaseur,space(2))),"IT",Partite.Bapaseur))
      this.w_Cccincab = left( Alltrim(Nvl(Partite.Cccincab,space(2))) + space(2) , 2)
      this.w_Cccodbic = Space(11)
      this.w_Cccoiban = Space(34)
      this.w_Badesban = Space(35)
      this.w_Baindiri = space(35)
    endif
    this.w_Provenienza = Partite.Provenienza
    this.w_Sezione = Space(6)
    this.w_Ptnumeff = Nvl(Partite.Ptnumeff,0)
    this.w_NumRec = Nvl(Partite.NumRec,1)
    do case
      case Partite.Tiposcad="D"
        this.w_NumDocOrigine = left(Alltrim(Str(this.w_Ptnumeff,6,0))+space(12),12)
        this.w_ChiaveCoge = Partite.SerialeDistinta+left(Alltrim(Str(this.w_Ptnumeff,6,0))+space(6),6)+Space(9)+Partite.Tiposcad
      case (Partite.Tiposcad="R" And this.w_Numrec=1) or Partite.Tiposcad="C"
        if Partite.Tiposcad="C"
          this.w_NumDocOrigine = Alltrim(Str(Nvl(Partite.Numdoc,0),15,0))+Alltrim(Nvl(Partite.Alfdoc,"  "))
          this.w_NumDocOrigine = left(this.w_NumDocOrigine+space(12),12)
        else
          this.w_NumDocOrigine = left(Alltrim(Str(this.w_Ptnumeff,6,0))+space(12),12)
        endif
        this.w_ChiaveCoge = Partite.Ptserial+left(Alltrim(Str(Partite.Ptroword,4,0))+space(4),4)+left(Alltrim(Str(Partite.Cprownum,4,0))+space(4),4)+Space(7)+"C"
      case Partite.Tiposcad="R" And this.w_Numrec>1
        this.w_NumDocOrigine = left(Alltrim(Str(this.w_Ptnumeff,6,0))+space(12),12)
        this.w_ChiaveCoge = left(Alltrim(Str(this.w_Ptnumeff,6,0))+space(6),6)+left(Alltrim(Partite.Modpag)+space(10),10)+this.w_DataScadenza+Space(1)+"R"
    endcase
    if Partite.Tiposcad="R" And Partite.Tipcon="C"
      this.w_Anvocfin = left( Alltrim(this.oParentObject.w_Dfvoceff) + space(6) ,6)
    endif
    this.w_Aztraazi = left( this.oParentObject.w_Dftraazi + space(10) , 10)
    this.w_DataDocOrigine = Dtoc(Cp_Todate(Partite.Datdoc))
    this.w_DataDocOrigine = left(this.w_DataDocOrigine,2) + substr(this.w_DataDocOrigine,4,2) + right(this.w_DataDocOrigine,4)
    this.w_Clifis = Nvl(Partite.Ancodfis,Space(16))
    this.w_Ptdescri = Nvl(Partite.Desrig,Space(50))
    this.w_Despar = Nvl(Partite.Andespar,"N")
    this.w_Note = ""
    * --- Devo verificare se esistono pi� partite associate allo stesso numero effetto
    if this.w_NumRec>1
      this.w_Note = left(Partite.Note+space(80) , 80)
    else
      if this.w_Ptnumeff<>0
        this.w_Note = "Doc:"+Alltrim(Str(Nvl(Partite.Numdoc,0),15,0))
        this.w_Note = this.w_Note+iif( Empty(Nvl(Partite.Alfdoc,space(2))) , "", "/" + Alltrim(Nvl(Partite.Alfdoc,Space(2))))+"-"+this.w_DataDocOrigine
      else
        this.w_Note = Alltrim(this.w_Andescri)+"-"+iif(Not Empty(this.w_Clifis),Alltrim(this.w_Clifis)+"-","")+"Doc:"+Alltrim(Str(Nvl(Partite.Numdoc,0),15,0))
        this.w_Note = this.w_Note+iif( Empty(Nvl(Partite.Alfdoc,space(2))) , "", "/" + Alltrim(Nvl(Partite.Alfdoc,Space(2))))+"-"+this.w_DataDocOrigine
      endif
      if this.w_Despar="S"
        this.w_Note = this.w_Note+iif(Not Empty(this.w_Ptdescri),"-"+Alltrim(this.w_Ptdescri),"")
      endif
      this.w_Note = left(this.w_Note+space(80) , 80)
    endif
    * --- Controllo se la tipologia pagamento � R.I.D.
    if this.w_Tippag="RI"
      if Nvl(Partite.Anidridy,"N")="S"
        this.w_TipoCodice = Str(Nvl(Partite.Antiidri,1),1)
        this.w_CodiceCliente = Nvl(Partite.Anibarid,Space(16))
      else
        this.w_TipoCodice = "4"
        this.w_CodiceCliente = this.w_Codpdc
      endif
    else
      this.w_TipoCodice = iif(this.w_Tippag $ "RB-MA","4",Space(1))
      this.w_CodiceCliente = iif(this.w_Tippag $ "RB-MA",this.w_Codpdc,Space(16))
    endif
    * --- Gestione dei campi dei Mandati
    if this.w_Tippag="RI" AND NOT EMPTY (NVL (MAXCODICE,"")) 
      * --- Se � un pagamento Rid e sono attivi i mandati Modifica il Tipo di Pagamento
      do case
        case NVL (MATIPSDD, " ")="B"
          this.w_Tippag = "RV"
        case NVL (MATIPSDD, " ")="C"
          this.w_Tippag = "RO"
      endcase
      * --- Al posto della data documento si mette la data sottoscrizione mandato
      this.w_DataDocOrigine = Dtoc( NVL (Partite.MADATSOT , ctod("  /  /  ") ) )
      this.w_DataDocOrigine = left(this.w_DataDocOrigine,2) + substr(this.w_DataDocOrigine,4,2) + right(this.w_DataDocOrigine,4)
    endif
    this.w_MATIPSEQ = IIF ( NVL (MATIPMAN ," ")="G" AND EMPTY (NVL (MANOMFIL," ")) and nvl (TIPSEQ ," ")="RCUR", "FRST" , NVL (MATIPSEQ," ") )
    this.w_TipoCodice = Alltrim(this.w_TipoCodice)
    this.w_CodiceCliente = Left(this.w_CodiceCliente+Space(16),16)
    this.w_Record = IIF (NVL(this.oParentObject.w_DFTRATIP," ")="S" AND NOT EMPTY (NVL (DFCODPAG,DFCODICE)), NVL(DFCODPAG,DFCODICE) ,this.w_Tippag)+this.w_Valore+this.w_CodIsoValuta+this.w_CambioApertura+this.w_Controvalore+Space(2)+ IIF ( (this.w_Tippag="RO" OR this.w_Tippag="RV" ) AND NOT EMPTY (NVL (MAXCODICE,"")) , left ( NVL ( this.w_MATIPSEQ ,"00001") +SPACE (5) ,5) , "00001")
    this.w_Record = this.w_Record+this.w_Anrating+this.w_DataScadenza+this.w_DataValuta+this.w_Bansbtes+this.w_Barbntes+this.w_FlagSospeso
    this.w_Record = this.w_Record+this.w_Anvocfin+this.w_Codpdc+this.w_Andescri+this.w_CliAbi+this.w_CliCab+Space(10)+this.w_Provenienza
    this.w_Record = this.w_Record+this.w_Sezione+"00000,000"+this.w_NumDocOrigine+this.w_ChiaveCoge+this.w_Aztraazi+Space(1)
    this.w_Record = this.w_Record+this.w_Annumcor+this.w_Cccinabi+Space(16)+this.w_Bapaseur+this.w_Cccincab+this.w_DataDocOrigine+this.w_Note
    this.w_Record = this.w_Record+this.w_Cccodbic+this.w_Cccoiban+this.w_TipoCodice+this.w_CodiceCliente+Space(1)+Space(8)+Space(4)
    this.w_Record = this.w_Record+this.w_Badesban+this.w_Baindiri
    * --- Scrittura record
    w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
    Endscan
    * --- Drop temporary table TMP_PART2
    i_nIdx=cp_GetTableDefIdx('TMP_PART2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART2')
    endif
    * --- Drop temporary table TMP_PART3
    i_nIdx=cp_GetTableDefIdx('TMP_PART3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART3')
    endif
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    * --- Drop temporary table ZOOMPART
    i_nIdx=cp_GetTableDefIdx('ZOOMPART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ZOOMPART')
    endif
    use in select ("Partite")
    if .not. fclose(this.w_Handle) Or this.w_oErrorLog.IsFullLog()
      Namefile='"'+this.w_FileScadenziario+'"'
      Delete File &Namefile
      this.w_ErrorEsportazione = .t.
      if Ah_Yesno("Esportazione scadenze fallita. %1Stampo situazione messaggi di errore?",,CHR(13))
        this.w_oErrorLog.PrintLog( this.oParentObject.oParentObject,"Errori riscontrati",.f.)     
      endif
    else
      ADDMSG("%1Esportazione scadenze avvenuta correttamente%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancio la routine Gste_Bpa per avere l'elenco delle partita aperte
    this.w_Tipo = "C"
    this.w_Codcon = Space(15)
    this.w_Codcon1 = Space(15)
    this.w_Codval = g_Perval
    this.w_Scaini = Cp_CharToDate("  -  -    ")
    this.w_Scafin = Cp_CharToDate("  -  -    ")
    this.w_Flrd1 = "XX"
    this.w_Flrb1 = iif(Empty(this.oParentObject.w_Dfdis_Ra),"XX",this.oParentObject.w_Dfdis_Ra)
    this.w_Flbo1 = "XX"
    this.w_Flri1 = iif(Empty(this.oParentObject.w_Dfdis_Ri),"XX",this.oParentObject.w_Dfdis_Ri)
    this.w_Flca1 = "XX"
    this.w_Flma1 = "XX"
    this.w_Abicod = Space(5)
    this.w_Codban = Space(15)
    this.w_Codzon = Space(3)
    this.w_Consup = Space(15)
    this.w_Catcom = Space(3)
    this.w_Lcodeff = Space(15)
    this.w_Tipsca = "C"
    this.w_Datdocini = Cp_CharToDate("  -  -    ")
    this.w_Datdocfin = Cp_CharToDate("  -  -    ")
    this.w_Alfdocfin = Space(10)
    this.w_Alfdocini = Space(10)
    this.w_Numdocfin = 0
    this.w_Numdocini = 0
    this.w_Ltippar = "T"
    this.w_Filrow = -3
    this.w_TIPMAN = "T"
    GSTE_BPA (this, this.w_Scaini , this.w_Scafin , this.w_Tipo , this.w_Codcon, this.w_Codcon1 , this.w_Codval , "", this.w_Flrb1,this.w_Flbo1,this.w_Flrd1,this.w_Flri1,this.w_Flma1,this.w_Flca1, "0000000000"+"A", "S", cp_CharToDate("  -  -  ") )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec("query\GSTE0KAI.VQR",this,"PAR_APE")
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    if Reccount("Par_Ape")>0
      Cur = WrCursor("PAR_APE")
      GSTE_BCP(this,"D","PAR_APE",this.w_Tipsca , "A" , "RB",.T. )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Filtro per pagamento della distinta e per Ns Banca, lo faccio adesso perch� prima devo calcolare la parte residua
      *     Eseguo una go top per problema riscontrato in Oracle\Db2 nella select successiva che ripescava record eliminati nella
      *     precedente delete
      select Par_ape 
 Go top
      Select Datsca,Tipcon,Codcon,Modpag,Banapp,; 
 Space(15) as Bannos,Anflragg,Ptserial,Ptroword,; 
 Cprownum,Ptcodrag,Tippag,Space(25) as Ptnumcor From Par_Ape Where Tippag in (this.w_Flrd1,this.w_Flbo1,this.w_Flca1,this.w_Flri1,this.w_Flma1,this.w_Flrb1) ; 
 And (Nvl(Bannos,Space(15))=this.w_Codban Or Empty(this.w_Codban)) And (Nvl(Codabi,Space(5))=this.w_Abicod ; 
 Or Empty(this.w_Abicod)) And (NVL(Ciconefa,Space(15))=this.w_Lcodeff Or Empty(this.w_Lcodeff)) ; 
 And (Empty( this.w_Datdocini) Or Cp_Todate(DatDoc)>=this.w_Datdocini) ; 
 And (Empty( this.w_Datdocfin) Or Cp_Todate(DatDoc)<=this.w_Datdocfin) ; 
 And (this.w_Numdocini=0 Or Nvl(NumDoc,0)>=this.w_Numdocini) ; 
 And (this.w_Numdocfin=0 Or Nvl(NumDoc,0)<=this.w_Numdocfin) ; 
 And (Empty( this.w_Alfdocini) Or Nvl(AlfDoc,Space(10))>=this.w_Alfdocini) ; 
 And (Empty( this.w_Alfdocfin) Or Nvl(AlfDoc,Space(10))<=this.w_Alfdocfin) ; 
 And Nvl(Ptnumeff,0)=0 ; 
 And ((this.w_Ltippar $ "A-T" And Ptroword>=this.w_Filrow) Or (this.w_Ltippar = "C" And Ptroword =this.w_Filrow)) AND Not Deleted() Into Cursor Selepart ; 
 order by Tippag,Datsca,Tipcon,Codcon,Modpag,Banapp,Bannos,Ptcodrag,Ptnumcor NoFilter
      * --- Devo effettuare una rottura per tipologia di pagamento per avere una numerazione
      *     degli effetti differente
      this.w_Newrec = "XXX"
      this.w_Oldrec = "ZZZ"
      this.w_TipoPagamento = "ZZ"
      if Reccount("Selepart")>0
        Select Selepart 
 Go Top 
 Scan
        if this.w_TipoPagamento<>Selepart.Tippag
          if this.w_TipoPagamento<>"ZZ"
            * --- Aggiorno il progressivo effetti
            * --- Try
            local bErr_029F67F0
            bErr_029F67F0=bTrsErr
            this.Try_029F67F0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              * --- Write into PRO_NUME
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PRO_NUME_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_NUME_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PRNUMPRO ="+cp_NullLink(cp_ToStrODBC(this.w_Progre),'PRO_NUME','PRNUMPRO');
                    +i_ccchkf ;
                +" where ";
                    +"PRCODAZI = "+cp_ToStrODBC(i_codazi);
                    +" and PRCODESE = "+cp_ToStrODBC(g_codese);
                    +" and PRTIPPAG = "+cp_ToStrODBC(this.w_TipoPagamento);
                    +" and PRTIPCON = "+cp_ToStrODBC("C");
                       )
              else
                update (i_cTable) set;
                    PRNUMPRO = this.w_Progre;
                    &i_ccchkf. ;
                 where;
                    PRCODAZI = i_codazi;
                    and PRCODESE = g_codese;
                    and PRTIPPAG = this.w_TipoPagamento;
                    and PRTIPCON = "C";

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            bTrsErr=bTrsErr or bErr_029F67F0
            * --- End
          endif
          this.w_TipoPagamento = Selepart.Tippag
          * --- Read from PRO_NUME
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRO_NUME_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PRNUMPRO"+;
              " from "+i_cTable+" PRO_NUME where ";
                  +"PRCODAZI = "+cp_ToStrODBC(i_codazi);
                  +" and PRCODESE = "+cp_ToStrODBC(g_CODESE);
                  +" and PRTIPPAG = "+cp_ToStrODBC(this.w_TipoPagamento);
                  +" and PRTIPCON = "+cp_ToStrODBC("C");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PRNUMPRO;
              from (i_cTable) where;
                  PRCODAZI = i_codazi;
                  and PRCODESE = g_CODESE;
                  and PRTIPPAG = this.w_TipoPagamento;
                  and PRTIPCON = "C";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PROGRE = NVL(cp_ToDate(_read_.PRNUMPRO),cp_NullValue(_read_.PRNUMPRO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_Progre = nvl(this.w_Progre,0)
        endif
        this.w_Ptserial = Selepart.Ptserial
        this.w_Ptroword = Selepart.Ptroword
        this.w_Cprownum = Selepart.Cprownum
        this.w_Newrec = Dtoc(Cp_Todate(Selepart.Datsca))+Nvl(Selepart.Tipcon," ")+Nvl(Selepart.Codcon,Space(15))+Nvl(Selepart.Modpag,Space(10))+Nvl(Selepart.Ptnumcor,"")
        this.w_Newrec = this.w_Newrec+Nvl(Selepart.Banapp,Space(10))+Space(15)+Nvl(Selepart.Ptcodrag," ")
        if Empty(Nvl(Selepart.Anflragg," ")) Or this.w_Newrec<>this.w_Oldrec
          this.w_Progre = this.w_Progre+1
          this.w_Oldrec = this.w_Newrec
        endif
        * --- Scrivo il numero Effetto
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'PAR_TITE','PTNUMEFF');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_Ptserial);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_Ptroword);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_Cprownum);
                 )
        else
          update (i_cTable) set;
              PTNUMEFF = this.w_PROGRE;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.w_Ptserial;
              and PTROWORD = this.w_Ptroword;
              and CPROWNUM = this.w_Cprownum;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        Endscan
        * --- Aggiorno il progressivo effetti
        * --- Try
        local bErr_029F0F70
        bErr_029F0F70=bTrsErr
        this.Try_029F0F70()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into PRO_NUME
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRO_NUME_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_NUME_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PRNUMPRO ="+cp_NullLink(cp_ToStrODBC(this.w_Progre),'PRO_NUME','PRNUMPRO');
                +i_ccchkf ;
            +" where ";
                +"PRCODAZI = "+cp_ToStrODBC(i_codazi);
                +" and PRCODESE = "+cp_ToStrODBC(g_codese);
                +" and PRTIPPAG = "+cp_ToStrODBC(this.w_TipoPagamento);
                +" and PRTIPCON = "+cp_ToStrODBC("C");
                   )
          else
            update (i_cTable) set;
                PRNUMPRO = this.w_Progre;
                &i_ccchkf. ;
             where;
                PRCODAZI = i_codazi;
                and PRCODESE = g_codese;
                and PRTIPPAG = this.w_TipoPagamento;
                and PRTIPCON = "C";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_029F0F70
        * --- End
      endif
      ADDMSG("%1Numerazione effetti avvenuta correttamente%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
      use in select ("Selepart")
      use in select ("Par_Ape")
    endif
  endproc
  proc Try_029F67F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRO_NUME
    i_nConn=i_TableProp[this.PRO_NUME_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRO_NUME_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODAZI"+",PRCODESE"+",PRTIPPAG"+",PRTIPCON"+",PRNUMPRO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(i_codazi),'PRO_NUME','PRCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(g_codese),'PRO_NUME','PRCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoPagamento),'PRO_NUME','PRTIPPAG');
      +","+cp_NullLink(cp_ToStrODBC("C"),'PRO_NUME','PRTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Progre),'PRO_NUME','PRNUMPRO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODAZI',i_codazi,'PRCODESE',g_codese,'PRTIPPAG',this.w_TipoPagamento,'PRTIPCON',"C",'PRNUMPRO',this.w_Progre)
      insert into (i_cTable) (PRCODAZI,PRCODESE,PRTIPPAG,PRTIPCON,PRNUMPRO &i_ccchkf. );
         values (;
           i_codazi;
           ,g_codese;
           ,this.w_TipoPagamento;
           ,"C";
           ,this.w_Progre;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_029F0F70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRO_NUME
    i_nConn=i_TableProp[this.PRO_NUME_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRO_NUME_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODAZI"+",PRCODESE"+",PRTIPPAG"+",PRTIPCON"+",PRNUMPRO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(i_codazi),'PRO_NUME','PRCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(g_codese),'PRO_NUME','PRCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoPagamento),'PRO_NUME','PRTIPPAG');
      +","+cp_NullLink(cp_ToStrODBC("C"),'PRO_NUME','PRTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Progre),'PRO_NUME','PRNUMPRO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODAZI',i_codazi,'PRCODESE',g_codese,'PRTIPPAG',this.w_TipoPagamento,'PRTIPCON',"C",'PRNUMPRO',this.w_Progre)
      insert into (i_cTable) (PRCODAZI,PRCODESE,PRTIPPAG,PRTIPCON,PRNUMPRO &i_ccchkf. );
         values (;
           i_codazi;
           ,g_codese;
           ,this.w_TipoPagamento;
           ,"C";
           ,this.w_Progre;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Record
    this.w_FilePdc = alltrim(this.oParentObject.w_Pathfile)+alltrim(this.oParentObject.w_Nomefile)
    Public erroHAND 
 erroHAND=0
    ON ERROR erroHAND= -1
    this.w_Handle = fcreate(this.w_FilePdc, 0)
    ON ERROR 
    if this.w_Handle = -1 OR erroHAND=-1
      Ah_Errormsg("Non � possibile esportare i dati",48)
      i_retcode = 'stop'
      return
    endif
    VQ_EXEC("..\Isdf\exe\query\gsdf_bes.VQR",this,"__TMP__")
    select __tmp__
    go top
    scan
    this.w_Aztraazi = left( this.oParentObject.w_Dftraazi + space(10) , 10)
    this.w_Codpdc = Antipcon+Alltrim(Ancodice)
    this.w_Codpdc = left( this.w_Codpdc + space(16) , 16)
    this.w_Flagpdc = Antipcon
    this.w_Andescri = left( Alltrim(Nvl(Andescri,space(80))) + space(80) , 80)
    this.w_NomDescri = left( Alltrim(this.w_Andescri) + space(6) , 6)
    this.w_Anindiri = left( Alltrim(Nvl(Anindiri,space(40))) + space(40) , 40)
    this.w_An___Cap = left( Alltrim(Nvl(An___Cap,space(9))) + space(9) , 9)
    this.w_Anlocali = left( Alltrim(Nvl(Anlocali,space(40))) + space(40) , 40)
    this.w_Anprovin = iif(Nvl(Anprovin,"  ")="EE",Space(2),left( Alltrim(Nvl(Anprovin,space(2))) + space(2) , 2))
    this.w_Nacodiso = left( Alltrim(Nvl(Nacodiso,space(4))) + space(4) , 4)
    this.w_Bansbtes = left( Alltrim(Nvl(Bansbtes,space(8))) + space(8) , 8)
    this.w_Barbntes = left( Alltrim(Nvl(Barbntes,space(4))) + space(4) , 4)
    if this.oParentObject.w_Dfcorrpc="S"
      this.w_Annumcor = iif(Empty(Nvl(Annumcor,Space(12))),Space(12),right(replicate("0", 12)+alltrim(Nvl(Annumcor,space(12))),12))
    else
      this.w_Annumcor = left( alltrim(Nvl(Annumcor,space(12))) + space(12) , 12)
    endif
    this.w_Anrating = left( Alltrim(Nvl(Anrating,space(2))) + space(2) , 2)
    this.w_Angiorit = right("000"+Alltrim(Str(Nvl(Angiorit,0),3,0)),3)
    this.w_Linea = this.oParentObject.w_Df_Linea
    this.w_Linea = left( this.w_Linea + space(4) , 4)
    this.w_Anvocfin = Nvl(Anvocfin,space(6))
    this.w_Anvocfin = iif(Empty(this.w_Anvocfin),iif(Antipcon="C",this.oParentObject.w_Dfvocfcl,iif(Antipcon="F",this.oParentObject.w_Dfvocffo,Space(6))),this.w_Anvocfin)
    this.w_Anvocfin = left( Alltrim(this.w_Anvocfin) + space(6) ,6)
    this.w_An_Email = left( Alltrim(Nvl(An_Email,space(80))) + space(80) , 80)
    this.w_Baflstra = Nvl(Baflstra,"N")
    if this.w_Baflstra="S"
      this.w_CliAbi = Space(5)
      this.w_CliCab = Space(5)
      this.w_Cccinabi = Space(1)
      this.w_Bapaseur = Space(2)
      this.w_Cccincab = space(2)
      this.w_Annumcor = space(12)
      this.w_Cccodbic = left( Alltrim(Nvl(Cccodbic,space(11))) + space(11) , 11)
      this.w_Cccoiban = left( Alltrim(Nvl(Cccoiban,space(34))) + space(34) , 34)
      this.w_Badesban = left( Alltrim(Nvl(Badesban,space(35))) + space(35) , 35)
      this.w_Baindiri = left( Alltrim(Nvl(Baindiri,space(35))) + space(35) , 35)
    else
      this.w_CliAbi = left( Alltrim(Nvl(Bacodabi,space(5))) + space(5) , 5)
      this.w_CliCab = left( Alltrim(Nvl(Bacodcab,space(5))) + space(5) , 5)
      this.w_Cccinabi = Nvl(Cccinabi,Space(1))
      this.w_Bapaseur = iif(Antipcon="-",Space(2),iif(empty(Nvl(Bapaseur,space(2))),"IT",Bapaseur))
      this.w_Cccincab = left( Alltrim(Nvl(Cccincab,space(2))) + space(2) , 2)
      this.w_Cccodbic = Space(11)
      this.w_Cccoiban = Space(34)
      this.w_Badesban = Space(35)
      this.w_Baindiri = space(35)
    endif
    this.w_Clifis = Left(Nvl(Ancodfis,space(16))+ space(16), 16)
    this.w_Anpariva = Left(Nvl(Anpariva,space(11))+ space(11), 11)
    this.w_Record = this.w_Aztraazi+this.w_Codpdc+this.w_Flagpdc+this.w_Andescri+this.w_NomDescri+this.w_Anindiri+this.w_An___Cap+this.w_Anlocali
    this.w_Record = this.w_Record+this.w_Anprovin+this.w_Nacodiso+this.w_Bansbtes+this.w_Barbntes+this.w_Anrating+this.w_Angiorit
    this.w_Record = this.w_Record+this.w_Linea+this.w_Annumcor+this.w_CliAbi+this.w_CliCab+this.w_Clifis+this.w_Anpariva+this.w_An_Email
    this.w_Record = this.w_Record+this.w_Cccinabi+this.w_Bapaseur+this.w_Cccincab+this.w_Cccodbic+this.w_Cccoiban+this.w_Anvocfin
    this.w_Record = this.w_Record+this.w_Badesban+this.w_Baindiri
    * --- Scrittura record
    w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
    Endscan
    use in select ("__TMP__")
    if .not. fclose(this.w_Handle)
      Namefile='"'+this.w_FilePdc+'"'
      Delete File &Namefile
    else
      * --- Write into DOCFINPA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOCFINPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOCFINPA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOCFINPA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DFDATUEX ="+cp_NullLink(cp_ToStrODBC(i_Datsys),'DOCFINPA','DFDATUEX');
            +i_ccchkf ;
        +" where ";
            +"DFCODAZI = "+cp_ToStrODBC(this.oParentObject.w_Codazi);
               )
      else
        update (i_cTable) set;
            DFDATUEX = i_Datsys;
            &i_ccchkf. ;
         where;
            DFCODAZI = this.oParentObject.w_Codazi;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      ADDMSG("%1Esportazione piano dei conti avvenuta correttamente%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esportazione movimenti
    this.w_FileMovimenti = alltrim(this.oParentObject.w_Pathfile)+alltrim(this.oParentObject.w_NomefileMov)
    Public erroHAND 
 erroHAND=0
    ON ERROR erroHAND= -1
    this.w_Handle = fcreate(this.w_FileMovimenti, 0)
    ON ERROR 
    if this.w_Handle = -1 OR erroHAND=-1
      Ah_Errormsg("Non � possibile esportare i dati",48)
      i_retcode = 'stop'
      return
    endif
    VQ_EXEC("..\Isdf\exe\query\gsdf27bes.VQR",this,"__TMP__")
    * --- Leggo il codice Iso della valuta di conto, se vuoto blocco l'esportazione 
    *     dei dati
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACODISO"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACODISO;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CodisoValuta = NVL(cp_ToDate(_read_.VACODISO),cp_NullValue(_read_.VACODISO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Empty(this.w_CodisoValuta)
      ah_ErrorMsg("Non � presente il codice ISO della valuta di conto")
      this.w_ErrorEsportazione = .t.
    else
      * --- Try
      local bErr_02A36F48
      bErr_02A36F48=bTrsErr
      this.Try_02A36F48()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_02A36F48
      * --- End
      if .not. fclose(this.w_Handle) Or this.w_ErrorEsportazione
        Namefile='"'+this.w_FileMovimenti+'"'
        Delete File &Namefile
      else
        ADDMSG("%1Esportazione movimenti avvenuta correttamente%2",this,SPACE(5)+chr(45)+chr(32),CHR(13))
      endif
    endif
  endproc
  proc Try_02A36F48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    select __tmp__
    go top
    scan
    this.w_TipoRec = "DF"
    this.w_Aztraazi = left( this.oParentObject.w_Dftraazi + space(10) , 10)
    this.w_Numeratore = left( Alltrim(this.oParentObject.w_Dfnummov) + space(4) ,4)
    this.w_CauCoge = iif(this.oParentObject.w_Dftrasca="S",Nvl(__Tmp__.Trcaudoc,Space(4)),Nvl(__Tmp__.Pecodice,Space(4)))
    this.w_CauCoge = left( Alltrim(this.w_CauCoge) + space(4) ,4)
    * --- Se � stato attivato il flag di trascodifica nei parametri, bisogna verificare se la 
    *     causale contabile � presente nelle tabella delle trascodifiche
    if this.oParentObject.w_Dftrasca="S" And Empty(this.w_CauCoge)
      ah_ErrorMsg("Causale contabile %1 non definita nell'archivio delle trascodifiche","",,Alltrim(Nvl(__Tmp__.Pecodice,Space(5))))
      this.w_ErrorEsportazione = .t.
      * --- Raise
      i_Error="Errore nell'esportazione"
      return
    endif
    this.w_IdOperaz = Space(4)
    this.w_DataReg = Dtoc(Cp_Todate(__Tmp__.Pndatreg))
    this.w_DataReg = left(this.w_DataReg,2) + substr(this.w_DataReg,4,2) + right(this.w_DataReg,4)
    this.w_DataOpe = Dtoc(iif(Empty(Cp_Todate(__Tmp__.Pndatdoc)),Cp_Todate(__Tmp__.Pndatreg),Cp_Todate(__Tmp__.Pndatdoc)))
    this.w_DataOpe = left(this.w_DataOpe,2) + substr(this.w_DataOpe,4,2) + right(this.w_DataOpe,4)
    this.w_PdcRbn = "-"+Alltrim(__Tmp__.Pecontro)
    this.w_PdcRbn = left( this.w_PdcRbn+Space(16),16)
    this.w_NumRif = Alltrim(Nvl(__Tmp__.Pemovban,Space(10)))
    this.w_NumRif = left( this.w_NumRif+Space(10),10)
    this.w_DivNeg = Space(5)
    this.w_CodIsoValuta = left( Alltrim(this.w_CodIsoValuta) + space(5) ,5)
    this.w_ImpDivNeg = __Tmp__.ImpDivNeg
    this.w_ImpDivNeg = iif(this.w_ImpDivNeg>=0,"+","-")+right(repl("0",11)+alltrim(Strtran(Str(Abs(this.w_ImpDivNeg),12,2),".",",")),15)
    this.w_Nummov = right(repl("0",10)+Alltrim(Str(Nvl(__Tmp__.Nummov,0),10,0)),10)
    this.w_Pnserial = __Tmp__.Pnserial
    this.w_Cprownum = __Tmp__.Cprownum
    this.w_DataEsecuzione = Date()
    if this.w_Nummov<>repl("0",10)
      * --- Write into DOCPNTRA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOCPNTRA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOCPNTRA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOCPNTRA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTCODUTE ="+cp_NullLink(cp_ToStrODBC(g_Codute),'DOCPNTRA','PTCODUTE');
        +",PTDATESE ="+cp_NullLink(cp_ToStrODBC(this.w_DataEsecuzione),'DOCPNTRA','PTDATESE');
        +",PTFLGTRA ="+cp_NullLink(cp_ToStrODBC("S"),'DOCPNTRA','PTFLGTRA');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_Pnserial);
            +" and PTROWNUM = "+cp_ToStrODBC(this.w_Cprownum);
               )
      else
        update (i_cTable) set;
            PTCODUTE = g_Codute;
            ,PTDATESE = this.w_DataEsecuzione;
            ,PTFLGTRA = "S";
            &i_ccchkf. ;
         where;
            PTSERIAL = this.w_Pnserial;
            and PTROWNUM = this.w_Cprownum;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      this.w_ProgrNumMov = Space(10)
      i_Connec = i_TableProp[this.DOCPNTRA_IDX,3]
      cp_NextTableProg(this,i_Connec,"PRSDF","i_Codazi,w_ProgrNumMov")
      this.w_Nummov = right(repl("0",10)+Alltrim(this.w_ProgrNumMov),10)
      * --- Insert into DOCPNTRA
      i_nConn=i_TableProp[this.DOCPNTRA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOCPNTRA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOCPNTRA_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PTSERIAL"+",PTROWNUM"+",PTCODUTE"+",PTDATESE"+",PTNUMMOV"+",PTFLGTRA"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_Pnserial),'DOCPNTRA','PTSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_Cprownum),'DOCPNTRA','PTROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(g_Codute),'DOCPNTRA','PTCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DataEsecuzione),'DOCPNTRA','PTDATESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ProgrNumMov),'DOCPNTRA','PTNUMMOV');
        +","+cp_NullLink(cp_ToStrODBC("S"),'DOCPNTRA','PTFLGTRA');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_Pnserial,'PTROWNUM',this.w_Cprownum,'PTCODUTE',g_Codute,'PTDATESE',this.w_DataEsecuzione,'PTNUMMOV',this.w_ProgrNumMov,'PTFLGTRA',"S")
        insert into (i_cTable) (PTSERIAL,PTROWNUM,PTCODUTE,PTDATESE,PTNUMMOV,PTFLGTRA &i_ccchkf. );
           values (;
             this.w_Pnserial;
             ,this.w_Cprownum;
             ,g_Codute;
             ,this.w_DataEsecuzione;
             ,this.w_ProgrNumMov;
             ,"S";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    this.w_ImpMov = __Tmp__.ImpMov
    this.w_ImpMov = iif(this.w_ImpMov>=0,"+","-")+right(repl("0",11)+alltrim(Strtran(Str(Abs(this.w_ImpMov),12,2),".",",")),15)
    this.w_TipoDoc = left( Alltrim(Nvl(__Tmp__.Petipdoc,Space(2))) + space(2) ,2)
    this.w_DataScadenza = Space(8)
    this.w_DataValuta = Space(8)
    this.w_Numdoc = "000000"
    this.w_Codpdc = "-"+Alltrim(__Tmp__.Pecodcon)
    this.w_CodPdc = left( this.w_CodPdc+Space(16),16)
    this.w_Note = Nvl(__Tmp__.Pndessup,Space(40))
    this.w_Note = left( this.w_Note+Space(40),40)
    this.w_Anvocfin = Nvl(__Tmp__.Ievocfin,Space(6))
    this.w_Anvocfin = left( Alltrim(this.w_Anvocfin) + space(6) ,6)
    this.w_NumRifCoge = Space(26)
    this.w_Sezione = Space(6)
    this.w_Record = this.w_TipoRec+this.w_Aztraazi+this.w_Numeratore+this.w_NumMov+this.w_CauCoge+this.w_IdOperaz+this.w_DataReg+this.w_DataOpe
    this.w_Record = this.w_Record+this.w_PdcRbn+this.w_NumRif+this.w_DivNeg+this.w_ImpDivNeg+this.w_CodisoValuta+this.w_ImpMov
    this.w_Record = this.w_Record+this.w_CodIsoValuta+this.w_Impmov+this.w_TipoDoc+this.w_DataScadenza+this.w_DataValuta+this.w_NumDoc
    this.w_Record = this.w_Record+this.w_CodPdc+this.w_Note+this.w_Anvocfin+this.w_NumRifCoge+this.w_Sezione
    * --- Scrittura record
    w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
    Endscan
    * --- commit
    cp_EndTrs(.t.)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='DOCFINPA'
    this.cWorkTables[2]='*TMP_PART2'
    this.cWorkTables[3]='*TMP_PART3'
    this.cWorkTables[4]='PAR_TITE'
    this.cWorkTables[5]='PRO_NUME'
    this.cWorkTables[6]='*ZOOMPART'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='DOCPNTRA'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
