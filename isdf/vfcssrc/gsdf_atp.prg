* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_atp                                                        *
*              Trascodifica categorie pagamento                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-03                                                      *
* Last revis.: 2014-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_atp"))

* --- Class definition
define class tgsdf_atp as StdForm
  Top    = 9
  Left   = 32

  * --- Standard Properties
  Width  = 348
  Height = 61+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-03-31"
  HelpContextID=175491991
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  DFTIPPAG_IDX = 0
  cFile = "DFTIPPAG"
  cKeySelect = "DFTIPPAG"
  cKeyWhere  = "DFTIPPAG=this.w_DFTIPPAG"
  cKeyWhereODBC = '"DFTIPPAG="+cp_ToStrODBC(this.w_DFTIPPAG)';

  cKeyWhereODBCqualified = '"DFTIPPAG.DFTIPPAG="+cp_ToStrODBC(this.w_DFTIPPAG)';

  cPrg = "gsdf_atp"
  cComment = "Trascodifica categorie pagamento"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFCODICE = space(2)
  w_DFTIPPAG = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DFTIPPAG','gsdf_atp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_atpPag1","gsdf_atp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Trascodifica categorie pagamento")
      .Pages(1).HelpContextID = 162279758
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDFTIPPAG_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DFTIPPAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DFTIPPAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DFTIPPAG_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DFTIPPAG = NVL(DFTIPPAG,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DFTIPPAG where DFTIPPAG=KeySet.DFTIPPAG
    *
    i_nConn = i_TableProp[this.DFTIPPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DFTIPPAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DFTIPPAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DFTIPPAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DFTIPPAG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFTIPPAG',this.w_DFTIPPAG  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DFCODICE = NVL(DFCODICE,space(2))
        .w_DFTIPPAG = NVL(DFTIPPAG,space(2))
        cp_LoadRecExtFlds(this,'DFTIPPAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DFCODICE = space(2)
      .w_DFTIPPAG = space(2)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DFTIPPAG = 'RD'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DFTIPPAG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDFCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oDFTIPPAG_1_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDFTIPPAG_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDFTIPPAG_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DFTIPPAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DFTIPPAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODICE,"DFCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFTIPPAG,"DFTIPPAG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DFTIPPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DFTIPPAG_IDX,2])
    i_lTable = "DFTIPPAG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DFTIPPAG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DFTIPPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DFTIPPAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DFTIPPAG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DFTIPPAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DFTIPPAG')
        i_extval=cp_InsertValODBCExtFlds(this,'DFTIPPAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DFCODICE,DFTIPPAG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DFCODICE)+;
                  ","+cp_ToStrODBC(this.w_DFTIPPAG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DFTIPPAG')
        i_extval=cp_InsertValVFPExtFlds(this,'DFTIPPAG')
        cp_CheckDeletedKey(i_cTable,0,'DFTIPPAG',this.w_DFTIPPAG)
        INSERT INTO (i_cTable);
              (DFCODICE,DFTIPPAG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DFCODICE;
                  ,this.w_DFTIPPAG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DFTIPPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DFTIPPAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DFTIPPAG_IDX,i_nConn)
      *
      * update DFTIPPAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DFTIPPAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DFCODICE="+cp_ToStrODBC(this.w_DFCODICE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DFTIPPAG')
        i_cWhere = cp_PKFox(i_cTable  ,'DFTIPPAG',this.w_DFTIPPAG  )
        UPDATE (i_cTable) SET;
              DFCODICE=this.w_DFCODICE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DFTIPPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DFTIPPAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DFTIPPAG_IDX,i_nConn)
      *
      * delete DFTIPPAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DFTIPPAG',this.w_DFTIPPAG  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DFTIPPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DFTIPPAG_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDFCODICE_1_1.value==this.w_DFCODICE)
      this.oPgFrm.Page1.oPag.oDFCODICE_1_1.value=this.w_DFCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDFTIPPAG_1_3.RadioValue()==this.w_DFTIPPAG)
      this.oPgFrm.Page1.oPag.oDFTIPPAG_1_3.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DFTIPPAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DFTIPPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFTIPPAG_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DFTIPPAG)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdf_atpPag1 as StdContainer
  Width  = 344
  height = 61
  stdWidth  = 344
  stdheight = 61
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFCODICE_1_1 as StdField with uid="SWOQGLEGQK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DFCODICE", cQueryName = "DFCODICE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Tipo pagamento Docfinance",;
    HelpContextID = 133609861,;
   bGlobalFont=.t.,;
    Height=21, Width=22, Left=147, Top=33, InputMask=replicate('X',2)


  add object oDFTIPPAG_1_3 as StdCombo with uid="EAPNAUUVWS",rtseq=2,rtrep=.f.,left=147,top=5,width=181,height=22;
    , ToolTipText = "Categoria pagamento di adhoc";
    , HelpContextID = 3910019;
    , cFormVar="w_DFTIPPAG",RowSource=""+"Anticipazioni /M.AV.,"+"Bonifico,"+"Cambiale/tratta,"+"R.I.D.,"+"Ric.bancaria/Ri.Ba.,"+"Rimessa diretta", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oDFTIPPAG_1_3.RadioValue()
    return(iif(this.value =1,'MA',;
    iif(this.value =2,'BO',;
    iif(this.value =3,'CA',;
    iif(this.value =4,'RI',;
    iif(this.value =5,'RB',;
    iif(this.value =6,'RD',;
    space(2))))))))
  endfunc
  func oDFTIPPAG_1_3.GetRadio()
    this.Parent.oContained.w_DFTIPPAG = this.RadioValue()
    return .t.
  endfunc

  func oDFTIPPAG_1_3.SetRadio()
    this.Parent.oContained.w_DFTIPPAG=trim(this.Parent.oContained.w_DFTIPPAG)
    this.value = ;
      iif(this.Parent.oContained.w_DFTIPPAG=='MA',1,;
      iif(this.Parent.oContained.w_DFTIPPAG=='BO',2,;
      iif(this.Parent.oContained.w_DFTIPPAG=='CA',3,;
      iif(this.Parent.oContained.w_DFTIPPAG=='RI',4,;
      iif(this.Parent.oContained.w_DFTIPPAG=='RB',5,;
      iif(this.Parent.oContained.w_DFTIPPAG=='RD',6,;
      0))))))
  endfunc

  add object oStr_1_2 as StdString with uid="LMZZSVFKDJ",Visible=.t., Left=16, Top=9,;
    Alignment=1, Width=129, Height=18,;
    Caption="Categoria pagamento: "  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="CDAMMQVISS",Visible=.t., Left=10, Top=37,;
    Alignment=1, Width=135, Height=18,;
    Caption="Trascodifica Docfinance:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_atp','DFTIPPAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFTIPPAG=DFTIPPAG.DFTIPPAG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
