* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_kmt                                                        *
*              Importazione da DocFinance                                      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-14                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_kmt",oParentObject))

* --- Class definition
define class tgsdf_kmt as StdForm
  Top    = 2
  Left   = 14

  * --- Standard Properties
  Width  = 758
  Height = 577
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-14"
  HelpContextID=199898217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  DOCFINPA_IDX = 0
  cPrg = "gsdf_kmt"
  cComment = "Importazione da DocFinance"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_PATHORIG = space(150)
  w_DFTRAAZI = space(10)
  w_PATH = space(200)
  o_PATH = space(200)
  w_SOVRASCRIVI = space(10)
  w_FLTLOG = space(1)
  w_Msg = space(0)
  w_TIPO = space(1)
  w_PrintLog = .F.
  w_TIPOIMPORT = space(10)
  w_CPROWNUM = space(10)
  o_CPROWNUM = space(10)
  w_DFCODAZI = space(10)
  w_DFNUMERA = space(4)
  w_DF__ANNO = space(4)
  w_DFMOVIME = space(10)
  w_DFDATREG = ctod('  /  /  ')
  w_DFCONTRO = space(22)
  w_DFPIANOC = space(16)
  w_ERRORLOG = space(0)
  w_DFSERTES = space(10)
  w_DFFLGDIS = space(10)
  w_DFTRASCA = space(1)
  w_DFFLPROV = space(1)
  w_BACKUP = space(200)
  w_DFNOMFIL = space(50)
  w_DFCHIAVE = space(10)
  w_ZOOMMOV = .NULL.
  w_ZOOMDET = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsdf_kmt
  oldDFCODAZI=''
  oldDFNUMERA=''
  oldDF__ANNO=''
  oldDFMOVIME=''
  oldDFDATREG=ctod('  /  /    ')
  oldDFCONTRO=''
  oldDFPIANOC=''
  oldDFCHIAVE=''
  w_ErrorLog1=' '
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_kmtPag1","gsdf_kmt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATH_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMMOV = this.oPgFrm.Pages(1).oPag.ZOOMMOV
    this.w_ZOOMDET = this.oPgFrm.Pages(1).oPag.ZOOMDET
    DoDefault()
    proc Destroy()
      this.w_ZOOMMOV = .NULL.
      this.w_ZOOMDET = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='DOCFINPA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PATHORIG=space(150)
      .w_DFTRAAZI=space(10)
      .w_PATH=space(200)
      .w_SOVRASCRIVI=space(10)
      .w_FLTLOG=space(1)
      .w_Msg=space(0)
      .w_TIPO=space(1)
      .w_PrintLog=.f.
      .w_TIPOIMPORT=space(10)
      .w_CPROWNUM=space(10)
      .w_DFCODAZI=space(10)
      .w_DFNUMERA=space(4)
      .w_DF__ANNO=space(4)
      .w_DFMOVIME=space(10)
      .w_DFDATREG=ctod("  /  /  ")
      .w_DFCONTRO=space(22)
      .w_DFPIANOC=space(16)
      .w_ERRORLOG=space(0)
      .w_DFSERTES=space(10)
      .w_DFFLGDIS=space(10)
      .w_DFTRASCA=space(1)
      .w_DFFLPROV=space(1)
      .w_BACKUP=space(200)
      .w_DFNOMFIL=space(50)
      .w_DFCHIAVE=space(10)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,3,.f.)
        .w_PATH = ADDBS(.w_PATHORIG) + alltrim(.w_DFNOMFIL)
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .w_SOVRASCRIVI = 'N'
        .w_FLTLOG = 'T'
          .DoRTCalc(7,7,.f.)
        .w_TIPO = 'N'
          .DoRTCalc(9,9,.f.)
        .w_TIPOIMPORT = 'DAFILE'
      .oPgFrm.Page1.oPag.ZOOMMOV.Calculate(.w_TIPO)
      .oPgFrm.Page1.oPag.ZOOMDET.Calculate(.w_TIPO)
        .w_CPROWNUM = .w_ZOOMDET.getVar('CPROWNUM')
        .w_DFCODAZI = .w_ZOOMMOV.getVar('DFCODAZI')
        .w_DFNUMERA = .w_ZOOMMOV.getVar('DFNUMERA')
        .w_DF__ANNO = .w_ZOOMMOV.getVar('DF__ANNO')
        .w_DFMOVIME = .w_ZOOMMOV.getVar('DFMOVIME')
        .w_DFDATREG = .w_ZOOMMOV.getVar('DFDATREG')
        .w_DFCONTRO = .w_ZOOMMOV.getVar('DFCONTRO')
        .w_DFPIANOC = .w_ZOOMMOV.getVar('DFPIANOC')
        .w_ERRORLOG = .w_ZOOMDET.getVar('DFERRLOG')
        .w_DFSERTES = .w_ZOOMMOV.getVar('DFSERTES')
        .w_DFFLGDIS = .w_ZOOMMOV.getVar('DFFLGDIS')
          .DoRTCalc(22,25,.f.)
        .w_DFCHIAVE = .w_ZOOMMOV.getVar('DFCHIAVE')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .DoRTCalc(2,4,.t.)
        if .o_PATH<>.w_PATH
            .w_SOVRASCRIVI = 'N'
        endif
        .oPgFrm.Page1.oPag.ZOOMMOV.Calculate(.w_TIPO)
        .oPgFrm.Page1.oPag.ZOOMDET.Calculate(.w_TIPO)
        .DoRTCalc(6,10,.t.)
            .w_CPROWNUM = .w_ZOOMDET.getVar('CPROWNUM')
            .w_DFCODAZI = .w_ZOOMMOV.getVar('DFCODAZI')
            .w_DFNUMERA = .w_ZOOMMOV.getVar('DFNUMERA')
            .w_DF__ANNO = .w_ZOOMMOV.getVar('DF__ANNO')
            .w_DFMOVIME = .w_ZOOMMOV.getVar('DFMOVIME')
            .w_DFDATREG = .w_ZOOMMOV.getVar('DFDATREG')
            .w_DFCONTRO = .w_ZOOMMOV.getVar('DFCONTRO')
            .w_DFPIANOC = .w_ZOOMMOV.getVar('DFPIANOC')
        if .o_CPROWNUM<>.w_CPROWNUM
            .w_ERRORLOG = .w_ZOOMDET.getVar('DFERRLOG')
        endif
            .w_DFSERTES = .w_ZOOMMOV.getVar('DFSERTES')
            .w_DFFLGDIS = .w_ZOOMMOV.getVar('DFFLGDIS')
        .DoRTCalc(22,25,.t.)
            .w_DFCHIAVE = .w_ZOOMMOV.getVar('DFCHIAVE')
        * --- Area Manuale = Calculate
        * --- gsdf_kmt
        ** Al variare delle righe di testata selezionate si aggiorna lo zoom
        IF this.oldDFCODAZI <> this.w_DFCODAZI OR this.oldDFNUMERA <> this.w_DFNUMERA OR this.oldDF__ANNO <> this.w_DF__ANNO OR this.oldDFMOVIME <> this.w_DFMOVIME OR this.oldDFDATREG <> this.w_DFDATREG OR this.oldDFCONTRO <> this.w_DFCONTRO OR this.oldDFPIANOC <> this.w_DFPIANOC  OR this.oldDFCHIAVE <> this.w_DFCHIAVE 
        this.oldDFCODAZI= this.w_DFCODAZI
        this.oldDFNUMERA = this.w_DFNUMERA
        this.oldDF__ANNO = this.w_DF__ANNO
        this.oldDFMOVIME = this.w_DFMOVIME 
        this.oldDFDATREG = this.w_DFDATREG
        this.oldDFCONTRO = this.w_DFCONTRO
        this.oldDFPIANOC = this.w_DFPIANOC 
        this.oldDFCHIAVE = this.w_DFCHIAVE
        this.notifyevent("Aggiornadett")
        this.w_ERRORLOG = this.w_ZOOMDET.getVar('DFERRLOG')
        ENDIF
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.ZOOMMOV.Calculate(.w_TIPO)
        .oPgFrm.Page1.oPag.ZOOMDET.Calculate(.w_TIPO)
    endwith
  return

  proc Calculate_OWVKMRGYWQ()
    with this
          * --- Stampa Log
          .w_ErrorLog1 = CreateObject('Ah_ErrorLog')
          .w_PrintLog = .w_ErrorLog1.AddMsgLog(.w_Msg)
          .w_PrintLog = .w_ErrorLog1.PrintLog(this,'Segnalazioni in fase di importazione',.f.,' ')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSOVRASCRIVI_1_7.enabled = this.oPgFrm.Page1.oPag.oSOVRASCRIVI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_27.visible=!this.oPgFrm.Page1.oPag.oBtn_1_27.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_33.visible=!this.oPgFrm.Page1.oPag.oBtn_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_37.visible=!this.oPgFrm.Page1.oPag.oBtn_1_37.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_42.visible=!this.oPgFrm.Page1.oPag.oBtn_1_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
        if lower(cEvent)==lower("Stampa")
          .Calculate_OWVKMRGYWQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZOOMMOV.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMDET.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
    i_lTable = "DOCFINPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2], .t., this.DOCFINPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFCODAZI,DFTRAAZI,DFTRASCA,DFFLPROV,DFPERIMP,DFPERBCK,DFNOMFIL";
                   +" from "+i_cTable+" "+i_lTable+" where DFCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFCODAZI',this.w_CODAZI)
            select DFCODAZI,DFTRAAZI,DFTRASCA,DFFLPROV,DFPERIMP,DFPERBCK,DFNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.DFCODAZI,space(5))
      this.w_DFTRAAZI = NVL(_Link_.DFTRAAZI,space(10))
      this.w_DFTRASCA = NVL(_Link_.DFTRASCA,space(1))
      this.w_DFFLPROV = NVL(_Link_.DFFLPROV,space(1))
      this.w_PATHORIG = NVL(_Link_.DFPERIMP,space(150))
      this.w_BACKUP = NVL(_Link_.DFPERBCK,space(200))
      this.w_DFNOMFIL = NVL(_Link_.DFNOMFIL,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_DFTRAAZI = space(10)
      this.w_DFTRASCA = space(1)
      this.w_DFFLPROV = space(1)
      this.w_PATHORIG = space(150)
      this.w_BACKUP = space(200)
      this.w_DFNOMFIL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])+'\'+cp_ToStr(_Link_.DFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DOCFINPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPATH_1_5.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_5.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oSOVRASCRIVI_1_7.RadioValue()==this.w_SOVRASCRIVI)
      this.oPgFrm.Page1.oPag.oSOVRASCRIVI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_9.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_9.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_10.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oERRORLOG_1_26.value==this.w_ERRORLOG)
      this.oPgFrm.Page1.oPag.oERRORLOG_1_26.value=this.w_ERRORLOG
    endif
    if not(this.oPgFrm.Page1.oPag.oBACKUP_1_41.value==this.w_BACKUP)
      this.oPgFrm.Page1.oPag.oBACKUP_1_41.value=this.w_BACKUP
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PATH = this.w_PATH
    this.o_CPROWNUM = this.w_CPROWNUM
    return

enddefine

* --- Define pages as container
define class tgsdf_kmtPag1 as StdContainer
  Width  = 754
  height = 577
  stdWidth  = 754
  stdheight = 577
  resizeXpos=284
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPATH_1_5 as StdField with uid="YJTBDAQENK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path+nome file",;
    HelpContextID = 194817546,;
   bGlobalFont=.t.,;
    Height=21, Width=620, Left=97, Top=7, InputMask=replicate('X',200)


  add object oObj_1_6 as cp_askfile with uid="OBMZWDHYCM",left=721, top=9, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_PATH",;
    nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 199697194

  add object oSOVRASCRIVI_1_7 as StdCheck with uid="FUTZQUZLIN",rtseq=5,rtrep=.f.,left=28, top=526, caption="Sovrascrivi la tabella di preelaborazione",;
    ToolTipText = "Sovrascrive la tabella di preelaborazione se i dati sono gi� presenti e non � stato generato il movimento primanota o la distinta",;
    HelpContextID = 193096456,;
    cFormVar="w_SOVRASCRIVI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSOVRASCRIVI_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oSOVRASCRIVI_1_7.GetRadio()
    this.Parent.oContained.w_SOVRASCRIVI = this.RadioValue()
    return .t.
  endfunc

  func oSOVRASCRIVI_1_7.SetRadio()
    this.Parent.oContained.w_SOVRASCRIVI=trim(this.Parent.oContained.w_SOVRASCRIVI)
    this.value = ;
      iif(this.Parent.oContained.w_SOVRASCRIVI=='S',1,;
      0)
  endfunc

  func oSOVRASCRIVI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not EMPTY (.w_PATH))
    endwith
   endif
  endfunc

  add object oMsg_1_9 as StdMemo with uid="ATBBCHAJSX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 199445562,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=114, Width=744, Left=6, Top=59, tabstop = .f., readonly = .t.


  add object oTIPO_1_10 as StdCombo with uid="FOEEOVNRIP",rtseq=8,rtrep=.f.,left=5,top=195,width=180,height=21;
    , ToolTipText = "Filtro sui movimenti da visualizzare nell' elenco";
    , HelpContextID = 194373066;
    , cFormVar="w_TIPO",RowSource=""+"Da generare,"+"Generati,"+"Con errori,"+"Tutti", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPO_1_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oTIPO_1_10.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_10.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='N',1,;
      iif(this.Parent.oContained.w_TIPO=='S',2,;
      iif(this.Parent.oContained.w_TIPO=='E',3,;
      iif(this.Parent.oContained.w_TIPO=='T',4,;
      0))))
  endfunc


  add object oBtn_1_11 as StdButton with uid="PSTUCDPWKC",left=699, top=175, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la query";
    , HelpContextID = 245459478;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      this.parent.oContained.NotifyEvent("Aggiorna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMMOV as cp_szoombox with uid="TIHUYNVHMX",left=-4, top=214, width=423,height=308,;
    caption='ZOOMMOV',;
   bGlobalFont=.t.,;
    cTable="DOCFINIM",cZoomFile="GSDF_KMT",bOptions=.f.,bQueryOnLoad=.t.,bQueryOnDblClick=.t.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",bNoMenuHeaderProperty=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Aggiorna,Init",;
    nPag=1;
    , HelpContextID = 137891990


  add object ZOOMDET as cp_zoombox with uid="DLWKNKWEPB",left=406, top=213, width=367,height=174,;
    caption='ZOOMDET',;
   bGlobalFont=.t.,;
    bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cTable="DOCFINIM",cZoomFile="GSDFDKMT",bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Aggiorna,Init , Aggiornadett",;
    nPag=1;
    , HelpContextID = 229118102

  add object oERRORLOG_1_26 as StdMemo with uid="ULABKCMNAK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ERRORLOG", cQueryName = "ERRORLOG",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 175488371,;
   bGlobalFont=.t.,;
    Height=133, Width=335, Left=416, Top=390, readonly = .t.


  add object oBtn_1_27 as StdButton with uid="JNNBOLOWHQ",left=434, top=528, width=48,height=45,;
    CpPicture="bmp\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il movimento associato";
    , HelpContextID = 11300015;
    , tabstop=.f., Caption='\<Tesoreria';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSAR_BZT(this.Parent.oContained,.w_DFSERTES,"T")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DFSERTES) )
      endwith
    endif
  endfunc

  func oBtn_1_27.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DFSERTES) OR g_APPLICATION="ADHOC REVOLUTION")
     endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="KWFWCRWNHO",left=487, top=528, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 159483158;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        .NotifyEvent('Stampa')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_Msg))
      endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="MBKLQHSULX",left=540, top=528, width=48,height=45,;
    CpPicture="bmp\auto.ico", caption="", nPag=1;
    , ToolTipText = "Premere per caricare i movimenti di DocFinance";
    , HelpContextID = 159483158;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSDF_BMT (this.Parent.oContained,"DATABELLA" , "S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as StdButton with uid="BLBTTYVQBU",left=593, top=528, width=48,height=45,;
    CpPicture="bmp\ScarGen.ico", caption="", nPag=1;
    , ToolTipText = "Permette di importare nella tabella di preelaborazione e contestualmente generare il movimento di primanota o la distinta";
    , HelpContextID = 159483158;
    , caption='I\<mp./gen.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSDF_BMT (this.Parent.oContained,"DAFILE" , "S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty (.w_PATH) OR NOT FILE (.w_PATH))
     endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="ZSONRLTWLJ",left=699, top=528, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 159483158;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="VPJECOCWYU",left=383, top=528, width=48,height=45,;
    CpPicture="bmp\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il movimento Docfinance generato";
    , HelpContextID = 150105910;
    , tabstop=.f., Caption='\<DocFin.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSDF_BDM (this.Parent.oContained,"DOCF")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DFCODAZI))
      endwith
    endif
  endfunc

  func oBtn_1_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DFCODAZI))
     endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="LMZSFFCJMH",left=434, top=528, width=48,height=45,;
    CpPicture="bmp\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione contabile generata";
    , HelpContextID = 214556406;
    , tabstop=.f., Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSDF_BDM (this.Parent.oContained,"MOV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DFSERTES) )
      endwith
    endif
  endfunc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DFSERTES) OR g_APPLICATION<>"ADHOC REVOLUTION" OR .w_DFFLGDIS='D')
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="QYJKFEQSEB",left=434, top=528, width=48,height=45,;
    CpPicture="bmp\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la distinta generata";
    , HelpContextID = 214556406;
    , tabstop=.f., Caption='\<Distinta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSDF_BDM (this.Parent.oContained,"MOV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DFSERTES) )
      endwith
    endif
  endfunc

  func oBtn_1_37.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DFSERTES) OR g_APPLICATION<>"ADHOC REVOLUTION" OR .w_DFFLGDIS<>'D')
     endwith
    endif
  endfunc

  add object oBACKUP_1_41 as StdField with uid="HPIXTOLOUP",rtseq=24,rtrep=.f.,;
    cFormVar = "w_BACKUP", cQueryName = "BACKUP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Directory di salvataggio delle copie di backup dei file da DocFinance",;
    HelpContextID = 162873622,;
   bGlobalFont=.t.,;
    Height=21, Width=620, Left=97, Top=32, InputMask=replicate('X',200)


  add object oBtn_1_42 as StdButton with uid="FCTKAGOXAT",left=646, top=528, width=48,height=45,;
    CpPicture="bmp\SCARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Permette di importare i dati nella tabella di preelaborazione";
    , HelpContextID = 159483158;
    , caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSDF_BMT (this.Parent.oContained,"DAFILE" , "N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty (.w_PATH)  OR NOT FILE (.w_PATH))
     endwith
    endif
  endfunc

  add object oStr_1_4 as StdString with uid="SHCDKROQHP",Visible=.t., Left=33, Top=6,;
    Alignment=1, Width=61, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="WSHZFRXYSI",Visible=.t., Left=255, Top=206,;
    Alignment=0, Width=175, Height=17,;
    Caption="I movimenti in rosso sono errati"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="FQYGHXUPTQ",Visible=.t., Left=5, Top=180,;
    Alignment=0, Width=77, Height=18,;
    Caption="Tipologia dati"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="VFLOXWYINJ",Visible=.t., Left=10, Top=31,;
    Alignment=1, Width=84, Height=18,;
    Caption="Path di backup:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_kmt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
