* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_kes                                                        *
*              Esportazione a DocFinance                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-06-06                                                      *
* Last revis.: 2017-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_kes",oParentObject))

* --- Class definition
define class tgsdf_kes as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 797
  Height = 445+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-03-17"
  HelpContextID=65680489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  _IDX = 0
  DOCFINPA_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsdf_kes"
  cComment = "Esportazione a DocFinance"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(10)
  w_AZFLSEVA = space(1)
  w_CODAZI = space(5)
  w_DFTRAAZI = space(10)
  w_DFPERESP = space(150)
  w_DFFILPDC = space(50)
  w_DFFILSCA = space(50)
  w_DFFILMOV = space(50)
  w_DFRATSCA = space(2)
  w_DFRATSDT = space(2)
  w_DFVOCFCL = space(6)
  w_DFVOCFFO = space(6)
  w_DFVOCFGE = space(6)
  w_DFESPACC = space(1)
  w_DF_LINEA = space(4)
  w_DFDIS_RI = space(2)
  w_DFDIS_RA = space(2)
  w_DFTRASCA = space(1)
  w_DFEXPMOV = space(1)
  w_PATHFILE = space(150)
  w_NOMEFILE = space(50)
  w_NOMEFILESCAD = space(50)
  w_NOMEFILEMOV = space(50)
  w_PDC = space(1)
  w_SCADENZIARIO = space(1)
  w_EXPCORRI = space(1)
  w_Msg = space(0)
  w_DFTIPEXP = space(1)
  w_DFDATUEX = ctod('  /  /  ')
  w_DFNUMMOV = space(4)
  w_DFVOCEFF = space(6)
  w_DFCORRPC = space(1)
  w_DFCORRSC = space(1)
  w_DFTRATIP = space(1)
  w_DFCASFLO = space(1)
  w_CFDATCRE = ctod('  /  /  ')
  w_FLSEVA = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_kesPag1","gsdf_kes",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Esportazione DocFinance")
      .Pages(2).addobject("oPag","tgsdf_kesPag2","gsdf_kes",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Messaggi di elaborazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATHFILE_1_22
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DOCFINPA'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do Gsdf_Bes with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(10)
      .w_AZFLSEVA=space(1)
      .w_CODAZI=space(5)
      .w_DFTRAAZI=space(10)
      .w_DFPERESP=space(150)
      .w_DFFILPDC=space(50)
      .w_DFFILSCA=space(50)
      .w_DFFILMOV=space(50)
      .w_DFRATSCA=space(2)
      .w_DFRATSDT=space(2)
      .w_DFVOCFCL=space(6)
      .w_DFVOCFFO=space(6)
      .w_DFVOCFGE=space(6)
      .w_DFESPACC=space(1)
      .w_DF_LINEA=space(4)
      .w_DFDIS_RI=space(2)
      .w_DFDIS_RA=space(2)
      .w_DFTRASCA=space(1)
      .w_DFEXPMOV=space(1)
      .w_PATHFILE=space(150)
      .w_NOMEFILE=space(50)
      .w_NOMEFILESCAD=space(50)
      .w_NOMEFILEMOV=space(50)
      .w_PDC=space(1)
      .w_SCADENZIARIO=space(1)
      .w_EXPCORRI=space(1)
      .w_Msg=space(0)
      .w_DFTIPEXP=space(1)
      .w_DFDATUEX=ctod("  /  /  ")
      .w_DFNUMMOV=space(4)
      .w_DFVOCEFF=space(6)
      .w_DFCORRPC=space(1)
      .w_DFCORRSC=space(1)
      .w_DFTRATIP=space(1)
      .w_DFCASFLO=space(1)
      .w_CFDATCRE=ctod("  /  /  ")
      .w_FLSEVA=space(1)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_2('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODAZI = i_Codazi
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_4('Full')
        endif
          .DoRTCalc(4,19,.f.)
        .w_PATHFILE = Alltrim(.w_Dfperesp)
        .w_NOMEFILE = Alltrim(.w_Dffilpdc)
        .w_NOMEFILESCAD = Alltrim(.w_Dffilsca)
        .w_NOMEFILEMOV = Alltrim(.w_Dffilmov)
        .w_PDC = 'S'
        .w_SCADENZIARIO = 'S'
        .w_EXPCORRI = .w_DFEXPMOV
          .DoRTCalc(27,36,.f.)
        .w_FLSEVA = .w_AZFLSEVA
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_2('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_4('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_MAXXHPSCJP()
    with this
          * --- Cash Flow (Evasione per data scadenza)
     if .w_DFCASFLO='E'
          GSDF_BDM(this;
              ,'RATEDOC';
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEXPCORRI_1_33.enabled = this.oPgFrm.Page1.oPag.oEXPCORRI_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDFTIPEXP_1_38.visible=!this.oPgFrm.Page1.oPag.oDFTIPEXP_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDFDATUEX_1_39.visible=!this.oPgFrm.Page1.oPag.oDFDATUEX_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oDFCASFLO_1_49.visible=!this.oPgFrm.Page1.oPag.oDFCASFLO_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oCFDATCRE_1_52.visible=!this.oPgFrm.Page1.oPag.oCFDATCRE_1_52.mHide()
    this.oPgFrm.Page1.oPag.oFLSEVA_1_53.visible=!this.oPgFrm.Page1.oPag.oFLSEVA_1_53.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_MAXXHPSCJP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLSEVA";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZFLSEVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_AZFLSEVA = NVL(_Link_.AZFLSEVA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_AZFLSEVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
    i_lTable = "DOCFINPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2], .t., this.DOCFINPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFCODAZI,DFTRAAZI,DFPERESP,DFFILPDC,DFFILSCA,DFRATSCA,DFRATSDT,DFVOCFCL,DFVOCFFO,DFESPACC,DFTIPEXP,DFDATUEX,DF_LINEA,DFDIS_RI,DFDIS_RA,DFTRASCA,DFFILMOV,DFVOCFGE,DFEXPMOV,DFNUMMOV,DFVOCEFF,DFCORRPC,DFCORRSC,DFTRATIP,DFCASFLO";
                   +" from "+i_cTable+" "+i_lTable+" where DFCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFCODAZI',this.w_CODAZI)
            select DFCODAZI,DFTRAAZI,DFPERESP,DFFILPDC,DFFILSCA,DFRATSCA,DFRATSDT,DFVOCFCL,DFVOCFFO,DFESPACC,DFTIPEXP,DFDATUEX,DF_LINEA,DFDIS_RI,DFDIS_RA,DFTRASCA,DFFILMOV,DFVOCFGE,DFEXPMOV,DFNUMMOV,DFVOCEFF,DFCORRPC,DFCORRSC,DFTRATIP,DFCASFLO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.DFCODAZI,space(5))
      this.w_DFTRAAZI = NVL(_Link_.DFTRAAZI,space(10))
      this.w_DFPERESP = NVL(_Link_.DFPERESP,space(150))
      this.w_DFFILPDC = NVL(_Link_.DFFILPDC,space(50))
      this.w_DFFILSCA = NVL(_Link_.DFFILSCA,space(50))
      this.w_DFRATSCA = NVL(_Link_.DFRATSCA,space(2))
      this.w_DFRATSDT = NVL(_Link_.DFRATSDT,space(2))
      this.w_DFVOCFCL = NVL(_Link_.DFVOCFCL,space(6))
      this.w_DFVOCFFO = NVL(_Link_.DFVOCFFO,space(6))
      this.w_DFESPACC = NVL(_Link_.DFESPACC,space(1))
      this.w_DFTIPEXP = NVL(_Link_.DFTIPEXP,space(1))
      this.w_DFDATUEX = NVL(cp_ToDate(_Link_.DFDATUEX),ctod("  /  /  "))
      this.w_DF_LINEA = NVL(_Link_.DF_LINEA,space(4))
      this.w_DFDIS_RI = NVL(_Link_.DFDIS_RI,space(2))
      this.w_DFDIS_RA = NVL(_Link_.DFDIS_RA,space(2))
      this.w_DFTRASCA = NVL(_Link_.DFTRASCA,space(1))
      this.w_DFFILMOV = NVL(_Link_.DFFILMOV,space(50))
      this.w_DFVOCFGE = NVL(_Link_.DFVOCFGE,space(6))
      this.w_DFEXPMOV = NVL(_Link_.DFEXPMOV,space(1))
      this.w_DFNUMMOV = NVL(_Link_.DFNUMMOV,space(4))
      this.w_DFVOCEFF = NVL(_Link_.DFVOCEFF,space(6))
      this.w_DFCORRPC = NVL(_Link_.DFCORRPC,space(1))
      this.w_DFCORRSC = NVL(_Link_.DFCORRSC,space(1))
      this.w_DFTRATIP = NVL(_Link_.DFTRATIP,space(1))
      this.w_DFCASFLO = NVL(_Link_.DFCASFLO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_DFTRAAZI = space(10)
      this.w_DFPERESP = space(150)
      this.w_DFFILPDC = space(50)
      this.w_DFFILSCA = space(50)
      this.w_DFRATSCA = space(2)
      this.w_DFRATSDT = space(2)
      this.w_DFVOCFCL = space(6)
      this.w_DFVOCFFO = space(6)
      this.w_DFESPACC = space(1)
      this.w_DFTIPEXP = space(1)
      this.w_DFDATUEX = ctod("  /  /  ")
      this.w_DF_LINEA = space(4)
      this.w_DFDIS_RI = space(2)
      this.w_DFDIS_RA = space(2)
      this.w_DFTRASCA = space(1)
      this.w_DFFILMOV = space(50)
      this.w_DFVOCFGE = space(6)
      this.w_DFEXPMOV = space(1)
      this.w_DFNUMMOV = space(4)
      this.w_DFVOCEFF = space(6)
      this.w_DFCORRPC = space(1)
      this.w_DFCORRSC = space(1)
      this.w_DFTRATIP = space(1)
      this.w_DFCASFLO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])+'\'+cp_ToStr(_Link_.DFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DOCFINPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDFTRAAZI_1_5.value==this.w_DFTRAAZI)
      this.oPgFrm.Page1.oPag.oDFTRAAZI_1_5.value=this.w_DFTRAAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oPATHFILE_1_22.value==this.w_PATHFILE)
      this.oPgFrm.Page1.oPag.oPATHFILE_1_22.value=this.w_PATHFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_24.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_24.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILESCAD_1_25.value==this.w_NOMEFILESCAD)
      this.oPgFrm.Page1.oPag.oNOMEFILESCAD_1_25.value=this.w_NOMEFILESCAD
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILEMOV_1_26.value==this.w_NOMEFILEMOV)
      this.oPgFrm.Page1.oPag.oNOMEFILEMOV_1_26.value=this.w_NOMEFILEMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oPDC_1_31.RadioValue()==this.w_PDC)
      this.oPgFrm.Page1.oPag.oPDC_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCADENZIARIO_1_32.RadioValue()==this.w_SCADENZIARIO)
      this.oPgFrm.Page1.oPag.oSCADENZIARIO_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEXPCORRI_1_33.RadioValue()==this.w_EXPCORRI)
      this.oPgFrm.Page1.oPag.oEXPCORRI_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMsg_2_1.value==this.w_Msg)
      this.oPgFrm.Page2.oPag.oMsg_2_1.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oDFTIPEXP_1_38.RadioValue()==this.w_DFTIPEXP)
      this.oPgFrm.Page1.oPag.oDFTIPEXP_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDATUEX_1_39.value==this.w_DFDATUEX)
      this.oPgFrm.Page1.oPag.oDFDATUEX_1_39.value=this.w_DFDATUEX
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCASFLO_1_49.RadioValue()==this.w_DFCASFLO)
      this.oPgFrm.Page1.oPag.oDFCASFLO_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDATCRE_1_52.value==this.w_CFDATCRE)
      this.oPgFrm.Page1.oPag.oCFDATCRE_1_52.value=this.w_CFDATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSEVA_1_53.RadioValue()==this.w_FLSEVA)
      this.oPgFrm.Page1.oPag.oFLSEVA_1_53.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdf_kesPag1 as StdContainer
  Width  = 793
  height = 445
  stdWidth  = 793
  stdheight = 445
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFTRAAZI_1_5 as StdField with uid="TGURFNISYK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DFTRAAZI", cQueryName = "DFTRAAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Indicazione dell'azienda da passare nel file di DocFinance",;
    HelpContextID = 243444097,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=218, Top=15, InputMask=replicate('X',10)

  add object oPATHFILE_1_22 as StdField with uid="GVCAIHPTDT",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PATHFILE", cQueryName = "PATHFILE",;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di esportazione file",;
    HelpContextID = 104639941,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=183, Top=85, InputMask=replicate('X',150)


  add object oBtn_1_23 as StdButton with uid="RUDQNHINUV",left=754, top=84, width=22,height=21,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 65479466;
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      with this.Parent.oContained
        .w_PATHFILE=left(getdir(IIF(EMPTY(.w_PATHFILE),sys(5)+sys(2003),.w_PATHFILE),"Percorso di destinazione")+space(150),150)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNOMEFILE_1_24 as StdField with uid="HVGDMYHCMO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file di esportazione del piano dei conti",;
    HelpContextID = 104861669,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=183, Top=125, InputMask=replicate('X',50)

  add object oNOMEFILESCAD_1_25 as StdField with uid="CMQVYYMSSO",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NOMEFILESCAD", cQueryName = "NOMEFILESCAD",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file di esportazione dello scadenzario",;
    HelpContextID = 100120501,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=183, Top=165, InputMask=replicate('X',50)

  add object oNOMEFILEMOV_1_26 as StdField with uid="VTXWYXVJHQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_NOMEFILEMOV", cQueryName = "NOMEFILEMOV",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file di esportazione dei movimenti",;
    HelpContextID = 104487957,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=183, Top=205, InputMask=replicate('X',50)

  add object oPDC_1_31 as StdCheck with uid="YFXHMVDMCJ",rtseq=24,rtrep=.f.,left=11, top=275, caption="Piano dei conti",;
    ToolTipText = "Se attivo, esporta il piano dei conti",;
    HelpContextID = 65387274,;
    cFormVar="w_PDC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDC_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPDC_1_31.GetRadio()
    this.Parent.oContained.w_PDC = this.RadioValue()
    return .t.
  endfunc

  func oPDC_1_31.SetRadio()
    this.Parent.oContained.w_PDC=trim(this.Parent.oContained.w_PDC)
    this.value = ;
      iif(this.Parent.oContained.w_PDC=='S',1,;
      0)
  endfunc

  add object oSCADENZIARIO_1_32 as StdCheck with uid="PYJGBLGEVL",rtseq=25,rtrep=.f.,left=11, top=326, caption="Scadenzario",;
    ToolTipText = "Se attivo, esporta lo scandenzario",;
    HelpContextID = 16643457,;
    cFormVar="w_SCADENZIARIO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCADENZIARIO_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSCADENZIARIO_1_32.GetRadio()
    this.Parent.oContained.w_SCADENZIARIO = this.RadioValue()
    return .t.
  endfunc

  func oSCADENZIARIO_1_32.SetRadio()
    this.Parent.oContained.w_SCADENZIARIO=trim(this.Parent.oContained.w_SCADENZIARIO)
    this.value = ;
      iif(this.Parent.oContained.w_SCADENZIARIO=='S',1,;
      0)
  endfunc

  add object oEXPCORRI_1_33 as StdCheck with uid="YAMKFOJEWA",rtseq=26,rtrep=.f.,left=12, top=397, caption="Movimenti",;
    ToolTipText = "Se attivo, esporta i movimenti di primanota",;
    HelpContextID = 55453839,;
    cFormVar="w_EXPCORRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXPCORRI_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEXPCORRI_1_33.GetRadio()
    this.Parent.oContained.w_EXPCORRI = this.RadioValue()
    return .t.
  endfunc

  func oEXPCORRI_1_33.SetRadio()
    this.Parent.oContained.w_EXPCORRI=trim(this.Parent.oContained.w_EXPCORRI)
    this.value = ;
      iif(this.Parent.oContained.w_EXPCORRI=='S',1,;
      0)
  endfunc

  func oEXPCORRI_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DFEXPMOV='S')
    endwith
   endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="QKYBLFAXXW",left=681, top=398, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 25265430;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        do GSDF_BES with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="UTJBWUDSTH",left=731, top=398, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 25265430;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oDFTIPEXP_1_38 as StdCombo with uid="PQQGGFCPTB",rtseq=28,rtrep=.f.,left=370,top=272,width=106,height=22, enabled=.f.;
    , HelpContextID = 107239046;
    , cFormVar="w_DFTIPEXP",RowSource=""+"Tutti,"+"Variati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDFTIPEXP_1_38.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oDFTIPEXP_1_38.GetRadio()
    this.Parent.oContained.w_DFTIPEXP = this.RadioValue()
    return .t.
  endfunc

  func oDFTIPEXP_1_38.SetRadio()
    this.Parent.oContained.w_DFTIPEXP=trim(this.Parent.oContained.w_DFTIPEXP)
    this.value = ;
      iif(this.Parent.oContained.w_DFTIPEXP=='T',1,;
      iif(this.Parent.oContained.w_DFTIPEXP=='V',2,;
      0))
  endfunc

  func oDFTIPEXP_1_38.mHide()
    with this.Parent.oContained
      return (.w_PDC<>'S')
    endwith
  endfunc

  add object oDFDATUEX_1_39 as StdField with uid="FJQPWCSSWJ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DFDATUEX", cQueryName = "DFDATUEX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 110843534,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=568, Top=275

  func oDFDATUEX_1_39.mHide()
    with this.Parent.oContained
      return (.w_DFTIPEXP='T' OR .w_PDC<>'S')
    endwith
  endfunc


  add object oDFCASFLO_1_49 as StdCombo with uid="XDHTLJOQWX",rtseq=35,rtrep=.f.,left=370,top=321,width=213,height=22, enabled=.f.;
    , HelpContextID = 141867387;
    , cFormVar="w_DFCASFLO",RowSource=""+"Standard (evasione proporzionale),"+"Evasione per data scadenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDFCASFLO_1_49.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oDFCASFLO_1_49.GetRadio()
    this.Parent.oContained.w_DFCASFLO = this.RadioValue()
    return .t.
  endfunc

  func oDFCASFLO_1_49.SetRadio()
    this.Parent.oContained.w_DFCASFLO=trim(this.Parent.oContained.w_DFCASFLO)
    this.value = ;
      iif(this.Parent.oContained.w_DFCASFLO=='S',1,;
      iif(this.Parent.oContained.w_DFCASFLO=='E',2,;
      0))
  endfunc

  func oDFCASFLO_1_49.mHide()
    with this.Parent.oContained
      return (.w_SCADENZIARIO<>'S')
    endwith
  endfunc

  add object oCFDATCRE_1_52 as StdField with uid="DUIGBXQLXK",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CFDATCRE", cQueryName = "CFDATCRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 77289067,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=370, Top=357

  func oCFDATCRE_1_52.mHide()
    with this.Parent.oContained
      return (.w_DFCASFLO='S' OR .w_SCADENZIARIO<>'S')
    endwith
  endfunc

  add object oFLSEVA_1_53 as StdCheck with uid="UWFTWYUGVD",rtseq=37,rtrep=.f.,left=475, top=362, caption="Evasione per data scadenza", enabled=.f.,;
    HelpContextID = 222278570,;
    cFormVar="w_FLSEVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSEVA_1_53.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSEVA_1_53.GetRadio()
    this.Parent.oContained.w_FLSEVA = this.RadioValue()
    return .t.
  endfunc

  func oFLSEVA_1_53.SetRadio()
    this.Parent.oContained.w_FLSEVA=trim(this.Parent.oContained.w_FLSEVA)
    this.value = ;
      iif(this.Parent.oContained.w_FLSEVA=='S',1,;
      0)
  endfunc

  func oFLSEVA_1_53.mHide()
    with this.Parent.oContained
      return (.w_DFCASFLO='S' OR .w_SCADENZIARIO<>'S')
    endwith
  endfunc

  add object oStr_1_1 as StdString with uid="LMFMTENFUN",Visible=.t., Left=101, Top=17,;
    Alignment=1, Width=112, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="KQYBAHEHOI",Visible=.t., Left=5, Top=89,;
    Alignment=1, Width=174, Height=18,;
    Caption="Path di esportazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="IQWRGOZWZA",Visible=.t., Left=3, Top=58,;
    Alignment=0, Width=170, Height=18,;
    Caption="Parametri di esportazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="KPEPCEHCCV",Visible=.t., Left=3, Top=242,;
    Alignment=0, Width=154, Height=18,;
    Caption="Tracciati esportati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="BGYCKVKOCA",Visible=.t., Left=26, Top=127,;
    Alignment=1, Width=153, Height=18,;
    Caption="Nome file piano dei conti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="XNBMCSCMQW",Visible=.t., Left=26, Top=166,;
    Alignment=1, Width=153, Height=18,;
    Caption="Nome file scadenzario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="PXGLAYOKCR",Visible=.t., Left=125, Top=278,;
    Alignment=1, Width=240, Height=18,;
    Caption="Tipo esportazione piano dei conti:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_PDC<>'S')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="YQBJYUWXQG",Visible=.t., Left=493, Top=278,;
    Alignment=1, Width=71, Height=18,;
    Caption="alla data:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_DFTIPEXP='T' Or .w_PDC<>'S')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="AKTJAJYFUA",Visible=.t., Left=26, Top=207,;
    Alignment=1, Width=153, Height=18,;
    Caption="Nome file movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="MNHIKVCAZA",Visible=.t., Left=222, Top=323,;
    Alignment=1, Width=143, Height=18,;
    Caption="Calcolo rate documenti:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_SCADENZIARIO<>'S')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="TRIBKYHAXY",Visible=.t., Left=126, Top=360,;
    Alignment=1, Width=239, Height=18,;
    Caption="Ultima data elaborazione storico:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_DFCASFLO='S' OR .w_SCADENZIARIO<>'S')
    endwith
  endfunc

  add object oBox_1_27 as StdBox with uid="GHOAAQEACX",left=2, top=75, width=751,height=2

  add object oBox_1_28 as StdBox with uid="EOWYSNVWMC",left=0, top=260, width=751,height=2
enddefine
define class tgsdf_kesPag2 as StdContainer
  Width  = 793
  height = 445
  stdWidth  = 793
  stdheight = 445
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_2_1 as StdMemo with uid="ATBBCHAJSX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 65227834,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=376, Width=785, Left=1, Top=8, tabstop = .f., readonly = .t.


  add object oBtn_2_2 as StdButton with uid="CTCPIKKCGT",left=738, top=398, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58363066;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_kes','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
