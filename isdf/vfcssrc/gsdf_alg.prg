* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_alg                                                        *
*              Log importazione DocFinance                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-12                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_alg"))

* --- Class definition
define class tgsdf_alg as StdForm
  Top    = 3
  Left   = 11

  * --- Standard Properties
  Width  = 785
  Height = 539+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-14"
  HelpContextID=227161193
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  DOCFINLG_IDX = 0
  cFile = "DOCFINLG"
  cKeySelect = "DFCSERIA,DFCODAZI"
  cKeyWhere  = "DFCSERIA=this.w_DFCSERIA and DFCODAZI=this.w_DFCODAZI"
  cKeyWhereODBC = '"DFCSERIA="+cp_ToStrODBC(this.w_DFCSERIA)';
      +'+" and DFCODAZI="+cp_ToStrODBC(this.w_DFCODAZI)';

  cKeyWhereODBCqualified = '"DOCFINLG.DFCSERIA="+cp_ToStrODBC(this.w_DFCSERIA)';
      +'+" and DOCFINLG.DFCODAZI="+cp_ToStrODBC(this.w_DFCODAZI)';

  cPrg = "gsdf_alg"
  cComment = "Log importazione DocFinance"
  icon = "anag.ico"
  cAutoZoom = 'GSDF_ALG'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFCSERIA = space(10)
  w_DFCODAZI = space(10)
  w_DFCIMPEX = space(1)
  w_DFFLGERR = space(1)
  w_DFCODUTE = 0
  w_DFCDATAE = ctod('  /  /  ')
  w_DFORAINI = space(8)
  w_DFORAFIN = space(8)
  w_DFNOMFIL = space(100)
  w_DFLICLOG = space(0)
  w_ErrorLog = .F.
  w_PrintLog = .F.
  w_DFNUMERA = space(4)
  w_DF__ANNO = space(4)
  w_DFMOVIME = space(10)
  w_DFDATREG = ctod('  /  /  ')
  w_DFCONTRO = space(22)
  w_DFPIANOC = space(16)
  w_DFCHIAVE = space(6)

  * --- Autonumbered Variables
  op_DFCODAZI = this.W_DFCODAZI
  op_DFCSERIA = this.W_DFCSERIA
  w_ZOOMMOV = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsdf_alg
  **disabilita l inserimento e modifica
  procedure ecpload ()
  return
  endproc
  
  **disabilita l inserimento
  procedure ecpquit ()
  DODEFAULT()
  If vartype(this.w_ZOOMMOV.enabled)='L'
  this.w_ZOOMMOV.enabled=.t.
  ENDIF
  endproc
  
  
    proc ecpPrint()
      if this.cFunction='Query'
        this.notifyevent ("Stampa")
      endif
    endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DOCFINLG','gsdf_alg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_algPag1","gsdf_alg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Log")
      .Pages(1).HelpContextID = 226709578
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDFCSERIA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZOOMMOV = this.oPgFrm.Pages(1).oPag.ZOOMMOV
      DoDefault()
    proc Destroy()
      this.w_ZOOMMOV = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DOCFINLG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOCFINLG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOCFINLG_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DFCSERIA = NVL(DFCSERIA,space(10))
      .w_DFCODAZI = NVL(DFCODAZI,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DOCFINLG where DFCSERIA=KeySet.DFCSERIA
    *                            and DFCODAZI=KeySet.DFCODAZI
    *
    i_nConn = i_TableProp[this.DOCFINLG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINLG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOCFINLG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOCFINLG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOCFINLG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFCSERIA',this.w_DFCSERIA  ,'DFCODAZI',this.w_DFCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ErrorLog = .f.
        .w_PrintLog = .f.
        .w_DFCSERIA = NVL(DFCSERIA,space(10))
        .op_DFCSERIA = .w_DFCSERIA
        .w_DFCODAZI = NVL(DFCODAZI,space(10))
        .op_DFCODAZI = .w_DFCODAZI
        .w_DFCIMPEX = NVL(DFCIMPEX,space(1))
        .w_DFFLGERR = NVL(DFFLGERR,space(1))
        .w_DFCODUTE = NVL(DFCODUTE,0)
        .w_DFCDATAE = NVL(cp_ToDate(DFCDATAE),ctod("  /  /  "))
        .w_DFORAINI = NVL(DFORAINI,space(8))
        .w_DFORAFIN = NVL(DFORAFIN,space(8))
        .w_DFNOMFIL = NVL(DFNOMFIL,space(100))
        .w_DFLICLOG = NVL(DFLICLOG,space(0))
        .oPgFrm.Page1.oPag.ZOOMMOV.Calculate()
        .w_DFNUMERA = .w_ZOOMMOV.getVar('DFNUMERA')
        .w_DF__ANNO = .w_ZOOMMOV.getVar('DF__ANNO')
        .w_DFMOVIME = .w_ZOOMMOV.getVar('DFMOVIME')
        .w_DFDATREG = .w_ZOOMMOV.getVar('DFDATREG')
        .w_DFCONTRO = .w_ZOOMMOV.getVar('DFCONTRO')
        .w_DFPIANOC = .w_ZOOMMOV.getVar('DFPIANOC')
        .w_DFCHIAVE = .w_ZOOMMOV.getVar('DFCHIAVE')
        cp_LoadRecExtFlds(this,'DOCFINLG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DFCSERIA = space(10)
      .w_DFCODAZI = space(10)
      .w_DFCIMPEX = space(1)
      .w_DFFLGERR = space(1)
      .w_DFCODUTE = 0
      .w_DFCDATAE = ctod("  /  /  ")
      .w_DFORAINI = space(8)
      .w_DFORAFIN = space(8)
      .w_DFNOMFIL = space(100)
      .w_DFLICLOG = space(0)
      .w_ErrorLog = .f.
      .w_PrintLog = .f.
      .w_DFNUMERA = space(4)
      .w_DF__ANNO = space(4)
      .w_DFMOVIME = space(10)
      .w_DFDATREG = ctod("  /  /  ")
      .w_DFCONTRO = space(22)
      .w_DFPIANOC = space(16)
      .w_DFCHIAVE = space(6)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.ZOOMMOV.Calculate()
          .DoRTCalc(1,12,.f.)
        .w_DFNUMERA = .w_ZOOMMOV.getVar('DFNUMERA')
        .w_DF__ANNO = .w_ZOOMMOV.getVar('DF__ANNO')
        .w_DFMOVIME = .w_ZOOMMOV.getVar('DFMOVIME')
        .w_DFDATREG = .w_ZOOMMOV.getVar('DFDATREG')
        .w_DFCONTRO = .w_ZOOMMOV.getVar('DFCONTRO')
        .w_DFPIANOC = .w_ZOOMMOV.getVar('DFPIANOC')
        .w_DFCHIAVE = .w_ZOOMMOV.getVar('DFCHIAVE')
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOCFINLG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsdf_alg
    this.w_ZOOMMOV.enabled=.t.
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOCFINLG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINLG_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SELOG","w_DFCODAZI,w_DFCSERIA")
      .op_DFCODAZI = .w_DFCODAZI
      .op_DFCSERIA = .w_DFCSERIA
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDFCSERIA_1_1.enabled = i_bVal
      .Page1.oPag.oDFCODAZI_1_3.enabled = i_bVal
      .Page1.oPag.oBtn_1_18.enabled = .Page1.oPag.oBtn_1_18.mCond()
      .Page1.oPag.ZOOMMOV.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDFCSERIA_1_1.enabled = .f.
        .Page1.oPag.oDFCODAZI_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDFCSERIA_1_1.enabled = .t.
        .Page1.oPag.oDFCODAZI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DOCFINLG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOCFINLG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCSERIA,"DFCSERIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODAZI,"DFCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCIMPEX,"DFCIMPEX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFFLGERR,"DFFLGERR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODUTE,"DFCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCDATAE,"DFCDATAE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFORAINI,"DFORAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFORAFIN,"DFORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFNOMFIL,"DFNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFLICLOG,"DFLICLOG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOCFINLG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINLG_IDX,2])
    i_lTable = "DOCFINLG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DOCFINLG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOCFINLG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINLG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DOCFINLG_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SELOG","w_DFCODAZI,w_DFCSERIA")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DOCFINLG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOCFINLG')
        i_extval=cp_InsertValODBCExtFlds(this,'DOCFINLG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DFCSERIA,DFCODAZI,DFCIMPEX,DFFLGERR,DFCODUTE"+;
                  ",DFCDATAE,DFORAINI,DFORAFIN,DFNOMFIL,DFLICLOG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DFCSERIA)+;
                  ","+cp_ToStrODBC(this.w_DFCODAZI)+;
                  ","+cp_ToStrODBC(this.w_DFCIMPEX)+;
                  ","+cp_ToStrODBC(this.w_DFFLGERR)+;
                  ","+cp_ToStrODBC(this.w_DFCODUTE)+;
                  ","+cp_ToStrODBC(this.w_DFCDATAE)+;
                  ","+cp_ToStrODBC(this.w_DFORAINI)+;
                  ","+cp_ToStrODBC(this.w_DFORAFIN)+;
                  ","+cp_ToStrODBC(this.w_DFNOMFIL)+;
                  ","+cp_ToStrODBC(this.w_DFLICLOG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOCFINLG')
        i_extval=cp_InsertValVFPExtFlds(this,'DOCFINLG')
        cp_CheckDeletedKey(i_cTable,0,'DFCSERIA',this.w_DFCSERIA,'DFCODAZI',this.w_DFCODAZI)
        INSERT INTO (i_cTable);
              (DFCSERIA,DFCODAZI,DFCIMPEX,DFFLGERR,DFCODUTE,DFCDATAE,DFORAINI,DFORAFIN,DFNOMFIL,DFLICLOG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DFCSERIA;
                  ,this.w_DFCODAZI;
                  ,this.w_DFCIMPEX;
                  ,this.w_DFFLGERR;
                  ,this.w_DFCODUTE;
                  ,this.w_DFCDATAE;
                  ,this.w_DFORAINI;
                  ,this.w_DFORAFIN;
                  ,this.w_DFNOMFIL;
                  ,this.w_DFLICLOG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DOCFINLG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINLG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DOCFINLG_IDX,i_nConn)
      *
      * update DOCFINLG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DOCFINLG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DFCIMPEX="+cp_ToStrODBC(this.w_DFCIMPEX)+;
             ",DFFLGERR="+cp_ToStrODBC(this.w_DFFLGERR)+;
             ",DFCODUTE="+cp_ToStrODBC(this.w_DFCODUTE)+;
             ",DFCDATAE="+cp_ToStrODBC(this.w_DFCDATAE)+;
             ",DFORAINI="+cp_ToStrODBC(this.w_DFORAINI)+;
             ",DFORAFIN="+cp_ToStrODBC(this.w_DFORAFIN)+;
             ",DFNOMFIL="+cp_ToStrODBC(this.w_DFNOMFIL)+;
             ",DFLICLOG="+cp_ToStrODBC(this.w_DFLICLOG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DOCFINLG')
        i_cWhere = cp_PKFox(i_cTable  ,'DFCSERIA',this.w_DFCSERIA  ,'DFCODAZI',this.w_DFCODAZI  )
        UPDATE (i_cTable) SET;
              DFCIMPEX=this.w_DFCIMPEX;
             ,DFFLGERR=this.w_DFFLGERR;
             ,DFCODUTE=this.w_DFCODUTE;
             ,DFCDATAE=this.w_DFCDATAE;
             ,DFORAINI=this.w_DFORAINI;
             ,DFORAFIN=this.w_DFORAFIN;
             ,DFNOMFIL=this.w_DFNOMFIL;
             ,DFLICLOG=this.w_DFLICLOG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOCFINLG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINLG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DOCFINLG_IDX,i_nConn)
      *
      * delete DOCFINLG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DFCSERIA',this.w_DFCSERIA  ,'DFCODAZI',this.w_DFCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOCFINLG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINLG_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMMOV.Calculate()
        .DoRTCalc(1,12,.t.)
            .w_DFNUMERA = .w_ZOOMMOV.getVar('DFNUMERA')
            .w_DF__ANNO = .w_ZOOMMOV.getVar('DF__ANNO')
            .w_DFMOVIME = .w_ZOOMMOV.getVar('DFMOVIME')
            .w_DFDATREG = .w_ZOOMMOV.getVar('DFDATREG')
            .w_DFCONTRO = .w_ZOOMMOV.getVar('DFCONTRO')
            .w_DFPIANOC = .w_ZOOMMOV.getVar('DFPIANOC')
            .w_DFCHIAVE = .w_ZOOMMOV.getVar('DFCHIAVE')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_DFCODAZI<>.w_DFCODAZI
           cp_AskTableProg(this,i_nConn,"SELOG","w_DFCODAZI,w_DFCSERIA")
          .op_DFCSERIA = .w_DFCSERIA
        endif
        .op_DFCODAZI = .w_DFCODAZI
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMMOV.Calculate()
    endwith
  return

  proc Calculate_OWVKMRGYWQ()
    with this
          * --- Stampa Log
          .w_ErrorLog = CreateObject('Ah_ErrorLog')
          .w_PrintLog = .w_ErrorLog.AddMsgLog(.w_DFLICLOG)
          .w_PrintLog = .w_ErrorLog.PrintLog(this,'Segnalazioni in fase di importazione',.f.,' ')
    endwith
  endproc
  proc Calculate_HVJZKNJOVX()
    with this
          * --- Apre movimento DocFinance
          GSDF_BDM(this;
              ,'DOCF';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDFNOMFIL_1_17.visible=!this.oPgFrm.Page1.oPag.oDFNOMFIL_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Stampa")
          .Calculate_OWVKMRGYWQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZOOMMOV.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMMOV selected")
          .Calculate_HVJZKNJOVX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDFCSERIA_1_1.value==this.w_DFCSERIA)
      this.oPgFrm.Page1.oPag.oDFCSERIA_1_1.value=this.w_DFCSERIA
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCODAZI_1_3.value==this.w_DFCODAZI)
      this.oPgFrm.Page1.oPag.oDFCODAZI_1_3.value=this.w_DFCODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCIMPEX_1_4.RadioValue()==this.w_DFCIMPEX)
      this.oPgFrm.Page1.oPag.oDFCIMPEX_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFFLGERR_1_5.RadioValue()==this.w_DFFLGERR)
      this.oPgFrm.Page1.oPag.oDFFLGERR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCODUTE_1_7.value==this.w_DFCODUTE)
      this.oPgFrm.Page1.oPag.oDFCODUTE_1_7.value=this.w_DFCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCDATAE_1_9.value==this.w_DFCDATAE)
      this.oPgFrm.Page1.oPag.oDFCDATAE_1_9.value=this.w_DFCDATAE
    endif
    if not(this.oPgFrm.Page1.oPag.oDFORAINI_1_11.value==this.w_DFORAINI)
      this.oPgFrm.Page1.oPag.oDFORAINI_1_11.value=this.w_DFORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDFORAFIN_1_13.value==this.w_DFORAFIN)
      this.oPgFrm.Page1.oPag.oDFORAFIN_1_13.value=this.w_DFORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDFNOMFIL_1_17.value==this.w_DFNOMFIL)
      this.oPgFrm.Page1.oPag.oDFNOMFIL_1_17.value=this.w_DFNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDFLICLOG_1_20.value==this.w_DFLICLOG)
      this.oPgFrm.Page1.oPag.oDFLICLOG_1_20.value=this.w_DFLICLOG
    endif
    cp_SetControlsValueExtFlds(this,'DOCFINLG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdf_algPag1 as StdContainer
  Width  = 781
  height = 539
  stdWidth  = 781
  stdheight = 539
  resizeXpos=200
  resizeYpos=309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFCSERIA_1_1 as StdField with uid="PKUSFLCGBB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DFCSERIA", cQueryName = "DFCSERIA",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 115521929,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=103, Top=11, InputMask=replicate('X',10)

  add object oDFCODAZI_1_3 as StdField with uid="LYXWXBPWJZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DFCODAZI", cQueryName = "DFCSERIA,DFCODAZI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Azienda DocFinance dei movimenti importati",;
    HelpContextID = 133609857,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=284, Top=11, InputMask=replicate('X',10)


  add object oDFCIMPEX_1_4 as StdCombo with uid="OVBHTJUKYY",rtseq=3,rtrep=.f.,left=481,top=11,width=179,height=22, enabled=.f.;
    , ToolTipText = "Tipo operazione (Import o Export)";
    , HelpContextID = 141343090;
    , cFormVar="w_DFCIMPEX",RowSource=""+"Importazione,"+"Importazione e generazione,"+"Generazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDFCIMPEX_1_4.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'X',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oDFCIMPEX_1_4.GetRadio()
    this.Parent.oContained.w_DFCIMPEX = this.RadioValue()
    return .t.
  endfunc

  func oDFCIMPEX_1_4.SetRadio()
    this.Parent.oContained.w_DFCIMPEX=trim(this.Parent.oContained.w_DFCIMPEX)
    this.value = ;
      iif(this.Parent.oContained.w_DFCIMPEX=='I',1,;
      iif(this.Parent.oContained.w_DFCIMPEX=='X',2,;
      iif(this.Parent.oContained.w_DFCIMPEX=='G',3,;
      0)))
  endfunc

  add object oDFFLGERR_1_5 as StdCheck with uid="FBNZSVADTU",rtseq=4,rtrep=.f.,left=666, top=10, caption="Movimenti errati", enabled=.f.,;
    HelpContextID = 204895880,;
    cFormVar="w_DFFLGERR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFFLGERR_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDFFLGERR_1_5.GetRadio()
    this.Parent.oContained.w_DFFLGERR = this.RadioValue()
    return .t.
  endfunc

  func oDFFLGERR_1_5.SetRadio()
    this.Parent.oContained.w_DFFLGERR=trim(this.Parent.oContained.w_DFFLGERR)
    this.value = ;
      iif(this.Parent.oContained.w_DFFLGERR=='S',1,;
      0)
  endfunc

  add object oDFCODUTE_1_7 as StdField with uid="JMMVPRPHCG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DFCODUTE", cQueryName = "DFCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha lanciato l importazione",;
    HelpContextID = 201934459,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=103, Top=41, cSayPict='"999999"', cGetPict='"999999"'

  add object oDFCDATAE_1_9 as StdField with uid="UKUKCUGPGN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DFCDATAE", cQueryName = "DFCDATAE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 181290619,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=284, Top=41

  add object oDFORAINI_1_11 as StdField with uid="ZJLILTNMTV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DFORAINI", cQueryName = "DFORAINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora Inizio Esecuzione Import",;
    HelpContextID = 266143359,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=481, Top=41, InputMask=replicate('X',8)

  add object oDFORAFIN_1_13 as StdField with uid="DUPVKGHNYJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DFORAFIN", cQueryName = "DFORAFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora Fine Esecuzione Import ",;
    HelpContextID = 52623740,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=676, Top=41, InputMask=replicate('X',8)

  add object oDFNOMFIL_1_17 as StdField with uid="GSRDWBLNSX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DFNOMFIL", cQueryName = "DFNOMFIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 40241534,;
   bGlobalFont=.t.,;
    Height=48, Width=436, Left=103, Top=70, InputMask=replicate('X',100)

  func oDFNOMFIL_1_17.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_DFNOMFIL))
    endwith
  endfunc


  add object oBtn_1_18 as StdButton with uid="KWFWCRWNHO",left=727, top=71, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 132220182;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        .NotifyEvent('Stampa')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_DFLICLOG))
      endwith
    endif
  endfunc

  add object oDFLICLOG_1_20 as StdMemo with uid="QNLHSAIONN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DFLICLOG", cQueryName = "DFLICLOG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 49534589,;
   bGlobalFont=.t.,;
    Height=396, Width=449, Left=4, Top=142, readonly = .t. , enabled = .t. 


  add object ZOOMMOV as cp_zoombox with uid="IEEULTBTOF",left=447, top=134, width=332,height=405,;
    caption='ZOOMMOV',;
   bGlobalFont=.t.,;
    bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cTable="DOCFINLG",cZoomFile="GSDF1ALG",bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 157806442

  add object oStr_1_2 as StdString with uid="YCUHBRCTCY",Visible=.t., Left=59, Top=15,;
    Alignment=1, Width=42, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="KYYFSCCQOP",Visible=.t., Left=195, Top=15,;
    Alignment=1, Width=88, Height=18,;
    Caption="Codice azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="CDAMMQVISS",Visible=.t., Left=20, Top=45,;
    Alignment=1, Width=81, Height=18,;
    Caption="Codice Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="VWRSIPVTZC",Visible=.t., Left=166, Top=45,;
    Alignment=1, Width=117, Height=18,;
    Caption="Data Esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="CJOYWXIBAN",Visible=.t., Left=356, Top=45,;
    Alignment=1, Width=121, Height=18,;
    Caption="Ora inizio esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="GLHQLINFMP",Visible=.t., Left=544, Top=45,;
    Alignment=1, Width=129, Height=18,;
    Caption="Ora fine esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="LSBWLNYZIL",Visible=.t., Left=377, Top=15,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TJHXTTWIID",Visible=.t., Left=4, Top=122,;
    Alignment=0, Width=106, Height=18,;
    Caption="Log elaborazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="JZCYGGSLCM",Visible=.t., Left=15, Top=77,;
    Alignment=1, Width=84, Height=18,;
    Caption="Nome del file:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_DFNOMFIL))
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="JQRZRCGQWL",Visible=.t., Left=463, Top=124,;
    Alignment=0, Width=106, Height=18,;
    Caption="Movimenti DocFinance"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_alg','DOCFINLG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFCSERIA=DOCFINLG.DFCSERIA";
  +" and "+i_cAliasName2+".DFCODAZI=DOCFINLG.DFCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
