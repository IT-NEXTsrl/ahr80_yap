* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_bdm                                                        *
*              Cancellazione da movimenti di tesoreria                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-07                                                      *
* Last revis.: 2017-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar,w_SERIALE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdf_bdm",oParentObject,m.pPar,m.w_SERIALE)
return(i_retval)

define class tgsdf_bdm as StdBatch
  * --- Local variables
  pPar = space(3)
  w_SERIALE = space(10)
  w_DFTRAAZI = space(10)
  w_TESO = .NULL.
  w_ERROR = space(50)
  w_DATSCA = ctod("  /  /  ")
  w_NUMPAR = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(10)
  w_CODVAL = space(4)
  w_MODPAG = space(4)
  w_NUMEFF = 0
  w_PTSERI = space(10)
  w_LETTO = space(5)
  w_MESS = space(100)
  w_LETTO = space(5)
  w_MESS = space(100)
  w_LETTO = space(5)
  w_MESS = space(100)
  w_MESS = space(100)
  w_Caudoc = space(4)
  * --- WorkFile variables
  DOCFINIM_idx=0
  PAR_TITE_idx=0
  DOCFINPA_idx=0
  DORATING_idx=0
  DOCTRCAU_idx=0
  CASHFLOW_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pPar="DEL"
        * --- Sbianca il flag di generazione del movimento di tesoreria nel caso in cui venga cancellato il movimento di tesoreria
        * --- Read from DOCFINPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOCFINPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCFINPA_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DFTRAAZI"+;
            " from "+i_cTable+" DOCFINPA where ";
                +"DFCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DFTRAAZI;
            from (i_cTable) where;
                DFCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DFTRAAZI = NVL(cp_ToDate(_read_.DFTRAAZI),cp_NullValue(_read_.DFTRAAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if UPPER (This.oparentobject.class)= "TGSCG_BCK"
          * --- Write into DOCFINIM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOCFINIM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOCFINIM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DFFLGMOV ="+cp_NullLink(cp_ToStrODBC(" "),'DOCFINIM','DFFLGMOV');
            +",DFSERTES ="+cp_NullLink(cp_ToStrODBC(space (10)),'DOCFINIM','DFSERTES');
                +i_ccchkf ;
            +" where ";
                +"DFSERTES = "+cp_ToStrODBC(this.w_SERIALE);
                +" and DFCODAZI = "+cp_ToStrODBC(this.w_DFTRAAZI);
                +" and DFFLGDIS <> "+cp_ToStrODBC("D");
                   )
          else
            update (i_cTable) set;
                DFFLGMOV = " ";
                ,DFSERTES = space (10);
                &i_ccchkf. ;
             where;
                DFSERTES = this.w_SERIALE;
                and DFCODAZI = this.w_DFTRAAZI;
                and DFFLGDIS <> "D";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into DOCFINIM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOCFINIM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOCFINIM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DFFLGMOV ="+cp_NullLink(cp_ToStrODBC(" "),'DOCFINIM','DFFLGMOV');
            +",DFSERTES ="+cp_NullLink(cp_ToStrODBC(space (10)),'DOCFINIM','DFSERTES');
                +i_ccchkf ;
            +" where ";
                +"DFSERTES = "+cp_ToStrODBC(this.w_SERIALE);
                +" and DFCODAZI = "+cp_ToStrODBC(this.w_DFTRAAZI);
                +" and DFFLGDIS = "+cp_ToStrODBC("D");
                   )
          else
            update (i_cTable) set;
                DFFLGMOV = " ";
                ,DFSERTES = space (10);
                &i_ccchkf. ;
             where;
                DFSERTES = this.w_SERIALE;
                and DFCODAZI = this.w_DFTRAAZI;
                and DFFLGDIS = "D";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pPar="MOV"
        * --- Visualizza il movimento di Primanota/distinta
        if this.oParentObject.w_DFFLGDIS="D"
          this.w_TESO = GSTE_ADI()
          this.w_TESO.w_DINUMDIS = this.oParentObject.w_DFSERTES
          this.w_TESO.QueryKeySet("DINUMDIS='"+this.oParentObject.w_DFSERTES+"'")     
          this.w_TESO.LoadRecWarn()     
        else
          this.w_TESO = GSCG_MPN()
          this.w_TESO.w_PNSERIAL = this.oParentObject.w_DFSERTES
          this.w_TESO.QueryKeySet("PNSERIAL='"+this.oParentObject.w_DFSERTES+"'")     
          this.w_TESO.LoadRecWarn()     
        endif
      case this.pPar="CANC"
        * --- Cancella il movimento di tesoreria se vine cancellato il movimento di DocFinance
        if this.oParentObject.w_DFFLGMOV="S"
          this.w_ERROR = ah_MSGFORMAT ("La cancellazione del movimento DocFinance non � possibile poich� %1" , IIF (this.oParentObject.w_DFFLGDIS="D", "� stata generata la distinta", "� stato generato il movimento di primanota"))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_ERROR
        endif
      case this.pPar="DOCF"
        this.w_TESO = GSDF_MMT()
        this.w_TESO.w_DFMOVIME = this.oParentObject.w_DFMOVIME
        this.w_TESO.w_DFNUMERA = this.oParentObject.w_DFNUMERA
        this.w_TESO.w_DF__ANNO = this.oParentObject.w_DF__ANNO
        this.w_TESO.w_DFCONTRo = this.oParentObject.w_DFCONTRo
        this.w_TESO.w_DFPIANOC = this.oParentObject.w_DFPIANOC
        this.w_TESO.w_DFPIANOC = this.oParentObject.w_DFPIANOC
        this.w_TESO.w_DFDATREG = this.oParentObject.w_DFDATREG
        this.w_TESO.w_DFCODAZI = this.oParentObject.w_DFCODAZI
        this.w_TESO.w_DFCHIAVE = this.oParentObject.w_DFCHIAVE
        this.w_TESO.QueryKeySet("DFMOVIME='"+this.oParentObject.w_DFMOVIME+"'" + "AND DFNUMERA='"+this.oParentObject.w_DFNUMERA+"'"+ "AND DF__ANNO='"+this.oParentObject.w_DF__ANNO+"'"+ "AND DFCONTRo='"+this.oParentObject.w_DFCONTRo+"'"+ "AND DFPIANOC='"+this.oParentObject.w_DFPIANOC+"'"+ "AND DFDATREG="+cp_ToStrODBC( TTOD ( this.oParentObject.w_DFDATREG ) )+" "+ "AND DFCODAZI='"+this.oParentObject.w_DFCODAZI+"'"+ "AND DFCHIAVE='"+this.oParentObject.w_DFCHIAVE+"'")     
        this.w_TESO.LoadRecWarn()     
      case this.pPar="ABBINA"
        * --- Associa al movimento una partita con stesso topo/codice conto, stessa valuta e importo
        if this.oParentObject.w_DFPTTIPO = "D"
          vx_exec("..\ISDF\exe\QUERY\GSDF1MMT.vzm",this)
        else
          vx_exec("..\ISDF\exe\QUERY\GSDF_MMT.vzm",this)
        endif
        * --- Se l utente non ha premutto ESC ed ha selezionato una partita 
        if NOT EMPTY (this.w_TIPCON)
          if this.oParentObject.w_DFPTTIPO = "D"
            * --- Recupera il seriale ptroword cprownum di una delle partite seleionate (uasndo una chiave logica rsalgo ad una chia fisica)
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERIAL,PTROWORD,CPROWNUM,PTNUMEFF"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTDATSCA = "+cp_ToStrODBC(this.w_DATSCA);
                    +" and PTNUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
                    +" and PTTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                    +" and PTCODCON = "+cp_ToStrODBC(this.w_CODCON);
                    +" and PTCODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                    +" and PTFLCRSA = "+cp_ToStrODBC("C");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERIAL,PTROWORD,CPROWNUM,PTNUMEFF;
                from (i_cTable) where;
                    PTDATSCA = this.w_DATSCA;
                    and PTNUMPAR = this.w_NUMPAR;
                    and PTTIPCON = this.w_TIPCON;
                    and PTCODCON = this.w_CODCON;
                    and PTCODVAL = this.w_CODVAL;
                    and PTFLCRSA = "C";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DFPTSERI = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
              this.oParentObject.w_DFPTROW = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
              this.oParentObject.w_DFCPROW = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
              this.oParentObject.w_DFNUMEFF = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.oParentObject.w_DFNUMEFF = this.w_NUMEFF
            this.oParentObject.w_DFPTSERI = this.w_PTSERI
          endif
        endif
      case this.pPar="DocFinance"
        * --- Read from DOCFINIM
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOCFINIM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DFSERTES"+;
            " from "+i_cTable+" DOCFINIM where ";
                +"DFSERTES = "+cp_ToStrODBC(w_CCSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DFSERTES;
            from (i_cTable) where;
                DFSERTES = w_CCSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DocFinance = NVL(cp_ToDate(_read_.DFSERTES),cp_NullValue(_read_.DFSERTES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pPar="CAUSALECANC"
        * --- Controllo in fase di cacnellazione delle causali che non siano usate nei parametri di docfinance
        * --- Read from DOCFINPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOCFINPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCFINPA_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DFCAUDIS"+;
            " from "+i_cTable+" DOCFINPA where ";
                +"DFCAUDIS = "+cp_ToStrODBC(this.oParentObject.w_CACODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DFCAUDIS;
            from (i_cTable) where;
                DFCAUDIS = this.oParentObject.w_CACODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LETTO = NVL(cp_ToDate(_read_.DFCAUDIS),cp_NullValue(_read_.DFCAUDIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY (this.w_LETTO)
          this.w_MESS = "La causale di tesoreria � utilizzata nei Dati generali del modulo DocFinance"
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pPar="VOCFINCANC"
        * --- Controllo alla cancellaizione delle voci finanziare per verificare che non siano usate ne dati generali di DocFinance
        * --- Controllo in fase di cacnellazione delle causali che non siano usate nei parametri di docfinance
        * --- Select from DOCFINPA
        i_nConn=i_TableProp[this.DOCFINPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCFINPA_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DFCODAZI) AS DFCODAZI  from "+i_cTable+" DOCFINPA ";
              +" where DFVOCFCL="+cp_ToStrODBC(this.oParentObject.w_DFVOCFIN)+" OR DFVOCFGE="+cp_ToStrODBC(this.oParentObject.w_DFVOCFIN)+" OR DFVOCFFO="+cp_ToStrODBC(this.oParentObject.w_DFVOCFIN)+" OR DFVOCFDC="+cp_ToStrODBC(this.oParentObject.w_DFVOCFIN)+" OR DFVOCEFF="+cp_ToStrODBC(this.oParentObject.w_DFVOCFIN)+"";
               ,"_Curs_DOCFINPA")
        else
          select MAX(DFCODAZI) AS DFCODAZI from (i_cTable);
           where DFVOCFCL=this.oParentObject.w_DFVOCFIN OR DFVOCFGE=this.oParentObject.w_DFVOCFIN OR DFVOCFFO=this.oParentObject.w_DFVOCFIN OR DFVOCFDC=this.oParentObject.w_DFVOCFIN OR DFVOCEFF=this.oParentObject.w_DFVOCFIN;
            into cursor _Curs_DOCFINPA
        endif
        if used('_Curs_DOCFINPA')
          select _Curs_DOCFINPA
          locate for 1=1
          do while not(eof())
          this.w_LETTO = DFCODAZI
            select _Curs_DOCFINPA
            continue
          enddo
          use
        endif
        if NOT EMPTY ( NVL (this.w_LETTO, "") )
          this.w_MESS = "La voce finanziaria � utilizzata nell'anagrafica dei Dati generali"
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pPar="RATINGCANC"
        * --- Controllo alla cancellaizione delle voci di Rating per verificare che non siano usate ne dati generali di DocFinance o ch non siano usate in da altre voci di rating
        * --- Controllo in fase di cacnellazione delle voci di rating che non siano usate nei parametri di docfinance
        * --- Select from DOCFINPA
        i_nConn=i_TableProp[this.DOCFINPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCFINPA_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DFCODAZI) AS DFCODAZI  from "+i_cTable+" DOCFINPA ";
              +" where DFRATSCA="+cp_ToStrODBC(this.oParentObject.w_RACODICE)+" OR DFRATSDT="+cp_ToStrODBC(this.oParentObject.w_RACODICE)+"";
               ,"_Curs_DOCFINPA")
        else
          select MAX(DFCODAZI) AS DFCODAZI from (i_cTable);
           where DFRATSCA=this.oParentObject.w_RACODICE OR DFRATSDT=this.oParentObject.w_RACODICE;
            into cursor _Curs_DOCFINPA
        endif
        if used('_Curs_DOCFINPA')
          select _Curs_DOCFINPA
          locate for 1=1
          do while not(eof())
          this.w_LETTO = DFCODAZI
            select _Curs_DOCFINPA
            continue
          enddo
          use
        endif
        if NOT EMPTY ( NVL (this.w_LETTO, "") )
          this.w_MESS = "La voce di rating � utilizzata nell'anagrafica dei Dati generali"
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
        * --- Verifica che la voce di rating non sia usata da altre voci di rating
        * --- Read from DORATING
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DORATING_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DORATING_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "RACODICE"+;
            " from "+i_cTable+" DORATING where ";
                +"RASCADUT = "+cp_ToStrODBC(this.oParentObject.w_RACODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            RACODICE;
            from (i_cTable) where;
                RASCADUT = this.oParentObject.w_RACODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LETTO = NVL(cp_ToDate(_read_.RACODICE),cp_NullValue(_read_.RACODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY ( NVL (this.w_LETTO, "") )
          this.w_MESS = AH_MSGFORMAT ("Il rating � utilizzato nella voce di rating %1", alltrim (this.w_LETTO))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pPar="TRACAU"
        * --- Verifico se la causale contabile associata nn sia presente in 
        *     altre trascodifiche
        * --- Read from DOCTRCAU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOCTRCAU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCTRCAU_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRCAUDOC"+;
            " from "+i_cTable+" DOCTRCAU where ";
                +"TRCAUCON = "+cp_ToStrODBC(this.oParentObject.w_Trcaucon);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRCAUDOC;
            from (i_cTable) where;
                TRCAUCON = this.oParentObject.w_Trcaucon;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Caudoc = NVL(cp_ToDate(_read_.TRCAUDOC),cp_NullValue(_read_.TRCAUDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_Caudoc<>this.oParentObject.w_Trcaudoc And Not Empty(this.w_Caudoc)
          this.oParentObject.w_RESCHK = -1
          Ah_ErrorMsg("Causale contabile gi� associata a causale DocFinance")
        endif
      case this.pPar="CONTINS"
        * --- Read from DOCFINPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOCFINPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCFINPA_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DFCONTIN"+;
            " from "+i_cTable+" DOCFINPA where ";
                +"DFCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DFCONTIN;
            from (i_cTable) where;
                DFCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DFCONTIN = NVL(cp_ToDate(_read_.DFCONTIN),cp_NullValue(_read_.DFCONTIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pPar="RATEDOC"
        * --- Select from CASHFLOW
        i_nConn=i_TableProp[this.CASHFLOW_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CASHFLOW_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CFDATCRE  from "+i_cTable+" CASHFLOW ";
              +" order by CFDATCRE DESC";
               ,"_Curs_CASHFLOW")
        else
          select CFDATCRE from (i_cTable);
           order by CFDATCRE DESC;
            into cursor _Curs_CASHFLOW
        endif
        if used('_Curs_CASHFLOW')
          select _Curs_CASHFLOW
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_CFDATCRE = _Curs_CASHFLOW.CFDATCRE
          exit
            select _Curs_CASHFLOW
            continue
          enddo
          use
        endif
        this.oParentObject.w_CFDATCRE = Nvl(this.oParentObject.w_CFDATCRE,{})
    endcase
  endproc


  proc Init(oParentObject,pPar,w_SERIALE)
    this.pPar=pPar
    this.w_SERIALE=w_SERIALE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='DOCFINIM'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='DOCFINPA'
    this.cWorkTables[4]='DORATING'
    this.cWorkTables[5]='DOCTRCAU'
    this.cWorkTables[6]='CASHFLOW'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_DOCFINPA')
      use in _Curs_DOCFINPA
    endif
    if used('_Curs_DOCFINPA')
      use in _Curs_DOCFINPA
    endif
    if used('_Curs_CASHFLOW')
      use in _Curs_CASHFLOW
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar,w_SERIALE"
endproc
