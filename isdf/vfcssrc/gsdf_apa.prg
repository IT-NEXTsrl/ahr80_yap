* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_apa                                                        *
*              Pagamenti                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-03                                                      *
* Last revis.: 2014-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_apa"))

* --- Class definition
define class tgsdf_apa as StdForm
  Top    = 9
  Left   = 32

  * --- Standard Properties
  Width  = 583
  Height = 61+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-03-31"
  HelpContextID=160052329
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  DOCPAGAM_IDX = 0
  PAG_AMEN_IDX = 0
  MOD_PAGA_IDX = 0
  cFile = "DOCPAGAM"
  cKeySelect = "DFPAGAME"
  cKeyWhere  = "DFPAGAME=this.w_DFPAGAME"
  cKeyWhereODBC = '"DFPAGAME="+cp_ToStrODBC(this.w_DFPAGAME)';

  cKeyWhereODBCqualified = '"DOCPAGAM.DFPAGAME="+cp_ToStrODBC(this.w_DFPAGAME)';

  cPrg = "gsdf_apa"
  cComment = "Pagamenti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFCODPAG = space(2)
  w_DFPAGAME = space(10)
  w_DESPAG = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DOCPAGAM','gsdf_apa')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_apaPag1","gsdf_apa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Trascodifica tipi pagamento")
      .Pages(1).HelpContextID = 174490948
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDFPAGAME_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PAG_AMEN'
    this.cWorkTables[2]='MOD_PAGA'
    this.cWorkTables[3]='DOCPAGAM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOCPAGAM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOCPAGAM_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DFPAGAME = NVL(DFPAGAME,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DOCPAGAM where DFPAGAME=KeySet.DFPAGAME
    *
    i_nConn = i_TableProp[this.DOCPAGAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCPAGAM_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOCPAGAM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOCPAGAM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOCPAGAM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFPAGAME',this.w_DFPAGAME  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESPAG = space(30)
        .w_DFCODPAG = NVL(DFCODPAG,space(2))
        .w_DFPAGAME = NVL(DFPAGAME,space(10))
          .link_1_3('Load')
        cp_LoadRecExtFlds(this,'DOCPAGAM')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DFCODPAG = space(2)
      .w_DFPAGAME = space(10)
      .w_DESPAG = space(30)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
          if not(empty(.w_DFPAGAME))
          .link_1_3('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOCPAGAM')
    this.DoRTCalc(3,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDFCODPAG_1_1.enabled = i_bVal
      .Page1.oPag.oDFPAGAME_1_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDFPAGAME_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDFPAGAME_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DOCPAGAM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOCPAGAM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODPAG,"DFCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFPAGAME,"DFPAGAME",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOCPAGAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCPAGAM_IDX,2])
    i_lTable = "DOCPAGAM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DOCPAGAM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOCPAGAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCPAGAM_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DOCPAGAM_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DOCPAGAM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOCPAGAM')
        i_extval=cp_InsertValODBCExtFlds(this,'DOCPAGAM')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DFCODPAG,DFPAGAME "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DFCODPAG)+;
                  ","+cp_ToStrODBCNull(this.w_DFPAGAME)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOCPAGAM')
        i_extval=cp_InsertValVFPExtFlds(this,'DOCPAGAM')
        cp_CheckDeletedKey(i_cTable,0,'DFPAGAME',this.w_DFPAGAME)
        INSERT INTO (i_cTable);
              (DFCODPAG,DFPAGAME  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DFCODPAG;
                  ,this.w_DFPAGAME;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DOCPAGAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCPAGAM_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DOCPAGAM_IDX,i_nConn)
      *
      * update DOCPAGAM
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DOCPAGAM')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DFCODPAG="+cp_ToStrODBC(this.w_DFCODPAG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DOCPAGAM')
        i_cWhere = cp_PKFox(i_cTable  ,'DFPAGAME',this.w_DFPAGAME  )
        UPDATE (i_cTable) SET;
              DFCODPAG=this.w_DFCODPAG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOCPAGAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCPAGAM_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DOCPAGAM_IDX,i_nConn)
      *
      * delete DOCPAGAM
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DFPAGAME',this.w_DFPAGAME  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOCPAGAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCPAGAM_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DFPAGAME
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFPAGAME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_DFPAGAME)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_DFPAGAME))
          select MPCODICE,MPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFPAGAME)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFPAGAME) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'oDFPAGAME_1_3'),i_cWhere,'GSAR_AMP',"Tipi di pagamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFPAGAME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_DFPAGAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_DFPAGAME)
            select MPCODICE,MPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFPAGAME = NVL(_Link_.MPCODICE,space(10))
      this.w_DESPAG = NVL(_Link_.MPDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DFPAGAME = space(10)
      endif
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFPAGAME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDFCODPAG_1_1.value==this.w_DFCODPAG)
      this.oPgFrm.Page1.oPag.oDFCODPAG_1_1.value=this.w_DFCODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDFPAGAME_1_3.value==this.w_DFPAGAME)
      this.oPgFrm.Page1.oPag.oDFPAGAME_1_3.value=this.w_DFPAGAME
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_5.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_5.value=this.w_DESPAG
    endif
    cp_SetControlsValueExtFlds(this,'DOCPAGAM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DFPAGAME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFPAGAME_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DFPAGAME)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdf_apaPag1 as StdContainer
  Width  = 579
  height = 61
  stdWidth  = 579
  stdheight = 61
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFCODPAG_1_1 as StdField with uid="SWOQGLEGQK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DFCODPAG", cQueryName = "DFCODPAG",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Tipo pagamento Docfinance",;
    HelpContextID = 83278211,;
   bGlobalFont=.t.,;
    Height=21, Width=22, Left=164, Top=33, InputMask=replicate('X',2)

  add object oDFPAGAME_1_3 as StdField with uid="EAPNAUUVWS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DFPAGAME", cQueryName = "DFPAGAME",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo pagamento di adhoc",;
    HelpContextID = 64219525,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=164, Top=5, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_DFPAGAME"

  func oDFPAGAME_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFPAGAME_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDFPAGAME_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'oDFPAGAME_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"Tipi di pagamento",'',this.parent.oContained
  endproc
  proc oDFPAGAME_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_DFPAGAME
     i_obj.ecpSave()
  endproc

  add object oDESPAG_1_5 as StdField with uid="CNEOZOXUTO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 237288138,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=259, Top=5, InputMask=replicate('X',30)

  add object oStr_1_2 as StdString with uid="LMZZSVFKDJ",Visible=.t., Left=33, Top=9,;
    Alignment=1, Width=129, Height=18,;
    Caption="Tipo pagamento: "  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="CDAMMQVISS",Visible=.t., Left=3, Top=37,;
    Alignment=1, Width=159, Height=18,;
    Caption="Trascodifica tipo pagamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_apa','DOCPAGAM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFPAGAME=DOCPAGAM.DFPAGAME";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
