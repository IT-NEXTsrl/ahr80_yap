* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_acc                                                        *
*              Dati import/export conti contabili                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-03                                                      *
* Last revis.: 2012-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdf_acc"))

* --- Class definition
define class tgsdf_acc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 685
  Height = 279+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-24"
  HelpContextID=109720681
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  DOCIXCON_IDX = 0
  CONTI_IDX = 0
  DORATING_IDX = 0
  VOC_FINZ_IDX = 0
  DOCFINPA_IDX = 0
  cFile = "DOCIXCON"
  cKeySelect = "IETIPCON,IECODCON"
  cKeyWhere  = "IETIPCON=this.w_IETIPCON and IECODCON=this.w_IECODCON"
  cKeyWhereODBC = '"IETIPCON="+cp_ToStrODBC(this.w_IETIPCON)';
      +'+" and IECODCON="+cp_ToStrODBC(this.w_IECODCON)';

  cKeyWhereODBCqualified = '"DOCIXCON.IETIPCON="+cp_ToStrODBC(this.w_IETIPCON)';
      +'+" and DOCIXCON.IECODCON="+cp_ToStrODBC(this.w_IECODCON)';

  cPrg = "gsdf_acc"
  cComment = "Dati import/export conti contabili"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IETIPCON = space(1)
  o_IETIPCON = space(1)
  w_IECODCON = space(15)
  o_IECODCON = space(15)
  w_IEFLGSCA = space(1)
  w_IEFLPROV = space(1)
  w_IERATING = space(2)
  w_IEGIORIT = 0
  w_ANDESCRI = space(60)
  w_ANPARTSN = space(1)
  w_RADESCRI = space(50)
  w_IEVOCFIN = space(6)
  w_DFDESCRI = space(60)
  w_CODAZI = space(5)
  w_DFEXPMOV = space(1)
  w_ANESCDOF = space(1)

  * --- Children pointers
  Gsdf_Mpe = .NULL.
  Gsdf_Mpi = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DOCIXCON','gsdf_acc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_accPag1","gsdf_acc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati di esportazione scadenze")
      .Pages(1).HelpContextID = 138769327
      .Pages(2).addobject("oPag","tgsdf_accPag2","gsdf_acc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri di esportazione")
      .Pages(2).HelpContextID = 237402390
      .Pages(3).addobject("oPag","tgsdf_accPag3","gsdf_acc",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Importazione provvisoria")
      .Pages(3).HelpContextID = 253582912
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIECODCON_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DORATING'
    this.cWorkTables[3]='VOC_FINZ'
    this.cWorkTables[4]='DOCFINPA'
    this.cWorkTables[5]='DOCIXCON'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOCIXCON_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOCIXCON_IDX,3]
  return

  function CreateChildren()
    this.Gsdf_Mpe = CREATEOBJECT('stdDynamicChild',this,'Gsdf_Mpe',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.Gsdf_Mpi = CREATEOBJECT('stdDynamicChild',this,'Gsdf_Mpi',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.Gsdf_Mpe)
      this.Gsdf_Mpe.DestroyChildrenChain()
      this.Gsdf_Mpe=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    if !ISNULL(this.Gsdf_Mpi)
      this.Gsdf_Mpi.DestroyChildrenChain()
      this.Gsdf_Mpi=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.Gsdf_Mpe.IsAChildUpdated()
      i_bRes = i_bRes .or. this.Gsdf_Mpi.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.Gsdf_Mpe.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.Gsdf_Mpi.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.Gsdf_Mpe.NewDocument()
    this.Gsdf_Mpi.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.Gsdf_Mpe.SetKey(;
            .w_IETIPCON,"PETIPCON";
            ,.w_IECODCON,"PECODCON";
            )
      this.Gsdf_Mpi.SetKey(;
            .w_IETIPCON,"IMTIPCON";
            ,.w_IECODCON,"IMCODCON";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .Gsdf_Mpe.ChangeRow(this.cRowID+'      1',1;
             ,.w_IETIPCON,"PETIPCON";
             ,.w_IECODCON,"PECODCON";
             )
      .WriteTo_Gsdf_Mpe()
      .Gsdf_Mpi.ChangeRow(this.cRowID+'      1',1;
             ,.w_IETIPCON,"IMTIPCON";
             ,.w_IECODCON,"IMCODCON";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.Gsdf_Mpe)
        i_f=.Gsdf_Mpe.BuildFilter()
        if !(i_f==.Gsdf_Mpe.cQueryFilter)
          i_fnidx=.Gsdf_Mpe.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.Gsdf_Mpe.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.Gsdf_Mpe.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.Gsdf_Mpe.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.Gsdf_Mpe.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.Gsdf_Mpi)
        i_f=.Gsdf_Mpi.BuildFilter()
        if !(i_f==.Gsdf_Mpi.cQueryFilter)
          i_fnidx=.Gsdf_Mpi.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.Gsdf_Mpi.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.Gsdf_Mpi.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.Gsdf_Mpi.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.Gsdf_Mpi.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_Gsdf_Mpe()
  if at('gsdf_mpe',lower(this.Gsdf_Mpe.class))<>0
    if this.Gsdf_Mpe.w_PETIPCON<>this.w_IETIPCON
      this.Gsdf_Mpe.w_PETIPCON = this.w_IETIPCON
      this.Gsdf_Mpe.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_IETIPCON = NVL(IETIPCON,space(1))
      .w_IECODCON = NVL(IECODCON,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DOCIXCON where IETIPCON=KeySet.IETIPCON
    *                            and IECODCON=KeySet.IECODCON
    *
    i_nConn = i_TableProp[this.DOCIXCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCIXCON_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOCIXCON')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOCIXCON.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOCIXCON '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IETIPCON',this.w_IETIPCON  ,'IECODCON',this.w_IECODCON  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ANDESCRI = space(60)
        .w_ANPARTSN = space(1)
        .w_RADESCRI = space(50)
        .w_DFDESCRI = space(60)
        .w_CODAZI = i_Codazi
        .w_DFEXPMOV = space(1)
        .w_ANESCDOF = space(1)
        .w_IETIPCON = NVL(IETIPCON,space(1))
        .w_IECODCON = NVL(IECODCON,space(15))
          if link_1_2_joined
            this.w_IECODCON = NVL(ANCODICE102,NVL(this.w_IECODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI102,space(60))
            this.w_ANPARTSN = NVL(ANPARTSN102,space(1))
            this.w_ANESCDOF = NVL(ANESCDOF102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_IEFLGSCA = NVL(IEFLGSCA,space(1))
        .w_IEFLPROV = NVL(IEFLPROV,space(1))
        .w_IERATING = NVL(IERATING,space(2))
          if link_1_5_joined
            this.w_IERATING = NVL(RACODICE105,NVL(this.w_IERATING,space(2)))
            this.w_RADESCRI = NVL(RADESCRI105,space(50))
          else
          .link_1_5('Load')
          endif
        .w_IEGIORIT = NVL(IEGIORIT,0)
        .w_IEVOCFIN = NVL(IEVOCFIN,space(6))
          if link_1_14_joined
            this.w_IEVOCFIN = NVL(DFVOCFIN114,NVL(this.w_IEVOCFIN,space(6)))
            this.w_DFDESCRI = NVL(DFDESCRI114,space(60))
          else
          .link_1_14('Load')
          endif
          .link_1_16('Load')
        cp_LoadRecExtFlds(this,'DOCIXCON')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IETIPCON = space(1)
      .w_IECODCON = space(15)
      .w_IEFLGSCA = space(1)
      .w_IEFLPROV = space(1)
      .w_IERATING = space(2)
      .w_IEGIORIT = 0
      .w_ANDESCRI = space(60)
      .w_ANPARTSN = space(1)
      .w_RADESCRI = space(50)
      .w_IEVOCFIN = space(6)
      .w_DFDESCRI = space(60)
      .w_CODAZI = space(5)
      .w_DFEXPMOV = space(1)
      .w_ANESCDOF = space(1)
      if .cFunction<>"Filter"
        .w_IETIPCON = 'G'
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_IECODCON))
          .link_1_2('Full')
          endif
        .w_IEFLGSCA = ' '
        .w_IEFLPROV = 'N'
        .w_IERATING = Space(2)
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_IERATING))
          .link_1_5('Full')
          endif
        .w_IEGIORIT = 0
          .DoRTCalc(7,9,.f.)
        .w_IEVOCFIN = Space(6)
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_IEVOCFIN))
          .link_1_14('Full')
          endif
          .DoRTCalc(11,11,.f.)
        .w_CODAZI = i_Codazi
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_CODAZI))
          .link_1_16('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOCIXCON')
    this.DoRTCalc(13,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oIECODCON_1_2.enabled = i_bVal
      .Page1.oPag.oIEFLGSCA_1_3.enabled = i_bVal
      .Page1.oPag.oIEFLPROV_1_4.enabled = i_bVal
      .Page1.oPag.oIERATING_1_5.enabled = i_bVal
      .Page1.oPag.oIEGIORIT_1_6.enabled = i_bVal
      .Page1.oPag.oIEVOCFIN_1_14.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oIECODCON_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oIECODCON_1_2.enabled = .t.
      endif
    endwith
    this.Gsdf_Mpe.SetStatus(i_cOp)
    this.Gsdf_Mpi.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DOCIXCON',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.Gsdf_Mpe.SetChildrenStatus(i_cOp)
  *  this.Gsdf_Mpi.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOCIXCON_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IETIPCON,"IETIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IECODCON,"IECODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IEFLGSCA,"IEFLGSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IEFLPROV,"IEFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IERATING,"IERATING",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IEGIORIT,"IEGIORIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IEVOCFIN,"IEVOCFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOCIXCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCIXCON_IDX,2])
    i_lTable = "DOCIXCON"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DOCIXCON_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOCIXCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCIXCON_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DOCIXCON_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DOCIXCON
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOCIXCON')
        i_extval=cp_InsertValODBCExtFlds(this,'DOCIXCON')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(IETIPCON,IECODCON,IEFLGSCA,IEFLPROV,IERATING"+;
                  ",IEGIORIT,IEVOCFIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_IETIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_IECODCON)+;
                  ","+cp_ToStrODBC(this.w_IEFLGSCA)+;
                  ","+cp_ToStrODBC(this.w_IEFLPROV)+;
                  ","+cp_ToStrODBCNull(this.w_IERATING)+;
                  ","+cp_ToStrODBC(this.w_IEGIORIT)+;
                  ","+cp_ToStrODBCNull(this.w_IEVOCFIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOCIXCON')
        i_extval=cp_InsertValVFPExtFlds(this,'DOCIXCON')
        cp_CheckDeletedKey(i_cTable,0,'IETIPCON',this.w_IETIPCON,'IECODCON',this.w_IECODCON)
        INSERT INTO (i_cTable);
              (IETIPCON,IECODCON,IEFLGSCA,IEFLPROV,IERATING,IEGIORIT,IEVOCFIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_IETIPCON;
                  ,this.w_IECODCON;
                  ,this.w_IEFLGSCA;
                  ,this.w_IEFLPROV;
                  ,this.w_IERATING;
                  ,this.w_IEGIORIT;
                  ,this.w_IEVOCFIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DOCIXCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCIXCON_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DOCIXCON_IDX,i_nConn)
      *
      * update DOCIXCON
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DOCIXCON')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " IEFLGSCA="+cp_ToStrODBC(this.w_IEFLGSCA)+;
             ",IEFLPROV="+cp_ToStrODBC(this.w_IEFLPROV)+;
             ",IERATING="+cp_ToStrODBCNull(this.w_IERATING)+;
             ",IEGIORIT="+cp_ToStrODBC(this.w_IEGIORIT)+;
             ",IEVOCFIN="+cp_ToStrODBCNull(this.w_IEVOCFIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DOCIXCON')
        i_cWhere = cp_PKFox(i_cTable  ,'IETIPCON',this.w_IETIPCON  ,'IECODCON',this.w_IECODCON  )
        UPDATE (i_cTable) SET;
              IEFLGSCA=this.w_IEFLGSCA;
             ,IEFLPROV=this.w_IEFLPROV;
             ,IERATING=this.w_IERATING;
             ,IEGIORIT=this.w_IEGIORIT;
             ,IEVOCFIN=this.w_IEVOCFIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- Gsdf_Mpe : Saving
      this.Gsdf_Mpe.ChangeRow(this.cRowID+'      1',0;
             ,this.w_IETIPCON,"PETIPCON";
             ,this.w_IECODCON,"PECODCON";
             )
      this.Gsdf_Mpe.mReplace()
      * --- Gsdf_Mpi : Saving
      this.Gsdf_Mpi.ChangeRow(this.cRowID+'      1',0;
             ,this.w_IETIPCON,"IMTIPCON";
             ,this.w_IECODCON,"IMCODCON";
             )
      this.Gsdf_Mpi.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- Gsdf_Mpe : Deleting
    this.Gsdf_Mpe.ChangeRow(this.cRowID+'      1',0;
           ,this.w_IETIPCON,"PETIPCON";
           ,this.w_IECODCON,"PECODCON";
           )
    this.Gsdf_Mpe.mDelete()
    * --- Gsdf_Mpi : Deleting
    this.Gsdf_Mpi.ChangeRow(this.cRowID+'      1',0;
           ,this.w_IETIPCON,"IMTIPCON";
           ,this.w_IECODCON,"IMCODCON";
           )
    this.Gsdf_Mpi.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOCIXCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCIXCON_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DOCIXCON_IDX,i_nConn)
      *
      * delete DOCIXCON
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'IETIPCON',this.w_IETIPCON  ,'IECODCON',this.w_IECODCON  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOCIXCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCIXCON_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_IECODCON<>.w_IECODCON
            .w_IEFLGSCA = ' '
        endif
        .DoRTCalc(4,4,.t.)
        if .o_IECODCON<>.w_IECODCON
            .w_IERATING = Space(2)
          .link_1_5('Full')
        endif
        if .o_IECODCON<>.w_IECODCON
            .w_IEGIORIT = 0
        endif
        .DoRTCalc(7,9,.t.)
        if .o_IECODCON<>.w_IECODCON
            .w_IEVOCFIN = Space(6)
          .link_1_14('Full')
        endif
        if  .o_IETIPCON<>.w_IETIPCON
          .WriteTo_Gsdf_Mpe()
        endif
        .DoRTCalc(11,11,.t.)
          .link_1_16('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIEFLGSCA_1_3.enabled = this.oPgFrm.Page1.oPag.oIEFLGSCA_1_3.mCond()
    this.oPgFrm.Page1.oPag.oIERATING_1_5.enabled = this.oPgFrm.Page1.oPag.oIERATING_1_5.mCond()
    this.oPgFrm.Page1.oPag.oIEGIORIT_1_6.enabled = this.oPgFrm.Page1.oPag.oIEGIORIT_1_6.mCond()
    this.oPgFrm.Page1.oPag.oIEVOCFIN_1_14.enabled = this.oPgFrm.Page1.oPag.oIEVOCFIN_1_14.mCond()
    this.Gsdf_Mpe.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_1.mCond()
    this.Gsdf_Mpi.enabled = this.oPgFrm.Page3.oPag.oLinkPC_3_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(3).enabled=not(this.w_IEFLPROV='S')
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IECODCON
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IECODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_IECODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IETIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANESCDOF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_IETIPCON;
                     ,'ANCODICE',trim(this.w_IECODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANESCDOF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IECODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IECODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oIECODCON_1_2'),i_cWhere,'GSAR_BZC',"Conti",'Gsdf_Acc.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_IETIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANESCDOF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANESCDOF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANESCDOF";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_IETIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANESCDOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IECODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANESCDOF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_IECODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_IETIPCON;
                       ,'ANCODICE',this.w_IECODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANESCDOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IECODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(60))
      this.w_ANPARTSN = NVL(_Link_.ANPARTSN,space(1))
      this.w_ANESCDOF = NVL(_Link_.ANESCDOF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_IECODCON = space(15)
      endif
      this.w_ANDESCRI = space(60)
      this.w_ANPARTSN = space(1)
      this.w_ANESCDOF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_Anescdof='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto incongruente")
        endif
        this.w_IECODCON = space(15)
        this.w_ANDESCRI = space(60)
        this.w_ANPARTSN = space(1)
        this.w_ANESCDOF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IECODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.ANCODICE as ANCODICE102"+ ",link_1_2.ANDESCRI as ANDESCRI102"+ ",link_1_2.ANPARTSN as ANPARTSN102"+ ",link_1_2.ANESCDOF as ANESCDOF102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on DOCIXCON.IECODCON=link_1_2.ANCODICE"+" and DOCIXCON.IETIPCON=link_1_2.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and DOCIXCON.IECODCON=link_1_2.ANCODICE(+)"'+'+" and DOCIXCON.IETIPCON=link_1_2.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IERATING
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_lTable = "DORATING"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2], .t., this.DORATING_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IERATING) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsdf_Ara',True,'DORATING')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RACODICE like "+cp_ToStrODBC(trim(this.w_IERATING)+"%");

          i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RACODICE',trim(this.w_IERATING))
          select RACODICE,RADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IERATING)==trim(_Link_.RACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IERATING) and !this.bDontReportError
            deferred_cp_zoom('DORATING','*','RACODICE',cp_AbsName(oSource.parent,'oIERATING_1_5'),i_cWhere,'Gsdf_Ara',"Rating",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',oSource.xKey(1))
            select RACODICE,RADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IERATING)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(this.w_IERATING);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',this.w_IERATING)
            select RACODICE,RADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IERATING = NVL(_Link_.RACODICE,space(2))
      this.w_RADESCRI = NVL(_Link_.RADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IERATING = space(2)
      endif
      this.w_RADESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])+'\'+cp_ToStr(_Link_.RACODICE,1)
      cp_ShowWarn(i_cKey,this.DORATING_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IERATING Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DORATING_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.RACODICE as RACODICE105"+ ",link_1_5.RADESCRI as RADESCRI105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on DOCIXCON.IERATING=link_1_5.RACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and DOCIXCON.IERATING=link_1_5.RACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IEVOCFIN
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_FINZ_IDX,3]
    i_lTable = "VOC_FINZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2], .t., this.VOC_FINZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IEVOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDF_AVF',True,'VOC_FINZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DFVOCFIN like "+cp_ToStrODBC(trim(this.w_IEVOCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DFVOCFIN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DFVOCFIN',trim(this.w_IEVOCFIN))
          select DFVOCFIN,DFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DFVOCFIN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IEVOCFIN)==trim(_Link_.DFVOCFIN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DFDESCRI like "+cp_ToStrODBC(trim(this.w_IEVOCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DFDESCRI like "+cp_ToStr(trim(this.w_IEVOCFIN)+"%");

            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IEVOCFIN) and !this.bDontReportError
            deferred_cp_zoom('VOC_FINZ','*','DFVOCFIN',cp_AbsName(oSource.parent,'oIEVOCFIN_1_14'),i_cWhere,'GSDF_AVF',"Voci finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',oSource.xKey(1))
            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IEVOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(this.w_IEVOCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',this.w_IEVOCFIN)
            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IEVOCFIN = NVL(_Link_.DFVOCFIN,space(6))
      this.w_DFDESCRI = NVL(_Link_.DFDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_IEVOCFIN = space(6)
      endif
      this.w_DFDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])+'\'+cp_ToStr(_Link_.DFVOCFIN,1)
      cp_ShowWarn(i_cKey,this.VOC_FINZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IEVOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_FINZ_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.DFVOCFIN as DFVOCFIN114"+ ",link_1_14.DFDESCRI as DFDESCRI114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on DOCIXCON.IEVOCFIN=link_1_14.DFVOCFIN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and DOCIXCON.IEVOCFIN=link_1_14.DFVOCFIN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
    i_lTable = "DOCFINPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2], .t., this.DOCFINPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFCODAZI,DFEXPMOV";
                   +" from "+i_cTable+" "+i_lTable+" where DFCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFCODAZI',this.w_CODAZI)
            select DFCODAZI,DFEXPMOV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.DFCODAZI,space(5))
      this.w_DFEXPMOV = NVL(_Link_.DFEXPMOV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_DFEXPMOV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])+'\'+cp_ToStr(_Link_.DFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DOCFINPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIECODCON_1_2.value==this.w_IECODCON)
      this.oPgFrm.Page1.oPag.oIECODCON_1_2.value=this.w_IECODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oIEFLGSCA_1_3.RadioValue()==this.w_IEFLGSCA)
      this.oPgFrm.Page1.oPag.oIEFLGSCA_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIEFLPROV_1_4.RadioValue()==this.w_IEFLPROV)
      this.oPgFrm.Page1.oPag.oIEFLPROV_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIERATING_1_5.value==this.w_IERATING)
      this.oPgFrm.Page1.oPag.oIERATING_1_5.value=this.w_IERATING
    endif
    if not(this.oPgFrm.Page1.oPag.oIEGIORIT_1_6.value==this.w_IEGIORIT)
      this.oPgFrm.Page1.oPag.oIEGIORIT_1_6.value=this.w_IEGIORIT
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_9.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_9.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRADESCRI_1_12.value==this.w_RADESCRI)
      this.oPgFrm.Page1.oPag.oRADESCRI_1_12.value=this.w_RADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oIEVOCFIN_1_14.value==this.w_IEVOCFIN)
      this.oPgFrm.Page1.oPag.oIEVOCFIN_1_14.value=this.w_IEVOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDESCRI_1_15.value==this.w_DFDESCRI)
      this.oPgFrm.Page1.oPag.oDFDESCRI_1_15.value=this.w_DFDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'DOCIXCON')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_IECODCON)) or not(.w_Anescdof='N'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIECODCON_1_2.SetFocus()
            i_bnoObbl = !empty(.w_IECODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto incongruente")
        endcase
      endif
      *i_bRes = i_bRes .and. .Gsdf_Mpe.CheckForm()
      if i_bres
        i_bres=  .Gsdf_Mpe.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .Gsdf_Mpi.CheckForm()
      if i_bres
        i_bres=  .Gsdf_Mpi.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IETIPCON = this.w_IETIPCON
    this.o_IECODCON = this.w_IECODCON
    * --- Gsdf_Mpe : Depends On
    this.Gsdf_Mpe.SaveDependsOn()
    * --- Gsdf_Mpi : Depends On
    this.Gsdf_Mpi.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsdf_accPag1 as StdContainer
  Width  = 681
  height = 279
  stdWidth  = 681
  stdheight = 279
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIECODCON_1_2 as StdField with uid="SJOFCVWWNZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IECODCON", cQueryName = "IETIPCON,IECODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto incongruente",;
    ToolTipText = "Codice conto",;
    HelpContextID = 251050540,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=92, Top=15, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_IETIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_IECODCON"

  func oIECODCON_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oIECODCON_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIECODCON_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_IETIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_IETIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oIECODCON_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'Gsdf_Acc.CONTI_VZM',this.parent.oContained
  endproc
  proc oIECODCON_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_IETIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_IECODCON
     i_obj.ecpSave()
  endproc

  add object oIEFLGSCA_1_3 as StdCheck with uid="JPHQOETLSW",rtseq=3,rtrep=.f.,left=184, top=57, caption="Scadenzario",;
    ToolTipText = "Se attivo il conto contabile viene incluso nell'esportazione delle scadenze",;
    HelpContextID = 20346311,;
    cFormVar="w_IEFLGSCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIEFLGSCA_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oIEFLGSCA_1_3.GetRadio()
    this.Parent.oContained.w_IEFLGSCA = this.RadioValue()
    return .t.
  endfunc

  func oIEFLGSCA_1_3.SetRadio()
    this.Parent.oContained.w_IEFLGSCA=trim(this.Parent.oContained.w_IEFLGSCA)
    this.value = ;
      iif(this.Parent.oContained.w_IEFLGSCA=='S',1,;
      0)
  endfunc

  func oIEFLGSCA_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Anpartsn='S')
    endwith
   endif
  endfunc

  add object oIEFLPROV_1_4 as StdCheck with uid="INRVEQCIQK",rtseq=4,rtrep=.f.,left=342, top=57, caption="Primanota provvisoria",;
    ToolTipText = "Se attivo, tutti i movimenti di primanota relativi al conto verranno importati con stato provvisorio",;
    HelpContextID = 255429156,;
    cFormVar="w_IEFLPROV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIEFLPROV_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIEFLPROV_1_4.GetRadio()
    this.Parent.oContained.w_IEFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oIEFLPROV_1_4.SetRadio()
    this.Parent.oContained.w_IEFLPROV=trim(this.Parent.oContained.w_IEFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_IEFLPROV=='S',1,;
      0)
  endfunc

  add object oIERATING_1_5 as StdField with uid="JDUXBOBACW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IERATING", cQueryName = "IERATING",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Livello di importanza per i pagamenti oppure livello di esigibilitÓ per gli incassi",;
    HelpContextID = 134466099,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=184, Top=100, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="DORATING", cZoomOnZoom="Gsdf_Ara", oKey_1_1="RACODICE", oKey_1_2="this.w_IERATING"

  func oIERATING_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Anpartsn='S')
    endwith
   endif
  endfunc

  func oIERATING_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oIERATING_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIERATING_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DORATING','*','RACODICE',cp_AbsName(this.parent,'oIERATING_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsdf_Ara',"Rating",'',this.parent.oContained
  endproc
  proc oIERATING_1_5.mZoomOnZoom
    local i_obj
    i_obj=Gsdf_Ara()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RACODICE=this.parent.oContained.w_IERATING
     i_obj.ecpSave()
  endproc

  add object oIEGIORIT_1_6 as StdField with uid="NAQITUCIVO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_IEGIORIT", cQueryName = "IEGIORIT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di ritardo su pagamenti e incassi",;
    HelpContextID = 256670246,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=184, Top=143, cSayPict='"999"', cGetPict='"999"'

  func oIEGIORIT_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Anpartsn='S')
    endwith
   endif
  endfunc

  add object oANDESCRI_1_9 as StdField with uid="GQPDWHLBLW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 32464463,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=231, Top=15, InputMask=replicate('X',60)

  add object oRADESCRI_1_12 as StdField with uid="LKXITUNVVR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_RADESCRI", cQueryName = "RADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 32461407,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=255, Top=100, InputMask=replicate('X',50)

  add object oIEVOCFIN_1_14 as StdField with uid="LSGWGQGNWC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IEVOCFIN", cQueryName = "IEVOCFIN",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Voce finanziaria",;
    HelpContextID = 66745812,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=184, Top=186, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VOC_FINZ", cZoomOnZoom="GSDF_AVF", oKey_1_1="DFVOCFIN", oKey_1_2="this.w_IEVOCFIN"

  func oIEVOCFIN_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Anpartsn='S')
    endwith
   endif
  endfunc

  func oIEVOCFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oIEVOCFIN_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIEVOCFIN_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_FINZ','*','DFVOCFIN',cp_AbsName(this.parent,'oIEVOCFIN_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDF_AVF',"Voci finanziarie",'',this.parent.oContained
  endproc
  proc oIEVOCFIN_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSDF_AVF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DFVOCFIN=this.parent.oContained.w_IEVOCFIN
     i_obj.ecpSave()
  endproc

  add object oDFDESCRI_1_15 as StdField with uid="LIZWSGVYPA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DFDESCRI", cQueryName = "DFDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 32462463,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=255, Top=186, InputMask=replicate('X',60)

  add object oStr_1_7 as StdString with uid="LDQYLNVNSW",Visible=.t., Left=36, Top=145,;
    Alignment=1, Width=143, Height=18,;
    Caption="Giorni di ritardo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="KJZQMUPLNK",Visible=.t., Left=27, Top=16,;
    Alignment=1, Width=60, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="ILIQAINTSU",Visible=.t., Left=102, Top=101,;
    Alignment=1, Width=77, Height=18,;
    Caption="Rating:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VPDDBFRBYL",Visible=.t., Left=54, Top=188,;
    Alignment=1, Width=125, Height=18,;
    Caption="Voce finanziaria:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsdf_accPag2 as StdContainer
  Width  = 681
  height = 279
  stdWidth  = 681
  stdheight = 279
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="SINTEIDHHA",left=5, top=23, width=672, height=230, bOnScreen=.t.;


  func oLinkPC_2_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DFEXPMOV='S')
      endwith
    endif
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsdf_mpe",lower(this.oContained.Gsdf_Mpe.class))=0
        this.oContained.Gsdf_Mpe.createrealchild()
        this.oContained.WriteTo_Gsdf_Mpe()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsdf_accPag3 as StdContainer
  Width  = 681
  height = 279
  stdWidth  = 681
  stdheight = 279
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="SQNUPYJRZM",left=13, top=38, width=496, height=237, bOnScreen=.t.;


  func oLinkPC_3_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_IEFLPROV='N')
      endwith
    endif
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsdf_mpi",lower(this.oContained.Gsdf_Mpi.class))=0
        this.oContained.Gsdf_Mpi.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_acc','DOCIXCON','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IETIPCON=DOCIXCON.IETIPCON";
  +" and "+i_cAliasName2+".IECODCON=DOCIXCON.IECODCON";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
