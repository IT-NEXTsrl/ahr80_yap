* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_bpe                                                        *
*              Visualizzazione causali contabili                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-05                                                      *
* Last revis.: 2012-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdf_bpe",oParentObject,m.pParame)
return(i_retval)

define class tgsdf_bpe as StdBatch
  * --- Local variables
  pParame = space(1)
  w_Cccodice = space(5)
  w_Ccdescri = space(35)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine lanciata per gestire lo zoom del campo "Pecodice" nella movimentazione
    *     Gsdf_Mpe.
    *     Verifico se � stato attivato il flag "Trascodifica causali DocFinance"
    if this.pParame $ "CP"
      if This.oParentObject.cFunction="Edit" And This.oParentObject.RowStatus()<>"A"
        Ah_ErrorMsg("Non � possibile modificare la causale contabile%0Cancellare la riga (F6) e ricrearne una nuova")
        if this.pParame="C"
          this.oParentObject.w_Pecodice = This.oParentObject.o_Pecodice
        else
          this.oParentObject.w_Imcodice = This.oParentObject.o_Imcodice
          this.oParentObject.w_Caudescri = This.oParentObject.o_Caudescri
        endif
      endif
    else
      if this.oParentObject.w_Dftrasca="S"
        vx_exec("..\Isdf\Exe\Query\GsdfTMpe.Vzm",this)
      else
        vx_exec("..\Isdf\Exe\Query\Gsdf_Mpe.Vzm",this)
      endif
      if this.pParame="I"
        this.oParentObject.w_Imcodice = this.w_Cccodice
        this.oParentObject.w_Caudescri = this.w_Ccdescri
      else
        this.oParentObject.w_Pecodice = this.w_Cccodice
      endif
    endif
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
