* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_bmt                                                        *
*              Importazione movimenti primanota DocFinance                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-07-02                                                      *
* Last revis.: 2016-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TIPOIMPORT,w_GENERA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdf_bmt",oParentObject,m.w_TIPOIMPORT,m.w_GENERA)
return(i_retval)

define class tgsdf_bmt as StdBatch
  * --- Local variables
  w_DINUMERO = 0
  w_EX_SEGNO = space(1)
  w_TIPOIMPORT = space(10)
  w_GENERA = space(10)
  w_DATIEL = space(2000)
  w_CONTARECORD = 0
  w_SEGNO = space(1)
  w_DFORAINI = ctot("")
  w_NUMERATORE = space(4)
  w_ANNO = space(4)
  w_MOVIMENTO = 0
  w_CHIAVEROTTURA = 0
  w_PNDATREG = ctod("  /  /  ")
  w_BANCA = space(8)
  w_RBN = space(4)
  w_PNCODCAU = space(4)
  w_PNCODVAL = space(5)
  w_PNIMPAVE = 0
  w_PNIMPDAR = 0
  w_CCCODVAL = space(5)
  w_CONTROVALORE = 0
  w_CCDATVAL = ctod("  /  /  ")
  w_CADESRIG = space(40)
  w_NUMPARDIS = 0
  w_ANTIPCON = space(16)
  w_ANCODICE = space(16)
  w_SEZIONEAZIENDALE = space(6)
  w_DESOP = space(40)
  w_PTNUMDOC = 0
  w_PTTOTIMP = 0
  w_PTDATDOC = ctod("  /  /  ")
  w_CCCAOVAT = 0
  w_CHIAVEMOV = space(0)
  w_PTNUMEFF = 0
  w_Corretto = .f.
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_RIGA = 0
  w_CCCONTR = space(22)
  w_CCPIANOC = space(16)
  w_CONTAMOV = 0
  w_CONTAINS = 0
  w_CONTADOPPI = 0
  w_DOPPIO = .f.
  w_CONTROVALORECRE = 0
  w_CONTROVALOREDEB = 0
  w_DIVISA = space(3)
  w_ErrImp = .f.
  w_ERRORLOG = space(0)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PNSERIAL = space(10)
  w_RIGADETT = .f.
  w_LETTO = space(10)
  w_MOV_LETTO = space(10)
  w_CPROWORD = 0
  w_PNCODESE = space(4)
  w_CODESE = space(4)
  w_SALINI = space(1)
  w_CODCON = space(15)
  w_FLOBCO = space(1)
  w_TIPOPE = space(1)
  w_CCSTATUS = space(0)
  w_TIPMOV = space(1)
  w_CODVAS = space(3)
  w_CATIPSCS = space(1)
  w_CATIPDIS = space(1)
  w_CAUCON = space(5)
  w_CCNUMCOR = space(15)
  w_PNNUMRER = 0
  w_CreVarDat = space(10)
  w_CALFIR = space(3)
  w_CAFLEFES = space(1)
  w_DATAR = ctod("  /  /  ")
  w_CAMOVACC = space(1)
  w_ULT_RNUM = 0
  w_ULT_RNUMCP = 0
  w_ULT_RNUMPA = 0
  w_PNFLSALD = space(1)
  w_PNFLSALI = space(1)
  w_PNFLSALF = space(1)
  w_PNDESRIG = space(50)
  w_CCCODRAG = space(20)
  w_CAFLPART = space(1)
  w_CAFLPART = space(1)
  w_CAMBIO = 0
  w_AZIENDA = space(10)
  w_RIGAMOV = 0
  w_COPIAFILE = .f.
  w_CREANUOVAPARTITA = .f.
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTMODPAG = space(10)
  w_PTBANAPP = space(10)
  w_PTNUMCOR = space(25)
  w_PTBANNOS = space(15)
  w_PTDATAPE = ctod("  /  /  ")
  w_PTALFDOC = space(2)
  w_PTFLIMPE = space(1)
  w_PTCAOAPE = 0
  w_MPTIPPAG = space(2)
  w_PTNUMPAR = space(14)
  w_PTCODVAL = space(2)
  w_PT_SEGNO = space(1)
  w_PTFLSOSP = space(1)
  w_PTNUMDIS = space(10)
  w_PTCODAGE = space(5)
  w_ptdesrig = space(40)
  w_CAMBIOPAR = 0
  w_PTDATSCA = ctod("  /  /  ")
  w_PTFLRITE = 0
  w_PTINESPO = space(1)
  w_VABENE = .f.
  w_DATBLO = ctod("  /  /  ")
  w_MAXDAT = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_DFCODAZI = space(1)
  w_DFCSERIA = space(10)
  w_FILEBACKUP = space(150)
  w_DFFLGERR = space(1)
  w_DFCIMPEX = space(0)
  w_FLSALI = space(1)
  w_FLSALF = space(1)
  w_CALDOC = space(1)
  w_FLANAL = space(1)
  w_PNFLIVDF = space(1)
  w_PNTIPDOC = space(1)
  w_PNTIPREG = space(1)
  w_PNNUMREG = space(1)
  w_FLPPRO = space(1)
  w_FLPDOC = space(1)
  w_FLRIFE = space(1)
  w_FLPDOC = space(1)
  w_FLPDOC = space(1)
  w_MANCA_ANALITICA = .f.
  w_Correttolog = .f.
  w_CCFLPART = space(1)
  w_PNCODUTE = 0
  w_PNCOMPET = space(4)
  w_PNPRG = ctod("  /  /  ")
  w_ANPARTSN = space(1)
  w_APPOGGIO = 0
  w_CCFLPARTOLD = space(1)
  w_CONSUP = space(15)
  w_ANCCTAGG = space(1)
  w_SEZBIL = space(1)
  w_COSAPAGA = space(10)
  w_PNCODPAG = space(5)
  w_COSTORIC = space(1)
  w_CHIAVEFILE = space(6)
  w_DFIMPVAL = 0
  w_DFPTTIPO = space(1)
  w_DFMODPAG = space(10)
  w_DFDATSCA = ctod("  /  /  ")
  w_DFFLGDIS = space(1)
  w_EFFETTO = 0
  w_ANFLACBD = space(1)
  w_AZFLVEBD = space(1)
  w_PTFLRAGG = space(1)
  NoName = space(0)
  w_PTFLVABD = space(1)
  w_NURATE = 0
  w_GIORN1 = 0
  w_GIOFIS = 0
  w_GIORN2 = 0
  w_MESE1 = 0
  w_MESE = 0
  w_ESCL2 = space(4)
  w_ESCL1 = space(4)
  w_LOOP = 0
  w_CONTAAZ = 0
  w_CCCONTRTESTATA = space(22)
  w_CCPIANOCTESTATA = space(16)
  w_FLAGDISTINTATESTATA = space(1)
  w_LETTODOCFIN = space(10)
  w_CONTARECORDDOCFIN = 0
  w_DFCHIAVE = space(6)
  w_TRCAUDIS = space(4)
  w_TRTIPCAU = space(1)
  w_CCCODISO = space(5)
  w_PNCODVALDOCF = space(5)
  w_DFIMPVAL = 0
  w_FLAG_PROV_CONTO = space(1)
  w_FLAG_PROV_CAUS = space(1)
  w_FLAG_PROV = space(1)
  w_COSTORIC = space(1)
  w_DFCODICECTR = space(10)
  w_CONTA = 0
  w_CATIPDIS = space(2)
  w_CATIPSCD = space(1)
  w_CAUTESADHOC = space(5)
  w_CBCODBAN = space(15)
  w_CACONSBF = space(1)
  w_CADATEFF = space(1)
  w_EXCODVOC = space(15)
  w_EXCODCEN = space(15)
  w_MRPARAME = 0
  w_TOTPAR = 0
  w_ULT_RNUMAN = 0
  w_IMPTOT = 0
  w_MRTOTIMP = 0
  w_RTOT = 0
  * --- WorkFile variables
  COC_MAST_idx=0
  VALUTE_idx=0
  CAU_CONT_idx=0
  CONTI_idx=0
  PAG_2AME_idx=0
  PAR_TITE_idx=0
  SALDICON_idx=0
  DOCFINIM_idx=0
  DOCFINLG_idx=0
  PNT_MAST_idx=0
  MASTRI_idx=0
  PNT_DETT_idx=0
  DIS_TINT_idx=0
  CAU_DIST_idx=0
  MOVICOST_idx=0
  COLLCENT_idx=0
  DOCTRCAU_idx=0
  DOCIXCON_idx=0
  DOCIXIMP_idx=0
  CONTROPA_idx=0
  AZIENDA_idx=0
  ESERCIZI_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- w_TIPOIMPORT  indica se bisogna fre l importazione da file o generare i soli movimenti di primanota dalla tabella di preelaborazione
    *     w_TIPOIMPORT = 'DAFILE'
    *     w_TIPOIMPORT='DATABELLA'
    * --- w_GENERA indica se generare i movimenti di primanota oppure si fare solo la simulazione dell import in modo da raccogliere eventuali indicazioni di errori nell insert
    *     w_GENERA='S' genera i movimenti di primanota
    *     w_GENERA='N' non genera i movimenti di primanota
    * --- w_SOVRASCRIVI indica se sovrascrivere automaticamente la tabella di preelaborazione se il record � gi� presente
    this.w_DFORAINI = time()
    this.w_Correttolog = .T.
    * --- usata per indicare che si sta elaborando un movimento che � gi� presente sul database (importato precedentemente)
    this.w_DOPPIO = .F.
    this.w_CONTAMOV = 0
    * --- Conta i movimenti inseriti
    this.w_CONTAINS = 0
    * --- Conta i movimenti non inseriti poich� gi� presenti sul database (importati precedentemetne)
    this.w_CONTADOPPI = 0
    * --- Inserisce la testata del movimento di primanota
    * --- Flag saldo iniziale
    * --- Tipo movimento (M)
    * --- Tipo movimento
    * --- Filtro valuta
    * --- Causale contabile
    * --- Data di riferimento valuta
    this.w_COPIAFILE = .F.
    * --- Codice azienda non specificato Impossibile caricare il file
    if EMPTY (this.oParentObject.w_DFTRAAZI)
      AH_ERRORMSG ("Codice azienda non specificato nei dati generali DocFinance ")
      i_retcode = 'stop'
      return
    endif
    * --- Abilita l importazione DocFinance esclusivamente ad un solo utenet (piu utenti non possono importare contemopopraneamente)
    this.w_CODAZI = i_CODAZI
    this.w_MAXDAT = i_datsys
    this.w_VABENE = .T.
    * --- Try
    local bErr_031C1900
    bErr_031C1900=bTrsErr
    this.Try_031C1900()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("%1",,"",i_errmsg)
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_031C1900
    * --- End
    this.oParentObject.w_Msg = ""
    this.w_CONTARECORD = 0
    this.w_CHIAVEMOV = "@@@"
    * --- w_corretto viene impostata a .f. se si verifica un errore 
    this.w_Corretto = .T.
    this.w_ErrImp = .t.
    this.w_RIGAMOV = 0
    * --- Recupera il codice dell Azienda
    this.w_RIGA = 0
    * --- Recupera seriale per il log di elaborazione (il seriale verr� scritto nella tabella di mocimenti DocFinance per mantenere il legame tra il movimento e  l ultimo log )
    this.w_DFCODAZI = this.oParentObject.w_DFTRAAZI
    this.w_DFCSERIA = space(10)
    * --- Try
    local bErr_031BEC30
    bErr_031BEC30=bTrsErr
    this.Try_031BEC30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_errormsg( "Il log non verr� salvato poich� si � verificato un errore nella generazione del seriale del log: %1",48,0,MESSAGE())
    endif
    bTrsErr=bTrsErr or bErr_031BEC30
    * --- End
    do case
      case this.w_TIPOIMPORT="DAFILE"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_GENERA="S"
          this.w_DFCIMPEX = "X"
        else
          this.w_DFCIMPEX = "I"
        endif
      case this.w_TIPOIMPORT="DATABELLA"
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_DFCIMPEX = "G"
    endcase
    if not this.w_ErrImp
      ADDMSGNL("%0%1",THIS,ah_msgformat("Importazione terminata con errori, verificare il file di log."))
    endif
    if this.w_GENERA="S"
      ADDMSGNL("%0%0%1 Movimenti elaborati",THIS, this.w_CONTAMOV ) 
 ADDMSGNL("%1 Movimenti inseriti",THIS,this.w_CONTAINS ) 
 ADDMSGNL("%1 Movimenti scartati perch� gi� importati precedentemente",THIS, this.w_CONTADOPPI)
    endif
    if USED ("CursTEMPO")
      SELECT CursTEMPO 
 USE
    endif
    * --- -- Fa copia di backup del file e cancella originale
    i_err=On("ERROR") 
 on error btrserr=.t.
    if this.w_COPIAFILE
      if NOT EMPTY (alltrim(justfname(this.oParentObject.w_PATH))) AND FILE (this.oParentObject.w_PATH) AND DIRECTORY (Justpath (this.oParentObject.w_BACKUP))
        this.w_FILEBACKUP = alltrim(Justpath (this.oParentObject.w_BACKUP)) + "\"+alltrim(juststem(this.oParentObject.w_PATH))
        this.w_FILEBACKUP = this.w_FILEBACKUP++"_"+dtos(date())+"_"+strtran(time(),":") +"."+alltrim (JUSTEXT(this.oParentObject.w_PATH))
        COPY FILE (this.oParentObject.w_PATH) to (this.w_FILEBACKUP)
        if FILE (this.w_FILEBACKUP)
          ah_errormsg( "Creata copia di backup del file di testo da Doc Finance in %1",48,0, alltrim (Justpath (this.oParentObject.w_BACKUP) ))
          DELETE FILE (this.oParentObject.w_PATH)
        else
          ah_errormsg( "Errore creazione copia di backup del file di testo da Doc Finance",48,0 )
        endif
      endif
      if NOT DIRECTORY (Justpath (this.oParentObject.w_BACKUP))
        ah_errormsg( "Non trovata directory di backup",48,0 )
      endif
    endif
     on error &i_err
    if this.w_GENERA="S"
      if this.w_CONTARECORD=0
        ah_errormsg( "Per l' azienda %1 non ci sono movimenti di primanota da generare",48,0,alltrim (i_codazi ))
      else
        ah_errormsg( "Elaborazione completata %1",48,0,IIF (this.w_CORRETTO,"","con errori, verificare il log degli errori"))
      endif
    else
      if this.w_CONTARECORDDOCFIN=0
        ah_errormsg( "Per l' azienda %1 non ci sono movimenti da importare",48,0,alltrim (i_codazi ))
      else
        ah_errormsg( "Elaborazione completata %1",48,0,IIF (this.w_CORRETTO,"","con errori, verificare il log degli errori"))
      endif
    endif
    * --- Nel caso di importazione inerisce log elaborazione
    this.w_DFFLGERR = IIF (not this.w_Corretto or not this.w_Correttolog, "S" , " ")
    * --- Try
    local bErr_031C2380
    bErr_031C2380=bTrsErr
    this.Try_031C2380()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_errormsg( "Il log dell importazione non � stato salvato a causa dell errore: %1",48,0,MESSAGE())
    endif
    bTrsErr=bTrsErr or bErr_031C2380
    * --- End
    * --- Rimuovo i blocchi
    * --- Stampa Libro giornale e Registri Iva
    * --- Try
    local bErr_031C09D0
    bErr_031C09D0=bTrsErr
    this.Try_031C09D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda",,"")
    endif
    bTrsErr=bTrsErr or bErr_031C09D0
    * --- End
    * --- In alcuni casi (non c � una casistica) lo zoom si sbianca completamente. Reinizializzo lo zoom per aggirare il problema
    this.oparentobject.w_ZOOMMOV.init() 
 this.oparentobject.w_zoommov.event("FormLoad") 
 this.oParentObject.NotifyEvent("Aggiorna")
  endproc
  proc Try_031C1900()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  "))) 
      * --- Inserisce <Blocco> per Primanota
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'AZIENDA','AZDATBLO');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZDATBLO = this.w_MAXDAT;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- UN altro utente ha impostato il blocco - controllo concorrenza
      this.w_VABENE = .F.
      * --- Raise
      i_Error=ah_MsgFormat("Verificare semaforo bollati in dati azienda. Impossibile generare movimenti da Docfinance")
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_031BEC30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.DOCFINLG_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEDLG", "w_DFCODAZI,w_DFCSERIA")
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_031C2380()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.DOCFINLG_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEDLG", "w_DFCODAZI,w_DFCSERIA")
    * --- Insert into DOCFINLG
    i_nConn=i_TableProp[this.DOCFINLG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCFINLG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOCFINLG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DFCSERIA"+",DFCODAZI"+",DFCODUTE"+",DFCDATAE"+",DFORAINI"+",DFORAFIN"+",DFFLGERR"+",DFCIMPEX"+",DFLICLOG"+",DFNOMFIL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DFCSERIA),'DOCFINLG','DFCSERIA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DFTRAAZI),'DOCFINLG','DFCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'DOCFINLG','DFCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DOCFINLG','DFCDATAE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DFORAINI),'DOCFINLG','DFORAINI');
      +","+cp_NullLink(cp_ToStrODBC(time()),'DOCFINLG','DFORAFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DFFLGERR),'DOCFINLG','DFFLGERR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DFCIMPEX),'DOCFINLG','DFCIMPEX');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_Msg),'DOCFINLG','DFLICLOG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PATH),'DOCFINLG','DFNOMFIL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DFCSERIA',this.w_DFCSERIA,'DFCODAZI',this.oParentObject.w_DFTRAAZI,'DFCODUTE',i_codute,'DFCDATAE',i_datsys,'DFORAINI',this.w_DFORAINI,'DFORAFIN',time(),'DFFLGERR',this.w_DFFLGERR,'DFCIMPEX',this.w_DFCIMPEX,'DFLICLOG',this.oParentObject.w_Msg,'DFNOMFIL',this.oParentObject.w_PATH)
      insert into (i_cTable) (DFCSERIA,DFCODAZI,DFCODUTE,DFCDATAE,DFORAINI,DFORAFIN,DFFLGERR,DFCIMPEX,DFLICLOG,DFNOMFIL &i_ccchkf. );
         values (;
           this.w_DFCSERIA;
           ,this.oParentObject.w_DFTRAAZI;
           ,i_codute;
           ,i_datsys;
           ,this.w_DFORAINI;
           ,time();
           ,this.w_DFFLGERR;
           ,this.w_DFCIMPEX;
           ,this.oParentObject.w_Msg;
           ,this.oParentObject.w_PATH;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_031C09D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce la testata del movimento
    if this.w_CHIAVEMOV <> this.w_CHIAVEROTTURA
      * --- Su ogni movimento se non ci sono stati errori si f� la commit
      if this.w_ChiaveMov<>"@@@"
        if this.w_corretto AND this.w_GENERA="S"
          * --- commit
          cp_EndTrs(.t.)
          this.w_CONTAINS = this.w_CONTAINS + 1
        else
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if this.w_GENERA="S"
            if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="A"
              if this.w_DFFLGDIS="D"
                ADDMSGNL("%1%0",THIS,ah_msgformat("Distinta non inserita"))
              else
                ADDMSGNL("%1%0",THIS,ah_msgformat("Movimento di primanota non inserito"))
              endif
            endif
          endif
        endif
      endif
      * --- begin transaction
      cp_BeginTrs()
      if not this.w_corretto
        this.w_Correttolog = .f.
      endif
      this.w_Corretto = .T.
      * --- Controllo se la data registrazione appartiene ad un esercizio
      this.w_PNCODESE = CALCESER(this.w_PNDATREG,Space(4))
      if EMPTY (this.w_PNCODESE)
        if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
          ADDMSGNL("%1",THIS,ah_msgformat( "Data registrazione non appartenente ad alcun esercizio"))
        endif
        this.w_Corretto = .F.
        this.w_ErrImp = .f.
        this.w_ERRORLOG = this.w_ERRORLOG +ah_msgformat( "Data registrazione non appartenente ad alcun esercizio")
      endif
      * --- La variabile w_Mov_Letto indicher� se il movimento � gi� stato caricato in precedenza (in tal caso il movimento, le contropartite e le partite associate al movimento non vengono caricate)
      this.w_CONTAMOV = this.w_CONTAMOV + 1
      this.w_CCCONTRTESTATA = this.w_CCCONTR
      this.w_CCPIANOCTESTATA = this.w_CCPIANOC
      this.w_MOV_LETTO = ""
      * --- Read from DOCFINIM
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOCFINIM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DFSERTES"+;
          " from "+i_cTable+" DOCFINIM where ";
              +"DFCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
              +" and DFMOVIME = "+cp_ToStrODBC(this.w_MOVIMENTO);
              +" and DFNUMERA = "+cp_ToStrODBC(this.w_NUMERATORE);
              +" and DF__ANNO = "+cp_ToStrODBC(this.w_ANNO);
              +" and DFDATREG = "+cp_ToStrODBC(this.w_PNDATREG);
              +" and DFCONTRO = "+cp_ToStrODBC(this.w_CCCONTRTESTATA);
              +" and DFPIANOC = "+cp_ToStrODBC(this.w_CCPIANOCTESTATA);
              +" and DFCHIAVE = "+cp_ToStrODBC(this.w_DFCHIAVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DFSERTES;
          from (i_cTable) where;
              DFCODAZI = this.w_AZIENDA;
              and DFMOVIME = this.w_MOVIMENTO;
              and DFNUMERA = this.w_NUMERATORE;
              and DF__ANNO = this.w_ANNO;
              and DFDATREG = this.w_PNDATREG;
              and DFCONTRO = this.w_CCCONTRTESTATA;
              and DFPIANOC = this.w_CCPIANOCTESTATA;
              and DFCHIAVE = this.w_DFCHIAVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MOV_LETTO = NVL(cp_ToDate(_read_.DFSERTES),cp_NullValue(_read_.DFSERTES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DFFLGDIS<>"D"
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNSERIAL,PNNUMREG,PNDATREG"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_MOV_LETTO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNSERIAL,PNNUMREG,PNDATREG;
            from (i_cTable) where;
                PNSERIAL = this.w_MOV_LETTO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PNSERIAL = NVL(cp_ToDate(_read_.PNSERIAL),cp_NullValue(_read_.PNSERIAL))
          this.w_PNNUMRER = NVL(cp_ToDate(_read_.PNNUMREG),cp_NullValue(_read_.PNNUMREG))
          this.w_DATAR = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from DIS_TINT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIS_TINT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DINUMDIS,DINUMERO,DIDATDIS"+;
            " from "+i_cTable+" DIS_TINT where ";
                +"DINUMDIS = "+cp_ToStrODBC(this.w_MOV_LETTO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DINUMDIS,DINUMERO,DIDATDIS;
            from (i_cTable) where;
                DINUMDIS = this.w_MOV_LETTO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PNSERIAL = NVL(cp_ToDate(_read_.DINUMDIS),cp_NullValue(_read_.DINUMDIS))
          this.w_PNNUMRER = NVL(cp_ToDate(_read_.DINUMERO),cp_NullValue(_read_.DINUMERO))
          this.w_DATAR = NVL(cp_ToDate(_read_.DIDATDIS),cp_NullValue(_read_.DIDATDIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Il movimento di primanota viene caricato solo se � nuovo (non � stato caricato precedentemente)
      this.w_CCFLPART = " "
      if EMPTY ( this.w_MOV_LETTO ) 
        * --- Valorizzo campi e variabili con i dati contenuti nella causale
        if this.w_DFFLGDIS<>"D"
          * --- Read from CAU_CONT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAU_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCFLIVDF,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLPPRO,CCFLPDOC,CCFLRIFE,CCFLSALI,CCFLSALF,CCCALDOC,CCFLANAL,CCCODICE,CCFLPART"+;
              " from "+i_cTable+" CAU_CONT where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCFLIVDF,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLPPRO,CCFLPDOC,CCFLRIFE,CCFLSALI,CCFLSALF,CCCALDOC,CCFLANAL,CCCODICE,CCFLPART;
              from (i_cTable) where;
                  CCCODICE = this.w_PNCODCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PNFLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
            this.w_PNTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
            this.w_PNTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
            this.w_PNNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
            this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
            this.w_FLPDOC = NVL(cp_ToDate(_read_.CCFLPDOC),cp_NullValue(_read_.CCFLPDOC))
            this.w_FLRIFE = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
            this.w_FLSALI = NVL(cp_ToDate(_read_.CCFLSALI),cp_NullValue(_read_.CCFLSALI))
            this.w_FLSALF = NVL(cp_ToDate(_read_.CCFLSALF),cp_NullValue(_read_.CCFLSALF))
            this.w_CALDOC = NVL(cp_ToDate(_read_.CCCALDOC),cp_NullValue(_read_.CCCALDOC))
            this.w_FLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
            this.w_LETTO = NVL(cp_ToDate(_read_.CCCODICE),cp_NullValue(_read_.CCCODICE))
            this.w_CCFLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from CAU_DIST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAU_DIST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_DIST_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACODICE"+;
              " from "+i_cTable+" CAU_DIST where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACODICE;
              from (i_cTable) where;
                  CACODICE = this.w_PNCODCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LETTO = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_SALINI = "N"
        this.w_CCSTATUS = "D"
        if EMPTY(this.w_LETTO) AND this.oParentObject.w_DFTRASCA<>"S"
          if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
            ADDMSGNL("%1",THIS,ah_msgformat("Elaborazione Movimento DocFinance N� %1 %0Causale %3 non presente nell'anagrafica delle causali %4",Alltrim(this.w_ChiaveRottura),Alltrim(Str(this.w_Riga)),Alltrim(this.w_PNCODCAU), IIF ( this.w_DFFLGDIS<>"D" , "contabili" , "distinta" ) ))
          endif
          this.w_Corretto = .F.
          this.w_ErrImp = .f.
          this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat("Causale %1 non presente nell'anagrafica delle causali %2%0",Alltrim(this.w_PNCODCAU), IIF ( this.w_DFFLGDIS<>"D" , "contabili" , "distinta" ) )
        endif
        this.w_CreVarDat = SetInfoDate( g_CALUTD )
        this.w_CAMBIO = ABS (IIF (this.w_CONTROVALORE<>0 , ( this.w_PNIMPAVE +this.w_PNIMPDAR) / this.w_CONTROVALORE , 1) )
        * --- COTNROLLI E INSERIMENTO PRIMANOTA
        if this.w_DFFLGDIS<>"D"
          this.w_PNCODUTE = iif(g_UNIUTE$"UE", i_codute, 0)
          this.w_PNCOMPET = this.w_PNCODESE
          * --- Controllo Data Consolidamento
          if Not Empty(CHKCONS(IIF(this.w_FLANAL="S","PC","P"),this.w_PNDATREG,"B","N"))
            if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
              ADDMSGNL("%1",THIS,AH_MSGFORMAT( "Attenzione, registrazione numero del %1 con %2" , dtoc(this.w_PNDATREG) , SUBSTR(CHKCONS(IIF(this.w_FLANAL="S","PC","P"),this.w_PNDATREG,"B","N"),12) ) )
            endif
            this.w_Corretto = .F.
            this.w_ErrImp = .f.
            this.w_ERRORLOG = this.w_ERRORLOG + AH_MSGFORMAT( "Attenzione, registrazione numero del %1 con %2" , dtoc(this.w_PNDATREG) , SUBSTR(CHKCONS(IIF(this.w_FLANAL="S","PC","P"),this.w_PNDATREG,"B","N"),12) )
          endif
          if this.w_Corretto
            * --- Calcolo
            *       - Seriale movimento
            *       - Progressivo di riga
            this.w_PNSERIAL = space(10)
            i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
            cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
            * --- Chiave per progressivo
            this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
            this.w_PNNUMRER = 0
            * --- Aggiorna progressivo Numero Registrazione
            this.w_PNNUMRER = gsdf_bpr(this,"PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER",this.w_PNCODESE,this.w_PNCODUTE,this.w_PNPRG,this.w_PNNUMRER)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Try
            local bErr_04D0ED70
            bErr_04D0ED70=bTrsErr
            this.Try_04D0ED70()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
                ADDMSGNL("%1",THIS,ah_msgformat( "Impossibile inserire il movimento di primanota %1 " , message() ))
              endif
              this.w_Corretto = .F.
              this.w_ErrImp = .f.
              this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Impossibile inserire il movimento di primanota %1 %0 " , message() )
            endif
            bTrsErr=bTrsErr or bErr_04D0ED70
            * --- End
          endif
        else
          * --- ** INSERIMENTO DISTINTA
          if this.w_Corretto
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        if this.w_Corretto
          * --- Progressivo riga dettaglio  movimento di primanota
          this.w_ULT_RNUM = IIF (this.w_DFFLGDIS="D", -2 , 0 )
          * --- Progressivo riga contropartite
          this.w_ULT_RNUMCP = 0
          * --- Progressivo riga partite
          this.w_ULT_RNUMPA = 1
        endif
      else
        this.w_CONTADOPPI = this.w_CONTADOPPI + 1
        if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="A"
          if this.w_DFFLGDIS<>"D"
            ADDMSGNL("%1",This,ah_msgformat("Movimento di primanota gi� inserito%0Seriale %1 numero reg. %2  del %3",Alltrim(this.w_PNSERIAL),Alltrim(Str(this.w_PNNUMRER,6,0)),Alltrim (dtoc(this.w_DATAR))))
          else
            ADDMSGNL("%1",This,ah_msgformat("Distinta gi� inserita%0Seriale %1 numero %2  del %3",Alltrim(this.w_PNSERIAL),Alltrim(Str(this.w_PNNUMRER,6,0)),Alltrim (dtoc(this.w_DATAR))))
          endif
        endif
        this.w_DOPPIO = .T.
        if this.w_DFFLGDIS<>"D"
          this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat("Movimento di primanota gi� inserito%0Seriale %1 numero reg. %2  del %3 %0",Alltrim(this.w_PNSERIAL),Alltrim(Str(this.w_PNNUMRER,6,0)),Alltrim (dtoc(this.w_DATAR)))
        else
          this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat("distinta gi� inserita%0Seriale %1 numero %2  del %3 %0",Alltrim(this.w_PNSERIAL),Alltrim(Str(this.w_PNNUMRER,6,0)),Alltrim (dtoc(this.w_DATAR)))
        endif
      endif
    endif
    if this.w_SEGNO = "-"
      this.w_PNIMPAVE = this.w_CONTROVALORECRE
      this.w_PNIMPDAR = 0
    else
      this.w_PNIMPAVE = 0
      this.w_PNIMPDAR = this.w_CONTROVALOREDEB
    endif
    * --- Per le distinte si inverte il segno 
    if this.w_DFFLGDIS="D"
      this.w_APPOGGIO = this.w_PNIMPAVE
      this.w_PNIMPAVE = this.w_PNIMPDAR
      this.w_PNIMPDAR = this.w_APPOGGIO
    endif
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODICE,ANPARTSN"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODICE,ANPARTSN;
        from (i_cTable) where;
            ANTIPCON = this.w_ANTIPCON;
            and ANCODICE = this.w_ANCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LETTO = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
      this.w_ANPARTSN = NVL(cp_ToDate(_read_.ANPARTSN),cp_NullValue(_read_.ANPARTSN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Empty(this.w_Mov_Letto) And this.w_Corretto 
      if this.w_DFFLGDIS<>"D"
        * --- INSERIMENTO  RIGHE DI PRIMANOTA
        if EMPTY(NVL(this.w_LETTO,""))
          if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
            ADDMSGNL("%1",THIS,ah_msgformat( "Elaborazione Movimento DocFinance N� %1 %0Si sta cercando di importare un movimento di primanota con conto %3 %4 inesistente",Alltrim(this.w_ChiaveRottura),Alltrim(Str(this.w_Riga)),Alltrim(this.w_ANTIPCON) ,Alltrim(this.w_ANCODICE) ))
          endif
          this.w_Corretto = .F.
          this.w_ErrImp = .f.
          this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Conto %1 %2 inesistente %0",Alltrim(this.w_ANTIPCON) ,Alltrim(this.w_ANCODICE) )
        endif
        * --- La riga contiene il conto di primanota quindi bisonga inserire il dettaglio del movimento 
        if this.w_Corretto
          this.w_ULT_RNUM = this.w_ULT_RNUM + 1
          this.w_CPROWORD = this.w_ULT_RNUM * 10
          * --- Try
          local bErr_04CEDB80
          bErr_04CEDB80=bTrsErr
          this.Try_04CEDB80()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_Corretto = .F.
          endif
          bTrsErr=bTrsErr or bErr_04CEDB80
          * --- End
        endif
      endif
      * --- Inserisco l'eventuale partita 
      * --- Si scrivono partite di acconto se la causale � di tipo acconto o se � una registrazione di primanota di tipo saldo che non ha chiave coge che movimenta un cliente/fornitore
      if (NOT EMPTY (this.w_DFPTTIPO) OR (this.w_CCFLPART="A" AND this.w_ANPARTSN="S") OR ( EMPTY (this.w_DFPTTIPO) AND this.w_ANTIPCON $ "CF" AND this.w_DFFLGDIS<>"D" ) ) AND this.w_Corretto and ( (this.w_CCFLPART<>"N" AND this.w_ANPARTSN="S") or this.w_DFFLGDIS="D")
        if EMPTY (this.w_DFPTTIPO) AND this.w_ANTIPCON $ "CF" AND this.w_DFFLGDIS<>"D"
          * --- Imposta la variabile per creare una partita di acconto
          if this.w_CCFLPART<>"N"
            this.w_CCFLPARTOLD = this.w_CCFLPART
            this.w_CCFLPART = "A"
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CCFLPART = this.w_CCFLPARTOLD
          endif
        else
          * --- Imposta la variabile per creare una partita di acconto
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    if not this.w_corretto
      this.w_Correttolog = .f.
    endif
  endproc
  proc Try_04D0ED70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Valorizza il campo che indica il legame tra la tabella di confine  e il movimento di primanota
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNNUMDOC"+",PNALFDOC"+",PNDATDOC"+",PNANNDOC"+",UTCC"+",UTDC"+",PNANNPRO"+",PNALFPRO"+",PNNUMPRO"+",PNPRP"+",PNPRD"+",PNPRG"+",PN__ANNO"+",PN__MESE"+",PNAGG_01"+",PNAGG_02"+",PNAGG_03"+",PNAGG_04"+",PNAGG_05"+",PNAGG_06"+",PNTIPCLF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPARDIS),'PNT_MAST','PNNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(space(10)),'PNT_MAST','PNALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("   -  -    ")),'PNT_MAST','PNDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(space (4)),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(space(4)),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(space(10)),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(IIF(this.w_PNTIPREG="A", "AC", "NN")),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC(IIF(this.w_PNTIPREG="V" OR (this.w_PNTIPREG="C" AND this.w_PNTIPDOC="FC"), "FV", "NN")),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC(SPACE (2)),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PN__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PN__MESE');
      +","+cp_NullLink(cp_ToStrODBC(SPACE (1)),'PNT_MAST','PNAGG_01');
      +","+cp_NullLink(cp_ToStrODBC(SPACE (1)),'PNT_MAST','PNAGG_02');
      +","+cp_NullLink(cp_ToStrODBC(SPACE (1)),'PNT_MAST','PNAGG_03');
      +","+cp_NullLink(cp_ToStrODBC(SPACE (1)),'PNT_MAST','PNAGG_04');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("   -  -    ")),'PNT_MAST','PNAGG_05');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("   -  -    ")),'PNT_MAST','PNAGG_06');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLRIFE),'PNT_MAST','PNTIPCLF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODCAU',this.w_PNCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNNUMDOC',this.w_NUMPARDIS,'PNALFDOC',space(10),'PNDATDOC',cp_CharToDate("   -  -    "),'PNANNDOC',space (4),'UTCC',i_codute)
      insert into (i_cTable) (PNSERIAL,PNCODESE,PNCODUTE,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNNUMDOC,PNALFDOC,PNDATDOC,PNANNDOC,UTCC,UTDC,PNANNPRO,PNALFPRO,PNNUMPRO,PNPRP,PNPRD,PNPRG,PN__ANNO,PN__MESE,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06,PNTIPCLF &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PNCODESE;
           ,this.w_PNCODUTE;
           ,this.w_PNNUMRER;
           ,this.w_PNDATREG;
           ,this.w_PNCODCAU;
           ,this.w_PNCOMPET;
           ,this.w_NUMPARDIS;
           ,space(10);
           ,cp_CharToDate("   -  -    ");
           ,space (4);
           ,i_codute;
           ,this.w_CreVarDat;
           ,space(4);
           ,space(10);
           ,0;
           ,IIF(this.w_PNTIPREG="A", "AC", "NN");
           ,IIF(this.w_PNTIPREG="V" OR (this.w_PNTIPREG="C" AND this.w_PNTIPDOC="FC"), "FV", "NN");
           ,SPACE (2);
           ,0;
           ,0;
           ,SPACE (1);
           ,SPACE (1);
           ,SPACE (1);
           ,SPACE (1);
           ,cp_CharToDate("   -  -    ");
           ,cp_CharToDate("   -  -    ");
           ,this.w_FLRIFE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into PNT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PNTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
      +",PNFLIVDF ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLIVDF),'PNT_MAST','PNFLIVDF');
      +",PNFLGDIF ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNFLGDIF');
      +",PNNUMREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
      +",PNTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
      +",PNCAOVAL ="+cp_NullLink(cp_ToStrODBC(1),'PNT_MAST','PNCAOVAL');
      +",PNDESSUP ="+cp_NullLink(cp_ToStrODBC(left (this.w_DESOP , 50)),'PNT_MAST','PNDESSUP');
      +",PNTOTDOC ="+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNTOTDOC');
      +",PNCOMIVA ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNCOMIVA');
      +",PNFLREGI ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNFLREGI');
      +",PNFLPROV ="+cp_NullLink(cp_ToStrODBC(this.w_FLAG_PROV),'PNT_MAST','PNFLPROV');
      +",PNVALNAZ ="+cp_NullLink(cp_ToStrODBC(g_PERVAL),'PNT_MAST','PNVALNAZ');
      +",PNCODVAL ="+cp_NullLink(cp_ToStrODBC(g_PERVAL),'PNT_MAST','PNCODVAL');
      +",PNRIFDOC ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNRIFDOC');
      +",PNRIFDIS ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNRIFDIS');
      +",PNRIFINC ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNRIFINC');
      +",PNRIFCES ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNRIFCES');
      +",PNRIFACC ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNRIFACC');
      +",PNFLLIBG ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNFLLIBG');
      +",PNDATPLA ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATPLA');
      +",PNRIFSAL ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PNT_MAST','PNRIFSAL');
      +",PNSCRASS ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_MAST','PNSCRASS');
          +i_ccchkf ;
      +" where ";
          +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
             )
    else
      update (i_cTable) set;
          PNTIPREG = this.w_PNTIPREG;
          ,PNFLIVDF = this.w_PNFLIVDF;
          ,PNFLGDIF = SPACE(1);
          ,PNNUMREG = this.w_PNNUMREG;
          ,PNTIPDOC = this.w_PNTIPDOC;
          ,PNCAOVAL = 1;
          ,PNDESSUP = left (this.w_DESOP , 50);
          ,PNTOTDOC = 0;
          ,PNCOMIVA = this.w_PNDATREG;
          ,PNFLREGI = SPACE(1);
          ,PNFLPROV = this.w_FLAG_PROV;
          ,PNVALNAZ = g_PERVAL;
          ,PNCODVAL = g_PERVAL;
          ,PNRIFDOC = SPACE(1);
          ,PNRIFDIS = SPACE(1);
          ,PNRIFINC = SPACE(1);
          ,PNRIFCES = SPACE(1);
          ,PNRIFACC = SPACE(1);
          ,PNFLLIBG = SPACE(1);
          ,PNDATPLA = this.w_PNDATREG;
          ,PNRIFSAL = SPACE(10);
          ,PNSCRASS = SPACE(1);
          &i_ccchkf. ;
       where;
          PNSERIAL = this.w_PNSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if (this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="A" ) AND this.w_GENERA="S"
      ADDMSGNL("%1",THIS,ah_msgformat("Elaborazione nuovo movimento di primanota%0Seriale %1 numero reg. %2  del %3 testata inserita correttamente",Alltrim(this.w_PNSERIAL),Alltrim(str(this.w_PNNUMRER,6,0)),Alltrim(Dtoc(this.w_PNDATREG)) ))
    endif
    return
  proc Try_04CEDB80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- inserimento testata dettaglio movimento
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ULT_RNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'PNT_DETT','PNCODCON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ULT_RNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_ANTIPCON,'PNCODCON',this.w_ANCODICE)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_ULT_RNUM;
           ,this.w_CPROWORD;
           ,this.w_ANTIPCON;
           ,this.w_ANCODICE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PNDESRIG = IIF ( not empty (this.w_CADESRIG) , this.w_CADESRIG , this.w_DESOP)
    * --- Flag provvisorio o definitiva
    this.w_CONSUP = SPACE(15)
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCONSUP,ANCCTAGG"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCONSUP,ANCCTAGG;
        from (i_cTable) where;
            ANTIPCON = this.w_ANTIPCON;
            and ANCODICE = this.w_ANCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
      this.w_ANCCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
      w_MASTRO = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PNFLSALD = iif ( this.w_FLAG_PROV<>"S" , "+" , " ")
    this.w_PNFLSALI = IIF(this.w_FLSALI="S" and this.w_FLAG_PROV<>"S", "+", " ")
    this.w_PNFLSALF = IIF(this.w_FLSALF="S" and this.w_FLAG_PROV<>"S", "+", " ")
    if this.w_ANTIPCON="G" AND (this.w_PNFLSALF<>" " OR this.w_PNFLSALI<>" ")
      * --- Verifica se Conto Transitorio (Non aggiorna Saldi Iniziale/Finale)
      this.w_SEZBIL = " "
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZBIL="T"
        this.w_PNFLSALF = " "
        this.w_PNFLSALI = " "
      endif
    endif
    this.w_COSAPAGA = SPACE(10)
    * --- Imposta la variabile per creare una partita di acconto
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COSAPAGA"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COSAPAGA;
        from (i_cTable) where;
            COCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COSAPAGA = NVL(cp_ToDate(_read_.COSAPAGA),cp_NullValue(_read_.COSAPAGA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Recupera il codice di pagamento dal cliente/fornitore
    if EMPTY(this.w_PNCODPAG) AND g_PERPAR="S" AND this.w_ANTIPCON<>"G"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODPAG"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODPAG;
          from (i_cTable) where;
              ANTIPCON = this.w_ANTIPCON;
              and ANCODICE = this.w_ANCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PNCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_PNCODPAG = SPACE(5)
    endif
    if IIF ((this.w_CCFLPART="A" AND this.w_ANPARTSN="S") OR (EMPTY (this.w_DFPTTIPO) AND this.w_ANTIPCON $ "CF" AND this.w_ANPARTSN="S" and this.w_CCFLPART="S"),"A" , IIF (EMPTY (this.w_DFPTTIPO) or this.w_ANPARTSN<>"S" OR this.w_CCFLPART="N","N","S")) = "A"
      this.w_PNCODPAG = this.w_COSAPAGA
    endif
    * --- Aggiornamento riga di dettaglio movimento di primanota
    * --- Try
    local bErr_04D6ECC0
    bErr_04D6ECC0=bTrsErr
    this.Try_04D6ECC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
        ADDMSGNL("%1",THIS,ah_msgformat( "Impossibile inserire il dettaglio movimento di primanota %1 " , message() ))
      endif
      this.w_Corretto = .F.
      this.w_ErrImp = .f.
      this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Impossibile inserire il dettaglio movimento di primanota %1 %0" , message() )
    endif
    bTrsErr=bTrsErr or bErr_04D6ECC0
    * --- End
    * --- Inserimento analitica
    this.w_COSTORIC = CALCSEZ(w_MASTRO)
    if this.w_ANTIPCON="G" AND this.w_FLANAL="S" AND g_PERCCR="S" AND this.w_COSTORIC $ "CR"
      if this.w_ANCCTAGG<>"E"
        this.Pag10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Aggiornamento saldi
    if this.w_FLAG_PROV<>"S"
      * --- Try
      local bErr_04D71DB0
      bErr_04D71DB0=bTrsErr
      this.Try_04D71DB0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04D71DB0
      * --- End
      * --- Try
      local bErr_04D720B0
      bErr_04D720B0=bTrsErr
      this.Try_04D720B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_Corretto = .F.
        this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Errore scrittura saldi movimenti di primanota %1 %0" , message() )
      endif
      bTrsErr=bTrsErr or bErr_04D720B0
      * --- End
    endif
  endproc
  proc Try_04D6ECC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PNT_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +",PNFLPART ="+cp_NullLink(cp_ToStrODBC(IIF ((this.w_CCFLPART="A" AND this.w_ANPARTSN="S") OR (EMPTY (this.w_DFPTTIPO) AND this.w_ANTIPCON $ "CF" AND this.w_ANPARTSN="S" and this.w_CCFLPART="S"),"A" , IIF (EMPTY (this.w_DFPTTIPO) or this.w_ANPARTSN<>"S" OR this.w_CCFLPART="N","N","S"))),'PNT_DETT','PNFLPART');
      +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_DETT','PNFLZERO');
      +",PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +",PNCAURIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +",PNCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +",PNFLSALD ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +",PNFLSALI ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALI),'PNT_DETT','PNFLSALI');
      +",PNFLSALF ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
      +",PNINICOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("   -  -    ")),'PNT_DETT','PNINICOM');
      +",PNFINCOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("   -  -    ")),'PNT_DETT','PNFINCOM');
      +",PNFLABAN ="+cp_NullLink(cp_ToStrODBC(SPACE(6)),'PNT_DETT','PNFLABAN');
      +",PNCODBUN ="+cp_NullLink(cp_ToStrODBC(g_codbun),'PNT_DETT','PNCODBUN');
      +",PNCODAGE ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'PNT_DETT','PNCODAGE');
      +",PNFLVABD ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PNT_DETT','PNFLVABD');
          +i_ccchkf ;
      +" where ";
          +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ULT_RNUM);
             )
    else
      update (i_cTable) set;
          PNIMPDAR = this.w_PNIMPDAR;
          ,PNIMPAVE = this.w_PNIMPAVE;
          ,PNFLPART = IIF ((this.w_CCFLPART="A" AND this.w_ANPARTSN="S") OR (EMPTY (this.w_DFPTTIPO) AND this.w_ANTIPCON $ "CF" AND this.w_ANPARTSN="S" and this.w_CCFLPART="S"),"A" , IIF (EMPTY (this.w_DFPTTIPO) or this.w_ANPARTSN<>"S" OR this.w_CCFLPART="N","N","S"));
          ,PNFLZERO = SPACE(1);
          ,PNDESRIG = this.w_PNDESRIG;
          ,PNCAURIG = this.w_PNCODCAU;
          ,PNCODPAG = this.w_PNCODPAG;
          ,PNFLSALD = this.w_PNFLSALD;
          ,PNFLSALI = this.w_PNFLSALI;
          ,PNFLSALF = this.w_PNFLSALF;
          ,PNINICOM = cp_CharToDate("   -  -    ");
          ,PNFINCOM = cp_CharToDate("   -  -    ");
          ,PNFLABAN = SPACE(6);
          ,PNCODBUN = g_codbun;
          ,PNCODAGE = SPACE(5);
          ,PNFLVABD = SPACE(1);
          &i_ccchkf. ;
       where;
          PNSERIAL = this.w_PNSERIAL;
          and CPROWNUM = this.w_ULT_RNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04D71DB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'SALDICON','SLCODESE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_ANTIPCON,'SLCODICE',this.w_ANCODICE,'SLCODESE',this.w_PNCOMPET)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE &i_ccchkf. );
         values (;
           this.w_ANTIPCON;
           ,this.w_ANCODICE;
           ,this.w_PNCOMPET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04D720B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_PNFLSALD,'SLDARPER','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_PNFLSALD,'SLAVEPER','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER ="+cp_NullLink(i_cOp1,'SALDICON','SLDARPER');
      +",SLAVEPER ="+cp_NullLink(i_cOp2,'SALDICON','SLAVEPER');
      +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
      +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
      +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
      +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCOMPET);
             )
    else
      update (i_cTable) set;
          SLDARPER = &i_cOp1.;
          ,SLAVEPER = &i_cOp2.;
          ,SLDARINI = &i_cOp3.;
          ,SLAVEINI = &i_cOp4.;
          ,SLDARFIN = &i_cOp5.;
          ,SLAVEFIN = &i_cOp6.;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_ANTIPCON;
          and SLCODICE = this.w_ANCODICE;
          and SLCODESE = this.w_PNCOMPET;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RIGA = this.w_RIGA + 1
    this.w_CONTARECORD = this.w_CONTARECORD + 1
    this.w_AZIENDA = UPPER(alltrim(SUBSTR(DATIEL,1,10)))
    this.w_DATIEL = NVL(DATIEL,"") + NVL(DATIEL1,"") + NVL(DATIEL2,"") + NVL(DATIEL3,"") + NVL(DATIEL4,"") 
    * --- Valorizza tutte le variabili di appoggio con i valori contenuti nel file e f� le conversioni di tipo
    this.w_NUMERATORE = SUBSTR(this.w_DATIEL,11,4)
    this.w_ANNO = SUBSTR(this.w_DATIEL,15,4)
    this.w_MOVIMENTO = SUBSTR(this.w_DATIEL,19,10)
    * --- Numero progressivo all interno del file asci, serve come chiave di rottura del movimento
    *     Pos 29 - 35
    this.w_CHIAVEFILE = SUBSTR(this.w_DATIEL,29,6)
    this.w_CHIAVEROTTURA = this.w_AZIENDA+this.w_NUMERATORE+this.w_ANNO+this.w_MOVIMENTO+this.w_CHIAVEFILE
    * --- Data contabilizzazione della banca  Pos 35 - 43
    this.w_PNDATREG = this.StringToDate(SUBSTR(this.w_DATIEL,35,8))
    this.w_PNDATREG = IIF (EMPTY (this.w_PNDATREG) , i_DATSYS , this.w_PNDATREG)
    this.w_BANCA = SUBSTR(this.w_DATIEL,43,8)
    this.w_RBN = SUBSTR(this.w_DATIEL,51,4)
    * --- Causale contabile corrispondente al movimento di primanota Pos 63 - 67
    this.w_PNCODCAU = SUBSTR(this.w_DATIEL,63,4)
    * --- Valuta in divisa Pos 67 - 72
    this.w_PNCODVAL = alltrim (SUBSTR(this.w_DATIEL,67,5))
    * --- Il segno dell importo nel file ci indica il segno del movimento di primanota Pos 72 - 94
    * --- IMPORTI DA ROCOSTRUIRE NELLA LETTURA DA TABELLA (sono DDIMPVAL , i� un importo con segno )
    this.w_SEGNO = SUBSTR(this.w_DATIEL,72,1) 
    this.w_DFIMPVAL = VAL (SUBSTR(this.w_DATIEL,72,22) )
    if this.w_SEGNO="-"
      this.w_PNIMPAVE = 0
      this.w_PNIMPDAR = VAL (SUBSTR(this.w_DATIEL,73,21) )
    else
      this.w_PNIMPAVE = VAL (SUBSTR(this.w_DATIEL,73,21) )
      this.w_PNIMPDAR = 0
    endif
    * --- Valuta in divisa azienda Pos 94 - 99
    this.w_CCCODVAL = SUBSTR(this.w_DATIEL,94,5)
    * --- Controvalore in divisa azienda Pos 99 -121
    * --- IMPORTI DA ROCOSTRUIRE NELLA LETTURA DA TABELLA (sono DDIMPORT , i� un importo con segno )
    this.w_CONTROVALORE = VAL ( SUBSTR(this.w_DATIEL,100, 21) )
    if SUBSTR(this.w_DATIEL,99, 1) ="-"
      this.w_CONTROVALORECRE = this.w_CONTROVALORE
      this.w_CONTROVALOREDEB = 0
    else
      this.w_CONTROVALORECRE = 0
      this.w_CONTROVALOREDEB = this.w_CONTROVALORE
    endif
    * --- Piano dei conti e controvalore utilizzati come chiave del movimento da DocFinance (da specifiche di prodotto)
    this.w_CCCONTR = SUBSTR(this.w_DATIEL,99, 22)
    this.w_CCPIANOC = SUBSTR(this.w_DATIEL,181, 16)
    * --- Rapporto di cambio ricavato dagli importi del movimento espressi nella valuta movimentata e dalla valuta dell azienda
    this.w_CCCAOVAT = Abs (IIF (this.w_CONTROVALORE<>0,(this.w_PNIMPAVE+this.w_PNIMPDAR)/this.w_CONTROVALORE,1) )
    * --- Data valuta del movimento Pos 121- 129
    this.w_CCDATVAL = this.StringToDate(SUBSTR(this.w_DATIEL,121,8))
    * --- Note del movimento sulle righe di contropartita Pos 129 - 169
    this.w_CADESRIG = SUBSTR(this.w_DATIEL,129, 40)
    * --- Numero partita Pos 171 - 181
    this.w_NUMPARDIS = Val(right(alltrim(SUBSTR(this.w_DATIEL,171,10)),6))
    * --- Codice contropartita  Pos 181 - 197   (� il campo Piano dei conti del tracciato)
    * --- TIPO E CODICE SONO IN DFTICPON E DFCODCON
    this.w_ANTIPCON = SUBSTR(this.w_DATIEL,181, 1)
    this.w_ANCODICE = SUBSTR(this.w_DATIEL,182, 15)
    this.w_SEZIONEAZIENDALE = SUBSTR(this.w_DATIEL,197, 6)
    * --- Tipo della chiave COGE 
    *     DFTTIPO=C  indica la chiave fisica  PTSERIAL PTROWORD CPROWNUM
    *     DFTTIPO=R  indica una partita ragruppata PTNUMEFF PTNUMPAG PTDATSCA
    *     DFTTIPO=D  indica una partita in distinta PTSERIAL PTNUMEFF
    this.w_DFPTTIPO = SUBSTR(this.w_DATIEL, 234 , 1)
    this.w_DFMODPAG = SPACE (10)
    this.w_DFDATSCA = ctod("  /  /  ")
    this.w_PTSERIAL = SPACE (10)
    this.w_PTROWORD = 0
    this.w_CPROWNUM = 0
    this.w_PTNUMEFF = 0
    do case
      case this.w_DFPTTIPO="C"
        * --- Chiave COGE che identifica la partita Pos 209 - 235
        *     La chiave coge contiene PTSERIAL (lunghezza 10). PTROWORD(lunghezza 4), CPROWNUM (lunghezza 1)  della partita 
        this.w_PTSERIAL = SUBSTR(this.w_DATIEL, 209 , 10)
        this.w_PTROWORD = Val(Alltrim(Substr(this.w_DATIEL,219,4)))
        this.w_CPROWNUM = Val(Alltrim(Substr(this.w_DATIEL,223,4)))
      case this.w_DFPTTIPO="R"
        * --- Riferimenti delle partite ragruppate  PTNUMEFF PTMODPAG PTDATSCA
        *     Per capire quali parite saldare si ricerca le partite con stesso  PTNUMEFF PTMODPAG PTDATSCA e stesso PTTIPCON PTCODCON
        this.w_PTNUMEFF = VAL (SUBSTR(this.w_DATIEL, 209 , 6))
        this.w_DFMODPAG = SUBSTR(this.w_DATIEL, 215 , 10)
        this.w_DFDATSCA = ctod (SUBSTR(this.w_DATIEL, 225 , 2) + "-"+SUBSTR(this.w_DATIEL, 227 , 2)+"-"+SUBSTR(this.w_DATIEL, 229 , 4))
      case this.w_DFPTTIPO="D"
        * --- Riferimenti delle partite indistinta PTSERIAL PTNUMEFF  (per capire quiali partite saldatre si ricerca sulle partita in distinta (PTROWORD=-2 ))
        this.w_PTSERIAL = SUBSTR(this.w_DATIEL, 209 , 10)
        this.w_PTNUMEFF = VAL (SUBSTR(this.w_DATIEL, 219 , 6))
        this.w_PTROWORD = -2
    endcase
    this.w_DESOP = SUBSTR(this.w_DATIEL, 252 , 40)
    this.w_DFFLGDIS = SUBSTR(this.w_DATIEL,292, 1)
    * --- Importo originario scadenza associata al movimento bancario Pos 305 - 321
    this.w_PTTOTIMP = this.w_PNIMPAVE+this.w_PNIMPDAR
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Recuperara valori della partita
    this.w_PTCODCON = ""
    * --- Tipo della chiave COGE 
    *     DFTTIPO=C  indica la chiave fisica  PTSERIAL PTROWORD CPROWNUM si rislae direttamente alla partita da saldare con la chiave fisica
    *     DFTTIPO=R  indica una partita ragruppata PTNUMEFF PTNUMPAG PTDATSCA si risale alle partite da saldare con PTNUMEFF PTNUMPAG PTDATSCA  + PTTIPCON e PTCODCON
    *     DFTTIPO=D  indica una partita in distinta PTSERIAL PTNUMEFF si risale alle partiet da saldare con PTSERIAL PTNUMEFF e PTROWORD=-2
    do case
      case this.w_DFPTTIPO="R"
        * --- SALDA LE PARTITE RAGRUPPATE
        * --- Select from PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PTSERIAL , PTROWORD , CPROWNUM  from "+i_cTable+" PAR_TITE ";
              +" where PTNUMEFF="+cp_ToStrODBC(this.w_PTNUMEFF)+" AND PTMODPAG="+cp_ToStrODBC(this.w_DFMODPAG)+" AND PTDATSCA="+cp_ToStrODBC(this.w_DFDATSCA)+" AND PTTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_ANCODICE)+" AND PTFLCRSA='C'";
               ,"_Curs_PAR_TITE")
        else
          select PTSERIAL , PTROWORD , CPROWNUM from (i_cTable);
           where PTNUMEFF=this.w_PTNUMEFF AND PTMODPAG=this.w_DFMODPAG AND PTDATSCA=this.w_DFDATSCA AND PTTIPCON=this.w_ANTIPCON AND PTCODCON=this.w_ANCODICE AND PTFLCRSA="C";
            into cursor _Curs_PAR_TITE
        endif
        if used('_Curs_PAR_TITE')
          select _Curs_PAR_TITE
          locate for 1=1
          if not(eof())
          do while not(eof())
          this.w_PTSERIAL = PTSERIAL
          this.w_PTROWORD = PTROWORD
          this.w_CPROWNUM = CPROWNUM
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_PAR_TITE
            continue
          enddo
          else
            ADDMSGNL("%1",THIS,ah_msgformat( "Elaborazione Movimento DocFinance N� %1 %0Si sta cercando di saldare una partita inesistente del %5 %6 (scadenza distinta %3 effetto %4)", Alltrim(this.w_ChiaveRottura), Alltrim(Str(this.w_Riga)), Alltrim(dtoc(this.w_DFDATSCA)), ALLTRIM(STR(this.w_PTNUMEFF)) , IIF (this.w_ANTIPCON="C" , "cliente" , IIF (this.w_ANTIPCON="F" , "fornitore", "conto" )) , this.w_ANCODICE ))
            this.w_Corretto = .F.
            this.w_ErrImp = .f.
            this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Si sta cercando di saldare una partita inesistente del %4 %5 (scadenza distinta %2 effetto %3)", Alltrim(Str(this.w_Riga)), Alltrim(dtoc(this.w_DFDATSCA)), ALLTRIM(STR(this.w_PTNUMEFF)) , IIF (this.w_ANTIPCON="C" , "cliente" , IIF (this.w_ANTIPCON="F" , "fornitore", "conto" )) , this.w_ANCODICE )
            select _Curs_PAR_TITE
          endif
          use
        endif
      case this.w_DFPTTIPO="D"
        * --- SALDA LE PARTITE IN DISTINTA
        if this.w_DFFLGDIS<>"D"
          * --- Select from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select PTSERIAL , PTROWORD , CPROWNUM  from "+i_cTable+" PAR_TITE ";
                +" where PTNUMEFF="+cp_ToStrODBC(this.w_PTNUMEFF)+" AND PTROWORD=-2 AND PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL)+" ";
                 ,"_Curs_PAR_TITE")
          else
            select PTSERIAL , PTROWORD , CPROWNUM from (i_cTable);
             where PTNUMEFF=this.w_PTNUMEFF AND PTROWORD=-2 AND PTSERIAL = this.w_PTSERIAL ;
              into cursor _Curs_PAR_TITE
          endif
          if used('_Curs_PAR_TITE')
            select _Curs_PAR_TITE
            locate for 1=1
            if not(eof())
            do while not(eof())
            this.w_PTSERIAL = PTSERIAL
            this.w_PTROWORD = PTROWORD
            this.w_CPROWNUM = CPROWNUM
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs_PAR_TITE
              continue
            enddo
            else
              ADDMSGNL("%1",THIS,ah_msgformat( "Elaborazione Movimento DocFinance N� %1 %0Si sta cercando di saldare una partita inesistente (Seriale distinta %3 effetto %4)", Alltrim(this.w_ChiaveRottura), Alltrim(Str(this.w_Riga)), Alltrim(this.w_PTSERIAL), ALLTRIM(STR(this.w_PTNUMEFF))))
              this.w_Corretto = .F.
              this.w_ErrImp = .f.
              this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Si sta cercando di saldare una partita inesistente (Seriale distinta %1 effetto %2)%0", Alltrim(this.w_PTSERIAL), ALLTRIM(STR(this.w_PTNUMEFF)))
              select _Curs_PAR_TITE
            endif
            use
          endif
        else
          * --- Read from DIS_TINT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIS_TINT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DINUMERO,DIDATDIS"+;
              " from "+i_cTable+" DIS_TINT where ";
                  +"DINUMDIS = "+cp_ToStrODBC(this.w_PTSERIAL );
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DINUMERO,DIDATDIS;
              from (i_cTable) where;
                  DINUMDIS = this.w_PTSERIAL ;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PNNUMRER = NVL(cp_ToDate(_read_.DINUMERO),cp_NullValue(_read_.DINUMERO))
            this.w_DATAR = NVL(cp_ToDate(_read_.DIDATDIS),cp_NullValue(_read_.DIDATDIS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          ADDMSGNL("%1",THIS,ah_msgformat( "Elaborazione Movimento DocFinance N� %1 %0La scadenza che si sta importando � gi� presente nella distinta.n. %2 del %3", Alltrim(this.w_ChiaveRottura), Alltrim(Str(this.w_PNNUMRER)), Alltrim( dtoc (this.w_DATAR)) ))
          this.w_Corretto = .F.
          this.w_ErrImp = .f.
          this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Elaborazione Movimento DocFinance N� %1 %0La scadenza che si sta importando � gi� presente nella distinta.n. %2 del %3", Alltrim(this.w_ChiaveRottura), Alltrim(Str(this.w_PNNUMRER)), Alltrim( dtoc (this.w_DATAR)) )
        endif
      case this.w_DFPTTIPO="C" OR (this.w_CCFLPART="A" AND this.w_ANPARTSN="S")
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisco la  partita collegata alla riga della contropartita
    this.w_EFFETTO = 0
    * --- Try
    local bErr_04DCE480
    bErr_04DCE480=bTrsErr
    this.Try_04DCE480()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
        ADDMSGNL("%1",THIS,ah_msgformat( "Impossibile inserire il dettaglio partita %1 ",message() ))
      endif
      this.w_Corretto = .F.
      this.w_ErrImp = .f.
      this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Impossibile inserire il dettaglio partita %1 ",message() )
    endif
    bTrsErr=bTrsErr or bErr_04DCE480
    * --- End
  endproc
  proc Try_04DCE480()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_CCFLPART="A"
      * --- Valorizza campio per la creazione delle partite di acconto
      this.w_PTTIPCON = this.w_ANTIPCON
      this.w_PTCODCON = this.w_ANCODICE
      * --- Read from PAG_2AME
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAG_2AME_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "P2MODPAG"+;
          " from "+i_cTable+" PAG_2AME where ";
              +"P2CODICE = "+cp_ToStrODBC(this.w_PNCODPAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          P2MODPAG;
          from (i_cTable) where;
              P2CODICE = this.w_PNCODPAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTMODPAG = NVL(cp_ToDate(_read_.P2MODPAG),cp_NullValue(_read_.P2MODPAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODBAN,ANCODBA2,ANNUMCOR,ANCODPAG,ANCODAG1"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODBAN,ANCODBA2,ANNUMCOR,ANCODPAG,ANCODAG1;
          from (i_cTable) where;
              ANTIPCON = this.w_ANTIPCON;
              and ANCODICE = this.w_ANCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTBANAPP = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
        this.w_PTBANNOS = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
        this.w_PTNUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
        this.w_PTMODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
        this.w_PTCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PTNUMPAR = CANUMPAR("S", this.w_PNCODESE)
      this.w_PTROWORD = this.w_ULT_RNUM
      this.w_ANFLACBD = " "
      this.w_AZFLVEBD = " "
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZFLVEBD"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZFLVEBD;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AZFLVEBD = NVL(cp_ToDate(_read_.AZFLVEBD),cp_NullValue(_read_.AZFLVEBD))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_ANTIPCON="F"
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANFLACBD"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANFLACBD;
            from (i_cTable) where;
                ANTIPCON = this.w_ANTIPCON;
                and ANCODICE = this.w_ANCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANFLACBD = NVL(cp_ToDate(_read_.ANFLACBD),cp_NullValue(_read_.ANFLACBD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_PTFLVABD = IIF(this.w_ANTIPCON="C", this.w_AZFLVEBD, IIF(this.w_ANTIPCON="F", this.w_ANFLACBD, " "))
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COSAPAGA"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COSAPAGA;
          from (i_cTable) where;
              COCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COSAPAGA = NVL(cp_ToDate(_read_.COSAPAGA),cp_NullValue(_read_.COSAPAGA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Saldo partite
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTTIPCON,PTCODCON,PTMODPAG,PTNUMCOR,PTBANNOS,PTDATAPE,PTNUMDOC,PTALFDOC,PTCAOAPE,PTNUMPAR,PTCODVAL,PT_SEGNO,PTCODAGE,ptdesrig,PTCAOVAL,PTDATSCA,PTTOTIMP,PTFLRITE,PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMEFF,PTFLVABD,PTBANAPP ,PTFLRAGG,PTDATDOC"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTTIPCON,PTCODCON,PTMODPAG,PTNUMCOR,PTBANNOS,PTDATAPE,PTNUMDOC,PTALFDOC,PTCAOAPE,PTNUMPAR,PTCODVAL,PT_SEGNO,PTCODAGE,ptdesrig,PTCAOVAL,PTDATSCA,PTTOTIMP,PTFLRITE,PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMEFF,PTFLVABD,PTBANAPP ,PTFLRAGG,PTDATDOC;
          from (i_cTable) where;
              PTSERIAL = this.w_PTSERIAL;
              and PTROWORD = this.w_PTROWORD;
              and CPROWNUM = this.w_CPROWNUM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTTIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
        this.w_PTCODCON = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
        this.w_PTMODPAG = NVL(cp_ToDate(_read_.PTMODPAG),cp_NullValue(_read_.PTMODPAG))
        this.w_PTNUMCOR = NVL(cp_ToDate(_read_.PTNUMCOR),cp_NullValue(_read_.PTNUMCOR))
        this.w_PTBANNOS = NVL(cp_ToDate(_read_.PTBANNOS),cp_NullValue(_read_.PTBANNOS))
        this.w_PTDATAPE = NVL(cp_ToDate(_read_.PTDATAPE),cp_NullValue(_read_.PTDATAPE))
        this.w_PTNUMDOC = NVL(cp_ToDate(_read_.PTNUMDOC),cp_NullValue(_read_.PTNUMDOC))
        this.w_PTALFDOC = NVL(cp_ToDate(_read_.PTALFDOC),cp_NullValue(_read_.PTALFDOC))
        this.w_PTCAOAPE = NVL(cp_ToDate(_read_.PTCAOAPE),cp_NullValue(_read_.PTCAOAPE))
        this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
        this.w_PTCODVAL = NVL(cp_ToDate(_read_.PTCODVAL),cp_NullValue(_read_.PTCODVAL))
        this.w_PT_SEGNO = NVL(cp_ToDate(_read_.PT_SEGNO),cp_NullValue(_read_.PT_SEGNO))
        this.w_PTCODAGE = NVL(cp_ToDate(_read_.PTCODAGE),cp_NullValue(_read_.PTCODAGE))
        this.w_ptdesrig = NVL(cp_ToDate(_read_.ptdesrig),cp_NullValue(_read_.ptdesrig))
        this.w_CAMBIOPAR = NVL(cp_ToDate(_read_.PTCAOVAL),cp_NullValue(_read_.PTCAOVAL))
        this.w_PTDATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
        this.w_PTTOTIMP = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
        this.w_PTFLRITE = NVL(cp_ToDate(_read_.PTFLRITE),cp_NullValue(_read_.PTFLRITE))
        this.w_PTTIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
        this.w_PTCODCON = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
        this.w_PTSERRIF = NVL(cp_ToDate(_read_.PTSERRIF),cp_NullValue(_read_.PTSERRIF))
        this.w_PTORDRIF = NVL(cp_ToDate(_read_.PTORDRIF),cp_NullValue(_read_.PTORDRIF))
        this.w_PTNUMRIF = NVL(cp_ToDate(_read_.PTNUMRIF),cp_NullValue(_read_.PTNUMRIF))
        this.w_EFFETTO = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
        this.w_PTFLVABD = NVL(cp_ToDate(_read_.PTFLVABD),cp_NullValue(_read_.PTFLVABD))
        this.w_PTBANAPP = NVL(cp_ToDate(_read_.PTBANAPP ),cp_NullValue(_read_.PTBANAPP ))
        this.w_PTFLRAGG = NVL(cp_ToDate(_read_.PTFLRAGG),cp_NullValue(_read_.PTFLRAGG))
        this.w_PTDATDOC = NVL(cp_ToDate(_read_.PTDATDOC),cp_NullValue(_read_.PTDATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DFFLGDIS="D"
        * --- Read from PAR_TITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PTNUMEFF"+;
            " from "+i_cTable+" PAR_TITE where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                +" and PTFLCRSA = "+cp_ToStrODBC("C");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PTNUMEFF;
            from (i_cTable) where;
                PTSERIAL = this.w_PTSERIAL;
                and PTROWORD = this.w_PTROWORD;
                and CPROWNUM = this.w_CPROWNUM;
                and PTFLCRSA = "C";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EFFETTO = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODBA2,ANNUMCOR,ANCODBAN"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODBA2,ANNUMCOR,ANCODBAN;
            from (i_cTable) where;
                ANTIPCON = this.w_ANTIPCON;
                and ANCODICE = this.w_ANCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PTBANNOS = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
          this.w_PTNUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
          this.w_PTBANAPP = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- lettura per valorizzare i_row e verificare se la partita esiste o si sta saldando una partita che non esiste
        * --- Read from PAR_TITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PTNUMEFF"+;
            " from "+i_cTable+" PAR_TITE where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PTNUMEFF;
            from (i_cTable) where;
                PTSERIAL = this.w_PTSERIAL;
                and PTROWORD = this.w_PTROWORD;
                and CPROWNUM = this.w_CPROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EFFETTO = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_EFFETTO = 0
      endif
    endif
    if i_Rows=0 AND not this.w_CCFLPART="A"
      ADDMSGNL("%1",THIS,ah_msgformat( "Elaborazione Movimento DocFinance N� %1 %0Si sta cercando di saldare una partita inesistente (PTSERIAL %3 PTROWORD %4 CPROWNUM %5)", Alltrim(this.w_ChiaveRottura), Alltrim(Str(this.w_Riga)), this.w_PTSERIAL, Alltrim(Str(this.w_PTROWORD)), Alltrim(Str(this.w_CPROWNUM))))
      this.w_Corretto = .F.
      this.w_ErrImp = .f.
      this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Si sta cercando di saldare una partita inesistente (PTSERIAL %1 PTROWORD %2 CPROWNUM %3)%0", this.w_PTSERIAL, Alltrim(Str(this.w_PTROWORD)), Alltrim(Str(this.w_CPROWNUM)))
    else
      * --- Controllo che la chiave Coge della partta che si sta saldando corrisponda al conto DocFinance movimentato
      if alltrim(upper(this.w_ANTIPCON)) <> alltrim(upper(this.w_PTTIPCON)) OR alltrim(upper(this.w_ANCODICE)) <> alltrim(upper(this.w_PTCODCON)) AND not this.w_CCFLPART="A"
        ADDMSGNL("%1",THIS,ah_msgformat( "Elaborazione Movimento DocFinance N� %1%0Si sta cercando di saldare una partita del %3 %4 su una riga DocFinance riferita al %5 %6 ", Alltrim(this.w_ChiaveRottura), Alltrim(Str(this.w_Riga)), IIF (this.w_PTTIPCON="C" , "cliente",IIF(this.w_PTTIPCON="F","fornitore","conto" )) , alltrim(upper(this.w_PTCODCON)) , IIF (alltrim(upper(this.w_ANTIPCON))="C" ,"cliente",IIF(alltrim(upper(this.w_ANTIPCON))="F","fornitore","conto" )) , alltrim(upper(this.w_ANCODICE)) ))
        this.w_Corretto = .F.
        this.w_ErrImp = .f.
        this.w_ERRORLOG = ah_msgformat( "Si sta cercando di saldare una partita del %1 %2 su una riga DocFinance riferita al %3 %4 ", IIF (this.w_PTTIPCON="C" , "cliente",IIF(this.w_PTTIPCON="F","fornitore","conto" )) , alltrim(upper(this.w_PTCODCON)) , IIF (alltrim(upper(this.w_ANTIPCON))="C" ,"cliente",IIF(alltrim(upper(this.w_ANTIPCON))="F","fornitore","conto" )) , alltrim(upper(this.w_ANCODICE)) )
      endif
      * --- Le partite in distinta e quelle ragruppate le saldo per pttotimp
      if (this.w_PTROWORD <> -2 OR this.w_CCFLPART="A") AND not this.w_DFPTTIPO="R"
        this.w_PTTOTIMP = this.w_PNIMPAVE + this.w_PNIMPDAR
      endif
      * --- Il segno della partia di saldo si determina in base al segno della partita di crea
      if this.w_PTROWORD <> -2
        if this.w_DFFLGDIS<>"D"
          if this.w_PNIMPAVE>0
            this.w_PT_SEGNO = "A"
          else
            this.w_PT_SEGNO = "D"
          endif
        else
          this.w_PT_SEGNO = IIF (this.w_PT_SEGNO="D","A", "D")
        endif
        * --- Se si saldano delle partite tramite i riferimenti ad una distinta allora si recuperano i rigferimenti della partita di origne da ptserrif ptordrif ptnumrif
        *     altrimenti i riferimenti alla partta di origine sono nelle variabili w_ptserial w_ptroword w_cprownum
        this.w_PTSERRIF = this.w_PTSERIAL
        this.w_PTORDRIF = this.w_PTROWORD
        this.w_PTNUMRIF = this.w_CPROWNUM
      endif
      * --- Per le partite ragruppate determino la parte aperta che bisogna saldare 
      if this.w_DFPTTIPO="R"
        * --- Select from ..\isdf\exe\query\gsdfTKMS.VQR
        do vq_exec with '..\isdf\exe\query\gsdfTKMS.VQR',this,'_Curs__d__d__isdf_exe_query_gsdfTKMS_d_VQR','',.f.,.t.
        if used('_Curs__d__d__isdf_exe_query_gsdfTKMS_d_VQR')
          select _Curs__d__d__isdf_exe_query_gsdfTKMS_d_VQR
          locate for 1=1
          do while not(eof())
          this.w_PTTOTIMP = ABS(SALDO)
          this.w_PT_SEGNO = IIF (SALDO>0 , "A", "D")
            select _Curs__d__d__isdf_exe_query_gsdfTKMS_d_VQR
            continue
          enddo
          use
        endif
      endif
      this.w_PTFLSOSP = " "
      this.w_PTNUMDIS = space(10)
      this.w_PTFLIMPE = SPACE(1)
      this.w_CAMBIOPAR = this.w_CAMBIO
      this.w_PTCODAGE = NVL (this.w_PTCODAGE,SPACE (5) )
      * --- Nel caso di chiave fisica ho una sola partita e gli assegno  l importo che arriva dal file mentre se ho chiave coge Ragruppata o distinta chuido le partite per l importo totale 
      if this.w_DFPTTIPO="C" 
      endif
      if this.w_CCFLPART="A"
        * --- Inserisci partite di acconto
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS;
            from (i_cTable) where;
                ANTIPCON = this.w_ANTIPCON;
                and ANCODICE = this.w_ANCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MESE = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
          this.w_MESE1 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
          this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
          this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
          this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ESCL2 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN2, 2, 0)
        this.w_ESCL1 = STR(this.w_MESE, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
        this.w_LOOP = 1
        DIMENSION w_ARRSCA[999,6]
        this.w_NURATE = SCADENZE("w_ARRSCA", this.w_COSAPAGA, this.w_PNDATREG, ABS ( this.w_DFIMPVAL ),0, 0, this.w_ESCL1, this.w_ESCL2, 2)
        * --- Ciclo sull'Array per determinare le scadenze
        this.w_LOOP = 1
        do while this.w_LOOP<=this.w_NURATE
          this.w_PTNUMPAR = CANUMPAR("S", this.w_PNCODESE, this.w_NUMPARDIS, SPACE (10))
          this.w_PTDATSCA = w_ARRSCA[ this.w_LOOP , 1]
          this.w_PTTOTIMP = w_ARRSCA[ this.w_LOOP , 2] + w_ARRSCA[ this.w_LOOP , 3]
          this.w_PTMODPAG = w_ARRSCA[ this.w_LOOP , 5]
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTMODPAG"+",PTBANAPP"+",PTNUMCOR"+",PTBANNOS"+",PT_SEGNO"+",PTTOTIMP"+",ptdesrig"+",PTCODVAL"+",PTTIPCON"+",PTCODCON"+",PTCAOVAL"+",PTFLCRSA"+",PTFLSOSP"+",PTFLIMPE"+",PTNUMDOC"+",PTALFDOC"+",PTCODAGE"+",PTDATAPE"+",PTCAOAPE"+",PTDATREG"+",PTIMPDOC"+",PTFLRAGG"+",PTFLVABD"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ULT_RNUM),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ULT_RNUMPA),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(ABS ( this.w_PTTOTIMP )),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PAR_TITE','ptdesrig');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVALDOCF),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAMBIO),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC("A"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLIMPE),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPARDIS),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(space(10)),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAMBIO),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
            +","+cp_NullLink(cp_ToStrODBC(ABS (this.w_PTTOTIMP)),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(1)),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(this.W_PTFLVABD),'PAR_TITE','PTFLVABD');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_ULT_RNUM,'CPROWNUM',this.w_ULT_RNUMPA,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTMODPAG',this.w_PTMODPAG,'PTBANAPP',this.w_PTBANAPP,'PTNUMCOR',this.w_PTNUMCOR,'PTBANNOS',this.w_PTBANNOS,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',ABS ( this.w_PTTOTIMP ),'ptdesrig',this.w_PNDESRIG)
            insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTMODPAG,PTBANAPP,PTNUMCOR,PTBANNOS,PT_SEGNO,PTTOTIMP,ptdesrig,PTCODVAL,PTTIPCON,PTCODCON,PTCAOVAL,PTFLCRSA,PTFLSOSP,PTFLIMPE,PTNUMDOC,PTALFDOC,PTCODAGE,PTDATAPE,PTCAOAPE,PTDATREG,PTIMPDOC,PTFLRAGG,PTFLVABD &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_ULT_RNUM;
                 ,this.w_ULT_RNUMPA;
                 ,this.w_PTNUMPAR;
                 ,this.w_PTDATSCA;
                 ,this.w_PTMODPAG;
                 ,this.w_PTBANAPP;
                 ,this.w_PTNUMCOR;
                 ,this.w_PTBANNOS;
                 ,this.w_PT_SEGNO;
                 ,ABS ( this.w_PTTOTIMP );
                 ,this.w_PNDESRIG;
                 ,this.w_PNCODVALDOCF;
                 ,this.w_PTTIPCON;
                 ,this.w_PTCODCON;
                 ,this.w_CAMBIO;
                 ,"A";
                 ,this.w_PTFLSOSP;
                 ,this.w_PTFLIMPE;
                 ,this.w_NUMPARDIS;
                 ,space(10);
                 ,this.w_PTCODAGE;
                 ,this.w_PNDATREG;
                 ,this.w_CAMBIO;
                 ,this.w_PNDATREG;
                 ,ABS (this.w_PTTOTIMP);
                 ,SPACE(1);
                 ,this.W_PTFLVABD;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ULT_RNUMPA = this.w_ULT_RNUMPA + 1
          this.w_LOOP = this.w_LOOP + 1
        enddo
        this.w_COSAPAGA = SPACE(10)
      else
        * --- Inserisci partite di saldo
        this.w_PTFLRAGG = IIF (this.w_ULT_RNUM<>-2 , " " , this.w_PTFLRAGG )
        * --- Insert into PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTMODPAG"+",PTBANAPP"+",PT_SEGNO"+",PTTOTIMP"+",ptdesrig"+",PTCODVAL"+",PTTIPCON"+",PTCODCON"+",PTCAOVAL"+",PTFLCRSA"+",PTFLSOSP"+",PTFLRITE"+",PTNUMDIS"+",PTFLIMPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTCODAGE"+",PTDATAPE"+",PTCAOAPE"+",PTDATREG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTIMPDOC"+",PTFLRAGG"+",PTFLINDI"+",PTFLVABD"+",PTRIFIND"+",PTNUMEFF"+",PTBANNOS"+",PTDATVAL"+",PTNUMCOR"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ULT_RNUM),'PAR_TITE','PTROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ULT_RNUMPA),'PAR_TITE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
          +","+cp_NullLink(cp_ToStrODBC(ABS (IIF ( this.w_DFPTTIPO="C" , this.w_DFIMPVAL , this.w_PTTOTIMP ))),'PAR_TITE','PTTOTIMP');
          +","+cp_NullLink(cp_ToStrODBC(IIF (not EMPTY ( this.w_CADESRIG) , Left(this.w_CADESRIG,35),this.w_ptdesrig )),'PAR_TITE','ptdesrig');
          +","+cp_NullLink(cp_ToStrODBC(IIF ( this.w_DFPTTIPO="C" , this.w_PNCODVALDOCF , this.w_PTCODVAL )),'PAR_TITE','PTCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAMBIOPAR),'PAR_TITE','PTCAOVAL');
          +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRITE),'PAR_TITE','PTFLRITE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDIS),'PAR_TITE','PTNUMDIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLIMPE),'PAR_TITE','PTFLIMPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERRIF),'PAR_TITE','PTSERRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTORDRIF),'PAR_TITE','PTORDRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMRIF),'PAR_TITE','PTNUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(ABS (this.w_PTTOTIMP)),'PAR_TITE','PTIMPDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRAGG),'PAR_TITE','PTFLRAGG');
          +","+cp_NullLink(cp_ToStrODBC(SPACE (1)),'PAR_TITE','PTFLINDI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTRIFIND');
          +","+cp_NullLink(cp_ToStrODBC(this.w_EFFETTO),'PAR_TITE','PTNUMEFF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
          +","+cp_NullLink(cp_ToStrODBC(IIF (this.w_DFFLGDIS="D" , this.w_CCDATVAL , cp_CharToDate("   -  -    ") )),'PAR_TITE','PTDATVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_ULT_RNUM,'CPROWNUM',this.w_ULT_RNUMPA,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTMODPAG',this.w_PTMODPAG,'PTBANAPP',this.w_PTBANAPP,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',ABS (IIF ( this.w_DFPTTIPO="C" , this.w_DFIMPVAL , this.w_PTTOTIMP )),'ptdesrig',IIF (not EMPTY ( this.w_CADESRIG) , Left(this.w_CADESRIG,35),this.w_ptdesrig ),'PTCODVAL',IIF ( this.w_DFPTTIPO="C" , this.w_PNCODVALDOCF , this.w_PTCODVAL ),'PTTIPCON',this.w_PTTIPCON)
          insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTMODPAG,PTBANAPP,PT_SEGNO,PTTOTIMP,ptdesrig,PTCODVAL,PTTIPCON,PTCODCON,PTCAOVAL,PTFLCRSA,PTFLSOSP,PTFLRITE,PTNUMDIS,PTFLIMPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTCODAGE,PTDATAPE,PTCAOAPE,PTDATREG,PTSERRIF,PTORDRIF,PTNUMRIF,PTIMPDOC,PTFLRAGG,PTFLINDI,PTFLVABD,PTRIFIND,PTNUMEFF,PTBANNOS,PTDATVAL,PTNUMCOR &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_ULT_RNUM;
               ,this.w_ULT_RNUMPA;
               ,this.w_PTNUMPAR;
               ,this.w_PTDATSCA;
               ,this.w_PTMODPAG;
               ,this.w_PTBANAPP;
               ,this.w_PT_SEGNO;
               ,ABS (IIF ( this.w_DFPTTIPO="C" , this.w_DFIMPVAL , this.w_PTTOTIMP ));
               ,IIF (not EMPTY ( this.w_CADESRIG) , Left(this.w_CADESRIG,35),this.w_ptdesrig );
               ,IIF ( this.w_DFPTTIPO="C" , this.w_PNCODVALDOCF , this.w_PTCODVAL );
               ,this.w_PTTIPCON;
               ,this.w_PTCODCON;
               ,this.w_CAMBIOPAR;
               ,"S";
               ,this.w_PTFLSOSP;
               ,this.w_PTFLRITE;
               ,this.w_PTNUMDIS;
               ,this.w_PTFLIMPE;
               ,this.w_PTNUMDOC;
               ,this.w_PTALFDOC;
               ,this.w_PTDATDOC;
               ,this.w_PTCODAGE;
               ,this.w_PTDATAPE;
               ,this.w_PTCAOAPE;
               ,this.w_PNDATREG;
               ,this.w_PTSERRIF;
               ,this.w_PTORDRIF;
               ,this.w_PTNUMRIF;
               ,ABS (this.w_PTTOTIMP);
               ,this.w_PTFLRAGG;
               ,SPACE (1);
               ,this.w_PTFLVABD;
               ,SPACE(10);
               ,this.w_EFFETTO;
               ,this.w_PTBANNOS;
               ,IIF (this.w_DFFLGDIS="D" , this.w_CCDATVAL , cp_CharToDate("   -  -    ") );
               ,this.w_PTNUMCOR;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_ULT_RNUMPA = this.w_ULT_RNUMPA + 1
      endif
    endif
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import Movimenti di primanota da DocFinance
    this.w_CONTAAZ = 0
    ADDMSGNL("Inizio importazione movimenti nella tabella di preelaborazione %0",THIS) 
    CREATE CURSOR CursTEMPO (DATIEL C(250) ,DATIEL1 C(250) ,DATIEL2 C(250) ,DATIEL3 C(250) ,DATIEL4 C(250) ) 
 APPEND FROM (this.oParentObject.w_PATH) TYPE SDF
    * --- FASE 1 : Caricamento della tabella di Preelaborazione  (se si genera un errore nel caricamento della tabella di preelaborazione allora si interrompe l imoportazione)
    this.w_CONTAMOV = 0
    this.w_CONTAINS = 0
    * --- begin transaction
    cp_BeginTrs()
    SELECT CursTempo 
 GO TOP 
 SCAN
    * --- Carica Tabella di Preelaborazione
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if alltrim (upper (this.oParentObject.w_DFTRAAZI)) = alltrim (upper ( this.w_AZIENDA))
      if this.w_CHIAVEMOV <> this.w_CHIAVEROTTURA
        * --- Se � attiva la trascodifica allora � la causale che decide se creare un registrazione di primanota o una distinta
        * --- Read from DOCTRCAU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOCTRCAU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCTRCAU_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRTIPCAU"+;
            " from "+i_cTable+" DOCTRCAU where ";
                +"TRCAUDOC = "+cp_ToStrODBC(this.w_PNCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRTIPCAU;
            from (i_cTable) where;
                TRCAUDOC = this.w_PNCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TRTIPCAU = NVL(cp_ToDate(_read_.TRTIPCAU),cp_NullValue(_read_.TRTIPCAU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DFFLGDIS = IIF ( this.oParentObject.w_DFTRASCA="S" , this.w_TRTIPCAU , this.w_DFFLGDIS)
        this.w_RIGAMOV = 1
        this.w_CONTAMOV = this.w_CONTAMOV + 1
        this.w_CCCONTRTESTATA = this.w_CCCONTR
        this.w_CCPIANOCTESTATA = this.w_CCPIANOC
        this.w_FLAGDISTINTATESTATA = this.w_DFFLGDIS
        * --- Read from DOCFINIM
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOCFINIM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DFCODAZI,DFSERTES"+;
            " from "+i_cTable+" DOCFINIM where ";
                +"DFCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
                +" and DFNUMERA = "+cp_ToStrODBC(this.w_NUMERATORE);
                +" and DF__ANNO = "+cp_ToStrODBC(this.w_ANNO);
                +" and DFMOVIME = "+cp_ToStrODBC(this.w_MOVIMENTO);
                +" and DFDATREG = "+cp_ToStrODBC(this.w_PNDATREG);
                +" and DFCONTRO = "+cp_ToStrODBC(this.w_CCCONTRTESTATA);
                +" and DFPIANOC = "+cp_ToStrODBC(this.w_CCPIANOCTESTATA);
                +" and DFCHIAVE = "+cp_ToStrODBC(this.w_CHIAVEFILE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DFCODAZI,DFSERTES;
            from (i_cTable) where;
                DFCODAZI = this.w_AZIENDA;
                and DFNUMERA = this.w_NUMERATORE;
                and DF__ANNO = this.w_ANNO;
                and DFMOVIME = this.w_MOVIMENTO;
                and DFDATREG = this.w_PNDATREG;
                and DFCONTRO = this.w_CCCONTRTESTATA;
                and DFPIANOC = this.w_CCPIANOCTESTATA;
                and DFCHIAVE = this.w_CHIAVEFILE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LETTODOCFIN = NVL(cp_ToDate(_read_.DFCODAZI),cp_NullValue(_read_.DFCODAZI))
          this.w_LETTO = NVL(cp_ToDate(_read_.DFSERTES),cp_NullValue(_read_.DFSERTES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY (this.w_LETTO) AND this.oParentObject.w_SOVRASCRIVI="S"
          * --- Try
          local bErr_04E70908
          bErr_04E70908=bTrsErr
          this.Try_04E70908()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ADDMSGNL("%1",THIS,ah_msgformat( "Impossibile modificare il record nella tabella di preelaborazione %1 " , message() ))
            this.w_Corretto = .F.
            this.w_ErrImp = .f.
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04E70908
          * --- End
        endif
        if EMPTY (this.w_LETTO) AND (EMPTY (this.w_LETTODOCFIN) OR this.oParentObject.w_SOVRASCRIVI ="S" )
          this.w_CONTAINS = this.w_CONTAINS + 1
        endif
        if EMPTY (this.w_LETTO) 
          ADDMSGNL("Import movimento %1 ( %2 )",THIS,this.w_CONTAINS , this.w_CHIAVEROTTURA)
        endif
        if (NOT EMPTY (this.w_LETTODOCFIN) AND this.oParentObject.w_SOVRASCRIVI <>"S") OR (not EMPTY (this.w_LETTO) AND this.oParentObject.w_SOVRASCRIVI ="S")
          ADDMSGNL("%1",THIS,ah_msgformat( "Movimento gi� inserito  " ))
        endif
      endif
      this.w_CHIAVEMOV = this.w_CHIAVEROTTURA
      if EMPTY (this.w_LETTO) AND (EMPTY (this.w_LETTODOCFIN) OR this.oParentObject.w_SOVRASCRIVI ="S" )
        * --- Try
        local bErr_04E40A68
        bErr_04E40A68=bTrsErr
        this.Try_04E40A68()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ADDMSGNL("%1",THIS,ah_msgformat( "Impossibile sovrascrivere il dettaglio della tabella di preelaborazione  " ))
          this.w_LETTO = "ERRORE"
          this.w_Corretto = .F.
          this.w_ErrImp = .f.
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04E40A68
        * --- End
      endif
      * --- Il legame con il log lsi mette sempre
      * --- Write into DOCFINIM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOCFINIM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOCFINIM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DFCSERIA ="+cp_NullLink(cp_ToStrODBC(this.w_DFCSERIA),'DOCFINIM','DFCSERIA');
            +i_ccchkf ;
        +" where ";
            +"DFCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
            +" and DFNUMERA = "+cp_ToStrODBC(this.w_NUMERATORE);
            +" and DF__ANNO = "+cp_ToStrODBC(this.w_ANNO);
            +" and DFMOVIME = "+cp_ToStrODBC(this.w_MOVIMENTO);
            +" and DFDATREG = "+cp_ToStrODBC(this.w_PNDATREG);
            +" and DFCONTRO = "+cp_ToStrODBC(this.w_CCCONTRTESTATA);
            +" and DFPIANOC = "+cp_ToStrODBC(this.w_CCPIANOCTESTATA);
            +" and DFCHIAVE = "+cp_ToStrODBC(this.w_CHIAVEFILE);
               )
      else
        update (i_cTable) set;
            DFCSERIA = this.w_DFCSERIA;
            &i_ccchkf. ;
         where;
            DFCODAZI = this.w_AZIENDA;
            and DFNUMERA = this.w_NUMERATORE;
            and DF__ANNO = this.w_ANNO;
            and DFMOVIME = this.w_MOVIMENTO;
            and DFDATREG = this.w_PNDATREG;
            and DFCONTRO = this.w_CCCONTRTESTATA;
            and DFPIANOC = this.w_CCPIANOCTESTATA;
            and DFCHIAVE = this.w_CHIAVEFILE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_RIGAMOV = this.w_RIGAMOV + 1
    else
      this.w_CONTAAZ = this.w_CONTAAZ + 1
    endif
    SELECT CursTempo
    ENDSCAN
    * --- Avvisa che ci sono movimenti di altre aziende
    if this.w_CONTAAZ>0
      ADDMSGNL("%1 Movimenti di altre aziende presenti nel file",THIS, this.w_CONTAAZ ) 
    endif
    * --- Se si sono verificati errori nel caricamento della tabella di preelaborazione viene anullato l import
    if this.w_Corretto
      * --- commit
      cp_EndTrs(.t.)
      ADDMSGNL("%1 Movimenti importati nella tabella di preelaborazione su %2 movimenti presenti nel file%0",THIS, this.w_CONTAINS, this.w_CONTAMOV ) 
    else
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    this.oParentObject.NotifyEvent("Aggiorna")
    this.w_CHIAVEMOV = "@@@"
    this.w_CONTAMOV = 0
    this.w_CONTAINS = 0
    * --- FASE 2  : Caricamento dei movimenti di primanota (i movimenti vengono generati solo se non si sonon verificati errori nel caricamento della tabella di preelaborazione)
    this.w_CONTARECORDDOCFIN = this.w_CONTARECORD
    if this.w_Corretto
      * --- -- Fa copia di backup del file e cancella originale
      this.w_COPIAFILE = .T.
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_04E70908()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from DOCFINIM
    i_nConn=i_TableProp[this.DOCFINIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DFCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
            +" and DFNUMERA = "+cp_ToStrODBC(this.w_NUMERATORE);
            +" and DF__ANNO = "+cp_ToStrODBC(this.w_ANNO);
            +" and DFMOVIME = "+cp_ToStrODBC(this.w_MOVIMENTO);
            +" and DFDATREG = "+cp_ToStrODBC(this.w_PNDATREG);
            +" and DFCONTRO = "+cp_ToStrODBC(this.w_CCCONTRTESTATA);
            +" and DFPIANOC = "+cp_ToStrODBC(this.w_CCPIANOCTESTATA);
            +" and DFCHIAVE = "+cp_ToStrODBC(this.w_CHIAVEFILE);
             )
    else
      delete from (i_cTable) where;
            DFCODAZI = this.w_AZIENDA;
            and DFNUMERA = this.w_NUMERATORE;
            and DF__ANNO = this.w_ANNO;
            and DFMOVIME = this.w_MOVIMENTO;
            and DFDATREG = this.w_PNDATREG;
            and DFCONTRO = this.w_CCCONTRTESTATA;
            and DFPIANOC = this.w_CCPIANOCTESTATA;
            and DFCHIAVE = this.w_CHIAVEFILE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04E40A68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_DFPTTIPO="C"
      * --- Per le partite con chiave fisica recuperio il nummero effetto della partita di crea per visualizzarlo nella maschera dei movimenti DocFinance
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTNUMEFF"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTNUMEFF;
          from (i_cTable) where;
              PTSERIAL = this.w_PTSERIAL;
              and PTROWORD = this.w_PTROWORD;
              and CPROWNUM = this.w_CPROWNUM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTNUMEFF = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Insert into DOCFINIM
    i_nConn=i_TableProp[this.DOCFINIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOCFINIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DFCODAZI"+",DFNUMERA"+",DF__ANNO"+",DFMOVIME"+",DFDATREG"+",DFCONTRO"+",DFPIANOC"+",DFDATIMP"+",DFNOMFIL"+",DFCHIAVE"+",CPROWNUM"+",DF_BANCA"+",DFCODRBN"+",DFCODCAU"+",DFCODVAC"+",DFIMPVAL"+",DFCODVAL"+",DFIMPORT"+",DFTIPCON"+",DFCODICE"+",DFDATVAL"+",DF__NOTE"+",DFNUMPAR"+",DFCODBUN"+",DFPTSERI"+",DFPTROW"+",DFCPROW"+",DFNUMEFF"+",DFDESOP"+",DFERRLOG"+",DFFLGMOV"+",DFERROR"+",DFDATSCA"+",DFMODPAG"+",DFPTTIPO"+",DFFLGDIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_AZIENDA),'DOCFINIM','DFCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMERATORE),'DOCFINIM','DFNUMERA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANNO),'DOCFINIM','DF__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MOVIMENTO),'DOCFINIM','DFMOVIME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'DOCFINIM','DFDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCONTRTESTATA),'DOCFINIM','DFCONTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCPIANOCTESTATA),'DOCFINIM','DFPIANOC');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DOCFINIM','DFDATIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PATH),'DOCFINIM','DFNOMFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CHIAVEFILE),'DOCFINIM','DFCHIAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIGAMOV),'DOCFINIM','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BANCA),'DOCFINIM','DF_BANCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RBN),'DOCFINIM','DFCODRBN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'DOCFINIM','DFCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'DOCFINIM','DFCODVAC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DFIMPVAL),'DOCFINIM','DFIMPVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODVAL),'DOCFINIM','DFCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTROVALOREDEB - this.w_CONTROVALORECRE ),'DOCFINIM','DFIMPORT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'DOCFINIM','DFTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'DOCFINIM','DFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDATVAL),'DOCFINIM','DFDATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADESRIG),'DOCFINIM','DF__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPARDIS),'DOCFINIM','DFNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEZIONEAZIENDALE),'DOCFINIM','DFCODBUN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'DOCFINIM','DFPTSERI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'DOCFINIM','DFPTROW');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOCFINIM','DFCPROW');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'DOCFINIM','DFNUMEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESOP),'DOCFINIM','DFDESOP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ERRORLOG),'DOCFINIM','DFERRLOG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOCFINIM','DFFLGMOV');
      +","+cp_NullLink(cp_ToStrODBC("N"),'DOCFINIM','DFERROR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DFDATSCA),'DOCFINIM','DFDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DFMODPAG),'DOCFINIM','DFMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DFPTTIPO),'DOCFINIM','DFPTTIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLAGDISTINTATESTATA),'DOCFINIM','DFFLGDIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DFCODAZI',this.w_AZIENDA,'DFNUMERA',this.w_NUMERATORE,'DF__ANNO',this.w_ANNO,'DFMOVIME',this.w_MOVIMENTO,'DFDATREG',this.w_PNDATREG,'DFCONTRO',this.w_CCCONTRTESTATA,'DFPIANOC',this.w_CCPIANOCTESTATA,'DFDATIMP',i_datsys,'DFNOMFIL',this.oParentObject.w_PATH,'DFCHIAVE',this.w_CHIAVEFILE,'CPROWNUM',this.w_RIGAMOV,'DF_BANCA',this.w_BANCA)
      insert into (i_cTable) (DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME,DFDATREG,DFCONTRO,DFPIANOC,DFDATIMP,DFNOMFIL,DFCHIAVE,CPROWNUM,DF_BANCA,DFCODRBN,DFCODCAU,DFCODVAC,DFIMPVAL,DFCODVAL,DFIMPORT,DFTIPCON,DFCODICE,DFDATVAL,DF__NOTE,DFNUMPAR,DFCODBUN,DFPTSERI,DFPTROW,DFCPROW,DFNUMEFF,DFDESOP,DFERRLOG,DFFLGMOV,DFERROR,DFDATSCA,DFMODPAG,DFPTTIPO,DFFLGDIS &i_ccchkf. );
         values (;
           this.w_AZIENDA;
           ,this.w_NUMERATORE;
           ,this.w_ANNO;
           ,this.w_MOVIMENTO;
           ,this.w_PNDATREG;
           ,this.w_CCCONTRTESTATA;
           ,this.w_CCPIANOCTESTATA;
           ,i_datsys;
           ,this.oParentObject.w_PATH;
           ,this.w_CHIAVEFILE;
           ,this.w_RIGAMOV;
           ,this.w_BANCA;
           ,this.w_RBN;
           ,this.w_PNCODCAU;
           ,this.w_PNCODVAL;
           ,this.w_DFIMPVAL;
           ,this.w_CCCODVAL;
           ,this.w_CONTROVALOREDEB - this.w_CONTROVALORECRE ;
           ,this.w_ANTIPCON;
           ,this.w_ANCODICE;
           ,this.w_CCDATVAL;
           ,this.w_CADESRIG;
           ,this.w_NUMPARDIS;
           ,this.w_SEZIONEAZIENDALE;
           ,this.w_PTSERIAL;
           ,this.w_PTROWORD;
           ,this.w_CPROWNUM;
           ,this.w_PTNUMEFF;
           ,this.w_DESOP;
           ,this.w_ERRORLOG;
           ," ";
           ,"N";
           ,this.w_DFDATSCA;
           ,this.w_DFMODPAG;
           ,this.w_DFPTTIPO;
           ,this.w_FLAGDISTINTATESTATA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il log degli errori deve essere sempre scritto percio bisogna aprire una 2� transazione per evitare di fare il rollback assieme al rollback dei movimenti di Primanota
    local w_nConn, w_cTable,w_ccchkf , w_newconn 
 w_nConn=i_TableProp[this.DOCFINIM_idx,3] 
 w_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2]) 
 w_ccchkf=""
    * --- Apre una seconda connessione per scrivere sullla tabella DOCFINIM
    w_newconn=sqlstringconnect(sqlgetprop(w_nConn,"ConnectString"))
    if w_newconn<=0
      ADDMSGNL("%0Errore nell apertura della 2� connessione per la scrittura del log : %1 %0",Message()) 
      this.w_Corretto = .F.
      this.w_ErrImp = .f.
    else
      cp_PostOpenConn(w_newconn)
    endif
    this.w_CONTARECORD = 0
    this.w_RIGA = 0
    if this.w_corretto
      * --- Select from DOCFINIM
      i_nConn=i_TableProp[this.DOCFINIM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOCFINIM ";
            +" where DFERROR<>'S' AND DFFLGMOV<>'S' AND DFCODAZI = "+cp_ToStrODBC(this.oParentObject.w_DFTRAAZI)+"";
            +" order by DFCODAZI,DF__ANNO,DFDATREG,DFNUMERA,DFMOVIME,DFCONTRO,DFPIANOC,DFCHIAVE,CPROWNUM";
             ,"_Curs_DOCFINIM")
      else
        select * from (i_cTable);
         where DFERROR<>"S" AND DFFLGMOV<>"S" AND DFCODAZI = this.oParentObject.w_DFTRAAZI;
         order by DFCODAZI,DF__ANNO,DFDATREG,DFNUMERA,DFMOVIME,DFCONTRO,DFPIANOC,DFCHIAVE,CPROWNUM;
          into cursor _Curs_DOCFINIM
      endif
      if used('_Curs_DOCFINIM')
        select _Curs_DOCFINIM
        locate for 1=1
        do while not(eof())
        * --- Import Movimenti di primanota da DocFinance
        this.w_RIGA = this.w_RIGA + 1
        this.w_AZIENDA = DFCODAZI
        this.w_CONTARECORD = this.w_CONTARECORD + 1
        * --- Valorizza tutte le variabili di appoggio con i valori contenuti nel file e f� le conversioni di tipo
        this.w_NUMERATORE = DFNUMERA
        this.w_ANNO = DF__ANNO
        this.w_MOVIMENTO = DFMOVIME
        this.w_DFCHIAVE = DFCHIAVE
        * --- Numero progressivo all interno del file asci, serve come chiave di rottura del movimento
        *     Pos 29 - 35
        this.w_CHIAVEROTTURA = DFCODAZI +"/" +DFNUMERA +"/" + DF__ANNO +"/" + DFMOVIME +"/" + dtoc(DFDATREG) +"/" + DFCONTRO +"/" + DFPIANOC + "/" + this.w_DFCHIAVE
        * --- Data contabilizzazione della banca  Pos 35 - 43
        this.w_PNDATREG = DFDATREG
        this.w_BANCA = DF_BANCA
        this.w_RBN = DFCODRBN
        * --- Causale contabile corrispondente al movimento di primanota Pos 63 - 67
        this.w_PNCODCAU = DFCODCAU
        * --- Nei dati generali Sedoc � attiva la trascodifica della causale
        if this.oParentObject.w_DFTRASCA="S" 
          * --- Read from DOCTRCAU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOCTRCAU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOCTRCAU_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCAUCON,TRCAUDIS,TRTIPCAU"+;
              " from "+i_cTable+" DOCTRCAU where ";
                  +"TRCAUDOC = "+cp_ToStrODBC(this.w_PNCODCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCAUCON,TRCAUDIS,TRTIPCAU;
              from (i_cTable) where;
                  TRCAUDOC = this.w_PNCODCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PNCODCAU = NVL(cp_ToDate(_read_.TRCAUCON),cp_NullValue(_read_.TRCAUCON))
            this.w_TRCAUDIS = NVL(cp_ToDate(_read_.TRCAUDIS),cp_NullValue(_read_.TRCAUDIS))
            this.w_TRTIPCAU = NVL(cp_ToDate(_read_.TRTIPCAU),cp_NullValue(_read_.TRTIPCAU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_TRTIPCAU="D"
            this.w_PNCODCAU = this.w_TRCAUDIS
          endif
          if EMPTY (this.w_PNCODCAU)
            if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
              ADDMSGNL("%1",THIS,ah_msgformat( "Causale %1 senza trascodifica" , alltrim(DFCODCAU) ))
            endif
            this.w_Corretto = .F.
            this.w_ErrImp = .f.
            this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Causale %1 senza trascodifica %0" , alltrim(DFCODCAU) )
          endif
        endif
        * --- Valuta in divisa Pos 67 - 72
        this.w_CCCODISO = DFCODVAC
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACODVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODISO = "+cp_ToStrODBC(this.w_CCCODISO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACODVAL;
            from (i_cTable) where;
                VACODISO = this.w_CCCODISO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PNCODVAL = NVL(cp_ToDate(_read_.VACODVAL),cp_NullValue(_read_.VACODVAL))
          this.w_PNCODVALDOCF = NVL(cp_ToDate(_read_.VACODVAL),cp_NullValue(_read_.VACODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Nel file arriva il codice iso della valuta (recupero la valuta)
        * --- Il segno dell importo nel file ci indica il segno del movimento di primanota Pos 72 - 94
        * --- IMPORTI DA ROCOSTRUIRE NELLA LETTURA DA TABELLA (sono DDIMPVAL , i� un importo con segno )
        this.w_SEGNO = IIF (DFIMPORT <0 , "-","+")
        this.w_DFIMPVAL = DFIMPVAL
        if this.w_SEGNO="-"
          this.w_PNIMPAVE = ABS (this.w_DFIMPVAL )
          this.w_PNIMPDAR = 0
        else
          this.w_PNIMPAVE = 0
          this.w_PNIMPDAR = ABS (this.w_DFIMPVAL )
        endif
        * --- Valuta in divisa azienda Pos 94 - 99
        this.w_CCCODVAL = left (alltrim (DFCODVAL),3)
        * --- Controvalore in divisa azienda Pos 99 -121
        * --- IMPORTI DA ROCOSTRUIRE NELLA LETTURA DA TABELLA (sono DDIMPORT , i� un importo con segno )
        this.w_CONTROVALORE = ABS (DFIMPORT)
        if DFIMPORT<0
          this.w_CONTROVALORECRE = this.w_CONTROVALORE
          this.w_CONTROVALOREDEB = 0
        else
          this.w_CONTROVALORECRE = 0
          this.w_CONTROVALOREDEB = this.w_CONTROVALORE
        endif
        * --- Piano dei conti e controvalore utilizzati come chiave del movimento da DocFinance (da specifiche di prodotto)
        this.w_CCCONTR = DFCONTRO
        this.w_CCPIANOC = DFPIANOC
        * --- Rapporto di cambio ricavato dagli importi del movimento espressi nella valuta movimentata e dalla valuta dell azienda
        this.w_CCCAOVAT = Abs (IIF (this.w_CONTROVALORE<>0,(this.w_PNIMPAVE+this.w_PNIMPDAR)/this.w_CONTROVALORE,1) )
        * --- Data valuta del movimento Pos 121- 129
        this.w_CCDATVAL = DFDATVAL
        * --- Note del movimento sulle righe di contropartita Pos 129 - 169
        this.w_CADESRIG = DF__NOTE
        * --- Numero partita Pos 171 - 181
        this.w_NUMPARDIS = DFNUMPAR
        * --- Codice contropartita  Pos 181 - 197   (� il campo Piano dei conti del tracciato)
        * --- TIPO E CODICE SONO IN DFTICPON E DFCODCON
        this.w_ANTIPCON = IIF (DFTIPCON="-" , "G",DFTIPCON)
        this.w_ANCODICE = DFCODICE
        this.w_SEZIONEAZIENDALE = DFCODBUN
        * --- Chiave COGE che identifica la partita Pos 209 - 235
        *     La chiave coge contiene PTSERIAL (lunghezza 10). PTROWORD(lunghezza 4), CPROWNUM (lunghezza 1) della partita 
        this.w_PTSERIAL = DFPTSERI
        this.w_PTROWORD = DFPTROW
        this.w_CPROWNUM = DFCPROW
        this.w_PTNUMEFF = DFNUMEFF
        this.w_DFPTTIPO = DFPTTIPO
        this.w_DFMODPAG = DFMODPAG
        this.w_DFDATSCA = DFDATSCA
        this.w_DESOP = DFDESOP
        this.w_RIGAMOV = CPROWNUM
        this.w_DFFLGDIS = DFFLGDIS
        * --- Determina se nel movimento ci sono conto che devono esserre registrati come provvisori
        if this.w_CHIAVEMOV <> this.w_CHIAVEROTTURA
          this.w_FLAG_PROV_CONTO = " "
          this.w_FLAG_PROV_CAUS = " "
          * --- Select from GSDF_BMT.VQR
          local _hHULMDDZNRG
          _hHULMDDZNRG=createobject('prm_container')
          addproperty(_hHULMDDZNRG,'w_AZIENDA',this.w_AZIENDA)
          addproperty(_hHULMDDZNRG,'w_NUMERATORE',this.w_NUMERATORE)
          addproperty(_hHULMDDZNRG,'w_ANNO',this.w_ANNO)
          addproperty(_hHULMDDZNRG,'w_MOVIMENTO',this.w_MOVIMENTO)
          addproperty(_hHULMDDZNRG,'w_PNDATREG',this.w_PNDATREG)
          addproperty(_hHULMDDZNRG,'w_CCCONTR',this.w_CCCONTR)
          addproperty(_hHULMDDZNRG,'w_CCPIANOC',this.w_CCPIANOC)
          addproperty(_hHULMDDZNRG,'w_DFTRASCA',this.oParentObject.w_DFTRASCA)
          addproperty(_hHULMDDZNRG,'w_DFCHIAVE',this.w_DFCHIAVE)
          do vq_exec with 'GSDF_BMT.VQR',_hHULMDDZNRG,'_Curs_GSDF_BMT_d_VQR','',.f.,.t.
          if used('_Curs_GSDF_BMT_d_VQR')
            select _Curs_GSDF_BMT_d_VQR
            locate for 1=1
            do while not(eof())
            this.w_FLAG_PROV_CONTO = NVL ( IEFLPROV , "  ")
            this.w_FLAG_PROV_CAUS = NVL (IMTIPCON , " ")
              select _Curs_GSDF_BMT_d_VQR
              continue
            enddo
            use
          endif
          * --- Verifica se la registrazione di primanota deve essere impostata a provvisoria
          *     Per determinare se la registrazione � provvisoria si guarda nell ordine : 
          *     - il conto ha il flag provvisorio settato nella tabella DOCIXCON
          *     - Se il conto non ha il flag provvisorio allora si v� a vedere se al conto che si st� movimentando � associata la causale su cui lo si movimenta (l associazione � nella tabella DOCIXIMP)
          *     - Nell anagrafica dei dati generali di DocFinance � attivo il check provvisorio
          *     Basta un solo conto tra quelli movimentati che abbia il flag provvisorio che tutta la registrazione v� impostata a provvisoria 
          * --- Determino l impostazione iniziale del flag provvisorio (se per uno qualsiasi dei conti movimentati fosse impostato il falg provvisorio allora bisogner� fare un update per mettere a provvisorio tutto il movimento )
          this.w_FLAG_PROV = this.oParentObject.w_DFFLPROV
          if this.oParentObject.w_DFFLPROV="S" OR NVL (this.w_FLAG_PROV_CONTO , " ") ="S" OR NVL (this.w_FLAG_PROV_CAUS," ")="S"
            * --- Basta 1 conto che abbia il flag provvisorio che tutta la registrazione viene impostata a provvisoria
            this.w_FLAG_PROV = "S"
          endif
          * --- Verifica se a qualche conto manca l analitica della registrazione di primanota che si andr� a fare
          if this.w_DFFLGDIS<>"D"
            * --- Read from CAU_CONT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAU_CONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CCFLANAL"+;
                " from "+i_cTable+" CAU_CONT where ";
                    +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CCFLANAL;
                from (i_cTable) where;
                    CCCODICE = this.w_PNCODCAU;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MANCA_ANALITICA = .F.
            * --- Select from GSDF1BMT.VQR
            local _hRSLYVYEMVY
            _hRSLYVYEMVY=createobject('prm_container')
            addproperty(_hRSLYVYEMVY,'w_AZIENDA',this.w_AZIENDA)
            addproperty(_hRSLYVYEMVY,'w_NUMERATORE',this.w_NUMERATORE)
            addproperty(_hRSLYVYEMVY,'w_ANNO',this.w_ANNO)
            addproperty(_hRSLYVYEMVY,'w_MOVIMENTO',this.w_MOVIMENTO)
            addproperty(_hRSLYVYEMVY,'w_PNDATREG',this.w_PNDATREG)
            addproperty(_hRSLYVYEMVY,'w_CCCONTR',this.w_CCCONTR)
            addproperty(_hRSLYVYEMVY,'w_CCPIANOC',this.w_CCPIANOC)
            addproperty(_hRSLYVYEMVY,'w_DFCHIAVE',this.w_DFCHIAVE)
            do vq_exec with 'GSDF1BMT.VQR',_hRSLYVYEMVY,'_Curs_GSDF1BMT_d_VQR','',.f.,.t.
            if used('_Curs_GSDF1BMT_d_VQR')
              select _Curs_GSDF1BMT_d_VQR
              locate for 1=1
              do while not(eof())
              this.w_DFCODICECTR = DFCODICE
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCONSUP,ANCCTAGG"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC("G");
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_DFCODICECTR);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCONSUP,ANCCTAGG;
                  from (i_cTable) where;
                      ANTIPCON = "G";
                      and ANCODICE = this.w_DFCODICECTR;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                w_MASTRO = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
                this.w_ANCCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_COSTORIC = CALCSEZ(w_MASTRO)
              this.w_CONTA = 0
              * --- Select from COLLCENT
              i_nConn=i_TableProp[this.COLLCENT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select count ( MRCODICE ) AS CONTA  from "+i_cTable+" COLLCENT ";
                    +" where MRTIPCON='G' AND MRCODICE="+cp_ToStrODBC(this.w_DFCODICECTR)+" AND MRPARAME<>0";
                     ,"_Curs_COLLCENT")
              else
                select count ( MRCODICE ) AS CONTA from (i_cTable);
                 where MRTIPCON="G" AND MRCODICE=this.w_DFCODICECTR AND MRPARAME<>0;
                  into cursor _Curs_COLLCENT
              endif
              if used('_Curs_COLLCENT')
                select _Curs_COLLCENT
                locate for 1=1
                do while not(eof())
                this.w_CONTA = CONTA
                  select _Curs_COLLCENT
                  continue
                enddo
                use
              endif
              if this.w_FLANAL="S" AND g_PERCCR="S" AND this.w_COSTORIC $ "CR" AND this.w_ANCCTAGG<>"E" and this.w_CONTA=0 
                if NOT this.w_MANCA_ANALITICA
                  * --- Visualizza messaggio di avvertimento che manca l analitica solo 1 volta per tutta la registrazione
                  if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="A"
                    ADDMSGNL("%1%0",THIS,ah_msgformat("Per il conto %1 non � stato possibile recuperare i conti di analitica da associare automaticamente alla registrazione di primanota oppure esistono dati di analitica associati al conto che sono privi di parametro", alltrim (this.w_DFCODICECTR)))
                  endif
                endif
                this.w_MANCA_ANALITICA = .T.
                this.w_FLAG_PROV = "S"
              endif
                select _Curs_GSDF1BMT_d_VQR
                continue
              enddo
              use
            endif
          endif
        endif
        * --- Importo originario scadenza associata al movimento bancario Pos 305 - 321
        this.w_PTTOTIMP = this.w_PNIMPAVE+this.w_PNIMPDAR
        if this.w_AZIENDA <> UPPER (this.oParentObject.w_DFTRAAZI)
          if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="A"
            ADDMSGNL("%1",THIS,ah_msgformat( "Riga %1 azienda di trascodifica %2 non valorizzata o non corrispondente",Alltrim(str(this.w_RIGA)), this.w_AZIENDA ))
          endif
          this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Azienda di trascodifica %1 non valorizzata o non corrispondente %0",this.w_AZIENDA )
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_CHIAVEMOV = this.w_CHIAVEROTTURA
        * --- Scrive Log elaborazione
        * --- Marco il movimento come errato (l utente dovr� modificare il record manualmente e rigenerare i movimenti dalla tabella)
        func_SetCCCHKVarsWrite(@w_ccchkf,this.DOCFINIM_idx,iif(w_newconn>0, w_newconn, w_nConn))
        w_Rows = SQLXInsUpdDel ( w_newconn , "update "+ w_cTable +" set "; 
 +"DFERRLOG ="+cp_NullLink(cp_ToStrODBC(this.w_ERRORLOG),"DOCFINIM","DFERRLOG"); 
 +",DFFLGMOV ="+cp_NullLink(cp_ToStrODBC( IIF (this.w_GENERA="S","S"," ")),"DOCFINIM","DFFLGMOV"); 
 +", DFSERTES ="+cp_NullLink(cp_ToStrODBC( IIF (this.w_GENERA="S",this.w_PNSERIAL," ")),"DOCFINIM","DFSERTES"); 
 +", DFCSERIA ="+cp_NullLink(cp_ToStrODBC(this.w_DFCSERIA),"DOCFINIM","DFCSERIA"); 
 +w_ccchkf ; 
 +" where "; 
 +"DFCODAZI = "+cp_ToStrODBC(this.w_AZIENDA); 
 +" and DFNUMERA = "+cp_ToStrODBC(this.w_NUMERATORE); 
 +" and DF__ANNO = "+cp_ToStrODBC(this.w_ANNO); 
 +" and DFMOVIME = "+cp_ToStrODBC(this.w_MOVIMENTO); 
 +" and DFDATREG = "+cp_ToStrODBC(this.w_PNDATREG); 
 +" and DFCONTRO = "+cp_ToStrODBC(this.w_CCCONTRTESTATA); 
 +" and DFPIANOC = "+cp_ToStrODBC(this.w_CCPIANOCTESTATA); 
 +" and DFCHIAVE = "+cp_ToStrODBC(this.w_DFCHIAVE); 
 +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGAMOV) )
        if not this.w_corretto
          i_Rows = SQLXInsUpdDel ( w_newconn ,"update "+ w_cTable +" set "; 
 +"DFERROR ="+cp_NullLink(cp_ToStrODBC("S"),"DOCFINIM","DFERROR"); 
 +",DFFLGMOV ="+cp_NullLink(cp_ToStrODBC(" "),"DOCFINIM","DFFLGMOV"); 
 +",DFSERTES ="+cp_NullLink(cp_ToStrODBC(" "),"DOCFINIM","DFSERTES"); 
 +", DFCSERIA ="+cp_NullLink(cp_ToStrODBC(this.w_DFCSERIA),"DOCFINIM","DFCSERIA"); 
 +w_ccchkf ; 
 +" where "; 
 +"DFCODAZI = "+cp_ToStrODBC(this.w_AZIENDA); 
 +" and DFNUMERA = "+cp_ToStrODBC(this.w_NUMERATORE); 
 +" and DF__ANNO = "+cp_ToStrODBC(this.w_ANNO); 
 +" and DFMOVIME = "+cp_ToStrODBC(this.w_MOVIMENTO); 
 +" and DFDATREG = "+cp_ToStrODBC(this.w_PNDATREG); 
 +" and DFCONTRO = "+cp_ToStrODBC(this.w_CCCONTRTESTATA); 
 +" and DFCHIAVE = "+cp_ToStrODBC(this.w_DFCHIAVE); 
 +" and DFPIANOC = "+cp_ToStrODBC(this.w_CCPIANOCTESTATA))
        endif
        this.w_ERRORLOG = ""
        this.w_ERRORLOG = ""
        this.w_CCNUMCOR = ""
        this.w_DOPPIO = .F.
          select _Curs_DOCFINIM
          continue
        enddo
        use
      endif
    endif
    if w_newconn>0
      sqldisconn(w_newconn)
    endif
    * --- Commit dell ultimo mivimento generato
    if this.w_Corretto AND this.w_GENERA="S"
      * --- commit
      cp_EndTrs(.t.)
      if this.w_CONTARECORD>0
        this.w_CONTAINS = (this.w_CONTAINS + 1)-this.w_Contadoppi
      endif
    else
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    * --- Aggiorna lo zoom con l eselnco dei movimenti da elaborare
    this.oparentobject.notifyevent ("Aggiorna")
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- * LE DISTINTE HANNO IL FLAG DISTINTA NEL TRACCIATO pos 292   w_DFFLGDIS='D'
    * --- Read from CAU_DIST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_DIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_DIST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CATIPDIS,CATIPSCD,CACODCAU,CACONSBF,CADATEFF"+;
        " from "+i_cTable+" CAU_DIST where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CATIPDIS,CATIPSCD,CACODCAU,CACONSBF,CADATEFF;
        from (i_cTable) where;
            CACODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CATIPDIS = NVL(cp_ToDate(_read_.CATIPDIS),cp_NullValue(_read_.CATIPDIS))
      this.w_CATIPSCD = NVL(cp_ToDate(_read_.CATIPSCD),cp_NullValue(_read_.CATIPSCD))
      this.w_CAUTESADHOC = NVL(cp_ToDate(_read_.CACODCAU),cp_NullValue(_read_.CACODCAU))
      this.w_CACONSBF = NVL(cp_ToDate(_read_.CACONSBF),cp_NullValue(_read_.CACONSBF))
      this.w_CADATEFF = NVL(cp_ToDate(_read_.CADATEFF),cp_NullValue(_read_.CADATEFF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Recupera una banca di presentazione tra quelle disponibili
    * --- Read from COC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "BACODBAN"+;
        " from "+i_cTable+" COC_MAST where ";
            +"BANSBTES = "+cp_ToStrODBC(this.w_BANCA);
            +" and BARBNTES = "+cp_ToStrODBC(this.w_RBN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        BACODBAN;
        from (i_cTable) where;
            BANSBTES = this.w_BANCA;
            and BARBNTES = this.w_RBN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CBCODBAN = NVL(cp_ToDate(_read_.BACODBAN),cp_NullValue(_read_.BACODBAN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_CBCODBAN)
      if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
        ADDMSGNL("%1",THIS,ah_msgformat("Impossibile individuare un conto per la banca %1 con RBN %2" , alltrim (this.w_BANCA) , alltrim(this.w_RBN) ) )
      endif
      this.w_Corretto = .F.
      this.w_ErrImp = .f.
      this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat("Impossibile individuare un conto per la banca %1 con RBN %2" , alltrim (this.w_BANCA) , alltrim(this.w_RBN) ) 
    endif
    this.w_PNSERIAL = space(10)
    this.w_DINUMERO = 0
    * --- devo cercare numero seriale e numero registrazione Distinta
    * --- Try
    local bErr_04EEC148
    bErr_04EEC148=bTrsErr
    this.Try_04EEC148()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
        ADDMSGNL("%1",THIS,ah_msgformat( "Errore nel riperimento del Seriale e del N� della Distinta %1 " , message() ))
      endif
      this.w_Corretto = .F.
      this.w_ErrImp = .f.
      this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Errore nel riperimento del Seriale e del N� della Distinta %1 %0 " , message() )
    endif
    bTrsErr=bTrsErr or bErr_04EEC148
    * --- End
    * --- Try
    local bErr_04EE7618
    bErr_04EE7618=bTrsErr
    this.Try_04EE7618()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
        ADDMSGNL("%1",THIS,ah_msgformat( "Impossibile inserire la distinta %1 " , message() ))
      endif
      this.w_Corretto = .F.
      this.w_ErrImp = .f.
      this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Impossibile inserire la distinta %1 %0 " , message() )
    endif
    bTrsErr=bTrsErr or bErr_04EE7618
    * --- End
  endproc
  proc Try_04EEC148()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_Connec = i_TableProp[this.DIS_TINT_IDX,3]
    cp_NextTableProg(this,i_Connec,"PRDIS","i_CODAZI,w_PNSERIAL")
    cp_NextTableProg(this,i_Connec,"PNDIS","i_CODAZI,w_PNCODESE,w_DINUMERO")
    return
  proc Try_04EE7618()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Scrivo Testata Distinta
    * --- Insert into DIS_TINT
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DIS_TINT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DINUMDIS"+",DIDATDIS"+",DI__ANNO"+",DINUMERO"+",DIDATVAL"+",DIDATCON"+",DICODESE"+",DITIPDIS"+",DITIPSCA"+",DIDESCRI"+",DICODVAL"+",DICAOVAL"+",DISCAINI"+",DISCAFIN"+",DISCOTRA"+",DIBANRIF"+",DICAURIF"+",DICODCAU"+",DICOSCOM"+",DIFLDEFI"+",DIFLAVVI"+",DICAUBON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'DIS_TINT','DINUMDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'DIS_TINT','DIDATDIS');
      +","+cp_NullLink(cp_ToStrODBC(year(this.w_PNDATREG)),'DIS_TINT','DI__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DINUMERO),'DIS_TINT','DINUMERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'DIS_TINT','DIDATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDATVAL),'DIS_TINT','DIDATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'DIS_TINT','DICODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPDIS),'DIS_TINT','DITIPDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPSCD),'DIS_TINT','DITIPSCA');
      +","+cp_NullLink(cp_ToStrODBC(Left(this.w_DESOP,35)),'DIS_TINT','DIDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'DIS_TINT','DICODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAMBIO),'DIS_TINT','DICAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(CTOD ( "01/01/1900" )),'DIS_TINT','DISCAINI');
      +","+cp_NullLink(cp_ToStrODBC(CTOD ( "01/01/3000" )),'DIS_TINT','DISCAFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'DIS_TINT','DISCOTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CBCODBAN),'DIS_TINT','DIBANRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'DIS_TINT','DICAURIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAUTESADHOC),'DIS_TINT','DICODCAU');
      +","+cp_NullLink(cp_ToStrODBC(0),'DIS_TINT','DICOSCOM');
      +","+cp_NullLink(cp_ToStrODBC("S"),'DIS_TINT','DIFLDEFI');
      +","+cp_NullLink(cp_ToStrODBC(space(1)),'DIS_TINT','DIFLAVVI');
      +","+cp_NullLink(cp_ToStrODBC("48000"),'DIS_TINT','DICAUBON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DINUMDIS',this.w_PNSERIAL,'DIDATDIS',this.w_PNDATREG,'DI__ANNO',year(this.w_PNDATREG),'DINUMERO',this.w_DINUMERO,'DIDATVAL',this.w_PNDATREG,'DIDATCON',this.w_CCDATVAL,'DICODESE',this.w_PNCODESE,'DITIPDIS',this.w_CATIPDIS,'DITIPSCA',this.w_CATIPSCD,'DIDESCRI',Left(this.w_DESOP,35),'DICODVAL',this.w_PNCODVAL,'DICAOVAL',this.w_CAMBIO)
      insert into (i_cTable) (DINUMDIS,DIDATDIS,DI__ANNO,DINUMERO,DIDATVAL,DIDATCON,DICODESE,DITIPDIS,DITIPSCA,DIDESCRI,DICODVAL,DICAOVAL,DISCAINI,DISCAFIN,DISCOTRA,DIBANRIF,DICAURIF,DICODCAU,DICOSCOM,DIFLDEFI,DIFLAVVI,DICAUBON &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PNDATREG;
           ,year(this.w_PNDATREG);
           ,this.w_DINUMERO;
           ,this.w_PNDATREG;
           ,this.w_CCDATVAL;
           ,this.w_PNCODESE;
           ,this.w_CATIPDIS;
           ,this.w_CATIPSCD;
           ,Left(this.w_DESOP,35);
           ,this.w_PNCODVAL;
           ,this.w_CAMBIO;
           ,CTOD ( "01/01/1900" );
           ,CTOD ( "01/01/3000" );
           ,0;
           ,this.w_CBCODBAN;
           ,this.w_PNCODCAU;
           ,this.w_CAUTESADHOC;
           ,0;
           ,"S";
           ,space(1);
           ,"48000";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore Scrittura Testata Distinta'
      return
    endif
    if (this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="A" ) AND this.w_GENERA="S" AND this.w_Corretto
      ADDMSGNL("%1",THIS, AH_MSGFORMAT ("Scritta Distinta. N� Reg.: %1 del: %2 . Seriale: %3" , alltrim(str(this.w_DINUMERO)) , dtoc(this.w_PNDATREG) , alltrim(this.w_PNSERIAL) ) )
    endif
    return


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_EX_SEGNO = iif( this.w_PNIMPAVE<>0 ,"A" ,"D" )
    this.w_ULT_RNUMAN = 0
    this.w_IMPTOT = iif(this.w_PNIMPAVE<>0,this.w_PNIMPAVE,this.w_PNIMPDAR)
    this.w_MRTOTIMP = 0
    this.w_RTOT = 0
    * --- Recupera il totale dei parametri
    * --- Select from COLLCENT
    i_nConn=i_TableProp[this.COLLCENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SUM ( MRPARAME ) AS TOTALE  from "+i_cTable+" COLLCENT ";
          +" where MRTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)+" AND MRCODICE="+cp_ToStrODBC(this.w_ANCODICE)+"";
           ,"_Curs_COLLCENT")
    else
      select SUM ( MRPARAME ) AS TOTALE from (i_cTable);
       where MRTIPCON=this.w_ANTIPCON AND MRCODICE=this.w_ANCODICE;
        into cursor _Curs_COLLCENT
    endif
    if used('_Curs_COLLCENT')
      select _Curs_COLLCENT
      locate for 1=1
      do while not(eof())
      this.w_TOTPAR = TOTALE
        select _Curs_COLLCENT
        continue
      enddo
      use
    endif
    * --- Select from COLLCENT
    i_nConn=i_TableProp[this.COLLCENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" COLLCENT ";
          +" where MRTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)+" AND MRCODICE="+cp_ToStrODBC(this.w_ANCODICE)+" AND MRPARAME<>0";
           ,"_Curs_COLLCENT")
    else
      select * from (i_cTable);
       where MRTIPCON=this.w_ANTIPCON AND MRCODICE=this.w_ANCODICE AND MRPARAME<>0;
        into cursor _Curs_COLLCENT
    endif
    if used('_Curs_COLLCENT')
      select _Curs_COLLCENT
      locate for 1=1
      do while not(eof())
      this.w_EXCODVOC = MRCODVOC
      this.w_EXCODCEN = MRCODCDC
      this.w_MRPARAME = MRPARAME
      * --- Ripartisce gli Importi
      if this.w_TOTPAR=0 OR this.w_MRPARAME=0 OR this.w_IMPTOT=0
        this.w_MRTOTIMP = 0
        this.oParentObject.w_IMPRIG = 0
      else
        this.w_MRTOTIMP = cp_ROUND((this.w_IMPTOT * this.w_MRPARAME) / this.w_TOTPAR, g_PERPVL)
      endif
      this.w_RTOT = this.w_RTOT + this.w_MRTOTIMP
      this.w_ULT_RNUMAN = this.w_ULT_RNUMAN + 1
      * --- Try
      local bErr_04F33550
      bErr_04F33550=bTrsErr
      this.Try_04F33550()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if this.oParentObject.w_FLTLOG="T" OR this.oParentObject.w_FLTLOG="E"
          ADDMSGNL("%1",THIS,ah_msgformat( "Impossibile inserire l' analitica per la riga di primanota %1 con Voce di costo %2 e Centro di costo %3 : 54 " , alltrim(str(this.w_ULT_RNUM)) , alltrim (this.w_EXCODVOC) , alltrim (this.w_EXCODCEN) , message() ))
        endif
        this.w_Corretto = .F.
        this.w_ErrImp = .f.
        this.w_ERRORLOG = this.w_ERRORLOG + ah_msgformat( "Impossibile inserire l' analitica per la riga di primanota %1 con Voce di costo %2 e Centro di costo %3 : 54 " , alltrim(str(this.w_ULT_RNUM)) , alltrim (this.w_EXCODVOC) , alltrim (this.w_EXCODCEN) , message() )
      endif
      bTrsErr=bTrsErr or bErr_04F33550
      * --- End
        select _Curs_COLLCENT
        continue
      enddo
      use
    endif
    * --- Assegna il resto all ultima riga
    if this.w_RTOT<> this.w_IMPTOT
      * --- Write into MOVICOST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVICOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRTOTIMP =MRTOTIMP+ "+cp_ToStrODBC(this.w_IMPTOT - this.w_RTOT );
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and MRROWORD = "+cp_ToStrODBC(this.w_ULT_RNUM);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ULT_RNUMAN);
               )
      else
        update (i_cTable) set;
            MRTOTIMP = MRTOTIMP + this.w_IMPTOT - this.w_RTOT ;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_PNSERIAL;
            and MRROWORD = this.w_ULT_RNUM;
            and CPROWNUM = this.w_ULT_RNUMAN;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc
  proc Try_04F33550()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVICOST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MRSERIAL"+",MRROWORD"+",CPROWNUM"+",MRCODVOC"+",MRCODICE"+",MR_SEGNO"+",MRTOTIMP"+",MRPARAME"+",MRINICOM"+",MRFINCOM"+",MRFLRIPA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'MOVICOST','MRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ULT_RNUM),'MOVICOST','MRROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ULT_RNUMAN),'MOVICOST','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EXCODVOC),'MOVICOST','MRCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EXCODCEN),'MOVICOST','MRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EX_SEGNO),'MOVICOST','MR_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRTOTIMP),'MOVICOST','MRTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRPARAME),'MOVICOST','MRPARAME');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("   -  -    ")),'MOVICOST','MRINICOM');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("   -  -    ")),'MOVICOST','MRFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(SPACE (1)),'MOVICOST','MRFLRIPA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_PNSERIAL,'MRROWORD',this.w_ULT_RNUM,'CPROWNUM',this.w_ULT_RNUMAN,'MRCODVOC',this.w_EXCODVOC,'MRCODICE',this.w_EXCODCEN,'MR_SEGNO',this.w_EX_SEGNO,'MRTOTIMP',this.w_MRTOTIMP,'MRPARAME',this.w_MRPARAME,'MRINICOM',cp_CharToDate("   -  -    "),'MRFINCOM',cp_CharToDate("   -  -    "),'MRFLRIPA',SPACE (1))
      insert into (i_cTable) (MRSERIAL,MRROWORD,CPROWNUM,MRCODVOC,MRCODICE,MR_SEGNO,MRTOTIMP,MRPARAME,MRINICOM,MRFINCOM,MRFLRIPA &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_ULT_RNUM;
           ,this.w_ULT_RNUMAN;
           ,this.w_EXCODVOC;
           ,this.w_EXCODCEN;
           ,this.w_EX_SEGNO;
           ,this.w_MRTOTIMP;
           ,this.w_MRPARAME;
           ,cp_CharToDate("   -  -    ");
           ,cp_CharToDate("   -  -    ");
           ,SPACE (1);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_TIPOIMPORT,w_GENERA)
    this.w_TIPOIMPORT=w_TIPOIMPORT
    this.w_GENERA=w_GENERA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,22)]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='PAG_2AME'
    this.cWorkTables[6]='PAR_TITE'
    this.cWorkTables[7]='SALDICON'
    this.cWorkTables[8]='DOCFINIM'
    this.cWorkTables[9]='DOCFINLG'
    this.cWorkTables[10]='PNT_MAST'
    this.cWorkTables[11]='MASTRI'
    this.cWorkTables[12]='PNT_DETT'
    this.cWorkTables[13]='DIS_TINT'
    this.cWorkTables[14]='CAU_DIST'
    this.cWorkTables[15]='MOVICOST'
    this.cWorkTables[16]='COLLCENT'
    this.cWorkTables[17]='DOCTRCAU'
    this.cWorkTables[18]='DOCIXCON'
    this.cWorkTables[19]='DOCIXIMP'
    this.cWorkTables[20]='CONTROPA'
    this.cWorkTables[21]='AZIENDA'
    this.cWorkTables[22]='ESERCIZI'
    return(this.OpenAllTables(22))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs__d__d__isdf_exe_query_gsdfTKMS_d_VQR')
      use in _Curs__d__d__isdf_exe_query_gsdfTKMS_d_VQR
    endif
    if used('_Curs_DOCFINIM')
      use in _Curs_DOCFINIM
    endif
    if used('_Curs_GSDF_BMT_d_VQR')
      use in _Curs_GSDF_BMT_d_VQR
    endif
    if used('_Curs_GSDF1BMT_d_VQR')
      use in _Curs_GSDF1BMT_d_VQR
    endif
    if used('_Curs_COLLCENT')
      use in _Curs_COLLCENT
    endif
    if used('_Curs_COLLCENT')
      use in _Curs_COLLCENT
    endif
    if used('_Curs_COLLCENT')
      use in _Curs_COLLCENT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsdf_bmt
  proc  StringToDate (pdata)
  return cp_CharToDate (left (pdata,2)+'-'+SUBSTR(pdata,3,2)+'-'+right (pdata,4))
  endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TIPOIMPORT,w_GENERA"
endproc
