* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdf_apd                                                        *
*              Dati generali DocFinance                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-06-04                                                      *
* Last revis.: 2017-03-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsdf_apd")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsdf_apd")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsdf_apd")
  return

* --- Class definition
define class tgsdf_apd as StdPCForm
  Width  = 784
  Height = 590+35
  Top    = 4
  Left   = 13
  cComment = "Dati generali DocFinance"
  cPrg = "gsdf_apd"
  HelpContextID=160052329
  add object cnt as tcgsdf_apd
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsdf_apd as PCContext
  w_DFCODAZI = space(5)
  w_DFTRAAZI = space(10)
  w_DFFLPROV = space(1)
  w_DFTRASCA = space(1)
  w_DFTRATIP = space(1)
  w_DFTIPEXP = space(1)
  w_DFDATUEX = space(8)
  w_DFCORRPC = space(1)
  w_DF_LINEA = space(4)
  w_DFDIS_RI = space(2)
  w_DFDIS_RA = space(2)
  w_DFESPACC = space(1)
  w_DFCORRSC = space(1)
  w_DFRATSCA = space(2)
  w_DFRATSDT = space(2)
  w_DFVOCFCL = space(6)
  w_DFVOCFFO = space(6)
  w_DFVOCFGE = space(6)
  w_DFVOCEFF = space(6)
  w_DFCASFLO = space(1)
  w_DFEXPMOV = space(1)
  w_DFNUMMOV = space(4)
  w_RADESCRI = space(50)
  w_RADESCRIS = space(50)
  w_RASCADUT = space(2)
  w_RASCADUTS = space(2)
  w_DFPERESP = space(150)
  w_DFFILPDC = space(50)
  w_DFFILSCA = space(50)
  w_DFFILMOV = space(50)
  w_DFPERIMP = space(150)
  w_DFPERBCK = space(150)
  w_DFNOMFIL = space(50)
  w_LPATH = space(10)
  w_TIPCOL = space(10)
  w_TIPSOT = space(1)
  w_DFCONTIN = space(15)
  w_OBTEST = space(8)
  w_PARTSN = space(1)
  proc Save(oFrom)
    this.w_DFCODAZI = oFrom.w_DFCODAZI
    this.w_DFTRAAZI = oFrom.w_DFTRAAZI
    this.w_DFFLPROV = oFrom.w_DFFLPROV
    this.w_DFTRASCA = oFrom.w_DFTRASCA
    this.w_DFTRATIP = oFrom.w_DFTRATIP
    this.w_DFTIPEXP = oFrom.w_DFTIPEXP
    this.w_DFDATUEX = oFrom.w_DFDATUEX
    this.w_DFCORRPC = oFrom.w_DFCORRPC
    this.w_DF_LINEA = oFrom.w_DF_LINEA
    this.w_DFDIS_RI = oFrom.w_DFDIS_RI
    this.w_DFDIS_RA = oFrom.w_DFDIS_RA
    this.w_DFESPACC = oFrom.w_DFESPACC
    this.w_DFCORRSC = oFrom.w_DFCORRSC
    this.w_DFRATSCA = oFrom.w_DFRATSCA
    this.w_DFRATSDT = oFrom.w_DFRATSDT
    this.w_DFVOCFCL = oFrom.w_DFVOCFCL
    this.w_DFVOCFFO = oFrom.w_DFVOCFFO
    this.w_DFVOCFGE = oFrom.w_DFVOCFGE
    this.w_DFVOCEFF = oFrom.w_DFVOCEFF
    this.w_DFCASFLO = oFrom.w_DFCASFLO
    this.w_DFEXPMOV = oFrom.w_DFEXPMOV
    this.w_DFNUMMOV = oFrom.w_DFNUMMOV
    this.w_RADESCRI = oFrom.w_RADESCRI
    this.w_RADESCRIS = oFrom.w_RADESCRIS
    this.w_RASCADUT = oFrom.w_RASCADUT
    this.w_RASCADUTS = oFrom.w_RASCADUTS
    this.w_DFPERESP = oFrom.w_DFPERESP
    this.w_DFFILPDC = oFrom.w_DFFILPDC
    this.w_DFFILSCA = oFrom.w_DFFILSCA
    this.w_DFFILMOV = oFrom.w_DFFILMOV
    this.w_DFPERIMP = oFrom.w_DFPERIMP
    this.w_DFPERBCK = oFrom.w_DFPERBCK
    this.w_DFNOMFIL = oFrom.w_DFNOMFIL
    this.w_LPATH = oFrom.w_LPATH
    this.w_TIPCOL = oFrom.w_TIPCOL
    this.w_TIPSOT = oFrom.w_TIPSOT
    this.w_DFCONTIN = oFrom.w_DFCONTIN
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_PARTSN = oFrom.w_PARTSN
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_DFCODAZI = this.w_DFCODAZI
    oTo.w_DFTRAAZI = this.w_DFTRAAZI
    oTo.w_DFFLPROV = this.w_DFFLPROV
    oTo.w_DFTRASCA = this.w_DFTRASCA
    oTo.w_DFTRATIP = this.w_DFTRATIP
    oTo.w_DFTIPEXP = this.w_DFTIPEXP
    oTo.w_DFDATUEX = this.w_DFDATUEX
    oTo.w_DFCORRPC = this.w_DFCORRPC
    oTo.w_DF_LINEA = this.w_DF_LINEA
    oTo.w_DFDIS_RI = this.w_DFDIS_RI
    oTo.w_DFDIS_RA = this.w_DFDIS_RA
    oTo.w_DFESPACC = this.w_DFESPACC
    oTo.w_DFCORRSC = this.w_DFCORRSC
    oTo.w_DFRATSCA = this.w_DFRATSCA
    oTo.w_DFRATSDT = this.w_DFRATSDT
    oTo.w_DFVOCFCL = this.w_DFVOCFCL
    oTo.w_DFVOCFFO = this.w_DFVOCFFO
    oTo.w_DFVOCFGE = this.w_DFVOCFGE
    oTo.w_DFVOCEFF = this.w_DFVOCEFF
    oTo.w_DFCASFLO = this.w_DFCASFLO
    oTo.w_DFEXPMOV = this.w_DFEXPMOV
    oTo.w_DFNUMMOV = this.w_DFNUMMOV
    oTo.w_RADESCRI = this.w_RADESCRI
    oTo.w_RADESCRIS = this.w_RADESCRIS
    oTo.w_RASCADUT = this.w_RASCADUT
    oTo.w_RASCADUTS = this.w_RASCADUTS
    oTo.w_DFPERESP = this.w_DFPERESP
    oTo.w_DFFILPDC = this.w_DFFILPDC
    oTo.w_DFFILSCA = this.w_DFFILSCA
    oTo.w_DFFILMOV = this.w_DFFILMOV
    oTo.w_DFPERIMP = this.w_DFPERIMP
    oTo.w_DFPERBCK = this.w_DFPERBCK
    oTo.w_DFNOMFIL = this.w_DFNOMFIL
    oTo.w_LPATH = this.w_LPATH
    oTo.w_TIPCOL = this.w_TIPCOL
    oTo.w_TIPSOT = this.w_TIPSOT
    oTo.w_DFCONTIN = this.w_DFCONTIN
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_PARTSN = this.w_PARTSN
    PCContext::Load(oTo)
enddefine

define class tcgsdf_apd as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 784
  Height = 590+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-03-29"
  HelpContextID=160052329
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  DOCFINPA_IDX = 0
  VOC_FINZ_IDX = 0
  DORATING_IDX = 0
  CCC_MAST_IDX = 0
  COC_MAST_IDX = 0
  CONTI_IDX = 0
  cFile = "DOCFINPA"
  cKeySelect = "DFCODAZI"
  cKeyWhere  = "DFCODAZI=this.w_DFCODAZI"
  cKeyWhereODBC = '"DFCODAZI="+cp_ToStrODBC(this.w_DFCODAZI)';

  cKeyWhereODBCqualified = '"DOCFINPA.DFCODAZI="+cp_ToStrODBC(this.w_DFCODAZI)';

  cPrg = "gsdf_apd"
  cComment = "Dati generali DocFinance"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFCODAZI = space(5)
  w_DFTRAAZI = space(10)
  w_DFFLPROV = space(1)
  w_DFTRASCA = space(1)
  w_DFTRATIP = space(1)
  w_DFTIPEXP = space(1)
  w_DFDATUEX = ctod('  /  /  ')
  w_DFCORRPC = space(1)
  w_DF_LINEA = space(4)
  w_DFDIS_RI = space(2)
  w_DFDIS_RA = space(2)
  w_DFESPACC = space(1)
  w_DFCORRSC = space(1)
  w_DFRATSCA = space(2)
  w_DFRATSDT = space(2)
  w_DFVOCFCL = space(6)
  w_DFVOCFFO = space(6)
  w_DFVOCFGE = space(6)
  w_DFVOCEFF = space(6)
  w_DFCASFLO = space(1)
  w_DFEXPMOV = space(1)
  o_DFEXPMOV = space(1)
  w_DFNUMMOV = space(4)
  w_RADESCRI = space(50)
  w_RADESCRIS = space(50)
  w_RASCADUT = space(2)
  w_RASCADUTS = space(2)
  w_DFPERESP = space(150)
  o_DFPERESP = space(150)
  w_DFFILPDC = space(50)
  w_DFFILSCA = space(50)
  w_DFFILMOV = space(50)
  w_DFPERIMP = space(150)
  o_DFPERIMP = space(150)
  w_DFPERBCK = space(150)
  o_DFPERBCK = space(150)
  w_DFNOMFIL = space(50)
  w_LPATH = space(10)
  w_TIPCOL = space(10)
  w_TIPSOT = space(1)
  w_DFCONTIN = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_PARTSN = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdf_apdPag1","gsdf_apd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati generali")
      .Pages(1).HelpContextID = 115931636
      .Pages(2).addobject("oPag","tgsdf_apdPag2","gsdf_apd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Path e file")
      .Pages(2).HelpContextID = 233583852
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDFTRAAZI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VOC_FINZ'
    this.cWorkTables[2]='DORATING'
    this.cWorkTables[3]='CCC_MAST'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='DOCFINPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOCFINPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOCFINPA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsdf_apd'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DOCFINPA where DFCODAZI=KeySet.DFCODAZI
    *
    i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOCFINPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOCFINPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOCFINPA '
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFCODAZI',this.w_DFCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RADESCRI = space(50)
        .w_RADESCRIS = space(50)
        .w_RASCADUT = space(2)
        .w_RASCADUTS = space(2)
        .w_LPATH = space(10)
        .w_TIPCOL = 'G'
        .w_TIPSOT = space(1)
        .w_OBTEST = i_DATSYS
        .w_PARTSN = space(1)
        .w_DFCODAZI = NVL(DFCODAZI,space(5))
        .w_DFTRAAZI = NVL(DFTRAAZI,space(10))
        .w_DFFLPROV = NVL(DFFLPROV,space(1))
        .w_DFTRASCA = NVL(DFTRASCA,space(1))
        .w_DFTRATIP = NVL(DFTRATIP,space(1))
        .w_DFTIPEXP = NVL(DFTIPEXP,space(1))
        .w_DFDATUEX = NVL(cp_ToDate(DFDATUEX),ctod("  /  /  "))
        .w_DFCORRPC = NVL(DFCORRPC,space(1))
        .w_DF_LINEA = NVL(DF_LINEA,space(4))
        .w_DFDIS_RI = NVL(DFDIS_RI,space(2))
        .w_DFDIS_RA = NVL(DFDIS_RA,space(2))
        .w_DFESPACC = NVL(DFESPACC,space(1))
        .w_DFCORRSC = NVL(DFCORRSC,space(1))
        .w_DFRATSCA = NVL(DFRATSCA,space(2))
          if link_1_15_joined
            this.w_DFRATSCA = NVL(RACODICE115,NVL(this.w_DFRATSCA,space(2)))
            this.w_RADESCRI = NVL(RADESCRI115,space(50))
            this.w_RASCADUT = NVL(RASCADUT115,space(2))
          else
          .link_1_15('Load')
          endif
        .w_DFRATSDT = NVL(DFRATSDT,space(2))
          if link_1_16_joined
            this.w_DFRATSDT = NVL(RACODICE116,NVL(this.w_DFRATSDT,space(2)))
            this.w_RADESCRIS = NVL(RADESCRI116,space(50))
            this.w_RASCADUTS = NVL(RASCADUT116,space(2))
          else
          .link_1_16('Load')
          endif
        .w_DFVOCFCL = NVL(DFVOCFCL,space(6))
          * evitabile
          *.link_1_17('Load')
        .w_DFVOCFFO = NVL(DFVOCFFO,space(6))
          * evitabile
          *.link_1_18('Load')
        .w_DFVOCFGE = NVL(DFVOCFGE,space(6))
          * evitabile
          *.link_1_19('Load')
        .w_DFVOCEFF = NVL(DFVOCEFF,space(6))
          * evitabile
          *.link_1_20('Load')
        .w_DFCASFLO = NVL(DFCASFLO,space(1))
        .w_DFEXPMOV = NVL(DFEXPMOV,space(1))
        .w_DFNUMMOV = NVL(DFNUMMOV,space(4))
        .w_DFPERESP = NVL(DFPERESP,space(150))
        .w_DFFILPDC = NVL(DFFILPDC,space(50))
        .w_DFFILSCA = NVL(DFFILSCA,space(50))
        .w_DFFILMOV = NVL(DFFILMOV,space(50))
        .w_DFPERIMP = NVL(DFPERIMP,space(150))
        .w_DFPERBCK = NVL(DFPERBCK,space(150))
        .w_DFNOMFIL = NVL(DFNOMFIL,space(50))
        .w_DFCONTIN = NVL(DFCONTIN,space(15))
          .link_1_52('Load')
        cp_LoadRecExtFlds(this,'DOCFINPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_12.enabled = this.oPgFrm.Page2.oPag.oBtn_2_12.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_15.enabled = this.oPgFrm.Page2.oPag.oBtn_2_15.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_DFCODAZI = space(5)
      .w_DFTRAAZI = space(10)
      .w_DFFLPROV = space(1)
      .w_DFTRASCA = space(1)
      .w_DFTRATIP = space(1)
      .w_DFTIPEXP = space(1)
      .w_DFDATUEX = ctod("  /  /  ")
      .w_DFCORRPC = space(1)
      .w_DF_LINEA = space(4)
      .w_DFDIS_RI = space(2)
      .w_DFDIS_RA = space(2)
      .w_DFESPACC = space(1)
      .w_DFCORRSC = space(1)
      .w_DFRATSCA = space(2)
      .w_DFRATSDT = space(2)
      .w_DFVOCFCL = space(6)
      .w_DFVOCFFO = space(6)
      .w_DFVOCFGE = space(6)
      .w_DFVOCEFF = space(6)
      .w_DFCASFLO = space(1)
      .w_DFEXPMOV = space(1)
      .w_DFNUMMOV = space(4)
      .w_RADESCRI = space(50)
      .w_RADESCRIS = space(50)
      .w_RASCADUT = space(2)
      .w_RASCADUTS = space(2)
      .w_DFPERESP = space(150)
      .w_DFFILPDC = space(50)
      .w_DFFILSCA = space(50)
      .w_DFFILMOV = space(50)
      .w_DFPERIMP = space(150)
      .w_DFPERBCK = space(150)
      .w_DFNOMFIL = space(50)
      .w_LPATH = space(10)
      .w_TIPCOL = space(10)
      .w_TIPSOT = space(1)
      .w_DFCONTIN = space(15)
      .w_OBTEST = ctod("  /  /  ")
      .w_PARTSN = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DFTRAAZI = i_Codazi
        .w_DFFLPROV = 'N'
        .w_DFTRASCA = 'N'
        .w_DFTRATIP = 'N'
        .w_DFTIPEXP = 'T'
          .DoRTCalc(7,7,.f.)
        .w_DFCORRPC = 'N'
        .w_DF_LINEA = '01'
          .DoRTCalc(10,10,.f.)
        .w_DFDIS_RA = 'RB'
        .w_DFESPACC = 'N'
        .w_DFCORRSC = 'N'
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_DFRATSCA))
          .link_1_15('Full')
          endif
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_DFRATSDT))
          .link_1_16('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_DFVOCFCL))
          .link_1_17('Full')
          endif
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_DFVOCFFO))
          .link_1_18('Full')
          endif
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_DFVOCFGE))
          .link_1_19('Full')
          endif
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_DFVOCEFF))
          .link_1_20('Full')
          endif
        .w_DFCASFLO = 'S'
        .w_DFEXPMOV = 'N'
        .w_DFNUMMOV = Space(4)
          .DoRTCalc(23,34,.f.)
        .w_TIPCOL = 'G'
        .DoRTCalc(36,37,.f.)
          if not(empty(.w_DFCONTIN))
          .link_1_52('Full')
          endif
        .w_OBTEST = i_DATSYS
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOCFINPA')
    this.DoRTCalc(39,39,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_12.enabled = this.oPgFrm.Page2.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_15.enabled = this.oPgFrm.Page2.oPag.oBtn_2_15.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsdf_apd
    this.bUpdated=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDFTRAAZI_1_2.enabled = i_bVal
      .Page1.oPag.oDFFLPROV_1_3.enabled = i_bVal
      .Page1.oPag.oDFTRASCA_1_4.enabled = i_bVal
      .Page1.oPag.oDFTRATIP_1_5.enabled = i_bVal
      .Page1.oPag.oDFTIPEXP_1_6.enabled = i_bVal
      .Page1.oPag.oDFDATUEX_1_7.enabled = i_bVal
      .Page1.oPag.oDFCORRPC_1_8.enabled = i_bVal
      .Page1.oPag.oDF_LINEA_1_9.enabled = i_bVal
      .Page1.oPag.oDFDIS_RI_1_11.enabled = i_bVal
      .Page1.oPag.oDFDIS_RA_1_12.enabled = i_bVal
      .Page1.oPag.oDFESPACC_1_13.enabled = i_bVal
      .Page1.oPag.oDFCORRSC_1_14.enabled = i_bVal
      .Page1.oPag.oDFRATSCA_1_15.enabled = i_bVal
      .Page1.oPag.oDFRATSDT_1_16.enabled = i_bVal
      .Page1.oPag.oDFVOCFCL_1_17.enabled = i_bVal
      .Page1.oPag.oDFVOCFFO_1_18.enabled = i_bVal
      .Page1.oPag.oDFVOCFGE_1_19.enabled = i_bVal
      .Page1.oPag.oDFVOCEFF_1_20.enabled = i_bVal
      .Page1.oPag.oDFCASFLO_1_21.enabled = i_bVal
      .Page1.oPag.oDFEXPMOV_1_22.enabled = i_bVal
      .Page1.oPag.oDFNUMMOV_1_23.enabled = i_bVal
      .Page2.oPag.oDFPERESP_2_1.enabled = i_bVal
      .Page2.oPag.oDFFILPDC_2_4.enabled = i_bVal
      .Page2.oPag.oDFFILSCA_2_7.enabled = i_bVal
      .Page2.oPag.oDFFILMOV_2_8.enabled = i_bVal
      .Page2.oPag.oDFPERIMP_2_10.enabled = i_bVal
      .Page2.oPag.oDFPERBCK_2_13.enabled = i_bVal
      .Page2.oPag.oDFNOMFIL_2_20.enabled = i_bVal
      .Page1.oPag.oDFCONTIN_1_52.enabled = i_bVal
      .Page2.oPag.oBtn_2_3.enabled = .Page2.oPag.oBtn_2_3.mCond()
      .Page2.oPag.oBtn_2_12.enabled = .Page2.oPag.oBtn_2_12.mCond()
      .Page2.oPag.oBtn_2_15.enabled = .Page2.oPag.oBtn_2_15.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'DOCFINPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODAZI,"DFCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFTRAAZI,"DFTRAAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFFLPROV,"DFFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFTRASCA,"DFTRASCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFTRATIP,"DFTRATIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFTIPEXP,"DFTIPEXP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFDATUEX,"DFDATUEX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCORRPC,"DFCORRPC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DF_LINEA,"DF_LINEA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFDIS_RI,"DFDIS_RI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFDIS_RA,"DFDIS_RA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFESPACC,"DFESPACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCORRSC,"DFCORRSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFRATSCA,"DFRATSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFRATSDT,"DFRATSDT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFVOCFCL,"DFVOCFCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFVOCFFO,"DFVOCFFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFVOCFGE,"DFVOCFGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFVOCEFF,"DFVOCEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCASFLO,"DFCASFLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFEXPMOV,"DFEXPMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFNUMMOV,"DFNUMMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFPERESP,"DFPERESP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFFILPDC,"DFFILPDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFFILSCA,"DFFILSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFFILMOV,"DFFILMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFPERIMP,"DFPERIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFPERBCK,"DFPERBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFNOMFIL,"DFNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCONTIN,"DFCONTIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DOCFINPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DOCFINPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOCFINPA')
        i_extval=cp_InsertValODBCExtFlds(this,'DOCFINPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DFCODAZI,DFTRAAZI,DFFLPROV,DFTRASCA,DFTRATIP"+;
                  ",DFTIPEXP,DFDATUEX,DFCORRPC,DF_LINEA,DFDIS_RI"+;
                  ",DFDIS_RA,DFESPACC,DFCORRSC,DFRATSCA,DFRATSDT"+;
                  ",DFVOCFCL,DFVOCFFO,DFVOCFGE,DFVOCEFF,DFCASFLO"+;
                  ",DFEXPMOV,DFNUMMOV,DFPERESP,DFFILPDC,DFFILSCA"+;
                  ",DFFILMOV,DFPERIMP,DFPERBCK,DFNOMFIL,DFCONTIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DFCODAZI)+;
                  ","+cp_ToStrODBC(this.w_DFTRAAZI)+;
                  ","+cp_ToStrODBC(this.w_DFFLPROV)+;
                  ","+cp_ToStrODBC(this.w_DFTRASCA)+;
                  ","+cp_ToStrODBC(this.w_DFTRATIP)+;
                  ","+cp_ToStrODBC(this.w_DFTIPEXP)+;
                  ","+cp_ToStrODBC(this.w_DFDATUEX)+;
                  ","+cp_ToStrODBC(this.w_DFCORRPC)+;
                  ","+cp_ToStrODBC(this.w_DF_LINEA)+;
                  ","+cp_ToStrODBC(this.w_DFDIS_RI)+;
                  ","+cp_ToStrODBC(this.w_DFDIS_RA)+;
                  ","+cp_ToStrODBC(this.w_DFESPACC)+;
                  ","+cp_ToStrODBC(this.w_DFCORRSC)+;
                  ","+cp_ToStrODBCNull(this.w_DFRATSCA)+;
                  ","+cp_ToStrODBCNull(this.w_DFRATSDT)+;
                  ","+cp_ToStrODBCNull(this.w_DFVOCFCL)+;
                  ","+cp_ToStrODBCNull(this.w_DFVOCFFO)+;
                  ","+cp_ToStrODBCNull(this.w_DFVOCFGE)+;
                  ","+cp_ToStrODBCNull(this.w_DFVOCEFF)+;
                  ","+cp_ToStrODBC(this.w_DFCASFLO)+;
                  ","+cp_ToStrODBC(this.w_DFEXPMOV)+;
                  ","+cp_ToStrODBC(this.w_DFNUMMOV)+;
                  ","+cp_ToStrODBC(this.w_DFPERESP)+;
                  ","+cp_ToStrODBC(this.w_DFFILPDC)+;
                  ","+cp_ToStrODBC(this.w_DFFILSCA)+;
                  ","+cp_ToStrODBC(this.w_DFFILMOV)+;
                  ","+cp_ToStrODBC(this.w_DFPERIMP)+;
                  ","+cp_ToStrODBC(this.w_DFPERBCK)+;
                  ","+cp_ToStrODBC(this.w_DFNOMFIL)+;
                  ","+cp_ToStrODBCNull(this.w_DFCONTIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOCFINPA')
        i_extval=cp_InsertValVFPExtFlds(this,'DOCFINPA')
        cp_CheckDeletedKey(i_cTable,0,'DFCODAZI',this.w_DFCODAZI)
        INSERT INTO (i_cTable);
              (DFCODAZI,DFTRAAZI,DFFLPROV,DFTRASCA,DFTRATIP,DFTIPEXP,DFDATUEX,DFCORRPC,DF_LINEA,DFDIS_RI,DFDIS_RA,DFESPACC,DFCORRSC,DFRATSCA,DFRATSDT,DFVOCFCL,DFVOCFFO,DFVOCFGE,DFVOCEFF,DFCASFLO,DFEXPMOV,DFNUMMOV,DFPERESP,DFFILPDC,DFFILSCA,DFFILMOV,DFPERIMP,DFPERBCK,DFNOMFIL,DFCONTIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DFCODAZI;
                  ,this.w_DFTRAAZI;
                  ,this.w_DFFLPROV;
                  ,this.w_DFTRASCA;
                  ,this.w_DFTRATIP;
                  ,this.w_DFTIPEXP;
                  ,this.w_DFDATUEX;
                  ,this.w_DFCORRPC;
                  ,this.w_DF_LINEA;
                  ,this.w_DFDIS_RI;
                  ,this.w_DFDIS_RA;
                  ,this.w_DFESPACC;
                  ,this.w_DFCORRSC;
                  ,this.w_DFRATSCA;
                  ,this.w_DFRATSDT;
                  ,this.w_DFVOCFCL;
                  ,this.w_DFVOCFFO;
                  ,this.w_DFVOCFGE;
                  ,this.w_DFVOCEFF;
                  ,this.w_DFCASFLO;
                  ,this.w_DFEXPMOV;
                  ,this.w_DFNUMMOV;
                  ,this.w_DFPERESP;
                  ,this.w_DFFILPDC;
                  ,this.w_DFFILSCA;
                  ,this.w_DFFILMOV;
                  ,this.w_DFPERIMP;
                  ,this.w_DFPERBCK;
                  ,this.w_DFNOMFIL;
                  ,this.w_DFCONTIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DOCFINPA_IDX,i_nConn)
      *
      * update DOCFINPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DOCFINPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DFTRAAZI="+cp_ToStrODBC(this.w_DFTRAAZI)+;
             ",DFFLPROV="+cp_ToStrODBC(this.w_DFFLPROV)+;
             ",DFTRASCA="+cp_ToStrODBC(this.w_DFTRASCA)+;
             ",DFTRATIP="+cp_ToStrODBC(this.w_DFTRATIP)+;
             ",DFTIPEXP="+cp_ToStrODBC(this.w_DFTIPEXP)+;
             ",DFDATUEX="+cp_ToStrODBC(this.w_DFDATUEX)+;
             ",DFCORRPC="+cp_ToStrODBC(this.w_DFCORRPC)+;
             ",DF_LINEA="+cp_ToStrODBC(this.w_DF_LINEA)+;
             ",DFDIS_RI="+cp_ToStrODBC(this.w_DFDIS_RI)+;
             ",DFDIS_RA="+cp_ToStrODBC(this.w_DFDIS_RA)+;
             ",DFESPACC="+cp_ToStrODBC(this.w_DFESPACC)+;
             ",DFCORRSC="+cp_ToStrODBC(this.w_DFCORRSC)+;
             ",DFRATSCA="+cp_ToStrODBCNull(this.w_DFRATSCA)+;
             ",DFRATSDT="+cp_ToStrODBCNull(this.w_DFRATSDT)+;
             ",DFVOCFCL="+cp_ToStrODBCNull(this.w_DFVOCFCL)+;
             ",DFVOCFFO="+cp_ToStrODBCNull(this.w_DFVOCFFO)+;
             ",DFVOCFGE="+cp_ToStrODBCNull(this.w_DFVOCFGE)+;
             ",DFVOCEFF="+cp_ToStrODBCNull(this.w_DFVOCEFF)+;
             ",DFCASFLO="+cp_ToStrODBC(this.w_DFCASFLO)+;
             ",DFEXPMOV="+cp_ToStrODBC(this.w_DFEXPMOV)+;
             ",DFNUMMOV="+cp_ToStrODBC(this.w_DFNUMMOV)+;
             ",DFPERESP="+cp_ToStrODBC(this.w_DFPERESP)+;
             ",DFFILPDC="+cp_ToStrODBC(this.w_DFFILPDC)+;
             ",DFFILSCA="+cp_ToStrODBC(this.w_DFFILSCA)+;
             ",DFFILMOV="+cp_ToStrODBC(this.w_DFFILMOV)+;
             ",DFPERIMP="+cp_ToStrODBC(this.w_DFPERIMP)+;
             ",DFPERBCK="+cp_ToStrODBC(this.w_DFPERBCK)+;
             ",DFNOMFIL="+cp_ToStrODBC(this.w_DFNOMFIL)+;
             ",DFCONTIN="+cp_ToStrODBCNull(this.w_DFCONTIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DOCFINPA')
        i_cWhere = cp_PKFox(i_cTable  ,'DFCODAZI',this.w_DFCODAZI  )
        UPDATE (i_cTable) SET;
              DFTRAAZI=this.w_DFTRAAZI;
             ,DFFLPROV=this.w_DFFLPROV;
             ,DFTRASCA=this.w_DFTRASCA;
             ,DFTRATIP=this.w_DFTRATIP;
             ,DFTIPEXP=this.w_DFTIPEXP;
             ,DFDATUEX=this.w_DFDATUEX;
             ,DFCORRPC=this.w_DFCORRPC;
             ,DF_LINEA=this.w_DF_LINEA;
             ,DFDIS_RI=this.w_DFDIS_RI;
             ,DFDIS_RA=this.w_DFDIS_RA;
             ,DFESPACC=this.w_DFESPACC;
             ,DFCORRSC=this.w_DFCORRSC;
             ,DFRATSCA=this.w_DFRATSCA;
             ,DFRATSDT=this.w_DFRATSDT;
             ,DFVOCFCL=this.w_DFVOCFCL;
             ,DFVOCFFO=this.w_DFVOCFFO;
             ,DFVOCFGE=this.w_DFVOCFGE;
             ,DFVOCEFF=this.w_DFVOCEFF;
             ,DFCASFLO=this.w_DFCASFLO;
             ,DFEXPMOV=this.w_DFEXPMOV;
             ,DFNUMMOV=this.w_DFNUMMOV;
             ,DFPERESP=this.w_DFPERESP;
             ,DFFILPDC=this.w_DFFILPDC;
             ,DFFILSCA=this.w_DFFILSCA;
             ,DFFILMOV=this.w_DFFILMOV;
             ,DFPERIMP=this.w_DFPERIMP;
             ,DFPERBCK=this.w_DFPERBCK;
             ,DFNOMFIL=this.w_DFNOMFIL;
             ,DFCONTIN=this.w_DFCONTIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DOCFINPA_IDX,i_nConn)
      *
      * delete DOCFINPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DFCODAZI',this.w_DFCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOCFINPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOCFINPA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,21,.t.)
        if .o_Dfexpmov<>.w_Dfexpmov
            .w_DFNUMMOV = Space(4)
        endif
        if .o_DFPERIMP<>.w_DFPERIMP
          .Calculate_NFRPPENVLD()
        endif
        if .o_DFPERESP<>.w_DFPERESP
          .Calculate_RBPNSTOYFG()
        endif
        if .o_DFPERBCK<>.w_DFPERBCK
          .Calculate_KNTHIXYXYU()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,39,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_NFRPPENVLD()
    with this
          * --- Gestione PATH
          .w_DFPERIMP = IIF(right(alltrim(.w_DFPERIMP),1)='\' or empty(.w_DFPERIMP),.w_DFPERIMP,alltrim(.w_DFPERIMP)+iif(len(alltrim(.w_DFPERIMP))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_DFPERIMP,'F')
    endwith
  endproc
  proc Calculate_RBPNSTOYFG()
    with this
          * --- Gestione PATH
          .w_DFPERESP = IIF(right(alltrim(.w_DFPERESP),1)='\' or empty(.w_DFPERESP),.w_DFPERESP,alltrim(.w_DFPERESP)+iif(len(alltrim(.w_DFPERESP))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_DFPERESP,'F')
    endwith
  endproc
  proc Calculate_KNTHIXYXYU()
    with this
          * --- Gestione PATH
          .w_DFPERBCK = IIF(right(alltrim(.w_DFPERBCK),1)='\' or empty(.w_DFPERBCK),.w_DFPERBCK,alltrim(.w_DFPERBCK)+iif(len(alltrim(.w_DFPERBCK))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_DFPERBCK,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDFDATUEX_1_7.enabled = this.oPgFrm.Page1.oPag.oDFDATUEX_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDFNUMMOV_1_23.visible=!this.oPgFrm.Page1.oPag.oDFNUMMOV_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DFRATSCA
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_lTable = "DORATING"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2], .t., this.DORATING_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFRATSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsdf_Ara',True,'DORATING')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RACODICE like "+cp_ToStrODBC(trim(this.w_DFRATSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI,RASCADUT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RACODICE',trim(this.w_DFRATSCA))
          select RACODICE,RADESCRI,RASCADUT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFRATSCA)==trim(_Link_.RACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFRATSCA) and !this.bDontReportError
            deferred_cp_zoom('DORATING','*','RACODICE',cp_AbsName(oSource.parent,'oDFRATSCA_1_15'),i_cWhere,'Gsdf_Ara',"Rating",'Gsdf_Ara.DORATING_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI,RASCADUT";
                     +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',oSource.xKey(1))
            select RACODICE,RADESCRI,RASCADUT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFRATSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI,RASCADUT";
                   +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(this.w_DFRATSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',this.w_DFRATSCA)
            select RACODICE,RADESCRI,RASCADUT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFRATSCA = NVL(_Link_.RACODICE,space(2))
      this.w_RADESCRI = NVL(_Link_.RADESCRI,space(50))
      this.w_RASCADUT = NVL(_Link_.RASCADUT,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_DFRATSCA = space(2)
      endif
      this.w_RADESCRI = space(50)
      this.w_RASCADUT = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty(.w_Rascadut)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Default rating non valido")
        endif
        this.w_DFRATSCA = space(2)
        this.w_RADESCRI = space(50)
        this.w_RASCADUT = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])+'\'+cp_ToStr(_Link_.RACODICE,1)
      cp_ShowWarn(i_cKey,this.DORATING_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFRATSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DORATING_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.RACODICE as RACODICE115"+ ",link_1_15.RADESCRI as RADESCRI115"+ ",link_1_15.RASCADUT as RASCADUT115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on DOCFINPA.DFRATSCA=link_1_15.RACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and DOCFINPA.DFRATSCA=link_1_15.RACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DFRATSDT
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_lTable = "DORATING"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2], .t., this.DORATING_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFRATSDT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsdf_Ara',True,'DORATING')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RACODICE like "+cp_ToStrODBC(trim(this.w_DFRATSDT)+"%");

          i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI,RASCADUT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RACODICE',trim(this.w_DFRATSDT))
          select RACODICE,RADESCRI,RASCADUT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFRATSDT)==trim(_Link_.RACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFRATSDT) and !this.bDontReportError
            deferred_cp_zoom('DORATING','*','RACODICE',cp_AbsName(oSource.parent,'oDFRATSDT_1_16'),i_cWhere,'Gsdf_Ara',"Rating",'Gsdf_Ara.DORATING_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI,RASCADUT";
                     +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',oSource.xKey(1))
            select RACODICE,RADESCRI,RASCADUT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFRATSDT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI,RASCADUT";
                   +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(this.w_DFRATSDT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',this.w_DFRATSDT)
            select RACODICE,RADESCRI,RASCADUT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFRATSDT = NVL(_Link_.RACODICE,space(2))
      this.w_RADESCRIS = NVL(_Link_.RADESCRI,space(50))
      this.w_RASCADUTS = NVL(_Link_.RASCADUT,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_DFRATSDT = space(2)
      endif
      this.w_RADESCRIS = space(50)
      this.w_RASCADUTS = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty(.w_Rascaduts)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Default rating non valido")
        endif
        this.w_DFRATSDT = space(2)
        this.w_RADESCRIS = space(50)
        this.w_RASCADUTS = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])+'\'+cp_ToStr(_Link_.RACODICE,1)
      cp_ShowWarn(i_cKey,this.DORATING_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFRATSDT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DORATING_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.RACODICE as RACODICE116"+ ",link_1_16.RADESCRI as RADESCRI116"+ ",link_1_16.RASCADUT as RASCADUT116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on DOCFINPA.DFRATSDT=link_1_16.RACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and DOCFINPA.DFRATSDT=link_1_16.RACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DFVOCFCL
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_FINZ_IDX,3]
    i_lTable = "VOC_FINZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2], .t., this.VOC_FINZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFVOCFCL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDF_AVF',True,'VOC_FINZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DFVOCFIN like "+cp_ToStrODBC(trim(this.w_DFVOCFCL)+"%");

          i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DFVOCFIN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DFVOCFIN',trim(this.w_DFVOCFCL))
          select DFVOCFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DFVOCFIN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFVOCFCL)==trim(_Link_.DFVOCFIN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFVOCFCL) and !this.bDontReportError
            deferred_cp_zoom('VOC_FINZ','*','DFVOCFIN',cp_AbsName(oSource.parent,'oDFVOCFCL_1_17'),i_cWhere,'GSDF_AVF',"Voci finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                     +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',oSource.xKey(1))
            select DFVOCFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFVOCFCL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                   +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(this.w_DFVOCFCL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',this.w_DFVOCFCL)
            select DFVOCFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFVOCFCL = NVL(_Link_.DFVOCFIN,space(6))
    else
      if i_cCtrl<>'Load'
        this.w_DFVOCFCL = space(6)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])+'\'+cp_ToStr(_Link_.DFVOCFIN,1)
      cp_ShowWarn(i_cKey,this.VOC_FINZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFVOCFCL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DFVOCFFO
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_FINZ_IDX,3]
    i_lTable = "VOC_FINZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2], .t., this.VOC_FINZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFVOCFFO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDF_AVF',True,'VOC_FINZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DFVOCFIN like "+cp_ToStrODBC(trim(this.w_DFVOCFFO)+"%");

          i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DFVOCFIN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DFVOCFIN',trim(this.w_DFVOCFFO))
          select DFVOCFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DFVOCFIN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFVOCFFO)==trim(_Link_.DFVOCFIN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFVOCFFO) and !this.bDontReportError
            deferred_cp_zoom('VOC_FINZ','*','DFVOCFIN',cp_AbsName(oSource.parent,'oDFVOCFFO_1_18'),i_cWhere,'GSDF_AVF',"Voci finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                     +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',oSource.xKey(1))
            select DFVOCFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFVOCFFO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                   +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(this.w_DFVOCFFO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',this.w_DFVOCFFO)
            select DFVOCFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFVOCFFO = NVL(_Link_.DFVOCFIN,space(6))
    else
      if i_cCtrl<>'Load'
        this.w_DFVOCFFO = space(6)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])+'\'+cp_ToStr(_Link_.DFVOCFIN,1)
      cp_ShowWarn(i_cKey,this.VOC_FINZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFVOCFFO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DFVOCFGE
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_FINZ_IDX,3]
    i_lTable = "VOC_FINZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2], .t., this.VOC_FINZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFVOCFGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDF_AVF',True,'VOC_FINZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DFVOCFIN like "+cp_ToStrODBC(trim(this.w_DFVOCFGE)+"%");

          i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DFVOCFIN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DFVOCFIN',trim(this.w_DFVOCFGE))
          select DFVOCFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DFVOCFIN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFVOCFGE)==trim(_Link_.DFVOCFIN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFVOCFGE) and !this.bDontReportError
            deferred_cp_zoom('VOC_FINZ','*','DFVOCFIN',cp_AbsName(oSource.parent,'oDFVOCFGE_1_19'),i_cWhere,'GSDF_AVF',"Voci finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                     +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',oSource.xKey(1))
            select DFVOCFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFVOCFGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                   +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(this.w_DFVOCFGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',this.w_DFVOCFGE)
            select DFVOCFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFVOCFGE = NVL(_Link_.DFVOCFIN,space(6))
    else
      if i_cCtrl<>'Load'
        this.w_DFVOCFGE = space(6)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])+'\'+cp_ToStr(_Link_.DFVOCFIN,1)
      cp_ShowWarn(i_cKey,this.VOC_FINZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFVOCFGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DFVOCEFF
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_FINZ_IDX,3]
    i_lTable = "VOC_FINZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2], .t., this.VOC_FINZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFVOCEFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDF_AVF',True,'VOC_FINZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DFVOCFIN like "+cp_ToStrODBC(trim(this.w_DFVOCEFF)+"%");

          i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DFVOCFIN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DFVOCFIN',trim(this.w_DFVOCEFF))
          select DFVOCFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DFVOCFIN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFVOCEFF)==trim(_Link_.DFVOCFIN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFVOCEFF) and !this.bDontReportError
            deferred_cp_zoom('VOC_FINZ','*','DFVOCFIN',cp_AbsName(oSource.parent,'oDFVOCEFF_1_20'),i_cWhere,'GSDF_AVF',"Voci finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                     +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',oSource.xKey(1))
            select DFVOCFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFVOCEFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN";
                   +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(this.w_DFVOCEFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',this.w_DFVOCEFF)
            select DFVOCFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFVOCEFF = NVL(_Link_.DFVOCFIN,space(6))
    else
      if i_cCtrl<>'Load'
        this.w_DFVOCEFF = space(6)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])+'\'+cp_ToStr(_Link_.DFVOCFIN,1)
      cp_ShowWarn(i_cKey,this.VOC_FINZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFVOCEFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DFCONTIN
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFCONTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DFCONTIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCOL);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCOL;
                     ,'ANCODICE',trim(this.w_DFCONTIN))
          select ANTIPCON,ANCODICE,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFCONTIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFCONTIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDFCONTIN_1_52'),i_cWhere,'GSAR_BZC',"Conti contabili banche",'GSDF_APD.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCOL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Inserire un conto congruente (tipo=banche) e non gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCOL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFCONTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DFCONTIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCOL;
                       ,'ANCODICE',this.w_DFCONTIN)
            select ANTIPCON,ANCODICE,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFCONTIN = NVL(_Link_.ANCODICE,space(15))
      this.w_TIPSOT = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DFCONTIN = space(15)
      endif
      this.w_TIPSOT = space(1)
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSOT='B' AND .w_PARTSN<>'S' 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un conto congruente (tipo=banche) e non gestito a partite")
        endif
        this.w_DFCONTIN = space(15)
        this.w_TIPSOT = space(1)
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFCONTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDFTRAAZI_1_2.value==this.w_DFTRAAZI)
      this.oPgFrm.Page1.oPag.oDFTRAAZI_1_2.value=this.w_DFTRAAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oDFFLPROV_1_3.RadioValue()==this.w_DFFLPROV)
      this.oPgFrm.Page1.oPag.oDFFLPROV_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFTRASCA_1_4.RadioValue()==this.w_DFTRASCA)
      this.oPgFrm.Page1.oPag.oDFTRASCA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFTRATIP_1_5.RadioValue()==this.w_DFTRATIP)
      this.oPgFrm.Page1.oPag.oDFTRATIP_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFTIPEXP_1_6.RadioValue()==this.w_DFTIPEXP)
      this.oPgFrm.Page1.oPag.oDFTIPEXP_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDATUEX_1_7.value==this.w_DFDATUEX)
      this.oPgFrm.Page1.oPag.oDFDATUEX_1_7.value=this.w_DFDATUEX
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCORRPC_1_8.RadioValue()==this.w_DFCORRPC)
      this.oPgFrm.Page1.oPag.oDFCORRPC_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDF_LINEA_1_9.value==this.w_DF_LINEA)
      this.oPgFrm.Page1.oPag.oDF_LINEA_1_9.value=this.w_DF_LINEA
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDIS_RI_1_11.RadioValue()==this.w_DFDIS_RI)
      this.oPgFrm.Page1.oPag.oDFDIS_RI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDIS_RA_1_12.RadioValue()==this.w_DFDIS_RA)
      this.oPgFrm.Page1.oPag.oDFDIS_RA_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFESPACC_1_13.RadioValue()==this.w_DFESPACC)
      this.oPgFrm.Page1.oPag.oDFESPACC_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCORRSC_1_14.RadioValue()==this.w_DFCORRSC)
      this.oPgFrm.Page1.oPag.oDFCORRSC_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFRATSCA_1_15.value==this.w_DFRATSCA)
      this.oPgFrm.Page1.oPag.oDFRATSCA_1_15.value=this.w_DFRATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDFRATSDT_1_16.value==this.w_DFRATSDT)
      this.oPgFrm.Page1.oPag.oDFRATSDT_1_16.value=this.w_DFRATSDT
    endif
    if not(this.oPgFrm.Page1.oPag.oDFVOCFCL_1_17.value==this.w_DFVOCFCL)
      this.oPgFrm.Page1.oPag.oDFVOCFCL_1_17.value=this.w_DFVOCFCL
    endif
    if not(this.oPgFrm.Page1.oPag.oDFVOCFFO_1_18.value==this.w_DFVOCFFO)
      this.oPgFrm.Page1.oPag.oDFVOCFFO_1_18.value=this.w_DFVOCFFO
    endif
    if not(this.oPgFrm.Page1.oPag.oDFVOCFGE_1_19.value==this.w_DFVOCFGE)
      this.oPgFrm.Page1.oPag.oDFVOCFGE_1_19.value=this.w_DFVOCFGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDFVOCEFF_1_20.value==this.w_DFVOCEFF)
      this.oPgFrm.Page1.oPag.oDFVOCEFF_1_20.value=this.w_DFVOCEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCASFLO_1_21.RadioValue()==this.w_DFCASFLO)
      this.oPgFrm.Page1.oPag.oDFCASFLO_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFEXPMOV_1_22.RadioValue()==this.w_DFEXPMOV)
      this.oPgFrm.Page1.oPag.oDFEXPMOV_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDFNUMMOV_1_23.value==this.w_DFNUMMOV)
      this.oPgFrm.Page1.oPag.oDFNUMMOV_1_23.value=this.w_DFNUMMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oRADESCRI_1_34.value==this.w_RADESCRI)
      this.oPgFrm.Page1.oPag.oRADESCRI_1_34.value=this.w_RADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRADESCRIS_1_35.value==this.w_RADESCRIS)
      this.oPgFrm.Page1.oPag.oRADESCRIS_1_35.value=this.w_RADESCRIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDFPERESP_2_1.value==this.w_DFPERESP)
      this.oPgFrm.Page2.oPag.oDFPERESP_2_1.value=this.w_DFPERESP
    endif
    if not(this.oPgFrm.Page2.oPag.oDFFILPDC_2_4.value==this.w_DFFILPDC)
      this.oPgFrm.Page2.oPag.oDFFILPDC_2_4.value=this.w_DFFILPDC
    endif
    if not(this.oPgFrm.Page2.oPag.oDFFILSCA_2_7.value==this.w_DFFILSCA)
      this.oPgFrm.Page2.oPag.oDFFILSCA_2_7.value=this.w_DFFILSCA
    endif
    if not(this.oPgFrm.Page2.oPag.oDFFILMOV_2_8.value==this.w_DFFILMOV)
      this.oPgFrm.Page2.oPag.oDFFILMOV_2_8.value=this.w_DFFILMOV
    endif
    if not(this.oPgFrm.Page2.oPag.oDFPERIMP_2_10.value==this.w_DFPERIMP)
      this.oPgFrm.Page2.oPag.oDFPERIMP_2_10.value=this.w_DFPERIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oDFPERBCK_2_13.value==this.w_DFPERBCK)
      this.oPgFrm.Page2.oPag.oDFPERBCK_2_13.value=this.w_DFPERBCK
    endif
    if not(this.oPgFrm.Page2.oPag.oDFNOMFIL_2_20.value==this.w_DFNOMFIL)
      this.oPgFrm.Page2.oPag.oDFNOMFIL_2_20.value=this.w_DFNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCONTIN_1_52.value==this.w_DFCONTIN)
      this.oPgFrm.Page1.oPag.oDFCONTIN_1_52.value=this.w_DFCONTIN
    endif
    cp_SetControlsValueExtFlds(this,'DOCFINPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DFTRAAZI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFTRAAZI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DFTRAAZI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DF_LINEA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDF_LINEA_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DF_LINEA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Empty(.w_Rascadut))  and not(empty(.w_DFRATSCA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFRATSCA_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Default rating non valido")
          case   not(Empty(.w_Rascaduts))  and not(empty(.w_DFRATSDT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFRATSDT_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Default rating non valido")
          case   (empty(.w_DFNUMMOV))  and not(.w_Dfexpmov='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFNUMMOV_1_23.SetFocus()
            i_bnoObbl = !empty(.w_DFNUMMOV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(JustExt(Upper(.w_Dffilpdc))='TXT' Or Empty(.w_Dffilpdc))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDFFILPDC_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("File di esportazione con estensione diversa da txt")
          case   not(JustExt(Upper(.w_Dffilsca))='TXT' Or Empty(.w_Dffilsca))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDFFILSCA_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("File di esportazione con estensione diversa da txt")
          case   not(JustExt(Upper(.w_Dffilmov))='TXT' Or Empty(.w_Dffilmov))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDFFILMOV_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("File di esportazione con estensione diversa da txt")
          case   not(JustExt(Upper(.w_DFNOMFIL))='TXT' Or Empty(.w_DFNOMFIL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDFNOMFIL_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("File di importazione con estensione diversa da txt")
          case   not(.w_TIPSOT='B' AND .w_PARTSN<>'S' )  and not(empty(.w_DFCONTIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFCONTIN_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un conto congruente (tipo=banche) e non gestito a partite")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DFEXPMOV = this.w_DFEXPMOV
    this.o_DFPERESP = this.w_DFPERESP
    this.o_DFPERIMP = this.w_DFPERIMP
    this.o_DFPERBCK = this.w_DFPERBCK
    return

enddefine

* --- Define pages as container
define class tgsdf_apdPag1 as StdContainer
  Width  = 780
  height = 590
  stdWidth  = 780
  stdheight = 590
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFTRAAZI_1_2 as StdField with uid="XYTMXCDZQX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DFTRAAZI", cQueryName = "DFTRAAZI",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Indicazione dell'azienda da passare nel file di DocFinance",;
    HelpContextID = 69380481,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=155, Top=32, InputMask=replicate('X',10)


  add object oDFFLPROV_1_3 as StdCombo with uid="ZZLNQVVYUT",rtseq=3,rtrep=.f.,left=390,top=32,width=100,height=22;
    , ToolTipText = "Stato delle registrazioni importate da DocFinance";
    , HelpContextID = 37325172;
    , cFormVar="w_DFFLPROV",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDFFLPROV_1_3.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oDFFLPROV_1_3.GetRadio()
    this.Parent.oContained.w_DFFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oDFFLPROV_1_3.SetRadio()
    this.Parent.oContained.w_DFFLPROV=trim(this.Parent.oContained.w_DFFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_DFFLPROV=='N',1,;
      iif(this.Parent.oContained.w_DFFLPROV=='S',2,;
      0))
  endfunc

  add object oDFTRASCA_1_4 as StdCheck with uid="OEQHUDSJVK",rtseq=4,rtrep=.f.,left=522, top=35, caption="Trascodifica causali DocFinance",;
    ToolTipText = "Se attivo, viene gestita la trascodifica delle causali sia in fase di importazione che esportazione movimenti",;
    HelpContextID = 232609399,;
    cFormVar="w_DFTRASCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFTRASCA_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDFTRASCA_1_4.GetRadio()
    this.Parent.oContained.w_DFTRASCA = this.RadioValue()
    return .t.
  endfunc

  func oDFTRASCA_1_4.SetRadio()
    this.Parent.oContained.w_DFTRASCA=trim(this.Parent.oContained.w_DFTRASCA)
    this.value = ;
      iif(this.Parent.oContained.w_DFTRASCA=='S',1,;
      0)
  endfunc

  add object oDFTRATIP_1_5 as StdCheck with uid="IKQLTKKPGJ",rtseq=5,rtrep=.f.,left=522, top=58, caption="Trascodifica tipi pagamento",;
    ToolTipText = "Se attivo, viene gestita la trascodifica dei tipi/categorie pagamento",;
    HelpContextID = 19048826,;
    cFormVar="w_DFTRATIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFTRATIP_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDFTRATIP_1_5.GetRadio()
    this.Parent.oContained.w_DFTRATIP = this.RadioValue()
    return .t.
  endfunc

  func oDFTRATIP_1_5.SetRadio()
    this.Parent.oContained.w_DFTRATIP=trim(this.Parent.oContained.w_DFTRATIP)
    this.value = ;
      iif(this.Parent.oContained.w_DFTRATIP=='S',1,;
      0)
  endfunc


  add object oDFTIPEXP_1_6 as StdCombo with uid="DMODWZSUTS",rtseq=6,rtrep=.f.,left=240,top=97,width=108,height=22;
    , ToolTipText = "Tipologia esportazione piano dei conti";
    , HelpContextID = 255568250;
    , cFormVar="w_DFTIPEXP",RowSource=""+"Tutti,"+"Variati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDFTIPEXP_1_6.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oDFTIPEXP_1_6.GetRadio()
    this.Parent.oContained.w_DFTIPEXP = this.RadioValue()
    return .t.
  endfunc

  func oDFTIPEXP_1_6.SetRadio()
    this.Parent.oContained.w_DFTIPEXP=trim(this.Parent.oContained.w_DFTIPEXP)
    this.value = ;
      iif(this.Parent.oContained.w_DFTIPEXP=='T',1,;
      iif(this.Parent.oContained.w_DFTIPEXP=='V',2,;
      0))
  endfunc

  add object oDFDATUEX_1_7 as StdField with uid="GJZYOJVDRV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DFDATUEX", cQueryName = "DFDATUEX",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultima esportazione piano dei conti",;
    HelpContextID = 16471694,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=643, Top=96

  func oDFDATUEX_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DFTIPEXP='V')
    endwith
   endif
  endfunc

  add object oDFCORRPC_1_8 as StdCheck with uid="KMPJXCAJRW",rtseq=8,rtrep=.f.,left=240, top=132, caption="Riempimento numero conto corrente",;
    ToolTipText = "Se attivo, il numero conto corrente sar� preceduto con zeri a completamento nell'esportazione ( Posizione 230)",;
    HelpContextID = 233391737,;
    cFormVar="w_DFCORRPC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFCORRPC_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDFCORRPC_1_8.GetRadio()
    this.Parent.oContained.w_DFCORRPC = this.RadioValue()
    return .t.
  endfunc

  func oDFCORRPC_1_8.SetRadio()
    this.Parent.oContained.w_DFCORRPC=trim(this.Parent.oContained.w_DFCORRPC)
    this.value = ;
      iif(this.Parent.oContained.w_DFCORRPC=='S',1,;
      0)
  endfunc

  add object oDF_LINEA_1_9 as StdField with uid="RXWNEGKSWW",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DF_LINEA", cQueryName = "DF_LINEA",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Linea commerciale",;
    HelpContextID = 156763767,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=643, Top=134, InputMask=replicate('X',4)

  add object oDFDIS_RI_1_11 as StdCheck with uid="QANMNLHFGU",rtseq=10,rtrep=.f.,left=240, top=179, caption="R.I.D.",;
    ToolTipText = "Se attivo, attribuisce il numero effetto alle scadenze di tipo R.I.D. esportate",;
    HelpContextID = 183719551,;
    cFormVar="w_DFDIS_RI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFDIS_RI_1_11.RadioValue()
    return(iif(this.value =1,'RI',;
    ' '))
  endfunc
  func oDFDIS_RI_1_11.GetRadio()
    this.Parent.oContained.w_DFDIS_RI = this.RadioValue()
    return .t.
  endfunc

  func oDFDIS_RI_1_11.SetRadio()
    this.Parent.oContained.w_DFDIS_RI=trim(this.Parent.oContained.w_DFDIS_RI)
    this.value = ;
      iif(this.Parent.oContained.w_DFDIS_RI=='RI',1,;
      0)
  endfunc

  add object oDFDIS_RA_1_12 as StdCheck with uid="FAVHXASJZY",rtseq=11,rtrep=.f.,left=336, top=179, caption="Ric.bancaria/Ri.Ba.",;
    ToolTipText = "Se attivo, attribuisce il numero effetto alle scadenze di tipo ricevute bancarie/Ri.Ba. esportate",;
    HelpContextID = 183719543,;
    cFormVar="w_DFDIS_RA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFDIS_RA_1_12.RadioValue()
    return(iif(this.value =1,'RB',;
    ' '))
  endfunc
  func oDFDIS_RA_1_12.GetRadio()
    this.Parent.oContained.w_DFDIS_RA = this.RadioValue()
    return .t.
  endfunc

  func oDFDIS_RA_1_12.SetRadio()
    this.Parent.oContained.w_DFDIS_RA=trim(this.Parent.oContained.w_DFDIS_RA)
    this.value = ;
      iif(this.Parent.oContained.w_DFDIS_RA=='RB',1,;
      0)
  endfunc

  add object oDFESPACC_1_13 as StdCheck with uid="FSDZSORJUQ",rtseq=12,rtrep=.f.,left=240, top=227, caption="Partite di acconto",;
    ToolTipText = "Se attivo, esporta anche le partite di tipo acconto",;
    HelpContextID = 214787705,;
    cFormVar="w_DFESPACC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFESPACC_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDFESPACC_1_13.GetRadio()
    this.Parent.oContained.w_DFESPACC = this.RadioValue()
    return .t.
  endfunc

  func oDFESPACC_1_13.SetRadio()
    this.Parent.oContained.w_DFESPACC=trim(this.Parent.oContained.w_DFESPACC)
    this.value = ;
      iif(this.Parent.oContained.w_DFESPACC=='S',1,;
      0)
  endfunc

  add object oDFCORRSC_1_14 as StdCheck with uid="OSZFOQKAYT",rtseq=13,rtrep=.f.,left=441, top=227, caption="Riempimento numero conto corrente",;
    ToolTipText = "Se attivo, il numero conto corrente sar� preceduto con zeri a completamento nell'esportazione ( Posizione 235)",;
    HelpContextID = 233391737,;
    cFormVar="w_DFCORRSC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFCORRSC_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDFCORRSC_1_14.GetRadio()
    this.Parent.oContained.w_DFCORRSC = this.RadioValue()
    return .t.
  endfunc

  func oDFCORRSC_1_14.SetRadio()
    this.Parent.oContained.w_DFCORRSC=trim(this.Parent.oContained.w_DFCORRSC)
    this.value = ;
      iif(this.Parent.oContained.w_DFCORRSC=='S',1,;
      0)
  endfunc

  add object oDFRATSCA_1_15 as StdField with uid="JPCUYVHHCE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DFRATSCA", cQueryName = "DFRATSCA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Default rating non valido",;
    ToolTipText = "Default rating da scadere",;
    HelpContextID = 251410039,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=240, Top=257, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="DORATING", cZoomOnZoom="Gsdf_Ara", oKey_1_1="RACODICE", oKey_1_2="this.w_DFRATSCA"

  func oDFRATSCA_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFRATSCA_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDFRATSCA_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DORATING','*','RACODICE',cp_AbsName(this.parent,'oDFRATSCA_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsdf_Ara',"Rating",'Gsdf_Ara.DORATING_VZM',this.parent.oContained
  endproc
  proc oDFRATSCA_1_15.mZoomOnZoom
    local i_obj
    i_obj=Gsdf_Ara()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RACODICE=this.parent.oContained.w_DFRATSCA
     i_obj.ecpSave()
  endproc

  add object oDFRATSDT_1_16 as StdField with uid="HKWTPIGHPS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DFRATSDT", cQueryName = "DFRATSDT",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Default rating non valido",;
    ToolTipText = "Default rating scaduti",;
    HelpContextID = 251410058,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=240, Top=289, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="DORATING", cZoomOnZoom="Gsdf_Ara", oKey_1_1="RACODICE", oKey_1_2="this.w_DFRATSDT"

  func oDFRATSDT_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFRATSDT_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDFRATSDT_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DORATING','*','RACODICE',cp_AbsName(this.parent,'oDFRATSDT_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsdf_Ara',"Rating",'Gsdf_Ara.DORATING_VZM',this.parent.oContained
  endproc
  proc oDFRATSDT_1_16.mZoomOnZoom
    local i_obj
    i_obj=Gsdf_Ara()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RACODICE=this.parent.oContained.w_DFRATSDT
     i_obj.ecpSave()
  endproc

  add object oDFVOCFCL_1_17 as StdField with uid="FEHRERVEEM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DFVOCFCL", cQueryName = "DFVOCFCL",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Default voce finanziaria clienti",;
    HelpContextID = 16414338,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=240, Top=321, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VOC_FINZ", cZoomOnZoom="GSDF_AVF", oKey_1_1="DFVOCFIN", oKey_1_2="this.w_DFVOCFCL"

  func oDFVOCFCL_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFVOCFCL_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDFVOCFCL_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_FINZ','*','DFVOCFIN',cp_AbsName(this.parent,'oDFVOCFCL_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDF_AVF',"Voci finanziarie",'',this.parent.oContained
  endproc
  proc oDFVOCFCL_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSDF_AVF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DFVOCFIN=this.parent.oContained.w_DFVOCFCL
     i_obj.ecpSave()
  endproc

  add object oDFVOCFFO_1_18 as StdField with uid="CIRDOFOUYW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DFVOCFFO", cQueryName = "DFVOCFFO",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Default voce finanziaria fornitori",;
    HelpContextID = 16414341,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=614, Top=321, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VOC_FINZ", cZoomOnZoom="GSDF_AVF", oKey_1_1="DFVOCFIN", oKey_1_2="this.w_DFVOCFFO"

  func oDFVOCFFO_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFVOCFFO_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDFVOCFFO_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_FINZ','*','DFVOCFIN',cp_AbsName(this.parent,'oDFVOCFFO_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDF_AVF',"Voci finanziarie",'',this.parent.oContained
  endproc
  proc oDFVOCFFO_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSDF_AVF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DFVOCFIN=this.parent.oContained.w_DFVOCFFO
     i_obj.ecpSave()
  endproc

  add object oDFVOCFGE_1_19 as StdField with uid="ZVZHDXBQUP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DFVOCFGE", cQueryName = "DFVOCFGE",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Default voce finanziaria generici",;
    HelpContextID = 16414331,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=240, Top=353, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VOC_FINZ", cZoomOnZoom="GSDF_AVF", oKey_1_1="DFVOCFIN", oKey_1_2="this.w_DFVOCFGE"

  func oDFVOCFGE_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFVOCFGE_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDFVOCFGE_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_FINZ','*','DFVOCFIN',cp_AbsName(this.parent,'oDFVOCFGE_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDF_AVF',"Voci finanziarie",'',this.parent.oContained
  endproc
  proc oDFVOCFGE_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSDF_AVF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DFVOCFIN=this.parent.oContained.w_DFVOCFGE
     i_obj.ecpSave()
  endproc

  add object oDFVOCEFF_1_20 as StdField with uid="SHWXBDAXQN",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DFVOCEFF", cQueryName = "DFVOCEFF",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Voce finanziaria effetti",;
    HelpContextID = 268072572,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=240, Top=385, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VOC_FINZ", cZoomOnZoom="GSDF_AVF", oKey_1_1="DFVOCFIN", oKey_1_2="this.w_DFVOCEFF"

  func oDFVOCEFF_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFVOCEFF_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDFVOCEFF_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_FINZ','*','DFVOCFIN',cp_AbsName(this.parent,'oDFVOCEFF_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDF_AVF',"Voci finanziarie",'',this.parent.oContained
  endproc
  proc oDFVOCEFF_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSDF_AVF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DFVOCFIN=this.parent.oContained.w_DFVOCEFF
     i_obj.ecpSave()
  endproc


  add object oDFCASFLO_1_21 as StdCombo with uid="JDNXLFVAYV",rtseq=20,rtrep=.f.,left=240,top=447,width=255,height=22;
    , ToolTipText = "Se evasione proporzionale � garantito il pregresso criterio di esportazione dati cash flow";
    , HelpContextID = 236239227;
    , cFormVar="w_DFCASFLO",RowSource=""+"Standard (evasione proporzionale),"+"Evasione per data scadenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDFCASFLO_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oDFCASFLO_1_21.GetRadio()
    this.Parent.oContained.w_DFCASFLO = this.RadioValue()
    return .t.
  endfunc

  func oDFCASFLO_1_21.SetRadio()
    this.Parent.oContained.w_DFCASFLO=trim(this.Parent.oContained.w_DFCASFLO)
    this.value = ;
      iif(this.Parent.oContained.w_DFCASFLO=='S',1,;
      iif(this.Parent.oContained.w_DFCASFLO=='E',2,;
      0))
  endfunc

  add object oDFEXPMOV_1_22 as StdCheck with uid="QVZFLNGZAB",rtseq=21,rtrep=.f.,left=106, top=503, caption="Esportazione movimenti",;
    ToolTipText = "Se attivo, abilita l'esportazione dei movimenti a DocFinance",;
    HelpContextID = 120428916,;
    cFormVar="w_DFEXPMOV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDFEXPMOV_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDFEXPMOV_1_22.GetRadio()
    this.Parent.oContained.w_DFEXPMOV = this.RadioValue()
    return .t.
  endfunc

  func oDFEXPMOV_1_22.SetRadio()
    this.Parent.oContained.w_DFEXPMOV=trim(this.Parent.oContained.w_DFEXPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_DFEXPMOV=='S',1,;
      0)
  endfunc

  add object oDFNUMMOV_1_23 as StdField with uid="HBLCSIWMBG",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DFNUMMOV", cQueryName = "DFNUMMOV",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Accoglie il numeratore attribuito in fase di esportazione movimenti",;
    HelpContextID = 123734388,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=499, Top=504, InputMask=replicate('X',4)

  func oDFNUMMOV_1_23.mHide()
    with this.Parent.oContained
      return (.w_Dfexpmov='N')
    endwith
  endfunc

  add object oRADESCRI_1_34 as StdField with uid="IKFYREDRER",rtseq=23,rtrep=.f.,;
    cFormVar = "w_RADESCRI", cQueryName = "RADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 250565215,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=290, Top=257, InputMask=replicate('X',50)

  add object oRADESCRIS_1_35 as StdField with uid="MXHXABQEYM",rtseq=24,rtrep=.f.,;
    cFormVar = "w_RADESCRIS", cQueryName = "RADESCRIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 250566543,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=290, Top=289, InputMask=replicate('X',50)

  add object oDFCONTIN_1_52 as StdField with uid="FDGSJLJTCW",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DFCONTIN", cQueryName = "DFCONTIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un conto congruente (tipo=banche) e non gestito a partite",;
    ToolTipText = "Conto transito insoluti",;
    HelpContextID = 5683580,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=240, Top=566, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCOL", oKey_2_1="ANCODICE", oKey_2_2="this.w_DFCONTIN"

  func oDFCONTIN_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFCONTIN_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDFCONTIN_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCOL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCOL)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDFCONTIN_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contabili banche",'GSDF_APD.CONTI_VZM',this.parent.oContained
  endproc
  proc oDFCONTIN_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCOL
     i_obj.w_ANCODICE=this.parent.oContained.w_DFCONTIN
     i_obj.ecpSave()
  endproc

  add object oStr_1_10 as StdString with uid="SVUGBFLLMG",Visible=.t., Left=21, Top=34,;
    Alignment=1, Width=130, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="GQOANBBNXZ",Visible=.t., Left=79, Top=259,;
    Alignment=1, Width=158, Height=18,;
    Caption="Default rating a scadere:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="ASGABOOWJG",Visible=.t., Left=70, Top=290,;
    Alignment=1, Width=167, Height=18,;
    Caption="Default rating scaduti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ZWCCHCFBIT",Visible=.t., Left=18, Top=324,;
    Alignment=1, Width=219, Height=18,;
    Caption="Default voce finanziaria clienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="UEOQJNDMSI",Visible=.t., Left=392, Top=324,;
    Alignment=1, Width=219, Height=18,;
    Caption="Default voce finanziaria fornitori:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="DNXUSICBKY",Visible=.t., Left=7, Top=477,;
    Alignment=0, Width=239, Height=18,;
    Caption="Parametri di esportazione movimenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="SNJBWRSHZI",Visible=.t., Left=7, Top=203,;
    Alignment=0, Width=257, Height=18,;
    Caption="Parametri per esportazione scadenze"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="PXPXVZEEUZ",Visible=.t., Left=7, Top=99,;
    Alignment=1, Width=230, Height=18,;
    Caption="Tipo esportazione piano dei conti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="MXEMBUXBKB",Visible=.t., Left=412, Top=99,;
    Alignment=1, Width=228, Height=18,;
    Caption="Ultima esportazione piano dei conti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="XMAUCWYPJM",Visible=.t., Left=501, Top=135,;
    Alignment=1, Width=139, Height=18,;
    Caption="Linea commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="FALEKZWFTZ",Visible=.t., Left=7, Top=156,;
    Alignment=0, Width=257, Height=18,;
    Caption="Parametri per numerazione effetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="WPWVQPTHAN",Visible=.t., Left=313, Top=506,;
    Alignment=1, Width=182, Height=18,;
    Caption="Numeratore fisso movimenti:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_Dfexpmov='N')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="ESIMQWPLYZ",Visible=.t., Left=260, Top=34,;
    Alignment=1, Width=126, Height=18,;
    Caption="Stato primanota:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="ZJHSQNFHRY",Visible=.t., Left=18, Top=356,;
    Alignment=1, Width=219, Height=18,;
    Caption="Default voce finanziaria generici:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="CBNXFEAERW",Visible=.t., Left=18, Top=387,;
    Alignment=1, Width=219, Height=18,;
    Caption="Voce finanziaria effetti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="PQUYZYZFVB",Visible=.t., Left=7, Top=71,;
    Alignment=0, Width=257, Height=18,;
    Caption="Parametri per esportazione piano dei conti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="JDYLJDROAO",Visible=.t., Left=7, Top=538,;
    Alignment=0, Width=257, Height=18,;
    Caption="Parametri per gestione insoluti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="WTZODTXYNH",Visible=.t., Left=84, Top=569,;
    Alignment=1, Width=153, Height=18,;
    Caption="Conto transito insoluti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="VPXVOKIPMF",Visible=.t., Left=7, Top=414,;
    Alignment=0, Width=257, Height=18,;
    Caption="Parametri per esportazione cash flow"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="GKVVBAFKTK",Visible=.t., Left=94, Top=448,;
    Alignment=1, Width=143, Height=18,;
    Caption="Calcolo rate documenti:"  ;
  , bGlobalFont=.t.

  add object oBox_1_29 as StdBox with uid="VTOIDYHWWF",left=0, top=495, width=779,height=2

  add object oBox_1_31 as StdBox with uid="OCUEYQBKHV",left=1, top=218, width=779,height=2

  add object oBox_1_40 as StdBox with uid="GREUCNWNGQ",left=1, top=175, width=779,height=2

  add object oBox_1_46 as StdBox with uid="OCSSSCQCBZ",left=1, top=90, width=779,height=2

  add object oBox_1_48 as StdBox with uid="DYNSOXQNWX",left=0, top=556, width=779,height=2

  add object oBox_1_56 as StdBox with uid="RMAHCEYUHU",left=0, top=431, width=779,height=2
enddefine
define class tgsdf_apdPag2 as StdContainer
  Width  = 780
  height = 590
  stdWidth  = 780
  stdheight = 590
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFPERESP_2_1 as StdField with uid="EQTXFVWNLB",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DFPERESP", cQueryName = "DFPERESP",;
    bObbl = .f. , nPag = 2, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di esportazione file",;
    HelpContextID = 14685830,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=174, Top=37, InputMask=replicate('X',150)


  add object oBtn_2_3 as StdButton with uid="RUDQNHINUV",left=751, top=37, width=22,height=21,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per selezionare il path del file di esportazione";
    , HelpContextID = 159851306;
  , bGlobalFont=.t.

    proc oBtn_2_3.Click()
      with this.Parent.oContained
        do Pathexport with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDFFILPDC_2_4 as StdField with uid="IAERQEMACX",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DFFILPDC", cQueryName = "DFFILPDC",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "File di esportazione con estensione diversa da txt",;
    ToolTipText = "Nome del file di esportazione del piano dei conti",;
    HelpContextID = 193164921,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=174, Top=70, InputMask=replicate('X',50)

  func oDFFILPDC_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (JustExt(Upper(.w_Dffilpdc))='TXT' Or Empty(.w_Dffilpdc))
    endwith
    return bRes
  endfunc

  add object oDFFILSCA_2_7 as StdField with uid="PSRIGTLBHT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DFFILSCA", cQueryName = "DFFILSCA",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "File di esportazione con estensione diversa da txt",;
    ToolTipText = "Nome del file di esportazione dello scadenzario",;
    HelpContextID = 243496567,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=174, Top=105, InputMask=replicate('X',50)

  func oDFFILSCA_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (JustExt(Upper(.w_Dffilsca))='TXT' Or Empty(.w_Dffilsca))
    endwith
    return bRes
  endfunc

  add object oDFFILMOV_2_8 as StdField with uid="FKMDHTIUBA",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DFFILMOV", cQueryName = "DFFILMOV",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "File di esportazione con estensione diversa da txt",;
    ToolTipText = "Nome del file di esportazione dei movimenti",;
    HelpContextID = 125602164,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=174, Top=138, InputMask=replicate('X',50)

  func oDFFILMOV_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (JustExt(Upper(.w_Dffilmov))='TXT' Or Empty(.w_Dffilmov))
    endwith
    return bRes
  endfunc

  add object oDFPERIMP_2_10 as StdField with uid="EKNOADPAKV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DFPERIMP", cQueryName = "DFPERIMP",;
    bObbl = .f. , nPag = 2, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di importazione file",;
    HelpContextID = 186640762,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=174, Top=209, InputMask=replicate('X',150)


  add object oBtn_2_12 as StdButton with uid="XUCMKAUUEH",left=751, top=209, width=22,height=21,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per selezionare il path del file di importazione";
    , HelpContextID = 159851306;
  , bGlobalFont=.t.

    proc oBtn_2_12.Click()
      with this.Parent.oContained
        do Pathimport with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDFPERBCK_2_13 as StdField with uid="ODGDVAJSBR",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DFPERBCK", cQueryName = "DFPERBCK",;
    bObbl = .f. , nPag = 2, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di backup file",;
    HelpContextID = 232789633,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=174, Top=246, InputMask=replicate('X',150)


  add object oBtn_2_15 as StdButton with uid="APTEJFRRTN",left=751, top=246, width=22,height=21,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per selezionare il path del file di backup";
    , HelpContextID = 159851306;
  , bGlobalFont=.t.

    proc oBtn_2_15.Click()
      with this.Parent.oContained
        do Pathbackup with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDFNOMFIL_2_20 as StdField with uid="UAOLDYFKMR",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DFNOMFIL", cQueryName = "DFNOMFIL",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "File di importazione con estensione diversa da txt",;
    ToolTipText = "Nome del file di importazione",;
    HelpContextID = 241568126,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=174, Top=279, InputMask=replicate('X',50)

  func oDFNOMFIL_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (JustExt(Upper(.w_DFNOMFIL))='TXT' Or Empty(.w_DFNOMFIL))
    endwith
    return bRes
  endfunc

  add object oStr_2_2 as StdString with uid="TBOLSULRYO",Visible=.t., Left=7, Top=39,;
    Alignment=1, Width=165, Height=18,;
    Caption="Path di esportazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="DWXAZAACIS",Visible=.t., Left=13, Top=71,;
    Alignment=1, Width=159, Height=18,;
    Caption="Nome file piano dei conti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="NONJILYVYZ",Visible=.t., Left=13, Top=106,;
    Alignment=1, Width=159, Height=18,;
    Caption="Nome file scadenzario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="FGXSPWEJHI",Visible=.t., Left=13, Top=139,;
    Alignment=1, Width=159, Height=18,;
    Caption="Nome file movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="JBUHZZOBXO",Visible=.t., Left=7, Top=211,;
    Alignment=1, Width=165, Height=18,;
    Caption="Path di importazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="EYFLJQJEPH",Visible=.t., Left=7, Top=248,;
    Alignment=1, Width=165, Height=18,;
    Caption="Path di backup:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="ORLYHCPUNP",Visible=.t., Left=5, Top=181,;
    Alignment=0, Width=165, Height=18,;
    Caption="Importazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_19 as StdString with uid="BBJGGCAGLV",Visible=.t., Left=5, Top=8,;
    Alignment=0, Width=165, Height=18,;
    Caption="Esportazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_21 as StdString with uid="ZVALEGNHTB",Visible=.t., Left=13, Top=280,;
    Alignment=1, Width=159, Height=18,;
    Caption="Nome file importazione:"  ;
  , bGlobalFont=.t.

  add object oBox_2_16 as StdBox with uid="TMWAQLFWPB",left=5, top=201, width=771,height=2

  add object oBox_2_18 as StdBox with uid="ZTXBBRGKSS",left=5, top=28, width=771,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdf_apd','DOCFINPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFCODAZI=DOCFINPA.DFCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsdf_apd
Proc Pathexport(pgm)
    Local L_PATHMEM
    L_PATHMEM=pgm.w_DFPERESP
    pgm.w_DFPERESP=Alltrim(left(cp_getdir(IIF(EMPTY(pgm.w_DFPERESP),sys(5)+sys(2003),pgm.w_DFPERESP),"Selezionare il percorso di esportazione DocFinance.","----")+space(200),200))
    If empty(pgm.w_DFPERESP)
      pgm.w_DFPERESP=L_PATHMEM
    Endif
    Release L_PATHMEM
Endproc

Proc Pathimport(pgm)
    Local L_PATHMEM
    L_PATHMEM=pgm.w_DFPERIMP
    pgm.w_DFPERIMP=Alltrim(left(cp_getdir(IIF(EMPTY(pgm.w_DFPERIMP),sys(5)+sys(2003),pgm.w_DFPERIMP),"Selezionare il percorso di importazione DocFinance.","----")+space(200),200))
    If empty(pgm.w_DFPERIMP)
      pgm.w_DFPERIMP=L_PATHMEM
    Endif
    Release L_PATHMEM
Endproc

Proc Pathbackup(pgm)
    Local L_PATHMEM
    L_PATHMEM=pgm.w_DFPERBCK
    pgm.w_DFPERBCK=Alltrim(left(cp_getdir(IIF(EMPTY(pgm.w_DFPERBCK),sys(5)+sys(2003),pgm.w_DFPERBCK),"Selezionare il percorso di backup DocFinance.","----")+space(200),200))
    If empty(pgm.w_DFPERBCK)
      pgm.w_DFPERBCK=L_PATHMEM
    Endif
    Release L_PATHMEM
Endproc
* --- Fine Area Manuale
