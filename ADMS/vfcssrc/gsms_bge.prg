* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsms_bge                                                        *
*              Generatore documentale da oggetto                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-09                                                      *
* Last revis.: 2015-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPADRE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsms_bge",oParentObject,m.pPADRE)
return(i_retval)

define class tgsms_bge as StdBatch
  * --- Local variables
  w_CHILDREF = space(10)
  pPADRE = .NULL.
  w_PADRE = .NULL.
  w_SELECTCURS = space(10)
  w_OLDAREA = space(10)
  w_RETVAL = space(254)
  w_bUnderTran = .f.
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_PRODOC = space(2)
  w_FLAGEN = space(1)
  w_CPCCCHK = space(10)
  w_MV__ANNO = 0
  w_MV__MESE = 0
  w_MV__NOTE = space(0)
  w_MV_FLAGG = space(1)
  w_MV_SEGNO = space(1)
  w_MVMC_PER = space(1)
  w_MVDATOAF = ctod("  /  /  ")
  w_MVTIPDIS = space(2)
  w_MVTIPCOL = space(2)
  w_MVFLRESC = space(1)
  w_MVFLRIAP = space(1)
  w_MVFLRVCL = space(1)
  w_MVPRECON = space(1)
  w_MVQTANOC = 0
  w_MVQTARES = 0
  w_MVGENPOS = .f.
  w_MVSTFILCB = space(1)
  w_MVACCOLD = 0
  w_MVACCONT = 0
  w_MVACCPRE = 0
  w_MVACCSUC = 0
  w_MVROWDDT = 0
  w_MVACIVA1 = space(5)
  w_MVACIVA2 = space(5)
  w_MVACIVA3 = space(5)
  w_MVACIVA4 = space(5)
  w_MVACIVA5 = space(5)
  w_MVACIVA6 = space(5)
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVAFLOM5 = space(1)
  w_MVAFLOM6 = space(1)
  w_MVAIMPN1 = 0
  w_MVAIMPN2 = 0
  w_MVAIMPN3 = 0
  w_MVAIMPN4 = 0
  w_MVAIMPN5 = 0
  w_MVAIMPN6 = 0
  w_MVAIMPS1 = 0
  w_MVAIMPS2 = 0
  w_MVAIMPS3 = 0
  w_MVAIMPS4 = 0
  w_MVAIMPS5 = 0
  w_MVAIMPS6 = 0
  w_MVAIRPOR = space(10)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVANNDOC = space(4)
  w_MVANNPRO = space(4)
  w_MVANNRET = space(4)
  w_MVASPEST = space(30)
  w_MVCACONT = space(5)
  w_MVCAOVAL = 0
  w_MVCATCON = space(5)
  w_MVCATOPE = space(2)
  w_MVCAUCOL = space(5)
  w_MVCAUCON = space(5)
  w_MVCAUIMB = 0
  w_MVCAUMAG = space(5)
  w_MVCESSER = space(10)
  w_MVCLADOC = space(2)
  w_MVCODAG2 = space(5)
  w_MVCODAGE = space(5)
  w_MVCODART = space(20)
  w_MVCODASP = space(3)
  w_MVCODATT = space(15)
  w_MVCODBA2 = space(15)
  w_MVCODBAN = space(10)
  w_MVCODCEN = space(15)
  w_MVCODCES = space(20)
  w_MVCODCLA = space(3)
  w_MVCODCOL = space(5)
  w_MVCODCOM = space(15)
  w_MVCODCOM = space(15)
  w_MVCODCON = space(15)
  w_MVCODCOS = space(5)
  w_MVCODDES = space(5)
  w_MVCODESE = space(4)
  w_MVCODICE = space(20)
  w_MVCODIVA = space(5)
  w_MVCODIVE = space(5)
  w_MVCODLIS = space(5)
  w_MVCODLOT = space(20)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVCODODL = space(15)
  w_MVCODORN = space(15)
  w_MVCODPAG = space(5)
  w_MVCODPOR = space(1)
  w_MVCODRES = space(5)
  w_MVCODSED = space(5)
  w_MVCODSPE = space(3)
  w_MVCODUB2 = space(20)
  w_MVCODUBI = space(20)
  w_MVCODUTE = 0
  w_MVCODVAL = space(3)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODVET = space(5)
  w_MVCONCON = space(1)
  w_MVCONIND = space(15)
  w_MVCONTRA = space(15)
  w_MVCONTRO = space(15)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATEST = ctod("  /  /  ")
  w_MVDATEVA = ctod("  /  /  ")
  w_MVDATGEN = ctod("  /  /  ")
  w_MVDATOAI = ctod("  /  /  ")
  w_MVDATPLA = ctod("  /  /  ")
  w_MVDATREG = ctod("  /  /  ")
  w_MVDATTRA = ctod("  /  /  ")
  w_MVDESART = space(40)
  w_MVDESDOC = space(50)
  w_MVDESSUP = space(0)
  w_MVDESSUP = space(0)
  w_MVEFFEVA = ctod("  /  /  ")
  w_MVF2CASC = space(1)
  w_MVF2IMPE = space(1)
  w_MVF2LOTT = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2RISE = space(1)
  w_MVFINCOM = ctod("  /  /  ")
  w_MVFLACCO = space(1)
  w_MVFLARIF = space(1)
  w_MVFLBLOC = space(1)
  w_MVFLCAPA = space(1)
  w_MVFLCASC = space(1)
  w_MVFLCOCO = space(1)
  w_MVFLCONT = space(1)
  w_MVFLELAN = space(1)
  w_MVFLELGM = space(1)
  w_MVFLERIF = space(1)
  w_MVFLEVAS = space(1)
  w_MVFLFOCA = space(1)
  w_MVFLFOSC = space(1)
  w_MVFLGIOM = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLINTE = space(1)
  w_MVFLINTR = space(1)
  w_MVFLLOTT = space(1)
  w_MVFLNOAN = space(1)
  w_MVFLOFFE = space(1)
  w_MVFLOMAG = space(1)
  w_MVFLORCO = space(1)
  w_MVFLORDI = space(1)
  w_MVFLPROV = space(1)
  w_MVFLRAGG = space(1)
  w_MVFLRIMB = space(1)
  w_MVFLRIMB = space(1)
  w_MVFLRINC = space(1)
  w_MVFLRIPA = space(1)
  w_MVFLRISE = space(1)
  w_MVFLRTRA = space(1)
  w_MVFLRTRA = space(1)
  w_MVFLSALD = space(1)
  w_MVFLSCAF = space(1)
  w_MVFLSCOM = space(1)
  w_MVFLSCOR = space(1)
  w_MVFLSEND = space(1)
  w_MVFLSFIN = space(1)
  w_MVFLTRAS = space(1)
  w_MVFLULCA = space(1)
  w_MVFLULPV = space(1)
  w_MVFLVABD = space(1)
  w_MVFLVEAC = space(1)
  w_MVGENEFF = space(1)
  w_MVGENPRO = .f.
  w_MVIMPAC2 = 0
  w_MVIMPACC = 0
  w_MVIMPARR = 0
  w_MVIMPCAP = 0
  w_MVIMPCOM = 0
  w_MVIMPEVA = 0
  w_MVIMPFIN = 0
  w_MVIMPNAZ = 0
  w_MVIMPPRO = 0
  w_MVIMPRBA = 0
  w_MVIMPSCO = 0
  w_MVINICOM = ctod("  /  /  ")
  w_MVIVAARR = space(5)
  w_MVIVABOL = space(5)
  w_MVIVACAU = space(5)
  w_MVIVAIMB = space(5)
  w_MVIVAINC = space(5)
  w_MVIVATRA = space(5)
  w_MVKEYSAL = space(20)
  w_MVMAXACC = 0
  w_MVMINTRA = space(2)
  w_MVMOLSUP = 0
  w_MVMOVCOM = space(10)
  w_MVNAZPRO = space(3)
  w_MVNOMENC = space(8)
  w_MVNOTAGG = space(40)
  w_MVNUMCOL = 0
  w_MVNUMCOR = space(25)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVNUMREG = 0
  w_MVNUMRIF = 0
  w_MVORATRA = space(2)
  w_MVPAEFOR = space(3)
  w_MVPERFIN = 0
  w_MVPERPRO = 0
  w_MVPERRET = 0
  w_MVPESNET = 0
  w_MVPRD = space(2)
  w_MVPREZZO = 0
  w_MVPROCAP = 0
  w_MVPROORD = space(2)
  w_MVPRP = space(2)
  w_MVQTACOL = 0
  w_MVQTAEV1 = 0
  w_MVQTAEVA = 0
  w_MVQTAIM1 = 0
  w_MVQTAIMP = 0
  w_MVQTALOR = 0
  w_MVQTAMOV = 0
  w_MVQTAPES = 0
  w_MVQTASAL = 0
  w_MVQTAUM1 = 0
  w_MVRIFACC = space(10)
  w_MVRIFCAC = 0
  w_MVRIFCON = space(10)
  w_MVRIFDCO = space(10)
  w_MVRIFDIC = space(15)
  w_MVRIFEDI = space(40)
  w_MVRIFESC = space(10)
  w_MVRIFESP = space(10)
  w_MVRIFFAD = space(10)
  w_MVRIFKIT = 0
  w_MVRIFODL = space(10)
  w_MVRIFORD = space(10)
  w_MVRIFPIA = space(10)
  w_MVRIGMAT = 0
  w_MVRITATT = 0
  w_MVRITPRE = 0
  w_MVROWRIF = 0
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVSCONTI = 0
  w_MVSCOPAG = 0
  w_MVSERDDT = space(10)
  w_MVSEREST = space(40)
  w_MVSERIAL = space(10)
  w_MVSERRIF = space(10)
  w_MVSERWEB = space(50)
  w_MVSPEBOL = 0
  w_MVSPEIMB = 0
  w_MVSPEIMB = 0
  w_MVSPEINC = 0
  w_MVSPETRA = 0
  w_MVSPETRA = 0
  w_MVTCAMAG = space(5)
  w_MVTCOLIS = space(5)
  w_MVTCONTR = space(15)
  w_MVTFICOM = ctod("  /  /  ")
  w_MVTFRAGG = space(1)
  w_MVTINCOM = ctod("  /  /  ")
  w_MVTIPATT = space(1)
  w_MVTIPCON = space(1)
  w_MVTIPDOC = space(5)
  w_MVTIPIMB = space(1)
  w_MVTIPOPE = space(10)
  w_MVTIPORN = space(1)
  w_MVTIPPER = space(1)
  w_MVTIPPR2 = space(2)
  w_MVTIPPRO = space(2)
  w_MVTIPRIG = space(1)
  w_MVTOTENA = 0
  w_MVTOTRIT = 0
  w_MVTRAINT = 0
  w_MVUMSUPP = space(3)
  w_MVUNILOG = space(18)
  w_MVUNIMIS = space(3)
  w_MVVALMAG = 0
  w_MVVALNAZ = space(3)
  w_MVVALRIG = 0
  w_MVVALULT = 0
  w_MVVOCCEN = space(15)
  w_MVLOTMAG = space(5)
  w_MVLOTMAT = space(5)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctod("  /  /  ")
  w_UTDV = ctod("  /  /  ")
  w_MVVOCCEN = space(15)
  w_MVVOCRIC = space(15)
  w_MVROWWEB = 0
  w_CPSERIAL = space(10)
  w_CPIMPPRE = 0
  w_CPIMPFIN = 0
  w_CPIMPCON = 0
  w_CPIMPCAR = 0
  w_CPIMPASS = 0
  w_CPCONPRE = space(15)
  w_CPCONFIN = space(15)
  w_CPCONCON = space(15)
  w_CPCONCAR = space(15)
  w_CPCONASS = space(15)
  w_DRTOTRIT = 0
  w_DRTOTDOC = 0
  w_DRTIPFOR = space(1)
  w_DRSPERIM = 0
  w_DRSOMESC = 0
  w_DRSOGGE = 0
  w_DRSERIAL = space(10)
  w_DRRITENU = 0
  w_DRRIINPS = 0
  w_DRPERRIT = 0
  w_DRPERPRO = 0
  w_DRNSOGGP = 0
  w_DRNSOGGI = 0
  w_DRNONSOG = 0
  w_DRNONSO1 = 0
  w_DRIMPOSTA = 0
  w_DRIMPONI = 0
  w_DRFODPRO = 0
  w_DRESIDUO = 0
  w_DRESIDU1 = 0
  w_DRDAVERS = 0
  w_DRDATREG = ctod("  /  /  ")
  w_DRDATCON = space(1)
  w_DRCODTRI = space(5)
  w_DRCODCAU = space(1)
  w_DRCODBUN = space(3)
  w_RSSERIAL = space(10)
  w_RSNUMRAT = 0
  w_RSMODPAG = space(10)
  w_RSIMPRAT = 0
  w_RSFLSOSP = space(1)
  w_RSFLPROV = space(1)
  w_RSDESRIG = space(50)
  w_RSDATRAT = ctod("  /  /  ")
  w_RSCONCOR = space(25)
  w_RSBANNOS = space(15)
  w_RSBANAPP = space(10)
  w_MTSERRIF = space(10)
  w_MTSERIAL = space(10)
  w_MTROWRIF = 0
  w_MTROWNUM = 0
  w_MTRIFSTO = 0
  w_MTRIFNUM = 0
  w_MTNUMRIF = 0
  w_MTMAGSCA = space(5)
  w_MTMAGCAR = space(5)
  w_MTKEYSAL = space(20)
  w_MTFLSCAR = space(1)
  w_MTFLCARI = space(1)
  w_MTCODUBI = space(20)
  w_MTCODMAT = space(40)
  w_MTCODLOT = space(20)
  w_MT_SALDO = 0
  w_MT__FLAG = space(1)
  w_FLVEBD = space(1)
  w_AZCODPOR = space(1)
  w_PLADOC = space(1)
  w_ASPETT = space(254)
  w_FLORAT = space(1)
  w_COMAG = space(5)
  w_COMAT = space(5)
  w_TPVSRI = space(1)
  w_FLGEFA = space(1)
  w_DESDOC = space(35)
  w_REP = space(1)
  w_FLDATT = space(1)
  w_NUMSCO = 0
  w_FLDTPR = space(1)
  w_ESCL1 = space(1)
  w_ESCL2 = space(1)
  w_ESCL3 = space(1)
  w_ESCL4 = space(1)
  w_ESCL5 = space(1)
  w_FLNSTA = space(1)
  w_FLVSRI = space(1)
  w_FLRISC = space(1)
  w_TPRDES = space(1)
  w_FLCASH = space(1)
  w_FLGCOM = space(1)
  w_FLRIDE = space(1)
  w_FLELAN = space(1)
  w_FLANAL = space(1)
  w_TDFLEXPL = space(1)
  w_CAUCOD = space(1)
  w_CAUPFI = space(1)
  w_FLARCO = space(1)
  w_FLSILI = space(1)
  w_QTADEF = 0
  w_DOCLIS = space(1)
  w_STACL1 = space(1)
  w_TPNSRI = space(1)
  w_DFLPP = space(1)
  w_FLIMAC = space(1)
  w_FLIMPA = space(1)
  w_FLCRIS = space(1)
  w_FLNSRI = space(1)
  w_FLPACK = space(1)
  w_FLMGPR = space(1)
  w_FLMTPR = space(1)
  w_VOCTIP = space(1)
  w_TD_SEGNO = space(1)
  w_PRZVAC = space(1)
  w_ASSCES = space(1)
  w_CAUCES = space(1)
  w_PRZDES = space(1)
  w_FLSTLM = space(1)
  w_NUMALT = space(1)
  w_VALCOM = space(1)
  w_FLCCAU = space(1)
  w_FLQRIO = space(1)
  w_FLPREV = space(1)
  w_CHKTOT = space(1)
  w_FLSPTR = space(1)
  w_FLSPIM = space(1)
  w_MCALST1 = space(1)
  w_MCALSI1 = space(1)
  w_FLBLEV = space(1)
  w_TDFLSCOR = space(1)
  w_MAXLEV = space(1)
  w_LOTDIF = space(1)
  w_FLSPIN = space(1)
  w_BOLDOG = space(1)
  w_CODSTR = space(1)
  w_FLRIAT = space(1)
  w_FLBACA = space(1)
  w_FLPRAT = space(1)
  w_TDFLAPCA = space(1)
  w_TDRICNOM = space(1)
  w_TDCOSEPL = space(1)
  w_TDRIPCON = space(1)
  w_TDNOSTCO = space(1)
  w_FLPDOC = space(1)
  w_CAUCOL = space(1)
  w_VARVAL = space(1)
  w_TESTCASC = space(1)
  w_PARIVA = space(16)
  w_DTOBSO = ctod("  /  /  ")
  w_FLSCOR = space(1)
  w_CODLIN = space(5)
  w_FLFOAG = space(1)
  w_FLRITE = space(1)
  w_TELFAX = space(1)
  w_MAGTER = space(5)
  w_CODESC = space(1)
  w_MCALST2 = space(1)
  w_MCALSI2 = space(1)
  w_ANFLIMBA = space(1)
  w_GRUPRO = space(1)
  w_CATCLI = space(1)
  w_CFCATCON = space(1)
  w_NAZCLF = space(1)
  w_FLGRITE = space(1)
  w_EMAIL1 = space(1)
  w_CATSCC = space(1)
  w_ANCODAG = space(5)
  w_CONCOR = space(25)
  w_XCONORN = space(15)
  w_CATSCC = space(1)
  w_GIORN1 = space(1)
  w_CLFPAG = space(1)
  w_CLFLIS = space(1)
  w_GIORN2 = space(1)
  w_VALCLF = space(1)
  w_CLBOLFAT = space(1)
  w_MESE1 = space(1)
  w_MESE2 = space(1)
  w_GRPCLI = space(1)
  w_PAGMOR = space(1)
  w_CATCOM = space(1)
  w_FLFIDO = space(1)
  w_FLBLVE = space(1)
  w_MAXFID = space(1)
  w_DATMOR = ctod("  /  /  ")
  w_GIOFIS = space(1)
  w_MAXORD = space(1)
  w_CODNAZ = space(1)
  w_IVACLI = space(1)
  w_PARCLF = space(1)
  w_FLGCON = space(1)
  w_FLINTR = space(1)
  w_BANCA = space(1)
  w_FLGCAU = space(1)
  w_NUMCOR = space(1)
  w_ANFLAPC1 = space(1)
  w_PREDEF = space(1)
  w_MCALSI4 = space(1)
  w_CODNAZ1 = space(1)
  w_MCALST4 = space(1)
  w_FLPPRO = space(1)
  w_SIMVAL = space(1)
  w_DECTOT = 0
  w_DECUNI = space(1)
  w_BOLESE = space(1)
  w_BOLSUP = space(1)
  w_BOLCAM = space(1)
  w_BOLARR = space(1)
  w_BOLMIN = space(1)
  w_CAOVAL = space(1)
  w_DATOBSO = space(1)
  w_SIMVAL2 = space(1)
  w_CODLIS = space(5)
  w_VALLIS = space(1)
  w_INILIS = ctod("  /  /  ")
  w_FINLIS = ctod("  /  /  ")
  w_IVALIS = space(1)
  w_QUALIS = 0
  w_SCOLIS = space(1)
  w_PATIPDOC = space(5)
  w_VALINC = space(1)
  w_SPEINC = space(1)
  w_VALIN2 = space(1)
  w_SPEIN2 = space(1)
  w_DTOBSOPA = space(1)
  w_PAGRIC = space(1)
  w_VALINC = space(1)
  w_SPEINC = space(1)
  w_VALIN2 = space(1)
  w_SPEIN2 = space(1)
  w_DTOBSOPA = space(1)
  w_FLN = space(1)
  w_TIPDOC = space(1)
  w_TIPREG = space(1)
  w_CFLPP = space(1)
  w_PARCAU = space(1)
  w_GESRIT = space(1)
  w_DTOBSOBC = space(1)
  w_CONSBF = space(1)
  w_AGEPRO = space(1)
  w_AGSCOPAG = space(1)
  w_AGEPR2 = space(1)
  w_AGSCOPA2 = space(1)
  w_BOLCAU = space(1)
  w_REVCAU = space(1)
  w_BOLBOL = space(1)
  w_COPERIMB = space(1)
  w_COPERINC = space(1)
  w_ALTIVCPA = space(1)
  w_ALTIVGEN = space(1)
  w_ALPERIMB = space(1)
  w_ALPERTRA = space(1)
  w_BOLIVE = space(1)
  w_PERIVE = 0
  w_COPERTRA = space(1)
  w_PEIIMB = space(1)
  w_PEICAU = space(1)
  w_PEIINC = space(1)
  w_OKRIG = .f.
  w_OMAG = space(5)
  w_OMAT = space(5)
  w_OCEN = space(15)
  w_OCOM = space(15)
  w_OATT = space(15)
  w_OK = .f.
  w_UNMIS3 = space(1)
  w_OPERA3 = space(1)
  w_MOLTI3 = space(1)
  w_TIPCO3 = space(1)
  w_TPCON3 = space(1)
  w_UNMIS1 = space(1)
  w_MOLTIP = space(1)
  w_UNMIS2 = space(1)
  w_OPERAT = space(1)
  w_ARTDIS = space(1)
  w_VOCRIC = space(1)
  w_TIPCO1 = space(1)
  w_TIPCO2 = space(1)
  w_TPCON1 = space(1)
  w_TPCON2 = space(1)
  w_FLSERG = space(1)
  w_FLUSEP = space(1)
  w_FLSERA = space(1)
  w_FLLOTT = space(1)
  w_MAGPRE = space(1)
  w_VOCCEN = space(1)
  w_FLDISC = space(1)
  w_GESMAT = space(1)
  w_ARTPRO = space(1)
  w_DISLOT = space(1)
  w_FLCESP = space(1)
  w_CODDIS = space(1)
  w_CODCEN = space(1)
  w_FLESIM = space(1)
  w_PRESTA = space(1)
  w_ARFLAPCA = space(1)
  w_ARRIPCON = space(1)
  w_PREZUM = space(1)
  w_ARUTISER = space(1)
  w_ARDATINT = space(1)
  w_PERIVA = space(1)
  w_PERIND = space(1)
  w_DATOBSO = space(1)
  w_BOLIVA = space(1)
  w_DTOBSO = space(1)
  w_FLCOMM = space(1)
  w_FLAVA1 = space(1)
  w_FLCASC = space(1)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_FLRISE = space(1)
  w_MTCARI = space(1)
  w_FLUBIC = space(1)
  w_F2CASC = space(1)
  w_F2ORDI = space(1)
  w_F2IMPE = space(1)
  w_F2RISE = space(1)
  w_F2UBIC = space(1)
  w_FLFRAZ = space(1)
  w_DUR_ORE = space(1)
  w_CHKTEMP = space(1)
  w_MODUM2 = space(1)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_QTAPER = space(1)
  w_QTRPER = space(1)
  w_ULTCAR = ctod("  /  /  ")
  w_ULTSCA = ctod("  /  /  ")
  w_VALUCA = space(1)
  w_CODVAA = space(1)
  w_Q2APER = space(1)
  w_Q2RPER = space(1)
  w_CODCOS = space(5)
  w_LIPREZZO = 0
  w_VALUCA = 0
  w_CODVAA = space(3)
  w_PROG = space(3)
  w_LIPREZZO = 0
  w_SCOLIS = space(1)
  w_CALPRZ = 0
  w_LISCON = 0
  w_PERIVA = 0
  w_APPO = space(12)
  w_IVACON = space(1)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_CLUNIMIS = space(3)
  w_PROPINS = 0
  w_PROPATT = 0
  w_NUMPROP = 0
  w_bSaveCurRec = .f.
  w_ROWMAT = 0
  w_SEARCH_IDX = 0
  w_IDX_OBJ = 0
  w_FOUND = .f.
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione documento da classe
    this.w_PADRE = IIF(VARTYPE(This.oParentObject)="O", This.oParentObject, this.pPADRE)
    this.w_RETVAL = ""
    if this.w_PADRE.w_DOC_DETT.Reccount()=0
      if Isalt()
        this.w_RETVAL = Ah_msgformat("deve essere presente almeno una prestazione")
      else
        this.w_RETVAL = Ah_msgformat("Impossibile generare documento senza righe di dettaglio")
      endif
      i_retcode = 'stop'
      i_retval = this.w_RETVAL
      return
    endif
    * --- Evito l'aggiornamento della gestione chiamante
    *     (in questo caso l'oggetto che contiene il documento da creare
    *     le cui variabili non necessitano di aggiornamenti al termine della routine)
    this.bUpdateParentObject = .F.
    * --- Valorizzazione variabili locali documento
    this.w_PADRE.SetLocalValues(This)     
    * --- Valorizzazione variabili oggetto documento da variabili locali
    this.w_PADRE.SetObjectValues(This)     
    this.w_MVGENPOS = .F.
    this.w_MVDATREG = iif(empty(this.w_MVDATREG),i_DATSYS,this.w_MVDATREG)
    this.w_MVDATDOC = iif(empty(this.w_MVDATDOC),i_DATSYS,this.w_MVDATDOC)
    this.w_MVANNDOC = iif(empty(this.w_MVANNDOC),STR(YEAR(this.w_MVDATDOC), 4, 0),this.w_MVANNDOC)
    this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
    DIMENSION ArrWhere(1,2)
    ArrWhere(1,1) = "AZCODAZI"
    ArrWhere(1,2) = i_CODAZI
    this.w_FLVEBD = this.w_PADRE.ReadTable( "AZIENDA" , "AZFLVEBD" , @ArrWhere )
    this.w_AZCODPOR = this.w_PADRE.ReadTable( "AZIENDA" , "AZCODPOR" , @ArrWhere )
    this.w_PLADOC = this.w_PADRE.ReadTable( "AZIENDA" , "AZPLADOC" , @ArrWhere )
    Release ArrWhere
    this.w_MVDATPLA = IIF(this.w_PLADOC="S",this.w_MVDATDOC,this.w_MVDATREG)
    if !EMPTY(NVL( this.w_MVTIPDOC, " "))
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "TDTIPDOC"
      ArrWhere(1,2) = this.w_MVTIPDOC
      this.w_ASPETT = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDASPETT" , @ArrWhere )
      this.w_MVCLADOC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCATDOC" , @ArrWhere )
      this.w_MVFLVEAC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLVEAC" , @ArrWhere )
      this.w_MVFLACCO = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLACCO" , @ArrWhere )
      this.w_PRODOC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDPRODOC" , @ArrWhere )
      this.w_FLPDOC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLPDOC" , @ArrWhere )
      this.w_MVALFDOC = IIF(EMPTY(this.w_MVALFDOC), this.w_PADRE.ReadTable( "TIP_DOCU" , "TDALFDOC" , @ArrWhere ), this.w_MVALFDOC)
      this.w_MVCAUCON = IIF(EMPTY(this.w_MVCAUCON),this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCAUCON" , @ArrWhere ),this.w_MVCAUCON)
      this.w_MVTCAMAG = IIF(EMPTY(this.w_MVTCAMAG),this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCAUMAG" , @ArrWhere ),this.w_MVTCAMAG)
      this.w_FLORAT = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLORAT" , @ArrWhere )
      this.w_COMAG = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCODMAG" , @ArrWhere )
      this.w_COMAT = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCODMAT" , @ArrWhere )
      this.w_MVFLINTE = IIF(EMPTY(this.w_MVFLINTE),this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLINTE" , @ArrWhere ),this.w_MVFLINTE)
      this.w_FLGEFA = this.w_PADRE.ReadTable( "TIP_DOCU" , "TFFLGEFA" , @ArrWhere )
      this.w_DESDOC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDDESDOC" , @ArrWhere )
      this.w_REP = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDPRGSTA" , @ArrWhere )
      this.w_FLDATT = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLDATT" , @ArrWhere )
      this.w_NUMSCO = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDNUMSCO" , @ArrWhere )
      this.w_FLAGEN = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLPROV" , @ArrWhere )
      this.w_FLDTPR = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLDTPR" , @ArrWhere )
      this.w_ESCL1 = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDESCCL1" , @ArrWhere )
      this.w_ESCL2 = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDESCCL2" , @ArrWhere )
      this.w_ESCL3 = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDESCCL3" , @ArrWhere )
      this.w_ESCL4 = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDESCCL4" , @ArrWhere )
      this.w_ESCL5 = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDESCCL5" , @ArrWhere )
      this.w_FLVSRI = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLVSRI" , @ArrWhere )
      this.w_FLNSTA = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLNSTA" , @ArrWhere )
      this.w_TPVSRI = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDTPVDOC" , @ArrWhere )
      this.w_FLNSRI = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLNSRI" , @ArrWhere )
      this.w_FLCRIS = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLCRIS" , @ArrWhere )
      this.w_FLIMPA = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLIMPA" , @ArrWhere )
      this.w_FLIMAC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLIMAC" , @ArrWhere )
      this.w_MVALFEST = IIF(EMPTY(this.w_MVALFEST),this.w_PADRE.ReadTable( "TIP_DOCU" , "TDSERPRO" , @ArrWhere ),this.w_MVALFEST)
      this.w_DFLPP = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLPPRO" , @ArrWhere )
      this.w_FLPACK = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLPACK" , @ArrWhere )
      this.w_TPNSRI = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDTPNDOC" , @ArrWhere )
      this.w_STACL1 = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDSTACL1" , @ArrWhere )
      this.w_DOCLIS = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCODLIS" , @ArrWhere )
      this.w_QTADEF = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDQTADEF" , @ArrWhere )
      this.w_FLSILI = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLSILI" , @ArrWhere )
      this.w_FLARCO = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLARCO" , @ArrWhere )
      this.w_CAUPFI = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCAUPFI" , @ArrWhere )
      this.w_CAUCOD = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCAUCOD" , @ArrWhere )
      this.w_TDFLEXPL = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLEXPL" , @ArrWhere )
      this.w_FLANAL = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLANAL" , @ArrWhere )
      this.w_FLELAN = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLELAN" , @ArrWhere )
      this.w_FLRIDE = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLRIDE" , @ArrWhere )
      this.w_FLGCOM = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLCOMM" , @ArrWhere )
      this.w_FLCASH = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLCASH" , @ArrWhere )
      this.w_TPRDES = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDTPRDES" , @ArrWhere )
      this.w_TDNOSTCO = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDNOSTCO" , @ArrWhere )
      this.w_TDRIPCON = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDRIPCON" , @ArrWhere )
      this.w_TDCOSEPL = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCOSEPL" , @ArrWhere )
      this.w_TDRICNOM = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDRICNOM" , @ArrWhere )
      this.w_TDFLAPCA = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLAPCA" , @ArrWhere )
      this.w_FLPRAT = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLPRAT" , @ArrWhere )
      this.w_FLBACA = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLBACA" , @ArrWhere )
      this.w_MVFLRTRA = IIF(EMPTY(this.w_MVFLRTRA),this.w_PADRE.ReadTable( "TIP_DOCU" , "TDRIPTRA" , @ArrWhere ),this.w_MVFLRTRA)
      this.w_MVFLRIMB = IIF(EMPTY(this.w_MVFLRIMB),this.w_PADRE.ReadTable( "TIP_DOCU" , "TDRIPIMB" , @ArrWhere ),this.w_MVFLRIMB)
      this.w_MVFLRINC = IIF(EMPTY(this.w_MVFLRINC),this.w_PADRE.ReadTable( "TIP_DOCU" , "TDRIPINC" , @ArrWhere ),this.w_MVFLRINC)
      this.w_FLRIAT = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLRIAT" , @ArrWhere )
      this.w_CODSTR = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCODSTR" , @ArrWhere )
      this.w_MVTIPIMB = IIF(EMPTY(this.w_MVTIPIMB),this.w_PADRE.ReadTable( "TIP_DOCU" , "TDTIPIMB" , @ArrWhere ),this.w_MVTIPIMB)
      this.w_BOLDOG = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDBOLDOG" , @ArrWhere )
      this.w_FLSPIN = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLSPIN" , @ArrWhere )
      this.w_LOTDIF = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDLOTDIF" , @ArrWhere )
      this.w_MAXLEV = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDMAXLEV" , @ArrWhere )
      this.w_TDFLSCOR = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLSCOR" , @ArrWhere )
      this.w_FLBLEV = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLBLEV" , @ArrWhere )
      this.w_MCALSI1 = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDMCALSI" , @ArrWhere )
      this.w_MCALST1 = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDMCALST" , @ArrWhere )
      this.w_FLSPIM = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLSPIM" , @ArrWhere )
      this.w_FLSPTR = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLSPTR" , @ArrWhere )
      this.w_CHKTOT = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCHKTOT" , @ArrWhere )
      this.w_FLPREV = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLPREV" , @ArrWhere )
      this.w_FLQRIO = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLQRIO" , @ArrWhere )
      this.w_FLCCAU = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLCCAU" , @ArrWhere )
      this.w_VALCOM = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDVALCOM" , @ArrWhere )
      this.w_NUMALT = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDPROALT" , @ArrWhere )
      this.w_FLSTLM = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLSTLM" , @ArrWhere )
      this.w_PRZDES = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDPRZDES" , @ArrWhere )
      this.w_CAUCES = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCAUCES" , @ArrWhere )
      this.w_ASSCES = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDASSCES" , @ArrWhere )
      this.w_PRZVAC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDPRZVAC" , @ArrWhere )
      this.w_TD_SEGNO = this.w_PADRE.ReadTable( "TIP_DOCU" , "TD_SEGNO" , @ArrWhere )
      this.w_VOCTIP = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDVOCECR" , @ArrWhere )
      this.w_FLMTPR = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLMTPR" , @ArrWhere )
      this.w_FLMGPR = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLMGPR" , @ArrWhere )
      this.w_FLRISC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLRISC" , @ArrWhere )
      this.w_MVTFRAGG = IIF(EMPTY(Nvl(this.w_MVTFRAGG," ")),this.w_PADRE.ReadTable( "TIP_DOCU" , "TFFLRAGG" , @ArrWhere ),this.w_MVTFRAGG)
      Release ArrWhere
    endif
    * --- Valorizzo l'anno di riferimento ed il mese solo se la causale documento
    *     vale fattura/nota di credito
    if this.w_MVCLADOC $ "FA-NC-FC"
      this.w_MV__ANNO = YEAR(this.w_MVDATDOC)
      this.w_MV__MESE = MONTH(this.w_MVDATDOC)
    endif
    if Not Empty(this.w_MVTCAMAG)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CMCODICE"
      ArrWhere(1,2) = this.w_MVTCAMAG
      this.w_CAUCOL = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMCAUCOL" , @ArrWhere )
      this.w_VARVAL = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMVARVAL" , @ArrWhere )
      this.w_TESTCASC = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLCASC" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVTIPCON = IIF(this.w_MVFLINTE $ "C-F" ,this.w_MVFLINTE," ")
    this.w_MVCODCON = iif(Nvl(this.w_MVTIPCON," ") $ "C-F" ,this.w_MVCODCON,Space(15))
    if !EMPTY(NVL( this.w_MVCODCON, " ")) and !EMPTY(NVL( this.w_MVTIPCON, " "))
      DIMENSION ArrWhere(2,2)
      ArrWhere(1,1) = "ANTIPCON"
      ArrWhere(1,2) = this.w_MVTIPCON
      ArrWhere(2,1) = "ANCODICE"
      ArrWhere(2,2) = this.w_MVCODCON
      this.w_PARIVA = this.w_PADRE.ReadTable( "CONTI" , "ANPARIVA" , @ArrWhere )
      this.w_DTOBSO = this.w_PADRE.ReadTable( "CONTI" , "ANDTOBSO" , @ArrWhere )
      this.w_FLSCOR = this.w_PADRE.ReadTable( "CONTI" , "ANSCORPO" , @ArrWhere )
      this.w_MVCONCON = IIF(EMPTY(this.w_MVCONCON),this.w_PADRE.ReadTable( "CONTI" , "ANCONCON" , @ArrWhere ),this.w_MVCONCON)
      this.w_CODLIN = this.w_PADRE.ReadTable( "CONTI" , "ANCODLIN" , @ArrWhere )
      this.w_FLFOAG = this.w_PADRE.ReadTable( "CONTI" , "ANTIPCLF" , @ArrWhere )
      this.w_FLRITE = this.w_PADRE.ReadTable( "CONTI" , "ANRITENU" , @ArrWhere )
      this.w_TELFAX = this.w_PADRE.ReadTable( "CONTI" , "ANTELFAX" , @ArrWhere )
      this.w_EMAIL1 = this.w_PADRE.ReadTable( "CONTI" , "AN_EMAIL" , @ArrWhere )
      this.w_MAGTER = this.w_PADRE.ReadTable( "CONTI" , "ANMAGTER" , @ArrWhere )
      this.w_CODESC = this.w_PADRE.ReadTable( "CONTI" , "ANCODESC" , @ArrWhere )
      this.w_MCALST2 = this.w_PADRE.ReadTable( "CONTI" , "ANMCALST" , @ArrWhere )
      this.w_MCALSI2 = this.w_PADRE.ReadTable( "CONTI" , "ANMCALSI" , @ArrWhere )
      this.w_MVCODORN = IIF(EMPTY(this.w_MVCODORN),Nvl(this.w_PADRE.ReadTable( "CONTI" , "ANCODORN" , @ArrWhere ),Space(15)),this.w_MVCODORN)
      this.w_ANFLIMBA = this.w_PADRE.ReadTable( "CONTI" , "ANFLIMBA" , @ArrWhere )
      this.w_GRUPRO = this.w_PADRE.ReadTable( "CONTI" , "ANGRUPRO" , @ArrWhere )
      this.w_CATCLI = this.w_PADRE.ReadTable( "CONTI" , "ANCATSCM" , @ArrWhere )
      this.w_CFCATCON = this.w_PADRE.ReadTable( "CONTI" , "ANCATCON" , @ArrWhere )
      this.w_NAZCLF = this.w_PADRE.ReadTable( "CONTI" , "ANNAZION" , @ArrWhere )
      this.w_FLGRITE = this.w_PADRE.ReadTable( "CONTI" , "ANFLRITE" , @ArrWhere )
      this.w_CATSCC = this.w_PADRE.ReadTable( "CONTI" , "ANCATSCM" , @ArrWhere )
      this.w_ANCODAG = this.w_PADRE.ReadTable( "CONTI" , "ANCODAG1" , @ArrWhere )
      this.w_CONCOR = this.w_PADRE.ReadTable( "CONTI" , "ANNUMCOR" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVCODAGE = iif(empty(Nvl(this.w_MVCODAGE,Space(5))),this.w_ANCODAG,this.w_MVCODAGE)
    this.w_MVNUMCOR = iif(empty(Nvl(this.w_MVNUMCOR,Space(25))),this.w_CONCOR,this.w_MVNUMCOR)
    this.w_MVTIPORN = iif(empty(this.w_MVTIPORN),this.w_MVTIPCON,this.w_MVTIPORN)
    this.w_XCONORN = IIF(Not Empty(this.w_MVCODORN) And g_XCONDI = "S",this.w_MVCODORN,this.w_MVCODCON)
    if !EMPTY(NVL( this.w_XCONORN, " ")) and !EMPTY(NVL( this.w_MVTIPORN, " "))
      DIMENSION ArrWhere(2,2)
      ArrWhere(1,1) = "ANTIPCON"
      ArrWhere(1,2) = this.w_MVTIPORN
      ArrWhere(2,1) = "ANCODICE"
      ArrWhere(2,2) = this.w_XCONORN
      this.w_ANFLAPC1 = this.w_PADRE.ReadTable( "CONTI" , "ANFLAPCA" , @ArrWhere )
      this.w_NUMCOR = this.w_PADRE.ReadTable( "CONTI" , "ANNUMCOR" , @ArrWhere )
      this.w_FLGCAU = this.w_PADRE.ReadTable( "CONTI" , "ANFLGCAU" , @ArrWhere )
      this.w_BANCA = this.w_PADRE.ReadTable( "CONTI" , "ANCODBAN" , @ArrWhere )
      this.w_FLINTR = this.w_PADRE.ReadTable( "CONTI" , "AFFLINTR" , @ArrWhere )
      this.w_FLGCON = this.w_PADRE.ReadTable( "CONTI" , "ANFLGCON" , @ArrWhere )
      this.w_PARCLF = this.w_PADRE.ReadTable( "CONTI" , "ANPARTSN" , @ArrWhere )
      this.w_IVACLI = this.w_PADRE.ReadTable( "CONTI" , "ANCODIVA" , @ArrWhere )
      this.w_CODNAZ = this.w_PADRE.ReadTable( "CONTI" , "ANNAZION" , @ArrWhere )
      this.w_MAXORD = this.w_PADRE.ReadTable( "CONTI" , "ANMAXORD" , @ArrWhere )
      this.w_GIOFIS = this.w_PADRE.ReadTable( "CONTI" , "ANGIOFIS" , @ArrWhere )
      this.w_DATMOR = this.w_PADRE.ReadTable( "CONTI" , "ANDATMOR" , @ArrWhere )
      this.w_MAXFID = this.w_PADRE.ReadTable( "CONTI" , "ANVALFID" , @ArrWhere )
      this.w_FLBLVE = this.w_PADRE.ReadTable( "CONTI" , "ANFLBLVE" , @ArrWhere )
      this.w_FLFIDO = this.w_PADRE.ReadTable( "CONTI" , "ANFLFIDO" , @ArrWhere )
      this.w_MVCODBA2 = this.w_PADRE.ReadTable( "CONTI" , "ANCODBA2" , @ArrWhere )
      this.w_CATCOM = this.w_PADRE.ReadTable( "CONTI" , "ANCATCOM" , @ArrWhere )
      this.w_PAGMOR = this.w_PADRE.ReadTable( "CONTI" , "ANPAGPAR" , @ArrWhere )
      this.w_GRPCLI = this.w_PADRE.ReadTable( "CONTI" , "ANGRUPRO" , @ArrWhere )
      this.w_MESE2 = this.w_PADRE.ReadTable( "CONTI" , "AN2MESCL" , @ArrWhere )
      this.w_MESE1 = this.w_PADRE.ReadTable( "CONTI" , "AN1MESCL" , @ArrWhere )
      this.w_CLBOLFAT = this.w_PADRE.ReadTable( "CONTI" , "ANBOLFAT" , @ArrWhere )
      this.w_VALCLF = this.w_PADRE.ReadTable( "CONTI" , "ANCODVAL" , @ArrWhere )
      this.w_MVCODBAN = IIF(EMPTY(this.w_MVCODBAN),Nvl(this.w_PADRE.ReadTable( "CONTI" , "ANCODBAN" , @ArrWhere ),Space(10)),this.w_MVCODBAN)
      this.w_MVSCOCL2 = IIF(EMPTY(this.w_MVSCOCL2),Nvl(this.w_PADRE.ReadTable( "CONTI" , "AN2SCONT" , @ArrWhere ),0),this.w_MVSCOCL2)
      this.w_MVSCOCL1 = IIF(EMPTY(this.w_MVSCOCL1),Nvl(this.w_PADRE.ReadTable( "CONTI" , "AN1SCONT" , @ArrWhere ),0),this.w_MVSCOCL1)
      this.w_GIORN2 = this.w_PADRE.ReadTable( "CONTI" , "ANGIOSC2" , @ArrWhere )
      this.w_CLFLIS = this.w_PADRE.ReadTable( "CONTI" , "ANNUMLIS" , @ArrWhere )
      this.w_CLFPAG = this.w_PADRE.ReadTable( "CONTI" , "ANCODPAG" , @ArrWhere )
      this.w_GIORN1 = this.w_PADRE.ReadTable( "CONTI" , "ANGIOSC1" , @ArrWhere )
      this.w_CATSCC = this.w_PADRE.ReadTable( "CONTI" , "ANCATSCM" , @ArrWhere )
      this.w_MVNAZPRO = this.w_CODNAZ
      Release ArrWhere
    endif
    if Not Empty(nvl(this.w_MVCODDES,space(5)))
      DIMENSION ArrWhere(3,2)
      ArrWhere(1,1) = "DDCODICE"
      ArrWhere(1,2) = this.w_MVCODCON
      ArrWhere(2,1) = "DDTIPCON"
      ArrWhere(2,2) = this.w_MVTIPCON
      ArrWhere(3,1) = "DDCODDES"
      ArrWhere(3,2) = this.w_MVCODDES
      this.w_MVCODVET = this.w_PADRE.ReadTable( "DES_DIVE" , "DDCODVET" , @ArrWhere )
      this.w_MVCODPOR = this.w_PADRE.ReadTable( "DES_DIVE" , "DDCODPOR" , @ArrWhere )
      this.w_MVCODSPE = this.w_PADRE.ReadTable( "DES_DIVE" , "DDCODSPE" , @ArrWhere )
      this.w_PREDEF = this.w_PADRE.ReadTable( "DES_DIVE" , "DDPREDEF" , @ArrWhere )
      this.w_CODNAZ1 = this.w_PADRE.ReadTable( "DES_DIVE" , "DDCODNAZ" , @ArrWhere )
      this.w_MCALSI4 = this.w_PADRE.ReadTable( "DES_DIVE" , "DDMCCODI" , @ArrWhere )
      this.w_MCALST4 = this.w_PADRE.ReadTable( "DES_DIVE" , "DDMCCODT" , @ArrWhere )
    endif
    this.w_CLFPAG = iif(Empty(this.w_CLFPAG),g_SAPAGA,this.w_CLFPAG)
    this.w_FLPPRO = IIF(this.w_DFLPP="P" OR this.w_MVCLADOC $ "FA-NC", this.w_CFLPP, this.w_DFLPP)
    this.w_MVANNPRO = iif(empty(this.w_MVANNPRO),CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO),this.w_MVANNPRO)
    this.w_MVPRD = iif(this.w_FLPDOC="S",iif(this.w_MVCLADOC="DT","DV",iif(this.w_MVCLADOC="DI","IV",this.w_PRODOC)),this.w_PRODOC)
    this.w_MVPRP = IIF(this.w_TIPREG="A" AND this.w_MVCLADOC $ "FA-NC", "AC", "NN")
    this.w_MVFLSCOR = IIF(this.w_MVCLADOC="RF","S",IIF( this.w_TDFLSCOR="I", this.w_FLSCOR, this.w_TDFLSCOR ))
    this.w_MVCODVAL = IIF(EMPTY(Nvl(this.w_MVCODVAL," ")),IIF(EMPTY(Nvl(this.w_VALCLF," ")), g_PERVAL, this.w_VALCLF),this.w_MVCODVAL)
    if !EMPTY(NVL( this.w_MVCODVAL, " "))
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "VACODVAL"
      ArrWhere(1,2) = this.w_MVCODVAL
      this.w_SIMVAL = this.w_PADRE.ReadTable( "VALUTE" , "VASIMVAL" , @ArrWhere )
      this.w_DECTOT = this.w_PADRE.ReadTable( "VALUTE" , "VADECTOT" , @ArrWhere )
      this.w_DECUNI = this.w_PADRE.ReadTable( "VALUTE" , "VADECUNI" , @ArrWhere )
      this.w_BOLESE = this.w_PADRE.ReadTable( "VALUTE" , "VABOLESE" , @ArrWhere )
      this.w_BOLSUP = this.w_PADRE.ReadTable( "VALUTE" , "VABOLSUP" , @ArrWhere )
      this.w_BOLCAM = this.w_PADRE.ReadTable( "VALUTE" , "VABOLCAM" , @ArrWhere )
      this.w_BOLARR = this.w_PADRE.ReadTable( "VALUTE" , "VABOLARR" , @ArrWhere )
      this.w_BOLMIN = this.w_PADRE.ReadTable( "VALUTE" , "VABOLMIM" , @ArrWhere )
      this.w_CAOVAL = this.w_PADRE.ReadTable( "VALUTE" , "VACAOVAL" , @ArrWhere )
      this.w_DATOBSO = this.w_PADRE.ReadTable( "VALUTE" , "VADTOBSO" , @ArrWhere )
      this.w_SIMVAL2 = this.w_SIMVAL
      Release ArrWhere
    endif
    this.w_MVTCOLIS = IIF(EMPTY(this.w_MVTCOLIS),IIF(EMPTY(this.w_CLFLIS), IIF((EMPTY(this.w_DOCLIS) and IsAlt()),this.w_CODLIS,this.w_DOCLIS), this.w_CLFLIS),this.w_MVTCOLIS)
    if NOT EMPTY(nvl(this.w_MVTCOLIS,space(5)))
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "LSCODLIS"
      ArrWhere(1,2) = this.w_MVTCOLIS
      this.w_SCOLIS = this.w_PADRE.ReadTable( "LISTINI" , "LSFLSCON" , @ArrWhere )
      this.w_QUALIS = this.w_PADRE.ReadTable( "LISTINI" , "LSQUANTI" , @ArrWhere )
      this.w_IVALIS = this.w_PADRE.ReadTable( "LISTINI" , "LSIVALIS" , @ArrWhere )
      this.w_FINLIS = this.w_PADRE.ReadTable( "LISTINI" , "LSDTOBSO" , @ArrWhere )
      this.w_VALLIS = this.w_PADRE.ReadTable( "LISTINI" , "LSVALLIS" , @ArrWhere )
      this.w_INILIS = this.w_PADRE.ReadTable( "LISTINI" , "LSDTINVA" , @ArrWhere )
      Release ArrWhere
      * --- Se la valuta dell'intestatario del documento non � coerente con quella 
      *     del listino, lo sbianco e di conseguenza tutti i link
      if this.w_MVCODVAL<>this.w_VALLIS
        this.w_MVTCOLIS = space(5)
        this.w_VALLIS = ""
        this.w_INILIS = cp_todate("  -  -  ")
        this.w_FINLIS = cp_todate("  -  -  ")
        this.w_IVALIS = ""
        this.w_QUALIS = 0
        this.w_SCOLIS = ""
      endif
    endif
    this.w_MVCODESE = IIF(EMPTY(this.w_MVCODESE),CALCESER(IIF(this.w_MVCLADOC$"DT-DI" And this.w_TESTCASC="+" And Empty(this.w_CAUCOL),this.w_MVDATREG,this.w_MVDATDOC), "    "),this.w_MVCODESE)
    if Not empty(nvl(this.w_MVCODESE,space(4)))
      DIMENSION ArrWhere(2,2)
      ArrWhere(1,1) = "ESCODAZI"
      ArrWhere(1,2) = i_CODAZI
      ArrWhere(2,1) = "ESCODESE"
      ArrWhere(2,2) = this.w_MVCODESE
      this.w_MVVALNAZ = this.w_PADRE.ReadTable( "ESERCIZI" , "ESVALNAZ" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_PATIPDOC = this.w_MVTIPDOC
    if Not empty(this.w_PATIPDOC)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "RFCAUDOC"
      ArrWhere(1,2) = this.w_PATIPDOC
      this.w_PAGRIC = this.w_PADRE.ReadTable( "RIFI_CADO" , "RFPAGRIC" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVCODPAG = IIF(empty(this.w_MVCODPAG),IIF(this.w_MVCLADOC="RF", this.w_PAGRIC, IIF(this.w_DATMOR>=this.w_MVDATDOC AND NOT EMPTY(this.w_PAGMOR), this.w_PAGMOR, this.w_CLFPAG)),this.w_MVCODPAG)
    if Not empty(nvl(this.w_MVCODPAG, space(5)))
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "PACODICE"
      ArrWhere(1,2) = this.w_MVCODPAG
      this.w_MVSCOPAG = iif(Empty(this.w_MVSCOPAG),Nvl(this.w_PADRE.ReadTable( "PAG_AMEN" , "PASCONTO" , @ArrWhere ),0),this.w_MVSCOPAG)
      this.w_VALINC = this.w_PADRE.ReadTable( "PAG_AMEN" , "PAVALINC" , @ArrWhere )
      this.w_SPEINC = this.w_PADRE.ReadTable( "PAG_AMEN" , "PASPEINC" , @ArrWhere )
      this.w_VALIN2 = this.w_PADRE.ReadTable( "PAG_AMEN" , "PAVALIN2" , @ArrWhere )
      this.w_SPEIN2 = this.w_PADRE.ReadTable( "PAG_AMEN" , "PASPEIN2" , @ArrWhere )
      this.w_DTOBSOPA = this.w_PADRE.ReadTable( "PAG_AMEN" , "PADTOBSO" , @ArrWhere )
      Release ArrWhere
    endif
    if Not Empty(this.w_MVCAUCON)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CCCODICE"
      ArrWhere(1,2) = this.w_MVCAUCON
      this.w_GESRIT = this.w_PADRE.ReadTable( "CAU_CONT" , "CCGESRIT" , @ArrWhere )
      this.w_PARCAU = this.w_PADRE.ReadTable( "CAU_CONT" , "CCFLPART" , @ArrWhere )
      this.w_FLN = this.w_PADRE.ReadTable( "CAU_CONT" , "CCFLANAL" , @ArrWhere )
      this.w_TIPDOC = this.w_PADRE.ReadTable( "CAU_CONT" , "CCTIPDOC" , @ArrWhere )
      this.w_TIPREG = this.w_PADRE.ReadTable( "CAU_CONT" , "CCTIPREG" , @ArrWhere )
      this.w_CFLPP = this.w_PADRE.ReadTable( "CAU_CONT" , "CCFLPPRO" , @ArrWhere )
      Release ArrWhere
    endif
    if Not Empty(this.w_MVCAUCON)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "BACODBAN"
      ArrWhere(1,2) = this.w_MVCODBA2
      this.w_DTOBSOBC = this.w_PADRE.ReadTable( "COC_MAST" , "BADTOBSO" , @ArrWhere )
      this.w_CONSBF = this.w_PADRE.ReadTable( "COC_MAST" , "BACONSBF" , @ArrWhere )
      Release ArrWhere
    endif
    if Not Empty(this.w_MVCODAGE)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "AGCODAGE"
      ArrWhere(1,2) = this.w_MVCODAGE
      this.w_MVCODAG2 = this.w_PADRE.ReadTable( "AGENTI" , "AGCZOAGE" , @ArrWhere )
      this.w_AGEPRO = this.w_PADRE.ReadTable( "AGENTI" , "AGCATPRO" , @ArrWhere )
      this.w_AGSCOPAG = this.w_PADRE.ReadTable( "AGENTI" , "AGSCOPAG" , @ArrWhere )
      Release ArrWhere
    endif
    if Not Empty(this.w_MVCODAG2)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "AGCODAGE"
      ArrWhere(1,2) = this.w_MVCODAG2
      this.w_AGEPR2 = this.w_PADRE.ReadTable( "AGENTI" , "AGCATPRO" , @ArrWhere )
      this.w_AGSCOPA2 = this.w_PADRE.ReadTable( "AGENTI" , "AGSCOPAG" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVIVACAU = iif(empty(this.w_MVIVACAU),g_COICAU,this.w_MVIVACAU)
    if Not Empty(this.w_MVIVACAU)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_MVIVACAU
      this.w_BOLCAU = this.w_PADRE.ReadTable( "VOCIIVA" , "IVBOLIVA" , @ArrWhere )
      this.w_REVCAU = this.w_PADRE.ReadTable( "VOCIIVA" , "IVREVCHA" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVIVABOL = iif(empty(this.w_MVIVABOL),g_COIBOL,this.w_MVIVABOL)
    if Not Empty(this.w_MVIVABOL)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_MVIVABOL
      this.w_BOLBOL = this.w_PADRE.ReadTable( "VOCIIVA" , "IVBOLIVA" , @ArrWhere )
      Release ArrWhere
    endif
    if Not Empty(g_COIIMB)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = g_COIIMB
      this.w_COPERIMB = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    if Not Empty(g_COIINC)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = g_COIINC
      this.w_COPERINC = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    if ISALT()
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "PACODAZI"
      ArrWhere(1,2) = i_CODAZI
      this.w_ALTIVCPA = this.w_PADRE.ReadTable( "PAR_ALTE" , "PACASPRE" , @ArrWhere )
      this.w_ALTIVGEN = this.w_PADRE.ReadTable( "PAR_ALTE" , "PASPEGEN" , @ArrWhere )
      Release ArrWhere
    else
      this.w_ALTIVCPA = g_COITRA
      this.w_ALTIVGEN = g_COIIMB
    endif
    if Not Empty(this.w_ALTIVGEN)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_ALTIVGEN
      this.w_ALPERIMB = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    if Not Empty(this.w_ALTIVCPA)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_ALTIVCPA
      this.w_ALPERTRA = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVCODIVE = this.w_IVACLI
    if Not Empty(this.w_MVCODIVE)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_MVCODIVE
      this.w_BOLIVE = this.w_PADRE.ReadTable( "VOCIIVA" , "IVBOLIVA" , @ArrWhere )
      this.w_PERIVE = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVIVATRA = iif(empty(this.w_MVIVATRA),g_COITRA,this.w_MVIVATRA)
    if Not Empty(this.w_MVIVATRA)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_MVIVATRA
      this.w_COPERTRA = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVIVATRA = IIF(NOT EMPTY(this.w_MVCODIVE), IIF(this.w_PERIVE<IIF(IsAlt(),this.w_ALPERTRA,this.w_COPERTRA),this.w_MVCODIVE,this.w_ALTIVCPA), EVL(this.w_ALTIVCPA, this.w_MVIVATRA))
    this.w_MVIVAIMB = iif(empty(this.w_MVIVAIMB),g_COIIMB,this.w_MVIVAIMB)
    if Not Empty(this.w_MVIVAIMB)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_MVIVAIMB
      this.w_PEIIMB = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVIVACAU = iif(empty(this.w_MVIVACAU),g_COICAU,this.w_MVIVACAU)
    if Not Empty(this.w_MVIVACAU)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_MVIVACAU
      this.w_PEICAU = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVIVAINC = iif(empty(this.w_MVIVAINC),g_COIINC,this.w_MVIVAINC)
    if Not Empty(this.w_MVIVAINC)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "IVCODIVA"
      ArrWhere(1,2) = this.w_MVIVAINC
      this.w_PEIINC = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_MVDATEST = iif(empty(this.w_MVDATEST) and this.w_MVFLVEAC="A" AND this.w_MVCLADOC <> "OR" ,i_DATSYS,this.w_MVDATEST)
    this.w_MVCAOVAL = IIF(this.w_MVCAOVAL=0,GETCAM(this.w_MVCODVAL, this.w_MVDATDOC, 7),this.w_MVCAOVAL)
    this.w_MVVALNAZ = iif(empty(this.w_MVVALNAZ),g_perval,this.w_MVVALNAZ)
    this.w_MVFLPROV = iif(empty(this.w_MVFLPROV),"N",this.w_MVFLPROV)
    this.w_MVFLSCAF = IIF(empty(Nvl(this.w_MVFLSCAF," "))," ",this.w_MVFLSCAF)
    this.w_MVNUMREG = iif(empty(this.w_MVNUMREG),this.w_MVNUMDOC,this.w_MVNUMREG)
    this.w_MVDATCIV = iif(empty(this.w_MVDATCIV),this.w_MVDATREG,this.w_MVDATCIV)
    this.w_MVIVABOL = iif(empty(this.w_MVIVABOL),IIF(this.w_MVFLVEAC="V", g_COIBOL, g_COABOL),this.w_MVIVABOL)
    this.w_MVMINTRA = substr(time(),4,2)
    this.w_MVORATRA = left(time(),2)
    this.w_MVDATTRA = iif(empty(this.w_MVDATTRA),this.w_MVDATREG,this.w_MVDATTRA)
    this.w_MVFLSCOM = g_FLSCOM
    if Empty(Nvl(this.w_MVSPEINC, 0) )
      this.w_MVSPEINC = CALSPEINC( this.w_MVFLVEAC, this.w_MVCODPAG, this.w_MVCODVAL, this.w_MVTIPCON, this.w_MVCODCON, this.w_FLSPIN )
    endif
    this.w_PADRE.SetObjectValues(This)     
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_PADRE.bCheckBeforeCreation
    endif
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
     
 Dimension L_ARRKEY(1,2) 
 L_ARRKEY[1,1]="MVSERIAL" 
 L_ARRKEY[1,2]=this.w_PADRE.w_MVSERIAL
    if EMPTY(this.w_RETVAL) and this.w_PADRE.bCheckAfterCreation
      bTrsOk = .T.
      l_Prg = Alltrim(this.w_PADRE.cAfterCheckRoutine)
      this.w_RETVAL = &l_Prg
      this.w_RETVAL = iif(Not bTRSOK ,this.w_RETVAL,"")
       
 Release bTRSOK 
 Release l_Prg 
    endif
    if EMPTY(this.w_RETVAL) and this.w_PADRE.bCalcSal
      bTrsOk = .T.
      l_Prg = Alltrim(this.w_PADRE.cCalcsalRoutine)
      this.w_RETVAL = &l_Prg
       
 Release bTRSOK 
 Release l_Prg 
    endif
    Release L_ARRKEY
    * --- Se nel memory cursor relativo non ho le rate provvedo a ricalcolarle
    this.w_PADRE.bNOCreateRates = this.w_PADRE.w_DOC_RATE.reccount()<>0
    if EMPTY(this.w_RETVAL) and this.w_PADRE.bCalcTot
       
 l_Prg = Alltrim(this.w_PADRE.cCalcTotRoutine) 
 &l_Prg
       
 Release l_Prg 
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo righe del corpo documento
    * --- cCheckRowType
    this.w_PADRE.w_DOC_DETT.Gotop()     
    do while not this.w_PADRE.w_DOC_DETT.eof()
      this.w_PADRE.w_DOC_DETT.ReadCurrentRecord()     
      this.w_PADRE.w_DOC_DETT.SetLocalValues(This)     
      this.w_MVNUMRIF = -20
      this.w_MVTIPRIG = this.w_PADRE.w_DOC_DETT.MVTIPRIG
      this.w_MVCODICE = this.w_PADRE.w_DOC_DETT.MVCODICE
      this.w_OMAG = IIF(EMPTY(this.w_MVCODMAG), IIF(EMPTY(this.w_OMAG),IIF(EMPTY(this.w_COMAG), g_MAGAZI, this.w_COMAG),this.w_OMAG), this.w_MVCODMAG)
      this.w_OMAT = IIF(EMPTY(this.w_MVCODMAT), IIF(EMPTY(this.w_OMAT),this.w_COMAT,this.w_OMAT), this.w_MVCODMAT)
      this.w_OCEN = IIF(this.w_MVTIPRIG<>"D" , iif( Not Empty( this.w_MVCODCEN ) , this.w_MVCODCEN, iif( Not Empty( this.w_CODCEN ) , this.w_CODCEN, this.w_OCEN)) , this.w_OCEN)
      this.w_OCOM = IIF(this.w_MVTIPRIG<>"D", this.w_MVCODCOM, this.w_OCOM)
      this.w_OATT = IIF(this.w_MVTIPRIG="D" , this.w_OATT , this.w_MVCODATT)
      this.w_OK = .T.
      if Empty(this.w_MVCODICE)
        this.w_RETVAL = Ah_msgformat("Esiste almeno una riga con codice di ricerca non valorizzato")
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_OK
        this.w_MVUNIMIS = this.w_PADRE.w_DOC_DETT.MVUNIMIS
        this.w_MVQTAMOV = this.w_PADRE.w_DOC_DETT.MVQTAMOV
        this.w_OKRIG = this.w_MVTIPRIG<>" " AND NOT EMPTY(this.w_MVCODICE) and ((Not Empty(this.w_MVUNIMIS) or this.w_MVTIPRIG="D") and (this.w_MVQTAMOV <>0 or this.w_MVTIPRIG="D"))
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "CACODICE"
        ArrWhere(1,2) = this.w_MVCODICE
        this.w_MVCODART = this.w_PADRE.ReadTable( "KEY_ARTI" , "CACODART" , @ArrWhere )
        this.w_MVTIPRIG = this.w_PADRE.ReadTable( "KEY_ARTI" , "CA__TIPO" , @ArrWhere )
        this.w_TPCON3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CATPCON3" , @ArrWhere )
        this.w_TIPCO3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CATIPCO3" , @ArrWhere )
        this.w_MOLTI3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAMOLTIP" , @ArrWhere )
        this.w_OPERA3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAOPERAT" , @ArrWhere )
        this.w_UNMIS3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAUNIMIS" , @ArrWhere )
        this.w_MVDESSUP = IIF( Empty(this.w_MVDESSUP),Nvl(this.w_PADRE.ReadTable( "KEY_ARTI" , "CADESSUP" , @ArrWhere )," "),this.w_MVDESSUP)
        this.w_MVDESART = IIF(Empty(this.w_MVDESART),Nvl(this.w_PADRE.ReadTable( "KEY_ARTI" , "CADESART" , @ArrWhere )," "),this.w_MVDESART)
        Release ArrWhere
        this.w_MVCODART = iif(Empty(this.w_MVCODART),this.w_MVCODICE,this.w_MVCODART)
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "ARCODART"
        ArrWhere(1,2) = this.w_MVCODART
        this.w_PREZUM = this.w_PADRE.ReadTable( "ART_ICOL" , "ARPREZUM" , @ArrWhere )
        this.w_ARRIPCON = this.w_PADRE.ReadTable( "ART_ICOL" , "ARRIPCON" , @ArrWhere )
        this.w_ARFLAPCA = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLAPCA" , @ArrWhere )
        this.w_PRESTA = this.w_PADRE.ReadTable( "ART_ICOL" , "ARPRESTA" , @ArrWhere )
        this.w_FLESIM = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLESIM" , @ArrWhere )
        this.w_CODDIS = this.w_PADRE.ReadTable( "ART_ICOL" , "ARCODDIS" , @ArrWhere )
        this.w_FLCESP = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLCESP" , @ArrWhere )
        this.w_DISLOT = this.w_PADRE.ReadTable( "ART_ICOL" , "ARDISLOT" , @ArrWhere )
        this.w_ARTPRO = this.w_PADRE.ReadTable( "ART_ICOL" , "ARGRUPRO" , @ArrWhere )
        this.w_GESMAT = this.w_PADRE.ReadTable( "ART_ICOL" , "ARGESMAT" , @ArrWhere )
        this.w_FLDISC = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLDISC" , @ArrWhere )
        this.w_MAGPRE = this.w_PADRE.ReadTable( "ART_ICOL" , "ARMAGPRE" , @ArrWhere )
        this.w_FLLOTT = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLLOTT" , @ArrWhere )
        this.w_FLSERA = this.w_PADRE.ReadTable( "ART_ICOL" , "ARTIPSER" , @ArrWhere )
        this.w_FLUSEP = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLUSEP" , @ArrWhere )
        this.w_FLSERG = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLSERG" , @ArrWhere )
        this.w_TPCON2 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARTPCON2" , @ArrWhere )
        this.w_TPCON1 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARTPCONF" , @ArrWhere )
        this.w_TIPCO2 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARTIPCO2" , @ArrWhere )
        this.w_TIPCO1 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARTIPCO1" , @ArrWhere )
        this.w_VOCRIC = iif(Empty(this.w_MVVOCCEN),NVL(this.w_PADRE.ReadTable( "ART_ICOL" , "ARVOCRIC" , @ArrWhere ),Space(15)),this.w_MVVOCCEN)
        this.w_VOCCEN = IIF(EMPTY(this.w_MVVOCCEN),NVL(this.w_PADRE.ReadTable( "ART_ICOL" , "ARVOCCEN" , @ArrWhere ),SPACE(15)), this.w_MVVOCCEN)
        this.w_CODCEN = IIF(EMPTY(this.w_MVCODCEN),NVL(this.w_PADRE.ReadTable( "ART_ICOL" , "ARCODCEN" , @ArrWhere ),SPACE(15)), this.w_MVCODCEN)
        this.w_MVCODCLA = iif(Empty(this.w_MVCODCLA),Nvl(this.w_PADRE.ReadTable( "ART_ICOL" , "ARCODCLA" , @ArrWhere ),Space(3)),this.w_MVCODCLA)
        this.w_ARTDIS = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLDISP" , @ArrWhere )
        this.w_MVCATCON = iif(Empty(this.w_MVCATCON),Nvl(this.w_PADRE.ReadTable( "ART_ICOL" , "ARCATCON" , @ArrWhere ),Space(5)),this.w_MVCATCON)
        this.w_MVPESNET = iif(Empty(this.w_MVPESNET),Nvl(this.w_PADRE.ReadTable( "ART_ICOL" , "ARPESNET" , @ArrWhere ),0),this.w_MVPESNET)
        this.w_MVMOLSUP = iif(Empty(this.w_MVMOLSUP),Nvl(this.w_PADRE.ReadTable( "ART_ICOL" , "ARMOLSUP" , @ArrWhere ),0),this.w_MVMOLSUP)
        this.w_MVUMSUPP = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUMSUPP" , @ArrWhere )
        this.w_MVNOMENC = iif(Empty(this.w_MVNOMENC),Nvl(this.w_PADRE.ReadTable( "ART_ICOL" , "ARNOMENC" , @ArrWhere ),space(8)),this.w_MVNOMENC)
        this.w_MVCODIVA = iif(Empty(this.w_MVCODIVA),Nvl(this.w_PADRE.ReadTable( "ART_ICOL" , "ARCODIVA" , @ArrWhere ),space(5)),this.w_MVCODIVA)
        this.w_OPERAT = this.w_PADRE.ReadTable( "ART_ICOL" , "AROPERAT" , @ArrWhere )
        this.w_UNMIS1 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS1" , @ArrWhere )
        this.w_MOLTIP = this.w_PADRE.ReadTable( "ART_ICOL" , "ARMOLTIP" , @ArrWhere )
        this.w_UNMIS2 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS2" , @ArrWhere )
        this.w_ARUTISER = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUTISER" , @ArrWhere )
        this.w_ARDATINT = this.w_PADRE.ReadTable( "ART_ICOL" , "ARDATINT" , @ArrWhere )
        this.w_MVDESSUP = IIF(Empty(this.w_MVDESSUP),Nvl(this.w_PADRE.ReadTable( "ART_ICOL" , "ARDESSUP" , @ArrWhere )," "),this.w_MVDESSUP)
        this.w_MVDESART = IIF(Empty(this.w_MVDESART),Nvl(this.w_PADRE.ReadTable( "ART_ICOL" , "ARDESART" , @ArrWhere )," "),this.w_MVDESART)
        this.w_MVFLNOAN = IIF(Empty(Nvl(this.w_MVFLNOAN,"N")),"N",this.w_MVFLNOAN)
        Release ArrWhere
        if this.w_PADRE.cCheckRowType="C" and Not this.w_OKRIG
          * --- Aggiorno condizione di riga piena dopo i link su MVCODICE avendo
          *     impostato completa i dati nel controllo di riga piena.
          this.w_OKRIG = this.w_MVTIPRIG<>" " AND NOT EMPTY(this.w_MVCODICE) and ((Not Empty(this.w_MVUNIMIS) or this.w_MVTIPRIG="D") and (this.w_MVQTAMOV <>0 or this.w_MVTIPRIG="D"))
        endif
        if !this.w_OKRIG
          this.w_RETVAL = Ah_msgformat("Esiste almeno una riga con condizione di riga piena non verificata")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      if this.w_OK
        if Not Empty(this.w_MVCODIVA)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "IVCODIVA"
          ArrWhere(1,2) = this.w_MVCODIVA
          this.w_BOLIVA = this.w_PADRE.ReadTable( "VOCIIVA" , "IVBOLIVA" , @ArrWhere )
          this.w_PERIVA = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIVA" , @ArrWhere )
          this.w_PERIND = this.w_PADRE.ReadTable( "VOCIIVA" , "IVPERIND" , @ArrWhere )
          this.w_DATOBSO = this.w_PADRE.ReadTable( "VOCIIVA" , "IVDTOBSO" , @ArrWhere )
          Release ArrWhere
        endif
        this.w_MVCAUMAG = iif(Empty(this.w_MVCAUMAG),this.w_MVTCAMAG,this.w_MVCAUMAG)
        if Not Empty(this.w_MVCAUMAG)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "CMCODICE"
          ArrWhere(1,2) = this.w_MVCAUMAG
          this.w_MTCARI = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMMTCARI" , @ArrWhere )
          this.w_MV_FLAGG = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMVARVAL" , @ArrWhere )
          this.w_FLCOMM = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLCOMM" , @ArrWhere )
          this.w_FLRISE = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLRISE" , @ArrWhere )
          this.w_FLIMPE = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLIMPE" , @ArrWhere )
          this.w_FLORDI = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLORDI" , @ArrWhere )
          this.w_FLCASC = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLCASC" , @ArrWhere )
          this.w_FLAVA1 = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLAVAL" , @ArrWhere )
          this.w_FLCOMM = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLCOMM" , @ArrWhere )
          this.w_MVCAUCOL = IIF(EMPTY(this.w_MVCAUCOL), Nvl(this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMCAUCOL" , @ArrWhere ),Space(5)),this.w_MVCAUCOL)
          this.w_DTOBSO = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMDTOBSO" , @ArrWhere )
          this.w_MVFLELGM = IIF(EMPTY(this.w_MVFLELGM), Nvl(this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLELGM" , @ArrWhere )," "),this.w_MVFLELGM)
          Release ArrWhere
        endif
        if Empty(this.w_MVCODMAG)
          this.w_MVCODMAG = IIF(this.w_MVTIPRIG="R" AND (NOT EMPTY(ALLTRIM(this.w_FLCASC+this.w_FLRISE+this.w_FLORDI+this.w_FLIMPE))Or (this.w_MV_FLAGG $"+-" And Empty(this.w_MVSERRIF))), IIF(NOT EMPTY(IIF(this.w_FLLOTT<>"C",SPACE(5),this.w_MVCODMAG)),IIF(this.w_FLLOTT<>"C",SPACE(5),this.w_MVCODMAG), CALCMAG(1, this.w_FLMGPR, "     ", this.w_COMAG, this.w_OMAG, this.w_MAGPRE, this.w_MAGTER)), SPACE(5))
        endif
        if Not Empty(this.w_MVCODMAG)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "MGCODMAG"
          ArrWhere(1,2) = this.w_MVCODMAG
          this.w_FLUBIC = this.w_PADRE.ReadTable( "MAGAZZIN" , "MGFLUBIC" , @ArrWhere )
          Release ArrWhere
        endif
        if Not Empty(this.w_MVCAUCOL)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "CMCODICE"
          ArrWhere(1,2) = this.w_MVCAUCOL
          this.w_F2CASC = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLCASC" , @ArrWhere )
          this.w_F2RISE = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLRISE" , @ArrWhere )
          this.w_F2IMPE = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLIMPE" , @ArrWhere )
          this.w_F2ORDI = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLORDI" , @ArrWhere )
          Release ArrWhere
        endif
        if Empty(this.w_MVCODMAT)
          this.w_MVCODMAT = IIF(this.w_MVTIPRIG="R" AND NOT EMPTY(this.w_F2CASC+this.w_F2RISE+this.w_F2ORDI+this.w_F2IMPE), CALCMAG(1, this.w_FLMTPR, "     ", this.w_COMAT, this.w_OMAT, this.w_MAGPRE, this.w_MAGTER), SPACE(5))
        endif
        if Not Empty(this.w_MVCODMAT)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "MGCODMAG"
          ArrWhere(1,2) = this.w_MVCODMAT
          this.w_F2UBIC = this.w_PADRE.ReadTable( "MAGAZZIN" , "MGFLUBIC" , @ArrWhere )
          Release ArrWhere
        endif
        this.w_MVUNIMIS = iif(Empty(this.w_MVUNIMIS),IIF(NOT EMPTY(this.w_UNMIS3) AND this.w_MOLTI3<>0, this.w_UNMIS3, this.w_UNMIS1),this.w_MVUNIMIS)
        if Not Empty(this.w_MVUNIMIS)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "UMCODICE"
          ArrWhere(1,2) = this.w_MVUNIMIS
          this.w_FLFRAZ = this.w_PADRE.ReadTable( "UNIMIS" , "UMFLFRAZ" , @ArrWhere )
          this.w_DUR_ORE = this.w_PADRE.ReadTable( "UNIMIS" , "UMDURORE" , @ArrWhere )
          this.w_CHKTEMP = this.w_PADRE.ReadTable( "UNIMIS" , "UMFLTEMP" , @ArrWhere )
          this.w_MODUM2 = this.w_PADRE.ReadTable( "UNIMIS" , "UMMODUM2" , @ArrWhere )
          Release ArrWhere
        endif
        if Not Empty(this.w_UNMIS1)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "UMCODICE"
          ArrWhere(1,2) = this.w_UNMIS1
          this.w_MODUM2 = this.w_PADRE.ReadTable( "UNIMIS" , "UMMODUM2" , @ArrWhere )
          this.w_FLFRAZ1 = this.w_PADRE.ReadTable( "UNIMIS" , "UMFLFRAZ" , @ArrWhere )
          Release ArrWhere
        endif
        this.w_MVKEYSAL = IIF(this.w_MVTIPRIG="R" AND (NOT EMPTY(ALLTRIM(this.w_FLCASC+this.w_FLRISE+this.w_FLORDI+this.w_FLIMPE+this.w_F2CASC+this.w_F2RISE+this.w_F2ORDI+this.w_F2IMPE))Or (this.w_MV_FLAGG $"+-" And Empty(this.w_MVSERRIF)) ), this.w_MVCODART, SPACE(20))
        if Not Empty(this.w_MVKEYSAL) OR Not Empty(this.w_MVCODMAG)
          DIMENSION ArrWhere(2,2)
          ArrWhere(1,1) = "SLCODICE"
          ArrWhere(1,2) = this.w_MVKEYSAL
          ArrWhere(2,1) = "SLCODMAG"
          ArrWhere(2,2) = this.w_MVCODMAG
          this.w_CODVAA = this.w_PADRE.ReadTable( "SALDIART" , "SLCODVAA" , @ArrWhere )
          this.w_VALUCA = this.w_PADRE.ReadTable( "SALDIART" , "SLVALUCA" , @ArrWhere )
          this.w_ULTSCA = this.w_PADRE.ReadTable( "SALDIART" , "SLDATUPV" , @ArrWhere )
          this.w_ULTCAR = this.w_PADRE.ReadTable( "SALDIART" , "SLDATUCA" , @ArrWhere )
          this.w_QTRPER = this.w_PADRE.ReadTable( "SALDIART" , "SLQTRPER" , @ArrWhere )
          this.w_QTAPER = this.w_PADRE.ReadTable( "SALDIART" , "SLQTAPER" , @ArrWhere )
          Release ArrWhere
        endif
        if Not Empty(this.w_MVKEYSAL) OR Not Empty(this.w_MVCODMAT)
          DIMENSION ArrWhere(2,2)
          ArrWhere(1,1) = "SLCODICE"
          ArrWhere(1,2) = this.w_MVKEYSAL
          ArrWhere(2,1) = "SLCODMAG"
          ArrWhere(2,2) = this.w_MVCODMAT
          this.w_Q2RPER = this.w_PADRE.ReadTable( "SALDIART" , "SLQTRPER" , @ArrWhere )
          this.w_Q2APER = this.w_PADRE.ReadTable( "SALDIART" , "SLQTAPER" , @ArrWhere )
          Release ArrWhere
        endif
        this.w_MVCODLIS = this.w_MVTCOLIS
        this.w_MVLOTMAG = iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUBI ) , SPACE(5) , this.w_MVCODMAG )
        this.w_MVLOTMAT = iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUB2 ) , SPACE(5) , this.w_MVCODMAT )
        this.w_MVQTAMOV = IIF(this.w_MVTIPRIG="F", 1, IIF(this.w_MVTIPRIG="D", 0, this.w_MVQTAMOV))
        msg=""
        this.w_MVQTAUM1 = IIF(this.w_MVTIPRIG="F", 1, CALQTA(this.w_MVQTAMOV,this.w_MVUNIMIS,this.w_UNMIS2,this.w_OPERAT, this.w_MOLTIP, this.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, @msg, this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3))
        if this.w_MVQTAUM1=0 AND this.w_MVTIPRIG<>"D"
          this.w_RETVAL = Ah_msgformat(msg)
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      if this.w_OK
        this.w_MVPROORD = g_PROAZI
        this.w_MVFLRAGG = this.w_MVTFRAGG
        this.w_MV_SEGNO = this.w_TD_SEGNO
        this.w_MVFLELAN = iif(empty(this.w_MVFLELAN),this.w_FLELAN,this.w_MVFLELAN)
        this.w_MVTIPPRO = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
        this.w_MVTIPPR2 = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
        this.w_MVDATOAI = this.w_MVDATREG
        this.w_MVFLOMAG = IIF(Not Empty(Nvl(this.w_MVFLOMAG," ")),this.w_MVFLOMAG,"X")
        this.w_MVQTASAL = this.w_MVQTAUM1
        this.w_MVFLTRAS = IIF(Empty(this.w_MVFLTRAS),IIF(this.w_ARUTISER="S","S",IIF( this.w_ARDATINT="S","Z",IIF( this.w_ARDATINT="F"," ",IIF( this.w_ARDATINT="I","I","S")))),this.w_MVFLTRAS)
        this.w_MVDATEVA = IIF(Not Empty(this.w_MVDATEVA),this.w_MVDATEVA,this.w_MVDATDOC)
        this.w_MVFLRISE = IIF(this.w_MVFLPROV="S"," ",this.w_FLRISE)
        this.w_MVFLCASC = IIF(this.w_MVFLPROV="S"," ",this.w_FLCASC)
        this.w_MVFLIMPE = IIF(this.w_MVFLPROV="S"," ",this.w_FLIMPE)
        this.w_MVFLORDI = IIF(this.w_MVFLPROV="S"," ",this.w_FLORDI)
        this.w_MVF2ORDI = IIF(this.w_MVFLPROV="S"," ",this.w_F2ORDI)
        this.w_MVF2CASC = IIF(this.w_MVFLPROV="S"," ",this.w_F2CASC)
        this.w_MVF2IMPE = IIF(this.w_MVFLPROV="S"," ",this.w_F2IMPE)
        this.w_MVF2RISE = IIF(this.w_MVFLPROV="S"," ",this.w_F2RISE)
        if g_MADV="S"
          this.w_MVFLLOTT = IIF(EMPTY(this.w_MVFLLOTT),IIF(this.w_FLLOTT $ "S-C" OR this.w_FLUBIC="S",LEFT(ALLTRIM(this.w_MVFLCASC)+IIF(this.w_MVFLRISE="+", "-", IIF(this.w_MVFLRISE="-", "+", " ")), 1), " "),this.w_MVFLLOTT)
          this.w_MVF2LOTT = IIF(EMPTY(this.w_MVF2LOTT),this.w_MVF2LOTT,IIF(this.w_FLLOTT $ "S-C" OR this.w_F2UBIC="S",LEFT(ALLTRIM(this.w_MVF2CASC) + IIF(this.w_MVF2RISE="+", "-", IIF(this.w_MVF2RISE="-", "+", " ")), 1), " "))
          this.w_MVCODLOT = IIF(this.w_FLLOTT $ "SC" AND (this.w_MVF2LOTT $ "+-" OR this.w_MVFLLOTT $ "+-") ,this.w_MVCODLOT,Space(20))
          this.w_MVCODUBI = IIF(Empty(this.w_MVCODMAG) AND this.w_MVFLLOTT $ "+-" ,this.w_MVCODUBI,Space(20))
          this.w_MVCODUB2 = IIF(Empty(this.w_MVCODMAT) AND this.w_MVF2LOTT $ "+-" ,this.w_MVCODUB2,Space(20))
        endif
        this.w_MVVOCCEN = IIF(this.w_MVTIPRIG<>"D" AND ((g_PERCCR="S" AND this.w_FLANAL="S") OR (g_COMM="S" AND this.w_FLGCOM="S")), IIF(this.w_VOCTIP="C", this.w_VOCCEN, this.w_VOCRIC), SPACE(15))
        if this.w_MVTIPRIG<>"D" AND ((g_PERCAN="S" AND this.w_FLANAL="S" and this.w_MVFLNOAN="N") OR (g_COMM="S" AND this.w_FLGCOM="S") OR (this.w_FLPRAT="S")) and Not IsAlt()
          this.w_MVFLCOCO = IIF(g_COMM="S" , iif(this.w_FLCOMM="C","+",IIF(this.w_FLCOMM="S","-"," "))," ")
          this.w_MVFLORCO = IIF(g_COMM="S" ,iif(this.w_FLCOMM="I","+",IIF(this.w_FLCOMM="D","-"," "))," ")
          this.w_MVCODCOM = IIF(this.w_MVTIPRIG<>"D", this.w_OCOM, SPACE(15))
          this.w_MVCODATT = IIF(Empty(this.w_MVCODATT),this.w_OATT,this.w_MVCODATT)
          this.w_MVTIPATT = "A"
        endif
        if Not Empty(this.w_MVVOCCEN)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "VCCODICE"
          ArrWhere(1,2) = this.w_MVVOCCEN
          this.w_CODCOS = this.w_PADRE.ReadTable( "VOC_COST" , "VCTIPCOS" , @ArrWhere )
          Release ArrWhere
        endif
        this.w_MVCODCOS = IIF(EMPTY(this.w_MVCODATT), SPACE(5), this.w_CODCOS)
        if g_PERCCR="S" AND this.w_FLANAL="S" AND this.w_MVTIPRIG<>"D" and this.w_MVFLNOAN="N"
          this.w_MVCODCEN = IIF(this.w_MVTIPRIG<>"D",iif( not empty( this.w_CODCEN ), this.w_CODCEN, this.w_OCEN ),SPACE(15))
        endif
        this.w_MVPREZZO = this.w_PADRE.w_DOC_DETT.MVPREZZO
        if this.w_MVPREZZO=0 AND this.w_PRZDES="S"
          * --- Memorizzo la qt� nella 1^ UM prima di lanciare CALPRZLI poich� all'interno
          *     della funzione potrebbe essere modificata da il Ricalcolo da Lotto di Riordino
          this.w_PROG = "V" + IIF(Empty(this.w_FLQRIO)," ",this.w_FLQRIO) + IIF(Empty(this.w_FLPREV)," ",this.w_FLPREV)+" "
          DECLARE ARRCALC (16,1)
          * --- Azzero l'Array che verr� riempito dalla Funzione
           
 ARRCALC(1)=0
          * --- Se parametro='Z' allora non devo calcolare il contratto...
          *     Al posto del Gruppo merceologico passo 'XXXXXX' per eliminazione variabile GRUMER dal Body dei documenti.
          *     In questo caso rileggo il gruppo merceologico all'interno di CALPRZLI
          *     Stessa cosa per Categoria Sconti maggiorazioni dell'articolo
          this.w_QTAUM3 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
          this.w_QTAUM2 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
          DIMENSION pArrUm[9]
          pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_MVUNIMIS 
 pArrUm [3] = this.w_MVQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_MVQTAUM1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
          this.w_CALPRZ = CalPrzli( Nvl(this.w_MVCONTRA," ") , this.w_MVTIPCON , this.w_MVCODLIS , this.w_MVCODART , "XXXXXX" , this.w_MVQTAUM1 , this.w_MVCODVAL , this.w_MVCAOVAL , this.w_MVDatReg , this.w_CATCLI , "XXXXXX", this.w_CODVAA, this.w_XCONORN, this.w_CATCOM, this.w_MVFLSCOR, this.w_SCOLIS, this.w_VALUCA,this.w_PROG, @ARRCALC, this.w_PRZVAC, this.w_MVFLVEAC ,"N", @pArrUm)
          this.w_MVCONTRA = IIF(ARRCALC(9)="XXXXXXXXXXXXXXXX",SPACE(15),ARRCALC(9))
          * --- Se il prezzo � calcolato da Listino/Contratto/U.C.A., U.P.V. LIPREZZO = al prezzo calcolato
          *     Nel caso in cui � presente un contratto valido, in ogni caso LIPREZZO = prezzo da contratto anche se 0
          *     Se non trovo un prezzo e non esiste contratto reimposto il prezzo precedentemente calcolato o inserito a mano
          *     Se lancio questo batch da Import documenti nel caso di Ricalcolo Qt� da Lotto di Riordino non devo ricalcolare il prezzo
          this.w_LIPREZZO = ARRCALC(5) 
          this.w_LISCON = ARRCALC(7)
          * --- Aggiorno gli sconti solo se arrivano da Listino/Contratto/Tabella ScontiMagg.
          *     solo se non sono nel ricalcolo Qt� da Lotto di Riordino in fase di Import
          this.w_MVSCONT1 = ARRCALC(1) 
          this.w_MVSCONT2 = ARRCALC(2) 
          this.w_MVSCONT3 = ARRCALC(3) 
          this.w_MVSCONT4 = ARRCALC(4) 
          this.w_IVACON = ARRCALC(12) 
          this.w_APPO = (IIF(Empty(this.w_UNMIS1),Space(3),this.w_UNMIS1))+(IIF(Empty(this.w_UNMIS2),Space(3),this.w_UNMIS2))+(IIF(Empty(this.w_UNMIS3),Space(3),this.w_UNMIS3))
          this.w_MVPREZZO = Round(CALMMLIS(this.w_LIPREZZO, this.w_APPO+this.w_MVUNIMIS+this.w_OPERAT+this.w_OPERA3+IIF(this.w_LISCON=2, this.w_IVALIS, "N")+"P"+ALLTRIM(STR(this.w_DECUNI)), this.w_MOLTIP, this.w_MOLTI3, IIF(this.w_MVFLSCOR="S",0,this.w_PERIVA)),this.w_DECUNI)
        endif
        this.w_MVFLULCA = IIF(this.w_FLAVA1="A" AND this.w_MVFLCASC="+" AND this.w_MVDATDOC>=this.w_ULTCAR AND this.w_MVFLOMAG<>"S", "=", " ")
        this.w_MVFLULPV = IIF(this.w_FLAVA1="V" AND this.w_MVFLCASC="-" AND this.w_MVDATDOC>=this.w_ULTSCA AND this.w_MVFLOMAG<>"S" AND (this.w_MVCLADOC<>"DT" OR this.w_MVPREZZO<>0), "=", " ")
        this.w_MVSCONT2 = iif(this.w_NUMSCO<2,0,this.w_MVSCONT2)
        this.w_MVSCONT3 = iif(this.w_NUMSCO<3,0,this.w_MVSCONT3)
        this.w_MVSCONT4 = iif(this.w_NUMSCO<4,0,this.w_MVSCONT4)
        this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
      endif
      * --- Mvimpnaz-... calcolati in GSA_BRS eseguit nel GSAR_BRD
      *     aggiornamento saldi eseguito nella funzione FLR_DOCUM
      if this.w_OK
        this.w_PADRE.w_DOC_DETT.SetObjectValues(This)     
        this.w_PADRE.w_DOC_DETT.SaveCurrentRecord()     
        this.w_PADRE.w_DOC_DETT.Next()     
      endif
      if Not Empty(this.w_RETVAL)
        i_retcode = 'stop'
        i_retval = this.w_RETVAL
        return
      endif
    enddo
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_PADRE.cCheckRowType="D"
      this.w_PADRE.w_DOC_DETT.Delete()     
      this.w_RETVAL = " "
    endif
    this.w_OK = .F.
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
    this.w_RETVAL = iif(this.w_PADRE.w_DOC_DETT.reccount()=0,Ah_msgformat("Impossibile generare documento senza righe di dettaglio"),this.w_RETVAL)
    if Empty(this.w_RETVAL)
      if EMPTY(this.w_PADRE.w_MVSERIAL)
        i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
        cp_NextTableProg(this.w_PADRE, i_Conn, "SEDOC", "i_CODAZI,w_MVSERIAL", .T. )
        if this.w_MVANNPRO<>" "
          i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
          cp_NextTableProg(this.w_PADRE, i_Conn, "PRPRO", "i_codazi,w_MVANNPRO,w_MVPRP,w_MVALFEST,w_MVNUMEST")
        endif
        if (this.w_MVFLVEAC="V" OR (this.w_MVFLVEAC="A" AND (this.w_MVPRD="DV" OR this.w_MVPRD="IV"))) AND empty(this.w_MVNUMDOC)
          i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
          cp_NextTableProg(this.w_PADRE, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
        endif
      endif
      Local cMacro
      this.w_PROPINS = 0
      this.w_PROPATT = 1
      this.w_NUMPROP = ALEN(this.w_PADRE.PropList, 1)
      OLD_ERR= ON("ERROR")
      ON ERROR AssVar=.T.
      do while this.w_PROPATT<=this.w_NUMPROP
        if this.w_PADRE.PropList(this.w_PROPATT,3)="F"
          this.w_PROPINS = this.w_PROPINS + 1
          DIMENSION ArrInsert( this.w_PROPINS ,2)
          cMacro = UPPER( ALLTRIM( this.w_PADRE.PropList(this.w_PROPATT,1) ) )
          ArrInsert( this.w_PROPINS ,1) = STRTRAN(m.cMacro, "W_", "")
          cMacro = "this.w_PADRE."+m.cMacro
          ArrInsert( this.w_PROPINS ,2) = &cMacro
        endif
        this.w_PROPATT = this.w_PROPATT + 1
      enddo
      release cMacro
      ON ERROR &OLD_ERR
      * --- Aggiungo un elemento all'array per ospitare il cpcchk
      DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
      ArrInsert( ALEN(ArrInsert,1) , 1) = "cpccchk"
      ArrInsert( ALEN(ArrInsert,1) , 2) = cp_NewCCChk()
      * --- Try
      local bErr_047C12F8
      bErr_047C12F8=bTrsErr
      this.Try_047C12F8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_RETVAL = ah_MsgFormat("Errore inserimento documento.%0%1", MESSAGE())
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_RETVAL
      endif
      bTrsErr=bTrsErr or bErr_047C12F8
      * --- End
      Release ArrInsert
    endif
    if EMPTY(this.w_RETVAL) and this.w_PADRE.w_DOC_RATE.reccount()<>0 
      this.w_PADRE.w_DOC_RATE.GoTop()     
      do while !this.w_PADRE.w_DOC_RATE.Eof()
        this.w_PADRE.w_DOC_RATE.ReadCurrentRecord()     
        this.w_PADRE.w_DOC_RATE.RSSERIAL = this.w_PADRE.w_MVSERIAL
        this.w_PADRE.w_DOC_RATE.cpccchk = cp_NewCCChk()
        this.w_PADRE.w_DOC_RATE.SaveCurrentRecord()     
        DIMENSION ArrInsert( 1 ,2)
        * --- Popolo l'array con i dati del rate corrente
        this.w_PADRE.w_DOC_RATE.CurrentRecordToArray(@ArrInsert)     
        * --- Inserimento record rate attuale
        * --- Try
        local bErr_047BA0F8
        bErr_047BA0F8=bTrsErr
        this.Try_047BA0F8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_RETVAL = ah_MsgFormat("Errore inserimento rate.%0%1", MESSAGE())
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_RETVAL
        endif
        bTrsErr=bTrsErr or bErr_047BA0F8
        * --- End
        Release ArrInsert
        this.w_PADRE.w_DOC_RATE.Next()     
      enddo
    endif
    if EMPTY(this.w_RETVAL) and this.w_PADRE.w_CON_PAGA.reccount()<>0 AND (this.w_MVACCONT <>0 OR this.w_MVACCPRE<>0 )
      this.w_PADRE.w_CON_PAGA.GoTop()     
      do while !this.w_PADRE.w_CON_PAGA.Eof()
        this.w_PADRE.w_CON_PAGA.ReadCurrentRecord()     
        this.w_PADRE.w_CON_PAGA.CPSERIAL = this.w_PADRE.w_MVSERIAL
        this.w_PADRE.w_CON_PAGA.cpccchk = cp_NewCCChk()
        this.w_PADRE.w_CON_PAGA.SaveCurrentRecord()     
        DIMENSION ArrInsert( 1 ,2)
        * --- Popolo l'array con i dati del dettaglio pagamenti
        this.w_PADRE.w_CON_PAGA.CurrentRecordToArray(@ArrInsert)     
        * --- Inserimento record del dettaglio pagamenti
        * --- Try
        local bErr_047B2788
        bErr_047B2788=bTrsErr
        this.Try_047B2788()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_RETVAL = ah_MsgFormat("Errore inserimento dettaglio pagamenti.%0%1", MESSAGE())
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_RETVAL
        endif
        bTrsErr=bTrsErr or bErr_047B2788
        * --- End
        Release ArrInsert
        this.w_PADRE.w_CON_PAGA.Next()     
      enddo
    endif
    if EMPTY(this.w_RETVAL) AND this.w_PADRE.w_VDATRITE.reccount()<>0 and (this.w_MVFLVEAC="V" and this.w_MVTIPCON="C" AND this.w_FLGRITE = "S" AND this.w_MVCLADOC $ "FA-NC" And this.w_GESRIT="S" AND NOT EMPTY(this.w_MVCODCON)) AND g_RITE="S" 
      this.w_PADRE.w_VDATRITE.GoTop()     
      this.w_CPROWNUM = 0
      this.w_SELECTCURS = SYS(2015)
      this.w_PADRE.w_VDATRITE.Exec_MCSelect(this.w_SELECTCURS , "MAX(CPROWNUM) AS CPROWNUM", "" , "" , "" , "")     
      if USED(this.w_SELECTCURS)
        SELECT (this.w_SELECTCURS)
        GO TOP
        this.w_CPROWNUM = MAX(NVL(CPROWNUM, 0), 0)
        USE IN SELECT(this.w_SELECTCURS)
      endif
      this.w_PADRE.w_VDATRITE.GoTop()     
      do while !this.w_PADRE.w_VDATRITE.Eof()
        this.w_PADRE.w_VDATRITE.ReadCurrentRecord()     
        if this.w_PADRE.w_VDATRITE.CPROWNUM = 0
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          this.w_PADRE.w_VDATRITE.CPROWNUM = this.w_CPROWNUM
        endif
        this.w_PADRE.w_VDATRITE.DRSERIAL = this.w_PADRE.w_MVSERIAL
        this.w_PADRE.w_VDATRITE.cpccchk = cp_NewCCChk()
        this.w_PADRE.w_VDATRITE.SaveCurrentRecord()     
        DIMENSION ArrInsert( 1 ,2)
        * --- Popolo l'array con i dati dei dati ritenute
        this.w_PADRE.w_VDATRITE.CurrentRecordToArray(@ArrInsert)     
        * --- Inserimento record dati ritenute
        * --- Try
        local bErr_047A6E48
        bErr_047A6E48=bTrsErr
        this.Try_047A6E48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_RETVAL = ah_MsgFormat("Errore inserimento dati ritenute.%0%1", MESSAGE())
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_RETVAL
        endif
        bTrsErr=bTrsErr or bErr_047A6E48
        * --- End
        Release ArrInsert
        this.w_PADRE.w_VDATRITE.Next()     
      enddo
    endif
    if EMPTY(this.w_RETVAL) and this.w_PADRE.w_DOC_DETT.reccount()<>0
      if this.w_PADRE.w_DOC_DETT.reccount()<>0
        this.w_PADRE.w_DOC_DETT.GoTop()     
        this.w_CPROWNUM = 0
        this.w_SELECTCURS = SYS(2015)
        this.w_PADRE.w_DOC_DETT.Exec_MCSelect(this.w_SELECTCURS , "MAX(CPROWNUM) AS CPROWNUM", "" , "" , "" , "")     
        if USED(this.w_SELECTCURS)
          SELECT (this.w_SELECTCURS)
          GO TOP
          this.w_CPROWNUM = MAX(NVL(CPROWNUM, 0), 0)
          USE IN SELECT(this.w_SELECTCURS)
        endif
        this.w_PADRE.w_DOC_DETT.GoTop()     
        do while !this.w_PADRE.w_DOC_DETT.Eof()
          this.w_PADRE.w_DOC_DETT.ReadCurrentRecord()     
          this.w_ROWMAT = IIF(this.w_PADRE.w_DOC_DETT.CPROWNUM <0,this.w_PADRE.w_DOC_DETT.CPROWNUM ,0)
          if this.w_PADRE.w_DOC_DETT.CPROWNUM <= 0
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_PADRE.w_DOC_DETT.CPROWNUM = this.w_CPROWNUM
          endif
          if this.w_PADRE.w_DOC_DETT.CPROWORD <= 0
            this.w_PADRE.w_DOC_DETT.CPROWORD = this.w_CPROWNUM * 10
          endif
          this.w_PADRE.w_DOC_DETT.MVSERIAL = this.w_PADRE.w_MVSERIAL
          this.w_PADRE.w_DOC_DETT.cpccchk = cp_NewCCChk()
          this.w_PADRE.w_DOC_DETT.SaveCurrentRecord()     
          DIMENSION ArrInsert( 1 ,2)
          * --- Popolo l'array con i dati della riga corrente
          this.w_PADRE.w_DOC_DETT.CurrentRecordToArray(@ArrInsert)     
          * --- Try
          local bErr_04786570
          bErr_04786570=bTrsErr
          this.Try_04786570()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_RETVAL = ah_MsgFormat("Errore inserimento riga documento.%0%1", MESSAGE())
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_RETVAL
          endif
          bTrsErr=bTrsErr or bErr_04786570
          * --- End
          Release ArrInsert
          this.w_PADRE.w_DOC_DETT.Next()     
        enddo
      endif
    endif
    if EMPTY(this.w_RETVAL) and Isalt()
      this.w_CHILDREF = "w_ALT_DETT"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_bUnderTran
      if EMPTY(this.w_RETVAL)
        * --- commit
        cp_EndTrs(.t.)
      else
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    endif
  endproc
  proc Try_047C12F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("DOC_MAST", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento documento.%0%1", this.w_RETVAL)
    else
      this.w_CHILDREF = "w_DOC_MAST"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_047BA0F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("DOC_RATE", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento rate.%0%1", this.w_RETVAL)
    else
      this.w_CHILDREF = "w_DOC_RATE"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_047B2788()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("CON_PAGA", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento dettaglio pagamenti.%0%1", this.w_RETVAL)
    else
      this.w_CHILDREF = "w_CON_PAGA"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_047A6E48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("VDATRITE", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento dati ritenute.%0%1", this.w_RETVAL)
    else
      this.w_CHILDREF = "w_VDATRITE"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_04786570()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento record dettaglio attuale
    this.w_RETVAL = InsertTable("DOC_DETT", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento riga documento.%0%1", this.w_RETVAL)
    else
      this.w_CHILDREF = "w_DOC_DETT"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FOUND = .T.
    this.w_SEARCH_IDX = 1
    do while this.w_FOUND
      local L_OLDERR, L_SRCERR
      L_OLDERR = ON("ERROR")
      ON ERROR L_SRCERR = .T.
      L_SRCERR = .F.
      this.w_IDX_OBJ = ASCAN(this.w_PADRE.AdditionalPropList, this.w_CHILDREF, this.w_SEARCH_IDX, ALEN(this.w_PADRE.AdditionalPropList,1), 4, 15)
      ON ERROR &L_OLDERR
      this.w_FOUND = !L_SRCERR and this.w_IDX_OBJ>0
      release L_OLDERR, L_SRCERR
      if this.w_FOUND
        Local cMacro
        cMacro = ALLTRIM(this.w_PADRE.AdditionalPropList(this.w_IDX_OBJ, 5) )
        this.w_RETVAL = &cMacro
        Release cMacro
        if EMPTY(this.w_RETVAL)
          if this.w_IDX_OBJ < ALEN(this.w_PADRE.AdditionalPropList,1)
            this.w_SEARCH_IDX = this.w_IDX_OBJ + 1
          else
            this.w_FOUND = .F.
          endif
        else
          this.w_RETVAL = ah_MsgFormat("Errore inserimento oggetto aggiuntivo %1.%0%2", ALLTRIM(this.w_PADRE.AdditionalPropList(this.w_IDX_OBJ, 1) ), this.w_RETVAL)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_RETVAL
          this.w_FOUND = .F.
        endif
      endif
    enddo
  endproc


  proc Init(oParentObject,pPADRE)
    this.pPADRE=pPADRE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPADRE"
endproc
