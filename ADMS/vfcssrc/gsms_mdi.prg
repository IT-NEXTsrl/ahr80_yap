* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsms_mdi                                                        *
*              Dettaglio IVA                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-11                                                      *
* Last revis.: 2014-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsms_mdi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsms_mdi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsms_mdi")
  return

* --- Class definition
define class tgsms_mdi as StdPCForm
  Width  = 605
  Height = 152
  Top    = 10
  Left   = 10
  cComment = "Dettaglio IVA"
  cPrg = "gsms_mdi"
  HelpContextID=80305001
  add object cnt as tcgsms_mdi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsms_mdi as PCContext
  w_IDSERIAL = space(10)
  w_IDCODIVA = space(10)
  w_IDFLOMAG = 0
  w_IDTOTIMP = 0
  w_IDTOTIVA = 0
  w_IDTOTSPE = 0
  proc Save(i_oFrom)
    this.w_IDSERIAL = i_oFrom.w_IDSERIAL
    this.w_IDCODIVA = i_oFrom.w_IDCODIVA
    this.w_IDFLOMAG = i_oFrom.w_IDFLOMAG
    this.w_IDTOTIMP = i_oFrom.w_IDTOTIMP
    this.w_IDTOTIVA = i_oFrom.w_IDTOTIVA
    this.w_IDTOTSPE = i_oFrom.w_IDTOTSPE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_IDSERIAL = this.w_IDSERIAL
    i_oTo.w_IDCODIVA = this.w_IDCODIVA
    i_oTo.w_IDFLOMAG = this.w_IDFLOMAG
    i_oTo.w_IDTOTIMP = this.w_IDTOTIMP
    i_oTo.w_IDTOTIVA = this.w_IDTOTIVA
    i_oTo.w_IDTOTSPE = this.w_IDTOTSPE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsms_mdi as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 605
  Height = 152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-04"
  HelpContextID=80305001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DOC_MOBT_IDX = 0
  cFile = "DOC_MOBT"
  cKeySelect = "IDSERIAL"
  cKeyWhere  = "IDSERIAL=this.w_IDSERIAL"
  cKeyDetail  = "IDSERIAL=this.w_IDSERIAL and IDCODIVA=this.w_IDCODIVA and IDFLOMAG=this.w_IDFLOMAG"
  cKeyWhereODBC = '"IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)';

  cKeyDetailWhereODBC = '"IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)';
      +'+" and IDCODIVA="+cp_ToStrODBC(this.w_IDCODIVA)';
      +'+" and IDFLOMAG="+cp_ToStrODBC(this.w_IDFLOMAG)';

  cKeyWhereODBCqualified = '"DOC_MOBT.IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsms_mdi"
  cComment = "Dettaglio IVA"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IDSERIAL = space(10)
  w_IDCODIVA = space(10)
  w_IDFLOMAG = 0
  w_IDTOTIMP = 0
  w_IDTOTIVA = 0
  w_IDTOTSPE = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsms_mdiPag1","gsms_mdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DOC_MOBT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOC_MOBT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOC_MOBT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsms_mdi'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DOC_MOBT where IDSERIAL=KeySet.IDSERIAL
    *                            and IDCODIVA=KeySet.IDCODIVA
    *                            and IDFLOMAG=KeySet.IDFLOMAG
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DOC_MOBT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MOBT_IDX,2],this.bLoadRecFilter,this.DOC_MOBT_IDX,"gsms_mdi")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOC_MOBT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOC_MOBT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOC_MOBT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IDSERIAL',this.w_IDSERIAL  )
      select * from (i_cTable) DOC_MOBT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_IDSERIAL = NVL(IDSERIAL,space(10))
        cp_LoadRecExtFlds(this,'DOC_MOBT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_IDCODIVA = NVL(IDCODIVA,space(10))
          .w_IDFLOMAG = NVL(IDFLOMAG,0)
          .w_IDTOTIMP = NVL(IDTOTIMP,0)
          .w_IDTOTIVA = NVL(IDTOTIVA,0)
          .w_IDTOTSPE = NVL(IDTOTSPE,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace IDCODIVA with .w_IDCODIVA
          replace IDFLOMAG with .w_IDFLOMAG
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_IDSERIAL=space(10)
      .w_IDCODIVA=space(10)
      .w_IDFLOMAG=0
      .w_IDTOTIMP=0
      .w_IDTOTIVA=0
      .w_IDTOTSPE=0
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOC_MOBT')
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DOC_MOBT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOC_MOBT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDSERIAL,"IDSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_IDCODIVA C(10);
      ,t_IDFLOMAG N(3);
      ,t_IDTOTIMP N(20,5);
      ,t_IDTOTIVA N(20,5);
      ,t_IDTOTSPE N(20,5);
      ,IDCODIVA C(10);
      ,IDFLOMAG N(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsms_mdibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODIVA_2_1.controlsource=this.cTrsName+'.t_IDCODIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDFLOMAG_2_2.controlsource=this.cTrsName+'.t_IDFLOMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTIMP_2_3.controlsource=this.cTrsName+'.t_IDTOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTIVA_2_4.controlsource=this.cTrsName+'.t_IDTOTIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTSPE_2_5.controlsource=this.cTrsName+'.t_IDTOTSPE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODIVA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOC_MOBT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MOBT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOC_MOBT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MOBT_IDX,2])
      *
      * insert into DOC_MOBT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOC_MOBT')
        i_extval=cp_InsertValODBCExtFlds(this,'DOC_MOBT')
        i_cFldBody=" "+;
                  "(IDSERIAL,IDCODIVA,IDFLOMAG,IDTOTIMP,IDTOTIVA"+;
                  ",IDTOTSPE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_IDSERIAL)+","+cp_ToStrODBC(this.w_IDCODIVA)+","+cp_ToStrODBC(this.w_IDFLOMAG)+","+cp_ToStrODBC(this.w_IDTOTIMP)+","+cp_ToStrODBC(this.w_IDTOTIVA)+;
             ","+cp_ToStrODBC(this.w_IDTOTSPE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOC_MOBT')
        i_extval=cp_InsertValVFPExtFlds(this,'DOC_MOBT')
        cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'IDCODIVA',this.w_IDCODIVA,'IDFLOMAG',this.w_IDFLOMAG)
        INSERT INTO (i_cTable) (;
                   IDSERIAL;
                  ,IDCODIVA;
                  ,IDFLOMAG;
                  ,IDTOTIMP;
                  ,IDTOTIVA;
                  ,IDTOTSPE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_IDSERIAL;
                  ,this.w_IDCODIVA;
                  ,this.w_IDFLOMAG;
                  ,this.w_IDTOTIMP;
                  ,this.w_IDTOTIVA;
                  ,this.w_IDTOTSPE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DOC_MOBT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MOBT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_IDCODIVA))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DOC_MOBT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and IDCODIVA="+cp_ToStrODBC(&i_TN.->IDCODIVA)+;
                 " and IDFLOMAG="+cp_ToStrODBC(&i_TN.->IDFLOMAG)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DOC_MOBT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and IDCODIVA=&i_TN.->IDCODIVA;
                      and IDFLOMAG=&i_TN.->IDFLOMAG;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_IDCODIVA))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and IDCODIVA="+cp_ToStrODBC(&i_TN.->IDCODIVA)+;
                            " and IDFLOMAG="+cp_ToStrODBC(&i_TN.->IDFLOMAG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and IDCODIVA=&i_TN.->IDCODIVA;
                            and IDFLOMAG=&i_TN.->IDFLOMAG;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace IDCODIVA with this.w_IDCODIVA
              replace IDFLOMAG with this.w_IDFLOMAG
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DOC_MOBT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DOC_MOBT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " IDTOTIMP="+cp_ToStrODBC(this.w_IDTOTIMP)+;
                     ",IDTOTIVA="+cp_ToStrODBC(this.w_IDTOTIVA)+;
                     ",IDTOTSPE="+cp_ToStrODBC(this.w_IDTOTSPE)+;
                     ",IDCODIVA="+cp_ToStrODBC(this.w_IDCODIVA)+;
                     ",IDFLOMAG="+cp_ToStrODBC(this.w_IDFLOMAG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and IDCODIVA="+cp_ToStrODBC(IDCODIVA)+;
                             " and IDFLOMAG="+cp_ToStrODBC(IDFLOMAG)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DOC_MOBT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      IDTOTIMP=this.w_IDTOTIMP;
                     ,IDTOTIVA=this.w_IDTOTIVA;
                     ,IDTOTSPE=this.w_IDTOTSPE;
                     ,IDCODIVA=this.w_IDCODIVA;
                     ,IDFLOMAG=this.w_IDFLOMAG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and IDCODIVA=&i_TN.->IDCODIVA;
                                      and IDFLOMAG=&i_TN.->IDFLOMAG;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOC_MOBT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MOBT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_IDCODIVA))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DOC_MOBT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and IDCODIVA="+cp_ToStrODBC(&i_TN.->IDCODIVA)+;
                            " and IDFLOMAG="+cp_ToStrODBC(&i_TN.->IDFLOMAG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and IDCODIVA=&i_TN.->IDCODIVA;
                              and IDFLOMAG=&i_TN.->IDFLOMAG;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_IDCODIVA))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOC_MOBT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MOBT_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODIVA_2_1.value==this.w_IDCODIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODIVA_2_1.value=this.w_IDCODIVA
      replace t_IDCODIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODIVA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDFLOMAG_2_2.RadioValue()==this.w_IDFLOMAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDFLOMAG_2_2.SetRadio()
      replace t_IDFLOMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDFLOMAG_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTIMP_2_3.value==this.w_IDTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTIMP_2_3.value=this.w_IDTOTIMP
      replace t_IDTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTIMP_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTIVA_2_4.value==this.w_IDTOTIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTIVA_2_4.value=this.w_IDTOTIVA
      replace t_IDTOTIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTIVA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTSPE_2_5.value==this.w_IDTOTSPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTSPE_2_5.value=this.w_IDTOTSPE
      replace t_IDTOTSPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTOTSPE_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'DOC_MOBT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_IDCODIVA))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_IDCODIVA)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_IDCODIVA=space(10)
      .w_IDFLOMAG=0
      .w_IDTOTIMP=0
      .w_IDTOTIVA=0
      .w_IDTOTSPE=0
    endwith
    this.DoRTCalc(1,6,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_IDCODIVA = t_IDCODIVA
    this.w_IDFLOMAG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDFLOMAG_2_2.RadioValue(.t.)
    this.w_IDTOTIMP = t_IDTOTIMP
    this.w_IDTOTIVA = t_IDTOTIVA
    this.w_IDTOTSPE = t_IDTOTSPE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_IDCODIVA with this.w_IDCODIVA
    replace t_IDFLOMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDFLOMAG_2_2.ToRadio()
    replace t_IDTOTIMP with this.w_IDTOTIMP
    replace t_IDTOTIVA with this.w_IDTOTIVA
    replace t_IDTOTSPE with this.w_IDTOTSPE
    if i_srv='A'
      replace IDCODIVA with this.w_IDCODIVA
      replace IDFLOMAG with this.w_IDFLOMAG
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsms_mdiPag1 as StdContainer
  Width  = 601
  height = 152
  stdWidth  = 601
  stdheight = 152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oStr_1_2 as StdString with uid="XWWAROHHBH",Visible=.t., Left=11, Top=8,;
    Alignment=0, Width=73, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="SKXBIQVAGV",Visible=.t., Left=97, Top=8,;
    Alignment=0, Width=59, Height=18,;
    Caption="Omaggio"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="XXPPOQJHKM",Visible=.t., Left=162, Top=8,;
    Alignment=0, Width=134, Height=18,;
    Caption="Imponibile Aliquota"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="LXCDSRNBUJ",Visible=.t., Left=304, Top=8,;
    Alignment=0, Width=131, Height=18,;
    Caption="Imposta Aliquota"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="HRMZYFURWJ",Visible=.t., Left=443, Top=8,;
    Alignment=0, Width=134, Height=18,;
    Caption="Totale Spese "  ;
  , bGlobalFont=.t.

  add object oBox_1_7 as StdBox with uid="OCPMCUQWIP",left=7, top=6, width=583,height=22

  add object oBox_1_8 as StdBox with uid="CVMAZJUBZD",left=92, top=5, width=2,height=138

  add object oBox_1_9 as StdBox with uid="YTDMSTDWHB",left=159, top=5, width=2,height=138

  add object oBox_1_10 as StdBox with uid="DQSJVTJNQX",left=299, top=5, width=2,height=138

  add object oBox_1_11 as StdBox with uid="SFCVCJYCFJ",left=437, top=5, width=2,height=138

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=28,;
    width=579+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=29,width=578+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsms_mdiBodyRow as CPBodyRowCnt
  Width=569
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oIDCODIVA_2_1 as StdTrsField with uid="BWSPRBFLBK",rtseq=2,rtrep=.t.,;
    cFormVar="w_IDCODIVA",value=space(10),isprimarykey=.t.,;
    HelpContextID = 120971833,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=-2, Top=0, InputMask=replicate('X',10)

  add object oIDFLOMAG_2_2 as StdTrsCombo with uid="FXFUQUEUTF",rtrep=.t.,;
    cFormVar="w_IDFLOMAG", RowSource=""+"Normale,"+"Sco. merce,"+"Om. imp.,"+"Om. imp.+IVA" , ;
    HelpContextID = 225922509,;
    Height=22, Width=59, Left=87, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIDFLOMAG_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IDFLOMAG,&i_cF..t_IDFLOMAG),this.value)
    return(iif(xVal =1,1,;
    iif(xVal =2,2,;
    iif(xVal =3,3,;
    iif(xVal =4,4,;
    0)))))
  endfunc
  func oIDFLOMAG_2_2.GetRadio()
    this.Parent.oContained.w_IDFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oIDFLOMAG_2_2.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_IDFLOMAG==1,1,;
      iif(this.Parent.oContained.w_IDFLOMAG==2,2,;
      iif(this.Parent.oContained.w_IDFLOMAG==3,3,;
      iif(this.Parent.oContained.w_IDFLOMAG==4,4,;
      0)))))
  endfunc

  func oIDFLOMAG_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIDTOTIMP_2_3 as StdTrsField with uid="BXWYJFGBBH",rtseq=4,rtrep=.t.,;
    cFormVar="w_IDTOTIMP",value=0,;
    HelpContextID = 164310486,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=134, Left=153, Top=0, cSayPict=["99999999999999.99999"], cGetPict=["99999999999999.99999"]

  add object oIDTOTIVA_2_4 as StdTrsField with uid="CAOSVJDBTT",rtseq=5,rtrep=.t.,;
    cFormVar="w_IDTOTIVA",value=0,;
    HelpContextID = 104124985,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=134, Left=292, Top=0, cSayPict=["99999999999999.99999"], cGetPict=["99999999999999.99999"]

  add object oIDTOTSPE_2_5 as StdTrsField with uid="RGPCCKBYOC",rtseq=6,rtrep=.t.,;
    cFormVar="w_IDTOTSPE",value=0,;
    HelpContextID = 63647179,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=134, Left=430, Top=0, cSayPict=["99999999999999.99999"], cGetPict=["99999999999999.99999"]
  add object oLast as LastKeyMover
  * ---
  func oIDCODIVA_2_1.When()
    return(.t.)
  proc oIDCODIVA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oIDCODIVA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsms_mdi','DOC_MOBT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IDSERIAL=DOC_MOBT.IDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
