public oADMS_Timer
oADMS_Timer = CREATEOBJECT("adms_AutoStartTimer")


Define class adms_AutoStartTimer AS Timer
	interval=5*1000
	oThis = null
	enabled = .t.
	nWaitStartup = 0
	proc Init
		this.oThis = this
	endproc
	
	proc Timer
		if i_CodUte > 0 and '..\ADMS\VFCSSRC' $ UPPER(SET("PATH"))
			this.nWaitStartup = this.nWaitStartup + 1
			if this.nWaitStartup > 1
				this.enabled = .f.
				this.oThis = null
				gsms_kgd('A')
				release oADMS_Timer
			endif
		endif
	endproc
ENDDEFINE