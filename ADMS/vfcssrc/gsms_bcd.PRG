* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsms_bcd                                                        *
*              Calcola campi dettaglio                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-18                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPADRE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsms_bcd",oParentObject,m.pPADRE)
return(i_retval)

define class tgsms_bcd as StdBatch
  * --- Local variables
  pPADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.pPADRE.w_DOC_DETT.GoTop()     
    do while !this.pPADRE.w_DOC_DETT.Eof()
      this.pPADRE.w_DOC_DETT.MVVALMAG = CAVALMAG(this.pPADRE.w_MVFLSCOR, this.pPADRE.w_DOC_DETT.MVVALRIG, this.pPADRE.w_DOC_DETT.MVIMPSCO, this.pPADRE.w_DOC_DETT.MVIMPACC, this.oParentObject.w_PERIVA, this.oParentObject.w_DECTOT, this.pPADRE.w_MVCODIVE, this.oParentObject.w_PERIVE )
      this.pPADRE.w_DOC_DETT.SaveCurrentRecord()     
      this.pPADRE.w_DOC_DETT.Next()     
    enddo
  endproc


  proc Init(oParentObject,pPADRE)
    this.pPADRE=pPADRE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPADRE"
endproc
