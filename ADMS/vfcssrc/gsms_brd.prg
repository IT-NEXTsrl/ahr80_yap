* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsms_brd                                                        *
*              Ricalcolo totali documenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_72]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-11                                                      *
* Last revis.: 2015-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Documento,pNOBFA,pNORAT,pNODET,pNOCUR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsms_brd",oParentObject,m.Documento,m.pNOBFA,m.pNORAT,m.pNODET,m.pNOCUR)
return(i_retval)

define class tgsms_brd as StdBatch
  * --- Local variables
  Documento = space(10)
  pNOBFA = .f.
  pNORAT = .f.
  pNODET = .f.
  pNOCUR = .f.
  DR = space(10)
  w_TDRIPCON = space(1)
  w_MVNUMDOC = 0
  w_MVPRD = space(2)
  w_MVFLINTE = space(1)
  w_MVCODESE = space(4)
  w_MVALFDOC = space(10)
  w_MVFLVEAC = space(1)
  w_MVTCAMAG = space(5)
  w_MVFLACCO = space(1)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTIPCON = space(1)
  w_MVTFRAGG = space(1)
  w_MVNUMREG = 0
  w_MVCODUTE = 0
  w_MVTIPDOC = space(5)
  w_MVCAUCON = space(5)
  w_DATDIV = ctod("  /  /  ")
  w_MVDATREG = ctod("  /  /  ")
  w_MVCLADOC = space(2)
  w_MVSERIAL = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_MVCONTRO = space(15)
  w_MVSPEINC = 0
  w_CPROWORD = 0
  w_MVCONTRA = space(15)
  w_MVSERRIF = space(10)
  w_MVIVAINC = space(5)
  w_MVNUMRIF = 0
  w_MVCONIND = space(15)
  w_MVROWRIF = 0
  w_MVFLRINC = space(1)
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVPERPRO = 0
  w_MVSPEIMB = 0
  w_MVCODICE = space(41)
  w_MVQTAMOV = 0
  w_MVIMPPRO = 0
  w_MVIVAIMB = space(5)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVDESART = space(40)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVDESSUP = space(10)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVUNIMIS = space(3)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVCATCON = space(5)
  w_MVSCONT4 = 0
  w_MVFLCASC = space(1)
  w_MVIVABOL = space(5)
  w_MVCODCLA = space(3)
  w_MVFLOMAG = space(1)
  w_MVFLORDI = space(1)
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCODCON = space(15)
  w_MVCODIVA = space(5)
  w_MVFLIMPE = space(1)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVCODIVE = space(5)
  w_MVCODPAG = space(5)
  w_MVFLRISE = space(1)
  w_MVVALNAZ = space(3)
  w_MVCODAGE = space(5)
  w_MVCODBAN = space(10)
  w_MVFLELGM = space(1)
  w_MVCAOVAL = 0
  w_MVTCONTR = space(15)
  w_MVCODVAL = space(3)
  w_MVMOLSUP = 0
  w_MVVALRIG = 0
  w_MVTCOCEN = space(15)
  w_MVPESNET = 0
  w_MVNUMCOL = 0
  w_MVIMPACC = 0
  w_MVTCOMME = space(15)
  w_MVFLTRAS = space(1)
  w_MVIMPSCO = 0
  w_MVTCOLIS = space(5)
  w_MVNOMENC = space(8)
  w_MVSCONTI = 0
  w_MVVALMAG = 0
  w_MVACIVA1 = space(5)
  w_MVUMSUPP = space(3)
  w_MVIMPARR = 0
  w_MVIMPNAZ = 0
  w_MVACIVA2 = space(5)
  w_MVAIMPN1 = 0
  w_MVAIMPS1 = 0
  w_MVAFLOM1 = space(1)
  w_MVACIVA3 = space(5)
  w_MVAIMPN2 = 0
  w_MVAIMPS2 = 0
  w_MVAFLOM2 = space(1)
  w_MVACIVA4 = space(5)
  w_MVAIMPN3 = 0
  w_MVAIMPS3 = 0
  w_MVAFLOM3 = space(1)
  w_MVACIVA5 = space(5)
  w_MVAIMPN4 = 0
  w_MVAIMPS4 = 0
  w_MVAFLOM4 = space(1)
  w_MVACIVA6 = space(5)
  w_MVAIMPN5 = 0
  w_MVAIMPS5 = 0
  w_MVAFLOM5 = space(1)
  w_APPART = space(10)
  w_MVAIMPN6 = 0
  w_MVAIMPS6 = 0
  w_MVAFLOM6 = space(1)
  w_SPEINC = 0
  w_DECTOT = 0
  w_PERIVA = 0
  w_PERIVE = 0
  w_BOLIVE = space(1)
  w_PEIINC = 0
  w_SPEIMB = 0
  w_BOLESE = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_SPETRA = 0
  w_BOLSUP = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_SPEBOL = 0
  w_BOLCAM = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_ACCONT = 0
  w_BOLARR = 0
  w_GIORN1 = 0
  w_PEITRA = 0
  w_IVAINC = space(5)
  w_BOLMIN = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_IVAIMB = space(5)
  w_TOTMERCE = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_IVATRA = space(5)
  w_TOTALE = 0
  w_CLBOLFAT = space(1)
  w_IVABOL = space(5)
  w_TOTIMPON = 0
  w_MVFLSCOR = space(1)
  w_ACQINT = space(1)
  w_CAOVAL = 0
  w_TOTIMPOS = 0
  w_MVTOTRIT = 0
  w_IMPARR = 0
  w_TOTFATTU = 0
  w_MVTOTENA = 0
  w_FLFOBO = space(1)
  w_RSNUMRAT = 0
  w_CODNAZ = space(3)
  w_MVACCPRE = 0
  w_FLINTR = space(1)
  w_RSDATRAT = ctod("  /  /  ")
  w_MVTDTEVA = ctod("  /  /  ")
  w_TIPDOC = space(2)
  w_RSIMPRAT = 0
  w_MVRIFDIC = space(10)
  w_MVRITPRE = 0
  w_TIPREG = space(1)
  w_RSMODPAG = space(10)
  w_MVFLSALD = space(1)
  w_MVCODCOM = space(15)
  w_MVIMPCOM = 0
  w_MVIMPAC2 = 0
  w_MVCODORN = space(15)
  w_XCONORN = space(15)
  w_RSFLPROV = space(1)
  w_MVFLFOSC = space(1)
  w_MVVALULT = 0
  w_SPEACC = 0
  w_PREC = 0
  w_TIMPACC = 0
  w_PRES = 0
  w_TIMPSCO = 0
  w_CODIVA = space(3)
  w_DATCOM = ctod("  /  /  ")
  w_CAONAZ = 0
  w_SPEACC_IVA = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  DOC_RATE_idx=0
  VALUTE_idx=0
  CONTI_idx=0
  VOCIIVA_idx=0
  KEY_ARTI_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- RICALCOLA TOTALE DOCUMENTO CON LA RIPARTIZIONE DEGLI SCONTI E SPESE ACCESSORIE
    * --- Riempie il cursore GeneApp con le righe del documento e calcola i totali realizzandola ripartizione delle spese accessorie e degli sconti.
    * --- --
    * --- Lanciato da add on app mobile
    * --- Parametri
    *     ===========
    *     Documento = seriale documento da elaborare
    * --- Se true non esegue BFA
    * --- Se true non esegue aggiornamento rate
    * --- Se true non esegue aggiornamento dettaglio
    * --- se true non chiude il cursore GENEAPP
    * --- Inizializza il vettore delle Rate Scadenze
    DIMENSION DR[51, 9]
    FOR i = 1 TO 51
    DR[i, 1] = cp_CharToDate("  -  -  ")
    DR[i, 2] = 0
    DR[i, 3] = "  "
    DR[i, 9] = " "
    ENDFOR
    * --- Inizializza le variabili utilizzate nei calcoli
    this.w_TOTMERCE = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVFLSCOR = " "
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_TIPDOC = "  "
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_TIPREG = " "
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVCODCON = SPACE(15)
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVFLVEAC = " "
    this.w_MVCAUCON = SPACE(5)
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTIPCON = " "
    this.w_MVTOTRIT = 0
    this.w_MVACCPRE = 0
    this.w_ACQINT = "N"
    this.w_MVTOTENA = 0
    this.w_FLFOBO = " "
    this.w_MVRITPRE = 0
    this.w_MVFLSALD = " "
    * --- Legge i dati di testata del documento
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.Documento);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            MVSERIAL = this.Documento;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVSPEINC = NVL(cp_ToDate(_read_.MVSPEINC),cp_NullValue(_read_.MVSPEINC))
      this.w_MVIVAINC = NVL(cp_ToDate(_read_.MVIVAINC),cp_NullValue(_read_.MVIVAINC))
      this.w_MVFLRINC = NVL(cp_ToDate(_read_.MVFLRINC),cp_NullValue(_read_.MVFLRINC))
      this.w_MVSPEIMB = NVL(cp_ToDate(_read_.MVSPEIMB),cp_NullValue(_read_.MVSPEIMB))
      this.w_MVIVAIMB = NVL(cp_ToDate(_read_.MVIVAIMB),cp_NullValue(_read_.MVIVAIMB))
      this.w_MVFLRIMB = NVL(cp_ToDate(_read_.MVFLRIMB),cp_NullValue(_read_.MVFLRIMB))
      this.w_MVSPETRA = NVL(cp_ToDate(_read_.MVSPETRA),cp_NullValue(_read_.MVSPETRA))
      this.w_MVIVATRA = NVL(cp_ToDate(_read_.MVIVATRA),cp_NullValue(_read_.MVIVATRA))
      this.w_MVFLRTRA = NVL(cp_ToDate(_read_.MVFLRTRA),cp_NullValue(_read_.MVFLRTRA))
      this.w_MVSPEBOL = NVL(cp_ToDate(_read_.MVSPEBOL),cp_NullValue(_read_.MVSPEBOL))
      this.w_MVIVABOL = NVL(cp_ToDate(_read_.MVIVABOL),cp_NullValue(_read_.MVIVABOL))
      this.w_MVACCONT = NVL(cp_ToDate(_read_.MVACCONT),cp_NullValue(_read_.MVACCONT))
      this.w_MVIMPARR = NVL(cp_ToDate(_read_.MVIMPARR),cp_NullValue(_read_.MVIMPARR))
      this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      this.w_MVCODIVE = NVL(cp_ToDate(_read_.MVCODIVE),cp_NullValue(_read_.MVCODIVE))
      this.w_MVCODAGE = NVL(cp_ToDate(_read_.MVCODAGE),cp_NullValue(_read_.MVCODAGE))
      this.w_MVCODPAG = NVL(cp_ToDate(_read_.MVCODPAG),cp_NullValue(_read_.MVCODPAG))
      this.w_MVCODBAN = NVL(cp_ToDate(_read_.MVCODBAN),cp_NullValue(_read_.MVCODBAN))
      this.w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
      this.w_MVSCOCL1 = NVL(cp_ToDate(_read_.MVSCOCL1),cp_NullValue(_read_.MVSCOCL1))
      this.w_MVSCOCL2 = NVL(cp_ToDate(_read_.MVSCOCL2),cp_NullValue(_read_.MVSCOCL2))
      this.w_MVSCOPAG = NVL(cp_ToDate(_read_.MVSCOPAG),cp_NullValue(_read_.MVSCOPAG))
      this.w_MVVALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
      this.w_MVTCONTR = NVL(cp_ToDate(_read_.MVTCONTR),cp_NullValue(_read_.MVTCONTR))
      this.w_MVTCOLIS = NVL(cp_ToDate(_read_.MVTCOLIS),cp_NullValue(_read_.MVTCOLIS))
      this.w_MVCAOVAL = NVL(cp_ToDate(_read_.MVCAOVAL),cp_NullValue(_read_.MVCAOVAL))
      this.w_MVDATDIV = NVL(cp_ToDate(_read_.MVDATDIV),cp_NullValue(_read_.MVDATDIV))
      this.w_MVFLSCOR = NVL(cp_ToDate(_read_.MVFLSCOR),cp_NullValue(_read_.MVFLSCOR))
      this.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
      this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
      this.w_MVCAUCON = NVL(cp_ToDate(_read_.MVCAUCON),cp_NullValue(_read_.MVCAUCON))
      this.w_MVTOTRIT = NVL(cp_ToDate(_read_.MVTOTRIT),cp_NullValue(_read_.MVTOTRIT))
      this.w_MVTOTENA = NVL(cp_ToDate(_read_.MVTOTENA),cp_NullValue(_read_.MVTOTENA))
      this.w_MVACCPRE = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
      this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_MVFLSALD = NVL(cp_ToDate(_read_.MVFLSALD),cp_NullValue(_read_.MVFLSALD))
      this.w_MVDATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      this.w_MVCODORN = NVL(cp_ToDate(_read_.MVCODORN),cp_NullValue(_read_.MVCODORN))
      this.w_MVFLFOSC = NVL(cp_ToDate(_read_.MVFLFOSC),cp_NullValue(_read_.MVFLFOSC))
      this.w_MVSCONTI = NVL(cp_ToDate(_read_.MVSCONTI),cp_NullValue(_read_.MVSCONTI))
      this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDRIPCON"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDRIPCON;
        from (i_cTable) where;
            TDTIPDOC = this.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TDRIPCON = NVL(cp_ToDate(_read_.TDRIPCON),cp_NullValue(_read_.TDRIPCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_MVCODIVE)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVCODIVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_XCONORN = IIF(Not Empty(this.w_MVCODORN) And g_XCONDI = "S",this.w_MVCODORN,this.w_MVCODCON)
    * --- Crea il cursore delle righe del documento e lo rende editabile
    vq_exec("QUERY\GSAR_BRD",this,"GeneApp")
    wrcursor("GeneApp")
    * --- Calcola le valorizzazioni del documento e le scadenze
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Chiusura Cursore
    if Not this.pNOCUR
      if used("GeneApp")
        select GeneApp
        use
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili Locali
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo totali documento
    * --- Calcola totale documento e totale merce
    * --- Legge i Dati Associati alla Valuta
    this.w_DECTOT = 0
    this.w_BOLESE = 0
    this.w_BOLSUP = 0
    this.w_BOLCAM = 0
    this.w_BOLARR = 0
    this.w_BOLMIN = 0
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM;
        from (i_cTable) where;
            VACODVAL = this.w_MVCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
      this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
      this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
      this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
      this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    select GeneApp
    go top
    scan
    if EMPTY(NVL(t_MVTIPRIG," "))
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CA__TIPO"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(t_MVCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CA__TIPO;
          from (i_cTable) where;
              CACODICE = t_MVCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      REPLACE t_MVTIPRIG WITH this.w_MVTIPRIG
    endif
    if NVL(t_MVVALRIG,0)=0
      this.w_MVVALRIG = CAVALRIG(t_MVPREZZO,t_MVQTAMOV, Nvl(t_MVSCONT1,0),Nvl(t_MVSCONT2,0),Nvl(t_MVSCONT3,0),Nvl(t_MVSCONT4,0),this.w_DECTOT)
      REPLACE t_MVVALRIG WITH this.w_MVVALRIG
    endif
    this.w_TOTALE = this.w_TOTALE + GeneApp.t_MVVALRIG
    this.w_TOTMERCE = this.w_TOTMERCE + IIF( GeneApp.t_MVFLOMAG="X", GeneApp.t_MVVALRIG, 0)
    endscan
    * --- Aggiorna le Spese Accessorie e gli Sconti Finali
    this.w_MVSCONTI = IIF(this.w_MVFLFOSC="S",this.w_MVSCONTI,Calsco(this.w_TOTMERCE, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT))
    * --- Ripartisce Spese Accessorie e Sconti
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_DATCOM = IIF(Empty(this.w_MVDATDOC),this.w_MVDATREG, this.w_MVDATDOC)
    * --- Letture delle percentuali Iva delle Spese
    if NOT EMPTY(this.w_MVIVAINC)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAINC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAINC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEIINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLINC = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_BOLINC = " "
      this.w_PEIINC = 0
    endif
    if NOT EMPTY(this.w_MVIVAIMB)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAIMB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAIMB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEIIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLIMB = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_BOLIMB = " "
      this.w_PEIIMB = 0
    endif
    if NOT EMPTY(this.w_MVIVATRA)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVATRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVATRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEITRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLTRA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_PEITRA = 0
      this.w_BOLTRA = " "
    endif
    if NOT EMPTY(this.w_MVIVABOL)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVABOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVABOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BOLBOL = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_BOLBOL = " "
    endif
    * --- Totale Spese da Ripartire
    *     Nel caso di Scorporo piede fattura e codice Iva spese presente, calcolo in w_SPEACC_IVA 
    *     la somma delle spese scorporate della loro Iva.
    this.w_SPEACC_IVA = 0
    this.w_SPEACC = 0
    if Not Empty(this.w_MVIVAINC) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = IIF(this.w_MVFLRINC="S", Calnet( this.w_MVSPEINC, this.w_PEIINC, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = IIF(this.w_MVFLRINC="S", this.w_MVSPEINC, 0)
      endif
    else
      this.w_SPEACC = IIF(this.w_MVFLRINC="S", this.w_MVSPEINC, 0)
    endif
    if Not Empty(this.w_MVIVAIMB) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRIMB="S", Calnet( this.w_MVSPEIMB, this.w_PEIIMB, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRIMB="S", this.w_MVSPEIMB, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRIMB="S", this.w_MVSPEIMB, 0)
    endif
    if Not Empty(this.w_MVIVATRA) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRTRA="S", Calnet( this.w_MVSPETRA, this.w_PEITRA, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRTRA="S", this.w_MVSPETRA, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRTRA="S", this.w_MVSPETRA, 0)
    endif
    if this.w_MVFLVEAC="A"
      * --- Se Acquisti, Ripartisce Spese Bolli e Arrotondamenti
      this.w_SPEACC = this.w_SPEACC+(this.w_MVSPEBOL+this.w_MVIMPARR)
    endif
    * --- Totalizzatori
    this.w_TIMPACC = 0
    this.w_TIMPSCO = 0
    * --- Lancio il Batch per la ripartizione delle spese e degli sconti
    GSAR_BRS(this,"B", "GeneApp", this.w_MVFLVEAC, this.w_MVFLSCOR, this.w_SPEACC, this.w_MVSCONTI, this.w_TOTMERCE, this.w_MVCODIVE, this.w_MVCAOVAL, this.w_DATCOM, this.w_CAONAZ, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_SPEACC_IVA, , this.w_TDRIPCON )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Legge i valori dagli archivi collegati
    this.w_GIORN1 = 0
    this.w_GIORN2 = 0
    this.w_MESE1 = 0
    this.w_MESE2 = 0
    this.w_CLBOLFAT = " "
    this.w_GIOFIS = 0
    this.w_FLINTR = " "
    this.w_CODNAZ = g_CODNAZ
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANBOLFAT,AFFLINTR,ANGIOFIS,ANNAZION"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_XCONORN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANBOLFAT,AFFLINTR,ANGIOFIS,ANNAZION;
        from (i_cTable) where;
            ANTIPCON = this.w_MVTIPCON;
            and ANCODICE = this.w_XCONORN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
      this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
      this.w_MESE1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
      this.w_MESE2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
      this.w_CLBOLFAT = NVL(cp_ToDate(_read_.ANBOLFAT),cp_NullValue(_read_.ANBOLFAT))
      this.w_FLINTR = NVL(cp_ToDate(_read_.AFFLINTR),cp_NullValue(_read_.AFFLINTR))
      this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
      this.w_CODNAZ = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Documento di Acquisto INTRA
    this.w_ACQINT = IIF(this.w_MVFLVEAC="A" AND this.w_FLINTR="S", "S", "N")
    * --- Calcoli Finali
    if Not this.pNOBFA
      GSAR_BFA(this,"D")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Cicla sulle Righe Documento
    if Not this.pNODET
      SELECT GeneApp
      GO TOP
      SCAN FOR t_MVTIPRIG <>" " AND NOT EMPTY(t_MVCODICE)
      * --- Scrive il Dettaglio
      this.w_MVSERIAL = t_MVSERIAL
      this.w_MVIMPSCO = t_MVIMPSCO
      this.w_CPROWNUM = t_CPROWNUM
      this.w_MVFLOMAG = t_MVFLOMAG
      this.w_MVNUMRIF = t_MVNUMRIF
      this.w_MVCODIVA = t_MVCODIVA
      this.w_MVTIPRIG = t_MVTIPRIG
      this.w_MVCODICE = t_MVCODICE
      this.w_MVPERPRO = t_MVPERPRO
      this.w_MVCODART = t_MVCODART
      this.w_MVIMPPRO = t_MVIMPPRO
      this.w_MVDESART = t_MVDESART
      this.w_MVSERRIF = t_MVSERRIF
      this.w_MVDESSUP = t_MVDESSUP
      this.w_MVROWRIF = t_MVROWRIF
      this.w_MVUNIMIS = t_MVUNIMIS
      this.w_MVPESNET = t_MVPESNET
      this.w_MVCATCON = t_MVCATCON
      this.w_MVFLTRAS = t_MVFLTRAS
      this.w_MVCONTRO = t_MVCONTRO
      this.w_MVNOMENC = t_MVNOMENC
      this.w_MVCODCLA = t_MVCODCLA
      this.w_MVUMSUPP = t_MVUMSUPP
      this.w_MVCONTRA = t_MVCONTRA
      this.w_MVMOLSUP = t_MVMOLSUP
      this.w_MVCODLIS = t_MVCODLIS
      this.w_MVNUMCOL = t_MVNUMCOL
      this.w_MVQTAMOV = t_MVQTAMOV
      this.w_MVQTAUM1 = t_MVQTAUM1
      this.w_MVCONIND = t_MVCONIND
      this.w_MVPREZZO = t_MVPREZZO
      this.w_MVVALMAG = t_MVVALMAG
      this.w_MVSCONT1 = t_MVSCONT1
      this.w_MVIMPNAZ = t_MVIMPNAZ
      this.w_MVSCONT2 = t_MVSCONT2
      this.w_MVIMPACC = t_MVIMPACC
      this.w_MVSCONT3 = t_MVSCONT3
      this.w_MVSCONT4 = t_MVSCONT4
      this.w_MVCODCOM = t_MVCODCOM
      this.w_MVIMPCOM = t_MVIMPCOM
      this.w_MVIMPAC2 = t_MVIMPAC2
      this.w_MVVALRIG = IIF(t_MVVALRIG<>0,t_MVVALRIG,CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT))
      this.w_MVVALULT = t_MVVALULT
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
        +",MVCODART ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
        +",MVCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
        +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
        +",MVCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
        +",MVCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
        +",MVCODLIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
        +",MVCONIND ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
        +",MVCONTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
        +",MVCONTRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRO),'DOC_DETT','MVCONTRO');
        +",MVDESART ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
        +",MVDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
        +",MVFLOMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
        +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
        +",MVIMPAC2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPAC2),'DOC_DETT','MVIMPAC2');
        +",MVIMPACC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
        +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
        +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
        +",MVIMPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
        +",MVIMPSCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
        +",MVMOLSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
        +",MVNOMENC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
        +",MVNUMCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMCOL),'DOC_DETT','MVNUMCOL');
        +",MVPERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
        +",MVPESNET ="+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
        +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
        +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
        +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
        +",MVROWRIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'DOC_DETT','MVROWRIF');
        +",MVSCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
        +",MVSCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
        +",MVSCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
        +",MVSCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
        +",MVSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'DOC_DETT','MVSERRIF');
        +",MVTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
        +",MVUMSUPP ="+cp_NullLink(cp_ToStrODBC(this.w_MVUMSUPP),'DOC_DETT','MVUMSUPP');
        +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
        +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
        +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
        +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALULT),'DOC_DETT','MVVALULT');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MVCATCON = this.w_MVCATCON;
            ,MVCODART = this.w_MVCODART;
            ,MVCODCLA = this.w_MVCODCLA;
            ,MVCODCOM = this.w_MVCODCOM;
            ,MVCODICE = this.w_MVCODICE;
            ,MVCODIVA = this.w_MVCODIVA;
            ,MVCODLIS = this.w_MVCODLIS;
            ,MVCONIND = this.w_MVCONIND;
            ,MVCONTRA = this.w_MVCONTRA;
            ,MVCONTRO = this.w_MVCONTRO;
            ,MVDESART = this.w_MVDESART;
            ,MVDESSUP = this.w_MVDESSUP;
            ,MVFLOMAG = this.w_MVFLOMAG;
            ,MVFLTRAS = this.w_MVFLTRAS;
            ,MVIMPAC2 = this.w_MVIMPAC2;
            ,MVIMPACC = this.w_MVIMPACC;
            ,MVIMPCOM = this.w_MVIMPCOM;
            ,MVIMPNAZ = this.w_MVIMPNAZ;
            ,MVIMPPRO = this.w_MVIMPPRO;
            ,MVIMPSCO = this.w_MVIMPSCO;
            ,MVMOLSUP = this.w_MVMOLSUP;
            ,MVNOMENC = this.w_MVNOMENC;
            ,MVNUMCOL = this.w_MVNUMCOL;
            ,MVPERPRO = this.w_MVPERPRO;
            ,MVPESNET = this.w_MVPESNET;
            ,MVPREZZO = this.w_MVPREZZO;
            ,MVQTAMOV = this.w_MVQTAMOV;
            ,MVQTAUM1 = this.w_MVQTAUM1;
            ,MVROWRIF = this.w_MVROWRIF;
            ,MVSCONT1 = this.w_MVSCONT1;
            ,MVSCONT2 = this.w_MVSCONT2;
            ,MVSCONT3 = this.w_MVSCONT3;
            ,MVSCONT4 = this.w_MVSCONT4;
            ,MVSERRIF = this.w_MVSERRIF;
            ,MVTIPRIG = this.w_MVTIPRIG;
            ,MVUMSUPP = this.w_MVUMSUPP;
            ,MVUNIMIS = this.w_MVUNIMIS;
            ,MVVALMAG = this.w_MVVALMAG;
            ,MVVALRIG = this.w_MVVALRIG;
            ,MVVALULT = this.w_MVVALULT;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;
            and CPROWNUM = this.w_CPROWNUM;
            and MVNUMRIF = this.w_MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      SELECT GeneApp
      ENDSCAN
    endif
    * --- Cicla Sul Vettore delle Rate (Calcolato in GSAR_BFA)
    if Not this.pNORAT
      * --- Prima di inserire le nuove rate cancella le vecchie rate e imposta il flag "scadenze confermate"
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLSCAF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLSCAF');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.Documento);
               )
      else
        update (i_cTable) set;
            MVFLSCAF = " ";
            &i_ccchkf. ;
         where;
            MVSERIAL = this.Documento;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Delete from DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RSSERIAL = "+cp_ToStrODBC(this.Documento);
               )
      else
        delete from (i_cTable) where;
              RSSERIAL = this.Documento;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      FOR L_i=1 TO 50
      if NOT EMPTY(DR[L_i, 1]) AND NOT EMPTY(DR[L_i, 2])
        this.w_RSNUMRAT = L_i
        this.w_RSDATRAT = DR[L_i, 1]
        this.w_RSIMPRAT = DR[L_i, 2]
        this.w_RSMODPAG = DR[L_i, 3]
        this.w_RSFLPROV = DR[L_i, 9]
        * --- Aggiorna rate
        * --- Insert into DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+",RSFLPROV"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.Documento),'DOC_RATE','RSSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSFLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSFLPROV),'DOC_RATE','RSFLPROV');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.Documento,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP'," ",'RSFLPROV',this.w_RSFLPROV)
          insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP,RSFLPROV &i_ccchkf. );
             values (;
               this.Documento;
               ,this.w_RSNUMRAT;
               ,this.w_RSDATRAT;
               ,this.w_RSIMPRAT;
               ,this.w_RSMODPAG;
               ," ";
               ,this.w_RSFLPROV;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      NEXT
    endif
  endproc


  proc Init(oParentObject,Documento,pNOBFA,pNORAT,pNODET,pNOCUR)
    this.Documento=Documento
    this.pNOBFA=pNOBFA
    this.pNORAT=pNORAT
    this.pNODET=pNODET
    this.pNOCUR=pNOCUR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_RATE'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='TIP_DOCU'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Documento,pNOBFA,pNORAT,pNODET,pNOCUR"
endproc
