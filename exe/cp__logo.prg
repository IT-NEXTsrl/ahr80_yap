private dy,dx
dy = 3
dx = wcols()-80
if g_APPLICATION = "ADHOC REVOLUTION"
  if not isalt()
    if i_vidgrf .and. file("BMP\AHR.BMP")
      activate screen
      if i_SplashImage
           @ 0,0 say "BMP\AHR.BMP" bitmap size wrows(),wcols() center
      endif   
    else
      @ 3,10 say "ADHOC REVOLUTION"
    endif
  endif
else   
  if p_Application='APA'
    if i_vidgrf .and. file("BMP\ahpa.jpg")
      activate screen
      if i_SplashImage
           @ 0,0 say "BMP\ahpa.jpg" bitmap size wrows(),wcols() center
      endif   
    else
      @ 3,10 say "ad hoc P.A."
    endif
  else
    if i_vidgrf .and. file("BMP\ahenter.jpg")
      activate screen
      if i_SplashImage
           @ 0,0 say "BMP\ahenter.jpg" bitmap size wrows(),wcols() center
      endif   
    else
      @ 3,10 say "ad hoc ENTERPRISE"
    endif
  endif
endif
return
