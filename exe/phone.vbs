' Parametri
' cExe = Eseguibile da lanciare
' cCNF = File CNF
'cSilent = Stringa per connessione silente
'cApplication = Applicazione da utilizzare
'cFileList = Elenco parametri
dim nParam
nParam = Wscript.Arguments.count
dim cEXE
dim cCNF
dim cSilent
dim cApplication
dim cFileList
dim lPath
cExe = #cExe#
cCNF = #cCNF#
cSilent = #cSilent#
cApplication = #cApplication#
if nParam>0 then
	cFileList = Trim(Wscript.Arguments(0))
	for nIdx = 1 to nParam - 1
		cFileList = cFileList + Chr(13) + Trim(Wscript.Arguments(nIdx))
	next
end if

Dim myFSO, WriteStuff, cFileName, cFileret, WriteStuffRet
'Scrivo il file di testo con l'elenco dei file
Set myFSO = CreateObject("Scripting.FileSystemObject")
Const TemporaryFolder = 2
cFileName = myFSO.GetSpecialFolder(TemporaryFolder)+"\ah_Phone.txt"
Set WriteStuff = myFSO.OpenTextFile(cFileName, 2, True)
WriteStuff.WriteLine(cFileList)
WriteStuff.Close
'Lancio l'applicativo oppure uso quello gi� aperto
'sClassName = "adone9c000000"
sClassName = #ClassName#
dim vfpRet
Set oShell = CreateObject("Wscript.Shell")
lPath = myFSO.GetParentFolderName(cExe)
oShell.CurrentDirectory = lPath
Dim MessageNr
Const WM_USER = &H400
MessageNr = WM_USER + 2
oShell.Run """"+lPath+"\vfp_sendmessage.exe"+"""" + " -Cvfp_sendmessage.fpw """ + sClassName + """ """+ CSTR(MessageNr) + """ 2", 0, true
cFileret = myFSO.GetSpecialFolder(TemporaryFolder)+"\fileret_vfpmsg.txt"
Set WriteStuffRet = myFSO.OpenTextFile(cFileret, 1)
vfpRet = WriteStuffRet.ReadLine()
WriteStuffRet.Close
if vfpRet="-1" then
	'Lancio Exe
	oShell.Run """"+cEXE+"""" + " """ + cCNF + """ """+ cSilent + """ """ + cApplication + """ ""PH"" """ + cFileName + """", 2, False
end if
'Clear memory
Set WriteStuff = NOTHING
Set WriteStuffRet = NOTHING
Set myFSO = NOTHING
Set oShell = NOTHING