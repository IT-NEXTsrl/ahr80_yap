* ----------------------------------------------------------------------------
*
* Oggettini ad hoc
*
* ----------------------------------------------------------------------------
* Modulo   : 
* Programma: 
* Ver      : 
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Zucchetti Aulla
* Data creazione: 26/02/03
	* Aggiornato il : 26/02/03
* #%&%#Build:  
* ----------------------------------------------------------------------------
* Libreria contenente gli oggettini definiti per ad hoc 
* ----------------------------------------------------------------------------
* Metodi Code Painter
#include "cp_app_lang.inc"
#Define TOOLBAR_BORDERCOLOR 55
#Define TOOLBAR_BTN_HOVER_BORDER	62
#Define TPM_RIGHTALIGN	0x8
#Define TPM_BOTTOMALIGN	0x20
#Define TPM_CENTERALIGN 0x4
#Define TPM_VCENTERALIGN 0x10
#Define TPM_NOANIMATION	0x4000
#Define TOOLBAR_BTN_HOVER 51
#Define TOOLBAR_BTN_TEXT 59
#Define TOOLBAR_BTN_HOVER_TEXT 60
#Define TOOLBAR_BTN_DOWN_TEXT 61
#Define TOOLBAR_BTN_HOVER_BORDER	62
#Define TOOLBAR_BTN_DOWN_BORDER	63
#Define UPARROW	5
#Define LEFTARROW	19
#Define RIGHTARROW	4
#Define DNARROW	24
#Define TOOLBAR_BACKGRD_V 53
#Define TOOLBAR_BACKGRD_H 54

* Oggettino di tipo grafico.
* il grafico carica i dati da una cursore Fox passatogli come parametro.
* La Tabella TAMGRAPH contiene le impostazioni dei grafici contenuti nel Modulo Produzione su Commessa
* attualmente solo in questo modulo sono utilizzati questi Oggettini.
* L'Archivio � strutturato con 2 campi uno per il nome della configurazione (C 15)
* e l'altro per la definizione del grafico (GENERAL).
* Quando si utilizza un tipo di grafico occorre riutilizzare la solita struttura del cursore passatogli in origine
* (anche come nome dei campi).
* Per creare un grafico occorre costruirlo con il wizard dei grafici doi Fox
* meglio creandosi prima una tabella con la struttura dei dati.
Define Class Tam_Graph as OleBoundControl
OleClass='MSGraph.chart.8'
Autosize=.T.
Enabled=.f.
Visible=.t.
Width=100
Height=100
Hastitle='t'
Titolo=''
* Proprieta TAM
NomeGraph='' && nome del grafico (utilizzato per la ricerca nella tabella)
Tabella='TAMGraph' && Nome della Tabella
tName='' && nome Tabella Temporanea
tCursor='' && cursore contenenete i dati o query se finisce per .vqr
cGraphString='' && stringa che conterr� i dati da passare al grafico
cTitle='' && titolo del grafico
tErrMsg='' && Messaggio in caso di Errore
* Fine propriet� TAM
* Propriet� Code Painter
cEvent=''


Proc Event(pEvent)
Local bErr,bResult,Tame,bQuery,Old_Error,ErrMsg
	if pEvent='Done'
	* tolgo tutti i cursori dalla memoria
		if Used(This.tabella)
	 	* chiudo la tabella con la definizione del grafico
			Select (This.Tabella)
			Use
		endif
	 
		if Used(This.tName)
		* chiudo la tabella temporanea
			Select (This.tName)
			Use
			* elimino fisicamente dal disco la tabella temporanea
			Drop table (This.tName)
		endif
		* Esco 
		return
	endif
	
    if ','+pEvent+','$','+this.cEvent+','
    * Eseguo il grafico
    * verifico se mi passano una query on un cursore
    if Upper(Right(alltrim(this.tCursor),4))='.VQR'
	    bQuery=This.tCursor
        this.tCursor='__graph__'
        vq_exec(bQuery,this.parent.oContained,this.tCursor)
        if !used(this.tCursor)
         This.tErrMsg=cp_msgformat(MSG_CANNOT_FIND_QUERY__,bQuery) 
         *wait wind 'Query '+bQuery+' inesistente'  
         bErr=.T.
        else
         bErr=.F.
        endif
    Endif
    If !bErr
    * se Tutto OK Costruisco la stringa da passare al Grafico
    	bErr=This.CaricaDati(This.tCursor)
    EndIF
    
	ErrMsg=''    
	Old_Error=On('Error')
	On Error ErrMsg=Message()

    if !bErr
      * apro la tabella contenente le definizioni dei grafici
        select olegraph from (this.Tabella) where Upper(Alltrim(Nome))==Upper(alltrim(this.Nomegraph));
        into table (this.tName)
      bResult= _Tally>0 And Empty(ErrMsg)
      if bResult
		* appendo il campo Olegraph della tabella grafici in tGraph del cursore con i dati
	    cp_msg(CP_TRANSLATE(MSG_LOADING_GRAPH_DATA_D),.t.)
		Select (this.tName)
		Append General OleGraph Data This.cGraphString
		* Riempio il grafico
		* cambio il Source per fargli fare il refresh
		this.ControlSource='tamgraph.Olegraph'
		*this.ControlSource=''
        this.ControlSource=this.tname+'.Olegraph'
        * Imposto il Titolo
        If Not Empty(This.Titolo)
        	This.ChartTitle.Caption=This.Titolo
        Endif
        * refresh del grafico
        this.refresh()
      ELSE       
       This.tErrMsg = iif(empty(ErrMsg),cp_msgformat(MSG_CONFIGURATION__MISSING,This.NomeGraph) , ErrMsg )
       bErr=.t.
      endif
   Endif
    
   if bErr 
	   if Empty(This.tErrMsg)
    	   cp_msg(cp_translate(MSG_ERROR_LOADING_DATA_CL) +Message(),.f.,.f.,.f.,.f.) 
	   else
    	   cp_msg( cp_translate(MSG_ERROR_LOADING_DATA_CL) + This.tErrMsg,.f.,.f.,.f.,.f.)
	   Endif
   Endif
   
    On Error &Old_Error  
      
  return

  endif
Endproc

Proc Init
	* carico la configurazione in una tabella temporanea locale per evitare 
	* problemi con la multiutenza
	This.tName=Sys(2015)	
EndProc

Proc Calculate()
 * Non faccio niente 
EndProc

Proc cpResize(i_dx,i_dy)
    * dimensionamento
  this.Object.width=this.width 
  This.Object.height=this.height
ENDPROC

Proc CaricaDati(pCursor)
* Carica i dati nel grafico
	Local NumCampi,i,bErr
	bErr=.f.
	if Not Used(pCursor)
	    This.tErrMsg = cp_translate(MSG_ERROR_LOADING_DATA_CL) + cp_translate (MSG_CURSOR_OR_QUERY_MISSING)
 		*wait wind 'Impossibile caricare i dati - Cursore o Query Inesistente'
 		bErr=.t.
 		return bErr
	Else
	    Select (pCursor)
	    if RecCOunt()=0
*	    	wait wind 'Impossibile caricare i dati - Cursore o Query Vuoti'
	    	This.tErrMsg =cp_translate(MSG_CANNOT_DISPLAY_F_DATA_MISSING)
 			bErr=.t.
 			return bErr
	    EndIf 		
	EndIf

	* costruisco la stringa da passare al grafico
	NumCampi=FCount(pCursor)
	This.cGraphString=''
	* Scorro il cursore e carico i dati
	cp_msg( CP_TRANSLATE(MSG_LOADING_GRAPH_DATA_D))	
	select (pCursor)
	
	* --- Scorro i campi del cursore che contiene i dati
	For I=1 to NumCampi
		This.cGraphString=This.cGraphString+ FIELDS(I)+IIF(I<NumCampi,CHR(9),CHR(13))
	EndFor
	
    * --- Scorro il cursore e recupero i dati da stampare
	Scan	
	
       for i=1 to numcampi
       
       	This.cgraphstring=This.cgraphstring+ ;
       		iif(TYPE(FIELDS(I))='C',;
       		EVALUATE(FIELDS(I)),iif(TYPE(FIELDS(I))='D',DTOC(EVALUATE(FIELDS(I))), ;
       		STR(EVALUATE(FIELDS(I)),16,2)))+ ;
       		IIF(I<numcampi,CHR(9),CHR(13))
       Endfor
       
	EndScan
	Return bErr
EndProc
EndDefine

* ===================================================================================================
* Classe di integrazione con ArchiWEB
* ===================================================================================================
Define Class ArchiWEB as OleControl
  OleClass='CtlAcq.CtlAcqF'
  Visible=.t.
  Width=2000
  Height=1000

  Proc Event(pEvent)
  endproc

  Proc Init
  EndProc

  Proc Calculate(nValue)
  EndProc

EndDefine

Define Class ArchiRIC as OleControl
  OleClass='CtlRicercaEx.RicercaEx'
  Visible=.t.
  Width=2000
  Height=1000

  Proc Event(pEvent)
  endproc

  Proc Init
  EndProc

  Proc Calculate(nValue)
  EndProc

EndDefine

* ===================================================================================================
* Classe utilizzata nel caricamento rapido per gestire il link
* ===================================================================================================
Define Class _OSource as Custom
 dimension xKey[15] 
EndDefine

* ===================================================================================================
* Classe di integrazione con InfoReader
* ===================================================================================================
Define class cp_InfoReader as container
  borderwidth=0
  add object oInfoReader as InfoReader with top=0,left=0,visible=.f.
  
  proc Init()
    this.oInfoReader.width=this.width
    this.oInfoReader.height=this.height
    this.oInfoReader.visible=.t.
  endproc
  
  proc Event(cEvent)
  EndProc
  
  proc Calculate(xValue)
  EndProc

EndDefine

Define Class InfoReader as OleControl
  OleClass='InfoReader.InfoReaderX'
  Visible=.t.
  AutoSize=.t.
  
  Proc Event(pEvent)
  endproc

  Proc Init
  EndProc

  Proc Calculate(nValue)
  EndProc

EndDefine

* ===================================================================================================
* Classe di integrazione con InfoPublisher
* ===================================================================================================
Define Class InfoPublisher as OleControl
  OleClass='IBPublisher20.IBPublisher'
  Visible=.f.

  Proc Event(pEvent)
  endproc

  Proc Init
  EndProc

  Proc Calculate(nValue)
  EndProc

EndDefine

* ===================================================================================================
* Job Scheduler - Timer utilizzato per lanciare i Job schedulati
* ===================================================================================================
Define Class schedTimer As Timer
	Interval=30*1000
	Proc Timer()
		Local w_nConn, w_IdxTable, w_cTable, w_cmd, w_verifica, w_oraatt, w_dataatt, w_jobass
		If odesktopbar.boxsched.Value<>CP_TRANSLATE(MSG_IN_EXECUTION)
			odesktopbar.boxsched.Value=CP_TRANSLATE(MSG_IN_EXECUTION)
			odesktopbar.schedjob.Enabled=.F.
			odesktopbar.Refresh()
			w_dataatt=Date()
			w_oraatt=Time()
			w_oraatt=Substr(w_oraatt,1,5)
			If Vartype(g_JobAssOnly)="L" And g_JobAssOnly
				w_jobass=" AND SHUTEPRE="+cp_ToStrODBC(i_CODUTE)
			Else
				w_jobass=""
			Endif
			w_IdxTable=cp_OpenTable('SCHEDJOB')
			w_nConn=i_TableProp[w_IdxTable,3]
			w_cTable=cp_SetAzi(i_TableProp[w_IdxTable,2])
			w_cmd="select SHCODICE,SHDATAPRO,SHORAPROS,SHTMPMAX,SHRELOAD,SHESENOW FROM "+w_cTable+;
				" where SHDATAPRO is not null AND SHFLAT='S' and (SHDATAPRO<"+cp_ToStrODBC(w_dataatt)+";
				OR (SHDATAPRO="+cp_ToStrODBC(w_dataatt)+"AND SHORAPROS<='"+w_oraatt+"'))"+w_jobass+;
				" order by SHDATAPRO,SHORAPROS"
			w_verifica=cp_SQL(w_nConn,w_cmd,"CursJob")
			cp_CloseTable('SCHEDJOB')
			If w_verifica>-1
				***esegue routine
				If Used('CursJob')
					Select CursJob
					Go Top
					w_SHCOD = SHCODICE
					w_SHDATA = SHDATAPRO
					w_SHORA = SHORAPROS
					w_SHTMPMAX = SHTMPMAX
					w_SHRELOAD = SHRELOAD
					w_SHESENOW = SHESENOW
					If w_SHESENOW='S' Or w_SHRELOAD <> 'S' Or Not(Type("G_ISSERVICE")<>"U" And G_ISSERVICE)
						If Not Empty(Nvl(w_SHCOD,Space(10)))
							l_sPrg="gsjb_bsj"
							Do (l_sPrg) With .F., w_SHCOD, w_SHDATA, w_SHORA, w_SHTMPMAX
						Endif
					Else
						If w_SHRELOAD = 'S'
							* -- scrivo SHESENOW per la successiva elaborazione
							w_IdxTable=cp_OpenTable('SCHEDJOB')
							w_nConn=i_TableProp[w_IdxTable,3]
							w_cTable=cp_SetAzi(i_TableProp[w_IdxTable,2])
							If w_nConn<>0
								w_Rows = cp_SQL(w_nConn,"update "+w_cTable+" set ";
									+"SHESENOW ="+cp_NullLink(cp_ToStrODBC("S"));
									+",cpccchk="+cp_ToStrODBC(cp_NewCCChk());
									+" where ";
									+"SHCODICE = "+cp_ToStrODBC(w_SHCOD);
									+" and SHDATAPRO = "+cp_ToStrODBC(w_SHDATA);
									+" and SHORAPROS = "+cp_ToStrODBC(w_SHORA);
									)
							Else
								Update (w_cTable) Set;
									SHESENOW = "S";
									,cpccchk=cp_NewCCChk();
									where;
									SHCODICE = w_SHCOD;
									and SHDATAPRO = w_SHDATA;
									and SHORAPROS = w_SHORA;

								w_Rows = _Tally
							Endif
							cp_CloseTable('SCHEDJOB')
							* chiudo sempre, tanto riapre il servizio
							Clear Events
						Endif
					Endif
				Endif
			Endif
		Endif
		If Used("CursJob")
			Select CursJob
			Use
		Endif

		odesktopbar.schedjob.Enabled=.T.
		odesktopbar.boxsched.Value=MSG_ENABLED_SCH
		odesktopbar.Refresh()
	Endproc

Enddefine

* ===================================================================================================
* Progress bar
* ===================================================================================================
define Class oProgressbar AS OLEControl
  OLEClass= "COMCTL.ProgCtrl.1" && "MSComctlLib.ProgCtrl.2"
  Top     = 100
  Left    = 100
  Height  = 30
  Width   = 200
  Visible = False
  BorderWidth=0
  BorderStyle = 0
ENDDEFINE

Define Class Tam_Progressbar AS container
ForeColor = RGB(0, 0, 0)
FontName = 'Tahoma'
FontSize = 0
BackStyle=0
BorderWidth = 0
BorderStyle = 0
oParentObject = .NULL.
oContained = .NULL.
Min = 0
Max = 100
bNoInsideForm = .t.
bNoMultiProgBar = .t.
cEvent = ''
tProcName =''
tCurProc = 1
tNumProc = 1
xOldValue = 0
tOldValue = 0
cMsgPartStep = ''
cMsgGlobStep = ''

w_STEPVAL=0
w_OLDPROC=0

nMinHeightMultiInside = 49
nMinHeightSingleInside = 22

add object OLEProgress as cp_HorizontalBar with Min=0, Max=100, Left=0,Top=25,Height=24,Width=50,Visible=.f.,bTransition=.f.,cValBar="P"
add object OLEProgresst as cp_HorizontalBar with Min=0, Max=100, Left=0,Top=53,Height=24,Width=50,Visible=.f.,bTransition=.f.,cValBar="P"

add object Lbl1 as label with Caption='0%',Top=0,Left=0,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.f.,BackStyle=0
add object Lbl2 as label with Caption='25%',Top=0,Left=(this.width/2)-(this.width/4)-15,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.f.,BackStyle=0
add object Lbl3 as label with Caption='50%',Top=0,Left=(this.width/2)-15,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.f.,BackStyle=0
add object Lbl4 as label with Caption='75%',Top=0,Left=(this.width/2)+(this.width/4)-15,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.f.,BackStyle=0
add object Lbl5 as label with Caption='100%',Top=0,Left=this.width-35,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.f.,BackStyle=0

add object Lbl6 as label with Caption="",Top=20,Left=0,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.f.,Visible=.f.,AutoSize=.t.,BackStyle=0
add object Lbl7 as label with Caption="",Top=59,Left=0,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.f.,Visible=.f.,AutoSize=.t.,BackStyle=0
* --- Label utilizzate nel caso di progress integrata nella forms
add object Lbl8 as label with Caption="",Top=20,Left=0,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.f.,AutoSize=.t.,BackStyle=0
add object Lbl9 as label with Caption="",Top=59,Left=0,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.f.,Visible=.f.,AutoSize=.t.,BackStyle=0
* add object Lbl10 as label with Caption="",Top=20,Left=0,Height=17,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.f.,Visible=.f.,AutoSize=.t.,BackStyle=0

add object Ln1 as Line with Top=15,Left=this.Left,Height=6,Width=0,Visible=.f.,BorderWidth=1,BorderColor=rgb(0,0,0)
add object Ln2 as Line with Top=15,Left=(this.width/2)-(this.width/4)-5,Height=6,Width=0,Visible=.f.,BorderWidth=1,BorderColor=rgb(0,0,0)
add object Ln3 as Line with Top=15,Left=(this.width/2)-5,Height=6,Width=0,Visible=.f.,BorderWidth=1,BorderColor=rgb(0,0,0)
add object Ln4 as Line with Top=15,Left=(this.width/2)+(this.width/4)-5,Height=6,Width=0,Visible=.f.,BorderWidth=1,BorderColor=rgb(0,0,0)
add object Ln5 as Line with Top=15,Left=this.width-1,Height=6,Width=0,Visible=.f.,BorderWidth=1,BorderColor=rgb(0,0,0)
add object Ln6 as Line with Top=18,Left=this.Left,Height=0,Width=this.Width-1,Visible=.f.,BorderWidth=1,BorderColor=rgb(0,0,0)



Proc Event(cEvent)
	If cEvent= 'FormLoad'
		This.oParentObject=This.Parent.oContained
		&& mi serve per assegnare la maschera alla variabile globale
		&& nel caso sia integrata in un form diverso da GSAR_SPB e GSAR1SPB
		&& altrimenti la funzione GesProgBar non saprebbe dove assegnare i valori
		i_PROGBAR = This.oParentObject
		This.InitProgBar()
	Endif
	If cEvent+','$This.cEvent+','
	Endif
Endproc

Proc Calculate(xValue, tValue)
LOCAL l_bUpdated, l_bUpdatedt
	If Type('xValue') = 'N'
		If xValue <> This.xOldValue
			This.OleProgress.Calculate(xValue, .F., This.Min, This.Max)
			This.xOldValue = xValue
			l_bUpdated=.t.
		Endif
	Endif
	If Type('tValue') = 'N' And !This.bNoMultiProgBar
		If tValue > This.tOldValue
			This.OleProgresst.Calculate(tValue, .F., This.Min, This.Max)
			This.tOldValue = tValue
			l_bUpdatedt=.t.
		Endif
	Endif

	IF l_bUpdated OR l_bUpdatedt
*!*			IF l_bUpdated
*!*				This.OleProgress.Refresh()
*!*			endif
*!*			
*!*			IF l_bUpdatedt
*!*				This.OleProgresst.Refresh()
*!*			endif
			This.Refresh()
		wait wind "" timeout 0.001
	endif


Endproc

Proc InitProgBar()


	This.Resize()
	*!*	    if This.MultiProgBar
	*!*	     This.Lbl6.Visible=.t.
	*!*	     This.Lbl7.Visible=.t.
	*!*	     This.OLEProgresst.visible=.t.
	*!*	    endif
	This.Refresh()
Endproc

Proc Resize()
	Local l_HeightObj

	If !This.bNoInsideForm
		* --- Integrata Nella Form
		If !This.bNoMultiProgBar
			* --- Progressbar Multipla (2)
			* --- Verifico che l'altezza sia almeno 68
			This.Height = Iif(This.Height < This.nMinHeightMultiInside, This.nMinHeightMultiInside, This.Height)
			m.l_HeightObj = 20 && Int(cp_Round(This.Height / 2 ,0))  - 3

			With This.OleProgress
				.Left=5
				.Top= 3
				.Height = m.l_HeightObj
				.Width = This.Width - 10
				.nAlignPerCent=4
				.bCurved=.F.
				.Visible=.T.
			Endwith

			With This.Lbl8
				.Left=7
				.Top = This.OleProgress.Top + 1
				*				.Height=m.l_HeightObj
				.ForeColor=Rgb(0,0,0)
				.Caption=.parent.cMsgPartStep
				.Visible=.T.
			Endwith

			With This.OleProgresst
				.Left=5
				.Top = This.OleProgress.Top + This.OleProgress.Height + 4 && this.Lbl8.Top + this.Lbl8.Height + 2 + This.OLEProgress.Top + This.OLEProgress.Height + 2
				.Height = m.l_HeightObj
				.Width = This.Width - 10
				.nAlignPerCent=4
				.bCurved=.F.
				.Visible=.T.
			Endwith

			With This.Lbl9
				.Left=7
				.Top = This.OleProgresst.Top + 1
				*				.Height=m.l_HeightObj
				.ForeColor=Rgb(0,0,0)
				.Caption=.parent.cMsgGlobStep
				.Visible=.T.
			Endwith
		Else

			This.Height = Iif(This.Height < This.nMinHeightSingleInside, This.nMinHeightSingleInside, This.Height)
			m.l_HeightObj = 20

			This.OleProgresst.Visible=.f.
			This.Lbl9.Visible=.F.
				
			This.Lbl8.Visible=.F.
			
			With This.OleProgress
				.Left=5
				.Top= 1
				.Height = m.l_HeightObj
				.Width = This.Width - 10
				.nAlignPerCent=3
				.Visible=.T.
			Endwith

		Endif
	Else

		If !This.bNoMultiProgBar
			This.OleProgress.Top = 34
			This.OleProgresst.Top = 67
		Endif
		This.OleProgress.Width = This.Width-This.OleProgress.Left -5
		This.OleProgresst.Width = This.Width-This.OleProgresst.Left -5
		This.lbl2.Left = (This.Width / 2) - (This.Width / 4) - 15
		This.lbl3.Left = (This.Width/2)-15
		This.lbl4.Left = (This.Width / 2) + (This.Width / 4) - 15
		This.lbl5.Left = This.Width - 35
		This.Ln1.Left = This.OleProgress.Left
		This.Ln2.Left = (This.Width / 2) - (This.Width / 4) - 5
		This.Ln3.Left = (This.Width / 2) - 5
		This.Ln4.Left = (This.Width / 2) + (This.Width / 4) - 5
		This.Ln5.Left = This.OleProgress.Width - 1
		This.Ln6.Width = This.OleProgress.Width - 1

	Endif
Endproc

Proc GetPValue
	Local i_pVal
	i_pVal = 0
	i_pVal = This.OleProgress.Value
	Return i_pVal
Endproc

Proc GetTValue
	Local i_pVal
	i_pVal = 0
	i_pVal = This.OleProgresst.Value
	Return i_pVal
Endproc

Proc SetMsgPartStep(cValue)
	This.cMsgPartStep = cValue
	If This.Lbl8.Visible
		This.Lbl8.Caption = cValue
	Endif
Endproc

Proc SetMsgGlobStep(cValue)
	This.cMsgGlobStep = cValue
	If This.Lbl9.Visible
		This.Lbl9.Caption = cValue
	Endif
Endproc

Proc ResetPBValue
	This.Init()

	This.tProcName =''
	This.tCurProc = 1
	This.tNumProc = 1
	This.xOldValue = 0
	This.tOldValue = 0

	This.w_STEPVAL=0
	This.w_OLDPROC=0

	This.OleProgress.Value = 0
	This.OleProgresst.Value = 0
Endproc

Procedure Init()
	This.SetMsgPartStep(CP_TRANSLATE(MSG_PARTIAL_PROGRESS))
	This.SetMsgGlobStep(CP_TRANSLATE(MSG_TOTAL_PROGRESS))
	This.Lbl6.Caption= This.cMsgPartStep
	This.Lbl7.Caption= This.cMsgGlobStep
	DoDefault()
Endproc

enddefine

* ===================================================================================================
* Classe oPrintToFile
* ===================================================================================================
DEFINE CLASS oPrintToFile as custom
* --- Obbligatori
cTypeClass='XFRX'					&& Tipo classe
cFormat='   '						&& Formato del file (PDF,RTF...)
cExtFormat='   '						&& Estensione formato del file (PDF,RTF...)
cFileName=space(254)				&& Nome del file di destinazione
cReportName=.null.					&& Report
lPlain=.f.							&& Se true HTML -> PLAIN (elimina le interruzioni di pagina)		
oHandle=.NULL.						&& Per AMYUNI
* --- Feedback
nNumPag=0							&& Numero pagine stampate
nNumPagTot=0						&& Numero pagine totali report
nErr=0								&& Flag di errore	
* --- Opzioni
cTempDirectory='' 			        && Cartella temporanea
lSilent=.t.							&& Flag elaborazione silente
*nFrom=0								&& Da pagina (TIFF)	
*nTo=0								&& A pagina (TIFF)
nJpegQuality=80						&& Qualit� JPEG	
nBpp=24								&& Bit per pixel
*nThumbnailWidth					&& Larghezza immagine in pixel
*nThumbnailHeight					&& Altezza immagine in pixel
cRange=' '							&& Range di stampa (Es. "1,4,10-20,30")
nSplitPages=0						&& Se diverso da 0 splitta i documenti word in pi� file di "nSplitPage" pagine
lViewProgress=.t.					&& Visualizza wait window con informazioni parziali ("Page #: Report #: %")
lNotOpenViewer=.t.					&& Se true non apre il file dopo la generazione	
cGeneraFilePDF=''					&& Se non vuoto genera il file contenuto in un file PDF passando dall'applicazione associata al file.
* --- Informazioni
cAuthor=i_MSGTITLE					&& Autore
cTitle=''							&& Titolo
cSubject=''							&& Soggetto
cKeywords=''						&& Parole chiave
* --- Sicurezza PDF
lPDFEncryption=.f.					&& Flag criptazione PDF
cOwnerPassword=' '					&& Password proprietario file pdf
cUserPassword=' '					&& Password utente file pdf
lPrintDocument=.t.     				&& Flag per autorizzazioni PDF  (se true il documento � printabile)
lModifyDocument=.f.					&& Flag per autorizzazioni PDF  (se true il documento � modificabile) 
lCopyTextAndGraphics=.f.			&& Flag per autorizzazioni PDF  (se true il testo � copiabile "ctrl+C")
lAddOrModifyAnnotations=.t.			&& Flag per autorizzazioni PDF  (se true si possono solo modificare o aggiungere annotazioni)
* --- Zip file
cArchive=''							&& Nome dell'archivio
lAdditive=.t.						&& Aggiunge ad un archivio gi� esitente
lDeleteFileAfter=.f.				&& Cancella file dopo l'archiviazione
lAppend=.f.							&& Gestione Append

PROC Init
  PARAMETERS pTypeClass, pFormat, pFileName, pReportName, pHandle, pGeneraFilePDF, pExtFormat
  * --- Inizializzo le propriet� base
  this.cTypeClass=pTypeClass
  this.cFormat=pFormat
  this.cFileName=pFileName
  this.cReportName=pReportName
  this.oHandle=pHandle
    IF VARTYPE(m.pExtFormat) = 'L'
       LOCAL l_ext
       l_ext = JUSTEXT(m.pFileName)
       IF EMPTY(l_ext)
	        this.cExtFormat = alltrim(m.pFormat)
       Else
         	 this.cExtFormat =  l_ext
       Endif
  ELSE
	  this.cExtFormat = alltrim(m.pExtFormat)	
  ENDIF
  this.cTempDirectory=tempadhoc()
  this.cGeneraFilePDF=pGeneraFilePDF
  * Se stampa offerte di Word su PDF allora sono costrtto ad utilizzare Amuyuni...
  IF VARTYPE(this.cReportName)='O' AND LOWER(this.cReportName.FirstReport())='documentoword'
   this.cTypeClass ='AMYUNI'
   this.cFormat='PDF'   
  endif    
  
  IF NOT EMPTY(this.cGeneraFilePDF)
   * Se genero file PDF da applicazione associata sono obbligato a passare
   * dalla stampante AMYUNI. In teoria potrei utilizzare una qualsiasi stampante che genera
   * file PDF a patto che sia disponible tra le stampanti e che sia possibile da codice decidere
   * su quale file PDF sar� salvata la stampa. Altrimenti l'utente dovrebbe comunicare ad ad hoc 
   * la posizione del PDF risultato della stampa, affinch� ad hoc possa allegare alla e-mail il file.
   this.cTypeClass ='AMYUNI'
   IF Empty(this.cFormat)
    * Se il formato non � disponibile opto per PDF...
    this.cFormat='PDF'
   endif
  endif
ENDPROC

Proc StartPrint
	Local CursorSet
	CursorSet=Alias()
	If Empty(CursorSet) And Used("__tmp__")
		Select __tmp__
	Endif
	Local lcSetConsole
	m.lcSetConsole = Alltrim(Set("CONSOLE"))
	Set Console Off
	This.cFormat = Alltrim(This.cFormat)
	If Vartype(This.cReportName)='O'
		This.cReportName.RemoveReportExt()
	Endif
	IF Upper( This.cTypeClass )=='XFRX' OR Upper( This.cTypeClass )=='XFRXIMG'
    * Copia i file di xfrx nella cartella exe
		If Type("g_NewXFRX")<>"L" Or g_NewXFRX
			Set Path To ( Sys(5)+Sys(2003) + Iif(g_adhocone,"\..\..\EXE","") + "\xfrxlib" ) Additive
			Set Classlib To ( Sys(5)+Sys(2003) + Iif(g_adhocone,"\..\..\EXE","") + "\xfrxlib\xfrxlib" ) Additive
			Set Library To ( Sys(5)+Sys(2003) + Iif(g_adhocone,"\..\..\EXE","") + "\xfrxlib\xfrxlib.fll" ) Additive
			Set Classlib To ( Sys(5)+Sys(2003) + Iif(g_adhocone,"\..\..\EXE","") + "\_reportlistener" ) Additive
		Else
			Set Path To ( Sys(5)+Sys(2003) + Iif(g_adhocone,"\..\..\EXE","") + "\xfrxlibold" ) Additive
			Set Library To ( Sys(5)+Sys(2003)+Iif(g_adhocone,"\..\..\EXE","")+"\xfrxlibold\xfrxlib.fll" ) Additive
		Endif
    ENDIF
	Do Case
	Case Upper( This.cTypeClass )=='XFRX'
		********** XFRX ***********
		Private loSession, loProgress, lcMsg
        **Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
        DecoratorState ("D")
		* Aggancio la libreria XFRX
		loSession=xfrx("XFRX#LISTENER")

		* Creo oggetto progress
		*loUpdate = CREATEOBJECT("progress")
		*loSession.successor = loUpdate

		* Setto i parametri
		* loSession.SetParams(<tcOutputName>, <tcTempDirectory>, <tlNotOpenViewer>, <tcCodePage>,
		*					    <tlSilent>, <tlNewSession>, <tcTarget>, <tcArchive>,
		*						<tlAdditive>, <tlDeleteFileAfter>)
		This.nErr=loSession.SetParams(This.cFileName, This.cTempDirectory, This.lNotOpenViewer, ,;
			this.lSilent, , This.cFormat, This.cArchive,;
			this.lAdditive, This.lDeleteFileAfter, This.lAppend)
		Do Case		&&Controllo eventuali errori
		Case This.nErr=-1
			lcMsg=cp_translate(MSG_CANNOT_OPEN_MSWORD)
		Case This.nErr=-2
			lcMsg=cp_translate(MSG_NEEDED_WORD2000_OR_MORE)
		Case This.nErr=-3
			lcMsg=cp_translate(MSG_CANNOT_ACCESS_TO_OUTPUT_FILE)
		Case This.nErr=-4
			lcMsg=cp_translate(MSG_UNKNOWN_FORMAT)
		Case This.nErr=-5
			lcMsg=CP_MSGFORMAT(MSG_UNABLE_TO_OPEN__,'hndlib.dll')
		Case This.nErr=-6
			lcMsg=CP_MSGFORMAT(MSG_UNABLE_TO_OPEN__,'xfrxlib.fll')
		Case This.nErr=-7
			lcMsg=CP_MSGFORMAT(MSG_UNABLE_TO_OPEN__,'zlib.dll')
		Endcase
		If This.nErr<>0
			cp_ErrorMsg(lcMsg,48,'',.F.)
		Else
			*PDF Font Embedding, salva nel PDF solo i font utilizzati
			loSession.setEmbeddingType(1)

			If Not Empty(This.cRange) And Version(5)=800
				* Setto l'intervallo di stampa
				loSession.setPageRange(This.cRange)
			Endif

			If (Upper( This.cFormat )=='PDF' OR this.cFormat=="PDF/A-1B") And This.lPDFEncryption
				* Setto pwd e permessi (PDF)
				loSession.setPasswords(This.cOwnerPassword,This.cUserPassword)
				loSession.setPermissions(This.lPrintDocument,This.lModifyDocument,This.lCopyTextAndGraphics,This.lAddOrModifyAnnotations)
			Endif

            * se il formato PDF richiesto � PDF/A-1B
            IF this.cFormat=="PDF/A" OR this.cFormat=="PDF/A-1B" OR this.cFormat=="ODT"
				LOCAL cXfrxLibFllVersion
				cXfrxLibFllVersion = Sys(2007, Filetostr(Sys(5)+Sys(2003)+Iif(g_adhocone,"\..\..\EXE","")+"\xfrxlib.fll"))
				IF INLIST( cXfrxLibFllVersion, "46770" )
					* E' in uso la versione 14.3 di xfrx
					IF this.cFormat=="PDF/A" OR this.cFormat=="PDF/A-1B"
						loSession.setpdfa(.T.)
					ENDIF
				ELSE
					* xfrx prima della versione 14.3 non supportava alcuni formati
					IF this.cFormat=="PDF/A" OR this.cFormat=="PDF/A-1B"
						cp_ErrorMsg("La versione in uso di XFRX non supporta il formato PDF/A. Sar� generato un PDF standard",48,'',.F.)
					ENDIF
					IF this.cFormat=="ODT"
						cp_ErrorMsg("La versione in uso di XFRX non supporta il formato ODT",48,'',.F.)
					ENDIF                
				ENDIF
            ENDIF
            
			* Setto le informazioni
			loSession.setAuthor(This.cAuthor)
			loSession.setTitle(This.cTitle)
			loSession.setSubject(This.cSubject)
			loSession.setKeywords(This.cKeywords)

			* Processo il report
			Public gnStopXFRX	&&Variabile pubblica per fermare XFRX
			* Lancio la stampa, eventualemte aggiungo il range di stampa (da pagina a pagina)
			Local l_i, l_NomeRep, l_NomeCur, l_next, l_first
			Local l_VarEnv, l_j, l_VarName, l_VarValue, l_bVarEnv
			l_i=0
			l_first=.t.
			Do While l_i<>-1
				l_i=This.cReportName.NextReport(l_i)
				If l_i<>-1
					*--- Setto i_paramqry per permettere l'uso di cp_queryparam()
					i_paramqry=This.cReportName.GetParamqry(l_i)
					*--- Creo l'environment con le variabili l_
					l_VarEnv=This.cReportName.GetVarEnv(l_i)
					l_bVarEnv=.F.
					If Not Isnull(l_VarEnv) And Not l_VarEnv.IsEmpty()
						l_NumVar=l_VarEnv.GetNumVar()
						*--- Creo le variabili
						For l_j = 1 To l_NumVar
							l_VarName=l_VarEnv.GetVarName(l_j)
							&l_VarName=l_VarEnv.GetVarValue(l_j)
						Endfor
						l_bVarEnv=.T.
					Endif
					l_NomeRep=cp_GetStdFile(Justpath(This.cReportName.GetReport(l_i))+'\'+Juststem(This.cReportName.GetReport(l_i)),'FRX')
					l_NomeCur=This.cReportName.GetCursorName(l_i)
					Select(l_NomeCur)
					Public g_CurCursor
					g_CurCursor=l_NomeCur
					l_next=This.cReportName.NextReport(l_i)
					Set Console Off
					If !(l_next=-1)
						If not l_first And This.cReportName.GetNoReset(l_i)
							Macro="REPORT FORM '"+l_NomeRep+"' OBJECT loSession"+Iif(!Empty(This.cRange), " RANGE "+This.cRange, "")+" NOPAGEEJECT NORESET"
							&Macro
						Else
							Macro="REPORT FORM '"+l_NomeRep+"' OBJECT loSession"+Iif(!Empty(This.cRange), " RANGE "+This.cRange, "")+" NOPAGEEJECT"
							&Macro
						Endif
					Else
						If not l_first And This.cReportName.GetNoReset(l_i)
							Macro="REPORT FORM '"+l_NomeRep+"' OBJECT loSession"+Iif(!Empty(This.cRange), " RANGE "+This.cRange, "")+" NORESET"
							&Macro
						Else
							Macro="REPORT FORM '"+l_NomeRep+"' OBJECT loSession"+Iif(!Empty(This.cRange), " RANGE "+This.cRange, "")
							&Macro
						Endif
					Endif
					Release g_CurCursor
					*--- Rivalorizzo le variabili dell' environment in modo da poterle poi interrogare alla fine della stampa
					If l_bVarEnv  &&Se � true ho creato le variabili quindi le risalvo all'interno dell'oggetto
						For l_j = 1 To l_NumVar
							l_VarName=l_VarEnv.GetVarName(l_j)
							l_VarEnv.SetVarValue(&l_VarName,l_j)
						Endfor
					Endif
				Endif
				l_first=.f.
			Enddo
			loSession.finalize()
			Release gnStopXFRX
			* Memorizzo il numero di pagine processate
			This.nNumPagTot=loSession.PageNo
		Endif
		*loUpdate=.null.
		loSession=.Null.
		* Sgancio la libreria XFRX
		If g_adhocone
			Release Library ..\..\Exe\xfrxlib.fll
		Else
		If Type("g_NewXFRX")<>"L" Or g_NewXFRX
				Release Library Sys(5)+Sys(2003)+"\xfrxlib\xfrxlib.fll"
			ELSE
				Release Library Sys(5)+Sys(2003)+"\xfrxlibold\xfrxlib.fll"
			ENDIF
		Endif
        **Al termine del esportazion riattiva il decorator dei form 
        DecoratorState ("A")
	Case Upper( This.cTypeClass )=='AMYUNI'
		********** AMYUNI ***********
		Private iRet, w_err, w_actualerr
		iRet = .F.
		* - Variabile di errore - *
		w_err = .F.
		* - Salvo la routine di errore attuale - *
		w_actualerr = On ('ERROR')
		* - Imposto la nuova routine di errore - *
		On Error w_err=.T.

		* - Dichiara le funzione della Common Dialog Interface - *
		Do Case
		Case Upper( This.cFormat )=='PDF' Or Upper( This.cFormat )=='PDF/A' Or Upper( this.cFormat )=="PDF/A-1B"
			Declare Long    PDFDriverInit            In cdintf.Dll String @ szPrinter
			Declare Long    DriverInit               In cdintf.Dll String @ szPrinter
		Case Upper( This.cFormat )=='HTML'
			Declare Long    HTMLDriverInit           In cdintf.Dll String @ szPrinter
		Case Upper( This.cFormat )=='RTF'
			Declare Long    RTFDriverInit            In cdintf.Dll String @ szPrinter
		Endcase
		Declare Long    SetDefaultFileName       In cdintf.Dll Long Handle, String @ szFileName
		Declare Integer SetFileNameOptions       In cdintf.Dll Long Handle, Integer nOptions
		Declare Integer SetDefaultPrinter        In cdintf.Dll Long Handle
		Declare Integer RestoreDefaultPrinter    In cdintf.Dll Long Handle
		Declare         DriverEnd                In cdintf.Dll Long Handle
		Declare Long SendMessagesTo			   In cdintf.Dll Long hPrinter, String szWndClass
		Declare Integer SetImageOptions 		In cdintf.Dll Long Handle, Integer nImageOptions

		*--- Rendo pubblica la variabile perch� pu� essere usata con la bindevent
		Public DRV
		If w_err
			cp_ErrorMsg(CP_MSGFORMAT(MSG_NOT_FOUND_LIBRARY__IN_PRESENT_DIRECTORY,Alltrim(This.cFormat)),48,'',.F.)
		Else
			Do Case
			Case Upper( This.cFormat )=='PDF' Or Upper( This.cFormat )=='PDF/A' Or Upper( this.cFormat )=="PDF/A-1B"
				* - Instanzia il driver PDF converter - *
				If Type('g_NOSTPDF')='U' Or Not g_NOSTPDF
					* - Se non specificato altrimenti creo la stampante PDF
					DRV = PDFDriverInit( "PDF ADHOC" )
				Else
					* - Nel caso di Utente non abilitato non creo la stampante
					DRV = DriverInit( "PDF ADHOC" )
				Endif
			Case Upper( This.cFormat )=='HTML'
				DRV = HTMLDriverInit( "HTM ADHOC" )
			Case Upper( This.cFormat )=='RTF'
				DRV = RTFDriverInit( "RTF ADHOC" )
			Otherwise
				* Formato sconosciuto
				DRV = 0
			Endcase
			If DRV = 0
				cp_ErrorMsg(CP_MSGFORMAT(MSG_ERROR_CREATING__FILE__,Alltrim(This.cFormat),"0"),48,'',.F.)
				w_err = .T.
			Endif
		Endif

		If Not w_err
			If g_TERMINALSS
				* - Il driver AMYUNI sotto CITRIX da problemi di creazione file su condivisioni
				* - creo il file sulla temporanea e poi lo sposto sulla cartella in cui l'utente lo aspetta
				Private w_xAPPOFILE, w_xNOMEFILE
				w_xNOMEFILE = This.cFileName
				w_xAPPOFILE = Forcepath(This.cFileName, tempadhoc())
				This.cFileName = w_xAPPOFILE
			Endif
			* - Setto impostazione del driver - *
			iRet=SetDefaultFileName( DRV, This.cFileName )
			If iRet = 0
				cp_ErrorMsg(CP_MSGFORMAT(MSG_ERROR_CREATING__FILE__,Alltrim(This.cFormat),"1"),48,'',.F.)
				w_err = .T.
			Endif
		Endif

		If Not w_err
			iRet=SetFileNameOptions( DRV, 1 + 2 )
			If iRet = 0
				cp_ErrorMsg(CP_MSGFORMAT(MSG_ERROR_CREATING__FILE__,Alltrim(This.cFormat),"2"),48,'',.F.)
				w_err = .T.
			Endif
		Endif

		Local l_Printer
		If Not w_err
			* - Setto la stampante virtuale..
			Do Case
			Case Upper( This.cFormat )=='PDF'
				l_Printer="PDF ADHOC"
			Case Upper( This.cFormat )=='PDF/A' OR UPPER(this.cFormat)=="PDF/A-1B"
				#Define IO_OUTPUTPDFA 0x00000008
				#Define IO_XMPMETADATA 0x00000010
				#Define IO_OUTPUTPDFA8 0x00000020
				#Define Noprompt			0x00000001	&& do not prompt for file name
				#Define UseFileName			0x00000002	&& use file name set by SetDefaultFileName else use document name
				*--- Embed fonts
				#Define EmbedFonts			0x00000010	&& embed fonts used in the input document
				#Define FullEmbed			0x00000200  && embed full font
				#Define EmbedStandardFonts	0x00200000	&& embed standard fonts such as Arial, Times, ...
				#Define EmbedLicensedFonts 	0x00400000	&& embed fonts requiring a license
				#Define EmbedSimulatedFonts	0x01000000	&& embed bold and italic simulated fonts
				#Define MultilingualSupport	0x00000080	&& activate multi-lingual support
        
				iRet=SetFileNameOptions( DRV, Noprompt + UseFileName + EmbedFonts + EmbedStandardFonts + EmbedLicensedFonts + MultilingualSupport)
				If iRet = 0
					cp_ErrorMsg(CP_MSGFORMAT(MSG_ERROR_CREATING__FILE__,Alltrim(This.cFormat),"2"),48,'',.F.)
					w_err = .T.
				Endif
				If Not w_err
					Local l_ImageOptions
					*--- Di dafault facciamo PDF/A compatibili con Acrobat 8
					l_ImageOptions = Iif(Vartype(g_PDFA7)<>'U' And g_PDFA7, IO_OUTPUTPDFA+IO_XMPMETADATA, IO_OUTPUTPDFA8+IO_XMPMETADATA)
					iRet = SetImageOptions(DRV, l_ImageOptions)
					If iRet = 0
						cp_ErrorMsg(CP_MSGFORMAT(MSG_ERROR_CREATING__FILE__,Alltrim(This.cFormat),"4"),48,'',.F.)
						w_err = .T.
					Endif
					Declare Integer SetDefaultConfig In cdintf.Dll Long Handle
					SetDefaultConfig(DRV)
					l_Printer="PDF ADHOC"
				Endif
			Case Upper( This.cFormat )=='HTML'
				l_Printer="HTML ADHOC"
			Case Upper( This.cFormat )=='RTF'
				l_Printer="RTF ADHOC"
			Endcase
			* - .. e la dichiaro di default - *
			iRet=SetDefaultPrinter( DRV )
			If iRet = 0
				=RestoreDefaultPrinter( DRV )
				cp_ErrorMsg(CP_MSGFORMAT(MSG_ERROR_CREATING__FILE__,Alltrim(This.cFormat),"3"),48,'',.F.)
				w_err = .T.
			Endif
			*--- Attivo la stampante
			ACTIVEDLL('A', @DRV)
			*--- Se definita la variabile g_PDFEVENT utilizzo il bindevent, pu� essere utile nel caso in cui la stampa risulti molto lenta e
			*--- la stampante vada in timeout
			If Type("g_PDFEVENT")<>'U' And g_PDFEVENT
				ah_Msg("Gestore eventi stampante PDF attivo", .T.)
				Declare Integer RegisterWindowMessage In user32 String lpString
				&&send messages to our application only
				Declare Integer GetClassName In user32 Integer HWnd,String lpClassName,Integer nMaxCount
				Local nBufsize, cBuffer
				cBuffer = Repli(Chr(32), 250)
				nBufsize = GetClassName(_vfp.HWnd, @cBuffer, Len(cBuffer))
				SendMessagesTo( DRV, cBuffer)
				#Define BroadcastMessages	0x00000020	&&broadcast PDF events
				If Upper( This.cFormat )=='PDF/A' OR UPPER( this.cFormat )=="PDF/A-1B"
					SetFileNameOptions( DRV, Noprompt + UseFileName + EmbedFonts + EmbedStandardFonts + EmbedLicensedFonts + MultilingualSupport + BroadcastMessages)
				Else
					SetFileNameOptions(DRV, 35)	&&BroadcastMessages
				Endif

				goCatchEvt = Createobject('CatchEvt')
				goCatchEvt .hDrv = DRV
				Bindevent(_vfp.HWnd, RegisterWindowMessage("PDF Driver Event"), m.goCatchEvt, 'GotEvt' )
			Endif
			If Empty(This.cGeneraFilePDF)
				Do Case
				Case lower(This.cReportName.FirstReport())='documentoword'
					*--- Attivo la stampante
					Declare Integer SetPrinterParamStr In cdintf.Dll Long hPrinter, String szParam, String szValue
					SetPrinterParamStr(DRV,"Activation Error Title", "")
					SetPrinterParamStr(DRV,"Activation Error Text", "")
					This.oHandle=pHandle
					This.oHandle.ActiveDocument.PrintOUT(false,,,This.cFileName,,,,,,,false)
					*case this.cReportName.FirstReport()='DocumentoOpenOffice'
				Otherwise
					*--- Stampo su stampante virtuale
					*--- Uso una macro per non inserire la cp_chprn nell'eseguibile
					Local l_macro
					l_macro = [do RunRep with null,this.cReportName, "S", "", 1, l_Printer, .t.]
					&l_macro
				Endcase
			Else
				ah_Msg("Inizio generazione file...",.T.)
				Do Case
				Case Upper( This.cFormat )='PDF' And Not Upper(Justext(This.cGeneraFilePDF))$('PDF-ZIP')
					* Elimino il file...
					If cp_fileexist(This.cFileName)
						Delete File (This.cFileName)
					Endif
					ACTIVEDLL('A', @DRV)
					* Gestione lancio applicazione associata al file per generare tramite stampante
					* il file PDF corrispondente...
					iRet=SHELLEX(This.cGeneraFilePDF,'printto','"'+l_Printer+'" "'+This.cFileName+'"')
					* --- La ShellExecute restituisce il comando immediatamente ad ad hoc,
					* --- per evitare che questo rimuova le impostazioni relative al nome file
					* --- occorre attendere che il file sia presente (per questo esiste un passo
					* --- preliminare per eliminare il file). Per farlo perfetto occorrerre lanciare
					* --- la CreateProcess ma avremmo la complicazione di determinare, dato un file
					* ---- il software per aprirlo ed il relativo comando per stamparlo su stampante...
					** Verifico che non vi siano stati errori in fase di generazione file PDF
					If iRet <= 32
						* Si � verificato un errore
						Do Case
						Case iRet=2
							ah_errormsg("File Pdf non generato!%0Bad association",48,'')
						Case iRet=29
							ah_errormsg("File Pdf non generato!%0Failure to load application ",48,'')
						Case iRet=30
							ah_errormsg("File Pdf non generato!%0Application is busy ",48,'')
						Case iRet=31
							ah_errormsg("File Pdf non generato!%0No application association",48,'')
						Otherwise
							ah_errormsg("File Pdf non generato!%0Errore durante la creazione del file",48,'')
						Endcase
					Else
						* File creato con successo
						Local nSec,nDelay
						nSec = 0
						nDelay = 0
						*Verifico se dichiarata la variabile pubblica per impostare il numero max di secondi di attesa, per la creazione del file PDF
						If Vartype(g_PDFDELAY) # 'N'
							* imposto un time out di default pari a 60 secondi
							nDelay = 10
						Else
							nDelay = g_PDFDELAY
						Endif
						*Attendo la creazione del file
						Do While nSec<nDelay And !cp_fileexist(This.cFileName)
							ah_Msg("Searching for PDF...",.F.,.F.,1)
							ACTIVEDLL('A', @DRV)
							nSec=nSec+1
						Enddo
						If nSec=nDelay
							ah_errormsg("File Pdf non generato!%0%1",48,'',Message())
						Endif
					Endif
				Otherwise
					*Invio la mail con allegato il file originario
					ah_Msg("Invio email...",.T.)
					If oCPMapi.mapiService=1 Or g_INVIO$'SP'
						RET = EMAIL(This.cGeneraFilePDF, g_UEDIAL)
					Endif
				Endcase
			Endif
			* - Resetto le impostazioni - *
			iRet=SetFileNameOptions( DRV, 0 )
			=RestoreDefaultPrinter( DRV )
			iRet=SetImageOptions(DRV, 0)
			DriverEnd( DRV )
			*--- Elimino la bindevent
			If Type("g_PDFEVENT")<>'U' And g_PDFEVENT
				Unbindevents(_vfp.HWnd, RegisterWindowMessage("PDF Driver Event"))
			Endif
			Release DRV

			If g_TERMINALSS And w_xNOMEFILE <> This.cFileName
				* - Il driver AMYUNI sotto CITRIX da problemi di creazione file su condivisioni
				* - creo il file sulla temporanea e poi lo sposto sulla cartella in cui l'utente lo aspetta
				Private w_CopyCommand
				w_CopyCommand='COPY FILE "'+w_xAPPOFILE+'" TO "'+w_xNOMEFILE+'"'
				&w_CopyCommand
				w_CopyCommand='DELETE FILE "'+w_xAPPOFILE+'"'
				&w_CopyCommand
				This.cFileName = w_xNOMEFILE
			Endif

		Endif

		On Error &w_actualerr
		* Memorizzo gli errori
		Iif(w_err,This.nErr=1,0)
	Case Upper( This.cTypeClass )=='XFRXIMG'
		****** XFRXIMG ******
		Private loSession, loProgress, lcMsg, loXFF, N

		* Aggancio la libreria XFRX
		loSession=xfrx("XFRX#LISTENER")

		* Creo oggetto progress
		*loUpdate = CREATEOBJECT("progress")
		*loSession.successor = loUpdate

		* Setto i parametri
		* loSession.SetParams(<tcOutputName>, <tcTempDirectory>, <tlNotOpenViewer>, <tcCodePage>,
		*					    <tlSilent>, <tlNewSession>, <tcTarget>, <tcArchive>,
		*						<tlAdditive>, <tlDeleteFileAfter>)
		This.nErr=loSession.SetParams(,,,,,,"XFF")
		Do Case		&&Controllo eventuali errori
		Case This.nErr=-1
			lcMsg=cp_translate(MSG_CANNOT_OPEN_MSWORD)
		Case This.nErr=-2
			lcMsg=cp_translate(MSG_NEEDED_WORD2000_OR_MORE)
		Case This.nErr=-3
			lcMsg=cp_translate(MSG_CANNOT_ACCESS_TO_OUTPUT_FILE)
		Case This.nErr=-4
			lcMsg=cp_translate(MSG_UNKNOWN_FORMAT)
		Case This.nErr=-5
			lcMsg=CP_MSGFORMAT(MSG_UNABLE_TO_OPEN__,'hndlib.dll')
		Case This.nErr=-6
			lcMsg=CP_MSGFORMAT(MSG_UNABLE_TO_OPEN__,'xfrxlib.fll')
		Case This.nErr=-7
			lcMsg=CP_MSGFORMAT(MSG_UNABLE_TO_OPEN__,'zlib.dll')
		Endcase
		If This.nErr<>0
			cp_ErrorMsg(lcMsg,48,'',.F.)
		Else

			* Processo il report
			Public gnStopXFRX	&&Variabile pubblica per fermare XFRX
			Local l_i, l_next, l_first, l_NomeRep, l_NomeCur
			Local l_VarEnv, l_j, l_VarName, l_VarValue, l_bVarEnv
			l_i=0
			l_first=.t.
			Do While l_i<>-1
				l_i=This.cReportName.NextReport(l_i)
				If l_i<>-1
					*--- Setto i_paramqry per permettere l'uso di cp_queryparam()
					i_paramqry=This.cReportName.GetParamqry(l_i)
					*--- Creo l'environment con le variabili l_
					l_VarEnv=This.cReportName.GetVarEnv(l_i)
					l_bVarEnv=.F.
					If Not Isnull(l_VarEnv) And Not l_VarEnv.IsEmpty()
						l_NumVar=l_VarEnv.GetNumVar()
						*--- Creo le variabili
						For l_j = 1 To l_NumVar
							l_VarName=l_VarEnv.GetVarName(l_j)
							&l_VarName=l_VarEnv.GetVarValue(l_j)
						Endfor
						l_bVarEnv=.T.
					Endif
					l_NomeRep=cp_GetStdFile(This.cReportName.GetReport(l_i),'FRX')
					l_NomeCur=This.cReportName.GetCursorName(l_i)
					Select(l_NomeCur)
					Public g_CurCursor
					g_CurCursor=l_NomeCur
					l_next=This.cReportName.NextReport(l_i)
					Set Console Off
					If !(l_next=-1)
						If not l_first And This.cReportName.GetNoReset(l_i)
							Report Form (l_NomeRep) Object loSession Nopageeject Noreset
						Else
							Report Form (l_NomeRep) Object loSession Nopageeject
						Endif
					Else
						If not l_first And This.cReportName.GetNoReset(l_i)
							Report Form (l_NomeRep) Object loSession Noreset
						Else
							Report Form (l_NomeRep) Object loSession
						Endif
					Endif
					Release g_CurCursor
					*--- Rivalorizzo le variabili dell' environment in modo da poterle poi interrogare alla fine della stampa
					If l_bVarEnv  &&Se � true ho creato le variabili quindi le risalvo all'interno dell'oggetto
						For l_j = 1 To l_NumVar
							l_VarName=l_VarEnv.GetVarName(l_j)
							l_VarEnv.SetVarValue(&l_VarName,l_j)
						Endfor
					Endif
				Endif
				l_first=.f.
			Enddo
			* Finalizzo
			loSession.finalize()
			loXFF = loSession.oxfDocument

			Local filename
			filename = Left(This.cFileName,Rat('.',This.cFileName)-1)
			For N = 1 To loXFF.PageCount
				loXFF.SavePicture(filename+"_Pag_"+Alltrim(Str(N))+"."+Alltrim(This.cExtFormat), ;
					this.cFormat,N,N,This.nBpp,This.nJPEGQuality,,)
			Endfor
			Release gnStopXFRX
			* Memorizzo il numero di pagine processate
			This.nNumPagTot=loXFF.PageCount
			If Type('pHandle')='N'
				pHandle=This.nNumPagTot
			Endif
		Endif
		*loUpdate=.null.
		loXFF=.Null.
		loSession=.Null.
		* Sgancio la libreria XFRX
		If g_adhocone
			Release Library ..\..\Exe\xfrxlib.fll
		Else
			If Type("g_NewXFRX")<>"L" Or g_NewXFRX
				Release Library Sys(5)+Sys(2003)+"\xfrxlib\xfrxlib.fll"
			ELSE
				Release Library Sys(5)+Sys(2003)+"\xfrxlibold\xfrxlib.fll"
			ENDIF
		ENDIF
	Case Upper( This.cTypeClass )=='VISUALFOXPRO'
		*--- Determino pParent
		Local pParent, l_macroPrn
		If Upper(_Screen.ActiveForm.Class) == "OPRINT1"
			pParent = _Screen.ActiveForm
		Else
			pParent = This.cReportName
		Endif
		Do Case
		Case Upper( This.cFormat ) == 'TXT'
			l_macroPrn = [do RunRep with null,this.cReportName, 'F', this.cFileName, 1, "", ""]
		Case Upper( This.cFormat ) == 'DBF'
			l_macroPrn = [do cp_ExpRep  with null,'DBF', this.cFileName, pParent]
		Case Upper( This.cFormat ) == 'SDF'
			l_macroPrn = [do cp_ExpRep  with null,'SDF', this.cFileName, pParent]
		Case Upper( This.cFormat ) == 'DELIMITED'
			l_macroPrn = [do cp_ExpRep with null,'DLM', this.cFileName, pParent]
		Endcase
		&l_macroPrn
	Otherwise
		* --- Nessuna classe associata al formato o classe sconosciuta
		lcMsg = Iif(Empty(This.cTypeClass),cp_translate(MSG_NO_CLASS_ASSOCIATE_TO_FORMAT),cp_translate(MSG_UNKNOWN_CLASS))
		cp_ErrorMsg(lcMsg+Chr(13)+ cp_translate(MSG_NOT_CREATED_FILE),48,'',.F.)
		This.nErr=2
	Endcase
	* --- In caso di XFRX utilizzo una variabile pubblica per reperire l'informazione sul numero di pagine
	Public g_PAGENUM
	g_PAGENUM = This.nNumPagTot
	If Not Empty(CursorSet) And Used(CursorSet)
		Select (CursorSet)
	Endif
	Set Console &lcSetConsole
Endproc

enddefine

#define DOCUMENTEVENT_LAST 14
&& this event is privately defined by the Document Converter products
#define DOCUMENTEVENT_ENABLEDPRE (DOCUMENTEVENT_LAST + 1) && before checking if printer is enabled

DEFINE CLASS CatchEvt as custom
	hDrv = 0
	PROCEDURE GotEvt( HWND_hwnd, UINT_uMsg, WPARAM_wParam, LPARAM_lParam )
		IF m.WPARAM_wParam = DOCUMENTEVENT_ENABLEDPRE
			LOCAL mDrv
			mDrv = This.hDrv
			ACTIVEDLL('A', @mDrv)		
			ah_Msg("Stampante PDF attivata", .t.)	
		ENDIF 		
	ENDPROC
ENDDEFINE

* ===================================================================================================
* Classe per gestione Progress Bar semplificata con sola wait windows
* ===================================================================================================
DEFINE CLASS progress AS CUSTOM
lViewProgress=.t.			&& Flag visualizza wait window
PROC Init
  PARAMETERS pViewProgress
  lViewProgress=pViewProgress
ENDPROC

PROC updateProgress
  lpara ta,tb, tc
  if this.lViewProgress
    * Mostra informazioni parziali
    cp_Msg("Page #: "+allt(str(tb))+" Report #: "+allt(str(ta))+" ("+allt(str(tc))+"%)")
  ENDIF
ENDPROC
ENDDEFINE


* --- Classe utilizzata da CASTIVARATE, parametro che contiene le informazioni
* --- sul documento di cui si vuole calcolare castelletto iva e rate
  DEFINE CLASS DOCOBJ AS CUSTOM
   cMVSERIAL=''
   cFLINCA='C' &&Possibili valori: 'S' (Attivo), ' ' (Disattivo), 'C' (Calcolato)
   mcHeader=.null.
   mcDetail=.null.
   cRead='N' &&'A' rilegge tutti i dati, 'H' rilegge solo dati di testata, 
             * 'D' rilegge i dati di dettaglio, 'N' non rilegge
   cAzione=' '
   cParam='D' && Parametro per pilotare comportamenti particolari in base alla gestione..
   cSoloRate=.f. && Se a .t. calcola solo le rate in base ai valori presenti in testata (totale imponibile / imposta..)
   * ---- Importi da passare per determinare le sole rate..
   cNoRelease=.f. && Se a .t. non rilascia i cursori
   nTOTIMPOS=0 && Totale imposta
   nTOTIMPON=0 && Totale imponibole
   nIMPSPE=0 && Totale Spese
   nMVSPEBOL=0 && Spese Bolli
   nMVACCONT=0 && Acconto
   nTROVPAG=0 && Verifica se tipo pagamento a incasso
   nTROVSAL=0 && Verifica se tipo pagamento a saldo totale
   * ---- Fine importi da passare per determinare le rate..
   proc init(pParent)
     this.mcHeader=createobject("cp_MemoryCursor",".\query\HeaderInfo.vqr",pParent)
     this.mcDetail=createobject("cp_MemoryCursor",".\query\DetailInfo.vqr",pParent)
   endproc  
   PROCEDURE Done
    this.mcHeader.Done()
    this.mcDetail.Done()
   Endproc  
  ENDDEFINE
  
  * --- Classe risultato della procedura CASTIVARATE, contiene Castelletto e Rate
  DEFINE CLASS CASTIVA AS CUSTOM
   mcCastIva = .null.
   mcRate=.null.
   nMVVALMAG=0
   nTOTIMPOS=0
   nTOTIMPON=0
   nMVSPEBOL=0
   nMVIMPFIN=0
   nMVIMPACC=0
   nTOTRATE=0
   nBOLARR=0
   nIMPSPE=0
   nErrorLog=''
   cWarningLog=''
   nMVACCONT=0
   nMVFLSALD=''
   nTROVPAG=0
   nTROVSAL=0 
   proc init(pParent)
     this.mcCastIva=createobject("cp_MemoryCursor",".\query\CastIvaInfo.vqr",pParent)
     this.mcRate=createobject("cp_MemoryCursor",".\query\RateInfo.vqr",pParent)
   ENDPROC
   PROCEDURE Done
    this.mcCastIva.Done()
    this.mcRate.Done()
   Endproc     
ENDDEFINE
* --- Classe per gestire DOCINFO ache nelle funzioni
DEFINE CLASS PARAMETRO AS CUSTOM 
 w_MVSERIAL="" 
 FUNCTION IsVar(i_var)
 	RETURN TYPE('This.'+ALLTRIM(i_var))<>'U'
 ENDFUNC 
 FUNCTION GetVar(i_var)
    mc='This.'+ALLTRIM(i_var)
 	RETURN &mc
 ENDFUNC 
ENDDEFINE

* --- Lancia Tutorial da men�
PROCEDURE Cp_Helptutorial()
 help Sistema informativo
endproc

* ===================================================================================================
* Classe per invio Mail SMTP
* ===================================================================================================

DEFINE CLASS vfpSock As Session

  * Properiet� pubbliche:
  * N - Stato
  * N - Bytes Ricevuti (Sola lettura)
  * C - Host (Sola lettura)
  * C - IP (Sola lettura)
  * N - Porta (Sola lettura)
  * C - Stringa d'ingresso (lettura/Scrittura)
  *
  * Metodi pubblici:
  * L - Connect( cServer, nServerPort )
  * L - Close()
  * L - SendData( cData )
  * L - GetData( @cDataOut )

  * Stato connessione
  * 0 Chiusa (Predefinito)
  * 1 Aperta
  * 2 In ascolto
  * 3 Connessione appesa
  * 4 Ricerca Host
  * 5 Host identificato
  * 6 In connessione
  * 7 Connesso
  * 8 L'Host sta terminando la connessione
  * 9 Errore
  State = 0
  BytesReceived = 0

  host = ""
  IP = ""
  Port = 80
  hSocket = 0
  cIn = ''

  WaitForRead = 0

  * Costanti lettura informazioni (modificabili):
  #DEFINE READ_SIZE 16384
  #DEFINE READ_FROM_SERVER_TIMEOUT 200

  * Costanti per API :
  #DEFINE SMTP_PORT 25
  #DEFINE HTTP_PORT 80
  #DEFINE AF_INET 2
  #DEFINE SOCK_STREAM 1
  #DEFINE IPPROTO_TCP 6
  #DEFINE SOCKET_ERROR -1
  #DEFINE FD_READ 1
  #DEFINE HOSTENT_SIZE 16

  FUNCTION Connect( tcServer, tnServerPort )
    LOCAL cBuffer, cPort, cHost, lResult

    THIS.IP = THIS.GetIP(tcServer)
    IF EMPTY(THIS.IP)
      RETURN .F.
    ENDIF
    THIS.Host = tcServer

    THIS.Port = tnServerPort

    THIS.hSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)
    IF THIS.hSocket = SOCKET_ERROR
        RETURN .F.
    ENDIF

    THIS.State = 6
    cPort = THIS.num2word(htons(THIS.Port))
    nHost = inet_addr(THIS.IP)
    cHost = THIS.num2dword(nHost)
    cBuffer = THIS.num2word(AF_INET) + cPort + cHost + Repli(Chr(0),8)
    lResult = (ws_connect(THIS.hSocket, @cBuffer, Len(cBuffer))=0)
    IF lResult
      THIS.State = 7
    ELSE
      THIS.State = 0
    ENDIF
  RETURN lResult

  FUNCTION Close
    if THIS.hSocket<>SOCKET_ERROR
      = closesocket(THIS.hSocket)
    endif
    THIS.hSocket = SOCKET_ERROR
    THIS.State = 0
  ENDFUNC

  FUNCTION SendData( cData )
    LOCAL cBuffer, nResult
    cBuffer = cData
    nResult = send(THIS.hSocket, @cBuffer, Len(cBuffer), 0)
    IF nResult = SOCKET_ERROR
        RETURN .F.
    ENDIF
    RETURN .T.
  ENDFUNC

  FUNCTION GetData( tcOutData )
    * NOTE: tcOutData MUST be passed by reference, ie: Sock.GetData( @Outstr )
    tcOutData = THIS.cIn
    THIS.cIn = ''
  ENDFUNC

  * Metodi privati:

  FUNCTION BytesReceived_Access
    THIS.Rd()
    RETURN LEN(THIS.cIn)
  ENDFUNC

  PROTECTED FUNCTION Rd
    LOCAL hEventRead, nWait, cRead, cRecv, nRecv, nFlags, lcRead
    DO WHILE .T.
        * creating event, linking it to the socket and wait
        hEventRead = WSACreateEvent()
        = WSAEventSelect(THIS.hSocket, hEventRead, FD_READ)

        * 1000 milliseconds can be not enough
        THIS.WaitForRead = WSAWaitForMultipleEvents(1, @hEventRead, 0, READ_FROM_SERVER_TIMEOUT, 0)
        = WSACloseEvent(hEventRead)

        IF THIS.WaitForRead <> 0 && error or timeout
            EXIT
        ENDIF

        * Lettura dati dal socket di connessione

        cRecv = Repli(Chr(0), READ_SIZE)
        nFlags = 0
        nRecv = recv(THIS.hSocket, @cRecv, READ_SIZE, nFlags)


        IF nRecv>0
          THIS.cIn = THIS.cIn + LEFT(cRecv, nRecv)
        ENDIF
    ENDDO
  ENDFUNC

  PROCEDURE Init()
    DECLARE INTEGER gethostbyname IN ws2_32 STRING host
    DECLARE STRING inet_ntoa IN ws2_32 INTEGER in_addr
    DECLARE INTEGER socket IN ws2_32 INTEGER af, INTEGER tp, INTEGER pt
    DECLARE INTEGER closesocket IN ws2_32 INTEGER s
    DECLARE INTEGER WSACreateEvent IN ws2_32
    DECLARE INTEGER WSACloseEvent IN ws2_32 INTEGER hEvent
    DECLARE GetSystemTime IN kernel32 STRING @lpSystemTime
    DECLARE INTEGER inet_addr IN ws2_32 STRING cp
    DECLARE INTEGER htons IN ws2_32 INTEGER hostshort
    DECLARE INTEGER WSAStartup IN ws2_32 INTEGER wVerRq, STRING lpWSAData
    DECLARE INTEGER WSACleanup IN ws2_32

    DECLARE INTEGER connect IN ws2_32 AS ws_connect ;
        INTEGER s, STRING @sname, INTEGER namelen

    DECLARE INTEGER send IN ws2_32;
        INTEGER s, STRING @buf, INTEGER buflen, INTEGER flags

    DECLARE INTEGER recv IN ws2_32;
        INTEGER s, STRING @buf, INTEGER buflen, INTEGER flags

    DECLARE INTEGER WSAEventSelect IN ws2_32;
        INTEGER s, INTEGER hEventObject, INTEGER lNetworkEvents

    DECLARE INTEGER WSAWaitForMultipleEvents IN ws2_32;
        INTEGER cEvents, INTEGER @lphEvents, INTEGER fWaitAll,;
        INTEGER dwTimeout, INTEGER fAlertable

    DECLARE RtlMoveMemory IN kernel32 As CopyMemory;
        STRING @Dest, INTEGER Src, INTEGER nLength


    IF WSAStartup(0x202, Repli(Chr(0),512)) <> 0
      * Impossibile inizializzare il socket
      RETURN .F.
    ENDIF
    RETURN .T.
  ENDPROC

  PROCEDURE Destroy

    = WSACleanup()
  ENDPROC

  PROTECTED FUNCTION GetIP( pcHost )
      LOCAL nStruct, nSize, cBuffer, nAddr, cIP
      nStruct = gethostbyname(pcHost)
      IF nStruct = 0
          RETURN ""
      ENDIF
      cBuffer = Repli(Chr(0), HOSTENT_SIZE)
      cIP = Repli(Chr(0), 4)
      = CopyMemory(@cBuffer, nStruct, HOSTENT_SIZE)
      = CopyMemory(@cIP, THIS.buf2dword(SUBS(cBuffer,13,4)),4)
      = CopyMemory(@cIP, THIS.buf2dword(cIP),4)
  RETURN inet_ntoa(THIS.buf2dword(cIP))
  ENDFUNC

  FUNCTION buf2dword(lcBuffer)
    RETURN Asc(SUBSTR(lcBuffer, 1,1)) + ;
        BitLShift(Asc(SUBSTR(lcBuffer, 2,1)), 8) +;
        BitLShift(Asc(SUBSTR(lcBuffer, 3,1)), 16) +;
        BitLShift(Asc(SUBSTR(lcBuffer, 4,1)), 24)
  ENDFUNC

  FUNCTION num2dword(lnValue)
      IF lnValue < 0
          lnValue = 0x100000000 + lnValue
      ENDIF
      LOCAL b0, b1, b2, b3
      b3 = Int(lnValue/2^24)
      b2 = Int((lnValue - b3*2^24)/2^16)
      b1 = Int((lnValue - b3*2^24 - b2*2^16)/2^8)
      b0 = Mod(lnValue, 2^8)
    RETURN Chr(b0)+Chr(b1)+Chr(b2)+Chr(b3)
  ENDFUNC

  FUNCTION num2word(lnValue)
    RETURN Chr(MOD(m.lnValue,256)) + CHR(INT(m.lnValue/256))
  ENDFUNC
ENDDEFINE

* ===================================================================================================
* Classe per la gestione dei messaggi, compresi quelli incrementali
* ===================================================================================================
DEFINE CLASS AH_Message AS Custom
    icon=''
    title=''
    nowait=.t.
    noclear=.f.
    timeout=.f.
   	* collezione di items
    ADD OBJECT AH_MsgParts as Collection
	* metodi
	PROCEDURE AddMsgPart(pMsg)
		LOCAL oMsgPart
		oMsgPart=CREATEOBJECT('AH_MsgPart')
		oMsgPart.text=pMsg
		* aggiunge pezzo di messaggio a collezione
		this.AH_MsgParts.Add(oMsgPart)
		* ritorna puntamento a pezzo
		RETURN (oMsgPart)
	ENDPROC
	* aggiunge al messaggio n-a-capo
	PROCEDURE AddNewLines(pNLines)
		LOCAL oMsgPart
		* check par
		pNLines=IIF(VARTYPE(pNLines)<>'N' or pNLines=0,1,pNLines)
		* crea parte
		oMsgPart=this.AddMsgPart(REPLICATE(CHR(13)+CHR(10),pNLines))
		* ritorna puntamento a pezzo
		RETURN (oMsgPart)
	ENDPROC
	PROCEDURE AddMsgPartNL(pMsg,pNLines)
		LOCAL oMsgPart
		* check par
		pNLines=IIF(VARTYPE(pNLines)<>'N' or pNLines=0,1,pNLines)
		* crea parte del messaggio vero e proprio
		oMsgPart=this.AddMsgPart(pMsg)
		* aggiunge gli a-capo
		this.AddNewLines(pNLines)
		* ritorna puntamento a pezzo di testo vero e proprio
		RETURN (oMsgPart)
	ENDPROC
	
	PROCEDURE ComposeMessage(pClear)
		LOCAL i,j,cMsg,cText,cTmp,cInd,lTranslate
		LOCAL cPar1,cPar2,cPar3,cPar4,cPar5
		STORE .F. TO cPar1,cPar2,cPar3,cPar4,cPar5
		pClear=IIF(Parameters()<1,.T.,pClear)
		i=0
		* Variabile che conterr� il messaggio
		cMsg = ''
		* Cicla sulle singole parti della stringa
		 DO while (pClear and this.AH_MsgParts.count<>0) or (!pClear and i<this.AH_MsgParts.count) 
			i=iif(pClear,1,i+1)
			* testo della parte di messaggio
			cText=this.AH_MsgParts(i).text
			lTranslate=this.AH_MsgParts(i).translate
			* se il testo � composto solo da chr(13) non deve essere passato alla cp_translate
			IF cText==REPLICATE(CHR(13),LEN(cText))
				lTranslate=.F.
			ENDIF
			* traduzione 
			IF this.AH_MsgParts(i).AH_MsgParams.count>0
				* la parte di stringa � parametrica
				FOR j=1 TO this.AH_MsgParts(i).AH_MsgParams.count
					* valorizza il generico parametro
					cTmp = this.AH_MsgParts(i).AH_MsgParams(j).text
					IF this.AH_MsgParts(i).AH_MsgParams(j).translate
						cTmp=ah_msgformat(cTmp)
					ENDIF
					* valorizza il parametro
					cInd = STR(j,1,0)
					cPar&cInd = cTmp
				NEXT
				* esegue la AH_msgformat
				cTmp=AH_MsgFormat(cText,cPar1,cPar2,cPar3,cPar4,cPar5)		
			ELSE
				* la parte non � parametrica
				IF lTranslate
					cTmp=ah_msgformat(cText)				
				ELSE
					cTmp=cText
				ENDIF
			ENDIF	
			*
			cMsg = cMsg + cTmp
			if pClear
				THIS.AH_MsgParts.Remove(i)
			endif
		 ENDDO
		*Sbianco parametro
		 Return(cMsg)
	ENDPROC
	*
	PROCEDURE AH_MSG(pClear)
		LOCAL cText
		pClear=IIF(Parameters()<1,.T.,pClear)
		cText=this.ComposeMessage(pClear)
		CP_MSG(cText,this.nowait,this.noclear,this.timeout,.F.)
	ENDPROC
	*
	PROCEDURE AH_ERRORMSG(pIcon,pTitle,pClear)
		LOCAL cText,cIcon,cTitle
		pClear=IIF(Parameters()<3,.T.,pClear)
		* controllo parametri
		IF VARTYPE(pIcon)$'CN' AND Not(EMPTY(pIcon))
			this.icon=pIcon
		ENDIF	
		IF VARTYPE(pTitle)='C' AND Not(EMPTY(pTitle))
			this.title=pTitle
		ENDIF	
		* Compone messaggio
		cText=this.ComposeMessage(pClear)
		* i_cSyncTitle � utilizzato nei messaggi all'utente per notificare nel titolo l'azienda
		IF TYPE("i_cSyncTitle")<>"C" OR EMPTY(i_cSyncTitle)
		    CP_ERRORMSG(cText,this.icon,this.title,.f.)
		ELSE
		    IF TYPE("this.title")="C" AND NOT EMPTY( this.title )
		        i_cSyncTitle = i_cSyncTitle + " " + this.title
		    ENDIF
		    CP_ERRORMSG(cText,this.icon,i_cSyncTitle,.f.)
		ENDIF
		*
	ENDPROC
		*
	PROCEDURE AH_YESNO(pIcon,pClear,p_NoBtnSel)
		LOCAL cText
		pClear=IIF(Parameters()<2,.T.,pClear)
		* controllo parametri
		IF VARTYPE(pIcon)$'CN' AND Not(EMPTY(pIcon))
			this.icon=pIcon
		ENDIF	
		* go ...
		cText=this.ComposeMessage(pClear)
		* i_cSyncTitle � utilizzato nei messaggi all'utente per notificare nel titolo l'azienda
        IF TYPE("i_cSyncTitle")<>"C" OR EMPTY(i_cSyncTitle)
            Return(CP_YESNO(cText,this.icon,.f.,p_NoBtnSel))
		ELSE
            Return(CP_YESNO(cText,this.icon,.f.,p_NoBtnSel,i_cSyncTitle))
		ENDIF
		
	ENDPROC
ENDDEFINE
	
DEFINE CLASS AH_MsgPart as Custom
	* propriet�
	text=''
	translate=.t.
	* collezione di items
    ADD OBJECT AH_MsgParams as Collection
	* metodi
	PROCEDURE AddParam(pParam,pTranslate)
		LOCAL oMsgParam
		oMsgParam=CREATEOBJECT('AH_MsgParam')
		oMsgParam.text=pParam
		oMsgParam.translate=pTranslate
		* aggiunge pezzo di messaggio a collezione
		this.AH_MsgParams.Add(oMsgParam)
		* ritorna puntamento a parametro
		RETURN (oMsgParam)
	ENDPROC
ENDDEFINE

DEFINE CLASS AH_MsgParam as Custom
	* propriet�
	text=''
	translate=.f.
ENDDEFINE

* ===================================================================================================
* Funzioni semplificate per la gestione della messaggistica di interfaccia
* ===================================================================================================
proc AH_Msg(p_msg,p_Nowait,p_Noclear,p_Timeout,p_P1,p_P2,p_P3,p_P4,p_P5)
  * --- This function allow to use parametric messagges in CP_MSG function
  * --- p_Msg=message, 
  *     the parameters are specified in messages with %1, %2, %3, %4, %5
  * --- p_Nowait  specifies that the program continues program execution 
  *     immediately after the message is displayed
  * --- p_Noclear specifies that a WAIT message window remains on the main window 
  *     until WAIT CLEAR or another WAIT WINDOW command is issued, 
  *     or a Visual FoxPro system message is displayed. 
  * --- p_Timeout specifies the number of seconds that can elapse without 
  *     input from the keyboard or the mouse before the WAIT is terminated
  * --- p_P1=message's first parameter
  * --- p_P2=message's second parameter
  * --- p_P3=message's third parameter
  * --- p_P4=message's fourth parameter
  * --- p_P5 =message's fifth parameter
  p_Nowait = IIF(Parameters()<2,.T.,p_Nowait)
  * usa sempre la ah_MsgFormat() per tradurre ...
  CP_Msg(AH_MsgFormat(p_msg,p_P1,p_P2,p_P3,p_P4,p_P5),p_Nowait,p_Noclear,p_Timeout,.F.)
endproc

func AH_YESNO(p_msg,p_Icon,p_P1,p_P2,p_P3,p_P4,p_P5,p_NoBtnSel)
  local i_yn
  * --- This function allow to use parametric messagges in CP_YESNO function
  * --- p_Msg=message, 
  *     the parameters are specified in messages with %1, %2, %3, %4, %5
  * --- p_Icon=icon of message's mask
  * --- p_P1=message's first parameter
  * --- p_P2=message's second parameter
  * --- p_P3=message's third parameter
  * --- p_P4=message's fourth parameter
  * --- p_P5 =message's fifth parameter
  * usa sempre la ah_MsgFormat() per tradurre ...
  * i_cSyncTitle � utilizzato nei messaggi all'utente per notificare nel titolo l'azienda
  IF TYPE("i_cSyncTitle")<>"C" OR EMPTY(i_cSyncTitle)  
    i_yn=CP_YesNo(AH_MsgFormat(p_msg,p_P1,p_P2,p_P3,p_P4,p_P5),p_Icon,.F.,p_NoBtnSel)
  ELSE
    i_yn=CP_YesNo(AH_MsgFormat(p_msg,p_P1,p_P2,p_P3,p_P4,p_P5),p_Icon,.F.,p_NoBtnSel,i_cSyncTitle)
  ENDIF
  * result
  return(i_yn)
endfunc

proc AH_ErrorMsg(p_Msg,p_Icon,p_Title,p_P1,p_P2,p_P3,p_P4,p_P5)
  * --- This function allow to use parametric messagges in CP_ERRORMSG function
  * --- p_Msg=error message, 
  *     the parameters are specified in messages with %1, %2, %3, %4, %5
  * --- p_Icon=icon of error message's mask
  * --- p_Title=title of error message's mask
  * --- p_P1=error message's first parameter
  * --- p_P2=error message's second parameter
  * --- p_P3=error message's third parameter
  * --- p_P4=error message's fourth parameter
  * --- p_P5 =error message's fifth parameter
  * usa sempre la ah_MsgFormat() per tradurre ...
  * i_cSyncTitle � utilizzato nei messaggi all'utente per notificare nel titolo l'azienda  
  IF TYPE("i_cSyncTitle")<>"C" OR EMPTY(i_cSyncTitle)
    CP_ErrorMsg(AH_MsgFormat(p_Msg,p_P1,p_P2,p_P3,p_P4,p_P5),p_Icon,p_Title,.F.)
  ELSE
    * i_cSyncTitle � usato dalla sincronizzazione per proporre il nome dell'azienda su cui si eseguono le operazioni
    CP_ErrorMsg(AH_MsgFormat(p_Msg,p_P1,p_P2,p_P3,p_P4,p_P5),p_Icon,i_cSyncTitle,.F.)  
  ENDIF
endproc

func AH_MsgFormat(i_cFmt,i_cP1,i_cP2,i_cP3,i_cP4,i_cP5,i_cP6,i_cP7,i_cP8,i_cP9)
  local i_p
  i_cFmt=cp_Translate(i_cFmt)
  * parametro di default %0 (chr(13))
  i_p=at('%0',i_cFmt)
  if i_p<>0
    i_cFmt=strtran(i_cFmt,'%0',CHR(13)+CHR(10))
  endif
  * "veri parametri"
  i_p=at('%1',i_cFmt)
  if i_p<>0 
	  IF type('i_cP1')='N'
	   i_cP1=ALLTRIM(STR(i_cp1 ))
	  endif
	  if type('i_cP1')='C'
	    i_cFmt=strtran(i_cFmt,'%1',i_cP1)
	  ENDIF
  ENDIF
  
  
  i_p=at('%2',i_cFmt)
  if i_p<>0 
	  IF type('i_cP2')='N'
	   i_cP2=ALLTRIM(STR(i_cp2 ))
	  endif
	  if type('i_cP2')='C'
	    i_cFmt=strtran(i_cFmt,'%2',i_cP2)
	  ENDIF
  ENDIF
  i_p=at('%3',i_cFmt)
  i_p=at('%3',i_cFmt)
  if i_p<>0 
	  IF type('i_cP3')='N'
	   i_cP3=ALLTRIM(STR(i_cp3 ))
	  endif
	  if type('i_cP3')='C'
	    i_cFmt=strtran(i_cFmt,'%3',i_cP3)
	  ENDIF
  ENDIF
  i_p=at('%4',i_cFmt)
  if i_p<>0 
	  IF type('i_cP4')='N'
	   i_cP4=ALLTRIM(STR(i_cp4 ))
	  endif
	  if type('i_cP4')='C'
	    i_cFmt=strtran(i_cFmt,'%4',i_cP4)
	  ENDIF
  ENDIF
  i_p=at('%5',i_cFmt)
  if i_p<>0 
	  IF type('i_cP5')='N'
	   i_cP5=ALLTRIM(STR(i_cp5 ))
	  endif
	  if type('i_cP5')='C'
	    i_cFmt=strtran(i_cFmt,'%5',i_cP5)
	  ENDIF
  ENDIF
  i_p=at('%6',i_cFmt)
  if i_p<>0 
	  IF type('i_cP6')='N'
	   i_cP6=ALLTRIM(STR(i_cp6 ))
	  endif
	  if type('i_cP6')='C'
	    i_cFmt=strtran(i_cFmt,'%6',i_cP6)
	  ENDIF
  ENDIF
  i_p=at('%7',i_cFmt)
  if i_p<>0 
	  IF type('i_cP7')='N'
	   i_cP7=ALLTRIM(STR(i_cp7 ))
	  endif
	  if type('i_cP7')='C'
	    i_cFmt=strtran(i_cFmt,'%7',i_cP7)
	  ENDIF
  ENDIF
  i_p=at('%8',i_cFmt)
  if i_p<>0 
	  IF type('i_cP8')='N'
	   i_cP8=ALLTRIM(STR(i_cp8 ))
	  endif
	  if type('i_cP8')='C'
	    i_cFmt=strtran(i_cFmt,'%8',i_cP8)
	  ENDIF
  ENDIF
  i_p=at('%9',i_cFmt)
  if i_p<>0 
	  IF type('i_cP9')='N'
	   i_cP9=ALLTRIM(STR(i_cp9 ))
	  endif
	  if type('i_cP9')='C'
	    i_cFmt=strtran(i_cFmt,'%9',i_cP9)
	  ENDIF
  ENDIF
  return(i_cFmt)

* ===================================================================================================
* FUNZIONE utilizzate nella gestione del Controllo flussi
* ===================================================================================================
    Function FormFlus(oactform)
        Local actform, cFile
        actform=oactform
        cFile=""
        Do While Empty(cFile) And Type("actform.parent")="O"
            actform=actform.Parent
            cFile = Iif(Pemstatus(actform,"cFile", 5) And Pemstatus(actform,"cTrsName", 5), actform.cFile , "")
        Enddo
        Return Iif(Empty(cFile), .Null., actform)
    Endfunc

* ===================================================================================================
* Classe per gestione stampa messaggi di errore
* ===================================================================================================
DEFINE CLASS AH_ErrorLog AS CUSTOM	
Lprintsave=.f.
cCursMessErr=''
Lerror=.f.
clasterror=''
PROCEDURE Init()
	local w_OldArea, w_err, L_errsav
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	this.Lprintsave=.f.
	this.cCursMessErr = Sys(2015)
	CREATE CURSOR (this.cCursMessErr) (MSG M)
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
 	RETURN Used(this.cCursMessErr)
ENDPROC		

PROC AddMsgLog(pMsg,pparmsg1,pparmsg2,pparmsg3,pparmsg4,pparmsg5)
	local w_OldArea, w_err, L_errsav
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	if type("pMsg")<>"C"
		pMsg = '' 
	endif
	pMsg = AH_MsgFormat(pMsg,pparmsg1,pparmsg2,pparmsg3,pparmsg4,pparmsg5)
	Insert Into (this.cCursMessErr)( MSG ) Values ( pMsg )
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
	RETURN (not(w_err))
ENDPROC

PROC AddMsgLogNoTranslate(pMsg)
	local w_OldArea, w_err, L_errsav
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	if type("pMsg")<>"C"
		pMsg = '' 
	endif
	Insert Into (this.cCursMessErr)( MSG ) Values ( pMsg )
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
	RETURN (not(w_err))
ENDPROC

PROC AddMsgLogPartTrans(pPart,pMsg,pparmsg1,pparmsg2,pparmsg3,pparmsg4,pparmsg5)
	local w_OldArea, w_err, L_errsav
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	if type("pPart")<>"C"
		pPart = '' 
	endif
	if type("pMsg")<>"C"
		pMsg = '' 
	endif
	pMsg = AH_MsgFormat(pMsg,pparmsg1,pparmsg2,pparmsg3,pparmsg4,pparmsg5)
	pPart = AH_MsgFormat(pPart)
	Insert Into (this.cCursMessErr)( MSG ) Values ( pPart+pMsg )
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
	RETURN (not(w_err))
ENDPROC

PROC AddMsgLogPartNoTrans(pPart,pMsg,pparmsg1,pparmsg2,pparmsg3,pparmsg4,pparmsg5)
	local w_OldArea, w_err, L_errsav
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	if type("pPart")<>"C"
		pPart = '' 
	endif
	if type("pMsg")<>"C"
		pMsg = '' 
	endif
        pMsg = AH_MsgFormat(pMsg,pparmsg1,pparmsg2,pparmsg3,pparmsg4,pparmsg5)
	Insert Into (this.cCursMessErr)( MSG ) Values ( pPart+pMsg )
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
	RETURN (not(w_err))
ENDPROC

PROC IsFullLog
	local w_OldArea, w_err, L_errsav
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	w_fullcur=RECCOUNT(this.cCursMessErr)>0
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
	return w_fullcur
ENDPROC

PROC PrintLog(pParentObject,ptitle,pQueryMess,pQueryTitle)
	local w_OldArea, w_err, L_errsav, w_printfile
	w_err=.f.
	pQueryMess=IIF(PARAMETERS()<3,.t.,pQueryMess)
	pQueryTitle=IIF(PARAMETERS()<4 or TYPE('pQueryTitle')<>'C',MSG_PRINT_ERROR_LOG_QP, pQueryTitle)
	w_OldArea=select()
	L_errsav=on('ERROR')
	on error w_err=.t.
	w_printfile=.t.
	if RECCOUNT(this.cCursMessErr)>0
		if pQueryMess
			w_printfile=AH_YESNO(pQueryTitle)
		endif
		if w_printfile
			L_TITOLO=IIF(TYPE('ptitle')='C',AH_MsgFormat(ptitle),AH_MsgFormat(MSG_ERROR_REPORT))
			SELECT * FROM (this.cCursMessErr) INTO CURSOR __TMP__
			* lancia stampa
			local l_sPrg
            l_sPrg="cp_chprn"
            
			IF VARTYPE(pParentObject)='O'
			    do (l_sPrg) WITH "QUERY\GSUT_ERR.FRX",'',pParentObject 
			ELSE
			    do (l_sPrg) WITH "QUERY\GSUT_ERR.FRX"			
			ENDIF	
			* chiude cursore
			use in Select('__TMP__')
			this.Lprintsave=.t.
		endif
	endif
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
ENDPROC

PROC SaveLog(ptitle,pNomeFile, pEXT, pOPZIONI)
	local w_OldArea, w_err, L_errsav, L_PagGen, L_TITOLO
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	L_PagGen=-1
	L_TITOLO=IIF(TYPE('ptitle')='C',ptitle,CP_TRANSLATE(MSG_ERROR_REPORT))
	SELECT * FROM (this.cCursMessErr) INTO CURSOR __TMP__
	Lsavefile=Print_To_File(pNomeFile, "QUERY\GSUT_ERR.FRX", pEXT, pOPZIONI, @L_PagGen,.t.)
	use in Select('__TMP__')
	this.Lprintsave=.t.
	if w_err or !Lsavefile
		this.Lerror=.t.
		if !Lsavefile
			this.clasterror='Error at Print_To_File'
		else
			this.clasterror=message()
		endif
	endif
	on error &L_errsav
	select (w_OldArea)
	return Lsavefile
ENDPROC

PROC JustPrintSave
	RETURN this.Lprintsave
ENDPROC

PROC ClearLog
	local w_OldArea, w_err, L_errsav
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	select(this.cCursMessErr)
        if Reccount()>0
		zap
	endif
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
ENDPROC

PROC NumRecLog
	local w_OldArea, w_err, L_errsav, l_NumRec
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	l_NumRec=RECCOUNT(this.cCursMessErr)
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	on error &L_errsav
	select (w_OldArea)
	return l_NumRec
ENDPROC

PROCEDURE GetRecordByIndex(nIndex)
	Local cRetVal, w_OldArea, w_err, L_errsav
	cRetVal=""
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	If nIndex<=this.NumRecLog
		Select (this.cCursMessErr)
		GO m.nIndex
		cRetVal=ALLTRIM(NVL(MSG, ' '))
	EndIf
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	ON ERROR &L_errsav
	select (w_OldArea)
	return cRetVal
ENDPROC

PROCEDURE GetLog
	Local cRetVal, w_OldArea, w_err, L_errsav
	cRetVal=""
	w_OldArea=select()
	w_err=.f.
	L_errsav=on('ERROR')
	on error w_err=.t.
	Select (this.cCursMessErr)
	GO TOP
	SCAN
			cRetVal=m.cRetVal+IIF(EMPTY(m.cRetVal), '', chr(13)+chr(10) ) + ALLTRIM(NVL(MSG, ' '))
	ENDSCAN
	if w_err
		this.Lerror=.t.
		this.clasterror=message()
	endif
	ON ERROR &L_errsav
	select (w_OldArea)
	return cRetVal
ENDPROC

PROCEDURE Destroy
	use in Select(this.cCursMessErr)
	use in Select('__TMP__')
	Lprintsave=.f.
	cCursMessErr=''
ENDPROC

enddefine

*---Zucchetti Bidge Client - Inizio
PROCEDURE ZBrgCli
Lparameters tcCommand, tcRemoteHost ,tnPort, tcFileOrigin, tcFileCommand
	#Define EOT Chr(4) && End of Transmission sign
	If Vartype(tnPort) # "N"
		tnPort = 63333
	Endif
	If Vartype(tcRemoteHost) # "C"
		tcRemoteHost = "localhost"
	ENDIF
	If Vartype(tcFileCommand) # "C"
		tcFileCommand = ""
	ENDIF
	If Vartype(tcFileOrigin) # "C"
		tcFileOrigin = ""
	Endif
	oForm = Createobject('ClientForm',tnPort,tcRemoteHost, tcCommand, tcFileOrigin, tcFileCommand)
	bRet=oForm.bOK
	Release oForm
	Return bRet
ENDPROC 

Define Class ClientForm As Form
	nProtocol = 0
	nPort = 0
	cRMHost = ""
	AutoCenter = .T.
	Caption = 'TCP-Client'
	nStat = 0
	lConnected = .F.
	bOK=.F.
	cFileCommand= ""
	cFileOrigin= ""

	Procedure Load
		Sys(2333,0)
		_vfp.AutoYield = .F.
		Return
	Endproc

	Procedure Unload
		_vfp.AutoYield = .T.
		Return
	Endproc

	Procedure QueryUnload
		Clear Events
		Return
	Endproc

	Procedure Init
		Lparameters tnPort, tcRemoteHost, tcCommand, tcFileOrigin , tcFileCommand
		Thisform.nPort = tnPort
		Thisform.cRMHost = tcRemoteHost
		Thisform.cFileCommand = tcFileCommand
		Thisform.cFileOrigin = tcFileOrigin
		IF Empty(Thisform.cFileCommand)
			*--- Creo Socket Client
			This.AddObject('oSock', 'TCPClient' )
			*--- tento la connessione
			This.Connect()
			*--- Se connesso trasmetto il comando
			If Thisform.lConnected
				This.SendData(tcCommand)
				nSec=SECONDS()
				Do While Thisform.oSock.State > 0
					DoEvents Force
					IF SECONDS()-nSec > IIF(TYPE("g_TIMEOUT_CLI")='N', g_TIMEOUT_CLI, 120)
						EXIT
						Thisform.lConnected=.f.
					ENDIF 
				Enddo
				Thisform.bOK= Thisform.lConnected
			Else
				Thisform.bOK= Thisform.lConnected
			ENDIF
		ELSE
		    * prima di creare il file controllo se esiste gi� e lo cancello
		    IF FILE(Thisform.cFileOrigin)
		       Erase(Thisform.cFileOrigin)
		    ENDIF
		    * creo il file di origine
		    Thisform.bOK = 0 <> STRTOFILE(tcCommand, Thisform.cFileOrigin)
		    IF Thisform.bOK AND FILE(Thisform.cFileOrigin)
		    * copio il file di origine nella destinazione finale. Questa gestione � stata pensata perch� sul client
		    * il programma Zucchetti Bridge in modalit� timer continua a cercare il file. Quindi si preferisce prima
		    * crearlo e poi copiarlo nella cartella definitiva.
				l_oError= ON("ERROR")
				ON ERROR Thisform.bOK = .F.
				COPY FILE (Thisform.cFileOrigin) To (Thisform.cFileCommand)
				ON ERROR &l_oError
			ELSE
			   	Thisform.bOK = .F.
			ENDIF
			IF NOT Thisform.bOK
			   	Ah_Msg("Il file di origine non esiste o non � possibile copiarlo nel client")
			Endif
		Endif
	Endproc

	*--- Procedure per la connessione
	Procedure Connect
		With Thisform.oSock
			If .State != 0
				*--- Se � gi� attiva una connessione la chiudo
				.Object.Close()
			Endif
			.protocol = 0  && TCP
			.remoteHost = Alltrim(Thisform.cRMHost)
			*--- RemoteHostname
			.remotePort = Thisform.nPort
			*--- RemotePort
			.localPort = 0
			*--- Eseguo la connessione
			.Object.Connect()
			nSec=SECONDS()
			Do While .Object.State < 7
				DoEvents Force
				*--- Ciclo finch� non sono nello stato connesso
				If .State = 9 OR SECONDS()-nSec > IIF(TYPE("g_TIMEOUT_CLI")='N', g_TIMEOUT_CLI, 120)
					Exit
				Endif
			Enddo
			If .Object.State = 7
				Thisform.lConnected = .T.
			Else
				*--- Connessione fallita
				Thisform.lConnected = .F.
			Endif
		Endwith
	Endproc

	Procedure Disconnect
		Thisform.oSock.Object.Close()
		Do While Thisform.oSock.State > 0
			DoEvents
		Enddo
		Thisform.lConnected = .F.
	ENDPROC
	
    *--- Invio messaggio cifrato
	Procedure SendData
		Lparameters cCommand
		With Thisform.oSock
			If .State = 7
				*--- Se non sono connesso non trasmetto
				.SendData(Cifracnf(Alltrim(cCommand),'C')+EOT)
			Else
				.Close()
				Thisform.lConnected = .F.
			Endif
		Endwith
	Endproc
Enddefine

*--- Client TCP
Define Class TCPClient As OleControl
	OleClass = "MSWinsock.Winsock"
	cReceiveBuffer = ''

	Procedure Close
		This.Object.Close()
	Endproc

	Procedure Destroy
		This.Object.Close()
	Endproc

	Procedure DataArrival
		Lparameters tnByteCount
		Local lcBuffer
		lcBuffer = Space(tnByteCount)
		*--- Ricevo i dati e li memorizzo in un buffer finch� non ricevo il carattere di EOT
		This.GetData( @lcBuffer, , tnByteCount )
		If At( EOT, lcBuffer ) = 0  && CHR(4) non trovato, continuo a ricevere
			This.cReceiveBuffer = This.cReceiveBuffer + lcBuffer
		Else
			This.cReceiveBuffer = This.cReceiveBuffer + Left( lcBuffer, At( EOT, lcBuffer ) -1 )
			*--- Ho ricevuto il pacchetto
			This.cReceiveBuffer = ''
		Endif
		Return
	Endproc
Enddefine
*---Zucchetti Bidge Client - Fine

*--- Zucchetti Aulla - Inizio Multireport
#define REP_NAME 1			&&Nome report stampa
#define REP_CURSOR 2		&&Nome Cursore stampa
#define REP_PARAMQRY 3		&&Oggetto Query Param
#define REP_ENVIRONMENT 4	&&Oggetto Ambiente
#define REP_NORESET 5		&&Flag NoReset numero pagina (solo grafiche?)
#define REP_MAIN 6			&&Flag report principale
#define REP_TEXT 7			&&Flag solo testo
#define REP_SPLIT 8			&&Espressione di rottura, utilizzata per spezzare il cursore principale in sottoinsiemi (fascicola stampe)
#define REP_LINK 9			&&Espressione di Link, lega la rottura (REP_SPLIT) del report principale con il report secondario		
#define REP_STMPORDER 10	&&Categoria 
#define REP_DETTORDER 11 	&&CPROWORD
#define REP_PRINTER 12 	    &&Stampante
#define REP_DESCRI 13		&&Descrizione Report
#define REP_ORDER 14		&&Colonna per l'ordinamento (REP_STMPORDER + REP_DETTORDER)
#define REP_SKIP 15   		&&Flag skip report	
#define REP_INDEX 16		&&Riferimento all'indice del multireport padre da cui si genera l'esplosione per la fascicolazione
#define REP_PARENT 17		&&Riferimento al padre
#define REP_CHILD 18		&&Riferimento al figlio
#define REP_KEY 19			&&Chiave
#define REP_NAMETRAD 20		&&Nome del report stampa tradotto
#define REP_CODISO 21		&&Codice ISO del report REP_NAME (origiale)
#define REP_CODISOTRAD 22	&&Codice ISO del report REP_NAMETRAD (tradotto)
#define REP_CODISOINST 23	&&Codice ISO dell'intestatario
#define REP_PDSTRPROC 24	&&pd_StrProcesso, stringa processo documentale
#define REP_SKIPANTE 25		&&Skip Anteprima (Document Management)
#define REP_EXTFILE 26		&&Estensione del file (necessaria per traduzione)
#define REP_NCOPY 27		&&Numero copie
#define REP_PRNSYS 28		&&Cambia flusso di stampa
#define REP_DOCM 29			&&Report DOCM (chkprocd)
#define REP_CODGDI 30		&&Codice gdi del report REP_NAME (origiale)
#define REP_DIMENSION  31   &&Numero campi array, deve essere aggiornato ogni volta che aggingo una nuova propriet� all'array
#define REP_CHGPRINTER '@Change_Printer@' &&Stringa di controllo cambio stampante
#define REP_CODISOREP 31	&&Codice ISO del report REP_NAMETRAD (tradotto)


*--- Class MultiReport
*--- Permette la gestione di report multipli
DEFINE CLASS MultiReport as custom
DIMENSION aReportList(1, REP_DIMENSION)  &&Lista dei report
cError = ''	&&Error Msg
cCurrentPrinter=''  			&&Stampa corrente
cModels=''						&&Indica la presenza di modelli (ES. E,W)
cRoot=''						&&Radice corrente
cLast=''						&&Figlio corrente
cReportBehavior="90"			&&REPORTBEHAVIOR
cCodISOSel = g_PROJECTLANGUAGE	&&Codice ISO selezionato
cSessionCode = ''				&&Session Code per evitare di tradurre pi� volte
bPDObbl = .f.					&&Processo obbligatorio
bPDLog = .f.					&&Log Processo
bPDAnte = .f.					&&Anteprima Processo
bPDProc = .f.					&&Processo da eseguire
nPDTotDoc = 0					&&DM TotDoc
nPDTotDocOk = 0					&&DM TotDocOk
nPDTotDocKo = 0					&&DM TotDocKo
bPDISOLan = .f.					&&DM ISOLanguage
cPDCodProc = ""					&&DM Codice processo se vuoto ho pi� processi (utilizzato nel log per valorizzare il campo codice processo se ho un solo processo)
bNumCopy = .t.					&&Indica se tutti i report hanno il solito numero di copie
nNumCopy = 1					&&Numero copie se tutti uguali	
bStampanti = .t.				&&Indica se tutti i report hanno settata la solita stampante
cStampante = ''					&&Stampante settata se uguale per tutti
cSelectedReport = ''			&&Stringa contenente gli indici dei report selezionati fra tutti quelli del multireport (la lista vuota corrisponde a tutti i report selezionati)
nPrnSysIndex = 0				&&Indice interno per il controllo CheckPrintSystemChanged
*--- Inizializzazione oggetto
PROCEDURE Init()
	this.SetCursor('', 1)
	this.SetReport('', 1)
	this.SetParamqry(.null., 1)
	this.SetNoReset(.f., 1)
	this.SetRepPrinter('', 1)
	this.SetCodISOTrad('', 1)
	this.SetCodISO('', 1)
	this.cError=''
ENDPROC 
*--- Distruttore
PROCEDURE Destroy()
	this.ResetReport()
ENDPROC 

PROCEDURE NewMain()
	this.cRoot=''			&&Radice corrente
	this.cLast=''			&&Figlio corrente
ENDPROC 

*--- Aggiunge un report alla lista, non esegue check errori
FUNCTION AddNewReport(cReportName, cRepDescription, cCursor, oParamqry, oVarEnv, bNoReset, bMain, bText,;
					   cSplit, cLink, cStmpOrder, nDettOrder, bChgPrinter, nIndex, bNoTrad, bPrnSys)
	LOCAL l_NumRow, l_NumCol, l_result, l_keyReport, l_nLast, cRepLanguage, cRepgdi
	*--- Check error
	l_result=0
	l_keyReport=''
	IF this.IsEmpty()
		nIndex=1
	ELSE 
		l_NumRow=this.GetNumReport()
		l_NumCol=REP_DIMENSION
		nIndex=IIF(TYPE('nIndex')='L' OR nIndex>l_NumRow OR nIndex<=0, l_NumRow+1, nIndex)
		DIMENSION this.aReportList(l_NumRow+1, l_NumCol)
		AINS(this.aReportList, nIndex)
	ENDIF 	
	this.SetReport(NVL(cReportName,''), nIndex)
	*--- Leggo codice ISO del report
	bNoTrad = IIF(bText, .f., bNoTrad)
	IF (VARTYPE(g_DISABLEREPORTTRANSLATION)='L' AND g_DISABLEREPORTTRANSLATION) OR bNoTrad
		cRepLanguage=g_PROJECTLANGUAGE
	ELSE	
  		cRepLanguage=cp_ReportLanguage(NVL(cReportName,''))
	ENDIF
	cRepgdi=cp_Reportgdi(NVL(cReportName,''))
	this.SetReportTrad("", nIndex)
	this.SetCodISO(cRepLanguage, nIndex)
	this.SetCodgdi(cRepgdi, nIndex)	
	this.SetCodISOTrad("", nIndex)
	this.SetCodISOInst("", nIndex)	
	this.SetCursor(NVL(cCursor,''), nIndex)
	this.SetParamqry(IIF(TYPE('oParamqry')='L', .null., oParamqry), nIndex)
	this.SetVarEnv(IIF(TYPE('oVarEnv')='L', .null., oVarEnv), nIndex)
	this.SetNoReset(bNoReset, nIndex)
	this.SetMain(bMain, nIndex)
	this.SetText(bText, nIndex)
	this.SetSplit(IIF(TYPE('cSplit')='L', '', NVL(cSplit,'')), nIndex)
	this.SetLink(IIF(TYPE('cLink')='L', '', NVL(cLink,'')), nIndex)
	cStmpOrder=IIF(TYPE('cStmpOrder')='L', '', NVL(cStmpOrder,''))
	this.SetStmpOrder(cStmpOrder, nIndex)
	nDettOrder=IIF(TYPE('nDettOrder')='L', '', NVL(nDettOrder,''))
	this.SetDettOrder(nDettOrder, nIndex)
	this.SetDescription(NVL(cRepDescription,''), nIndex)
	this.SetOrder(ALLTRIM(cStmpOrder)+'_'+ALLTRIM(PADL(TRANSFORM(nDettOrder),5,"0")), nIndex)
	this.SetRepPrinter(IIF(bChgPrinter, REP_CHGPRINTER, ''), nIndex)
	this.SetSkip(.F., nIndex)
	this.SetSkipAnte(.F., nIndex)	
	this.SetStrProc("", nIndex)
	this.SetNumCopy(1, nIndex)
	this.SetPrnSys(bPrnSys, nIndex)
	l_keyReport=SYS(2015)
	this.SetKeyReport(l_keyReport, nIndex)
	*--- Setto main corrente
	IF bMain
		this.SetChild(this.cRoot, nIndex)
		this.SetParent('', nIndex)
		this.cRoot=l_keyReport
		this.cLast=IIF(EMPTY(this.cLast),l_keyReport, this.cLast)
	ELSE  
	*--- Creo il collegamento tra padre e figlio (report principale e secondario)
		this.SetParent(this.cLast, nIndex)
		l_nLast=this.GetIndexByKey(this.cLast)
		IF l_nLast>0
			this.SetChild(l_keyReport, l_nLast)
		ENDIF 
		this.cRoot=IIF(EMPTY(this.cRoot),l_keyReport, this.cRoot)
		this.cLast=l_keyReport
	ENDIF 
	RETURN l_result
ENDFUNC
 
*--- Aggiunge un report creando il cursore da VQR e Batch, esegue check errori
FUNCTION AddReport(oParent, cReportName, cRepDescription, cVQR, cBatch, bNoReset, bMain, bText,;
					cSplit, cLink, cStmpOrder, nDettOrder, bChgPrinter, nCarMemo, nIndex, cISOField, bPrnSys)
	LOCAL l_CurName, l_FileName, l_Ext, l_result, l_pospar
	*--- Check error
	l_result=0
	*--- Verifico esistenza report
	l_FileName=ADDBS(JUSTPATH(cReportName))+JUSTSTEM(cReportName)
	l_Ext=JUSTEXT(cReportName)
	IF EMPTY(l_Ext)
		l_Ext=IIF(bText, "FXP", "FRX")
	ENDIF 
	IF EMPTY(cReportName)
		l_result=l_result-1
		this.cError = this.cError + cp_MsgFormat(MSG_REPORT_UNDEFINED__,CHR(13))
	ELSE 
		IF !cp_IsStdFile(l_FileName, l_Ext)
			l_result=l_result-2
			this.cError = this.cError + cp_MsgFormat(MSG_REPORT__INEXISTENT__, ALLTRIM(cReportName),CHR(13))
		ENDIF
	ENDIF  
	l_CurName=SYS(2015)
	*-- Creo cursore
	PUBLIC g_MultiReportParamqry
	LOCAL  l_MultiReportVarEnv
	g_MultiReportParamqry=.null.
	l_MultiReportVarEnv=.null.
	*--- g_MultiReportParamqry viene valorizzato all'interno di vq_exec
	*--- Verifico esistenza query
	IF EMPTY(NVL(cVqr,''))
		l_result=l_result-4
		this.cError = this.cError + cp_MsgFormat(MSG_QUERY_UNDEFINED__, CHR(13))
	ELSE 
		IF !cp_IsPFile(cVqr)
			l_result=l_result-8
			this.cError = this.cError + cp_MsgFormat(MSG_QUERY__INEXISTENT__, ALLTRIM(cVqr), CHR(13))
		ELSE 
			vq_exec(cVQR, IIF(TYPE('oParent')='L', .null., oParent), l_CurName)	
		ENDIF
	ENDIF 
	*--- Se ho specificato un batch lancio l'elaborazione
	IF TYPE('cBatch')='C' AND NOT EMPTY(NVL(cBatch,''))
	*--- Verifico esistenza batch
		l_pospar=AT('(', cBatch)
		IF l_pospar<>0
			l_FileName=FORCEEXT(ALLTRIM(LEFT(cBatch, l_pospar-1)), "FXP")
		ELSE 
			l_FileName=FORCEEXT(ALLTRIM(cBatch), "FXP")
		ENDIF 
		IF !cp_IsPFile(l_FileName)
			l_result=l_result-16
			this.cError = this.cError + cp_MsgFormat(MSG_PROGRAM__INEXISTENT__, ALLTRIM(cBatch), CHR(13))
		ELSE 
		*--- l_MultiReportVarEnv viene valorizzato all'interno del batch
			LOCAL l_batchRet, l_macro, l_batch
			l_MultiReportVarEnv = CREATEOBJECT("ReportEnvironment")
			l_batch=JUSTSTEM(cBatch)  &&Elimino eventuali path
			l_pospar=AT('(', l_batch)
			IF l_pospar<>0	&&Ho inserito un parametro
				l_macro=LEFT(l_batch,l_pospar)+"IIF(TYPE('oParent')='L', .null., oParent), l_CurName, l_MultiReportVarEnv, "+SUBSTR(l_batch, l_pospar+1)
			ELSE
				l_macro=l_batch+"(IIF(TYPE('oParent')='L', .null., oParent), l_CurName, l_MultiReportVarEnv)"
			ENDIF 
			LOCAL l_oldarea
			l_oldarea=SELECT()
		  	l_batchRet=&l_macro
		  	SELECT(l_oldarea)
		  	*--- Verifico errore da batch
		  	IF NOT EMPTY(l_batchRet)
		  		l_result=l_result-32
		  		this.cError = this.cError + l_batchRet + CHR(13)+CHR(10)
		  	ENDIF
	  	ENDIF  
	ENDIF
	l_result = l_result + this.AddNewReport(cReportName, cRepDescription, l_CurName, g_MultiReportParamqry, l_MultiReportVarEnv, bNoReset, bMain, bText,;
					  						cSplit, cLink, cStmpOrder, nDettOrder, bChgPrinter, nIndex, .f., bPrnSys)
    IF l_result = 0 AND TYPE("cISOField") = "C"
    	LOCAL cMacro
    	cMacro = l_CurName+"."+ALLTRIM(cISOField)
    	IF TYPE(cMacro)<>"U"
    	    IF USED(l_CurName)
    	        SELECT(l_CurName)
    	        GO TOP
    	    ENDIF
	       	g_DEMANDEDLANGUAGE =  &cMacro
    ENDIF
        *--- Se la linguadi stampa non � assegnata utilizza la lingua del progetto
    	g_DEMANDEDLANGUAGE = IIF(EMPTY(g_DEMANDEDLANGUAGE), g_PROJECTLANGUAGE, g_DEMANDEDLANGUAGE)
    ENDIF 
    this.SetCodISOrep (g_DEMANDEDLANGUAGE,this.getnumreport()) 
	*--- Rilascio le variabili pubbliche
	RELEASE g_MultiReportParamqry
	RETURN l_result
ENDFUNC 

*--- Aggiunge un report alla lista dei selezionati
*--- Restituisce l'indice del report inserito nella lista
*--- (-1) altrimenti
FUNCTION CheckReport(nIndex, bNoClear)
	LOCAL l_numReport, l_checkIndex
	l_numReport = this.getNumReport()
	l_checkIndex = PADL(ALLTRIM(STR(nIndex)),5,'0')+'-'
	IF nIndex>0 AND nIndex<=l_numReport AND NOT l_checkIndex$this.cSelectedReport
		*--- Aggiorno i valori bStampanti e cStampante in base al report appena selezionato
		DO CASE
		CASE EMPTY(this.cSelectedReport) && primo report selezionato
			this.bStampanti = .T.
			this.cStampante = this.getRepPrinter(nIndex)
		CASE NOT EMPTY(this.cSelectedReport) AND this.bStampanti AND this.getRepPrinter(nIndex)<>this.cStampante
			this.bStampanti = .F.
			this.cStampante = ''
		ENDCASE
		*--- Inserisco il report
		this.cSelectedReport = this.cSelectedReport + l_checkIndex
		*--- Se sono stati tutti selezionati (e il flag "bNoClear" � disattivo) sbianco la lista
		IF OCCURS('-', this.cSelectedReport)=l_numReport AND NOT bNoClear
			this.cSelectedReport = ''
		ENDIF
		RETURN nIndex
	ELSE
		RETURN -1
	ENDIF
ENDFUNC

*--- Rimuove un report dalla lista
PROCEDURE RemoveReport(nIndex)
	LOCAL l_NumRow, l_NumCol, l_cCurName, l_cParent, l_nParent
	*--- Sistemo la lista interna
	l_cParent=this.GetParent(nIndex)
	l_cChild=this.GetChild(nIndex)
	l_nParent=this.GetIndexByKey(l_cParent)
	l_nChild=this.GetIndexByKey(l_cChild)
	IF l_nParent>0
		this.SetChild(l_cChild, l_nParent)
	ENDIF
	IF l_nChild>0
		this.SetParent(l_cParent, l_nChild)
	ENDIF

	l_NumRow=this.GetNumReport()
	l_NumCol=REP_DIMENSION
	*--- Chiudo i cursori
	l_cCurName=this.GetCursorName(nIndex)
	*--- GarbageCollect
	g_GarbageCollect.RemoveCursor(l_cCurName)
	this.SetCursor('', nIndex)
	*--- Distruggo gli oggetti Paramqry
	this.SetParamqry(.null., nIndex)
	*--- Distruggo gli oggetti VarEnv
	this.SetVarEnv(.null., nIndex)	
	*--- Rimuovo l'elemento dall'array
	ADEL(this.aReportList, nIndex)
	IF l_NumRow>1
		DIMENSION this.aReportList(l_NumRow-1, l_NumCol)
	ELSE 
		this.SetReport('', 1)
		this.AddNewReport('', '', '')
		this.cError=''
	ENDIF
ENDPROC 

*--- Rimuove un report dalla lista dei selezionati
*--- Restituisce l'indice del report cancellato dalla lista
*--- (-1) altrimenti
FUNCTION UncheckReport(nIndex)
	LOCAL l_numReport, l_checkIndex, l_i
	l_numReport = this.getNumReport()
	l_checkIndex = PADL(ALLTRIM(STR(nIndex)),5,'0')+'-'
	IF nIndex>0 AND nIndex<=l_numReport AND (EMPTY(this.cSelectedReport) OR l_checkIndex$this.cSelectedReport)
		*--- Se la lista � vuota devo prima popolarla con tutti i report per poter poi deselezionarne uno
		IF EMPTY(this.cSelectedReport)
			FOR l_i=1 TO l_numReport
				this.CheckReport(l_i, .T.) && bNoClear a true per evitare che la lista si sbianchi
			ENDFOR
		ENDIF
		this.cSelectedReport = STRTRAN(this.cSelectedReport, l_checkIndex, '')
		*--- Aggiorno i valori bStampanti e cStampante in base al report appena deselezionato
		DO CASE
		CASE EMPTY(this.cSelectedReport) && nessun report selezionato
			this.bStampanti = .F.
			this.cStampante = ''
		CASE NOT EMPTY(this.cSelectedReport) AND NOT this.bStampanti
			*--- Controllo se i report rimasti hanno la stessa stampante settata
			this.bStampanti = .T.
			this.cStampante = ''
			l_i = 1
			DO WHILE l_i<=l_numReport AND this.bStampanti
				IF this.IsCheckReport(l_i)
					DO CASE
					CASE EMPTY(this.cStampante)
						this.cStampante = this.getRepPrinter(l_i)
					CASE NOT EMPTY(this.cStampante) AND this.cStampante<>this.getRepPrinter(l_i)
						this.bStampanti = .F.
						this.cStampante = ''	
					ENDCASE
				ENDIF
				l_i = l_i + 1
			ENDDO
		ENDCASE
		RETURN nIndex
	ELSE
		RETURN -1
	ENDIF
ENDFUNC

*--- Controlla se un report � selezionato (presente nella lista dei selezionati)
*--- (la lista vuota corrisponde a tutti i report selezionati)
*--- Restituisce TRUE se il report (dato l'indice) � presente nella lista
*--- FALSE altrimenti
FUNCTION IsCheckReport(nIndex)
	LOCAL l_numReport, l_checkIndex
	l_numReport = this.getNumReport()
	l_checkIndex = PADL(ALLTRIM(STR(nIndex)),5,'0')+'-'
	IF nIndex>0 AND nIndex<=l_numReport AND (EMPTY(this.cSelectedReport) OR l_checkIndex$this.cSelectedReport)
		RETURN .T.
	ELSE
		RETURN .F.
	ENDIF
ENDFUNC

*--- Sbianca la lista dei report selezionati
*--- (la lista vuota corrisponde a tutti i report selezionati)
PROCEDURE ClearCheck()
	this.cSelectedReport = ''
	this.IsOnePrinter()
ENDPROC

*--- Chiude e distrugge oggetto, chiude i cursori e distrugge gli oggetti Paramqry e VarEnv
PROCEDURE ResetReport()
	LOCAL l_Count
	LOCAL l_NumRow, l_NumCol, l_cCurName
	l_NumRow=this.GetNumReport()
	l_NumCol=REP_DIMENSION
	FOR l_Count=1 TO l_NumRow
	  *--- Chiudo i cursori
	  l_cCurName=this.GetCursorName(l_Count)
	  *--- GarbageCollect
	  IF TYPE("g_GarbageCollect") = 'O'  AND Not ISNULL(g_GarbageCollect)
		  g_GarbageCollect.RemoveCursor(l_cCurName)
	  ENDIF 
	  this.SetCursor('', l_Count)
	  *--- Distruggo gli oggetti Paramqry
	  this.SetParamqry(.null., l_Count)
	  *--- Distruggo gli oggetti VarEnv
	  this.SetVarEnv(.null., l_Count)	
	NEXT 
	DIMENSION this.aReportList(1, l_NumCol)
	this.SetReport('', 1)
	this.AddNewReport('', '', '')
	this.cError=''
	this.cRoot=''			&&Radice corrente
	this.cLast=''			&&Figlio corrente
ENDPROC 
*--- Copy report
FUNCTION CopyReport(oSourceMReport, nCopy, nIndex, bNoSetCursor)
	LOCAL l_Count
	LOCAL l_NumRow, l_NumCol
	l_NumCol=REP_DIMENSION
	IF this.IsEmpty()
		nIndex=1
	ELSE 
		l_NumRow=this.GetNumReport()
		nIndex=IIF(TYPE('nIndex')='L' OR nIndex>l_NumRow OR nIndex<=0, l_NumRow+1, nIndex)
		DIMENSION this.aReportList(l_NumRow+1, l_NumCol)
		AINS(this.aReportList, nIndex)
	ENDIF 
	FOR l_Count=1 TO l_NumCol
		this.aReportList[nIndex, l_Count]=oSourceMReport.aReportList[nCopy, l_Count]
	NEXT
	*--- Nel caso in cui un report non sia stato copiato (cursore di stampa vuoto) aggiorno i riferimenti
	*--- sul report precedente e su quello successivo per mantenere la lista linkata
	LOCAL l_parentIndex
	l_parentIndex = this.getIndexByKey(this.getParent(nIndex))
	DO WHILE not EMPTY(this.getParent(nIndex)) AND (l_parentIndex=-1 OR this.getKeyReport(l_parentIndex)<>this.getKeyReport(nIndex-1))
		LOCAL l_sourceIndex
		l_sourceIndex = oSourceMReport.getIndexByKey(this.getParent(nIndex))
		IF NOT oSourceMReport.getMain(l_sourceIndex) AND nIndex>1
			this.setParent(oSourceMReport.getParent(l_sourceIndex),nIndex)
			this.setChild(this.getKeyReport(nIndex),nIndex-1)
		ELSE
			this.setParent("",nIndex)
		ENDIF
		l_parentIndex = this.getIndexByKey(this.getParent(nIndex))
	ENDDO
	*--- GarbageCollect - Devo usare SetCursor per aggiungere il riferiemnto al cursore
	IF !bNoSetCursor
		this.SetCursor(oSourceMReport.GetCursorName(nCopy), nIndex)
	ENDIF 
	
	RETURN nIndex
*--- Copy translate report, (oSourceMReport = SplitReport, this oNumReport)
FUNCTION CopyTranslateReport(oSourceMReport)
	LOCAL l_Count
	LOCAL l_NumRow, l_NumCol, l_nIndex 
	l_NumCol = oSourceMReport.GetNumReport()
	FOR l_Count=1 TO l_NumCol
		l_nIndex = oSourceMReport.GetIndex(l_Count)
		this.SetReportTrad(oSourceMReport.GetReportTrad(l_Count), l_nIndex)
		this.SetCodISOTrad(oSourceMReport.GetCodISOTrad(l_Count), l_nIndex)
	NEXT 	
	*--- Lingua corrente
	this.cCodISOSel = oSourceMReport.cCodISOSel
	this.cSessionCode = oSourceMReport.cSessionCode 
*--- Test First report empty
FUNCTION IsReport()
	RETURN 'FRX'$ this.aReportList[1, REP_EXTFILE]
ENDFUNC

*--- Test First report empty
FUNCTION IsEmpty()
	RETURN EMPTY(this.aReportList[1, REP_NAME])
ENDFUNC 
*--- Ritorno True se tutti i report principali sono vuoti
FUNCTION IsEmptyCursor()
	LOCAL l_Count, l_NumRow, l_cCurName, l_cListCat, l_nNumCat, l_Index, l_bRemove, l_result, l_NumRec
	l_result=.T.
	IF !this.IsEmpty()
		l_cListCat=this.GetStmpOrderList()
		l_nNumCat=ALINES(l_arrListCat, l_cListCat, ",")
		FOR l_i=1 TO l_nNumCat
			l_Index=0
			DO WHILE .T.
				l_Index=this.GetMainReport(l_arrListCat[l_i], l_Index)
				IF l_Index<>-1	&&Testo se il cursore � pieno
					l_cCurName = this.GetCursorName(l_Index)
					IF USED(l_cCurName)
						SELECT(l_cCurName)
						COUNT FOR NOT DELETED() TO l_NumRec
						IF l_NumRec<1
			    	  		l_bRemove=.T.
			    	  	ELSE 
			    	  		l_result=.F.
						ENDIF
					ENDIF 
				ELSE
					EXIT 
				ENDIF 
			ENDDO
		NEXT 
	ENDIF 
	RETURN l_result
ENDFUNC 
*--- Upper di tutti i  report 
PROCEDURE UpperReport()
	LOCAL l_Count, l_NumRow
	l_NumRow=this.GetNumReport()
	FOR l_Count=1 TO l_NumRow
		this.SetReport(UPPER(ALLTRIM(this.GetReport(l_Count))), l_Count)
	NEXT
ENDPROC 
*--- Forza estensione se diversa da '.FRX','.FXP','.XLT','.DOC'
PROCEDURE ForceReportExt(cExt)
	LOCAL l_Count, l_NumRow, l_cExt
	l_NumRow=this.GetNumReport()
	FOR l_Count=1 TO l_NumRow
		l_ext=JUSTEXT(this.GetReport(l_Count))
		l_cExt = l_ext
	    IF NOT (l_ext='FRX' OR l_ext='FXP' OR l_ext='XLT' OR l_ext='XLTX' OR l_ext='DOC' OR l_ext='DOCX' OR l_ext='STC' OR l_ext='OTS')
	    	l_cExt=IIF(TYPE('cExt')<>'C', IIF(this.GetText(l_Count), "FXP", "FRX"), cExt)
	    	this.SetReport(FORCEEXT(this.GetReport(l_Count), l_cExt), l_Count)
		ENDIF 	
		this.SetExtReport(l_cExt, l_Count)
	NEXT
ENDPROC 
*--- Rimuove estensione
PROCEDURE RemoveReportExt(cExt)
	LOCAL l_Count, l_NumRow, l_FileName
	l_NumRow=this.GetNumReport()
	FOR l_Count=1 TO l_NumRow
		*this.SetExtReport(JUSTEXT(this.GetReport(l_Count)), l_Count)
		l_FileName=ADDBS(JUSTPATH(this.GetReport(l_Count)))+JUSTSTEM(this.GetReport(l_Count))
		this.SetReport(l_FileName, l_Count)
	NEXT
ENDPROC 
*--- Controlla se esistono report nella cartella custom
PROCEDURE CheckCustomReport()
	LOCAL l_Count, l_NumRow, l_CustFile, l_ext, l_FileName ,cRepgdi
	l_NumRow=this.GetNumReport()
	FOR l_Count=1 TO l_NumRow
	*--- Memorizzo il report con il percorso di default, per ovviare errori in fase di ricerca del processo documentale...
		this.SetReportDOCM(this.GetReport(l_Count), l_Count)

	IF G_application="ADHOC REVOLUTION"
	        *--- Cerco nella custom l'esistenza di un report personalizzato
		l_CustFile='CUSTOM\'+JUSTFNAME(this.GetReport(l_Count))
		IF cp_fileexist(l_CustFile)
		       this.SetReport(l_CustFile, l_Count)
		ENDIF 
	*--- Cerco eventuale _1 _G1, ecc...
	ENDIF
	
		l_ext=JUSTEXT(this.GetReport(l_Count))
		l_FileName=ADDBS(JUSTPATH(this.GetReport(m.l_Count)))+JUSTSTEM(this.GetReport(m.l_Count))
		l_CustFile=cp_GetStdFile(cp_FindNamedDefaultFile(l_FileName,l_ext),l_ext)
		IF NOT EMPTY(l_CustFile)
			this.SetReport(l_CustFile, l_Count)
		ENDIF
		*--- Aggiorno codice ISO del report
		LOCAL cRepLanguage
		IF (VARTYPE(g_DISABLEREPORTTRANSLATION)='L' AND g_DISABLEREPORTTRANSLATION)
			cRepLanguage=g_PROJECTLANGUAGE
		ELSE	
	  		cRepLanguage=cp_ReportLanguage(NVL(this.GetReport(m.l_Count),''))
		ENDIF
	  	cRepgdi=cp_Reportgdi(NVL(this.GetReport(m.l_Count),''))
		this.SetCodISO(m.cRepLanguage, m.l_Count)
		this.SetCodgdi(m.cRepgdi, m.l_Count)
	NEXT 
ENDPROC 
*--- Settaggio stampanti
PROCEDURE SetAllPrinter()
	local i_nCurs, i_oa, i_workstation, l_i, l_NumRow, l_device, tempStamp, l_nCopy, l_nCopyOld
    i_oa = select()
    i_nCurs = sys(2015)
    i_workstation=LEFT(SYS(0), RAT(' # ',SYS(0))-1)
    l_NumRow=this.GetNumReport()
	l_nCopyOld = this.GetNumCopy(1)
    FOR l_i=1 TO l_NumRow
    	IF this.IsCheckReport(l_i)
            l_device=''
	    	l_bText=this.GetText(l_i)
	    	l_cReportName=this.GetReport(l_i)
	    	l_cReport=IIF(l_bText, FORCEEXT(l_cReportName, '.FXP'), FORCEEXT(l_cReportName, '.FRX'))
		    if i_ServerConn[1,2]<>0
		      cp_sql(i_ServerConn[1,2],"select * from cpusrrep where repasso="+cp_ToStr(l_cReport)+" and (aziasso="+cp_ToStr(i_codazi)+" or aziasso='          ' or aziasso is null) and (wstasso="+cp_ToStr(i_workstation)+" or wstasso='          ' or wstasso is null) and (usrasso="+cp_ToStr(i_codute)+" or usrasso=0 or usrasso is null) order by wstasso desc,aziasso desc, usrasso desc",i_nCurs)
		    else
		      select * from cpusrrep where repasso=l_cReport AND (TRIM(i_codazi)==TRIM(aziasso) OR EMPTY(aziasso)) AND (TRIM(wstasso)==TRIM(i_workstation) OR EMPTY(wstasso)) AND (usrasso=i_codute or usrasso=0) ORDER BY wstasso desc,aziasso desc, usrasso desc into curs (i_nCurs)
		    endif
		    if used(i_nCurs)
		      *--- Ordino per utente e azienda in modo da prendere sempre prima le associazioni con utente e/o azienda valorizzate 
			  *--- indipendentemente dall'ordine di caricamento; ordino il cursore fox per evitare problemi con DB2/Oracle e il verso asc o desc
	      	  SELECT * FROM (i_nCurs) ORDER BY usrasso DESC, aziasso DESC INTO CURSOR (i_nCurs)
		      l_nCopy=this.GetNumCopy(l_i)
		      if not(eof())
		        go top
		        l_device=trim(DEVASSO)
		        l_nCopy=NVL(COPASSO, 1)
		        
		        *--- Di default � 90, appena ne trovo uno a 80 uso sempre quest'ultimo
		        this.cReportBehavior=IIF(this.cReportBehavior="80", this.cReportBehavior, TRIM(NVL(BEHASSO,"90")))
		        this.SetRepPrinter(l_device, l_i)
		        this.SetNumCopy(l_nCopy, l_i)
		      ELSE 
		        this.cReportBehavior=IIF (UPPER (ALLTRIM(NVL(this.GetCodgdi(l_i) ,' ')))='BNOGDI','80','90')
		      endif
		      USE
		      l_device=Alltrim(l_device)
		      IF EMPTY(l_device)
		      	l_device=SET("PRINTER", IIF(TYPE("g_VfpDefPrinter")<>'U' and g_VfpDefPrinter, 3, 2))
		      	this.SetRepPrinter(l_device, l_i)
		      ENDIF 
		      * Zucchetti Aulla Inizio : Ricerca stampante windows corretta su terminal Server
		      If Right(l_device, Len(g_PRTJOLLY)) = g_PRTJOLLY
		         tempStamp=printerChecked(Left(l_device, Len(l_device)-Len(g_PRTJOLLY)))
		         IF NOT EMPTY(tempStamp)
		            this.SetRepPrinter(tempStamp, l_i)
		         Endif
		      Endif
		      * Zucchetti Aulla Fine : Ricerca stampante windows corretta 
	          *--- Controllo se tutti i report hanno il solito numero di copie
		      IF l_nCopy<>l_nCopyOld AND l_i>1
		      	this.bNumCopy = .f.
		      ENDIF 
		      l_nCopyOld = l_nCopy
		    else
		      cp_msg(cp_Translate(MSG_ERROR_OPENING_PRINTING_TABLE))
		      EXIT &&Mi fermo al primo errore
		    ENDIF
	    ENDIF
    NEXT 
    *--- Setto REPORTBEHAVIOR
    SET REPORTBEHAVIOR VAL((this.cReportBehavior))
    *--- Se tutti i numero copia sono uguali setto il numero copia totale
    IF this.bNumCopy
    	this.nNumCopy = l_nCopy
    ENDIF 
    select (i_oa)
ENDPROC 

*--- True se � settata la stessa stampante per tutti i report selezionati (valorizza bStampanti e cStampante)
PROCEDURE IsOnePrinter()
	LOCAL l_Count, l_NumRow
	l_Count = 1
	l_NumRow=this.GetNumReport()
	this.bStampanti = .t.
	this.cStampante = ""
	DO WHILE l_Count<=l_NumRow AND this.bStampanti
		IF this.IsCheckReport(l_Count)
			DO CASE
			CASE EMPTY(this.cStampante)
				this.cStampante = this.getRepPrinter(l_Count)
			CASE NOT EMPTY(this.cStampante) AND this.cStampante<>this.getRepPrinter(l_Count)
					this.bStampanti = .f.
			ENDCASE
		ENDIF
		l_Count = l_Count+1
	ENDDO
	IF NOT this.bStampanti
		this.cStampante = ""
	ENDIF
	RETURN this.bStampanti
ENDPROC 

*--- Traduce report nel codice ISO cCodISO, se non specificato traduce per codice ISO intestatario
PROCEDURE TranslateReport(cCodISO, cSessionCode)
	LOCAL l_Count, l_NumRow, l_RepTrad, l_bIntestatario, l_langRep
	cSessionCode = IIF(TYPE("cSessionCode")<>"C", SYS(2015), cSessionCode)
	This.cSessionCode = cSessionCode
	*--- Setto lingua corrente
	this.cCodISOSel=cCodISO
	l_NumRow=this.GetNumReport()
	l_bIntestatario=.f.
	IF cCodISO = "@@@"
		*--- Traduco per intestatario
		l_bIntestatario=.t.
	ENDIF 
	LOCAL bMsg
	bMsg = .t.
	FOR l_Count=1 TO l_NumRow
		IF l_bIntestatario
		*--- Leggo il codice ISO dell'intestatario
			cCodISO = this.GetCodISOInst(l_Count)
			IF EMPTY(cCodISO)
				*--- Non ho un codice ISO associato alla lingua del destinatario, do il msg solo una volta
				IF bMsg
					Ah_ErrorMsg("Per la lingua dell'intestario non � stato associato un codice ISO%0Il report non verr� tradotto")
				ENDIF
				bMsg = .f.				
				LOOP
			ENDIF 
		ENDIF 
		*--- Solo per stampe grafiche
		LOCAL l_macro
		l_langRep=this.GetCodISO(l_Count)
		IF l_langRep != cCodISO AND l_langRep != "NOTRANSLATE" AND this.GetCodISOTrad(l_Count) != cCodISO AND !this.GetText(l_Count)
			DO CASE 
				CASE  (UPPER(this.GetExtReport(l_Count)) == "XLT" OR  UPPER(this.GetExtReport(l_Count)) == "XLTX" ) AND g_OFFICE='M'
				*--- Traduco Excel
					l_macro = "l_RepTrad = cp_TranslateExcel(this.GetReportOrig(l_Count), this.GetCodISO(l_Count), cCodISO, cSessionCode)"
					&l_macro
				CASE UPPER(this.GetExtReport(l_Count)) == "STC" AND g_OFFICE='O'
				*--- Traduco Calc
					l_macro = "l_RepTrad = cp_TranslateCalc(this.GetReportOrig(l_Count), this.GetCodISO(l_Count), cCodISO, cSessionCode)"
					&l_macro
				CASE UPPER(this.GetExtReport(l_Count)) == "FRX"
				*--- Traduco il report
			    	 l_macro = "l_RepTrad = cp_TranslateReport(this.GetReportOrig(l_Count), this.GetCodISO(l_Count), cCodISO, cSessionCode)"
				 	&l_macro	
				 OTHERWISE
				 IF g_OFFICE<>'O'
				 	ah_errormsg(MSG_TRANSLATING_SYSTEM_DEACTIVATED)	
				 Endif		
			ENDCASE 
			IF NOT EMPTY(l_RepTrad)
				this.SetReportTrad(l_RepTrad, l_Count)
				this.SetCodISOTrad(cCodISO, l_Count)
			ENDIF 
		ENDIF 
	NEXT 
ENDPROC 

*--- Verifica esistenza di tutti i report
PROCEDURE IsStdFile(cExt, cType)
	LOCAL l_Count, l_NumRow, l_result
	l_result=.F.
	cType = IIF(TYPE("cType")='L', 'A', cType)
	l_NumRow=this.GetNumReport()
	FOR l_Count=1 TO l_NumRow
		IF (!this.GetText(l_Count) AND cType='G') OR (this.GetText(l_Count) AND cType='T') OR cType='A'
			IF !cp_IsStdFile(this.GetReport(l_Count), cExt)
				RETURN .F.
			ELSE 
				l_result=.T.
			ENDIF 
		ENDIF
	NEXT
	RETURN l_result
ENDPROC 
*--- Check XLT, DOC, SXW
FUNCTION CheckModels()
	LOCAL l_Count, l_NumRow, l_result
	this.cModels=''
	l_NumRow=this.GetNumReport()
	IF !this.IsEmptyCursor() &&Se i cursori sono vuoti non ho niente da stampare quindi disabilito i bottoni
		FOR l_Count=1 TO l_NumRow
			l_FileName=this.GetReportOrig(l_Count)
			IF !('E'$this.cModels) AND (cp_IsStdFile(l_FileName,'XLT') OR cp_IsStdFile(l_FileName,'XLTX') OR cp_IsStdFile(l_FileName,'XLTM')) and g_OFFICE='M'
				this.cModels=this.cModels+'E'
			ENDIF 
			IF !('W'$this.cModels) AND ((cp_IsStdFile(l_FileName,'DOC') OR cp_IsStdFile(l_FileName,'DOCX'))and g_OFFICE='M') OR (cp_IsStdFile(l_FileName,'SXW') and g_OFFICE='O')
				this.cModels=this.cModels+'W'
			ENDIF 
			IF !('G'$this.cModels)
				IF cp_IsStdFile(l_FileName,'VFC') 
					this.cModels=this.cModels+'F'
				ELSE
					IF cp_IsStdFile(l_FileName,'VGR')
						this.cModels=this.cModels+'G'
					endif
				ENDIF
			ENDIF
		NEXT
	ENDIF  
ENDFUNC 
*--- Check word
FUNCTION CheckWord()
	RETURN 'W'$this.cModels
*--- Check excel
FUNCTION CheckExcel()
	RETURN 'E'$this.cModels
*--- Check graph
FUNCTION CheckGraph()
	RETURN 'G'$this.cModels Or 'F'$this.cModels
FUNCTION CheckGraphVer()
	IF this.CheckGraph()
		IF 'G'$this.cModels
			RETURN "G"
		ELSE
			RETURN "F"
		ENDIF
	ELSE
		RETURN "N"
	ENDIF
*--- Ordina per il campo DETTORDER
PROCEDURE OrderByDett()
	ASORT(this.aReportList, REP_DETTORDER)
ENDPROC 
*--- Ordina per il campo STMPORDER
PROCEDURE OrderByStmp()
	ASORT(this.aReportList, REP_STMPORDER)
ENDPROC 
*--- Ordina per STMPORDER e DETTORDER
PROCEDURE OrderReport()
	ASORT(this.aReportList, REP_ORDER)
ENDPROC 

*--- Rimuove tutti i report con StmpOrder specificato, ritorna il numero di report eliminati
FUNCTION RemoveStmpOrder(cStmpOrder)
	LOCAL l_Count, l_NumRow, l_nNumRemove
	l_nNumRemove=0
	l_NumRow=this.GetNumReport()
	FOR l_Count=l_NumRow TO 1	&&Ciclo al rovescio perch� elimino degli elementi
		IF this.GetStmpOrder(l_Count)==cStmpOrder
			this.RemoveReport(l_Count)
			l_nNumRemove=l_nNumRemove+1
		ENDIF 
	NEXT	
	RETURN l_nNumRemove
*--- Ritorna una lista separata da vigole di tutte le categorie presenti
FUNCTION GetStmpOrderList()
	LOCAL l_Count, l_NumRow, l_result,l_stmporder
	l_NumRow=this.GetNumReport()
	l_result=''
	FOR l_Count=1 TO l_NumRow
		l_stmporder=this.GetStmpOrder(l_Count)+CHR(250)
		IF !(l_stmporder$l_result)
			l_result=l_result + l_stmporder
		ENDIF 
	NEXT
	l_result=CHRTRAN(l_result,CHR(250),",")
	l_result=LEFT(l_result,LEN(l_result)-1)
	RETURN l_result
*--- Ritorna il numero del report principale della categoria specificata successivo a nNumRec
*--- Se non esiste principale ritorna -1
FUNCTION GetMainReport(cStmpOrder, nNumRec)
	LOCAL l_Count, l_NumRow, bAll
	l_NumRow=this.GetNumReport()
	nNumRec=IIF(TYPE('nNumRec')='L' OR nNumRec<=0, 0, nNumRec)
	IF nNumRec+1>l_NumRow
		RETURN -1
	ENDIF 	
	bAll = TYPE("cStmpOrder")<>"C"
	FOR l_Count=nNumRec+1 TO l_NumRow
		IF (bAll OR this.GetStmpOrder(l_Count)==cStmpOrder) AND this.GetMain(l_Count)
			RETURN l_Count
		ENDIF 
	NEXT
	RETURN -1
*--- Ritorna il numero dei report principali della categoria specificata
FUNCTION GetNumMainReport(cStmpOrder)
	LOCAL l_Count, l_NumRow, l_Ret, bAll
	l_NumRow=this.GetNumReport()
	l_Ret = 0
	bAll = TYPE("cStmpOrder")<>"C"
	FOR l_Count=1 TO l_NumRow
		IF (bAll OR this.GetStmpOrder(l_Count)==cStmpOrder) AND this.GetMain(l_Count)
			l_Ret = l_Ret + 1
		ENDIF 
	NEXT
	RETURN l_Ret	
*--- Ritorna l'indice del report non principale successivo a nNumRec di categoria cStmpOrder
*--- Se non esitono di successivi ritorna -1
FUNCTION GetIndexReport(cStmpOrder, nNumRec)
	LOCAL l_Count, l_NumRow
	l_NumRow=this.GetNumReport()
	nNumRec=IIF(TYPE('nNumRec')='L' OR nNumRec<=0, 0, nNumRec)
	IF nNumRec+1>l_NumRow
		RETURN -1
	ENDIF 	
	FOR l_Count=nNumRec+1 TO l_NumRow
		IF this.GetStmpOrder(l_Count)==cStmpOrder AND !this.GetMain(l_Count)
			RETURN l_Count
		ENDIF 
	NEXT
	RETURN -1
*--- Ritorna il numero del report non skip successivo a nNumRec con cursore pieno
FUNCTION NextReport(nNumRec, bAnte)
	LOCAL l_Count, l_NumRow
	l_NumRow=this.GetNumReport()
	IF nNumRec+1>l_NumRow
		RETURN -1
	ENDIF 
	nNumRec=IIF(TYPE('nNumRec')='L' OR nNumRec<=0, 0, nNumRec)
	FOR l_Count=nNumRec+1 TO l_NumRow
		IF !this.GetSkip(l_Count) AND RecordCount(this.GetCursorName(l_Count))>0 AND (!bAnte OR !this.GetSkipAnte(l_Count))
			RETURN l_Count
		ENDIF 
	NEXT
	RETURN -1	

*--- Estrae in oMReport_dest la catena multireport che ha come main index nMainIndex
Function ExportReport(oMReport_dest, nMainIndex, cMainCursor, cCodISO, bSkip, cStrProc)
	LOCAL nSonIndex, nSonIndex, l_nNewIndex, l_child, l_ret
	*--- Copio main report
	nSonIndex=nMainIndex
	l_nNewIndex=oMReport_dest.CopyReport(this, nSonIndex, , (TYPE("cMainCursor")='C' AND NOT EMPTY(cMainCursor)))
	IF TYPE("cMainCursor")='C' AND NOT EMPTY(cMainCursor)
		oMReport_dest.SetCursor(cMainCursor, l_nNewIndex)
    ENDIF 
    *--- Valorizzo cCodISO
   	IF TYPE("cCodISO")<>'C' OR EMPTY(cCodISO)
		cCodISO = g_PROJECTLANGUAGE
    ENDIF 
    *--- Valorizzo il CodISO intestatario
    oMReport_dest.SetCodISOInst(cCodISO, l_nNewIndex)
    *--- Skip anteprima
    oMReport_dest.SetSkipAnte(bSkip, l_nNewIndex)
    *--- Stringa Processo Documentale
    IF TYPE("cStrProc") = 'C'
	    oMReport_dest.SetStrProc(cStrProc, l_nNewIndex)
	ENDIF 	    
	*--- Salvo il valore del nuovo valore inserito, prima dell'ordinamento
    l_ret = l_nNewIndex
	*--- Ciclo sui figli
	l_child=this.GetChild(nSonIndex)
	DO WHILE NOT EMPTY(l_child)
		nSonIndex=this.GetIndexByKey(l_child, nMainIndex)
		*--- Inserisco il figlio
		IF nSonIndex<>-1 AND NOT EMPTY(this.GetCursorName(nSonIndex)) &&AND  RecordCount(this.GetCursorName(nSonIndex))>0
			l_nNewIndex=oMReport_dest.CopyReport(this, nSonIndex)
			*--- Setto Cod. ISO
			oMReport_dest.SetCodISOInst(cCodISO, l_nNewIndex)
			*--- Skip anteprima: nei report secondari non lo disabilito mai
			* oMReport_dest.SetSkipAnte(bSkip, l_nNewIndex)
			l_child=this.GetChild(nSonIndex)
		ELSE
			oMReport_dest.SetChild("", l_nNewIndex)
			l_child=""
		ENDIF 
	ENDDO 
	*--- Ordino il multireport
	oMReport_dest.OrderReport()
	*--- Valorizza reportbehavior
	this.CopyHeader(oMReport_dest)
	RETURN l_ret
ENDFUNC 
*--- Controlla se necessario inserire il flag cambia flusso di stampa (utilizzato in caso di ristampa)	
FUNCTION CheckPrintSystemChanged()
	LOCAL l_Count, l_NumRow
	l_NumRow=this.GetNumReport()
	this.nPrnSysIndex = this.nPrnSysIndex + 1
	FOR l_Count=this.nPrnSysIndex TO l_NumRow
		IF this.GetPrnSys(l_Count)
			this.SetPrnSys(.T., this.nPrnSysIndex)
			EXIT
		ENDIF
	NEXT
	this.nPrnSysIndex = l_NumRow
*--- Controlla se esiste almeno un report con il check cambia print system
FUNCTION GetMRepPrnSysChange(oMReport, nIndex)
	LOCAL l_Count, l_NumRow, l_nIndex, l_nRet, l_Index, l_StmpOrder
	l_nRet = 0
	l_NumRow=this.GetNumReport()
	l_nIndex = IIF(TYPE('nIndex')='L' or nIndex>l_NumRow or nIndex<1, 1, nIndex)
	FOR l_Count=l_nIndex TO l_NumRow
		IF l_Count<>nIndex AND this.GetPrnSys(l_Count)
		*--- Rottura
			l_nRet=l_Count
			EXIT	
		ELSE 
		*--- Aggiungo i report
			oMReport.CopyReport(this, l_Count)
		ENDIF 
	NEXT
	*--- Copio testata
	this.CopyHeader(oMReport)
	*--- Controllo report principali
	l_StmpOrder=oMReport.GetStmpOrder(1)
	l_Index=oMReport.GetMainReport(l_StmpOrder)
	IF l_Index=-1
	*--- Non ho principale, promuovo il primo report come principale
		oMReport.SetMain(.t., 1)
	ENDIF
	*--- Controllo figli, se nello spezzare il multireport non riporto uno dei figli devo interrompere la catena
	l_NumRow=oMReport.GetNumReport()
	LOCAL l_KChild
	FOR l_Count=1 TO l_NumRow
		l_KChild=oMReport.GetChild(l_Count)
		IF NOT EMPTY(l_KChild) AND oMReport.GetIndexByKey(l_KChild)=-1
			oMReport.SetChild('', l_Count)
		ENDIF
	NEXT
	RETURN l_nRet
ENDFUNC 
	
*--- Crea oggetti oMultiReport_Text, oMultireport_Graphic, passati per riferimento 
*--- Ritorna 0 se il multireport che contiene il report principale � oMReport_Text, altrimenti 1
FUNCTION SplitTextGraphic(oMReport_Text, oMReport_Graphic)
	LOCAL l_Count, l_NumRow
	LOCAL l_main, l_nRet

	l_nRet=0
	l_NumRow=this.GetNumReport()
	FOR l_Count=1 TO l_NumRow
	        l_main=this.GetMain(l_Count)
		IF this.GetText(l_Count)  &&Solo testo
			oMReport_Text.CopyReport(this, l_Count)
		       l_nRet=IIF(l_main, 0, l_nRet)
		ELSE 	&&Grafica
			oMReport_Graphic.CopyReport(this, l_Count)
			l_nRet=IIF(l_main, 1, l_nRet)
		ENDIF 
		*--- Setto altre propriet�
	NEXT
	*--- Copia informazioni di header
	this.CopyHeader(oMReport_Text)
	this.CopyHeader(oMReport_Graphic)		
	RETURN l_nRet
ENDFUNC
*--- Popola il cursore dello zoom
PROCEDURE GetInfoZoom(cCursor, bCheck, cType)
	LOCAL l_NumRow, l_i, l_oa, l_FileName, l_EXCLWRD
	l_oa=SELECT()
	l_NumRow=this.GetNumReport()
	FOR l_i=1 TO l_NumRow
		IF bCheck
			INSERT INTO (cCursor) (XCHK, REPORT, DESCRI, PRINTER, TEXT, INDICE, EXCLWRD, NUMCOPY) VALUES (IIF(this.GetSkip(l_i),0,1), this.GetReport(l_i), this.GetDescription(l_i), this.GetRepPrinter(l_i), IIF(this.GetText(l_i),'S','N'), l_i, '  ', this.GetNumCopy(l_i))
		ELSE 
			l_FileName=this.GetReport(l_i)
			IF (cType<>'T')
			DO CASE 
				CASE cType='E' AND cp_IsStdFile(l_FileName,'XLTX') and g_OFFICE='M'
					l_EXCLWRD='E'
				CASE cType='W' AND (cp_IsStdFile(l_FileName,'DOCX') and g_OFFICE='M') OR (cp_IsStdFile(l_FileName,'SXW') and g_OFFICE='O')
					l_EXCLWRD='W'
				CASE cType='G' AND cp_IsStdFile(l_FileName,'VGR')
					l_EXCLWRD='G'					
				OTHERWISE 
					l_EXCLWRD=' '
			ENDCASE 
			INSERT INTO (cCursor) (REPORT, DESCRI, PRINTER, TEXT, INDICE, EXCLWRD, NUMCOPY) VALUES (this.GetReport(l_i), this.GetDescription(l_i), this.GetRepPrinter(l_i), IIF(this.GetText(l_i),'S','N'), l_i, l_EXCLWRD, this.GetNumCopy(l_i))
			ELSE
			*Memorizza nel cursore tutti i formati supportati
				l_EXCLWRD=''
				IF cp_IsStdFile(l_FileName,'XLTX') and g_OFFICE='M'
					l_EXCLWRD=l_EXCLWRD + 'E'
				ENDIF
				IF (cp_IsStdFile(l_FileName,'DOCX') and g_OFFICE='M') OR (cp_IsStdFile(l_FileName,'SXW') and g_OFFICE='O')
					l_EXCLWRD=l_EXCLWRD + 'W'
				ENDIF
				IF cp_IsStdFile(l_FileName,'VGR')
					l_EXCLWRD=l_EXCLWRD + 'G'
				ENDIF
			IF NOT EMPTY (NVL (this.GetReport(l_i),''))
			INSERT INTO (cCursor) ( REPORT, DESCRI, PRINTER, TEXT, INDICE, EXCLWRD, NUMCOPY,XCHK) VALUES (  this.GetReport(l_i), this.GetDescription(l_i), this.GetRepPrinter(l_i), IIF(this.GetText(l_i),'S','N'), l_i, l_EXCLWRD, this.GetNumCopy(l_i),IIF(this.GetSkip(l_i),0,1))
			ENDIF			
			ENDIF
		ENDIF 
	NEXT 
	SELECT(l_oa)
ENDPROC
*--- Aggiorno oggetto da cursore
PROCEDURE SetInfoZoom(cCursor)
	LOCAL l_oa , oripos
	l_oa=SELECT()
	SELECT(cCursor)
	oripos=RECNO()
	GO TOP 
	SCAN
		this.SetRepPrinter(PRINTER, INDICE)
		this.SetSkip(IIF(XCHK=1, .F., .T.), INDICE)
		this.SetNumCopy(NUMCOPY, INDICE)
	ENDSCAN
	IF oripos<=RECCOUNT()
		GOTO oripos 
	endif
	LOCAL l_cCursor
	l_cCursor = SYS(2015)
	SELECT DISTINCT NUMCOPY FROM (cCursor) INTO CURSOR (l_cCursor)
	IF RECCOUNT(l_cCursor)<2
		this.nNumCopy = NUMCOPY
		this.bNumCopy = .t.
	ELSE 
		this.bNumCopy = .f.
		this.nNumCopy = 1
	ENDIF
	USE IN (l_cCursor)
	SELECT(l_oa)
	**Setta la stampante se unica
	this.IsOnePrinter()
ENDPROC 
*--- Settaggio info da parent
PROCEDURE SetInfoFromParent(oMRParent)
	LOCAL l_i, l_NumRow, l_j, l_NumRow_Parent, l_printer, l_skip, l_nCopy, l_idx 
	l_NumRow_Parent=oMRParent.GetNumReport()
	l_NumRow=this.GetNumReport()
	FOR l_i=1 TO l_NumRow_Parent
		l_skip=oMRParent.GetSkip(l_i)
		l_printer=oMRParent.GetRepPrinter(l_i)
		l_nCopy=oMRParent.GetNumCopy(l_i)
		FOR l_j=1 TO l_NumRow
			l_idx = this.GetIndex(l_j)
			IF VARTYPE(l_idx)='N' And l_idx=l_i
				this.SetSkip(l_skip, l_j)
				this.SetRepPrinter(l_printer, l_j)
				this.SetNumCopy(l_nCopy, l_j)
			ENDIF 
		NEXT 
	NEXT 
ENDPROC 
*--- Copia informazioni del header da this a OMReport_dest
PROCEDURE CopyHeader(OMReport_dest)
	OMReport_dest.cReportBehavior = this.cReportBehavior
	OMReport_dest.cCodISOSel = this.cCodISOSel 
	OMReport_dest.cSessionCode = this.cSessionCode 
	OMReport_dest.bPDObbl = this.bPDObbl 			&&Processo obbligatorio
	OMReport_dest.bPDLog = this.bPDLog 				&&Log Processo
	OMReport_dest.bPDAnte = this.bPDAnte 			&&Anteprima Processo
	OMReport_dest.bPDProc = this.bPDProc 			&&Processo da eseguire
	OMReport_dest.nPDTotDoc = this.nPDTotDoc 		&&DM TotDoc
	OMReport_dest.nPDTotDocOk = this.nPDTotDocOk 	&&DM TotDocOk
	OMReport_dest.nPDTotDocKo = this.nPDTotDocKo 	&&DM TotDocKo
	OMReport_dest.bPDISOLan = this.bPDISOLan 		&&DM ISOLanguage
	OMReport_dest.cPDCodProc = this.cPDCodProc		&&DM Codice processo
	OMReport_dest.bNumCopy = this.bNumCopy 			&& bNumCopy 
	OMReport_dest.nNumCopy = this.nNumCopy 			&& nNumCopy 
ENDPROC 
*--- Settaggio numero copie su tutti i report
PROCEDURE SetAllNumCopy(nNumCopy)
	LOCAL l_i, l_NumRow
	IF this.bNumCopy
		l_NumRow=this.GetNumReport()
		FOR l_i=1 TO l_NumRow
			this.SetNumCopy(nNumCopy, l_i)
		NEXT
	ENDIF
ENDPROC 
*--- Funzioni Get
FUNCTION GetReport(nIndex)
 *--- Se il codice ISO � presente ritorno il report tradotto
	cCodISO = IIF(this.cCodISOSel == "@@@", this.GetCodISOInst(nIndex), this.cCodISOSel)
	*--- Se cCodISO � vuoto significa traduzione per destinatario
	IF cCodISO == this.GetCodISOTrad(nIndex) AND cCodISO <> this.GetCodISO(nIndex) AND NOT EMPTY(this.GetCodISOTrad(nIndex)) AND NOT EMPTY(this.GetCodISO(nIndex))
		RETURN this.GetReportTrad(nIndex)
	ENDIF 
 *--- Se il report non � tradotto ritorno il report originale
	RETURN this.GetReportOrig(nIndex)	
FUNCTION GetReportOrig(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_NAME])='C', this.aReportList[nIndex, REP_NAME], '')	
 *--- Ritorna il nome del report con il percorso di default (No Custom\...) 	
FUNCTION GetReportDOCM(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_DOCM])='C', this.aReportList[nIndex, REP_DOCM], '')
FUNCTION GetReportTrad(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_NAMETRAD])='C', this.aReportList[nIndex, REP_NAMETRAD], '')
FUNCTION GetCodISO(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_CODISO])='C', this.aReportList[nIndex, REP_CODISO], '')	
FUNCTION GetCodGDI(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_CODGDI])='C', this.aReportList[nIndex, REP_CODGDI], '')	
FUNCTION GetCodISOTrad(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_CODISOTRAD])='C',this.aReportList[nIndex, REP_CODISOTRAD], '')	
FUNCTION GetCodISOrep(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_CODISOREP])='C',this.aReportList[nIndex, REP_CODISOREP], '')		
FUNCTION GetCodISOInst(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_CODISOINST])='C', this.aReportList[nIndex, REP_CODISOINST], '')
FUNCTION GetCursorName(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_CURSOR])='C', this.aReportList[nIndex, REP_CURSOR], '')
FUNCTION GetParamqry(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_PARAMQRY])='O', this.aReportList[nIndex, REP_PARAMQRY], .null.)	
FUNCTION GetNoReset(nIndex)
	RETURN this.aReportList[nIndex, REP_NORESET]
FUNCTION GetVarEnv(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_ENVIRONMENT])='O', this.aReportList[nIndex, REP_ENVIRONMENT], .null.)	
FUNCTION GetMain(nIndex)
	RETURN this.aReportList[nIndex, REP_MAIN]	
FUNCTION GetText(nIndex)
	RETURN this.aReportList[nIndex, REP_TEXT]	
FUNCTION GetSplit(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_SPLIT])='C',this.aReportList[nIndex, REP_SPLIT], '')
FUNCTION GetLink(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_LINK])='C', this.aReportList[nIndex, REP_LINK], '')	
FUNCTION GetStmpOrder(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_STMPORDER])='C', this.aReportList[nIndex, REP_STMPORDER], '')
FUNCTION GetDettOrder(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_DETTORDER])='N', this.aReportList[nIndex, REP_DETTORDER], 0)	
FUNCTION GetRepPrinter(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_PRINTER])='C', this.aReportList[nIndex, REP_PRINTER], '')	
FUNCTION GetDescription(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_DESCRI])='C', this.aReportList[nIndex, REP_DESCRI], '')
FUNCTION GetOrder(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_ORDER])='C', this.aReportList[nIndex, REP_ORDER], '')
FUNCTION GetSkip(nIndex)
	RETURN this.aReportList[nIndex, REP_SKIP]	
FUNCTION GetIndex(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_INDEX])='N', this.aReportList[nIndex, REP_INDEX], 0)	
FUNCTION GetParent(nIndex)
	RETURN  IIF(VARTYPE(this.aReportList[nIndex, REP_PARENT])='C', this.aReportList[nIndex, REP_PARENT], '')
FUNCTION GetChild(nIndex)
	RETURN  IIF(VARTYPE(this.aReportList[nIndex, REP_CHILD])='C', this.aReportList[nIndex, REP_CHILD], '')
FUNCTION FirstReport()
	RETURN IIF(VARTYPE(this.aReportList[1, REP_NAME])='C', this.aReportList[1, REP_NAME], '')
FUNCTION GetNumReport()
	RETURN ALEN(this.aReportList, 1)
FUNCTION GetStrProc(nIndex)
	RETURN  IIF(VARTYPE(this.aReportList[nIndex, REP_PDSTRPROC])='C', this.aReportList[nIndex, REP_PDSTRPROC], '')
FUNCTION GetSKipAnte(nIndex)
	RETURN  this.aReportList[nIndex, REP_SKIPANTE]	
FUNCTION GetReportList()
	LOCAL l_Count, l_NumRow, l_result
	l_result=''
	l_NumRow=this.GetNumReport()
	FOR l_Count=1 TO l_NumRow
		l_result=l_result + this.GetDescription(l_Count)+',' 
	NEXT	
	RETURN LEFT(l_result, LEN(l_result)-1)	&&Elimino l'ultima virgola
FUNCTION GetReportISO()
	LOCAL l_Count, l_NumRow, l_result
	l_result=''
	l_NumRow=this.GetNumReport()
	FOR l_Count=1 TO l_NumRow
		l_result=l_result + this.GetCodISO(l_Count)+',' 
	NEXT	
	RETURN LEFT(l_result, LEN(l_result)-1)	&&Elimino l'ultima virgola	
FUNCTION GetKeyReport(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_KEY])='C', this.aReportList[nIndex, REP_KEY], '')
FUNCTION GetIndexByKey(cKey, nMainIndex)
	LOCAL l_Count, l_NumRow, l_result
	nMainIndex=IIF(VARTYPE(nMainIndex)='N', nMainIndex, 1)
	l_result=0
	IF NOT EMPTY(cKey)
	l_NumRow=this.GetNumReport()
	FOR l_Count=nMainIndex TO l_NumRow
		IF this.GetKeyReport(l_Count)==cKey
			RETURN l_Count
		ENDIF 
	NEXT
	ENDIF
 	RETURN -1
FUNCTION GetExtReport(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_EXTFILE])='C', this.aReportList[nIndex, REP_EXTFILE], '') 
FUNCTION GetNumCopy(nIndex)
	RETURN IIF(VARTYPE(this.aReportList[nIndex, REP_NCOPY])='N', this.aReportList[nIndex, REP_NCOPY] , 1)
FUNCTION GetPrnSys(nIndex)
	RETURN this.aReportList[nIndex, REP_PRNSYS]	
FUNCTION CountAllReportsNotSkipped()
* Controlla se ci sono dei report da stampare e ne restituisce il numero
    LOCAL l_NumReport, l_Count, l_TotReportsNotSkipped
    l_TotReportsNotSkipped=0
    l_NumReport=this.GetNumReport()
    FOR l_Count=1 TO l_NumReport
        IF NOT this.GetSkip(l_Count)
                l_TotReportsNotSkipped=l_TotReportsNotSkipped+1
        ENDIF        
    NEXT
    RETURN l_TotReportsNotSkipped
FUNCTION CountDOCMReportsNotSkipped()
* Controlla se ci sono dei report con processo documentale da stampare e ne restituisce il numero
    LOCAL l_NumReport, l_Count, l_TotReportsNotSkipped
    l_TotReportsNotSkipped=0
    l_NumReport=this.GetNumReport()
    FOR l_Count=1 TO l_NumReport
        IF NOT this.GetSkip(l_Count) AND NOT EMPTY(this.GetStrProc(l_Count))
                l_TotReportsNotSkipped=l_TotReportsNotSkipped+1
        ENDIF        
    NEXT
    RETURN l_TotReportsNotSkipped
*--- Funzioni Set	
PROCEDURE SetReport(cReportName, nIndex)
	this.aReportList[nIndex, REP_NAME]= UPPER (cReportName)
PROCEDURE SetReportDOCM(cReportName, nIndex)
	this.aReportList[nIndex, REP_DOCM]=UPPER (cReportName)	
PROCEDURE SetReportTrad(cReportName, nIndex)
	this.aReportList[nIndex, REP_NAMETRAD]=UPPER (cReportName)
PROCEDURE SetCodISO(cCodISO, nIndex)
	this.aReportList[nIndex, REP_CODISO]=cCodISO
PROCEDURE SetCodgdi(cCodGdi, nIndex)
	this.aReportList[nIndex, REP_CODGDI]=ALLTRIM(NVL (cCodGdi,' '))
PROCEDURE SetCodISOTrad(cCodISO, nIndex)
	this.aReportList[nIndex, REP_CODISOTRAD]=cCodISO	
PROCEDURE SetCodISOrep(cCodISO, nIndex)
	this.aReportList[nIndex, REP_CODISOREP]=cCodISO		
PROCEDURE SetCodISOInst(cCodISO, nIndex)
	this.aReportList[nIndex, REP_CODISOINST]=cCodISO	
PROCEDURE SetCursor(cCursor, nIndex)
	this.aReportList[nIndex, REP_CURSOR]=cCursor
	*--- GarbageCollect 
	IF NOT EMPTY(cCursor)
		IF TYPE("g_GarbageCollect")<>'O'
			PUBLIC g_GarbageCollect
			g_GarbageCollect = CREATEOBJECT("GarbageCollect")
		ENDIF 
		g_GarbageCollect.AddCursor(cCursor)
	ENDIF 
PROCEDURE SetParamqry(oParamqry, nIndex)
	this.aReportList[nIndex, REP_PARAMQRY]=oParamqry
PROCEDURE SetNoReset(bNoReset, nIndex)
	this.aReportList[nIndex, REP_NORESET]=bNoReset
PROCEDURE SetVarEnv(oVarEnv, nIndex)
	this.aReportList[nIndex, REP_ENVIRONMENT]=oVarEnv
PROCEDURE SetMain(bMain, nIndex)
	this.aReportList[nIndex, REP_MAIN]=bMain	
PROCEDURE SetText(bText, nIndex)
	this.aReportList[nIndex, REP_TEXT]=bText	
PROCEDURE SetSplit(cSplit, nIndex)
	this.aReportList[nIndex, REP_SPLIT]=cSplit
PROCEDURE SetLink(cLink, nIndex)
	this.aReportList[nIndex, REP_LINK]=cLink	
PROCEDURE SetStmpOrder(cStmpOrder, nIndex)
	this.aReportList[nIndex, REP_STMPORDER]=alltrim(cStmpOrder)
PROCEDURE SetDettOrder(nDettOrder, nIndex)
	this.aReportList[nIndex, REP_DETTORDER]=nDettOrder
PROCEDURE SetRepPrinter(bChgPrinter, nIndex)
	this.aReportList[nIndex, REP_PRINTER]= UPPER(ALLTRIM(bChgPrinter) )
PROCEDURE SetDescription(cRepDescription, nIndex)
	this.aReportList[nIndex, REP_DESCRI]=cRepDescription
PROCEDURE SetOrder(cOrder, nIndex)
	this.aReportList[nIndex, REP_ORDER]=cOrder
PROCEDURE SetSkip(bSkip, nIndex)
	this.aReportList[nIndex, REP_SKIP]=bSkip
PROCEDURE SetIndex(RepIndex, nIndex)
	this.aReportList[nIndex, REP_INDEX]=RepIndex
PROCEDURE SetParent(RepIndex, nIndex)
	this.aReportList[nIndex, REP_PARENT]=RepIndex
PROCEDURE SetChild(RepIndex, nIndex)
	this.aReportList[nIndex, REP_CHILD]=RepIndex
PROCEDURE SetKeyReport(RepIndex, nIndex)
	this.aReportList[nIndex, REP_KEY]=RepIndex
PROCEDURE SetStrProc(cStrProc, nIndex)
	this.aReportList[nIndex, REP_PDSTRPROC]=cStrProc
PROCEDURE SetSkipAnte(bSkipAnte, nIndex)
	this.aReportList[nIndex, REP_SKIPANTE]=bSkipAnte	
PROCEDURE SetExtReport(cExt, nIndex)
	this.aReportList[nIndex, REP_EXTFILE]=cExt	
PROCEDURE SetNumCopy(nCopy, nIndex)
	this.aReportList[nIndex, REP_NCOPY]=nCopy	
PROCEDURE SetPrnSys(bPrnSys, nIndex)
	this.aReportList[nIndex, REP_PRNSYS]=bPrnSys	
ENDDEFINE

*--- Record count , conta il numero di record non cancellati di un cursore
FUNCTION RecordCount(cCursor)
	LOCAL l_oldarea, l_NumRec, l_error, l_err
	l_err = .f.
	l_oldarea = SELECT()
	l_error = ON("Error")
	ON ERROR l_err = .t.
	SELECT (cCursor)
	COUNT FOR NOT DELETED() TO l_NumRec
	ON ERROR &l_error
	SELECT(l_oldarea)
	RETURN IIF(l_err, 0, l_NumRec)
ENDFUNC 

*--- Class GarbageCollect
*--- Gestisce la chiusura dei cursori e la cancellazione dei report tradotti che possono essere stati generati da un multireport
*--- L'oggetto viene istanziato come pubblico dalla AddNewReport
#define GARBAGE_NAME 1
#define GARBAGE_COUNT 2
DEFINE CLASS GarbageCollect As Custom 
	DIMENSION aGarbageCursor(1,2)
	nNumElem = 0
PROCEDURE Init()
	this.SetCursor("",0,1)	
*--- Aggiunge un riferimento a cursore	
PROCEDURE AddCursor(cCursor)
	LOCAL l_nCount, l_Index
	IF NOT EMPTY(cCursor)
		l_nCount = this.GetCursorCount(cCursor, @l_Index)
		IF l_nCount<>0
		*--- il cursore � gi� stato usato, incremento il contatore
			this.SetCursor(cCursor, l_nCount + 1, l_Index)
		ELSE
			*--- il cursore non � ancora mai stato utilizzato aggiungo un elemento
			this.nNumElem = this.nNumElem + 1
			DIMENSION this.aGarbageCursor(this.nNumElem, 2)
			this.SetCursor(cCursor, 1, this.nNumElem)
		ENDIF 
	ENDIF 
*--- Rimouve un cursore
PROCEDURE RemoveCursor(cCursor)	
	LOCAL l_nCount, l_Index
	IF NOT EMPTY(cCursor)
		l_nCount = this.GetCursorCount(cCursor, @l_Index)
		IF l_nCount > 1
			this.SetCursor(cCursor, l_nCount - 1, l_Index)
		ELSE
			IF l_nCount = 1
				*--- Chiudo il cursore
				* --- Il cursore __TMP__ non si rimuove per utilizzi a seguito della stampa
				IF UPPER(cCursor)<>'__TMP__'
				USE IN SELECT(cCursor)
				endif
				ADEL(this.aGarbageCursor, l_Index)
				this.nNumElem = this.nNumElem - 1
				IF this.nNumElem > 0
					DIMENSION this.aGarbageCursor(this.nNumElem, 2)				
				ELSE
					*--- non ho pi� elementi distruggo l'oggetto
					RELEASE g_GarbageCollect
				ENDIF 
			ENDIF 
		ENDIF 
	ENDIF 
*--- Get function		
*--- Numero di riferimenti
FUNCTION GetCursorCount(cCursor, l_Index)
	l_Index = ASCAN(this.aGarbageCursor, cCursor,1,-1,1,8)
	IF l_Index > 0
		RETURN this.aGarbageCursor[l_Index, GARBAGE_COUNT]
	ENDIF 
	RETURN 0
*--- Set procedure	
PROCEDURE SetCursor(cCursor, nCount, nIndex)
	this.aGarbageCursor[nIndex, GARBAGE_NAME] = cCursor
	this.aGarbageCursor[nIndex, GARBAGE_COUNT] = nCount
ENDDEFINE 

*--- Class ReportEnvironment
*--- Gestisce l'ambiente delle variabili l_
#define VAR_NAME 1
#define VAR_VALUE 2
DEFINE CLASS ReportEnvironment As Custom 
	DIMENSION aVariables(1,2)
*--- Inizializzazione
PROCEDURE Init()
	this.SetVarName('',1)
	this.SetVarValue('',1)
ENDPROC 
*--- L'oggetto � vuoto?
FUNCTION IsEmpty()
RETURN EMPTY(this.aVariables[1,1])
*--- Aggiunge un nuova variabile
PROCEDURE AddVariable(cVarName, pVarValue, nIndex)
	LOCAL l_NumRow, l_NumCol
	IF this.IsEmpty()
		nIndex=1
	ELSE 
		l_NumRow=ALEN(this.aVariables, 1)
		l_NumCol=ALEN(this.aVariables, 2)
		nIndex=IIF(TYPE('nIndex')='L' OR nIndex>l_NumRow OR nIndex<=0, l_NumRow+1, nIndex)
		DIMENSION this.aVariables(l_NumRow+1, l_NumCol)
		AINS(this.aVariables, nIndex)
	ENDIF 	
	this.SetVarName(cVarName, nIndex)
	this.SetVarValue(pVarValue, nIndex)
ENDPROC 
*--- Set
PROCEDURE SetVarName(cVarName, nIndex)
	this.aVariables[nIndex,VAR_NAME]= cVarName
ENDPROC 	
PROCEDURE SetVarValue(pVarValue, nIndex)
	this.aVariables[nIndex,VAR_VALUE]= pVarValue	
ENDPROC 
*--- Get
FUNCTION GetVarName(nIndex)
	RETURN this.aVariables[nIndex,VAR_NAME]
FUNCTION GetVarValue(nIndex)
	RETURN this.aVariables[nIndex,VAR_VALUE]
FUNCTION GetNumVar()
	RETURN ALEN(this.aVariables, 1)
ENDDEFINE 

*--- Form di selezione report
DEFINE CLASS selform AS CpSysForm
	Height = 250
	Width = 500
	AutoCenter = .T.
	Caption = "REPORT LIST"
	MaxButton = .F.
	MinButton = .F.
	AlwaysOnTop = .t.
	Name = "SelForm"
	
	nNumReport=0
	
	ADD OBJECT replist AS listbox WITH ;
		Height = 250, ;
		Left = 0, ;
		Top = 0, ;
		Width = 500, ;
		Name = "RepList",;
		Anchor = 15
		
	PROCEDURE replist.DblClick
		LOCAL nCnt
		FOR nCnt = 1 TO ThisForm.RepList.ListCount
		   IF ThisForm.RepList.Selected(nCnt)
     		  ThisForm.nNumReport=nCnt
			  ThisForm.Hide()
		      EXIT 
		   ENDIF
		ENDFOR
	ENDPROC
ENDDEFINE
*--- Zucchetti Aulla - Fine Multireport

*--- Zucchetti Aulla Inizio - Bottoni con menu per toolbar
DEFINE CLASS BtnMenuTB as CPToolBtn
nRow=0
nCol=0
	PROCEDURE Click()
		*--- Determino posizione da cui far partire il menu
		*--- Le posizioni sono espresse in foxel
		IF INLIST(this.Parent.dockposition, -1, 1, 2) 
			this.nRow=MROW(_screen.caption)
		ENDIF 
		IF INLIST(this.Parent.dockposition,-1,0,3)
			This.nCol=MCOL(_screen.caption)		
		ENDIF 
		IF this.Parent.dockposition=0
			this.nRow=0
		ENDIF 
		IF this.Parent.dockposition=3
			this.nRow=(_screen.Height+this.Parent.Height)/FONTMETRIC(1)
		ENDIF
		IF this.Parent.dockposition=1
			This.nCol=0
		ENDIF
		IF this.Parent.dockposition=2
			This.nCol=_screen.Width/FONTMETRIC(6)
		ENDIF 
		this.nRow=ROUND(this.nRow,3)
		This.nCol=ROUND(This.nCol,3)
	ENDPROC 
ENDDEFINE 


* --- Zucchetti Aulla Inizio - Assegna application role
PROCEDURE AH_ApplicationRole( nConnessione , bSincrona )
     LOCAL l_ErrorForSettingApplicationRole,i_Result
     l_ErrorForSettingApplicationRole = ON('error')	                 
     ON ERROR
     i_Result=0
     do while i_Result=0
       i_Result=Sqlexec(nConnessione,"exec sp_setapprole '"+g_APPROLE+"','"+g_APPPSW+"'")
     enddo
     ON error &l_ErrorForSettingApplicationRole
     RETURN i_Result
ENDPROC
* --- Zucchetti Aulla Fine - Assegna Application Role

* --- Zucchetti Aulla Inizio - campo ricerca per contenuto
DEFINE CLASS AH_SEARCHFLD AS StdField
   * propriet� per contenere il nome dello shape per i campi obbligatori
   CShape=''
   * Valore precedente per cui � stata notificato l'evento di ricerca
   * Nel caso in cui sia necessario impostare un valore di init, la propriet� dovr� essere 
   * opportunamente gestita ed inizializzata con lo stesso valore di init
   OldSearchValue=''
    
procedure Init()
    DODEFAULT()  
    LOCAL l_olderr, berr
    l_olderr=ON('error')
    ON ERROR berr=.t.
    This.CShape='TTxOb'+This.Name
    This.Parent.AddObject(This.CShape,'RicContShape',This.Top-1,This.Left-1,This.Width+2,This.Height+2,1,This)
    on error &l_olderr
ENDPROC 

PROCEDURE value_Assign(xvalue)
IF !m.xValue==this.value
   IF LEFT(m.xValue,1)='%'
      m.xValue=SUBSTR(m.xValue,2)
   ENDIF 
   IF RIGHT(ALLTRIM(m.xValue),1)='%'
      m.xValue=SUBSTR(m.xValue,1,LEN(ALLTRIM( m.xValue))-1)
   ENDIF 
   this.value=m.xValue
ENDIF 
ENDPROC

PROCEDURE visible_assign(xvalue)
	DODEFAULT()
	Local CShape
	IF this.visible <> m.xValue
		this.visible = m.xValue
		If Type('this.cShape')='C' And Not Empty(this.cShape)
		   CShape = this.CShape
		   * nascondo il rettangolo di campo obbligatorio
		   * se campo non visible
		   this.parent.&CShape..visible = this.visible And this.enabled
		endif
	Endif
ENDPROC 

FUNCTION Valid()
DODEFAULT()
local i_var, bUpd
i_var=this.cFormVar
IF NOT EMPTY(THIS.VALUE) 
   This.parent.oContained.&i_var='%'+ALLTRIM(this.value)+'%'
ENDIF 
* --- Lancio un evento specifico per gestire le ricerche
IF !ALLTRIM(this.value)==this.OldSearchValue
   this.OldSearchValue=ALLTRIM(this.value)
   this.parent.oContained.NotifyEvent(i_var+' Searching')   
endif

Proc Draw_Link_Obj
DODEFAULT()
LOCAL ctShape
ctShape='this.parent.'+This.cShape
IF Type(ctShape)='O'
   ctShape=This.cShape
   This.Parent.&ctShape..Riposiziona()
ENDIF 
ENDPROC

ENDDEFINE

DEFINE CLASS AH_SEARCHMEMO AS StdMemo
   * propriet� per contenere il nome dello shape per i campi obbligatori
   CShape=''
   * Valore precedente per cui � stata notificato l'evento di ricerca
   * Nel caso in cui sia necessario impostare un valore di init, la propriet� dovr� essere 
   * opportunamente gestita ed inizializzata con lo stesso valore di init
   OldSearchValue=''

procedure Init()
    DODEFAULT()
    LOCAL l_olderr, berr
    l_olderr=ON('error')
    ON ERROR berr=.t.
    This.CShape='TTxOb'+This.Name
    This.Parent.AddObject(This.CShape,'RicContShape',This.Top-1,This.Left-1,This.Width+2,This.Height+2,1,This)
    on error &l_olderr
ENDPROC 

PROCEDURE value_Assign(xvalue)
IF !m.xValue==this.value
   IF LEFT(m.xValue,1)='%'
      m.xValue=SUBSTR(m.xValue,2)
   ENDIF 
   IF RIGHT(ALLTRIM(m.xValue),1)='%'
      m.xValue=SUBSTR(m.xValue,1,LEN(ALLTRIM( m.xValue))-1)
   ENDIF 
   this.value=m.xValue
ENDIF 
ENDPROC

PROCEDURE visible_assign(m.xValue)
	DODEFAULT()
	Local CShape
	IF this.visible <> m.xValue
		this.visible = m.xValue
		If Type('this.cShape')='C' And Not Empty(this.cShape)
		   CShape = this.CShape
		   * nascondo il rettangolo di campo obbligatorio
		   * se campo non visible
		   this.parent.&CShape..visible = this.visible And this.enabled
		endif
	Endif
ENDPROC 

FUNCTION Valid()
DODEFAULT()
local i_var, bUpd
i_var=this.cFormVar
IF NOT EMPTY(THIS.VALUE) 
   This.parent.oContained.&i_var='%'+ALLTRIM(this.value)+'%'
ENDIF 
* --- Lancio un evento specifico per gestire le ricerche
IF !ALLTRIM(this.value)==this.OldSearchValue
   this.OldSearchValue=ALLTRIM(this.value)
   this.parent.oContained.NotifyEvent(i_var+' Searching')   
endif

Proc Draw_Link_Obj
DODEFAULT()
LOCAL ctShape
ctShape='this.parent.'+This.cShape
IF Type(ctShape)='O'
   ctShape=This.cShape
   This.Parent.&ctShape..Riposiziona()
ENDIF 
ENDPROC

ENDDEFINE

* Rettangolino per evidenziazione ricerca per contenuto
DEFINE CLASS RicContShape as OblShape

PROCEDURE init(tTop,tLeft,tWidth,tHeight,_Zorder,father)
 DODEFAULT(tTop,tLeft,tWidth,tHeight,_Zorder,father)
 THIS.Bordercolor=g_RicContColor
ENDPROC

ENDDEFINE
* --- Zucchetti Aulla Fine - campo ricerca per contenuto

* --- Zucchetti Aulla Inizio - OCX Calendario
define CLASS cp_ctDropDate as container
  cEvent=''
  var=''
  nBaseCal=0
  MonthNames=""
  DayNames=""
  TodayText=""
  FontName=''
  FontSize=0
  
  add object cal as DBIDropDate

  proc Init()  
    this.cal.left=0
    this.cal.top=0
    this.cal.width=this.width
    this.cal.height=this.height
    this.cal.visible=.t.
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.Font.Name = this.FontName
    this.cal.Font.Size = this.FontSize
    this.cal.CalFont.Name = this.FontName
    this.cal.CalFont.Size = this.FontSize
    this.cal.CalTitleFont.Name = this.FontName
    this.cal.CalTitleFont.Size = this.FontSize
    this.cal.FormatType=1
    this.cal.MinValue=1
    this.cal.MaxValue=172142
    this.cal.MonthNames=g_Mese[1]+","+g_Mese[2]+","+g_Mese[3]+","+g_Mese[4]+","+g_Mese[5]+","+g_Mese[6]+","+g_Mese[7]+","+g_Mese[8]+","+g_Mese[9]+","+g_Mese[10]+","+g_Mese[11]+","+g_Mese[12]
    this.cal.DayNames=ah_MsgFormat("D,L,M,M,G,V,S")
    this.cal.DateOffset=0
    this.cal.EditOnError=.t.
    * calendar
    this.cal.CalHeight=160
    this.cal.CalWidth=180    
    this.cal.CalDayHeader="D,L,M,M,G,V,S"
    this.cal.CalBorderType=0
    this.cal.CalBorderStyle=0
    this.cal.CalDateBorder=1
    this.cal.CalSelectType=1
    this.cal.CalFirstDay=1
    this.cal.CalTitleAlign=2
    this.cal.CalButtonType=2
    this.cal.CalTodayFormat=2
    this.cal.CalLevelDepth=2
    this.cal.CalLevelBorder=0
    this.cal.CalLevelOffset=2
    * this.cal.CalWeekEnds=65
    this.cal.CalWeekEnds=1
    * color
    this.cal.CalLevelColor = g_ColHeader
    this.cal.BackColor = g_ColPrime
    this.cal.CalBackColor = g_ColPrime
    this.cal.CalSelectBackColor = g_ColSelect
    this.cal.CalTodayColor = g_ColSelect    
    * this.cal.CalHeaderColor=g_ColHeader
    this.cal.CalTitleColor=0
    this.nBaseCal=VAL(SYS(11,cp_CharToDate("01-01-1900","dd-mm-yyyy")))
    * init 
    this.cal.CalTodayText=this.TodayText
*!*	  proc UIEnable(bEnable)
*!*	    this.visible=bEnable

  proc Calculate(xValue)

  proc Event(cEvent)

  proc SetDate(dDate)
    this.cal.date=VAL(SYS(11,dDate))-this.nBaseCal
  
  PROCEDURE SetColours
    PARAMETERS ColPrime, ColSelect, ColHeader, ColNonPrime
    this.cal.CalLevelColor = ColHeader
    this.cal.BackColor = ColPrime
    this.cal.CalBackColor = ColPrime
    this.cal.CalSelectBackColor = ColSelect
    this.cal.CalTodayColor = ColSelect
    
  proc cal.Change()
  	LOCAL dNewDate,n,d,o, old
    * check data
    if this.date > this.MaxValue OR this.date < this.MinValue
      cp_ErrorMsg("Attenzione! Data inserita non corretta!",48)
      dNewDate = i_datsys
      this.date = VAL(SYS(11,dNewDate))-this.parent.nBaseCal
    else 
   	  dNewDate = CTOD(SYS(10,this.Date+this.parent.nBaseCal))
    endif 
    * assegna variabile
    IF not(empty(this.parent.var)) and this.parent.enabled
      n=this.parent.var
      old = this.parent.parent.parent.parent.parent.&n
      IF old # dNewDate 
        * Notifichiamo l'evento solo se la data � cambiata
        this.parent.parent.parent.parent.parent.&n = dNewDate
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
        this.parent.parent.parent.parent.parent.NotifyEvent('ChangeDate')
      ENDIF 
    ENDIF 

 PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
ENDDEFINE

define CLASS cp_ctMDay as container
  cEvent=''
  var=''
  serial=''
  Visibile=.F.
  nBaseCal=0
  * Propriet� disponibili nell'oggettino
  IncludeAmPm=.F.
  AllDayHeight=0
  DisplayColumns=0
  VirtualColumns=0
  HeaderLineBreak=.F.
  MonthNames=""
  DayNames=""
  DateFormat="d n M Y"
  FontName=''
  FontSize=0
  varStart=''
  varEnd=''
  varDate=''
  VarTxt=''
  VarClmn=''
  
  ADD OBJECT CalTimer AS CalendarTimer
  
  add object cal as DBIMDay

  proc Init()
    this.cal.left=0
    this.cal.top=0
    this.cal.width=this.width
    this.cal.height=this.height
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.Font.Name = this.FontName
    this.cal.Font.Size = this.FontSize
    this.cal.HeaderFont.Name = this.FontName
    this.cal.HeaderFont.Size = this.FontSize
    this.cal.HourFont.Name = this.FontName
    this.cal.HourFont.Size = this.FontSize
    this.cal.PrintFont.Name = this.FontName
    this.cal.PrintFont.Size = this.FontSize
    this.cal.PrintTitleFont.Name = this.FontName
    this.cal.PrintTitleFont.Size = this.FontSize
    this.cal.PrintTimeFont.Name = this.FontName
    this.cal.PrintTimeFont.Size = this.FontSize
    this.cal.TimeFont.Name = this.FontName
    this.cal.TimeFont.Size = this.FontSize
    this.cal.TitleFont.Name = this.FontName
    this.cal.TitleFont.Size = this.FontSize
    this.cal.MonthNames=g_Mese[1]+","+g_Mese[2]+","+g_Mese[3]+","+g_Mese[4]+","+g_Mese[5]+","+g_Mese[6]+","+g_Mese[7]+","+g_Mese[8]+","+g_Mese[9]+","+g_Mese[10]+","+g_Mese[11]+","+g_Mese[12]
    this.cal.DayNames=g_Giorno[1]+","+g_giorno[2]+","+g_giorno[3]+","+g_giorno[4]+","+g_giorno[5]+","+g_giorno[6]+","+g_giorno[7]
    this.cal.EnterKey=1 
    this.cal.TabIndex=1 
    this.cal.AppointMoveType=0
    this.cal.KillFocusEvent=.T.
    this.cal.HeaderBorder=1
    this.cal.HeaderLineBreak=.f.
    this.cal.IncludeAmPm=.f.
    this.cal.MaskBitmap=.F.
    this.cal.DisplayHeader=.T.
    this.cal.MilitaryTime=.T.
    this.cal.HeaderFillType=2 && 0 - Solid, 1 - Horizontal, 2 - Vertical, 3 - Diagonal, 4 - Reverse Diagonal, 5 - Horizontal Bump, 6 - Vertical Bump, 7 - Diamond, 8 - Pyramid, 9 - Circle, 10 - Ellipse
    this.cal.TitleFillType=2
    this.cal.TimeFillType=1
    this.cal.TimeType=1
    this.cal.TimeUnits=3
    this.cal.DateFormat=this.DateFormat 
    this.cal.DateOffset=0
    this.cal.ColumnSepWidth=5
    this.cal.Column=0
    this.cal.DisplayColumns=this.DisplayColumns
    this.cal.ColumnAppType=0
    * Color 
    this.cal.AllDayBackColor=g_ColNnPrm
    this.cal.NonPrimeColor=g_ColNnPrm
    this.cal.PrimeTimeColor=g_ColPrime
    this.cal.SelectedBackColor=g_ColSelect
    this.cal.BarMoveColor=g_ColHeader
    this.cal.BarSizeColor=g_ColHeader
    this.cal.HeaderBackColor=g_ColHeader
    this.cal.TitleBackColor=g_ColHeader
    this.cal.TimeBackColor=g_ColHeader
    this.cal.ColumnSepColor=g_ColSelect
    this.cal.SelectedTimeColor=g_ColSelect
    this.cal.ColumnSepWidth=0
    this.cal.TimeBorder=0
    this.cal.BorderColor=g_ColHeader
    this.nBaseCal=VAL(SYS( 11 , cp_CharToDate('01-01-1900', 'dd-mm-yyyy') ))
    * Ci posizioniamo sul giorno corrente
    this.cal.Today  
    this.cal.PrimeTimeStart=g_OraLavIni*60
    this.cal.PrimeTimeEnd=g_OraLavFin*60
    this.cal.TopTime=g_OraLavIni*60
    this.cal.SetTopTime=g_OraLavIni*60
    this.cal.PrintTimeStart=g_OraStaIni*60
    this.cal.PrintTimeEnd =g_OraStaFin*60
    * Propriet� settate dall'utente
    IF !EMPTY(this.MonthNames)
       this.cal.MonthNames=this.MonthNames
    ENDIF 
    IF !EMPTY(this.DayNames)
       this.cal.DayNames=this.DayNames
    ENDIF 
    this.cal.IncludeAmPm=this.IncludeAmPm
    this.cal.AllDayHeight=this.AllDayHeight
    IF this.AllDayHeight=0
       this.cal.DisplayAllDay=.F.
    ELSE 
       this.cal.DisplayAllDay=.T.
       this.cal.MaxAllDayApps=100
    ENDIF 
    this.cal.DisplayColumns=this.DisplayColumns
    this.cal.TimeTextWidth=40
    this.cal.VirtualColumns=this.VirtualColumns
    this.cal.HeaderLineBreak=this.HeaderLineBreak
    this.cal.Visible=this.Visible
    this.cal.DateFormat=this.DateFormat
    this.cal.CustomMenu=IIF(VarType(g_NoRightClick)='L',!g_NoRightClick,.t.)
  
  PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
    IF !Vis
       this.ClearCal() 
    ENDIF 
       
  proc ClearCal
    this.cal.ClearAppointments()
*!*	    IF this.cal.appointments > 0
*!*	       this.cal.ClearAppointments()
*!*	    ENDIF 
       
  proc SetDate(dDate)
    * Setta la data nel calendario giornaliero
    this.ClearCal()
    this.cal.date=VAL(SYS(11,dDate))-this.nBaseCal
    
    
  proc SetMultiDate(dDate)
    * Nel multigiornaliero, setta la data al primo luned� antecedente.
    this.ClearCal()
    this.cal.date=VAL(SYS(11,dDate-DOW(dDate,2)+1))-this.nBaseCal
    * Selezioniamo la colonna del giorno
    this.cal.Column=DOW(dDate,2)+1

  PROCEDURE SetColours
    PARAMETERS ColPrime, ColSelect, ColHeader, ColNonPrime
    this.AllDayBackColor=ColNonPrime
    this.NonPrimeColor=ColNonPrime
    this.PrimeTimeColor=ColPrime
    this.SelectedBackColor=ColSelect
    this.HeaderBackColor=ColHeader
    this.cal.AllDayBackColor=ColNonPrime
    this.cal.NonPrimeColor=ColNonPrime
    this.cal.PrimeTimeColor=ColPrime
    this.cal.SelectedBackColor=ColSelect
    this.cal.HeaderBackColor=ColHeader
    
  PROCEDURE SetPrimeTime
    PARAMETERS PTStart, PTEnd
    this.cal.PrimeTimeStart=PTStart
    this.cal.PrimeTimeEnd=PTEnd
    this.cal.TopTime=PTStart
    this.cal.SetTopTime=PTStart
    
  proc Resize()
    this.cal.width=this.width
    this.cal.height=this.height
    
  proc Calculate(xValue)

  proc Event(cEvent)
  
  PROCEDURE AppointLockTimes
    PARAMETERS Idx
    this.cal.AppointLockTimes(Idx) = .T.
    
  proc cal.AppSelect(nindex)
  	IF this.parent.enabled AND nindex>0 AND !EMPTY(this.parent.Serial) AND !EMPTY( this.AppointKeyID(nindex) )
  	   * Notifichiamo che � stato selezionato un appuntamento solo se nel frattempo 
  	   * � stato modificato il seriale della selezione
  	   LOCAL Ser
  	   Ser = this.parent.Serial 
  	   * IF NOT this.parent.parent.parent.parent.parent.&Ser == this.AppointKeyID(nindex)
  	   this.parent.parent.parent.parent.parent.&Ser = this.AppointKeyID(nindex)
  	   cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
  	   this.parent.parent.parent.parent.parent.NotifyEvent('Selezionato')
  	   * ENDIF 
  	ENDIF 
  	
  PROCEDURE cal.TextChange 
    PARAMETERS nindex, nkey
    * E' stata cambiata la descrizione
    IF this.parent.enabled AND !EMPTY(this.parent.Serial) AND !EMPTY(this.Parent.VarTxt)
  	   * Notifichiamo al padre 
  	   LOCAL Ser, Txt 
  	   Ser = this.parent.Serial 
  	   Txt = this.Parent.VarTxt
  	   this.parent.parent.parent.parent.parent.&Ser = this.AppointKeyID(nindex)
  	   this.parent.parent.parent.parent.parent.&Txt = this.AppointText(nindex)
  	   cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
  	   this.parent.parent.parent.parent.parent.NotifyEvent('TestoModificato')
  	ENDIF 
  ENDPROC 
  
  PROCEDURE cal.AddAppointment
    LPARAMETERS nindex, nstart, nend, ldate, bkeyapp
    IF this.parent.enabled AND VAL(this.AppointKeyID(nindex))=0  AND !ASC(ALLTRIM(this.AppointText(nindex)))=27 AND !EMPTY(ALLTRIM(this.AppointText(nindex))) AND !EMPTY(this.parent.Serial) AND !EMPTY(this.Parent.VarTxt)
  	   * Stiamo caricando un appuntamento da calendario
  	   * Notifichiamo al padre 
  	   LOCAL Ser, Txt
  	   Ser = this.parent.Serial 
  	   Txt = this.Parent.VarTxt
  	   this.parent.parent.parent.parent.parent.&Ser = ''
  	   this.parent.parent.parent.parent.parent.&Txt = this.AppointText(nindex)
  	   IF !EMPTY(this.Parent.VarStart) AND !EMPTY(this.Parent.VarEnd) AND !EMPTY(this.Parent.VarDate)
  	      LOCAL vStart, vEnd, vDt
  	      vStart = this.Parent.VarStart
  	      vEnd = this.Parent.VarEnd
  	      vDt = this.Parent.VarDate
  	      this.parent.parent.parent.parent.parent.&vStart = nstart
  	      this.parent.parent.parent.parent.parent.&vEnd = nend
  	      this.parent.parent.parent.parent.parent.&vDt = CTOD(SYS(10,lDate+this.parent.nBaseCal))
  	   ENDIF 
  	   IF !EMPTY(this.Parent.VarClmn)
  	      LOCAL VClmn
  	      VClmn = this.Parent.VarClmn
  	      this.parent.parent.parent.parent.parent.&VClmn = this.FindColumnCode( this.SelectStartColumn )
  	   ENDIF 
  	   cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
  	   this.parent.parent.parent.parent.parent.NotifyEvent('AggiungiApp')
  	   * Adesso � avvalorato il seriale
       LOCAL TestMacto
  	   TestMacto=this.parent.parent.parent.parent.parent.&Ser
  	   IF !EMPTY(TestMacto)
  	      this.AppointKeyID(nindex) = this.parent.parent.parent.parent.parent.&Ser
  	   ELSE 
  	      * Non � andato a buon fine il caricamento: cancelliamo la selezione
*!*	  	      IF this.appointments > 1
*!*	  	         * Posso cancellare tranquillamente perch� non avr� problemi
*!*	  	         this.DeleteAppointment(nindex)
*!*	  	         this.AppointSelect = 0
*!*	  	      ELSE
  	         * Non posso cancellare l'unico appuntamento esistente...
  	         this.AppointText(nindex) = ''
  	         this.AppointReadOnly(nindex) = .T. 
  	         this.AppointTimeEnd(nindex) = this.AppointTimeStart(nindex)
  	         this.AppointSelect = 0
  	         this.AppointKeyID(nindex) = ""
  	         this.AppUnSelect(nindex)
*!*	  	      ENDIF
  	   ENDIF
  	ENDIF 	
  ENDPROC 
  
  PROCEDURE cal.AppChanged 
    LPARAMETERS nindex, ckeyid, nstart, nend, ncolumn
    * Sono stati cambiati ora/durata di un appuntamento
    IF this.parent.enabled AND !EMPTY(this.parent.Serial) AND !EMPTY(this.Parent.VarStart) AND !EMPTY(this.Parent.VarEnd) AND !EMPTY(this.Parent.VarDate)
  	   * Notifichiamo al padre 
  	   LOCAL Ser, vStart, vEnd, vDt
  	   Ser = this.parent.Serial 
  	   vStart = this.Parent.VarStart
  	   vEnd = this.Parent.VarEnd
  	   vDt = this.Parent.VarDate
  	   this.parent.parent.parent.parent.parent.&vStart = nstart
  	   this.parent.parent.parent.parent.parent.&vEnd = nend
  	   this.parent.parent.parent.parent.parent.&vDt = CTOD(SYS(10,this.FindColumnDate(ncolumn)+this.parent.nBaseCal))
  	   this.parent.parent.parent.parent.parent.&Ser = this.AppointKeyID(nindex)
  	   cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
  	   this.parent.parent.parent.parent.parent.NotifyEvent('OraModificata')
  	ENDIF 	
  ENDPROC 
  
  PROCEDURE AddKeyAppointment
    PARAMETERS nstart, nend, ldate, strtext, strkey, LockTimes, BarCol, TxtCol
    Private Idx
    Idx = this.cal.AddKeyAppointment(nstart, nend, ldate, strtext, strkey)
    IF (Idx # -1) AND LockTimes 
       this.cal.AppointLockTimes(Idx)
    ENDIF 
    this.cal.AppointForeColor(Idx)=TxtCol
    this.cal.TaskBarColor(Idx)=BarCol
    RETURN Idx 
  
  proc cal.DblClick()
  	IF this.parent.enabled AND !EMPTY(this.parent.Serial )
  	   LOCAL Ser
  	   Ser = this.parent.Serial 
  	   IF this.Appointselect > 0
  	      * Un appuntamento selezionato
  	      this.parent.parent.parent.parent.parent.&Ser = this.AppointKeyID(this.Appointselect)
  	      cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
  	      this.Parent.CalTimer.MaskToAgg=this.parent.parent.parent.parent.parent
  	      * Impostiamo come intervallo 1 millisecondo, il timer notificher� l'evento 'Modifica'
  	      this.Parent.CalTimer.interval=1
  	   ELSE 
  	      * Nessun appuntamento selezionato
  	      this.parent.parent.parent.parent.parent.&Ser = ''
  	      IF !EMPTY(this.Parent.VarClmn)
  	         LOCAL VClmn
  	         VClmn = this.Parent.VarClmn
  	         this.parent.parent.parent.parent.parent.&VClmn = this.FindColumnCode( this.SelectStartColumn )
  	      ENDIF
  	      IF !EMPTY(this.Parent.VarStart) AND !EMPTY(this.Parent.VarEnd) AND !EMPTY(this.Parent.VarDate)
  	         LOCAL vStart, vEnd, vDt
  	         vStart = this.Parent.VarStart
  	         vEnd = this.Parent.VarEnd
  	         vDt = this.Parent.VarDate
  	         this.parent.parent.parent.parent.parent.&vStart = this.selectStartTime
  	         this.parent.parent.parent.parent.parent.&vEnd = this.selectEndTime
  	         this.parent.parent.parent.parent.parent.&vDt = CTOD(SYS(10,THIS.FindColumnDate(THIS.SelectColumn())+this.parent.nBaseCal))
  	         cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
  	         this.parent.parent.parent.parent.parent.NotifyEvent('Carica')
  	      ENDIF 
  	   ENDIF 
  	ENDIF 
    
  PROCEDURE PrintTitles
    PARAMETERS Tit, SubT
    this.cal.PrintTitle=Tit
    this.cal.PrintSubTitle=SubT
  ENDPROC
  
  PROCEDURE PrintSchedule
    this.cal.PrintSchedule()
  ENDPROC
  
  PROCEDURE SetAppCode
     PARAMETERS Appt, Cde
     this.cal.AppointCode(Appt)=Cde
  ENDPROC 
  
  PROCEDURE GetAppCode
     PARAMETERS Appt
     RETURN this.cal.AppointCode(Appt)
  ENDPROC 
  
  PROCEDURE ClearHeaders
     this.cal.ClearHeaders
  ENDPROC 
  
  PROCEDURE AddHeader
     PARAMETERS nCode, cText 
     RETURN this.cal.AddHeader(nCode, cText)
  ENDPROC
  
  PROCEDURE AppointHorzLock
     PARAMETERS nCode
     this.cal.AppointHorzLock(nCode) = .T. 
  ENDPROC 
  
  PROCEDURE SetColAppType
     PARAMETERS Typ
     * 0=sort by DATE, 1=sort by COLUMN
     this.cal.ColumnAppType=Typ
  ENDPROC 
  
  PROCEDURE GetClmnApps
     PARAMETERS Cln
     RETURN this.cal.ColumnApps(Cln)
  ENDPROC 
  
  PROCEDURE SetSortWithKey
     this.cal.SortWithKey=.T.
  ENDPROC 
  
  PROCEDURE DelAppoint
     PARAMETERS nCode
     this.cal.DeleteAppointment(nCode)
  ENDPROC 
  
  PROCEDURE GetApptNumb
     RETURN this.cal.appointments
  ENDPROC 
  
  PROCEDURE DeleteCodeHeader
     PARAMETERS Cln
     this.cal.DeleteCodeHeader(Cln)
  ENDPROC 
  
  PROCEDURE GetVirtualClmn
     RETURN this.cal.VirtualColumns
  ENDPROC
  
  PROCEDURE FindColCode
     PARAMETERS Cln
     RETURN this.cal.FindColumnCode(Cln)
  ENDPROC 
  
  PROCEDURE SetAppBackColor
    PARAMETERS ncode, nColor
    this.cal.AppointBackColor(nCode)=nColor
  ENDPROC 
  
  PROCEDURE cal.DisplayMenu
    this.parent.parent.parent.parent.parent.NotifyEvent('RightClick_CtMDay')
  ENDPROC
  
  PROCEDURE cal.GotFocus
    this.parent.parent.parent.parent.parent.NotifyEvent('CtMDay GotFocus')
  ENDPROC

  PROCEDURE cal.LostFocus
    this.parent.parent.parent.parent.parent.NotifyEvent('CtMDay LostFocus')
  ENDPROC
  
ENDDEFINE

define class cp_ctCALENDAR as container
  cEvent=''
  var=''
  nBaseCal=0
  enabled=.t.
  MonthNames=''
  DayNames=''
  Visible=.F.
  FontName=''
  FontSize=0
  VarDate=''
  
  add object cal as DBICalendar
  
  proc Init()
    this.cal.left=0
    this.cal.top=0
    this.cal.width=this.width
    this.cal.height=this.height
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.DateFont.Name = this.FontName
    this.cal.DateFont.Size = this.FontSize
    this.cal.HeaderFont.Name = this.FontName
    this.cal.HeaderFont.Size = this.FontSize
    this.cal.PrintFont.Name = this.FontName
    this.cal.PrintFont.Size = this.FontSize
    this.cal.PrintTextFont.Name = this.FontName
    this.cal.PrintTextFont.Size = this.FontSize
    this.cal.PrintTitleFont.Name = this.FontName
    this.cal.PrintTitleFont.Size = this.FontSize
    this.cal.TextFont.Name = this.FontName
    this.cal.TextFont.Size = this.FontSize
    this.cal.TipsFont.Name = this.FontName
    this.cal.TipsFont.Size = this.FontSize
    this.cal.visible=.F.
    *
    this.cal.TextWrap=.t.
    * General
    this.cal.MonthNames=g_Mese[1]+","+g_Mese[2]+","+g_Mese[3]+","+g_Mese[4]+","+g_Mese[5]+","+g_Mese[6]+","+g_Mese[7]+","+g_Mese[8]+","+g_Mese[9]+","+g_Mese[10]+","+g_Mese[11]+","+g_Mese[12]
    this.cal.DayNames=g_Giorno[1]+","+g_giorno[2]+","+g_giorno[3]+","+g_giorno[4]+","+g_giorno[5]+","+g_giorno[6]+","+g_giorno[7]
    this.cal.ViewType=1
    this.cal.YearDisplay=0
    this.cal.BorderType=3    
    this.cal.BorderStyle=0
    this.cal.BackFillType=0
    * Date
    this.cal.DateHeight=60
    this.cal.DateAlign=1
    this.cal.DatePosition=0
    this.cal.DateBorder=2
    this.cal.DateFormat='d n M Y'
    this.cal.DateOffset=0
    * Ci posizioniamo sul giorno corrente
    this.cal.Today 
    this.cal.DisplayHeader=.t.
    this.cal.HeaderBorder=1
    this.cal.HeaderAlign=2
    this.cal.HeaderFillType=2
    this.cal.SelectType=0
    this.cal.SelectOffset=0
    this.cal.TextAlign=0
    this.cal.TextPosition=0
    * Color
    this.cal.BackColor=g_ColPrime
    this.cal.HeaderBackColor=g_ColHeader
    this.cal.SelectBackColor=g_ColSelect
    this.cal.SelectForeColor=0
    this.cal.WeekEndColor=g_ColNnPrm
    * 
    this.nBaseCal=VAL(SYS( 11 , cp_CharToDate('01-01-1900', 'dd-mm-yyyy') ))
    * Propriet� settate dall'utente
    IF !EMPTY(this.MonthNames)
       this.cal.MonthNames=this.MonthNames
    ENDIF 
    IF !EMPTY(this.DayNames)
       this.cal.DayNames=this.DayNames
    ENDIF 
             
*!*	  proc cal.UIEnable(bEnable)
*!*	    this.visible=bEnable

  proc Calculate(xValue)
    
  proc Event(cEvent)
  
  proc ClearCal
    this.cal.ClearDays()
  
  PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
    IF ! Vis
       this.ClearCal() 
    ENDIF 
    
  proc SetDate(dDate)
    this.ClearCal()
    this.cal.date=VAL(SYS(11,dDate))-this.nBaseCal
    this.cal.datestart=this.StartWeek(dDate)
    this.cal.dateend=this.EndWeek(dDate)
    this.cal.printdatestart=this.cal.datestart
    this.cal.printdateend=this.cal.dateend
  
  proc SetText(dDate,cText)
  	LOCAL nDate
    nDate=VAL(SYS(11,dDate))-this.nBaseCal
    this.cal.DateText(nDate)=AllTrim(cText)
    this.cal.DateText2(nDate)=ALLTrim(cText)
  
  PROCEDURE SetColours
    PARAMETERS ColPrime, ColSelect, ColHeader, ColNonPrime
    * Color
    this.cal.BackColor=ColPrime
    this.cal.SelectBackColor=ColSelect
    this.cal.HeaderBackColor=ColHeader
    this.cal.WeekEndColor=ColNonPrime
  
  proc Resize()
    this.cal.width=this.width
    this.cal.height=this.height
  
  func StartWeek(dDate)
    local nDate
    dDate=dDate-DOW(dDate,2)+1
    nDate=VAL(SYS(11,dDate))-this.nBaseCal
    RETURN nDate

  func EndWeek(dDate)
    local nDate
    dDate=dDate+7-DOW(dDate,2)
    nDate=VAL(SYS(11,dDate))-this.nBaseCal
    RETURN nDate
       
  func DayNumber2DayJulian(nDate)   
    RETURN (CTOD(SYS(10,nDate+this.nBaseCal)))

  func DayJulian2DayNumber(dDate)   
    RETURN (VAL(SYS(11,dDate))-this.nBaseCal)
  
  proc cal.DateClick()
  	PARAMETERS nDate
    LOCAL dNewDate,n,old
    * check data
    dNewDate = CTOD(SYS(10,ndate+this.parent.nBaseCal))
    * assegna variabile
    IF not(empty(this.parent.var)) and this.parent.enabled
      n=this.parent.var
      old = this.parent.parent.parent.parent.parent.&n
      IF old # dNewDate 
        * la data � cambiata
        this.parent.parent.parent.parent.parent.&n = dNewDate
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
        * Notifichiamo di aggiornare la variabile ma non il calendario
        this.parent.parent.parent.parent.parent.NotifyEvent('ChangeNoUpdate')
      ENDIF 
    
      IF !EMPTY(this.Parent.VarDate)
        LOCAL vDt
        vDt = this.Parent.VarDate
        this.parent.parent.parent.parent.parent.&vDt = dNewDate 
      ENDIF 
      cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
      IF !EMPTY(this.Parent.VarDate)
        this.parent.parent.parent.parent.parent.NotifyEvent('GiornoSelezionato')
      ENDIF 
    ENDIF 

  proc cal.DateDblClick()
  	PARAMETERS nDate
  	LOCAL dNewDate,n,old
    * check data
    dNewDate = CTOD(SYS(10,this.Date+this.parent.nBaseCal))
    * assegna variabile    
    IF not(empty(this.parent.var)) and this.parent.enabled
      n=this.parent.var
      old = this.parent.parent.parent.parent.parent.&n
      IF old # dNewDate 
        * la data � cambiata
        this.parent.parent.parent.parent.parent.&n = dNewDate
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
        * Notifichiamo di aggiornare la variabile ma non il calendario
        this.parent.parent.parent.parent.parent.NotifyEvent('ChangeNoUpdate')
      ENDIF 
    
      IF !EMPTY(this.Parent.VarDate)
        LOCAL vDt
        vDt = this.Parent.VarDate
        this.parent.parent.parent.parent.parent.&vDt = dNewDate 
      ENDIF 
      cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
      IF !EMPTY(this.Parent.VarDate)
        this.parent.parent.parent.parent.parent.NotifyEvent('AttivaGiornaliero')
      ENDIF 
    ENDIF 
  
  PROCEDURE PrintTitles
    PARAMETERS Tit, SubT
    this.cal.PrintTitle=Tit
    this.cal.PrintSubTitle=SubT
  ENDPROC
  
  PROCEDURE PrintCalendar
    this.cal.PrintCalendar()
  ENDPROC
ENDDEFINE

define CLASS cp_ctMonth as container
  cEvent=''
  var=''
  nBaseCal=0
  * Propriet� disponibili nell'oggettino
  DateAlign=0
  DateBorder=0
  DatePosition=0
  FirstDay=1
  FillDates=.T.
  HeaderBorder=0
  Visible=.F.
  MonthNames=""
  DayHeader=""
  FontName=''
  FontSize=0
  varDate=''

  add object cal as DBIMonth

  proc Init()
    this.cal.left=0
    this.cal.top=0
    this.cal.width=this.width
    this.cal.height=this.height
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.Font.Name = this.FontName
    this.cal.Font.Size = this.FontSize
    this.cal.PrintFont.Name = this.FontName
    this.cal.PrintFont.Size = this.FontSize
    this.cal.PrintNumberFont.Name = this.FontName
    this.cal.PrintNumberFont.Size = this.FontSize
    this.cal.PrintTextFont.Name = this.FontName
    this.cal.PrintTextFont.Size = this.FontSize
    this.cal.PrintTitleFont.Name = this.FontName
    this.cal.PrintTitleFont.Size = this.FontSize
    this.cal.TitleFont.Name = this.FontName
    this.cal.TitleFont.Size = this.FontSize
    this.cal.visible=.f.
    this.cal.BackFillType=0
    this.cal.BorderType=0
    this.cal.ButtonType=0   
    this.cal.BorderType=0
    this.cal.HeaderBorder=0
    this.cal.HeaderFillType=2
    this.cal.DateOffset=0
    this.cal.MonthNames=g_Mese[1]+","+g_Mese[2]+","+g_Mese[3]+","+g_Mese[4]+","+g_Mese[5]+","+g_Mese[6]+","+g_Mese[7]+","+g_Mese[8]+","+g_Mese[9]+","+g_Mese[10]+","+g_Mese[11]+","+g_Mese[12]
    this.cal.DayHeader=g_Giorno[1]+","+g_giorno[2]+","+g_giorno[3]+","+g_giorno[4]+","+g_giorno[5]+","+g_giorno[6]+","+g_giorno[7]
    this.cal.DATEALIGN=this.DateAlign
    this.cal.DATEBORDER=this.DateBorder
    this.cal.DATEPOSITION=this.DatePosition
    this.cal.TitleSize=1 && Small
    * Color 
    this.cal.BackColor = g_ColPrime
    this.cal.FillColor = g_ColSelect
    this.cal.SelectBackColor = g_ColSelect
    this.cal.HeaderBackColor = g_ColHeader
    this.cal.TitleBackColor = g_ColHeader
    this.cal.BorderColor = g_ColHeader
    this.cal.WeekEnds = 1
    this.cal.WeekEndColor = g_ColNnPrm
    this.cal.TextColor = 0
    this.nBaseCal=VAL(SYS( 11 , cp_CharToDate('01-01-1900', 'dd-mm-yyyy') ))
    * Propriet� settate dall'utente nell'oggettino
    IF !EMPTY(this.MonthNames)
       this.cal.MonthNames=this.MonthNames
    ENDIF 
    IF !EMPTY(this.DayHeader)
       this.cal.DayHeader=this.DayHeader
    ENDIF 
    this.cal.FirstDay=this.FirstDay
    this.cal.FillDates=this.FillDates
    this.cal.MultiText = .F.
    this.cal.TextWrap = .T.
    this.cal.TextAlign = 0
    this.cal.TextPosition = 0
    this.cal.TextTop = this.FontSize + 1
    this.cal.HeaderBorder=this.HeaderBorder
    this.cal.Visible=this.Visible

  proc Calculate(xValue)

  proc Event(cEvent)

  PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
    IF !Vis
       this.ClearCal() 
    ENDIF 
    
  proc ClearCal()
    this.cal.ClearDays()
       
  proc SetDate(dDate)
    this.ClearCal()  
    this.cal.date=VAL(SYS(11,dDate))-this.nBaseCal

  PROCEDURE SetColours
    PARAMETERS ColPrime, ColSelect, ColHeader, ColNonPrime
    this.cal.NonPrimeColor = ColNonPrime
    this.cal.PrimeTimeColor = ColPrime
    this.cal.AppBarColor = ColPrime
    this.cal.SelectedColor = ColPrime
    this.cal.HeaderBackColor = ColHeader
    
  proc Resize()
    this.cal.width=this.width
    this.cal.height=this.height
  	        
  PROCEDURE DateText 
     PARAMETERS Giorno, Testo
     PRIVATE OldTxt
     OldTxt = this.Cal.DateText(Giorno)
     IF !EMPTY(OldTxt)
        * Testo = OldTxt+CHR(13)+CHR(10)+Testo
        Testo = OldTxt+' - '+Testo
     ENDIF 
     this.cal.DateText(giorno)=Testo
     
  PROCEDURE cal.Click
  	PARAMETERS nDate
    LOCAL dNewDate,n,old
    * check data
    dNewDate = CTOD(SYS(10,this.Date+this.parent.nBaseCal))
    * assegna variabile
    IF not(empty(this.parent.var)) and this.parent.enabled
      n=this.parent.var
      old = this.parent.parent.parent.parent.parent.&n
      IF old # dNewDate 
        * la data � cambiata
        this.parent.parent.parent.parent.parent.&n = dNewDate
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
        * Notifichiamo di aggiornare la variabile ma non il calendario
        this.parent.parent.parent.parent.parent.NotifyEvent('ChangeNoUpdate')
      ENDIF 
    
      IF !EMPTY(this.Parent.VarDate)
        LOCAL vDt
        vDt = this.Parent.VarDate
        this.parent.parent.parent.parent.parent.&vDt = dNewDate 
      ENDIF 
      cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
      IF !EMPTY(this.Parent.VarDate)
        this.parent.parent.parent.parent.parent.NotifyEvent('GiornoSelezionato')
      ENDIF 
    ENDIF 
  
  proc cal.DblClick()
  	LOCAL dNewDate,n,old
    * check data
    dNewDate = CTOD(SYS(10,this.Date+this.parent.nBaseCal))
    * assegna variabile    
    IF not(empty(this.parent.var)) and this.parent.enabled
      n=this.parent.var
      old = this.parent.parent.parent.parent.parent.&n
      IF old # dNewDate 
        * la data � cambiata
        this.parent.parent.parent.parent.parent.&n = dNewDate
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
        * Notifichiamo di aggiornare la variabile ma non il calendario
        this.parent.parent.parent.parent.parent.NotifyEvent('ChangeNoUpdate')
      ENDIF 
    
      IF !EMPTY(this.Parent.VarDate)
        LOCAL vDt
        vDt = this.Parent.VarDate
        this.parent.parent.parent.parent.parent.&vDt = dNewDate 
      ENDIF 
      cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
      IF !EMPTY(this.Parent.VarDate)
        this.parent.parent.parent.parent.parent.NotifyEvent('AttivaGiornaliero')
      ENDIF 
    ENDIF 
         
  PROCEDURE Refresh
     this.cal.Refresh 
     
  PROCEDURE PrintTitles
    PARAMETERS Tit, SubT
    this.cal.PrintTitle=Tit
    this.cal.PrintSubTitle=SubT
  ENDPROC
  
  PROCEDURE PrintCalendar
    this.cal.PrintCalendar()
  ENDPROC
ENDDEFINE

define class cp_ctYEAR as container
  cEvent=''
  var=''
  nBaseCal=0
  Visible=.F.
  DisplayType=0
  MonthNames=""
  DayHeader="" 
  FontName=''
  FontSize=0
  ReverseDisplay=.F.
  varDate=''
  
  add object cal as DBIYear
  
  proc Init()
    this.cal.left=0
    this.cal.top=0
    this.cal.width=this.width
    this.cal.height=this.height
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.Font.Name = this.FontName
    this.cal.Font.Size = this.FontSize
    this.cal.DayFont1.Name = this.FontName
    this.cal.DayFont1.Size = this.FontSize
    this.cal.DayFont1.Bold = .T.
    this.cal.DayFont2.Name = this.FontName
    this.cal.DayFont2.Size = this.FontSize
    this.cal.PrintFont.Name = this.FontName
    this.cal.PrintFont.Size = this.FontSize
    this.cal.TitleFont.Name = this.FontName
    this.cal.TitleFont.Size = this.FontSize
    this.cal.visible=.F.
    this.cal.MonthNames=g_Mese[1]+","+g_Mese[2]+","+g_Mese[3]+","+g_Mese[4]+","+g_Mese[5]+","+g_Mese[6]+","+g_Mese[7]+","+g_Mese[8]+","+g_Mese[9]+","+g_Mese[10]+","+g_Mese[11]+","+g_Mese[12]
    this.cal.DayHeader=ah_MsgFormat("D,L,M,M,G,V,S")
    this.cal.SelectType=1
    this.cal.FirstDay=1
    this.cal.DateBorder=1
    this.cal.TitleAlign=2
    this.cal.LevelDepth=2
    this.cal.LevelBorder=0
    this.cal.LevelOffset=3
    * Appearance
    this.cal.DateXSize=0
    this.cal.DateYSize=0
    this.cal.DateOffset=0
    this.cal.DisplayInset=.T.
    this.cal.BorderType=1
    this.cal.InsetBorder=1
    this.cal.InsetWidth=1
    this.cal.ReverseDisplay=.t.
    this.cal.MultiSelect=.f.
    this.cal.Enabled=.t.
    this.nBaseCal=VAL(SYS( 11 , cp_CharToDate('01-01-1900', 'dd-mm-yyyy') ))
    * Init
    * this.SetDate(i_DATSYS)
    * Color
    this.cal.BackColor = g_ColPrime
    this.cal.BorderColor = g_ColHeader
    this.cal.InsetBackColor = g_ColHeader
    this.cal.SelectBackColor = g_ColSelect
    this.cal.LevelColor = g_ColHeader
    this.cal.WeekEndColor = g_ColPrime
    * Propriet� settate dall'utente
    IF !EMPTY(this.MonthNames)
       this.cal.MonthNames=this.MonthNames
    ENDIF 
    IF !EMPTY(this.DayHeader)
       this.cal.DayHeader=this.DayHeader
    ENDIF 
    this.cal.DisplayType=this.DisplayType
    this.cal.ReverseDisplay=this.ReverseDisplay
    this.cal.visible=this.visible

  proc Calculate(xValue)
    
  proc Event(cEvent)
  
  proc SetDate(dDate)
    this.cal.date=VAL(SYS(11,dDate))-this.nBaseCal
    IF this.cal.DisplayType=0 && Abbiamo 12 mesi
       this.cal.StartMonth=1
       this.cal.StartYear=YEAR(dDate)
    ELSE 
       this.cal.StartMonth=IIF(MONTH(dDate)=1,12,MONTH(dDate)-1)
       this.cal.StartYear=YEAR(dDate)-IIF(MONTH(dDate)=1,1,0)  
    ENDIF 
      
  PROCEDURE SetColours
    PARAMETERS ColPrime, ColSelect, ColHeader, ColNonPrime
    this.cal.BackColor = ColPrime
    this.cal.SelectBackColor = ColSelect
    this.cal.LevelColor = ColHeader
    this.cal.WeekEndColor = ColNonPrime
    
  proc cal.Click()
  	LOCAL dNewDate,n,d,o, old
    * check data
    dNewDate = CTOD(SYS(10,this.Date+this.parent.nBaseCal))
    * assegna variabile
    IF not(empty(this.parent.var)) and this.parent.enabled
      n=this.parent.var
      old = this.parent.parent.parent.parent.parent.&n
      IF old # dNewDate 
        * la data � cambiata
        this.parent.parent.parent.parent.parent.&n = dNewDate
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
        * Notifichiamo di aggiornare la variabile ma non il calendario
        this.parent.parent.parent.parent.parent.NotifyEvent('ChangeNoUpdate')
      ENDIF 
      IF !EMPTY(this.Parent.VarDate)
        LOCAL vDt
        vDt = this.Parent.VarDate
        this.parent.parent.parent.parent.parent.&vDt = dNewDate 
      ENDIF 
      cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
      IF !EMPTY(this.Parent.VarDate)
        this.parent.parent.parent.parent.parent.NotifyEvent('GiornoSelezionato')
      ENDIF 
    ENDIF 
  
  proc cal.DblClick()
  	LOCAL dNewDate,n,d,o, old
    * check data
    dNewDate = CTOD(SYS(10,this.Date+this.parent.nBaseCal))
    * assegna variabile    
    IF not(empty(this.parent.var)) and this.parent.enabled
      n=this.parent.var
      old = this.parent.parent.parent.parent.parent.&n
      IF old # dNewDate 
        * Aggiorniamo solo se la data � cambiata
        this.parent.parent.parent.parent.parent.&n = dNewDate
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
      ENDIF 
      this.parent.parent.parent.parent.parent.NotifyEvent('AttivaGiornaliero')
    ENDIF 
   
  PROCEDURE DateFont
    PARAMETERS nMonth, nDay
    this.cal.DateFont(nMonth, nDay)=1 
  
  PROCEDURE AppointmentColor
    PARAMETERS idx, Colr
    this.cal.AppointBorder(idx) = 0
    this.Cal.AppointBackColor(idx) = Colr
    
  proc Resize()
    this.cal.width=this.width
    this.cal.height=this.height

  proc ClearCal
    this.cal.ClearDays()

  PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
    IF !Vis
       this.ClearCal() 
    ENDIF 

  PROCEDURE PrintTitles
    PARAMETERS Tit, SubT
    this.cal.PrintTitle=Tit
    this.cal.PrintSubTitle=SubT
  ENDPROC
  
  PROCEDURE PrintCalendar
    this.cal.PrintCalendar()
  ENDPROC
              
ENDDEFINE

define CLASS cp_ctDate as container
  cEvent=''
  var=''
  nBaseCal=0
  MonthNames=""
  DayHeader=""
  TodayText=""
  FontName=''
  FontSize=0
  DualMonths=.F.
  
  add object cal as DBIDate

  proc Init()  
    this.cal.left=0
    this.cal.top=0
    this.cal.width=this.width
    this.cal.height=this.height
    this.cal.visible=.t.
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.Font.Name = this.FontName
    this.cal.Font.Size = this.FontSize
    this.cal.DayFont1.Name = this.FontName
    this.cal.DayFont1.Size = this.FontSize
    this.cal.DayFont1.Bold = .T.
    this.cal.DayFont2.Name = this.FontName
    this.cal.DayFont2.Size = this.FontSize
    this.cal.WeekFont.Name = this.FontName
    this.cal.WeekFont.Size = this.FontSize
    this.cal.PrintHighFont.Name = this.FontName
    this.cal.PrintHighFont.Size = this.FontSize
    this.cal.PrintFont.Name = this.FontName
    this.cal.PrintFont.Size = this.FontSize
    this.cal.PrintTitleFont.Name = this.FontName
    this.cal.PrintTitleFont.Size = this.FontSize
    this.cal.TitleFont.Name = this.FontName
    this.cal.TitleFont.Size = this.FontSize
    this.cal.TitleFont.Bold=.T. 
    this.cal.WeekFont.Name = this.FontName
    this.cal.WeekFont.Size = this.FontSize
    * Color
    this.cal.BackColor = g_ColPrime
    this.cal.BorderColor = g_ColPrime
    this.cal.FillColor = g_ColNnPrm
    this.cal.FillDates = .T. 
    * this.cal.HeaderColor = g_ColHeader
    this.cal.HeaderLineColor = g_ColHeader
    this.cal.LevelColor = g_ColHeader
    this.cal.SelectBackColor = g_ColSelect
    this.cal.TodayColor = g_ColSelect
    this.cal.WeekendColor = g_ColNnPrm
    this.cal.MonthNames=g_Mese[1]+","+g_Mese[2]+","+g_Mese[3]+","+g_Mese[4]+","+g_Mese[5]+","+g_Mese[6]+","+g_Mese[7]+","+g_Mese[8]+","+g_Mese[9]+","+g_Mese[10]+","+g_Mese[11]+","+g_Mese[12]
    this.cal.DayHeader=ah_MsgFormat("d,l,m,m,g,v,s")
    this.cal.DateFormat=1 && 0 - mm/dd/yy, 1 - dd/mm/yy, 2 - yy/mm/dd
    this.cal.DateOffset=0
    this.cal.DateBorder=1 && 0 - Regular, 1 - None, 2 - Raised, 3 - Lowered
    this.cal.HeaderLine=.T.
    * this.cal.WeekEnds=1
    this.cal.WeekEnds=0
    this.cal.BorderType=1 && 0 - Regular, 1 - None, 2 - Raised, 3 - Lowered, 4 - Raised Thin, 5 - Lowered Thin
    this.cal.ButtonStyle=0
    this.cal.ButtonType=0
    this.cal.DualMonths=this.DualMonths
    this.cal.MonthYOffset=-2
    this.cal.HeaderOffset=1
    this.cal.LevelBorder=1
    this.cal.LevelDepth=1
    this.cal.SelectType=2 && 0 - Regular, 1 - Flat, 2 - Raised, 3 - Lowered, 4 - Round
    * Inizia con il luned�
    this.cal.FirstDay=1
    * color
    this.cal.BackColor = g_ColPrime
    this.nBaseCal=VAL(SYS(11,cp_CharToDate("01-01-1900","dd-mm-yyyy")))
    * init 
    this.cal.TodayFormat=2 && 0 - None, 1 - Mark Date, 2 - Show Text (Mark date and display TodayText string)
    this.cal.TodayText=this.TodayText
*!*	  proc UIEnable(bEnable)
*!*	    this.visible=bEnable

  proc Calculate(xValue)

  proc Event(cEvent)

  proc SetDate(dDate)   
    this.cal.date=VAL(SYS(11,dDate))-this.nBaseCal

  PROCEDURE ClearCal
    this.cal.ClearDays()
      
  PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
    IF ! Vis
       this.ClearCal() 
    ENDIF 
    
  PROCEDURE cal.DateChange(nDow, nDay, nMOnth, nYear)
  	LOCAL dNewDate,n,d,o
  	dNewDate = cp_CharToDate(ALLTRIM(STR(nDay))+'-'+ALLTRIM(STR(nMonth))+'-'+ALLTRIM(STR(nYear)),'dd-mm-yyyy')
  	this.parent.parent.parent.parent.parent.Paint() 
    IF not(empty(this.parent.var)) and this.parent.enabled
      n=this.parent.var
      old = this.parent.parent.parent.parent.parent.&n
      IF old # dNewDate 
        * Notifichiamo l'evento solo se la data � cambiata
        this.parent.parent.parent.parent.parent.&n = dNewDate
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
        this.parent.parent.parent.parent.parent.NotifyEvent('ChangeDate')
      ENDIF 
    ENDIF 
    
   PROCEDURE DateSection
      LOCAL Side
      this.parent.parent.parent.parent.Paint()
      * Ritorna la sezione del control dove risiede la data corrente
      Side = this.cal.DateSection
      RETURN Side 
   ENDPROC 
   
   PROCEDURE IsDualMonths
      LOCAL Dual
      Dual = this.cal.DualMonths
      RETURN Dual
   ENDPROC 
   
   PROCEDURE SetSection
      PARAMETERS Side
      * Setta la sezione
      this.cal.DateSection=Side
   ENDPROC 
   
   PROCEDURE DateFont
     PARAMETERS nDay, nSection
     this.cal.DateFont(nDay, nSection)=1 
     
   proc Resize()
    this.cal.width=this.width
    this.cal.height=this.height
ENDDEFINE

define CLASS cp_ctList as container
  cEvent=''
  serial=''
  nBaseCal=0
  FontName=''
  FontSize=0
  HeadrText=''
  cNodeBmp=''
  cLeafBmp=''
  ItemSel=''
  ColumnSel=''
  HeaderFillType=0
  HeaderBorder=0
  VertGridLines=True
  
  add object cal as DBIList
  add object ImgList as OLEImageList
  
  ADD OBJECT CalTimer AS CalendarTimer
  
  proc Init()  
    this.cal.left=0
    this.cal.top=0
    this.cal.width=this.width
    this.cal.height=this.height
    this.cal.visible=.t.
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.Font.Name=this.FontName
    this.cal.Font.Size=this.FontSize 
    this.cal.HeaderFont.Name = this.FontName
    this.cal.HeaderFont.Size = this.FontSize
    this.cal.PrintTitleFont.Name = this.FontName
    this.cal.PrintTitleFont.Size = this.FontSize
    this.cal.TipsFont.Name = this.FontName
    this.cal.TipsFont.Size = this.FontSize
    this.cal.TitleFont.Name = this.FontName
    this.cal.TitleFont.Size = this.FontSize
    this.cal.SubTextFont= this.cal.Font
    * Color
    this.cal.BackColor = g_ColPrime
    this.cal.BorderColor = g_ColPrime
    this.cal.HeaderBackColor = g_ColHeader
    this.cal.HorzGridColor=g_ColHeader
    this.cal.VertGridColor = g_ColHeader
    this.cal.SelectedBackColor = g_ColSelect 
    this.cal.ShadowColor= g_ColPrime 
    this.cal.SubTextColor=0
    
    this.cal.BorderType = 0
    this.cal.CheckAlign = 2 && Centered
    this.cal.DisplayHeader=.T.
    
    this.cal.HeaderBorder = 0 && 0 - Regular, 1 - None, 2 - Raised, 3 - Lowered, 4 - Raised Thin, 5 - Lowered Thin
    IF this.HeaderBorder>=0 AND this.HeaderBorder<=5
       this.cal.HeaderBorder=this.HeaderBorder
    ENDIF 
    this.cal.HeaderFillType=2 && 0 - Solid, 1 - Horizontal, 2 - Vertical, 3 - Diagonal, 4 - Reverse Diagonal, 5 - Horizontal Bump, 6 - Vertical Bump, 7 - Diamond, 8 - Pyramid, 9 - Circle, 10 - Ellipse
    IF this.HeaderFillType>=0 AND this.HeaderFillType<=10
       this.cal.HeaderFillType=this.HeaderFillType
    ENDIF 
    this.cal.HorzAutoSize = .T. 
    this.cal.HorzGridLines = .T. 
    this.cal.HorzAutoSize = .T.
    this.cal.HeightOffset = 4 
    this.cal.HorzScroll = .F.
    this.cal.ListAlign=0 && 0 - Left Justify, 1 - Right Justify, 2 - Centered
    this.cal.Multiline=.F. 
    this.cal.ScrollOnHThumb = .T.
    this.cal.ScrollOnVThumb = .T.
    this.cal.SelectedStyle = 0 && 0 - All, 1 - Text Only 
    this.cal.SelectOnScroll=.F. 
    this.cal.SortArrows=.T. 
    IF !EMPTY(this.HeadrText)
       this.cal.TitleText=ah_MsgFormat(this.HeadrText)
       this.cal.DisplayTitle=.T.
    ELSE 
       this.cal.DisplayTitle=.F.
    ENDIF 
    this.cal.TitleAlign=2 && 0 - Left Justify, 1 - Right Justify, 2 - Centered
    this.cal.TitleBorder=0 && 0 - Regular, 1 - None, 2 - Raised, 3 - Lowered, 4 - Raised Thin, 5 - Lowered Thin 
    this.cal.TitleTextStyle=0 && 0 - Regular, 1 - Raised, 2 - Lowered
    this.cal.VertGridLines=this.VertGridLines
    * this.cal.PreColumnType = 0
    * Non � specificata la data di partenza
    this.cal.DateOffset=+2
    this.cal.dateFormat=1  && 0 - mm/dd/yy, 1 - dd/mm/yy,  2 - yy/mm/dd
    this.cal.FocusType = 1
    this.cal.IncludeSeconds=.F.
    this.cal.IncludeTime=.F.
    this.cal.LongYear=.T.
    this.cal.SmallIcons=.T. 
    * --  inizializzazione oggetto ImgList 
    this.ImgList.ListImages.Clear
    this.ImgList.Init

  proc Calculate(xValue)

  proc Event(cEvent)
      
  PROCEDURE ClearList
    this.cal.ClearList()

  PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
    IF ! Vis
       this.ClearList() 
    ENDIF 
  
  proc Resize()
    this.cal.width=this.width
    this.cal.height=this.height
    
  PROCEDURE AddItem
    PARAMETERS Txt, Ser, ForCol
    LOCAL Ind
    Ind = this.cal.AddItem(Txt)
    this.cal.ListFont(this.cal.Font, Ind)
    this.cal.ListCargo(Ind)=Ser   
    * this.cal.ListData(Ind)=VAL(Ser)
    IF VARTYPE(ForCol)='N'
       this.cal.ListForeColor(Ind)=ForCol
    ENDIF
    RETURN Ind 
    
  PROCEDURE AddColumn
    PARAMETERS Txt, Wid, TypeCol, Chk, LockCol, SortYN, TxtAlg
    * Testo, profondit�, Tipo (0 - Text, 1 - Integer, 2 - Real, 3 - Date / Time)
    * Check (0 - None, 1 - 2 Dimensional, 2 - 3 Dimensional, 3 - User Defined)
    * LockCol blocca le colonne, SortYN - abilitato o meno il sort, 
    * TxtAlg - Allineamento del testo 0 - Left Justified, 1 - Right Justified, 2 - Centered, 3 - Default
    LOCAL Ind
    Ind = this.cal.addColumn(Txt,Wid)
    IF Ind>0
       this.cal.ColumnDataType(Ind) =TypeCol
       this.cal.ColumnCheckBox(Ind)=Chk
       this.cal.ColumnLock(Ind)=LockCol
       this.cal.ColumnSortable(Ind)=SortYN
       this.cal.ColumnTextAlign(Ind)=TxtAlg
    ENDIF 
    RETURN Ind 
    
  PROCEDURE AddImage
    PARAMETERS i_cBmpName
    LOCAL Ind
    Ind=0 
    * Aggiugniamo l'immagine ad ImgList in modo da poterla utilizzare
    this.ImgList.ListImages.add(,trim(i_cBmpName),LoadPicture(trim(i_cBmpName)))
    IF this.ImgList.ListImages.Count>0
       * Ha aggiunto il BMP
       Ind = this.cal.addImage( this.ImgList.ListImages.Item( this.ImgList.ListImages.Count).Picture )
    ENDIF 
    RETURN Ind 

  PROCEDURE SubTextNodes
    PARAMETERS ShowSub, ClmnStart, ClmnEnd
    this.cal.SubTextNodes = ShowSub
    this.cal.SubColumnStart = ClmnStart
    this.cal.SubColumnEnd = ClmnEnd
    this.cal.SubTextFont = this.cal.Font
    
  PROCEDURE SubText
    PARAMETERS nIndex, cText, ShowNote
    * Prima di impostare il testo ribadiamo il font dato che occasionalmente perde l'impostazione
    this.cal.SubTextFont = this.cal.Font
    * Impostiamo il testo
    this.cal.ListSubText(nIndex) = cText
    * Con il font corretto dovrebbe calcolare l'altezza giusta
    this.cal.ListSubHeight(nIndex) = this.cal.CalcSubTextHeight(nIndex)
    IF this.cal.ListSubHeight(nIndex) < this.cal.Font.size 
       * Non ha calcolato bene l'altezza
       this.cal.SubTextFont = this.cal.Font
       this.cal.ListSubHeight(nIndex) = this.cal.CalcSubTextHeight(nIndex)
    ENDIF 
    * Visualizza il testo solo se l'opzione � attiva
    this.cal.ListSubVisible( nIndex ) = ShowNote

  PROCEDURE VisNote
    PARAMETERS Vis
    * Visualizza o nasconde le note di tutti gli elementi
    LOCAL idx
    FOR idx = 0 TO this.cal.ListCount
       this.cal.ListSubVisible( idx ) = Vis
    ENDFOR     

  PROCEDURE cal.ItemDblClick
    PARAMETERS  nIndex, nColumn
    IF !EMPTY(this.parent.Serial) 
       LOCAL Ser
       Ser = this.parent.Serial 
       this.parent.parent.parent.parent.parent.&Ser = this.ListCargo(nIndex)  
       cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
       this.Parent.CalTimer.MaskToAgg=this.parent.parent.parent.parent.parent
       * Impostiamo come intervallo 1 millisecondo, il timer notificher� l'evento 'Modifica'
       this.Parent.CalTimer.interval=1
  	ENDIF 
   
   PROCEDURE CellCheck
     PARAMETERS Ind, Coln, Vlue
     this.cal.CellCheck(Ind,Coln)=Vlue 
     
   PROCEDURE CheckOn
     PARAMETERS Ind, Coln 
     LOCAL Cargo, Ret 
     IF this.cal.CellCheck(Ind,Coln)=1
        * Check attivo
        Ret = this.cal.ListCargo(Ind) 
     ELSE 
        Ret = ''
     ENDIF 
     RETURN Ret 
     
   PROCEDURE CellPicture
     PARAMETERS Ind, Coln, Vlue
     IF Vlue>0
        this.cal.CellPicture(Ind,Coln)=Vlue 
     ENDIF 
  
  PROCEDURE cal.CheckClick 
     PARAMETERS nIndex, nColumn, nState
     IF nIndex>=0 and !EMPTY(this.parent.Serial)
        LOCAL Ser, Itm, Cln
        Ser = this.parent.Serial 
        this.parent.parent.parent.parent.parent.&Ser = this.ListCargo(nIndex)  
        IF !EMPTY(this.parent.ItemSel) AND !EMPTY(this.parent.ColumnSel)
           Itm = this.parent.ItemSel
           this.parent.parent.parent.parent.parent.&Itm = nIndex  
           Cln = this.parent.ColumnSel
           this.parent.parent.parent.parent.parent.&Cln = nColumn  
        ENDIF 
        cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
        IF nState=0
           this.parent.parent.parent.parent.parent.NotifyEvent(this.Parent.Name+'_CheckDisattivo')
        ELSE 
           this.parent.parent.parent.parent.parent.NotifyEvent(this.Parent.Name+'_CheckAttivo')
        ENDIF 
     ENDIF 
     
 PROCEDURE ListCount
    LOCAL ItmCount
    ItmCount = this.cal.ListCount
    RETURN ItmCount
ENDDEFINE

define CLASS cp_ctSchedule as container
  cEvent=''
  serial=''
  nBaseCal=0
  FontName=''
  FontSize=0
  MonthNames=""
  DayNames=""
  HeaderFillType=0
  
  ADD OBJECT CalTimer AS CalendarTimer
  
  add object cal as olecontrol with oleclass="ctSCHEDULE.ctScheduleCtrl.8",  OLETypeAllowed=-2, width=50,height=50,visible=.f.
    
  proc Init()  
    this.cal.width=this.width
    this.cal.height=this.height
    this.cal.visible=.t.
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.FontIndex1.Name = this.FontName
    this.cal.FontIndex1.Name = "Verdana"
    this.cal.FontIndex1.Size = this.FontSize
    this.cal.TitleFont.Name = this.FontName
    this.cal.TitleFont.Size = this.FontSize
    this.cal.ListFont.Name = this.FontName
    this.cal.ListFont.Size = this.FontSize
    this.cal.TitleFont.Name = this.FontName
    this.cal.TitleFont.Size = this.FontSize
    this.cal.RulerFont.Name = this.FontName
    this.cal.RulerFont.Size = this.FontSize
    * Color
    this.cal.BackColor = g_ColPrime
    this.cal.RulerBackColor = g_ColHeader
    this.cal.TitleBackColor = g_ColHeader
    this.cal.ListBackColor = g_ColPrime
    this.cal.ListBarWidth=1
    this.cal.BorderColor = g_ColPrime  
    this.cal.WeekendColor = g_ColNnPrm
    this.cal.WeekEnds=1
    this.cal.OverlapColor = g_ColSelect
    this.cal.OverlapLines=.T.
    this.cal.OverlapWidth=1
    this.cal.ConflictOffset=1 
    this.cal.MonthNames=g_Mese[1]+","+g_Mese[2]+","+g_Mese[3]+","+g_Mese[4]+","+g_Mese[5]+","+g_Mese[6]+","+g_Mese[7]+","+g_Mese[8]+","+g_Mese[9]+","+g_Mese[10]+","+g_Mese[11]+","+g_Mese[12]
    this.cal.DayNames=g_Giorno[1]+","+g_giorno[2]+","+g_giorno[3]+","+g_giorno[4]+","+g_giorno[5]+","+g_giorno[6]+","+g_giorno[7]
    * Propriet� settate dall'utente
    IF !EMPTY(this.MonthNames)
       this.cal.MonthNames=this.MonthNames
    ENDIF 
    IF !EMPTY(this.DayNames)
       this.cal.DayNames=this.DayNames
    ENDIF 
    this.cal.StartWeekDay=1 && Luned�
    this.cal.dateFormat="M Y"
    this.nBaseCal=VAL(SYS(11,cp_CharToDate("01-01-1900","dd-mm-yyyy")))
    this.cal.TimeType=1 && 0 - Hours, 1 - Days (Days of the month), 2 - Weeks, 3 - Months, 4 - Years
    this.cal.TimeLines=.T.
    this.cal.RulerSplit = .F.
    this.cal.RulerDivisions = 4
    this.cal.RulerDays = .T. 
    this.cal.RulerDaySize = 3 && Visualizza la descrizione del giorno utilizzando 3 caratteri 
    this.cal.HorzScrollType = 0 
    * this.cal.IncludeAmPm=.F.
    this.cal.MilitaryTime=.T.
    this.cal.DisplayOnly=.T.
    this.cal.AdjustOnMove=.F.
    this.cal.BarSelectFocus=.F.
    this.cal.BorderType = 1
    this.cal.RulerFillType=2  && 0 - Solid, 1 - Horizontal, 2 - Vertical, 3 - Diagonal, 4 - Reverse Diagonal, 5 - Horizontal Bump, 6 - Vertical Bump, 7 - Diamond, 8 - Pyramid, 9 - Circle, 10 - Ellipse  
    this.cal.TitleFillType=2 
    this.cal.TitleHeight=50
    * Propriet� settate dall'utente
    IF !EMPTY(this.MonthNames)
       this.cal.MonthNames=this.MonthNames
    ENDIF 
    IF !EMPTY(this.DayNames)
       this.cal.DayNames=this.DayNames
    ENDIF 
    IF this.HeaderFillType>=0 AND this.HeaderFillType<=10
       this.cal.RulerFillType=this.HeaderFillType    
       this.cal.TitleFillType=this.HeaderFillType       
    ENDIF 
    
  proc Calculate(xValue)

  proc Event(cEvent)
      
  PROCEDURE ClearSchedule
    this.cal.ClearSchedule()

  PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
    IF ! Vis
       this.ClearSchedule() 
    ENDIF 
  
  proc Resize()
    this.cal.width=this.width
    this.cal.height=this.height
    
  PROCEDURE AddItem
    PARAMETERS Txt, Ser
    LOCAL Ind
    Ind = this.cal.AddItem(Txt)
    this.cal.ItemCargo(Ind)=Ser   
    RETURN Ind 
    
  PROCEDURE ItemCargo
     PARAMETERS Itm
     LOCAL Crg 
     Crg = this.cal.ItemCargo(Itm)
     RETURN Crg  
     
  PROCEDURE DelItem
    PARAMETERS Ser
    * Prima cancella le time bar e poi rimuove l'item
    this.cal.ClearTimeBars(Ser)
    this.cal.DeleteItem(Ser)
    ENDPROC 

  PROCEDURE ListCount
    LOCAL ItmCount
    ItmCount = this.cal.ListCount
    RETURN ItmCount
    
  PROCEDURE AddTimeBar
    PARAMETERS lTimeStart, lTimeEnd, lDateStart, lDateEnd, clrTimeColor, nIndex
    LOCAL Idx
    Idx = this.cal.AddTimeBar(nIndex, lTimeStart, lTimeEnd, lDateStart, lDateEnd)
    IF Idx >0
       * this.cal.CBarTimeColor(Idx)=clrTimeColor
       this.cal.BarBackColor(nIndex,Idx)=clrTimeColor
       this.cal.BarReadOnly(nIndex,Idx)=.T.
    ENDIF 
    ENDPROC
    
  PROCEDURE AddColorBar 
    PARAMETERS lTimeStart, lTimeEnd, lDateStart, lDateEnd, clrTimeColor, nIndex
    this.cal.AddColorBar(lTimeStart, lTimeEnd, lDateStart, lDateEnd, clrTimeColor, nIndex)
    ENDPROC
  
  PROCEDURE AddColumn
    PARAMETERS Txt, Wid, Lck 
    LOCAL Idx 
    Idx=this.cal.AddColumn(Txt, Wid)
    this.cal.ColumnLock(Idx)=Lck
    RETURN Idx 
    ENDPROC
    
  PROCEDURE SetDate
    PARAMETERS DataIni, DataFin, OraIni, OraFin
    this.cal.DateStart=VAL(SYS(11,DataIni))-this.nBaseCal
    this.cal.DateEnd=VAL(SYS(11,DataFin))-this.nBaseCal
    IF DataIni=DataFin
       * Un solo giorno 
       this.cal.TimeType=0 && 0 - Hours, 1 - Days (Days of the month), 2 - Weeks, 3 - Months, 4 - Years
       this.cal.RulerSplit = .F.
       * this.cal.RulerDivisions = 4
       this.cal.RulerDays = .T. 
       this.cal.TimeStart = OraIni
       this.cal.TimeEnd = OraFin
    ELSE 
       * Pi� giorni
       this.cal.TimeType=1 && 0 - Hours, 1 - Days (Days of the month), 2 - Weeks, 3 - Months, 4 - Years
       this.cal.RulerSplit = .T.
       this.cal.RulerDivisions = 4
       this.cal.RulerDays = .T. 
    ENDIF 
    
  PROCEDURE PrintTitles
    PARAMETERS Tit, SubT
    this.cal.PrintTitle=Tit
    this.cal.PrintSubTitle=SubT
  ENDPROC
  
  PROCEDURE PrintSchedule
    this.cal.PrintSchedule()
  ENDPROC
  
  PROCEDURE cal.ListDblClick
    PARAMETERS nIndex
  	IF this.parent.enabled AND nIndex>0 AND !EMPTY(this.parent.Serial) && AND !EMPTY( this.AppointKeyID(nindex) )
  	   * Notifichiamo che � stato fatto il doppio click su una risorsa
  	   LOCAL Ser
  	   Ser = this.parent.Serial 
  	   this.parent.parent.parent.parent.parent.&Ser = nindex
  	   cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
  	   * Notifica l'evento "Modificato" alla maschera
  	   this.Parent.CalTimer.MaskToAgg=this.parent.parent.parent.parent.parent
  	   * Impostiamo come intervallo 1 millisecondo, il timer notificher� l'evento 'Modifica'
  	   this.Parent.CalTimer.interval=1
  	ENDIF 
  ENDPROC
ENDDEFINE

* --- Classe per la gestione del temporizzatore che notifica l'apertura dell'anagrafica
DEFINE CLASS CalendarTimer as Timer
       interval=0
       MaskToAgg=SPACE(10)
       eventtofire=SPACE(10)
       destroyafterexec=.F.
       proc init(p_parent,p_event,p_destroy)
       * -- Viene eseguita al momento della creazione dell'oggetto MyTimer
          this.MaskToAgg=IIF(VARTYPE(p_parent)<>'O',SPACE(10),p_parent)
          this.eventtofire=IIF(VARTYPE(p_event)<>'C','Modifica',p_event)
          this.destroyafterexec=p_destroy
         interval=0     && In tal modo non viene eseguita la proc Timer()
       Proc Timer
           * Attiva il batch di sincronizzazione 
           IF VARTYPE(this.MaskToAgg)='O'
              this.MaskToAgg.NotifyEvent(this.eventtofire)
              this.MaskToAgg=SPACE(10)
              IF this.destroyafterexec
                 This.Destroy()
              ENDIF
           ELSE 
              this.Interval=0
           ENDIF 
       ENDPROC 
ENDDEFINE

define CLASS cp_ctContact as container
  cEvent=''
  Serial=''
  nBaseCal=0
  FontName=''
  FontSize=0
  Ncolumn=0
  HeaderFillType=0  
  
  add object cal as DBIContact
  
  ADD OBJECT CalTimer AS CalendarTimer
  
  proc Init()  
    this.cal.left=0
    this.cal.top=0
    this.cal.width=this.width
    this.cal.height=this.height
    IF this.Ncolumn>0
       * Numero di colonne
       this.cal.ColumnWidth = INT( this.Width / this.Ncolumn)
    ELSE 
       this.cal.ColumnWidth = INT( this.Width / 3)
       * Se non sono specificate, impostiamo come default 3
    ENDIF 
    this.cal.visible=.t.
    * Font
    IF EMPTY(this.FontName)
       this.FontName='MS Sans Serif'
    ENDIF 
    IF EMPTY(this.FontSize)
       this.FontSize=8
    ENDIF 
    this.cal.Font.Name = this.FontName
    this.cal.Font.Size = this.FontSize
    this.cal.HeaderFont.Name = this.FontName
    this.cal.HeaderFont.Size = this.FontSize
    * Color
    this.cal.BackColor = g_ColPrime   
    this.cal.BorderColor=g_ColNnPrm
    this.cal.ColumnLineColor=g_ColNnPrm
    this.cal.HeaderBackColor = g_ColHeader
    this.cal.SelectedBackColor = g_ColSelect 
    this.cal.BorderType = 0 && 0 - Regular, 1 - None, 2 - Raised, 3 - Lowered, 4 - Raised Thin, 5 - Lowered Thin, 6 - Left Round, 7 - Right Round, 8 - Both Round      
    this.cal.ReadOnly=.T. 
    this.cal.SelectOnFocus=.F.
    this.cal.HeaderFillType=2 && 0 - Solid, 1 - Horizontal, 2 - Vertical, 3 - Diagonal, 4 - Reverse Diagonal, 5 - Horizontal Bump, 6 - Vertical Bump, 7 - Diamond, 8 - Pyramid, 9 - Circle, 10 - Ellipse
    IF this.HeaderFillType>=0 AND this.HeaderFillType<=10
       this.cal.HeaderFillType=this.HeaderFillType
    ENDIF
  endproc
  
  proc FontName_assign
	LPARAMETERS cNewVal
	IF EMPTY(m.cNewVal)
       this.FontName='MS Sans Serif'
    else
		this.FontName = m.cNewVal
	endif
    this.cal.Font.Name = this.FontName
    this.cal.HeaderFont.Name = this.FontName
  endproc

  proc FontSize_assign
	LPARAMETERS nNewVal
	IF EMPTY(m.nNewVal)
       this.FontSize=8
	else
		this.FontSize = m.nNewVal
	endif
    this.cal.Font.Size = this.FontSize
    this.cal.HeaderFont.Size = this.FontSize
  endproc

  proc Ncolumn_assign
	LPARAMETERS nNewVal
	if m.nNewVal>0
		this.Ncolumn = m.nNewVal
	else
		this.Ncolumn = 3
	endif
	this.Resize()
  endproc

  proc Calculate(xValue)

  proc Event(cEvent)
      
  PROCEDURE ClearAll
    this.cal.ClearHeaders()
    this.cal.ClearTitles()
  endproc

  PROCEDURE SetVisible
    PARAMETERS Vis
    this.visible=Vis
    this.Enabled=Vis 
    this.cal.Visible=Vis
    this.cal.Enabled=Vis
    IF ! Vis
       this.ClearList() 
    ENDIF
  endproc
  
  proc Resize()
    this.cal.width=this.width
    this.cal.height=this.height
	if this.Ncolumn>0
		this.cal.ColumnWidth = INT( this.Width / this.Ncolumn)
	endif
  endproc
    
  PROCEDURE cal.ItemDblClick
    PARAMETERS  nIndex, nColumn
    IF !EMPTY(this.parent.Serial) AND !EMPTY(this.HeaderCargo(nIndex))
       LOCAL Ser 
       Ser = this.parent.Serial 
       * Nel seriale potremmo avere i nomi di due variabili separate da ,
       IF AT(',',this.parent.Serial) >0
          Ser = LEFT(this.parent.Serial, AT(',',this.parent.Serial)-1)
       ENDIF 
       
       this.parent.parent.parent.parent.parent.&Ser = this.HeaderCargo(nIndex)
       
       cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
       this.Parent.CalTimer.MaskToAgg=this.parent.parent.parent.parent.parent
       * Impostiamo come intervallo 1 millisecondo, il timer notificher� l'evento 'Modifica'
       this.Parent.CalTimer.interval=1
  	ENDIF
  endproc
  
  PROCEDURE AddTitle
     PARAMETERS Txt
     LOCAL Ind
     Ind = this.cal.AddTitle(Txt)
     RETURN Ind
  endproc
    
  PROCEDURE AddHeader
     PARAMETERS Txt, Serial 
     LOCAL Ind
     Ind = this.cal.AddHeader(Txt)
     this.cal.HeaderCargo(Ind)=Serial
     RETURN Ind
  endproc
     
  PROCEDURE ItemText
    PARAMETERS nIndex, Hdr, Txt
    this.cal.ItemText(nIndex, Hdr) = Txt
  endproc

  PROCEDURE FindVal
     PARAMETERS FirstValToFind, NextStringToFind
     LOCAL Ind
     IF EMPTY(FirstValToFind)
        * Nessun valore da ricercare, si posiziona sul primo elemento utile
        this.cal.TopRecord = 1
     ELSE 
        Ind=this.cal.Findtext(ALLTRIM(FirstValToFind), 0, .T., .F. )
        IF Ind>0
           * Ha trovato il primo valore da ricercare 
           this.cal.TopRecord = Ind
        ELSE 
           IF !EMPTY(NextStringToFind)
              DO WHILE !EMPTY(NextStringToFind) AND Ind<=0
                 Ind=this.cal.Findtext(LEFT(NextStringToFind,1), 0, .T., .F. )
                 IF Ind>0
                    this.cal.TopRecord = Ind
                 ELSE 
                    NextStringToFind=SUBSTR(NextStringToFind,2)
                 ENDIF 
              ENDDO 
           ENDIF 
        ENDIF 
     ENDIF
  endproc

 PROCEDURE ListCount
    LOCAL ItmCount
    ItmCount = this.cal.TitleCount
    RETURN ItmCount
  endproc
    
 PROCEDURE GetTopRecord
    LOCAL TopRec
    TopRec = this.cal.TopRecord 
    RETURN TopRec
  endproc
    
 PROCEDURE GetFirstIndex
    LOCAL FirstIdx
    FirstIdx = this.cal.FirstIndex 
    RETURN FirstIdx
  endproc
    
 PROCEDURE HeaderCount
    LOCAL ItmCount
    ItmCount = this.cal.HeaderCount
    RETURN ItmCount
  endproc
    
 PROCEDURE cal.MouseDown
    PARAMETERS Tasto, mm, xx, yy
    IF Tasto=2 AND !EMPTY(this.parent.Serial) AND AT(',',this.parent.Serial) >0 AND !EMPTY(this.HeaderCargo(this.selected))
       * Gestiamo la pressione del tasto destro: nel seriale inseriamo la concatenazione delle due variabili
       LOCAL Ser 
       * Nel seriale potremmo avere i nomi di due variabili separate da ,
       Ser = LEFT(this.parent.Serial, AT(',',this.parent.Serial)-1)
       * Mettiamo a blank la prima variabile
       this.parent.parent.parent.parent.parent.&Ser = ''
       * Impostiamo la seconda prima variabile
       Ser = SUBSTR(this.parent.Serial, AT(',',this.parent.Serial)+1)
       this.parent.parent.parent.parent.parent.&Ser = this.HeaderCargo(this.selected)
       cp_UpdatedFromObj(this.parent.parent.parent.parent.parent,this.parent.nPag)
       this.Parent.CalTimer.MaskToAgg=this.parent.parent.parent.parent.parent
       * Impostiamo come intervallo 1 millisecondo, il timer notificher� l'evento 'Modifica'
       this.Parent.CalTimer.interval=1
  	ENDIF
  endproc
ENDDEFINE
* --- Zucchetti Aulla Fine - OCX Calendario

*---Zucchetti oggetto per il controllo della sicurezza - Fine
Define Class SecurityObj As custom
  *
  bFox26=.f.
  *
  bSec1=.t.                         && primo parametro di sicurezza (accesso)
  bSec2=.t.                         && secondo parametro di sicurezza (inserimento)
  bSec3=.t.                         && terzo parametro di sicurezza (modifica)
  bSec4=.t.                         && quarto parametro di sicurezza (cancellazione)  

enddefine

*******************************************************************************
***																			***
***							GESTIONE TABELLA PIATTA							***
***																			***
*******************************************************************************
function GetFTPhName(pTabName)
	* Restituisce il nome fisico della flat table passata come parametro
	* gi� comprensivo dell'eventuale nome azienda
	
	if lower(left(alltrim(pTabName),3))=='xxx'
		pTabName=lower(left(alltrim(pTabName),3))+"FT_"+RIGHT(ALLTRIM(pTabName),LEN(ALLTRIM(pTabName))-3)
	else
		pTabName="FT_"+ALLTRIM(pTabName)
	endif
	Return cp_SetAzi(ALLTRIM(pTabName))
endfunc

function GetETPhName(pTabName,pArchName)
	local RT_Res, L_FTFL_AZI
	LOCAL lTabName, lArchName
	lTabName=pTabName
	lArchName=pArchName
	lTabName=STRTRAN(pTabName,'xxx','',1)
	lArchName=STRTRAN(pArchName,'xxx','',1)
	RT_Res=READTABLE('FLATMAST','FTFL_AZI','','',.F.,"FTCODICE='"+lTabName+"'")
	if !EMPTY(RT_Res)
		Select (RT_Res)
		go top
		L_FTFL_AZI=NVL(FTFL_AZI,'N')
		lTabName=ALLTRIM(IIF(L_FTFL_AZI='S','xxx','')+lTabName)
		if lower(left(alltrim(lTabName),3))=='xxx'
			lTabName="EDI_"+RIGHT(ALLTRIM(lTabName),LEN(ALLTRIM(lTabName))-3)+"_"+lower(left(alltrim(lTabName),3))+lArchName
		else
			lTabName="EDI_"+ALLTRIM(pTabName)+"_"+lArchName
		ENDIF
		use in (RT_Res)
		Return cp_SetAzi(ALLTRIM(lTabName))
	else
		Return ''
	endif
endfunc

function ChkFTStatus(pTabName)
	* Verifica stato tabella 
	* Ritorna:
	*	-1	= Tabella da creare	
	*	 0 	= Tabella aggiornata
	*	 1	= Tabella da aggiornare
	local r,TabPhName,nfld,i_oldexact,res, rfld, afld
	local i,p,fldname,fldtype,fldlen,flddec,k
	TabPhName= GetFTPhName(pTabName)
	r= ReadTable(TabPhName, '*' , '', '', .F. , '1=2')
	if empty(r)
		return(-1)
	endif
	Select(r)
	GO TOP
	DIMENSION TabFld(1,1)
	rfld=afields(TabFld)
	Dimension ArrAnag(1,18)
	if tfields ( pTabName , @ArrAnag)=-1
		return(-1)
	endif
	res=0
	afld=alen(ArrAnag,1)
	i_oldexact=set('EXACT')
	set exact on
	for i=1 to afld
		fldname=ArrAnag(i,1)
		fldtype=ArrAnag(i,2)
		fldlen=ArrAnag(i,3)
		flddec=ArrAnag(i,4)
		p=ascan(TabFld,upper(fldname))
		if p=0 .or. cp_IsFldModified(TabFld(p+1),TabFld(p+2),TabFld(p+3),fldtype,fldlen,flddec)
		  res=1
		endif
	next
	set exact &i_oldexact
	Select(r)
	use
	return(res)
endfunc

function ChkETStatus(pTabName,pArchName)
	* Verifica stato tabella 
	* Ritorna:
	*	-1	= Tabella da creare	
	*	 0 	= Tabella aggiornata
	*	 1	= Tabella da aggiornare
	local r,TabPhName,nfld,i_oldexact,res, rfld, afld
	local i,p,fldname,fldtype,fldlen,flddec,k
	TabPhName= GetETPhName(pTabName,pArchName)
	r= ReadTable(TabPhName, '*' , '', '', .F. , '1=2')
	if empty(r)
		return(-1)
	endif
	Select(r)
	GO TOP
	DIMENSION TabFld(1,1)
	rfld=afields(TabFld)
	Dimension ArrAnag(1,18)
	if t_etfields ( pTabName , pArchName, @ArrAnag)=-1
		return(-1)
	endif
	res=0
	afld=alen(ArrAnag,1)
	i_oldexact=set('EXACT')
	set exact on
	for i=1 to afld
		fldname=ArrAnag(i,1)
		fldtype=ArrAnag(i,2)
		fldlen=ArrAnag(i,3)
		flddec=ArrAnag(i,4)
		p=ascan(TabFld,upper(fldname))
		if p=0 .or. cp_IsFldModified(TabFld(p+1),TabFld(p+2),TabFld(p+3),fldtype,fldlen,flddec)
		  res=1
		endif
	next
	set exact &i_oldexact
	Select(r)
	use
	return(res)
endfunc

function ReadFTStructure(pTabName, pTabFilter, CurMast, CurDett)
	* Popola i cursori passati per riferimento con la
	* struttura della tabella piatta. Utilizzabile
	* per la creazione o la modifica della tabella
	* Ritorna:
	*	>0	= Tabella lette correttamente, numero di campi di servizio
	*	-1	= Errore durante la lettura delle tabelle
	local TabPhName
	TabPhName= GetFTPhName(pTabName)
	CurMast= ReadTable(cp_setazi('xxxFLATMAST'), 'FT___UID AS SF1,FTOBJUID AS SF2,FTROWORD AS SF3,FTSTUCID AS SF4' , '', '', .F. , "FTCODICE='"+StrTran(pTabName,'xxx','')+"'")
	if empty(CurMast)
		return(-1)
	endif
	CurDett= ReadTable(cp_setazi('xxxFLATDETT'), '*' , '', '', .F. , "FTCODICE='"+StrTran(pTabName,'xxx','')+"'"+ IIF(EMPTY(pTabFilter),""," AND FTTABNAM='"+ALLTRIM(pTabFilter)+"'"))
	if empty(CurDett)
		return(-1)
	endif
	return 4
endfunc

function ReadETStructure(pTabName, pTabFilter, CurMast, CurDett)
	* Popola i cursori passati per riferimento con la
	* struttura della tabella piatta. Utilizzabile
	* per la creazione o la modifica della tabella
	* Ritorna:
	*	>0	= Tabella lette correttamente, numero di campi di servizio
	*	-1	= Errore durante la lettura delle tabelle
	local TabPhName
	TabPhName= GetETPhName(pTabName,pTabFilter)
	CurMast= ReadTable(cp_setazi('xxxFLATMAST'), 'FT___UID AS SF1,FTOBJUID AS SF2,FTROWORD AS SF3,FTSTUCID AS SF4,FTSTATUS AS SF5' , '', '', .F. , "FTCODICE='"+StrTran(pTabName,'xxx','')+"'")
	if empty(CurMast)
		return(-1)
	endif
	CurDett= ReadTable(cp_setazi('xxxFLATDETT'), '*' , '', '', .F. , "FTCODICE='"+StrTran(pTabName,'xxx','')+"'"+ IIF(EMPTY(pTabFilter),""," AND FTTABNAM='"+ALLTRIM(pTabFilter)+"'"))
	if empty(CurDett)
		return(-1)
	endif
	return 5
endfunc

function tfields( pTabName, pArrName)
	* Popola l'array passatogli per riferimento con la struttura
	* dei campi della tabella in formato compatibile con il comando
	* afields del FoxPro, i valori da 5 a 18 vengono inizializzati
	* con il valore vuoto per il tipo del campo.
	* Ritorna:
	*	0	= Array popolato correttamente
	*	-1	= Errore durante l'elaborazione
	
	local r, phName, TotRow, logName,i, cFldTabName, t, TotServiceFld
	logName = Alltrim(STRTRAN(pTabName,'xxx',''))
	t=""
	r=""
	TotServiceFld=ReadFTStructure(pTabName,'', @t, @r)
	if TotServiceFld<=0
		return (-1)
	endif
	Select(r)
	go top
    TotRow = RecCount() 
	if TotRow<1
		return -1
	endif
	Dimension pArrName(TotRow+TotServiceFld,18)
	*Inserisco nell'Array i dati dei campi di servizio
	Select(t)
	go top
	for i=1 to TotServiceFld
		cReadFld = 'SF'+ALLTRIM(STR(i))
		cFldTabName = &t .&cReadFld
		cFldTabName=UPPER(ALLTRIM(cFldTabName))
		if 'ROWORD' $ cFldTabName
			cFldType='N'
			nFldDim=5
		else
			cFldType='C'
			nFldDim=10
		endif
		pArrName(i,1) = cFldTabName
		pArrName(i,2) = ALLTRIM(cFldType)
		pArrName(i,3) = nFldDim
		pArrName(i,4) = 0
		pArrName(i,5) = .F.
		pArrName(i,6) = .F.
		pArrName(i,7) = ''
		pArrName(i,8) = ''
		pArrName(i,9) = ''
		pArrName(i,10) = ''
		pArrName(i,11) = ''
		pArrName(i,12) = ''
		pArrName(i,13) = ''
		pArrName(i,14) = ''
		pArrName(i,15) = ''
		pArrName(i,16) = ''
		pArrName(i,17) = 0
		pArrName(i,18) = 0
	next
	*Inserisco nel vettore i dati dei campi definiti in anagrafica
	Select(r)
	go top
	LOCAL TestMacto
	for i=1+TotServiceFld to TotRow+TotServiceFld
  	   TestMacto=&r .FTFLDNAM
		if !EMPTY(ALLTRIM(TestMacto))
			cFldTabName=UPPER(ALLTRIM(&r .FTTABNAM))
			pArrName(i,1) = cFldTabName+IIF(EMPTY(cFldTabName),"","_")+ALLTRIM(&r .FTFLDNAM)
			pArrName(i,2) = ALLTRIM(&r .FTFLDTYP)
			pArrName(i,3) = &r .FTFLDDIM
			pArrName(i,4) = &r .FTFLDDEC
			pArrName(i,5) = .F.
			pArrName(i,6) = .F.
			pArrName(i,7) = ''
			pArrName(i,8) = ''
			pArrName(i,9) = ''
			pArrName(i,10) = ''
			pArrName(i,11) = ''
			pArrName(i,12) = ''
			pArrName(i,13) = ''
			pArrName(i,14) = ''
			pArrName(i,15) = ''
			pArrName(i,16) = ''
			pArrName(i,17) = 0
			pArrName(i,18) = 0
			SKIP
		endif
	next
	Select (r)
	Use
	Select (t)
	Use
	Return 0
endfunc

function t_etfields( pTabName, pArchName, pArrName)
	* Popola l'array passatogli per riferimento con la struttura
	* dei campi della tabella in formato compatibile con il comando
	* afields del FoxPro, i valori da 5 a 18 vengono inizializzati
	* con il valore vuoto per il tipo del campo.
	* Ritorna:
	*	0	= Array popolato correttamente
	*	-1	= Errore durante l'elaborazione
	
	local r, phName, TotRow, logName,i, cFldTabName, t, TotServiceFld
	logName = Alltrim(STRTRAN(pTabName,'xxx',''))
	t=""
	r=""
	TotServiceFld=ReadETStructure(pTabName,pArchName, @t, @r)
	if TotServiceFld<=0
		return (-1)
	endif
	Select(r)
	go top
    TotRow = RecCount() 
	if TotRow<1
		return -1
	endif
	Dimension pArrName(TotRow+TotServiceFld,18)
	*Inserisco nell'Array i dati dei campi di servizio
	Select(t)
	go top
	for i=1 to TotServiceFld
		cReadFld = 'SF'+ALLTRIM(STR(i))
		cFldTabName = &t .&cReadFld
		cFldTabName=UPPER(ALLTRIM(cFldTabName))
		if 'ROWORD' $ cFldTabName
			cFldType='N'
			nFldDim=5
		else
			cFldType='C'
			nFldDim=10
		endif
		pArrName(i,1) = cFldTabName
		pArrName(i,2) = ALLTRIM(cFldType)
		pArrName(i,3) = nFldDim
		pArrName(i,4) = 0
		pArrName(i,5) = .F.
		pArrName(i,6) = .F.
		pArrName(i,7) = ''
		pArrName(i,8) = ''
		pArrName(i,9) = ''
		pArrName(i,10) = ''
		pArrName(i,11) = ''
		pArrName(i,12) = ''
		pArrName(i,13) = ''
		pArrName(i,14) = ''
		pArrName(i,15) = ''
		pArrName(i,16) = ''
		pArrName(i,17) = 0
		pArrName(i,18) = 0
	next
	*Inserisco nel vettore i dati dei campi definiti in anagrafica
	Select(r)
	go top
	for i=1+TotServiceFld to TotRow+TotServiceFld
		if !EMPTY(ALLTRIM(&r .FTFLDNAM))
			cFldTabName=UPPER(ALLTRIM(&r .FTTABNAM))
			pArrName(i,1) = ALLTRIM(&r .FTFLDNAM)
			pArrName(i,2) = ALLTRIM(&r .FTFLDTYP)
			pArrName(i,3) = &r .FTFLDDIM
			pArrName(i,4) = &r .FTFLDDEC
			pArrName(i,5) = .F.
			pArrName(i,6) = .F.
			pArrName(i,7) = ''
			pArrName(i,8) = ''
			pArrName(i,9) = ''
			pArrName(i,10) = ''
			pArrName(i,11) = ''
			pArrName(i,12) = ''
			pArrName(i,13) = ''
			pArrName(i,14) = ''
			pArrName(i,15) = ''
			pArrName(i,16) = ''
			pArrName(i,17) = 0
			pArrName(i,18) = 0
			SKIP
		endif
	next
	Select (r)
	Use
	Select (t)
	Use
	Return 0
endfunc

function MakeFlatTable(pTabName, pTabFilter, bTempTable, pTempName)
	* Crea la tabella sul database. La funzione aggiunge anche
	* il campo cpcchk per renderla compatibile con le scritture
	* Painter
	* Ritorna:
	*	1	= Creazione tabella avvenuta correttamente
	*	-1	= Errore durante la creazione della tabella
	local r, TotRow,s, cFldtabName, t, cFldAtt, cReadFld, cFldType, nFldDim, TotServiceFld
	t=""
	r=""
	if VARTYPE(pTabFilter)='L'
		pTabFilter=""
	endif
	if bTempTable
		TabPhName=pTempName
	else
		TabPhName= GetFTPhName(pTabName)
	endif
	* Leggo la struttura della flat table
	TotServiceFld= ReadFTStructure(pTabName, pTabFilter, @t, @r)
	if TotServiceFld<=0
		return (-1)
	endif
	s=""
	nConn=i_serverconn[1,2]
	* Inserimento campi di servizio
	Select(t)
	go top
	for i=1 to TotServiceFld
		cReadFld = 'SF'+ALLTRIM(STR(i))
		cFldAtt = &t .&cReadFld
		if 'ROWORD' $ cFldAtt
			cFldType='N'
			nFldDim=5
		else
			cFldType='C'
			nFldDim=10
		endif
		s=s+iif(i<>1,', ','')+ALLTRIM(cFldAtt)+' '+db_FieldType(cp_GetDatabaseType(nConn),cFldType,nFldDim,0)
	next
	* Inserimento campi definiti in anagrafica
	Select(r)
	go top
    TotRow = RecCount()
	for i=1 to TotRow
		cFldtabName=UPPER(ALLTRIM(&r .FTTABNAM))
		s=s+', '+IIF(bTempTable Or EMPTY(cFldTabName),"",cFldtabName+"_")+ALLTRIM(&r .FTFLDNAM)+' '+db_FieldType(cp_GetDatabaseType(nConn),&r .FTFLDTYP,&r .FTFLDDIM,&r .FTFLDDEC)
		SKIP
	next
	Select (r)
	Use
	Select (t)
	Use
	s=s+", cpcchk Char(10)"
	r=cp_SQL(nConn,"create table "+TabPhName+" ("+s+")"+cp_PostTable(cp_GetDatabaseType(nConn)))
	Return (r)
endfunc

function MakeEDITable(pTabName, pTabFilter, bTempTable, pTempName)
	* Crea la tabella sul database. La funzione aggiunge anche
	* il campo cpcchk per renderla compatibile con le scritture
	* Painter
	* Ritorna:
	*	1	= Creazione tabella avvenuta correttamente
	*	-1	= Errore durante la creazione della tabella
	local r, TotRow,s, cFldtabName, t, cFldAtt, cReadFld, cFldType, nFldDim, TotServiceFld
	t=""
	r=""
	if VARTYPE(pTabFilter)='L'
		pTabFilter=""
	endif
	if bTempTable
		TabPhName=pTempName
	else
		TabPhName= GetETPhName(pTabName, pTabFilter)
	endif
	* Leggo la struttura della flat table
	TotServiceFld= ReadETStructure(pTabName, pTabFilter, @t, @r)
	if TotServiceFld<=0
		return (-1)
	endif
	s=""
	nConn=i_serverconn[1,2]
	* Inserimento campi di servizio
	Select(t)
	go top
	for i=1 to TotServiceFld
		cReadFld = 'SF'+ALLTRIM(STR(i))
		cFldAtt = &t .&cReadFld
		if 'ROWORD' $ cFldAtt
			cFldType='N'
			nFldDim=5
		else
			cFldType='C'
			nFldDim=10
		endif
		s=s+iif(i<>1,', ','')+ALLTRIM(cFldAtt)+' '+db_FieldType(cp_GetDatabaseType(nConn),cFldType,nFldDim,0)
	next
	* Inserimento campi definiti in anagrafica
	Select(r)
	go top
    TotRow = RecCount()
	for i=1 to TotRow
		* *** SOLO FT *** cFldtabName=UPPER(ALLTRIM(&r .FTTABNAM))
		s=s+', '+IIF(bTempTable Or EMPTY(cFldTabName),"",cFldtabName+"_")+ALLTRIM(&r .FTFLDNAM)+' '+db_FieldType(cp_GetDatabaseType(nConn),&r .FTFLDTYP,&r .FTFLDDIM,&r .FTFLDDEC)
		SKIP
	next
	Select (r)
	Use
	Select (t)
	Use
	s=s+", cpcchk Char(10)"
	r=cp_SQL(nConn,"create table "+TabPhName+" ("+s+")"+cp_PostTable(cp_GetDatabaseType(nConn)))
	Return (r)
endfunc

function ModifyFlatTable(pTabName)
	* Modifica la flat table aggiungendo i campi nuovi e
	* modificando se possibile i campi gi� presenti
	* Ritorna:
	*	1	= Modifica della tabella avvenuta correttamente
	*	-1	= Errore durante la modifica della tabelle o la lettura delle definizioni
	
	local r,TabPhName,nfld,i_oldexact,res,rfld,afld,bTypeMod, fldtypedef
	local i,p,fldname,fldtype,fldlen,flddec,k
	nConn=i_serverconn[1,2]
	cDatabaseType=cp_GetDatabaseType(nConn)
	TabPhName= GetFTPhName(pTabName)
	r= ReadTable(TabPhName, '*' , '', '', .F. , '1=2')
	if empty(r)
		return(-1)
	endif
	Select(r)
	GO TOP
	DIMENSION TabFld(1,1)
	rfld=afields(TabFld)
	Dimension ArrAnag(1,18)
	if tfields ( pTabName , @ArrAnag)=-1
		return(-1)
	endif
	res=0
	afld=alen(ArrAnag,1)
	i_oldexact=set('EXACT')
	set exact on
	for i=1 to afld
		fldname=ArrAnag(i,1)
		fldtype=ArrAnag(i,2)
		fldlen=ArrAnag(i,3)
		flddec=ArrAnag(i,4)
		p=ascan(TabFld,upper(fldname))
		bTypeMod=.f.
		if p<>0
			bTypeMod=cp_IsFldModified(TabFld(p+1),TabFld(p+2),TabFld(p+3),fldtype,fldlen,flddec)
		endif
		if p=0 or bTypeMod
			fldtypedef=db_FieldType(cDatabaseType,fldtype,fldlen,flddec)
			if nConn<>0
				if p=0
					* --- nuova definizione della colonna + check
					res=cp_SQL(nConn,"alter table &TabPhName add &fldname &fldtypedef ")
				else
					if bTypeMod
						* --- solo alcuni database prevedono la modifica della colonna
						k=db_ModifyColumn(fldname,fldtypedef,cDatabaseType,nConn)
						if empty(k)
							res=-1
						else
							res=cp_SQL(nConn,"alter table &TabPhName "+k)
						endif
					endif
				endif
			endif
		endif  
		if res<0
			exit
		endif
	next
	set exact &i_oldexact
	Select(r)
	use
	return(res)
endfunc

function ModifyEDITable(pTabName, pArchName)
	* Modifica la flat table aggiungendo i campi nuovi e
	* modificando se possibile i campi gi� presenti
	* Ritorna:
	*	1	= Modifica della tabella avvenuta correttamente
	*	-1	= Errore durante la modifica della tabelle o la lettura delle definizioni
	
	local r,TabPhName,nfld,i_oldexact,res,rfld,afld,bTypeMod, fldtypedef
	local i,p,fldname,fldtype,fldlen,flddec,k
	nConn=i_serverconn[1,2]
	cDatabaseType=cp_GetDatabaseType(nConn)
	TabPhName= GetETPhName(pTabName, pArchName)
	r= ReadTable(TabPhName, '*' , '', '', .F. , '1=2')
	if empty(r)
		return(-1)
	endif
	Select(r)
	GO TOP
	DIMENSION TabFld(1,1)
	rfld=afields(TabFld)
	Dimension ArrAnag(1,18)
	if t_etfields ( pTabName , pArchName, @ArrAnag)=-1
		return(-1)
	endif
	res=0
	afld=alen(ArrAnag,1)
	i_oldexact=set('EXACT')
	set exact on
	for i=1 to afld
		fldname=ArrAnag(i,1)
		fldtype=ArrAnag(i,2)
		fldlen=ArrAnag(i,3)
		flddec=ArrAnag(i,4)
		p=ascan(TabFld,upper(fldname))
		bTypeMod=.f.
		if p<>0
			bTypeMod=cp_IsFldModified(TabFld(p+1),TabFld(p+2),TabFld(p+3),fldtype,fldlen,flddec)
		endif
		if p=0 or bTypeMod
			fldtypedef=db_FieldType(cDatabaseType,fldtype,fldlen,flddec)
			if nConn<>0
				if p=0
					* --- nuova definizione della colonna + check
					res=cp_SQL(nConn,"alter table &TabPhName add &fldname &fldtypedef ")
				else
					if bTypeMod
						* --- solo alcuni database prevedono la modifica della colonna
						k=db_ModifyColumn(fldname,fldtypedef,cDatabaseType,nConn)
						if empty(k)
							res=-1
						else
							res=cp_SQL(nConn,"alter table &TabPhName "+k)
						endif
					endif
				endif
			endif
		endif  
		if res<0
			exit
		endif
	next
	set exact &i_oldexact
	Select(r)
	use
	return(res)
endfunc

function AdminFlatTable(pTabName)
	* Crea o modifica la flat table se necessario
	* Ritorna:
	*	0	= Nessuna operazione svolta
	*	1	= Tabella creata o modificata correttamente
	*	-1	= Errore durante la creazione o modifica della tabella
	
	local nFTStatus, ret
	nFTStatus=ChkFTStatus(ptabName)
	do case
		case nFTStatus=-1
			*Creazione tabella
			ret=MakeFlatTable(pTabName)
		case nFTStatus=1
			*Aggiornamento tabella
			ret=ModifyFlatTable(pTabName)
		case nFTStatus=0
			*Nothing to do
			ret = 0
	endcase
	return (ret)
endfunc

function AdminEDITable(pTabName, pArchName)
	* Crea o modifica la flat table se necessario
	* Ritorna:
	*	0	= Nessuna operazione svolta
	*	1	= Tabella creata o modificata correttamente
	*	-1	= Errore durante la creazione o modifica della tabella
	
	local nFTStatus, ret
	nFTStatus=ChkETStatus(ptabName, pArchName)
	do case
		case nFTStatus=-1
			*Creazione tabella
			ret=MakeEDITable(pTabName, pArchName)
		case nFTStatus=1
			*Aggiornamento tabella
			ret=ModifyEDITable(pTabName, pArchName)
		case nFTStatus=0
			*Nothing to do
			ret = 0
	endcase
	return (ret)
endfunc

*******************************************************************************
***																			***
***				Gestione Tabelle temporanee da flat table					***
***																			***
*******************************************************************************
* classe oggetto XML utilizzato in import EDI
define class Ah_XMLObject as container
  oxml=.null.
  proc init
    this.oxml=createobject("Msxml2.DOMDocument")
  ENDPROC
  * Distruggo oggetto XML
  proc DestroyXml
    this.oxml=.null.
  ENDPROC
  * Save eseguo salvataggio oggetto XML su file passato
  proc SaveXml(pFile)
    this.oxml.save(pFile)
  ENDPROC 
enddefine
Procedure InitFT_TempArray()
	* Inizializza l'array delle corrispondenze nomi fisici/logici 
	* tabelle temporaenee FlatTable
	if VARTYPE(ArrFlatTmpTable)='U'
		PUBLIC ARRAY ArrFlatTmpTable(1,2) 
		STORE '' TO ArrFlatTmpTable
	endif
Endproc

function AddFT_TmpName(pTabName)
	* Aggiunge al vettore un riferimento a tabella temporanea
	* il nome fisico viene calcolato dinamicamente
	* Ritorna:
	*	Il nome fisico della tabella temporanea
	local nConn, TempName, LenArray
	nConn=i_serverconn[1,2]
	InitFT_TempArray()
	TempName=cp_getTempTableName(nConn)
	if EMPTY(ArrFlatTmpTable(1,1))
		LenArray=1
	else
		LenArray=ALEN(ArrFlatTmpTable,1)
		LenArray = LenArray + 1
		Dimension ArrFlatTmpTable(LenArray,2)
	endif
	ArrFlatTmpTable(LenArray,1)=UPPER(pTabName)
	ArrFlatTmpTable(LenArray,2)=TempName
	return (ArrFlatTmpTable(LenArray,2))
endfunc

function GetFT_TmpPhName(pTabName, bNoAdd)
	* Restituisce il nome fisico della tabella temporanea
	* Se il paramento bNoAdd non � .T. e la tabella temporanea
	* non esiste ancora la aggiunge
	* Ritorna:
	*	valore vuoto	=	Errore durante la ricerca/creazione
	*	Nome fisico tabella in caso di successo
	local ret, nIdxTab
	InitFT_TempArray()
	ret=''
	nIdxTab=-1
	nIdxTab=GetFT_TmpIdx(pTabName)
	if nIdxTab>0
		ret=ArrFlatTmpTable(nIdxTab,2)
	else
		if nIdxTab=0 And  !bNoAdd
			ret=AddFT_Tmpname(pTabName)
		endif
	endif
	return (ret)
endfunc

function GetFT_TmpIdx(pTabName)
	* Restituisce l'indice del vettore della tabella temporanea
	* Ritorna:
	*	-1	= Errore
	*	0	= tabella non ancora creata
	*	>0	= indice tabella
	local ret
	InitFT_TempArray()
	ret=-1
	ret=ASCAN(ArrFlatTmpTable, UPPER(pTabName),1,ALEN(ArrFlatTmpTable,1),1,14)
	return (ret)
endfunc

function DropFT_TempTable(pTabName)
	* Elimina la tabella temporanea associata al nome tabella passatogli
	* Ritorna:
	*	-1	= errore reperimento tabella
	*	0	= tabelle non trovata
	*	>0	= Operazione riuscita correttamente
	local nConn, TempName, nIdxTab, LenArray
	InitFT_TempArray()
	nConn=i_serverconn[1,2]
	nIdxTab=-1
	nIdxTab=GetFT_TmpIdx(pTabName)
	if nIdxTab>0
		TempName=GetFT_TmpPhName(pTabName, .T.)
		sqlexec(nConn,'drop table '+TempName)
		LenArray=ALEN(ArrFlatTmpTable,1)
		if LenArray>1 
			ADEL(ArrFlatTmpTable, nIdxTab)
			DIMENSION ArrFlatTmpTable(LenArray-1,2)
		else
			ArrFlatTmpTable(1,1)=""
			ArrFlatTmpTable(1,2)=""
		endif
	endif
	return(nIdxTab)
endfunc

function DropFT_All()
	* Elimina tutte le tabelle temporanee create
	* e relativi riferimenti
	do while (!EMPTY(ArrFlatTmpTable(1,1)))
		DropFT_TempTable(ArrFlatTmpTable(1,1))
	enddo
endfunc

Function DestroyFT_TempTable()
	* Elimina tutte le tabelle temporanee, i riferimenti ad esse
	* e rilascia l'array delle associazioni
	
	*Elimino tutte le tabelle temporanee
	DropFT_All()
	*Rilascio dalla memoria l'array
	RELEASE ArrFlatTmpTable
endfunc

function MakeTempFT(pFlatName, pFilterTable)
	* Crea una nuova tabella temporanea, se esiste gi�
	* una tabella associata elimina quella esistente e ne crea una nuova
	* Ritorna:
	*	-1	= Errore durante l'eliminazione/creazione tabella
	*	1	= Creazione avvenuta correttamente
	local TmpName, ret
	InitFT_TempArray()
	ret=-1
	* se necessario effettuo la drop della tabella temporanea gi� esistente
	DropFT_TempTable(pFilterTable)
	* Assegno un nome alla tabella temporanea legata alla tabella indicata
	TmpName=GetFT_TmpPhName(pFilterTable)
	* Creo la tabella temporanea
	ret=MakeFlatTable(pFlatName, pFilterTable, .T., TmpName)
	return (ret)
endfunc

function MakeAllTempFT(pFlatName, pArrName)
	* Crea tutte le tabelle temporanee associate alla struttura indicata
	* Inserisce i nomi logici delle tabelle nell'array passato per riferimento
	* Ritorna:
	*	-1	= Errore in lettura, creazione tabella temoporanea
	*	1	= Creazione avvenuta correttamente
	local TabPhName, NumTab, i, TabAtt, ret
	TabPhName= GetFTPhName(pFlatName)
	*Leggo il campo con il nome delle tabelle
	CurDett= ReadTable(cp_setazi('xxxFLATDETT'), 'FTTABNAM' , '', '', .F. , "FTCODICE='"+StrTran(pFlatName,'xxx','')+"'")
	if empty(CurDett)
		return(-1)
	endif
	*Elimino le ripetizioni delle tabelle
	SELECT DISTINCT * FROM (CurDett) INTO CURSOR (CurDett)
	SELECT (CurDett)
	NumTab=RECCOUNT()
	*Ridimensiono l'array risultato con il numero effettivo delle tabelle
	DIMENSION pArrName(NumTab,1)
	GO TOP
	i=1
	ret=0
	*Creo le tabelle temporanee e memorizzo i nomi logici nel vettore risultato
	do While (i<=NumTab and !ret=-1)
		TabAtt = ALLTRIM( &CurDett .FTTABNAM )
		ret=MakeTempFT(pFlatName, TabAtt)
		if ret=-1
			exit
		endif
		pArrName(i,1)= TabAtt
		i = i + 1
		SELECT (CurDett)
		SKIP
	enddo
	If USED(CurDett)
		USE IN (CurDett)
	endif
	ret=IIF(ret=-1, ret, NumTab)
	Return (ret)
endfunc

*#############################################################################*
*##																			##*
*##			FINE Gestione Tabelle temporanee da flat table FINE				##*
*##																			##*
*#############################################################################*


* --- Zucchetti Aulla Inizio - Controllo/Creazione chiave registro di sistema per invio Fax (Acrobat reader)
PROCEDURE SetRegFax()
 DECLARE INTEGER RegQueryValueEx IN advapi32; 
    INTEGER   hKey,; 
    STRING    lpValueName,; 
    INTEGER   lpReserved,; 
    INTEGER @ lpType,; 
    STRING  @ lpData,; 
    INTEGER @ lpcbData 
    
 DECLARE INTEGER RegCreateKeyEx IN advapi32; 
    INTEGER   hKey,; 
    STRING    lpSubKey,; 
    INTEGER   Reserved,; 
    STRING    lpClass,; 
    INTEGER   dwOptions,; 
    INTEGER   samDesired,; 
    INTEGER   lpSecurityAttributes,; 
    INTEGER @ phkResult,; 
    INTEGER @ lpdwDisposition        
    
 DECLARE LONG RegSetValueEx in ADVAPI32.DLL INTEGER hKey,STRING @lpValueName,LONG Reserved,LONG dwType,STRING @lpData, LONG cbData                          
    
 DECLARE INTEGER RegOpenKey in ADVAPI32.DLL INTEGER, STRING, INTEGER @nKeyHandle  

 DECLARE INTEGER RegCloseKey IN win32api INTEGER hKey
 
 #DEFINE HKEY_CURRENT_USER  0x80000001  
 #DEFINE HKEY_CLASSES_ROOT  0x80000000 
 #DEFINE SECURITY_ACCESS_MASK  983103
 
 * dichirazione/assegnamento variabili
  lpType=0
  lpcbData = 250 
  lpData = SPACE(lpcbData)  
  
  nKeyHandle = 0
  nKeyPos = 0
  ret = 0
  *Apro chiave contenente il programma per apertura pdf
  ret=RegOpenKey(HKEY_CLASSES_ROOT,"AcroExch.Document\Shell\open\command",@nKeyHandle)
  IF ret = 0
     *Leggo il valore predefinito della chiave
     ret = RegQueryValueEx(nKeyHandle,"",0,0,@lpdata,@lpcbData)
     IF ret = 0
       * Chiudo la chiave
       RegCloseKey(nKeyHandle)
       *Verifico se � installato acrobat 
       IF 'AcroRd32.exe' $ lpData
         * Estraggo il path in cui � salvato il file 'AcroRd32.exe'
         path_AR=ALLTRIM(SUBSTR(ALLTRIM(lpData),1,AT('%',ALLTRIM(lpData))-2))
         *Verifico presenza chiave 'printto'    
         ret=RegOpenKey(HKEY_CLASSES_ROOT,"AcroExch.Document\Shell\printto\command",@nKeyHandle)
         IF ret <> 0
           *Creo la chiave 'printto\command' 
           ret=RegCreateKeyEx(HKEY_CLASSES_ROOT,"AcroExch.Document\Shell\printto\command",0,'',0,SECURITY_ACCESS_MASK,0,@nKeyHandle,@nKeyPos)  
           IF ret = 0
             *Imposto il valore da assegnare alla nuova chiave
             path_comando='"'+ADDBS(path_AR)+'"'+'  /t "%1" "%2" "%3" "%4"'
             *Assegno il valore  alla chiave
             ret=RegSetValueEx(nKeyHandle,"",0,1,path_comando,LEN(path_comando))
           ENDIF 
         ENDIF
         * Chiudo la chiave
         RegCloseKey(nKeyHandle) 
       ENDIF   
     ENDIF               
  ELSE
    *Acrobat non trovato
    cp_Msg(cp_Translate(MSG_ACROBAT_NOT_FOUND),.f.)     
  ENDIF
  * Verifico se ci sono stati degli errori      
  IF ret <> 0
    cp_Msg(cp_Translate(MSG_ERROR_SET_REGISTRY),.f.)
  ELSE
    DO CASE 
      CASE VARTYPE(path_comando)='C'
         cp_Msg(cp_MsgFormat(MSG_SET_REGISTRY_OK,"HKEY_CLASSES_ROOT\AcroExch.Document\Shell\printto\command",path_comando),.f.)
      OTHERWISE
         cp_Msg(cp_MsgFormat(MSG_REGISTRY_OK,"HKEY_CLASSES_ROOT\AcroExch.Document\Shell\printto\command"),.f.)
    ENDCASE           
  ENDIF     
ENDPROC
* --- Zucchetti Aulla Fine - Controllo/Creazione chiave registro di sistema per invio Fax (Acrobat reader)

* --- Procedura per il calcolo delle date di modifica / carica
FUNCTION SetInfoDate(cCalUtd)
 Return( IIF(cCalUtd='A', CTOT(DTOC(i_datsys)+" "+TIME()), DATETIME()) )
ENDFUNC


*--- Zucchetti Aulla Inizio - OutputCombo
DEFINE CLASS cp_OutputCombo as Combobox
 cCursor = ""		  	&&Cursore contenente l'output utente
 cPrg = ""				&&Nome gestione
 nNumrep = 0			&&Report selezionato (OUROWNUM)
 nDefItem = 1			&&Report default
 oParentObject = .null. &&Obj form
 cDefSep = ""
 cNoDefSep = ""
 FontName = "Arial"
 FontItalic = .t.
 FontBold = .f.
 FontSize = 8
 Style=2
 bObbl = .f.				&&Campo obbligatorio (configurazione gestioni)
 
 PROCEDURE DefineVAr()
   WITH This 
     *--- Valorizzo nome gestione
     .oParentObject = .Parent.oContained
     *--- Se non presenti aggiungo sul padre le propriet�: w_OQRY, w_ONUME, w_OREP, w_ODES
     *--- w_OQRY
     IF VARTYPE(.oParentObject.w_OQRY)="U"
       .oParentObject.AddProperty("w_OQRY", "")
     ENDIF 
     *--- w_OREP
     IF VARTYPE(.oParentObject.w_OREP)="U"
       .oParentObject.AddProperty("w_OREP", "")
     ENDIF      
     *--- w_ODES
     IF VARTYPE(.oParentObject.w_ODES)="U"
       .oParentObject.AddProperty("w_ODES", "")
     ENDIF      
     *--- w_ONUME/ w_ONUM
	   IF G_application="ADHOC REVOLUTION"
	     IF VARTYPE(.oParentObject.w_ONUME)="U"
	       .oParentObject.AddProperty("w_ONUME", "")
	     ENDIF
	   ELSE
	     IF VARTYPE(.oParentObject.w_ONUM)="U"
	       .oParentObject.AddProperty("w_ONUM", "")
	     ENDIF
	   ENDIF   
	*--- Ref.
     IF TYPE("This.oParentObject.w_"+ALLTRIM(.Name))="U"
	       .oParentObject.AddProperty("w_"+ALLTRIM(.Name), This)
     ENDIF
   ENDWITH               
 endproc
 
 PROCEDURE InitCombo()
   WITH This
     .DefineVAr()
     .cPrg = IIF (TYPE(".oParentObject.w_NOMPRG")='C' , .oParentObject.w_NOMPRG  , UPPER(.oParentObject.cPrg) )
     .FillCombo()    
   ENDWITH
 ENDPROC 

 PROCEDURE FillCombo()
   LOCAL oldArea
   oldArea = SELECT()
   WITH This
     *--- Valorizzo cursore
     USE IN SELECT(.cCursor)
     .cCursor = SYS(2015)
     PRIVATE w_NOMPRG, w_CODUTE, w_CODAZI
     w_NOMPRG = UPPER(.cPrg)
     w_CODUTE = i_CODUTE
     w_CODAZI = i_CODAZI
     vq_exec("query\lookout.vqr", createobject('cp_getfuncvar'), .cCursor, '', .f., .t.)
     *--- Popolo elementi della combo
     LOCAL idx, idx_def
     idx=0
     idx_def=0
     if used(.cCursor)
        *--- Svuoto la lista
        .Clear()
        SELECT(.cCursor)
        locate for 1=1
        do while not(eof())
          *--- Se predefinito aggiungo l'immagine
          IF OUPREDEF = "S"
            .AddItem(this.cDefSep+ALLTRIM(OUDESCRI))
            idx_def = .ListCount
          ELSE 
            .AddItem(this.cNoDefSep+ALLTRIM(OUDESCRI))
          ENDIF
          SELECT(.cCursor)
          CONTINUE
        enddo
     ENDIF
     *-- Aggiungo voci fisse
     .AddItem("\-")
     .AddItem(cp_translate(MSG_OUTPUT_DETAILS))
     .AddItem(cp_translate(MSG_UPDATE_LIST))
     *--- Seleziono il report predefinito
     .ListIndex = idx_def
     .nDefItem = idx_def
     *--- Valorizza variabili del padre
     .FillProp(idx_def)
   ENDWITH
   SELECT(oldArea)
 endproc
 
 *--- Chiudo il cursore degli output
 PROCEDURE Destroy()
   USE IN SELECT(this.cCursor)
 ENDPROC
 
 PROCEDURE Valid()
   LOCAL nNumItem, nSelItem
   nSelItem = this.ListIndex
   nNumItem = this.ListCount
   DO CASE 
     CASE nSelItem = nNumItem
     *--- Aggiorna lista
       this.FillCombo()
     CASE nSelItem = nNumItem - 1
     *--- Dettagli Output...
       this.OpenOutput(this.nNumrep)
     CASE nSelItem = nNumItem - 2
       this.ListIndex = nSelItem - 1
       this.FillProp(nSelItem - 1)
     OTHERWISE 
     *--- Selezionato un report
       this.FillProp(nSelItem)  
   ENDCASE  
   this.oParentObject.NotifyEvent(this.caption+' Changed')
   this.oParentObject.mCalc(.t.)
   this.oParentObject.SaveDependsOn()      
 ENDPROC 
 
 PROCEDURE TrimText()
   LOCAL cSep, nSelItem, cText
   cText = this.DisplayValue
   nSelItem = this.ListIndex
   cSep = IIF(nSelItem = this.nDefItem, this.cDefSep, this.cNoDefSep)
   IF AT(cSep, this.DisplayValue)=1
     this.DisplayValue = SUBSTR(cText, LEN(cSep)+1)
   ENDIF
 endproc
 
 PROCEDURE FillProp(nItem)
 *--- Valorizza propriet� di oParentObject  
   this.oParentObject.w_OQRY = ""
   this.oParentObject.w_OREP = ""
   this.oParentObject.w_ODES = ""
   IF nItem <> 0
	   LOCAL oldArea
	   oldArea = SELECT()
	   SELECT(this.cCursor)
	   GOTO nItem
	   this.oParentObject.w_OQRY = ALLTRIM(NVL(OUNOMQUE,""))
	   this.oParentObject.w_OREP = ALLTRIM(NVL(OUNOMREP,""))
	   this.oParentObject.w_ODES = ALLTRIM(NVL(OUDESCRI,""))
	   * --- Revolution si appoggia alla variabile w_ONUME mentre ENTRERPRISE su w_ONUM
	   IF G_application="ADHOC REVOLUTION"
	 	   this.oParentObject.w_ONUME = NVL(OUROWNUM,0)
	   ELSE
		   this.oParentObject.w_ONUM = NVL(OUROWNUM,0)	   
	   ENDIF
	   this.nNumrep = NVL(OUROWNUM,0)
	   SELECT(oldArea) 
   ENDIF 
   *this.TrimText()
 ENDPROC 
 
 PROCEDURE OpenOutput(nItem)
 LOCAL numrow, obj
 obj=GSUT_MOU()
 IF obj.bSec1
 	obj.w_OUNOMPRG=UPPER(THIS.cPrg)
 	obj.querykeyset ("OUNOMPRG='"+UPPER(THIS.cPrg)+"'")
 	obj.loadrecwarn()
    *OpenGest("A", "GSUT_MOU", "OUNOMPRG", UPPER(this.cPrg))
 	numrow = i_curform.Search('t_OUROWNUM='+cp_tostrodbc(THIS.nNumrep))
 	IF numrow>0
 		i_curform.SetRow(numrow)
 		KEYBOARD "{TAB}"
 	ELSE
 		KEYBOARD "{DNARROW}"
 		KEYBOARD "{UPARROW}"
 	ENDIF
 	THIS.LISTINDEX = THIS.nDefItem
 	THIS.FillProp(THIS.nDefItem)
 ENDIF
 ENDPROC
 
 proc Event(cEvent)
   * --- Alla FormLoad definisco le variabili w_OQRY... per evenuali utilizzi nel metodo Blank
   IF AT("FormLoad", cEvent)<>0
      this.DefineVar()
   ENDIF   
 
 *--- All'evento blank riempio la combo, allo init della classe non posso farlo perch� oContained non � ancora valorizzato
   IF AT("Blank", cEvent)<>0
      this.InitCombo()
   ENDIF 
 endproc
   
 proc Calculate(xValue)
   IF !EMPTY(m.xValue) and Not(UPPER(ALLTRIM(this.cPrg))==UPPER(ALLTRIM(m.xValue)))
       this.DefineVAr()
	   this.cPrg=UPPER(m.xValue)
	   this.FillCombo()
   ENDIF
 endproc
 * --- Zucchetti Aulla - Numero di elementi nella lista di drop down della combo 
 PROCEDURE GotFocus()
   if vartype(g_DispCnt)='N'
      this.DisplayCount = g_DispCnt 
   endif 
 endproc
 * --- Zucchetti Aulla - Fine numero di elementi nella lista di drop down della combo 
    
ENDDEFINE


**********************************************************
*CLASSE USATA PER COMPILAZIONE DOCUMENTI WORD/OPEN OFFICE*
**********************************************************
DEFINE CLASS QueryParam as custom
  nomeattrib=''
  
    PROCEDURE AddParVal (NomePar,ValorePar)
         *definisco una nuova propriet� il cui nome � contenuto in 'NomePar'     
         this.nomeattrib = 'w_'+UPPER(alltrim(NomePar))
         *Assegno al nuovo attributo il rispettivo valore
         this.addproperty(this.nomeattrib,ValorePar)
    ENDPROC    
    
    FUNC IsVar(cVarName)
    return(type("this."+cVarName)<>'U')
        
    FUNC GetVar(cVarName)
    return(this.&cVarName)
    
ENDDEFINE

**********************************************************
**       CLASSE UTILIZZATA PER SCRITTURA ENTITA'        **
**********************************************************
DEFINE CLASS EntityWriter as custom
	DIMENSION PropList(1,3)
	*Oggetti aggiuntivi
	*	Descrizione posizioni:
	*		1	=	Nome oggetto (Es. w_MOVIMATR)
	*		2	=	Stringa inizializzazione (Es. createobject("cp_MemoryCursor",".\query\movimatr.vqr",this.oParentObject))
	*		3	=	Tipo oggetto (Attualmente deve essere forzato a "O" )
	*		4	=	Nome oggetto di dipendenza (Es. per w_MOVIMATR specificando w_DOC_DETT al termine della scrittura di riga verr� lanciata la procedura associata)
	*		5	=	Routine di gestione da eseguire alla scrittura dell'oggetto di riferimento (Es. GSXX_BYY(this.w_PADRE))
	DIMENSION AdditionalPropList(1,5)
	
	*Collegamento tabelle e relativo cursore per letture
	* 1 = Nome tabella
	* 2 = Nome cursore
	DIMENSION WorkTables(1,2)
	oParentObject = .Null.
	cTableName = ""
	
	proc Init(pParent, cTableName)
		local L_IdxProp, cMacro, L_IdxFld, L_FldType, L_FldLen, L_FldDec
		this.oParentObject = pParent
		this.cTableName = m.cTableName
		If !EMPTY(m.cTableName)
			cp_ReadXdc()
			L_IdxProp=ALEN(this.PropList,1)
			If L_IdxProp=1 and VARTYPE(this.PropList(1,1))='L'
				L_IdxProp = L_IdxProp - 1
			EndIf
			*Inizializzo il vettore delle propriet� con i campi della tabella principale
			For L_IdxFld=1 to i_dcx.GetFieldsCount(m.cTableName)
				L_IdxProp = L_IdxProp + 1
				DIMENSION this.PropList(L_IdxProp,3)
				this.PropList(L_IdxProp,1) = "w_"+ALLTRIM(i_dcx.GetFieldName(m.cTableName, L_IdxFld))
				L_FldType = I_dcx.GetFieldType(m.cTableName, L_IdxFld)
				L_FldLen = I_dcx.GetFieldLen(m.cTableName, L_IdxFld)
				L_FldDec = I_dcx.GetFieldDec(m.cTableName, L_IdxFld)
				DO CASE
					CASE m.L_FldType='C'
						*Char
						this.PropList(L_IdxProp,2) = "Space("+ALLTRIM(STR(m.L_FldLen,200,0))+")"
					CASE m.L_FldType='N'
						*Numeric
						if UPPER(this.PropList(L_IdxProp,1)) = 'W_UTCC'
							this.PropList(L_IdxProp,2) = "i_CodUte"
						Else
							this.PropList(L_IdxProp,2) = "0"
						EndIf
					CASE m.L_FldType='D'
						*Date
						if UPPER(this.PropList(L_IdxProp,1)) = 'W_UTDC'
							this.PropList(L_IdxProp,2) = "i_DatSys"
						Else
							this.PropList(L_IdxProp,2) = "cp_CharToDate('  -  -    ')"
						EndIf
					CASE m.L_FldType='L'
						*Logical
						this.PropList(L_IdxProp,2) = ".F."
					CASE m.L_FldType='M'
						*Memo
						this.PropList(L_IdxProp,2) = ".Null."
					CASE m.L_FldType='T'
						this.PropList(L_IdxProp,2) = "CTOT('')"
				ENDCASE
				this.PropList(L_IdxProp,3) = "F"
			EndFor
			For L_IdxProp=1 to ALEN(this.PropList, 1)
				If this.PropList(L_IdxProp,3) $ 'FW'
					cMacro = this.PropList(L_IdxProp,2)
					this.AddProperty(this.PropList(L_IdxProp,1), &cMacro)
				Else
					cMacro = this.PropList(L_IdxProp,2)
					this.AddProperty(this.PropList(L_IdxProp,1), &cMacro)
				EndIf
			EndFor
		EndIf
	endproc
	
	Proc AddAdditionalProp
		LPARAMETERS cObjName, cInitStr, cObjType, cObjDep, cRoutineName
		If VARTYPE(this.AdditionalPropList(ALEN(this.AdditionalPropList,1),1))<>"L"
			DIMENSION this.AdditionalPropList(ALEN(this.AdditionalPropList,1)+1,5)
		EndIf
		this.AdditionalPropList(ALEN(this.AdditionalPropList,1),1) = ALLTRIM(m.cObjName)
		this.AdditionalPropList(ALEN(this.AdditionalPropList,1),2) = ALLTRIM(m.cInitStr)
		this.AdditionalPropList(ALEN(this.AdditionalPropList,1),3) = ALLTRIM(m.cObjType)
		this.AdditionalPropList(ALEN(this.AdditionalPropList,1),4) = ALLTRIM(m.cObjDep)
		this.AdditionalPropList(ALEN(this.AdditionalPropList,1),5) = ALLTRIM(m.cRoutineName)
		cMacro = this.AdditionalPropList(ALEN(this.AdditionalPropList,1),2)
		this.AddProperty(this.AdditionalPropList(ALEN(this.AdditionalPropList,1),1), &cMacro)
	EndProc
	
	Proc Blank
		local L_IdxProp, cMacro, cAddObj
		For L_IdxProp=1 to ALEN(this.PropList, 1)
			If this.PropList(L_IdxProp,3) $ "FW"
				cMacro = "this."+this.PropList(L_IdxProp,1)+" = "+ this.PropList(L_IdxProp,2)
				&cMacro
			Else
				cMacro = this.PropList(L_IdxProp,1)+" = .Null."
				&cMacro
				cAddObj = this.PropList(L_IdxProp,2)
				cMacro = "this."+this.PropList(L_IdxProp,1)
				&cMacro  =  &cAddObj
			endif
		EndFor
		If VARTYPE(this.AdditionalPropList(ALEN(this.AdditionalPropList,1),1))<>"L"
			For L_IdxProp=1 to ALEN(this.AdditionalPropList,1)
				cMacro = this.AdditionalPropList(ALEN(this.AdditionalPropList,1),1)+" = .Null."
				&cMacro
				cAddObj = this.AdditionalPropList(ALEN(this.AdditionalPropList,1),2)
				cMacro = "this."+this.AdditionalPropList(ALEN(this.AdditionalPropList,1),1)
				&cMacro  =  &cAddObj
			EndFor
		EndIf
	EndProc
	
	Proc ReadTable
		*Arrval deve essere passato per riferimento
		LPARAMETERS cTableName, cReadFld, ArrVal
		Local nCursor, bRecFound, cCursor, nOldArea, cWhere, xRetVal, cNewCurs
		nOldArea = SELECT()
		bRecFound = .F.
		nCursor = ASCAN(this.WorkTables, ALLTRIM(m.cTableName), 1, ALEN(this.WorkTables, 1), 1, 15)
		If m.nCursor>0
			cCursor = this.WorkTables(m.nCursor, 2)
			cWhere = CreaWhere(@m.ArrVal,.t.)
			cNewCurs = "AW"+SYS(2015)
			SELECT(m.cCursor)
			GO TOP
			* Non si pu� utilizzare la locate for poich� da errore con where contenti like, ecc.
			SELECT * FROM ( m.cCursor ) WHERE &cWhere INTO CURSOR (m.cNewCurs)
			If USED(m.cNewCurs) AND RECCOUNT(m.cNewCurs)>0
				m.bRecFound = .T.
				xRetVal = &cReadFld
			EndIf
			USE IN SELECT(m.cNewCurs)
			cNewCurs = ""
		EndIf
		If !m.bRecFound
			cNewCurs = ReadTable( m.cTableName , "*" , @ArrVal )
			If EMPTY(m.cNewCurs)
				*Record non trovato
				m.bRecFound = .F.
				xRetVal = ""
			Else
				m.bRecFound = .T.
				SELECT (m.cNewCurs)
				GO TOP
				xRetVal = &cReadFld
				If m.nCursor>0
					* Cursore gi� in uso lo accodo
					SELECT * FROM (this.WorkTables(m.nCursor, 2)) UNION SELECT * FROM (m.cNewCurs) INTO CURSOR (this.WorkTables(m.nCursor, 2))
					USE IN SELECT(m.cNewCurs)
				Else
					*Cursore nuovo lo aggiungo alle worktable
					SELECT * FROM (m.cNewCurs) INTO CURSOR "AW"+m.cNewCurs
					USE IN SELECT (m.cNewCurs)
					cNewCurs = "AW"+m.cNewCurs
					If VARTYPE(This.WorkTables(1,1))<>'L'
						*Ridimensiono l'array
						DIMENSION This.WorkTables( ALEN(This.WorkTables,1)+1 , 2 )
					EndIf
					This.WorkTables( ALEN(This.WorkTables,1) ,1) = m.cTableName
					This.WorkTables( ALEN(This.WorkTables,1) ,2) = m.cNewCurs
				EndIf
			EndIf
		ENDIF
		* Normalizzo risultato
		L_FldType = I_dcx.GEtfieldtype(Alltrim(m.cTableName),I_DCX.GETfieldidx(Alltrim(m.cTableName),Alltrim(m.cReadFld)))
		DO CASE
			CASE L_FldType='C' OR L_FldType='M'
			*Char\Memo
			  xRetVal=NVL(xRetVal,' ')
			CASE L_FldType='N'
			*Numeric
			  xRetVal=NVL(xRetVal,0)
			CASE L_FldType='D'
			*Date			
			  xRetVal=NVL(xRetVal,cp_CharToDate('  -  -    '))
			CASE L_FldType='L'
			*Logical
              xRetVal=NVL(xRetVal,.f.)
			CASE L_FldType='T'
			 *Datetime
			 xRetVal=NVL(xRetVal,CTOT(''))
			ENDCASE
		Select (m.nOldArea)
		return xRetVal
	EndProc
	
	Proc SelectTable
		*Arrval deve essere passato per riferimento
		LPARAMETERS cTableName, cReadFlds, ArrVal
		Local nRecTable, cWhere, nCursor, cNewCursor, nOldArea
		nOldArea = SELECT()
		*Individuo i record della tabella o eventualmente li estraggo dal database
		nRecTable = this.ReadTable( m.cTableName, "RECCOUNT()", @ArrVal )
		nRecTable = IIF(VARTYPE(m.nRecTable)<>'N', -1 , m.nRecTable)
		m.cNewCursor = ""
		if nRecTable>0
			cWhere = CreaWhere(@m.ArrVal)
			nCursor = ASCAN(this.WorkTables, ALLTRIM(m.cTableName), 1, ALEN(this.WorkTables, 1), 1, 15)
			cNewCursor = "AW"+SYS(2015)
			cReadFlds = IIF(VARTYPE(m.cReadFlds)<>'C' OR EMPTY(m.cReadFlds), "*", m.cReadFlds)
			SELECT &cReadFlds FROM ( this.WorkTables(m.nCursor, 2) ) WHERE &cWhere INTO CURSOR (m.cNewCursor)
			If !USED(m.cNewCursor)
				m.cNewCursor = ""
			EndIf
		Endif
		Select (m.nOldArea)
		Return m.cNewCursor 
	EndProc
	
	Proc Destroy
		local L_IdxProp, cMacro
		*Elimino tutti gli oggetti (MemoryCursor, Ecc.)
		For L_IdxProp=1 to ALEN(this.PropList, 1)
			If this.PropList(L_IdxProp,3)="O"
				cMacro = this.PropList(L_IdxProp,1)+" = .Null."
				&cMacro
			endif
		EndFor
		*Elimino tutti gli oggetti aggiuntivi
		If VARTYPE(this.AdditionalPropList(ALEN(this.AdditionalPropList,1),1))<>"L"
			For L_IdxProp=1 to ALEN(this.AdditionalPropList,1)
				cMacro = this.AdditionalPropList(ALEN(this.AdditionalPropList,1),1)+" = .Null."
				&cMacro
			EndFor
		EndIf
		*Chiudo tutti i cursori aperti per la lettura delle tabelle
		For L_IdxProp=1 to ALEN(This.WorkTables,1)
			If VARTYPE( This.WorkTables(m.L_IdxProp,1) )<>'L'
				cMacro = This.WorkTables(m.L_IdxProp,2)
				USE IN SELECT(m.cMacro)
			EndIf
		EndFor
	EndProc
	
	Proc SetControlsValue
		*Richiamata al termine di alcune funzionalit� CodePainter
		*ad esempio calcolo progressivi
		return
	EndProc
	
	Proc SetProperyValues
		LPARAMETERS oChildren, cDest, cOrig
		Local L_PropAtt, L_NumProp, cMacro,cAssVar
		L_NumProp = ALEN(This.PropList, 1)
		OLD_ERR= ON('ERROR')
		ON ERROR cAssVar=.T.
		For L_PropAtt = 1 to m.L_NumProp
			If This.PropList(m.L_PropAtt,3) $ "FW"
				cMacro = ALLTRIM(m.cDest)+"."+ALLTRIM(This.PropList(m.L_PropAtt,1))+" = " +IIF(VARTYPE(oChildren)='O',ALLTRIM(m.cOrig)+".","")+ALLTRIM(This.PropList(m.L_PropAtt,1))
				&cMacro
			EndIf
		ENDFOR
		ON ERROR &OLD_ERR
	EndProc
	
	Proc SetLocalValues
		LPARAMETERS oChildren
		LOCAL cAssVar
		OLD_ERR= ON('ERROR')
		ON ERROR cAssVar=.T.
		this.SetProperyValues(m.oChildren, "oChildren","this")
		ON ERROR &OLD_ERR
	EndProc
	
	Proc SetObjectValues
		LPARAMETERS oChildren
		this.SetProperyValues(m.oChildren, "this", "oChildren")
	EndProc
	
ENDDEFINE


**********************************************************
**       CLASSE UTILIZZATA PER SCRITTURA ATTIVITA       **
**********************************************************
DEFINE CLASS ActivityWriter as EntityWriter
	DIMENSION PropList(9,3)
		PropList(1,1)="w_DATINI"
		PropList(1,2)="cp_CharToDate('  -  -    ')"
		PropList(1,3)="W"
		PropList(2,1)="w_ORAINI"
		PropList(2,2)="SPACE(2)"
		PropList(2,3)="W"
		PropList(3,1)="w_MININI"
		PropList(3,2)="SPACE(2)"
		PropList(3,3)="W"
		PropList(4,1)="w_DATFIN"
		PropList(4,2)="cp_CharToDate('  -  -    ')"
		PropList(4,3)="W"
		PropList(5,1)="w_ORAFIN"
		PropList(5,2)="SPACE(2)"
		PropList(5,3)="W"
		PropList(6,1)="w_MINFIN"
		PropList(6,2)="SPACE(2)"
		PropList(6,3)="W"
		* ATDATINI e ATDATFIN sono i dati che verrano inseriti nel database
		* se non vengono passati vengono popolati con DATINI e DATFIN
		* altrimenti inizializzati con data/ora correnti
		PropList(7,1)="w_OFF_PART"
		PropList(7,2)='createobject("cp_MemoryCursor",".\query\ActivityPart.vqr",this.oParentObject)'
		PropList(7,3)="O"
		PropList(8,1)="w_COM_ATTI"
		PropList(8,2)='createobject("cp_MemoryCursor",".\query\ActivityComp.vqr",this.oParentObject)'
		PropList(8,3)="O"
		PropList(9,1)="w_OFFDATTI"
		PropList(9,2)='createobject("cp_MemoryCursor",".\query\ActivityDetail.vqr",this.oParentObject)'
		PropList(9,3)="O"
	bCheckBeforeCreation = .T.
	cCreationRoutine = "GSAG_BAC(.Null., this)"
	cCreationRoutinePre = "GSAG_BGP(.Null., this)"
	* genera prestazioni per le parti 
	bParass=.f.
	cCodant=' '
	cCodspe=' '
	nImpant=0
	nImpspe=0
	cDesant=' '
	cDesspe=' '
	bCreateDoc = .T.
	cDocCreationRoutine = "GSAG_BGE(this.w_PADRE,w_PADRE.w_ATSERIAL,space(10),space(15),0,.f.,'D')"
	bSendMSG = .T.
	bNoMSG = .F.
	cSendMSGRoutine = "GSAG_BAP(this.w_PADRE, 'C', this.w_PADRE.w_ATSERIAL,this.w_PADRE.bNoMSG)"
	dStartDateTime = null 	&& Determinazione data inizio attivit� tramite calendari partecipanti (datetime)
	cStartDateTimeError = ''&& Determinazione data inizio attivit� tramite calendari partecipanti (errore incontrato) 
	bSospForensi = .f. 		&& Determinazione data inizio attivit� se a .t. considera anche le sospensioni forensi
	cGetFirstDay = "GSAG_BGF( null, this )"
	bCalcPrezzo = .F.  && Ricalcolo o meno del prezzo della prestazione, rileggendo tutti i dati necessari
	bCalcCosto = .F.   && Ricalcolo del costo interno, rileggendo tutti i dati necessari
	bRicTar=.F.      && Ricalcola le tariffe valore minimo e massimo
	bTarCon=.f.  && Esegue lettura tariffa concordata 
	*La procedura init nonesegue nulla se non la Init di default
	*Lasciata scopo documentativo per richiamare correttamente la
	*DoDefault()
	proc Init(pParent, cTableName)
		local L_IdxProp, cMacro
		DoDefault(m.pParent, m.cTableName)
	endproc
	
	Proc CreateActivity
		local cMacro, cRetVal
		this.SyncDate()
		cRetVal = ""
		cMacro = "cRetVal = "+ALLTRIM(this.cCreationRoutine)
		&cMacro
		return m.cRetVal
	ENDPROC
	Proc CreatePrest
		local cMacro, cRetVal
		cRetVal = ""
		cMacro = "cRetVal = "+ALLTRIM(this.cCreationRoutinePre)
		&cMacro
		return m.cRetVal
	EndProc
	
	Proc SyncDate
		if EMPTY(this.w_ATDATINI)
			If !EMPTY(this.w_DATINI)
				this.w_ATDATINI = DATETIME( YEAR(this.w_DATINI), MONTH(this.w_DATINI) , DAY(this.w_DATINI), VAL(this.w_ORAINI), VAL(this.w_MININI), 0 )
			EndIf
		else
			this.w_DATINI = TTOD(this.w_ATDATINI)
			this.w_ORAINI = RIGHT("00"+ALLTRIM( STR( HOUR( this.w_ATDATINI ) ) ) , 2 )
			this.w_MININI = RIGHT("00"+ALLTRIM( STR( MINUTE( this.w_ATDATINI ) ) ) , 2 )
		endif
		if EMPTY(this.w_ATDATFIN)
			If !EMPTY(this.w_DATFIN)
				this.w_ATDATFIN = DATETIME( YEAR(this.w_DATFIN), MONTH(this.w_DATFIN) , DAY(this.w_DATFIN), VAL(this.w_ORAFIN), VAL(this.w_MINFIN), 0 )
			EndIf
		else
			this.w_DATFIN = TTOD(this.w_ATDATFIN)
			this.w_ORAFIN = RIGHT("00"+ALLTRIM( STR( HOUR( this.w_ATDATFIN ) ) ) , 2 )
			this.w_MINFIN = RIGHT("00"+ALLTRIM( STR( MINUTE( this.w_ATDATFIN ) ) ) , 2 )
		endif
	Endproc
	
	Proc AddPart
		LPARAMETERS nCPROWNUM, nCPROWORD, cTIPRIS, cCODRIS, cGRURIS
		* I Parametri CPROWNUM e CPROWORD sono opzionali, passarli a 0 (Zero) per farli calcolare in automatico
		With this.w_OFF_PART
			.AppendBlank()
			.CPROWNUM = m.nCPROWNUM
			.CPROWORD = m.nCPROWORD
			.PATIPRIS = m.cTIPRIS
			.PACODRIS = m.cCODRIS
			.PAGRURIS = m.cGRURIS
			.SaveCurrentRecord()
		EndWith
	EndProc
	
	Proc AddComp
		LPARAMETERS nCPROWORD, cCODIMP, cCODPIM
		* Il Parametro CPROWORD � opzionale, passarlo a 0 (Zero) per farlo calcolare in automatico
		With this.w_COM_ATTI
			.AppendBlank()
			.CPROWORD = m.nCPROWORD
			.CACODIMP = m.cCODIMP
			.CACODCOM = m.cCODPIM
			.SaveCurrentRecord()
		EndWith
	EndProc
	
	Proc AddDetail
		LPARAMETERS nCPROWNUM, nCPROWORD, cDACODRES, cDACODATT, cDADESATT, cDAUNIMIS, nDAQTAMOV, nDAPREZZO, cDATIPRIG, nDACODOPE, dDADATMOD, cDAFLDEFF, cDA_SEGNO, cDATIPRI2
		* I Parametri CPROWNUM e CPROWORD sono opzionali, passarli a 0 (Zero) per farli calcolare in automatico
		With this.w_OFFDATTI
			.AppendBlank()
			.CPROWNUM = m.nCPROWNUM
			.CPROWORD = m.nCPROWORD
			.DACODRES = m.cDACODRES
			IF Isahe()
			 .DACODICE = m.cDACODATT			
			ELSE
			.DACODATT = m.cDACODATT			
			endif
			.DADESATT = m.cDADESATT
			.DAUNIMIS = m.cDAUNIMIS
			.DAQTAMOV = m.nDAQTAMOV
			.DAPREZZO = m.nDAPREZZO
			.DATIPRIG = m.cDATIPRIG
			.DACODOPE = m.nDACODOPE
			.DADATMOD = m.dDADATMOD
			.DAFLDEFF = m.cDAFLDEFF
			.DA_SEGNO = m.cDA_SEGNO
			.DATIPRI2 = m.cDATIPRI2
			.SaveCurrentRecord()
		EndWith
	EndProc
	
	* valorizz con il primo giorno libero in base ai calendari dei partecipanti all'attivit�
	* le propriet� dStartDate, dStartHour e dStartMinute
	PROCEDURE GetFirstDay()
	 	LOCAL l_macro
	 	l_macro = this.cGetFirstDay		
		&L_macro
	EndProc
		
ENDDEFINE
**********************************************************
**       CLASSE UTILIZZATA PER SCRITTURA PRESTAZIONI       **
**********************************************************
DEFINE CLASS PrestWriter as EntityWriter
	bCheckBeforeCreation = .T.
	cCreationRoutine = "GSAG_BAC(.Null., this)"
	cCreationRoutinePre = "GSAG_BGP(.Null., this)"
	* genera prestazioni per le parti
	bParass=.f.
	cCodant=' '
	cCodspe=' '
	nImpant=0
	nImpspe=0
	cDesant=' '
	cDesspe=' '
	bCreateDoc = .T.
	cDocCreationRoutine = "GSAG_BGE(this.w_PADRE,w_PADRE.w_ATSERIAL,space(10),space(15),0,.f.,'D')"
	bSendMSG = .T.
	bNoMSG = .F.
	cSendMSGRoutine = "GSAG_BAP(this.w_PADRE, 'C', this.w_PADRE.w_ATSERIAL,this.w_PADRE.bNoMSG)"
	bCalcPrezzo = .F.  && Ricalcolo o meno del prezzo della prestazione, rileggendo tutti i dati necessari
	bCalcCosto = .F. && Ricalcolo del costo interno, rileggendo tutti i dati necessari
	bRicTar =.F. && Ricalcolo tariffe valore minimo e massimo
	bTarCon=.f.  && Esegue lettura tariffa concordata 
	*La procedura init nonesegue nulla se non la Init di default
	*Lasciata scopo documentativo per richiamare correttamente la
	*DoDefault()
	proc Init(pParent, cTableName)
		local L_IdxProp, cMacro
		DoDefault(m.pParent, m.cTableName)
	endproc
	Proc CreatePrest
		local cMacro, cRetVal
		cRetVal = ""
		cMacro = "cRetVal = "+ALLTRIM(this.cCreationRoutinePre)
		&cMacro
		return m.cRetVal
	EndProc
ENDDEFINE
**********************************************************
**       CLASSE UTILIZZATA PER SCRITTURA DOCUMENTI      **
**********************************************************
DEFINE CLASS DocumWriter as EntityWriter
	DIMENSION PropList(4,3)
		PropList(1,1)="w_DOC_RATE"
		PropList(1,2)='createobject("cp_MemoryCursor",".\query\docrate.vqr",this.oParentObject)'
		PropList(1,3)="O"
		PropList(2,1)="w_CON_PAGA"
		PropList(2,2)='createobject("cp_MemoryCursor",".\query\conpaga.vqr",this.oParentObject)'
		PropList(2,3)="O"
		PropList(3,1)="w_VDATRITE"
		PropList(3,2)='createobject("cp_MemoryCursor",".\query\vdatrite.vqr",this.oParentObject)'
		PropList(3,3)="O"
		PropList(4,1)="w_DOC_DETT"
		PropList(4,2)='createobject("cp_MemoryCursor",".\query\docdett.vqr",this.oParentObject)'
		PropList(4,3)="O"					
	
	bCheckBeforeCreation = .T.
	bCheckAfterCreation = .T.
	bNOCreateRates=.F.
	bNOcalciva=.F.
	bNOcalcbody=.F.
	bCalcsal=.T.
	bCalctot=.T.
	bCalcRit=.F.
	bRicSpe=.F.
	bWarning=.F.
	btrsok=.F.
	bNOcalcFido = .F.
	bEvasDoc = .F.
	bRicPro=.F.
	cCalcsalRoutine="FLR_DOCUM( @l_arrkey, 'Post', 'I', @bTrsOk)"
	cCalctotRoutine="GSAR_BRD(This,this.w_PADRE.w_MVSERIAL,this.w_PADRE.bNOcalciva,this.w_PADRE.bNOCreateRates,this.w_PADRE.bNOcalcbody,.f.,this.w_PADRE.bCalcRit,this.w_PADRE.bRicSpe,this.w_PADRE.bRicPro)"
	cAfterCheckRoutine="GSAR_BAD(This,'I',@l_arrkey,.NULL.,@bTrsOk)"
	cCreationRoutine = "GSAR_BGD(.Null.,this)"
	* tipo controlli riga piena
	* C=completa dati
	* D= elimina riga che non soddisfa la condizione di riga piena
	* E= riporta errore ed esci senza creare documento
	cCheckRowType='C'
	* propiet� utilizzate per passare all'oggetto 
	* causale documento di acquisto	
	cCauAcquisti=' '
	* centro di costo documento di acquisto	
	cCencosto=' '
	* commessa documento di acquisto	
	cComcosto=' '
	* attivit� documento di acquisto	
	cAttcosto=' '
	* listino documento di acquisto	
	cLisAcquisti=' '
	* Data inizio competenza documento di acquisto
	dIniCom=' '
	*Data fine competenza documento di acquisto
	dFinCom=' '
	* ritornare seriale documento di acquisto
	cSeracquisti=' '
	cRetLogWarn='N' && ritorna warning dal generatore 
	*Array per i messaggi di ritorno dai batch collegati
	DIMENSION aLogRetMsg(1,2)
	nTotLogMsg = 0
	* bNoCalcPrz = .T. : Non esegue il calcolo dei prezzi da listino/contratto
    * bNoCalcPrz = .F. : Esegue il calcolo dei prezzi da listino/contratto (solo se MVPREZZO=0)
    bNoCalcPrz = .T.
	
	proc Init(pParent, cTableName)
		local L_IdxProp, cMacro
		DoDefault(m.pParent, m.cTableName)
	ENDPROC
	
	Proc AddLogMsg
		LPARAMETERS cMsgType, cMsg
		If VARTYPE(this.aLogRetMsg(ALEN(this.aLogRetMsg,1),1))<>'L' OR !EMPTY(this.aLogRetMsg(ALEN(this.aLogRetMsg,1),1))
			DIMENSION this.aLogRetMsg(ALEN(this.aLogRetMsg,1)+1,2)
		EndIf
		this.aLogRetMsg(ALEN(this.aLogRetMsg,1),1)=m.cMsgType
		this.aLogRetMsg(ALEN(this.aLogRetMsg,1),2)=m.cMsg
		this.nTotLogMsg = this.nTotLogMsg + 1
	EndProc
	
	Proc ResetLogMsg
		DIMENSION this.aLogRetMsg(1,2)
		this.aLogRetMsg(1,1)=.F.
		this.aLogRetMsg(1,2)=.F.
		this.nTotLogMsg = 0
	EndProc
	
	Proc AddPropFido
		This.AddProperty( "w_TOTFATTU", 0)
		This.AddProperty( "w_TOTIMPOS", 0)
		This.AddProperty( "w_CAONAZ", 0)
		This.AddProperty( "w_DECTOP", 0)
		This.AddProperty( "w_FIDO4", 0)
		This.AddProperty( "w_FIDO5", 0)
		This.AddProperty( "w_FIDO6", 0)
		This.AddProperty( "w_FLRISC", "N")
		This.AddProperty( "w_MAXFID", 0)
		This.AddProperty( "w_cFUNC", "Load")
	EndProc
	
	Proc AddCon
		LPARAMETERS nCPIMPPRE,cCPCONPRE,nCPIMPCON,cCPCONCON,nCPIMPASS,cCPCONASS,nCPIMPCAR,cCPCONCAR,nCPIMPFIN,cCPCONFIN 
		With this.w_CON_PAGA
			.AppendBlank()
			.CPIMPPRE = m.nCPIMPPRE
            .CPIMPASS = m.nCPIMPASS
			.CPIMPCON = m.nCPIMPCON
            .CPIMPCAR = m.nCPIMPCAR           
 			.CPIMPFIN = m.nCPIMPFIN
            .CPCONPRE = m.cCPCONPRE
            .CPCONASS = m.cCPCONASS 
            .CPCONCON = m.cCPCONCON
            .CPCONCAR = m.cCPCONCAR 
            .CPCONFIN = m.cCPCONFIN                                  
			.SaveCurrentRecord()
		EndWith
	ENDPROC
	
	Proc AddRate
		LPARAMETERS nRSNUMRAT, dRSDATRAT, nRSIMPRAT, cRSMODPAG,cRSFLSOSP
		* Il Parametro RSNUMRAT � opzionale, passarlo a 0 (Zero) per farlo calcolare in automatico
		With this.w_DOC_RATE
			.AppendBlank()
			.RSNUMRAT = m.nRSNUMRAT
			.RSDATRAT = m.dRSDATRAT
			.RSIMPRAT = m.nRSIMPRAT
			.RSMODPAG = m.cRSMODPAG
			.RSFLSOSP = m.cRSFLSOSP		
			.SaveCurrentRecord()
		EndWith
	ENDPROC
	
	Proc AddRite
		LPARAMETERS nCPROWNUM, nDRSOMESC, nDRFODPRO,cDRCODTRI,cDRCODCAU,nDRNONSOG,nDRIMPONI,nDRRITENU,nDRRIINPS,nDRPERRIT 
		* Il Parametro CPROWNUM � opzionale, passarlo a 0 (Zero) per farlo calcolare in automatico
		With this.w_VDATRITE
			.AppendBlank()
			.CPROWNUM = m.nCPROWNUM
			.DRSOMESC = m.nDRSOMESC
			.DRFODPRO = m.nDRFODPRO
			.DRCODTRI = m.cDRCODTRI
			.DRCODCAU = m.cDRCODCAU
			.DRNONSOG = m.nDRNONSOG
			.DRIMPONI = m.nDRIMPONI
			.DRRITENU = m.nDRRITENU
			.DRRIINPS = m.nDRRIINPS	
			.DRPERRIT = m.nDRPERRIT					
			.SaveCurrentRecord()
		EndWith
	ENDPROC
	
	Proc AddDetail
		LPARAMETERS nCPROWNUM, nCPROWORD, cMVCODICE, cMVUNIMIS,nMVQTAMOV,nMVPREZZO,cMVTIPRIG 
		* I Parametri CPROWNUM e CPROWORD sono opzionali, passarli < 0 (Zero) per farli calcolare in automatico
		* utile per legare le righe con il dettaglio matricole in modo da tenerli coordinati in
		* caso di aggiunta di righe (ad esempio righe descrittive riferimento per evasione documentale)
		With this.w_DOC_DETT
			.AppendBlank()
			.CPROWNUM = m.nCPROWNUM
			.CPROWORD = m.nCPROWORD
			.MVCODICE = m.cMVCODICE
			.MVUNIMIS = m.cMVUNIMIS
			.MVQTAMOV = m.nMVQTAMOV
			.MVUNIMIS = m.cMVUNIMIS
			.MVQTAMOV = m.nMVQTAMOV
			.MVPREZZO = m.nMVPREZZO
			.MVTIPRIG = m.cMVTIPRIG
			.SaveCurrentRecord()
		EndWith
	EndProc
	
	Proc CreateDocument
		local cMacro, cRetVal
		cRetVal = ""
		cMacro = "cRetVal = "+ALLTRIM(this.cCreationRoutine)
		&cMacro
		return m.cRetVal
	EndProc
ENDDEFINE

#Define TOOLBAR_BORDERCOLOR 55
#Define TOOLBAR_BTN_HOVER_BORDER	62
#Define OVERFLOWPANEL_MENUBUTTON_PICTURE	16

Define Class ButtonMenu As Container
    BorderWidth = 0
    BorderColor=0
    BackStyle = 0
    Anchor = 0
    Width =  45
    Height = 23
    Top = 0
    Left = 0
    Caption=''
    FontName=''
    FontSize=0
    FontBold=.f.
    FontItalic=.f.
    FontUnderline=.f.
    FontStrikeThru=.f.
    ToolTipText=''
    IsExtended=.f.
    ExtendedCommand=''
    PictureEnabled=''
    PictureDisabled=''
    BackgroundDisable=.F.
    PictureBackgroundDisable=""
    PictureBackground=""
	disabledpicture=''
    bGlobalFont=.t.
	ImgSize = i_nBtnImgSize	&&Dimenisione bmp
	NoImgAllign = 1 &&Allignamento in caso di g_NoButtonImage; 0 = Top, 1 = Bottom	

 Add Object ImgBackGroundOver As Image With ;
        Top = 1, Left = 1,;
        Width = 44, Height = 22, ;
        Visible = .F., BackStyle=1 ,BorderWidth = 0 , stretch=2

 Add Object ImgBackGround As Image With ;
        Top = 1, Left = 1,;
        Width = 44, Height = 22, ;
        Visible = .T., BackStyle=1 ,BorderWidth = 0 , stretch=2

 Add Object ImgButton As Image With ;
        Top = 0, Left = 2,;
        Width = 45, Height = 24, ;
        Visible = .F., BackStyle=0 ,BorderWidth = 1
		
*Il bottone deve essere prima della label altrimenti i tasti di scelta rapida (short-key) funzionano al secondo tentativo
 Add Object HiddenButton As CommandButton With ;
        Top = 0, Left = 0,;
        Width = 45, Height = 22, BorderWidth = 0,;
        Visible = .T., BackStyle=0, style=1

 ADD OBJECT LblButton as label with;
  		  Top=24, left = 0, caption='',;
        Width = 45, Height = 12, ;
        Visible = .T., BackStyle=0  ,BorderWidth = 1,;
        Alignment=2

PROCEDURE Init
	If Pemstatus(This, "CpPicture", 5)
		This.Picture = This.CpPicture
	Else
		this.picture=this.picture
	Endif
	this.Width=this.Width
	this.caption=STRTRAN(ALLTRIM(this.caption),'','\<')

	This.FontName =SetFontName('B', This.FontName,This.bGlobalFont)
	This.FontSize = SetFontSize('B', This.Fontsize,This.bGlobalFont)  
	This.FontBold=SetFontBold('B', This.FontBold,This.bGlobalFont)
	This.FontItalic=SetFontItalic('B', This.FontItalic,This.bGlobalFont)
	This.FontUnderline=This.FontUnderline
	This.FontStrikeThru=This.FontStrikeThru
	This.ToolTipText=This.ToolTipText
	this.parent.BorderColor=0
	If Vartype(i_ThemesManager)=='O' 
		this.ImgBackGroundOver.picture = i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER)
		this.PictureBackground = i_ThemesManager.GetProp(101)
		*--- Enabled background
		this.ImgBackGround.picture = this.PictureBackground
		*--- Disabled background
		this.PictureBackgroundDisable = i_ThemesManager.GetProp(11)
	Endif
	If i_ThemesManager.GetProp(102)=1
		this.ImgBackGround.BorderStyle=1
		this.ImgBackGround.Themes=.f.	
		this.ImgBackGround.BorderColor=i_ThemesManager.getProp(TOOLBAR_BTN_HOVER_BORDER)
		this.BorderColor=i_ThemesManager.getProp(TOOLBAR_BTN_HOVER_BORDER)
	Endif
ENDPROC

PROCEDURE Caption_assign
	Lparameters cNewVal
	IF  EMPTY  (cNewVal  )
		return
	ENDIF
	this.LblButton.caption= cp_Translate(cNewVal)
	this.LblButton.visible=iif(not empty(this.LblButton.caption), .t., .f.)
	this.HiddenButton.caption=cNewVal
	this.caption=cNewVal
ENDPROC

PROCEDURE Enabled_assign
	Lparameters cNewVal
	DODEFAULT()
	this.Enabled=m.cNewVal
	this.HiddenButton.Enabled=m.cNewVal
	this.LblButton.Enabled=m.cNewVal  
	this.ImgButton.enabled= m.cNewVal
	IF (cNewVal)
		this.ImgButton.Picture=this.PictureEnabled
		this.ImgBackGround.picture = this.PictureBackground
	ELSE	 
		this.ImgButton.Picture=this.PictureDisabled
		this.ImgBackGround.picture = this.PictureBackgroundDisable
	ENDIF
ENDPROC

PROCEDURE FontName_assign
	Lparameters cNewVal
	this.LblButton.FontName=cNewVal
ENDPROC

PROCEDURE FontSize_assign
	Lparameters nNewVal
	this.LblButton.FontSize=nNewVal
ENDPROC

PROCEDURE FontBold_assign
	Lparameters bNewVal
	this.LblButton.FontBold=bNewVal
ENDPROC

PROCEDURE FontItalic_assign
	Lparameters bNewVal
	this.LblButton.FontItalic=bNewVal
ENDPROC

PROCEDURE FontUnderline_assign
	Lparameters bNewVal
	this.LblButton.FontUnderline=bNewVal
ENDPROC

PROCEDURE FontStrikeThru_assign
	Lparameters bNewVal
	this.LblButton.FontStrikeThru=bNewVal
ENDPROC

Proc disabledpicture_addign
	Lparameters bNewVal
	this.PictureDisabled=bNewVal
Endproc

Procedure SetFont()
	This.LblButton.FontName = This.FontName
	This.LblButton.FontSize = This.FontSize
	This.LblButton.FontBold =This.FontBold
	This.LblButton.FontItalic=This.FontItalic
	This.LblButton.FontUnderline=This.FontUnderline
	This.LblButton.FontStrikeThru=This.FontStrikeThru
ENDPROC

PROCEDURE Width_assign
	Lparameters nNewVal
	this.Width=nNewVal
	this.Resize()
ENDPROC

PROCEDURE Height_assign
	Lparameters nNewVal
	this.height=nNewVal
	this.Resize()
ENDPROC

Procedure Picture_assign
	Lparameters cNewVal
	* --- Zucchetti Aulla escludere le immagini
	Local nHeightOld
	Do case
		case Vartype(g_NoButtonImage)='L' And g_NoButtonImage And Not Empty(This.Caption)
			nHeightOld = This.Height
			If i_ThemesManager.GetProp(103)=1
				This.Height = Max(21, This.Height - 24)
				If This.NoImgAllign = 1 and nHeightOld>40
					This.Top = This.Top + nHeightOld - This.Height
				Endif
			Endif
			this.ImgButton.picture= ''
			this.ImgButton.visible=.F.
			* --- Zucchetti Aulla fine	
			Return
		case Empty(m.cNewVal)
			this.ImgButton.picture= ''
			this.ImgButton.visible=.F.
			Return			
	Endcase	
	this.ImgButton.visible=.T.
	If Vartype(g_DMIP)='C' And g_DMIP='S' And Inlist(Justext(Lower(Justfname(m.cNewVal))),"bmp","ico") And Inlist(Lower(Juststem(m.cNewVal)),"cattura","folder_edit","scanner","visualicat","zarchivia")
		m.cNewVal=Forceext(Addbs(Justpath(m.cNewVal))+Juststem(m.cNewVal)+"_infinityproject", Justext(Justfname(m.cNewVal)))
	Endif	
	Local cFileBmp, l_Dim , cDisabledBmp 
	l_Dim=Iif(This.ImgSize<>0, This.ImgSize, i_nBtnImgSize)
	If Vartype(i_ThemesManager)=='O'
		i_ThemesManager.PutBmp(this.ImgButton,m.cNewVal, l_Dim)
		this.pictureEnabled=this.imgButton.Picture
		cDisabledBmp = i_ThemesManager.GrayScaleBmp(FORCEEXT(this.pictureEnabled,''), l_Dim)
		this.PictureDisabled = m.cDisabledBmp + ".bmp"
		IF NOT this.enabled
			this.imgButton.Picture=this.PictureDisabled
		ENDIF
		
	Else
		this.ImgButton.picture= m.cNewVal
	ENDIF
Endproc

PROCEDURE mCond
	DODEFAULT()
ENDPROC

PROCEDURE Resize()
 	this.HiddenButton.width=this.width
    this.HiddenButton.height=this.Height
	Local l_Dim
	l_Dim=Iif(This.ImgSize<>0, This.ImgSize, i_nBtnImgSize)	
	if empty(this.caption)
		this.ImgButton.top=((this.height - this.ImgButton.height)/2)+iif(this.height - this.ImgButton.height>9,(l_Dim/4)-2,0)
	else
		this.ImgButton.top=(this.height/2 - (this.ImgButton.height+this.LblButton.height)/2)+(l_Dim/4)-2
		this.LblButton.left=0
		this.LblButton.width=this.width
		If Not empty(this.ImgButton.picture)
			this.LblButton.top=this.ImgButton.top+this.ImgButton.height + 2
		Else
			this.LblButton.top=6
		Endif
	endif
    this.ImgButton.left=this.width/2 - this.ImgButton.width/2
    this.ImgBackGroundOver.width=this.width -2
    this.ImgBackGround.width=this.width -2    
    this.ImgBackGroundOver.height=this.Height -2  
    this.ImgBackGround.height=this.Height-2  
    this.LblButton.height= 22   
ENDPROC

Procedure ToolTipText_Assign(xValue)
    This.ToolTipText = m.xValue
    this.HiddenButton.ToolTipText =cp_Translate (m.xValue)
    this.LblButton.ToolTipText =cp_Translate (m.xValue)
ENDPROC

procedure HiddenButton.click()
	LOCAL comando
	comando= STRTRAN (this.parent.ExtendedCommand,'#',' with ')
	IF this.enabled
		IF this.parent.IsExtended
			DO &comando
			NODEFAULT
		ELSE
			this.parent.click()
		ENDIF
	ENDIF
ENDPROC

procedure LblButton.click()
	IF this.enabled
		this.parent.HiddenButton.click()
	endif
ENDPROC

Procedure MouseEnter
	LPARAMETERS nButton, nShift, nXCoord, nYCoord
	this.HiddenButton.MouseEnter(nButton, nShift, nXCoord, nYCoord)
ENDPROC

Procedure Mouseleave
	LPARAMETERS nButton, nShift, nXCoord, nYCoord
	this.HiddenButton.Mouseleave(nButton, nShift, nXCoord, nYCoord)
ENDPROC

Procedure HiddenButton.MouseEnter
	LPARAMETERS nButton, nShift, nXCoord, nYCoord
	IF this.parent.enabled
		IF (nButton=0)
			If Vartype(i_ThemesManager)=='O'
				If i_ThemesManager.GetProp(102)=1
					this.parent.ImgBackGroundOver.BorderStyle=1
					this.parent.ImgBackGroundOver.Themes=.f.
					this.parent.ImgBackGroundOver.BorderColor=i_ThemesManager.getProp(TOOLBAR_BTN_HOVER_BORDER)
					this.parent.LblButton.Forecolor=i_ThemesManager.getProp(TOOLBAR_BTN_HOVER_TEXT)
				Endif
				this.parent.BorderColor=i_ThemesManager.getProp(TOOLBAR_BTN_HOVER_BORDER)
			Else
				this.parent.BorderColor=RGB (0,0,200)
			Endif
		ENDIF
		this.parent.ImgBackGroundOver.visible = Vartype(i_ThemesManager)=='O' AND NOT this.parent.BackgroundDisable
		this.parent.ImgBackGround.visible = !this.parent.ImgBackGroundOver.visible AND Vartype(i_ThemesManager)=='O'
		IF this.parent.IsExtended
			this.parent.parent.ExMouseEnter(nButton, nShift, nXCoord, nYCoord,1)
		ENDIF
	ENDIF
ENDPROC

Procedure HiddenButton.Mouseleave
	LPARAMETERS nButton, nShift, nXCoord, nYCoord
	* If Vartype(i_ThemesManager)=='O'
	*            this.parent.BorderColor=i_ThemesManager.getProp(TOOLBAR_BORDERCOLOR)
	*        Else
	* Endif
	IF this.parent.enabled
		this.parent.ImgBackGroundOver.BorderStyle=0
		this.parent.ImgBackGroundOver.Themes=.t.
		If Vartype(i_ThemesManager)=='O'
			this.parent.LblButton.Forecolor=i_ThemesManager.getProp(TOOLBAR_BTN_TEXT)
		Else
			this.parent.LblButton.Forecolor=0
		Endif
		IF this.parent.IsExtended 
		   this.parent.parent.ExMouseleave(nButton, nShift, nXCoord, nYCoord,1)
		ENDIF
		this.parent.ImgBackGroundOver.visible=.F.
		this.parent.ImgBackGround.visible=.T. AND Vartype(i_ThemesManager)=='O'
	ENDIF
ENDPROC

PROCEDURE GotFocus
	this.HiddenButton.MouseEnter(0)
ENDPROC

PROCEDURE LostFocus
	this.HiddenButton.Mouseleave(0)
ENDPROC

PROCEDURE HiddenButton.keypress
	LPARAMETERS nKeyCode, nShiftAltCtrl
	IF nKeyCode=DNARROW AND this.Parent.parent.class='Advbuttonmenu'
		this.Parent.parent.DownArrow()
	ELSE
		*con la chiamata alla dodefault la funzionalit� associata al bottone viene chiamata 2 volte
		*DODEFAULT(nKeyCode, nShiftAltCtrl)
	ENDIF
ENDPROC

function HiddenButton.NotifyEvent(cEvent)
	this.Parent.NotifyEvent(cEvent)
ENDFUNC

function NotifyEvent(cEvent)
	this.Parent.NotifyEvent(cEvent)
ENDFUNC

Enddefine


Define Class ButtonArrow As Container
    BorderWidth = 1
    Borderstyle = 3
    BorderColor=0
    BackStyle = 0
    Anchor = 0
    Width =  15
    Height = 23
    LarghezzaFissa =15
    ToolTipText=''
	
  Add Object ImgButton As Image With ;
        Top = 0, Left = 2,;
        Width = 15, Height = 23, ;
        Visible = .T., BackStyle=0 ,BorderWidth = 0 ;

  Add Object HiddenButton As CommandButton With ;
        Top = 0, Left = 0,;
        Width = 15, Height = 23, BorderWidth = 0,;
        Visible = .T., BackStyle=0, style=1

PROCEDURE Init
	This.Picture = i_ThemesManager.GetProp(OVERFLOWPANEL_MENUBUTTON_PICTURE)
	this.Width=this.Width
	this.HiddenButton.width=this.width
	this.HiddenButton.height=this.Height
	This.ToolTipText=This.ToolTipText
	this.parent.BorderColor=0
ENDPROC

PROCEDURE mCond
	DODEFAULT()
ENDPROC

PROCEDURE Resize()
	this.ImgButton.width=this.width
	this.ImgButton.top=this.height/2 - this.ImgButton.height/2
	this.ImgButton.left=0
	this.HiddenButton.width=this.width
	this.HiddenButton.height=this.Height
ENDPROC

PROCEDURE Width_assign
  	Lparameters nNewVal
  	this.Width=this.LarghezzaFissa
  	this.Resize()
ENDPROC

PROCEDURE Height_assign
  	Lparameters nNewVal
  	this.height=nNewVal
  	this.Resize()
ENDPROC

PROCEDURE ToolTipText_Assign(xValue)
    This.ToolTipText = m.xValue
    this.HiddenButton.ToolTipText =m.xValue
ENDPROC

PROCEDURE Picture_assign
        Lparameters cNewVal
        If Empty(m.cNewVal)
            this.ImgButton.picture= ''
            Return
        Endif
        Local cFileBmp, l_Dim
        l_Dim=24
		this.picture= m.cNewVal
        If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(this.ImgButton,m.i_ThemesManager.GetProp(OVERFLOWPANEL_MENUBUTTON_PICTURE), l_Dim)
        Else
            this.ImgButton.picture= m.cNewVal
        Endif
ENDPROC

procedure HiddenButton.click()
    this.parent.VisualizzaMenu()
ENDPROC

PROCEDURE VisualizzaMenu
	*implementare la visualizzazione menu
	return
ENDPROC

PROCEDURE MouseEnter
	LPARAMETERS nButton, nShift, nXCoord, nYCoord
	this.HiddenButton.MouseEnter(nButton, nShift, nXCoord, nYCoord)
ENDPROC

PROCEDURE Mouseleave
  LPARAMETERS nButton, nShift, nXCoord, nYCoord
  this.HiddenButton.Mouseleave(nButton, nShift, nXCoord, nYCoord)
ENDPROC

PROCEDURE HiddenButton.MouseEnter
	LPARAMETERS nButton, nShift, nXCoord, nYCoord
	IF this.parent.enabled
		IF (nButton=0)
			If Vartype(i_ThemesManager)=='O'
				this.parent.BorderWidth=i_ThemesManager.getProp(104)
				this.parent.BorderColor=i_ThemesManager.getProp(TOOLBAR_BTN_HOVER_BORDER)
			Else
				this.parent.BorderColor=RGB (0,0,200)
			Endif
		ENDIF
		this.parent.parent.ExMouseEnter(nButton, nShift, nXCoord, nYCoord,2)
	ENDIF
ENDPROC

PROCEDURE HiddenButton.Mouseleave
	LPARAMETERS nButton, nShift, nXCoord, nYCoord
	IF this.parent.enabled
		this.parent.BorderColor=RGB (200,200,200)
		this.parent.parent.ExMouseleave(nButton, nShift, nXCoord, nYCoord,2)
	ENDIF
ENDPROC

PROCEDURE HiddenButton.Click
	LOCAL x,y,omenu 
	x = cb_GetWindowLeft(this.Parent.parent.parent.parent.parent.parent.hwnd)+;
		OBJTOCLIENT(this.Parent.parent,2)+Icase(thisform.borderstyle=3, Sysmetric(3),INLIST(thisform.borderstyle,2,1), Sysmetric(10), 0)
	y = cb_GetWindowTop(this.Parent.parent.parent.parent.parent.parent.hwnd)+;
		OBJTOCLIENT(this.Parent.parent,1)+this.Parent.parent.Height+;
		IIF(thisform.titlebar=1,Sysmetric(9),0)+Icase(thisform.borderstyle=3, Sysmetric(6),INLIST(thisform.borderstyle,2,1), Sysmetric(11), 0)
	**aggiunge lo spazio occupato dalla selezione pagine
	this.Parent.parent.MenuArrowPress ()
	omenu=CREATEOBJECT("ShowRightClickMenu",this.Parent.parent.CommandsMenu,x ,y)
	omenu=null
	this.Parent.parent.MenuArrowrelease ()
ENDPROC

Enddefine


Define Class AdvButtonMenu As Container
    BorderWidth = 0
    BorderColor=0
    BackStyle = 0
    Anchor = 0
    Width =  62
    Height = 24
    picture=''
    cpPicture=''
    Top = 0
    Left = 0
    Caption=''
    FixedCaption=.F.
    FontName=''
    FontSize=8
    FontBold=.f.
    FontItalic=.f.
    FontUnderline=.f.
    FontStrikeThru=.f.
    ToolTipText=''
    HiddenButton =null
    CommandsMenu=''
    nomevoce=''
    MenuLoaded=.F.
    bGlobalFont=.t.
    
  Add Object ImgBackGroundOver As Image With ;
        Top = 1, Left = 1,;
        Width = 61, Height = 45, ;
        Visible = .F., BackStyle=1 ,BorderWidth = 0 , stretch=2    
              
  Add Object ImgBackGround As Image With ;
        Top = 1, Left = 1,;
        Width = 61, Height = 45, ;
        Visible = .T., BackStyle=1 ,BorderWidth = 0 , stretch=2  
    
  Add Object LeftButton As ButtonMenu With ;
        Top = 1 , Left = 1,;
        Width = 44, Height = 22, ;
        Visible = .T., BackStyle=0 ,BorderWidth = 0 , FontSize=8 , IsExtended=.T. , BackgroundDisable=.F.

  Add Object RightButton As ButtonArrow With ;
        Top = 0, Left = 44,;
        Width = 15, Height = 23, BorderWidth = 2, Borderstyle =3 ,BorderColor=RGB (200,200,200),;
        Visible = .T.,tabstop=.f., BackStyle=0

PROCEDURE Init
	This.FontName =SetFontName('B', This.FontName,This.bGlobalFont)  
	THIS.LeftButton.FontName =This.FontName 
	This.FontSize =SetFontSize('B', This.Fontsize,This.bGlobalFont)  
	THIS.LeftButton.FontSize =This.FontSize 
	This.FontBold= SetFontBold('B', This.FontBold,This.bGlobalFont)
	THIS.LeftButton.FontBold=This.FontBold
	This.FontItalic=SetFontItalic('B', This.FontItalic,This.bGlobalFont)
	THIS.LeftButton.FontItalic=This.FontItalic
	This.FontUnderline=This.FontUnderline
	This.FontStrikeThru=This.FontStrikeThru
	This.ToolTipText=This.ToolTipText
	If Vartype(i_ThemesManager)=='O'
		this.parent.BorderColor=i_ThemesManager.getProp(TOOLBAR_BORDERCOLOR)
	Else
		this.parent.BorderColor=0
	ENDIF
	HiddenButton =this.LeftButton.HiddenButton
	*    this.LoadMenu()
	this.caption=iif(empty(this.nomevoce) and not empty(this.caption), this.caption, this.nomevoce)
	If Vartype(i_ThemesManager)=='O' 
		this.ImgBackGroundOver.picture = i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER)     
		this.ImgBackGround.picture = i_ThemesManager.GetProp(TOOLBAR_BACKGRD_H)            
	ENDIF  
	this.picture=this.picture
	this.Width=MAX(this.Width , 65)
ENDPROC

PROCEDURE mcond()
   this.LoadMenu()
   this.caption=this.nomevoce
   this.caption=''
   RETURN .T.
ENDPROC

procedure click()
	this.LeftButton.HiddenButton.click()
ENDPROC

PROCEDURE ReLoadMenu 
this.MenuLoaded =.F.
this.LoadMenu()
ENDPROC

PROCEDURE LoadMenu
	LOCAL curname, CondizioneVoce , OldArea , bFirstLap, bNoMenu
	bFirstLap=.t.
	**Se � stata definita la variabile globale i_LoadMenu allora il menu viene caricato solo quando la variabile globale i_LoadMenu � true
	IF  NOT this.MenuLoaded AND (VARTYPE(i_LoadMenu)<>'L' OR i_LoadMenu)
	THIS.LeftButton.ENABLED=.f.
	THIS.RightButton.ENABLED=.f.	
	this.Enabled=.f.
	curname = SYS(2015)
	OldArea=SELECT ()
	
	* Modifica per consentire di creare il menu anche a partire da un file vqr (prima era possibile usare solo un vmn)
	* 
	* la query deve avere 3 campi col seguente formato:
	* VOCE C 60
	* PROCEDURA M
	* BMP C 254
	* CONDIZ M
	IF UPPER(JUSTEXT(this.CommandsMenu))='VQR'
		LOCAL curtemp, nProgressivo, cChiave, cVoce, cProcedura, cBmp, mCond
		curtemp = SYS(2015)
		nProgressivo = 0
		DO vq_exec WITH this.CommandsMenu,.null.,curtemp
		bNoMenu = RECCOUNT(curtemp)=0
		CREATE CURSOR (curname) (vocemenu C(60), level N(5), nameproc M, directory N(1), rifpadre N(5),;
		 elementi N(5), id C(10), enabled L, progress N(5), cpbmpname C(254), modulo M, ins_point C(254),;
		 position C(6), deleted C(1), lvlkey C(100), insertion C(254), note C(254), mru C(1), tearoff C(1))
		INSERT INTO (curname);
		 (vocemenu, level, directory, elementi, enabled, progress, deleted, lvlkey, mru, tearoff);
		 VALUES ("Men� principale", 1, 0, 0, .T., nProgressivo, 'T', '001', 'N', 'N')
		SELECT (curtemp)
		SCAN
			cVoce = ALLTRIM(VOCE)
			cProcedura = ALLTRIM(PROCEDURA)
			cBmp = ALLTRIM(BMP)
			cCond = ALLTRIM(CONDIZ)
			nProgressivo = nProgressivo + 1
			cChiave = '001.'+PADL(ALLTRIM(STR(nProgressivo)),3,'0')
			INSERT INTO (curname);
		 	(vocemenu, level, nameproc, directory, elementi, enabled, progress, cpbmpname, modulo, deleted, lvlkey, insertion, mru, tearoff);
		 	 VALUES (cVoce, 2, cProcedura, 1, 0, .T., nProgressivo, cBmp, cCond, 'T', cChiave, cVoce, 'N', 'N')
			SELECT (curtemp) 	 
		ENDSCAN
		USE IN SELECT (curtemp)
	ELSE
		DO vmn_to_cur WITH this.CommandsMenu,curname  IN vu_exec
	ENDIF
	* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *
	
	IF USED  (curname )
		SELECT  (curname )
		GO TOP
		SCAN FOR LEVEL=2
			CondizioneVoce=Modulo
			**Nel caso in cui l espressione sia vuota la voce di menu � sempre attiva
			IF EMPTY (CondizioneVoce) 
				CondizioneVoce=.t.
			ELSE
			 	CondizioneVoce=&CondizioneVoce
			ENDIF
			THIS.LeftButton.ENABLED=.f.
			THIS.LeftButton.HiddenButton.ENABLED=.f.	
			THIS.LeftButton.LblButton.ENABLED=.f.	
			THIS.RightButton.ENABLED=.f.
			this.Enabled=.f.			
			IF CondizioneVoce OR bFirstLap
				this.picture=ALLTRIM(cpbmpname)
				IF NOT this.FixedCaption
					this.caption=STRTRAN(ALLTRIM(vocemenu),'&','\<')
					this.nomevoce=STRTRAN(ALLTRIM(vocemenu),'&','\<')
				ENDIF
				this.leftbutton.ExtendedCommand=ALLTRIM(NameProc)
				IF CondizioneVoce
					THIS.LeftButton.ENABLED=.T.
					THIS.RightButton.ENABLED=.T.
					this.Enabled=.T.
					THIS.LeftButton.HiddenButton.ENABLED=.T.					
					THIS.LeftButton.HiddenButton.tooltiptext=cp_translate(this.tooltiptext)
					THIS.LeftButton.LblButton.ENABLED=.T. 
					THIS.LeftButton.resize()
					EXIT
				ENDIF
				bFirstLap=.F.
			ENDIF
		ENDSCAN
		THIS.LeftButton.resize()
		USE IN SELECT (curname )
	ENDIF
	IF bNoMenu=.T.
		this.picture=this.cpPicture
		this.Width=MAX(this.Width , 65)
	ENDIF
	SELECT (OldArea)
	this.MenuLoaded=.T. 	
	ENDIF
ENDPROC

PROCEDURE AddMenu (mfunz , indicePag ,indiceCon )
	LOCAL curname, CondizioneVoce , OldArea , bFirstLap, sottomenu , loMenuItem , m , macromenu,indicemenu
	bFirstLap=.T.
	indicemenu=1
	**Se � stata definita la variabile globale i_LoadMenu allora il menu viene caricato solo quando la variabile globale i_LoadMenu � true
	curname = SYS(2015)
	OldArea=SELECT ()
	
	* Modifica per consentire di creare il menu anche a partire da un file vqr (prima era possibile usare solo un vmn)
	* 
	* la query deve avere 3 campi col seguente formato:
	* VOCE C 60
	* PROCEDURA M
	* BMP C 254
	IF UPPER(JUSTEXT(this.CommandsMenu))='VQR'
		LOCAL curtemp, nProgressivo, cChiave, cVoce, cProcedura, cBmp
		curtemp = SYS(2015)
		nProgressivo = 0
		DO vq_exec WITH this.CommandsMenu,.null.,curtemp
		CREATE CURSOR (curname) (vocemenu C(60), level N(5), nameproc M, directory N(1), rifpadre N(5),;
		 elementi N(5), id C(10), enabled L, progress N(5), cpbmpname C(254), modulo M, ins_point C(254),;
		 position C(6), deleted C(1), lvlkey C(100), insertion C(254), note C(254), mru C(1), tearoff C(1))
		INSERT INTO (curname);
		 (vocemenu, level, directory, elementi, enabled, progress, deleted, lvlkey, mru, tearoff);
		 VALUES ("Men� principale", 1, 0, 0, .T., nProgressivo, 'T', '001', 'N', 'N')
		SELECT (curtemp)
		SCAN
			cVoce = ALLTRIM(VOCE)
			cProcedura = ALLTRIM(PROCEDURA)
			cBmp = ALLTRIM(BMP)
			nProgressivo = nProgressivo + 1
			cChiave = '001.'+PADL(ALLTRIM(STR(nProgressivo)),3,'0')
			INSERT INTO (curname);
		 	(vocemenu, level, nameproc, directory, elementi, enabled, progress, cpbmpname, deleted, lvlkey, insertion, mru, tearoff);
		 	 VALUES (cVoce, 2, cProcedura, 1, 0, .T., nProgressivo, cBmp, 'T', cChiave, cVoce, 'N', 'N')
			SELECT (curtemp) 	 
		ENDSCAN
		USE IN SELECT (curtemp)
	ELSE
		DO vmn_to_cur WITH this.CommandsMenu,curname  IN vu_exec
	ENDIF
	* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *
	
	IF USED  (curname )
		SELECT  (curname )
		GO TOP
		SCAN FOR LEVEL=2
     		CondizioneVoce=''
			CondizioneVoce=Modulo
			**Nel caso in cui l espressione sia vuota la voce di menu � sempre attiva
			IF EMPTY (CondizioneVoce) 
				CondizioneVoce=.t.
			ELSE
			 	CondizioneVoce=&CondizioneVoce
			ENDIF
			**Se esiste almeno una voce di menu abilitata allora si aggiunge il sottomenu 
			IF CondizioneVoce
				* Se ci sono delle voci di menu attive le aggiunge
				IF bFirstLap
					If i_VisualTheme<>-1
						sottomenu=mfunz.AddMenuItem()
						sottomenu.Caption = this.Caption
						sottomenu.Visible = this.Visible 
						sottomenu.BeginGroup = .T.
					ELSE 
						**Ogni sottomenu deve avere un nome diverso percio si usa la macro per generare aruntime il nome del sottomenu
						Define Bar mfunz Of funz Prompt cp_Translate(STRTRAN(ALLTRIM(this.Caption),'&','\<'))
						macromenu="Define Popup miofunz"+ALLTRIM(STR(mfunz))+" Margin shortcut"
						&macromenu
						macromenu="On Bar mfunz Of funz Activate Popup miofunz"+ALLTRIM(STR(mfunz))
						&macromenu
					ENDIF
					bFirstLap=.F.			    
				ENDIF

			   *Aggiunge la voce di menu
				If i_VisualTheme<>-1 
						  loMenuItem= sottomenu.AddMenuItem()
						  loMenuItem.Caption = cp_Translate(STRTRAN(ALLTRIM(vocemenu),'&','\<'))
						  loMenuItem.SkipFor = !CondizioneVoce
						  loMenuItem.Visible = .T.           
						  loMenuItem.Description = g_Omenu.oParentObject.oPgFrm.Pages(indicePag).oPag.Controls(indiceCon).ToolTipText
						  loMenuItem.OnClick = "Do mhit In cp_menu With ["+Strtran(Trim(Alltrim(NAMEPROC)),'#',' with ')+"],["+ Alltrim(VOCEMENU)+"]"
				ELSE
				   If CondizioneVoce
							macromenu= "Define Bar (indicemenu) Of miofunz"+ALLTRIM(STR(mfunz))+" Prompt cp_Translate(cp_Translate(STRTRAN(ALLTRIM(vocemenu),'&','\<'))) Skip For .F. Message g_Omenu.oParentObject.oPgFrm.Pages(indicePag).oPag.Controls(indiceCon).ToolTipText"
							&macromenu
				   Else
							macromenu="Define Bar (indicemenu) Of miofunz"+ALLTRIM(STR(mfunz))+" Prompt cp_Translate(cp_Translate(STRTRAN(ALLTRIM(vocemenu),'&','\<'))) Skip For .T. Message g_Omenu.oParentObject.oPgFrm.Pages(indicePag).oPag.Controls(indiceCon).ToolTipText"
							&macromenu
				   Endif
				   m="on sele BAR ("+Alltrim(Str(indicemenu))+") of miofunz"+ALLTRIM(STR(mfunz))+" Do mhit In cp_menu With ["+Strtran(Trim(Alltrim(NAMEPROC)),'#',' with ')+"],["+ Alltrim(VOCEMENU)+"]"
				   &m 
				   indicemenu=indicemenu+1           
				ENDIF             
			ENDIF
		ENDSCAN
		USE IN SELECT (curname )
	ENDIF
	SELECT (OldArea)	
ENDPROC


PROCEDURE Caption_assign
  Lparameters cNewVal
  *La caption del container deve rimanere vuota altrimenti il bottone fuori campo non cattura i tasti di scelta rapida (shortcut-key)
  IF EMPTY (cNewVal )
  	return
  ENDIF
  this.LeftButton.caption=cNewVal
  this.caption=cNewVal
 ENDPROC

PROCEDURE Resize()
 	this.LeftButton.width=this.width - this.RightButton.LarghezzaFissa -1
    this.RightButton.width=this.RightButton.LarghezzaFissa
    this.LeftButton.height=this.Height 
    this.RightButton.height=this.Height - 2
    this.LeftButton.top=0
    this.LeftButton.left=0
    this.RightButton.top=1
    this.RightButton.left=this.width - this.RightButton.LarghezzaFissa -2
	this.LeftButton.Resize()
ENDPROC


PROCEDURE Width_assign
  	Lparameters nNewVal
  	this.Width=max(  nNewVal , 65)
  	this.Resize()
ENDPROC

PROCEDURE Height_assign
  	Lparameters nNewVal
  	this.height=nNewVal
  	this.Resize()
ENDPROC

Procedure Picture_assign
	Lparameters cNewVal
	this.LeftButton.picture =m.cNewVal
Endproc

PROCEDURE FontName_assign
	Lparameters cNewVal
	this.LeftButton.FontName=cNewVal
	this.FontName=cNewVal   
ENDPROC

PROCEDURE FontSize_assign
	Lparameters nNewVal
	this.LeftButton.FontSize=nNewVal
	this.FontSize=nNewVal   
ENDPROC

PROCEDURE FontBold_assign
	Lparameters bNewVal
	this.LeftButton.FontBold=bNewVal
	this.FontBold=bNewVal
ENDPROC

PROCEDURE FontItalic_assign
	Lparameters bNewVal
	this.LeftButton.FontItalic=bNewVal
	this.FontItalic=bNewVal   
ENDPROC

PROCEDURE FontUnderline_assign
	Lparameters bNewVal
	this.LeftButton.FontUnderline=bNewVal
ENDPROC

PROCEDURE FontStrikeThru_assign
	Lparameters bNewVal
	this.LeftButton.FontStrikeThru=bNewVal
ENDPROC

Procedure ToolTipText_Assign(xValue)
    This.LeftButton.ToolTipText = m.xValue
    This.RightButton.ToolTipText = m.xValue
ENDPROC

Procedure ExMouseEnter
	LPARAMETERS nButton, nShift, nXCoord, nYCoord, nOrig
	IF this.enabled
		Do case
			case norig=1
				this.RightButton.MouseEnter (nButton, nShift, nXCoord, nYCoord)
			case norig=2
				this.LeftButton.MouseEnter (nButton, nShift, nXCoord, nYCoord)
		ENDCASE
		this.ImgBackGroundOver.visible = Vartype(i_ThemesManager)=='O' 
		this.ImgBackGround.visible = !this.ImgBackGroundOver.visible AND Vartype(i_ThemesManager)=='O'
	ENDIF
ENDPROC

Procedure ExMouseleave
	LPARAMETERS nButton, nShift, nXCoord, nYCoord , nOrig
	IF this.enabled
		Do case
			case norig=1
				this.RightButton.Mouseleave (nButton, nShift, nXCoord, nYCoord )
			case norig=2
				this.LeftButton.Mouseleave (nButton, nShift, nXCoord, nYCoord )  
		ENDCASE
		this.ImgBackGroundOver.visible = .F.  
		this.ImgBackGround.visible = Vartype(i_ThemesManager)=='O'   
	ENDIF
ENDPROC

Procedure MenuArrowPress
*	This.BorderWidth = 1
*	This.LeftButton.BorderWidth=0
*	This.LeftButton.ImgButton.BorderWidth=0
*	This.LeftButton.HiddenButton.BorderWidth=0
*	This.RightButton.BorderWidth=0
Endproc

Procedure MenuArrowrelease
*	This.BorderWidth = 0
*	This.LeftButton.BorderWidth=1
*	This.RightButton.BorderWidth=1
Endproc

PROCEDURE DownArrow
	This.RightButton.HiddenButton.Click ()
ENDPROC

function NotifyEvent(cEvent)
	this.Parent.NotifyEvent(cEvent)
ENDFUNC

Enddefine


DEFINE Class ShowRightClickMenu as Custom 

PROCEDURE Init (CommandsMenu, posX, posY)
    LOCAL l_oPopupMenu, curname ,OldArea 
  	OldArea=SELECT()
  	curname=SYS(2015)
	
	* Modifica per consentire di creare il menu anche a partire da un file vqr (prima era possibile usare solo un vmn)
	* 
	* la query deve avere 3 campi col seguente formato:
	* VOCE C 60
	* PROCEDURA M
	* BMP C 254
	IF UPPER(JUSTEXT(CommandsMenu))='VQR'
		LOCAL curtemp, nProgressivo, cChiave, cVoce, cProcedura, cBmp
		curtemp = SYS(2015)
		nProgressivo = 0
		DO vq_exec WITH CommandsMenu,.null.,curtemp
		CREATE CURSOR (curname) (vocemenu C(60), level N(5), nameproc M, directory N(1), rifpadre N(5),;
		 elementi N(5), id C(10), enabled L, progress N(5), cpbmpname C(254), modulo M, ins_point C(254),;
		 position C(6), deleted C(1), lvlkey C(100), insertion C(254), note C(254), mru C(1), tearoff C(1))
		INSERT INTO (curname);
		 (vocemenu, level, directory, elementi, enabled, progress, deleted, lvlkey, mru, tearoff);
		 VALUES ("Men� principale", 1, 0, 0, .T., nProgressivo, 'T', '001', 'N', 'N')
		SELECT (curtemp)
		SCAN
			cVoce = ALLTRIM(VOCE)
			cProcedura = ALLTRIM(PROCEDURA)
			cBmp = ALLTRIM(BMP)
			nProgressivo = nProgressivo + 1
			cChiave = '001.'+PADL(ALLTRIM(STR(nProgressivo)),3,'0')
			INSERT INTO (curname);
		 	(vocemenu, level, nameproc, directory, elementi, enabled, progress, cpbmpname, deleted, lvlkey, insertion, mru, tearoff);
		 	 VALUES (cVoce, 2, cProcedura, 1, 0, .T., nProgressivo, cBmp, 'T', cChiave, cVoce, 'N', 'N')
			SELECT (curtemp) 	 
		ENDSCAN
		USE IN SELECT (curtemp)
	ELSE
		DO vmn_to_cur WITH CommandsMenu,curname  IN vu_exec
	ENDIF
	* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *
	
    l_oPopupMenu= Createobject("cbPopupMenu")
    l_oPopupMenu.oPopupMenu.Init()
    l_oPopupMenu.oPopupMenu.cCursorName=SYS(2015)
    l_oPopupMenu.oPopupMenu.SetCursorVmn (curname )
    l_oPopupMenu.oPopupMenu.InitMenuFromCursor()  
    l_oPopupMenu.ShowMenu(posX, posY )
	USE IN SELECT (curname )
   	SELECT (OldArea)
ENDPROC


ENDDEFINE 


**************************************************
*-- Class:           zcntxfrxmultipage
*-- ParentClass:     container
*-- BaseClass:       container
*-- Time Stamp:      03/01/11 12:40:09 PM
*
DEFINE CLASS zcntxfrxmultipage AS container


 Width = 10 &&515
 Height = 10 &&222
 BackStyle = 0
 BorderWidth = 0
 itool = 0
 *-- Specifies the name of the font used to display text.
 fontname = "MS Sans Serif"
 *-- Specifies the font size for text displayed with an object.
 fontsize = 8
 otoolbar = (null)
 classmaintoolbar = ""
 lshowtabforonereport = .F.
 showstatus = .T.
 oextensionhandler = .F.
 clanguage = .F.
 displaymode = 1
 cjobname = .F.
 oprogress = .F.
 ibook = 2
 classpreviewpage = "ZXFCont"
 iemail = 0
 underlinehyperlinksonprint = 2
 Name = "zcntxfrxmultipage"
 nextpagecaption = .F.
 nCurrentPageNumber = 0
 nTotalPages = 0
 sEmbeddedToolbarClass = ""
 sToolbarClass = ""
 oEmbeddedToolbar = .null.
 oToolbar = .null.
 oBook =.Null.
 oZoomBook=.Null.
 oTreeBook=.Null.
 nOldzoomFactor = 100
 * Print option parameters
 w_PRINTER = ""
 w_PRINTEROPTIONS = ""
 w_PAGERANGE = ""
 w_COPIES = 1
 w_PAGES = 0
 w_OKPRINT = .F.
 
 * --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
 bNoSetAnchorForm = .T.
 * --- Zucchetti Aulla Fine  - Gestione SetAnchorForm

 ADD OBJECT pages AS pageframe WITH ;
  ErasePage = .T., ;
  PageCount = 0, ;
  TabStretch = 1, ;
  TabStyle = 1, ;
  ActivePage = 0, ;
  Top = 67, ;
  Left = 24, ;
  Width = 229, ;
  Height = 120, ;
  Tabs = .T., ;
  Name = "pages"


 ADD OBJECT odisplaydefaults AS zxfpreviewerparameters WITH ;
  Top = 60, ;
  Left = 396, ;
  Name = "oDisplayDefaults"

 ADD OBJECT oSearchParameters AS _xfrxfsettings WITH ;
  lMatchCase = .F., ;
  lSearchBackWard = .F.

  Proc sToolbarClass_assign
	LPARAMETERS sNewVal
	this.sToolbarClass = m.sNewVal
	this.oToolbar = CREATEOBJECT(this.sToolbarClass, i_VisualTheme<>-1 And Vartype(_Screen.cp_ThemesManager)=="O")
	this.oToolbar.oPreviewObject=this
    this.oToolBar.Dock(-1)
  EndProc
  
  Proc sEmbeddedToolbarClass_assign
	LPARAMETERS sNewVal
	this.sEmbeddedToolbarClass = m.sNewVal
	this.oEmbeddedToolbar = CREATEOBJECT(this.sToolbarClass, i_VisualTheme<>-1 And Vartype(_Screen.cp_ThemesManager)=="O")
	this.oEmbeddedToolbar.oPreviewObject=this
  ENDPROC

  Proc oBook_assign
    * Alla propriet� oBook � stato assegnato l'oggetto bookmark.
	LPARAMETERS sNewVal
	this.oBook = m.sNewVal
    * Alla propriet� oPreviewObject dell'oggetto bookmark viene assegnato l'oggetto preview.
    IF vartype(this.oBook) = "O"
        this.oBook.oPreviewObject = this
    ENDIF
  EndProc

 PROCEDURE Visible_Assign
 	LPARAMETERS bNewVal
 	this.Visible = bNewVal
 	this.pages.Visible = bNewVal
 ENDPROC

 PROCEDURE getnewpage
	  LPARAMETERS tnPageNo
	  LOCAL lnCurrentITool

	  IF this.pages.ActivePage>0
	   WITH this.pages.Pages(this.pages.ActivePage).cntPreviewer
		IF !ISNULL(.oTool)
		 lnCurrentITool = 1
		ELSE
		 IF this.iTool = 1
		  lnCurrentITool = 0
		 ELSE
		  lnCurrentITool = this.iTool
		 ENDIF
		ENDIF
	   ENDWITH
	  ELSE
	   lnCurrentITool = this.iTool
	  ENDIF
	  LOCAL loObj, loCurrentPage, lnCurrentPage
	  IF EMPTY(tnPageNo) OR tnPageNo > this.pages.PageCount
	   this.pages.PageCount = this.pages.PageCount + 1
	   lnCurrentPage = this.pages.PageCount
	  ELSE
	   lnCurrentPage = tnPageNo
	  endif

	  loCurrentPage = this.pages.Pages(lnCurrentPage)
	  this.pages.ActivePage = lnCurrentPage

	  IF this.pages.PageCount>1
	   this.pages.Tabs = .t.
	  ELSE
	   IF !this.lShowTabForOneReport
		this.pages.Tabs = .f.
	   endif
	  endif

	  IF TYPE("loCurrentpage.cntPreviewer")="O" AND NOT ISNULL(loCurrentpage.cntPreviewer)
	   loCurrentPage.cntPreviewer.reset()
	  else
	   loCurrentPage.addObject("cntPreviewer", this.ClassPreviewPage)
	  endif
	  loObj = loCurrentPage.cntPreviewer
	  loObj.visible = .t.
	  loObj.itool = lnCurrentITool
	  loObj.iEmail = this.iEmail
	  loObj.ShowStatus = this.ShowStatus
	  loObj.oExtensionHandler = this.oExtensionHandler
	  loObj.iBook = this.iBook
	  loObj.DisplayMode = this.DisplayMode
	  loObj.UnderlineHyperlinksOnPrint = this.UnderlineHyperlinksOnPrint
	  loObj.oDisplayDefaults.startingpage = this.oDisplayDefaults.startingpage
	  loObj.oDisplayDefaults.zoomfactor = this.oDisplayDefaults.zoomfactor
	  loObj.oDisplayDefaults.pagespersheet = this.oDisplayDefaults.pagespersheet
	  loObj.oDisplayDefaults.defaultoutputfilename = this.oDisplayDefaults.defaultoutputfilename

	  IF NOT EMPTY(this.cJobName)
	   loObj.cJobName = this.cJobName
	  ENDIF
	  IF TYPE("this.oProgress")="O" AND NOT ISNULL(this.oProgress)
	   loObj.oProgress = this.oProgress
	  endif

	  IF NOT EMPTY(this.cLanguage)
	   loObj.setLanguage(this.cLanguage)
	  endif
	  loObj.classmaintoolbar = this.classmaintoolbar
	  loCurrentPage.fontName = this.fontName
	  loCurrentPage.fontSize = this.fontsize
	  IF NOT EMPTY(this.nextpagecaption)
	   loCurrentPage.caption = this.nextpagecaption
	  endif
	  loObj.move(0,1,this.pages.PageWidth, this.pages.PageHeight-1)
	  this.pageactivated(this.pages.Pages(this.pages.ActivePage))
	  RETURN loObj
 ENDPROC
 
 
 PROCEDURE pageactivated
	  LPARAMETERS toPage
	  toPage.cntPreviewer.move(0,2,this.pages.PageWidth, this.pages.PageHeight-2)
	  IF !ISNULL(this.oEmbeddedToolbar)
	   toPage.cntPreviewer.oTool = this.oEmbeddedToolbar
	   toPage.cntPreviewer.oTool.oCNT=toPage.cntPreviewer
	   toPage.cntPreviewer.oTool.lastInited()
	  ENDIF
	  this.oEmbeddedToolbar = null

	  IF NOT ISNULL(this.oExtensionhandler)
	   IF PEMSTATUS(this.oextensionhandler, "oPreviewContainer",5)
		this.oExtensionhandler.OpreVIEWContainer = toPage.cntPreviewer
	   endif
	  ENDIF
 ENDPROC


 PROCEDURE clearlink
	  LOCAL lnI
	  this.oToolbar = null
	  FOR lnI = 1 TO this.pages.PageCount
	   this.pages.Pages(lnI).cntPreviewer.clearlink()
	  ENDFOR
 ENDPROC


 PROCEDURE pagedeactivated
	  LPARAMETERS toPage
	  IF !ISNULL(toPage.cntPreviewer.oTool)
	   this.oToolbar = toPage.cntPreviewer.oTool
	  endif
	  toPage.cntPreviewer.oTool = null
 ENDPROC


 PROCEDURE previewxff
	  LPARAMETERS loXFF, tcPageCaption, tnPage, tcPrinterJobName

	  this.lShowTabForOneReport = this.lShowTabForOneReport
	  this.iTool = 0 &&this.iTool
	  this.visible = .T.
	  this.iEmail = 0 &&this.iEmail
	  this.ShowStatus = .T. &&this.ShowStatus
	  this.oExtensionHandler = null
	  this.classmaintoolbar = ""
	  this.DisplayMode = 1 &&this.DisplayMode
	  this.SetLanguage(.F.) &&this.cLanguage)
	  this.oProgress = .F. &&this.oProgress
	  this.iBook = 2 &&this.iBook
	  this.UnderlineHyperlinksOnPrint = 2 &&this.UnderlineHyperlinksOnPrint
	  this.oDisplayDefaults.startingpage = 1
	  this.oDisplayDefaults.zoomfactor = 100
	  this.oDisplayDefaults.pagespersheet = 1
	  this.oDisplayDefaults.defaultoutputfilename = "1x1"

	  Thisform.LockScreen = .T.
	  IF !EMPTY(tcPrinterJobName)
	   this.cJobName = tcPrinterJobName
	  ELSE
	   IF !EMPTY(tcPageCaption)
		this.cJobName = tcPageCaption
	   ENDIF
	  ENDIF

	  IF NOT EMPTY(tcPageCaption)
	   this.nextpagecaption = tcPageCaption
	  ELSE
	   this.nextpagecaption = " "
	  endif

	  LOCAL loSession, lnRetVal

	  IF VERSION(5)=900
	   loSession=EVALUATE([xfrx("XFRX#LISTENER")])
	  else
	   loSession=EVALUATE([xfrx("XFRX#INIT")])
	  ENDIF
	  IF VERSION(5)=900
	   loSession.XFRXPreviewer = this.getnewpage(tnPage)
	   loSession.previewReport(loXFF)
	  ELSE
	   lnRetVal = loSession.SetParams(,,,,,,"CNT")
	   loSession.setOtherParams(this.getnewpage())
	   loSession.TransformReport(loXFF)
	  endif
	  this.UpdatePageNumbers()
	  IF TYPE("this.oToolBar.oPageToGo")='O'
	      this.oToolBar.oPageToGo.TEXT1.SetPageStringValue()
	  ENDIF
	  Thisform.LockScreen = .F.
 ENDPROC


 PROCEDURE removepage
	  LPARAMETERS tnPage

	  IF EMPTY(tnPage) OR tnPage > this.pages.PageCount
	   tnPage = this.pages.PageCount
	  endif

	  LOCAL llLockScreen, loPage
	  llLockScreen = thisform.LockScreen
	  thisform.LockScreen = .t.
	  loPage = this.pages.pages(tnPage)
	  loPage.pageorder = this.pages.pagecount
	  this.pagedeactivated(loPage)
	  loPage.cntPreviewer.clearlink()
	  loPage.removeObject("cntPreviewer")
	  loPage = null
	  this.pages.pagecount = this.pages.pagecount - 1
	  thisform.LockScreen = llLockScreen
	  IF this.pages.PageCount<=1
	   IF !this.lShowTabForOneReport
		this.pages.Tabs = .f.
	   endif
	  endif
	  this.pages.ActivePage = tnPage-1
 ENDPROC


 PROCEDURE setextensionhandler
	  LPARAMETERS toHandler
	  IF TYPE("toHandler")="O"
	   this.oExtensionHandler = toHandler
	   RETURN .t.
	  ELSE
	   this.oExtensionHandler = null
	   RETURN .f.
	  ENDIF
 ENDPROC


 PROCEDURE setlanguage
  LPARAMETERS tcLanguage
  this.cLanguage = tcLanguage
 ENDPROC


 PROCEDURE repaint
  this.pages.Pages(this.pages.ActivePage).cntPreviewer.rePaint()
 ENDPROC


  PROCEDURE Move
   LPARAMETERS nLeft, nTop, nWidth, nHeight
   this.left = nLeft
   this.top = ntop
   this.Width = nWidth
   this.Height = nHeight
   this.Resize()
  ENDPROC


 PROCEDURE Resize
  this.pages.top=0
  this.pages.left=0
  this.pages.width=this.width
  this.pages.height=this.height
  IF this.pages.ActivePage>0
    this.pages.Pages(this.pages.ActivePage).cntPreviewer.move(0,0,this.pages.PageWidth, this.pages.PageHeight)
  endif
 ENDPROC


 PROCEDURE Init
  this.oExtensionHandler = null
  this.Resize()
 ENDPROC
 
 PROCEDURE Quit
	if vartype(this.oEmbeddedToolbar)='O'
		this.oEmbeddedToolbar.Destroy()
		this.oEmbeddedToolbar = .null.
	endif
    if vartype(this.oToolbar)='O'
		this.oToolbar.Destroy()
		this.oToolbar = .null.
	ENDIF
    if vartype(this.oBook)='O'
        this.oBook.oPreviewObject = .null.
		this.oBook.Destroy()
		this.oBook = .null.
	ENDIF	
	this.pages.Pages(this.pages.ActivePage).cntPreviewer.quit()	
	DoDefault()	
 ENDPROC

 PROCEDURE Event
  LPARAMETERS cEvent
  DoDefault()
 ENDPROC
 
 PROCEDURE Calculate
   DODEFAULT()
 ENDPROC
 
 PROCEDURE GotoPage
   LPARAMETERS nPageToGo
   local nInputPage   
   if not empty(nPageToGo)
       this.pages.Pages(this.pages.ActivePage).cntPreviewer.nPageNo=nPageToGo
   else
       nInputPage = val( inputbox(ah_msgformat("Pagina ( %1 - %2 ):","1",alltrim(str(this.nTotalPages))),ah_msgformat("Va alla pagina")) )
       If nInputPage>=1 and nInputPage<=this.nTotalPages
           this.pages.Pages(this.pages.ActivePage).cntPreviewer.nPageNo = nInputPage
       Endif    
   endif
   this.pages.Pages(this.pages.ActivePage).cntPreviewer.GotoPage()
   this.UpdatePageNumbers()
 ENDPROC

 PROCEDURE GotoFirstPage
   this.pages.Pages(this.pages.ActivePage).cntPreviewer.GotoFirstPage()
   this.UpdatePageNumbers()
 ENDPROC
 
 PROCEDURE GotoPreviousPage
   this.pages.Pages(this.pages.ActivePage).cntPreviewer.GotoPreviousPage()
   this.UpdatePageNumbers()
 ENDPROC

 PROCEDURE GotoNextPage
   this.pages.Pages(this.pages.ActivePage).cntPreviewer.GotoNextPage()
   this.UpdatePageNumbers()
 ENDPROC

 PROCEDURE GotoLastPage
   this.pages.Pages(this.pages.ActivePage).cntPreviewer.GotoLastPage()
   this.UpdatePageNumbers()
 ENDPROC

 PROCEDURE Zoom
   LPARAMETERS nZoomFactor
   this.pages.Pages(this.pages.ActivePage).cntPreviewer.Zoom(nZoomFactor)
   this.UpdatePageNumbers()
 ENDPROC
 
 PROCEDURE Print
   * this.pages.Pages(this.pages.ActivePage).cntPreviewer.Print()
   * this.Parent.Parent.Parent.Parent.oParentObject.opARENTOBJECT.w_RUNREP_SESSION.oXFFDOCUMENT.printdocument(SET("Printer",3), "Stampa xff")
   DO CP_CHPFUN WITH this.Parent.ocontained.opARENTOBJECT.oparentobject.oparentobject, 'StpClick'
   this.UpdatePageNumbers()
 ENDPROC
 
 PROCEDURE PrintOpt
   * this.pages.Pages(this.pages.ActivePage).cntPreviewer.Print()
   LOCAL L_OldError, L_xfrxprinterror
   L_OldError = On('error')   
   ON ERROR L_xfrxprinterror = .T.
   this.w_PRINTER = SET( "Printer",3)
   this.w_PRINTEROPTIONS = _xfPrinterProperties(w_PRINTER, "", .F.)
   this.w_PAGERANGE = ""
   this.w_COPIES = 1
   this.w_PAGES = this.nTotalPages
   this.w_OKPRINT = .F.
   * Se presente, disabilita la toolbar
   IF VARTYPE(this.oToolBar.enabled)="L"
       this.oToolBar.enabled = .F.
   ENDIF
   do CP_GETPRINTEROPTIONS with this
   * Se presente, riabilita la toolbar
   IF VARTYPE(this.oToolBar.enabled)="L"
       this.oToolBar.enabled = .T.
   ENDIF
   IF this.w_OKPRINT
       do while this.w_COPIES >= 1
           this.Parent.Parent.Parent.Parent.oParentObject.opARENTOBJECT.w_RUNREP_SESSION.oXFFDOCUMENT.printdocument(this.w_PRINTER, "Stampa xff", this.w_PAGERANGE, ,this.w_PRINTEROPTIONS)
           * this.Parent.Parent.Parent.Parent.oParentObject.opARENTOBJECT.w_RUNREP_SESSION.oXFFDOCUMENT.printdocument(GETPRINTER(), "Stampa xff")
           this.w_COPIES = this.w_COPIES - 1
       enddo       
   ENDIF       
   On Error &L_OldError
   this.UpdatePageNumbers()
 ENDPROC
 
 PROCEDURE Export
   this.pages.Pages(this.pages.ActivePage).cntPreviewer.Export()
   this.UpdatePageNumbers()
 ENDPROC
 
 PROCEDURE FindString
    LPARAMETERS cStringToFind, cCaseSensitive, NoMsg, bClearSearchResult
    LOCAL llAgain, llFound, cInputString
    IF cCaseSensitive
      this.oSearchParameters.lMatchCase=.t.
    ENDIF
    If not empty(cStringToFind)
      this.objects(1).page1.cntpreviewer.Find(cStringToFind, llAgain, this.oSearchParameters,NoMsg)
    else
      if !bClearSearchResult
        cInputString = alltrim( inputbox(ah_msgformat("Stringa da cercare"),ah_msgformat("Cerca")) )
      endif
      if not empty(cInputString)
        this.objects(1).page1.cntpreviewer.Find(cInputString, llAgain, this.oSearchParameters,NoMsg)
      endif
    endif
    this.oSearchParameters.lMatchCase=.f.
    this.UpdatePageNumbers()    
 ENDPROC
 
 PROCEDURE UpdatePageNumbers()
   this.nCurrentPageNumber = 0
   IF TYPE("this.pages.Pages(this.pages.ActivePage).cntPreviewer.nPageNo") = "N"
     this.nCurrentPageNumber = this.pages.Pages(this.pages.ActivePage).cntPreviewer.nPageNo
   ENDIF
   IF TYPE("this.pages.Pages(this.pages.ActivePage).cntPreviewer.nSheets") = "N"
     this.nTotalPages = this.pages.Pages(this.pages.ActivePage).cntPreviewer.nSheets
   ENDIF
   IF TYPE("this.Parent.Parent.Parent.Parent") = "O"
     IF PEMSTATUS(this.Parent.Parent.Parent.Parent,"Notifyevent",5)
       this.Parent.Parent.Parent.Parent.NotifyEvent("PreviewUpdated")
     ENDIF
   ENDIF
    IF TYPE("this.oToolBar.oPageToGo")='O'
        this.oToolBar.oPageToGo.TEXT1.SetPageStringValue()
    ENDIF
    IF TYPE("this.oToolBar.oBtnFirstPage.enabled")<>"U"
        this.oToolBar.oBtnFirstPage.enabled = .T.
        this.oToolBar.oBtnPreviousPage.enabled = .T.
        this.oToolBar.oBtnNextPage.enabled = .T.
        this.oToolBar.oBtnLastPage.enabled = .T.
        IF THIS.nCurrentPageNumber = 1
            * Se si va sulla prima pagina disabilita i bottoni per andare indietro
            this.oToolBar.oBtnFirstPage.enabled = .F.
            this.oToolBar.oBtnPreviousPage.enabled = .F.
        ENDIF
        IF THIS.nCurrentPageNumber=THIS.nTotalPages
            * Se si va sulla prima pagina disabilita i bottoni per andare avanti        
            this.oToolBar.oBtnNextPage.enabled = .F.
            this.oToolBar.oBtnLastPage.enabled = .F.
        ENDIF
    ENDIF
 ENDPROC
 
 PROCEDURE QuitPreview
     this.Parent.Parent.Parent.Parent.ecpQuit()
 ENDPROC
 
 PROCEDURE DblClickOnPreview
   LOCAL L_ZoomFactor
   * nZoom = -2 � 'adatta pagina'
   L_ZoomFactor = this.pages.Pages(this.pages.ActivePage).cntPreviewer.nZoom    
   IF this.pages.Pages(this.pages.ActivePage).cntPreviewer.nZoom <> -2
      this.Zoom(-2)
   ELSE
      this.Zoom(this.nOldzoomFactor)
   ENDIF
   this.nOldzoomFactor = L_ZoomFactor   
   * se presente la toolbar, la aggiorna
   IF TYPE("this.oToolBar.oZoomFactor.w_COMBO1") <> "U"
      this.oToolBar.oZoomFactor.w_COMBO1 = this.pages.Pages(this.pages.ActivePage).cntPreviewer.nZoom
      this.oToolBar.oZoomFactor.COMBO1.SetRadio()
   ENDIF
 ENDPROC 
 
 PROCEDURE SetControlsValue
     RETURN
 ENDPROC
 
ENDDEFINE
*
*-- EndDefine: zcntxfrxmultipage
**************************************************
**************************************************
*-- Class:           zxfpreviewerparameters
*-- ParentClass:     custom
*-- BaseClass:       custom
*-- Time Stamp:      11/11/09 12:40:11 PM
*
DEFINE CLASS zxfpreviewerparameters AS custom


	startingpage = 1
	zoomfactor = 100
	pagespersheet = "1x1"
	defaultoutputfilename = "output"
	Name = "zxfpreviewerparameters"
	
 PROCEDURE Event
  LPARAMETERS cEvent
  DoDefault()
 ENDPROC	
 
 PROCEDURE Calculate
  DoDefault()
 ENDPROC


ENDDEFINE
*
*-- EndDefine: zxfpreviewerparameters
**************************************************

DEFINE CLASS zxfcont AS container


	Width = 563
	Height = 285
	SpecialEffect = 2
	*-- Number of Sheets
	nsheets = 0
	*-- Object reference to active sheet
	activesheet = .NULL.
	*-- Zoom value
	nzoom = 100
	*-- Current page
	npageno = 0
	*-- Odkaz na formul�� pro hled�n�
	ofind = .NULL.
	*-- Object reference to object of settings for finding
	ofsetting = .NULL.
	*-- Flag of First init
	firstinit = .T.
	onkeyf = ""
	*-- Odkaz na objekt toolbaru
	otool = .NULL.
	*-- Flag of main toolbar Visible
	itool = 1
	*-- Flag of Bookmark panel Visible
	ibook = 2
	*-- Short cut popup exececute command
	shpexec = ""
	*-- Flag of Status bar Visible
	showstatus = .T.
	*-- Odkaz na objekt vlastnost� objekt�
	odm = .NULL.
	*-- Object reference to Report Controls toolbar
	odmtoolrc = .NULL.
	*-- Name of procedure or function for calling GetFile dialog like XP
	cgetfileex = ""
	*-- Flag of Enable Desig  Mode
	idm = -1
	*-- Flag of visible Property in Design Mode
	iprop = 0
	*-- Class for MainToolbar
	classmaintoolbar = ""
	*-- Alias deleted objectes
	delobjs = ""
	*-- Id language
	langid = "ENG"
	*-- Alias for  A Name's cursor
	anameobjs = ""
	*-- The Last Boomark ID
	bookid = 0
	*-- Extern CSS file
	css = ""
	*-- Settings for design mode
	odmsetting = .NULL.
	*-- Object reference to settings for main toolbar
	omtsetting = .NULL.
	*-- Object reference to Layout toolbar
	odmtooll = .NULL.
	*-- XFRXLIX folder
	xpath = ""
	*-- Class name of main toolbar settings
	clssetmaintoolbar = "xfrxtlbset"
	*-- Class name of Find settings
	clssetfind = "_xfrxFSettings"
	*-- Class name of DM settings
	clssetdm = "xfrxDMSetting"
	*-- XFXR object reference
	oxfrx = .NULL.
	*-- XFF Writer object reference
	oxfrxwriter = .NULL.
	onkeygo = ""
	*-- As IndexNewObject of xfrxDM
	indexnewobject = 0
	*-- Flag of generate Bookmarks for pages
	generatebookmarksforpages = .F.
	*-- Alias of cursor with stringes
	alang = ""
	*-- Object reference to "Go to page" dialog
	ogpage = .NULL.
	*-- Base CSS file
	cssbase = "xfrx.css"
	cjobname = "XFRX"
	*-- Active object
	activeobject = ""
	otoolbarcontainer = .F.
	oextensionhandler = "null"
	ngdiptoken = .F.
	displaymode = 1
	sheetleft = .F.
	sheettop = .F.
	canvashwnd = .F.
	npageshorizontal = 1
	npagesvertical = 1
	lpostinit = .F.
	asearchresults = .F.
	csearchstring = .F.
	linkalias = .F.
	oprogress = .F.
	iemail = 0
	underlinehyperlinksonprint = 2
	Name = "xfcont"

	*-- Flag of F4 key upon combobox
	cbozoom_f4 = .F.

	*-- Verze VFP
	vfpvers = .F.

	*-- Flag of SM/DM
	dmswitch = .F.

	*-- Flag of Bookmark panel
	bookswitch = .F.

	*-- Flag of property switch
	propswitch = .F.

	*-- Previews value PropSwitch
	proplast = .F.

	*-- Alias for Boomarks's cursor
	abookmarks = .F.

	*-- Alias for index (find)
	aindex = .F.
	generatebookmarksforpageswithoutbookmarks = .F.

	*-- Internal flag of error
	lerr = .F.
	otxtbox = .F.
	lstoprefreshing = .F.

	*-- Object reference to sheets extendet properties
	DIMENSION asheets[1]
    BOOK = .NULL.

	ADD OBJECT pages AS container WITH ;
		Top = 0, ;
		Left = 135, ;
		Width = 192, ;
		Height = 240, ;
		SpecialEffect = 2, ;
		TabIndex = 1, ;
		Name = "pages"


	ADD OBJECT horscrl AS xfscrollbar WITH ;
		Top = 12, ;
		Left = 135, ;
		Height = 18, ;
		SpecialEffect = 2, ;
		TabIndex = 2, ;
		Name = "horscrl", ;
		back.SpecialEffect = 2, ;
		back.Name = "back", ;
		cmdleft.FontName = "Webdings", ;
		cmdleft.FontSize = 10, ;
		cmdleft.Caption = "3", ;
		cmdleft.Name = "cmdleft", ;
		cmdright.FontName = "Webdings", ;
		cmdright.FontSize = 10, ;
		cmdright.Caption = "4", ;
		cmdright.Name = "cmdright", ;
		bar.Name = "bar"


	ADD OBJECT verscrl AS xfscrollbar WITH ;
		Top = 72, ;
		Left = 135, ;
		SpecialEffect = 2, ;
		TabIndex = 3, ;
		vertical = .T., ;
		Name = "verscrl", ;
		back.SpecialEffect = 2, ;
		back.Name = "back", ;
		cmdleft.FontName = "Webdings", ;
		cmdleft.FontSize = 10, ;
		cmdleft.Caption = "5", ;
		cmdleft.Name = "cmdleft", ;
		cmdright.FontName = "Webdings", ;
		cmdright.FontSize = 10, ;
		cmdright.Caption = "6", ;
		cmdright.Name = "cmdright", ;
		bar.Name = "bar"

	ADD OBJECT tmrmdmove AS timer WITH ;
		Top = 163, ;
		Left = 365, ;
		Height = 23, ;
		Width = 23, ;
		Enabled = .F., ;
		Interval = 250, ;
		Name = "tmrMdMove"


	ADD OBJECT cntdmm AS xfrxmove WITH ;
		Top = 0, ;
		Left = 424, ;
		Width = 5, ;
		Height = 240, ;
		SpecialEffect = 2, ;
		TabIndex = 6, ;
		typemove = 1, ;
		minleft = 50, ;
		moveanimation = .T., ;
		Name = "cntDMM"


	ADD OBJECT tmrfind AS timer WITH ;
		Top = 192, ;
		Left = 366, ;
		Height = 23, ;
		Width = 23, ;
		Enabled = .F., ;
		Interval = 250, ;
		Name = "tmrFind"


	ADD OBJECT txthide AS textbox WITH ;
		BorderStyle = 0, ;
		Height = 10, ;
		Left = -15, ;
		SpecialEffect = 1, ;
		Top = 0, ;
		Width = 10, ;
		Name = "txtHide"


	ADD OBJECT postinit AS timer WITH ;
		Top = 156, ;
		Left = 396, ;
		Height = 23, ;
		Width = 23, ;
		Name = "postInit"


	ADD OBJECT odisplaydefaults AS xfpreviewerparameters WITH ;
		Top = 48, ;
		Left = 456, ;
		Name = "oDisplayDefaults"
  
	PROCEDURE Visible_Assign
		LPARAMETERS bNewVal
		this.Visible = bNewVal
		this.pages.Visible = bNewVal
		this.horscrl.Visible = bNewVal
		this.verscrl.Visible = bNewVal
		this.SetAll("Visible",bNewVal)
	ENDPROC

	PROCEDURE Resize
		LOCAL llLockscreen,llDMove
		llLockScreen = Thisform.LockScreen
		Thisform.LockScreen = .T.
		liWidth=this.Width
		this.top=0
        this.left=0
		This.cntDMM.MaxLeft=INT(This.Width/8)*7
		This.cntDMM.MinLeft=INT(This.Width/2)
		IF This.BookSwitch
		   =This.Book.Move(0,0,This.Book.Width,This.Height)
		   llDMove=This.Book.Left+This.Book.Width &&=This.cntMove.Left
		ELSE
		   llDMove=0 &&=This.cntMove.Left
		ENDIF
		liLeft=IIF(This.BookSwitch,This.Book.Left+This.Book.Width+5,5)
		liWidth=IIF(This.DMSwitch AND This.PropSwitch AND This.Width#This.cntDMM.Left,This.cntDMM.Left,This.Width) - This.verscrl.cmdleft.width
        this.Pages.Move(liLeft,0,liWidth-liLeft,IIF((This.Height)<40,40,This.Height))

		liTop=This.Pages.Height
		liLeft=liLeft &&+IIF(.cntStatus.Visible,.cntStatus.Width,0)

		*This.horscrl.Move(liLeft,liTop,IIF((liWidth - liLeft)<40,40,liWidth - liLeft), 0) &&.cntStatus.height)
		* l-pld This.horscrl.Move(0,liTop-This.horscrl.height,IIF((liWidth - liLeft)<40,40,liWidth - liLeft), 0) &&.cntStatus.height)
		This.horscrl.Move(0,liTop-This.horscrl.height,IIF((liWidth - liLeft)<40,40,liWidth - liLeft)-This.verscrl.Width, This.horscrl.height)

		This.horscrl.Visible=(liWidth - liLeft)>=40
		*fabrizio
		This.horscrl.Visible=.T.

		This.verscrl.Move(liWidth,0,This.verscrl.Width,IIF(This.Pages.Height<40,40,This.Pages.Height))
		This.verscrl.Visible=(This.Height)>=40 && - .cntStatus.Height)>=40

		This.RepositionFindObj()

		This.RepositionActiveSheet()
		This.UpdateBars()
		This.calculatelefttop()
		Thisform.LockScreen=llLockScreen

		IF This.DMSwitch AND This.PropSwitch
		   =This.DM.Move(This.Width-This.DM.Width,0,This.DM.Width,This.Height)
		   llDMove=This.DM.Left=This.cntDMM.Left+This.cntDMM.Width
		   This.cntDMM.Move(This.DM.Left-This.cntDMM.Width,0,This.cntDMM.Width,This.Height)
		ELSE
		   llDMove=This.Width=This.cntDMM.Left
		   This.cntDMM.Move(This.Width,0,This.cntDMM.Width,This.Height)
		ENDIF
		=IIF(llDMove,This.cntDMM.Moved(),.T.)

		this._zoom()
		Thisform.LockScreen = llLockScreen
	ENDPROC

	*-- Add sheet to pages
	PROCEDURE addsheet
		LPARAMETERS tnHeight, tnWidth,liSheet

		IF !UPPER("xfrxlib.vcx")$UPPER(SET("CLASSLIB"))
		   Set Classlib To  (Sys(5)+Sys(2003)+"\xfrxlib\xfrxlib")  Additive
		ENDIF

		WITH This
		LOCAL lcName, loRef
		lcName = "_"+ALLTRIM(STR(This.nPageNo+liSheet,11))
		*? "creating sheet:",lcName
		.Pages.AddObject(lcName, "zxfrxsheet")
		loRef = EVALUATE(".Pages."+lcName)
		loRef.Move(0,0)
		loRef.nOriWidth = tnWidth
		loref.nOriHeight = tnHeight
		*loRef.Visible=.F.
		*loRef.BackStyle= 0
		.ActiveSheet = loRef

		LOCAL hApp, lcVariableName
		loRef.UniqueID= VAL(SYS(3))
		#DEFINE GWL_HINSTANCE           -6 
		#DEFINE WS_VISIBLE 0x10000000 
		#DEFINE WS_CHILD 0x40000000 
		#DEFINE WM_COMMAND 0x0111

		#define SS_NOTIFY           0x00000100
		#define SS_CENTERIMAGE      0x00000200
		#define SS_RIGHTJUST        0x00000400
		#define SS_REALSIZEIMAGE    0x00000800
		#define SS_SUNKEN           0x00001000

		#define SS_OWNERDRAW        0x0000000D
		hApp = GetWindowLong(this.gethwnd(), GWL_HINSTANCE) 

		loRef.canvashwnd = CreateWindow (0, "static",; 
		        "", WS_VISIBLE + WS_CHILD + SS_NOTIFY+SS_OWNERDRAW,; 
		        0, 0, loRef.width, loRef.height,; 
		        this.gethwnd(), loRef.UniqueID, hApp, 0)
		lcVariableName = "__xf_g_ca"+ALLTRIM(STR(ABS(loRef.UniqueID)))
		PUBLIC &lcVariableName
		*? "unique id:",loRef.UniqueID
		*? "handle:",loRef.canvashwnd
		STORE loRef TO (lcVariableName)
		loRef.setVisibility(this.Visible)

		**
		lcVariableName = "__xf_g_wl"+ALLTRIM(STR(ABS(loRef.canvashwnd)))
		PUBLIC &lcVariableName
		STORE GetWindowLong(loRef.canvashwnd, -4) TO (lcVariableName) 
		=_xfRegisterCanvas(loRef.canvashwnd)
		**
		lcVariableName = "__xf_g_ch"+ALLTRIM(STR(ABS(loRef.canvashwnd)))
		PUBLIC &lcVariableName
		STORE loRef TO (lcVariableName) 
		**

		**

		**
		#define WM_DRAWITEM                     0x002B
		#DEFINE WM_PARENTNOTIFY 0x0210
		#define WM_MOUSEWHEEL                   0x020A
		#DEFINE WM_COMMAND 0x0111

		ENDWITH
		RETURN loRef
	ENDPROC


	*-- Add textbox to active sheet
	PROCEDURE addtextbox
		LPARAMETERS lcName, tcText, tcFont, tnFontSize, tlBold, ;
					tlItalics, tnLeft, tnTop, tnWidth, tnHeight, ;
					tnpenred, tnpengreen, tnpenblue, ;
					tnMode, ;
					tnfillred,tnfillgreen,tnfillblue, ;
					tcAlign, tlUnderline,;
		            tcUniqueID, tcLinkName, tcLinkHref, tcOutline, tnOutlineStyle, tcTooltip,liRow

		* tcLinkName - jmeno aktualniho textboxu - aktualni textbox je "target" - bude se na neho skakat
		* tcLinkHref - aktualni textbox je link
		* tcOutline - na aktualni textbox se ma vytvorit bookmark -  nazev bookmarku je tcOutline
		* tnOutlineStyle - styl zobrazeni bookmarku - 1 italics, 2 - bold, 3 - bold italics
		* tcTooltip - tooltip aktualniho textboxu

		LOCAL loRef,lii
		lii=AT("_",lcName)
		IF VAL(SUBS(IIF(liRow=1,lcName,LEFT(lcName,lii-1)),3))>This.IndexNewObject
		   This.IndexNewObject=VAL(SUBS(IIF(liRow=1,lcName,LEFT(lcName,lii-1)),3))
		ENDIF

		* Magic values
		IF UPPER(tcFont)=="COURIER NEW" AND INLIST(tnfontSize,8,9)
		   tnHeight=tnHeight+2*1/96*10000
		ENDIF

		IF EMPTY(tcLinkHRef)
			This.ActiveSheet.AddObject(lcName, "xfrxlabel")
		ELSE
			This.ActiveSheet.AddObject(lcName, "xfrxHyperLink", tcLinkHref)
		ENDIF

		*This.ActiveSheet.AddObject(lcName, IIF(EMPTY(tcLinkHref),"xfrxlabel","xfrxHyperLink"),tcLinkHref)
		loRef = EVALUATE("This.ActiveSheet."+lcName)
		loRef.Name = LOWER(lcName)
		IF tcText = "="
			tcText = " "+tcText
		endif
		loRef.Caption = tcText
		loRef.nOriLeft = tnLeft
		loRef.nOriTop = tnTop
		loRef.nOriWidth = tnWidth
		loRef.nOriHeight = tnHeight

		loRef._Left = INT(tnLeft/(10000/96))
		loRef._Top = INT(tnTop/(10000/96))
		loRef._Width = INT(tnWidth/(10000/96))
		loRef._Height = INT(tnHeight/(10000/96))

			IF VARTYPE(this.otxtbox)="O" AND !ISNULL(this.otxtbox)
		#IF .F. &&VERSION(5)>=900
				loRef.Rotation = this.oTXTBox.nRotate
		#ENDIF
				loRef.FontStrikeThru = this.oTxtBox.lStriked
			ENDIF

		loRef.FontName = tcFont
		loRef.nOriFontsize = tnfontSize
		loRef.FontBold = tlBold
		loRef.FontItalic = tlItalics
		IF loRef.Type#_xfrxhyperlink
		   loref.FontUnderLine = tlUnderline
		ENDIF

		loRef.Alignment = IIF(tcAlign="right",1,IIF(tcAlign="center",2,0))

		IF tnpenred>=0 AND loRef.Type#_xfrxhyperlink
		   loRef.ForeColor = RGB(tnpenred, tnpengreen, tnpenblue)
		ENDIF
		loRef.BackStyle=IIF(tnMode = 0,1,0)
		IF tnfillred>=0
		   loRef.BackColor = RGB(tnfillred,tnfillgreen,tnfillblue)
		ENDIF
		loRef.ToolTipText=tcTooltip
		IF !EMPTY(tcLinkName)
		   loRef.LinkName=tcLinkName
		   =IIF(liRow>1,This.AddAName(This.ActiveSheet.Name,loRef.Name,tcLinkName),.T.)
		ENDIF
		IF !EMPTY(tcOutline)
		   loRef.BookName=tcOutline
		   =IIF(liRow=1,.T.,;
		        This.AddBookName(This.ActiveSheet.Name,loRef.Name,tcOutline,tnOutlineStyle,LEFT(lcName,lii-1))) && tcUniqueID
		ENDIF
		loRef._FontSize=(tnFontSize*100*1)/100
	ENDPROC


	*-- Zoom
	PROCEDURE zoom
		LPARAM liZoom,liUI

		IF liZoom <> -2
			this.nPagesHorizontal = 1
			this.nPagesVertical = 1
		endif
		liZoom=IIF(PCOUNT()=0,This.nZoom,liZoom)
		This._Zoom(liZoom)
		*This.event_Zoom(IIF(PCOUNT()<2,__xfrxlib_UI_tlb,liUI))
		this.gotoPage()
		This.RepositionFindObj()
		this.resize()
	ENDPROC


	*-- Go to previewous page
	PROCEDURE gotopreviouspage
		IF this.npageno > 1
			this.npageno = MAX(1,this.npageno - this.nPagesHorizontal * this.nPagesVertical)
			this.gotopage()
		   This.RepositionFindObj()
		endif
	ENDPROC


	*-- Internal method for go to any page
	PROCEDURE gotopage
		LPARAMETERS tcSearchString

		IF This.nSheets<1
			RETURN
		ENDIF
		LOCAL loRef,lcSheetName,llLS,liPage,loSP
		llLS=Thisform.LockScreen
		Thisform.LockScreen=.T.
		liPage=This.nPageNo

		lcSheetName = "_"+ALLTRIM(STR(liPage))

		this.lstoprefreshing = .t.
		IF !ISNULL(This.ActiveSheet) AND PEMSTATUS(This.ActiveSheet,"Name",5) AND !This.ActiveSheet.Name==lcSheetName
		   IF !This.ActiveSheet.Change && No modification
		      * Remove the whole page
		      IF this.DisplayMode = 0
			      This.Pages.RemoveObject(This.ActiveSheet.Name)
			  endif
		   ELSE
		      * Hide the active page
		      This.ActiveSheet.Visible=.F.
		   ENDIF
		ENDIF

		* if the page doesn't exist, create it
		IF .t. OR !PEMSTATUS(This.Pages,lcSheetName,5)
			IF this.DisplayMode = 0
				This.oXFRX.TransformReport(This.oXFRXWriter,,,liPage)
			ELSE
		*		ss = SECONDS()
		*		ACTIVATE SCREEN
				LOCAL lnI, loNewSheet
				CREATE CURSOR __xf_sheets(pageno i, used l)
				*? "pages: "
				*FOR lnI = 1 TO this.pages.ControlCount
				*	ACTIVATE scre
				*	?? this.pages.Controls(lnI).name
				*	this.pages.Controls(lnI).name = "__u"+ALLTRIM(STR(lnI))
		*		ENDFOR
				*? SECONDS()-ss
				LOCAL lnControl, llFound, lcMacro
				lnControl = 1
				FOR lnI = 0 TO this.nPagesHorizontal * this.nPagesVertical-1
					IF lnI+liPage > this.oXFRXWriter.PageCount
						EXIT
					endif
		*			? "going:",lnI
					lcSheetName = "_"+ALLTRIM(STR(liPage+lnI))
					IF PEMSTATUS(this.pages,lcSheetName,5) 
						IF EMPTY(tcSearchString)
							lcMacro = "this.pages."+lcSheetName
							&lcMacro..setVisibility(this.visible)
							LOOP
						ELSE
							This.Pages.RemoveObject(lcSheetName)
						endif
					endif
					this.oXFRXWriter.gotoPage(liPage+lni)

					llFound = .f.
					*? "trying to find a slot..."
					DO WHILE lnControl<=this.pages.controlCount AND !llFound
						*?? lnControl
						IF BETWEEN(VAL(SUBSTR(this.pages.Controls(lnControl).name,2)),liPage,liPage+this.nPagesHorizontal * this.nPagesVertical-1)
							lnControl = lnControl + 1 
						*	?? "..skip.."
							LOOP
						endif
						*? "reusing"
						llFound = .t.
						loNewSheet = this.pages.Controls(lnControl)
						loNewSheet.name = "_"+ALLTRIM(STR(This.nPageNo+lnI,11))
						loNewSheet.nOriWidth = This.oXFRXWriter.pagewidth
						loNewSheet.nOriHeight = This.oXFRXWriter.pageheight
						lnControl = lnControl + 1
					ENDDO
					IF !llFound
						loNewSheet = this.addSheet(This.oXFRXWriter.pageheight, This.oXFRXWriter.pagewidth, lnI)
					ENDIF
					lcFilename = "test"+ALLTRIM(STR(liPage+lni))+".emf"
					loNewSheet.deleteMetafile()
					IF liPage+lnI > this.oXFRXWriter.PageCount
						loNewSheet.emf = 0
						loNewSheet.setVisibility(.f.)
					else
						loNewSheet.emf = this.oXFRXWriter.savePicture_internal(, "p_emf", liPage+lni,liPage+lni,24,,,,,tcSearchString)
						loNewSheet.setVisibility(this.visible)
					endif
				ENDFOR
				DO WHILE lnControl<=this.pages.controlCount
					IF !BETWEEN(VAL(SUBSTR(this.pages.Controls(lnControl).name,2)),liPage,liPage+this.nPagesHorizontal * this.nPagesVertical-1)
						this.pages.Controls(lnControl).setVisibility(.F.)
					endif
					lnControl = lnControl + 1 
				enddo
			endif
		ENDIF
		this.calculateLeftTop()
		loSP=This.GetSheetProp(liPage) && get back the sheet properties

		STORE EVALUATE("This.Pages."+lcSheetName) TO loRef,This.ActiveSheet

		IF liPage>0 AND !loRef.Change AND loSP.iSelect>0 AND This.DMSwitch
			This.oDM.SelectObjectByList(@loRef,@loSP)
		ENDIF

		this.lstoprefreshing = .f.
		This.EDGoButtons()
		This._Zoom()

		=IIF(This.DMSwitch,This.oDMToolL.EDButtons(loSP.iSelect),.T.)
		=IIF(This.PropSwitch,This.DM.LastInited() AND This.DM.SelectObject(IIF(loSP.iSelect=1,loSP.aSelect(1),"")),.T.)

		Thisform.LockScreen=llLS
		this.repaint()
		
		this.Parent.Parent.Parent.updatePageNumbers()
	ENDPROC


	*-- Skip to next page
	PROCEDURE gotonextpage
		IF this.npageno< (this.nSheets - this.nPagesHorizontal * this.nPagesVertical + 1)
			this.npageno = this.npageno + this.nPagesHorizontal * this.nPagesVertical
			this.gotopage()
		    This.RepositionFindObj()
		endif
	ENDPROC


	PROCEDURE updatebars
		IF this.nSheets < 1 OR ISNULL(this.activesheet)
		*IF this.nSheets<1
		    RETURN
		endif
		this.horscrl.update(this.activesheet.width, this.pages.Width,  -this.activesheet.left)
		this.verscrl.update(this.activesheet.height, this.pages.height,  -this.activesheet.top)
	ENDPROC


	PROCEDURE updatepos
		LPARAMETERS hpos, tlvert
		this.pages.BackStyle= 0

		IF This.nSheets>0 AND IIF(tlVert,This.Pages.Height<This.ActiveSheet.Height,This.Pages.Width<This.ActiveSheet.Width)
		   STORE -hpos TO (IIF(tlVert,"This.ActiveSheet.Top","This.ActiveSheet.Left"))
		   This.ActiveSheet.repaint()
		ENDIF
	ENDPROC


	*-- Skip to first page
	PROCEDURE gotofirstpage
		this.npageno =  1
		this.gotopage()
		This.RepositionFindObj()
	ENDPROC


	*-- Skip to last page
	PROCEDURE gotolastpage
		this.npageno = MAX(1,this.nSheets - this.nPagesHorizontal * this.nPagesVertical + 1)
		this.gotopage()
		This.RepositionFindObj()
	ENDPROC


	*-- Add shape to active sheet
	PROCEDURE addrectangle
		LPARAMETERS lcName, tnLeft, tnTop, tnWidth, tnHeight, ;
		 tnpensize, tnfillpat, ;
		 tnfillred, tnfillgreen, tnfillblue, ;
		 tnpenpat, ;
		 tnpenred, tnpengreen, tnpenblue, ;
		 tnoffset

		IF VAL(SUBS(lcName,3))>This.IndexNewObject
		   This.IndexNewObject=VAL(SUBS(lcName,3))
		ENDIF

		This.ActiveSheet.AddObject(lcName, "xfrxrectangle")
		loRef = EVALUATE("This.ActiveSheet."+lcName)
		loRef.Name = LOWER(lcName)
		loRef.nOriLeft = tnLeft
		loRef.nOriTop = tnTop
		loRef.noriWidth = tnWidth
		loRef.noriHeight = tnHeight

		loRef._Left = INT(tnLeft/(10000/96))
		loRef._Top = INT(tnTop/(10000/96))
		loRef._Width = INT(tnWidth/(10000/96))
		loRef._Height = INT(tnHeight/(10000/96))

		loRef.BorderStyle=IIF(tnpenpat=8,1,IIF(tnpenpat>=3,tnpenpat+1,IIF(tnpenpat=1,3,tnpenpat)))
		STORE tnpensize TO loRef.BorderWidth,loRef._BorderWidth
		loRef.noriBorderWidth=loRef._BorderWidth*10000/96
		loRef.BorderColor=IIF(tnpenred = -1,0,RGB(tnpenred, tnpengreen, tnpenblue))

		*loRef.backstyle = tnmode
		loRef.BackStyle = 0

		IF tnFillRed = -1
		   loRef.FillColor = RGB(255, 255, 255)
		ELSE
		   loRef.FillColor = RGB(tnfillred, tnfillgreen, tnfillblue)
		ENDIF
		loRef.FillStyle = IIF(tnFillpat=0,1,IIF(tnFillpat=1,0,IIF(tnFillpat=4,5,IIF(tnFillpat=5,4,tnFillpat))))
		loRef.Curvature = tnoffset
	ENDPROC


	*-- Resets the Timer control so that it starts counting from 0.
	PROCEDURE reset
		LOCAL lii
		This.ActiveSheet = .NULL.

		WITH This.Pages
		FOR lii=.ControlCount  TO 1 STEP -1
		    .RemoveObject(.Controls(lii).Name)
		ENDFOR
		ENDWITH

		WITH This
		STORE 0 TO .nSheets,.nsheets,.BookID
		IF !ISNULL(.oDM)
		   .oDM.IndexNewObject=0
		ENDIF
		=IIF(!ISNULL(.oFSetting),.oFSetting.Reset(),.T.)

		*mh 11/3/2003
		.SetHostsInterface(.NULL.,.NULL.)

		IF USED(.DelObjs)
		   USE IN (.DelObjs)
		ENDIF

		IF USED(.ANameObjs)
		   USE IN (.ANameObjs)
		ENDIF

		IF USED(.ABookMarks)
		   USE IN (.ABookMarks)
		ENDIF

		IF USED(.AIndex)
		   USE IN (.AIndex)
		ENDIF
		ENDWITH
	ENDPROC


	*-- Add picture to active sheet
	PROCEDURE addpicture
		LPARAMETERS lcName, tnLeft, tnTop, tnWidth, tnHeight, ;
			tnpensize, tnfillpat, ;
			tnfillred, tnfillgreen, tnfillblue, ;
			tnpenpat, ;
			tnpenred, tnpengreen, tnpenblue, ;
			tnoffset, tcPath

		IF VAL(SUBS(lcName,3))>This.IndexNewObject
		   This.IndexNewObject=VAL(SUBS(lcName,3))
		ENDIF

		This.ActiveSheet.AddObject(lcName, "xfrximage")
		loRef = EVALUATE("This.ActiveSheet."+lcName)
		loRef.Name = LOWER(lcName)
		loref.nOriLeft = tnLeft
		loref.nOriTop = tnTop
		loref.noriWidth = tnWidth
		loref.noriHeight = tnHeight

		loref._Left = INT(tnLeft/(10000/96))
		loref._Top = INT(tnTop/(10000/96))
		loref._Width = INT(tnWidth/(10000/96))
		loref._Height = INT(tnHeight/(10000/96))

		loref.Picture = tcPath
		*SET STEP ON 
		*loref.PictureVal = FILETOSTR(tcPath)
		loRef._Stretch=loRef.Stretch

		*!*	loref.borderwidth = tnpensize

		*!*	IF tnFillRed = -1
		*!*		loref.fillcolor = RGB(255, 255, 255)
		*!*		loref.backstyle = 0
		*!*	else
		*!*		loref.fillcolor = RGB(tnfillred, tnfillgreen, tnfillblue)
		*!*		loref.fillstyle = 0
		*!*	endif
		*!*	IF tnpenred = -1
		*!*		loref.bordercolor = 0
		*!*	else
		*!*		loref.bordercolor = RGB(tnpenred, tnpengreen, tnpenblue)
		*!*	endif
		*!*	loref.curvature = tnoffset
	ENDPROC


	*-- Occurs when the user presses and releases a key.
	PROCEDURE keypress
		LPARAMETERS nKeyCode, nShiftAltCtrl
		IF TYPE("Thisform.ActiveControl")="O" AND !ISNULL(Thisform.ActiveControl)

		      IF nKeyCode=27
		         IF AT(".CNT.",UPPER(SYS(1272,Thisform.ActiveControl)))>0
		            STORE .NULL. TO This.oFind
		         ELSE
		            This.Quit()
		         ENDIF
		      ENDIF
		ENDIF

		IF TYPE("Thisform.ActiveControl")="O" AND !ISNULL(Thisform.ActiveControl)
		   IF AT(".DM.",UPPER(SYS(1272,Thisform.ActiveControl)))>0
		      RETURN .F.
		   ENDIF
		ENDIF

		IF nKeyCode = 7 AND nShiftAltCtrl=0 AND this.DMSwitch && 'DEL'
		   * Delete all selected objects
		   LOCAL loAS,lii,loObj,loSP,liPage
		   loAS=This.ActiveSheet
		   loSP=This.GetSheetProp(This.nPageNo) && Vra� vlastnosti sheetu 
		   liPage=VAL(SUBST(loAS.Name,2))

		   IF !USED(This.DelObjs)
		      CREATE CURSOR (This.DelObjs) (XX000 M, XX001 M)
		   ENDIF

		   FOR lii=1 TO loSP.iSelect
		       INSERT INTO (This.DelObjs) (XX000,XX001) VALUES (loSP.Name,loSP.aSelect(lii))

		       loObj=EVAL("loAs."+loSP.aSelect(lii))
		       IF USED(This.aBookMarks)
		          SELE (This.aBookMarks)
		          LOCATE FOR XX000==liPage AND XX001==loSP.aSelect(lii) AND XX002==loObj.BookName
		          IF FOUND()
		             DELE NEXT 1
		             =IIF(This.BookSwitch AND This.Book.Visible,This.Book.RemoveBookMark(XX005),.T.)
		          ENDIF
		       ENDIF

		       IF USED(This.aNameObjs)
		          SELE (This.aNameObjs)
		          LOCATE FOR XX000==liPage AND XX001==loSP.aSelect(lii) AND XX002==loObj.LinkName
		          IF FOUND()
		             DELE NEXT 1
		          ENDIF
		       ENDIF

		       IF USED(This.aIndex)
		          SELE (This.aIndex)
		          LOCATE FOR XX000==liPage AND XX001==loSP.aSelect(lii)
		          IF FOUND()
		             DELE NEXT 1
		          ENDIF
		       ENDIF

		       =loAS.RemoveObject(loSP.aSelect(lii)) AND ;
		        loAS.RemoveObject("cntSel_"+loSP.aSelect(lii))

		       * --OK--
		       IF PEMSTATUS(loAS,"lblHL",5) AND !ISNULL(This.oFSetting) AND ;
		          This.oFSetting.lPage>0 AND This.oFSetting.cObject==loSP.aSelect(lii)
		          loAS.lblHL.Visible=.F.
		       ENDIF

		       
		   NEXT

		   STORE "" TO loSP.aSelect
		   loSP.iSelect=0
		   =IIF(PEMSTATUS(This,"DM",5),This.DM.LastInited(.T.),.T.)
		   RETURN .T.
		ENDIF

		IF !This.DMSwitch
		   IF nKeyCode = 5 AND nShiftAltCtrl=0 && 'UPAR'
		      This.VersCrl.cmdLeft.Click()
		      RETURN .T.
		   ENDIF
		   
		   IF nKeyCode = 24 AND nShiftAltCtrl=0 && 'DNAR'
		      This.VersCrl.cmdRight.Click()
		      RETURN .T.
		   ENDIF

		   IF nKeyCode = 19 AND nShiftAltCtrl=0 && 'LEFT'
		      This.HorsCrl.cmdLeft.Click()
		      RETURN .T.
		   ENDIF

		   IF nKeyCode = 4 AND nShiftAltCtrl=0 && 'RIGHT'
		      This.HorsCrl.cmdRight.Click()
		      RETURN .T.
		   ENDIF

		ELSE
		   IF nKeyCode = 5 AND nShiftAltCtrl=0 && 'UPAR'
		      This.oDM.RepositionSelectedObjectBy(This.ActiveSheet,0,-1,.T.)
		      RETURN .T.
		   ENDIF
		   
		   IF nKeyCode = 24 AND nShiftAltCtrl=0 && 'DNAR'
		      This.oDM.RepositionSelectedObjectBy(This.ActiveSheet,0,1,.T.)
		      RETURN .T.
		   ENDIF

		   IF nKeyCode = 19 AND nShiftAltCtrl=0 && 'LEFT'
		      This.oDM.RepositionSelectedObjectBy(This.ActiveSheet,-1,0,.T.)
		      RETURN .T.
		   ENDIF

		   IF nKeyCode = 4 AND nShiftAltCtrl=0 && 'RIGHT'
		      This.oDM.RepositionSelectedObjectBy(This.ActiveSheet,1,0,.T.)
		      RETURN .T.
		   ENDIF
		 
		   IF nKeyCode = 141 AND nShiftAltCtrl=2 && 'UPAR'
		      This.oDM.ResizeSelectedObjectByN(This.ActiveSheet,_xfrx_Mode_ROB,-1)
		      RETURN .T.
		   ENDIF
		   
		   IF nKeyCode = 145 AND nShiftAltCtrl=2 && 'DNAR'
		      This.oDM.ResizeSelectedObjectByN(This.ActiveSheet,_xfrx_Mode_ROB,1)
		      RETURN .T.
		   ENDIF

		   IF nKeyCode = 26 AND nShiftAltCtrl=2 && 'LEFT'
		      This.oDM.ResizeSelectedObjectByN(This.ActiveSheet,_xfrx_Mode_ROR,-1)
		      RETURN .T.
		   ENDIF

		   IF nKeyCode = 2 AND nShiftAltCtrl=2 && 'RIGHT'
		      This.oDM.ResizeSelectedObjectByN(This.ActiveSheet,_xfrx_Mode_ROR,1)
		      RETURN .T.
		   ENDIF
		ENDIF

		IF nKeyCode = 18 AND nShiftAltCtrl=0 && 'PAGEUP'
		   This.gotoPreviousPage()
		   RETURN .T.
		ENDIF

		IF nKeyCode = 3 AND nShiftAltCtrl=0 && 'PAGEDOWN'
		   This.gotoNextPage()
		   RETURN .T.
		ENDIF

		IF nKeyCode = 1 AND nShiftAltCtrl=0 && HOME
		   This.goToFirstPage()
		   RETURN .T.
		ENDIF

		IF nKeyCode = 6 AND nShiftAltCtrl=0 && 'END'
		   This.goToLastPage()
		   RETURN .T.
		ENDIF

		IF nKeyCode = 33 AND nShiftAltCtrl=4 && 'CTRL+F'
		   This.ShowFind(__xfrxlib_UI_status)
		   RETURN .T.
		ENDIF

		IF nKeyCode = 7 AND nShiftAltCtrl=4 && 'CTRL+G'
		   This.GoToPageX(__xfrxlib_UI_status)
		   RETURN .T.
		ENDIF
		RETURN .F.
	ENDPROC


	*-- Enabled/Disable buttons for skip to page
	PROCEDURE edgobuttons
		LOCAL llLS,loSTA
		IF !ISNULL(This.oTool)
		   llLS=This.oTool.LockScreen
		   This.oTool.LockScreen=.T.

		   loSTA=This.oTool
		   This.oTool.LockScreen=llLS
		ENDIF

	ENDPROC


	*-- Find nay value
	PROCEDURE find
		LPARAM lcValue,llAgain,loFSet,NoMSG
		LOCAL bFound
		this.cSearchString = lcValue
		this.book=this.Parent.Parent.Parent.oBooK
		bFound = .t.
		IF loFSet.lMatchCase
			SELECT RECNO() rn, name, PAGE FROM (this.oXFRXWriter.cXFFAlias) WHERE (rcType="T" OR rcType="L") AND AT(lcValue, TEXT)>0 ;
				ORDER BY PAGE INTO CURSOR _xf_sr1
		ELSE
			SELECT RECNO() rn, name, PAGE FROM (this.oXFRXWriter.cXFFAlias) WHERE (rcType="T" OR rcType="L") AND ATC(lcValue, TEXT)>0 ;
				ORDER BY PAGE INTO CURSOR _xf_sr1
		endif

		IF RECCOUNT("_xf_sr1") = 0
			*USE IN SELECT("_xf_sr1")
		*	WAIT WIND (This.GetString(__xfrxlib_NotFound)) NOWAIT
		    IF !NoMSG
			  this.Displaymessage("Non trovato")
			endif  
			*RETURN .f.
			bFound = .f.
		ENDIF

		thisform.LockScreen = .t.
		this.lstoprefreshing = .t.
		LOCAL lnOcc, lnAt
		CREATE CURSOR _xf_searchResults (foundstr C(30), page i, occ i, name c(12))
		SELECT _xf_sr1
		SCAN all
			SELECT (this.oXFRXWriter.cXFFAlias)
			GO (_xf_sr1.rn)
			lnOcc = 1
			lctext = text
			lnAt = ATC(lcValue, lcText, lnOcc)
			DO WHILE lnAt>0
				INSERT INTO _xf_searchResults VALUES (SUBSTR(lcText,lnAt,30), _xf_sr1.page, lnOcc, _xf_sr1.name)
				lnOcc = lnOcc + 1
				lnAt = ATC(lcValue, lcText, lnOcc)
			ENDDO
		ENDSCAN
		GO TOP IN _xf_sr1
		this.activateobject(_xf_sr1.page, _xf_sr1.name, .t.)
		USE IN SELECT("_xf_sr1")
		SELECT _xf_searchResults
		this.shbook(.t.)
		IF VARTYPE(this.book)='O'
		    IF bfound
			    this.book.showSearchResults()
			ELSE
			    this.book.clearSearchResults()			
			endif
		ENDIF		
		IF VARTYPE(this.Parent.Parent.Parent.oZoomBook)='O' OR VARTYPE(this.Parent.Parent.Parent.oTreeBook)='O'
            * Se lo zoom o la treeview sono presenti, inserisce i risultati nella ricerca
            LOCAL L_NC, L_NC2, L_FND_STR, L_FND_PAG, L_FND_OCC, L_FND_NAM, L_CNG_PAG, L_TRE_ROW, L_LVLKEY
            IF VARTYPE(this.Parent.Parent.Parent.oZoomBook)='O'
                * zoom
                L_NC = this.Parent.Parent.Parent.oZoomBook.cCursor
                SELECT (L_NC)
                ZAP
            ENDIF
            IF VARTYPE(this.Parent.Parent.Parent.oTreeBook)='O'
                * treeview
                IF EMPTY( this.Parent.Parent.Parent.oTreeBooK.cCursor )
                    * crea il cursore per la treeview
                    L_NC2 = SYS( 2015 )
                    CREATE CURSOR ( L_NC2 ) ( LVLKEY C(200), FND_STR C(30) )
                    this.Parent.Parent.Parent.oTreeBooK.cCursor = L_NC2
                ENDIF
                L_NC2 = this.Parent.Parent.Parent.oTreeBooK.cCursor
                SELECT (L_NC2)
                ZAP
            ENDIF
            L_FND_PAG = 0
            L_TRE_ROW = 0
            SELECT("_xf_searchResults")
            GO TOP
            SCAN
                L_FND_STR = LEFT(foundstr, 30)
                L_CNG_PAG = L_FND_PAG <> page
                L_FND_PAG = page
                L_FND_OCC = occ
                L_FND_NAM = LEFT(name, 30)
                IF VARTYPE(this.Parent.Parent.Parent.oZoomBook)='O'
                    * zoom
                    SELECT (L_NC)
                    APPEND BLANK
                    REPLACE FND_STR WITH L_FND_STR, FND_PAG WITH L_FND_PAG, FND_OCC WITH L_FND_OCC, FND_NAM WITH L_FND_NAM
                ENDIF                
                IF VARTYPE(this.Parent.Parent.Parent.oTreeBook)='O'
                    * treeview
                    SELECT (L_NC2)
                    IF L_CNG_PAG
                        APPEND BLANK                    
                        * se la pagina � nuova, viene inserita
                        REPLACE LVLKEY WITH RIGHT( "000000" + ALLTRIM( STR( L_FND_PAG ) ), 6 ), FND_STR WITH "Pag. " + ALLTRIM( STR( L_FND_PAG ) )
                        L_TRE_ROW = 0
                        L_CNG_PAG = .F.
                    ENDIF
                    L_TRE_ROW = L_TRE_ROW + 1
                    L_LVLKEY = RIGHT( "000000" + ALLTRIM( STR( L_FND_PAG ) ), 6 ) + "." + RIGHT( "000000" + ALLTRIM( STR( L_TRE_ROW ) ), 6 )                    
                    APPEND BLANK
                    REPLACE LVLKEY WITH L_LVLKEY, FND_STR WITH L_FND_STR
                ENDIF                
                SELECT("_xf_searchResults")
*                SKIP		
            ENDSCAN
            this.Parent.Parent.Parent.oZoomBooK.Refresh()
            * Notifica l'evento alla maschera che contiene lo zoom
            
            IF RECCOUNT("_xf_searchResults") > 0
                IF PEMSTATUS(this.Parent.Parent.Parent.oZoomBooK.Parent.Parent.Parent.Parent,"Notifyevent",5)
                    this.Parent.Parent.Parent.oZoomBooK.Parent.Parent.Parent.Parent.NotifyEvent("BookmarkOn")
                ENDIF
            ENDIF
        ENDIF		
		USE IN SELECT("_xf_searchResults")

		This.postInit.Interval = 10
		thisform.Resize()
		thisform.LockScreen = .f.
		RETURN bFound


		LOCAL loPages,lii,lcPom,loObj,lcValue,llFound,llFirst,;
		      liLSS,llLS,llMatchCase,llSearchBackWard,liScope,llWildCards,lcName,;
		      liData,lcData,lcRValue,lcRPom

		loPages=This.Pages
		llFound=.F.
		llFirst=.T.

		llMatchCase=loFSet.lMatchCase
		llSearchBackWard=loFSet.lSearchBackWard
		liScope=loFSet.iScope
		llWildCards=loFSet.lUseWildCards

		llLS=Thisform.Lockscreen
		Thisform.Lockscreen=.T.


		IF !USED(This.AIndex)
		   * XX000 - Sheet number
		   * XX001 - Object Name
		   * XX006 - Index value

		   CREATE CURSOR (This.AIndex) (XX000 I, XX001 M, XX006 M)
		   INDEX ON XX000 TAG I01 COLLATE "MACHINE" FOR !DELE()
		ENDIF


		* --OK--
		IF loFSet.lPage>0 AND ;
		   LEFT(loFSet.cObject,2)="tx" AND ;
		   PEMSTATUS(loPages,"_"+LTRIM(STR(loFSet.lPage,11)),5) AND ;
		   PEMSTATUS(EVAL("loPages._"+LTRIM(STR(loFSet.lPage,11))),"lblHL",5)

		   STORE .F. TO ("loPages._"+LTRIM(STR(loFSet.lPage,11))+".lblHL.Visible")
		ENDIF


		* kdy� nevyhled�v� znova, pak mus�m za��t na aktu�ln� str�nce a objektu
		IF llSearchBackWard
		   FOR lii=IIF(liScope=1,This.nPageNo,IIF(llAgain,loFSet.lPage,This.nSheets)) TO IIF(liScope=1,This.nPageNo,1) STEP -1
		       * Vlastn� vyhled�v�n�

		       SELE (This.AIndex)
		       IF !SEEK(lii)
		          IF PEMSTATUS(loPages,"_"+LTRIM(STR(lii,11)),5) AND loPages.Controls(lii).Change
		             loPage=loPages.Controls(lii)
		             FOR liy=1 TO loPage.ControlCount
		                 loObj=loPage.Controls(liy)
		                 =IIF(LEFT(loObj.Name,2)="tx",This.AddIndex(lii,loObj.Name,loObj.Caption),.T.)
		             NEXT
		          ELSE
		             This.oXFRXWriter.ReadStringForFinding(This,lii)
		          ENDIF
		       ENDIF

		       SELE (This.AIndex)
		       =SEEK(STR(lii,11)) && Find page
		       
		       IF llAgain AND llFirst
		          LOCATE WHILE XX000=lii FOR XX001==loFSet.cObject
		          IF llWildCards
		*             SKIP -1
		          ENDIF
		       ELSE
		          *S: MJ 2005-04-07
		          SET KEY TO (lii+1)
		          GO TOP
		          SET KEY TO
		          SKIP -1
		          *E: MJ 2005-04-07
		         
		*          LOCATE WHILE XX000=lii+1
		*          IF FOUND()
		*             SKIP -1
		*          ELSE
		*          ENDIF

		       ENDIF

		       DO WHILE XX000=lii AND !BOF()
		           lcName=ALLT(XX001)
		           lcPom=IIF(llAgain AND llFirst,LEFT(XX006,loFSet.lSelStart-1),XX006) &&  AND !llWildCards

		           IF llWildCards
		              IF LEFT(m.lcValue,1)="*" 
		                 lcRValue=This.ReverseString(m.lcValue)
		                 lcRPom=This.ReverseString(m.lcPom)
		                 liSelStart=IIF(IIF(llMatchCase,LIKE(lcRValue,lcRPom),LIKE(LOWER(lcRValue),LOWER(lcRPom))),1,0)
		                 IF liSelStart>0
		                    =This.LikeEx(lcRValue,lcRPom,@liSelStart,@lcValue, llMatchCase)
		                    lcValue=This.ReverseString(m.lcValue)
		                    liSelStart=LEN(lcPom)-liSelStart+2-LEN(m.lcValue)
		                 ENDIF
		              ELSE
		                 liSelStart=IIF(IIF(llMatchCase,LIKE(lcValue,lcPom),LIKE(LOWER(lcValue),LOWER(lcPom))),1,0)
		                 IF liSelStart>0
		                    =This.LikeEx(lcValue,lcPom,@liSelStart,@lcValue, llMatchCase)
		                 ENDIF
		              ENDIF
		           ELSE
		              liSelStart=IIF(llMatchCase,AT(lcValue,lcPom),ATC(lcValue,lcPom))
		           ENDIF


		           IF liSelStart>0
		              This.ActiveHLObject(lii,lcName,liSelStart,@lcValue)
		              SELE (This.AIndex)
		              STORE lii TO loFSet.lPage,loFSet._lPage
		              STORE lcName TO loFSet.cObject,loFSet._cObject
		              STORE liSelStart TO loFSet.lSelStart,loFSet._lSelStart
		              loFSet._cValue=lcValue
		              llFound=.T.
		              EXIT
		           ENDIF
		           llFirst=.F.
		           SKIP -1
		       ENDDO

		       IF llFound
		          EXIT && Ukon�i smy�ku
		       ENDIF
		   NEXT
		ELSE
		   liLSS=loFSet.lSelStart

		   FOR lii=IIF(liScope=1,This.nPageNo,IIF(llAgain,loFSet.lPage,1)) TO IIF(liScope=1,This.nPageNo,This.nSheets)
		       * Vlastn� vyhled�v�n�

		       SELE (This.AIndex)
		       IF !SEEK(lii)
		          IF PEMSTATUS(loPages,"_"+LTRIM(STR(lii,11)),5) AND loPages.Controls(lii).Change
		             loPage=loPages.Controls(lii)
		             FOR liy=1 TO loPage.ControlCount
		                 loObj=loPage.Controls(liy)
		                 =IIF(LEFT(loObj.Name,2)="tx",This.AddIndex(lii,loObj.Name,loObj.Caption),.T.)
		             NEXT
		          ELSE
		             This.oXFRXWriter.ReadStringForFinding(This,lii)
		          ENDIF
		       ENDIF

		       SELE (This.AIndex)
		       =SEEK(STR(lii,11)) && Find page

		       IF llAgain AND llFirst
		          LOCATE WHILE XX000=lii FOR XX001==loFSet.cObject
		          IF llWildCards
		 *            SKIP 1
		          ENDIF
		       ENDIF

		       SCAN WHILE XX000=lii
		            lcName=ALLT(XX001)
		            lcPom=IIF(llAgain AND llFirst,SUBST(XX006,loFSet.lSelStart+1),XX006) &&  AND !llWildCards
		            liSelStart=IIF(llWildCards,;
		                           IIF(IIF(llMatchCase,LIKE(lcValue,lcPom),LIKE(LOWER(lcValue),LOWER(lcPom))),1,0),;
		                           IIF(llMatchCase,AT(lcValue,lcPom),ATC(lcValue,lcPom)))

		            IF liSelStart>0 AND llWildCards
		               =This.LikeEx(lcValue,lcPom,@liSelStart,@lcValue, llMatchCase) && lcPom
		            ENDIF

		            IF liSelStart>0
		               This.ActiveHLObject(lii,lcName,IIF(llAgain AND llFirst,liLSS,0)+liSelStart,@lcValue) &&  AND !llWildCards
		               SELE (This.AIndex)
		               STORE lii TO loFSet.lPage,loFSet._lPage
		               STORE lcName TO loFSet.cObject,loFSet._cObject
		               STORE IIF(llAgain AND llFirst,liLSS,0)+liSelStart TO loFSet.lSelStart,loFSet._lSelStart
		               loFSet._cValue=lcValue
		               llFound=.T.
		               EXIT
		            ENDIF
		            llFirst=.F.
		       ENDSCAN

		       IF llFound
		          EXIT && Ukon�i smy�ku
		       ENDIF
		   NEXT
		ENDIF
		Thisform.Lockscreen=llLS
		IF !llFound
		*   WAIT WIND (This.GetString(__xfrxlib_NotFound)) NOWAIT
		   this.Displaymessage("Non trovato")
		   loFSet.lPage=0
		ENDIF
		RETURN llFound
	ENDPROC


	*-- Activate object for highlight
	PROCEDURE activehlobject
		LPARAM liPage, loObj,liSelStart,lcValue,llOnlyObj

		LOCAL loPage,loPages,liLeft,liTop,loHL,lcPom,liTW,liLVW,liWidth,liFTop,liFLeft,;
		      liOLeft,liOTop,liOFTop,liOFLeft,liMTop,liMLeft
		      
		LOCAL ARRAY laFont(6)

		IF !llOnlyObj
		   This.nPageNo=liPage
		   This.GoToPage()
		ENDIF

		loPages=This.Pages
		loPage=This.ActiveSheet
		IF TYPE("loObj")="C"
		   loObj=EVAL("loPage."+loObj)
		ENDIF

		=IIF(!PEMSTATUS(loPage,"lblHL",5),loPage.AddObject("lblHL","xfrxlblHL"),.T.)
		loHL=loPage.lblHL

		laFont(1)=_SCREEN.FontName
		laFont(2)=_SCREEN.FontSize
		laFont(3)=_SCREEN.FontBold
		laFont(4)=_SCREEN.FontItalic
		laFont(5)=_SCREEN.FontStrikethru
		laFont(6)=_SCREEN.FontUnderline

		* Kopie vlastnosti fontu
		STORE loObj.FontName TO loHL.FontName,_SCREEN.FontName
		STORE loObj.FontSize TO loHL.FontSize,_SCREEN.FontSize
		STORE loObj.FontBold TO loHL.FontBold,_SCREEN.FontBold
		STORE loObj.FontItalic TO loHL.FontItalic,_SCREEN.FontItalic
		STORE loObj.FontStrikethru TO loHL.FontStrikethru,_SCREEN.FontStrikethru
		STORE loObj.FontUnderline TO loHL.FontUnderline,_SCREEN.FontUnderline

		lcPom=loObj.Caption

		loHL.Caption=SUBST(lcPom,liSelStart,LEN(lcValue))
		liWidth=_SCREEN.TextWidth(loHL.Caption)
		liLVW=_SCREEN.TextWidth(LEFT(lcPom,liSelStart-1))

		IF loObj.Alignment=0 && Left
		   loHL.Move(loObj.Left+liLVW,loObj.Top,liWidth,loObj.Height)
		ELSE
		   liTW=_SCREEN.TextWidth(lcPom)
		   loHL.Move(loObj.Left+IIF(loObj.Alignment=1,loObj.Width-liTW,(loObj.Width-liTW)/2)+liLVW,loObj.Top,liWidth,loObj.Height)
		ENDIF

		_SCREEN.FontName=laFont(1)
		_SCREEN.FontSize=laFont(2)
		_SCREEN.FontBold=laFont(3)
		_SCREEN.FontItalic=laFont(4)
		_SCREEN.FontStrikethru=laFont(5)
		_SCREEN.FontUnderline=laFont(6)

		loHL.Visible=.T.

		IF llOnlyObj
		   RETURN
		ENDIF

		IF loPages.Width>loPage.Width
		   liLeft=loPage.Left
		ELSE
		   liLeft=loPage.Left+(loPages.Width/2-(loPage.Left+loHL.Left))
		   IF liLeft>0 AND liLeft+loPage.Width>loPages.Width &&  AND loPage.Width>loPages.Width
		      liLeft=0
		   ELSE
		      IF liLeft<0 AND liLeft+loPage.Width<loPages.Width &&  AND loPage.Width>loPages.Width
		         liLeft=loPages.Width-loPage.Width
		      ENDIF
		   ENDIF
		ENDIF

		IF loPages.Height>loPage.Height
		   liTop=loPage.Top
		ELSE
		   liTop=loPage.Top+(loPages.Height/2-(loPage.Top+loHL.Top))
		   IF liTop>0 AND liTop+loPage.Height>loPages.Height &&  AND loPage.Height>loPages.Height
		      liTop=0
		   ELSE
		      IF liTop<0 AND liTop+loPage.Height<loPages.Height &&  AND loPage.Height>loPages.Height
		         liTop=loPages.Height-loPage.Height
		      ENDIF
		   ENDIF
		ENDIF

		loPage.Move(liLeft,liTop)
		This.UpdateBars()

		IF ISNULL(This.oFind) && If toolbar not visible
		   RETURN
		ENDIF

		* Nyn� zkontroluj kde je toolbar
		* pokud toolbar p�ekr�v� nalezen� objekt (jen hledanou ��st)
		* pak toolbar posu� tak aby nalezen� ��st byla vid�t

		liOLeft=OBJTOCLIENT(loHL,2)+IIF(loPage.Left<0,loPage.Left,0) && +Thisform.Left
		liOTop=OBJTOCLIENT(loHL,1)+SYSMETRIC(9)+SYSMETRIC(3)+IIF(loPage.Top<0,loPage.Top,0) && +Thisform.Top

		*debugout str(lioleft,11)+str(liotop,11)
		*debugout str(This.oFind.LEFT,11)+str(This.oFind.Left+This.oFind.Width+2*SYSMETRIC(4),11)
		*debugout str(This.oFind.Top,11)+str(This.oFind.Top+SYSMETRIC(34)+2*SYSMETRIC(3)+This.oFind.Height,11)
		*debugout "======================="


		IF (BETWEEN(liOTop,This.oFind.Top,This.oFind.Top+SYSMETRIC(34)+2*SYSMETRIC(3)+This.oFind.Height) OR ;
		    BETWEEN(liOTop+loHL.Height,This.oFind.Top,This.oFind.Top+SYSMETRIC(34)+2*SYSMETRIC(3)+This.oFind.Height)) AND ;
		   (BETWEEN(liOLeft,This.oFind.Left,This.oFind.Left+This.oFind.Width+2*SYSMETRIC(4)) OR ;
		    BETWEEN(liOLeft+loHL.Width,This.oFind.Left,This.oFind.Left+This.oFind.Width+2*SYSMETRIC(4)))

		   STORE This.oFind.Top TO liFTop,liOFTop
		   STORE This.oFind.Left TO liFLeft,liOFLeft

		   liFTop=liOTop+loHL.Height+1
		   
		   IF liFTop+SYSMETRIC(34)+2*SYSMETRIC(3)+This.oFind.Height>loPages.Height
		      liFTop=liOTop-(SYSMETRIC(34)+2*SYSMETRIC(3)+This.oFind.Height)

		      IF liFTop<OBJTOCLIENT(loPages,1)+SYSMETRIC(9)+SYSMETRIC(3)
		         liFTop=liOTop+loHL.Height+1
		         
		         liFLeft=liOLeft+loHL.Width+1
		         IF liFLeft+This.oFind.Width+2*SYSMETRIC(4)>loPages.Width
		            liFLeft=liOLeft-(This.oFind.Width+2*SYSMETRIC(4))
		         ENDIF
		      ENDIF
		   ENDIF

		   liFLeft=IIF(liFLeft<0 OR liFLeft>loPages.Width,0,liFLeft)
		   liFTop=IIF(liFTop<0 OR liFTop>loPages.Height,0,liFTop)

		   This.oFind.Move(liFLeft,liFTop)

		   liMTop=this.MROW(Thisform.Name,3)+(liFTop-liOFTop)
		   liMLeft=this.MCOL(Thisform.Name,3)+(liFLeft-liOFLeft)
		   
		   MOUSE AT liMTop,liMLeft WINDOW (Thisform.Name) PIXELS
		ENDIF
	ENDPROC


	*-- Method for inicialization
	PROCEDURE lastinited
		LPARAM liFirstPage
		LOCAL liDelta,loSTA,lii,loStatus
		liDelta=0
		=IIF(!ISNULL(This.oXFRXWriter),This.oXFRXWriter.ReadAllAnchors(This),.T.)

		IF USED(This.ABookMarks) AND RECC(This.ABookMarks)>0 AND ;
		   (This.GenerateBookmarksForPages OR !This.GenerateBookmarksForPages AND This.GenerateBookmarksForPagesWithoutBookmarks)
		   FOR lii=1 TO This.nSheets
		       =IIF(This.GenerateBookmarksForPages OR !SEEK(STR(lii,11),This.ABookMarks),;
		            This.AddBookName(lii,"",__xfrxlib_Page+LTRIM(STR(lii,11)),0,"_"+LTRIM(STR(lii,11))),.T.)
		   NEXT
		ENDIF

		liFirstPage = this.oDisplayDefaults.startingpage

		This.nPageNo=IIF(PCOUNT()=0 OR TYPE("liFirstPage")#"N",1,liFirstPage)
		IF This.iBook=__xfrxlib_book_Show OR ;
		   (This.iBook=__xfrxlib_book_Auto AND USED(This.ABookMarks) AND RECC(This.ABookMarks)>0)
		   This.SHBook(.T.,__xfrxlib_UI_menu)
		ELSE
		   This.SHBook(.T.,__xfrxlib_UI_menu)
		   This.SHBook(.F.,__xfrxlib_UI_menu)

		ENDIF

		This.GoToPage()
		* Show main toolbar
		IF This.iTool>=__xfrxlib_tlb_Show
		   This.ShowToolbar(.T.)
		ENDIF



		loSTA.Visible=This.ShowStatus



		This.postInit.Interval = 50

		IF this.oDisplayDefaults.PagesPerSheet != "1x1"
			this.setupnup(this.oDisplayDefaults.PagesPerSheet)
			IF !ISNULL(This.oTool) AND PEMSTATUS(This.oTool,"cboNUP",5)
				This.oTool.cboNUP.Value=this.oDisplayDefaults.PagesPerSheet
			ENDIF
		ELSE
			this.zoom(this.oDisplayDefaults.zoomfactor, 3)
			this.event_zoom(__xfrxlib_UI_tlb)
		ENDIF
	ENDPROC


	*-- Zobraz� z�kladn� toolbar
	PROCEDURE showtoolbar
		LPARAM llShow
		llShow=IIF(ISNULL(llShow),IIF(ISNULL(This.oTool),.T.,!This.oTool.Visible),llShow)
		IF ISNULL(This.oMTSetting)
		   This.oMTSetting=CREATEOBJECT(This.ClsSetMainToolbar)
		   This.oMTSetting.iDock = 0
		ENDIF

		IF ISNULL(This.oTool) AND llShow
			IF NOT ISNULL(this.oToolbarcontainer)
				this.oTool = this.oToolbarcontainer
				this.oTool.init(this)
			    This.otool.LastInited()
			ENDIF

			IF NOT ISNULL(this.oExtensionhandler)
				IF PEMSTATUS(this.oExtensionhandler, "ToolbarOnInit", 5)
					this.oExtensionhandler.ToolbarOnInit(this.oTool)
				ENDIF
				IF PEMSTATUS(this.oextensionhandler, "OpreVIEWContainer",5)
					this.oExtensionhandler.OpreVIEWContainer = this
				endif
			ENDIF

		ELSE
		   IF !ISNULL(This.oTool)
		      IF !llShow
		         This.oTool.SavePosition()
		         This.oTool.oCNT=.NULL.
		         This.oTool=.NULL.
		      ELSE
		         This.oTool.EDButtons()
		      ENDIF
		   ENDIF
		ENDIF
	ENDPROC


	*-- Sma�e objektov� linky
	PROCEDURE clearlink
		this.lstoprefreshing = .t.
		IF type("This.PAGES.name")="C"
		   FOR lnI = 1 TO this.pages.ControlCount
		   	this.pages.Controls(lnI).Destroy()
		   endfor
		ENDIF

		IF PEMSTATUS(This,"DM",5)
		   STORE .NULL. TO This.DM.oCNT
		ENDIF

		IF !ISNULL(This.oDM)
		   STORE .NULL. TO This.oDM.oCNT
		ENDIF

		STORE .NULL. TO This.oFind,This.oTool,;
		               This.oDMToolRC, This.oDMToolL,;
		               This.ActiveSheet,This.aSheets,This.oDM,;
		               This.oXFRX,This.oXFRXWriter, this.oToolbarcontainer

		=IIF(!ISNULL(This.oGPage),This.oGPage.Release(),.T.)

		IF NOT ISNULL(this.oExtensionhandler)
			IF PEMSTATUS(this.oextensionhandler, "OpreVIEWContainer",5)
				this.oExtensionhandler.OpreVIEWContainer = null
			endif
		ENDIF
	ENDPROC


	*-- Zobraz�/skryje bookmarky
	PROCEDURE shbook
		LPARAM llVisible,liUI
		LOCAL llLockscreen,loIE,llNew,loPage
		llVisible=IIF(PCOUNT()=0 OR ISNULL(llVisible),!This.BookSwitch,llVisible)



		llLockScreen = Thisform.LockScreen
		Thisform.LockScreen = .T.

		* Pokud to m� zobrazit a neexistuje to
		IF llVisible AND !PEMSTATUS(This,"Book",5)
		   llNew=.T.
		ENDIF

		This.Resize()

		&&This.event_SHBook(IIF(PCOUNT()<2,__xfrxlib_UI_tlb,liUI))
		This.event_SHBook(IIF(PCOUNT()<2,.F.,liUI))
		Thisform.LockScreen = llLockScreen  
	ENDPROC


	*-- Zobrazi dialog pro hled�n�
	PROCEDURE showfind
		LPARAM liUI
		LOCAL loForm,liLeft,liTop,loFSet,loCNT
		loCNT=This
		IF ISNULL(loCNT.oFSetting)
		   loCNT.oFSetting=CREATEOBJECT(This.ClsSetFind)
		ENDIF
		loFSet=loCNT.oFSetting
		IF ISNULL(loCNT.oFind)
		   STORE CREATEOBJECT("XFRXfrmFind_"+This.LangID,@loCNT) TO loForm,loCNT.oFind
		ELSE
		   loForm=loCNT.oFind
		ENDIF
		IF loFSet.oFirstFind
		   liLeft=(_Screen.Width-(loForm.Width+2*SYSMETRIC(4)))/2
		   liTop=(_Screen.Height-(loForm.Height+SYSMETRIC(34)+2*SYSMETRIC(3)))/2

		   liLeft=IIF(liLeft<0 OR liLeft>loCNT.Pages.Width,0,liLeft)
		   liTop=IIF(liTop<0 OR liTop>loCNT.Pages.Height,0,liTop)

		   loForm.Move(liLeft,liTop)
		   loFSet.oFirstFind=.F.
		ENDIF
		This.SetFocus()
		loForm.Show()
		This.tmrFind.Enabled=.T.
	ENDPROC


	*-- Method for call commands from html
	PROCEDURE gohtml
		LPARAM lcHash
		LOCAL lcValue,lcPom
		DO CASE
		   CASE lcHash=="first"
		        This.GoToFirstPage()
		        This.event_GoToPage(__xfrxlib_UI_html)

		   CASE lcHash=="prev"
		        This.GoToPreviousPage()
		        This.event_GoToPage(__xfrxlib_UI_html)

		   CASE lcHash=="next"
		        This.GoToNextPage()
		        This.event_GoToPage(__xfrxlib_UI_html)

		   CASE lcHash=="last"
		        This.GoToLastPage()
		        This.event_GoToPage(__xfrxlib_UI_html)

		   CASE LEFT(lcHash,5)="zoom_"
		        lcPom=SUBS(lcHash,6)
		        This.Zoom(VAL(lcPom),__xfrxlib_UI_html)

		ENDCASE
	ENDPROC


	*-- Event go to page
	PROCEDURE event_gotopage
		LPARAM liUI

		DO CASE 
		   CASE liUI=__xfrxlib_UI_tlb
		   CASE liUI=__xfrxlib_UI_html
		   CASE liUI=__xfrxlib_UI_menu
		   CASE liUI=__xfrxlib_UI_status
		ENDCASE
	ENDPROC


	*-- Event Zoom
	PROCEDURE event_zoom
		LPARAM liUI

		LOCAL loObj


	ENDPROC


	*-- Show shortcut popup
	PROCEDURE showscpopup
		LOCAL liVal,liZoom

	ENDPROC


	*-- Prints a character string on a Form object.
	PROCEDURE print
		LPARAMETERS cText

		IF NOT ISNULL(this.oExtensionhandler)
			IF PEMSTATUS(this.oExtensionhandler, "Print", 5)
				IF NOT this.oExtensionhandler.Print(This.oxfrxwriter)
					RETURN
				ENDIF
			ENDIF
		ENDIF

		LOCAL loOptions, loForm

		loOptions = CREATEOBJECT("xfrxcusprinteroptions")
		loOptions.oCNT=This
		loForm=CREATEOBJECT("xfrxfrmprinteroptions_"+This.LangID,@loOptions, this)
		if vartype(loForm) <> 'O'
			return
		endif
		loForm.Show()
		loOptions.oCNT=.NULL.

		IF NOT ISNULL(this.oExtensionhandler)
			IF PEMSTATUS(this.oExtensionhandler, "PrintOptions", 5)
				IF NOT this.oExtensionhandler.PrintOptions(This.oxfrxwriter, loOptions)
					RETURN
				ENDIF
			ENDIF
		ENDIF


		with loOptions
			IF NOT EMPTY(.printerName)
				This.oxfrxwriter.printDocument(.printerName, this.cJobName, .pageRange,,.cDEVMODE, .zoom, .alloddeven, .copies, this.Underlinehyperlinksonprint)
			ENDIF
		ENDWITH

	ENDPROC


	*-- Internal method for zooming
	PROCEDURE _zoom
		LPARAMETERS tnZoom
		IF this.nSheets<1
		    RETURN
		endif

		************************************************
		* Author: MJ
		* Date: 2003-07-06
		* Descr: optimization
		*************************************************
		IF PCOUNT()=0
		   tnzoom=This.nZoom
		ELSE
		   IF NOT EMPTY(tnZoom)
		      This.nZoom=tnzoom
		   ENDIF
		ENDIF
		IF EMPTY(tnZoom)
		   RETURN
		ENDIF
		tnZoom=This.GetZoomFactor(tnZoom,This.ActiveSheet)

		LOCAL i, nfactor, nfontSize,llLS,loPage,llVis,loSheet
		llLS=Thisform.LockScreen
		Thisform.LockScreen=.T.
		nFactor = 10000/(96*tnZoom/100)
		loSheet=This.ActiveSheet
		IF ISNULL(this.activesheet)
			RETURN
		endif
		WITH This.ActiveSheet
		llVis=._Visible
		This.RepositionActiveSheet()

		This.UpdateBars()
		.Visible = .T.
		IF llVis
		   ._Visible=.F.
		   .SetAll("Visible",.T.)
		ENDIF
		ENDWITH

		Thisform.LockScreen=llLS
		this.repaint()
	ENDPROC


	*-- Event Show/hide book
	PROCEDURE event_shbook
		LPARAM liUI

	ENDPROC


	*-- Quit method
	PROCEDURE quit
		This.ClearLink()
		thisform.Release()
		*RELE WIND (Thisform.Name)
	ENDPROC


	*-- Show Mode/Design Mode switch
	PROCEDURE shdesignm
		LPARAM llSwitch,liUI
		LOCAL llLockScreen,loPage,llProp,loAS,loSP
		llSwitch=IIF(PCOUNT()=0 OR ISNULL(llSwitch),!This.DMSwitch,llSwitch)

		llLockScreen = Thisform.LockScreen
		Thisform.LockScreen = .T.

		STORE llSwitch TO This.DMSwitch

		IF !llSwitch
		   This.PropLast=This.PropSwitch
		   This.SHProp(.F.,IIF(PCOUNT()<2,__xfrxlib_UI_tlb,liUI))
		   This.Pages.SetAll("Visible",llSwitch,"xfrxselect")

		   IF !ISNULL(This.oDMToolRC)
		      This.oDMToolRC.SavePosition()
		      This.oDMToolRC.oCNT=.NULL.
		      This.oDMToolRC=.NULL.
		   ENDIF

		   IF !ISNULL(This.oDMToolL)
		      This.oDMToolL.SavePosition()
		      This.oDMToolL.oCNT=.NULL.
		      This.oDMToolL=.NULL.
		   ENDIF

		ELSE
		   loAS=This.ActiveSheet
		   loSP=This.GetSheetProp(This.nPageNo) && Vra� vlastnosti sheetu 

		   IF ISNULL(This.oDMSetting)
		      This.oDMSetting=CREATEOBJECT(This.ClsSetDM)
		   ENDIF

		   IF ISNULL(This.oDM)
		      This.oDM=CREATEOBJECT("xfrxDM")
		      This.oDM.oCNT=This
		      This.oDM.IndexNewObject=This.IndexNewObject
		   ENDIF

		   This.PropSwitch=This.PropLast
		   This.SHProp(This.PropSwitch,IIF(PCOUNT()<2,__xfrxlib_UI_tlb,liUI))

		   IF ISNULL(This.oDMToolRC)
		      This.oDMToolRC=CREATEOBJECT("xfrxtlbobject",This)
		      This.oDMToolRC.LastInited()
		      This.oDMToolRC.SetButton(This.oDMSetting.oRC.iClass)
		      This.oDMToolRC.Show()
		   ELSE
		      This.oDMToolRC.SetButton(This.oDMSetting.oRC.iClass)
		      This.oDMToolRC.EDButtons()
		   ENDIF

		   IF ISNULL(This.oDMToolL)
		      This.oDMToolL=CREATEOBJECT("xfrxtlbLayout",This)
		      This.oDMToolL.LastInited()
		      This.oDMToolL.Show()
		   ELSE
		      This.oDMToolL.EDButtons()
		   ENDIF

		   =This.oDM.SelectObjectByList(This.ActiveSheet,@loSP)
		ENDIF

		Thisform.LockScreen = llLockScreen  
	ENDPROC


	*-- Event show/hide design mode
	PROCEDURE event_shdesignm
		LPARAM liUI


	ENDPROC


	*-- Show/hide properties
	PROCEDURE shprop
		LPARAM llSwitch,liUI
		LOCAL llLockScreen,llNew,loPage
		llSwitch=IIF(PCOUNT()=0 OR ISNULL(llSwitch),!This.PropSwitch,llSwitch)

		* Pokud to m� skr�t a neexistuje to
		IF !llSwitch AND !PEMSTATUS(This,"DM",5)
		   RETURN && Pak vysko�
		ENDIF

		llLockScreen = Thisform.LockScreen
		Thisform.LockScreen = .T.

		* Pokud to m� zobrazit a neexistuje to
		IF llSwitch AND !PEMSTATUS(This,"DM",5)
		   This.AddObject("DM","xfrxProp")
		   llNew=.T.
		ENDIF

		STORE llSwitch TO This.DM.Visible,This.cntDMM.Visible,This.PropSwitch
		This.Resize()

		IF llNew
		   This.DM.oCNT=This
		ENDIF

		This.DM.LastInited()

		This.RepositionFindObj()

		This.event_SHProp(IIF(PCOUNT()<2,__xfrxlib_UI_tlb,liUI))

		IF !llSwitch
		   This.txtHide.SetFocus()
		ENDIF

		Thisform.LockScreen = llLockScreen  
	ENDPROC


	*-- Event show/hide list of properties
	PROCEDURE event_shprop
		LPARAM liUI

	ENDPROC


	*-- Move highlighting object to new position
	PROCEDURE repositionfindobj
		IF !ISNULL(This.oFSetting) AND This.oFSetting.lPage=VAL(SUBST(This.ActiveSheet.Name,2)) && This.oFSetting.lPage>0
		   loPage=EVAL("This.Pages._"+LTRIM(STR(This.oFSetting.lPage,11)))
		   This.ActiveHLObject(This.oFSetting.lPage, EVAL("loPage."+This.oFSetting.cObject),This.oFSetting.lSelStart,This.oFSetting._cValue,.T.) && This.oFSetting.aValues(This.oFSetting.lValue)
		ENDIF
	ENDPROC


	*-- Return Zoom factor
	PROCEDURE getzoomfactor
		LPARAM liZoom,loPage
		* liZoom - Zoom index
		* loPage - Page (Sheet)

		IF liZoom=-1
		   liZoom=INT(This.Pages.Width / (loPage.NoRiWidth*96/10000)*100)
		ELSE
		   IF liZoom=-2
		      liZoom = INT(This.Pages.Width / (loPage.NoRiWidth*96/10000)*100)
		      liZoom = MIN(liZoom,INT(This.Pages.Height / (loPage.NoRiHeight*96/10000)*100))
		   ENDIF
		ENDIF

		IF this.nPagesHorizontal*this.nPagesVertical > 1
			LOCAL lnr, liWidth, liHeight
			liWidth = this.pages.width / this.nPagesHorizontal
			liHeight = this.pages.height / this.nPagesVertical
			lnr = loPage.noriWidth / loPage.noriHeight
			IF liWidth / lnr > liHeight
				liWidth = liHeight * lnr
			ELSE
				liHeight = liWidth / lnr
			endif
			liZoom = INT(liWidth / (loPage.NoRiWidth*96/10000)*100)
			liZoom = MIN(liZoom,INT(liHeight / (loPage.NoRiHeight*96/10000)*100))
		ENDIF

		RETURN liZoom
	ENDPROC


	*-- Move ActiveSheet to new position
	PROCEDURE repositionactivesheet
		LOCAL loLS,liTop,liLeft,loAS,nFactor,liWidth,liHeight
		llLS=Thisform.LockScreen
		Thisform.LockScreen=.T.
		loAS=This.ActiveSheet
		*ACTIVATE SCREEN
		*? SECONDS(),"reposition"
		*SET ASSERTS ON
		*ASSERT  .f. MESSAGE "reposition"
		FOR lnI = 0 TO this.nPagesHorizontal * this.nPagesVertical-1

			lcSheetName = "_"+ALLTRIM(STR(this.npageno+lnI))
			IF !PEMSTATUS(this.pages,lcSheetName,5)
				LOOP
			ENDIF
			loAS = EVALUATE("This.Pages."+lcSheetName) 

			IF TYPE("loAS")!="O" OR ISNULL(loAS)
			   RETURN
			ENDIF

			DO case
				CASE this.nPagesHorizontal * this.nPagesVertical = 1
					nFactor = 10000/(96*This.GetZoomFactor(This.nZoom,loAS)/100)

					liWidth=loAS.noRiWidth/nFactor
					liHeight=loAS.noRiHeight/nFactor

					liTop=IIF(This.Pages.Height<liHeight,;
					          IIF(loAS.Top+liHeight<This.Pages.Height,This.Pages.Height-liHeight,0),;
					          (This.Pages.Height-liHeight)/2)

					liLeft=IIF(This.Pages.Width<liWidth,;
					           IIF(loAS.Left+liWidth<This.Pages.Width,This.Pages.Width-liWidth,0),;
					           (This.Pages.Width-liWidth)/2)
				OTHERWISE
					liWidth = this.pages.width / this.nPagesHorizontal
					liHeight = this.pages.height / this.nPagesVertical
					LOCAL lnr
					lnr = loAS.noriWidth / loAs.noriHeight
					IF liWidth / lnr > liHeight
						liWidth = liHeight * lnr
					ELSE
						liHeight = liWidth / lnr
					endif
					liLeft = (lnI%this.nPagesHorizontal)*liWidth
					liTop = INT(lnI/this.nPagesHorizontal)*liHeight
			ENDCASE
			liTop=IIF(liTop>=0,liTop,loAS.Top)
			liLeft=IIF(liLeft>=0,liLeft,loAS.Left)
			loAS.Move(liLeft,liTop,liWidth,liHeight)
			*? liLeft, liTop, liWidth, liHeight
			loAS.resize()
			loAS.repaint()
		endfor
		*loAS.repaint()
		Thisform.LockScreen=llLS
	ENDPROC


	*-- Select object in design mode
	PROCEDURE selectobject
		LPARAM loObj
	ENDPROC


	*-- Add name for anchor to internal list
	PROCEDURE addaname
		LPARAM liPage,lcObjName,lcAName,llFind
		* liPage    - Sheet Name
		* lcObjName - Object Name
		* lcAName   - Anchor Name (Link Name)
		* llFind    - Flag of find

		LOCAL liSele
		liSele=SELE()
		IF !USED(This.ANameObjs)
		   * XX000 - Sheet number
		   * XX001 - Object Name
		   * XX002 - Target Name

		   CREATE CURSOR (This.ANameObjs) (XX000 I, XX001 M, XX002 M)
		ENDIF

		IF llFind
		   SELE (This.ANameObjs)
		   LOCATE FOR XX000==liPage AND XX001==lcObjName
		   llFind=FOUND()
		ENDIF
		IF !llFind
		   INSERT INTO (This.ANameObjs) (XX000, XX001, XX002) VALUES (liPage,lcObjName,lcAName)
		ELSE
		  REPL XX002 WITH lcAName
		ENDIF
		SELE (liSele)
	ENDPROC


	*-- Convert Sheet Name to Page number
	PROCEDURE snametosn
		LPARAM liNumber
		LOCAL lii,loPages
		lcSName=ALLT(lcSName)
		FOR lii=1 TO This.nSheets
		    IF This.Pages.Controls(lii).Name==lcSName
		       RETURN lii
		    ENDIF
		NEXT
		RETURN 0
	ENDPROC


	*-- Add BookMark to internal list
	PROCEDURE addbookname
		LPARAM liPage,lcObjName,lcBook,liBook,lcUID
		* liPage    - Sheet Number
		* lcObjName - Object Name
		* lcBook    - BookMark Name
		* liBook    - BookMark Style
		* lcUID     - Unique ID from FRX
		LOCAL llFound,liSele
		liSele=SELE()
		IF !USED(This.ABookMarks)
		   * XX000 - Sheet Number
		   * XX006 - Page/Object
		   * XX001 - Object Name
		   * XX002 - BookMark
		   * XX003 - BookMark style
		   * XX004 - UniqueID
		   * XX005 - ID Boomark
		   CREATE CURSOR (This.ABookMarks) (XX000 I, XX006 I, XX001 M, XX003 I, XX002 M, XX004 C(12), XX005 I)
		   INDEX ON STR(XX000,11)+STR(XX006,1) TAG I01 COLLATE "MACHINE"
		ENDIF

		SELE (This.ABookMarks)
		IF !EMPTY(lcUID)
		   LOCATE FOR XX004==PADR(lcUID,12)
		   llFound=FOUND()
		ENDIF
		IF !llFound
		   This.BookID=This.BookID+1 && Increment counter
		   INSERT INTO (This.ABookMarks) (XX000, XX001, XX002, XX003, XX004, XX005, XX006) ;
		          VALUES (liPage,lcObjName,lcBook, liBook, lcUID, This.BookID, IIF(EMPTY(lcObjName),0,1))
		ENDIF

		SELE (liSele)
	ENDPROC


	*-- Return object reference  to form
	PROCEDURE getform
		RETURN Thisform
	ENDPROC


	*-- Set internal information about new object
	PROCEDURE setnewobject
		LPARAM liClass
		LOCAL lii,loObj,loAS
		loAS=This.ActiveSheet
		loAS._Mode=IIF(liClass=0,_xfrx_Mode_None ,_xfrx_Mode_NewObject)

		loAS.SetMousePointer(IIF(liClass=0,.NULL.,2))
		FOR lii=1 TO loAS.ControlCount
		    loObj=loAS.Controls(lii)
		    =IIF(PEMSTATUS(loObj,"SetMousePointer",5),loObj.SetMousePointer(IIF(liClass=0,.NULL.,2)),.T.)
		NEXT
	ENDPROC


	*-- Set XFRX and XFRXWriter hosts interface
	PROCEDURE sethostsinterface
		LPARAM loXFRX,loXFRXWriter
		* loXFRX       - XFXR object reference
		* loXFRXWriter - XFF Writer object reference

		This.oXFRX=loXFRX
		This.oXFRXWriter=loXFRXWriter
	ENDPROC


	*-- Go to ... Page
	PROCEDURE gotopagex
		LPARAM liUI
		LOCAL loForm,loProp

		loProp=CREATEOBJECT("xfrxfrmpageprop")
		loProp.Page=This.nPageNo
		loProp.Pages=This.nSheets
		loForm=CREATEOBJECT("xfrxfrmpage_"+This.LangID,@loProp)
		This.oGPage=loForm
		loForm.Show()
		This.oGPage=.NULL.


		IF loProp.OK
		   This.nPageNo=loProp.Page
		   This.GoToPage()
		   This.RepositionFindObj()

		   This.event_GoToPage(IIF(PCOUNT()<2,__xfrxlib_UI_tlb,liUI))
		ENDIF
	ENDPROC


	*-- Return object Sheet properties, if not exist, creates it
	PROCEDURE getsheetprop
		Lparam liPage
		Local lii
		If m.liPage=0
			Return .Null.
		Endif
		lii=Ascan(This.aSheets,m.liPage)
		If lii=0
			lii=Asubsc(This.aSheets,Ascan(This.aSheets,0),1)
			If m.lii=0
				Return .Null.
			Endif
			This.aSheets(lii,2)=Createobject("xfrxsheetprop")
			This.aSheets(lii,1)=liPage
		Else
			lii=Asubsc(This.aSheets,m.lii,1)
		Endif
		Return This.aSheets(lii,2)
	ENDPROC


	*-- Add index value for finding
	PROCEDURE addindex
		LPARAM liPage,lcObjName,lcValue
		* liPage    - Sheet Name
		* lcObjName - Object Name
		* lcValue   - Anchor Name (Link Name)

		LOCAL liSele
		liSele=SELE()

		SELE (This.AIndex)
		LOCATE WHILE XX000==liPage FOR XX001==lcObjName
		*LOCATE FOR XX000==liPage AND XX001==lcObjName
		IF !FOUND()
		   INSERT INTO (This.AIndex) (XX000, XX001, XX006) VALUES (liPage,lcObjName,lcValue)
		ELSE
		  REPL XX006 WITH lcValue
		ENDIF
		SELE (liSele)
	ENDPROC


	*-- Add line object to active sheet
	PROCEDURE addline
		LPARAMETERS lcName, tnLeft, tnTop, tnWidth, tnHeight, ;
		 tnpensize, tnpenpat, ;
		 tnpenred, tnpengreen, tnpenblue, ;
		 lcSlant

		IF EMPTY(lcSlant)
			lcSlant = "\"
		endif

		IF VAL(SUBS(lcName,3))>This.IndexNewObject
		   This.IndexNewObject=VAL(SUBS(lcName,3))
		ENDIF

		This.ActiveSheet.AddObject(lcName, "xfrxline")
		loRef = EVALUATE("This.ActiveSheet."+lcName)
		loRef.Name = LOWER(lcName)
		loRef.nOriLeft = tnLeft
		loRef.nOriTop = tnTop
		loRef.noriWidth = tnWidth
		loRef.noriHeight = tnHeight

		loRef._Left = INT(tnLeft/(10000/96))
		loRef._Top = INT(tnTop/(10000/96))
		loRef._Width = INT(tnWidth/(10000/96))
		loRef._Height = INT(tnHeight/(10000/96))

		loRef.BorderStyle=IIF(tnpenpat=8,1,IIF(tnpenpat>=3,tnpenpat+1,IIF(tnpenpat=1,3,tnpenpat)))

		STORE tnpensize TO loRef.BorderWidth,loRef._BorderWidth
		loRef.noriBorderWidth=loRef._BorderWidth*10000/96

		loRef.BorderColor=IIF(tnpenred = -1,0,RGB(tnpenred, tnpengreen, tnpenblue))
		loRef.LineSlant=lcSlant
	ENDPROC


	*-- Set language
	PROCEDURE setlanguage
		LPARAM lcLang

		IF This.LangID==lcLang
		   RETURN
		ENDIF

		LOCAL llDal, lcAlias
		lnAlias = SELECT(0)
		USE IN (This.ALang)
		* If exist VCX library and table
		IF lcLang#"ENG" AND FILE(This.XPath+"xfrxlib_"+lcLang+".vcx") AND FILE(This.XPath+"xfrxlib_"+lcLang+".dbf")
		   This.lErr=.F.
		   SET CLASSLIB TO (This.XPath+"xfrxlib_"+lcLang+".vcx") ADDITIVE
		   llDal=!This.lErr

		   IF llDal
		      USE (This.XPath+"xfrxlib_"+lcLang+".dbf") ALIAS (This.ALang) IN 0 NOUPDATE AGAIN
		      llDal=!This.lErr
		      IF llDal
		         SET ORDER TO "I01" IN (This.ALang)
		      ENDIF
		   ENDIF

		   IF !llDal
		      IF USED(This.ALang)
		         USE IN (This.ALang)
		      ENDIF
		   
		      IF ATC("xfrxlib_"+lcLang+".vcx",SET("CLASSLIB"))>0
		         RELEASE CLASSLIB (This.XPath+"xfrxlib_"+lcLang+".vcx")
		      ENDIF
		   ENDIF
		ENDIF

		IF lcLang=="ENG" OR !llDal
		   lcLang="ENG"
		ENDIF

		This.LangID=lcLang
		SELE 0
		select (lnAlias)
	ENDPROC




	*-- LikeEx
	PROCEDURE likeex
		LPARAMETERS lcWhat,lcString,liData,lcData, llMatchCase
		* lcWhat      - Co se hled�
		* lcString    - V cem se hled�
		*@liData      - Poc�tecn� pozice
		*@lcData      - Nalezen� retezec
		* llMatchCase - Pr�znak

		LOCAL llType,lcText,liStart,lii,liy,liz,lcPom,lcBuff,llMatch,liStartX

		liData=0

		lcText=lcWhat
		llType=ATC("*",lcText,1)>0

		DO WHILE LEFT(lcText,1)=="*"
		   lcText=STUFF(lcText,1,1,"")
		ENDDO


		IF !llType
		   liStart=1
		ELSE
		   * Nyn� z�skej text, kter� bude� hledat

		   lcPom=lcText
		   * Odstra� po��te�n� otazn�ky
		   DO WHILE LEFT(lcPom,1)=="?"
		      lcPom=STUFF(lcPom,1,1,"")
		   ENDDO

		   * odstra� zbyl� ?* za prvn�m znakem
		   lii=AT("?",lcPom)
		   lii=IIF(lii=0,AT("*",lcPom),lii)
		   IF lii>0
		      lcPom=LEFT(lcPom,lii-1)
		   ENDIF
		   liStartX=ATC(lcPom,lcText)-1
		ENDIF


		DO WHILE RIGHT(lcText,1)=="*"
		   lcText=STUFF(lcText,LEN(lcText),1,"")
		ENDDO


		liz=0
		DO WHILE liz>-1
		   liz=liz+1

		   STORE "" TO lcData,lcBuff

		   IF llType
		      liStart=IIF(llMatchCase,AT(lcPom,lcString,liz),ATC(lcPom,lcString,liz))
		      IF liStart=0
		         EXIT
		      ENDIF
		      liStart=liStart-liStartX
		      IF liStart<=0
		         LOOP
		      ENDIF
		   ENDIF


		   lii=1
		   FOR liy=liStart TO LEN(lcString)

		       IF SUBS(lcText,lii,1)="?"
		          lcBuff=lcBuff+SUBS(lcString,liy,1)
		          lii=lii+1
		          IF lii>LEN(lcText)
		             llMatch=.T.
		             EXIT
		          ENDIF
		          LOOP
		       ENDIF

		       IF SUBS(lcText,lii,1)="*" 
		          IF IIF(llMatchCase,SUBS(lcText,lii+1,1)==SUBS(lcString,liy,1),LOWER(SUBS(lcText,lii+1,1))==LOWER(SUBS(lcString,liy,1)))
		             lii=lii+1
		             IF lii>LEN(lcText)
		                llMatch=.T.
		                EXIT
		             ENDIF
		          ELSE
		             lcBuff=lcBuff+SUBS(lcString,liy,1)
		             lii=lii+0
		             llMatch=liy=LEN(lcString) AND lii=LEN(lcText)
		             LOOP
		          ENDIF
		       ENDIF

		       IF !IIF(llMatchCase,SUBS(lcText,lii,1)==SUBS(lcString,liy,1),LOWER(SUBS(lcText,lii,1))==LOWER(SUBS(lcString,liy,1)))
		          EXIT
		       ENDIF
		       lcBuff=lcBuff+SUBS(lcString,liy,1)

		       lii=lii+1
		       IF lii>LEN(lcText)
		          llMatch=.T.
		          EXIT
		       ENDIF
		   NEXT


		   IF llMatch
		      liData=liStart
		      lcData=lcBuff
		      liz=-1
		      EXIT
		   ELSE
		      IF !llType
		         EXIT
		      ENDIF
		   ENDIF
		ENDDO
		RETURN llMatch
	ENDPROC


	PROCEDURE reversestring
		LPARAM lcString
		LOCAL lcResult,lii
		lcResult=""
		FOR lii=LEN(m.lcString) TO 1 STEP -1
		    lcResult=m.lcResult+SUBST(m.lcString,m.lii,1)
		NEXT
		RETURN m.lcResult
	ENDPROC


	*-- Convert ANEm to object name
	PROCEDURE anametoobjectname
		LPARAMETERS liXX000,lcXX002
		SELE (This.ANameObjs)
		LOCATE FOR XX000==liXX000 AND XX002==lcXX002
		IF !FOUND()
		   LOCATE FOR XX000#liXX000 AND XX002==lcXX002
		ENDIF
		liXX000=XX000
		lcXX002=XX001
		RETURN FOUND()
	ENDPROC


	*-- Move object to visible range
	PROCEDURE activateobject
		LPARAM liPage,luObj, llSearch
		* liPage - Page number
		* luObj  - Object/ObjectName
		LOCAL loPages, liPage,llLS,liLeft,liTop,loObj

		llLS=Thisform.LockScreen
		Thisform.LockScreen=.T.
		this.lstoprefreshing = .t.
		loPages=This.Pages

		IF llSearch OR This.nPageno#liPage
		   This.nPageno=liPage
		   IF llSearch
			   This.GoToPage(this.cSearchString)
		   ELSE
		       This.GoToPage()
		   endif
		ENDIF


		IF !ISNULL(luObj)
		   LOCAL nObjLeft, nObjTop, nFactor
		   loPage=This.ActiveSheet

			IF this.DisplayMode = 0
				loObj=IIF(TYPE("luObj")="C",EVAL("loPage."+luObj),luObj)
				nObjLeft = loObj.left
				nObjTop  = loObj.Top
			ELSE
				nFactor = 10000/(96*this.nZoom/100)
				SELECT (this.oXFRXWriter.cXFFAlias)
				LOCATE ALL FOR name = luObj
				IF FOUND()
					nObjLeft = left / nFactor
					nObjTop =  top / nFactor
				ENDIF
			endif

		   IF loPages.Width>loPage.Width
		      liLeft=loPage.Left
		   ELSE
		      liLeft=loPage.Left+(loPages.Width/2-(loPage.Left+nObjLeft))
		      IF liLeft>0 AND liLeft+loPage.Width>loPages.Width &&  AND loPage.Width>loPages.Width
		         liLeft=0
		      ELSE
		         IF liLeft<0 AND liLeft+loPage.Width<loPages.Width &&  AND loPage.Width>loPages.Width
		            liLeft=loPages.Width-loPage.Width
		         ENDIF
		      ENDIF
		   ENDIF

		   IF loPages.Height>loPage.Height
		      liTop=loPage.Top
		   ELSE
		      liTop=loPage.Top+(loPages.Height/2-(loPage.Top+nObjTop))
		      IF liTop>0 AND liTop+loPage.Height>loPages.Height &&  AND loPage.Height>loPages.Height
		         liTop=0
		      ELSE
		         IF liTop<0 AND liTop+loPage.Height<loPages.Height &&  AND loPage.Height>loPages.Height
		            liTop=loPages.Height-loPage.Height
		         ENDIF
		      ENDIF
		   ENDIF

		   loPage.Move(liLeft,liTop)
		   This.UpdateBars()
		   this.lstoprefreshing = .f.
		   loPage.repaint()
		ENDIF
		this.lstoprefreshing = .f.
		Thisform.LockScreen=llLS
	ENDPROC


	*-- Hook event
	PROCEDURE activeobjectevent
		LPARAMETERS loObj, lcEvent
	ENDPROC


	PROCEDURE export
		LPARAMETERS tcOutput

		IF NOT ISNULL(this.oExtensionhandler)
			IF PEMSTATUS(this.oExtensionhandler, "Export", 5)
				IF NOT this.oExtensionhandler.Export(This.oxfrxwriter)
					RETURN
				ENDIF
			ENDIF
		ENDIF

		LOCAL opt, optform, loXFF
		opt = CREATEOBJECT("cusOptions")
		opt.cPagescope = ""
		opt.cTarget = IIF(EMPTY(tcOutput),"",tcOutput)
		opt.coutputfile = this.oDisplayDefaults.defaultoutputfilename && "output"
		optform = CREATEOBJECT("XFRXfrmExportOptions_"+This.LangID,@opt, this)
		IF TYPE("optForm")!="O" OR ISNULL(optform)
			RETURN .f.
		endif
		optform.Show()

		IF EMPTY(opt.cOutputFile)
			RETURN .f.
		ENDIF

		IF NOT ISNULL(this.oExtensionhandler)
			IF PEMSTATUS(this.oExtensionhandler, "ExportOptions", 5)
				IF NOT this.oExtensionhandler.ExportOptions(This.oxfrxwriter, opt)
					RETURN
				ENDIF
			ENDIF
		ENDIF

		loXFF = this.oXFRXWriter

		DO case
			CASE LEFT(opt.cTarget,5) = "image"
				IF EMPTY(opt.cPageScope)
					loXFF.savePicture(opt.coutputfile, SUBSTR(opt.cTarget,7), 1, loXFF.pagecount, 24,,"DPI", opt.imageDPI, opt.zoom, opt.alloddeven, opt.copies)
				ELSE
					loXFF.savePicture(opt.coutputfile, SUBSTR(opt.cTarget,7), opt.cPageScope,   , 24,,"DPI", opt.imageDPI, opt.zoom, opt.alloddeven, opt.copies)
				ENDIF

			CASE opt.cTarget = "XFF"
				SELECT (loXFF.cXFFAlias)
				COPY TO (opt.coutputfile)

			OTHERWISE
				LOCAL loSession
				loSession=EVALUATE([xfrx("XFRX#INIT")])

				IF loSession.SetParams(opt.coutputfile,,,,,,opt.cTarget) = 0
					IF NOT EMPTY(opt.cPageScope)
						loSession.setPageRange(opt.cPageScope)
					ENDIF
					IF TYPE("this.oProgress")="O" AND NOT ISNULL(this.oProgress)
						loSession.setProgressObj(this.oProgress)
					endif
					loSession.TransformReport(loXFF)
				ENDIF
		ENDCASE
	ENDPROC


	PROCEDURE setextensionhandler
		LPARAMETERS toHandler
		IF TYPE("toHandler")="O"
			this.oExtensionHandler = toHandler
			RETURN .t.
		ELSE
			this.oExtensionHandler = null
			RETURN .f.
		ENDIF
	ENDPROC


	PROCEDURE repaint
		LPARAMETERS loSheet&&, hWindow, nMsgID, wParam, lParam

		IF VARTYPE(loSheet)<>"O" OR ISNULL(loSheet)
		*	? "assigning active"
			loSheet = this.ActiveSheet
		ENDIF

		IF !this.lpostinit
			RETURN
		ENDIF
		IF this.lstoprefreshing
			return
		endif
		IF this.DisplayMode = 0
			RETURN
		ENDIF

		*ACTIVATE SCREEN
		*? SECONDS() - loSheet.lastRefresh
		IF SECONDS() - loSheet.lastRefresh < 0.1
			loSheet.refreshtimer.interval = 80
			RETURN
		ENDIF
		loSheet.lastRefresh = SECONDS()
		this.pages.Visible = .f.
		*	_xfDrawMetafile(this.ActiveSheet.emf, this.gethwnd(), MAX(-this.activeSheet.left,0),MAX(-this.activeSheet.top,0),this.activeSheet.width, this.activeSheet.height,;
		*		MAX(this.activeSheet.left,0)+10, MAX(this.activeSheet.top,0)+10, MIN(this.activeSheet.width,this.pages.width)-20, MIN(this.activeSheet.height,this.pages.height)-20 )


		IF TYPE("loSheet.emf")="N" AND loSheet.emf<>0
		*	_xfDrawMetafile(this.ActiveSheet.emf, abc.hwnd, MAX(-this.activeSheet.left,0),MAX(-this.activeSheet.top,0),this.activeSheet.width, this.activeSheet.height,;
		*		MAX(this.activeSheet.left,0)+10, MAX(this.activeSheet.top,0)+10, MIN(this.activeSheet.width,this.pages.width)-20, MIN(this.activeSheet.height,this.pages.height)-20 )

		*ACTIVATE scre
		*? MAX(-loSheet.left,0), MAX(-loSheet.top,0), loSheet.width, loSheet.height
		*? 0, 0, loSheet.CanvasWidth, loSheet.CanvasHeight

			_xfDrawMetafile(loSheet.emf, loSheet.canvashwnd, ;
				MAX(-loSheet.left,0), MAX(-loSheet.top,0), loSheet.width, loSheet.height, ;
				0, 0, loSheet.CanvasWidth, loSheet.CanvasHeight )

		*	_xfDrawMetafile(this.ActiveSheet.emf, this.gethwnd(), ;
		*		MAX(-this.activeSheet.left,0),MAX(-this.activeSheet.top,0),this.activeSheet.width, this.activeSheet.height,;
		*		this.sheetleft+MAX(this.activeSheet.left,0), this.sheettop+MAX(this.activeSheet.top,0), MIN(this.activeSheet.width,this.pages.width), MIN(this.activeSheet.height,this.pages.height) )
		endif
	ENDPROC


	PROCEDURE gethwnd
		local lnhWnd

		IF _xfVFPVersion()<7
			*
			* this requires foxtools
			*
			lnhWnd = _WhToHwnd(_wfindtitl(thisform.caption))
		ELSE
			lnhWnd = thisform.HWnd
		endif

		if Thisform.ShowWindow = 2
			#define GW_CHILD 5
			declare integer GetWindow in Win32API integer, integer
		    lnhWnd = GetWindow(lnhWnd, GW_CHILD)
		endif

		return lnhWnd
	ENDPROC


	PROCEDURE calculatelefttop
		LOCAL loObj
		loObj = this.pages
		this.sheetleft = loObj.left
		this.sheettop = loObj.top
		DO WHILE .t.
			loObj = loObj.parent
			IF VARTYPE(loObj)<>"O"
				EXIT
			ENDIF
			IF ISNULL(loObj)
				EXIT
			ENDIF
			IF UPPER(loObj.baseclass) = "FORM"
				EXIT
			ENDIF
			IF PEMSTATUS(loObj, "left",5)
				this.sheetleft = this.sheetleft + loObj.left
				this.sheettop = this.sheettop + loObj.top
			endif
		ENDDO
		*IF PEMSTATUS(This,"Book",5) AND this.book.visible
		*	this.sheetleft = this.sheetleft + this.book.width
		*endif
	ENDPROC


	PROCEDURE apideclaration
		DECLARE INTEGER GetWindowLong IN user32 as GetWindowLong; 
		    INTEGER hWnd, INTEGER nIndex 
		    
		DECLARE INTEGER CreateWindowEx IN user32 AS CreateWindow; 
		    INTEGER dwExStyle, STRING lpClassName,; 
		    STRING lpWindowName, INTEGER dwStyle,; 
		    INTEGER x, INTEGER y, INTEGER nWidth, INTEGER nHeight,; 
		    INTEGER hWndParent, INTEGER hMenu, INTEGER hInstance,; 
		    INTEGER lpParam 

		DECLARE integer _xfDrawMetafile IN xfrxlib.fll integer, integer, integer, integer, integer, integer, integer, integer, integer, integer
		DECLARE integer _xfDeleteMetafile IN xfrxlib.fll integer
		DECLARE integer _xfgdipStartup IN xfrxlib.fll
		DECLARE integer _xfgdipShutdown IN xfrxlib.fll integer
		DECLARE integer _xfRegisterCanvas IN xfrxlib.fll integer
		DECLARE integer _xfUnregisterCanvas IN xfrxlib.fll integer, integer

		DECLARE INTEGER ShowWindow 		IN user32 	as ShowWindow		INTEGER hWnd, integer nCmdShow
		DECLARE INTEGER DestroyWindow	IN user32 	as DestroyWindow	INTEGER hWnd 
		DECLARE integer MoveWindow		IN user32 	AS MoveWindow		integer hWnd, integer X, integer Y, integer nWidth, integer nHeight, integer bRepaint

		DECLARE integer Sleep 			IN kernel32 as Sleep 			integer
	ENDPROC


	PROCEDURE m_click
		LPARAMETERS lnY, lnX, toSheet
		LOCAL nFactor, lnZoom, lnPage
		lnZoom=This.GetZoomFactor(this.nZoom,This.ActiveSheet)
		nFactor = 10000/(96*lnZoom/100)
		lnPage = VAL(SUBSTR(toSheet.name,2))

		SELECT linkhref FROM (this.oXFRXWriter.cXFFAlias) WHERE (rcType="T" OR rcType="L") AND page = lnPage AND left/nFactor<=lnX AND (left+width)/nFactor>=lnX AND ;
			top/nFactor<=lnY AND (top+height)/nFactor>=lnY AND !EMPTY(linkhref) ;
			INTO CURSOR _xf_hr

		IF RECCOUNT("_xf_hr")>0

			DO case
				CASE LOWER(LEFT(linkhref,4))="#top"
				CASE LEFT(linkhref,1)="#"
					SELECT page, name FROM (this.oXFRXWriter.cXFFAlias) WHERE linkname==SUBSTR(_xf_hr.linkhref,2) ;
					INTO CURSOR _xf_hr2

					IF RECCOUNT("_xf_hr2")>0
						this.ActivateObject(_xf_hr2.page,IIF(EMPTY(_xf_hr2.name),.NULL.,_xf_hr2.name))
					ENDIF
					USE IN SELECT("_xf_hr2")
				OTHERWISE
					LOCAL lohref
					lohref = CREATEOBJECT("xfrxhyperlink",_xf_hr.linkhref)
					lohref.navigate(this)
			ENDCASE

		ENDIF

		USE IN SELECT("_xf_hr")
	ENDPROC


	PROCEDURE isoverhyperlink
		LPARAMETERS lnX, lnY, toSheet

		LOCAL nFactor, loAlias, lnZoom, lnPage
		lnZoom=This.GetZoomFactor(this.nZoom,This.ActiveSheet)
		nFactor = 10000/(96*lnZoom/100)
		lnPage = VAL(SUBSTR(toSheet.name,2))
		*lnPage = this.nPageNo

		IF !USED(This.oXFRXWriter.cXFFAlias)
			RETURN .f.
		ENDIF

		loAlias = CREATEOBJECT("cusSelect")

		IF NOT USED(this.linkAlias)
			SELECT left, top, width, height, rcType, page FROM (this.oXFRXWriter.cXFFAlias) ;
			where !EMPTY(linkhref) ;
			into cursor (this.linkAlias)
			INDEX on page TAG page addi
		ENDIF

		SELECT (this.linkAlias)
		LOCATE ALL FOR (rcType="T" OR rcType="L") ;
			AND page = lnPage AND left/nFactor<=lnX AND (left+width)/nFactor>=lnX AND ;
			top/nFactor<=lnY AND (top+height)/nFactor>=lnY

		IF FOUND()
			RETURN .t.
		ELSE
			RETURN .f.
		ENDIF
	ENDPROC


	PROCEDURE setupnup
		LPARAMETERS tcMatrix
		LOCAL lnx
		lnx = AT("x",tcMatrix)

		this.nPagesHorizontal = VAL(LEFT(tcMatrix,lnx-1))
		this.nPagesVertical = VAL(SUBSTR(tcMatrix,lnx+1))
		this.zoom(-2)
		this.event_zoom(__xfrxlib_UI_status)
		this.gotoPage()
	ENDPROC


	PROCEDURE mrow
		LPARAMETERS ta, tb
		LOCAL lnRetval, llOK, lnLoops

		lnLoops = 0
		DO WHILE !llOK
			IF EMPTY(tb)
				lnRetval = MROW(ta)
			else
				lnRetval = MROW(ta, tb)
			endif
			IF TYPE("lnRetval") = "N"
				RETURN lnRetval
			ENDIF
			=sleep(100)
			lnLoops = lnLoops + 1 
			IF lnLoops = 25
				RETURN 0
			ENDIF
		ENDDO
	ENDPROC


	PROCEDURE mcol
		LPARAMETERS ta, tb
		LOCAL lnRetval, llOK, lnLoops

		lnLoops = 0
		DO WHILE !llOK
			IF EMPTY(tb)
				lnRetval = MCOL(ta)
			else
				lnRetval = MCOL(ta, tb)
			endif
			IF TYPE("lnRetval") = "N"
				RETURN lnRetval
			ENDIF
			=sleep(100)
			lnLoops = lnLoops + 1 
			IF lnLoops = 25
				RETURN 0
			ENDIF
		ENDDO
	ENDPROC


	PROCEDURE m_mousewheel
		LPARAMETERS nDirection, nShift, nXCoord, nYCoord
		IF PEMSTATUS(This,"Parent",5) AND !ISNULL(This.Parent)
		   IF nDirection=-120
		      This.VersCrl.cmdRight.Click()
		   ENDIF

		   IF nDirection=120 && 'DNAR'
		      This.VersCrl.cmdLeft.Click()
		   ENDIF
		ENDIF
	ENDPROC


	PROCEDURE displaymessage
		LPARAMETERS tcMessage
		ah_errormsg(tcMessage)
	ENDPROC


	PROCEDURE email
		IF NOT ISNULL(this.oExtensionhandler)
			IF PEMSTATUS(this.oExtensionhandler, "Email", 5)
				IF NOT this.oExtensionhandler.Email(This.oxfrxwriter)
					RETURN
				ENDIF
			ENDIF
		ENDIF

		LOCAL opt, optform, loXFF
		opt = CREATEOBJECT("cusEmailOptions")
		opt.cAttachmentName = "output"
		optform = CREATEOBJECT("XFRXfrmEmailOptions_"+This.LangID,@opt, this)
		IF TYPE("optForm")!="O" OR ISNULL(optform)
			RETURN .f.
		endif
		optform.Show()

		IF !opt.lSend
			RETURN .f.
		ENDIF

		IF NOT ISNULL(this.oExtensionhandler)
			IF PEMSTATUS(this.oExtensionhandler, "EmailOptions", 5)
				IF NOT this.oExtensionhandler.EmailOptions(This.oxfrxwriter, opt)
					RETURN
				ENDIF
			ENDIF
		ENDIF

		IF EMPTY(opt.cSMTP_HOST) OR EMPTY(opt.cFrom) OR EMPTY(opt.cTo)
			LOCAL lcMessage
			lcMessage = "Message not sent."
			IF EMPTY(opt.cSMTP_HOST)
				lcMessage = lcMessage+CHR(13)+"- SMTP HOST is empty"
			ENDIF
			IF EMPTY(opt.cFrom)
				lcMessage = lcMessage+CHR(13)+"- 'From' field is empty"
			ENDIF
			IF EMPTY(opt.cTo)
				lcMessage = lcMessage+CHR(13)+"- 'To' field is empty"
			ENDIF

			cp_errormsg(lcMessage, 16, "Send email")
			RETURN
		ENDIF

		LOCAL loSession, lcFileName
		loSession=EVALUATE([xfrx("XFRX#INIT")])
		lcFileName = FORCEEXT(Addbs(Sys(2023))+Alltrim(opt.cAttachmentName), "PDF")
		If loSession.SetParams(lcFileName,,.T.,,,,"PDF") = 0
			If Type("this.oProgress")="O" And Not Isnull(This.oProgress)
				loSession.setProgressObj(This.oProgress)
			Endif
			loSession.TransformReport(This.oXFRXWriter)

			LOCAL sm
			Set Procedure To ("vfpwinsock") ADDITIVE 
			sm = Createobject("vfp_winsock_send_mail")
			sm.smtp_host = opt.cSMTP_HOST
			sm.From = opt.cFrom
			sm.To = opt.cTo
			sm.cc = opt.cCC
			sm.cci = opt.cBCC
			sm.message = opt.cBody
			sm.subject = opt.cSubject
			sm.silence = .T.
			sm.attaCHMENT = lcFileName
			if sm.Send()
				ERASE (lcFileName)
			else
				cp_errormsg(sm.erreur,16,"The email could not have been sent.")
			endif
		Endif
	ENDPROC


	PROCEDURE MouseMove
		LPARAMETERS nButton, nShift, nXCoord, nYCoord
	ENDPROC


	PROCEDURE RightClick
		This.ShowSCPopup()
	ENDPROC


	PROCEDURE UIEnable
		LPARAMETERS lEnable
		if UPPER(this.Parent.Parent.Parent.class) = "CNTXFRXMULTIPAGE"
			IF lEnable
				this.Parent.Parent.Parent.pageActivated(this.Parent)
			ELSE
				this.Parent.Parent.Parent.pageDeactivated(this.Parent)
			ENDIF
		ENDIF

		this.lstoprefreshing = !lEnable
		FOR lnI = 1 TO this.pages.ControlCount
			this.pages.Controls(lnI).setVisibility(lEnable)
		endfor

		this.Visible = lEnable
		IF lEnable
			IF this.bookswitch AND VARTYPE(this.book)="O" AND NOT ISNULL(this.book) AND PEMSTATUS(this.book, "ie", 5)
				this.book.ie.Visible = .f.
				this.book.ie.Visible = .t.
				this.book.visible = .t.
			endif
		endif
		*ACTIVATE SCREEN
		*? "ui enable:"
		*?? lEnable
	ENDPROC


	PROCEDURE Destroy
		This.ClearLink()
		thisform._xfcont_instances = thisform._xfcont_instances - 1
		IF 	thisform._xfcont_instances = 0
			IF NOT EMPTY(this.nGDIPToken)
				=_xfgdipShutdown(this.nGDIPToken)
				this.nGDIPToken = 0
			ENDIF
			LOCAL lcVariableName
			lcVariableName = "__xf_g_wl"+ALLTRIM(STR(ABS(this.gethwnd())))
			IF TYPE(lcVariableName)#"U"
				=_xfUnregisterCanvas(this.gethwnd(), EVALUATE(lcVariableName))
				RELEASE (lcVariableName)
			ENDIF
			lcVariableName = "__xf_g_ch"+ALLTRIM(STR(ABS(this.gethwnd())))
			STORE null TO (lcVariableName)
			RELEASE (lcVariableName)
		ENDIF

		=IIF(!ISNULL(This.oFSetting),This.oFSetting.Reset(),.T.)

		LOCAL lcOnKey
		lcOnKey=This.OnKeyF
		IF EMPTY(lcOnKey)
		   ON KEY LABEL CTRL+F
		ELSE
		   ON KEY LABEL CTRL+F &lcOnKey.
		ENDIF

		lcOnKey=This.OnKeyGo
		IF EMPTY(lcOnKey)
		   ON KEY LABEL CTRL+G
		ELSE
		   ON KEY LABEL CTRL+G &lcOnKey.
		ENDIF

		IF USED(This.DelObjs)
		   USE IN (This.DelObjs)
		ENDIF

		IF USED(This.ANameObjs)
		   USE IN (This.ANameObjs)
		ENDIF

		IF USED(This.ABookMarks)
		   USE IN (This.ABookMarks)
		ENDIF

		IF USED(This.AIndex)
		   USE IN (This.AIndex)
		ENDIF

		IF USED(This.ALang)
		   USE IN (This.ALang)
		ENDIF

		use in select(This.Linkalias)
		use in select('__xf_sheets')
	ENDPROC



	PROCEDURE Init
		this.lstoprefreshing = .t.

		SET LIBRARY TO .\xfrxlib\xfrxlib.fll ADDITIVE 

		This.VFPVers=STRTRAN(SUBS(VERSION(),LEN("Visual FoxPro ")+1,2),"0","")
		SET PROCEDURE TO ("xfrx.app") ADDITIVE 

		IF this.VFPVers = "6"
			IF ATC("foxtools.fll", SET("Library")) = 0
				IF FILE("foxtools.fll")
				    SET LIBRARY TO foxtools.fll addi
				ELSE
					IF FILE(HOME()+"foxtools.fll")
						SET LIBRARY TO (HOME()+"foxtools.fll") addi
					ENDIF
				endif
			ENDIF
			IF ATC("foxtools.fll", SET("Library")) = 0
				RETURN .f.
			endif
		endif

		this.nPagesHorizontal = 1&&2&&5
		this.nPagesVertical = 1

		this.APIDeclaration()

		This.DelObjs="XFRX_"+SYS(2015)
		This.ANameObjs="XFRX_"+SYS(2015)
		this.linkAlias="XFRX_"+SYS(2015)
		This.ABookMarks="XFRX_"+SYS(2015)
		this.ASearchResults="XFRX_"+SYS(2015)
		This.AIndex="XFRX_"+SYS(2015)
		This.ALang="XFRX_"+SYS(2015)
		this.oToolbarcontainer = null
		this.oExtensionHandler = null

		LOCAL lcPath
		lcPath=SYS(16)
		lcPath=SUBST(lcPath,AT(" ",lcPath,2)+1)
		STORE LEFT(lcPath,RAT("\",lcPath)) TO This.XPath

		LOCAL lcVariableName, lnHandle
		lnHandle = this.gethwnd()
		lcVariableName = "__xf_g_wl"+ALLTRIM(TRANSFORM(ABS(lnHandle)))
		IF TYPE(lcVariableName) = "U"
			PUBLIC &lcVariableName
			STORE GetWindowLong(this.gethwnd(), -4) TO (lcVariableName) 
			=_xfRegisterCanvas(this.gethwnd())
			this.nGDIPToken = _xfgdipStartup()
			thisform.AddProperty("_xfcont_instances", 1)
			lcVariableName = "__xf_g_ch"+ALLTRIM(STR(ABS(this.gethwnd())))
			PUBLIC &lcVariableName
			STORE thisform TO (lcVariableName) 

		ELSE
			thisform._xfcont_instances = thisform._xfcont_instances + 1
		ENDIF
	ENDPROC


	PROCEDURE GotFocus
		IF This.FirstInit
		   This.OnKeyF=ON("key","CTRL+F")
		   ON KEY LABEL CTRL+F IIF(TYPE("_Screen.ActiveForm")="O" AND !ISNULL(_Screen.ActiveForm),_Screen.ActiveForm.KeyPress(33,4),.T.)

		   This.OnKeyGo=ON("key","CTRL+G")
		   ON KEY LABEL CTRL+G IIF(TYPE("_Screen.ActiveForm")="O" AND !ISNULL(_Screen.ActiveForm),_Screen.ActiveForm.KeyPress(7,4),.T.)

		   This.FirstInit=.F.
		ENDIF
	ENDPROC


	PROCEDURE ZOrder
		LPARAMETERS nOrder
		LPARAMETERS nError, cMethod, nLine
		This.lErr=.T.
		IF UPPER(cMethod)=="SETLANGUAGE"
		   
		ELSE
		   debugout "XFRX: "+LTRIM(STR(nError,11))+" "+cMethod+" "+LTRIM(STR(nLine,11))
		   DODEFAULT(nError, cMethod, nLine)
		ENDIF
	ENDPROC


	*-- Remove Book Name from internal list
	PROCEDURE removebookname
	ENDPROC


	PROCEDURE repaint1
	ENDPROC


	PROCEDURE pages.RightClick
		This.Parent.ShowSCPopup()
	ENDPROC


	PROCEDURE pages.MouseUp
		LPARAMETERS nButton, nShift, nXCoord, nYCoord,loObj

		LOCAL llLS,loCnt,lii,loAS,lcPom,liTop,liPage,lcName,loSP
		llLS=Thisform.LockScreen
		Thisform.LockScreen=.T.
		WITH This.Parent
		loAS=.ActiveSheet
		loSP=.GetSheetProp(.nPageNo) && Vra� vlastnosti sheetu 

		IF nButton=1 AND .DMSwitch AND loAS._Mode=_xfrx_Mode_None
		   IF TYPE("loObj")="O" AND !loObj.Name==loAs.Name
		      * Vytvo� kontainer okolo objektu
		  
		      IF .oDM.SelectObject(@loAS,@loObj,loAS._nShift=1)=1 
		         =IIF(.PropSwitch,.DM.SelectObject(IIF(loSP.iSelect<=1,.DM.cboHierarchy.List(1,1),"")),.T.)
		         .oDMToolL.EDButtons(loSP.iSelect)
		         Thisform.LockScreen=llLS
		         RETURN
		      ENDIF

		      =IIF(.PropSwitch,.DM.SelectObject(IIF(loSP.iSelect>1,"",loObj.Name)),.T.)
		   ELSE
		      .oDM.DeselectAllObjectes(@loAS)
		      =IIF(.PropSwitch,.DM.SelectObject(.DM.cboHierarchy.List(1,1)),.T.)
		   ENDIF

		   .txtHide.SetFocus()
		   .oDMToolL.EDButtons(loSP.iSelect)
		ENDIF

		IF nButton=1 AND .DMSwitch AND loAS._Mode=_xfrx_Mode_SelectObjectes
		   .oDM.SelectObjectByRange(loAs,loAS.cntSelectOs,loAS._nShift=1)
		   =IIF(.PropSwitch,.DM.SelectObject(IIF(loSP.iSelect>1 ,"",loSP.aSelect(1))),.T.)
		   .txtHide.SetFocus()
		   .oDMToolL.EDButtons(loSP.iSelect)
		ENDIF

		IF nButton=1 AND !.DMSwitch AND TYPE("loObj")="O" AND loObj.Type=_xfrxhyperlink
		   lcPom=loObj.URL
		   DO CASE
		      CASE LOWER(LEFT(lcPom,4))="#top"
		           * Posun str�nku na za��tek
		           liTop=IIF(This.Height>loAS.Height,loAS.Top,0)
		           IF liTop=0
		              loAS.Move(loAS.Left,liTop)
		              .UpdateBars()
		           ENDIF

		      CASE LEFT(lcPom,1)="#"
		           * za # je text. kam m�m sko�it
		           * Nejd��ve prohled�m targety pro aktu�ln� str�nku
		           * Kdy� nenjdu, pak hled�m dal�� mimo aktu�ln� str�nku

		           liXX000=VAL(SUBS(loAS.Name,2))
		           lcXX002=SUBS(lcPom,2)
		           IF .ANameToObjectName(@liXX000,@lcXX002)
		              * lokalizuj objekt a posu� str�nku tak, aby byl vid�t
		              .ActivateObject(liXX000,lcXX002)
		           ENDIF

		      OTHERWISE
		           * Sko� na URL (http, FTP, etc)
		           loObj.Navigate()

		   ENDCASE
		ENDIF

		IF nButton=1 AND .DMSwitch AND loAS._Mode=_xfrx_Mode_NewObject
		   lcName=.oDM.AddNewObject(loAS,.oDM._NewClass)
		   .oDM.SelectObject(@loAS,EVAL("loAs."+lcName),.F.)
		   .oDMToolL.EDButtons(loSP.iSelect)
		   =EVAL("loAs."+lcName+".SetMousePointer(2)")
		   =EVAL("loAs.cntSel_"+lcName+".SetMousePointer(2)")

		   =IIF(PEMSTATUS(This.Parent,"DM",5),.DM.LastInited(.T.),.T.)
		   =IIF(.PropSwitch,.DM.SelectObject(lcName),.T.)
		ENDIF

		ENDWITH
		Thisform.LockScreen=llLS
	ENDPROC


	PROCEDURE pages.Click
		This.Parent.SetFocus()
	ENDPROC


	PROCEDURE pages.MiddleClick
		local loPageFrame,lnPage

		loPageFrame = This.Parent.Parent.Parent

		if loPageFrame.PageCount > 1
			lnPage = loPageFrame.ActivePage
		    Thisform.cntXFRXMultiPage1.RemovePage(lnPage)
		endif 
	ENDPROC
	
	PROCEDURE pages.Destroy
		DoDefault()
	ENDPROC

	PROCEDURE tmrmdmove.Timer
		This.Parent.ActiveSheet.MouseMove(4,8,this.Parent.MCOL(Thisform.Name,3),this.parent.MROW(Thisform.Name,3))
	ENDPROC


	PROCEDURE cntdmm.Moved
		LOCAL llLockscreen,liLeft,liWidth
		llLockScreen = Thisform.LockScreen
		Thisform.LockScreen = .T.

		WITH This.Parent
		IF This.Left<.Width
		   .DM.Left=This.Left+This.Width
		   .DM.Width=.Width-.DM.Left
		ENDIF

		liLeft=IIF(.DMSwitch AND .PropSwitch,This.Left,.Width)
		.Pages.Move(.Pages.Left,0,liLeft-.Pages.Left-.verscrl.cmdleft.width,.Pages.Height)

		liTop=.Pages.Height
		.verscrl.Left=liLeft - .verscrl.cmdleft.width
		.horscrl.Width= IIF((liLeft-.horscrl.Left)<40,40,liLeft-.horscrl.Left-.verscrl.width)
		.horscrl.Visible=(liLeft-.horscrl.Left)>=40
		.verscrl.Height=.horscrl.Top-.verscrl.Top

		.RepositionFindObj()
		.RepositionActiveSheet()
		.UpdateBars()

		ENDWITH
		Thisform.LockScreen=llLockScreen
	ENDPROC


	PROCEDURE tmrfind.Timer
		This.Parent.oFind.cnt.cboValues.SetFocus()
		This.Enabled=.F.
	ENDPROC


	PROCEDURE postinit.Timer
		This.Interval = 0
		this.parent.lPostinit = .t.
		this.parent.lstoprefreshing = .f.
		this.parent.Resize()
&& 		This.Parent.cntmove.Moved()
	ENDPROC


ENDDEFINE
*
*-- EndDefine: xfcont
**************************************************
**************************************************
*-- Class:        zxfrxie
*-- ParentClass:  olecontrol
*-- BaseClass:    olecontrol
*-- Time Stamp:   12/21/08 06:13:12 PM
*-- OLEObject = C:\Windows\system32\ieframe.dll
*

* XFRX system constantes
#DEFINE __xfrxlib_tlb_Disable -1
#DEFINE __xfrxlib_tlb_Hide     0
#DEFINE __xfrxlib_tlb_Show     1

#DEFINE __xfrxlib_Book_Disable -1
#DEFINE __xfrxlib_Book_Hide     0
#DEFINE __xfrxlib_Book_Show     1
#DEFINE __xfrxlib_Book_Auto     2

#DEFINE __xfrxlib_DM_Disable -1
#DEFINE __xfrxlib_DM_Hide     0
#DEFINE __xfrxlib_DM_Show     1

#DEFINE __xfrxlib_Prop_Disable -1
#DEFINE __xfrxlib_Prop_Hide     0
#DEFINE __xfrxlib_Prop_Show     1


#DEFINE __xfrxlib_UI_tlb    0
#DEFINE __xfrxlib_UI_html   1
#DEFINE __xfrxlib_UI_menu   2
#DEFINE __xfrxlib_UI_status 3



* XFRX language constants
#DEFINE __xfrxlib_NotFound 1  && "Not Found" 

#DEFINE __xfrxlib_NoPrinters 85 && There are no printers installed.

* Tooltipes
#DEFINE __xfrxlib_Bookmark_ttt 2 && "Show/hide bookmark panel"
#DEFINE __xfrxlib_FirstP_ttt  3  &&  "Go to first page"
#DEFINE __xfrxlib_LastP_ttt   4 &&  "Go to last page"
#DEFINE __xfrxlib_PrevP_ttt    5 && "Go to previous page"
#DEFINE __xfrxlib_NextP_ttt   6 &&  "Go to next page"
#DEFINE __xfrxlib_Print_ttt   7 &&  "Print report..."
#DEFINE __xfrxlib_Find_ttt    8 &&  "Find..."
#DEFINE __xfrxlib_Quit_ttt    9 &&  "Quit report preview"
#DEFINE __xfrxlib_Zoom_ttt    10 &&  "Set zoom"
#DEFINE __xfrxlib_PageDisp_ttt 11 && "Active page %Start% of %End%"
#DEFINE __xfrxlib_PageDisp     65 && "%Start% of %End%"
#DEFINE __xfrxlib_DM_ttt     12 &&   "Design Mode"
#DEFINE __xfrxlib_Prop_ttt   13 &&   "Object properties"
#DEFINE __xfrxlib_Page_ttt   14 &&   "Go to page..."
#DEFINE __xfrxlib_Save_ttt   15 &&   "Save changes"
#DEFINE __xfrxlib_Export_ttt   86 &&  "Export report..."

#DEFINE __xfrxlib_Picture  16 && "Picture"
#DEFINE __xfrxlib_Page  17 && "Page "

#DEFINE __xfrxlib_Cursor_ttt  18 &&   "Cursor"
#DEFINE __xfrxlib_Label_ttt   19 &&   "Label"
#DEFINE __xfrxlib_Image_ttt   20 &&   "Image"
#DEFINE __xfrxlib_Shape_ttt   21 &&   "Shape"
#DEFINE __xfrxlib_Hyperlink_ttt 22 && "Hyperlink"
#DEFINE __xfrxlib_Line_ttt    23 &&   "Line"


* Zoom values
#DEFINE __xfrxlib_Zoom_300 24 && "300%"
#DEFINE __xfrxlib_Zoom_200 25 && "200%"
#DEFINE __xfrxlib_Zoom_175 26 && "175%"
#DEFINE __xfrxlib_Zoom_150 27 && "150%"
#DEFINE __xfrxlib_Zoom_125 28 && "125%"
#DEFINE __xfrxlib_Zoom_100 29 && "100%"
#DEFINE __xfrxlib_Zoom_75  30 && " 75%"
#DEFINE __xfrxlib_Zoom_50  31 && " 50%"
#DEFINE __xfrxlib_Zoom_25  32 && " 25%"
#DEFINE __xfrxlib_Zoom_10  33 && " 10%"
#DEFINE __xfrxlib_Zoom_FWd 34 && "Fit Width"
#DEFINE __xfrxlib_Zoom_FWi 35 && "Fit in Window"

* Menu
#DEFINE __xfrxlib_Book_prm 36 && "Bookmark"
#DEFINE __xfrxlib_Book_stt __xfrxlib_Bookmark_ttt
#DEFINE __xfrxlib_Quit_prm 37 && "Quit"
#DEFINE __xfrxlib_Quit_stt __xfrxlib_Quit_ttt
#DEFINE __xfrxlib_Zoom_prm 38 && "Zoom"
#DEFINE __xfrxlib_Zoom_stt __xfrxlib_Zoom_ttt
#DEFINE __xfrxlib_Find_prm 39 && "Find..."
#DEFINE __xfrxlib_Find_stt __xfrxlib_Find_ttt
#DEFINE __xfrxlib_Print_prm 40 && "Print..."
#DEFINE __xfrxlib_Print_stt __xfrxlib_Print_ttt
#DEFINE __xfrxlib_Go_prm 41 && "Go"
#DEFINE __xfrxlib_Go_stt 42 && "Go to page"
#DEFINE __xfrxlib_GoToPage_prm 43 && "Go to page..."
#DEFINE __xfrxlib_GoToPage_stt 44 && "Go to page..."
#DEFINE __xfrxlib_Toolbar_prm 45 && "Toolbar"
#DEFINE __xfrxlib_Toolbar_stt 46 && "Show/hide report preview toolbar"

#DEFINE __xfrxlib_FirstP_prm   47 && "First"
#DEFINE __xfrxlib_FirstP_stt   __xfrxlib_FirstP_ttt
#DEFINE __xfrxlib_LastP_prm    48 && "Last"
#DEFINE __xfrxlib_LastP_stt    __xfrxlib_LastP_ttt
#DEFINE __xfrxlib_PrevP_prm    49 && "Previous"
#DEFINE __xfrxlib_PrevP_stt    __xfrxlib_PrevP_ttt
#DEFINE __xfrxlib_NextP_prm    50 && "Next"
#DEFINE __xfrxlib_NextP_stt    __xfrxlib_NextP_ttt

#DEFINE __xfrxlib_Zoom_300_prm __xfrxlib_Zoom_300
#DEFINE __xfrxlib_Zoom_300_stt 51 && "Zoom to 300%"
#DEFINE __xfrxlib_Zoom_200_prm __xfrxlib_Zoom_200
#DEFINE __xfrxlib_Zoom_200_stt 52 && "Zoom to 200%"
#DEFINE __xfrxlib_Zoom_175_prm __xfrxlib_Zoom_175
#DEFINE __xfrxlib_Zoom_175_stt 52 && "Zoom to 175%"
#DEFINE __xfrxlib_Zoom_150_prm __xfrxlib_Zoom_150
#DEFINE __xfrxlib_Zoom_150_stt 54 && "Zoom to 150%"
#DEFINE __xfrxlib_Zoom_125_prm __xfrxlib_Zoom_125
#DEFINE __xfrxlib_Zoom_125_stt 55 && "Zoom to 125%"
#DEFINE __xfrxlib_Zoom_100_prm __xfrxlib_Zoom_100
#DEFINE __xfrxlib_Zoom_100_stt 56 && "Zoom to 100%"
#DEFINE __xfrxlib_Zoom_75_prm  __xfrxlib_Zoom_75
#DEFINE __xfrxlib_Zoom_75_stt  57 && "Zoom to 75%"
#DEFINE __xfrxlib_Zoom_50_prm  __xfrxlib_Zoom_50
#DEFINE __xfrxlib_Zoom_50_stt  58 && "Zoom to 50%"
#DEFINE __xfrxlib_Zoom_25_prm  __xfrxlib_Zoom_25
#DEFINE __xfrxlib_Zoom_25_stt  59 && "Zoom to 25%"
#DEFINE __xfrxlib_Zoom_10_prm  __xfrxlib_Zoom_10
#DEFINE __xfrxlib_Zoom_10_stt  60 && "Zoom to 10%"
#DEFINE __xfrxlib_Zoom_FWd_prm __xfrxlib_Zoom_FWd
#DEFINE __xfrxlib_Zoom_FWd_stt 61 && "Fit Width"
#DEFINE __xfrxlib_Zoom_FWi_prm __xfrxlib_Zoom_FWi
#DEFINE __xfrxlib_Zoom_FWi_stt 62 && "Fit in Window"

#DEFINE _xfrx_prop_Name   63 && "Name"
#DEFINE _xfrx_prop_Value  64 && "Value"

#DEFINE __xfrxlib_tlbLayout    66 && "Layout"
#DEFINE __xfrxlib_tlbToolbar   67 && "Report toolbar"
#DEFINE __xfrxlib_tlbRControls 68 && "Report Controls"


#DEFINE __xfrxlib_AL_ttt 69 &&  Align Left Sides
#DEFINE __xfrxlib_AR_ttt 70 &&  Align Right Sides
#DEFINE __xfrxlib_AT_ttt 71 &&  Align Top Edges
#DEFINE __xfrxlib_AB_ttt 72 &&  Align Bottom Edges
#DEFINE __xfrxlib_ACV_ttt 73 && Align Vertical Centers
#DEFINE __xfrxlib_ACH_ttt 74 && Align Horizontal Centers
#DEFINE __xfrxlib_SW_ttt  75 && Same Width
#DEFINE __xfrxlib_SH_ttt  76 && Same Height
#DEFINE __xfrxlib_SM_ttt  77 && Same Size

#DEFINE __xfrxlib_CH_ttt  78 && Center Horizontally
#DEFINE __xfrxlib_CV_ttt  79 && Center Vertikally
#DEFINE __xfrxlib_ZT_ttt  80 && Bring to Front
#DEFINE __xfrxlib_ZB_ttt  81 && Send to Back

#DEFINE __xfrxlib_SMW_ttt  82 && Same Min Width
#DEFINE __xfrxlib_SMH_ttt  83 && Same Min Height
#DEFINE __xfrxlib_SMS_ttt  84 && Same Min Size

*#DEFINE __xfrxlib_Page   87 && "page" word
#DEFINE __xfrxlib_Pages  88 && "pages" word
#DEFINE __xfrxlib_Pages2  89 && "pages" word for more than 4
#DEFINE __xfrxlib_AllPagesInRange 90
#DEFINE __xfrxlib_OddPagesOnly    91
#DEFINE __xfrxlib_EvenPagesOnly   92

#DEFINE __xfrxlib_Email_ttt  93  &&   "Email report..."

#DEFINE CRLF CHR(13)+CHR(10)

*********************************
*
*********************************

#DEFINE __xfrxlib_None_val      0
#DEFINE __xfrxlib_Book_val      2
#DEFINE __xfrxlib_Quit_val      1
#DEFINE __xfrxlib_Find_val      3
#DEFINE __xfrxlib_Print_val     4
#DEFINE __xfrxlib_Toolbar_val   7
#DEFINE __xfrxlib_GoToPage_val  8

#DEFINE __xfrxlib_FirstP_val   51
#DEFINE __xfrxlib_PrevP_val    52
#DEFINE __xfrxlib_NextP_val    53
#DEFINE __xfrxlib_LastP_val    54

#DEFINE __xfrxlib_Zoom_300_val 61
#DEFINE __xfrxlib_Zoom_200_val 62
#DEFINE __xfrxlib_Zoom_175_val 63
#DEFINE __xfrxlib_Zoom_150_val 64
#DEFINE __xfrxlib_Zoom_125_val 65
#DEFINE __xfrxlib_Zoom_100_val 66
#DEFINE __xfrxlib_Zoom_75_val  67
#DEFINE __xfrxlib_Zoom_50_val  68
#DEFINE __xfrxlib_Zoom_25_val  69
#DEFINE __xfrxlib_Zoom_10_val  70
#DEFINE __xfrxlib_Zoom_FWd_val 71
#DEFINE __xfrxlib_Zoom_FWi_val 72


#DEFINE _xfrx_Mode_None            0
#DEFINE _xfrx_Mode_MoveObject      1
#DEFINE _xfrx_Mode_MoveSheet       3
#DEFINE _xfrx_Mode_ROL             4
#DEFINE _xfrx_Mode_ROT             5
#DEFINE _xfrx_Mode_ROR             6
#DEFINE _xfrx_Mode_ROB             7
#DEFINE _xfrx_Mode_SelectObjectes  8
#DEFINE _xfrx_Mode_NewObject       9
#DEFINE _xfrx_Mode_MoveSheetTimer 10

****************************
#DEFINE _xfrx_ROP_A    0 && All
#DEFINE _xfrx_ROP_L    1 && Left
#DEFINE _xfrx_ROP_T    2 && Top
#DEFINE _xfrx_ROP_W    4 && Width
#DEFINE _xfrx_ROP_H    8 && Height


* ID classes
#DEFINE _xfrxrectangle   1
#DEFINE _xfrxline        2
#DEFINE _xfrxlabel       8
#DEFINE _xfrximage      16
#DEFINE _xfrxhyperlink  32


#DEFINE _xfrxrectangle_c   "xfrxrectangle"
#DEFINE _xfrxline_c        "xfrxline"
#DEFINE _xfrxlabel_c       "xfrxlabel"
#DEFINE _xfrximage_c       "xfrximage"
#DEFINE _xfrxhyperlink_c   "xfrxhyperlink"

*
DEFINE CLASS zxfrxie AS olecontrol


	Height = 100
	Width = 100
	Name = "zxfrxie"
	OLEClass = "Shell.Explorer.2"
	OLETypeAllowed = -2
    BorderWidth=0
    BackStyle=0
    BorderStyle=0

	*-- Requery Bookmark from internal list
	PROCEDURE requery
		LPARAM loCnt, llSearchResults
		* Object reference to CNT object
		* foundstr C(30), page i, name c(12)
		LOCAL lcPath,lcPom,liPage,liRecno,lii,llPage
		IF TYPE("loCNT.ABookMarks") = "C" AND llSearchResults OR USED(loCNT.ABookMarks)
		   IF g_adhocone
 	          lcPath=SYS(5)+SYS(2003) + "\..\..\EXE\xfrxlib\"
           ELSE
              lcPath=SYS(5)+SYS(2003) + "\xfrxlib\"
           ENDIF
           lcCSS="xfrx.css"
		   lcPom=[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">]+CRLF+;
		         [<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">]+CRLF+;
		         [<head>]+CRLF+;
		         [ <meta content="text/html; charset=windows-]+LTRIM(STR(CPCURRENT(),6))+[" http-equiv="Content-Type" />]+CRLF+;
		         [ <meta name="Generator" content="XFRX" />]+CRLF+;
		         [ <link rel="stylesheet" href="]+lcPath+["xfrx.css type="text/css" />]+CRLF+;
		         [</head>]+CRLF+[<body class="xfrxbody" scroll="yes">]+CRLF+;
		         [<div class="xfrxblock">]+CRLF+;
		         [<div class="xfrxbodyx">]+CRLF+;
		         [<table>]+CRLF+[ <tbody>]+CRLF

		   IF llSearchResults
		      lcPom = lcPom + [<tr><td><div class="xfrx_Pbookmark" /></td><td colspan="2"><a class="xfrx_Pbookmark]+;
		      			[" href="#_SClose]+[">]+"Close"+[</a></td></tr>]+CRLF
		      lcPom = lcPom + [<tr><td><div class="xfrx_Pbookmark" /></td><td colspan="2">&nbsp;</td></tr>]+CRLF
		      
		   	  SELECT _xf_searchResults
		   	  SCAN all
		      lcPom = lcPom + [<tr><td><div class="xfrx_Pbookmark" /></td><td colspan="2"><a class="xfrx_Pbookmark]+;
		      			[" href="#_S]+ALLTRIM(name)+[_]+ALLTRIM(STR(page))+[_]+ALLTRIM(STR(occ))+[" title="]+ALLT(foundstr)+[">]+ALLTRIM(STR(page))+": "+ALLT(foundstr)+[</a></td></tr>]+CRLF
		      endscan
		   else
			   SELE (loCNT.ABookMarks)
			   STORE 0 TO lii,liPage
			   SCAN FOR !DELE()
			        IF !XX000==liPage
			           IF XX001=="" AND (loCNT.GenerateBookmarksForPages OR !loCNT.GenerateBookmarksForPages AND loCNT.GenerateBookmarksForPagesWithoutBookmarks)
			              lcPom=lcPom+[  <tr><td><div class="xfrx_Pbookmark" /></td><td colspan="2"><a class="xfrx_Pbookmark]+;
			                    IIF(XX003=0,"",IIF(XX003=1," italics",IIF(XX003=2," bold"," bold_italics")))+;
			                    [" href="#_]+LTRIM(STR(XX005,11))+[" title="]+ALLT(XX002)+[">]+ALLT(XX002)+[</a></td></tr>]+CRLF
			   
			              llPage=.T.
			           ELSE
			              llPage=.F.
			           ENDIF
			           liPage=XX000

			           liRecno=RECNO()
			           COUNT TO lii WHILE XX000==liPage
			           GO (liRecno)
			           IF !loCNT.GenerateBookmarksForPages AND loCNT.GenerateBookmarksForPagesWithoutBookmarks
			              GO (liRecno)
			              LOCATE WHILE XX000==liPage FOR XX001==""
			              GO (liRecno)
			           ENDIF
			           lii=lii-IIF(loCNT.GenerateBookmarksForPages OR !loCNT.GenerateBookmarksForPages AND loCNT.GenerateBookmarksForPagesWithoutBookmarks AND FOUND(),1,0)
			           IF llPage
			              LOOP
			           ENDIF
			        ENDIF
			        IF lii>0
			           lcPom=lcPom+[  <tr>]+IIF(llPage,[<td><div class="xfrx_empty" /></td>],[])+[<td><div class="xfrx_bookmark" /></td><td]+IIF(llPage,[],[ colspan="2"])+[><a class="xfrx_bookmark]+IIF(XX003=0,"",IIF(XX003=1," italics",IIF(XX003=2," bold"," bold_italics")))+[" href="#_]+LTRIM(STR(XX005,11))+[" title="]+ALLT(XX002)+[">]+ALLT(XX002)+[</a></td></tr>]+CRLF
			        ENDIF

			   ENDSCAN
		   ENDIF   
		   lcPom=lcPom+;
		         [ </tbody>]+CRLF+[</table>]+CRLF+;
		         [</div>]+CRLF+;
		         [</div>]+CRLF+;
		         "</body>"+CRLF+"</html>"+CRLF

			LOCAL lcSafety
			lcSafety = SET("Safety")
			SET SAFETY off
		   =STRTOFILE(lcPom,This.Parent.tmpFile)
		   SET SAFETY &lcSafety

			LOCAL lcScript

			IF _xfVFPVersion()>7
				TEXT TO lcScript NOSHOW 
				LPARAMETERS tt
				TRY
				Tt.Navigate(Tt.Parent.tmpFile)
				CATCH
				ENDTRY
				ENDTEXT
				EXECSCRIPT(lcScript, this)
			ELSE
				This.Navigate(This.Parent.tmpFile)
			ENDIF

			LOCAL liCount,liMax
			liMax=6
			liCount=0
			DO WHILE This.ReadyState<=3 AND liCount<liMax
			   =Sleep(100)
			   liCount=liCount+1
			ENDDO 


		   This.Visible=.F.
		   This.Visible=.T.
		ELSE
			this.Visible = .f.
		ENDIF
	ENDPROC
 
	PROCEDURE BeforeNavigate2
		*** ActiveX Control Event ***
		LPARAMETERS pdisp, url, flags, targetframename, postdata, headers, cancel
		LOCAL lii
		lii=AT("#",url)
      This.Parent.GoHTML(IIF(lii=0,"",SUBS(url,lii+1)))    
    if SUBS(url,lii+1)="_SClose"
      * Da cambiare
      this.Parent.oPreviewObject.pages.page1.cntPreviewer.find("",.f., this.parent.oPreviewObject.oSEARCHPARAMETERS,.t.)
    else

    endif      
	ENDPROC


	PROCEDURE Refresh
		*** ActiveX Control Method ***
		nodefault
	ENDPROC


ENDDEFINE
*
*-- EndDefine: zxfrxie
**************************************************

**************************************************
*-- Class:           zxfrxbook
*-- ParentClass:     container
*-- BaseClass:       container
*-- Time Stamp:      12/16/08 08:52:00 PM
*-- Bookmark container
*
DEFINE CLASS zxfrxbook AS container


	Width = 200
	Height = 200
	BackStyle = 0
	BorderWidth = 1
	Visible = .T.
	*-- Folder and name of temporaly file
	tmpfile = ""
	Name = "zxfrxbook"
	*-- Oggetto preview
	oPreviewObject = .null.


	*-- Post Last initilization
	PROCEDURE lastinited
		LOCAL loCNT,llLS
		loCNT=This.Parent
		llLS=Thisform.LockScreen
		Thisform.LockScreen=.T.

		=IIF(!PEMSTATUS(This,"IE",5),This.AddIE(),.T.)
		=IIF(!PEMSTATUS(This,"IE",5) AND !PEMSTATUS(This,"lstBM",5),This.AddObject("lstBM","zxfrxlstBook"),.T.)

		IF PEMSTATUS(This,"IE",5)
		   This.IE.Requery(@loCNT)
		ENDIF

		IF PEMSTATUS(This,"lstBM",5)
		   This.lstBM.Requery(@loCNT)
		ENDIF

		Thisform.LockScreen=llLS
	ENDPROC


	*-- RemoveBoomark from list
	PROCEDURE removebookmark
		LPARAM liID
		* liID - BookMark ID
		LOCAL lolst,llLS,lii

		llLS=Thisform.LockScreen
		Thisform.LockScreen=.T.

		IF PEMSTATUS(This,"IE",5)
		   This.IE.Requery(This.Parent)
		ENDIF

		IF PEMSTATUS(This,"lstBM",5)
		   lolst=This.lstBM
		   lii=lolst.ListIndex
		   loLst.RemoveListItem(liID)
		   IF lii>lolst.ListCount
		      lolst.ListIndex=lolst.ListCount
		   ENDIF
		ENDIF
		Thisform.LockScreen=llLS
	ENDPROC


	*-- Add Shell.Explorer.2
	PROCEDURE addie
		This.AddObject("IE","zxfrxIE")
		IF PEMSTATUS(This,"IE",5)
		   This.IE.Visible=.T.
		   This.Resize()
			*** DH 09/05/2007: Call Resize or else IE isn't always sized properly
		ENDIF
	ENDPROC


	PROCEDURE gohtml
		LPARAM lcHash
		*ACTIVATE SCREEN
		*? lcHash
		IF !EMPTY(lcHash)
			IF lcHash = "_SClose"
				IF USED(this.oPreviewObject.pages.page1.cntpreviewer.ABookMarks)
					this.lastinited()

				ELSE
					this.oPreviewObject.pages.page1.cntpreviewer.shbook(.F.)
				endif
			else
			   This.GoToBookMark(lcHash)
			ENDIF
		ENDIF
	ENDPROC


	PROCEDURE gotobookmark
		LPARAM tcHash
		LOCAL loCNT, liPage, lcField
		loCNT=This.Parent

		IF LEFT(tcHash,2)=="_S"
			lnAt = AT("_", tcHash, 2)
			lcField = SUBSTR(tcHash, 3, lnAt-3)
			liPage = VAL(SUBSTR(tcHash, lnAt+1))
			*loCNT.ActivateObject(liPage,IIF(EMPTY(lcField),.NULL.,lcField), .t.)
			loCNT.xfrxpreview.pages.page1.CNTPREVIEWER.activateobject(liPage,IIF(EMPTY(lcField),.NULL.,lcField), .t.)
		else
			liID = VAL(SUBST(tcHash,2))

			SELE (loCNT.ABookMarks)
			LOCATE FOR XX005=liID

			* lokalizuj objekt a posu� str�nku tak, aby byl vid�t
			loCNT.ActivateObject(XX000,IIF(EMPTY(XX001),.NULL.,XX001))
		endif
	ENDPROC


	PROCEDURE showsearchresults
	    PARAMETERS bClear
		LOCAL loCNT,llLS
		*loCNT=This.Parent
		loCNT=This.oPreviewObject.pages.page1.cntpreviewer

		IF PEMSTATUS(This,"IE",5)
		   IF NOT bClear
		       This.IE.Requery(@loCNT, .T.)
		   ELSE
		       This.IE.Requery(@loCNT, .F.)
		   ENDIF
		ENDIF
		
        IF PEMSTATUS(This.parent.parent.parent.parent,"NotifyEvent",5)
            IF NOT bClear
                This.parent.parent.parent.parent.NotifyEvent("BookmarkOn")
            ELSE
                This.parent.parent.parent.parent.NotifyEvent("BookmarkOff")
            ENDIF
        ENDIF
	ENDPROC
	
	PROCEDURE clearSearchResults
	    * Cancella il resoconto della ricerca
	    this.showSearchResults(.T.)
	ENDPROC	


	PROCEDURE Destroy
		IF PEMSTATUS(This,"IE",5)
		   This.RemoveObject("IE")
		ENDIF
		DELE FILE (This.tmpFile)
	ENDPROC


	PROCEDURE Error
		LPARAMETERS nError, cMethod, nLine
		LOCAL lcOnE
		IF UPPER(cMethod)=="ADDIE"
		   
		ELSE
		   lcOnE=ON("ERROR")
		   IF !EMPTY(lcOnE)
		      &lcOnE.
		   ENDIF
		ENDIF
	ENDPROC


	PROCEDURE Resize
		IF PEMSTATUS(This,"IE",5)
		   This.IE.Move(0,0,This.Width,This.Height)
		ENDIF

		IF PEMSTATUS(This,"lstBM",5)
		   This.lstBM.Move(0,0,This.Width,This.Height)
		ENDIF
	ENDPROC


	PROCEDURE Init
		This.tmpFile=SYS(2023)+"\"+SYS(2015)+".html"
    this.AddIE()
	ENDPROC

        PROCEDURE Event
          LPARAMETERS cEvent
          DoDefault()
        ENDPROC

        PROCEDURE Calculate
           DODEFAULT()
        ENDPROC
ENDDEFINE
*
*-- EndDefine: zxfrxbook
**************************************************

**************************************************
*-- Class:           xfrxlstbook (p:\ahom100\exe\xfrxlib\xfrxlib.vcx)
*-- ParentClass:     listbox
*-- BaseClass:       listbox
*-- Time Stamp:      09/26/03 05:44:07 PM
*
DEFINE CLASS zxfrxlstbook AS listbox


	FontName = "Tahoma"
	FontSize = 8
	ColumnCount = 1
	ColumnWidths = "200"
	Height = 62
	ColumnLines = .F.
	SpecialEffect = 1
	Width = 111
	Name = "xfrxlstbook"


	PROCEDURE Click
		This.Parent.GoToBookMark(This.ListItemID)
	ENDPROC


	PROCEDURE Requery
		LPARAM loCNT
		This.Clear()
		IF USED(loCNT.ABookMarks)
		   SELE (loCNT.ABookMarks)
		   SCAN FOR !DELE() AND lolst.AddListItem(ALLT(XX002),XX005,1)
		   ENDSCAN
		ENDIF
	ENDPROC


ENDDEFINE
*
*-- EndDefine: xfrxlstbook
**************************************************

**************************************************
*-- Class:        _xfrxfsettings (p:\ahom100_l\exe\xfrxlib\xfrxlib.vcx)
*-- ParentClass:  custom
*-- BaseClass:    custom
*-- Time Stamp:   03/01/11 12:54:09 PM
*-- Settings of Find  Dialog
*
DEFINE CLASS _xfrxfsettings AS custom


	Height = 17
	Width = 103
	*-- Last Scope resolution
	iscope = 2
	*-- Last Page
	lpage = 0
	*-- Last foud object
	lobject = 0
	*-- Last SelStart
	lselstart = 0
	*-- Count od Values
	ivalue = 0
	*-- Last Value
	lvalue = 0
	*-- Last Left position
	ileft = 0
	*-- Last Top position
	itop = 0
	*-- Flag of first run
	ofirstfind = .T.
	*-- Name of last found object
	cobject = ""
	_lpage = 0
	_cobject = ""
	_lselstart = 0
	*-- Find last value...
	_cvalue = ""
	Name = "_xfrxfsettings"

	*-- Last MatchCase
	lmatchcase = .F.

	*-- Last Search Backward
	lsearchbackward = .F.

	*-- Last Use Wild Cards
	lusewildcards = .F.

	*-- Internal list of values
	DIMENSION avalues[1]


	*-- Reset internal flags
	PROCEDURE reset
		STORE 0 TO This.lPage,This.lObject,This.lSelStart
	ENDPROC


ENDDEFINE
*
*-- EndDefine: _xfrxfsettings
**************************************************

DEFINE CLASS zxfrxMakeDocument as custom
	oSession = .null.
	oXffDocument = .null.
	cReport = ""
	cCursor = ""
	
	Proc Init
	    *Abilita per default l'anteprima di stampa di xfrx
	    IF TYPE("g_xfrxpreview")="U"
	        PUBLIC g_xfrxpreview
	        g_xfrxpreview = .t.
	    endif
&& ###start remove from setup###
		If g_adhocone
			if !'xfrxlib'$LOWER(SET('PATH'))
				SET PATH TO ( ADDBS(ALLTRIM(SYS(5)+SYS(2003))) + "..\..\EXE\xfrxlib" ) ADDITIVE
			endif
			if !'xfrxlib'$LOWER(SET('CLASSLIB'))
				SET CLASSLIB TO ( ADDBS(ALLTRIM(SYS(5)+SYS(2003))) + "\..\..\EXE\xfrxlib\xfrxlib" ) ADDITIVE
			endif
			if !'xfrxlib'$LOWER(SET('LIBRARY'))
				SET LIBRARY TO ..\..\EXE\xfrxlib\xfrxlib.fll ADDITIVE
			endif
			if !'_reportlistener'$LOWER(SET('CLASSLIB'))
				SET CLASSLIB TO ( ADDBS(ALLTRIM(SYS(5)+SYS(2003))) + "\..\..\EXE\_reportlistener" ) ADDITIVE
			endif
		else
&& ###end remove from setup###
			if !'xfrxlib'$LOWER(SET('PATH'))
				SET PATH TO ( ADDBS(ALLTRIM(SYS(5)+SYS(2003))) + "xfrxlib" ) ADDITIVE
			endif
			if !'xfrxlib'$LOWER(SET('CLASSLIB'))
				SET CLASSLIB TO ( ADDBS(ALLTRIM(SYS(5)+SYS(2003))) + "xfrxlib\xfrxlib" ) ADDITIVE
			endif
			if !'xfrxlib'$LOWER(SET('LIBRARY'))
				SET LIBRARY TO ADDBS(ALLTRIM(SYS(5)+SYS(2003))) + "xfrxlib\xfrxlib.fll" ADDITIVE
			endif
			if !'_reportlistener'$LOWER(SET('CLASSLIB'))
				SET CLASSLIB TO ( ADDBS(ALLTRIM(SYS(5)+SYS(2003))) + "_reportlistener" ) ADDITIVE
			endif
&& ###start remove from setup###
		endif
&& ###end remove from setup###
		this.ResetXfrx()
	EndProc
	
	Proc Destroy
		DoDefault()
		this.oXffDocument = .null.
		this.oSession = .null.
		this.cReport = ""
		this.cCursor = ""
	EndProc
	
	Proc cReport_Assign
		LPARAMETERS cNewVal
		this.cReport = alltrim(m.cNewVal)
	EndProc
	
	Proc cCursor_Assign
		LPARAMETERS cNewVal
		If Used(m.cNewVal)
			this.cCursor = alltrim(m.cNewVal)
		else
			this.cCursor = ""
		endif
	EndProc
	
	Function GetXfDocument
		return this.oXffDocument
	EndFunc
	
	Proc SetTargetType
		LPARAMETERS cTargetType
		this.oSession.TargetType = m.cTargetType
	EndpRoc
	
	Proc SetTargetFileName
		LPARAMETERS cTargetFileName
		this.oSession.TargetFileName = m.cTargetFileName
	EndProc
	
	Proc ResetXfrx
		this.oXffDocument = .null.
		this.oSession = .null.
		this.cReport = ""
		this.cCursor = ""
		this.oSession = XFRX('XFRX#LISTENER')
		this.oSession.TargetType = "XFF"
		this.oSession.TargetFileName = ""
	EndProc
	
	Proc Preview (bNoReset AS Boolean)
		Local OldArea, OldConsole, OldTalk
		OldArea = SELECT()
		OldConsole = SET("CONSOLE")
		OldTalk = SET("TALK")
		SET CONSOLE OFF
		SET TALK OFF
		If !EMPTY(this.cCursor)
			Select (this.cCursor)
		EndIf
		IF bNoReset
			REPORT FORM ( this.cReport ) OBJECT this.oSESSION NORESET
		ELSE
			REPORT FORM ( this.cReport ) OBJECT this.oSESSION
		ENDIF
		this.oXffDocument = this.oSession.oXfDocument
		SET CONSOLE &OldConsole
		SET TALK &OldTalk
		Select (m.OldArea)
	EndProc
	
	Proc PreviewNoPageEject (bNoReset AS Boolean)
		local OldArea, OldConsole, OldTalk
		OldArea = SELECT()
		OldConsole = SET("CONSOLE")
		OldTalk = SET("TALK")
		SET CONSOLE OFF
		SET TALK OFF
		If !EMPTY(this.cCursor)
			Select (this.cCursor)
		ENDIF
		IF bNoReset
			REPORT FORM ( this.cReport ) OBJECT this.oSESSION NOPAGEEJECT NORESET
		ELSE
			REPORT FORM ( this.cReport ) OBJECT this.oSESSION NOPAGEEJECT
		ENDIF
		SET CONSOLE &OldConsole
		SET TALK &OldTalk
		Select (m.OldArea)
	EndProc
	
ENDDEFINE

DEFINE CLASS zXfrxEmbeddedToolbar AS container
	Width = 10
	Height = 10
	BackStyle = 0
	BorderWidth = 0
	Name = "zXfrxEmbeddedToolbar"
	oPreviewObject = .null.
	FontStrikeThru = .F.
	FontUnderline = .F.
	FontItalic = .F.
	FontBold = .F.
	FontSize = 8
	FontName = "Arial"
	bFirstPageVisible=.T.
	bPreviousPageVisible=.T.
	bNextPageVisible=.T.
	bLastPageVisible=.T.
	bFindStringVisible=.T.
	bFindButtonVisible=.T.
	bPageStringVisible=.T.
	bZoomComboVisible=.T.
	&&bPageButtonVisible=.T.
	cPageString = "Pag. %1 di %2"
	w_PAGETOGO = ""
	w_ZOOMFACTOR = -2
	w_STRINGTOFIND = ""
	
	add object oBtnFirstPage as StdButton with uid="APAVZFFBVY",left=0, top=0, width=48,height=45,;
					Picture="BMP\FIRST.BMP", caption="", nPag=1;
					, ToolTipText = "Va alla prima pagina";
					, HelpContextID = 88223879;
					, caption='\<Inizio';
					, FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
	
	add object oBtnPreviousPage as StdButton with uid="APAVZFFBVY",left=49, top=0, width=48,height=45,;
					Picture="BMP\LEFT.BMP", caption="", nPag=1;
					, ToolTipText = "Va alla pagina precedente";
					, HelpContextID = 88223879;
					, caption='\<Prec.';
					, FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
	
	add object oPageToGo as StdField with uid="RLZTIOPFVQ",rtseq=2,rtrep=.f.,;
					cFormVar = "w_PAGETOGO", cQueryName = "PAGETOGO",;
					bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
					HelpContextID = 151006422,;
					bGlobalFont=.t.,;
					Height=21, Width=111, Left=100, Top=12, cGetPict='"999999999999999"', InputMask=replicate('X',100) , Alignment=2, SelectOnEntry=.T.
	
	add object oBtnNextPage as StdButton with uid="DKZVDNLNDH",left=212, top=0, width=48,height=45,;
		Picture="BMP\RIGHT.BMP", caption="", nPag=1;
		, ToolTipText = "Va alla pagina successiva";
		, HelpContextID = 88009930;
		, caption='\<Succ.';
		, FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBtnLastPage as StdButton with uid="QDKEUFRXJO",left=261, top=0, width=48,height=45,;
		Picture="BMP\Last.BMP", caption="", nPag=1;
		, ToolTipText = "Va all'ultima pagina";
		, HelpContextID = 87822190;
		, caption='\<Fine';
		, FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

	add object oStrVisual as StdString with uid="UZUFGMQYZD",Visible=.t., Left=310, Top=0,;
		Alignment=2, Width=144, Height=18,;
		Caption="Visualizzazione"  ;
		, bGlobalFont=.t.
		
	add object oZOOMFACTOR as StdCombo with uid="GOOQMLKTZS",rtseq=3,rtrep=.f.,left=310,top=18,width=144,height=21;
		, ToolTipText = "Al cambio";
		, HelpContextID = 38709834;
		, cFormVar="w_ZOOMFACTOR",RowSource=""+"500%,"+"300%,"+"200%,"+"150%,"+"100%,"+"75%,"+"50%,"+"25%,"+"10%,"+"Adatta larghezza,"+"Adatta pagina", bObbl = .f. , nPag = 1;
		, bGlobalFont=.t.
	
	add object oStrFind as StdString with uid="UZUFGMQYZD",Visible=.t., Left=455, Top=0,;
		Alignment=2, Width=111, Height=18,;
		Caption="Cerca"  ;
		, bGlobalFont=.t.
	
	add object oSTRINGTOFIND as StdField with uid="VRDFUHHXVK",rtseq=1,rtrep=.f.,;
		cFormVar = "w_STRINGTOFIND", cQueryName = "STRINGTOFIND",;
		bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
		ToolTipText = "Stringa da cercare",;
		HelpContextID = 104326462,;
		bGlobalFont=.t.,;
		Height=21, Width=111, Left=455, Top=18, InputMask=replicate('X',100), SelectOnEntry=.T.
	
	add object oBtnFind as StdButton with uid="QDKEUFRXJO",left=566, top=18, width=21,height=21,;
		Picture="BMP\Find.BMP", caption="", nPag=1;
		, ToolTipText = "Esegue la ricerca";
		, HelpContextID = 87822190;
		, caption='';
		, FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f., ImgSize = 16
	
	
	func oZOOMFACTOR.RadioValue()
		return(iif(this.value =1,500,;
		iif(this.value =2,300,;		
		iif(this.value =3,200,;
		iif(this.value =4,150,;
		iif(this.value =5,100,;		
		iif(this.value =6,75,;		
		iif(this.value =7,50,;
		iif(this.value =8,25,;
		iif(this.value =9,10,;		
		iif(this.value =10,-1,;
		iif(this.value =11,-2,;
		0))))))))))))
	endfunc
	
	func oZOOMFACTOR.GetRadio()
		this.Parent.w_ZOOMFACTOR = this.RadioValue()
		return .t.
	endfunc

	func oZOOMFACTOR.SetRadio()
		this.value = ;
		  iif(this.Parent.w_ZOOMFACTOR==500,1,;
		  iif(this.Parent.w_ZOOMFACTOR==300,2,;
		  iif(this.Parent.w_ZOOMFACTOR==200,3,;
		  iif(this.Parent.w_ZOOMFACTOR==150,4,;
		  iif(this.Parent.w_ZOOMFACTOR==100,5,;
		  iif(this.Parent.w_ZOOMFACTOR==75,6,;
		  iif(this.Parent.w_ZOOMFACTOR==50,7,;
		  iif(this.Parent.w_ZOOMFACTOR==25,8,;
		  iif(this.Parent.w_ZOOMFACTOR==10,9,;		  		  
		  iif(this.Parent.w_ZOOMFACTOR==-1,10,;
		  iif(this.Parent.w_ZOOMFACTOR==-2,11,;
		  0)))))))))))
	endfunc
	
	Proc oZOOMFACTOR.GotFocus()
        Local i_var
        Local i_oldvalue
        If !Inlist(This.Parent.Parent.oContained.cFunction,"Query","Filter")
            This.Parent.Parent.oContained.SaveDependsOn()
            i_oldvalue=This.Value
            This.mDefault()
            oCpToolBar.b9.Enabled=This.bHasZoom
            This.bUpd=This.Value<>i_oldvalue
        Endif
        If This.Parent.Parent.oContained.cFunction<>"Filter"
            This.bUpd=.F.
            This.mBefore()
            * --- il valore puo' essere cambiato dalla funzione di "Before"
            i_var=This.cFormVar
            If This.RadioValue()<>This.Parent.&i_var
                This.SetControlValue()
                This.bUpd=.T.
            Endif
        Endif
        If !This.bNoBackColor
            This.BackColor = i_nBackColor
        Endif
        * --- Numero di elementi nella lista di drop down della combo
        If Vartype(g_DispCnt)='N'
            This.DisplayCount = g_DispCnt
        Endif
        Return
	endproc
	
    Func oZOOMFACTOR.When()
        Local i_bRes
        * --- Procedure standard del campo
        i_bRes=.T.
        If !Inlist(This.Parent.Parent.oContained.cFunction,"Query","Filter")
            i_bRes = This.mCond()
            If !Empty(This.cfgCond)
                i_e=Strtran(This.cfgCond,'w_','this.parent.w_')
                i_bRes=&i_e
            Endif
        Endif
        Return(i_bRes)
	endfunc
	
	Func oZOOMFACTOR.Valid()
        Local i_bRes,i_bErr,i_var,i_bUpd,i_xOldValue
        i_bRes=.T.
        * --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
        i_var=This.cFormVar
        i_bUpd=.F.
        i_xOldValue=This.Parent.&i_var
  	   LOCAL TestMacro
  	   TestMacro=This.bUpd Or (Not(This.RadioValue()==i_xOldValue) And Not(Empty(This.Value) And Empty(This.Parent.&i_var)))
        If TestMacro
            i_bUpd=.T.
            This.GetRadio()
            This.SetModified()
            This.bUpd=.F.
        Endif
        * --- After Input solo alla variazione del campo
       LOCAL TestMacro
  	   TestMacro=This.Parent.&i_var
        If i_bUpd
            This.mAfter()
            If This.RadioValue()<>TestMacro
                This.SetControlValue()
            Endif
        Endif
        * ---
        If Not(Inlist(This.Parent.Parent.oContained.cFunction,'Filter','Query'))
            i_bErr=.F.
            i_bRes=Iif(i_bUpd,.F.,.T.)
            oCpToolBar.b9.Enabled=.F.
            If Not(i_bErr) And i_bUpd
                * --- Procedura di check e calcolo se il campo e' cambiato
                If This.Check()
                    i_bRes=.T.
                Else
                    This.Parent.&i_var=cp_NullValue(This.Parent.&i_var)
                    This.SetControlValue()
                Endif
                This.SetTotal(i_xOldValue)
                This.Parent.Parent.oContained.NotifyEvent(i_var+' Changed')
                This.Parent.Parent.oContained.mCalc(i_bUpd)
                This.Parent.Parent.oContained.SaveDependsOn()
            Else
                This.SetTotal(i_xOldValue)
            Endif
        Endif
        Return(i_bRes)
	endfunc
	
	Proc oZOOMFACTOR.InteractiveChange
		if VarType(this.Parent.oPreviewObject)='O'
			this.Parent.oPreviewObject.Zoom(this.RadioValue())
		endif
	EndProc
	
	Proc oZOOMFACTOR.ProgrammaticChange
		if VarType(this.Parent.oPreviewObject)='O'
			this.Parent.oPreviewObject.Zoom(this.RadioValue())
		endif
	EndProc
	
	Proc oZOOMFACTOR.LostFocus()
        Local i_cFlt,i_cFile,i_nConn
        *This.Parent.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
		
        If !This.bNoBackColor
            This.BackColor=i_nEBackColor
        Endif
        Return
    endproc
    
	proc oBtnFirstPage.Click()
      if VarType(this.Parent.oPreviewObject)='O'
        this.Parent.oPreviewObject.GoToFirstPage()
      endif
    endproc
	
	proc oBtnPreviousPage.Click()
      if VarType(this.Parent.oPreviewObject)='O'
        this.Parent.oPreviewObject.GoToPreviousPage()
      endif
    endproc
	
	proc oBtnNextPage.Click()
      if VarType(this.Parent.oPreviewObject)='O'
        this.Parent.oPreviewObject.GoToNextPage()
      endif
    endproc
	
	proc oBtnLastPage.Click()
      if VarType(this.Parent.oPreviewObject)='O'
        this.Parent.oPreviewObject.GoToLastPage()
      endif
    endproc
	
	Func oPageToGo.When()
        Local i_bRes,i_e
        * --- Procedure standard del campo
        i_bRes=.T.
        If !Inlist(This.Parent.Parent.oContained.cFunction,"Query","Filter")
            i_bRes = This.mCond()
            If !Empty(This.cfgCond)
                i_e=Strtran(This.cfgCond,'w_','this.parent.w_')
                i_bRes=&i_e
            Endif
        Endif
        Return(i_bRes)
	endfunc
	
	proc oPageToGo.LostFocus
		if this.valid()=1
			this.Parent.oPreviewObject.GoToPage(val(this.Value))
		else
			if VarType(this.Parent.oPreviewObject)='O'
				this.value = this.Parent.oPreviewObject.nCurrentPageNumber
			else
				this.value = 0
			endif
		endif
		this.Parent.SetPageStringValue()
	endproc
	
	Proc oPageToGo.GotFocus()
		if VarType(this.Parent.oPreviewObject)='O'
			this.value = ALLTRIM(STR(this.Parent.oPreviewObject.nCurrentPageNumber))
		else
			this.value = "0"
		endif
	EndProc
	
	func oPageToGo.Valid
		return iif(VarType(this.Parent.oPreviewObject)='O' and val(this.value)>=1 and val(this.value)<=this.Parent.oPreviewObject.nTotalPages, 1 , 0 )
	endfunc
	
	Proc SetPageStringValue
		if VarType(this.oPreviewObject)='O'
			this.oPageToGo.value = ah_MsgFormat(this.cPageString, Alltrim(Str(this.oPreviewObject.nCurrentPageNumber)), Alltrim(Str(this.oPreviewObject.nTotalPages)))
		endif
	EndProc
	
	Proc oBtnFind.Click
		if VarType(this.Parent.oPreviewObject)='O' and !EMPTY(this.parent.oSTRINGTOFIND.text)
			this.Parent.oPreviewObject.FindString(alltrim(this.parent.oSTRINGTOFIND.text), .F.)
		endif
	endproc 
	
	Func oSTRINGTOFIND.When()
        Local i_bRes,i_e
        * --- Procedure standard del campo
        i_bRes=.T.
        If !Inlist(This.Parent.Parent.oContained.cFunction,"Query","Filter")
            i_bRes = This.mCond()
            If !Empty(This.cfgCond)
                i_e=Strtran(This.cfgCond,'w_','this.parent.w_')
                i_bRes=&i_e
            Endif
        Endif
        Return(i_bRes)
	endfunc
	
	proc oSTRINGTOFIND.LostFocus
		if this.valid()=1
			if !this.parent.bFindButtonVisible and !EMPTY(this.text)
				this.Parent.oPreviewObject.FindString(alltrim(this.text), .F.)
			endif
		endif
	endproc
	
	Proc oSTRINGTOFIND.GotFocus()
	EndProc
	
	func oSTRINGTOFIND.Valid
		return iif(VarType(this.Parent.oPreviewObject)='O', 1 , 0 )
	endfunc
	
	PROCEDURE Init
		this.oZOOMFACTOR.value = 7
		this.oZOOMFACTOR.RadioValue()
	ENDPROC
	
	PROCEDURE Resize
		local nCurrentX
		DoDefault()
		nCurrentX = 0
		this.oBtnFirstPage.Visible = this.bFirstPageVisible
		nCurrentX = nCurrentX + IIF(this.oBtnFirstPage.Visible, this.oBtnFirstPage.Width + 1, 0 )
		this.oBtnPreviousPage.Visible =  this.bPreviousPageVisible
		this.oBtnPreviousPage.Left = nCurrentX
		nCurrentX = nCurrentX + IIF(this.oBtnPreviousPage.Visible, this.oBtnPreviousPage.Width + 1, 0 )
		this.oPageToGo.Visible =  this.bPageStringVisible
		this.oPageToGo.Left = nCurrentX
		nCurrentX = nCurrentX + IIF(this.oPageToGo.Visible, this.oPageToGo.Width + 1, 0 )
		this.oBtnNextPage.Visible = this.bNextPageVisible
		this.oBtnNextPage.Left = nCurrentX
		nCurrentX = nCurrentX + IIF(this.oBtnNextPage.Visible, this.oBtnNextPage.Width + 1, 0 )
		this.oBtnLastPage.Visible = this.bLastPageVisible
		this.oBtnLastPage.Left = nCurrentX
		nCurrentX = nCurrentX + IIF(this.oBtnLastPage.Visible, this.oBtnLastPage.Width + 1, 0 )
		this.oStrVisual.Visible = this.bZoomComboVisible
		this.oStrVisual.Left = nCurrentX
		this.oZOOMFACTOR.Visible = this.bZoomComboVisible
		this.oZOOMFACTOR.Left = nCurrentX
		nCurrentX = nCurrentX + IIF(this.oZOOMFACTOR.Visible, this.oZOOMFACTOR.Width + 1, 0 )
		this.oStrFind.Visible = this.bFindStringVisible
		this.oStrFind.Left = nCurrentX
		this.oStrFind.Width = this.width - this.oStrFind.Left - IIF(this.bFindButtonVisible, this.oBtnFind.width + 1 , 0)
		this.oSTRINGTOFIND.Visible = this.bFindStringVisible
		this.oSTRINGTOFIND.Left = nCurrentX
		this.oSTRINGTOFIND.Width = this.oStrFind.Width
		this.oBtnFind.Visible = this.bFindButtonVisible
		this.oBtnFind.Left = nCurrentX + this.oSTRINGTOFIND.Width + 1
	ENDPROC
	
	PROCEDURE oPreviewObject_Assign
		LPARAMETERS oNewVal
		If VarType(m.oNewVal)='O'
			this.oPreviewObject = m.oNewVal
		else
			this.oPreviewObject = .null.
		endif
	ENDPROC

	PROCEDURE Destroy
		DoDefault()
	ENDPROC

	PROCEDURE Event
		LPARAMETERS cEvent
		DoDefault()
	ENDPROC

	PROCEDURE Calculate
		DODEFAULT()
	ENDPROC
	
	PROCEDURE PreviewUpdated
		this.SetPageStringValue()
	ENDPROC
ENDDEFINE

DEFINE CLASS TextBoxForPageToGo as Container
     width=80
     height=34
     BorderWidth=0
     BackStyle=0
     BorderStyle=0
     nPageToGo=0
	 cPageString = "Pag. %1 di %2"     

     Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1
     add object Text1 as textbox with Caption='',Top=8,Left=0,Height=24,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.t.

     PROCEDURE INIT(oXfrxToolBar)
         this.TEXT1.SetPageStringValue()
         this.Width=this.TEXT1.width+1
         This.TEXT1.top=int((this.Height-this.TEXT1.height)/2)
         this.TEXT1.Anchor=1         
     ENDPROC
     
     Proc TEXT1.SetPageStringValue
		if VarType(this.Parent.Parent.oPreviewObject)='O'
			this.value = ah_MsgFormat(this.parent.cPageString, Alltrim(Str(this.Parent.Parent.oPreviewObject.nCurrentPageNumber)), Alltrim(Str(this.Parent.Parent.oPreviewObject.nTotalPages)))
		endif
     ENDPROC
     
     PROCEDURE text1.gotfocus
         this.value=""
         if type("this.Parent.Parent.cActiveBox")="C"
             this.Parent.Parent.cActiveBox="PageToGo"
         ENDIF
         this.parent.btnDummy.SetFocus()
         this.SetFocus()
         this.parent.btnDummy.SetFocus()
         this.SetFocus()
         this.parent.btnDummy.SetFocus()
         this.SetFocus()
         this.parent.btnDummy.SetFocus()
         this.SetFocus()                           
         this.parent.btnDummy.SetFocus()
         this.SetFocus()         
         DoDefault()
     ENDPROC
     
*!*	     PROCEDURE text1.valid
*!*	         IF NOT EMPTY(val(this.Value))
*!*	             this.Parent.Parent.oPreviewObject.GoToPage(val(this.Value))
*!*	             this.SetPageStringValue() 
*!*	         ENDIF
*!*	         return .t.
*!*	     endproc
     
ENDDEFINE

DEFINE CLASS TextBoxForSearch as Container
     width=80
     height=34
     BorderWidth=0
     BackStyle=0
     BorderStyle=0

     Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1
     add object Text1 as textbox with Caption='',Top=8,Left=0,Height=24,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.t.

     PROCEDURE INIT(oXfrxToolBar)
         this.Width=this.TEXT1.width+1
         This.TEXT1.top=int((this.Height-this.TEXT1.height)/2)
         this.TEXT1.Anchor=1         
     ENDPROC

     PROCEDURE text1.gotfocus
         this.value=""
         if type("this.Parent.Parent.cActiveBox")="C"
             this.Parent.Parent.cActiveBox="StringToFind"
         endif
         this.parent.btnDummy.SetFocus()
         this.SetFocus()
         this.parent.btnDummy.SetFocus()
         this.SetFocus()
         this.parent.btnDummy.SetFocus()
         this.SetFocus()
         this.parent.btnDummy.SetFocus()
         this.SetFocus()
         this.parent.btnDummy.SetFocus()
         this.SetFocus()                           
         DoDefault()

     ENDPROC

ENDDEFINE

DEFINE CLASS ComboBoxForZoom as Container
     width=114
     height=34
     BorderWidth=0
     BackStyle=0
     BorderStyle=0
     w_COMBO1=-2
     Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1     
     add object Combo1 as Combobox with Top=8,Left=0,width=114,aheight=18,FontSize=8,ForeColor=RGB(0,0,0),FontItalic=.t.,Visible=.t.,RowSourceType=1,RowSource=""+"500%,"+"300%,"+"200%,"+"150%,"+"100%,"+"75%,"+"50%,"+"25%,"+"10%,"+"Adatta larghezza,"+"Adatta pagina"
	 func COMBO1.RadioValue()
		 return(iif(this.value =1,500,;
		 iif(this.value =2,300,;
		 iif(this.value =3,200,;
		 iif(this.value =4,150,;
		 iif(this.value =5,100,;		 
		 iif(this.value =6,75,;
		 iif(this.value =7,50,;
		 iif(this.value =8,25,;
		 iif(this.value =9,10,;		 
		 iif(this.value =10,-1,;
		 iif(this.value =11,-2,;
		 0))))))))))))
	 endfunc

	 func COMBO1.GetRadio()
	     this.Parent.w_COMBO1 = this.RadioValue()
		 return .t.
	 endfunc

	 func COMBO1.SetRadio()
		 this.value = ;
		   iif(this.Parent.w_COMBO1==500,1,;
		   iif(this.Parent.w_COMBO1==300,2,;
		   iif(this.Parent.w_COMBO1==200,3,;
		   iif(this.Parent.w_COMBO1==150,4,;
		   iif(this.Parent.w_COMBO1==100,5,;
		   iif(this.Parent.w_COMBO1==75,6,;
		   iif(this.Parent.w_COMBO1==50,7,;
		   iif(this.Parent.w_COMBO1==25,8,;
		   iif(this.Parent.w_COMBO1==10,9,;		   		   
		   iif(this.Parent.w_COMBO1==-1,10,;
		   iif(this.Parent.w_COMBO1==-2,11,;
		   0)))))))))))
	 endfunc
	
	 Proc COMBO1.InteractiveChange
	     if VarType(this.Parent.Parent.oPreviewObject)='O'
		     this.Parent.Parent.oPreviewObject.Zoom(this.RadioValue())
		 endif
	 EndProc
	
	 Proc COMBO1.ProgrammaticChange
		 if VarType(this.Parent.Parent.oPreviewObject)='O'
			 this.Parent.Parent.oPreviewObject.Zoom(this.RadioValue())
		 endif
	 EndProc
	
     PROCEDURE INIT(oXfrxToolBar)
          * this.combo1.value=100
          this.COMBO1.SetRadio()
          this.Width=this.combo1.width+1
          *this.Height=oXfrxToolbar.Height-2
          This.Combo1.top=int((this.Height-this.combo1.height)/2)
          this.Combo1.Anchor=1
     ENDPROC
     
     PROCEDURE Combo1.gotfocus
         if type("this.Parent.Parent.cActiveBox")="C"
             this.Parent.Parent.cActiveBox="ZoomFactor"
         ENDIF
         this.parent.btnDummy.SetFocus()
         this.SetFocus()
         this.parent.btnDummy.SetFocus()
         this.SetFocus()         
         DoDefault()
     ENDPROC
ENDDEFINE

DEFINE CLASS zxfrxToolbar as CommandBar
	oPreviewObject = .null.
	bFirstPageVisible=.T.
	bPreviousPageVisible=.T.
    bGoToPageVisible=.T.
	bNextPageVisible=.T.
	bLastPageVisible=.T.
	bFindStringVisible=.T.
	bFindButtonVisible=.T.
	bPageStringVisible=.T.
	bZoomComboVisible=.T.
	bPrintVisible=.T.
	bPrintOptVisible=.T.	
	bQuitVisible=.T.	
	&&bPageButtonVisible=.T.
	cPageString = "Pag. %1 di %2"
	* Per consentire di lanciare metodi alla pressione dell'invio.
	* cActiveBox pu� essere "PageToGo", "ZoomFactor", "StringToFind"
	KeyPreview = .T.
	cActiveBox = "None"
	caption = "Anteprima di stampa"
	
	
Proc Init(bStyle)
    DoDefault(bStyle)
    Local oBtn
    *this.FontName="Arial"
    oBtnFirstPage = this.AddButton("oBtnFirstPage")
    oBtnFirstPage.Caption = ''
    oBtnFirstPage.cImage = "BMP\FIRST.BMP"
    oBtnFirstPage._OnClick = "This.parent.oPreviewObject.GotoFirstPage()"
    oBtnFirstPage.ToolTipText = cp_Translate("Va alla prima pagina")
    oBtnFirstPage.Visible=this.bFirstPageVisible
		
    oBtnPreviousPage = this.AddButton("oBtnPreviousPage")
    oBtnPreviousPage.Caption = ''
    oBtnPreviousPage.cImage = "BMP\LEFT.BMP"
    oBtnPreviousPage._OnClick = "This.parent.oPreviewObject.GoToPreviousPage()"
    oBtnPreviousPage.ToolTipText = cp_Translate("Va alla pagina precedente")
    oBtnPreviousPage.Visible=this.bPreviousPageVisible		
        
    * this.AddObject("oPageToGo","TextBoxForPageToGo")
    * this.oPageToGo.Visible=.T.        
		* this.oPageToGo.ToolTipText = cp_Translate("Va alla pagina")
        
    oBtnGoToPage = this.AddButton("oBtnGoToPage")
    oBtnGoToPage.Caption = ''
    oBtnGoToPage.cImage = "BMP\Visualiz.bmp"
    oBtnGoToPage._OnClick = "This.parent.oPreviewObject.GoToPage()" 
    oBtnGoToPage.ToolTipText = cp_Translate("Va alla pagina") 
    oBtnGoToPage.Visible=this.bGoToPageVisible
		
    oBtnNextPage = this.AddButton("oBtnNextPage")
    oBtnNextPage.Caption = ''
    oBtnNextPage.cImage = "BMP\RIGHT.BMP"
    oBtnNextPage._OnClick = "This.parent.oPreviewObject.GoToNextPage()"
    oBtnNextPage.ToolTipText = cp_Translate("Va alla pagina successiva")
    oBtnNextPage.Visible=this.bNextPageVisible
        
   	oBtnNextPage = this.AddButton("oBtnLastPage")
    oBtnNextPage.Caption = ''
    oBtnNextPage.cImage = "BMP\LAST.BMP"
    oBtnNextPage._OnClick = "This.parent.oPreviewObject.GoToLastPage()"
    oBtnNextPage.ToolTipText = cp_Translate("Va all'ultima pagina")
    oBtnNextPage.Visible=this.bLastPageVisible
        
    this.AddObject("oZoomFactor","ComboBoxForZoom",this)
    this.oZoomFactor.Visible=.T.        
    this.oZoomFactor.ToolTipText = cp_Translate("Visualizzazione")
        
    * this.AddObject("oStringToFind","TextBoxForSearch")
    * this.oStringToFind.Visible=.T.	
    * this.oStringToFind.ToolTipText = cp_Translate("Stringa da cercare")
		
   	oBtnFind = this.AddButton("oBtnFind")
    oBtnFind.Caption = ''
    oBtnFind.cImage = "BMP\Find.BMP"
    oBtnFind._OnClick = "this.Parent.oPreviewObject.FindString(, .F.)"
    oBtnFind.ToolTipText = cp_Translate("Esegue la ricerca")
    oBtnFind.Visible=this.bLastPageVisible

    oBtnPrint = this.AddButton("oBtnPrint")
    oBtnPrint.Caption = ''
    oBtnPrint.cImage = "BMP\Print.bmp"
    oBtnPrint._OnClick = "this.Parent.oPreviewObject.Print()"
    oBtnPrint.ToolTipText = cp_Translate("Stampa")
    oBtnPrint.Visible=this.bPrintVisible
    
    oBtnPrintOpt = this.AddButton("oBtnPrintOpt")
    oBtnPrintOpt.Caption = ''
    oBtnPrintOpt.cImage = "BMP\Proption.bmp"
    oBtnPrintOpt._OnClick = "this.Parent.oPreviewObject.PrintOpt()"
    oBtnPrintOpt.ToolTipText = cp_Translate("Stampa con opzioni")
    oBtnPrintOpt.Visible=this.bPrintOptVisible    
    
    oBtnQuit = this.AddButton("oBtnQuit")
    oBtnQuit.Caption = ''
    oBtnQuit.cImage = "BMP\EscTb.bmp"
    oBtnQuit._OnClick = "this.Parent.oPreviewObject.QuitPreview()"
    oBtnQuit.ToolTipText = cp_Translate("Uscita")
    oBtnQuit.Visible=this.bQuitVisible   
    
    this.visible=.t.
  endproc
   
***	    Procedure KeyPress
***	        Lparameters nKeyCode, nShiftAltCtrl
***	        If m.nKeyCode = 13 and this.cActiveBox="PageToGo"
***	                *this.oPageToGo.TEXT1.VALID()
***	                IF NOT EMPTY(val(this.oPageToGo.TEXT1.Value))
***	                    * Se l'espressione nel campo corrisponde a un numero di pagina esegue il posizionamento
***	                    IF this.oPreviewObject.nTotalPages >= VAL(this.oPageToGo.TEXT1.Value)
***	                        * Se � stata richiesta una pagina esistente
***                            this.oPreviewObject.GoToPage(val(this.oPageToGo.TEXT1.Value))
***                        ELSE
***                            * Se la pagina richiesta dall'utente non esiste rimane sulla pagina corrente
***                            this.oPageToGo.TEXT1.Value = this.oPreviewObject.nCurrentPageNumber
***                        ENDIF
***                        * Scrive 'Pag x di y'
***                        this.oPageToGo.TEXT1.SetPageStringValue() 
***                    ENDIF
***	                
***	                this.oPageToGo.btnDummy.SetFoCus()
***	        ENDIF
***	        DODEFAULT(nKeyCode, nShiftAltCtrl)
***	    Endproc

*!*		Proc oZOOMFACTOR.InteractiveChange
*!*			if VarType(this.oPreviewObject)='O'
*!*			    WAIT WINDOW "xyz"
*!*				this.oPreviewObject.Zoom(this.oPreviewObject.RadioValue())
*!*			endif
*!*		EndProc
*!*		
*!*		Proc oZOOMFACTOR.ProgrammaticChange
*!*			if VarType(this.oPreviewObject)='O'
*!*			    WAIT WINDOW "xyz"		
*!*				this.oPreviewObject.Zoom(this.oPreviewObject.RadioValue())
*!*			endif
*!*		EndProc

	
ENDDEFINE

Define Class zPageMoverButton As CPCommandButton
	bDirectionUp = .F.
	bGoToFirstStep = .F.
	
	Proc Init
		*Verifico se nella pagina corrente � gi� stato aggiunta la propriet� 
		*per memorizzare le varie posizioni assunte del pageframe, se non � presenta la aggiungo
		if type('this.parent.aPageMovers')='U'
			this.Parent.AddProperty("aPageMovers",CREATEOBJECT("Collection"))
		endif
	EndProc
	
	Proc Click
		if this.bDirectionUp
			* Devo tornare allo step precedente, recupero l'ultima posizione assunta dal pageframe
			if this.parent.parent.oPAG.aPageMovers.Count>0
				* Se non sono arrivato all'inizio della collezione sposto
				* il pageframe alla posizione precedente e elimino l'informazioni dalla collezione
				* oppure se la propriet� � settata vado alla prima posizione e elimino tutte le posizioni
				if this.bGoToFirstStep
					this.parent.parent.oPAG.Top = this.parent.parent.oPAG.aPageMovers[1]
					do while this.parent.parent.oPAG.aPageMovers.Count>0
						this.parent.parent.oPAG.aPageMovers.Remove(this.parent.parent.oPAG.aPageMovers.Count)
					enddo
				else
					this.parent.parent.oPAG.Top = this.parent.parent.oPAG.aPageMovers[this.parent.parent.oPAG.aPageMovers.Count]
					this.parent.parent.oPAG.aPageMovers.Remove(this.parent.parent.oPAG.aPageMovers.Count)
				endif
			endif
		else
			* Salvo la posizione del pagaframe attuale nella collezione
			this.parent.parent.oPAG.aPageMovers.Add(this.parent.parent.oPAG.Top)
			* Sposto il pageframe alla nuova posizione
			this.parent.parent.oPAG.Top = (this.top + this.width ) * -1
		endif
		this.parent.parent.oPAG.Refresh()
	EndProc
	
	Proc mCond
		DoDefault()
	endproc
	
EndDefine

DEFINE CLASS zxfrxsheet AS container


	Width = 257
	Height = 292
	BackColor = RGB(255,255,255)
	nprevx = -100
	nprevy = -100
	*-- Shift, Ctrl, Alt
	_nshift = 0
	*-- Mode move mouse
	_mode = 0
	*-- Flag of first visible
	_visible = .T.
	*-- Backup MouseIcon
	_mouseicon = ""
	*-- Backup MousePointer
	_mousepointer = 0
	emf = .F.
	canvashwnd = .F.
	uniqueid = .F.
	canvaswidth = .F.
	canvasheight = .F.
	llmm = .F.
	lastrefresh = .F.
	Name = "zxfrxsheet"
	noriwidth = .F.
	noriheight = .F.
	_mousedown = .F.
	_mousemove = .F.

	*-- Internal flag
	_setmouseicon = .F.

	*-- Flag of any change on any object
	change = .F.
	
	*-- Simulazione dblclick
	nDblClickCounter = -1
	nDoubleClickTime = 0


	ADD OBJECT refreshtimer AS timer WITH ;
		Top = 72, ;
		Left = 108, ;
		Height = 23, ;
		Width = 23, ;
		Name = "refreshtimer"


	*-- Set custom mouse pointer
	PROCEDURE setmousepointer
		LPARAM luPointer
		* luPointer - Mouse Pointer

		IF ISNULL(luPointer)
		   This.MousePointer=This._MousePointer
		   This.MouseIcon=This._MouseIcon
		   This._SetMouseIcon=.F.
		ELSE
		   IF !This._SetMouseIcon
		      This._MousePointer=This.MousePointer
		      This._MouseIcon=This.MouseIcon

		      IF TYPE("luPointer")="C"
		         This.MousePointer=99
		         This.MouseIcon=luPointer
		      ELSE
		         This.MousePointer=luPointer
		      ENDIF
		      This._SetMouseIcon=.T.
		   ENDIF
		ENDIF
	ENDPROC


	*-- Set info about Active object and event
	PROCEDURE setactiveobject
		LPARAMETERS loObj,lcEvent
		IF TYPE("This.Parent")="O" AND !ISNULL(This.Parent)
		   This.Parent.Parent.ActiveObjectEvent(loObj,lcEvent)
		ENDIF
	ENDPROC


	PROCEDURE deletemetafile
		IF !EMPTY(this.emf)
			_xfDeleteMetafile(this.emf)
			this.emf = 0
		endif
	ENDPROC


	PROCEDURE repaint
		this.Parent.Parent.repaint(this)
	ENDPROC


	PROCEDURE setvisibility
		LPARAMETERS tlShow

		#define SW_HIDE             0
		#define SW_SHOWNORMAL       1
		#define SW_NORMAL           1
		#define SW_SHOWMINIMIZED    2
		#define SW_SHOWMAXIMIZED    3
		#define SW_MAXIMIZE         3
		#define SW_SHOWNOACTIVATE   4
		#define SW_SHOW             5
		#define SW_MINIMIZE         6
		#define SW_SHOWMINNOACTIVE  7
		#define SW_SHOWNA           8
		#define SW_RESTORE          9
		#define SW_SHOWDEFAULT      10
		#define SW_FORCEMINIMIZE    11
		#define SW_MAX              11

		IF NOT EMPTY(this.canvashwnd)
			IF tlShow
				ShowWindow(this.canvashwnd, SW_SHOW)
			ELSE
				ShowWindow(this.canvashwnd, SW_HIDE)
			ENDIF
		ENDIF
	ENDPROC


	PROCEDURE m_click
		LPARAMETERS tnButton

		DO case
			CASE tnButton = 0
				lnY = MROW(thisform.name,3)
				lnX = MCOL(thisform.name,3)

				IF TYPE("lnY")<>"N" OR TYPE("lnX")<>"N"
					RETURN
				endif

				lnY = lnY-OBJTOCLIENT(this, 1)
				lnX = lnX-OBJTOCLIENT(this, 2)

			    *-- simulazione doppio click
			    IF this.nDblClickCounter <> -1 AND SECONDS() - this.nDblClickCounter < this.nDoubleClickTime
                    this.nDblClickCounter = -1
                    IF PEMSTATUS(this.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent,"NotifyEvent",5)
                        this.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent.NotifyEvent("DblClickOnPreview")
                    ENDIF                    
                ELSE
                    this.Parent.Parent.m_click(lnY, lnX, this)
			        this.nDblClickCounter = SECONDS()                    
                ENDIF			        


			OTHERWISE
				this.Parent.Parent.rightclick()
		ENDCASE
	ENDPROC


	PROCEDURE m_move
		LPARAMETERS nButton, nShift, lnX, lnY
		IF this.llMM
			RETURN
		ENDIF

		IF !PEMSTATUS(thisform,"_xf_iLastMM",5)
			thisform.AddProperty("_xf_iLastMM",this)
		ELSE
			thisform._xf_iLastMM = this.Parent.parent
		ENDIF

		this.llMM = .t.
		lnY = MROW(thisform.name,3)
		lnX = MCOL(thisform.name,3)
		IF TYPE("lnY")<>"N" OR TYPE("lnX")<>"N"
			this.llMM = .f.
			RETURN
		endif

		lnY = lnY-OBJTOCLIENT(this, 1)
		lnX = lnX-OBJTOCLIENT(this, 2)

		IF this.Parent.Parent.isOverHyperlink(lnX, lnY, this)
			IF VERSION(5)>=800
				this.MousePointer=15
			ELSE
				this.MousePointer = 99
				this.MouseIcon = "xfrxhand2.cur"
			endif
		ELSE
			this.MousePointer=0
		ENDIF
		this.llMM = .f.
	ENDPROC


	PROCEDURE Init
		this.lastrefresh = 0
		DECLARE INTEGER GetDoubleClickTime IN user32
    	this.nDoubleClickTime = GetDoubleClickTime()/1000
	ENDPROC


	PROCEDURE Resize
		IF NOT EMPTY(this.canvashwnd)
			lnsheettop  = OBJTOCLIENT(this.Parent, 1)+1
			lnSheetLeft = OBJTOCLIENT(this.Parent, 2)

			#IF .f.
			LOCAL loObj, lnsheetleft, lnsheettop
			loObj = this.parent
			lnsheetleft = loObj.left
			lnsheettop = loObj.top
			? lnSheetTop
			DO WHILE .t.
				loObj = loObj.parent
				IF VARTYPE(loObj)<>"O"
					EXIT
				ENDIF
				IF ISNULL(loObj)
					EXIT
				ENDIF
				IF UPPER(loObj.baseclass) = "FORM"
					EXIT
				ENDIF
				IF PEMSTATUS(loObj, "left",5)
					lnsheetleft = lnsheetleft + loObj.left
					lnsheettop = lnsheettop + loObj.top
				endif
				? lnSheetTop
			ENDDO
			#ENDIF
			LOCAL lnLeft, lnTop
			lnLeft = lnsheetleft + MAX(this.left,0)
			lnTop = lnsheettop + MAX(this.top,0)
			this.canvasWidth = lnsheetleft + min(this.left + this.width,this.Parent.Width)-lnLeft
			this.canvasHeight= lnsheettop + min(this.top + this.height,this.Parent.height-1)-lnTop-this.Parent.parent.horscrl.height
			=MoveWindow(this.canvashwnd, lnleft,lntop,this.canvasWidth,this.canvasHeight,0)
		*	this.repaint()
		ENDIF
	ENDPROC


	PROCEDURE Destroy
		LOCAL lcVariableName 

		lcVariableName = "__xf_g_wl"+ALLTRIM(STR(ABS(this.canvasHWND)))
		IF TYPE(lcVariableName)="N"
			_xfUnregisterCanvas(this.canvasHWND, EVALUATE(lcVariableName))
			RELEASE (lcVariableName)
		ENDIF

		lcVariableName = "__xf_g_ch"+ALLTRIM(STR(ABS(this.canvasHWND)))
		IF TYPE(lcVariableName)="O"
			STORE null TO (lcVariableName)
			RELEASE (lcVariableName)
		endif

		IF NOT EMPTY(this.UniqueID)
			lcVariableName = "__xf_g_ca"+ALLTRIM(STR(ABS(this.UniqueID)))
			STORE null TO (lcVariableName)
			RELEASE (lcVariableName)
			DestroyWindow(THIS.canvashwnd)
			this.DeleteMetafile()
		endif
	ENDPROC


	PROCEDURE RightClick
		IF TYPE("This.Parent")="O" AND !ISNULL(This.Parent)
		   This.Parent.RightClick()
		ENDIF
	ENDPROC


	PROCEDURE MouseWheel
		LPARAMETERS nDirection, nShift, nXCoord, nYCoord
		IF PEMSTATUS(This,"Parent",5) AND !ISNULL(This.Parent)
		   IF nDirection=-120
		      This.Parent.Parent.VersCrl.cmdRight.Click()
		   ENDIF

		   IF nDirection=120 && 'DNAR'
		      This.Parent.Parent.VersCrl.cmdLeft.Click()
		   ENDIF
		ENDIF
	ENDPROC


	PROCEDURE MouseDown
		LPARAMETERS nButton, nShift, nXCoord, nYCoord,loObj
		This._nShift=nShift

		IF nButton=1 AND This._Mode=_xfrx_Mode_None && Left Click
		   This.nprevx = nXCoord
		   This.nprevy = nYCoord
		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_NewObject
		   This.nprevx = nXCoord
		   This.nprevy = nYCoord
		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_ROR
		   This.nprevx = nXCoord
		   This.nprevy = nYCoord

		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_ROB
		   This.nprevx = nXCoord
		   This.nprevy = nYCoord

		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_ROL
		   This.nprevx = nXCoord
		   This.nprevy = nYCoord

		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_ROT
		   This.nprevx = nXCoord
		   This.nprevy = nYCoord

		ENDIF

		IF nButton=4 && Middle Click
		   LOCAL lcPath,lii,loCNT
		   loCNT=This.Parent.Parent
		   This._MouseDown=!This._MouseDown
		   IF This._MouseDown
		      This._Mode=_xfrx_Mode_MoveSheetTimer
		      This.nprevx = nXCoord
		      This.nprevy = nYCoord

		      lcPath=loCNT.XPath
		      This.SetMousePointer(.NULL.)
		      This.SetMousePointer(lcPath+"xfrxoff.cur")
		      FOR lii=1 TO This.ControlCount
		          loObj=This.Controls(lii)
		          =IIF(PEMSTATUS(loObj,"SetMousePointer",5),loObj.SetMousePointer(.NULL.),.T.)
		          =IIF(PEMSTATUS(loObj,"SetMousePointer",5),loObj.SetMousePointer(lcPath+"xfrxoff.cur"),.T.)
		      NEXT
		      loCNT.tmrMdMove.Enabled=.T.
		   ELSE
		      STORE -100 TO This.nPrevX,This.nPrevY
		      loObj=IIF(TYPE("loObj")="O",loObj,This)
		      loObj.SetMousePointer(.NULL.)
		      IF loCNT.DMSwitch AND loCNT.oDM._NewClass#0
		         This.SetMousePointer(.NULL.)
		         FOR lii=1 TO This.ControlCount
		             loObj=This.Controls(lii)
		             =IIF(PEMSTATUS(loObj,"SetMousePointer",5),loObj.SetMousePointer(.NULL.),.T.)
		         NEXT
		         loCNT.SetNewObject(loCNT.oDM._NewClass)
		         This._Mode=_xfrx_Mode_NewObject
		      ELSE
		         This._Mode=_xfrx_Mode_None
		      ENDIF
		      loCNT.tmrMdMove.Enabled=.F.
		   ENDIF
		ENDIF
	ENDPROC


	PROCEDURE MouseUp
		LPARAMETERS nButton, nShift, nXCoord, nYCoord,loObj
		LOCAL lii
		This.Parent.MouseUp(nButton, nShift, nXCoord, nYCoord,loObj)

		IF nButton=1
		   STORE -100 TO this.nprevx,this.nprevy
		ENDIF

		IF This.nPrevX=-100
		   This._MouseMove=.F.
		   This._MouseDown=.F.

		   IF This._Mode=_xfrx_Mode_MoveSheetTimer OR This._Mode=_xfrx_Mode_NewObject
		   ELSE
		      This.SetMousePointer(.NULL.)
		      FOR lii=1 TO This.ControlCount
		          loObj=This.Controls(lii)
		          =IIF(PEMSTATUS(loObj,"SetMousePointer",5),loObj.SetMousePointer(.NULL.),.T.)
		      NEXT
		      This._Mode=_xfrx_Mode_None
		   ENDIF
		ENDIF

		This._nShift=0
		IF PEMSTATUS(This,"cntSelectOs",5)
		   This.cntSelectOs.Visible=.F.
		ENDIF
	ENDPROC


	PROCEDURE MouseMove
		LPARAMETERS nButton, nShift, nXCoord, nYCoord,loObjX
		IF (This.nPrevX=-100 OR This._MouseMove) AND This._Mode#_xfrx_Mode_NewObject
		   IF This.nPrevX=-100
		      This._Mode=_xfrx_Mode_None
		   ENDIF
		   RETURN
		ENDIF

		LOCAL llLS,lii,loCnt,loObj,loParent,liLeft,liTop,lnLeft,lnTop,;
		      liMLeft,liMTop,liNLeft,liNTop,liALeft,liATop,liWidth,liHeight,lcPath

		llLS=Thisform.LockScreen
		Thisform.LockScreen=.T.
		loParent=This.Parent
		loCnt=loParent.Parent

		lcPath=loCNT.XPath

		IF TYPE("loObjX")="L"
		   IF !INLIST(This._Mode,_xfrx_Mode_NewObject,_xfrx_Mode_MoveSheetTimer)
		      This._Mode=IIF(nButton=1 AND loCnt.DMSwitch,_xfrx_Mode_SelectObjectes,_xfrx_Mode_MoveSheet)
		   ENDIF
		ELSE
		   IF This._Mode=_xfrx_Mode_None
		      IF nButton=1 AND loCnt.DMSwitch AND !UPPER(loObjX.Class)=="zxfrxsheet" AND ;
		         TYPE("loCnt.ActiveSheet.cntSel_"+loObjX.Name+".Visible")="L" AND ;
		         EVAL("loCnt.ActiveSheet.cntSel_"+loObjX.Name+".Visible")
		         This._Mode=_xfrx_Mode_MoveObject
		      ELSE
		         IF nButton=1 AND (loCnt.DMSwitch AND UPPER(loObjX.Class)=="zxfrxsheet" OR !loCnt.DMSwitch)
		            This._Mode=_xfrx_Mode_MoveSheet
		         ENDIF
		      ENDIF
		   ENDIF
		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_MoveSheet
		   IF ABS(This.nprevx - nXCoord) + ABS(This.nprevy - nYCoord) > 5 AND nButton=1
		      This._MouseMove=.T.

		      loObjX=IIF(TYPE("loObjX")="O",loObjX,This)
		      loObjX.SetMousePointer(lcPath+"xfrxhand.cur")

		      liTop=IIF(loParent.Height<This.Height,0,(loParent.Height-This.Height)/2)
		      liLeft=IIF(loParent.Width<This.Width,0,(loParent.Width-This.Width)/2)

		      lnLeft = IIF(liLeft>0,liLeft,MIN(MAX(This.Left - This.nprevx +nXCoord, -This.Width+loParent.Width),0))
		      lnTop = IIF(liTop>0,liTop,MIN(MAX(This.Top - This.nprevy +nYCoord, -This.Height+loParent.Height),0))
		      This.Move(lnLeft,lnTop)
		      This.nPrevX = nXCoord
		      This.nPrevY = nYCoord
		      loCNT.UpdateBars()
		      This._MouseMove=.F.
		   ENDIF
		ENDIF

		lnLeft=nXCoord-This.nPrevX
		lnTop=nYCoord-This.nPrevY

		   * Timer Event
		IF nButton=4 AND nShift=8
		   IF This.nPrevX # nXCoord OR This.nPrevY # nYCoord
		      This._MouseMove=.T.

		      liTop=IIF(loParent.Height<This.Height,0,(loParent.Height-This.Height)/2)
		      liLeft=IIF(loParent.Width<This.Width,0,(loParent.Width-This.Width)/2)

		      lnLeft = IIF(liLeft>0 OR lnLeft=0,liLeft,MIN(MAX(This.Left + ABS(INT(lnLeft/10))*IIF(lnLeft<0,+10,-10), -This.Width+loParent.Width),0))
		      lnTop = IIF(liTop>0 OR lnTop=0,liTop,MIN(MAX(This.Top + ABS(INT(lnTop/10))*IIF(lnTop<0,+10,-10), -This.Height+loParent.Height),0))

		      This.Move(lnLeft,lnTop)
		      loCNT.UpdateBars()
		      This._MouseMove=.F.
		   ENDIF
		ENDIF


		IF nButton=1 AND (This._Mode=_xfrx_Mode_SelectObjectes OR This._Mode=_xfrx_Mode_NewObject)
		   This._MouseMove=.T.
		   =IIF(!PEMSTATUS(This,"cntSelectOs",5),This.AddObject("cntSelectOs","xfrxSelectOs"),.T.)
		   loObj=This.cntSelectOs

		   liALeft=nXCoord-OBJTOCLIENT(This,2)
		   liATop=nYCoord-OBJTOCLIENT(This,1)

		   IF !loObj.Visible
		      loObj.Visible=.T.
		      loObj.Move(liALeft,liATop,1,1)
		      loObj._Left=loObj.Left
		      loObj._Top=loObj.Top

		      loObjX=IIF(TYPE("loObjX")="O",loObjX,This)
		      loObjX.SetMousePointer(lcPath+"xfrxhand2.cur")

		   ELSE
		      liLeft=IIF(lnLeft>0 AND liALeft<=loObj._Left OR lnLeft<0 AND liALeft<=loObj._Left,liALeft,loObj.Left)
		      liTop=IIF(lnTop>0 AND liATop<=loObj._Top OR lnTop<0 AND liATop<=loObj._Top,liATop,loObj.Top)
		      liWidth=ABS(liALeft-loObj._Left)
		      liHeight=ABS(liATop-loObj._Top)

		      loObj.Move(liLeft,liTop,liWidth,liHeight)

		      * Otestuj zda nov� poloha nebude mimo viditelnost.
		      * pokud ano, pak mus� posunout plochou

		      liALeft=This.Left
		      IF This.Width>loParent.Width
		         IF lnLeft>0 && do prava
		            IF liLeft+loObj.Width+lnLeft>loParent.Width+ABS(This.Left)
		               liALeft=liALeft-5
		               liALeft=IIF(This.Width+liALeft<loParent.Width,loParent.Width-This.Width,liALeft)
		            ENDIF
		         ELSE
		            * do leva
		            IF liLeft+lnLeft<ABS(This.Left)
		               liALeft=liALeft+5
		               liALeft=IIF(liALeft>0,0,liALeft)
		            ENDIF
		         ENDIF
		      ENDIF

		      liATop=This.Top
		      IF This.Height>loParent.Height
		         IF lnTop>0
		            IF liTop+loObj.Height+lnTop>loParent.Height+ABS(This.Top)
		               liATop=liATop-5
		               liATop=IIF(This.Height+liATop<loParent.Height,loParent.Height-This.Height,liATop)
		            ENDIF
		         ELSE
		            IF liTop+lnTop<ABS(This.Top)
		               liATop=liATop+5
		               liATop=IIF(liATop>0,0,liATop)
		            ENDIF
		         ENDIF
		      ENDIF

		      IF liALeft#This.Left OR liATop#This.Top
		         This.Move(liALeft,liATop)
		         loCnt.UpdateBars()
		      ENDIF


		   ENDIF
		   This.nPrevX = nXCoord
		   This.nPrevY = nYCoord
		   This._MouseMove=.F.

		ENDIF

		IF This._Mode=_xfrx_Mode_MoveObject
		   * Ud�lost MouseMove prob�h� nad n�jak�m vybran�m objektem
		   * Tud� mus�m v�echny objekty posunout nov�m sm�rem

		   * Matice aSelect(.iSelect) obsahuje seznam vybran�ch objekt�
		   * krom vlastn�ch objekt�, mus�m tak� posunout ohrani�uj�c� konteinery
		   This._MouseMove=.T.
		   loCnt.oDM.RepositionSelectedObjectBy(This,lnLeft,lnTop,.T.)

		   This.nPrevX = nXCoord
		   This.nPrevY = nYCoord
		   This._MouseMove=.F.

		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_ROR AND;
		   loObjX.Left+loObjX.Width+lnLeft<=This.Width

		   This._MouseMove=.T.
		   =IIF(nShift=2,loCnt.oDM.ResizeSelectedObjectByN(This,_xfrx_Mode_ROR,lnLeft),;
		        loCnt.oDM.ResizeObjectByN(loObjX,_xfrx_Mode_ROR,lnLeft))
		   This.nPrevX = nXCoord
		   This.nPrevY = nYCoord
		   This._MouseMove=.F.
		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_ROB AND;
		   loObjX.Top+loObjX.Height+lnTop<=This.Height

		   This._MouseMove=.T.
		   =IIF(nShift=2,loCnt.oDM.ResizeSelectedObjectByN(This,_xfrx_Mode_ROB,lnTop),;
		        loCnt.oDM.ResizeObjectByN(loObjX,_xfrx_Mode_ROB,lnTop))
		   This.nPrevX = nXCoord
		   This.nPrevY = nYCoord
		   This._MouseMove=.F.
		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_ROL AND;
		   loObjX.Left+lnLeft>=0

		   This._MouseMove=.T.
		   =IIF(nShift=2,loCnt.oDM.ResizeSelectedObjectByN(This,_xfrx_Mode_ROL,lnLeft),;
		        loCnt.oDM.ResizeObjectByN(loObjX,_xfrx_Mode_ROL,lnLeft))
		   This.nPrevX = nXCoord
		   This.nPrevY = nYCoord
		   This._MouseMove=.F.
		ENDIF

		IF nButton=1 AND This._Mode=_xfrx_Mode_ROT AND;
		   loObjX.Top+lnTop>=0

		   This._MouseMove=.T.
		   =IIF(nShift=2,loCnt.oDM.ResizeSelectedObjectByN(This,_xfrx_Mode_ROT,lnTop),;
		        loCnt.oDM.ResizeObjectByN(loObjX,_xfrx_Mode_ROT,lnTop))
		   
		   This.nPrevX = nXCoord
		   This.nPrevY = nYCoord
		   This._MouseMove=.F.
		ENDIF


		Thisform.LockScreen=llLS

		RETURN
	ENDPROC


	PROCEDURE DblClick
		IF TYPE("This.Parent")="O" AND !ISNULL(This.Parent)
		   This.Parent.DblClick()
		ENDIF
	ENDPROC


	PROCEDURE Click
		LPARAM loObj
		IF TYPE("This.Parent")="O" AND !ISNULL(This.Parent)
		   This.Parent.Click()
		ENDIF
	ENDPROC


	PROCEDURE MiddleClick
		IF TYPE("This.Parent")="O" AND !ISNULL(This.Parent)
		   This.Parent.MiddleClick()
		ENDIF
	ENDPROC


	PROCEDURE Moved
		this.Resize()
	ENDPROC


	PROCEDURE refreshtimer.Timer
		this.Interval = 0
		this.parent.Repaint()
	ENDPROC


ENDDEFINE
*
*-- EndDefine: zxfrxsheet
**************************************************


Define Class iCalProp As Custom
    icsRow = ''	&&Valore presente sul file ics, sempre di tipo stringa
    icsValue = ''
    icsType = ''	&&Tipologia propriet�
    Value = ''  	&&Valore tradotto, data, stringa, ecc...
    Value2 = ''		&&Valore tradotto, data, stringa, ecc...
    Add Object icsOptions As Collection	&&Opzioni propriet�: nome, valore

    Procedure Init(pValue)
    	pValue=IIF(VARTYPE(m.pValue)='L', "", m.pValue)
        This.Setup(m.pValue)
        This.SetValue()
    Endproc

    Function RetValue(xIcsValue, vType, xRetVal)
        Local l_result, l_p
        xIcsValue = m.xIcsValue
        Do Case
            Case m.vType=="DATE" Or m.vType=="DATE-TIME"
                Local l_value, l_data

                l_value = Left(m.xIcsValue, 15)
                l_p = At("T", m.l_value)
                If l_p<>0
                    l_data = Substr(m.l_value, 1, l_p-1)
                Else
                    l_data = m.l_value
                Endif
                *--- Data
                If Len(m.l_data)<8
                    l_year = Int(Val(Left(m.l_data, 2)))
                    l_month = Int(Val(Substr(m.l_data, 3, 2)))
                Else
                    l_year = Int(Val(Left(m.l_data, 4)))
                    l_month = Int(Val(Substr(m.l_data, 5, 2)))
                Endif
                l_day = Int(Val(Right(m.l_data, 2)))
                *--- Ora
                If l_p<>0
                    l_time = Substr(m.l_value, l_p+1, 6)
                    l_hour = Int(Val(Left(m.l_time, 2)))
                    l_min = Int(Val(Substr(m.l_time, 3, 2)))
                    l_sec = Int(Val(Right(m.l_time, 2)))
                    l_result = Datetime(m.l_year, m.l_month, m.l_day, m.l_hour, m.l_min, m.l_sec)
                ELSE
                   IF m.l_year>0 AND  m.l_month>0 AND  m.l_day>0
                     l_result = Date(m.l_year, m.l_month, m.l_day)
                   ELSE
                    l_result=cp_chartodate('  -  -    ')
                   endif
                Endif
                Do Case
                    Case Right(m.xIcsValue, 1)=="Z"  && 19980118T230000Z Zulu Time
                        *--- Devo aggiungere 1-2 ore
                        l_result = m.l_result + 3600
                        If OraLegale(m.l_result)
                            l_result = m.l_result + 3600
                        Endif
                        *!*					CASE This.icsOptions.GetKey("TZID")<>0
                        *!*						DO CASE
                        *!*							CASE This.icsOptions.Item("TZID")=="Europe/Rome"
                        *!*							CASE This.icsOptions.Item("TZID")=="America/New_York"
                        *!*						ENDCASE
                Endcase

            Case m.vType=="TEXT"
                l_result = m.xIcsValue
                l_result = Strtran(m.l_result, "\N", Chr(10))
                l_result = Strtran(m.l_result, "\n", Chr(10))
                l_result = Strtran(m.l_result, "\R", Chr(13))
                l_result = Strtran(m.l_result, "\r", Chr(13))

            Case m.vType=="PERIOD"
                Local l_dataStart, l_dataEnd, l_dEnd
                l_p = At("/", m.xIcsValue)
                If l_p<>0
                    l_dataStart = This.RetValue(Substr(m.xIcsValue,1,l_p-1), "DATE-TIME")
                    l_dEnd = Alltrim(Substr(m.xIcsValue,l_p+1))
                    If Left(l_dEnd, 1)='P'
                        l_dataEnd = l_dataStart + This.RetValue(l_dEnd, "DURATION")
                    Else
                        l_dataEnd = This.RetValue(l_dEnd, "DATE-TIME")
                    Endif
                    If Type("xRetVal")='C'
                        Dimension xRetVal(2)
                        m.xRetVal[1] = l_dataStart
                        m.xRetVal[2] = l_dataEnd
                    Endif
                Endif
                l_result = .T.
            Case m.vType=="INTEGER"
                l_result = Int(Val(m.xIcsValue))

            Case m.vType=="DURATION"	&&Ritorna il numero di sec da sommare al datetime
                Local l_i, l_concat, l_numSec
                l_concat = ""
                l_numSec = 0
                For l_i=1 To Len(m.xIcsValue)
                    l_c=Substr(m.xIcsValue, l_i, 1)
                    Do Case
                        Case l_c='P'
                            *--- Scarto P
                        Case l_c='W'
                            l_numSec = l_numSec + Int(Val(l_concat))*604800
                            l_concat = ""
                        Case l_c='D'
                            l_numSec = l_numSec + Int(Val(l_concat))*86400
                            l_concat = ""
                        Case l_c='H'
                            l_numSec = l_numSec + Int(Val(l_concat))*3600
                            l_concat = ""
                        Case l_c='M'
                            l_numSec = l_numSec + Int(Val(l_concat))*60
                            l_concat = ""
                        Case l_c='S'
                            l_numSec = l_numSec + Int(Val(l_concat))
                            l_concat = ""
                        Otherwise
                            l_concat = l_concat + l_c
                    Endcase
                Next
                l_result = l_numSec
        Endcase
        Return l_result
    Endfunc

    Function SetValue()
        Local l_i
        Do Case
            Case Inlist(This.icsType, "DTSTART", "DTEND", "DTSTAMP", "CREATED")
                This.Value = This.RetValue(This.icsValue, "DATE-TIME")
            Case Inlist(This.icsType, "PRODID", "VERSION", "DESCRIPTION", "LOCATION", "CATEGORIES", "SUMMARY", "BEGIN", "END")
                This.Value = This.RetValue(This.icsValue, "TEXT")
            Case Inlist(This.icsType, "PRIORITY")
                This.Value = This.RetValue(This.icsValue, "INTEGER")                
            Case Inlist(This.icsType, "FREEBUSY")
                Dimension aVal[2]
                aVal[1]=''
                If This.RetValue(This.icsValue, "PERIOD", @aVal)
                    This.Value = aVal[1]
                    This.Value2 = aVal[2]
                Endif
            Otherwise
                This.Value = This.RetValue(This.icsValue, "TEXT")
        Endcase
    Endfunc

    Procedure Setup(pValue)
        This.icsRow = m.pValue
        l_p = At(':', This.icsRow)
        If m.l_p<>0
            l_prop = Substr(This.icsRow, 1, m.l_p-1)
            This.icsValue = Substr(This.icsRow, m.l_p+1)
            If Occurs(';', m.l_prop)>0
                *--- Controllo presenza parametri aggiuntivi
                l_i=1
                l_p = At(';', m.l_prop)
                This.icsType = Upper(Substr(m.l_prop, 1, m.l_p-1))
                m.l_pp = At(';', m.l_prop, l_i+1)
                Do While m.l_pp<>0
                    l_par = Substr(m.l_prop, m.l_p+1, m.l_pp-m.l_p-1)
                    This.icsOptions.Add(Substr(l_par, At('=', l_par)+1), Upper(Substr(l_par, 1, At('=', l_par)-1)))
                    l_p = l_pp
                    l_i = l_i+1
                    l_pp = At(';', m.l_prop, l_i+1)
                Enddo
                l_par = Substr(m.l_prop, m.l_p+1)
                This.icsOptions.Add(Substr(l_par, At('=', l_par)+1), Upper(Substr(l_par, 1, At('=', l_par)-1)))
            Else
                This.icsType = Upper(Substr(m.l_prop, 1, m.l_p))
            Endif
        Else
            *--- Error
        Endif
    Endproc

    *--- Salva
    Function SaveProp
        This.icsValue = This.GetIcsValue()
        Return This._SaveProp()
    Endfunc

    *--- Salva
    Function _SaveProp
        Local l_result, l_i
        l_result = This.icsType
        For l_i=1 To This.icsOptions.Count
            l_result = l_result + ";" + This.icsOptions.GetKey(l_i) + "=" + This.icsOptions.Item(l_i)
        NEXT
        l_result = l_result + ":" + This.icsValue
        This.icsRow = l_result
        *--- "folding" technique
        Local l_text
        l_text = l_result
        l_result = ""
        Do While Len(l_text)>75
            l_result = l_result + Substr(m.l_text, 1, 75) + CRLF + Space(1)
            l_text = Substr(m.l_text, Min(76, Len(m.l_text)))
        Enddo
        l_result = l_result + l_text
        Return l_result
    Endfunc

    Function RetIcsValue(xValue, vType, xArr, bNoRemove)
        Local l_result, l_i
        Do Case
            Case m.vType=="DATE"
                l_result = Alltrim(Str(Year(m.xValue))) + Padl(Alltrim(Str(Month(m.xValue))),2,"0") + Padl(Alltrim(Str(Day(m.xValue))),2,'0')
                l_optIdx = This.icsOptions.GetKey("VALUE")
                If l_optIdx <> 0
                    This.icsOptions.Remove("VALUE")
                Endif
                This.icsOptions.Add("DATE", "VALUE")
            Case m.vType=="DATE-TIME"
                m.xValue = m.xValue - 3600
                If OraLegale(m.xValue)
                    m.xValue = m.xValue - 3600
                Endif
                l_result = Alltrim(Str(Year(m.xValue))) + Padl(Alltrim(Str(Month(m.xValue))),2,"0") + Padl(Alltrim(Str(Day(m.xValue))),2,'0')
                l_result = l_result + "T"+Padl(Alltrim(Str(Hour(m.xValue))),2,'0')+Padl(Alltrim(Str(Minute(m.xValue))),2,'0')+Padl(Alltrim(Str(Sec(m.xValue))),2,'0')+"Z"
                IF !bNoRemove
	                *--- Scrivo la data in Zulu Time quindi elimino le vecchie opzioni se necessario
	                *--- Al momento le tolgo tutte
	                For l_i=1 To This.icsOptions.Count
	                    l_key = This.icsOptions.Remove(l_i)
	                Next
				Endif
            Case m.vType=="TEXT"
                l_result = m.xValue
                l_result = Strtran(m.l_result, Chr(10), "\N")
                l_result = Strtran(m.l_result, Chr(13), "\R")
            Case m.vType=="PERIOD"
                l_result = This.RetIcsValue(xArr[1], "DATE-TIME", ,.T.) + "/" + This.RetIcsValue(xArr[2], "DATE-TIME", ,.T.)
            Case m.vType=="INTEGER"
                l_result = Alltrim(Str(m.xValue))
            Otherwise
                *--- Per gli elementi non gestiti ritorno il valore cos� com'�
                l_result = m.xValue
        Endcase
        Return l_result
    Endfunc

    Function GetIcsValue()
        Local l_result
        Do Case
            Case Inlist(This.icsType, "DTSTART", "DTEND", "DTSTAMP", "CREATED")
                If Vartype(This.Value) = 'D'
                    l_result = This.RetIcsValue(This.Value, "DATE")
                Else
                    l_result = This.RetIcsValue(This.Value, "DATE-TIME")
                Endif
            Case Inlist(This.icsType, "PRODID", "VERSION", "DESCRIPTION", "LOCATION", "CATEGORIES", "SUMMARY", "BEGIN", "END")
                l_result = This.RetIcsValue(This.Value, "TEXT")
            Case Inlist(This.icsType, "PRIORITY")
                l_result = This.RetIcsValue(This.Value, "INTEGER")                
            Case Inlist(This.icsType, "FREEBUSY")
                Dimension aVal[2]
                aVal[1] = This.Value
                aVal[2] = This.Value2
                l_result = This.RetIcsValue("", "PERIOD", @aVal)
            Case Inlist(This.icsType, "RRULE")
                l_result = This.RetIcsValue(This.Value, "RRULE")
            Otherwise
                l_result = This.RetIcsValue(This.Value, "TEXT")
        Endcase
        Return l_result
    Endfunc

    *--- Confrontatore propriet�
    Function Compare(vProp)
        IF TYPE('vProp.Value')='C'
           Return ALLTRIM(vProp.Value) == ALLTRIM(This.Value)
        ELSE
          Return vProp.Value == This.Value
        Endif
        
    Endfunc

Enddefine

Define Class iCalEvent As Collection
    UID = ""	&&Chiave Evento
    
	FUNCTION New(cUID)
		LOCAL l_prop, l_result
		l_result = IIF(VARTYPE(cUID)='C', cUID, SYS(2015))
		l_prop = Createobject("iCalProp", "UID:" + l_result)
		This.Add(m.l_prop, m.l_prop.icsType)		
		This.UID = l_result
    	Return l_result
	EndFunc
    
    Function SetInfo(tDtStart, tDtEnd, tDtStamp, cSummary, cDescription, cLocation, cFbType, cCategories, nPriority)
      LOCAL l_prop, l_result
      *--- DTSTART
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "DTSTART"
      l_prop.Value = m.tDtStart
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType)		
      *--- DTEND     
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "DTEND"
      l_prop.Value = m.tDtEnd
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType)	      
      *---      
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "DTSTAMP"
      l_prop.Value = m.tDtStamp
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType)	      
      *--- SUMMARY      
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "SUMMARY"
      l_prop.Value = m.cSummary
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType)   
      *--- DESCRIPTION      
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "DESCRIPTION"
      l_prop.Value = m.cDescription
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType)   
      *--- LOCATION      
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "LOCATION"
      l_prop.Value = m.cLocation
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType)  
      *--- CATEGORIES    
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "CATEGORIES"
      l_prop.Value = m.cCategories
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType)
      *--- PRIORITY     
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "PRIORITY"
      l_prop.Value = m.nPriority
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType) 
      *--- FREEBUSY
      l_prop = Createobject("iCalProp")
      l_prop.icsType = "FREEBUSY"
      l_prop.Value = m.tDtStart
      l_prop.Value2 = m.tDtEnd
      *--- 'FREE', 'BUSY-UNAVAILABLE', 'BUSY'
      l_prop.icsOptions.Add(m.cFbType, "FBTYPE")
      l_prop.SaveProp()
      This.Add(m.l_prop, m.l_prop.icsType)       
    EndFunc
    
    Function GetValueByProp(vType, xDefault)
        Local l_iCalProp, l_result
        If This.GetKey(m.vType)>0
            l_iCalProp = This.Item(m.vType)
            l_result = l_iCalProp.Value
	Else
	    l_result = m.xDefault
        Endif
        Return m.l_result
    Endfunc

    Function GetProp(vType)
        Local l_result
        If This.GetKey(m.vType)>0
            l_result = This.Item(m.vType)
        Endif
        Return m.l_result
    Endfunc

    Procedure AddiCalProp(icsValue)
        Local l_iCalProp
        l_iCalProp = Createobject("iCalProp", m.icsValue)
        This.Add(l_iCalProp, m.l_iCalProp.icsType)
    Endproc

    Function SaveEvent()
        Local l_result, l_obj
        l_result = "BEGIN:VEVENT" + CRLF
        For Each l_obj In This
            l_result = m.l_result + m.l_obj.SaveProp() + CRLF
        Next
        l_result = m.l_result + "END:VEVENT" + CRLF
        Return m.l_result
    Endfunc

    Function Compare(vEvent, aKeys)
    	LOCAL l_result
        For l_i=1 To Alen(m.aKeys)
            l_key1 = This.GetKey(m.aKeys[l_i])
            l_key2 = vEvent.GetKey(m.aKeys[l_i])
			IF m.l_key1==0 AND m.l_key2==0
				l_result = .T.
			ELSE
				IF l_key1<>0 AND l_key2<>0
				*--- Confronto le propriet�
					l_result = This.Item(m.aKeys[l_i]).Compare(vEvent.Item(m.aKeys[l_i]))
					IF ! l_result
					  * Disuguaglianza esco
					   RETURN m.l_result
					Endif
				ELSE
					l_result = .F.
				ENDIF 
			Endif
        NEXT
        RETURN m.l_result
    Endfunc

Enddefine

Define Class iCalCalendar As Custom
    cIcsFile = ""
    cIcsText = ""
    Add Object aProp As Collection
    Add Object aVEvent As Collection

	FUNCTION New(cProdID, cVersion)
		LOCAL l_prop
		*--- PRODID
		l_prop = Createobject("iCalProp", "PRODID:-//Zucchetti SPA//"+IIF(VARTYPE(m.cProdID)='C' And !EMPTY(m.cProdID), cProdID,ALLTRIM(i_MsgTitle))+"//IT")
		This.aProp.Add(m.l_prop, m.l_prop.icsType)
		*--- VERSION
		l_prop = Createobject("iCalProp", "VERSION:"+IIF(VARTYPE(m.cVersion)='C' And !EMPTY(m.cVersion), m.cVersion, "2.0"))
		This.aProp.Add(m.l_prop, m.l_prop.icsType)
		*--- UTF
		l_prop = Createobject("iCalProp", "CONTENT-TYPE:text/calendar;charset=UTF-8")
		This.aProp.Add(m.l_prop, m.l_prop.icsType)
	EndFunc

    *--- Carica calendario ics da file
    Func LoadIcsFile(cFile)
        If cp_fileExist(m.cFile)
            Return This.LoadIcsText(Filetostr(m.cFile))
        Else
            Return .F.
        Endif
    Endfunc

    *--- Carica calendario ics da testo
    Func LoadIcsText(cText)
        Local l_numLines, l_i, l_obj, l_bEvent, l_objEvent, l_bSkip,l_bNoAdd
        l_bSkip = .F.
        l_bEvent = .F.	&&Quando a true sono dentro ad un evento
        l_bNoAdd=.F.    &&Quando a true sono dentro ad una parte da escludere
        *--- Sostituisco CR+LF+SPACE(1) con "" in questo modo ripristino lo split con "folding" technique
        cText = Strtran(m.cText, Chr(13)+Chr(10)+Space(1), "")
	*--- Sostituisco CR+LF+TAB con ""
	cText = Strtran(m.cText, Chr(13)+Chr(10)+Chr(9), "")
        l_numLines = Alines(aCalLines, m.cText)
        For l_i = 1 To m.l_numLines
            l_obj = Createobject("iCalProp", m.aCalLines[l_i])
            Do Case
                Case m.l_obj.icsType = "BEGIN"
                    Do Case
                        Case m.l_obj.Value = "VEVENT"
                            l_bEvent = .T.
                            l_objEvent = Createobject("iCalEvent")
                        Case m.l_obj.Value = "VCALENDAR"
                            l_bSkip = .F.
                        Case m.l_obj.Value = "VALARM"
                            l_bNoAdd=.t.
                        Otherwise
                            l_bSkip = .T.	&&Salto tutte le entit� non riconosciute
                    Endcase
                Case m.l_obj.icsType = "END"
                    Do Case
                        Case m.l_obj.Value = "VEVENT"
                            l_bEvent = .F.
                            If This.aVEvent.GetKey(m.l_objEvent.Item("UID").Value)=0
                            	m.l_objEvent.UID = m.l_objEvent.Item("UID").Value
                                This.aVEvent.Add(m.l_objEvent, m.l_objEvent.UID)
                            Else
                                *--- Scarto l'evento perch� gi� presente ?!?
                            ENDIF
                        Case m.l_obj.Value = "VALARM"
                            l_bNoAdd = .F.
                        Otherwise
                            l_bSkip = .F.
                    Endcase
                OTHERWISE
                    If !l_bNoAdd
                       If m.l_bEvent
                           If l_objEvent.GetKey(m.l_obj.icsType)=0
                              l_objEvent.Add(m.l_obj, m.l_obj.icsType)
                           Endif
                       Else
                           If !m.l_bSkip and This.aProp.GetKey(m.l_obj.icsType)=0
                              This.aProp.Add(m.l_obj, m.l_obj.icsType)
                           Endif
                      Endif
		    Endif
            Endcase
        Next
        Return .T.
    Endfunc

    *--- Salva il calendario su file
    Function SaveIcsFile(cFile)
        Local l_cText
        l_cText = This.SaveIcsText()
        If !Empty(m.l_cText)
            Strtofile(m.l_cText, m.cFile)
        Endif
    EndFunc

    *--- Restituisce il calendario in formato ics
    Function SaveIcsText
        Local l_result, l_obj
        l_result = "BEGIN:VCALENDAR" + CRLF
        For Each l_obj In This.aProp
            l_result = l_result + l_obj.SaveProp() + CRLF
        Next
        For Each l_obj In This.aVEvent
            l_result = l_result + l_obj.SaveEvent()
        Next
        l_result = l_result + "END:VCALENDAR" + CRLF
        Return l_result
    EndFunc

    Function Compare(vCal, cListPropToCompare, cRetCursor)
    	LOCAL l_vEvent, l_vEvent2, l_UID
    	cRetCursor = IIF(VARTYPE(m.cRetCursor)='C', m.cRetCursor, SYS(2015))
    	CREATE CURSOR (m.cRetCursor) (vEventUID C(150), vEventStatus C(1))
    	ALINES(aKeys, m.cListPropToCompare, 1, ",")
    	*--- Le propriet� del calendario non le confronto, verifico solo se ci sono eventi variati
    	FOR EACH l_vEvent IN This.aVEvent
	    	l_status = "R"
    		FOR EACH l_vEvent2 IN m.vCal.aVEvent
    			IF m.l_vEvent2.UID == m.l_vEvent.UID
    				*--- Verifico se variato
    				l_status = IIF(m.l_vEvent.Compare(m.l_vEvent2, @aKeys), "","M")
    				Exit
    			ENDIF 
	    	NEXT
	    	INSERT INTO (m.cRetCursor) (vEventUID, vEventStatus) VALUES (m.l_vEvent.UID, m.l_status)
    	NEXT
    	FOR EACH l_vEvent IN vCal.aVEvent
    		SELECT (m.cRetCursor)
    		LOCATE FOR ALLTRIM(vEventUID) == ALLTRIM(m.l_vEvent.UID)
    		IF !FOUND()
    			INSERT INTO (m.cRetCursor) (vEventUID, vEventStatus) VALUES (m.l_vEvent.UID, "A")
    		EndIf
    	NEXT
    	RETURN m.cRetCursor
	EndFunc
Enddefine

* Crea un'immagine per eseguire un programma al click
Define Class ah_ClickImage As Image
	cColorPalette = ""           && Colori di stato (normale,over,disabilitato,selezionato) da passare alla funzione cp_TmpColorizeImg() separati da ";"
	nOverImage = -1              && -1 = bmp non definito, 0  non sopra, 1 = sopra
	cPathImage = ""              && Percorso Immagine
	bmpImage = ""
	cPathOverImage = ""          && Percorso Immagine quando il mouse � sopra
	bmpOverImage = ""
	cEnabled = ""                && Condizione di Enable
	cHided= ""                   && Condizione di Hide
	bSelected = .F.              && Oggetto selezionato (click)
	cPathSelectedImage = ""      && Percorso Immagine oggetto selezionato (click)
	bmpSelectedImage = ""
	cPathDisabledImage = ""      && Percorso Immagine disabilitata
	bmpDisabledImage = ""
	cClickCmd = ""               && Programma da eseguire
	cTitle = ""                  && Titolo da visualizzare sul controllo (opzionale)
	cOldTitle = ""
	cHelpControl = ""            && Nome del controllo per visualizzare il titolo
	nHelpControl = 0
	oHelpControl = .Null.
	bHelpControl = .F.
	MousePointer = 15            && Puntatore mouse ditino

	Procedure Init
		Local cOnError,ncolor
		If Not Empty(This.cColorPalette)
			cOnError = On("ERROR")
			On Error ncolor=-1
			*** Immagine normale
			ncolor = Evaluate(Getwordnum(This.cColorPalette,1,";"))
			If ncolor>=0
				This.bmpImage = Filetostr(cp_TmpColorizeImg(This.cPathImage,ncolor))
			Endif
			*** Immagine quando il mouse � sopra
			ncolor = Evaluate(Getwordnum(This.cColorPalette,2,";"))
			If ncolor>=0
				This.nOverImage  = 0
				This.bmpOverImage = Filetostr(cp_TmpColorizeImg(This.cPathImage,ncolor))
			Endif
			*** Immagine disabilitata
			ncolor = Evaluate(Getwordnum(This.cColorPalette,3,";"))
			If ncolor>=0
				This.bmpDisabledImage = Filetostr(cp_TmpColorizeImg(This.cPathImage,ncolor))
			Endif
			*** Immagine oggetto selezionato (click)
			ncolor = Evaluate(Getwordnum(This.cColorPalette,4,";"))
			If ncolor>=0
				This.bmpSelectedImage = Filetostr(cp_TmpColorizeImg(This.cPathImage,ncolor))
			Endif
			On Error &cOnError
		Else
			This.bmpImage = Filetostr(This.cPathImage)
			If cp_fileexist(This.cPathOverImage)
				This.nOverImage  = 0
				This.bmpOverImage = Filetostr(This.cPathOverImage)
			Endif
			If cp_fileexist(This.cPathDisabledImage)
				This.bmpDisabledImage = Filetostr(This.cPathDisabledImage)
			Endif
			If cp_fileexist(This.cPathSelectedImage)
				This.bmpSelectedImage = Filetostr(This.cPathSelectedImage)
			Endif
		Endif
		This.PictureVal = This.bmpImage
		This.cEnabled = STRTRAN(This.cEnabled,"w_",".w_")
		This.cHided = STRTRAN(This.cHided,"w_",".w_")
	Endproc

	Procedure SetPictureVal()
		If This.bSelected
			This.PictureVal = This.bmpSelectedImage
		Else
			If This.Enabled
				If This.nOverImage =1
					This.PictureVal = This.bmpOverImage
				Else
					This.PictureVal = This.bmpImage
				Endif
			Else
				This.PictureVal = EVL(This.bmpDisabledImage,This.bmpImage)
			Endif
		Endif
	Endproc
	
	Procedure SetHelpControl(cTitle)
		If This.nHelpControl = 1
			This.bHelpControl = .T.
			This.oHelpControl.Value = cTitle
			This.oHelpControl.Visible = Not Empty(cTitle)
			*** questo assegnamento server per impedire che la SetControlValue ripriastini il vecchio valore
			This.Parent.oContained.AddProperty(This.cHelpControl,cTitle)
		Endif
	Endproc

	Procedure Calculate(cValue)
		* Aggiorno il titolo e lo stato enable
		This.cTitle = cValue
		If Not Empty(This.cEnabled)
			With This.Parent.oContained
				This.Enabled = Eval(This.cEnabled)
			Endwith
		Endif
		This.SetPictureVal()
		If Not Empty(This.cHided)
			With This.Parent.oContained
				This.Visible = Not Eval(This.cHided)
			Endwith
		Endif
	Endproc

	Procedure Event(cEvent)
	Endproc

	* all'entrata del mouse cambio l'immagine
	Procedure MouseEnter
		Lparameters nButton, nShift, nXCoord, nYCoord
		This.nOverImage  = IIF(This.nOverImage =0, 1, This.nOverImage )
		This.SetPictureVal()
		If This.nHelpControl = 0
			If Not Empty(This.cHelpControl) And Not Empty(This.cTitle) And Pemstatus(This.Parent.oContained,This.cHelpControl,5)
				This.nHelpControl = 1
				octrl = This.Parent.oContained.getctrl(This.cHelpControl)
				If Vartype(octrl)='O'
					This.oHelpControl = octrl
				Else
					This.nHelpControl = -1
				Endif
			Else
				This.nHelpControl = -1
			Endif
		Endif
		This.SetHelpControl(This.cTitle)
	Endproc

	* all'uscita del mouse ripristino l'immagine originale
	Procedure MouseLeave
		Lparameters nButton, nShift, nXCoord, nYCoord
		If Not This.bSelected
			This.nOverImage  = IIF(This.nOverImage =1, 0, This.nOverImage)
			This.SetPictureVal()
			If This.bHelpControl
				This.SetHelpControl(This.cOldTitle)
				This.bHelpControl = .F.
			Endif
		Endif
	Endproc

	Procedure Click()
		Local cCommand,pospar,iobj,jobj,i_ctrl,cEvent,cName
		If Not Empty(This.cClickCmd)
			pospar = At("(", This.cClickCmd)
			If Upper(This.cClickCmd)="SELECT"
				* Se non � definita l'immagine non posso selezionare l'oggetto
				If Not Empty(This.bmpSelectedImage)
					This.bSelected = Not This.bSelected
					If pospar>0
						* deseleziono gli altri oggetti
						numobj = Alines(aobj, Strextract(This.cClickCmd,"(",")"), 1, ",")
						For iobj=1 To numobj
							cName = Alltrim(aobj[iobj])
							i_ctrl = This.Parent.&cName
							If i_ctrl.class=this.Class
								i_ctrl.cOldTitle = IIF(This.bSelected, This.cTitle, "")
								If This<>i_ctrl
									i_ctrl.bSelected = .F.
									i_ctrl.MouseLeave()
								Endif
							Endif
						Next
					Endif
					This.SetPictureVal()
					This.SetHelpControl(This.cTitle)
					cEvent = Upper(This.Name) + IIF(This.bSelected, " Selected", " Unselected")
					This.parent.oContained.NotifyEvent(cEvent)
					This.parent.oContained.mEnableControls()
				Endif
			Else
				If pospar>0
					cCommand = Left(This.cClickCmd,pospar-1) + "(this.Parent.oContained," + Substr(This.cClickCmd,pospar+1)
				Else
					cCommand = Alltrim(This.cClickCmd)+"(this.Parent.oContained)"
				Endif
				With This.Parent.oContained
					&cCommand
				Endwith
				If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cfunction,"Edit","Load")
					This.Parent.oContained.mCalc(.T.)
				Endif
			Endif
		Endif
	Endproc

Enddefine

*--- Zucchetti Aulla Inizio - Servizio per esecuzione in background
Define Class KPITimer As Timer
    *--- Intervallo minuti (default 30 minuti)
    Interval=30*60000
    
    *--- Controlla che la connessione al servizio sia attivo (altrimenti si disabilita) e lancia un'esecuzione massiva
    Proc Timer()
    	If Vartype(bEnableAHService)='L' And bEnableAHService And Vartype(i_DDEchannel)='N' And i_DDEchannel<>-1
    		Local l_err
    		m.l_err = ah_taskexec('GSUT_BEE')
    		If m.l_err<>0
    			cp_msg(cp_Translate("Esecuzione KPI in background fallita"))
    		Endif
    	Else
    		this.Enabled = .f.
    	Endif
    Endproc

	Procedure Interval_Assign(xValue)
		If Vartype(m.xValue)='N'
			If m.xValue>0
				This.Interval = m.xValue*60000
			Else
				This.Interval = 30*60000
			Endif
		Endif
	Endproc
Enddefine
*--- Zucchetti Aulla Fine - Servizio per esecuzione in background

* --- Zucchetti Aulla Inizio - Creazione immagine 
PROCEDURE ah_drawstring( PFileOrig,PCursor,PFileDest )
** Drawstring crea un file di immagine partendo da un file immagine originario e scrivendoci sopra delle stringhe
** La funzione si aspetta come 2� �parametro il nome di un cursore su contenenete le informazioni necessarie per scrivere sul file (stringa da scrivere,posizione x e y,font, larghezza altezza area scrittura, dimensione, Flag per tipologia font bold/corsivo, colore ))

** La funzione riceve come Parametri : 
** PFileOrig = File dell immagine originale da modificare
** PCursor = nome del cursore contenente le stringhe da scrivere con el risppettive informazioni di formattazione (font,dimensione , posizione,...)
** PFileDest = File di destinazione da scrivere

** La funzione restituisce una stringa vuoto nel caso in cui non si siano verificati errori altrimenti restituisce un messaggio di errore
** Nel caso in cui l elaborazione vada a buon fine allora l immagine modificata viene salvata con posizione/nnome file indicati dal aprametro PFileDest 

** Il cursore cotnenente i dati da scrivere deve avere il seguente formato : 
** TESTO char, create cursor DATI (CAMPO char (100),TESTO M, CFONTNAME char(250), FONTSTYLE I, SIZEFONT I,ALLINEAM I, COLORE I, POSX I, POSY I , XLEN I, YLEN I)

DECLARE LONG    GdipDrawString IN GDIPLUS.DLL Long graphics, String str, Long length, Long thefont, String @layoutRect, Long StringFormat, Long brush 
DECLARE INTEGER GdipCreateStringFormat IN GdiPlus.dll INTEGER formatAttributes, INTEGER language, INTEGER @nFormat 
DECLARE INTEGER GdipDeleteBrush IN GdiPlus.dll INTEGER brush 
DECLARE INTEGER GdipCreateFont IN GdiPlus.dll INTEGER fontFamily, SINGLE emSize,INTEGER fntstyle, INTEGER unit, INTEGER @fnt 
DECLARE INTEGER GdipDeleteFont IN GdiPlus.dll INTEGER fnt 
DECLARE INTEGER GdipCreateFontFamilyFromName IN GdiPlus.dll STRING familyname, INTEGER FontCollection, INTEGER @FontFamily 
DECLARE INTEGER GdipDeleteFontFamily IN GdiPlus.dll INTEGER FontFamily 
DECLARE INTEGER GdipDeleteGraphics IN GdiPlus.dll INTEGER graphics  
Declare LONG    MultiByteToWideChar In kernel32 Long iCodePage, Long dwFlags, String @ lpStr, Long iMultiByte, String @ lpWideStr, Long iWideChar 
DECLARE INTEGER GdipLoadImageFromFile IN gdiplus.DLL STRING wFilename, INTEGER @nImage 
DECLARE INTEGER GdipSaveImageToFile IN gdiplus.DLL INTEGER nImage, STRING wFilename, STRING qEncoder, INTEGER nEncoderParamsPtr      
DECLARE INTEGER GdipGetImageGraphicsContext IN gdiplus.DLL  INTEGER nImage, INTEGER @ nGraphics 
DECLARE INTEGER GdipCreateHatchBrush IN Gdiplus.dll INTEGER hatchstyle, INTEGER forecol, INTEGER backcol, INTEGER @brush 
Declare Integer GdipDeleteGraphics In GDIPLUS.DLL Integer graphics
DECLARE INTEGER GdipDeleteBrush IN gdiplus.dll INTEGER brush
DECLARE INTEGER GdipDeleteStringFormat IN gdiplus.dll INTEGER fmt 
Declare LONG GdipDisposeImage In GDIPlus.Dll Long Bitmap
DECLARE LONG GdipSetStringFormatAlign IN GDIPlus.Dll INTEGER StringFormat, integer nAlign
DECLARE LONG GdipFlush IN GDIPLUS LONG graphics, LONG intention
DECLARE INTEGER DeleteObject IN gdi32 INTEGER hObject
Declare Long GdiplusStartup in GdiPlus.dll Long @ token, String @ input, Long @ output
Declare Long GdiplusShutdown in GdiPlus.dll Long token


**Controlla l estensione dei file di destinazione da creare 
LOCAL estensione, Errore,status
m.errore=''
m.estensione=UPPER(ALLTRIM(JUSTEXT(PFileDest)))
m.lcEncoder=0h01F47C55041AD3119A730000F81EF32E 

*** Controllo disponibilit� delle librerie grafiche 
 m.gdiplusStartupInput = Chr(1) + Replicate(Chr(0), 15)
 * Initialize GDI+.
 m.lcToken = 0
 m.status=GdiplusStartup(@lcToken, @m.gdiplusStartupInput, 0) 
 IF m.status!=0
 cp_ErrorHandle (m.status, "Errore nell avvio della libreria GDI  : GdiplusStartup code  " ,@m.Errore )
 RETURN m.Errore                        
ENDIF

** Se l estensione non � tra quelle riconosciute allora viene generato un file PNG
Do Case
		Case m.estensione='BMP'
	       m.lcEncoder=0h00F47C55041AD3119A730000F81EF32E
        Case m.estensione='JPG'
           m.lcEncoder=0h01F47C55041AD3119A730000F81EF32E
      	Case m.estensione='GIF'
      	    m.lcEncoder=0h02F47C55041AD3119A730000F81EF32E
        Case m.estensione='TIFF'
            m.lcEncoder=0h05F47C55041AD3119A730000F81EF32E
        OTHERWISE
          *se non si riconosce il formato del file di destinazione crea un file PNG
          FORCEEXT(m.PFileDest, 'PNG')
          m.lcEncoder=0h06F47C55041AD3119A730000F81EF32E
Endcase

**Carica l immagine oringinale in memoria
m.graphics=0 
m.nimage=0  
m.status=0
m.status=GdipLoadImageFromFile(STRCONV(m.PFileOrig+ CHR(0),5),@m.nimage) 
cp_ErrorHandle (m.status,"Errore nel caricamento immagine : GdipLoadImageFromFile error code  " ,@m.Errore )
m.curFont=' '
m.OldFont=' '
m.oldcolor=0
LOCAL oldcurs
m.oldcurs=ALIAS()

**Scandisce il cursore per scrivere le stringhe nell immagine
SELECT (m.PCursor)
GO TOP
SCAN

** Testo da scrivere
m.lcText=ALLTRIM(TESTO)
m.lnLen = 2 * (Len(m.lcText) + 1)
m.lcWideStr = Replicate(Chr(0), m.lnLen) 
MultiByteToWideChar(0,0,@m.lcText,Len(m.lcText), @m.lcWideStr,m.lnLen) 
m.tcText=m.lcWideStr 

** Legge le informazioni del Font da cursore e prepara gli oggetti Font e FontFamily
m.lcFontName=ALLTRIM(CFONTNAME)
m.lcFontName=STRCONV(m.lcFontName+CHR(0),5) 
m.lnFontStyle=FONTSTYLE
m.tnSize=SIZEFONT
m.thAlignment=ALLINEAM 
m.curFont=CFONTNAME + '-' + ALLTRIM(STR(FONTSTYLE))+ '-'+ ALLTRIM(STR(SIZEFONT)) + '-' + ALLTRIM(STR(ALLINEAM))

** Se cambia il font bisogna distruggere il vecchio Font 
IF m.curFont<>m.OldFont AND NOT EMPTY(m.OldFont)
	IF m.lnFont<>0
		m.status=GdipDeleteFont(m.lnFont)
		cp_ErrorHandle (m.status,"Errore nella distruzione dell oggetto Font : GdipDeleteFont code  " ,@m.Errore )
	ENDIF
	IF m.lnFontFamily<>0
		m.status=GdipDeleteFontFamily(m.lnFontFamily) 
		cp_ErrorHandle (m.status,"Errore nella distruzione dell oggetto FontFamily : GdipDeleteFontFamily code  " ,@m.Errore )
	ENDIF
ENDIF

** Se cambia il colore distruggo il vecchio pennelllo
IF m.oldcolor<>COLORE AND NOT EMPTY(m.OldFont)
 	IF m.brush<>0
		m.status=GdipDeleteBrush (m.brush)
		cp_ErrorHandle (m.status,"Errore nella distruzione dell oggetto Brush : GdipDeleteBrush code  " ,@m.Errore )
	ENDIF
ENDIF

*Se il font non esiste bisogna costruirlo (bisogna ricostruire il font anche nel caso in cui il vecchio Font sia di tip diverso)
IF m.curFont<>m.OldFont 
	m.lnFontFamily=0 
	m.lnFont=0 
	m.lnFormatHandle = 0 
	m.status=GdipCreateFontFamilyFromName(m.lcFontName,0,@m.lnFontFamily)
	cp_ErrorHandle (m.status,"Errore nella creazione del FontFamily : GdipCreateFontFamilyFromName error code  " ,@m.Errore )
	m.status=GdipCreateFont(m.lnFontFamily,m.tnSize,m.lnFontStyle, 3,@m.lnFont)
	cp_ErrorHandle (m.status,"Errore nella creazione del Font : GdipCreateFont error code  " ,@m.Errore )
	m.status=GdipCreateStringFormat(0, 0, @m.lnFormatHandle )
	cp_ErrorHandle (m.status,"Errore nella creazione del Formato Font  : GdipCreateStringFormat error code  " ,@m.Errore )
	m.status=GdipSetStringFormatAlign(lnFormatHandle, m.thAlignment) 	
	cp_ErrorHandle (m.status,"Errore nella creazione del Formato Font  : GdipSetStringFormatAlign error code  " ,@m.Errore )
ENDIF

** Prepara l oggetto pennello per disegnare il testo sull immagine
IF m.oldcolor<>COLORE OR EMPTY(m.OldFont)
  	m.brush=0 
	m.ucolor1=COLORE 
	m.aphpi=255
	m.lnColor1 = CTOBIN(CHR(m.aphpi)+LEFT(BINTOC(m.uColor1,'4rs'),3),'4s') 
	m.ucolor2=COLORE 
	m.aphpi=255
	m.lnColor2 = CTOBIN(CHR(m.aphpi)+LEFT(BINTOC(m.uColor2,'4rs'),3),'4s') 
	m.status=GdipCreateHatchBrush(11,m.lnColor1,m.lnColor2,@m.brush)
	cp_ErrorHandle (m.status,"Errore nella creazione dell oggetto Brush : GdipCreateHatchBrush error code  " ,@m.Errore )
ENDIF

** Leggo dal cursore la posizione su cui scrivere il testo e la dimensione del rettangolo di scrittura (il rettangolo di scrittura sserve per determinare l allineamento)
m.x=POSX
m.y=POSY
m.w=XLEN
m.h=YLEN
*GdipGetImageWidth(nImage,@w) 
*GdipGetImageHeight(nImage,@h) 
m.lcRectangleF=BINTOC(m.x,'F')+BINTOC(m.y,'F')+BINTOC(m.w,'F')+BINTOC(m.h,'F') 

IF EMPTY(m.OldFont)
	m.status=m.GdipGetImageGraphicsContext (m.nImage, @m.Graphics)
	cp_ErrorHandle (m.status,"Errore nella creazione dell oggetto Graphics : GdipGetImageGraphicsContext error code  " ,@m.Errore )
ENDIF 

m.status=GdipDrawString(m.Graphics, m.tcText, LEN(m.lcText), m.lnFont,@m.lcRectangleF, m.lnFormatHandle, m.brush)
cp_ErrorHandle (m.status,"Errore nella scrittura del testo nell immagine : GdipDrawString error code  " ,@m.Errore ) 

** al cambio del font/dimensione/tipo bisogna ricostruire l oggetto Font
m.OldFont=CFONTNAME + '-' + ALLTRIM(STR(FONTSTYLE))+ '-'+ ALLTRIM(STR(SIZEFONT)) + '-' + ALLTRIM(STR(ALLINEAM))
m.oldcolor=COLORE
**Se c � stato un errore termina l elaborazione e restituisce il codice di errore 
IF NOT EMPTY(errore)
	EXIT
ENDIF
ENDSCAN

GdipDeleteStringFormat(m.lnFormatHandle)
GdipDeleteGraphics(m.Graphics)

** Salvataggio su disco dell immagine modificata
m.status=GdipSaveImageToFile (m.nImage, STRCONV(m.PFileDest,5) + CHR(0), m.lcEncoder, 0)
cp_ErrorHandle (m.status,"Errore nella scrittura del file immagine : GdipSaveImageToFile error code  " ,@m.Errore )


** Distrugge gli oggetti usati per creare l immagine e riposiziona il cursore
IF NOT EMPTY (m.oldcurs)
	SELECT (m.oldcurs)
ENDIF

IF m.lnFont <>0
	m.status=GdipDeleteFont(lnFont)
	cp_ErrorHandle (m.status,"Errore nella distruzione dell oggetto Font : : GdipDeleteFont code  " ,@m.Errore )	
ENDIF
IF m.lnFontFamily<>0
	m.status=GdipDeleteFontFamily(m.lnFontFamily) 
	cp_ErrorHandle (m.status,"Errore nella distruzione dell oggetto FontFamily : GdipDeleteFontFamily code %1" ,@m.Errore )
ENDIF
IF m.brush<>0
	m.status=GdipDeleteBrush (m.brush)
	cp_ErrorHandle (m.status, "Errore nella distruzione dell oggetto Brush : GdipDeleteBrush code  " ,@m.Errore )
ENDIF
*GdipDeleteStringFormat (lnFormatHandle) 

IF m.nimage<>0
	m.status=GdipDisposeImage(m.nimage)
	cp_ErrorHandle (m.status, "Errore nella distruzione dell oggetto immagine : GdipDisposeImage code  " ,@m.Errore )
ENDIF

** Rilascia la libreria GDI
m.status=GdiplusShutdown(lcToken)
	
ACTIVATE SCREEN
RETURN m.Errore                        
ENDPROC
** Gestione errori delle funzioni grafiche                            
PROCEDURE cp_ErrorHandle 
Lparam stato , messaggio ,Errore  
	IF m.stato <>0 AND EMPTY (m.Errore)
		m.Errore=m.messaggio + ALLTRIM(STR(m.stato ))
	ENDIF 
ENDPROC
* --- Zucchetti Aulla Fine - Creazione immagine

*--- Zucchetti Aulla Inizio - iRevolution
*--- Classe per gestione filtri query profilo, tabelle ZR, Log ZR
Define Class RevInfSyncEntity As Custom
    cAction = "" && Azione
    cZRTab  = "" && Tabella ZR
    nConn   = 0  && Connessione
    nUser	= 0  && Codice utente
    
    w_SOTABGES = ""	&&Nome gestione

    ADD OBJECT aParamName as Collection

	Proc SetAction(pAction)
	    This.cAction = m.pAction
	Endproc

	Proc SetZRTab(pZRTab)
	    This.cZRTab = Alltrim(m.pZRTab)
	Endproc

    *--- Aggiunge parametri per query profilo da stringa chiave=valore
    Proc SetParamVQR(pKeys, pSeparator, pEvaluate)
        If !Empty(pKeys)
            This.SetKeys(Alltrim(m.pKeys), m.pSeparator, .F., pEvaluate)
        Endif
    Endproc

    *--- Aggiunge chiave ZR da stringa chiave=valore
    Proc SetZRKey(pKeys, pSeparator)
        If !Empty(pKeys)
            This.SetKeys(Alltrim(m.pKeys), m.pSeparator, .T., .T.)
        Endif
    Endproc

    Proc SetKeys(pKeys, pSeparator, pZR, pEvaluate, pForceUpdate)
        Local nCount, cPkValue, nCountKey, cKey, xValue, oRegExp
        *--- Classe per utilizzo espressioni regolari
        If Not("_REGEXP"$Upper(Set("Classlib")))
			Set Classlib To '_REGEXP.VCX' Additive
		Endif
        m.oRegExp = CreateObject("_REGEXP")
        *--- Normalizzazione parametri
        pSeparator = Evl(m.pSeparator, ";")
        *--- Espressione regoalre per separare correttamente le coppie chiave=valore
        If m.pEvaluate
	        TEXT TO m.oRegExp.Pattern TEXTMERGE NOSHOW
([^<<m.pSeparator>>]*?=\s*?)(('([^']|'')*?')|([0-9\.]*)|(".*?"))((\s*<<m.pSeparator>>)|(\s*$))
	        ENDTEXT 
	    ELSE
	        TEXT TO m.oRegExp.Pattern TEXTMERGE NOSHOW
([^<<m.pSeparator>>]*?=\s*?)([^;]*?)((\s*<<m.pSeparator>>)|(\s*$))
	        ENDTEXT 	    	
	    EndIF
       && m.oRegExp.Pattern=Textmerge("([^<<m.pSeparator>>]*?=\s*?'([^']|'')*?'\s*<<m.pSeparator>>)|([^<<m.pSeparator>>]*?=\s*?'([^']|'')*?'\s*$)|([^<<m.pSeparator>>]*?=\s*?[0-9\.]*\s*<<m.pSeparator>>)|([^<<m.pSeparator>>]*?=\s*?[0-9\.]*\s*$)|")+;
       && 				  Textmerge('([^<<m.pSeparator>>]*?=\s*".*?"\s*<<m.pSeparator>>)|([^<<m.pSeparator>>]*?=\s*?".*?"\s*$)') 
        nCount = 1
        nCountKey = m.oRegExp.Execute(m.pKeys)
        *--- Usa una variabile local per poter fare la &
        Do While m.nCount <= m.nCountKey
            *--- coppia chiave=valore
            cKey = Alltrim(m.oRegExp.Matches[m.nCount,2])
            *--- Tolgo il separatore che viene agganciato dall'espressione regolare
            cKey = Iif(Right(m.cKey,1)=m.pSeparator, Left(m.cKey,Len(m.cKey)-1), m.cKey)
            *--- Recupero il valore
            cPkValue = Substr(m.cKey, At("=", m.cKey)+1)
            IF m.pEvaluate
	            m.cPkValue = Strtran(m.cPkValue, [''], [��])
	            xValue = &cPkValue
				IF TYPE("xValue")='C'
					xValue = Strtran(m.xValue, [��], [''])
				ENDIF 
	        ELSE
		        xValue = m.cPkValue
	        ENDIF 
            If m.pZR
                *--- Aggiungo i parametri
                This.AddZRKey(Getwordnum(m.cKey, 1, "="), Iif(Type("m.xValue")$'CM', Strtran(m.xValue, "''", "'"), m.xValue), m.pForceUpdate)
            Else
				If m.pEvaluate
                *--- Aggiungo i parametri senza w_
    	        	cParam = Substr(Getwordnum(m.cKey, 1, "="), 3) 
				Else
					*--- Caso richiesta dati, lascio il parametro cos� 
					cParam = Getwordnum(m.cKey, 1, "=")
				EndIf
				This.AddParam(m.cParam, Iif(Type("m.xValue")$'CM', Strtran(m.xValue, "''", "'"), m.xValue))
            Endif
            nCount = m.nCount + 1
        Enddo
    Endproc

    *--- Aggiunge parametri per filtro profilo recuperando i valori da un oggetto (cKeySelect)
    Proc SetKeysFromObj(pKeys, pObj, pSeparator, cPrefix)
        *--- Normalizzazione parametri
        pSeparator = Evl(m.pSeparator, ",")
        cPrefix = Iif(!m.cPrefix, "w_", m.cPrefix)
        Local nCount, nCountKey, cKey, cVar
        nCount = 1
        nCountKey = Getwordcount(m.pKeys, m.pSeparator)
        *--- Usa una variabile local per poter fare la &
        Do While m.nCount <= m.nCountKey
            cKey = Getwordnum(m.pKeys, m.nCount, m.pSeparator)
            *--- Recupero il valore
            cVar = (m.cPrefix + m.cKey)
            *--- Aggiungo i parametri senza prefisso (w_)
            This.AddParam(m.cKey, pObj.&cVar)
            nCount = m.nCount + 1
        Enddo
		*--- Valorizzo w_SOTABGES che indica il nome della gestione 
		If Pemstatus(pObj, "cPrg", 5)
		    This.w_SOTABGES = pObj.cPrg
		Endif
    Endproc

    *--- Aggiunge nuova chiave ZR
    Proc AddZRKey(pNomeZRPK, pValoreZRPK, pForceUpdate)
        *--- Definisco una nuova propriet� il cui nome � contenuto in 'pNomeZRPK'
        Local l_pk
        l_pk = 'zr_'+Upper(Alltrim(m.pNomeZRPK))
        If !Pemstatus(This, m.l_pk, 5) OR m.pForceUpdate
        *--- Assegno al nuovo attributo il rispettivo valore
        This.AddProperty(m.l_pk, m.pValoreZRPK)
    	EndIf
    Endproc

    *--- Aggiunge parametro per filtro query profilo
    Proc AddParam(pNomePar, pValorePar)
        *--- Definisco una nuova propriet� il cui nome � contenuto in 'pNomePar'
        Local l_pk
        l_pk = 'w_'+Upper(Alltrim(m.pNomePar))
        If !Pemstatus(This, m.l_pk, 5)
            *--- Aggiungo il parametro nel dizionario cos� posso recuperare il nome casesesitive
            This.aParamName.Add(m.pNomePar, Upper(m.l_pk))
        Endif
        *--- Assegno al nuovo attributo il rispettivo valore
        This.AddProperty(m.l_pk, m.pValorePar)
    Endproc

    *--- Metodi per VQR (cp_Query)
    Func IsVar(cVarName)
        Return(Type("This."+m.cVarName)<>'U')
    Endfunc

    Func GetVar(cVarName)
        Return(This.&cVarName)
    Endfunc

    *--- Restituisce filtro per incrocio cursore ZR
    Func GetZRFilter()
        Return This.GetFilter("zr_", " and ", .T.)
    Endfunc

    *--- Restituisce filtro per scrittura in coda chiave AHR
    Func GetParamFilter()
        Return This.GetFilter("w_", ";")
    Endfunc

    *--- Filtro nella forma: Key=Value
    Func GetFilter(pPrefix, pSeparator, pRemovePrefix)
        Local nIdx, nNumProp, cFilter, cProp
        *--- Ciclo su tutte le propriet� che iniziano con pPrefix
        nNumProp = Amembers(arProp, This)
        cFilter = ''
        For nIdx = 1 To m.nNumProp
            If Left(Lower(arProp[m.nIdx]),Len(m.pPrefix)) == m.pPrefix
                cProp = Iif(m.pRemovePrefix, Substr(arProp[m.nIdx], Len(m.pPrefix)+1), arProp[m.nIdx])
                cFilter = m.cFilter + m.cProp + "=" + cp_ToStrODBC(This.&arProp[m.nIdx]) + m.pSeparator
            Endif
        Endfor
        *--- Tolgo ultimo pSeparator
        Return Left(m.cFilter, Len(m.cFilter) - Len(m.pSeparator))
    Endfunc

    *--- Filtro per XML: <Param>Value</Param>
    *--- Utilizzato nella richiesta dati
    Func GetFilterXML(pPrefix, pSeparator, pSpace)
        pSeparator = Evl(m.pSeparator, Chr(13)+Chr(10))
        pSpace = Evl(m.pSpace, 0)  &&In caso di false
        Local cFilter, cProp, nIdx, lxValue
        *--- Ciclo su tutte le propriet� presenti in aParamName
        cFilter = ''
        nIdx = 0
        For Each cProp In This.aParamName
		        nIdx = nIdx + 1 
                *---<cProp>Value</cProp>
                lxValue = This.aParamName.GetKey[m.nIdx]
                *--- I valori in questo caso sono sempre stringhe
                cFilter = m.cFilter + Space(m.pSpace) + "<"+m.cProp+">" + Transform(This.&lxValue) + "</"+m.cProp+">" + m.pSeparator
        Endfor
        *--- Tolgo ultimo pSeparator
        Return Left(m.cFilter, Len(m.cFilter) - Len(m.pSeparator))
    Endfunc

    *--- Restituisce insert per Log ZR inserendo le chiavi della ZR
    Func GetZRLogInsert(pFields, pValues, pZRTab)
        Local nIdx, nNumProp, cFields, cValues, cZRTabPh, cResult
        If Empty(NVL(This.cZRTab,"")) And Empty(NVL(pZRTab,""))
            Return ""
        Endif
        cResult = ""
        *--- Recupero tabella fisica
        pZRTab = Evl(m.pZRTab, This.cZRTab + "_LOG")
        nIdx = cp_OpenTable(m.pZRTab)
        If m.nIdx # 0
            This.nConn = i_TableProp[m.nIdx, 3]
            *--- ZR tab
            cZRTabPh = cp_SetAzi(i_TableProp[m.nIdx, 2])
        Endif
        *--- Ciclo su tutte le propriet� che iniziano con "zr_"
        nNumProp = Amembers(arProp, This)
        cFields = ''
        cValues = ''
        For nIdx = 1 To m.nNumProp
            If Left(Lower(arProp[m.nIdx]),3) == "zr_"
                cFields = m.cFields + SUBSTR(arProp[m.nIdx],4) + ","
                cValues = m.cValues + cp_ToStrODBC(This.&arProp[m.nIdx]) + ","
            Endif
        Endfor
        *--- Chiudo tabella ZR
        cp_CloseTable(m.pZRTab)
        If Not Empty(m.cFields)
            *--- Concateno le chiavi con quelle passate come parametro
            cFields = m.cFields + m.pFields
            *--- Concateno il valore delle chiavi con i valori passati come parametro
            cValues = m.cValues + m.pValues
            cResult = "Insert into "+ m.cZRTabPh + " (" + m.cFields + ") Values (" + m.cValues + ")"
        Endif
        *--- Restituisco frase SQL di insert per aggiornamento tabella Log ZR
        Return cResult
    Endfunc

    *--- Restituisce delete per Log ZR
    Function GetZRLogDelete(pMaxNumRow, pAll, pZRTab)
        Local nIdx, nNumProp, cFields, cZRTabPh, cFilter, bZRfound
        *--- Recupero tabella fisica
        pZRTab = Evl(m.pZRTab, This.cZRTab + "_LOG")
        nIdx = cp_OpenTable(m.pZRTab)
        If m.nIdx # 0
            This.nConn = i_TableProp[m.nIdx, 3]
            *--- ZR tab
            cZRTabPh = cp_SetAzi(i_TableProp[m.nIdx, 2])
        Endif
        Local l_TAB_ZR_LOG, l_KEY_DELETE, l_ZR_KEY, l_MAX_NUM_ROW
        l_TAB_ZR_LOG = m.cZRTabPh
        bZRfound = .F.
        *--- Ciclo su tutte le propriet� che iniziano con "zr_"
        nNumProp = Amembers(arProp, This)
        l_ZR_KEY = ''
        l_KEY_DELETE = ''
        cFilter = ''
        For nIdx = 1 To m.nNumProp
            If Left(Lower(arProp[m.nIdx]),3) == "zr_"
                bZRfound = .T.
                cProp = Substr(arProp[m.nIdx],4)
                l_ZR_KEY = m.l_ZR_KEY+ m.cProp + ","
                If Not m.pAll
                    cFilter = m.cFilter  + " and " + m.cProp + "=" + cp_ToStrODBC(This.&arProp[m.nIdx])
                Endif
            Endif
        Endfor
        If bZRfound
            Do Case
                Case CP_DBTYPE='SQLServer'
                    *--- in SQLServer char+int da errore
                    *--- La funzione Concat � disponibile in SQLServer solo dalla versione 2012 in poi
                    l_KEY_DELETE = "cast(" + Strtran(m.l_ZR_KEY,","," as varchar) + cast(") + "ZRTIMEST as varchar)"
                Case CP_DBTYPE='Oracle'
                    *--- nel caso char||int l'operatore || esegue automaticamente la conversione dei tipi numerici in char
                    *--- nel caso int||int l'operatore || da errore: non esistono tabelle con una PK di questo tipo
                    *--- non uso la funzione CONCAT perch� prevede al massimo due argomenti
                    l_KEY_DELETE = "(" + Strtran(m.l_ZR_KEY,",","||") + "ZRTIMEST)"
                Case CP_DBTYPE='PostgreSQL'
                    *--- uso la funzione CONCAT che esegue automaticamente la conversione di tutti i tipi numerici in char
                    *--- non uso l'operatore || perch� esegue la trim dei char
                    l_KEY_DELETE = "CONCAT(" + m.l_ZR_KEY + "ZRTIMEST)"
            Endcase
            l_ZR_KEY = Left(m.l_ZR_KEY, Len(m.l_ZR_KEY)-1)
            l_MAX_NUM_ROW = Evl(m.pMaxNumRow, 1)
            l_DBCmd = "delete from <<m.l_TAB_ZR_LOG>> where <<m.l_KEY_DELETE>> in "
            l_DBCmd = m.l_DBCmd + "(select <<m.l_KEY_DELETE>> from (select <<m.l_ZR_KEY>>, ZRTIMEST, row_number() over(partition by <<m.l_ZR_KEY>> order by ZRTIMEST DESC) as riga from <<m.l_TAB_ZR_LOG>>)"
            l_DBCmd = m.l_DBCmd + " tmptt where tmptt.riga > <<m.l_MAX_NUM_ROW>><<m.cFilter>>)"
            l_DBCmd = Textmerge(m.l_DBCmd)
        Else
            l_DBCmd = ""
        Endif
        *--- Chiudo tabella ZR
        cp_CloseTable(m.pZRTab)
        Return m.l_DBCmd
    Endfunc

    *--- Aggiunge attributi con valore vuoto (empty) per eseguire VQR con remove filter
    Proc SetEmptyParamFromVQR(pVQR)
        Local oQuery, oLoader, nIdx, cType, defValue
        Public g_testvar && impedisco l'apertura della richiesta param. delle query
        g_testvar = "S"
        oQuery = Createobject('cpquery')
        oLoader = Createobject('cpqueryloader')
        oLoader.LoadQuery(Addbs(Justpath(m.pVQR))+Juststem(m.pVQR), m.oQuery, .Null., .T., .T.)
        Release g_testvar
        With oQuery
            For nIdx = 1 To .nFilterParam
                *--- Assegno al nuovo attributo il valore vuoto
                cType = Left(.i_FilterParam[m.nIdx, 3], 1)
                Do Case
                    Case cType="N"
                        defValue = 0
                    Case cType="D"
                        defValue = {}
                    Otherwise
                        defValue = ""
                Endcase
                This.AddProperty(.i_FilterParam[m.nIdx, 1], defValue)
            Endfor
        Endwith
    Endproc
Enddefine

Define Class RevInfSyncInfo As Image
	BorderStyle = 0
	BorderColor = Rgb(161,161,161)
	Themes = .F.
	bClick = .F.
	&&cCntInfo = "oSyncInfoCnt"

	Procedure Init()
	    With This.Parent
	        *--- Aggiungo contenitore informazioni sincronizzazione, default non visibile e fuori schermo
	        .AddObject("oSyncInfoCnt", "RevInfSyncInfoCnt")
	        .oSyncInfoCnt.Move(.Width, OBJTOCLIENT(.oPgFrm.Page1, 1))
	        		    
		    *--- BindEvent
		    BINDEVENT(.oPgFrm, "ActivePage", This, "OnHide")
	    Endwith
	Endproc

	Procedure Destroy()
		WITH This.Parent
		    .RemoveObject("oSyncInfoCnt")
		    UNBINDEVENTS(.oPgFrm, "ActivePage", This, "OnHide")
		EndWith
	Endproc

	PROCEDURE OnHide()
		IF This.bClick
			This.bClick = .F.
		    With This.Parent.oSyncInfoCnt
			    .Anchor = 0
		        .Visible = .F.
		        .Left = .Parent.Width
		        .Anchor = 8
		    Endwith
		EndIf
	EndProc

	Procedure MouseLeave
	    Lparameters nButton, nShift, nXCoord, nYCoord
        This.BorderStyle = 0
	Endproc

	Procedure MouseEnter
	    Lparameters nButton, nShift, nXCoord, nYCoord
	    This.BorderStyle = 1
	Endproc

	Procedure Click()
	    With This
	        .bClick = !.bClick

		    Local tm
		    tm = Createobject("cp_TransitionManager")
		    	        
	        If .bClick
				*--- Aggiorno stato sincronizzazione
				.Parent.CheckiRevolutionSync()
	        	
	            *--- Rendo visibile e sposto il contenitore
	            .Parent.oSyncInfoCnt.AdjustSize()
	            .Parent.oSyncInfoCnt.Anchor = 0
	            .Parent.oSyncInfoCnt.Left = .Parent.Width
	            .Parent.oSyncInfoCnt.Visible = .T.
			    tm.Add(.Parent.oSyncInfoCnt, "Left", .Parent.Width - .Parent.oSyncInfoCnt.Width, "N", 300, "SineEaseOut")
			    tm.OnCompleted(This.Parent.oSyncInfoCnt, "Anchor = 8")
	        Else
				*.BorderColor = Rgb(40,40,40)
				*--- Sposto il contenitore e lo nascondo
				.Parent.oSyncInfoCnt.Anchor = 0
			    tm.Add(.Parent.oSyncInfoCnt, "Left", .Parent.Width, "N", 300, "SineEaseOut")
			    tm.OnCompleted(This, "Hide()")
	        Endif

		    tm.Start()
	    Endwith
	Endproc

	Procedure Hide()
	    With This.Parent.oSyncInfoCnt
	        .Visible = .F.
	        .Anchor = 8
	    Endwith
	Endproc

	PROCEDURE Visible_Assign(xValue)
		IF This.bClick AND NOT m.xValue
			This.Click()
		ENDIF 
		This.Visible = m.xValue
	EndProc

EndDefine

Define Class RevInfSyncInfoCnt As Container
    Top = 0
    Left = 0
    Height = 250
    Width = 348
    Backcolor = RGB(255,255,255)
    Bordercolor = Rgb(161,161,161)
    Name = "RevInfSyncInfo"

	Dimension aPolyPoints[3,2]	
	bSizer = .F.
	nXCoord = 0
	nYCoord = 0
	
	cAZCODICE = ""
	cZRUIDLOG = ""

	PROCEDURE Init()
		With This
			*--- Traduzione etichette
		    .label1.Caption = cp_translate("Utente:")
		    .label2.Caption = cp_translate("Ultima sincronizzazione:")
		    .label3.Caption = cp_translate("Richiesta da:")
		    .btnmore.ToolTipText = cp_translate("Visualizza informazioni di sincronizzazione")
		    
		    *--- Gripper
		    .aPolyPoints[1,1] = 1&&A
		    .aPolyPoints[1,2] = 1
		    .aPolyPoints[2,1] = 1&&B
		    .aPolyPoints[2,2] = 100
		    .aPolyPoints[3,1] = 100&&C
		    .aPolyPoints[3,2] = 100
		    .shpGripper.Polypoints ="This.Parent.aPolyPoints" 	
		Endwith
	EndProc

    Add Object label1 As Label With ;
        FontBold = .T., ;
        FontName = "Open Sans", ;
        FontSize = 10, ;
        BackStyle = 0, ;
        Caption = "Utente:", ;
        Height = 17, ;
        Left = 9, ;
        Top = 10, ;
        Width = 59, ;
        Name = "Label1"

    Add Object label2 As Label With ;
        FontBold = .T., ;
        FontName = "Open Sans", ;
        FontSize = 10, ;
        BackStyle = 0, ;
        Caption = "Ultima sincronizzazione:", ;
        Height = 17, ;
        Left = 9, ;
        Top = 36, ;
        Width = 171, ;
        Name = "Label2"

    Add Object lblUser As Label With ;
        FontName = "Open Sans", ;
        FontSize = 10, ;
        BackStyle = 0, ;        
        Caption = "", ;
        Anchor = 10, ;
        Height = 17, ;
        Left = 72, ;
        Top = 10, ;
        Width = 252, ;
        Name = "lblUser"

    Add Object lblDate As Label With ;
        FontName = "Open Sans", ;
        FontSize = 10, ;
        BackStyle = 0, ;        
        Caption = "", ;
        Anchor = 10, ;
        Height = 17, ;
        Left = 180, ;
        Top = 36, ;
        Width = 144, ;
        Name = "lblDate"

    Add Object label3 As Label With ;
        FontBold = .T., ;
        FontName = "Open Sans", ;
        FontSize = 10, ;
        BackStyle = 0, ;
        Caption = "Direzione:", ;
        Height = 17, ;
        Left = 9, ;
        Top = 60, ;
        Width = 87, ;
        Name = "Label3"

    Add Object lblDirection As Label With ;
        FontName = "Open Sans", ;
        FontSize = 10, ;
        BackStyle = 0, ;        
        Caption = "", ;
        Anchor = 10, ;
        Height = 17, ;
        Left = 104, ;
        Top = 60, ;
        Width = 240, ;
        Name = "lblDirection"

    Add Object txtLog As EditBox With ;
        Height = 144, ;
        Left = 9, ;
        Anchor = 15, ;
        ReadOnly = .T., ;
        Top = 96, ;
        Width = 315, ;
        Name = "txtLog"

    Add Object btnMore As CommandButton With ;
		Top = 221, ;
		Left = 326, ;
		Height = 20, ;
		Width = 20, ;
		Caption = "...", ;
		Anchor = 12, ;
		ToolTipText = "Visualizza maggiori dettagli", ;
		SpecialEffect = 2, ;
        Name = "btnMore"

    Add Object shpGripper As Shape With ;
		Top = 241, ;
		Left = 2, ;
		Height = 7, ;
		Width = 7, ;
		Anchor = 6, ;
		Mousepointer = 6, ;
        BorderStyle = 1, ;
        FillStyle = 0, ;
        BackStyle = 0, ;
        BorderColor = Rgb(161,161,161), ;
        FillColor = Rgb(161,161,161), ;	
        Name = "shpGripper"

	Procedure shpGripper.MouseDown
	    Lparameters nButton, nShift, nXCoord, nYCoord
	    WITH This.Parent
		    .bSizer = (m.nButton==1)
		    .nXCoord = m.nXCoord
		    .nYCoord = m.nYCoord
		ENDWITH
	endProc

	Procedure shpGripper.MouseUp
	    Lparameters nButton, nShift, nXCoord, nYCoord
	    This.Parent.bSizer = .F.
	endProc	    

	Procedure shpGripper.MouseMove
	    Lparameters nButton, nShift, nXCoord, nYCoord
	    LOCAL nLeft, nWidth, nHeight
	    With This.Parent
	        If m.nButton = 1 And .bSizer
	        	nWidth = MAX(.Width - m.nXCoord + .nXCoord, 250)
	        	nLeft = MIN(.Left + m.nXCoord - .nXCoord, .parent.width - m.nWidth)
	        	nHeight = MAX(.Height + m.nYCoord - .nYCoord, 150)
	        	.Anchor = 0
	            .Move(m.nLeft, .Top, m.nWidth, m.nHeight)
	            .Anchor = 8
	            Thisform.Draw()
	            .nXCoord = m.nXCoord
	            .nYCoord = m.nYCoord
	        Endif
	    Endwith
	Endproc

	Procedure shpGripper.DblClick()
		WITH This.Parent
			.Anchor = 0
			.Move(.Left, .Top, 348, 250)
			.AdjustSize()
			.Anchor = 0
			.Left = .Parent.Width - .Width 
			.Anchor = 8
		EndWith	
	EndProc

	PROCEDURE AdjustSize()
		LOCAL nWidth, nHeight
		WITH This
			nWidth = MAX(MIN(.Width,.Parent.Width), 250)
			nHeight = MAX(MIN(.Height,.Parent.Height - OBJTOCLIENT(.Parent.oPgfrm.Page1,1)), 150)
			.Anchor = 0
			.Move(.Left, .Top, m.nWidth, m.nHeight)
			.Anchor = 8
		EndWith		
	EndProc

	PROCEDURE btnMore.Click()
		LOCAL l_macro
		l_macro = "GSRV_KES(This.Parent.Parent)"
		&l_macro	&&Mashera modale
		*--- Aggiorno stato sincronizzazione
		This.Parent.Parent.CheckiRevolutionSync()		
	EndProc

Enddefine

Define Class MsgTimer As Timer
    *--- Intervallo 1 secondo
    Interval=0
    cMsg=""
    nDots=0

    Proc Init(cMsg)
        This.SetMsg(cMsg)
    Endproc

    Proc Timer()
        AH_MSG(This.cMsg+Replicate(".",Mod(This.nDots,4)))
        This.nDots = This.nDots + 1
    Endproc

    Proc SetMsg(cMsg)
        If Empty(cMsg)
            This.Interval = 0
        Else
            This.Interval = 500
            This.cMsg = cMsg
        Endif
    Endproc

    Proc ClearMsg()
        Release This
    Endproc
Enddefine


*--------------------------------------------------------------------------------------
* FoxBarcodeQR Class Definition
*--------------------------------------------------------------------------------------
DEFINE CLASS FoxBarcodeQR AS CUSTOM

  m.cTempPath = "" && Temp adhoc
  m.lDeleteTempFiles = .T. && Delete the temporary folder and image files

  *---------------------------------------------------------
  * PROCEDURE QRBarcodeImage()
  *---------------------------------------------------------
  * Generated QR Barcode image with BarCodeLibrary.DLL
  *  Parameters:
  *   tcText: Text to encode
  *   tcFile: Imagen File Name (optional)
  *   tnSize: Imagen Size [2..12] (default = 4)
  *     2 = 66 x 66 (in pixels)
  *     3 = 99 x 99
  *     4 = 132 x 132
  *     5 = 165 x 165
  *     6 = 198 x 198
  *     7 = 231 x 231
  *     8 = 264 x 264
  *     9 = 297 x 297
  *    10 = 330 x 330
  *    11 = 363 x 363
  *    12 = 396 x 396
  *   tnType: Imagen Type [BMP, JPG or PNG] (default = 0)
  *     0 = BMP
  *     1 = JPG
  *     2 = PNG
  *---------------------------------------------------------
  PROCEDURE QRBarcodeImage(tcText, tcFile, tnSize, tnType)
    LOCAL lcType, lcFolder

    IF VARTYPE(m.tnSize) <> "N"
      m.tnSize = 4 && default size:  132 x 132 pixels
    ENDIF

    IF VARTYPE(m.tnType) <> "N"
      m.tnType = 0  && defaul type: BMP
    ENDIF

    m.tnSize = MIN(MAX(m.tnSize, 2), 12)
    m.tnType = MIN(MAX(m.tnType, 0), 2)
    m.lcType = IIF(m.tnType = 1, "JPG", IIF(m.tnType = 2, "PNG", "BMP"))

    IF EMPTY(m.tcFile)
      m.lcFolder = THIS.cTempPath
      IF NOT DIRECTORY(m.lcFolder)
        MD (m.lcFolder)
      ENDIF
      m.tcFile = FORCEEXT(m.lcFolder + SYS(2015), m.lcType)
    ELSE
      m.lcFolder = JUSTPATH(m.tcFile)
      IF NOT DIRECTORY(m.lcFolder)
        MD (m.lcFolder)
      ENDIF
      m.tcFile = FORCEEXT(m.tcFile, m.lcType)
    ENDIF

    *- Declare the functions of BarCodeLibrary.dll
    DECLARE INTEGER GenerateFile IN BarCodeLibrary.DLL ;
      STRING cData, STRING cFileName

    DECLARE INTEGER SetConfiguration IN BarCodeLibrary.DLL ;
      INTEGER nSize, INTEGER nImageType

    SetConfiguration(m.tnSize, m.tnType)
    GenerateFile(m.tcText, m.tcFile)

    CLEAR DLLS SetConfiguration, GenerateFile

    RETURN m.tcFile
  ENDPROC

  *------------------------------------------------------
  * PROCEDURE Init()
  *------------------------------------------------------
  PROCEDURE INIT()
    THIS.cTempPath = ADDBS(g_tempadhoc)
  ENDPROC

  *------------------------------------------------------
  * PROCEDURE Destroy()
  *------------------------------------------------------
*!*	  PROCEDURE DESTROY()
*!*	    IF THIS.lDeleteTempFiles
*!*	      THIS.EmptyFolder(THIS.cTempPath)
*!*	      IF DIRECTORY(THIS.cTempPath)
*!*	        RD (THIS.cTempPath)
*!*	      ENDIF
*!*	    ENDIF
*!*	  ENDPROC

  *------------------------------------------------------
  * PROCEDURE EmptyFolder(tcFolder)
  *------------------------------------------------------
  * Empty temporary image folder
  *------------------------------------------------------
  PROCEDURE EmptyFolder(tcFolder)
    LOCAL loFso AS OBJECT
    LOCAL lcMask
    DO CASE
      CASE EMPTY(m.tcFolder)
        RETURN .F.
      CASE NOT DIRECTORY(m.tcFolder)
        RETURN .F.
    ENDCASE
    m.lcMask = ADDBS(m.tcFolder) + "*.*"
    #IF .T. && Use FSO
      m.loFso  = CREATEOBJECT("Scripting.FileSystemObject")
      m.loFso.DeleteFile(m.lcMask, .T.)
    #ELSE && Not Use FSO
      ERASE (m.lcMask)
    #ENDIF
    RETURN  .T.
  ENDPROC

  *---------------------------------------------------------
  * PROCEDURE Error
  *---------------------------------------------------------
  * Error procedure
  *---------------------------------------------------------
  PROCEDURE ERROR
    LPARAMETERS nError, cMethod, nLine
    LOCAL lcErrMsg
    LOCAL la[1]
    AERROR(la)
    m.lcErrMsg =  "Error number: " + TRANSFORM(m.la(1, 1)) + CHR(13) + ;
      "Error message: " + m.la(1, 2) + CHR(13) + CHR(13) + ;
      "Method: " + m.cMethod + CHR(13) + ;
      "Line: " + TRANSFORM(m.nLine)
    MESSAGEBOX(m.lcErrMsg, 0 + 16, "FoxBarcodeQR error")
  ENDPROC

  *---------------------------------------------------------

ENDDEFINE && FoxBarcodeQR

*--------------------------------------------------------------------------------------
* END DEFINE FoxBarcodeQR Class
*--------------------------------------------------------------------------------------

*--- Zucchetti Aulla Fine - iRevolution
