* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP3START
* Ver      : 2.1.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 24/03/95
* Aggiornato il : 01/09/97
* #%&%#Build:  56
* ----------------------------------------------------------------------------
* Starting program.
*
*#include "cp_app_lang.inc"

* CP3START is the starting program for applications generated with
* CodePainter Revolution. This program initializes the environment and
* the system variables. Moreover, it opens the database files.
*
*  Variables:
*    CP_DBTYPE : database type (Oracle,SQLServer,Access,DB2,VFP)
*    CP_ODBCCONN : name of the ODBC connection to use
* ----------------------------------------------------------------------------
* --- PROGRAMMA DI PARTENZA PER APPLICAZIONI CODEPAINTER IN VISUAL FOXPRO
* --- Parametri opzionali
Parameters p_FileCnf,p_SilentConnect,p_Application,p_SendTo,p_FileList,p_EntryKey && Permette specificare il File Cnf, Parametri per connessione silente

Public g_ADHOCONE
g_ADHOCONE=.F.
p_Application=Iif(Parameters()<3,"",p_Application)
Set Path To
* --- Default
p_FileCnf=Iif(Parameters()<1,"",p_FileCnf)

*--- Zucchetti Aulla Inizio - Considero come non passati i parametri valorizzati a '*'
Local cEmptyValue
m.cEmptyValue = '*'
p_FileCnf = Iif(Alltrim(p_FileCnf)==m.cEmptyValue, '', p_FileCnf)
p_SilentConnect = Iif(Vartype(p_SilentConnect)='C' And Alltrim(p_SilentConnect)==m.cEmptyValue, '', p_SilentConnect)
p_Application = Iif(Alltrim(p_Application)==m.cEmptyValue, '', p_Application)
p_SendTo = Iif(Vartype(p_SendTo)='C' And Alltrim(p_SendTo)==m.cEmptyValue, .f., p_SendTo)
p_FileList = Iif(Vartype(p_FileList)='C' And Alltrim(p_FileList)==m.cEmptyValue, .f., p_FileList)
Release cEmptyValue
*--- Zucchetti Aulla Inizio - Considero come non passati i parametri valorizzati a '*'

*MessageBox('1: '+Tran(p_FileCnf)+Chr(10)+'2: '+Tran(p_SilentConnect)+Chr(10)+'3: '+Tran(p_Application)+Chr(10)+'4: '+Tran(p_SendTo)+Chr(10)+;
'5: '+Tran(p_FileList)+Chr(10)+'6: '+Tran(p_EntryKey ))
*--- Zucchetti Aulla Fine - Considero come non passati i parametri valorizzati a '--'

* --- Zucchetti Aulla Inizio - Performance in modalit� remota
Sys(2450, 1)
* --- Zucchetti Aulla Fine - Performance in modalit� remota



* --- Controllo Versione Visual FoxPro
#If Version(5)>=800
	Set ENGINEBEHAVIOR 70
	Set Notify Cursor Off
#Endif

p_SilentConnect=Iif(Parameters()<2,"",p_SilentConnect)

Do declare_dlls IN ..\vfcsim\cp_gridex.fxp

* --- Zucchetti Aulla Inizio - Controllo riavvio applicazione
** usata da schedulatore per riaprire connessione
Public g_riavvio
g_riavvio=.T.
Do While Type("g_riavvio")='L' And g_riavvio
	g_riavvio=.F.
	* --- Zucchetti Aulla Fine - Controllo riavvio applicazione

	* --- STARTING PROGRAM FOR CODEPAINTER REVOLUTION APPLICATIONS GENERATED IN VISUAL FOXPRO
	_vfp.AutoYield=.F.
	Set Talk Off
	* --- variable for error administration
	Public bTrsErr
	bTrsErr = .F.
	* --- global variables
	i_codute = 1
	i_GROUPROLE=0
	i_bMobileMode = .F.
	i_codazi = "xxx"
	i_datsys = Date()
	i_curform = .Null.                  && Current Form
	i_vidgrf = .T.
	i_cBmpPath = ''                     && Path for cursors and BMP
	i_cStdIcon = 'painter.ico'          && standard icon
	i_bDisablePostIn=.F.
	i_ab_btnpostin=.T.                   && abilita/disabilita bottoni post-in su application bar
	i_ab_btnuser=.T.                     && abilita/disabilita bottone gestione utenti su application bar
	i_bDisableBackgroundImage=.T. && Zucchetti Aulla - Standard Painter a .F.
	*i_bDisableAsyncConn=.t. && Zucchetti Aulla - elenchi sincroni
	*i_nZoomMaxRows=200 && Zucchetti Aulla - elenchi sincroni
	i_CpDic = "plan"
	i_cModules = "VFCSIM,ecrm"
	i_cLanguage = ''
	i_cLanguageData = ''
	i_bSecurityRecord = .F. && Se attivo abilita la sicurezza a livello di dato.
	i_bOptimizerHints = .T.	&& Ottimizzazione SQL
	i_MsgTitle=''
	#If Version(5)<900
		i_cTmpImg = "C:\cp_TmImgs"+Sys(2003) && nelle versioni precedenti VFP ha problemi a creare subdirectory sotto la %TEMP%
	#Else
		i_cTmpImg = Addbs(Sys(2023))+"cp_TmImgs"+Sys(2003)
	#Endif

	Public i_MonitorFramework
	i_MonitorFramework = .F. && Se dichiarata sul file CNF disabilita il monitor framework

	*--- Numero massimo di item feed scaricati
	Public g_MaxItemFeedCount
	g_MaxItemFeedCount = 50

	*Variabile per impostare il comporatmento di default nella visaulizzazione dei report (report gdi o report non gdi)
	Public i_bnogdi
	i_bnogdi=.f.

	* Variabili pubbliche generiche
	Public g_CODESE, g_CODBUN, g_FILCON, g_CODUTE, g_VERSION, g_DEMO, g_INIZ
	Public g_PARPER, g_READKEY, g_DEMOLIMITS, g_NOKEY, g_NOUTE, g_LOCKALL,g_NOSTPDF
	Public i_EMAIL, i_EMAIL_PEC, i_bPEC, i_FAXNO, i_DEST, i_EMAILSUBJECT, i_EMAILTEXT, i_FAXSUBJECT, i_FAXNOTETEXT
	Public i_WEDEST, i_WEALLENAME, i_WEALLETITLE
	Public g_StrStrCon		&& stringa di connessione utente
	Public g_COLON, g_DispCnt,	g_RicContColor
	Public g_AgeVis, g_ColPrime, g_ColNnPrm, g_ColSelect, g_ColHeader, g_OraLavIni, g_OraLavFin, g_OraStaIni, g_OraStaFin
	Public g_ColFuoriSede, g_ColOccupato, g_ColUrgenze, g_ColLibero, g_ColAttConf
	* Fuori sede, blu
	g_ColFuoriSede=Rgb(0, 117, 234)
	* Occupato, rosso
	g_ColOccupato=Rgb(255, 0, 0)
	* Urgenze, giallo
	g_ColUrgenze=Rgb(242, 242, 0)
	* Libero, verde
	g_ColLibero= Rgb(33, 161, 33)
	* Definizione variabili per la gestione delle funzionalit� all'interno dei men�
	* contestuali
	Public cHomeDir
	cHomeDir = Addbs(Sys(5)+Curdir())  && use whatever method you prefer

	Public i_PROGBAR
	i_PROGBAR = .Null.                   && ProgressBar

	Public g_disable_cp_Round			&& disabilita funzione cp_round e utilizza la funzione standard del fox
	g_disable_cp_Round=.F.
	
	* --- Zucchetti Aulla Inizio - Lingua del progetto
	Public g_PROJECTLANGUAGE,g_DEMANDEDLANGUAGE
	g_PROJECTLANGUAGE='ITA'
	* --- Zucchetti Aulla Fine - Lingua del progetto

	* --------- Parametrizzare interfaccia
	* Sceglie quale data deve essere utilizzata come filtro per le date di validit� dei postin
	* i_DatSysPostInFilterDate = .t. usa i_DatSys, altrimenti usa la funzione date().
	Public i_DatSysPostInFilterDate
	i_DatSysPostInFilterDate = .T.
	Public i_dPostInFilterDate
	i_dPostInFilterDate = i_datsys

	Public i_VisualTheme
	i_VisualTheme = 7 && -1 Vista Classica, 5 Office 2007 Blue, ADHOC 2012
	Public g_RicPerCont
	g_RicPerCont = ''
	Public g_USE_ILIKE && usa ILIKE al posto della LIKE (solo per db PostgreSQL)
	g_USE_ILIKE = 'S'
	Public g_MNODEBMP, g_MLEAFBMP

	* ---- Nomi bitmap di default se bitmap vuoti...
	g_MLEAFBMP='DIR_DESC.BMP' && Bitmap per le foglie
	g_MNODEBMP='DIRCL.BMP'    && Bitmap per i nodi

	*--- Interfaccia - Font
	Public i_cProjectFontName, i_nProjectFontSize
	i_cProjectFontName='Arial'       && Font name di progetto
	i_nProjectFontSize=9            && Font size di progetto

	*--- Zucchetti Aulla Inizio - gestione minuti Post-IN
	Public g_MinutiPostIN
	Public g_ModoPostIN
	g_MinutiPostIN=10
	g_ModoPostIN=1
	*--- Zucchetti Aulla Fine - gestione minuti Post-IN
	* Zucchetti Aulla Inizio - Colore Post-IN
	Public g_PostInColor
	g_PostInColor=Rgb(255, 255, 0)
	* Zucchetti Aulla Fine - Colore Post-IN
	*--- DateTime Mask, la dimensione delle colonne che contengono campi data viene forzata
	*--- per non mostrare l'ora (00:00:00), ma se nel formato della colonna dello zoom inserisco g_DateTimeMask,
	*--- viene mantenuta la dimensione standard
	*---  Priorit� email 3 normale, 5 bassa,1 alta
	Public g_EmailPrior
	g_EmailPrior=3
	Public g_DateTimeMask
	g_DateTimeMask = 'DT'

	Public g_DatePickerFloating
	g_DatePickerFloating = .T.

	*--- Zucchetti Aulla Inizio - Nascondi immagini bottoni
	Public g_NoButtonImage
	g_NoButtonImage = .F.
	*--- Zucchetti Aulla Fine - Nascondi immagini bottoni

	* Gia valorizzati per evitare errori in caso di non
	* aggiornamento del database
	* colore righe griglie
	* COLORE RETTANGOLO EVIDENZIATORE CONTROL OBBLIGATORI
	* Curvatura rettangolo campi obbligatori
	* Dimensioni bottoni toolbar

	* Toolbar/Form/Menu/Screen
	Public i_bShowCPToolBar, i_bShowDeskTopBar, i_bShowToolMenu,;
		g_nNumRecent, g_MDIForm, g_bShowMenu, g_bShowHideMenu, i_bMenuFix ,  g_DisableMenuImage, g_AB_VIEWBUTTON, ;
		i_nCPToolBarPos, i_nDeskTopBarPos, i_bToolBarDisappear
	*--- Ricerca bmp/ico immagini menu
	*--- Gestione riconnessione automatica
	Public i_cFileCNF
	*--- Gestione riconnessione automatica
	i_bMenuFix  = .T.
	i_bShowCPToolBar = .T.
	i_bShowDeskTopBar = .T.
	i_nCPToolBarPos = 0
	i_nDeskTopBarPos = 0
	i_bToolBarDisappear = .F.
	g_nNumRecent = 5
	g_MDIForm = .F.
	i_bShowToolMenu = .T.
	g_bShowMenu = .T.
	g_bShowHideMenu = .T.
	g_AB_VIEWBUTTON = SPACE(20)
	i_bFirstPage=.T. && alla presssione dell'F3 rimane nella pagina corrente..

	* --- Zucchetti Aulla - variabili globali per iRevolution
	Public g_RevNoAzi
	g_RevNoAzi = "##NESSUNA#" && codice usato nella tabella ASSREVAZ per indicare che l'azinda AHR non � associata a nessuna azienda di Infinity
	Public g_PKREVAL
	g_PKREVAL = "RE" && Valore da usare nelle ricerche come filtro per la tabella ASSREVAZ.MACODICE=g_PKREVAL

	*Abilita la nuova Print System
	Public i_bNewPrintSystem
	Public i_NroCopie
	i_NroCopie=0

	Public i_bnoTrimStrOdbc
	i_bnoTrimStrOdbc=.T.

	Set Procedure To ..\vfcsim\cp_lib.fxp,..\vfcsim\cp_class.fxp
	* --- Zucchetti Aulla - gestione cachefile
	cp_CreateCacheFile()
	* --- Zucchetti Aulla - Inizio - Creazione variabili per la gestione dell'ActivityLogger
	Set Procedure To ..\vfcsim\cp_activitylogger.fxp,..\vfcsim\cp_themesmanager.fxp Additive
	*--- Activity logger, setta variabili pubbliche
	oActivityLoggerSetup = Createobject("ActivityLoggerSetup")
	* --- Zucchetti Aulla - Inizio - Creazione variabili per la gestione dell'ActivityLogger
	Public g_APPLICATION, i_nXPTheme
	i_nXPTheme = 1
	i_nTbBtnSpEfc =0
	i_nBtnSpEfc =0
	i_nPrnBtnSpEfc =0


	g_APPLICATION = "ADHOC REVOLUTION"
		g_VERSION = ' Rel. 8.0' && La versione del prodotto deve essere nel cp3start x verifica congruit� blackbox (GSUT_BVB)
		public i_SplashImage
		public i_LoadINF
		i_cStdIcon = 'ahr.ico'			&& standard icon
		i_cModules="vfcsim,DISB,ecrm"
		i_LoadINF = .t.					&& abilita/disabilita caricamento automatico file INF (ServicePack) all'ingresso
		i_SplashImage=.T.				&& abilita/disabilita immagine iniziale dell'applicativo
		g_VAHBE=.f.

	_Screen.Caption='Zucchetti'

	Public i_PROGBAR
	i_PROGBAR = .Null.                   && ProgressBar
	* ---
	* --- Traduzioni
	#include "cp_app_lang.inc"

	*--- Gestione Help
	Public i_HelpExt
	If GetVersion() <> 393216006
		i_HelpExt = '.hlp'
	Else
		i_HelpExt = '.chm'
	Endif
	*--- Fine gestione Help

		g_func = '..\vfcssrc\BlackBox.fxp'

	Set Proc To &g_func

	Public i_WinDIR                   && Contiene directory di Windows
	i_WinDIR = GetWinDir()
	*
	Public i_WinVER                   && Contiene versione di Windows (NT4.xx,WIN4.xx)
	i_WinVER = GetWinVer()
	*
	Public g_PRTJOLLY                 && Contiene carattere Jolly per identificazione Stampante su Terminal Server
	g_PRTJOLLY = '#'
	*
	Public g_PRINTMERGE		&& 2: Stampa su File, 1: Lancia la Stampa (senza creare il file)
	g_PRINTMERGE = '2'
	*
	Public g_PRINTERARCHI
	g_PRINTERARCHI=''
	*** Variabile per i menu contestuali
	Public g_oMenu
	g_oMenu=.Null.
	*** Contiene il Nome delle tabelle su cui g_oMenu deve effettuare la lettura in fase di caricamento del menu contestuale
	Public g_LISTGOMENUTABLE
	g_LISTGOMENUTABLE = "KEY_ARTI,OFF_ATTI,CAN_TIER,OFF_NOMI,TODOLIST"
	*** Contiene la lista delle tabelle aggiuntive nella gestione del menu contestuale g_oMenu
	Public g_CUSTGOMENUTABLE
	g_CUSTGOMENUTABLE=''
	*
	If g_APPLICATION = "ADHOC REVOLUTION"
		* Variabili per Logistica remota
		Public g_cPrefiProg && Prefisso per gli autonumber legato alla singola postazione remota
		Public g_aLoreProg && Array pubblico che contiene l'elenco degli autonumber gestiti con prefisso
		Dimension g_aLoreProg(1)
	Endif

	* -------------------------Zucchetti Aulla Accesso sicurtato ------------------------------------------------------------------
	Public g_CRYPT 		&& Valori possibili 'N'- (default)comportamento attuale,'C' stringa connessione CNF criptata
	&& 'A' - Solo per SQL Server, utilizzo application Roles (utilizzabile anch'esso tramite cryptatura)
	&& Nel caso di Application Roles dovrebbe essere necessario utilizzare una stringa di connessione
	&& in quanto l'accesso avverr� tramite la sicurezza di NT
	Public g_APPROLE	&& Solo per SQL Server, contiene il nome dell'Application Role da utilizzare
	Public g_APPPSW		&& Password Application Role
	g_CRYPT='N'			&& sar� eventualmente attivato nel file di configurazione (CNF).

	* --------------------------Zucchetti Aulla Fine Accesso sicurtato ------------------------------------------------------------

	*Inserisco le variabili pubbliche per la gestione dello zoom sui numeri FAX
	Public i_CLIFORDES, i_CODDES, i_TIPDES
	If g_APPLICATION = "ADHOC REVOLUTION"
		*Inserisco variabile pubblica per selezione email da anagrafica AGENTI
		Public i_AGENTCOD
	Endif
	* --------------------------Zucchetti Aulla Inizio Invio Mail Supporto SMTP ------------------------------------------------------------
	g_INVIO='M'			&& M - Mapi, S - Smtp
	g_SRVMAIL=''		&& Server di invio Mail (su cui gira servizio SMTP)
	g_SRVPORTA=0		&& Numero porta servizio servizio SMTP
	g_MITTEN=''			&& Indirizzo E-Mail completo da utilizzare come mittente (mionome@server.it)
	g_AccountMail=''	&& Serial della tabella ACC_MAIL con le impostazioni dell'account mail
	* --------------------------Zucchetti Aulla Fine Invio Mail Supporto SMTP ------------------------------------------------------------
	* --------------------------Zucchetti Aulla Inizio Invio Mail Micrsoft Outlook ----------------------------------------------------------
	g_MAILSYNC='N'
	g_MAILSYNC_TIMEOUT=30
	* --------------------------Zucchetti Aulla Fine Invio Mail Micrsoft Outlook -----------------------------------------------------------
	Public i_nFormScrollBars
	i_nFormScrollBars = 0   && Come Default Disabilito la gestione delle scrollbars
	&& impostando la variabile nel cnf si pu� modificare l'impostazione
	Public g_CRYPTSilentConnect      && Mi indica che il secondo parametro per la connessione silente � criptato
	g_CODESE = Space(4)
	g_CODBUN = Space(3)
	g_CODUTE = 0
	g_INIZ = .T.
	g_LOCKALL=.F.
	Set Date Italian
	g_APPOSNAMEIN = Upper(Substr(Sys(16),Rat('\',Sys(16))+1))
	g_APPTITLEAHE = ""
	g_ORIPATH = ''
	* --- ZUCCHETTI AULLA INIZIO
	g_XDCPATH = ''					&& Path utlizzato nella cp_dcx e nella cp_menu
	* --- ZUCCHETTI AULLA FINE
	* --- ZUCCHETTI AULLA INIZIO - parametri di connessione ODBC per Postgres
	* Array di connessione
	* 1: Parametro da inserire nella connessione
	* 2: Parametro cifrato di risposta da Postgres
	* 3: Valore da impostare e/o di controllo
	* 4: Messaggio di errore se parametro errato
	Public i_PGCONN
	DIMENSION i_PGCONN(14,4)
	i_PGCONN[1,1]="MaxVarcharSize="
	i_PGCONN[1,2]="B0="
	i_PGCONN[1,3]="254"
	i_PGCONN[1,4]="%0 <Max Varchar> errato, impostare 254"
	
	i_PGCONN[2,1]="MaxLongVarcharSize="
	i_PGCONN[2,2]="B1="
	i_PGCONN[2,3]="8190"
	i_PGCONN[2,4]="%0 <Max LongVarchar> errato, impostare 8190"
	
	* PARAMETRO CX VALIDO = 1c15223b
	i_PGCONN[3,1]="CX="
	i_PGCONN[3,2]=""
	i_PGCONN[3,3]="1c15223b"
	i_PGCONN[3,4]=""

	i_PGCONN[4,1]="TextAsLongVarchar="
	i_PGCONN[4,2]="" && CX 5 carattere da dx, 1^ bit
	i_PGCONN[4,3]="1"
	i_PGCONN[4,4]=""
	
	i_PGCONN[5,1]="UseDeclareFetch="
	i_PGCONN[5,2]="" && CX 4 carattere da dx, 3^ bit
	i_PGCONN[5,3]="0"
	i_PGCONN[5,4]=""
	
	i_PGCONN[6,1]="CancelAsFreeStmt="
	i_PGCONN[6,2]="" && CX 4 carattere da dx, 2^ bit
	i_PGCONN[6,3]="1"
	i_PGCONN[6,4]=""
	
	i_PGCONN[7,1]="ShowSystemTables="
	i_PGCONN[7,2]=""
	i_PGCONN[7,3]="1"
	i_PGCONN[7,4]=""
	
	i_PGCONN[8,1]="BI="
	i_PGCONN[8,2]="BI="
	i_PGCONN[8,3]="2"   && Numeric, 4=Integer
	i_PGCONN[8,4]="%0 <Int8 As> errato, impostare <numeric>"
	
	i_PGCONN[9,1]="Protocol="
	i_PGCONN[9,2]="A1="
	i_PGCONN[9,3]="7.4-2"  && L'ultimo carattere definisce <Level of Rollback>
	i_PGCONN[9,4]="%0 <Level of rollback on errors> errato, impostare <Statement>%0 <Protocol> errato, impostare <7.4+>"
	
	i_PGCONN[10,1]="UnknownSizes="
	i_PGCONN[10,2]="" && CX 2 carattere da dx, 3 e 4 bit
	i_PGCONN[10,3]="2" && Longest
	i_PGCONN[10,4]=""
	
	* 2car da dx 34^ bit: UnknownSizes deve essere 2 (Longest)
	i_PGCONN[11,1]=""
	i_PGCONN[11,2]="CX="
	i_PGCONN[11,3]="2****00**" && 2<=>3, 1<=>7, 0<=>b
	i_PGCONN[11,4]="%0 <Unknown Sizes> errato, impostare <Longest>"

	* 5car da dx 1^ bit: TextAsLongVarchar deve essere 1
	i_PGCONN[12,1]=""
	i_PGCONN[12,2]="CX="
	i_PGCONN[12,3]="5*******1"
	i_PGCONN[12,4]="%0 <Text As LongVarchar> errato, attivare flag"

	* 4car da dx 2^ bit: CancelAsFreeStmt deve essere On (Valore 1)
	i_PGCONN[13,1]=""
	i_PGCONN[13,2]="CX="
	i_PGCONN[13,3]="4******1*"
	i_PGCONN[13,4]="%0 <Cancel As FreeStmt> errato, attivare flag"

	* 4car da dx 3^ bit: UseDeclareFetch deve essere Off (valore 0)
	i_PGCONN[14,1]=""
	i_PGCONN[14,2]="CX="
	i_PGCONN[14,3]="4*****0**"
	i_PGCONN[14,4]="%0 <Use Declare/Fetch> errato, disattivare flag"
	* --- ZUCCHETTI AULLA FINE

	g_StrUsrCon = Sys(2015)+Ltrim(Padr(Sys(0),10))

	Public g_CHECKACCESS, g_CHECKUNC
	g_CHECKACCESS=.T.
	g_CHECKUNC=.F.
	i_LoadXDC = .T.                      && abilita/disabilita caricamento automatico dizionario dati
	i_PathHELP= 'help\'                  && path per files di help
	i_LoadImage= .F.                     && abilita/disabilita caricamento automatico delle immagini associate alle maschere
	i_usePostIn= .T.                     && abilita/disabilita uso dei post-in
	i_lcheckaddon=.T.                    && abilita/disabilita resort priorit� per add-ON

	g_FILCON = i_CpDic
	* --- Zucchetti Aulla Fine
	* --------- Parametrizzare interfaccia End

	Store ''  To i_EMAIL, i_EMAIL_PEC, i_FAXNO, i_DEST, i_WEDEST, i_CLIFORDES, i_CODDES, i_TIPDES,i_ORIGINE
	If g_APPLICATION = "ADHOC REVOLUTION"
		Store ''  To i_AGENTCOD
	Endif
	Public i_inidat,i_findat,i_MsgErrDat
	i_inidat={^1900/01/01}
	i_findat={^3000/01/01}

	Public i_iniNUm,i_finNUm,i_MsgErrNum
	i_iniNUm=-99999999999999
	i_finNUm=99999999999999

	* disabilita check riga vuota
	Public i_bCheckEmptyRows
	i_bCheckEmptyRows=.F.

	*** Variabile Messaggi schedulatore
	Public g_MSG
	g_MSG=''

	*** Variabile cartella temporanea
	Public g_TEMPADHOC
	Local l_OldError, bTempadhocErr, l_cFolderPath, l_WSHShell, l_cSpecialFolder

	#Define CSIDL_APPDATA 0x001a
	*!*   Version 4.71. The file system directory that serves as a common repository for application-specific data. A typical path is C:\Documents and Settings\username\Application Data. This CSIDL is supported by the redistributable Shfolder.dll for systems that do not have the Microsoft� Internet Explorer 4.0 integrated Shell installed.

	l_cFolderPath = Space(255)
	l_OldError=On("ERROR")
	On Error bTempadhocErr=.T.
	bTempadhocErr=.F.
	* Recupero il percorso della cartella applicationdata con l'utilizzo dell'apposita dll
	Declare SHORT SHGetFolderPath In SHFolder.Dll ;
		INTEGER hwndOwner, Integer nFolder, Integer hToken, ;
		INTEGER dwFlags, String @pszPath

	If !bTempadhocErr
		SHGetFolderPath(0, CSIDL_APPDATA, 0, 0, @l_cFolderPath)
		Clear Dlls SHGetFolderPath
	Else
		* Se la prima dll non � disponibile recupero il percorso della cartella applicationdata con l'utilizzo della libreria per recuperare le cartelle speciali
		Declare SHGetSpecialFolderPath In SHELL32.Dll ;
			LONG hwndOwner, ;
			STRING @cSpecialFolderPath, ;
			LONG nWhichFolder
		If !bTempadhocErr
			SHGetSpecialFolderPath(0, @l_cFolderPath, CSIDL_APPDATA)
			Clear Dlls SHGetSpecialFolderPath
		Else
			* Se ambedue le dll non sono disponibili tento di recuperare il percorso attraverso windows scripting host
			l_WSHShell = Createobject("WScript.Shell")
			If Vartype(l_WSHShell) = "O"
				l_cSpecialFolder = "AppData"
				l_cFolderPath = l_WSHShell.SpecialFolders(l_cSpecialFolder)
			Else
				bTempadhocErr=.T.
			Endif
		Endif
	Endif

	If !bTempadhocErr And !Empty(l_cFolderPath)
		g_TEMPADHOC = Addbs(Alltrim(l_cFolderPath)) +'Zucchetti\ADHOCTEMP'
	Else
		* Se con nessuno dei tre metodi possibili ho ottenuto il percorso della cartella dati applicazioni utilizzo la temp
		g_TEMPADHOC=Sys(2023)+'\ADHOCTEMP'
	Endif
	l_WSHShell=.Null.
	On Error &l_OldError
	Release l_OldError, bTempadhocErr, l_cFolderPath, l_WSHShell, l_cSpecialFolder

	g_DEMO = .F. &&  .t. = Versione DEMO
	* --- setta l' ambiente
	CP_PATH=''        && program paths
	CP_DBTYPE=''      && database type:VFP,Access,SQLServer,Oracle,DB2,Informix,Interbase,SAPDB,MySQL,PostgreSQL
	CP_ODBCCONN=''    && ODBC connection name
	CP_ODBCCADD=''    && parametri di connessione
	CP_DBNAME=''	  && Nome del database
	CP_DBSERVER=''	  && Nome del server di database
	* --- Zucchetti Aulla - Inizio - Dsabilita esclusione CPCCCHK da scritture sul database
	Public i_stopCheckCPCCCHKExistence, g_stopCheckCPCCCHKExistence

	SetEnv()

	*--- Zucchetti Aulla Inizio - SendTo
	Public oSendMsgHandler
	oSendMsgHandler=Newobject("SendMsgHandler")
	*--- Zucchetti Aulla Fine - SendTo

	* --- Zucchetti Aulla - Inizio - DB2 non permette di utilizzare date antecedenti il 02/01/1900 per i tipi DateTime
	If CP_DBTYPE='DB2'
		i_inidat={^1900/01/02}
	Endif
	* --- Zucchetti Aulla - Fine
	* --- All'avvio della procedura non controllo il cpccchk perch� la routine carica l'XDC
	* --- e questa operazione � ammessa solo dopo aver definito l'i_cmodules
	* --- g_stop.. utilizzata per preservare un'eventuale impostazione da file di configurazione
	g_stopCheckCPCCCHKExistence=i_stopCheckCPCCCHKExistence
	i_stopCheckCPCCCHKExistence=.T.
	* --- Zucchetti Aulla - Fine

	* --- Zucchetti Aulla - Barra in basso Inizio
	* --- Zucchetti Aulla Inizio - Interfaccia
	If i_VisualTheme = -1
		Set Status Bar On
	Endif
	* --- Zucchetti Aulla Fine - Interfaccia
	*Set Status Off
	Set Notify On
	* --- Zucchetti Aulla - Barra in basso Fine


	* --- ZUCCHETTI AULLA Inizio
	* Controllo Preliminare sull'utilizzo per path UNC
	If g_CHECKUNC
		If Left(Sys(5),2)='\\'
			cp_errormsg(CP_MSGFORMAT(MSG_APPLICATION_CANNOT_WORK_WITH_UNC_PATH,Chr(13)),16, g_APPLICATION,.f.)
			ResetEnv()
			Return
		Endif
		If At(' ',Sys(2003))>0
			cp_errormsg(CP_TRANSLATE(MSG_APPLICATION_CANNOT_WORK_WITH_INSTALLATION_PATH_CONTAINING_SPACES_1)+;
				CP_TRANSLATE(MSG_APPLICATION_CANNOT_WORK_WITH_INSTALLATION_PATH_CONTAINING_SPACES_2),16, g_APPLICATION,.f.)
			ResetEnv()
			Return
		Endif
	Endif

	* --- ZUCCHETTI AULLA Fine

	_Screen.Icon=i_cBmpPath+i_cStdIcon
	* --- Zucchetti Aulla -Se attivo g_crypt cripto l'accesso
	If g_CRYPT<>'N'
		Do Case
			Case g_CRYPT='C'
				CP_ODBCCONN=CifraCnf(CP_ODBCCONN,'D')
			Case g_CRYPT='A' And Type('g_APPROLE')='C' And Type('g_APPPSW')='C'
				g_APPROLE=CifraCnf(g_APPROLE,'D')	&& Decripta Ruolo
				g_APPPSW=CifraCnf(g_APPPSW,'D')		&& Decripta Password
		Endcase
	Endif
	* --- Zucchetti Aulla - Fine cripto l'accesso
	
	*--- Zucchetti Aulla Inizio - Librearia con funzioni di criptazione
	SET LIBRARY TO vfpencryption71.fll ADDITIVE
	*--- Zucchetti Aulla Fine - Librearia con funzioni di criptazione
	
	*--- Zucchetti Aulla Inizio - Compressione file in formato Zip da inviare a infinity
	Set Library To vfpcompression.fll ADDITIVE 
	*--- Zucchetti Aulla fine - Compressione file in formato Zip da inviare a infinity
	
	*--- Zucchetti Aulla Inizio - Esportazione dati su Excel
	SET LIBRARY TO "..\vfcsim\cp_xlsx.fll" ADDITIVE
 	*--- Zucchetti Aulla Fine - Esportazione dati su Excel
	
	*--- Zucchetti Aulla Inizio - VFP2C32
	&& somewhere at your programs startup code
	#INCLUDE vfp2c.h
	SET LIBRARY TO vfp2c32.fll ADDITIVE
	IF !InitVFP2C32(VFP2C_INIT_ALL) && you can vary the initialization parameter depending on what functions of the FLL you intend to use
	  LOCAL laError[1], lnCount, xj, lcError
	  lnCount = AERROREX('laError')
	  lcError = 'VFP2C32 Library initialization failed:' + CHR(13)
	  FOR xj = 1 TO lnCount
	    lcError = lcError + ;
	    'Error No : ' + TRANSFORM(laError[1]) + CHR(13) + ;
	    'Function : ' + laError[2] + CHR(13) + ;
	    'Message : "' + laError[3] + '"'
	  ENDFOR
	  && show/log error and abort program initialization ..
	  cp_ErrorMsg(lcError)
	ENDIF

	*--- Zucchetti Aulla Inizio - Libreria per la conversione cursore-Json e viceversa
	Set Library To ForcePath("cp_Json.fll",cPathVfcsim) Additive
	*--- Zucchetti Aulla Fine - Libreria per la conversione cursore-Json e viceversa
	
	
	* --- Configurazione connessione database
	If Vartype(CP_ODBCCONN)<>'C' Or Empty(Nvl(CP_ODBCCONN, ' '))
		Set Library To foxtools.fll Additive
		*Set Library To vfp2c32.fll Additive
		l_sPrg="gswdbcfg.fxp"
		Do (l_sPrg) With .Null.
		l_sPrg=""
		Release Library foxtools
		*Release Library vfp2c32
	Endif

	If !Empty(CP_ODBCCADD)
		CP_ODBCCONN="DSN="+CP_ODBCCONN+";"+CP_ODBCCADD
	Endif
	* --- The following variables cannot be changed from user setup
	i_cSuperPwd = 'codepainter'   && password for superuser rights
	i_demolimits = ''             && demo version limits
	* --- Managing servers and files
	Public i_TableProp, i_nTables
	i_nTables = 1
	Dimension i_TableProp[1,5]
	Public i_ServerConn, i_nServers, i_ServerConnBis
	i_nServers = 1
	*--- Gestione riconnessione
	Dimension i_ServerConn[1,8]
	Dimension i_ServerConnBis[1,2]
	*--- Gestione riconnessione
	* --- "Local" connection definition
	i_ServerConn[1,1]='local'
	i_ServerConn[1,4]=1
	i_ServerConn[1,5]=0
	i_ServerConn[1,7]=! i_bDisablePostIn
	i_ServerConn[1,2]=Iif(Empty(CP_DBTYPE) Or CP_DBTYPE='VFP',0,-1) && 0 for foxpro database, -1 for ODBC connection
	i_ServerConn[1,6]=CP_DBTYPE
	i_ServerConn[1,3]=CP_ODBCCONN
	*--- Gestione riconnessione
	i_ServerConn[1,8]=""
	i_ServerConnBis[1,1]=-2
	i_ServerConnBis[1,2]=CP_ODBCCONN
	*--- Gestione riconnessione
	* --- Controllo errori
	IF Vartype(g_COLLATEGENERAL)='L' And g_COLLATEGENERAL
	 SET COLLATE TO "GENERAL"
	endif
	If Vartype(g_DISABLECTRLERROR)<>'L' Or Not g_DISABLECTRLERROR
		On Error cp_ChkError(Error(),Message(),Program(),Line())
	Endif

	* i_bnogdi=IsAlt() non va bene perch� altrimenti si sovrascrive l assegnamento fatto nel CNF
	IF IsAlt()
	i_bnogdi=.T.
	endif

	* --- Open connection with the database
	* --- ADHOC Inizio
	If i_ServerConn[1,2]=-1 And Not Empty(CP_ODBCCONN)
		If cp_OpenDatabase()
			* --- ZUCCHETTI AULLA INIZIO
			*--- Controllo certificazioni OS e DBEngine
			l_macroRTN = "GSUT_BOS(.Null., .T.)"
			&l_macroRTN
			* Controllo versione blackbox
			If cp_fileexist('GSUT_BVB.FXP')
				Local ok, Batch
				ok=.T.
				Batch=''
				Batch='ok=GSUT_BVB(.NULL.)'
				&Batch
				If !ok
					* --- Versione blackbox non congruente con cp3start, esco...
					ResetEnv()
					Return
				Endif
			Endif
			* --- cmq riforzo la g_VERSIONE
			setversion()
			_Screen.Icon=i_cBmpPath+i_cStdIcon
			* oCpToolBar.show()
			*SET CENTURY ON
			Set Century To 19 ROLLOVER 30
			*SET POINT TO ','
			*SET SEPARATOR TO '.'
			i_MsgErrDat=CP_MSGFORMAT(MSG_INSERT_A_DATE_INCLUDED_BETWEEN__AND__,Dtoc(i_inidat),Dtoc(i_findat))
			i_MsgErrNum=CP_MSGFORMAT(MSG_INSERT_AMOUNTS_INCLUDED_BETWEEN__AND__,Alltrim(Tran(i_iniNUm,'@z 999,999,999,999,999,999,999,999,999')),Alltrim(Tran(i_finNUm,'@z 999,999,999,999,999,999,999,999,999')))

			*--- Zucchetti Aulla - Traduzioni - Inizio
			*SET SYSFORMAT ON
			*--- Zucchetti Aulla - Traduzioni - Fine
			* --- Carica Parametri di Configurazione Utente
			* --- I Parametri selezionati sulla Maschera saranno registrati in GS___BAZ
			g_FILCON = tempadhoc()+'\'+Iif(g_APPLICATION = "ADHOC REVOLUTION",'AHR','AHE')+Alltrim(Str(i_codute))+'.CNF'
			If cp_fileexist(g_FILCON)
				If !Empty(g_FILCON)
					Private AA0, AA1
					AA0=Fopen(g_FILCON)
					Do While !Feof(AA0)
						AA1=Fgets(AA0)
						&AA1
					Enddo
					Fclose(AA0)
				Endif
			Endif
			* --- Zucchetti Aulla Variabile per segnalare attivit� sessione
			Public g_LASTVERACT
			g_LASTVERACT=Seconds()
			* ---
			* se ae valorizzo sempre sicurezza record
			i_bSecurityRecord = Iif( Not i_bSecurityRecord And IsAlt(), .T., i_bSecurityRecord)
			* leggo le impostazioni generali
			* Deve andare dopo la ricostruzione database (legge tabelle create appositamente per la gestione)
			* interfaccia (macro per evitare che il batch vada a finire nell'eseguibile)
			l_sPrg="GSUT_BEU.FXP"
			Do (l_sPrg) With .Null. , 'Avvio'
			* --- ZUCCHETTI AULLA FINE
	
			cp_AskUser()
			If i_codute<>0
				*--- Zucchetti Aulla Inizio - Send To...
				If Vartype(p_SendTo)='C' And p_SendTo="ST"
					_Screen.WindowState = 0
					Local l_batch
					l_batch = "GSUT_BSO(.null., 'IN', p_FileList)"
					&l_batch
					l_sPrg="GSUT_BCU"
					Do (l_sPrg)
					cp_CloseDatabase()
					ResetEnv()
					Return
				Endif
				*--- Zucchetti Aulla Fine - Send To...
				
				* --- logistica remota inizio
				If g_APPLICATION = "ADHOC REVOLUTION" And g_LORE="S"
					l_sPrg="CALPROGSED()"
					&l_sPrg
				Endif
				* --- logistica remota fine

				* --- Produzione Interface manager COMaCS Inside inizio
				If g_APPLICATION = "ad hoc ENTERPRISE" And g_DIBA="S" And g_SCCI="S"
					Local l_Comacs_func
						l_Comacs_func = "..\DIBA\VFCSSRC\GSSC_CLASS"
					Set Procedure To &l_Comacs_func Additive
				Endif
				* --- Produzione Interface manager COMaCS Inside fine

				* --- Build application menu
				If(Not g_UserScheduler)
					cp_backgroundmask()				
					cp_menu()
					*--- Zucchetti Aulla Inizio - Window Menu
					If g_bShowMenu And Type('g_UserScheduler')<>'L' Or (Type('g_UserScheduler')='L' And Not g_UserScheduler) &&Mostro menu quindi aggiorno i recenti
						*--- Ho gi� caricato le impostazioni della cfg interfaccia per utente
						If i_bWindowMenu
							GSUT_BMR('L')
						Endif
					Else
						*--- Nascondo il menu
						If i_VisualTheme<>-1
							i_MenuToolbar.Visible=.F.
						Else
							Hide Menu _Msysmenu
						Endif
					Endif
					*--- Zucchetti Aulla Fine - Window Menu

				Endif
				
				cp_desk()
				
				*--- Zucchetti Aulla Inizio - iRevolution
				If Vartype(p_EntryKey)=="C" And !Empty(p_EntryKey) And Vartype(g_REVI)=="C" And g_REVI=='S'
					Local l_batch
					l_batch = "revi_ddeserver(p_EntryKey)"
					&l_batch
				Endif
				*--- Zucchetti Aulla Fine - iRevolution
				
				WAIT Clear

				* Zucchetti Aulla Inizio: gestisce la chiusura dell'applicativo:
				* in caso di gestione aperta in Load/Edit chiede conferma chiusura
				Local l_read_ev, l_MESS, l_MESS1, l_Prog
				* Zucchetti Aulla Inizio: gestisce l'apertura automatica delle gestioni all'avvio
				l_sPrg="GSUT_BEU.FXP"
				Do (l_sPrg) With .Null. , 'Open'
				* Zucchetti Aulla Fine
				*--- Zucchetti Aulla Inizio - AGFA - Apertura attivit�
				If Vartype(p_SendTo)='C' And p_SendTo="PH" And g_AGFA='S'
					_Screen.WindowState = 0
					Local l_batch
					l_batch = "GSFA_BSK(.null., p_FileList, 'IN')"
					&l_batch
				Endif
				*--- Zucchetti Aulla Fine				
				
				l_Prog="OpenForm(.f.)"
				l_read_ev = .F.
				
				Do While Not l_read_ev
					Read Events
					l_read_ev=.T.
					l_MESS1 = &l_Prog
					l_MESS=CP_MSGFORMAT(MSG_MASK__OPENED_QM_CLOSE_ANYWAY_QP,l_MESS1,Chr(10))
					 If g_AVVISOUSCITA<>2 And Not Empty(l_MESS1) And Not(CP_YESNO(l_MESS,'',.F.) )
						l_read_ev=.F.
				        ELSE
				        *--- Zucchetti Aulla Inizio: verifico se i tool aperti hanno delle modifiche da salvare, e se devo eventualmente annullare la chiusura
				        i=1
				        Do While i<=_Screen.FormCount
				        IF TYPE("_Screen.Forms(i).exbtn")<>'U'
					     * memorizzo la variabile, dopo la exit perde il riferimento all'oggetto corrente
					     old_i=i
					      IF _Screen.Forms( i ).bnewstyle
						  _screen.Forms(i).exbtn.onclick()
						  ELSE
						  _screen.Forms(i).exbtn.click()
						  Endif
                         * se il tool � ancora aperto vuol dire che ho annullato la chiusura
						 IF TYPE("_Screen.Forms(old_i )")<>'U' AND VARTYPE(_Screen.Forms( old_i ).class)='C'  AND TYPE("_Screen.Forms(old_i).exbtn")<>'U'
					      l_read_ev=.F.
						Endif
						Exit
					   ENDIF
					   i=i+1
				       Enddo
				       *--- Zucchetti Aulla Fine
				    Endif
				ENDDO
				
				* Zucchetti Aulla Fine
				*--- Salvo lo stato delle toolbars
				* Gestione Uscita Back Up Automatico Db2 - inizio
				If Vartype(g_DB2Bck)='C'
					ResetEnv()
					Return
				Else
					* --- Lancio back up automatico
					l_sPrg="GSUT_BLB.FXP"
					Do (l_sPrg)
					If Vartype(g_DB2Bck)='C'
						ResetEnv()
						Return
					Endif
				ENDIF
				 * --- ZUCCHETTI AULLA FINE
				 * --- Program end
				 *--- Distruggo qui la toolbar perch� ho ancora bisogno delle connessione al db
				 If Vartype(odesktopbar) = 'O'
					odesktopbar.Destroy()
					odesktopbar = .Null.
				 Endif
				 * --- ZUCCHETTI AULLA Cancella utente da lista utente attivi
				 l_sPrg="GSUT_BCU"
				 Do (l_sPrg)
				 * --- ZUCCHETTI AULLA FINE
				 Release opostittimer
				* --- Release all forms. This is needed to save Post-INs before
				*     the main connection is closed
				i=_Screen.FormCount
				Do While i>0
					If TYPE('_Screen.Forms(i).BaseClass')='C' AND _Screen.Forms(i).BaseClass='Form'
						_Screen.Forms(i).Release()
					ENDIF
					i=i-1
				Enddo
			Else
				CP_MSG(CP_TRANSLATE(MSG_PROCEDURE_EXIT),.F.)
			ENDIF
			cp_CloseDatabase()
		Else
			**Se � stata impostata la terminazione silente prima di uscire attende 5 secondi (nel caso � attiva la terminazione silente si vuole impedire che lo scheduler service
			**ritenti immediatamente di rilanciare adhoc
			If Vartype(i_TERMINASILENTE)='C' And i_TERMINASILENTE='S'
				Local nSeconds
				nSeconds=Seconds()
				Do While Seconds()-nSeconds<=5
					*--- attendo passivamente
				Enddo
			Else
				CP_MSG(CP_TRANSLATE(MSG_CANNOT_OPEN_DATABASE) ,.F.)
			Endif
		Endif
		* --- ADHOC Inizio
	Else
		CP_MSG(CP_TRANSLATE(MSG_CANNOT_CONNECT_TO_DATABASE)+ Chr(13) + Chr(13)+ ;
			IIF(Empty(CP_DBTYPE),CP_TRANSLATE(MSG_DATABASE_TYPE_CL_NO_SPECIFIED),CP_MSGFORMAT(MSG_DATABASE_TYPE_CL__,CP_DBTYPE))+Chr(13)+;
			IIF(Empty(CP_ODBCCONN),CP_TRANSLATE(MSG_ODBC_CONNECTION_NAME_CL_NO_SPECIFIED),CP_MSGFORMAT(MSG_ODBC_CONNECTION_NAME_CL__,CP_ODBCCONN)),.F.,.F.,.F.,.F.)
	Endif
	* --- ADHOC Fine
	 ResetEnv()
	* --- Zucchetti Aulla Inizio - Controllo riavvio applicazione
Enddo
* --- Zucchetti Aulla Fine - Controllo riavvio applicazione

Return

Proc SetEnv()
	Set Talk Off
	Set Safety Off
	Set Deleted On
	Set Confirm On
	Set Escape On
	*set date italian && Zucchetti Aulla - gestione dell'internazionalizzazione
	Set Hours To 24
	Set Exclusive Off
	Set Multilock On
	Set Decimal To 6
	Set Status Bar Off
	Set Cpdialog Off
	Set NullDisplay To ''
	Set Escape Off
	Set Reprocess To Automatic
	*set sysmenu to _msm_view
	Release Pad _mview Of _Msysmenu
	*--- Zucchetti Aulla Inizio - Interfaccia
	Set Sysmenu To
	Set Sysmenu Off
	*--- Zucchetti Aulla Inizio - Interfaccia
	On Shutdown Clear Events
	* --- Zucchetti Aulla - non eseguo ora la getconfigfile perch� ho bisogno che venga trovata la cp_func
	* --- sar� eseguita dopo la set proc
	*GetConfigFile()
	* --- path e procedure
	cPath = "..\vfcsim"
	PUBLIC cPathVfcsim
	cPathVfcsim=cHomeDir+"..\vfcsim"
	*--- Zucchetti Aulla Inizio - Evito che il painter sovrascriva la variabile cPath
	cPath=cHomeDir+Iif(Directory("..\..\vfcsim"),"..\..\vfcsim","..\vfcsim")
	*--- Zucchetti Aulla Fine - Evito che il painter sovrascriva la variabile cPath
	cPath=Iif(!Empty(CP_PATH),CP_PATH,cPath)
	i_cBmpPath = cPath+'\'
	* --- ZUCCHETTI AULLA INIZIO
		Set Procedure To &cPathVfcsim\cp_lib.fxp Additive
		g_ORIPATH = '.\custom;..\vfcssrc;.\std;'+cPath
		g_XDCPATH = '.\custom;..\vfcsim;'
		If cp_fileexist(cHomeDir+"custom\cp_func.fxp",.F.)
			g_func = cHomeDir+'custom\cp_func.fxp'
		Else
			g_func = cHomeDir+'..\vfcssrc\cp_func.fxp'
		Endif
	Release Procedure &cPathVfcsim\cp_lib.fxp


	If Type('bLoadRuntimeConfig')='U'     && abilita/disabilita il runtime sui form
		Public bLoadRuntimeConfig
		bLoadRuntimeConfig=.F.
	Endif

	* --- Imposto il nuovo Layout per il disegnatore di Report
	#If Version(5)>=900
			* Set the system variable to use the fully-qualified path:
			*--- Zucchetti Aulla inizio - ReportBuilder modificato per non mostrare il warning draft mode preview
			*--- _REPORTBUILDER = cHomeDir+"ReportBuilder.App"
			_ReportBuilder = cHomeDir+"ah_ReportBuilder.App"
			_ReportOutput = cHomeDir+"ReportOutput.app"
			_ReportPreview = cHomeDir+"ReportPreview.app"
			Set REPORTBEHAVIOR 90
	#Endif

	* --- ZUCCHETTI AULLA inizio - evita di inserire cp_func nell'eseguibile
	g_vfcsim_cpfunc = Addbs(cPathVfcsim)+"cp_func.fxp"
	Set Procedure To &cPathVfcsim\cp_lib.fxp,&cPathVfcsim\cp_tbar.fxp,&cPathVfcsim\CP_FORMS.fxp,&cPathVfcsim\CP_BUTTON.fxp,&cPathVfcsim\CP_CTRLS.fxp,&cPathVfcsim\cp_zoom.fxp,&cPathVfcsim\cp_desk.fxp,&cPathVfcsim\cp_sec.fxp,&cPathVfcsim\cp_ppx.fxp,&cPathVfcsim\cp_dbadm.fxp,&cPathVfcsim\cp_dcx.fxp,&cPathVfcsim\CP_SQLX2.fxp,&cPathVfcsim\CP_CLASS.fxp
	Set Procedure To &cPathVfcsim\cp_themesmanager.fxp, &cPathVfcsim\cp_NavBar.fxp, &cPathVfcsim\cp_DockWnd.fxp, &cPathVfcsim\cp_monlib.fxp, &cPathVfcsim\cp_alertclass.fxp, &cPathVfcsim\cp_activitylogger.fxp, &g_vfcsim_cpfunc,&cPathVfcsim\vq_lib.fxp,&cPathVfcsim\cp_gridex.fxp Additive && Zucchetti Aulla inizio aggiunto cp_monlib e ..\vfcsim\cp_func
	Set Procedure To &cPathVfcsim\CP_BALLOON.fxp, &cPathVfcsim\cp_chprn.fxp, &cPathVfcsim\cp_chp.fxp, &cPathVfcsim\cp_chp3.fxp, &cPathVfcsim\cp_chpfun.fxp, &cPathVfcsim\cp_chpfun3.fxp, &cPathVfcsim\loadconfig.fxp, &cPathVfcsim\loadconfig3.fxp Additive && Zucchetti Aulla inizio aggiunto form e routine per la print system
	Set Classlib To &cPathVfcsim\stdz.vcx,&cPathVfcsim\FoxFeeds.vcx,&cPathVfcsim\FoxCharts.vcx,&cPathVfcsim\FoxCharts.vcx,_regexp.vcx  Additive
	Do &cPathVfcsim\System.App

	g_BZPROC=cHomeDir+IIF(cp_fileexist(cHomeDir+"custom\custom_class.fxp"),"custom\custom_class.fxp,","std\custom_class.fxp,")+cHomeDir+"..\vfcssrc\BlackBox.fxp,"+cHomeDir+IIF(cp_fileexist(cHomeDir+"custom\ztam_class.fxp"),"custom\ztam_class.fxp","std\ztam_class.fxp")
	Set Procedure To &g_func,&g_BZPROC Additive
	Set Procedure To &g_BZPROC Additive

	Set Path To .\Custom;..\vfcssrc;.\Std;&cPath

	* --- Zucchetti Aulla - Inizio - Creazione variabili per la gestione dell'ActivityLogger
	* --- Activity logger, setta variabili pubbliche
	oActivityLoggerSetup = Createobject("ActivityLoggerSetup")
	* --- Zucchetti Aulla - Fine - Creazione variabili per la gestione dell'ActivityLogger

	* --- Zucchetti Aulla - eseguo ora la getconfigfile perch� ho bisogno che venga trovata la cp_func
	GetConfigFile()
	* --- Release standard toolbar
	Deactivate Window "Color Palette"
	Deactivate Window "Database Designer"
	Deactivate Window "Form Controls"
	Deactivate Window "Form Designer"
	Deactivate Window "Layout"
	Deactivate Window "Print Preview"
	Deactivate Window "Query Designer"
	Deactivate Window "Report Controls"
	Deactivate Window "Report Designer"
	Deactivate Window "Standard"
	Deactivate Window "View Designer"
	* --- Function Keys
	Push Key
	On Key Label F1    Do cp_help
	i_help = ''
	On Key Label F2    Do cp_DoAction With "ecpPrint"
	On Key Label F3    Do cp_DoAction With "ecpEdit"
	On Key Label F4    Do cp_DoAction With "ecpLoad"
	On Key Label F5    Do cp_DoAction With "ecpDelete"
	On Key Label F6    Do cp_DoAction With "ecpF6"
	On Key Label F7    Do cp_DoAction With "ecpPrior"
	On Key Label F8    Do cp_DoAction With "ecpNext"
	On Key Label F9    Do cp_DoAction With "ecpZoom"
	*on key label Ctrl+F9    do cp_DoAction with "ecpZoomOnZoom" && Zucchetti Aulla disabilito il doppio F9
	On Key Label Alt+F9  Do vrt_addfield
	On Key Label F10   Do cp_DoAction With "ecpSave"
	On Key Label F11   Do cp_DoAction With "ecpRefresh"
	On Key Label F12   Do cp_DoAction With "ecpFilter"
	On Key Label Alt+F12   Do cp_DoAction With "ecpSecurity"
	On Key Label Ctrl+F12  Do vrt_build
	On Key Label Alt+F11   Do cp_DoAction With "ecpInfo"
	On Key Label PGUP  Do cp_DoAction With "ecpPgUp"
	On Key Label PGDN  Do cp_DoAction With "ecpPgDn"
	On Key Label ESC   Do cp_DoAction With "ecpQuit"
	On Key Label Ctrl+D Do cp_NavBar With .T.
	* controlli per sicurezza al livello di record
	* --- visualizzo lo zoom
	On Key Label Ctrl+F7 Do cp_recseczoom With i_curform
	* --- costruisco l'anagrafica dal  business object di riferimento
	On Key Label Alt+F7 Do cp_DoAction With "ecpSecurityRecord"
	* --- Zucchetti aulla schedulatore
	On Key Label Ctrl+F11  Do cp_DoAction With "ecpReadPar"
	* --- Zucchetti aulla sincronizzazione
	On Key Label Ctrl+F8 Do cp_DoAction With "ecpSinc"
	* --- Zucchetti aulla inizio (controllo flussi)
	On Key Label Ctrl+F5 Do cp_DoAction With "ecpCtrlFlus"
	On Key Label Alt+F5 Do cp_DoAction With "ecpViewFlus"
	ON KEY LABEL Ctrl+L Do SetToolBarVisible
	ON KEY LABEL Ctrl+S Do CallStartScreen
	* --- Zucchetti aulla fine (controllo flussi)
	*--- Zucchetti Aulla - Gadget
	ON KEY LABEL Ctrl+W Do cp_Gadget
	*--- Zucchetti Aulla - Gadget
	* --- Background
	Clear
		Do cp__logo.fxp

	*--- Interfaccia
	* --- imposto il path di memorizzazione delle immagini nella tempadhoc
	i_cTmpImg = Addbs(g_TEMPADHOC)+"cp_TmImgs"+Sys(2003)
	Public g_disableMenuItem, g_alertmanager
	g_disableMenuItem = 0
	*--- gestore temi
	If Not Vartype(_Screen.cp_themesmanager)=="O"
		_Screen.Newobject("cp_ThemesManager","cp_ThemesManager")
		Public i_ThemesManager
		i_ThemesManager = _Screen.cp_themesmanager
		If !(Vartype(i_bDisableNewPostIt)='L' And i_bDisableNewPostIt)
			g_alertmanager = Createobject("cp_alertmanager")
		Endif
		*--- Disattivo il menu
		Set Sysmenu Off
	Endif
	*--- OutLook Navigation Panel


	*--- Dimensione Toolbar
	i_nTBtnW = Iif(IsAlt(), 32, 24)
	i_nTBtnH = i_nTBtnW
	*--- Interfaccia

	*--- Toolbar
	Public oCpToolBar
	oCpToolBar=Createobject('CPToolBar', i_VisualTheme<>-1 And Vartype(_Screen.cp_themesmanager)=="O")
	oCpToolBar.Dock(i_nDeskTopBarPos)
	oCpToolBar.Show()
	
	*--- Oggetto per la memorizzazione della posizione delle form
	PUBLIC oMemoryFormPosition
	oMemoryFormPosition=CREATEOBJECT("cp_MemoryFormPosition")
	
	** copia i file dell help nell temp (alcuni S.O. non visualizzazno i file di help in rete)
	IF TYPE ("i_cDisableLocalHelp")<>"C"
		i_PathHelp=ALLTRIM(tempadhoc())+"\help\"
		IF NOT DIRECTORY(ALLTRIM(tempadhoc())+"\help")
			filesystemobj ("copyfolder",".\help\",i_PathHelp)
		ENDIF
	ENDIF 
	
	
	* --- Screen
	_Screen.Icon=i_cBmpPath+i_cStdIcon
	If Type('i_PathHelp')='C'
		If cp_fileexist(i_PathHELP+i_CpDic+'.hlp')
			Set Help To (i_PathHELP+i_CpDic)
		Endif
	Else
		If cp_fileexist(i_CpDic+'.hlp')
			Set Help To (i_CpDic)
		Endif
	Endif
	
	* --- ADHOC Inizio
	* --- Servizi MAPI
	Public oCPMapi
	oCPMapi=Createobject('CPMapi')
	If oCPMapi.mapiService=1
		Private w_actualerr
		w_actualerr = On ('ERROR')
		On Error oCPMapi.mapiService=-1
		oCPMapi.AddObject("oMessage","OLEControl","MSMAPI.MAPIMessages")
		oCPMapi.AddObject("oSession","OLEControl","MSMAPI.MAPISession")

		On Error &w_actualerr

	Endif
	*--- Zucchetti Aulla Inizio - Nuova cp_calendar
	If !(Upper(Set("Classlib"))$"DATEPICKER")
		Set Classlib To DatePicker Additive
	Endif
	*--- Zucchetti Aulla Fine - Nuova cp_calendar
	* --- ADHOC Fine
Endproc

PROCEDURE CallStartScreen
	cp_backgroundmask(.t.)
	Actionall('M')
ENDPROC

Proc GetConfigFile()
	Local h,l,F
	F=''
	* --- ADHOC Inizio
	If !Empty(p_FileCnf)
		F=Strtran(Lower(p_FileCnf),".cnf","")
		If !File(F+'.cnf')
			*---Traduzioni (la cp_lib non � ancora nell'elenco delle procedure richiamate, ma serve per la funzione cp_msgformat)
			Set Procedure To ..\vfcsim\cp_lib Additive
			cp_errormsg(CP_MSGFORMAT(MSG_CONFIGURATION_FILE__NOT_FOUND,F,Chr(13)),48, _Screen.Caption,.f.)
			F=''
		Endif
	Endif
	If Empty(F)
		If File(i_CpDic+'.cnf')
			F=i_CpDic
		Else
			* --- ADHOC Inizio
			* --- Cerca nella temp Locale
			If cp_fileexist(tempadhoc()+'\cp3start.cnf')
				F=tempadhoc()+'\cp3start'
			Else
				If cp_fileexist('cp3start.cnf')
					F='cp3start'
				Endif
			Endif

		Endif
	Endif
	If !Empty(F)
		* --- ADHOC Fine
		h=Fopen(F+'.cnf')
		Do While !Feof(h)
			l=Fgets(h)
			&l
		Enddo
		Fclose(h)
	Endif
	*--- Gestione riconnessione automatica
	i_cFileCNF=F+'.cnf'
	*--- Gestione riconnessione automatica
	*--- Zucchetti Aulla inizio - parametri connessione odbc per postgres
	If Vartype(CP_DBTYPE)='C' And CP_DBTYPE="PostgreSQL" And Vartype(CP_ODBCCONN)='C' And Upper(CP_ODBCCONN)="DRIVER={POSTGRESQL"
	    CP_ODBCCONN = CP_ODBCCONN+";"+ah_MakeODBCPar()
	Endif
	*--- Zucchetti Aulla fien - parametri connessione odbc per postgres
Endproc

Proc ResetEnv()
	On Error =.T.
	If Version(2)=0
	SET RESOURCE OFF
	ENDIF
	IF UPPER(JUSTFNAME(SYS(16,0)))<>'CP3START.FXP'
		KillProcess()
	ELSE
		Set Sysmenu To Defa
		Clear Events
		If Type('_screen.cnt')='O'
			* --- VFP6 can generate an error when clear all is executed if we do not remove
			* the background dialog window.
			_Screen.RemoveObject('cnt')
		Endif
		*** Zucchetti Aulla - gestione immagine desktop
		IF TYPE("_screen.bckImg")="O"
			UNBINDEVENTS(_SCREEN,"Resize",_screen.BCKIMG,"DesktopResize")
			_Screen.RemoveObject('bckimg')
		ENDIF
		IF TYPE("_screen.ahlogo")="O"
			_Screen.RemoveObject('ahlogo')
		ENDIF		
		If Vartype(_Screen.navBar)=="O"
			_Screen.RemoveObject("NavBar")
		Endif
		If Vartype(_Screen.ImgBackground)=="O"
			_Screen.RemoveObject("ImgBackground")
		Endif
		If Vartype(_Screen.TBMDI)=="O"
			_Screen.RemoveObject("TBMDI")
		Endif
		If Vartype(_Screen.cp_AlertManager)=='O'
			_Screen.RemoveObject("cp_AlertManager")
			Wait Clear
		Endif
		*--- Distruggo commandbar
		oCpToolBar.Destroy()
		oCpToolBar = .Null.
		i_MenuToolbar.Destroy()
		i_MenuToolbar = .Null.
		If Vartype(_Screen.cp_themesmanager)=="O"
			_Screen.RemoveObject("cp_ThemesManager")
		Endif
		* rilasciamo toolbar
		If Type('odesktopbar')='O'
			Release odesktopbar
		Endif
		If Type('OCPTOOLBAR')='O'
			Release oCpToolBar
		Endif
		
		*--- Zucchetti Aulla Inizio - iRevolution
		If Vartype(g_DDEServerName)=="C" And !Empty(g_DDEServerName)
			For l_i=1 To Alen(g_TopicNames,1)
				DDESetTopic(g_DDEServerName, g_TopicNames(m.l_i,1))
			Endfor
			DDESetService(g_DDEServerName, 'RELEASE')
		Endif
		*--- Zucchetti Aulla Fine - iRevolution
		
		* --- Zucchetti Aulla Inizio - Terminazione AHService
		If Vartype(i_DDEchannel)=="N" And i_DDEchannel>=0
			If Vartype(i_DDEChannel)='N'
				DDETerminate(i_DDEChannel)
			Else
				If Vartype(i_DDEhandle)='N'
					Declare Integer TerminateProcess in Win32API As TrPr Integer hProcess, Integer uExitCode
					TrPr(i_DDEhandle,0)
				Endif
			Endif
		Endif
		* --- Zucchetti Aulla Fine - Terminazione AHService
		
		* --- Zucchetti Aulla Inizio - Cancellazione immagini colorizzate temporanee
		If Vartype(i_TmpColImgCur)=="C" And !Empty(i_TmpColImgCur) And Used(i_TmpColImgCur)
			Local l_oldArea
			m.l_oldArea = Select()
			Select(i_TmpColImgCur)
			Go Top
			Scan
				Delete File (DstImg)
				If cp_FileExist(ForceExt(DstImg, 'msk'))
					Delete File (ForceExt(DstImg, 'msk'))
				Endif
				Select(i_TmpColImgCur)
			Endscan
			Use In Select(i_TmpColImgCur)
			Select(m.l_oldArea)
			Release m.l_oldArea
		Endif
		* --- Zucchetti Aulla Fine - Cancellazione immagini colorizzate temporanee
		
		* --- ZUCCHETTI AULLA INIZIO - uscita a seguito di Backup su DB2
		If (Type('g_DB2Bck')<>'C' Or Not Empty(g_DB2Bck)) And (Type('g_close_changeazi')<>'L' Or g_close_changeazi=.T.) And (Type("g_RIAVVIO")<>'L' Or Not g_riavvio)
			Clear All
		Endif
		* --- ZUCCHETTI AULLA FINE - uscita a seguito di Backup su DB2  * Zucchetti AULLA -Fine 8 Ottobre 2003 - Nicola
		Release All
		Close All
		On Error =.T.
		Set Help To
		Pop Key
		On Shutdown
		_Screen.Icon=''
		_Screen.Caption='Microsoft Visual FoxPro'
		On Error
		*--- Zucchetti Aulla Inizio - Nuova cp_calendar
		If Upper(Set("Classlib"))$"DATEPICKER"
			Release Classlib DatePicker
		Endif
		*--- Zucchetti Aulla Fine - Nuova cp_calendar
		If (Lower("vfp2c32.fll")$Lower(Set('LIBRARY')))
			*		release Library "vfp2c32.fll"
			Release Library vfp2c32
		Endif
		*--- Zucchetti Aulla Inizio - Libreria per la conversione cursore-Json e viceversa
		If Lower("cp_Json.fll")$Lower(Set('LIBRARY'))
			Release Library ..\vfcsim\cp_Json.fll
		Endif
		*--- Zucchetti Aulla Fine - Libreria per la conversione cursore-Json e viceversa
	ENDIF
Endproc

Proc AddModuleToPath()
	**************************************************
	* Zucchetti Aulla - Inserimento nel path della cartella custom\azienda
	* Non funziona dal cambio azienda perch� le classi caricate precedentemente restano in memoria
	
	If Type('g_NOCUSTOMAZI') = 'U'
		Local g_sp, g_findcustom
		g_sp=Set('path')
		g_findcustom='.\custom;'
		If Atc(g_findcustom,g_sp)>0
			g_sp=Right(g_sp,Len(g_sp)-Atc(g_findcustom,g_sp)+1)
			g_sp='.\CUSTOM\'+Alltrim(i_codazi)+';'+g_sp
			* --- ZUCCHETTI AULLA INIZIO
			If Occurs('.\CUSTOM\'+Alltrim(i_codazi)+';',g_XDCPATH) = 0
				g_XDCPATH = '.\CUSTOM\'+Alltrim(i_codazi)+';'+g_XDCPATH
			Endif
			* --- ZUCCHETTI AULLA FINE
			Set Path To &g_sp
		Endif
	Endif
	**************************************************
	If Type('i_cModules')='C' And Not(Empty(i_cModules))
		Local i_p,i_m, l_module
		*Verifico la presenza del modulo mobility, se presente lo elimino dalla varibile
		*Se sto effettuando l'ingresso in modalit� mobile e ho il modulo lo antepongo a tutti gli altri
		i_m=i_cModules
		i_p=At(',',i_m)
		Do While i_p<>0
			l_module = Left(i_m,i_p-1)
			If Upper(l_module)<>"VFCSIM" And (Upper(l_module)<>"MOBY" Or i_bMobileMode) &&Escludo modulo vfcsim e modulo mobile per non averli due volte nel path
				AddModuleToPath1(cp_FindModule(m.l_module), Upper(l_module)=="MOBY")
			Endif
			i_m=Substr(i_m,i_p+1)
			i_p=At(',',i_m)
		Enddo
		If Upper(i_m)<>"VFCSIM" And (Upper(i_m)<>"MOBY" Or i_bMobileMode)   &&Escludo modulo vfcsim e modulo mobile per non averli due volte nel path
			AddModuleToPath1(cp_FindModule(i_m), Upper(i_m)=="MOBY")
		Endif
		* --- ZUCCHETTI AULLA INIZIO
		If i_lcheckaddon
			Local i_sp
			i_sp=Set('path')
			If At('..\PCON',i_sp)>0
				i_sp=Strtran(i_sp,';..\VFCSSRC')
				i_sp=Left(i_sp,At('..\PCON',i_sp)-1)+'..\VFCSSRC;'+Substr(i_sp,At('..\PCON',i_sp))
				Set Path To &i_sp
			Endif
		Endif
		* --- ZUCCHETTI AULLA FINE
	Endif
Endproc

Proc AddModuleToPath1(i_mp, i_bSetPathAsFirst)
	Local i_r, i_folder
	i_r=Rat('\',i_mp)
	If i_r<>0
		i_mp=Left(i_mp,i_r-1)
		If Lower(Right(i_mp,4))='\exe'
			***** Zucchetti Tam Inizio
				i_folder='..\'
			SetPathAdhoc(i_mp,i_folder, i_bSetPathAsFirst)
			***** Zucchetti Tam Fine
		Else
			If i_bSetPathAsFirst
				Set Path To i_mp+';'+Set('path')
				Set Proc To i_mp+'\CP_FUNC'+';'+Set('proc')
			Else
				Set Path To Set('path')+';'+i_mp
				Set Proc To i_mp+'\CP_FUNC' Additive
			Endif

		Endif
	Endif
Endproc

Procedure SetPathAdhoc(i_mp,i_folder, i_bSetPathAsFirst)
	Local i_clocalmp
	i_clocalmp=Left(i_mp,Len(i_mp)-4)
	If i_bSetPathAsFirst
		Set Path To Iif(Left(i_clocalmp,3)<>'..\',i_folder,'')+i_clocalmp+'\VFCSSRC'+';'+Set('path')
	Else
		Set Path To Set('path')+';'+Iif(Left(i_clocalmp,3)<>'..\',i_folder,'')+i_clocalmp+'\VFCSSRC'
	Endif
	* set proc to i_clocalmp+'\VFCSSRC\CP_FUNC' additive
	If cp_fileexist(Iif(Left(i_clocalmp,3)<>'..\',i_folder,cHomeDir)+i_clocalmp+'\VFCSSRC\CP_FUNC.FXP',.T.)
		If cp_fileexist(cHomeDir+"custom\"+i_clocalmp+"\CP_FUNC.FXP")
			If i_bSetPathAsFirst
				Set Proc To cHomeDir+"custom\"+i_clocalmp+"\CP_FUNC.FXP"+";"+Set('proc')
			Else
				Set Proc To cHomeDir+"custom\"+i_clocalmp+"\CP_FUNC.FXP" Additive
			Endif
		Else
			If i_bSetPathAsFirst
				Set Proc To Iif(Left(i_clocalmp,3)<>'..\',i_folder,cHomeDir)+i_clocalmp+'\VFCSSRC\CP_FUNC.FXP'+";"+Set('proc')
			Else
				Set Proc To Iif(Left(i_clocalmp,3)<>'..\',i_folder,cHomeDir)+i_clocalmp+'\VFCSSRC\CP_FUNC.FXP' Additive
			Endif
		Endif
	Endif
Endproc

Procedure ReadAppParam(i_xParm1,i_xParm2,i_xParm3,i_xParm4,i_xParm5)
	Local i_n,i_i
	For i_i=1 To 5
		i_n='i_xParm'+Alltrim(Str(i_i))
		If Type(i_n)='C' And Left(&i_n,5)='-sso='
			Public i_cSsoID,i_xSsoCredentials
			i_cSsoID=Substr(&i_n,6)
		Endif
		If Type(i_n)='C' And Left(&i_n,6)='-ssoc='
			Public i_xSsoCredentials
			i_xSsoCredentials=Substr(&i_n,7)
		Endif
	Next
	Return

Procedure cp_ChkError(pErr,pMsg,pPrg,pLine)
	Local chkError, L_nTotStack, L_sMsgStack, L_nStackProcessed, l_macro
	bTrsErr=.F.
	=cp_TrsError(m.pErr,m.pMsg,m.pPrg,m.pLine)
	i_TrsMsg=Iif(Vartype(m.i_TrsMsg)<>'C', Message(), m.i_TrsMsg)
	Astackinfo(aStackArr)
	L_nTotStack = Alen(aStackArr,1) - 1
	L_nStackProcessed = 0
	L_sMsgStack = ""
	Do While L_nStackProcessed<5 And L_nTotStack>0
		L_sMsgStack = L_sMsgStack + ah_MsgFormat("%0File:%1%0Procedura:%2%0Riga %3: %4%0%5", Alltrim(aStackArr[L_nTotStack,4]), ;
			ALLTRIM(aStackArr[L_nTotStack,3]), Alltrim(Str(aStackArr[L_nTotStack,5])), Alltrim(aStackArr[L_nTotStack,6]), ;
			IIF(L_nStackProcessed<4, Repl(Chr(45),50), "") )
		L_nTotStack = L_nTotStack - 1
		L_nStackProcessed = L_nStackProcessed + 1
	Enddo
	Release aStackArr
	If Version(2)>0 Or (Vartype(g_bVerboseErrorLog)<>'U' And g_bVerboseErrorLog)
		If ah_YesNo("%1%2%0Si vuole interrompere l'esecuzione del programma?",48,m.i_TrsMsg, L_sMsgStack )
			If Version(2)>0
				l_macro="de"+"bu"+"g"
				&l_macro
				l_macro="su"+"sp"+"end"
				&l_macro
			Else
				KillProcess()
			Endif
		Endif
	Else
		If ah_YesNo("%1%0Si vuole terminare l'applicazione?",48,m.i_TrsMsg)
			KillProcess()
		Endif
	Endif
	Return
Endproc

*==============================================================================
* Program:   KillProcess
* Returns:   .T. if it succeeded
* Environment in: none
* Environment out: if successful, the application has been terminated
*==============================================================================
Function KillProcess()

	Local lnhWnd, ;
		llReturn, ;
		lnProcessID, ;
		lnHandle

	* Declare the Win32API functions we need.

	#Define WM_DESTROY 0x0002
	Declare Integer FindWindow In Win32API ;
		string @cClassName, String @cWindowName
	Declare Integer SendMessage In Win32API ;
		integer HWnd, Integer uMsg, Integer wParam, Integer Lparam
	Declare Sleep In Win32API ;
		integer nMilliseconds
	Declare Integer GetWindowThreadProcessId In Win32API ;
		integer HWnd, Integer @lpdwProcessId
	Declare Integer OpenProcess In Win32API ;
		integer dwDesiredAccess, Integer bInheritHandle, Integer dwProcessID
	Declare Integer TerminateProcess In Win32API ;
		integer hProcess, Integer uExitCode

	*** elimino l'utente dalla lista utenti attivi se riesco
	Try
		gsut_bcu()
	Catch
	Endtry

	* Get a handle to the window by its caption.

	lnhWnd   = _Screen.HWnd &&FindWindow(0, tcCaption)
	llReturn = lnhWnd = 0

	* If we found the window, send a "destroy" message to it, then wait for it to
	* be gone. If it didn't, let's use the big hammer: we'll terminate its process.

	If Not llReturn
		SendMessage(lnhWnd, WM_DESTROY, 0, 0)
		lnProcessID = 0
		GetWindowThreadProcessId(lnhWnd, @lnProcessID)
		lnHandle = OpenProcess(1, 1, lnProcessID)
		llReturn = TerminateProcess(lnHandle, 0) > 0
	Endif Not llReturn
	Return llReturn

* --- ZUCCHETTI AULLA INIZIO - parametri di connessione ODBC per Postgres
Function ah_MakeODBCPar() As String
	Local indp,i_cRes
	If Vartype(g_ODBC_Postgres)="C" And Not Empty(g_ODBC_Postgres)
		i_cRes = g_ODBC_Postgres
	Else
		i_cRes=""
		For indp=1 To Alen(i_PGCONN,1)
			If Not Empty(i_PGCONN[indp,1]) And Not Empty(i_PGCONN[indp,3])
				i_cRes = i_cRes + i_PGCONN[indp,1] + i_PGCONN[indp,3] + ";"
			Endif
		Next
	Endif
	Return i_cRes
Endfunc

Function ah_CheckODBCPar(cStrConn As String) As String
	Local i_bOK,indp,i_cRes,i_cPar,i_cVal,i_npos,i_cx,icx,i_bcx,i_nPP,i_bPP,i_pCX
	i_cRes = ""
	indp = 0
	i_bOK = .T.
	cStrConn = cStrConn +";"
	Do While indp<Alen(i_PGCONN,1)
		indp = indp + 1
		i_cPar = i_PGCONN[indp,2]
		i_cVal = i_PGCONN[indp,3]
		If Not Empty(i_cPar) And Not Empty(i_cVal)
			i_npos = At(i_cPar,cStrConn)
			If i_npos=0
				i_cRes = "Parametro <"+i_cPar+"> non trovato nella stringa di connessione."
				i_bOK = .F.
				indp = 999
			Else
				i_cPP = Substr(cStrConn,i_npos+Len(i_cPar))
				i_cPP = Left(i_cPP, At(";",i_cPP)-1)
				i_pCX = i_cPP
				If i_cPar="CX="
					* i_cval = "nbbbbbbbb" prendo l n-esimo carattere da dx del parametro CX
					* eseguo il confronto bit a bit sul carattere trovato
					i_cPP = Left(Right(i_cPP, Val(Left(i_cVal,1))) ,1)
					i_nPP = Evaluate("0x"+i_cPP)
					For icx=2 To 9
						i_cx = Substr(i_cVal,icx,1)
						If i_cx $ "01"
							i_bcx = (i_cx="1")
							i_bPP = Bittest(i_nPP,9-icx)
							i_bOK = i_bOK And (i_bcx=i_bPP)
						Endif
					Next
				Else
					i_bOK = (i_cVal==i_cPP)
				Endif
				if not i_bOK
					i_cRes = i_cRes  + i_PGCONN[indp,4]
					i_bOK = .T.
				Endif
			Endif
		Endif
	Enddo
	If i_bOK AND (Len(i_pCX)<8 Or Substr(i_pCX,Len(i_pCX)-5,1)<>"1")
		* parametro CX, ShowSystemTables deve essere On (Valore 1) : 6� carattere da dx se lunghezza>7
		i_cRes = i_cRes + "%0 <Show System Tables> errato, attivare flag"
	Endif
	i_cRes = ah_msgformat(i_cRes)
	Return i_cRes
Endfunc
* --- ZUCCHETTI AULLA FINE - parametri di connessione ODBC per Postgres
