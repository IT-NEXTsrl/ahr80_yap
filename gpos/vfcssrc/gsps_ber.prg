* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_ber                                                        *
*              Generazione rischio negozio                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-30                                                      *
* Last revis.: 2003-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_ber",oParentObject)
return(i_retval)

define class tgsps_ber as StdBatch
  * --- Local variables
  w_TIPCLI = space(1)
  w_DATRIF = ctod("  /  /  ")
  w_CODCLI = space(15)
  w_GIORIS = 0
  w_DATULT = ctod("  /  /  ")
  w_CLCODCLI = space(15)
  w_CLFIDUTI = 0
  * --- WorkFile variables
  CLI_VEND_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Rischio Negozio (da GSPS_KER)
    this.w_TIPCLI = "C"
    this.w_DATRIF = this.oParentObject.w_NDATRIF
    this.w_GIORIS = this.oParentObject.w_NGIORIS
    this.w_DATULT = this.oParentObject.w_NDATULT
    this.w_CODCLI = SPACE(15)
    * --- Legge eventuale Cliente Azienda di Selezione
    if NOT EMPTY(this.oParentObject.w_CODNOM)
      * --- Read from CLI_VEND
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLCODCON"+;
          " from "+i_cTable+" CLI_VEND where ";
              +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_CODNOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLCODCON;
          from (i_cTable) where;
              CLCODCLI = this.oParentObject.w_CODNOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCON),cp_NullValue(_read_.CLCODCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Lancia Elaborazione Rischio (Parametro P: previene messaggi a video)
    gsve_ber(this,"P")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if USED("TMPRISC")
      SELECT TMPRISC
      USE
    endif
    ah_Msg("Azzera rischio clienti negozio...",.T.)
    if NOT EMPTY(this.oParentObject.w_CODNOM)
      * --- Write into CLI_VEND
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLFIDUTI ="+cp_NullLink(cp_ToStrODBC(0),'CLI_VEND','CLFIDUTI');
            +i_ccchkf ;
        +" where ";
            +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_CODNOM);
               )
      else
        update (i_cTable) set;
            CLFIDUTI = 0;
            &i_ccchkf. ;
         where;
            CLCODCLI = this.oParentObject.w_CODNOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into CLI_VEND
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLFIDUTI ="+cp_NullLink(cp_ToStrODBC(0),'CLI_VEND','CLFIDUTI');
            +i_ccchkf ;
        +" where ";
            +"CLFIDUTI <> "+cp_ToStrODBC(0);
               )
      else
        update (i_cTable) set;
            CLFIDUTI = 0;
            &i_ccchkf. ;
         where;
            CLFIDUTI <> 0;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    ah_Msg("Ricerca rischio clienti azienda e corrispettivi non generati...",.T.)
    vq_exec("..\GPOS\EXE\QUERY\GSPS_BER.VQR", this, "TMPRISC")
    if USED("TMPRISC")
      ah_Msg("Aggiorna rischio clienti azienda e corrispettivi non generati...",.T.)
      SELECT TMPRISC
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CODCLI, ""))
      this.w_CLCODCLI = CODCLI
      this.w_CLFIDUTI = NVL(TOTIMP, 0)
      * --- Write into CLI_VEND
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLFIDUTI =CLFIDUTI+ "+cp_ToStrODBC(this.w_CLFIDUTI);
            +i_ccchkf ;
        +" where ";
            +"CLCODCLI = "+cp_ToStrODBC(this.w_CLCODCLI);
               )
      else
        update (i_cTable) set;
            CLFIDUTI = CLFIDUTI + this.w_CLFIDUTI;
            &i_ccchkf. ;
         where;
            CLCODCLI = this.w_CLCODCLI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      SELECT TMPRISC
      ENDSCAN
      SELECT TMPRISC
      USE
    endif
    ah_Msg("Ricerca documenti corrispettivi generati...",.T.)
    vq_exec("..\GPOS\EXE\QUERY\GSPS2BER.VQR", this, "TMPRISC")
    if USED("TMPRISC")
      ah_Msg("Aggiorna documenti corrispettivi generati...",.T.)
      SELECT TMPRISC
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CODCLI, ""))
      this.w_CLCODCLI = CODCLI
      this.w_CLFIDUTI = NVL(TOTIMP1, 0)+NVL(TOTIMP2, 0)+NVL(TOTIMP3, 0)+NVL(TOTIMP4, 0)+NVL(TOTIMP5, 0)+NVL(TOTIMP6, 0)
      this.w_CLFIDUTI = this.w_CLFIDUTI + NVL(TOTIVA1, 0)+NVL(TOTIVA2, 0)+NVL(TOTIVA3, 0)+NVL(TOTIVA4, 0)+NVL(TOTIVA5, 0)+NVL(TOTIVA6, 0)
      this.w_CLFIDUTI = this.w_CLFIDUTI - NVL(TOTACC, 0)
      * --- Write into CLI_VEND
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLFIDUTI =CLFIDUTI+ "+cp_ToStrODBC(this.w_CLFIDUTI);
            +i_ccchkf ;
        +" where ";
            +"CLCODCLI = "+cp_ToStrODBC(this.w_CLCODCLI);
               )
      else
        update (i_cTable) set;
            CLFIDUTI = CLFIDUTI + this.w_CLFIDUTI;
            &i_ccchkf. ;
         where;
            CLCODCLI = this.w_CLCODCLI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      SELECT TMPRISC
      ENDSCAN
      SELECT TMPRISC
      USE
    endif
    if NOT EMPTY(this.oParentObject.w_CODNOM)
      ah_ErrorMsg("Generazione rischio negozio, cliente: %1 completata",,"", ALLTRIM(this.oParentObject.w_CODNOM) )
    else
      ah_ErrorMsg("Generazione rischio negozio completata",,"")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CLI_VEND'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
