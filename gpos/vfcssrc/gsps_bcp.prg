* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bcp                                                        *
*              Caricamento da clienti azienda                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_62]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-17                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODICE,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bcp",oParentObject,m.pCODICE,m.pTIPO)
return(i_retval)

define class tgsps_bcp as StdBatch
  * --- Local variables
  pCODICE = space(15)
  pTIPO = space(1)
  w_CODCLI = space(15)
  w_CODICE = space(15)
  w_ANDESCRI = space(40)
  w_ANINDIRI = space(35)
  w_AN___CAP = space(8)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANNAZION = space(3)
  w_ANPARIVA = space(12)
  w_ANCODFIS = space(16)
  w_ANTELEFO = space(18)
  w_ANNUMCEL = space(18)
  w_ANTELFAX = space(18)
  w_ANINDWEB = space(50)
  w_AN_EMAIL = space(50)
  w_AN_EMPEC = space(50)
  w_ANDTINVA = ctod("  /  /  ")
  w_ANDTOBSO = ctod("  /  /  ")
  w_ANCATCOM = space(3)
  w_ANCATSCM = space(5)
  w_ANCODPAG = space(5)
  w_ANCODLIS = space(5)
  w_AN1SCONT = 0
  w_AN2SCONT = 0
  w_ANFLFIDO = space(1)
  w_MSG = space(200)
  w_DESCLI = space(40)
  w_NEWCOD = space(15)
  w_NEWDES = space(40)
  w_FLCPOS = space(1)
  w_CLCODCLI = space(15)
  w_OK = space(1)
  w_ANVALFID = 0
  w_PITROV = space(1)
  * --- WorkFile variables
  CONTI_idx=0
  CLI_VEND_idx=0
  PAR_VDET_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da batch GSAR_BPS con parametro Codice Cliente
    * --- Carica nell'archivioo Clienti Negoizo i Clienti Azienda con Flag Cliente Pos attivo.
    this.w_CLCODCLI = SPACE(15)
    this.w_FLCPOS = " "
    this.w_MSG = " "
    this.w_CODICE = this.pCODICE
    this.w_NEWCOD = " "
    this.w_NEWDES = " "
    this.w_OK = "F"
    do case
      case this.pTIPO="C"
        * --- Caricamento nuovo cliente
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO="E"
        * --- Write into CLI_VEND
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CLI_VEND_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCODCON ="+cp_NullLink(cp_ToStrODBC(" "),'CLI_VEND','CLCODCON');
              +i_ccchkf ;
          +" where ";
              +"CLCODCON = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              CLCODCON = " ";
              &i_ccchkf. ;
           where;
              CLCODCON = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.pTIPO="V"
        if this.oParentObject.w_OLDCLPOS="S" AND this.oParentObject.w_ANCLIPOS<>"S"
          * --- Disattivato check cliente pos su un cliente azienda: elimino il riferimento al cliente sui clienti negozio
          * --- Write into CLI_VEND
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCODCON ="+cp_NullLink(cp_ToStrODBC(" "),'CLI_VEND','CLCODCON');
                +i_ccchkf ;
            +" where ";
                +"CLCODCON = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                CLCODCON = " ";
                &i_ccchkf. ;
             where;
                CLCODCON = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Controllo se esistono gi� clienti negozio con riferimento al cliente
          * --- Read from CLI_VEND
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLCODCLI"+;
              " from "+i_cTable+" CLI_VEND where ";
                  +"CLCODCON = "+cp_ToStrODBC(this.w_CODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLCODCLI;
              from (i_cTable) where;
                  CLCODCON = this.w_CODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCLI),cp_NullValue(_read_.CLCODCLI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0
            * --- Se non esistono carico un nuovo cliente negozio
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Se esistono aggiorno i clienti negozio con lo stesso riferimento al cliente azienda
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANTELEFO,ANTELFAX,ANNUMCEL,ANCODFIS,ANPARIVA,ANCODPAG,ANNUMLIS,ANCATCOM,ANCATSCM,ANINDWEB,AN_EMAIL,AN_EMPEC,ANDTINVA,ANDTOBSO,AN1SCONT,AN2SCONT,ANVALFID,ANFLFIDO"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC("C");
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANTELEFO,ANTELFAX,ANNUMCEL,ANCODFIS,ANPARIVA,ANCODPAG,ANNUMLIS,ANCATCOM,ANCATSCM,ANINDWEB,AN_EMAIL,AN_EMPEC,ANDTINVA,ANDTOBSO,AN1SCONT,AN2SCONT,ANVALFID,ANFLFIDO;
                from (i_cTable) where;
                    ANTIPCON = "C";
                    and ANCODICE = this.w_CODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
              this.w_ANINDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
              this.w_AN___CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
              this.w_ANLOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
              this.w_ANPROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
              this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
              this.w_ANTELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
              this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
              this.w_ANNUMCEL = NVL(cp_ToDate(_read_.ANNUMCEL),cp_NullValue(_read_.ANNUMCEL))
              this.w_ANCODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
              this.w_ANPARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
              this.w_ANCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
              this.w_ANCODLIS = NVL(cp_ToDate(_read_.ANNUMLIS),cp_NullValue(_read_.ANNUMLIS))
              this.w_ANCATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
              this.w_ANCATSCM = NVL(cp_ToDate(_read_.ANCATSCM),cp_NullValue(_read_.ANCATSCM))
              this.w_ANINDWEB = NVL(cp_ToDate(_read_.ANINDWEB),cp_NullValue(_read_.ANINDWEB))
              this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
              this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
              this.w_ANDTINVA = NVL(cp_ToDate(_read_.ANDTINVA),cp_NullValue(_read_.ANDTINVA))
              this.w_ANDTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
              this.w_AN1SCONT = NVL(cp_ToDate(_read_.AN1SCONT),cp_NullValue(_read_.AN1SCONT))
              this.w_AN2SCONT = NVL(cp_ToDate(_read_.AN2SCONT),cp_NullValue(_read_.AN2SCONT))
              this.w_ANVALFID = NVL(cp_ToDate(_read_.ANVALFID),cp_NullValue(_read_.ANVALFID))
              this.w_ANFLFIDO = NVL(cp_ToDate(_read_.ANFLFIDO),cp_NullValue(_read_.ANFLFIDO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_AN_EMAIL = LEFT(this.w_AN_EMAIL,40)
            this.w_AN_EMPEC = LEFT(this.w_AN_EMPEC,40)
            * --- Write into CLI_VEND
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CLI_VEND_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'CLI_VEND','CLDESCRI');
              +",CL___CAP ="+cp_NullLink(cp_ToStrODBC(this.w_AN___CAP),'CLI_VEND','CL___CAP');
              +",CLLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_ANLOCALI),'CLI_VEND','CLLOCALI');
              +",CLPROVIN ="+cp_NullLink(cp_ToStrODBC(this.w_ANPROVIN),'CLI_VEND','CLPROVIN');
              +",CLNAZION ="+cp_NullLink(cp_ToStrODBC(this.w_ANNAZION),'CLI_VEND','CLNAZION');
              +",CLINDIRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANINDIRI),'CLI_VEND','CLINDIRI');
              +",CLTELEFO ="+cp_NullLink(cp_ToStrODBC(this.w_ANTELEFO),'CLI_VEND','CLTELEFO');
              +",CLTELFAX ="+cp_NullLink(cp_ToStrODBC(this.w_ANTELFAX),'CLI_VEND','CLTELFAX');
              +",CLNUMCEL ="+cp_NullLink(cp_ToStrODBC(this.w_ANNUMCEL),'CLI_VEND','CLNUMCEL');
              +",CL_EMAIL ="+cp_NullLink(cp_ToStrODBC(this.w_AN_EMAIL),'CLI_VEND','CL_EMAIL');
              +",CL_EMPEC ="+cp_NullLink(cp_ToStrODBC(this.w_AN_EMPEC),'CLI_VEND','CL_EMPEC');
              +",CLPARIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANPARIVA),'CLI_VEND','CLPARIVA');
              +",CLCODFIS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODFIS),'CLI_VEND','CLCODFIS');
              +",CLCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODPAG),'CLI_VEND','CLCODPAG');
              +",CLCATCOM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATCOM),'CLI_VEND','CLCATCOM');
              +",CLCATSCM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATSCM),'CLI_VEND','CLCATSCM');
              +",CLSCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_AN1SCONT),'CLI_VEND','CLSCONT1');
              +",CLSCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_AN2SCONT),'CLI_VEND','CLSCONT2');
              +",CLCODLIS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODLIS),'CLI_VEND','CLCODLIS');
              +",CLDTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANDTINVA),'CLI_VEND','CLDTINVA');
              +",CLDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_ANDTOBSO),'CLI_VEND','CLDTOBSO');
              +",CLVALFID ="+cp_NullLink(cp_ToStrODBC(this.w_ANVALFID),'CLI_VEND','CLVALFID');
              +",CLFLFIDO ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLFIDO),'CLI_VEND','CLFLFIDO');
                  +i_ccchkf ;
              +" where ";
                  +"CLCODCON = "+cp_ToStrODBC(this.w_CODICE);
                     )
            else
              update (i_cTable) set;
                  CLDESCRI = this.w_ANDESCRI;
                  ,CL___CAP = this.w_AN___CAP;
                  ,CLLOCALI = this.w_ANLOCALI;
                  ,CLPROVIN = this.w_ANPROVIN;
                  ,CLNAZION = this.w_ANNAZION;
                  ,CLINDIRI = this.w_ANINDIRI;
                  ,CLTELEFO = this.w_ANTELEFO;
                  ,CLTELFAX = this.w_ANTELFAX;
                  ,CLNUMCEL = this.w_ANNUMCEL;
                  ,CL_EMAIL = this.w_AN_EMAIL;
                  ,CL_EMPEC = this.w_AN_EMPEC;
                  ,CLPARIVA = this.w_ANPARIVA;
                  ,CLCODFIS = this.w_ANCODFIS;
                  ,CLCODPAG = this.w_ANCODPAG;
                  ,CLCATCOM = this.w_ANCATCOM;
                  ,CLCATSCM = this.w_ANCATSCM;
                  ,CLSCONT1 = this.w_AN1SCONT;
                  ,CLSCONT2 = this.w_AN2SCONT;
                  ,CLCODLIS = this.w_ANCODLIS;
                  ,CLDTINVA = this.w_ANDTINVA;
                  ,CLDTOBSO = this.w_ANDTOBSO;
                  ,CLVALFID = this.w_ANVALFID;
                  ,CLFLFIDO = this.w_ANFLFIDO;
                  &i_ccchkf. ;
               where;
                  CLCODCON = this.w_CODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento nuovo cliente negozio
    * --- Lettura tipo codifica cliente numerica/alfanumerica
    * --- Read from PAR_VDET
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_VDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAFLCPOS"+;
        " from "+i_cTable+" PAR_VDET where ";
            +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and PACODNEG = "+cp_ToStrODBC(g_CODNEG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAFLCPOS;
        from (i_cTable) where;
            PACODAZI = i_CODAZI;
            and PACODNEG = g_CODNEG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLCPOS = NVL(cp_ToDate(_read_.PAFLCPOS),cp_NullValue(_read_.PAFLCPOS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura valori del cliente in azienda da copiare nell'archivio clienti negozio
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANTELEFO,ANTELFAX,ANNUMCEL,ANCODFIS,ANPARIVA,ANCODPAG,ANNUMLIS,ANCATCOM,ANCATSCM,ANINDWEB,AN_EMAIL,AN_EMPEC,ANDTINVA,ANDTOBSO,AN1SCONT,AN2SCONT,ANVALFID,ANFLFIDO"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC("C");
            +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANTELEFO,ANTELFAX,ANNUMCEL,ANCODFIS,ANPARIVA,ANCODPAG,ANNUMLIS,ANCATCOM,ANCATSCM,ANINDWEB,AN_EMAIL,AN_EMPEC,ANDTINVA,ANDTOBSO,AN1SCONT,AN2SCONT,ANVALFID,ANFLFIDO;
        from (i_cTable) where;
            ANTIPCON = "C";
            and ANCODICE = this.w_CODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
      this.w_ANINDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
      this.w_AN___CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
      this.w_ANLOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
      this.w_ANPROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
      this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
      this.w_ANTELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
      this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
      this.w_ANNUMCEL = NVL(cp_ToDate(_read_.ANNUMCEL),cp_NullValue(_read_.ANNUMCEL))
      this.w_ANCODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
      this.w_ANPARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
      this.w_ANCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
      this.w_ANCODLIS = NVL(cp_ToDate(_read_.ANNUMLIS),cp_NullValue(_read_.ANNUMLIS))
      this.w_ANCATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
      this.w_ANCATSCM = NVL(cp_ToDate(_read_.ANCATSCM),cp_NullValue(_read_.ANCATSCM))
      this.w_ANINDWEB = NVL(cp_ToDate(_read_.ANINDWEB),cp_NullValue(_read_.ANINDWEB))
      this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
      this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
      this.w_ANDTINVA = NVL(cp_ToDate(_read_.ANDTINVA),cp_NullValue(_read_.ANDTINVA))
      this.w_ANDTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
      this.w_AN1SCONT = NVL(cp_ToDate(_read_.AN1SCONT),cp_NullValue(_read_.AN1SCONT))
      this.w_AN2SCONT = NVL(cp_ToDate(_read_.AN2SCONT),cp_NullValue(_read_.AN2SCONT))
      this.w_ANVALFID = NVL(cp_ToDate(_read_.ANVALFID),cp_NullValue(_read_.ANVALFID))
      this.w_ANFLFIDO = NVL(cp_ToDate(_read_.ANFLFIDO),cp_NullValue(_read_.ANFLFIDO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PITROV = .F.
    if NOT EMPTY(this.w_ANPARIVA) 
      * --- Controllo su  partita iva
      * --- Read from CLI_VEND
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLCODCLI"+;
          " from "+i_cTable+" CLI_VEND where ";
              +"CLPARIVA = "+cp_ToStrODBC(this.w_ANPARIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLCODCLI;
          from (i_cTable) where;
              CLPARIVA = this.w_ANPARIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCLI),cp_NullValue(_read_.CLCODCLI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0 AND NOT EMPTY(this.w_ANPARIVA)
        this.w_MSG = "Cliente con stessa partita IVA gi� presente tra i clienti negozio%0Si desidera considerarli come un unico cliente?"
        if ah_YesNo(this.w_MSG)
          * --- Write into CLI_VEND
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'CLI_VEND','CLCODCON');
                +i_ccchkf ;
            +" where ";
                +"CLCODCLI = "+cp_ToStrODBC(this.w_CODCLI);
                   )
          else
            update (i_cTable) set;
                CLCODCON = this.w_CODICE;
                &i_ccchkf. ;
             where;
                CLCODCLI = this.w_CODCLI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_PITROV = .T.
          i_retcode = 'stop'
          return
        else
          * --- Write into CONTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANCLIPOS');
                +i_ccchkf ;
            +" where ";
                +"ANTIPCON = "+cp_ToStrODBC("C");
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                ANCLIPOS = " ";
                &i_ccchkf. ;
             where;
                ANTIPCON = "C";
                and ANCODICE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if NOT EMPTY(this.w_ANCODFIS) and NOT this.w_PITROV
      * --- Controllo su  codice Fiscale
      * --- Read from CLI_VEND
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLCODCLI"+;
          " from "+i_cTable+" CLI_VEND where ";
              +"CLCODFIS = "+cp_ToStrODBC(this.w_ANCODFIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLCODCLI;
          from (i_cTable) where;
              CLCODFIS = this.w_ANCODFIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCLI),cp_NullValue(_read_.CLCODCLI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_MSG = "Cliente con stesso codice fiscale gi� presente tra i clienti negozio%0Si desidera considerarli come un unico cliente?"
        if ah_YesNo(this.w_MSG)
          * --- Write into CLI_VEND
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'CLI_VEND','CLCODCON');
                +i_ccchkf ;
            +" where ";
                +"CLCODCLI = "+cp_ToStrODBC(this.w_CODCLI);
                   )
          else
            update (i_cTable) set;
                CLCODCON = this.w_CODICE;
                &i_ccchkf. ;
             where;
                CLCODCLI = this.w_CODCLI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          i_retcode = 'stop'
          return
        else
          * --- Write into CONTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANCLIPOS');
                +i_ccchkf ;
            +" where ";
                +"ANTIPCON = "+cp_ToStrODBC("C");
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                ANCLIPOS = " ";
                &i_ccchkf. ;
             where;
                ANTIPCON = "C";
                and ANCODICE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if this.w_FLCPOS="S"
      * --- Nel caso di codifica numerica ricalcolo il progressivo
      i_Conn=i_TableProp[this.CLI_VEND_IDX, 3]
      cp_NextTableProg(this, i_Conn, "PRCLIPOS", "i_codazi,w_CLCODCLI")
      p_LL=LEN(ALLTRIM(g_MASPOS))
      * --- Ridimensiono il codice in base alla 'maschera'  impostata sui parametri negozio principale
      this.w_CODCLI = RIGHT(ALLTRIM(this.w_CLCODCLI),p_LL)
      this.w_OK = "T"
    else
      * --- Read from CLI_VEND
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLCODCLI,CLDESCRI"+;
          " from "+i_cTable+" CLI_VEND where ";
              +"CLCODCLI = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLCODCLI,CLDESCRI;
          from (i_cTable) where;
              CLCODCLI = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCLI),cp_NullValue(_read_.CLCODCLI))
        this.w_DESCLI = NVL(cp_ToDate(_read_.CLDESCRI),cp_NullValue(_read_.CLDESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_NEWDES = this.w_DESCLI
      if i_Rows<>0
        this.w_MSG = "ATTENZIONE%0La codifica � gi� presente nell'archivio clienti negozio (P.O.S.)%0Si desidera inserirlo con una nuova codifica?"
        if ah_YesNo(this.w_MSG)
          * --- Nel caso esiste gi� la codifica chiedo di inserirne una nuova
          do GSPS_KCP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Se caricamento e No inserito Cliente Vendita, toglie il Flag POS
          this.w_OK = "F"
        endif
        this.w_CODCLI = IIF(NOT EMPTY(this.w_NEWCOD),this.w_NEWCOD,this.w_CODICE)
        this.w_ANDESCRI = IIF(NOT EMPTY(this.w_NEWDES),this.w_NEWDES,this.w_ANDESCRI)
      else
        this.w_CODCLI = ALLTRIM(this.w_CODICE)
        this.w_DESCLI = this.w_ANDESCRI
        this.w_NEWDES = this.w_DESCLI
        if LEN(this.w_CODCLI)>LEN(g_MASPOS)
          this.w_MSG = "ATTENZIONE%0La codifica supera il massimo delle posizioni definite%0nei parametri vendita negozio in struttura codice nominativo%0Si desidera inserirlo con una nuova codifica?"
          if ah_YesNo(this.w_MSG)
            * --- Nel caso la codifica superi il numero di posizioni definite nei parametri Vendita Negozio chiedo di reinserire il codice
            do GSPS_KCP with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            this.w_OK = "F"
          endif
        else
          this.w_OK = "T"
        endif
        this.w_CODCLI = IIF(NOT EMPTY(this.w_NEWCOD),this.w_NEWCOD,this.w_CODCLI)
        this.w_ANDESCRI = IIF(NOT EMPTY(this.w_NEWDES),this.w_NEWDES,this.w_ANDESCRI)
      endif
    endif
    this.w_AN_EMAIL = LEFT(this.w_AN_EMAIL,40)
    this.w_AN_EMPEC = LEFT(this.w_AN_EMPEC,40)
    if this.w_OK="T"
      * --- Inserimento nuovo cliente negozio
      * --- Insert into CLI_VEND
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CLCODCLI"+",CLTIPCON"+",CLDESCRI"+",CLINDIRI"+",CL___CAP"+",CLLOCALI"+",CLPROVIN"+",CLNAZION"+",CLTELEFO"+",CLTELFAX"+",CLNUMCEL"+",CL_EMAIL"+",CL_EMPEC"+",CLPARIVA"+",CLCODFIS"+",CLCODCON"+",CLCODPAG"+",CLCATCOM"+",CLCATSCM"+",CLSCONT1"+",CLSCONT2"+",CLCODLIS"+",CLDTINVA"+",CLDTOBSO"+",CLVALFID"+",CLFLFIDO"+",CLCODNEG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODCLI),'CLI_VEND','CLCODCLI');
        +","+cp_NullLink(cp_ToStrODBC("C"),'CLI_VEND','CLTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'CLI_VEND','CLDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANINDIRI),'CLI_VEND','CLINDIRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AN___CAP),'CLI_VEND','CL___CAP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANLOCALI),'CLI_VEND','CLLOCALI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANPROVIN),'CLI_VEND','CLPROVIN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANNAZION),'CLI_VEND','CLNAZION');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANTELEFO),'CLI_VEND','CLTELEFO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANTELFAX),'CLI_VEND','CLTELFAX');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANNUMCEL),'CLI_VEND','CLNUMCEL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AN_EMAIL),'CLI_VEND','CL_EMAIL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AN_EMPEC),'CLI_VEND','CL_EMPEC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANPARIVA),'CLI_VEND','CLPARIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODFIS),'CLI_VEND','CLCODFIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'CLI_VEND','CLCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODPAG),'CLI_VEND','CLCODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANCATCOM),'CLI_VEND','CLCATCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANCATSCM),'CLI_VEND','CLCATSCM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AN1SCONT),'CLI_VEND','CLSCONT1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AN2SCONT),'CLI_VEND','CLSCONT2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODLIS),'CLI_VEND','CLCODLIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANDTINVA),'CLI_VEND','CLDTINVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANDTOBSO),'CLI_VEND','CLDTOBSO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANVALFID),'CLI_VEND','CLVALFID');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANFLFIDO),'CLI_VEND','CLFLFIDO');
        +","+cp_NullLink(cp_ToStrODBC(g_CODNEG),'CLI_VEND','CLCODNEG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CLCODCLI',this.w_CODCLI,'CLTIPCON',"C",'CLDESCRI',this.w_ANDESCRI,'CLINDIRI',this.w_ANINDIRI,'CL___CAP',this.w_AN___CAP,'CLLOCALI',this.w_ANLOCALI,'CLPROVIN',this.w_ANPROVIN,'CLNAZION',this.w_ANNAZION,'CLTELEFO',this.w_ANTELEFO,'CLTELFAX',this.w_ANTELFAX,'CLNUMCEL',this.w_ANNUMCEL,'CL_EMAIL',this.w_AN_EMAIL)
        insert into (i_cTable) (CLCODCLI,CLTIPCON,CLDESCRI,CLINDIRI,CL___CAP,CLLOCALI,CLPROVIN,CLNAZION,CLTELEFO,CLTELFAX,CLNUMCEL,CL_EMAIL,CL_EMPEC,CLPARIVA,CLCODFIS,CLCODCON,CLCODPAG,CLCATCOM,CLCATSCM,CLSCONT1,CLSCONT2,CLCODLIS,CLDTINVA,CLDTOBSO,CLVALFID,CLFLFIDO,CLCODNEG &i_ccchkf. );
           values (;
             this.w_CODCLI;
             ,"C";
             ,this.w_ANDESCRI;
             ,this.w_ANINDIRI;
             ,this.w_AN___CAP;
             ,this.w_ANLOCALI;
             ,this.w_ANPROVIN;
             ,this.w_ANNAZION;
             ,this.w_ANTELEFO;
             ,this.w_ANTELFAX;
             ,this.w_ANNUMCEL;
             ,this.w_AN_EMAIL;
             ,this.w_AN_EMPEC;
             ,this.w_ANPARIVA;
             ,this.w_ANCODFIS;
             ,this.w_CODICE;
             ,this.w_ANCODPAG;
             ,this.w_ANCATCOM;
             ,this.w_ANCATSCM;
             ,this.w_AN1SCONT;
             ,this.w_AN2SCONT;
             ,this.w_ANCODLIS;
             ,this.w_ANDTINVA;
             ,this.w_ANDTOBSO;
             ,this.w_ANVALFID;
             ,this.w_ANFLFIDO;
             ,g_CODNEG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_ErrorMsg("Caricamento cliente negozio completata")
    else
      if this.pTIPO="C"
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANCLIPOS');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC("C");
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              ANCLIPOS = " ";
              &i_ccchkf. ;
           where;
              ANTIPCON = "C";
              and ANCODICE = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pCODICE,pTIPO)
    this.pCODICE=pCODICE
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CLI_VEND'
    this.cWorkTables[3]='PAR_VDET'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODICE,pTIPO"
endproc
