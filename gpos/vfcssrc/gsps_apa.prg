* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_apa                                                        *
*              Parametri vendita negozio                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_162]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2012-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_apa"))

* --- Class definition
define class tgsps_apa as StdForm
  Top    = 7
  Left   = 11

  * --- Standard Properties
  Width  = 586
  Height = 388+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-09"
  HelpContextID=159996009
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=120

  * --- Constant Properties
  PAR_VDET_IDX = 0
  AZIENDA_IDX = 0
  BUSIUNIT_IDX = 0
  MAGAZZIN_IDX = 0
  PAG_AMEN_IDX = 0
  CLI_VEND_IDX = 0
  TIP_DOCU_IDX = 0
  MOD_PAGA_IDX = 0
  CAM_AGAZ_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  LINGUE_IDX = 0
  CAU_CONT_IDX = 0
  CENCOST_IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  cFile = "PAR_VDET"
  cKeySelect = "PACODAZI,PACODNEG"
  cKeyWhere  = "PACODAZI=this.w_PACODAZI and PACODNEG=this.w_PACODNEG"
  cKeyWhereODBC = '"PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';
      +'+" and PACODNEG="+cp_ToStrODBC(this.w_PACODNEG)';

  cKeyWhereODBCqualified = '"PAR_VDET.PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';
      +'+" and PAR_VDET.PACODNEG="+cp_ToStrODBC(this.w_PACODNEG)';

  cPrg = "gsps_apa"
  cComment = "Parametri vendita negozio"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PACODAZI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_PACODNEG = space(3)
  w_PAPRINEG = space(1)
  w_PAALFDOC = space(10)
  w_DESNEG = space(40)
  w_obtest = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_PAPARETI = space(40)
  w_UNITST = space(1)
  w_PAMASPOS = space(15)
  w_PAFLCPOS = space(1)
  w_PAFLAUTC = space(1)
  w_PAMASCON = space(15)
  w_PACATCON = space(5)
  w_PACODLIN = space(3)
  w_DESMAS = space(40)
  w_DESCCN = space(35)
  w_DESLIN = space(30)
  w_ARTPOS = space(1)
  w_ARTOBSO = ctod('  /  /  ')
  w_TIPART = space(2)
  w_PAARTMAG = space(20)
  w_ARTDES = space(40)
  w_PAFLESSC = space(1)
  o_PAFLESSC = space(1)
  w_PADATESC = ctod('  /  /  ')
  w_OFLESSC = space(1)
  w_PAFLFAFI = space(1)
  o_PAFLFAFI = space(1)
  w_PASERFID = space(20)
  w_SERDES = space(40)
  w_ARTPOSF = space(1)
  w_ARTOBSOF = ctod('  /  /  ')
  w_TIPARTF = space(2)
  w_PAFLPROM = space(1)
  w_FLVEAC = space(1)
  w_FILRF = space(1)
  w_FILFA = space(1)
  w_FILDT = space(1)
  w_DESCOR = space(35)
  w_PADOCRIC = space(5)
  w_DESRIC = space(35)
  w_PADOCFAF = space(5)
  o_PADOCFAF = space(5)
  w_DESFAF = space(35)
  w_PADOCFAT = space(5)
  o_PADOCFAT = space(5)
  w_DESFAT = space(35)
  w_PADOCRIF = space(5)
  w_DESRIF = space(35)
  w_PADOCDDT = space(5)
  w_DESDDT = space(35)
  w_PADOCCOR = space(5)
  w_PAFLCONT = space(1)
  w_CATCOR = space(2)
  w_CATRIC = space(2)
  w_CATFAF = space(2)
  w_CATFAT = space(2)
  w_CATRIF = space(2)
  w_CATDDT = space(2)
  w_FLFAT = space(1)
  w_FLRIF = space(1)
  w_FLFAF = space(1)
  w_FLDDT = space(1)
  w_FLRI1 = space(1)
  w_FLRI2 = space(1)
  w_CAUCO1 = space(5)
  w_CAUCO2 = space(5)
  w_TIPDO1 = space(2)
  w_TIPDO2 = space(2)
  w_CAUFAF = space(5)
  w_CASFAF = space(1)
  w_PACAURES = space(5)
  w_DESRES = space(35)
  w_FLRESI = space(1)
  w_PACARKIT = space(5)
  w_PASCAKIT = space(5)
  w_DESKCA = space(35)
  w_FLKCA = space(1)
  w_DESKSC = space(35)
  w_FLKSC = space(1)
  w_SCACOL = space(5)
  w_RESCOL = space(5)
  w_CARCOL = space(5)
  w_PACODCEN = space(15)
  w_OBSCEN = ctod('  /  /  ')
  w_DESPIA = space(40)
  w_RESCHK = 0
  w_PACONPRE = space(15)
  w_DESCON = space(40)
  w_DESPRE = space(40)
  w_DESASS = space(40)
  w_DESCAR = space(40)
  w_DESFIN = space(40)
  w_TIPCON = space(1)
  w_DTOPRE = ctod('  /  /  ')
  w_PACONCON = space(15)
  w_PACONASS = space(15)
  w_PACONCAR = space(15)
  w_PACONFIN = space(15)
  w_DTOCON = ctod('  /  /  ')
  w_DTOASS = ctod('  /  /  ')
  w_DTOCAR = ctod('  /  /  ')
  w_DTOFIN = ctod('  /  /  ')
  w_FILC = space(1)
  w_PA3KCORR = space(5)
  w_DES3KC = space(35)
  w_PA3KRICF = space(5)
  w_PA3KIMPO = space(1)
  o_PA3KIMPO = space(1)
  w_DES3KR = space(35)
  w_CAT3KC = space(2)
  w_FL3KC = space(1)
  w_CON3KC = space(5)
  w_PA3KDFIS = space(1)
  w_CAT3KR = space(2)
  w_FL3KR = space(1)
  w_CON3KR = space(5)
  w_PAACCPRE = space(1)
  w_PAPAGCON = space(1)
  w_PAPAGASS = space(1)
  w_PAPAGCAR = space(1)
  w_PAPAGFIN = space(1)
  w_PAPAGCLI = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAR_VDET','gsps_apa')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_apaPag1","gsps_apa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impostazioni")
      .Pages(1).HelpContextID = 42823568
      .Pages(2).addobject("oPag","tgsps_apaPag2","gsps_apa",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Causali predefinite")
      .Pages(2).HelpContextID = 171773628
      .Pages(3).addobject("oPag","tgsps_apaPag3","gsps_apa",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Contropartite pagamenti")
      .Pages(3).HelpContextID = 254361208
      .Pages(4).addobject("oPag","tgsps_apaPag4","gsps_apa",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Oper. maggiori 3.000 EU")
      .Pages(4).HelpContextID = 241656374
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPACODNEG_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[16]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='BUSIUNIT'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='CLI_VEND'
    this.cWorkTables[6]='TIP_DOCU'
    this.cWorkTables[7]='MOD_PAGA'
    this.cWorkTables[8]='CAM_AGAZ'
    this.cWorkTables[9]='MASTRI'
    this.cWorkTables[10]='CACOCLFO'
    this.cWorkTables[11]='LINGUE'
    this.cWorkTables[12]='CAU_CONT'
    this.cWorkTables[13]='CENCOST'
    this.cWorkTables[14]='ART_ICOL'
    this.cWorkTables[15]='CONTI'
    this.cWorkTables[16]='PAR_VDET'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(16))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_VDET_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_VDET_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PACODAZI = NVL(PACODAZI,space(5))
      .w_PACODNEG = NVL(PACODNEG,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_34_joined
    link_1_34_joined=.f.
    local link_1_45_joined
    link_1_45_joined=.f.
    local link_2_42_joined
    link_2_42_joined=.f.
    local link_2_48_joined
    link_2_48_joined=.f.
    local link_2_49_joined
    link_2_49_joined=.f.
    local link_2_59_joined
    link_2_59_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_VDET where PACODAZI=KeySet.PACODAZI
    *                            and PACODNEG=KeySet.PACODNEG
    *
    i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_VDET')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_VDET.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_VDET '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_34_joined=this.AddJoinedLink_1_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_45_joined=this.AddJoinedLink_1_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_42_joined=this.AddJoinedLink_2_42(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_48_joined=this.AddJoinedLink_2_48(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_49_joined=this.AddJoinedLink_2_49(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_59_joined=this.AddJoinedLink_2_59(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  ,'PACODNEG',this.w_PACODNEG  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESNEG = space(40)
        .w_obtest = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_UNITST = space(1)
        .w_DESMAS = space(40)
        .w_DESCCN = space(35)
        .w_DESLIN = space(30)
        .w_ARTPOS = space(1)
        .w_ARTOBSO = ctod("  /  /  ")
        .w_TIPART = space(2)
        .w_ARTDES = space(40)
        .w_OFLESSC = space(1)
        .w_SERDES = space(40)
        .w_ARTPOSF = space(1)
        .w_ARTOBSOF = ctod("  /  /  ")
        .w_TIPARTF = space(2)
        .w_DESCOR = space(35)
        .w_DESRIC = space(35)
        .w_DESFAF = space(35)
        .w_DESFAT = space(35)
        .w_DESRIF = space(35)
        .w_DESDDT = space(35)
        .w_CATCOR = space(2)
        .w_CATRIC = space(2)
        .w_CATFAF = space(2)
        .w_CATFAT = space(2)
        .w_CATRIF = space(2)
        .w_CATDDT = space(2)
        .w_FLFAT = space(1)
        .w_FLRIF = space(1)
        .w_FLFAF = space(1)
        .w_FLDDT = space(1)
        .w_FLRI1 = space(1)
        .w_FLRI2 = space(1)
        .w_CAUCO1 = space(5)
        .w_CAUCO2 = space(5)
        .w_TIPDO1 = space(2)
        .w_TIPDO2 = space(2)
        .w_CAUFAF = space(5)
        .w_CASFAF = space(1)
        .w_DESRES = space(35)
        .w_FLRESI = space(1)
        .w_DESKCA = space(35)
        .w_FLKCA = space(1)
        .w_DESKSC = space(35)
        .w_FLKSC = space(1)
        .w_SCACOL = space(5)
        .w_RESCOL = space(5)
        .w_CARCOL = space(5)
        .w_OBSCEN = ctod("  /  /  ")
        .w_DESPIA = space(40)
        .w_RESCHK = 0
        .w_DESCON = space(40)
        .w_DESPRE = space(40)
        .w_DESASS = space(40)
        .w_DESCAR = space(40)
        .w_DESFIN = space(40)
        .w_DTOPRE = ctod("  /  /  ")
        .w_DTOCON = ctod("  /  /  ")
        .w_DTOASS = ctod("  /  /  ")
        .w_DTOCAR = ctod("  /  /  ")
        .w_DTOFIN = ctod("  /  /  ")
        .w_FILC = 'C'
        .w_DES3KC = space(35)
        .w_DES3KR = space(35)
        .w_CAT3KC = space(2)
        .w_FL3KC = space(1)
        .w_CON3KC = space(5)
        .w_CAT3KR = space(2)
        .w_FL3KR = space(1)
        .w_CON3KR = space(5)
        .w_PACODAZI = NVL(PACODAZI,space(5))
        .w_OBTEST = i_DATSYS
        .w_PACODNEG = NVL(PACODNEG,space(3))
          if link_1_3_joined
            this.w_PACODNEG = NVL(BUCODICE103,NVL(this.w_PACODNEG,space(3)))
            this.w_DESNEG = NVL(BUDESCRI103,space(40))
          else
          .link_1_3('Load')
          endif
        .w_PAPRINEG = NVL(PAPRINEG,space(1))
        .w_PAALFDOC = NVL(PAALFDOC,space(10))
        .w_PAPARETI = NVL(PAPARETI,space(40))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .w_PAMASPOS = NVL(PAMASPOS,space(15))
        .w_PAFLCPOS = NVL(PAFLCPOS,space(1))
        .w_PAFLAUTC = NVL(PAFLAUTC,space(1))
        .w_PAMASCON = NVL(PAMASCON,space(15))
          if link_1_20_joined
            this.w_PAMASCON = NVL(MCCODICE120,NVL(this.w_PAMASCON,space(15)))
            this.w_DESMAS = NVL(MCDESCRI120,space(40))
          else
          .link_1_20('Load')
          endif
        .w_PACATCON = NVL(PACATCON,space(5))
          if link_1_21_joined
            this.w_PACATCON = NVL(C2CODICE121,NVL(this.w_PACATCON,space(5)))
            this.w_DESCCN = NVL(C2DESCRI121,space(35))
          else
          .link_1_21('Load')
          endif
        .w_PACODLIN = NVL(PACODLIN,space(3))
          if link_1_22_joined
            this.w_PACODLIN = NVL(LUCODICE122,NVL(this.w_PACODLIN,space(3)))
            this.w_DESLIN = NVL(LUDESCRI122,space(30))
          else
          .link_1_22('Load')
          endif
        .w_PAARTMAG = NVL(PAARTMAG,space(20))
          if link_1_34_joined
            this.w_PAARTMAG = NVL(ARCODART134,NVL(this.w_PAARTMAG,space(20)))
            this.w_ARTDES = NVL(ARDESART134,space(40))
            this.w_ARTPOS = NVL(ARARTPOS134,space(1))
            this.w_ARTOBSO = NVL(cp_ToDate(ARDTOBSO134),ctod("  /  /  "))
            this.w_TIPART = NVL(ARTIPART134,space(2))
          else
          .link_1_34('Load')
          endif
        .w_PAFLESSC = NVL(PAFLESSC,space(1))
        .w_PADATESC = NVL(cp_ToDate(PADATESC),ctod("  /  /  "))
        .w_PAFLFAFI = NVL(PAFLFAFI,space(1))
        .w_PASERFID = NVL(PASERFID,space(20))
          if link_1_45_joined
            this.w_PASERFID = NVL(ARCODART145,NVL(this.w_PASERFID,space(20)))
            this.w_SERDES = NVL(ARDESART145,space(40))
            this.w_ARTPOSF = NVL(ARARTPOS145,space(1))
            this.w_ARTOBSOF = NVL(cp_ToDate(ARDTOBSO145),ctod("  /  /  "))
            this.w_TIPARTF = NVL(ARTIPART145,space(2))
          else
          .link_1_45('Load')
          endif
        .w_PAFLPROM = NVL(PAFLPROM,space(1))
        .w_FLVEAC = 'V'
        .w_FILRF = 'RF'
        .w_FILFA = 'FA'
        .w_FILDT = 'DT'
        .w_PADOCRIC = NVL(PADOCRIC,space(5))
          .link_2_6('Load')
        .w_PADOCFAF = NVL(PADOCFAF,space(5))
          .link_2_8('Load')
        .w_PADOCFAT = NVL(PADOCFAT,space(5))
          .link_2_10('Load')
        .w_PADOCRIF = NVL(PADOCRIF,space(5))
          .link_2_12('Load')
        .w_PADOCDDT = NVL(PADOCDDT,space(5))
          .link_2_14('Load')
        .w_PADOCCOR = NVL(PADOCCOR,space(5))
          .link_2_16('Load')
        .w_PAFLCONT = NVL(PAFLCONT,space(1))
          .link_2_36('Load')
          .link_2_37('Load')
          .link_2_40('Load')
        .w_PACAURES = NVL(PACAURES,space(5))
          if link_2_42_joined
            this.w_PACAURES = NVL(CMCODICE242,NVL(this.w_PACAURES,space(5)))
            this.w_DESRES = NVL(CMDESCRI242,space(35))
            this.w_FLRESI = NVL(CMFLCASC242,space(1))
            this.w_RESCOL = NVL(CMCAUCOL242,space(5))
          else
          .link_2_42('Load')
          endif
        .w_PACARKIT = NVL(PACARKIT,space(5))
          if link_2_48_joined
            this.w_PACARKIT = NVL(CMCODICE248,NVL(this.w_PACARKIT,space(5)))
            this.w_DESKCA = NVL(CMDESCRI248,space(35))
            this.w_FLKCA = NVL(CMFLCASC248,space(1))
            this.w_CARCOL = NVL(CMCAUCOL248,space(5))
          else
          .link_2_48('Load')
          endif
        .w_PASCAKIT = NVL(PASCAKIT,space(5))
          if link_2_49_joined
            this.w_PASCAKIT = NVL(CMCODICE249,NVL(this.w_PASCAKIT,space(5)))
            this.w_DESKSC = NVL(CMDESCRI249,space(35))
            this.w_FLKSC = NVL(CMFLCASC249,space(1))
            this.w_SCACOL = NVL(CMCAUCOL249,space(5))
          else
          .link_2_49('Load')
          endif
        .w_PACODCEN = NVL(PACODCEN,space(15))
          if link_2_59_joined
            this.w_PACODCEN = NVL(CC_CONTO259,NVL(this.w_PACODCEN,space(15)))
            this.w_DESPIA = NVL(CCDESPIA259,space(40))
            this.w_OBSCEN = NVL(cp_ToDate(CCDTOBSO259),ctod("  /  /  "))
          else
          .link_2_59('Load')
          endif
        .oPgFrm.Page2.oPag.oObj_2_66.Calculate()
        .w_PACONPRE = NVL(PACONPRE,space(15))
          .link_3_1('Load')
        .w_TIPCON = 'G'
        .w_PACONCON = NVL(PACONCON,space(15))
          .link_3_14('Load')
        .w_PACONASS = NVL(PACONASS,space(15))
          .link_3_15('Load')
        .w_PACONCAR = NVL(PACONCAR,space(15))
          .link_3_16('Load')
        .w_PACONFIN = NVL(PACONFIN,space(15))
          .link_3_17('Load')
        .w_PA3KCORR = NVL(PA3KCORR,space(5))
          .link_4_2('Load')
        .w_PA3KRICF = NVL(PA3KRICF,space(5))
          .link_4_4('Load')
        .w_PA3KIMPO = NVL(PA3KIMPO,space(1))
        .w_PA3KDFIS = NVL(PA3KDFIS,space(1))
        .w_PAACCPRE = NVL(PAACCPRE,space(1))
        .w_PAPAGCON = NVL(PAPAGCON,space(1))
        .w_PAPAGASS = NVL(PAPAGASS,space(1))
        .w_PAPAGCAR = NVL(PAPAGCAR,space(1))
        .w_PAPAGFIN = NVL(PAPAGFIN,space(1))
        .w_PAPAGCLI = NVL(PAPAGCLI,space(1))
        cp_LoadRecExtFlds(this,'PAR_VDET')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
    this.Calculate_SAZZFPKTSY()
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PACODAZI = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_PACODNEG = space(3)
      .w_PAPRINEG = space(1)
      .w_PAALFDOC = space(10)
      .w_DESNEG = space(40)
      .w_obtest = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_PAPARETI = space(40)
      .w_UNITST = space(1)
      .w_PAMASPOS = space(15)
      .w_PAFLCPOS = space(1)
      .w_PAFLAUTC = space(1)
      .w_PAMASCON = space(15)
      .w_PACATCON = space(5)
      .w_PACODLIN = space(3)
      .w_DESMAS = space(40)
      .w_DESCCN = space(35)
      .w_DESLIN = space(30)
      .w_ARTPOS = space(1)
      .w_ARTOBSO = ctod("  /  /  ")
      .w_TIPART = space(2)
      .w_PAARTMAG = space(20)
      .w_ARTDES = space(40)
      .w_PAFLESSC = space(1)
      .w_PADATESC = ctod("  /  /  ")
      .w_OFLESSC = space(1)
      .w_PAFLFAFI = space(1)
      .w_PASERFID = space(20)
      .w_SERDES = space(40)
      .w_ARTPOSF = space(1)
      .w_ARTOBSOF = ctod("  /  /  ")
      .w_TIPARTF = space(2)
      .w_PAFLPROM = space(1)
      .w_FLVEAC = space(1)
      .w_FILRF = space(1)
      .w_FILFA = space(1)
      .w_FILDT = space(1)
      .w_DESCOR = space(35)
      .w_PADOCRIC = space(5)
      .w_DESRIC = space(35)
      .w_PADOCFAF = space(5)
      .w_DESFAF = space(35)
      .w_PADOCFAT = space(5)
      .w_DESFAT = space(35)
      .w_PADOCRIF = space(5)
      .w_DESRIF = space(35)
      .w_PADOCDDT = space(5)
      .w_DESDDT = space(35)
      .w_PADOCCOR = space(5)
      .w_PAFLCONT = space(1)
      .w_CATCOR = space(2)
      .w_CATRIC = space(2)
      .w_CATFAF = space(2)
      .w_CATFAT = space(2)
      .w_CATRIF = space(2)
      .w_CATDDT = space(2)
      .w_FLFAT = space(1)
      .w_FLRIF = space(1)
      .w_FLFAF = space(1)
      .w_FLDDT = space(1)
      .w_FLRI1 = space(1)
      .w_FLRI2 = space(1)
      .w_CAUCO1 = space(5)
      .w_CAUCO2 = space(5)
      .w_TIPDO1 = space(2)
      .w_TIPDO2 = space(2)
      .w_CAUFAF = space(5)
      .w_CASFAF = space(1)
      .w_PACAURES = space(5)
      .w_DESRES = space(35)
      .w_FLRESI = space(1)
      .w_PACARKIT = space(5)
      .w_PASCAKIT = space(5)
      .w_DESKCA = space(35)
      .w_FLKCA = space(1)
      .w_DESKSC = space(35)
      .w_FLKSC = space(1)
      .w_SCACOL = space(5)
      .w_RESCOL = space(5)
      .w_CARCOL = space(5)
      .w_PACODCEN = space(15)
      .w_OBSCEN = ctod("  /  /  ")
      .w_DESPIA = space(40)
      .w_RESCHK = 0
      .w_PACONPRE = space(15)
      .w_DESCON = space(40)
      .w_DESPRE = space(40)
      .w_DESASS = space(40)
      .w_DESCAR = space(40)
      .w_DESFIN = space(40)
      .w_TIPCON = space(1)
      .w_DTOPRE = ctod("  /  /  ")
      .w_PACONCON = space(15)
      .w_PACONASS = space(15)
      .w_PACONCAR = space(15)
      .w_PACONFIN = space(15)
      .w_DTOCON = ctod("  /  /  ")
      .w_DTOASS = ctod("  /  /  ")
      .w_DTOCAR = ctod("  /  /  ")
      .w_DTOFIN = ctod("  /  /  ")
      .w_FILC = space(1)
      .w_PA3KCORR = space(5)
      .w_DES3KC = space(35)
      .w_PA3KRICF = space(5)
      .w_PA3KIMPO = space(1)
      .w_DES3KR = space(35)
      .w_CAT3KC = space(2)
      .w_FL3KC = space(1)
      .w_CON3KC = space(5)
      .w_PA3KDFIS = space(1)
      .w_CAT3KR = space(2)
      .w_FL3KR = space(1)
      .w_CON3KR = space(5)
      .w_PAACCPRE = space(1)
      .w_PAPAGCON = space(1)
      .w_PAPAGASS = space(1)
      .w_PAPAGCAR = space(1)
      .w_PAPAGFIN = space(1)
      .w_PAPAGCLI = space(1)
      if .cFunction<>"Filter"
        .w_PACODAZI = i_codazi
        .w_OBTEST = i_DATSYS
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_PACODNEG))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,6,.f.)
        .w_obtest = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .DoRTCalc(8,14,.f.)
          if not(empty(.w_PAMASCON))
          .link_1_20('Full')
          endif
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_PACATCON))
          .link_1_21('Full')
          endif
        .w_PACODLIN = g_CODLIN
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_PACODLIN))
          .link_1_22('Full')
          endif
        .DoRTCalc(17,23,.f.)
          if not(empty(.w_PAARTMAG))
          .link_1_34('Full')
          endif
          .DoRTCalc(24,25,.f.)
        .w_PADATESC = IIf(.w_PAFLESSC='S',.w_PADATESC,cp_CharToDate("  -  -  "))
          .DoRTCalc(27,27,.f.)
        .w_PAFLFAFI = ' '
        .w_PASERFID = IIF(.w_PAFLFAFI<>'S',Space(20),.w_PASERFID)
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_PASERFID))
          .link_1_45('Full')
          endif
          .DoRTCalc(30,34,.f.)
        .w_FLVEAC = 'V'
        .w_FILRF = 'RF'
        .w_FILFA = 'FA'
        .w_FILDT = 'DT'
        .DoRTCalc(39,40,.f.)
          if not(empty(.w_PADOCRIC))
          .link_2_6('Full')
          endif
        .DoRTCalc(41,42,.f.)
          if not(empty(.w_PADOCFAF))
          .link_2_8('Full')
          endif
        .DoRTCalc(43,44,.f.)
          if not(empty(.w_PADOCFAT))
          .link_2_10('Full')
          endif
        .DoRTCalc(45,46,.f.)
          if not(empty(.w_PADOCRIF))
          .link_2_12('Full')
          endif
        .DoRTCalc(47,48,.f.)
          if not(empty(.w_PADOCDDT))
          .link_2_14('Full')
          endif
        .DoRTCalc(49,50,.f.)
          if not(empty(.w_PADOCCOR))
          .link_2_16('Full')
          endif
        .w_PAFLCONT = 'N'
        .DoRTCalc(52,64,.f.)
          if not(empty(.w_CAUCO1))
          .link_2_36('Full')
          endif
        .DoRTCalc(65,65,.f.)
          if not(empty(.w_CAUCO2))
          .link_2_37('Full')
          endif
        .DoRTCalc(66,68,.f.)
          if not(empty(.w_CAUFAF))
          .link_2_40('Full')
          endif
        .DoRTCalc(69,70,.f.)
          if not(empty(.w_PACAURES))
          .link_2_42('Full')
          endif
        .DoRTCalc(71,73,.f.)
          if not(empty(.w_PACARKIT))
          .link_2_48('Full')
          endif
        .DoRTCalc(74,74,.f.)
          if not(empty(.w_PASCAKIT))
          .link_2_49('Full')
          endif
        .DoRTCalc(75,82,.f.)
          if not(empty(.w_PACODCEN))
          .link_2_59('Full')
          endif
          .DoRTCalc(83,84,.f.)
        .w_RESCHK = 0
        .oPgFrm.Page2.oPag.oObj_2_66.Calculate()
        .DoRTCalc(86,86,.f.)
          if not(empty(.w_PACONPRE))
          .link_3_1('Full')
          endif
          .DoRTCalc(87,91,.f.)
        .w_TIPCON = 'G'
        .DoRTCalc(93,94,.f.)
          if not(empty(.w_PACONCON))
          .link_3_14('Full')
          endif
        .DoRTCalc(95,95,.f.)
          if not(empty(.w_PACONASS))
          .link_3_15('Full')
          endif
        .DoRTCalc(96,96,.f.)
          if not(empty(.w_PACONCAR))
          .link_3_16('Full')
          endif
        .DoRTCalc(97,97,.f.)
          if not(empty(.w_PACONFIN))
          .link_3_17('Full')
          endif
          .DoRTCalc(98,101,.f.)
        .w_FILC = 'C'
        .DoRTCalc(103,103,.f.)
          if not(empty(.w_PA3KCORR))
          .link_4_2('Full')
          endif
        .DoRTCalc(104,105,.f.)
          if not(empty(.w_PA3KRICF))
          .link_4_4('Full')
          endif
        .w_PA3KIMPO = 'B'
          .DoRTCalc(107,110,.f.)
        .w_PA3KDFIS = 'B'
          .DoRTCalc(112,114,.f.)
        .w_PAACCPRE = 'S'
        .w_PAPAGCON = 'S'
        .w_PAPAGASS = 'S'
        .w_PAPAGCAR = 'S'
        .w_PAPAGFIN = 'S'
        .w_PAPAGCLI = 'S'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_VDET')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPACODNEG_1_3.enabled = i_bVal
      .Page1.oPag.oPAPRINEG_1_4.enabled = i_bVal
      .Page1.oPag.oPAALFDOC_1_6.enabled = i_bVal
      .Page1.oPag.oPAPARETI_1_11.enabled = i_bVal
      .Page1.oPag.oPAMASPOS_1_16.enabled = i_bVal
      .Page1.oPag.oPAFLCPOS_1_17.enabled = i_bVal
      .Page1.oPag.oPAFLAUTC_1_19.enabled = i_bVal
      .Page1.oPag.oPAMASCON_1_20.enabled = i_bVal
      .Page1.oPag.oPACATCON_1_21.enabled = i_bVal
      .Page1.oPag.oPACODLIN_1_22.enabled = i_bVal
      .Page1.oPag.oPAARTMAG_1_34.enabled = i_bVal
      .Page1.oPag.oPAFLESSC_1_39.enabled = i_bVal
      .Page1.oPag.oPADATESC_1_40.enabled = i_bVal
      .Page1.oPag.oPAFLFAFI_1_44.enabled = i_bVal
      .Page1.oPag.oPASERFID_1_45.enabled = i_bVal
      .Page1.oPag.oPAFLPROM_1_51.enabled = i_bVal
      .Page2.oPag.oPADOCRIC_2_6.enabled = i_bVal
      .Page2.oPag.oPADOCFAF_2_8.enabled = i_bVal
      .Page2.oPag.oPADOCFAT_2_10.enabled = i_bVal
      .Page2.oPag.oPADOCRIF_2_12.enabled = i_bVal
      .Page2.oPag.oPADOCDDT_2_14.enabled = i_bVal
      .Page2.oPag.oPADOCCOR_2_16.enabled = i_bVal
      .Page2.oPag.oPAFLCONT_2_17.enabled = i_bVal
      .Page2.oPag.oPACAURES_2_42.enabled = i_bVal
      .Page2.oPag.oPACARKIT_2_48.enabled = i_bVal
      .Page2.oPag.oPASCAKIT_2_49.enabled = i_bVal
      .Page2.oPag.oPACODCEN_2_59.enabled = i_bVal
      .Page3.oPag.oPACONPRE_3_1.enabled = i_bVal
      .Page3.oPag.oPACONCON_3_14.enabled = i_bVal
      .Page3.oPag.oPACONASS_3_15.enabled = i_bVal
      .Page3.oPag.oPACONCAR_3_16.enabled = i_bVal
      .Page3.oPag.oPACONFIN_3_17.enabled = i_bVal
      .Page4.oPag.oPA3KCORR_4_2.enabled = i_bVal
      .Page4.oPag.oPA3KRICF_4_4.enabled = i_bVal
      .Page4.oPag.oPA3KIMPO_4_5.enabled = i_bVal
      .Page4.oPag.oPA3KDFIS_4_13.enabled = i_bVal
      .Page4.oPag.oPAACCPRE_4_21.enabled = i_bVal
      .Page4.oPag.oPAPAGCON_4_22.enabled = i_bVal
      .Page4.oPag.oPAPAGASS_4_23.enabled = i_bVal
      .Page4.oPag.oPAPAGCAR_4_24.enabled = i_bVal
      .Page4.oPag.oPAPAGFIN_4_25.enabled = i_bVal
      .Page4.oPag.oPAPAGCLI_4_26.enabled = i_bVal
      .Page1.oPag.oObj_1_13.enabled = i_bVal
      .Page1.oPag.oObj_1_15.enabled = i_bVal
      .Page2.oPag.oObj_2_66.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPACODNEG_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPACODNEG_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PAR_VDET',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODAZI,"PACODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODNEG,"PACODNEG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPRINEG,"PAPRINEG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAALFDOC,"PAALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPARETI,"PAPARETI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAMASPOS,"PAMASPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLCPOS,"PAFLCPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLAUTC,"PAFLAUTC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAMASCON,"PAMASCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACATCON,"PACATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODLIN,"PACODLIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAARTMAG,"PAARTMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLESSC,"PAFLESSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATESC,"PADATESC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLFAFI,"PAFLFAFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASERFID,"PASERFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLPROM,"PAFLPROM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADOCRIC,"PADOCRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADOCFAF,"PADOCFAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADOCFAT,"PADOCFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADOCRIF,"PADOCRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADOCDDT,"PADOCDDT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADOCCOR,"PADOCCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLCONT,"PAFLCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACAURES,"PACAURES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACARKIT,"PACARKIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASCAKIT,"PASCAKIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODCEN,"PACODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACONPRE,"PACONPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACONCON,"PACONCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACONASS,"PACONASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACONCAR,"PACONCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACONFIN,"PACONFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA3KCORR,"PA3KCORR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA3KRICF,"PA3KRICF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA3KIMPO,"PA3KIMPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA3KDFIS,"PA3KDFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAACCPRE,"PAACCPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPAGCON,"PAPAGCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPAGASS,"PAPAGASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPAGCAR,"PAPAGCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPAGFIN,"PAPAGFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPAGCLI,"PAPAGCLI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
    i_lTable = "PAR_VDET"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAR_VDET_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("..\GPOS\EXE\QUERY\GSPS_APA.VQR,..\GPOS\EXE\QUERY\GSPS_APA.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_VDET_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_VDET
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_VDET')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_VDET')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PACODAZI,PACODNEG,PAPRINEG,PAALFDOC,PAPARETI"+;
                  ",PAMASPOS,PAFLCPOS,PAFLAUTC,PAMASCON,PACATCON"+;
                  ",PACODLIN,PAARTMAG,PAFLESSC,PADATESC,PAFLFAFI"+;
                  ",PASERFID,PAFLPROM,PADOCRIC,PADOCFAF,PADOCFAT"+;
                  ",PADOCRIF,PADOCDDT,PADOCCOR,PAFLCONT,PACAURES"+;
                  ",PACARKIT,PASCAKIT,PACODCEN,PACONPRE,PACONCON"+;
                  ",PACONASS,PACONCAR,PACONFIN,PA3KCORR,PA3KRICF"+;
                  ",PA3KIMPO,PA3KDFIS,PAACCPRE,PAPAGCON,PAPAGASS"+;
                  ",PAPAGCAR,PAPAGFIN,PAPAGCLI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PACODAZI)+;
                  ","+cp_ToStrODBCNull(this.w_PACODNEG)+;
                  ","+cp_ToStrODBC(this.w_PAPRINEG)+;
                  ","+cp_ToStrODBC(this.w_PAALFDOC)+;
                  ","+cp_ToStrODBC(this.w_PAPARETI)+;
                  ","+cp_ToStrODBC(this.w_PAMASPOS)+;
                  ","+cp_ToStrODBC(this.w_PAFLCPOS)+;
                  ","+cp_ToStrODBC(this.w_PAFLAUTC)+;
                  ","+cp_ToStrODBCNull(this.w_PAMASCON)+;
                  ","+cp_ToStrODBCNull(this.w_PACATCON)+;
                  ","+cp_ToStrODBCNull(this.w_PACODLIN)+;
                  ","+cp_ToStrODBCNull(this.w_PAARTMAG)+;
                  ","+cp_ToStrODBC(this.w_PAFLESSC)+;
                  ","+cp_ToStrODBC(this.w_PADATESC)+;
                  ","+cp_ToStrODBC(this.w_PAFLFAFI)+;
                  ","+cp_ToStrODBCNull(this.w_PASERFID)+;
                  ","+cp_ToStrODBC(this.w_PAFLPROM)+;
                  ","+cp_ToStrODBCNull(this.w_PADOCRIC)+;
                  ","+cp_ToStrODBCNull(this.w_PADOCFAF)+;
                  ","+cp_ToStrODBCNull(this.w_PADOCFAT)+;
                  ","+cp_ToStrODBCNull(this.w_PADOCRIF)+;
                  ","+cp_ToStrODBCNull(this.w_PADOCDDT)+;
                  ","+cp_ToStrODBCNull(this.w_PADOCCOR)+;
                  ","+cp_ToStrODBC(this.w_PAFLCONT)+;
                  ","+cp_ToStrODBCNull(this.w_PACAURES)+;
                  ","+cp_ToStrODBCNull(this.w_PACARKIT)+;
                  ","+cp_ToStrODBCNull(this.w_PASCAKIT)+;
                  ","+cp_ToStrODBCNull(this.w_PACODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_PACONPRE)+;
                  ","+cp_ToStrODBCNull(this.w_PACONCON)+;
                  ","+cp_ToStrODBCNull(this.w_PACONASS)+;
                  ","+cp_ToStrODBCNull(this.w_PACONCAR)+;
                  ","+cp_ToStrODBCNull(this.w_PACONFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PA3KCORR)+;
                  ","+cp_ToStrODBCNull(this.w_PA3KRICF)+;
                  ","+cp_ToStrODBC(this.w_PA3KIMPO)+;
                  ","+cp_ToStrODBC(this.w_PA3KDFIS)+;
                  ","+cp_ToStrODBC(this.w_PAACCPRE)+;
                  ","+cp_ToStrODBC(this.w_PAPAGCON)+;
                  ","+cp_ToStrODBC(this.w_PAPAGASS)+;
                  ","+cp_ToStrODBC(this.w_PAPAGCAR)+;
                  ","+cp_ToStrODBC(this.w_PAPAGFIN)+;
                  ","+cp_ToStrODBC(this.w_PAPAGCLI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_VDET')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_VDET')
        cp_CheckDeletedKey(i_cTable,0,'PACODAZI',this.w_PACODAZI,'PACODNEG',this.w_PACODNEG)
        INSERT INTO (i_cTable);
              (PACODAZI,PACODNEG,PAPRINEG,PAALFDOC,PAPARETI,PAMASPOS,PAFLCPOS,PAFLAUTC,PAMASCON,PACATCON,PACODLIN,PAARTMAG,PAFLESSC,PADATESC,PAFLFAFI,PASERFID,PAFLPROM,PADOCRIC,PADOCFAF,PADOCFAT,PADOCRIF,PADOCDDT,PADOCCOR,PAFLCONT,PACAURES,PACARKIT,PASCAKIT,PACODCEN,PACONPRE,PACONCON,PACONASS,PACONCAR,PACONFIN,PA3KCORR,PA3KRICF,PA3KIMPO,PA3KDFIS,PAACCPRE,PAPAGCON,PAPAGASS,PAPAGCAR,PAPAGFIN,PAPAGCLI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PACODAZI;
                  ,this.w_PACODNEG;
                  ,this.w_PAPRINEG;
                  ,this.w_PAALFDOC;
                  ,this.w_PAPARETI;
                  ,this.w_PAMASPOS;
                  ,this.w_PAFLCPOS;
                  ,this.w_PAFLAUTC;
                  ,this.w_PAMASCON;
                  ,this.w_PACATCON;
                  ,this.w_PACODLIN;
                  ,this.w_PAARTMAG;
                  ,this.w_PAFLESSC;
                  ,this.w_PADATESC;
                  ,this.w_PAFLFAFI;
                  ,this.w_PASERFID;
                  ,this.w_PAFLPROM;
                  ,this.w_PADOCRIC;
                  ,this.w_PADOCFAF;
                  ,this.w_PADOCFAT;
                  ,this.w_PADOCRIF;
                  ,this.w_PADOCDDT;
                  ,this.w_PADOCCOR;
                  ,this.w_PAFLCONT;
                  ,this.w_PACAURES;
                  ,this.w_PACARKIT;
                  ,this.w_PASCAKIT;
                  ,this.w_PACODCEN;
                  ,this.w_PACONPRE;
                  ,this.w_PACONCON;
                  ,this.w_PACONASS;
                  ,this.w_PACONCAR;
                  ,this.w_PACONFIN;
                  ,this.w_PA3KCORR;
                  ,this.w_PA3KRICF;
                  ,this.w_PA3KIMPO;
                  ,this.w_PA3KDFIS;
                  ,this.w_PAACCPRE;
                  ,this.w_PAPAGCON;
                  ,this.w_PAPAGASS;
                  ,this.w_PAPAGCAR;
                  ,this.w_PAPAGFIN;
                  ,this.w_PAPAGCLI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_VDET_IDX,i_nConn)
      *
      * update PAR_VDET
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_VDET')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PAPRINEG="+cp_ToStrODBC(this.w_PAPRINEG)+;
             ",PAALFDOC="+cp_ToStrODBC(this.w_PAALFDOC)+;
             ",PAPARETI="+cp_ToStrODBC(this.w_PAPARETI)+;
             ",PAMASPOS="+cp_ToStrODBC(this.w_PAMASPOS)+;
             ",PAFLCPOS="+cp_ToStrODBC(this.w_PAFLCPOS)+;
             ",PAFLAUTC="+cp_ToStrODBC(this.w_PAFLAUTC)+;
             ",PAMASCON="+cp_ToStrODBCNull(this.w_PAMASCON)+;
             ",PACATCON="+cp_ToStrODBCNull(this.w_PACATCON)+;
             ",PACODLIN="+cp_ToStrODBCNull(this.w_PACODLIN)+;
             ",PAARTMAG="+cp_ToStrODBCNull(this.w_PAARTMAG)+;
             ",PAFLESSC="+cp_ToStrODBC(this.w_PAFLESSC)+;
             ",PADATESC="+cp_ToStrODBC(this.w_PADATESC)+;
             ",PAFLFAFI="+cp_ToStrODBC(this.w_PAFLFAFI)+;
             ",PASERFID="+cp_ToStrODBCNull(this.w_PASERFID)+;
             ",PAFLPROM="+cp_ToStrODBC(this.w_PAFLPROM)+;
             ",PADOCRIC="+cp_ToStrODBCNull(this.w_PADOCRIC)+;
             ",PADOCFAF="+cp_ToStrODBCNull(this.w_PADOCFAF)+;
             ",PADOCFAT="+cp_ToStrODBCNull(this.w_PADOCFAT)+;
             ",PADOCRIF="+cp_ToStrODBCNull(this.w_PADOCRIF)+;
             ",PADOCDDT="+cp_ToStrODBCNull(this.w_PADOCDDT)+;
             ",PADOCCOR="+cp_ToStrODBCNull(this.w_PADOCCOR)+;
             ",PAFLCONT="+cp_ToStrODBC(this.w_PAFLCONT)+;
             ",PACAURES="+cp_ToStrODBCNull(this.w_PACAURES)+;
             ",PACARKIT="+cp_ToStrODBCNull(this.w_PACARKIT)+;
             ",PASCAKIT="+cp_ToStrODBCNull(this.w_PASCAKIT)+;
             ",PACODCEN="+cp_ToStrODBCNull(this.w_PACODCEN)+;
             ",PACONPRE="+cp_ToStrODBCNull(this.w_PACONPRE)+;
             ",PACONCON="+cp_ToStrODBCNull(this.w_PACONCON)+;
             ",PACONASS="+cp_ToStrODBCNull(this.w_PACONASS)+;
             ",PACONCAR="+cp_ToStrODBCNull(this.w_PACONCAR)+;
             ",PACONFIN="+cp_ToStrODBCNull(this.w_PACONFIN)+;
             ",PA3KCORR="+cp_ToStrODBCNull(this.w_PA3KCORR)+;
             ",PA3KRICF="+cp_ToStrODBCNull(this.w_PA3KRICF)+;
             ",PA3KIMPO="+cp_ToStrODBC(this.w_PA3KIMPO)+;
             ",PA3KDFIS="+cp_ToStrODBC(this.w_PA3KDFIS)+;
             ",PAACCPRE="+cp_ToStrODBC(this.w_PAACCPRE)+;
             ",PAPAGCON="+cp_ToStrODBC(this.w_PAPAGCON)+;
             ",PAPAGASS="+cp_ToStrODBC(this.w_PAPAGASS)+;
             ",PAPAGCAR="+cp_ToStrODBC(this.w_PAPAGCAR)+;
             ",PAPAGFIN="+cp_ToStrODBC(this.w_PAPAGFIN)+;
             ",PAPAGCLI="+cp_ToStrODBC(this.w_PAPAGCLI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_VDET')
        i_cWhere = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  ,'PACODNEG',this.w_PACODNEG  )
        UPDATE (i_cTable) SET;
              PAPRINEG=this.w_PAPRINEG;
             ,PAALFDOC=this.w_PAALFDOC;
             ,PAPARETI=this.w_PAPARETI;
             ,PAMASPOS=this.w_PAMASPOS;
             ,PAFLCPOS=this.w_PAFLCPOS;
             ,PAFLAUTC=this.w_PAFLAUTC;
             ,PAMASCON=this.w_PAMASCON;
             ,PACATCON=this.w_PACATCON;
             ,PACODLIN=this.w_PACODLIN;
             ,PAARTMAG=this.w_PAARTMAG;
             ,PAFLESSC=this.w_PAFLESSC;
             ,PADATESC=this.w_PADATESC;
             ,PAFLFAFI=this.w_PAFLFAFI;
             ,PASERFID=this.w_PASERFID;
             ,PAFLPROM=this.w_PAFLPROM;
             ,PADOCRIC=this.w_PADOCRIC;
             ,PADOCFAF=this.w_PADOCFAF;
             ,PADOCFAT=this.w_PADOCFAT;
             ,PADOCRIF=this.w_PADOCRIF;
             ,PADOCDDT=this.w_PADOCDDT;
             ,PADOCCOR=this.w_PADOCCOR;
             ,PAFLCONT=this.w_PAFLCONT;
             ,PACAURES=this.w_PACAURES;
             ,PACARKIT=this.w_PACARKIT;
             ,PASCAKIT=this.w_PASCAKIT;
             ,PACODCEN=this.w_PACODCEN;
             ,PACONPRE=this.w_PACONPRE;
             ,PACONCON=this.w_PACONCON;
             ,PACONASS=this.w_PACONASS;
             ,PACONCAR=this.w_PACONCAR;
             ,PACONFIN=this.w_PACONFIN;
             ,PA3KCORR=this.w_PA3KCORR;
             ,PA3KRICF=this.w_PA3KRICF;
             ,PA3KIMPO=this.w_PA3KIMPO;
             ,PA3KDFIS=this.w_PA3KDFIS;
             ,PAACCPRE=this.w_PAACCPRE;
             ,PAPAGCON=this.w_PAPAGCON;
             ,PAPAGASS=this.w_PAPAGASS;
             ,PAPAGCAR=this.w_PAPAGCAR;
             ,PAPAGFIN=this.w_PAPAGFIN;
             ,PAPAGCLI=this.w_PAPAGCLI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_VDET_IDX,i_nConn)
      *
      * delete PAR_VDET
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  ,'PACODNEG',this.w_PACODNEG  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .DoRTCalc(3,25,.t.)
        if .o_PAFLESSC<>.w_PAFLESSC
            .w_PADATESC = IIf(.w_PAFLESSC='S',.w_PADATESC,cp_CharToDate("  -  -  "))
        endif
        .DoRTCalc(27,28,.t.)
        if .o_PAFLFAFI<>.w_PAFLFAFI
            .w_PASERFID = IIF(.w_PAFLFAFI<>'S',Space(20),.w_PASERFID)
          .link_1_45('Full')
        endif
        .DoRTCalc(30,34,.t.)
            .w_FLVEAC = 'V'
            .w_FILRF = 'RF'
            .w_FILFA = 'FA'
            .w_FILDT = 'DT'
        .DoRTCalc(39,63,.t.)
        if .o_PADOCFAF<>.w_PADOCFAF
          .link_2_36('Full')
        endif
        if .o_PADOCFAT<>.w_PADOCFAT
          .link_2_37('Full')
        endif
        .DoRTCalc(66,67,.t.)
        if .o_PADOCFAF<>.w_PADOCFAF
          .link_2_40('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_66.Calculate()
        .DoRTCalc(69,91,.t.)
            .w_TIPCON = 'G'
        if .o_PA3KIMPO<>.w_PA3KIMPO
          .Calculate_KYLBVMHVVJ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(93,120,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_66.Calculate()
    endwith
  return

  proc Calculate_SAZZFPKTSY()
    with this
          * --- Valore iniziale paflessc
          .w_OFLESSC = .w_PAFLESSC
    endwith
  endproc
  proc Calculate_KYLBVMHVVJ()
    with this
          * --- PA3KDFIS
          .w_PA3KDFIS = iif(.w_PA3KIMPO="N", "N", .w_PA3KDFIS)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPADATESC_1_40.enabled = this.oPgFrm.Page1.oPag.oPADATESC_1_40.mCond()
    this.oPgFrm.Page2.oPag.oPACODCEN_2_59.enabled = this.oPgFrm.Page2.oPag.oPACODCEN_2_59.mCond()
    this.oPgFrm.Page4.oPag.oPA3KDFIS_4_13.enabled = this.oPgFrm.Page4.oPag.oPA3KDFIS_4_13.mCond()
    this.oPgFrm.Page4.oPag.oPAACCPRE_4_21.enabled = this.oPgFrm.Page4.oPag.oPAACCPRE_4_21.mCond()
    this.oPgFrm.Page4.oPag.oPAPAGCON_4_22.enabled = this.oPgFrm.Page4.oPag.oPAPAGCON_4_22.mCond()
    this.oPgFrm.Page4.oPag.oPAPAGASS_4_23.enabled = this.oPgFrm.Page4.oPag.oPAPAGASS_4_23.mCond()
    this.oPgFrm.Page4.oPag.oPAPAGCAR_4_24.enabled = this.oPgFrm.Page4.oPag.oPAPAGCAR_4_24.mCond()
    this.oPgFrm.Page4.oPag.oPAPAGFIN_4_25.enabled = this.oPgFrm.Page4.oPag.oPAPAGFIN_4_25.mCond()
    this.oPgFrm.Page4.oPag.oPAPAGCLI_4_26.enabled = this.oPgFrm.Page4.oPag.oPAPAGCLI_4_26.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(4).enabled=not(g_AIFT<>"S")
    local i_show3
    i_show3=not(g_AIFT<>"S")
    this.oPgFrm.Pages(4).enabled=i_show3 and not(g_AIFT<>"S")
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Oper. maggiori 3.000 EU"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    this.oPgFrm.Page1.oPag.oPADATESC_1_40.visible=!this.oPgFrm.Page1.oPag.oPADATESC_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oPASERFID_1_45.visible=!this.oPgFrm.Page1.oPag.oPASERFID_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oSERDES_1_47.visible=!this.oPgFrm.Page1.oPag.oSERDES_1_47.mHide()
    this.oPgFrm.Page2.oPag.oPACODCEN_2_59.visible=!this.oPgFrm.Page2.oPag.oPACODCEN_2_59.mHide()
    this.oPgFrm.Page2.oPag.oDESPIA_2_61.visible=!this.oPgFrm.Page2.oPag.oDESPIA_2_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_64.visible=!this.oPgFrm.Page2.oPag.oStr_2_64.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_66.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PACODNEG
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODNEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_KAN',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_PACODNEG)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_PACODAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_PACODAZI;
                     ,'BUCODICE',trim(this.w_PACODNEG))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODNEG)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODNEG) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oPACODNEG_1_3'),i_cWhere,'GSPS_KAN',"Negozi azienda",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PACODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_PACODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODNEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_PACODNEG);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_PACODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_PACODAZI;
                       ,'BUCODICE',this.w_PACODNEG)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODNEG = NVL(_Link_.BUCODICE,space(3))
      this.w_DESNEG = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACODNEG = space(3)
      endif
      this.w_DESNEG = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODNEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BUSIUNIT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.BUCODICE as BUCODICE103"+ ",link_1_3.BUDESCRI as BUDESCRI103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on PAR_VDET.PACODNEG=link_1_3.BUCODICE"+" and PAR_VDET.PACODAZI=link_1_3.BUCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and PAR_VDET.PACODNEG=link_1_3.BUCODICE(+)"'+'+" and PAR_VDET.PACODAZI=link_1_3.BUCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAMASCON
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAMASCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PAMASCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PAMASCON))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAMASCON)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_PAMASCON)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_PAMASCON)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAMASCON) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPAMASCON_1_20'),i_cWhere,'GSAR_AMC',"Mastri contabili (clienti)",'GSAR_ACL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAMASCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PAMASCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PAMASCON)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAMASCON = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PAMASCON = space(15)
      endif
      this.w_DESMAS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAMASCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.MCCODICE as MCCODICE120"+ ",link_1_20.MCDESCRI as MCDESCRI120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on PAR_VDET.PAMASCON=link_1_20.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and PAR_VDET.PAMASCON=link_1_20.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACATCON
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_PACATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_PACATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oPACATCON_1_21'),i_cWhere,'GSAR_AC2',"Categorie contabili clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_PACATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_PACATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCCN = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PACATCON = space(5)
      endif
      this.w_DESCCN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.C2CODICE as C2CODICE121"+ ",link_1_21.C2DESCRI as C2DESCRI121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on PAR_VDET.PACATCON=link_1_21.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and PAR_VDET.PACATCON=link_1_21.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODLIN
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_PACODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_PACODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oPACODLIN_1_22'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_PACODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_PACODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PACODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.LUCODICE as LUCODICE122"+ ",link_1_22.LUDESCRI as LUDESCRI122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on PAR_VDET.PACODLIN=link_1_22.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and PAR_VDET.PACODLIN=link_1_22.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAARTMAG
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAARTMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_PAARTMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_PAARTMAG))
          select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAARTMAG)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PAARTMAG) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oPAARTMAG_1_34'),i_cWhere,'GSMA_BZA',"Servizi",'GSPS_ASV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAARTMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PAARTMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PAARTMAG)
            select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAARTMAG = NVL(_Link_.ARCODART,space(20))
      this.w_ARTDES = NVL(_Link_.ARDESART,space(40))
      this.w_ARTPOS = NVL(_Link_.ARARTPOS,space(1))
      this.w_ARTOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PAARTMAG = space(20)
      endif
      this.w_ARTDES = space(40)
      this.w_ARTPOS = space(1)
      this.w_ARTOBSO = ctod("  /  /  ")
      this.w_TIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTPOS='S' and .w_TIPART='FO' and (.w_ARTOBSO>i_DATSYS or empty(.w_ARTOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio non a valore, obsoleto o non P.O.S.")
        endif
        this.w_PAARTMAG = space(20)
        this.w_ARTDES = space(40)
        this.w_ARTPOS = space(1)
        this.w_ARTOBSO = ctod("  /  /  ")
        this.w_TIPART = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAARTMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_34.ARCODART as ARCODART134"+ ",link_1_34.ARDESART as ARDESART134"+ ",link_1_34.ARARTPOS as ARARTPOS134"+ ",link_1_34.ARDTOBSO as ARDTOBSO134"+ ",link_1_34.ARTIPART as ARTIPART134"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_34 on PAR_VDET.PAARTMAG=link_1_34.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_34"
          i_cKey=i_cKey+'+" and PAR_VDET.PAARTMAG=link_1_34.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PASERFID
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PASERFID) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_PASERFID)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_PASERFID))
          select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PASERFID)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PASERFID) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oPASERFID_1_45'),i_cWhere,'GSMA_BZA',"Servizi",'GSPS_ASV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PASERFID)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PASERFID);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PASERFID)
            select ARCODART,ARDESART,ARARTPOS,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PASERFID = NVL(_Link_.ARCODART,space(20))
      this.w_SERDES = NVL(_Link_.ARDESART,space(40))
      this.w_ARTPOSF = NVL(_Link_.ARARTPOS,space(1))
      this.w_ARTOBSOF = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPARTF = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PASERFID = space(20)
      endif
      this.w_SERDES = space(40)
      this.w_ARTPOSF = space(1)
      this.w_ARTOBSOF = ctod("  /  /  ")
      this.w_TIPARTF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTPOSF='S' and .w_TIPARTF='FO' and (.w_ARTOBSOF>i_DATSYS or empty(.w_ARTOBSOF))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio non a valore, obsoleto o non P.O.S.")
        endif
        this.w_PASERFID = space(20)
        this.w_SERDES = space(40)
        this.w_ARTPOSF = space(1)
        this.w_ARTOBSOF = ctod("  /  /  ")
        this.w_TIPARTF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PASERFID Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_45.ARCODART as ARCODART145"+ ",link_1_45.ARDESART as ARDESART145"+ ",link_1_45.ARARTPOS as ARARTPOS145"+ ",link_1_45.ARDTOBSO as ARDTOBSO145"+ ",link_1_45.ARTIPART as ARTIPART145"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_45 on PAR_VDET.PASERFID=link_1_45.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_45"
          i_cKey=i_cKey+'+" and PAR_VDET.PASERFID=link_1_45.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PADOCRIC
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PADOCRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PADOCRIC)+"%");
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);

          i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDCATDOC',this.w_FILRF;
                     ,'TDTIPDOC',trim(this.w_PADOCRIC))
          select TDCATDOC,TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PADOCRIC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PADOCRIC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oPADOCRIC_2_6'),i_cWhere,'GSVE_ATD',"Causali documento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FILRF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',oSource.xKey(1);
                       ,'TDTIPDOC',oSource.xKey(2))
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PADOCRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PADOCRIC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',this.w_FILRF;
                       ,'TDTIPDOC',this.w_PADOCRIC)
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PADOCRIC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESRIC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATRIC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PADOCRIC = space(5)
      endif
      this.w_DESRIC = space(35)
      this.w_CATRIC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATRIC='RF'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PADOCRIC = space(5)
        this.w_DESRIC = space(35)
        this.w_CATRIC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PADOCRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PADOCFAF
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PADOCFAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PADOCFAF)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILFA;
                     ,'TDTIPDOC',trim(this.w_PADOCFAF))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PADOCFAF)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PADOCFAF) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oPADOCFAF_2_8'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILFA<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PADOCFAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PADOCFAF);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILFA;
                       ,'TDTIPDOC',this.w_PADOCFAF)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PADOCFAF = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESFAF = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATFAF = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLFAF = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLRI1 = NVL(_Link_.TDFLRICE,space(1))
      this.w_CAUCO1 = NVL(_Link_.TDCAUCON,space(5))
      this.w_CAUFAF = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PADOCFAF = space(5)
      endif
      this.w_DESFAF = space(35)
      this.w_CATFAF = space(2)
      this.w_FLFAF = space(1)
      this.w_FLRI1 = space(1)
      this.w_CAUCO1 = space(5)
      this.w_CAUFAF = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATFAF='FA' AND .w_FLFAF='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PADOCFAF = space(5)
        this.w_DESFAF = space(35)
        this.w_CATFAF = space(2)
        this.w_FLFAF = space(1)
        this.w_FLRI1 = space(1)
        this.w_CAUCO1 = space(5)
        this.w_CAUFAF = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PADOCFAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PADOCFAT
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PADOCFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PADOCFAT)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILFA;
                     ,'TDTIPDOC',trim(this.w_PADOCFAT))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PADOCFAT)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PADOCFAT) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oPADOCFAT_2_10'),i_cWhere,'GSVE_ATD',"Causali documento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILFA<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PADOCFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PADOCFAT);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILFA;
                       ,'TDTIPDOC',this.w_PADOCFAT)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PADOCFAT = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESFAT = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATFAT = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLFAT = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLRI2 = NVL(_Link_.TDFLRICE,space(1))
      this.w_CAUCO2 = NVL(_Link_.TDCAUCON,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PADOCFAT = space(5)
      endif
      this.w_DESFAT = space(35)
      this.w_CATFAT = space(2)
      this.w_FLFAT = space(1)
      this.w_FLRI2 = space(1)
      this.w_CAUCO2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATFAT='FA' AND .w_FLFAT='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PADOCFAT = space(5)
        this.w_DESFAT = space(35)
        this.w_CATFAT = space(2)
        this.w_FLFAT = space(1)
        this.w_FLRI2 = space(1)
        this.w_CAUCO2 = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PADOCFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PADOCRIF
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PADOCRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PADOCRIF)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILDT;
                     ,'TDTIPDOC',trim(this.w_PADOCRIF))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PADOCRIF)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PADOCRIF) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oPADOCRIF_2_12'),i_cWhere,'GSVE_ATD',"Causali documento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILDT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PADOCRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PADOCRIF);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILDT;
                       ,'TDTIPDOC',this.w_PADOCRIF)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PADOCRIF = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESRIF = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATRIF = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLRIF = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PADOCRIF = space(5)
      endif
      this.w_DESRIF = space(35)
      this.w_CATRIF = space(2)
      this.w_FLRIF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATRIF='DT' AND .w_FLRIF='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PADOCRIF = space(5)
        this.w_DESRIF = space(35)
        this.w_CATRIF = space(2)
        this.w_FLRIF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PADOCRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PADOCDDT
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PADOCDDT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PADOCDDT)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILDT;
                     ,'TDTIPDOC',trim(this.w_PADOCDDT))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PADOCDDT)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PADOCDDT) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oPADOCDDT_2_14'),i_cWhere,'GSVE_ATD',"Causali documento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILDT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PADOCDDT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PADOCDDT);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILDT;
                       ,'TDTIPDOC',this.w_PADOCDDT)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PADOCDDT = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDDT = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDDT = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLDDT = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PADOCDDT = space(5)
      endif
      this.w_DESDDT = space(35)
      this.w_CATDDT = space(2)
      this.w_FLDDT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDDT='DT' AND .w_FLDDT='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PADOCDDT = space(5)
        this.w_DESDDT = space(35)
        this.w_CATDDT = space(2)
        this.w_FLDDT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PADOCDDT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PADOCCOR
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PADOCCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PADOCCOR)+"%");
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);

          i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDCATDOC',this.w_FILRF;
                     ,'TDTIPDOC',trim(this.w_PADOCCOR))
          select TDCATDOC,TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PADOCCOR)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PADOCCOR) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oPADOCCOR_2_16'),i_cWhere,'GSVE_ATD',"Causali documento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FILRF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',oSource.xKey(1);
                       ,'TDTIPDOC',oSource.xKey(2))
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PADOCCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PADOCCOR);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',this.w_FILRF;
                       ,'TDTIPDOC',this.w_PADOCCOR)
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PADOCCOR = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCOR = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATCOR = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PADOCCOR = space(5)
      endif
      this.w_DESCOR = space(35)
      this.w_CATCOR = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATCOR='RF'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PADOCCOR = space(5)
        this.w_DESCOR = space(35)
        this.w_CATCOR = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PADOCCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCO1
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCO1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCO1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCO1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCO1)
            select CCCODICE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCO1 = NVL(_Link_.CCCODICE,space(5))
      this.w_TIPDO1 = NVL(_Link_.CCTIPDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCO1 = space(5)
      endif
      this.w_TIPDO1 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCO1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCO2
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCO2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCO2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCO2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCO2)
            select CCCODICE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCO2 = NVL(_Link_.CCCODICE,space(5))
      this.w_TIPDO2 = NVL(_Link_.CCTIPDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCO2 = space(5)
      endif
      this.w_TIPDO2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCO2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUFAF
  func Link_2_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUFAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUFAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLCASC";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUFAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUFAF)
            select CMCODICE,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUFAF = NVL(_Link_.CMCODICE,space(5))
      this.w_CASFAF = NVL(_Link_.CMFLCASC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUFAF = space(5)
      endif
      this.w_CASFAF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUFAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACAURES
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACAURES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PACAURES)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PACAURES))
          select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACAURES)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_PACAURES)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_PACAURES)+"%");

            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACAURES) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPACAURES_2_42'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACAURES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PACAURES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PACAURES)
            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACAURES = NVL(_Link_.CMCODICE,space(5))
      this.w_DESRES = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLRESI = NVL(_Link_.CMFLCASC,space(1))
      this.w_RESCOL = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PACAURES = space(5)
      endif
      this.w_DESRES = space(35)
      this.w_FLRESI = space(1)
      this.w_RESCOL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLRESI='+' AND EMPTY(.w_RESCOL)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale magazzino resi inesistente o incongruente")
        endif
        this.w_PACAURES = space(5)
        this.w_DESRES = space(35)
        this.w_FLRESI = space(1)
        this.w_RESCOL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACAURES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_42(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_42.CMCODICE as CMCODICE242"+ ",link_2_42.CMDESCRI as CMDESCRI242"+ ",link_2_42.CMFLCASC as CMFLCASC242"+ ",link_2_42.CMCAUCOL as CMCAUCOL242"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_42 on PAR_VDET.PACAURES=link_2_42.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_42"
          i_cKey=i_cKey+'+" and PAR_VDET.PACAURES=link_2_42.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACARKIT
  func Link_2_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACARKIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PACARKIT)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PACARKIT))
          select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACARKIT)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_PACARKIT)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_PACARKIT)+"%");

            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACARKIT) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPACARKIT_2_48'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACARKIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PACARKIT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PACARKIT)
            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACARKIT = NVL(_Link_.CMCODICE,space(5))
      this.w_DESKCA = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLKCA = NVL(_Link_.CMFLCASC,space(1))
      this.w_CARCOL = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PACARKIT = space(5)
      endif
      this.w_DESKCA = space(35)
      this.w_FLKCA = space(1)
      this.w_CARCOL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLKCA='+' AND EMPTY(.w_CARCOL)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale magazzino carichi kit inesistente o incongruente")
        endif
        this.w_PACARKIT = space(5)
        this.w_DESKCA = space(35)
        this.w_FLKCA = space(1)
        this.w_CARCOL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACARKIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_48(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_48.CMCODICE as CMCODICE248"+ ",link_2_48.CMDESCRI as CMDESCRI248"+ ",link_2_48.CMFLCASC as CMFLCASC248"+ ",link_2_48.CMCAUCOL as CMCAUCOL248"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_48 on PAR_VDET.PACARKIT=link_2_48.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_48"
          i_cKey=i_cKey+'+" and PAR_VDET.PACARKIT=link_2_48.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PASCAKIT
  func Link_2_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PASCAKIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PASCAKIT)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PASCAKIT))
          select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PASCAKIT)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_PASCAKIT)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_PASCAKIT)+"%");

            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PASCAKIT) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPASCAKIT_2_49'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PASCAKIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PASCAKIT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PASCAKIT)
            select CMCODICE,CMDESCRI,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PASCAKIT = NVL(_Link_.CMCODICE,space(5))
      this.w_DESKSC = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLKSC = NVL(_Link_.CMFLCASC,space(1))
      this.w_SCACOL = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PASCAKIT = space(5)
      endif
      this.w_DESKSC = space(35)
      this.w_FLKSC = space(1)
      this.w_SCACOL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLKSC='-' AND EMPTY(.w_SCACOL)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale magazzino scarichi componenti kit inesistente o incongruente")
        endif
        this.w_PASCAKIT = space(5)
        this.w_DESKSC = space(35)
        this.w_FLKSC = space(1)
        this.w_SCACOL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PASCAKIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_49(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_49.CMCODICE as CMCODICE249"+ ",link_2_49.CMDESCRI as CMDESCRI249"+ ",link_2_49.CMFLCASC as CMFLCASC249"+ ",link_2_49.CMCAUCOL as CMCAUCOL249"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_49 on PAR_VDET.PASCAKIT=link_2_49.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_49"
          i_cKey=i_cKey+'+" and PAR_VDET.PASCAKIT=link_2_49.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODCEN
  func Link_2_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PACODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PACODCEN))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPACODCEN_2_59'),i_cWhere,'GSCA_ACC',"Centri di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PACODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PACODCEN)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_OBSCEN = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACODCEN = space(15)
      endif
      this.w_DESPIA = space(40)
      this.w_OBSCEN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_OBSCEN,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo obsoleto")
        endif
        this.w_PACODCEN = space(15)
        this.w_DESPIA = space(40)
        this.w_OBSCEN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_59(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_59.CC_CONTO as CC_CONTO259"+ ",link_2_59.CCDESPIA as CCDESPIA259"+ ",link_2_59.CCDTOBSO as CCDTOBSO259"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_59 on PAR_VDET.PACODCEN=link_2_59.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_59"
          i_cKey=i_cKey+'+" and PAR_VDET.PACODCEN=link_2_59.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACONPRE
  func Link_3_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACONPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PACONPRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PACONPRE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACONPRE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PACONPRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PACONPRE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACONPRE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPACONPRE_3_1'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACONPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PACONPRE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PACONPRE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACONPRE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPRE = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOPRE = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACONPRE = space(15)
      endif
      this.w_DESPRE = space(40)
      this.w_DTOPRE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOPRE Or Empty(.w_DTOPRE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_PACONPRE = space(15)
        this.w_DESPRE = space(40)
        this.w_DTOPRE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACONPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACONCON
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACONCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PACONCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PACONCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACONCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PACONCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PACONCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACONCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPACONCON_3_14'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACONCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PACONCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PACONCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACONCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOCON = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACONCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DTOCON = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOCON Or Empty(.w_DTOCON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_PACONCON = space(15)
        this.w_DESCON = space(40)
        this.w_DTOCON = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACONCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACONASS
  func Link_3_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACONASS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PACONASS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PACONASS))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACONASS)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PACONASS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PACONASS)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACONASS) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPACONASS_3_15'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACONASS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PACONASS);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PACONASS)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACONASS = NVL(_Link_.ANCODICE,space(15))
      this.w_DESASS = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOASS = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACONASS = space(15)
      endif
      this.w_DESASS = space(40)
      this.w_DTOASS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOASS Or Empty(.w_DTOASS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_PACONASS = space(15)
        this.w_DESASS = space(40)
        this.w_DTOASS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACONASS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACONCAR
  func Link_3_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACONCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PACONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PACONCAR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACONCAR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PACONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PACONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACONCAR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPACONCAR_3_16'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACONCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PACONCAR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PACONCAR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACONCAR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCAR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOCAR = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACONCAR = space(15)
      endif
      this.w_DESCAR = space(40)
      this.w_DTOCAR = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOCAR Or Empty(.w_DTOCAR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_PACONCAR = space(15)
        this.w_DESCAR = space(40)
        this.w_DTOCAR = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACONCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACONFIN
  func Link_3_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACONFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PACONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PACONFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACONFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PACONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PACONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACONFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPACONFIN_3_17'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACONFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PACONFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PACONFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACONFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOFIN = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACONFIN = space(15)
      endif
      this.w_DESFIN = space(40)
      this.w_DTOFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOFIN Or Empty(.w_DTOFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_PACONFIN = space(15)
        this.w_DESFIN = space(40)
        this.w_DTOFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACONFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PA3KCORR
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PA3KCORR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PA3KCORR)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                   +" and TDFLINTE="+cp_ToStrODBC(this.w_FILC);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC,TDDESDOC,TDCAUCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILRF;
                     ,'TDFLINTE',this.w_FILC;
                     ,'TDTIPDOC',trim(this.w_PA3KCORR))
          select TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC,TDDESDOC,TDCAUCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PA3KCORR)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PA3KCORR) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC',cp_AbsName(oSource.parent,'oPA3KCORR_4_2'),i_cWhere,'GSVE_ATD',"Causali documento",'gsps_apa.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILRF<>oSource.xKey(2);
           .or. this.w_FILC<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC,TDDESDOC,TDCAUCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC,TDDESDOC,TDCAUCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC,TDDESDOC,TDCAUCON";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(4));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                     +" and TDFLINTE="+cp_ToStrODBC(this.w_FILC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDFLINTE',oSource.xKey(3);
                       ,'TDTIPDOC',oSource.xKey(4))
            select TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC,TDDESDOC,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PA3KCORR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC,TDDESDOC,TDCAUCON";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PA3KCORR);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                   +" and TDFLINTE="+cp_ToStrODBC(this.w_FILC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILRF;
                       ,'TDFLINTE',this.w_FILC;
                       ,'TDTIPDOC',this.w_PA3KCORR)
            select TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC,TDDESDOC,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PA3KCORR = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DES3KC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CAT3KC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FL3KC = NVL(_Link_.TDFLINTE,space(1))
      this.w_CON3KC = NVL(_Link_.TDCAUCON,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PA3KCORR = space(5)
      endif
      this.w_DES3KC = space(35)
      this.w_CAT3KC = space(2)
      this.w_FL3KC = space(1)
      this.w_CON3KC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CAT3KC='RF' AND .w_FL3KC='C' AND NVL(LOOKTAB("CAU_CONT", "CCFLRIFE", "CCCODICE", .w_CON3KC)," ")="C"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PA3KCORR = space(5)
        this.w_DES3KC = space(35)
        this.w_CAT3KC = space(2)
        this.w_FL3KC = space(1)
        this.w_CON3KC = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDFLINTE,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PA3KCORR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PA3KRICF
  func Link_4_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PA3KRICF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PA3KRICF)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILRF;
                     ,'TDTIPDOC',trim(this.w_PA3KRICF))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PA3KRICF)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PA3KRICF) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oPA3KRICF_4_4'),i_cWhere,'GSVE_ATD',"Causali documento",'gsps_apa.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILRF<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUCON";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PA3KRICF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUCON";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PA3KRICF);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILRF;
                       ,'TDTIPDOC',this.w_PA3KRICF)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLINTE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PA3KRICF = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DES3KR = NVL(_Link_.TDDESDOC,space(35))
      this.w_CAT3KR = NVL(_Link_.TDCATDOC,space(2))
      this.w_FL3KR = NVL(_Link_.TDFLINTE,space(1))
      this.w_CON3KR = NVL(_Link_.TDCAUCON,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PA3KRICF = space(5)
      endif
      this.w_DES3KR = space(35)
      this.w_CAT3KR = space(2)
      this.w_FL3KR = space(1)
      this.w_CON3KR = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CAT3KR='RF' AND .w_FL3KR='C' AND NVL(LOOKTAB("CAU_CONT", "CCFLRIFE", "CCCODICE", .w_CON3KR)," ")="C"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PA3KRICF = space(5)
        this.w_DES3KR = space(35)
        this.w_CAT3KR = space(2)
        this.w_FL3KR = space(1)
        this.w_CON3KR = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PA3KRICF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPACODNEG_1_3.value==this.w_PACODNEG)
      this.oPgFrm.Page1.oPag.oPACODNEG_1_3.value=this.w_PACODNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oPAPRINEG_1_4.RadioValue()==this.w_PAPRINEG)
      this.oPgFrm.Page1.oPag.oPAPRINEG_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAALFDOC_1_6.value==this.w_PAALFDOC)
      this.oPgFrm.Page1.oPag.oPAALFDOC_1_6.value=this.w_PAALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNEG_1_7.value==this.w_DESNEG)
      this.oPgFrm.Page1.oPag.oDESNEG_1_7.value=this.w_DESNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oPAPARETI_1_11.value==this.w_PAPARETI)
      this.oPgFrm.Page1.oPag.oPAPARETI_1_11.value=this.w_PAPARETI
    endif
    if not(this.oPgFrm.Page1.oPag.oPAMASPOS_1_16.value==this.w_PAMASPOS)
      this.oPgFrm.Page1.oPag.oPAMASPOS_1_16.value=this.w_PAMASPOS
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLCPOS_1_17.RadioValue()==this.w_PAFLCPOS)
      this.oPgFrm.Page1.oPag.oPAFLCPOS_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLAUTC_1_19.RadioValue()==this.w_PAFLAUTC)
      this.oPgFrm.Page1.oPag.oPAFLAUTC_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAMASCON_1_20.value==this.w_PAMASCON)
      this.oPgFrm.Page1.oPag.oPAMASCON_1_20.value=this.w_PAMASCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPACATCON_1_21.value==this.w_PACATCON)
      this.oPgFrm.Page1.oPag.oPACATCON_1_21.value=this.w_PACATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODLIN_1_22.value==this.w_PACODLIN)
      this.oPgFrm.Page1.oPag.oPACODLIN_1_22.value=this.w_PACODLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAS_1_23.value==this.w_DESMAS)
      this.oPgFrm.Page1.oPag.oDESMAS_1_23.value=this.w_DESMAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCN_1_25.value==this.w_DESCCN)
      this.oPgFrm.Page1.oPag.oDESCCN_1_25.value=this.w_DESCCN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIN_1_26.value==this.w_DESLIN)
      this.oPgFrm.Page1.oPag.oDESLIN_1_26.value=this.w_DESLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPAARTMAG_1_34.value==this.w_PAARTMAG)
      this.oPgFrm.Page1.oPag.oPAARTMAG_1_34.value=this.w_PAARTMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oARTDES_1_36.value==this.w_ARTDES)
      this.oPgFrm.Page1.oPag.oARTDES_1_36.value=this.w_ARTDES
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLESSC_1_39.RadioValue()==this.w_PAFLESSC)
      this.oPgFrm.Page1.oPag.oPAFLESSC_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATESC_1_40.value==this.w_PADATESC)
      this.oPgFrm.Page1.oPag.oPADATESC_1_40.value=this.w_PADATESC
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLFAFI_1_44.RadioValue()==this.w_PAFLFAFI)
      this.oPgFrm.Page1.oPag.oPAFLFAFI_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPASERFID_1_45.value==this.w_PASERFID)
      this.oPgFrm.Page1.oPag.oPASERFID_1_45.value=this.w_PASERFID
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDES_1_47.value==this.w_SERDES)
      this.oPgFrm.Page1.oPag.oSERDES_1_47.value=this.w_SERDES
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLPROM_1_51.RadioValue()==this.w_PAFLPROM)
      this.oPgFrm.Page1.oPag.oPAFLPROM_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOR_2_5.value==this.w_DESCOR)
      this.oPgFrm.Page2.oPag.oDESCOR_2_5.value=this.w_DESCOR
    endif
    if not(this.oPgFrm.Page2.oPag.oPADOCRIC_2_6.value==this.w_PADOCRIC)
      this.oPgFrm.Page2.oPag.oPADOCRIC_2_6.value=this.w_PADOCRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIC_2_7.value==this.w_DESRIC)
      this.oPgFrm.Page2.oPag.oDESRIC_2_7.value=this.w_DESRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oPADOCFAF_2_8.value==this.w_PADOCFAF)
      this.oPgFrm.Page2.oPag.oPADOCFAF_2_8.value=this.w_PADOCFAF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAF_2_9.value==this.w_DESFAF)
      this.oPgFrm.Page2.oPag.oDESFAF_2_9.value=this.w_DESFAF
    endif
    if not(this.oPgFrm.Page2.oPag.oPADOCFAT_2_10.value==this.w_PADOCFAT)
      this.oPgFrm.Page2.oPag.oPADOCFAT_2_10.value=this.w_PADOCFAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAT_2_11.value==this.w_DESFAT)
      this.oPgFrm.Page2.oPag.oDESFAT_2_11.value=this.w_DESFAT
    endif
    if not(this.oPgFrm.Page2.oPag.oPADOCRIF_2_12.value==this.w_PADOCRIF)
      this.oPgFrm.Page2.oPag.oPADOCRIF_2_12.value=this.w_PADOCRIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIF_2_13.value==this.w_DESRIF)
      this.oPgFrm.Page2.oPag.oDESRIF_2_13.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page2.oPag.oPADOCDDT_2_14.value==this.w_PADOCDDT)
      this.oPgFrm.Page2.oPag.oPADOCDDT_2_14.value=this.w_PADOCDDT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDDT_2_15.value==this.w_DESDDT)
      this.oPgFrm.Page2.oPag.oDESDDT_2_15.value=this.w_DESDDT
    endif
    if not(this.oPgFrm.Page2.oPag.oPADOCCOR_2_16.value==this.w_PADOCCOR)
      this.oPgFrm.Page2.oPag.oPADOCCOR_2_16.value=this.w_PADOCCOR
    endif
    if not(this.oPgFrm.Page2.oPag.oPAFLCONT_2_17.RadioValue()==this.w_PAFLCONT)
      this.oPgFrm.Page2.oPag.oPAFLCONT_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPACAURES_2_42.value==this.w_PACAURES)
      this.oPgFrm.Page2.oPag.oPACAURES_2_42.value=this.w_PACAURES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRES_2_46.value==this.w_DESRES)
      this.oPgFrm.Page2.oPag.oDESRES_2_46.value=this.w_DESRES
    endif
    if not(this.oPgFrm.Page2.oPag.oPACARKIT_2_48.value==this.w_PACARKIT)
      this.oPgFrm.Page2.oPag.oPACARKIT_2_48.value=this.w_PACARKIT
    endif
    if not(this.oPgFrm.Page2.oPag.oPASCAKIT_2_49.value==this.w_PASCAKIT)
      this.oPgFrm.Page2.oPag.oPASCAKIT_2_49.value=this.w_PASCAKIT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESKCA_2_50.value==this.w_DESKCA)
      this.oPgFrm.Page2.oPag.oDESKCA_2_50.value=this.w_DESKCA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESKSC_2_52.value==this.w_DESKSC)
      this.oPgFrm.Page2.oPag.oDESKSC_2_52.value=this.w_DESKSC
    endif
    if not(this.oPgFrm.Page2.oPag.oPACODCEN_2_59.value==this.w_PACODCEN)
      this.oPgFrm.Page2.oPag.oPACODCEN_2_59.value=this.w_PACODCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPIA_2_61.value==this.w_DESPIA)
      this.oPgFrm.Page2.oPag.oDESPIA_2_61.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page3.oPag.oPACONPRE_3_1.value==this.w_PACONPRE)
      this.oPgFrm.Page3.oPag.oPACONPRE_3_1.value=this.w_PACONPRE
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCON_3_2.value==this.w_DESCON)
      this.oPgFrm.Page3.oPag.oDESCON_3_2.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page3.oPag.oDESPRE_3_3.value==this.w_DESPRE)
      this.oPgFrm.Page3.oPag.oDESPRE_3_3.value=this.w_DESPRE
    endif
    if not(this.oPgFrm.Page3.oPag.oDESASS_3_4.value==this.w_DESASS)
      this.oPgFrm.Page3.oPag.oDESASS_3_4.value=this.w_DESASS
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCAR_3_5.value==this.w_DESCAR)
      this.oPgFrm.Page3.oPag.oDESCAR_3_5.value=this.w_DESCAR
    endif
    if not(this.oPgFrm.Page3.oPag.oDESFIN_3_6.value==this.w_DESFIN)
      this.oPgFrm.Page3.oPag.oDESFIN_3_6.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oPACONCON_3_14.value==this.w_PACONCON)
      this.oPgFrm.Page3.oPag.oPACONCON_3_14.value=this.w_PACONCON
    endif
    if not(this.oPgFrm.Page3.oPag.oPACONASS_3_15.value==this.w_PACONASS)
      this.oPgFrm.Page3.oPag.oPACONASS_3_15.value=this.w_PACONASS
    endif
    if not(this.oPgFrm.Page3.oPag.oPACONCAR_3_16.value==this.w_PACONCAR)
      this.oPgFrm.Page3.oPag.oPACONCAR_3_16.value=this.w_PACONCAR
    endif
    if not(this.oPgFrm.Page3.oPag.oPACONFIN_3_17.value==this.w_PACONFIN)
      this.oPgFrm.Page3.oPag.oPACONFIN_3_17.value=this.w_PACONFIN
    endif
    if not(this.oPgFrm.Page4.oPag.oPA3KCORR_4_2.value==this.w_PA3KCORR)
      this.oPgFrm.Page4.oPag.oPA3KCORR_4_2.value=this.w_PA3KCORR
    endif
    if not(this.oPgFrm.Page4.oPag.oDES3KC_4_3.value==this.w_DES3KC)
      this.oPgFrm.Page4.oPag.oDES3KC_4_3.value=this.w_DES3KC
    endif
    if not(this.oPgFrm.Page4.oPag.oPA3KRICF_4_4.value==this.w_PA3KRICF)
      this.oPgFrm.Page4.oPag.oPA3KRICF_4_4.value=this.w_PA3KRICF
    endif
    if not(this.oPgFrm.Page4.oPag.oPA3KIMPO_4_5.RadioValue()==this.w_PA3KIMPO)
      this.oPgFrm.Page4.oPag.oPA3KIMPO_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDES3KR_4_6.value==this.w_DES3KR)
      this.oPgFrm.Page4.oPag.oDES3KR_4_6.value=this.w_DES3KR
    endif
    if not(this.oPgFrm.Page4.oPag.oPA3KDFIS_4_13.RadioValue()==this.w_PA3KDFIS)
      this.oPgFrm.Page4.oPag.oPA3KDFIS_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPAACCPRE_4_21.RadioValue()==this.w_PAACCPRE)
      this.oPgFrm.Page4.oPag.oPAACCPRE_4_21.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPAGCON_4_22.RadioValue()==this.w_PAPAGCON)
      this.oPgFrm.Page4.oPag.oPAPAGCON_4_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPAGASS_4_23.RadioValue()==this.w_PAPAGASS)
      this.oPgFrm.Page4.oPag.oPAPAGASS_4_23.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPAGCAR_4_24.RadioValue()==this.w_PAPAGCAR)
      this.oPgFrm.Page4.oPag.oPAPAGCAR_4_24.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPAGFIN_4_25.RadioValue()==this.w_PAPAGFIN)
      this.oPgFrm.Page4.oPag.oPAPAGFIN_4_25.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPAGCLI_4_26.RadioValue()==this.w_PAPAGCLI)
      this.oPgFrm.Page4.oPag.oPAPAGCLI_4_26.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PAR_VDET')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(ckserneg(.w_PACODAZI,.w_PAALFDOC,.w_PACODNEG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAALFDOC_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile associare la stessa serie, anche nulla, a due negozi.")
          case   not(.w_ARTPOS='S' and .w_TIPART='FO' and (.w_ARTOBSO>i_DATSYS or empty(.w_ARTOBSO)))  and not(empty(.w_PAARTMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAARTMAG_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio non a valore, obsoleto o non P.O.S.")
          case   (empty(.w_PADATESC))  and not(.w_PAFLESSC<>'S')  and (.w_PAFLESSC='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPADATESC_1_40.SetFocus()
            i_bnoObbl = !empty(.w_PADATESC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PASERFID)) or not(.w_ARTPOSF='S' and .w_TIPARTF='FO' and (.w_ARTOBSOF>i_DATSYS or empty(.w_ARTOBSOF))))  and not(.w_PAFLFAFI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPASERFID_1_45.SetFocus()
            i_bnoObbl = !empty(.w_PASERFID)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio non a valore, obsoleto o non P.O.S.")
          case   not(.w_CATRIC='RF')  and not(empty(.w_PADOCRIC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPADOCRIC_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATFAF='FA' AND .w_FLFAF='V')  and not(empty(.w_PADOCFAF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPADOCFAF_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATFAT='FA' AND .w_FLFAT='V')  and not(empty(.w_PADOCFAT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPADOCFAT_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATRIF='DT' AND .w_FLRIF='V')  and not(empty(.w_PADOCRIF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPADOCRIF_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATDDT='DT' AND .w_FLDDT='V')  and not(empty(.w_PADOCDDT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPADOCDDT_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATCOR='RF')  and not(empty(.w_PADOCCOR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPADOCCOR_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PACAURES)) or not(.w_FLRESI='+' AND EMPTY(.w_RESCOL)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPACAURES_2_42.SetFocus()
            i_bnoObbl = !empty(.w_PACAURES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale magazzino resi inesistente o incongruente")
          case   ((empty(.w_PACARKIT)) or not(.w_FLKCA='+' AND EMPTY(.w_CARCOL)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPACARKIT_2_48.SetFocus()
            i_bnoObbl = !empty(.w_PACARKIT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale magazzino carichi kit inesistente o incongruente")
          case   ((empty(.w_PASCAKIT)) or not(.w_FLKSC='-' AND EMPTY(.w_SCACOL)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPASCAKIT_2_49.SetFocus()
            i_bnoObbl = !empty(.w_PASCAKIT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale magazzino scarichi componenti kit inesistente o incongruente")
          case   not(CHKDTOBS(.w_OBSCEN,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.))  and not(g_PERCCR<>'S')  and (g_PERCCR='S')  and not(empty(.w_PACODCEN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPACODCEN_2_59.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo obsoleto")
          case   not(i_DATSYS < .w_DTOPRE Or Empty(.w_DTOPRE))  and not(empty(.w_PACONPRE))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPACONPRE_3_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   not(i_DATSYS < .w_DTOCON Or Empty(.w_DTOCON))  and not(empty(.w_PACONCON))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPACONCON_3_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   not(i_DATSYS < .w_DTOASS Or Empty(.w_DTOASS))  and not(empty(.w_PACONASS))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPACONASS_3_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   not(i_DATSYS < .w_DTOCAR Or Empty(.w_DTOCAR))  and not(empty(.w_PACONCAR))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPACONCAR_3_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   not(i_DATSYS < .w_DTOFIN Or Empty(.w_DTOFIN))  and not(empty(.w_PACONFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPACONFIN_3_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   not(.w_CAT3KC='RF' AND .w_FL3KC='C' AND NVL(LOOKTAB("CAU_CONT", "CCFLRIFE", "CCCODICE", .w_CON3KC)," ")="C")  and not(empty(.w_PA3KCORR))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPA3KCORR_4_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAT3KR='RF' AND .w_FL3KR='C' AND NVL(LOOKTAB("CAU_CONT", "CCFLRIFE", "CCCODICE", .w_CON3KR)," ")="C")  and not(empty(.w_PA3KRICF))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPA3KRICF_4_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_apa
      if i_bRes=.t. and .w_PAFLAUTC='S'
        * --- Verifica presenza Impostazioni di Default
        do case
          case empty(.w_PAMASCON)
            i_bRes = .f.
            i_bnoChk = .f.		
      	    i_cErrorMsg = Ah_MsgFormat("Mastro contabile non definito")
          case empty(.w_PACATCON)
            i_bRes = .f.
            i_bnoChk = .f.		
      	    i_cErrorMsg = Ah_MsgFormat("Categoria contabile non definita")
          case empty(.w_PACODLIN)
            i_bRes = .f.
            i_bnoChk = .f.		
      	    i_cErrorMsg = Ah_MsgFormat("Codice lingua non definito")
        endcase
      endif
      if i_bRes=.t.
        * --- Verifica presenza Negozio Principale
        .NotifyEvent('Controllo')
        if this.w_UNITST='F'
           i_bRes = .f.
           i_bnoChk = .t.		
      	   i_cErrorMsg = ""
        endif
      endif
      if i_bRes=.t.
        * --- Verifica congruenza causali
        if not empty(.w_PADOCFAF)
          if (g_COGE<>'S' AND .w_FLRI1<>'S') OR (g_COGE='S' AND .w_TIPDO1<>'FC')
            i_bRes = .f.
            i_bnoChk = .f.		
      	    i_cErrorMsg = Ah_MsgFormat("Causale fattura fiscale incongruente (no compresa in corrispettivi)")
          else
            if not empty(.w_CASFAF)
              i_bRes = .f.
              i_bnoChk = .f.		
      	      i_cErrorMsg = Ah_MsgFormat("Causale fattura fiscale incongruente (aggiorna il magazzino)")
            endif
          endif
        endif
        if not empty(.w_PADOCFAT)
          if (g_COGE<>'S' AND .w_FLRI2='S') OR (g_COGE='S' AND .w_TIPDO2='FC')
            i_bRes = .f.
            i_bnoChk = .f.		
      	    i_cErrorMsg = Ah_MsgFormat("Causale fattura immediata incongruente (compresa in corrispettivi)")
          endif
        endif
      endif
      
      if i_bRes
          Ah_Msg('Check finali...',.T.)
           .NotifyEvent('ChkFinali')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PAFLESSC = this.w_PAFLESSC
    this.o_PAFLFAFI = this.w_PAFLFAFI
    this.o_PADOCFAF = this.w_PADOCFAF
    this.o_PADOCFAT = this.w_PADOCFAT
    this.o_PA3KIMPO = this.w_PA3KIMPO
    return

enddefine

* --- Define pages as container
define class tgsps_apaPag1 as StdContainer
  Width  = 582
  height = 388
  stdWidth  = 582
  stdheight = 388
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACODNEG_1_3 as StdField with uid="MTQJQUEQVV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PACODNEG", cQueryName = "PACODAZI,PACODNEG",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice negozio",;
    HelpContextID = 151658045,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=13, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSPS_KAN", oKey_1_1="BUCODAZI", oKey_1_2="this.w_PACODAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_PACODNEG"

  func oPACODNEG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODNEG_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODNEG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_PACODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_PACODAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oPACODNEG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_KAN',"Negozi azienda",'',this.parent.oContained
  endproc
  proc oPACODNEG_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSPS_KAN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_PACODAZI
     i_obj.w_BUCODICE=this.parent.oContained.w_PACODNEG
     i_obj.ecpSave()
  endproc

  add object oPAPRINEG_1_4 as StdCheck with uid="ZQGYBVIPRN",rtseq=4,rtrep=.f.,left=125, top=39, caption="Negozio principale",;
    ToolTipText = "Se attivo: indica il negozio principale",;
    HelpContextID = 157150781,;
    cFormVar="w_PAPRINEG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAPRINEG_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAPRINEG_1_4.GetRadio()
    this.Parent.oContained.w_PAPRINEG = this.RadioValue()
    return .t.
  endfunc

  func oPAPRINEG_1_4.SetRadio()
    this.Parent.oContained.w_PAPRINEG=trim(this.Parent.oContained.w_PAPRINEG)
    this.value = ;
      iif(this.Parent.oContained.w_PAPRINEG=='S',1,;
      0)
  endfunc

  add object oPAALFDOC_1_6 as StdField with uid="KWACTBPWSD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PAALFDOC", cQueryName = "PAALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile associare la stessa serie, anche nulla, a due negozi.",;
    ToolTipText = "Serie dei documenti generati",;
    HelpContextID = 14221767,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=419, Top=40, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oPAALFDOC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (ckserneg(.w_PACODAZI,.w_PAALFDOC,.w_PACODNEG))
    endwith
    return bRes
  endfunc

  add object oDESNEG_1_7 as StdField with uid="SBPRTBOZLA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESNEG", cQueryName = "DESNEG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione negozio",;
    HelpContextID = 233168586,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=174, Top=13, InputMask=replicate('X',40)

  add object oPAPARETI_1_11 as StdField with uid="PZLFTABCXS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PAPARETI", cQueryName = "PAPARETI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Intestazione applicata alle stampe etichette/frontalini barcode articoli",;
    HelpContextID = 14478911,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=125, Top=67, InputMask=replicate('X',40)


  add object oObj_1_13 as cp_runprogram with uid="FGHNDXSWWG",left=10, top=412, width=340,height=19,;
    caption='GSAR_BAM(modifica)',;
   bGlobalFont=.t.,;
    prg="GSAR_BAM('modifica')",;
    cEvent = "Record Updated,Record Inserted",;
    nPag=1;
    , HelpContextID = 71786842


  add object oObj_1_15 as cp_runprogram with uid="ZXIPXOKENR",left=10, top=437, width=199,height=19,;
    caption='GSPC_BCU',;
   bGlobalFont=.t.,;
    prg="GSPS_BCU",;
    cEvent = "Controllo",;
    nPag=1;
    , HelpContextID = 22085701

  add object oPAMASPOS_1_16 as StdField with uid="HHCZQVIGJS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PAMASPOS", cQueryName = "PAMASPOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici nominativi",;
    HelpContextID = 68370871,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=125, Top=119, InputMask=replicate('X',15)

  add object oPAFLCPOS_1_17 as StdCheck with uid="WOEWOKGIDF",rtseq=12,rtrep=.f.,left=262, top=119, caption="Codifica numerica",;
    ToolTipText = "Se attivo: i nominativi verranno codificati numericamente",;
    HelpContextID = 84455863,;
    cFormVar="w_PAFLCPOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLCPOS_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLCPOS_1_17.GetRadio()
    this.Parent.oContained.w_PAFLCPOS = this.RadioValue()
    return .t.
  endfunc

  func oPAFLCPOS_1_17.SetRadio()
    this.Parent.oContained.w_PAFLCPOS=trim(this.Parent.oContained.w_PAFLCPOS)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLCPOS=='S',1,;
      0)
  endfunc

  add object oPAFLAUTC_1_19 as StdCheck with uid="QXLHVOUKFB",rtseq=13,rtrep=.f.,left=125, top=145, caption="Inserimento automatico",;
    ToolTipText = "Se attivo: la procedura di generazione documenti inserir� automaticamente il cliente se non esiste",;
    HelpContextID = 265768505,;
    cFormVar="w_PAFLAUTC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLAUTC_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLAUTC_1_19.GetRadio()
    this.Parent.oContained.w_PAFLAUTC = this.RadioValue()
    return .t.
  endfunc

  func oPAFLAUTC_1_19.SetRadio()
    this.Parent.oContained.w_PAFLAUTC=trim(this.Parent.oContained.w_PAFLAUTC)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLAUTC=='S',1,;
      0)
  endfunc

  add object oPAMASCON_1_20 as StdField with uid="XYMPAGOFGY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PAMASCON", cQueryName = "PAMASCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro contabile di default",;
    HelpContextID = 18039228,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=125, Top=172, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PAMASCON"

  func oPAMASCON_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAMASCON_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAMASCON_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPAMASCON_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (clienti)",'GSAR_ACL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPAMASCON_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PAMASCON
     i_obj.ecpSave()
  endproc

  add object oPACATCON_1_21 as StdField with uid="EIIEQTDXBY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PACATCON", cQueryName = "PACATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile di default",;
    HelpContextID = 17031612,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=198, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_PACATCON"

  func oPACATCON_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACATCON_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACATCON_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oPACATCON_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili clienti",'',this.parent.oContained
  endproc
  proc oPACATCON_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_PACATCON
     i_obj.ecpSave()
  endproc

  add object oPACODLIN_1_22 as StdField with uid="JIPABBNPNV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PACODLIN", cQueryName = "PACODLIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice lingua di default in cui stampare i documenti",;
    HelpContextID = 118103620,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=224, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_PACODLIN"

  func oPACODLIN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODLIN_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODLIN_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oPACODLIN_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oPACODLIN_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_PACODLIN
     i_obj.ecpSave()
  endproc

  add object oDESMAS_1_23 as StdField with uid="XPHLNRZMZA",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESMAS", cQueryName = "DESMAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 36101834,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=262, Top=172, InputMask=replicate('X',40)

  add object oDESCCN_1_25 as StdField with uid="KZGQVEUTXV",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCCN", cQueryName = "DESCCN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 118546122,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=262, Top=198, InputMask=replicate('X',35)

  add object oDESLIN_1_26 as StdField with uid="XAIAAIABJP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESLIN", cQueryName = "DESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 111664842,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=262, Top=224, InputMask=replicate('X',30)

  add object oPAARTMAG_1_34 as StdField with uid="PHMQEZAUKW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PAARTMAG", cQueryName = "PAARTMAG",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio non a valore, obsoleto o non P.O.S.",;
    ToolTipText = "Servizio utilizzato per gestire le maggiorazioni sulla vendita",;
    HelpContextID = 116588995,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=125, Top=275, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_PAARTMAG"

  func oPAARTMAG_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAARTMAG_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAARTMAG_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oPAARTMAG_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Servizi",'GSPS_ASV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oPAARTMAG_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_PAARTMAG
     i_obj.ecpSave()
  endproc

  add object oARTDES_1_36 as StdField with uid="VKNSUEAKNW",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ARTDES", cQueryName = "ARTDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo x maggiorazioni",;
    HelpContextID = 32489978,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=280, Top=275, InputMask=replicate('X',40)

  add object oPAFLESSC_1_39 as StdCheck with uid="BQLJHLOVPS",rtseq=25,rtrep=.f.,left=125, top=302, caption="Esplicita sconti di riga",;
    ToolTipText = "Se attivo: gli sconti di riga vengono trattati come promozioni",;
    HelpContextID = 236408377,;
    cFormVar="w_PAFLESSC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLESSC_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLESSC_1_39.GetRadio()
    this.Parent.oContained.w_PAFLESSC = this.RadioValue()
    return .t.
  endfunc

  func oPAFLESSC_1_39.SetRadio()
    this.Parent.oContained.w_PAFLESSC=trim(this.Parent.oContained.w_PAFLESSC)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLESSC=='S',1,;
      0)
  endfunc

  add object oPADATESC_1_40 as StdField with uid="YONXZPBCQT",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PADATESC", cQueryName = "PADATESC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data attivazione check esplicita sconti",;
    HelpContextID = 16526905,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=497, Top=302

  func oPADATESC_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAFLESSC='S')
    endwith
   endif
  endfunc

  func oPADATESC_1_40.mHide()
    with this.Parent.oContained
      return (.w_PAFLESSC<>'S')
    endwith
  endfunc

  add object oPAFLFAFI_1_44 as StdCheck with uid="JKAEWDJSWQ",rtseq=28,rtrep=.f.,left=125, top=324, caption="No fattura Fidelity",;
    ToolTipText = "Attivo: l'importo utilizzato della Fidelity sulla vendita non sar� fatturato",;
    HelpContextID = 203902527,;
    cFormVar="w_PAFLFAFI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLFAFI_1_44.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLFAFI_1_44.GetRadio()
    this.Parent.oContained.w_PAFLFAFI = this.RadioValue()
    return .t.
  endfunc

  func oPAFLFAFI_1_44.SetRadio()
    this.Parent.oContained.w_PAFLFAFI=trim(this.Parent.oContained.w_PAFLFAFI)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLFAFI=='S',1,;
      0)
  endfunc

  add object oPASERFID_1_45 as StdField with uid="ZHPINXHMVC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PASERFID", cQueryName = "PASERFID",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio non a valore, obsoleto o non P.O.S.",;
    ToolTipText = "Servizio a valore per decrementare importo Fidelity in fattura",;
    HelpContextID = 31530554,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=125, Top=349, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_PASERFID"

  func oPASERFID_1_45.mHide()
    with this.Parent.oContained
      return (.w_PAFLFAFI<>'S')
    endwith
  endfunc

  func oPASERFID_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oPASERFID_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPASERFID_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oPASERFID_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Servizi",'GSPS_ASV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oPASERFID_1_45.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_PASERFID
     i_obj.ecpSave()
  endproc

  add object oSERDES_1_47 as StdField with uid="GTAXISELAM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SERDES", cQueryName = "SERDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione servizio per decremento Fidelity",;
    HelpContextID = 32501210,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=280, Top=349, InputMask=replicate('X',40)

  func oSERDES_1_47.mHide()
    with this.Parent.oContained
      return (.w_PAFLFAFI<>'S')
    endwith
  endfunc

  add object oPAFLPROM_1_51 as StdCheck with uid="VAPZALDGSP",rtseq=34,rtrep=.f.,left=319, top=324, caption="Considera promozioni per singolo articolo",;
    ToolTipText = "Nello scontrino le promozioni sono valutate sempre sui singoli articoli",;
    HelpContextID = 37269949,;
    cFormVar="w_PAFLPROM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLPROM_1_51.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPAFLPROM_1_51.GetRadio()
    this.Parent.oContained.w_PAFLPROM = this.RadioValue()
    return .t.
  endfunc

  func oPAFLPROM_1_51.SetRadio()
    this.Parent.oContained.w_PAFLPROM=trim(this.Parent.oContained.w_PAFLPROM)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLPROM=='S',1,;
      0)
  endfunc

  add object oStr_1_5 as StdString with uid="GXYPLQXSWV",Visible=.t., Left=13, Top=13,;
    Alignment=1, Width=110, Height=18,;
    Caption="Negozio:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="FADZELJGHA",Visible=.t., Left=286, Top=41,;
    Alignment=1, Width=131, Height=18,;
    Caption="Serie documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="QZFWFCESNE",Visible=.t., Left=13, Top=68,;
    Alignment=1, Width=110, Height=18,;
    Caption="Intest. etichette:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="BFLNHWFYPX",Visible=.t., Left=13, Top=119,;
    Alignment=1, Width=110, Height=18,;
    Caption="Struttura codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="BGLBCBPKZV",Visible=.t., Left=13, Top=173,;
    Alignment=1, Width=110, Height=18,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WDCOXCICMD",Visible=.t., Left=13, Top=201,;
    Alignment=1, Width=110, Height=18,;
    Caption="Categ.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KQCUBMHVKW",Visible=.t., Left=13, Top=225,;
    Alignment=1, Width=110, Height=18,;
    Caption="Codice lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="MWZMAGNJGN",Visible=.t., Left=17, Top=92,;
    Alignment=0, Width=119, Height=18,;
    Caption="Dati nominativo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="BQCEDVGNTM",Visible=.t., Left=13, Top=276,;
    Alignment=1, Width=110, Height=18,;
    Caption="Articolo per magg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="LLVXLMODHW",Visible=.t., Left=20, Top=249,;
    Alignment=0, Width=167, Height=18,;
    Caption="Vendita negozio"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="GWYQSZDDNM",Visible=.t., Left=304, Top=304,;
    Alignment=1, Width=191, Height=18,;
    Caption="Data attivazione esplicita sconti:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_PAFLESSC<>'S')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="XEDPOFGBHS",Visible=.t., Left=2, Top=351,;
    Alignment=1, Width=121, Height=18,;
    Caption="Servizio per Fidelity:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_PAFLFAFI<>'S')
    endwith
  endfunc

  add object oBox_1_29 as StdBox with uid="JTXJGABCFW",left=15, top=112, width=556,height=2

  add object oBox_1_37 as StdBox with uid="PKREPAHBTS",left=18, top=269, width=556,height=2
enddefine
define class tgsps_apaPag2 as StdContainer
  Width  = 582
  height = 388
  stdWidth  = 582
  stdheight = 388
  resizeXpos=446
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCOR_2_5 as StdField with uid="QSWIJOSZYS",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCOR", cQueryName = "DESCOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 38854346,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=168, InputMask=replicate('X',35)

  add object oPADOCRIC_2_6 as StdField with uid="WCJRHIQUPP",rtseq=40,rtrep=.f.,;
    cFormVar = "w_PADOCRIC", cQueryName = "PADOCRIC",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale per la generazione documenti di tipo ricevuta fiscale",;
    HelpContextID = 217722425,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=18, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDCATDOC", oKey_1_2="this.w_FILRF", oKey_2_1="TDTIPDOC", oKey_2_2="this.w_PADOCRIC"

  func oPADOCRIC_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPADOCRIC_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPADOCRIC_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILRF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILRF)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oPADOCRIC_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'',this.parent.oContained
  endproc
  proc oPADOCRIC_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDCATDOC=w_FILRF
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PADOCRIC
     i_obj.ecpSave()
  endproc

  add object oDESRIC_2_7 as StdField with uid="LNZIGWZGVY",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESRIC", cQueryName = "DESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 27385546,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=18, InputMask=replicate('X',35)

  add object oPADOCFAF_2_8 as StdField with uid="YCWOIXNUMY",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PADOCFAF", cQueryName = "PADOCFAF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale per la generazione documenti di tipo fattura fiscale (compresa nei corrispettivi)",;
    HelpContextID = 252039620,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=48, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILFA", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_PADOCFAF"

  func oPADOCFAF_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPADOCFAF_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPADOCFAF_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILFA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILFA)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oPADOCFAF_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oPADOCFAF_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILFA
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PADOCFAF
     i_obj.ecpSave()
  endproc

  add object oDESFAF_2_9 as StdField with uid="LCMHANXRVJ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESFAF", cQueryName = "DESFAF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 254664394,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=48, InputMask=replicate('X',35)

  add object oPADOCFAT_2_10 as StdField with uid="HFEGOHEWWH",rtseq=44,rtrep=.f.,;
    cFormVar = "w_PADOCFAT", cQueryName = "PADOCFAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale per la generazione documenti di tipo fattura immediata",;
    HelpContextID = 252039606,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=78, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILFA", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_PADOCFAT"

  func oPADOCFAT_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPADOCFAT_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPADOCFAT_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILFA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILFA)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oPADOCFAT_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'',this.parent.oContained
  endproc
  proc oPADOCFAT_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILFA
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PADOCFAT
     i_obj.ecpSave()
  endproc

  add object oDESFAT_2_11 as StdField with uid="FFRYKACSLY",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESFAT", cQueryName = "DESFAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 19783370,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=78, InputMask=replicate('X',35)

  add object oPADOCRIF_2_12 as StdField with uid="MFTDFPYQGD",rtseq=46,rtrep=.f.,;
    cFormVar = "w_PADOCRIF", cQueryName = "PADOCRIF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale per la generazione documenti di tipo ric.segue fattura (DDT)",;
    HelpContextID = 217722428,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILDT", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_PADOCRIF"

  func oPADOCRIF_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPADOCRIF_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPADOCRIF_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILDT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILDT)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oPADOCRIF_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'',this.parent.oContained
  endproc
  proc oPADOCRIF_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILDT
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PADOCRIF
     i_obj.ecpSave()
  endproc

  add object oDESRIF_2_13 as StdField with uid="UAEJDESXVP",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 245489354,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=108, InputMask=replicate('X',35)

  add object oPADOCDDT_2_14 as StdField with uid="PFRTTJBUZH",rtseq=48,rtrep=.f.,;
    cFormVar = "w_PADOCDDT", cQueryName = "PADOCDDT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale per la generazione documenti di tipo doc. di trasprto",;
    HelpContextID = 251276874,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=138, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILDT", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_PADOCDDT"

  func oPADOCDDT_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPADOCDDT_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPADOCDDT_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILDT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILDT)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oPADOCDDT_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'',this.parent.oContained
  endproc
  proc oPADOCDDT_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILDT
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PADOCDDT
     i_obj.ecpSave()
  endproc

  add object oDESDDT_2_15 as StdField with uid="UAGPOINZDM",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESDDT", cQueryName = "DESDDT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 16768714,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=138, InputMask=replicate('X',35)

  add object oPADOCCOR_2_16 as StdField with uid="HGNWZHHSOH",rtseq=50,rtrep=.f.,;
    cFormVar = "w_PADOCCOR", cQueryName = "PADOCCOR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale per documenti corrispettivi (chiusure: nessuna stampa, emiss. scontrino, brogliaccio)",;
    HelpContextID = 33935800,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=168, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDCATDOC", oKey_1_2="this.w_FILRF", oKey_2_1="TDTIPDOC", oKey_2_2="this.w_PADOCCOR"

  func oPADOCCOR_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPADOCCOR_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPADOCCOR_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILRF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILRF)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oPADOCCOR_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'',this.parent.oContained
  endproc
  proc oPADOCCOR_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDCATDOC=w_FILRF
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PADOCCOR
     i_obj.ecpSave()
  endproc

  add object oPAFLCONT_2_17 as StdCheck with uid="QNLKVMEROR",rtseq=51,rtrep=.f.,left=160, top=194, caption="Singolo corrispettivo",;
    ToolTipText = "Se attivo esegue generazione per singolo corrispettivo delle vendite intestate",;
    HelpContextID = 101233078,;
    cFormVar="w_PAFLCONT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAFLCONT_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLCONT_2_17.GetRadio()
    this.Parent.oContained.w_PAFLCONT = this.RadioValue()
    return .t.
  endfunc

  func oPAFLCONT_2_17.SetRadio()
    this.Parent.oContained.w_PAFLCONT=trim(this.Parent.oContained.w_PAFLCONT)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLCONT=='S',1,;
      0)
  endfunc

  add object oPACAURES_2_42 as StdField with uid="KMCSISEMYY",rtseq=70,rtrep=.f.,;
    cFormVar = "w_PACAURES", cQueryName = "PACAURES",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale magazzino resi inesistente o incongruente",;
    ToolTipText = "Causale di magazzino utilizzata per movimentare i resi",;
    HelpContextID = 235675209,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=244, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PACAURES"

  func oPACAURES_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACAURES_2_42.ecpDrop(oSource)
    this.Parent.oContained.link_2_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACAURES_2_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPACAURES_2_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oPACAURES_2_42.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PACAURES
     i_obj.ecpSave()
  endproc

  add object oDESRES_2_46 as StdField with uid="JDHTMXHMBX",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DESRES", cQueryName = "DESRES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 31579850,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=244, InputMask=replicate('X',35)

  add object oPACARKIT_2_48 as StdField with uid="MWDHGDKEDA",rtseq=73,rtrep=.f.,;
    cFormVar = "w_PACARKIT", cQueryName = "PACARKIT",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale magazzino carichi kit inesistente o incongruente",;
    ToolTipText = "Causale di magazzino utilizzata per i movimenti di carico articoli kit",;
    HelpContextID = 115088970,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=274, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PACARKIT"

  func oPACARKIT_2_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACARKIT_2_48.ecpDrop(oSource)
    this.Parent.oContained.link_2_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACARKIT_2_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPACARKIT_2_48'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oPACARKIT_2_48.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PACARKIT
     i_obj.ecpSave()
  endproc

  add object oPASCAKIT_2_49 as StdField with uid="ALJBJRGKXQ",rtseq=74,rtrep=.f.,;
    cFormVar = "w_PASCAKIT", cQueryName = "PASCAKIT",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale magazzino scarichi componenti kit inesistente o incongruente",;
    ToolTipText = "Causale di magazzino utilizzata per i movimenti di scarico componenti kit",;
    HelpContextID = 97459786,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=160, Top=304, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PASCAKIT"

  func oPASCAKIT_2_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oPASCAKIT_2_49.ecpDrop(oSource)
    this.Parent.oContained.link_2_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPASCAKIT_2_49.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPASCAKIT_2_49'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oPASCAKIT_2_49.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PASCAKIT
     i_obj.ecpSave()
  endproc

  add object oDESKCA_2_50 as StdField with uid="FSZAUIEAGA",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESKCA", cQueryName = "DESKCA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 67690186,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=274, InputMask=replicate('X',35)

  add object oDESKSC_2_52 as StdField with uid="BULOUAZENZ",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESKSC", cQueryName = "DESKSC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 17358538,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=229, Top=304, InputMask=replicate('X',35)

  add object oPACODCEN_2_59 as StdField with uid="NYIQZQLGSM",rtseq=82,rtrep=.f.,;
    cFormVar = "w_PACODCEN", cQueryName = "PACODCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo obsoleto",;
    ToolTipText = "Centro di costo/ricavo utilizzato nella generazione documenti",;
    HelpContextID = 235544132,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=160, Top=361, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PACODCEN"

  func oPACODCEN_2_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oPACODCEN_2_59.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oPACODCEN_2_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_59('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODCEN_2_59.ecpDrop(oSource)
    this.Parent.oContained.link_2_59('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODCEN_2_59.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPACODCEN_2_59'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di ricavo",'',this.parent.oContained
  endproc
  proc oPACODCEN_2_59.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_PACODCEN
     i_obj.ecpSave()
  endproc

  add object oDESPIA_2_61 as StdField with uid="MCWRNJWYMN",rtseq=84,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61071050,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=280, Top=361, InputMask=replicate('X',40)

  func oDESPIA_2_61.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc


  add object oObj_2_66 as cp_runprogram with uid="VHTQAEYMAL",left=28, top=411, width=168,height=24,;
    caption='GSPS_BMM',;
   bGlobalFont=.t.,;
    prg="GSPS_BMM(w_PADOCCOR, w_PADOCRIC, w_PADOCFAF, w_PADOCFAT, w_PADOCRIF, w_PADOCDDT)",;
    cEvent = "ChkFinali",;
    nPag=2;
    , HelpContextID = 21037133

  add object oStr_2_18 as StdString with uid="ITOYUYNQMG",Visible=.t., Left=19, Top=48,;
    Alignment=1, Width=138, Height=18,;
    Caption="Fattura fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="ZNNAMOSESY",Visible=.t., Left=19, Top=138,;
    Alignment=1, Width=138, Height=18,;
    Caption="Doc. di trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="BOKISVVCPW",Visible=.t., Left=3, Top=108,;
    Alignment=1, Width=154, Height=18,;
    Caption="Ricevuta segue fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="JBDAPAISJU",Visible=.t., Left=19, Top=168,;
    Alignment=1, Width=138, Height=18,;
    Caption="Corrispettivo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="PALDRIRNQW",Visible=.t., Left=19, Top=18,;
    Alignment=1, Width=138, Height=18,;
    Caption="Ricevuta fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="ZTBQAHDSOX",Visible=.t., Left=19, Top=78,;
    Alignment=1, Width=138, Height=18,;
    Caption="Fattura immediata:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="ZLWARMTDBT",Visible=.t., Left=19, Top=244,;
    Alignment=1, Width=138, Height=18,;
    Caption="Causale per resi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="JDMIGWQYBX",Visible=.t., Left=19, Top=274,;
    Alignment=1, Width=138, Height=18,;
    Caption="Carichi da kit:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="EKHSHKCHSX",Visible=.t., Left=4, Top=304,;
    Alignment=1, Width=153, Height=18,;
    Caption="Scarico componenti kit:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="HVEQCHVJOU",Visible=.t., Left=5, Top=213,;
    Alignment=0, Width=143, Height=18,;
    Caption="Causali magazzino"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_62 as StdString with uid="VMLUJYAJYC",Visible=.t., Left=20, Top=361,;
    Alignment=1, Width=137, Height=18,;
    Caption="Centro di costo/ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_64 as StdString with uid="IJHGFBVTFP",Visible=.t., Left=5, Top=333,;
    Alignment=0, Width=119, Height=18,;
    Caption="Analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_64.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oBox_2_54 as StdBox with uid="UXPUIOYEUR",left=3, top=233, width=554,height=2

  add object oBox_2_63 as StdBox with uid="WBCFHCWWNX",left=5, top=353, width=556,height=2
enddefine
define class tgsps_apaPag3 as StdContainer
  Width  = 582
  height = 388
  stdWidth  = 582
  stdheight = 388
  resizeXpos=446
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACONPRE_3_1 as StdField with uid="RITVCHXJEU",rtseq=86,rtrep=.f.,;
    cFormVar = "w_PACONPRE", cQueryName = "PACONPRE",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata nella contabilizzazione del documento generato contenente questo tipo di pagamento",;
    HelpContextID = 72737221,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=148, Top=35, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PACONPRE"

  func oPACONPRE_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACONPRE_3_1.ecpDrop(oSource)
    this.Parent.oContained.link_3_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACONPRE_3_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPACONPRE_3_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oPACONPRE_3_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PACONPRE
     i_obj.ecpSave()
  endproc

  add object oDESCON_3_2 as StdField with uid="KBYQEKCVTZ",rtseq=87,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 105963210,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=277, Top=65, InputMask=replicate('X',40)

  add object oDESPRE_3_3 as StdField with uid="MXMCUQAUMF",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESPRE", cQueryName = "DESPRE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 252960458,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=277, Top=35, InputMask=replicate('X',40)

  add object oDESASS_3_4 as StdField with uid="IGHNRFYXPG",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DESASS", cQueryName = "DESASS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 18013898,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=277, Top=95, InputMask=replicate('X',40)

  add object oDESCAR_3_5 as StdField with uid="KDTIFYAJSB",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DESCAR", cQueryName = "DESCAR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 53534410,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=277, Top=125, InputMask=replicate('X',40)

  add object oDESFIN_3_6 as StdField with uid="YYDQZBWVGX",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 112058058,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=277, Top=155, InputMask=replicate('X',40)

  add object oPACONCON_3_14 as StdField with uid="GWHBREPBFT",rtseq=94,rtrep=.f.,;
    cFormVar = "w_PACONCON", cQueryName = "PACONCON",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata nella contabilizzazione del documento generato contenente questo tipo di pagamento",;
    HelpContextID = 22405564,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=148, Top=65, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PACONCON"

  func oPACONCON_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACONCON_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACONCON_3_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPACONCON_3_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oPACONCON_3_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PACONCON
     i_obj.ecpSave()
  endproc

  add object oPACONASS_3_15 as StdField with uid="EMWZFJIJVO",rtseq=95,rtrep=.f.,;
    cFormVar = "w_PACONASS", cQueryName = "PACONASS",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata nella contabilizzazione del documento generato contenente questo tipo di pagamento",;
    HelpContextID = 55959991,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=148, Top=95, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PACONASS"

  func oPACONASS_3_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACONASS_3_15.ecpDrop(oSource)
    this.Parent.oContained.link_3_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACONASS_3_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPACONASS_3_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oPACONASS_3_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PACONASS
     i_obj.ecpSave()
  endproc

  add object oPACONCAR_3_16 as StdField with uid="RMUOVSHIAM",rtseq=96,rtrep=.f.,;
    cFormVar = "w_PACONCAR", cQueryName = "PACONCAR",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata nella contabilizzazione del documento generato contenente questo tipo di pagamento",;
    HelpContextID = 22405560,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=148, Top=125, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PACONCAR"

  func oPACONCAR_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACONCAR_3_16.ecpDrop(oSource)
    this.Parent.oContained.link_3_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACONCAR_3_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPACONCAR_3_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oPACONCAR_3_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PACONCAR
     i_obj.ecpSave()
  endproc

  add object oPACONFIN_3_17 as StdField with uid="SPAOPWHVGT",rtseq=97,rtrep=.f.,;
    cFormVar = "w_PACONFIN", cQueryName = "PACONFIN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata nella contabilizzazione del documento generato contenente questo tipo di pagamento",;
    HelpContextID = 27926084,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=148, Top=155, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PACONFIN"

  func oPACONFIN_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACONFIN_3_17.ecpDrop(oSource)
    this.Parent.oContained.link_3_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACONFIN_3_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPACONFIN_3_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oPACONFIN_3_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PACONFIN
     i_obj.ecpSave()
  endproc

  add object oStr_3_7 as StdString with uid="VYNPJUJXFH",Visible=.t., Left=2, Top=35,;
    Alignment=1, Width=144, Height=18,;
    Caption="Prepagato/Fidelity Card:"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="SWRGXFFJVR",Visible=.t., Left=41, Top=66,;
    Alignment=1, Width=105, Height=18,;
    Caption="Contanti:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="ROOJKAHPSP",Visible=.t., Left=41, Top=97,;
    Alignment=1, Width=105, Height=18,;
    Caption="Assegni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="JJJOXQVNEC",Visible=.t., Left=41, Top=128,;
    Alignment=1, Width=105, Height=18,;
    Caption="Carta/bancomat:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="FRHEYSHLNG",Visible=.t., Left=41, Top=159,;
    Alignment=1, Width=105, Height=18,;
    Caption="Finanziamento:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsps_apaPag4 as StdContainer
  Width  = 582
  height = 388
  stdWidth  = 582
  stdheight = 388
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPA3KCORR_4_2 as StdField with uid="HGBQUVCXGA",rtseq=103,rtrep=.f.,;
    cFormVar = "w_PA3KCORR", cQueryName = "PA3KCORR",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale per documenti corrispettivi (chiusure: nessuna stampa, emiss. scontrino, brogliaccio)",;
    HelpContextID = 101376440,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=202, Top=29, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILRF", oKey_3_1="TDFLINTE", oKey_3_2="this.w_FILC", oKey_4_1="TDTIPDOC", oKey_4_2="this.w_PA3KCORR"

  func oPA3KCORR_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPA3KCORR_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPA3KCORR_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILRF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLINTE="+cp_ToStrODBC(this.Parent.oContained.w_FILC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILRF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLINTE="+cp_ToStr(this.Parent.oContained.w_FILC)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDFLINTE,TDTIPDOC',cp_AbsName(this.parent,'oPA3KCORR_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'gsps_apa.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPA3KCORR_4_2.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILRF
    i_obj.TDFLINTE=w_FILC
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PA3KCORR
     i_obj.ecpSave()
  endproc

  add object oDES3KC_4_3 as StdField with uid="LZFQXLSSSW",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DES3KC", cQueryName = "DES3KC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 27320010,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=271, Top=29, InputMask=replicate('X',35)

  add object oPA3KRICF_4_4 as StdField with uid="XGAODYYTLJ",rtseq=105,rtrep=.f.,;
    cFormVar = "w_PA3KRICF", cQueryName = "PA3KRICF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale per documenti corrispettivi (chiusure: ricavuta fiscale)",;
    HelpContextID = 82124348,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=202, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILRF", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_PA3KRICF"

  func oPA3KRICF_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPA3KRICF_4_4.ecpDrop(oSource)
    this.Parent.oContained.link_4_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPA3KRICF_4_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILRF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILRF)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oPA3KRICF_4_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'gsps_apa.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPA3KRICF_4_4.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILRF
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PA3KRICF
     i_obj.ecpSave()
  endproc


  add object oPA3KIMPO_4_5 as StdCombo with uid="THMDEVZWEJ",rtseq=106,rtrep=.f.,left=202,top=99,width=148,height=21;
    , ToolTipText = "Controlla che in caso di superamento dei 3.000 euro sia presente l'intestatario";
    , HelpContextID = 128639419;
    , cFormVar="w_PA3KIMPO",RowSource=""+"Nessun controllo,"+"Solo warning,"+"Bloccante", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPA3KIMPO_4_5.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'W',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oPA3KIMPO_4_5.GetRadio()
    this.Parent.oContained.w_PA3KIMPO = this.RadioValue()
    return .t.
  endfunc

  func oPA3KIMPO_4_5.SetRadio()
    this.Parent.oContained.w_PA3KIMPO=trim(this.Parent.oContained.w_PA3KIMPO)
    this.value = ;
      iif(this.Parent.oContained.w_PA3KIMPO=='N',1,;
      iif(this.Parent.oContained.w_PA3KIMPO=='W',2,;
      iif(this.Parent.oContained.w_PA3KIMPO=='B',3,;
      0)))
  endfunc

  add object oDES3KR_4_6 as StdField with uid="WLRULCCAYE",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DES3KR", cQueryName = "DES3KR",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 44097226,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=271, Top=63, InputMask=replicate('X',35)


  add object oPA3KDFIS_4_13 as StdCombo with uid="YOXOCZVHLM",rtseq=111,rtrep=.f.,left=202,top=131,width=148,height=21;
    , ToolTipText = "Controlla la presenza del codice fiscale o della partita iva nell'intestatario";
    , HelpContextID = 17112649;
    , cFormVar="w_PA3KDFIS",RowSource=""+"Nessun controllo,"+"Solo warning,"+"Bloccante", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPA3KDFIS_4_13.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'W',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oPA3KDFIS_4_13.GetRadio()
    this.Parent.oContained.w_PA3KDFIS = this.RadioValue()
    return .t.
  endfunc

  func oPA3KDFIS_4_13.SetRadio()
    this.Parent.oContained.w_PA3KDFIS=trim(this.Parent.oContained.w_PA3KDFIS)
    this.value = ;
      iif(this.Parent.oContained.w_PA3KDFIS=='N',1,;
      iif(this.Parent.oContained.w_PA3KDFIS=='W',2,;
      iif(this.Parent.oContained.w_PA3KDFIS=='B',3,;
      0)))
  endfunc

  func oPA3KDFIS_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PA3KIMPO<>"N")
    endwith
   endif
  endfunc

  add object oPAACCPRE_4_21 as StdCheck with uid="OHYJOYFKTX",rtseq=115,rtrep=.f.,left=202, top=184, caption="Acconto precedente",;
    ToolTipText = "Se attivo: considera l'acconto precedente nel totale da confrontare con l'importo fissato",;
    HelpContextID = 85066181,;
    cFormVar="w_PAACCPRE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPAACCPRE_4_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAACCPRE_4_21.GetRadio()
    this.Parent.oContained.w_PAACCPRE = this.RadioValue()
    return .t.
  endfunc

  func oPAACCPRE_4_21.SetRadio()
    this.Parent.oContained.w_PAACCPRE=trim(this.Parent.oContained.w_PAACCPRE)
    this.value = ;
      iif(this.Parent.oContained.w_PAACCPRE=='S',1,;
      0)
  endfunc

  func oPAACCPRE_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PA3KIMPO<>"N")
    endwith
   endif
  endfunc

  add object oPAPAGCON_4_22 as StdCheck with uid="SXVICCUWYE",rtseq=116,rtrep=.f.,left=202, top=210, caption="Pagamento contanti",;
    ToolTipText = "Se attivo: considera il pagamento contanti nel totale da confrontare con l'importo fissato",;
    HelpContextID = 30609852,;
    cFormVar="w_PAPAGCON", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPAPAGCON_4_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAPAGCON_4_22.GetRadio()
    this.Parent.oContained.w_PAPAGCON = this.RadioValue()
    return .t.
  endfunc

  func oPAPAGCON_4_22.SetRadio()
    this.Parent.oContained.w_PAPAGCON=trim(this.Parent.oContained.w_PAPAGCON)
    this.value = ;
      iif(this.Parent.oContained.w_PAPAGCON=='S',1,;
      0)
  endfunc

  func oPAPAGCON_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PA3KIMPO<>"N")
    endwith
   endif
  endfunc

  add object oPAPAGASS_4_23 as StdCheck with uid="DXQWFYKVQG",rtseq=117,rtrep=.f.,left=202, top=236, caption="Pagamento C/assegno",;
    ToolTipText = "Se attivo: considera il pagamento c/assegno nel totale da confrontare con l'importo fissato",;
    HelpContextID = 64164279,;
    cFormVar="w_PAPAGASS", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPAPAGASS_4_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAPAGASS_4_23.GetRadio()
    this.Parent.oContained.w_PAPAGASS = this.RadioValue()
    return .t.
  endfunc

  func oPAPAGASS_4_23.SetRadio()
    this.Parent.oContained.w_PAPAGASS=trim(this.Parent.oContained.w_PAPAGASS)
    this.value = ;
      iif(this.Parent.oContained.w_PAPAGASS=='S',1,;
      0)
  endfunc

  func oPAPAGASS_4_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PA3KIMPO<>"N")
    endwith
   endif
  endfunc

  add object oPAPAGCAR_4_24 as StdCheck with uid="KFIBFGBBEI",rtseq=118,rtrep=.f.,left=202, top=262, caption="C/credito/bancomat",;
    ToolTipText = "Se attivo: considera il pagamento C/credito/bancomat nel totale da confrontare con l'importo fissato",;
    HelpContextID = 30609848,;
    cFormVar="w_PAPAGCAR", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPAPAGCAR_4_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAPAGCAR_4_24.GetRadio()
    this.Parent.oContained.w_PAPAGCAR = this.RadioValue()
    return .t.
  endfunc

  func oPAPAGCAR_4_24.SetRadio()
    this.Parent.oContained.w_PAPAGCAR=trim(this.Parent.oContained.w_PAPAGCAR)
    this.value = ;
      iif(this.Parent.oContained.w_PAPAGCAR=='S',1,;
      0)
  endfunc

  func oPAPAGCAR_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PA3KIMPO<>"N")
    endwith
   endif
  endfunc

  add object oPAPAGFIN_4_25 as StdCheck with uid="OWZPDOTSKK",rtseq=119,rtrep=.f.,left=202, top=288, caption="Finanziamento",;
    ToolTipText = "Se attivo: considera il Finanziamento nel totale da confrontare con l'importo fissato",;
    HelpContextID = 19721796,;
    cFormVar="w_PAPAGFIN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPAPAGFIN_4_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAPAGFIN_4_25.GetRadio()
    this.Parent.oContained.w_PAPAGFIN = this.RadioValue()
    return .t.
  endfunc

  func oPAPAGFIN_4_25.SetRadio()
    this.Parent.oContained.w_PAPAGFIN=trim(this.Parent.oContained.w_PAPAGFIN)
    this.value = ;
      iif(this.Parent.oContained.w_PAPAGFIN=='S',1,;
      0)
  endfunc

  func oPAPAGFIN_4_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PA3KIMPO<>"N")
    endwith
   endif
  endfunc

  add object oPAPAGCLI_4_26 as StdCheck with uid="MECCATRQFZ",rtseq=120,rtrep=.f.,left=202, top=314, caption="Pagamento C/cliente",;
    ToolTipText = "Se attivo: considera il pagamento C/cliente nel totale da confrontare con l'importo fissato",;
    HelpContextID = 30609857,;
    cFormVar="w_PAPAGCLI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPAPAGCLI_4_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAPAGCLI_4_26.GetRadio()
    this.Parent.oContained.w_PAPAGCLI = this.RadioValue()
    return .t.
  endfunc

  func oPAPAGCLI_4_26.SetRadio()
    this.Parent.oContained.w_PAPAGCLI=trim(this.Parent.oContained.w_PAPAGCLI)
    this.value = ;
      iif(this.Parent.oContained.w_PAPAGCLI=='S',1,;
      0)
  endfunc

  func oPAPAGCLI_4_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PA3KIMPO<>"N")
    endwith
   endif
  endfunc

  add object oStr_4_7 as StdString with uid="JMNZPMYRGC",Visible=.t., Left=10, Top=29,;
    Alignment=1, Width=187, Height=18,;
    Caption="Causale intestata corrispettivi:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="DFECKBYYYP",Visible=.t., Left=10, Top=63,;
    Alignment=1, Width=187, Height=18,;
    Caption="Causale intestata ricevuta:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="QFVIJYKKAY",Visible=.t., Left=10, Top=99,;
    Alignment=1, Width=187, Height=18,;
    Caption="Controllo importo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="QKBNUPEKFL",Visible=.t., Left=10, Top=132,;
    Alignment=1, Width=187, Height=18,;
    Caption="Controllo dati intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="MQXZASEUMK",Visible=.t., Left=85, Top=165,;
    Alignment=0, Width=307, Height=18,;
    Caption="Pagamenti abilitati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_4_19 as StdBox with uid="ZRSQZRVNIG",left=84, top=180, width=356,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_apa','PAR_VDET','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODAZI=PAR_VDET.PACODAZI";
  +" and "+i_cAliasName2+".PACODNEG=PAR_VDET.PACODNEG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
