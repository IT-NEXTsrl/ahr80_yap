* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bmm                                                        *
*              Controllo causali documento                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-21                                                      *
* Last revis.: 2005-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pDocCor,pDocRic,pDocFaf,pDocFat,pDocRif,pDocDdt
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bmm",oParentObject,m.pDocCor,m.pDocRic,m.pDocFaf,m.pDocFat,m.pDocRif,m.pDocDdt)
return(i_retval)

define class tgsps_bmm as StdBatch
  * --- Local variables
  pDocCor = space(5)
  pDocRic = space(5)
  pDocFaf = space(5)
  pDocFat = space(5)
  pDocRif = space(5)
  pDocDdt = space(5)
  w_CAUDOC = space(5)
  w_CAUMAG = space(5)
  w_FLCASC = space(1)
  w_FLRISE = space(1)
  w_FLIMPE = space(1)
  w_FLORDI = space(1)
  w_MESS = space(1)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  CAM_AGAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per controllo causali Documenti da GSPS_APA e GSPS_AMV
    this.w_MESS = ""
    this.oParentObject.w_RESCHK = 0
    if Not Empty(this.pDocCor)
      this.w_CAUDOC = this.pDocCor
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty(this.pDocRic) And this.oParentObject.w_RESCHK = 0
      this.w_CAUDOC = this.pDocRic
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty(this.pDocFat) And this.oParentObject.w_RESCHK = 0
      this.w_CAUDOC = this.pDocFat
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty(this.pDocFaf) And this.oParentObject.w_RESCHK = 0
      this.w_CAUDOC = this.pDocFaf
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty(this.pDocRif) And this.oParentObject.w_RESCHK = 0
      this.w_CAUDOC = this.pDocRif
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not Empty(this.pDocDdt) And this.oParentObject.w_RESCHK = 0
      this.w_CAUDOC = this.pDocDdt
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_RESCHK=-1
      ah_ErrorMsg(this.w_MESS,,"")
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo causale di magazzino
    this.w_FLCASC = " "
    this.w_FLRISE = " "
    this.w_FLIMPE = " "
    this.w_FLORDI = " "
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDCAUMAG"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDCAUMAG;
        from (i_cTable) where;
            TDTIPDOC = this.w_CAUDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
        from (i_cTable) where;
            CMCODICE = this.w_CAUMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- La fattura Fiscale non deve movimentare il magazzino
    if this.w_CAUDOC = this.pDocFaf And Not Empty(Alltrim(this.w_FLCASC)+Alltrim(this.w_FLRISE)+Alltrim(this.w_FLIMPE)+Alltrim(this.w_FLORDI))
      this.w_MESS = "La causale per fatture fiscali non deve aggiornare il magazzino"
      this.oParentObject.w_RESCHK = -1
    else
      * --- Le altre causali documenti devono aggiornare solo l'Esistenza
      if this.w_CAUDOC <> this.pDocFaf And (this.w_FLCASC<>"-" Or Not Empty(Alltrim(this.w_FLRISE)+Alltrim(this.w_FLIMPE)+Alltrim(this.w_FLORDI)) )
        this.w_MESS = "Le causali documenti devono solo diminuire l'esistenza di magazzino"
        this.oParentObject.w_RESCHK = -1
      endif
    endif
  endproc


  proc Init(oParentObject,pDocCor,pDocRic,pDocFaf,pDocFat,pDocRif,pDocDdt)
    this.pDocCor=pDocCor
    this.pDocRic=pDocRic
    this.pDocFaf=pDocFaf
    this.pDocFat=pDocFat
    this.pDocRif=pDocRif
    this.pDocDdt=pDocDdt
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CAM_AGAZ'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pDocCor,pDocRic,pDocFaf,pDocFat,pDocRif,pDocDdt"
endproc
