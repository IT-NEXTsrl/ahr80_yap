* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_sap                                                        *
*              Analisi promozioni                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_525]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_sap",oParentObject))

* --- Class definition
define class tgsps_sap as StdForm
  Top    = 7
  Left   = 35

  * --- Standard Properties
  Width  = 590
  Height = 276
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=144091031
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  ART_ICOL_IDX = 0
  LISTINI_IDX = 0
  CONTI_IDX = 0
  PAG_AMEN_IDX = 0
  VALUTE_IDX = 0
  INVENTAR_IDX = 0
  GRUMERC_IDX = 0
  REP_ARTI_IDX = 0
  MAGAZZIN_IDX = 0
  BUSIUNIT_IDX = 0
  CLI_VEND_IDX = 0
  OPE_RATO_IDX = 0
  cPrg = "gsps_sap"
  cComment = "Analisi promozioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_CODNEG = space(3)
  w_DADATA = ctod('  /  /  ')
  w_ADATA = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_ORAFIN = space(2)
  o_ORAFIN = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATSTA = ctod('  /  /  ')
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_TIPO = space(10)
  w_CLIENTE = space(15)
  w_CODMAG = space(5)
  w_DECTOT = 0
  w_TIPOLN = space(1)
  w_ONUME = 0
  w_TIPOCON = space(1)
  w_VALESE = space(3)
  w_CAOESE = 0
  w_DESNEG = space(40)
  w_ONUME = 0
  w_DESCLI = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST1 = ctod('  /  /  ')
  w_DESMAG = space(30)
  w_GIOLUN = 0
  w_GIOMAR = 0
  w_GIOMER = 0
  w_GIOGIO = 0
  w_GIOVEN = 0
  w_GIOSAB = 0
  w_GIODOM = 0
  w_OMINI = space(4)
  w_OMFIN = space(4)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_sapPag1","gsps_sap",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODNEG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='PAG_AMEN'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='INVENTAR'
    this.cWorkTables[8]='GRUMERC'
    this.cWorkTables[9]='REP_ARTI'
    this.cWorkTables[10]='MAGAZZIN'
    this.cWorkTables[11]='BUSIUNIT'
    this.cWorkTables[12]='CLI_VEND'
    this.cWorkTables[13]='OPE_RATO'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_CODNEG=space(3)
      .w_DADATA=ctod("  /  /  ")
      .w_ADATA=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_ORAFIN=space(2)
      .w_MININI=space(2)
      .w_DATSTA=ctod("  /  /  ")
      .w_MINFIN=space(2)
      .w_TIPO=space(10)
      .w_CLIENTE=space(15)
      .w_CODMAG=space(5)
      .w_DECTOT=0
      .w_TIPOLN=space(1)
      .w_ONUME=0
      .w_TIPOCON=space(1)
      .w_VALESE=space(3)
      .w_CAOESE=0
      .w_DESNEG=space(40)
      .w_ONUME=0
      .w_DESCLI=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST1=ctod("  /  /  ")
      .w_DESMAG=space(30)
      .w_GIOLUN=0
      .w_GIOMAR=0
      .w_GIOMER=0
      .w_GIOGIO=0
      .w_GIOVEN=0
      .w_GIOSAB=0
      .w_GIODOM=0
      .w_OMINI=space(4)
      .w_OMFIN=space(4)
        .w_AZIENDA = i_CODAZI
        .w_CODNEG = g_CODNEG
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODNEG))
          .link_1_2('Full')
        endif
        .w_DADATA = i_datsys
        .w_ADATA = i_datsys+30
        .w_ORAINI = IIF(EMPTY(.w_ORAINI),'  ',RIGHT('00'+.w_ORAINI,2))
        .w_ORAFIN = IIF(EMPTY(.w_ORAFIN),'  ',RIGHT('00'+.w_ORAFIN,2))
        .w_MININI = IIF(EMPTY(.w_ORAINI) OR EMPTY(.w_MININI),'00',RIGHT('00'+.w_MININI,2))
        .w_DATSTA = i_DATSYS
        .w_MINFIN = IIF(EMPTY(.w_ORAFIN) OR EMPTY(.w_MINFIN),'59',RIGHT('00'+.w_MINFIN,2))
        .w_TIPO = 'C'
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CLIENTE))
          .link_1_13('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODMAG))
          .link_1_14('Full')
        endif
          .DoRTCalc(13,15,.f.)
        .w_TIPOCON = 'C'
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_VALESE))
          .link_1_21('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
          .DoRTCalc(18,21,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(23,23,.f.)
        .w_OBTEST1 = i_DATSYS
          .DoRTCalc(25,25,.f.)
        .w_GIOLUN = 2
        .w_GIOMAR = 3
        .w_GIOMER = 4
        .w_GIOGIO = 5
        .w_GIOVEN = 6
        .w_GIOSAB = 7
        .w_GIODOM = 1
        .w_OMINI = ALLTRIM(.w_ORAINI)+ALLTRIM(.w_MININI)
        .w_OMFIN = ALLTRIM(.w_ORAFIN)+ALLTRIM(.w_MINFIN)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_ORAINI<>.w_ORAINI
            .w_ORAINI = IIF(EMPTY(.w_ORAINI),'  ',RIGHT('00'+.w_ORAINI,2))
        endif
        if .o_ORAFIN<>.w_ORAFIN
            .w_ORAFIN = IIF(EMPTY(.w_ORAFIN),'  ',RIGHT('00'+.w_ORAFIN,2))
        endif
        if .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
            .w_MININI = IIF(EMPTY(.w_ORAINI) OR EMPTY(.w_MININI),'00',RIGHT('00'+.w_MININI,2))
        endif
        .DoRTCalc(8,8,.t.)
        if .o_ORAINI<>.w_ORAINI.or. .o_MINFIN<>.w_MINFIN
            .w_MINFIN = IIF(EMPTY(.w_ORAFIN) OR EMPTY(.w_MINFIN),'59',RIGHT('00'+.w_MINFIN,2))
        endif
        .DoRTCalc(10,16,.t.)
          .link_1_21('Full')
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .DoRTCalc(18,32,.t.)
            .w_OMINI = ALLTRIM(.w_ORAINI)+ALLTRIM(.w_MININI)
            .w_OMFIN = ALLTRIM(.w_ORAFIN)+ALLTRIM(.w_MINFIN)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMININI_1_7.enabled = this.oPgFrm.Page1.oPag.oMININI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_10.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODNEG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_KAN',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODNEG)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_AZIENDA;
                     ,'BUCODICE',trim(this.w_CODNEG))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNEG)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODNEG) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODNEG_1_2'),i_cWhere,'GSPS_KAN',"Negozi azienda",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODNEG);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_AZIENDA;
                       ,'BUCODICE',this.w_CODNEG)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNEG = NVL(_Link_.BUCODICE,space(3))
      this.w_DESNEG = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODNEG = space(3)
      endif
      this.w_DESNEG = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIENTE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACV',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_CLIENTE))
          select CLCODCLI,CLDESCRI,CLDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIENTE)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStr(trim(this.w_CLIENTE)+"%");

            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIENTE) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oCLIENTE_1_13'),i_cWhere,'GSPS_ACV',"Clienti negozi",'CLIPOS.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_CLIENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_CLIENTE)
            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIENTE = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESCLI = NVL(_Link_.CLDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CLDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIENTE = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
        endif
        this.w_CLIENTE = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_14'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   +" and VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODNEG_1_2.value==this.w_CODNEG)
      this.oPgFrm.Page1.oPag.oCODNEG_1_2.value=this.w_CODNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_3.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_3.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oADATA_1_4.value==this.w_ADATA)
      this.oPgFrm.Page1.oPag.oADATA_1_4.value=this.w_ADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_5.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_5.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_6.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_6.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_7.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_7.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_8.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_8.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_10.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_10.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIENTE_1_13.value==this.w_CLIENTE)
      this.oPgFrm.Page1.oPag.oCLIENTE_1_13.value=this.w_CLIENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_14.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_14.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNEG_1_26.value==this.w_DESNEG)
      this.oPgFrm.Page1.oPag.oDESNEG_1_26.value=this.w_DESNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_29.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_29.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_34.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_34.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOLUN_1_36.RadioValue()==this.w_GIOLUN)
      this.oPgFrm.Page1.oPag.oGIOLUN_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOMAR_1_37.RadioValue()==this.w_GIOMAR)
      this.oPgFrm.Page1.oPag.oGIOMAR_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOMER_1_38.RadioValue()==this.w_GIOMER)
      this.oPgFrm.Page1.oPag.oGIOMER_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOGIO_1_39.RadioValue()==this.w_GIOGIO)
      this.oPgFrm.Page1.oPag.oGIOGIO_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOVEN_1_40.RadioValue()==this.w_GIOVEN)
      this.oPgFrm.Page1.oPag.oGIOVEN_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOSAB_1_41.RadioValue()==this.w_GIOSAB)
      this.oPgFrm.Page1.oPag.oGIOSAB_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIODOM_1_42.RadioValue()==this.w_GIODOM)
      this.oPgFrm.Page1.oPag.oGIODOM_1_42.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_adata)) OR (.w_adata>=.w_dadata))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATA_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   ((empty(.w_ADATA)) or not((.w_ADATA>=.w_DADATA) OR  ( empty(.w_ADATA))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oADATA_1_4.SetFocus()
            i_bnoObbl = !empty(.w_ADATA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not(val(.w_ORAINI)<24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora inizio selezione non corretta")
          case   not(val(.w_ORAFIN)<24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora fine selezione non corretta")
          case   not(VAL(.w_MININI)<60)  and (NOT EMPTY(.w_ORAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti inizio selezione non corretti")
          case   (empty(.w_DATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_MINFIN)<60)  and (NOT EMPTY(.w_ORAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti fine selezione non corretti")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_CLIENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLIENTE_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_sap
      IF (this.w_GIOLUN + this.w_GIOMAR + this.w_GIOMER + this.w_GIOGIO + this.w_GIOVEN + this.w_GIOSAB + this.w_GIODOM) = 0
        AH_ERRORMSG("Selezionare almeno un giorno",'!','')
        i_bRes=.f.
      ENDIF
      IF EMPTY(this.w_ODES)
       AH_ERRORMSG("Selezionare un tipo di stampa",'!','')
        i_bRes=.f.
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ORAINI = this.w_ORAINI
    this.o_ORAFIN = this.w_ORAFIN
    this.o_MININI = this.w_MININI
    this.o_MINFIN = this.w_MINFIN
    return

enddefine

* --- Define pages as container
define class tgsps_sapPag1 as StdContainer
  Width  = 586
  height = 276
  stdWidth  = 586
  stdheight = 276
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODNEG_1_2 as StdField with uid="ADRORXSOVO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODNEG", cQueryName = "CODNEG",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice negozio",;
    HelpContextID = 197575898,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=104, Top=14, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSPS_KAN", oKey_1_1="BUCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODNEG"

  func oCODNEG_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNEG_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNEG_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODNEG_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_KAN',"Negozi azienda",'',this.parent.oContained
  endproc
  proc oCODNEG_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSPS_KAN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_AZIENDA
     i_obj.w_BUCODICE=this.parent.oContained.w_CODNEG
     i_obj.ecpSave()
  endproc

  add object oDADATA_1_3 as StdField with uid="KSMMEHAABE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data di inizio dei documenti di vendita da considerare",;
    HelpContextID = 14930634,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=44

  func oDADATA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_adata)) OR (.w_adata>=.w_dadata))
    endwith
    return bRes
  endfunc

  add object oADATA_1_4 as StdField with uid="TMLQZZAHGL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ADATA", cQueryName = "ADATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data di termine dei documenti di vendita da considerare",;
    HelpContextID = 218038278,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=70

  func oADATA_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ADATA>=.w_DADATA) OR  ( empty(.w_ADATA)))
    endwith
    return bRes
  endfunc

  add object oORAINI_1_5 as StdField with uid="OMNEGAALWK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora inizio selezione non corretta",;
    ToolTipText = "Ora vendita inizio selezione",;
    HelpContextID = 154923290,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=266, Top=44, InputMask=replicate('X',2)

  func oORAINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_ORAINI)<24)
    endwith
    return bRes
  endfunc

  add object oORAFIN_1_6 as StdField with uid="QGDVDGRKAW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora fine selezione non corretta",;
    ToolTipText = "Ora vendita fine selezione",;
    HelpContextID = 76476698,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=266, Top=70, InputMask=replicate('X',2)

  func oORAFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_ORAFIN)<24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_7 as StdField with uid="BEWFXPZRIC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti inizio selezione non corretti",;
    ToolTipText = "Minuti vendita inizio selezione",;
    HelpContextID = 154872378,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=302, Top=44, InputMask=replicate('X',2)

  func oMININI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ORAINI))
    endwith
   endif
  endfunc

  func oMININI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI)<60)
    endwith
    return bRes
  endfunc

  add object oDATSTA_1_8 as StdField with uid="XKCTIADZZD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di elaborazione, usata come riferimento per la promozione",;
    HelpContextID = 13685450,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=476, Top=44

  add object oMINFIN_1_10 as StdField with uid="XRNBKXQFRA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti fine selezione non corretti",;
    ToolTipText = "Minuti vendita fine selezione",;
    HelpContextID = 76425786,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=302, Top=70, InputMask=replicate('X',2)

  func oMINFIN_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ORAFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN)<60)
    endwith
    return bRes
  endfunc

  add object oCLIENTE_1_13 as StdField with uid="DNZWFJCDJL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CLIENTE", cQueryName = "CLIENTE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice cliente � inesistente oppure obsoleto",;
    ToolTipText = "Codice cliente selezionato",;
    HelpContextID = 29394982,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=104, Top=97, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", cZoomOnZoom="GSPS_ACV", oKey_1_1="CLCODCLI", oKey_1_2="this.w_CLIENTE"

  func oCLIENTE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIENTE_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIENTE_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oCLIENTE_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACV',"Clienti negozi",'CLIPOS.CLI_VEND_VZM',this.parent.oContained
  endproc
  proc oCLIENTE_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLCODCLI=this.parent.oContained.w_CLIENTE
     i_obj.ecpSave()
  endproc

  add object oCODMAG_1_14 as StdField with uid="ARVFREAYMP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 201835738,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=123, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc


  add object oObj_1_23 as cp_outputCombo with uid="ZLERXIVPWT",left=104, top=198, width=474,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 214782234


  add object oBtn_1_24 as StdButton with uid="XAOFRYAIID",left=475, top=226, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 250990298;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ODES) AND (.w_GIOLUN + .w_GIOMAR + .w_GIOMER + .w_GIOGIO + .w_GIOVEN + .w_GIOSAB + .w_GIODOM) > 0)
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="KYQESPJULE",left=530, top=226, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 151408454;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESNEG_1_26 as StdField with uid="JWGIIKSIAV",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESNEG", cQueryName = "DESNEG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 197517002,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=157, Top=14, InputMask=replicate('X',40)

  add object oDESCLI_1_29 as StdField with uid="TWSXVINGFB",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 157343434,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=236, Top=97, InputMask=replicate('X',40)

  add object oDESMAG_1_34 as StdField with uid="EJUWTMEEDX",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 201776842,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=236, Top=123, InputMask=replicate('X',30)

  add object oGIOLUN_1_36 as StdCheck with uid="YLFBHHZYJJ",rtseq=26,rtrep=.f.,left=8, top=158, caption="Luned�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 63445658,;
    cFormVar="w_GIOLUN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOLUN_1_36.RadioValue()
    return(iif(this.value =1,2,;
    0))
  endfunc
  func oGIOLUN_1_36.GetRadio()
    this.Parent.oContained.w_GIOLUN = this.RadioValue()
    return .t.
  endfunc

  func oGIOLUN_1_36.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GIOLUN==2,1,;
      0)
  endfunc

  add object oGIOMAR_1_37 as StdCheck with uid="BPOTZCVRPB",rtseq=27,rtrep=.f.,left=78, top=159, caption="Marted�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 17242778,;
    cFormVar="w_GIOMAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOMAR_1_37.RadioValue()
    return(iif(this.value =1,3,;
    0))
  endfunc
  func oGIOMAR_1_37.GetRadio()
    this.Parent.oContained.w_GIOMAR = this.RadioValue()
    return .t.
  endfunc

  func oGIOMAR_1_37.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GIOMAR==3,1,;
      0)
  endfunc

  add object oGIOMER_1_38 as StdCheck with uid="PLMVJQUSYV",rtseq=28,rtrep=.f.,left=156, top=159, caption="Mercoled�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 13048474,;
    cFormVar="w_GIOMER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOMER_1_38.RadioValue()
    return(iif(this.value =1,4,;
    0))
  endfunc
  func oGIOMER_1_38.GetRadio()
    this.Parent.oContained.w_GIOMER = this.RadioValue()
    return .t.
  endfunc

  func oGIOMER_1_38.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GIOMER==4,1,;
      0)
  endfunc

  add object oGIOGIO_1_39 as StdCheck with uid="VBNKFXJXZF",rtseq=29,rtrep=.f.,left=249, top=158, caption="Gioved�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 59579034,;
    cFormVar="w_GIOGIO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOGIO_1_39.RadioValue()
    return(iif(this.value =1,5,;
    0))
  endfunc
  func oGIOGIO_1_39.GetRadio()
    this.Parent.oContained.w_GIOGIO = this.RadioValue()
    return .t.
  endfunc

  func oGIOGIO_1_39.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GIOGIO==5,1,;
      0)
  endfunc

  add object oGIOVEN_1_40 as StdCheck with uid="EPAPSYXMPA",rtseq=30,rtrep=.f.,left=342, top=157, caption="Venerd�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 79567514,;
    cFormVar="w_GIOVEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOVEN_1_40.RadioValue()
    return(iif(this.value =1,6,;
    0))
  endfunc
  func oGIOVEN_1_40.GetRadio()
    this.Parent.oContained.w_GIOVEN = this.RadioValue()
    return .t.
  endfunc

  func oGIOVEN_1_40.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GIOVEN==6,1,;
      0)
  endfunc

  add object oGIOSAB_1_41 as StdCheck with uid="PEJMLNCTOC",rtseq=31,rtrep=.f.,left=412, top=157, caption="Sabato",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 16849562,;
    cFormVar="w_GIOSAB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOSAB_1_41.RadioValue()
    return(iif(this.value =1,7,;
    0))
  endfunc
  func oGIOSAB_1_41.GetRadio()
    this.Parent.oContained.w_GIOSAB = this.RadioValue()
    return .t.
  endfunc

  func oGIOSAB_1_41.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GIOSAB==7,1,;
      0)
  endfunc

  add object oGIODOM_1_42 as StdCheck with uid="IDKNYMLIXI",rtseq=32,rtrep=.f.,left=490, top=159, caption="Domenica",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 87038618,;
    cFormVar="w_GIODOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIODOM_1_42.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oGIODOM_1_42.GetRadio()
    this.Parent.oContained.w_GIODOM = this.RadioValue()
    return .t.
  endfunc

  func oGIODOM_1_42.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GIODOM==1,1,;
      0)
  endfunc

  add object oStr_1_9 as StdString with uid="IOVTSCSLRF",Visible=.t., Left=21, Top=44,;
    Alignment=1, Width=80, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ICFOIHRQGF",Visible=.t., Left=21, Top=73,;
    Alignment=1, Width=80, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="DYGLJWSYXZ",Visible=.t., Left=4, Top=198,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="AMTALZPSCH",Visible=.t., Left=337, Top=44,;
    Alignment=1, Width=136, Height=18,;
    Caption="Data di elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="BSEDNZTUNR",Visible=.t., Left=21, Top=15,;
    Alignment=1, Width=80, Height=18,;
    Caption="Negozio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="GLXRFLHNHN",Visible=.t., Left=21, Top=97,;
    Alignment=1, Width=80, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="MOTKUDJLHM",Visible=.t., Left=21, Top=124,;
    Alignment=1, Width=80, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="STSHYDYXIG",Visible=.t., Left=201, Top=44,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="VPMQFJYJWY",Visible=.t., Left=201, Top=73,;
    Alignment=1, Width=62, Height=18,;
    Caption="A ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="JYAPMVHYHX",Visible=.t., Left=294, Top=43,;
    Alignment=1, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="KPGDAYDNIW",Visible=.t., Left=294, Top=69,;
    Alignment=1, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_sap','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
