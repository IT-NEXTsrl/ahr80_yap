* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bli                                                        *
*              Generazione clienti negozio                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_86]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-06                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bli",oParentObject)
return(i_retval)

define class tgsps_bli as StdBatch
  * --- Local variables
  w_CODCLI = space(15)
  w_CODICE = space(15)
  w_ANTIPCON = space(1)
  w_NOPARIVA = space(12)
  w_NOCODFIS = space(16)
  w_NOCODICE = space(15)
  w_FLGNUM = space(1)
  w_FLGNCL = space(1)
  w_MSG = space(200)
  w_APPO = space(254)
  w_OK = .f.
  w_OKPI = .f.
  w_OKCF = .f.
  w_NUM = 0
  w_NUM2 = 0
  w_NCAR = 0
  * --- WorkFile variables
  CONTI_idx=0
  CLI_VEND_idx=0
  PAR_VDET_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Generazione Clienti Negozio (GSPS_CLI)
    this.w_CODCLI = " "
    * --- Lettura tipo codifica cliente numerica/alfanumerica
    * --- Read from PAR_VDET
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_VDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAFLCPOS"+;
        " from "+i_cTable+" PAR_VDET where ";
            +"PACODAZI = "+cp_ToStrODBC(i_codazi);
            +" and PACODNEG = "+cp_ToStrODBC(this.oParentObject.w_CODNEG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAFLCPOS;
        from (i_cTable) where;
            PACODAZI = i_codazi;
            and PACODNEG = this.oParentObject.w_CODNEG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLGNUM = NVL(cp_ToDate(_read_.PAFLCPOS),cp_NullValue(_read_.PAFLCPOS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura flag numerico su cliente
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCFNUME"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCFNUME;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLGNCL = NVL(cp_ToDate(_read_.AZCFNUME),cp_NullValue(_read_.AZCFNUME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Aggiorno la tabella CONTI 
    vq_exec("..\GPOS\EXE\QUERY\GSPS_CLI.VQR",this,"CLIENTI")
    * --- Caricamento nuovo cliente negozio
    * --- Lettura valori del cliente in azienda da copiare nell'archivio clienti Negozio
    if reccount("CLIENTI")=0 
      ah_ErrorMsg("Non ci sono nominativi da generare",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Try
    local bErr_03635218
    bErr_03635218=bTrsErr
    this.Try_03635218()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Generazione nominativi fallita %1",,"",Message())
    endif
    bTrsErr=bTrsErr or bErr_03635218
    * --- End
  endproc
  proc Try_03635218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ANTIPCON,ANCODICE"
      do vq_exec with '..\GPOS\EXE\QUERY\GSPS_CLI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
          +i_ccchkf;
          +" from "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 set ";
      +"CONTI.ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
          +Iif(Empty(i_ccchkf),"",",CONTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CONTI.ANTIPCON = t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = t2.ANCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set (";
          +"ANCLIPOS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set ";
      +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ANTIPCON = "+i_cQueryTable+".ANTIPCON";
              +" and "+i_cTable+".ANCODICE = "+i_cQueryTable+".ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    SELECT CLIENTI
    GO TOP
    * --- Ciclo su tutti i clienti che non sono nei nominativi
    SCAN 
    this.w_OK = .T.
    this.w_OKPI = .T.
    this.w_OKCF = .T.
    this.w_ANTIPCON = CLIENTI.ANTIPCON
    this.w_CODICE = ALLTRIM(CLIENTI.ANCODICE)
    this.w_CODCLI = this.w_CODICE
    * --- Controllo sulla Partita IVA
    if NOT EMPTY(ALLTRIM(CLIENTI.ANPARIVA))
      * --- Controllo su  partita iva
      * --- Read from CLI_VEND
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" CLI_VEND where ";
              +"CLPARIVA = "+cp_ToStrODBC(ALLTRIM(CLIENTI.ANPARIVA));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              CLPARIVA = ALLTRIM(CLIENTI.ANPARIVA);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_OKPI = .F.
      endif
    else
      if NOT EMPTY(ALLTRIM(CLIENTI.ANCODFIS))
        * --- Controllo su  codice fiscale
        * --- Read from CLI_VEND
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CLI_VEND_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" CLI_VEND where ";
                +"CLCODFIS = "+cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODFIS));
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                CLCODFIS = ALLTRIM(CLIENTI.ANCODFIS);
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_OKCF = .F.
        endif
      endif
    endif
    * --- Se esiste gi� un nominativo con quella partita iva o con il solito 
    *     codice fiscale aggiorno i Clienti P.O.S.
    if this.w_OKPI=.F.
      * --- Write into CLI_VEND
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLINDIRI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANINDIRI),'CLI_VEND','CLINDIRI');
        +",CL___CAP ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN___CAP),'CLI_VEND','CL___CAP');
        +",CLLOCALI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANLOCALI),'CLI_VEND','CLLOCALI');
        +",CLPROVIN ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANPROVIN),'CLI_VEND','CLPROVIN');
        +",CLNAZION ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNAZION),'CLI_VEND','CLNAZION');
        +",CLTELEFO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANTELEFO),'CLI_VEND','CLTELEFO');
        +",CLTELFAX ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANTELFAX),'CLI_VEND','CLTELFAX');
        +",CLNUMCEL ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNUMCEL),'CLI_VEND','CLNUMCEL');
        +",CL_EMAIL ="+cp_NullLink(cp_ToStrODBC(LEFT(CLIENTI.AN_EMAIL,40)),'CLI_VEND','CL_EMAIL');
        +",CL_EMPEC ="+cp_NullLink(cp_ToStrODBC(LEFT(CLIENTI.AN_EMPEC,40)),'CLI_VEND','CL_EMPEC');
        +",CLPARIVA ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANPARIVA),'CLI_VEND','CLPARIVA');
        +",CLCODFIS ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODFIS),'CLI_VEND','CLCODFIS');
        +",CLCODPAG ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODPAG),'CLI_VEND','CLCODPAG');
        +",CLCATCOM ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCATCOM),'CLI_VEND','CLCATCOM');
        +",CLCATSCM ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCATSCM),'CLI_VEND','CLCATSCM');
        +",CLSCONT1 ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN1SCONT),'CLI_VEND','CLSCONT1');
        +",CLSCONT2 ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN2SCONT),'CLI_VEND','CLSCONT2');
        +",CLCODLIS ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNUMLIS),'CLI_VEND','CLCODLIS');
        +",CLDTINVA ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTINVA),'CLI_VEND','CLDTINVA');
        +",CLDTOBSO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTOBSO),'CLI_VEND','CLDTOBSO');
        +",CLVALFID ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANVALFID),'CLI_VEND','CLVALFID');
        +",CLFLFIDO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANFLFIDO),'CLI_VEND','CLFLFIDO');
        +",CLDESCRI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDESCRI),'CLI_VEND','CLDESCRI');
        +",CLCODCON ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODICE),'CLI_VEND','CLCODCON');
        +",CLCODNEG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODNEG),'CLI_VEND','CLCODNEG');
            +i_ccchkf ;
        +" where ";
            +"CLPARIVA = "+cp_ToStrODBC(CLIENTI.ANPARIVA);
               )
      else
        update (i_cTable) set;
            CLINDIRI = CLIENTI.ANINDIRI;
            ,CL___CAP = CLIENTI.AN___CAP;
            ,CLLOCALI = CLIENTI.ANLOCALI;
            ,CLPROVIN = CLIENTI.ANPROVIN;
            ,CLNAZION = CLIENTI.ANNAZION;
            ,CLTELEFO = CLIENTI.ANTELEFO;
            ,CLTELFAX = CLIENTI.ANTELFAX;
            ,CLNUMCEL = CLIENTI.ANNUMCEL;
            ,CL_EMAIL = LEFT(CLIENTI.AN_EMAIL,40);
            ,CL_EMPEC = LEFT(CLIENTI.AN_EMPEC,40);
            ,CLPARIVA = CLIENTI.ANPARIVA;
            ,CLCODFIS = CLIENTI.ANCODFIS;
            ,CLCODPAG = CLIENTI.ANCODPAG;
            ,CLCATCOM = CLIENTI.ANCATCOM;
            ,CLCATSCM = CLIENTI.ANCATSCM;
            ,CLSCONT1 = CLIENTI.AN1SCONT;
            ,CLSCONT2 = CLIENTI.AN2SCONT;
            ,CLCODLIS = CLIENTI.ANNUMLIS;
            ,CLDTINVA = CLIENTI.ANDTINVA;
            ,CLDTOBSO = CLIENTI.ANDTOBSO;
            ,CLVALFID = CLIENTI.ANVALFID;
            ,CLFLFIDO = CLIENTI.ANFLFIDO;
            ,CLDESCRI = CLIENTI.ANDESCRI;
            ,CLCODCON = CLIENTI.ANCODICE;
            ,CLCODNEG = this.oParentObject.w_CODNEG;
            &i_ccchkf. ;
         where;
            CLPARIVA = CLIENTI.ANPARIVA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_OKCF=.F.
      * --- Write into CLI_VEND
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLINDIRI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANINDIRI),'CLI_VEND','CLINDIRI');
        +",CL___CAP ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN___CAP),'CLI_VEND','CL___CAP');
        +",CLLOCALI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANLOCALI),'CLI_VEND','CLLOCALI');
        +",CLPROVIN ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANPROVIN),'CLI_VEND','CLPROVIN');
        +",CLNAZION ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNAZION),'CLI_VEND','CLNAZION');
        +",CLTELEFO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANTELEFO),'CLI_VEND','CLTELEFO');
        +",CLTELFAX ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANTELFAX),'CLI_VEND','CLTELFAX');
        +",CLNUMCEL ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNUMCEL),'CLI_VEND','CLNUMCEL');
        +",CL_EMAIL ="+cp_NullLink(cp_ToStrODBC(LEFT(CLIENTI.AN_EMAIL,40)),'CLI_VEND','CL_EMAIL');
        +",CL_EMPEC ="+cp_NullLink(cp_ToStrODBC(LEFT(CLIENTI.AN_EMPEC,40)),'CLI_VEND','CL_EMPEC');
        +",CLPARIVA ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANPARIVA),'CLI_VEND','CLPARIVA');
        +",CLCODFIS ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODFIS),'CLI_VEND','CLCODFIS');
        +",CLCODPAG ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODPAG),'CLI_VEND','CLCODPAG');
        +",CLCATCOM ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCATCOM),'CLI_VEND','CLCATCOM');
        +",CLCATSCM ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCATSCM),'CLI_VEND','CLCATSCM');
        +",CLSCONT1 ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN1SCONT),'CLI_VEND','CLSCONT1');
        +",CLSCONT2 ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN2SCONT),'CLI_VEND','CLSCONT2');
        +",CLCODLIS ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNUMLIS),'CLI_VEND','CLCODLIS');
        +",CLDTINVA ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTINVA),'CLI_VEND','CLDTINVA');
        +",CLDTOBSO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTOBSO),'CLI_VEND','CLDTOBSO');
        +",CLVALFID ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANVALFID),'CLI_VEND','CLVALFID');
        +",CLFLFIDO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANFLFIDO),'CLI_VEND','CLFLFIDO');
        +",CLDESCRI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDESCRI),'CLI_VEND','CLDESCRI');
        +",CLCODCON ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODICE),'CLI_VEND','CLCODCON');
        +",CLCODNEG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODNEG),'CLI_VEND','CLCODNEG');
            +i_ccchkf ;
        +" where ";
            +"CLCODFIS = "+cp_ToStrODBC(CLIENTI.ANCODFIS);
               )
      else
        update (i_cTable) set;
            CLINDIRI = CLIENTI.ANINDIRI;
            ,CL___CAP = CLIENTI.AN___CAP;
            ,CLLOCALI = CLIENTI.ANLOCALI;
            ,CLPROVIN = CLIENTI.ANPROVIN;
            ,CLNAZION = CLIENTI.ANNAZION;
            ,CLTELEFO = CLIENTI.ANTELEFO;
            ,CLTELFAX = CLIENTI.ANTELFAX;
            ,CLNUMCEL = CLIENTI.ANNUMCEL;
            ,CL_EMAIL = LEFT(CLIENTI.AN_EMAIL,40);
            ,CL_EMPEC = LEFT(CLIENTI.AN_EMPEC,40);
            ,CLPARIVA = CLIENTI.ANPARIVA;
            ,CLCODFIS = CLIENTI.ANCODFIS;
            ,CLCODPAG = CLIENTI.ANCODPAG;
            ,CLCATCOM = CLIENTI.ANCATCOM;
            ,CLCATSCM = CLIENTI.ANCATSCM;
            ,CLSCONT1 = CLIENTI.AN1SCONT;
            ,CLSCONT2 = CLIENTI.AN2SCONT;
            ,CLCODLIS = CLIENTI.ANNUMLIS;
            ,CLDTINVA = CLIENTI.ANDTINVA;
            ,CLDTOBSO = CLIENTI.ANDTOBSO;
            ,CLVALFID = CLIENTI.ANVALFID;
            ,CLFLFIDO = CLIENTI.ANFLFIDO;
            ,CLDESCRI = CLIENTI.ANDESCRI;
            ,CLCODCON = CLIENTI.ANCODICE;
            ,CLCODNEG = this.oParentObject.w_CODNEG;
            &i_ccchkf. ;
         where;
            CLCODFIS = CLIENTI.ANCODFIS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_OK = this.w_OKPI and this.w_OKCF
    if this.w_OK
      * --- Controllo se impostata codifica numerica nei parametri negozio
      if this.w_FLGNUM="S"
        * --- Nel caso di codifica numerica ricalcolo il progressivo
        this.w_NOCODICE = SPACE(15)
        i_Conn=i_TableProp[this.CLI_VEND_IDX, 3]
        cp_NextTableProg(this, i_Conn, "PRCLIPOS", "i_codazi,w_NOCODICE")
        p_LL=LEN(ALLTRIM(g_MASPOS))
        * --- Ridimensiono il codice in base alla 'maschera'  impostata sui parametri offerte
        this.w_CODCLI = RIGHT(ALLTRIM(this.w_NOCODICE),p_LL)
      else
        * --- Read from CLI_VEND
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CLI_VEND_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" CLI_VEND where ";
                +"CLCODCLI = "+cp_ToStrODBC(substr(this.w_CODCLI,1,len(g_MASPOS)));
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                CLCODCLI = substr(this.w_CODCLI,1,len(g_MASPOS));
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se il codice � gi� presente
        * --- Codifica cliente maggiore o solita codifica
        if i_Rows<>0
          this.w_NUM = 1
          this.w_NUM2 = 1
          this.w_OK = .F.
          this.w_NCAR = LEN(g_MASPOS)-1
          if this.w_FLGNCL="S"
            this.w_CODCLI = RIGHT(this.w_CODCLI,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
          else
            this.w_CODCLI = LEFT(this.w_CODCLI,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
          endif
          * --- Controllo sulla lunghezza del codice da creare
          do while this.w_OK=.F. AND this.w_NCAR>=0
            * --- Read from CLI_VEND
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CLI_VEND_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" CLI_VEND where ";
                    +"CLCODCLI = "+cp_ToStrODBC(this.w_CODCLI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    CLCODCLI = this.w_CODCLI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Nel caso esista gi� la codifica ne viene costruita una
            if i_Rows<>0 AND this.w_NUM <= 999999
              this.w_NUM = this.w_NUM+1
              this.w_NUM2 = this.w_NUM2+1
              if this.w_NUM2=10
                this.w_NCAR = this.w_NCAR-1
                this.w_NUM2 = 0
              endif
              if this.w_FLGNCL="S"
                this.w_CODCLI = RIGHT(this.w_CODCLI,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
              else
                this.w_CODCLI = LEFT(this.w_CODCLI,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
              endif
            else
              this.w_OK = .T.
            endif
          enddo
        endif
        if this.w_OK=.F.
          * --- Write into CLI_VEND
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLINDIRI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANINDIRI),'CLI_VEND','CLINDIRI');
            +",CL___CAP ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN___CAP),'CLI_VEND','CL___CAP');
            +",CLLOCALI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANLOCALI),'CLI_VEND','CLLOCALI');
            +",CLPROVIN ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANPROVIN),'CLI_VEND','CLPROVIN');
            +",CLNAZION ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNAZION),'CLI_VEND','CLNAZION');
            +",CLTELEFO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANTELEFO),'CLI_VEND','CLTELEFO');
            +",CLTELFAX ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANTELFAX),'CLI_VEND','CLTELFAX');
            +",CLNUMCEL ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNUMCEL),'CLI_VEND','CLNUMCEL');
            +",CL_EMAIL ="+cp_NullLink(cp_ToStrODBC(LEFT(CLIENTI.AN_EMAIL,40)),'CLI_VEND','CL_EMAIL');
            +",CL_EMPEC ="+cp_NullLink(cp_ToStrODBC(LEFT(CLIENTI.AN_EMPEC,40)),'CLI_VEND','CL_EMPEC');
            +",CLPARIVA ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANPARIVA),'CLI_VEND','CLPARIVA');
            +",CLCODFIS ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODFIS),'CLI_VEND','CLCODFIS');
            +",CLCODPAG ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODPAG),'CLI_VEND','CLCODPAG');
            +",CLCATCOM ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCATCOM),'CLI_VEND','CLCATCOM');
            +",CLCATSCM ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCATSCM),'CLI_VEND','CLCATSCM');
            +",CLSCONT1 ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN1SCONT),'CLI_VEND','CLSCONT1');
            +",CLSCONT2 ="+cp_NullLink(cp_ToStrODBC(CLIENTI.AN2SCONT),'CLI_VEND','CLSCONT2');
            +",CLCODLIS ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNUMLIS),'CLI_VEND','CLCODLIS');
            +",CLDTINVA ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTINVA),'CLI_VEND','CLDTINVA');
            +",CLDTOBSO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTOBSO),'CLI_VEND','CLDTOBSO');
            +",CLVALFID ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANVALFID),'CLI_VEND','CLVALFID');
            +",CLFLFIDO ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANFLFIDO),'CLI_VEND','CLFLFIDO');
            +",CLDESCRI ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDESCRI),'CLI_VEND','CLDESCRI');
            +",CLCODCON ="+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODICE),'CLI_VEND','CLCODCON');
            +",CLCODNEG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODNEG),'CLI_VEND','CLCODNEG');
                +i_ccchkf ;
            +" where ";
                +"CLCODCLI = "+cp_ToStrODBC(this.w_CODCLI);
                   )
          else
            update (i_cTable) set;
                CLINDIRI = CLIENTI.ANINDIRI;
                ,CL___CAP = CLIENTI.AN___CAP;
                ,CLLOCALI = CLIENTI.ANLOCALI;
                ,CLPROVIN = CLIENTI.ANPROVIN;
                ,CLNAZION = CLIENTI.ANNAZION;
                ,CLTELEFO = CLIENTI.ANTELEFO;
                ,CLTELFAX = CLIENTI.ANTELFAX;
                ,CLNUMCEL = CLIENTI.ANNUMCEL;
                ,CL_EMAIL = LEFT(CLIENTI.AN_EMAIL,40);
                ,CL_EMPEC = LEFT(CLIENTI.AN_EMPEC,40);
                ,CLPARIVA = CLIENTI.ANPARIVA;
                ,CLCODFIS = CLIENTI.ANCODFIS;
                ,CLCODPAG = CLIENTI.ANCODPAG;
                ,CLCATCOM = CLIENTI.ANCATCOM;
                ,CLCATSCM = CLIENTI.ANCATSCM;
                ,CLSCONT1 = CLIENTI.AN1SCONT;
                ,CLSCONT2 = CLIENTI.AN2SCONT;
                ,CLCODLIS = CLIENTI.ANNUMLIS;
                ,CLDTINVA = CLIENTI.ANDTINVA;
                ,CLDTOBSO = CLIENTI.ANDTOBSO;
                ,CLVALFID = CLIENTI.ANVALFID;
                ,CLFLFIDO = CLIENTI.ANFLFIDO;
                ,CLDESCRI = CLIENTI.ANDESCRI;
                ,CLCODCON = CLIENTI.ANCODICE;
                ,CLCODNEG = this.oParentObject.w_CODNEG;
                &i_ccchkf. ;
             where;
                CLCODCLI = this.w_CODCLI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    if this.w_OK=.T.
      * --- Inserimento nuovo cliente offerte
      if this.w_FLGNUM<>"S"
        this.w_CODCLI = substr(this.w_CODCLI,1, len(g_MASPOS))
      endif
      ah_Msg("Inserimento nominativo: %1",.T.,.F.,.F., this.w_CODCLI)
      * --- Insert into CLI_VEND
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CLCODCLI"+",CLDESCRI"+",CLCODCON"+",CLINDIRI"+",CL___CAP"+",CLLOCALI"+",CLPROVIN"+",CLNAZION"+",CLPARIVA"+",CLCODFIS"+",CLTELEFO"+",CLTELFAX"+",CL_EMAIL"+",CL_EMPEC"+",CLDTOBSO"+",CLNUMCEL"+",CLCODPAG"+",CLCATCOM"+",CLCATSCM"+",CLSCONT1"+",CLSCONT2"+",CLVALFID"+",CLCODLIS"+",CLDTINVA"+",CLFLFIDO"+",CLCODNEG"+",CLTIPCON"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODCLI),'CLI_VEND','CLCODCLI');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANDESCRI)),'CLI_VEND','CLDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'CLI_VEND','CLCODCON');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANINDIRI)),'CLI_VEND','CLINDIRI');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.AN___CAP)),'CLI_VEND','CL___CAP');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANLOCALI)),'CLI_VEND','CLLOCALI');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANPROVIN)),'CLI_VEND','CLPROVIN');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANNAZION)),'CLI_VEND','CLNAZION');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANPARIVA)),'CLI_VEND','CLPARIVA');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODFIS)),'CLI_VEND','CLCODFIS');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANTELEFO)),'CLI_VEND','CLTELEFO');
        +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANTELFAX)),'CLI_VEND','CLTELFAX');
        +","+cp_NullLink(cp_ToStrODBC(LEFT(CLIENTI.AN_EMAIL,40)),'CLI_VEND','CL_EMAIL');
        +","+cp_NullLink(cp_ToStrODBC(LEFT(CLIENTI.AN_EMPEC,40)),'CLI_VEND','CL_EMPEC');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTOBSO),'CLI_VEND','CLDTOBSO');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNUMCEL),'CLI_VEND','CLNUMCEL');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCODPAG),'CLI_VEND','CLCODPAG');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCATCOM),'CLI_VEND','CLCATCOM');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANCATSCM),'CLI_VEND','CLCATSCM');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.AN1SCONT),'CLI_VEND','CLSCONT1');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.AN2SCONT),'CLI_VEND','CLSCONT2');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANVALFID),'CLI_VEND','CLVALFID');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANNUMLIS),'CLI_VEND','CLCODLIS');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTINVA),'CLI_VEND','CLDTINVA');
        +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANFLFIDO),'CLI_VEND','CLFLFIDO');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODNEG),'CLI_VEND','CLCODNEG');
        +","+cp_NullLink(cp_ToStrODBC("C"),'CLI_VEND','CLTIPCON');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CLCODCLI',this.w_CODCLI,'CLDESCRI',ALLTRIM(CLIENTI.ANDESCRI),'CLCODCON',this.w_CODICE,'CLINDIRI',ALLTRIM(CLIENTI.ANINDIRI),'CL___CAP',ALLTRIM(CLIENTI.AN___CAP),'CLLOCALI',ALLTRIM(CLIENTI.ANLOCALI),'CLPROVIN',ALLTRIM(CLIENTI.ANPROVIN),'CLNAZION',ALLTRIM(CLIENTI.ANNAZION),'CLPARIVA',ALLTRIM(CLIENTI.ANPARIVA),'CLCODFIS',ALLTRIM(CLIENTI.ANCODFIS),'CLTELEFO',ALLTRIM(CLIENTI.ANTELEFO),'CLTELFAX',ALLTRIM(CLIENTI.ANTELFAX))
        insert into (i_cTable) (CLCODCLI,CLDESCRI,CLCODCON,CLINDIRI,CL___CAP,CLLOCALI,CLPROVIN,CLNAZION,CLPARIVA,CLCODFIS,CLTELEFO,CLTELFAX,CL_EMAIL,CL_EMPEC,CLDTOBSO,CLNUMCEL,CLCODPAG,CLCATCOM,CLCATSCM,CLSCONT1,CLSCONT2,CLVALFID,CLCODLIS,CLDTINVA,CLFLFIDO,CLCODNEG,CLTIPCON &i_ccchkf. );
           values (;
             this.w_CODCLI;
             ,ALLTRIM(CLIENTI.ANDESCRI);
             ,this.w_CODICE;
             ,ALLTRIM(CLIENTI.ANINDIRI);
             ,ALLTRIM(CLIENTI.AN___CAP);
             ,ALLTRIM(CLIENTI.ANLOCALI);
             ,ALLTRIM(CLIENTI.ANPROVIN);
             ,ALLTRIM(CLIENTI.ANNAZION);
             ,ALLTRIM(CLIENTI.ANPARIVA);
             ,ALLTRIM(CLIENTI.ANCODFIS);
             ,ALLTRIM(CLIENTI.ANTELEFO);
             ,ALLTRIM(CLIENTI.ANTELFAX);
             ,LEFT(CLIENTI.AN_EMAIL,40);
             ,LEFT(CLIENTI.AN_EMPEC,40);
             ,CLIENTI.ANDTOBSO;
             ,CLIENTI.ANNUMCEL;
             ,CLIENTI.ANCODPAG;
             ,CLIENTI.ANCATCOM;
             ,CLIENTI.ANCATSCM;
             ,CLIENTI.AN1SCONT;
             ,CLIENTI.AN2SCONT;
             ,CLIENTI.ANVALFID;
             ,CLIENTI.ANNUMLIS;
             ,CLIENTI.ANDTINVA;
             ,CLIENTI.ANFLFIDO;
             ,this.oParentObject.w_CODNEG;
             ,"C";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Generazione nominativi completata",,"")
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("CLIENTI")
      select CLIENTI
      use
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CLI_VEND'
    this.cWorkTables[3]='PAR_VDET'
    this.cWorkTables[4]='AZIENDA'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
