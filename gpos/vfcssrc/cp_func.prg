* --- Container for functions
* --- START PSCALZER
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: pscalzer                                                        *
*              Autonumber cli pos                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_18]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2001-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func pscalzer
param pCODICE,pFUNC

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_APPO
  m.w_APPO=space(15)
  private w_FUNC
  m.w_FUNC=space(15)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "pscalzer"
if vartype(__pscalzer_hook__)='O'
  __pscalzer_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'pscalzer('+Transform(pCODICE)+','+Transform(pFUNC)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pscalzer')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
pscalzer_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'pscalzer('+Transform(pCODICE)+','+Transform(pFUNC)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pscalzer')
Endif
*--- Activity log
if vartype(__pscalzer_hook__)='O'
  __pscalzer_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure pscalzer_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  m.w_FUNC = UPPER(m.pFUNC)
  m.w_APPO = m.pCODICE
  if g_FLCPOS="S" AND LEN(ALLTRIM(g_MASPOS))<>0 AND NOT EMPTY(m.w_APPO)
    m.w_APPO = ALLTRIM(m.w_APPO)
    * ---  Controllo Zero Fill Modificato:
    * --- 1 - Viene testato solo se l'input e' INTERAMENTE composto da Cifre
    * --- 2 - Se presente una Picture sulla GET, calcola sulla Lunghezza di questa
    Private p_L, p_LL
    p_LL=0
    p_L=0
    * --- Cerca se presente un Qualsiasi Carattere diverso da un numero
    do while p_LL<LEN(m.w_APPO)
      p_LL = p_LL + 1
      if NOT SUBSTR(m.w_APPO, p_LL, 1) $ "1234567890"
        * --- Se esiste non esegue la Zero Fill
        p_L=-1
        EXIT
      endif
    enddo
    if p_L<>-1
      * --- Calcola la lunghezza dell'input sulla Picture di GET (se esiste)
      p_LL=LEN(ALLTRIM(g_MASPOS))
      m.w_APPO = RIGHT(REPL("0", p_LL) + ALLTRIM(m.w_APPO), p_LL)
    endif
  endif
  i_retcode = 'stop'
  i_retval = IIF(m.w_FUNC="LOAD", LEFT(m.w_APPO, 15), m.w_APPO)
  return
endproc


* --- END PSCALZER
* --- START PSCHKART
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: pschkart                                                        *
*              Verifica se articolo P.O.S.                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_486]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-04                                                      *
* Last revis.: 2012-08-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func pschkart
param pCodart,pCodice,pDatreg,pCodesc,pCodcli,pPadre

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODART
  m.w_CODART=space(20)
  private w_CODICE
  m.w_CODICE=space(20)
  private w_ARTPOS
  m.w_ARTPOS=space(1)
  private w_OK
  m.w_OK=.f.
  private w_MESS
  m.w_MESS=space(200)
  private w_DTOBSO
  m.w_DTOBSO=ctod("  /  /  ")
  private w_LENSCF
  m.w_LENSCF=0
  private w_ARTCLI
  m.w_ARTCLI=space(1)
  private w_oMESS
  m.w_oMESS = .null.
  private w_oPART
  m.w_oPART = .null.
* --- WorkFile variables
  private ART_ICOL_idx
  ART_ICOL_idx=0
  private KEY_ARTI_idx
  KEY_ARTI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "pschkart"
if vartype(__pschkart_hook__)='O'
  __pschkart_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'pschkart('+Transform(pCodart)+','+Transform(pCodice)+','+Transform(pDatreg)+','+Transform(pCodesc)+','+Transform(pCodcli)+','+Transform(pPadre)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pschkart')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if pschkart_OpenTables()
  pschkart_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'pschkart('+Transform(pCodart)+','+Transform(pCodice)+','+Transform(pDatreg)+','+Transform(pCodesc)+','+Transform(pCodcli)+','+Transform(pPadre)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pschkart')
Endif
*--- Activity log
if vartype(__pschkart_hook__)='O'
  __pschkart_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure pschkart_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Verifica se Articolo P.O.S., se obsoleto, se suffisso congruente con intestatario
  m.w_OK = .T.
  if NOT EMPTY(m.pCodice)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[ART_ICOL_idx,2],.t.,ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARARTPOS,ARDTOBSO"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(m.pCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARARTPOS,ARDTOBSO;
        from (i_cTable) where;
            ARCODART = m.pCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_ARTPOS = NVL(cp_ToDate(_read_.ARARTPOS),cp_NullValue(_read_.ARARTPOS))
      m.w_DTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo esistenza 
    if i_ROWS=0
      m.w_MESS = "Articolo inesistente"
      m.w_OK = .F.
    endif
    if m.w_OK and m.w_ARTPOS<>"S"
      m.w_MESS = "Articolo no P.O.S"
      m.w_OK = .F.
    endif
    * --- prima su articolo
    if m.w_OK AND NOT EMPTY(m.w_DTOBSO) AND m.w_DTOBSO<=m.pDatreg
      m.w_MESS = AH_MSGFORMAT("Codice articolo obsoleto dal %1",dtoc(m.w_DTOBSO) )
      m.w_OK = .F.
    endif
    * --- poi su codice di ricerca
    if m.w_OK
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[KEY_ARTI_idx,2],.t.,KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CADTOBSO,CALENSCF,CATIPCON"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(m.pCodice);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CADTOBSO,CALENSCF,CATIPCON;
          from (i_cTable) where;
              CACODICE = m.pCodice;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_DTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        m.w_LENSCF = NVL(cp_ToDate(_read_.CALENSCF),cp_NullValue(_read_.CALENSCF))
        m.w_ARTCLI = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(m.w_DTOBSO) AND m.w_DTOBSO<=m.pDatreg
        m.w_MESS = AH_MSGFORMAT("Codice ricerca obsoleto dal %1",dtoc(m.w_DTOBSO) )
        m.w_OK = .F.
      endif
    endif
    if m.w_OK And g_FLCESC = "S" And ((m.w_LENSCF<>0 And Right(Alltrim(m.pCodice), m.w_LENSCF) <> IIF(Empty(Alltrim(m.pCodesc)),"######",Alltrim(m.pCodesc))) Or (m.w_LENSCF=0 And Not Empty(m.pCodesc) And m.w_ARTCLI$"C-F"))
      * --- istanzio oggetto per mess. incrementali
      w_oMESS=createobject("ah_message")
      w_oMESS.AddMsgPartNL("Codice articolo incongruente con intestatario")
      if Empty(m.pCodesc)
        m.w_oPART = m.w_oMESS.addmsgpartNL("Nessun suffisso codice di ricerca legato a %1")
        w_oPART.addParam(Alltrim(m.pCodcli))
      else
        m.w_oPART = m.w_oMESS.addmsgpartNL("Suffisso codice di ricerca legato a %1 = %2")
        w_oPART.addParam(Alltrim(m.pCodcli))
        w_oPART.addParam(m.pCodesc)
      endif
      if m.w_LENSCF = 0
        w_oMESS.AddMsgPartNL("Nessun suffisso assegnato al codice di ricerca di tipo cliente")
      else
        m.w_oPART = m.w_oMESS.addmsgpartNL("Suffisso assegnato al codice di ricerca = %1")
        w_oPART.addParam(Right(Alltrim(m.pCodice),m.w_LENSCF))
      endif
      m.w_MESS = m.w_oMESS.ComposeMessage()
      m.w_OK = .F.
    endif
  endif
  if !m.w_OK
    m.pPadre.w_Errorcod = m.w_MESS
  endif
  i_retcode = 'stop'
  i_retval = m.w_OK
  return
endproc


  function pschkart_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='ART_ICOL'
    i_cWorkTables[2]='KEY_ARTI'
    return(cp_OpenFuncTables(2))
* --- END PSCHKART
* --- START PSCHKCLI
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: pschkcli                                                        *
*              Controllo univocita cliente                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-20                                                      *
* Last revis.: 2003-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func pschkcli
param pCODICE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODCLI
  m.w_CODCLI=space(15)
  private w_APPO
  m.w_APPO=.f.
* --- WorkFile variables
  private CLI_VEND_idx
  CLI_VEND_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "pschkcli"
if vartype(__pschkcli_hook__)='O'
  __pschkcli_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'pschkcli('+Transform(pCODICE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pschkcli')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if pschkcli_OpenTables()
  pschkcli_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'pschkcli('+Transform(pCODICE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pschkcli')
Endif
*--- Activity log
if vartype(__pschkcli_hook__)='O'
  __pschkcli_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure pschkcli_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione per il controllo di univocit� del codice cliente
  m.w_APPO = .F.
  m.w_CODCLI = ALLTRIM(m.pCODICE)
  * --- Read from CLI_VEND
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[CLI_VEND_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[CLI_VEND_idx,2],.t.,CLI_VEND_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "CLCODCLI"+;
      " from "+i_cTable+" CLI_VEND where ";
          +"CLCODCLI = "+cp_ToStrODBC(m.w_CODCLI);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      CLCODCLI;
      from (i_cTable) where;
          CLCODCLI = m.w_CODCLI;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCLI),cp_NullValue(_read_.CLCODCLI))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if i_Rows=0
    m.w_APPO = .T.
  endif
  i_retcode = 'stop'
  i_retval = m.w_APPO
  return
endproc


  function pschkcli_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='CLI_VEND'
    return(cp_OpenFuncTables(1))
* --- END PSCHKCLI
* --- START PSCHKKIT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: pschkkit                                                        *
*              Verifica se articolo P.O.S.                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_486]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-04                                                      *
* Last revis.: 2003-04-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func pschkkit
param pCodi

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODART
  m.w_CODART=space(20)
  private w_OK
  m.w_OK=.f.
* --- WorkFile variables
  private KIT_MAST_idx
  KIT_MAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "pschkkit"
if vartype(__pschkkit_hook__)='O'
  __pschkkit_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'pschkkit('+Transform(pCodi)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pschkkit')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if pschkkit_OpenTables()
  pschkkit_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_KIT_MAST')
  use in _Curs_KIT_MAST
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'pschkkit('+Transform(pCodi)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pschkkit')
Endif
*--- Activity log
if vartype(__pschkkit_hook__)='O'
  __pschkkit_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure pschkkit_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Verifica se Articolo KIT
  m.w_CODART = m.pCodi
  m.w_OK = .F.
  * --- Select from KIT_MAST
  i_nConn=i_TableProp[KIT_MAST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[KIT_MAST_idx,2],.t.,KIT_MAST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select * from "+i_cTable+" KIT_MAST ";
        +" where KICODART="+cp_ToStrODBC(m.w_CODART)+"";
         ,"_Curs_KIT_MAST")
  else
    select * from (i_cTable);
     where KICODART=m.w_CODART;
      into cursor _Curs_KIT_MAST
  endif
  if used('_Curs_KIT_MAST')
    select _Curs_KIT_MAST
    locate for 1=1
    do while not(eof())
    m.w_OK = .T.
      select _Curs_KIT_MAST
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_OK
  return
endproc


  function pschkkit_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='KIT_MAST'
    return(cp_OpenFuncTables(1))
* --- END PSCHKKIT
* --- START PSEDTIVA
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: psedtiva                                                        *
*              Editabilita cod.IVA reparto                                     *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-08-26                                                      *
* Last revis.: 2005-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func psedtiva
param pCodRep

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=.f.
* --- WorkFile variables
  private ART_ICOL_idx
  ART_ICOL_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "psedtiva"
if vartype(__psedtiva_hook__)='O'
  __psedtiva_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'psedtiva('+Transform(pCodRep)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'psedtiva')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if psedtiva_OpenTables()
  psedtiva_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_ART_ICOL')
  use in _Curs_ART_ICOL
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'psedtiva('+Transform(pCodRep)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'psedtiva')
Endif
*--- Activity log
if vartype(__psedtiva_hook__)='O'
  __psedtiva_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure psedtiva_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Se esiste un articolo con il reparto allora non posso modificare il codice IVA
  m.w_RESULT = .T.
  * --- Select from ART_ICOL
  i_nConn=i_TableProp[ART_ICOL_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ART_ICOL_idx,2],.t.,ART_ICOL_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select Distinct 1 from "+i_cTable+" ART_ICOL ";
        +" where ARCODREP= "+cp_ToStrODBC(m.pCodRep)+"";
         ,"_Curs_ART_ICOL")
  else
    select Distinct 1 from (i_cTable);
     where ARCODREP= m.pCodRep;
      into cursor _Curs_ART_ICOL
  endif
  if used('_Curs_ART_ICOL')
    select _Curs_ART_ICOL
    locate for 1=1
    do while not(eof())
    m.w_RESULT = .F.
      select _Curs_ART_ICOL
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function psedtiva_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='ART_ICOL'
    return(cp_OpenFuncTables(1))
* --- END PSEDTIVA
* --- START PSRIGDES
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: psrigdes                                                        *
*              Righe descrittive scontrino                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_13]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-11                                                      *
* Last revis.: 2003-06-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func psrigdes
param pString,pNumcar,pCod

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_STR
  m.w_STR=space(100)
  private w_NUM
  m.w_NUM=0
  private w_COD
  m.w_COD=space(10)
  private w_RECORD
  m.w_RECORD=space(100)
  private w_Separa
  m.w_Separa=space(2)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "psrigdes"
if vartype(__psrigdes_hook__)='O'
  __psrigdes_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'psrigdes('+Transform(pString)+','+Transform(pNumcar)+','+Transform(pCod)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'psrigdes')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
psrigdes_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'psrigdes('+Transform(pString)+','+Transform(pNumcar)+','+Transform(pCod)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'psrigdes')
Endif
*--- Activity log
if vartype(__psrigdes_hook__)='O'
  __psrigdes_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure psrigdes_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione per la suddivisione su pi� righe delle righe descrittive su uno scontrino
  * --- 
  *     pString : Stringa da elaborare
  *     pNumcar: Numero di caratteri da suddividere su ogni riga
  *     pCod: Codice utilizzato per aprire la stringa (codifica del linguaggio Sarema)
  *     
  m.w_Separa = chr(13)+chr(10)
  m.w_STR = alltrim(m.pString)
  m.w_NUM = iif(m.pNumcar=0,19,m.pNumcar)
  m.w_COD = alltrim(m.pCod)
  m.w_RECORD = ""
  do while not empty(m.w_STR)
    m.w_RECORD = m.w_RECORD+m.w_COD+LEFT(m.w_STR,m.w_NUM)+";"+m.w_Separa
    m.w_STR = SUBSTR(m.w_STR,m.w_NUM+1)
  enddo
  i_retcode = 'stop'
  i_retval = m.w_RECORD
  return
endproc


* --- END PSRIGDES
* --- START CKDELNEG
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ckdelneg                                                        *
*              Check eliminazione negozio                                      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-01-19                                                      *
* Last revis.: 2007-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ckdelneg
param pCodNeg

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=.f.
* --- WorkFile variables
  private CLI_VEND_idx
  CLI_VEND_idx=0
  private PAR_VDET_idx
  PAR_VDET_idx=0
  private COR_RISP_idx
  COR_RISP_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ckdelneg"
if vartype(__ckdelneg_hook__)='O'
  __ckdelneg_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ckdelneg('+Transform(pCodNeg)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ckdelneg')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if ckdelneg_OpenTables()
  ckdelneg_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_CLI_VEND')
  use in _Curs_CLI_VEND
endif
if used('_Curs_PAR_VDET')
  use in _Curs_PAR_VDET
endif
if used('_Curs_COR_RISP')
  use in _Curs_COR_RISP
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ckdelneg('+Transform(pCodNeg)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ckdelneg')
Endif
*--- Activity log
if vartype(__ckdelneg_hook__)='O'
  __ckdelneg_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ckdelneg_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Verifica se il codice negozio passato come parametro � eliminabile
  m.w_RESULT = .T.
  * --- Verifico se presente nei clienti negozio
  * --- Select from CLI_VEND
  i_nConn=i_TableProp[CLI_VEND_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[CLI_VEND_idx,2],.t.,CLI_VEND_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select Count(*) As Conta from "+i_cTable+" CLI_VEND ";
        +" where CLCODNEG="+cp_ToStrODBC(m.pCodneg)+"";
         ,"_Curs_CLI_VEND")
  else
    select Count(*) As Conta from (i_cTable);
     where CLCODNEG=m.pCodneg;
      into cursor _Curs_CLI_VEND
  endif
  if used('_Curs_CLI_VEND')
    select _Curs_CLI_VEND
    locate for 1=1
    do while not(eof())
    m.w_RESULT = Nvl( _Curs_CLI_VEND.CONTA ,0 )=0
      select _Curs_CLI_VEND
      continue
    enddo
    use
  endif
  if m.w_RESULT
    * --- Parametri POS
    * --- Select from PAR_VDET
    i_nConn=i_TableProp[PAR_VDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[PAR_VDET_idx,2],.t.,PAR_VDET_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta from "+i_cTable+" PAR_VDET ";
          +" where PACODNEG="+cp_ToStrODBC(m.pCodneg)+"";
           ,"_Curs_PAR_VDET")
    else
      select Count(*) As Conta from (i_cTable);
       where PACODNEG=m.pCodneg;
        into cursor _Curs_PAR_VDET
    endif
    if used('_Curs_PAR_VDET')
      select _Curs_PAR_VDET
      locate for 1=1
      do while not(eof())
      m.w_RESULT = Nvl( _Curs_PAR_VDET.CONTA ,0 )=0
        select _Curs_PAR_VDET
        continue
      enddo
      use
    endif
  endif
  if m.w_RESULT
    * --- Movimenti POS
    * --- Select from COR_RISP
    i_nConn=i_TableProp[COR_RISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[COR_RISP_idx,2],.t.,COR_RISP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta from "+i_cTable+" COR_RISP ";
          +" where MDCODNEG="+cp_ToStrODBC(m.pCodneg)+"";
           ,"_Curs_COR_RISP")
    else
      select Count(*) As Conta from (i_cTable);
       where MDCODNEG=m.pCodneg;
        into cursor _Curs_COR_RISP
    endif
    if used('_Curs_COR_RISP')
      select _Curs_COR_RISP
      locate for 1=1
      do while not(eof())
      m.w_RESULT = Nvl( _Curs_COR_RISP.CONTA ,0 )=0
        select _Curs_COR_RISP
        continue
      enddo
      use
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function ckdelneg_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='CLI_VEND'
    i_cWorkTables[2]='PAR_VDET'
    i_cWorkTables[3]='COR_RISP'
    return(cp_OpenFuncTables(3))
* --- END CKDELNEG
* --- START CKSERNEG
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ckserneg                                                        *
*              Funzione per controllo unicit� della serie                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-26                                                      *
* Last revis.: 2008-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ckserneg
param p_CODAZI,pALFDOC,pCODNEG

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PAALFDOC
  m.w_PAALFDOC=space(10)
  private w_PACODAZI
  m.w_PACODAZI=space(5)
  private w_PACODNEG
  m.w_PACODNEG=space(3)
  private w_TEMPC
  m.w_TEMPC=space(1)
  private w_RET
  m.w_RET=.f.
* --- WorkFile variables
  private PAR_VDET_idx
  PAR_VDET_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ckserneg"
if vartype(__ckserneg_hook__)='O'
  __ckserneg_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ckserneg('+Transform(p_CODAZI)+','+Transform(pALFDOC)+','+Transform(pCODNEG)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ckserneg')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if ckserneg_OpenTables()
  ckserneg_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_PAR_VDET')
  use in _Curs_PAR_VDET
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ckserneg('+Transform(p_CODAZI)+','+Transform(pALFDOC)+','+Transform(pCODNEG)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ckserneg')
Endif
*--- Activity log
if vartype(__ckserneg_hook__)='O'
  __ckserneg_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ckserneg_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- --Controllo sull'univocita della serie, ogni negozio deve avere una serie diversa
  *     � necessario permettere di avere una serie "nulla" in un solo negozio POS al fine di poter mantenere la numerazione dei documenti senza serie.
  m.w_RET = .F.
  m.w_PAALFDOC = nvl(m.pALFDOC," ")
  m.w_PACODAZI = m.p_CODAZI
  m.w_PACODNEG = IIF(Vartype(m.pCODNEG)="C",m.pCODNEG,"   ")
  * --- --Controllo che la serie non sia gi� associato ad un negozio
  m.w_RET = .T.
  * --- Select from PAR_VDET
  i_nConn=i_TableProp[PAR_VDET_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PAR_VDET_idx,2],.t.,PAR_VDET_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select PACODAZI,PAALFDOC,PACODNEG from "+i_cTable+" PAR_VDET ";
        +" where PACODAZI="+cp_ToStrODBC(m.w_PACODAZI)+" AND PAALFDOC="+cp_ToStrODBC(m.w_PAALFDOC)+"   AND PACODNEG <> "+cp_ToStrODBC(m.w_PACODNEG)+"";
         ,"_Curs_PAR_VDET")
  else
    select PACODAZI,PAALFDOC,PACODNEG from (i_cTable);
     where PACODAZI=m.w_PACODAZI AND PAALFDOC=m.w_PAALFDOC   AND PACODNEG <> m.w_PACODNEG;
      into cursor _Curs_PAR_VDET
  endif
  if used('_Curs_PAR_VDET')
    select _Curs_PAR_VDET
    locate for 1=1
    do while not(eof())
    m.w_RET = .F.
      select _Curs_PAR_VDET
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_RET
  return
endproc


  function ckserneg_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PAR_VDET'
    return(cp_OpenFuncTables(1))
* --- END CKSERNEG
* --- START PSCHKMOD
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: pschkmod                                                        *
*              Verifica se mod.vendita corretta                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_486]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-04                                                      *
* Last revis.: 2012-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func pschkmod
param pDocddt,pTipchi

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_TDMINVEN
  m.w_TDMINVEN=space(1)
  private w_TDTOTDOC
  m.w_TDTOTDOC=space(1)
  private w_MESS
  m.w_MESS=space(200)
  private w_OK
  m.w_OK=.f.
* --- WorkFile variables
  private TIP_DOCU_idx
  TIP_DOCU_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "pschkmod"
if vartype(__pschkmod_hook__)='O'
  __pschkmod_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'pschkmod('+Transform(pDocddt)+','+Transform(pTipchi)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pschkmod')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if pschkmod_OpenTables()
  pschkmod_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'pschkmod('+Transform(pDocddt)+','+Transform(pTipchi)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'pschkmod')
Endif
*--- Activity log
if vartype(__pschkmod_hook__)='O'
  __pschkmod_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure pschkmod_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Verifica se la causale doc di trasporto associato alla modalit� di vendita selezionata ha attivi i controlli su quantit� e importo
  m.w_OK = .T.
  if NOT EMPTY(m.pDocddt) and m.pTipchi="DT"
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TIP_DOCU_idx,2],.t.,TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDTOTDOC,TDMINVEN"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(m.pDocddt);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDTOTDOC,TDMINVEN;
        from (i_cTable) where;
            TDTIPDOC = m.pDocddt;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_TDTOTDOC = NVL(cp_ToDate(_read_.TDTOTDOC),cp_NullValue(_read_.TDTOTDOC))
      m.w_TDMINVEN = NVL(cp_ToDate(_read_.TDMINVEN),cp_NullValue(_read_.TDMINVEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo flag documento ddt di destinazione 
    if m.w_TDMINVEN<>"E" OR m.w_TDTOTDOC<>"E"
      m.w_OK = .F.
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_OK
  return
endproc


  function pschkmod_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='TIP_DOCU'
    return(cp_OpenFuncTables(1))
* --- END PSCHKMOD
