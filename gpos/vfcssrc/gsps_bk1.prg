* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bk1                                                        *
*              Autonumber cliente                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-11                                                      *
* Last revis.: 2003-03-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bk1",oParentObject)
return(i_retval)

define class tgsps_bk1 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiusta il Codice Cliente/Fornitore calcolato dall'Autonumber alla effettiva lunghezza della Picture Parametrica p_NOM (da GSPS_KCC)
    if g_FLCPOS="S"
      if LEN(ALLTRIM(p_CLF))<>0
        this.oParentObject.w_ANCODICE = RIGHT(this.oParentObject.w_ANCODICE, LEN(ALLTRIM(p_CLF)))
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
