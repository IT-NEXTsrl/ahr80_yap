* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps1bpz                                                        *
*              Riga sconti                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][21][VRS_82]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-06                                                      *
* Last revis.: 2015-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps1bpz",oParentObject)
return(i_retval)

define class tgsps1bpz as StdBatch
  * --- Local variables
  w_ARTDES = space(20)
  w_ARTPOS = space(1)
  w_TIPOPE = space(1)
  w_SIMVAL = space(5)
  w_SCOVEN = 0
  w_TOTRIP = 0
  w_FIRST = 0
  w_PADRE = .NULL.
  w_FOUNDREC = 0
  w_POSI = 0
  w_GIAINS = .f.
  w_TOT_RIP = 0
  w_GRUFID = space(5)
  w_REPFID = space(5)
  w_IVAFID = space(5)
  w_UMFID = space(5)
  w_CONTA = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  VALUTE_idx=0
  REP_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da area manuale Replace End di GSPS_MVD
    *     Creazione riga descrittiva per sconti di piede
    this.w_PADRE = this.oParentObject
    this.w_TIPOPE = this.w_PADRE.cFunction
    this.w_SIMVAL = " "
    * --- Leturra simbolo valuta per riga descrittiva
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_CODEUR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_CODEUR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Memorizzo il posizionamento sul Temporaneo
    this.w_PADRE.MarkPos()     
    if this.w_TIPOPE="Edit"
      * --- Se sono in modifica prima di riscrivere le righe descrittive di sconti di piede 
      *     cancello quella inserita precedentemente
      this.w_POSI = this.w_PADRE.RowIndex()
      this.w_FOUNDREC = this.w_PADRE.Search("t_MDCODPRO = '#########*' And not Deleted() ")
      if this.w_FOUNDREC>0
        this.w_PADRE.SetRow(this.w_FOUNDREC)     
        this.w_PADRE.DeleteRow()     
        * --- Mi vado a riposizionarmi sulla prima riga, non posso fare altrimenti
        *     avendo cancellato una riga
        this.w_PADRE.FirstRow()     
        this.w_PADRE.MarkPos()     
      else
        * --- Mi riposiziono sulla riga di partenza
        this.w_PADRE.SetRow(this.w_POSI)     
      endif
    endif
    if this.oParentObject.w_MDSCOCL1<>0 OR this.oParentObject.w_MDSCOPAG<>0 OR ABS(this.oParentObject.w_MDSCONTI)<>0
      this.w_FIRST = 0
      this.w_PADRE.FirstRow()     
      this.w_GIAINS = .F.
      this.w_TOT_RIP = 0
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        if this.oParentObject.w_MDTIPRIG<>"D" And this.oParentObject.w_MDFLOMAG="X" And this.oParentObject.w_FLSERA<>"S"
          * --- Memorizzo la prima riga sulla quale eseguo la ripartizione per eventuali differenze
          if this.w_FIRST=0
            this.w_FIRST = this.w_PADRE.RowIndex()
          endif
          * --- Ripartito su riga
          this.w_SCOVEN = cp_ROUND(((this.oParentObject.w_MDSCONTI * (NVL(this.oParentObject.w_VALRIG,0))) / this.oParentObject.w_MDTOTVEN ),this.oParentObject.w_DECTOT)
          * --- Somma dei ripartiti. Sottraggo per averlo in positivo
          this.w_TOT_RIP = this.w_TOT_RIP - this.w_SCOVEN-Nvl(this.oParentObject.w_MDPROMOZ,0)-Nvl(this.oParentObject.w_MDESPSCO,0)
          this.w_TOTRIP = this.w_TOTRIP - this.w_SCOVEN
          this.w_PADRE.Set("w_MDSCOVEN" , this.w_SCOVEN+Nvl(this.oParentObject.w_MDPROMOZ,0)+Nvl(this.oParentObject.w_MDESPSCO,0))     
          this.w_SCOVEN = this.oParentObject.w_MDSCOVEN + this.w_SCOVEN
        endif
        this.w_PADRE.NextRow()     
      enddo
      if ABS(this.oParentObject.w_MDSCONTI+this.oParentObject.w_MDSTOPRO) <> ABS(this.w_TOT_RIP)
        * --- Nel caso la somma dei ripartiti � diversa dal totale scontato, 
        *     riporto la differenza sulla prima riga sulla quale ho fatto una ripartizione
        this.w_SCOVEN = this.oParentObject.w_MDSCONTI +this.oParentObject.w_MDSTOPRO+ this.w_TOT_RIP
        this.w_PADRE.SetRow(this.w_FIRST)     
        this.w_SCOVEN = this.oParentObject.w_MDSCOVEN + this.w_SCOVEN
        this.w_PADRE.Set("w_MDSCOVEN" , this.w_SCOVEN)     
      endif
      this.w_ARTDES = IIF(NOT EMPTY(g_ARTDES),g_ARTDES,SPACE(20))
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARARTPOS"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARTDES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARARTPOS;
          from (i_cTable) where;
              ARCODART = this.w_ARTDES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ARTPOS = NVL(cp_ToDate(_read_.ARARTPOS),cp_NullValue(_read_.ARARTPOS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_ARTDES) OR this.w_ARTPOS<>"S"
        if EMPTY(this.w_ARTDES) 
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg="Deve essere impostato obbligatoriamente l'articolo per riferimenti nelle contropartite di vendita!"
        else
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg="L'articolo per riferimenti nelle contropartite di vendita deve essere un servizio P.O.S.!"
        endif
        * --- Mi riposiziono sul record di posizionamento iniziale
        this.w_PADRE.RePos()     
        i_retcode = 'stop'
        return
      endif
      * --- Aggiungo una riga in append
      if !this.w_GIAINS
        this.w_PADRE.AddRow()     
        this.w_GIAINS = .T.
        this.oParentObject.w_MDCODICE = this.w_ARTDES
        this.oParentObject.w_MDCODART = this.w_ARTDES
        if this.oParentObject.w_MDFLFOSC="S"
          this.oParentObject.w_MDDESART = ah_Msgformat("Sc. globali %1", IIF(NOT EMPTY(this.oParentObject.w_MDSCONTI), ALLTRIM(STR(cp_ROUND(this.oParentObject.w_MDSCONTI,g_PERPVL),18,g_PERPVL)),"") + " " + this.w_SIMVAL)
        else
          this.oParentObject.w_MDDESART = ah_Msgformat("Sc. globali %1", IIF(NOT EMPTY(this.oParentObject.w_MDSCOCL1), STR(this.oParentObject.w_MDSCOCL1,6,2)+"%","") + IIF(NOT EMPTY(this.oParentObject.w_MDSCOCL2), STR(this.oParentObject.w_MDSCOCL2,6,2)+"%","") + IIF(NOT EMPTY(this.oParentObject.w_MDSCOPAG), STR(this.oParentObject.w_MDSCOPAG,6,2)+"%","") )
        endif
        this.oParentObject.w_MDSCOPRO = this.oParentObject.w_MDSCONTI
        this.oParentObject.w_MDQTAMOV = 0
        this.oParentObject.w_MDTIPRIG = "D"
        this.oParentObject.w_MDCODPRO = "#########*"
      endif
      * --- Aggiorno il cursore alle variabili di lavoro
      this.w_PADRE.SaveRow()     
    endif
    if this.oParentObject.w_MDIMPPRE<>0 And Not Empty(this.oParentObject.w_SERFID)
      this.w_CONTA = 0
      this.w_PADRE.Exec_Select("_Test_Prep_"," Count(*) As Conta ", "Not Deleted() and Nvl(t_MDCODPRO,Space(15)) = '#########F'","","","")     
      if Used( "_Test_Prep_" )
         
 Select ( "_Test_Prep_" ) 
 Go Top
        this.w_CONTA = Nvl( _Test_Prep_.Conta , 0 )
         
 Use in ( "_Test_Prep_" )
      endif
      if this.w_CONTA = 0
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODGRU,ARCODREP,ARUNMIS1"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_SERFID);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODGRU,ARCODREP,ARUNMIS1;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_SERFID;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GRUFID = NVL(cp_ToDate(_read_.ARCODGRU),cp_NullValue(_read_.ARCODGRU))
          this.w_REPFID = NVL(cp_ToDate(_read_.ARCODREP),cp_NullValue(_read_.ARCODREP))
          this.w_UMFID = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from REP_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.REP_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.REP_ARTI_idx,2],.t.,this.REP_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "RECODIVA"+;
            " from "+i_cTable+" REP_ARTI where ";
                +"RECODREP = "+cp_ToStrODBC(this.w_REPFID);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            RECODIVA;
            from (i_cTable) where;
                RECODREP = this.w_REPFID;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IVAFID = NVL(cp_ToDate(_read_.RECODIVA),cp_NullValue(_read_.RECODIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Aggiungo una riga in append
        this.w_PADRE.AddRow()     
        this.oParentObject.w_MDCODICE = this.oParentObject.w_SERFID
        this.oParentObject.w_MDCODART = this.oParentObject.w_SERFID
        this.oParentObject.w_MDDESART = ah_Msgformat("Imp. prepagato %1", ALLTRIM(STR(cp_ROUND(this.oParentObject.w_MDIMPPRE,g_PERPVL),18,g_PERPVL)) )
        this.oParentObject.w_MDQTAMOV = 1
        this.oParentObject.w_MDTIPRIG = "F"
        this.oParentObject.w_MDFLOMAG = "X"
        this.oParentObject.w_GRUMER = this.w_GRUFID
        this.oParentObject.w_MDCODREP = this.w_REPFID
        this.oParentObject.w_MDCODPRO = "#########F"
        this.oParentObject.w_MDPREZZO = - this.oParentObject.w_MDIMPPRE
        this.oParentObject.w_MDCODIVA = this.w_IVAFID
        this.oParentObject.w_MDUNIMIS = this.w_UMFID
        this.oParentObject.w_UNMIS1 = this.w_UMFID
        this.oParentObject.w_MDQTAUM1 = 1
        * --- Aggiorno il cursore alle variabili di lavoro
        this.w_PADRE.SaveRow()     
      endif
    endif
    * --- Aggiorno gli oggetti e mi riposiziono
    this.w_PADRE.RePos()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='REP_ARTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
