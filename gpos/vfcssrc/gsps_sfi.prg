* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_sfi                                                        *
*              Visualizza schede Fidelity                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_67]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-23                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_sfi",oParentObject))

* --- Class definition
define class tgsps_sfi as StdForm
  Top    = 3
  Left   = 1

  * --- Standard Properties
  Width  = 661
  Height = 431
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=40458345
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  FID_CARD_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gsps_sfi"
  cComment = "Visualizza schede Fidelity"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_FIDEL = space(20)
  w_DESFID = space(40)
  w_COMPET = space(4)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_SERIALE = space(10)
  w_TIPO = space(1)
  w_TOTPRE = 0
  w_TOTPUN = 0
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_sfiPag1","gsps_sfi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFIDEL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='FID_CARD'
    this.cWorkTables[2]='ESERCIZI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_FIDEL=space(20)
      .w_DESFID=space(40)
      .w_COMPET=space(4)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_SERIALE=space(10)
      .w_TIPO=space(1)
      .w_TOTPRE=0
      .w_TOTPUN=0
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_FIDEL))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_COMPET = g_CODESE
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_COMPET))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.Zoom.Calculate()
          .DoRTCalc(5,6,.f.)
        .w_SERIALE = .w_Zoom.getVar('MDSERIAL')
        .w_TIPO = .w_Zoom.getVar('TIPO')
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
    this.DoRTCalc(9,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(1,6,.t.)
            .w_SERIALE = .w_Zoom.getVar('MDSERIAL')
            .w_TIPO = .w_Zoom.getVar('TIPO')
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FIDEL
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_lTable = "FID_CARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2], .t., this.FID_CARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FIDEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FID_CARD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FCCODFID like "+cp_ToStrODBC(trim(this.w_FIDEL)+"%");

          i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FCCODFID","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FCCODFID',trim(this.w_FIDEL))
          select FCCODFID,FCDESFID;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FCCODFID into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FIDEL)==trim(_Link_.FCCODFID) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FCDESFID like "+cp_ToStrODBC(trim(this.w_FIDEL)+"%");

            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FCDESFID like "+cp_ToStr(trim(this.w_FIDEL)+"%");

            select FCCODFID,FCDESFID;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FIDEL) and !this.bDontReportError
            deferred_cp_zoom('FID_CARD','*','FCCODFID',cp_AbsName(oSource.parent,'oFIDEL_1_2'),i_cWhere,'',"Fidelity Card",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                     +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',oSource.xKey(1))
            select FCCODFID,FCDESFID;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FIDEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                   +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(this.w_FIDEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',this.w_FIDEL)
            select FCCODFID,FCDESFID;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FIDEL = NVL(_Link_.FCCODFID,space(20))
      this.w_DESFID = NVL(_Link_.FCDESFID,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FIDEL = space(20)
      endif
      this.w_DESFID = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])+'\'+cp_ToStr(_Link_.FCCODFID,1)
      cp_ShowWarn(i_cKey,this.FID_CARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FIDEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPET
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_COMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_COMPET))
          select ESCODAZI,ESCODESE,ESFINESE,ESINIESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCOMPET_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_COMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_COMPET)
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPET = NVL(_Link_.ESCODESE,space(4))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_DATINI = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COMPET = space(4)
      endif
      this.w_DATFIN = ctod("  /  /  ")
      this.w_DATINI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFIDEL_1_2.value==this.w_FIDEL)
      this.oPgFrm.Page1.oPag.oFIDEL_1_2.value=this.w_FIDEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFID_1_3.value==this.w_DESFID)
      this.oPgFrm.Page1.oPag.oDESFID_1_3.value=this.w_DESFID
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPET_1_4.value==this.w_COMPET)
      this.oPgFrm.Page1.oPag.oCOMPET_1_4.value=this.w_COMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_5.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_5.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_6.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_6.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPRE_1_20.value==this.w_TOTPRE)
      this.oPgFrm.Page1.oPag.oTOTPRE_1_20.value=this.w_TOTPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPUN_1_21.value==this.w_TOTPUN)
      this.oPgFrm.Page1.oPag.oTOTPUN_1_21.value=this.w_TOTPUN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FIDEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIDEL_1_2.SetFocus()
            i_bnoObbl = !empty(.w_FIDEL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Carta Fidelity inesistente")
          case   ((empty(.w_DATINI)) or not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DATFIN)) or not(.w_DATINI<=.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � anteriore a quella iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_sfiPag1 as StdContainer
  Width  = 657
  height = 431
  stdWidth  = 657
  stdheight = 431
  resizeXpos=647
  resizeYpos=264
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFIDEL_1_2 as StdField with uid="YQGMQJKUIA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FIDEL", cQueryName = "FIDEL",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Carta Fidelity inesistente",;
    ToolTipText = "Carta Fidelity selezionata",;
    HelpContextID = 44053846,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=84, Top=7, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FID_CARD", oKey_1_1="FCCODFID", oKey_1_2="this.w_FIDEL"

  func oFIDEL_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oFIDEL_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFIDEL_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FID_CARD','*','FCCODFID',cp_AbsName(this.parent,'oFIDEL_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fidelity Card",'',this.parent.oContained
  endproc

  add object oDESFID_1_3 as StdField with uid="NTFWKBHZGE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESFID", cQueryName = "DESFID",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione carta Fidelity",;
    HelpContextID = 108142902,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=235, Top=7, InputMask=replicate('X',40)

  add object oCOMPET_1_4 as StdField with uid="REZYRBBTNW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COMPET", cQueryName = "COMPET",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Competenza selezionata",;
    HelpContextID = 104581926,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=593, Top=6, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_COMPET"

  func oCOMPET_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMPET_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMPET_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCOMPET_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDATINI_1_5 as StdField with uid="STDEZMCELC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di inizio interrogazione",;
    HelpContextID = 197471542,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=84, Top=35

  func oDATINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_6 as StdField with uid="GILLVSXLSD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � anteriore a quella iniziale",;
    ToolTipText = "Data registrazione di fine interrogazione",;
    HelpContextID = 7482678,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=235, Top=35

  func oDATFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc


  add object Zoom as cp_zoombox with uid="CZNPQZKXRC",left=8, top=81, width=651,height=291,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="COR_RISP",cZoomFile="GSPS_SFI",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 137539302


  add object oBtn_1_13 as StdButton with uid="LCZIDVPVYS",left=604, top=33, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la ricerca con le selezioni impostate";
    , HelpContextID = 136463894;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSPS_BFI(this.Parent.oContained,Space(10),"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATINI) AND NOT EMPTY(.w_DATFIN))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="FPXATHRNJC",left=5, top=377, width=48,height=45,;
    CpPicture="BMP\visualizza.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il movimento di vendita/Fidelity associato";
    , HelpContextID = 165118762;
    , caption='\<Movimento';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSPS_BFI(this.Parent.oContained,.w_SERIALE,.w_TIPO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALE,'')))
      endwith
    endif
  endfunc


  add object oObj_1_17 as cp_outputCombo with uid="ZLERXIVPWT",left=165, top=400, width=376,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 137539302


  add object oBtn_1_18 as StdButton with uid="TFLPNIIXHN",left=547, top=377, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 101331238;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) and not empty(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="ZQCYXHBPGO",left=604, top=377, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 33140922;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTOTPRE_1_20 as StdField with uid="YHLAAGIGVM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TOTPRE", cQueryName = "TOTPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale prepagato",;
    HelpContextID = 135019574,;
   bGlobalFont=.t.,;
    Height=21, Width=119, Left=347, Top=373, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oTOTPUN_1_21 as StdField with uid="PUJAKJPDLA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_TOTPUN", cQueryName = "TOTPUN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale punti Fidelity",;
    HelpContextID = 20724790,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=472, Top=373, cSayPict='"999,999"', cGetPict='"999,999"'

  add object oStr_1_8 as StdString with uid="DCIREAPNQV",Visible=.t., Left=33, Top=38,;
    Alignment=1, Width=50, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="CHKICECMSA",Visible=.t., Left=184, Top=37,;
    Alignment=1, Width=46, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FLDCGDBCJO",Visible=.t., Left=1, Top=9,;
    Alignment=1, Width=81, Height=18,;
    Caption="Carta Fidelity:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="QDGYMJNQPH",Visible=.t., Left=507, Top=10,;
    Alignment=1, Width=85, Height=15,;
    Caption="Competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="EUIDGZENRM",Visible=.t., Left=57, Top=407,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ATHIDAZYDQ",Visible=.t., Left=273, Top=374,;
    Alignment=1, Width=71, Height=18,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_sfi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
