* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_afc                                                        *
*              Fidelity Card                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_28]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-04                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_afc"))

* --- Class definition
define class tgsps_afc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 557
  Height = 181+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=59332713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  FID_CARD_IDX = 0
  CLI_VEND_IDX = 0
  cFile = "FID_CARD"
  cKeySelect = "FCCODFID"
  cKeyWhere  = "FCCODFID=this.w_FCCODFID"
  cKeyWhereODBC = '"FCCODFID="+cp_ToStrODBC(this.w_FCCODFID)';

  cKeyWhereODBCqualified = '"FID_CARD.FCCODFID="+cp_ToStrODBC(this.w_FCCODFID)';

  cPrg = "gsps_afc"
  cComment = "Fidelity Card"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODNEG = space(10)
  w_FCCODFID = space(20)
  w_FCDESFID = space(40)
  w_FCDATATT = ctod('  /  /  ')
  w_FCDATSCA = ctod('  /  /  ')
  w_FCCODCLI = space(15)
  w_FCIMPPRE = 0
  w_FCIMPRES = 0
  w_FCDATUAC = ctod('  /  /  ')
  w_FCTOTIMP = 0
  w_FCPUNFID = 0
  w_FCNUMVIS = 0
  w_DESCLI = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'FID_CARD','gsps_afc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_afcPag1","gsps_afc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Fidelity card")
      .Pages(1).HelpContextID = 56281807
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFCCODFID_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CLI_VEND'
    this.cWorkTables[2]='FID_CARD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.FID_CARD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.FID_CARD_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_FCCODFID = NVL(FCCODFID,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from FID_CARD where FCCODFID=KeySet.FCCODFID
    *
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('FID_CARD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "FID_CARD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' FID_CARD '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FCCODFID',this.w_FCCODFID  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODNEG = iif(type('g_CODNEG')<>'U',g_CODNEG,' ')
        .w_DESCLI = space(40)
        .w_FCCODFID = NVL(FCCODFID,space(20))
        .w_FCDESFID = NVL(FCDESFID,space(40))
        .w_FCDATATT = NVL(cp_ToDate(FCDATATT),ctod("  /  /  "))
        .w_FCDATSCA = NVL(cp_ToDate(FCDATSCA),ctod("  /  /  "))
        .w_FCCODCLI = NVL(FCCODCLI,space(15))
          if link_1_7_joined
            this.w_FCCODCLI = NVL(CLCODCLI107,NVL(this.w_FCCODCLI,space(15)))
            this.w_DESCLI = NVL(CLDESCRI107,space(40))
          else
          .link_1_7('Load')
          endif
        .w_FCIMPPRE = NVL(FCIMPPRE,0)
        .w_FCIMPRES = NVL(FCIMPRES,0)
        .w_FCDATUAC = NVL(cp_ToDate(FCDATUAC),ctod("  /  /  "))
        .w_FCTOTIMP = NVL(FCTOTIMP,0)
        .w_FCPUNFID = NVL(FCPUNFID,0)
        .w_FCNUMVIS = NVL(FCNUMVIS,0)
        cp_LoadRecExtFlds(this,'FID_CARD')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODNEG = space(10)
      .w_FCCODFID = space(20)
      .w_FCDESFID = space(40)
      .w_FCDATATT = ctod("  /  /  ")
      .w_FCDATSCA = ctod("  /  /  ")
      .w_FCCODCLI = space(15)
      .w_FCIMPPRE = 0
      .w_FCIMPRES = 0
      .w_FCDATUAC = ctod("  /  /  ")
      .w_FCTOTIMP = 0
      .w_FCPUNFID = 0
      .w_FCNUMVIS = 0
      .w_DESCLI = space(40)
      if .cFunction<>"Filter"
        .w_CODNEG = iif(type('g_CODNEG')<>'U',g_CODNEG,' ')
          .DoRTCalc(2,3,.f.)
        .w_FCDATATT = i_DATSYS
        .DoRTCalc(5,6,.f.)
          if not(empty(.w_FCCODCLI))
          .link_1_7('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'FID_CARD')
    this.DoRTCalc(7,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFCCODFID_1_2.enabled = i_bVal
      .Page1.oPag.oFCDESFID_1_4.enabled = i_bVal
      .Page1.oPag.oFCDATATT_1_5.enabled = i_bVal
      .Page1.oPag.oFCDATSCA_1_6.enabled = i_bVal
      .Page1.oPag.oFCCODCLI_1_7.enabled = i_bVal
      .Page1.oPag.oFCIMPPRE_1_9.enabled = i_bVal
      .Page1.oPag.oFCDATUAC_1_12.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oFCCODFID_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFCCODFID_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'FID_CARD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCCODFID,"FCCODFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCDESFID,"FCDESFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCDATATT,"FCDATATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCDATSCA,"FCDATSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCCODCLI,"FCCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCIMPPRE,"FCIMPPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCIMPRES,"FCIMPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCDATUAC,"FCDATUAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCTOTIMP,"FCTOTIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCPUNFID,"FCPUNFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FCNUMVIS,"FCNUMVIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    i_lTable = "FID_CARD"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.FID_CARD_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSPS_SFC with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.FID_CARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.FID_CARD_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into FID_CARD
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'FID_CARD')
        i_extval=cp_InsertValODBCExtFlds(this,'FID_CARD')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FCCODFID,FCDESFID,FCDATATT,FCDATSCA,FCCODCLI"+;
                  ",FCIMPPRE,FCIMPRES,FCDATUAC,FCTOTIMP,FCPUNFID"+;
                  ",FCNUMVIS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_FCCODFID)+;
                  ","+cp_ToStrODBC(this.w_FCDESFID)+;
                  ","+cp_ToStrODBC(this.w_FCDATATT)+;
                  ","+cp_ToStrODBC(this.w_FCDATSCA)+;
                  ","+cp_ToStrODBCNull(this.w_FCCODCLI)+;
                  ","+cp_ToStrODBC(this.w_FCIMPPRE)+;
                  ","+cp_ToStrODBC(this.w_FCIMPRES)+;
                  ","+cp_ToStrODBC(this.w_FCDATUAC)+;
                  ","+cp_ToStrODBC(this.w_FCTOTIMP)+;
                  ","+cp_ToStrODBC(this.w_FCPUNFID)+;
                  ","+cp_ToStrODBC(this.w_FCNUMVIS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'FID_CARD')
        i_extval=cp_InsertValVFPExtFlds(this,'FID_CARD')
        cp_CheckDeletedKey(i_cTable,0,'FCCODFID',this.w_FCCODFID)
        INSERT INTO (i_cTable);
              (FCCODFID,FCDESFID,FCDATATT,FCDATSCA,FCCODCLI,FCIMPPRE,FCIMPRES,FCDATUAC,FCTOTIMP,FCPUNFID,FCNUMVIS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FCCODFID;
                  ,this.w_FCDESFID;
                  ,this.w_FCDATATT;
                  ,this.w_FCDATSCA;
                  ,this.w_FCCODCLI;
                  ,this.w_FCIMPPRE;
                  ,this.w_FCIMPRES;
                  ,this.w_FCDATUAC;
                  ,this.w_FCTOTIMP;
                  ,this.w_FCPUNFID;
                  ,this.w_FCNUMVIS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.FID_CARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.FID_CARD_IDX,i_nConn)
      *
      * update FID_CARD
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'FID_CARD')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FCDESFID="+cp_ToStrODBC(this.w_FCDESFID)+;
             ",FCDATATT="+cp_ToStrODBC(this.w_FCDATATT)+;
             ",FCDATSCA="+cp_ToStrODBC(this.w_FCDATSCA)+;
             ",FCCODCLI="+cp_ToStrODBCNull(this.w_FCCODCLI)+;
             ",FCIMPPRE="+cp_ToStrODBC(this.w_FCIMPPRE)+;
             ",FCIMPRES="+cp_ToStrODBC(this.w_FCIMPRES)+;
             ",FCDATUAC="+cp_ToStrODBC(this.w_FCDATUAC)+;
             ",FCTOTIMP="+cp_ToStrODBC(this.w_FCTOTIMP)+;
             ",FCPUNFID="+cp_ToStrODBC(this.w_FCPUNFID)+;
             ",FCNUMVIS="+cp_ToStrODBC(this.w_FCNUMVIS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'FID_CARD')
        i_cWhere = cp_PKFox(i_cTable  ,'FCCODFID',this.w_FCCODFID  )
        UPDATE (i_cTable) SET;
              FCDESFID=this.w_FCDESFID;
             ,FCDATATT=this.w_FCDATATT;
             ,FCDATSCA=this.w_FCDATSCA;
             ,FCCODCLI=this.w_FCCODCLI;
             ,FCIMPPRE=this.w_FCIMPPRE;
             ,FCIMPRES=this.w_FCIMPRES;
             ,FCDATUAC=this.w_FCDATUAC;
             ,FCTOTIMP=this.w_FCTOTIMP;
             ,FCPUNFID=this.w_FCPUNFID;
             ,FCNUMVIS=this.w_FCNUMVIS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.FID_CARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.FID_CARD_IDX,i_nConn)
      *
      * delete FID_CARD
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FCCODFID',this.w_FCCODFID  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FCCODCLI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FCCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACV',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_FCCODCLI)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_FCCODCLI))
          select CLCODCLI,CLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FCCODCLI)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FCCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oFCCODCLI_1_7'),i_cWhere,'GSPS_ACV',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FCCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_FCCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_FCCODCLI)
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FCCODCLI = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESCLI = NVL(_Link_.CLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FCCODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FCCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLI_VEND_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.CLCODCLI as CLCODCLI107"+ ",link_1_7.CLDESCRI as CLDESCRI107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on FID_CARD.FCCODCLI=link_1_7.CLCODCLI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and FID_CARD.FCCODCLI=link_1_7.CLCODCLI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFCCODFID_1_2.value==this.w_FCCODFID)
      this.oPgFrm.Page1.oPag.oFCCODFID_1_2.value=this.w_FCCODFID
    endif
    if not(this.oPgFrm.Page1.oPag.oFCDESFID_1_4.value==this.w_FCDESFID)
      this.oPgFrm.Page1.oPag.oFCDESFID_1_4.value=this.w_FCDESFID
    endif
    if not(this.oPgFrm.Page1.oPag.oFCDATATT_1_5.value==this.w_FCDATATT)
      this.oPgFrm.Page1.oPag.oFCDATATT_1_5.value=this.w_FCDATATT
    endif
    if not(this.oPgFrm.Page1.oPag.oFCDATSCA_1_6.value==this.w_FCDATSCA)
      this.oPgFrm.Page1.oPag.oFCDATSCA_1_6.value=this.w_FCDATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oFCCODCLI_1_7.value==this.w_FCCODCLI)
      this.oPgFrm.Page1.oPag.oFCCODCLI_1_7.value=this.w_FCCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oFCIMPPRE_1_9.value==this.w_FCIMPPRE)
      this.oPgFrm.Page1.oPag.oFCIMPPRE_1_9.value=this.w_FCIMPPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oFCIMPRES_1_11.value==this.w_FCIMPRES)
      this.oPgFrm.Page1.oPag.oFCIMPRES_1_11.value=this.w_FCIMPRES
    endif
    if not(this.oPgFrm.Page1.oPag.oFCDATUAC_1_12.value==this.w_FCDATUAC)
      this.oPgFrm.Page1.oPag.oFCDATUAC_1_12.value=this.w_FCDATUAC
    endif
    if not(this.oPgFrm.Page1.oPag.oFCTOTIMP_1_14.value==this.w_FCTOTIMP)
      this.oPgFrm.Page1.oPag.oFCTOTIMP_1_14.value=this.w_FCTOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oFCPUNFID_1_16.value==this.w_FCPUNFID)
      this.oPgFrm.Page1.oPag.oFCPUNFID_1_16.value=this.w_FCPUNFID
    endif
    if not(this.oPgFrm.Page1.oPag.oFCNUMVIS_1_18.value==this.w_FCNUMVIS)
      this.oPgFrm.Page1.oPag.oFCNUMVIS_1_18.value=this.w_FCNUMVIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_23.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_23.value=this.w_DESCLI
    endif
    cp_SetControlsValueExtFlds(this,'FID_CARD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FCCODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFCCODCLI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_FCCODCLI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FCIMPPRE >= .w_FCIMPRES)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFCIMPPRE_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'importo massimo Fidelity deve essere superiore al residuo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_afcPag1 as StdContainer
  Width  = 553
  height = 181
  stdWidth  = 553
  stdheight = 181
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFCCODFID_1_2 as StdField with uid="CPHTXJSZEO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FCCODFID", cQueryName = "FCCODFID",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice Fidelity Card",;
    HelpContextID = 118103962,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=88, Top=11, InputMask=replicate('X',20)

  add object oFCDESFID_1_4 as StdField with uid="CSXFQDKMBB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FCDESFID", cQueryName = "FCDESFID",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione Fidelity Card",;
    HelpContextID = 133181338,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=244, Top=11, InputMask=replicate('X',40)

  add object oFCDATATT_1_5 as StdField with uid="UZHKJUXUCI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FCDATATT", cQueryName = "FCDATATT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data attivazione Fidelity Card",;
    HelpContextID = 50081706,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=244, Top=40

  add object oFCDATSCA_1_6 as StdField with uid="QONWMDCHVB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FCDATSCA", cQueryName = "FCDATSCA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di scadenza Fidelity Card",;
    HelpContextID = 83636119,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=470, Top=40

  add object oFCCODCLI_1_7 as StdField with uid="AMZJPEJYND",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FCCODCLI", cQueryName = "FCCODCLI",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente negozio",;
    HelpContextID = 200663137,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=123, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", cZoomOnZoom="GSPS_ACV", oKey_1_1="CLCODCLI", oKey_1_2="this.w_FCCODCLI"

  proc oFCCODCLI_1_7.mAfter
    with this.Parent.oContained
      .w_FCCODCLI=PSCALZER(.w_FCCODCLI, "XXX")
    endwith
  endproc

  func oFCCODCLI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oFCCODCLI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFCCODCLI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oFCCODCLI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACV',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this.parent.oContained
  endproc
  proc oFCCODCLI_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLCODCLI=this.parent.oContained.w_FCCODCLI
     i_obj.ecpSave()
  endproc

  add object oFCIMPPRE_1_9 as StdField with uid="SVQCDAKCHR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FCIMPPRE", cQueryName = "FCIMPPRE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'importo massimo Fidelity deve essere superiore al residuo",;
    ToolTipText = "Indica l'importo massimo di ricarica della Fidelity",;
    HelpContextID = 29917083,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=123, Top=102, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oFCIMPPRE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FCIMPPRE >= .w_FCIMPRES)
    endwith
    return bRes
  endfunc

  add object oFCIMPRES_1_11 as StdField with uid="JLSUQOBAUO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_FCIMPRES", cQueryName = "FCIMPRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare dell'importo residuo: ricaricare mediante movimento Fidelity",;
    HelpContextID = 63471529,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=123, Top=129, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oFCDATUAC_1_12 as StdField with uid="SCZEGJQEQP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FCDATUAC", cQueryName = "FCDATUAC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultimo acquisto",;
    HelpContextID = 117190553,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=409, Top=129, tabstop=.f.

  add object oFCTOTIMP_1_14 as StdField with uid="PVBKBFXSFM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_FCTOTIMP", cQueryName = "FCTOTIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale degli acquisti effettuati utilizzando la Fidelity Card",;
    HelpContextID = 83152986,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=409, Top=102, cSayPict="v_PV(18)", cGetPict="v_GV(18)", tabstop=.f.

  add object oFCPUNFID_1_16 as StdField with uid="TBOOFCBUXZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FCPUNFID", cQueryName = "FCPUNFID",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Punti Fidelity Card",;
    HelpContextID = 129036186,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=409, Top=156, cSayPict='"999999"', cGetPict='"999999"', tabstop=.f.

  add object oFCNUMVIS_1_18 as StdField with uid="GXVOMMOLOL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FCNUMVIS", cQueryName = "FCNUMVIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contatore visite",;
    HelpContextID = 140456023,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=123, Top=156, cSayPict='"999999"', cGetPict='"999999"', tabstop=.f.

  add object oDESCLI_1_23 as StdField with uid="VYWZUKAXCA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cliente negozio",;
    HelpContextID = 92331722,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=244, Top=75, InputMask=replicate('X',40)

  add object oStr_1_3 as StdString with uid="OFVFFTFJSS",Visible=.t., Left=2, Top=11,;
    Alignment=1, Width=83, Height=19,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="EKFSFDQBIH",Visible=.t., Left=3, Top=76,;
    Alignment=1, Width=118, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="MPRFMPRPNO",Visible=.t., Left=3, Top=103,;
    Alignment=1, Width=118, Height=18,;
    Caption="Importo massimo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="GPXHFDFDFE",Visible=.t., Left=3, Top=130,;
    Alignment=1, Width=118, Height=18,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="GIFBVNQHDN",Visible=.t., Left=275, Top=103,;
    Alignment=1, Width=133, Height=18,;
    Caption="Totale acquisti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="AQMWKLRKFG",Visible=.t., Left=275, Top=157,;
    Alignment=1, Width=133, Height=18,;
    Caption="Punti Fidelity:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="JOUOXOMWKB",Visible=.t., Left=2, Top=157,;
    Alignment=1, Width=119, Height=18,;
    Caption="Contatore visite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="JDLIFEPEGF",Visible=.t., Left=125, Top=40,;
    Alignment=1, Width=118, Height=18,;
    Caption="Data attivazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="NNIKKFKSBV",Visible=.t., Left=350, Top=40,;
    Alignment=1, Width=118, Height=18,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ZFUWTPUJCW",Visible=.t., Left=275, Top=130,;
    Alignment=1, Width=133, Height=18,;
    Caption="Ultimo acquisto:"  ;
  , bGlobalFont=.t.

  add object oBox_1_24 as StdBox with uid="UFICQFGJGA",left=9, top=64, width=537,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_afc','FID_CARD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FCCODFID=FID_CARD.FCCODFID";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
