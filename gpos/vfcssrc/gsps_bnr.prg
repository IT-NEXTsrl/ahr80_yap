* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bnr                                                        *
*              Controllo modifica pos                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-01                                                      *
* Last revis.: 2005-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bnr",oParentObject)
return(i_retval)

define class tgsps_bnr as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_MESS = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo in Modifica di una Vendita POS da GSPS_MVD
    this.oParentObject.w_HASEVENT = .T.
    this.w_PADRE = this.oParentObject
    * --- Eseguo controllo sulle matricole solo se ho gi� generato un documento
    if Not Empty(this.oParentObject.w_MDRIFDOC) Or Not Empty(this.oParentObject.w_MDRIFCOR)
      * --- Select from gsps_bnr
      do vq_exec with 'gsps_bnr',this,'_Curs_gsps_bnr','',.f.,.t.
      if used('_Curs_gsps_bnr')
        select _Curs_gsps_bnr
        locate for 1=1
        do while not(eof())
        do case
          case UPPER(this.oParentObject.w_HASEVCOP)="ECPEDIT"
            this.w_MESS = "Impossibile modificare la vendita poich� � stato generato un documento con le matricole%0La vendita deve essere eliminata e ricaricata"
            this.oParentObject.w_HASEVENT = .F.
          case UPPER(this.oParentObject.w_HASEVCOP)="ECPDELETE"
            if _Curs_gsps_bnr.SALDO>0
              this.w_MESS = "Impossibile cancellare la vendita. Le matricole del documento generato sono state rimovimentate%0Eliminare prima il documento che ha rimovimentato le matricole"
              this.oParentObject.w_HASEVENT = .F.
            endif
        endcase
          select _Curs_gsps_bnr
          continue
        enddo
        use
      endif
      if Not Empty( this.w_MESS )
        ah_ErrorMsg(this.w_MESS,,"")
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gsps_bnr')
      use in _Curs_gsps_bnr
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
