* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_sbd                                                        *
*              Stampa brogliaccio vendite                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_517]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_sbd",oParentObject))

* --- Class definition
define class tgsps_sbd as StdForm
  Top    = 7
  Left   = 35

  * --- Standard Properties
  Width  = 517
  Height = 210+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=107567209
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  ART_ICOL_IDX = 0
  LISTINI_IDX = 0
  CONTI_IDX = 0
  PAG_AMEN_IDX = 0
  VALUTE_IDX = 0
  INVENTAR_IDX = 0
  GRUMERC_IDX = 0
  REP_ARTI_IDX = 0
  MAGAZZIN_IDX = 0
  BUSIUNIT_IDX = 0
  CLI_VEND_IDX = 0
  OPE_RATO_IDX = 0
  MOD_VEND_IDX = 0
  cPrg = "gsps_sbd"
  cComment = "Stampa brogliaccio vendite"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_CODNEG = space(3)
  w_TIPVEN = space(5)
  w_TIPCHI = space(2)
  o_TIPCHI = space(2)
  w_TIPCHI1 = space(2)
  w_CODOPE = 0
  w_DADATA = ctod('  /  /  ')
  w_ADATA = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_ORAFIN = space(2)
  o_ORAFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_SCOINI = 0
  w_SCOFIN = 0
  w_MATSCO = space(10)
  w_DECTOT = 0
  w_TIPOLN = space(1)
  w_DATINV = ctod('  /  /  ')
  w_ONUME = 0
  w_TIPOCON = space(1)
  w_CAOVAL1 = 0
  w_VALESE = space(3)
  w_CAOESE = 0
  w_DESNEG = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_SERIAL = space(10)
  w_DESVEN = space(40)
  w_CLIENTE = space(15)
  w_PAGAM = space(5)
  w_CODMAG = space(5)
  w_GIOLUN = space(1)
  w_GIOMAR = space(1)
  w_GIOMER = space(1)
  w_GIOGIO = space(1)
  w_GIOVEN = space(1)
  w_GIOSAB = space(1)
  w_GIODOM = space(1)
  w_DESPAG = space(30)
  w_DESCLI = space(40)
  w_DESMAG = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_sbdPag1","gsps_sbd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsps_sbdPag2","gsps_sbd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODNEG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='PAG_AMEN'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='INVENTAR'
    this.cWorkTables[8]='GRUMERC'
    this.cWorkTables[9]='REP_ARTI'
    this.cWorkTables[10]='MAGAZZIN'
    this.cWorkTables[11]='BUSIUNIT'
    this.cWorkTables[12]='CLI_VEND'
    this.cWorkTables[13]='OPE_RATO'
    this.cWorkTables[14]='MOD_VEND'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_CODNEG=space(3)
      .w_TIPVEN=space(5)
      .w_TIPCHI=space(2)
      .w_TIPCHI1=space(2)
      .w_CODOPE=0
      .w_DADATA=ctod("  /  /  ")
      .w_ADATA=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_ORAFIN=space(2)
      .w_MINFIN=space(2)
      .w_SCOINI=0
      .w_SCOFIN=0
      .w_MATSCO=space(10)
      .w_DECTOT=0
      .w_TIPOLN=space(1)
      .w_DATINV=ctod("  /  /  ")
      .w_ONUME=0
      .w_TIPOCON=space(1)
      .w_CAOVAL1=0
      .w_VALESE=space(3)
      .w_CAOESE=0
      .w_DESNEG=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_SERIAL=space(10)
      .w_DESVEN=space(40)
      .w_CLIENTE=space(15)
      .w_PAGAM=space(5)
      .w_CODMAG=space(5)
      .w_GIOLUN=space(1)
      .w_GIOMAR=space(1)
      .w_GIOMER=space(1)
      .w_GIOGIO=space(1)
      .w_GIOVEN=space(1)
      .w_GIOSAB=space(1)
      .w_GIODOM=space(1)
      .w_DESPAG=space(30)
      .w_DESCLI=space(40)
      .w_DESMAG=space(30)
        .w_AZIENDA = i_CODAZI
        .w_CODNEG = g_CODNEG
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODNEG))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TIPVEN))
          .link_1_3('Full')
        endif
        .w_TIPCHI = 'TT'
        .w_TIPCHI1 = IIF(.w_TIPCHI='TT',' ',.w_TIPCHI)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODOPE))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_ADATA = i_datsys
        .w_ORAINI = IIF(EMPTY(.w_ORAINI),'00',RIGHT('00'+.w_ORAINI,2))
        .w_MININI = IIF(EMPTY(.w_ORAINI) OR EMPTY(.w_MININI),'00',RIGHT('00'+.w_MININI,2))
        .w_ORAFIN = IIF(EMPTY(.w_ORAFIN),'23',RIGHT('00'+.w_ORAFIN,2))
        .w_MINFIN = IIF(EMPTY(.w_ORAFIN) OR EMPTY(.w_MINFIN),'59',RIGHT('00'+.w_MINFIN,2))
        .w_SCOINI = 0
        .w_SCOFIN = 999999
        .w_MATSCO = space(10)
          .DoRTCalc(16,19,.f.)
        .w_TIPOCON = 'C'
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_VALESE))
          .link_1_24('Full')
        endif
          .DoRTCalc(23,24,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(26,26,.f.)
        .w_SERIAL = SPACE(10)
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_CLIENTE))
          .link_2_1('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_PAGAM))
          .link_2_2('Full')
        endif
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CODMAG))
          .link_2_3('Full')
        endif
        .w_GIOLUN = '2'
        .w_GIOMAR = '3'
        .w_GIOMER = '4'
        .w_GIOGIO = '5'
        .w_GIOVEN = '6'
        .w_GIOSAB = '7'
        .w_GIODOM = '1'
    endwith
    this.DoRTCalc(39,41,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_TIPCHI<>.w_TIPCHI
            .w_TIPCHI1 = IIF(.w_TIPCHI='TT',' ',.w_TIPCHI)
        endif
        .DoRTCalc(6,8,.t.)
        if .o_ORAINI<>.w_ORAINI
            .w_ORAINI = IIF(EMPTY(.w_ORAINI),'00',RIGHT('00'+.w_ORAINI,2))
        endif
        if .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
            .w_MININI = IIF(EMPTY(.w_ORAINI) OR EMPTY(.w_MININI),'00',RIGHT('00'+.w_MININI,2))
        endif
        if .o_ORAFIN<>.w_ORAFIN
            .w_ORAFIN = IIF(EMPTY(.w_ORAFIN),'23',RIGHT('00'+.w_ORAFIN,2))
        endif
        if .o_ORAFIN<>.w_ORAFIN.or. .o_MINFIN<>.w_MINFIN
            .w_MINFIN = IIF(EMPTY(.w_ORAFIN) OR EMPTY(.w_MINFIN),'59',RIGHT('00'+.w_MINFIN,2))
        endif
        .DoRTCalc(13,21,.t.)
          .link_1_24('Full')
        .DoRTCalc(23,26,.t.)
            .w_SERIAL = SPACE(10)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,41,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMININI_1_10.enabled = this.oPgFrm.Page1.oPag.oMININI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_12.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODNEG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_KAN',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODNEG)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_AZIENDA;
                     ,'BUCODICE',trim(this.w_CODNEG))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNEG)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODNEG) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODNEG_1_2'),i_cWhere,'GSPS_KAN',"Negozi azienda",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODNEG);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_AZIENDA;
                       ,'BUCODICE',this.w_CODNEG)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNEG = NVL(_Link_.BUCODICE,space(3))
      this.w_DESNEG = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODNEG = space(3)
      endif
      this.w_DESNEG = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPVEN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
    i_lTable = "MOD_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2], .t., this.MOD_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPVEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_AMV',True,'MOD_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_TIPVEN)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_TIPVEN))
          select MOCODICE,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPVEN)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPVEN) and !this.bDontReportError
            deferred_cp_zoom('MOD_VEND','*','MOCODICE',cp_AbsName(oSource.parent,'oTIPVEN_1_3'),i_cWhere,'GSPS_AMV',"Modalit� di vendita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPVEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_TIPVEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_TIPVEN)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPVEN = NVL(_Link_.MOCODICE,space(5))
      this.w_DESVEN = NVL(_Link_.MODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TIPVEN = space(5)
      endif
      this.w_DESVEN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPVEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODOPE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OPE_RATO_IDX,3]
    i_lTable = "OPE_RATO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2], .t., this.OPE_RATO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_AOP',True,'OPE_RATO')
        if i_nConn<>0
          i_cWhere = " OPCODOPE="+cp_ToStrODBC(this.w_CODOPE);

          i_ret=cp_SQL(i_nConn,"select OPCODOPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OPCODOPE',this.w_CODOPE)
          select OPCODOPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODOPE) and !this.bDontReportError
            deferred_cp_zoom('OPE_RATO','*','OPCODOPE',cp_AbsName(oSource.parent,'oCODOPE_1_6'),i_cWhere,'GSPS_AOP',"Operatori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OPCODOPE";
                     +" from "+i_cTable+" "+i_lTable+" where OPCODOPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OPCODOPE',oSource.xKey(1))
            select OPCODOPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OPCODOPE";
                   +" from "+i_cTable+" "+i_lTable+" where OPCODOPE="+cp_ToStrODBC(this.w_CODOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OPCODOPE',this.w_CODOPE)
            select OPCODOPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODOPE = NVL(_Link_.OPCODOPE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODOPE = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2])+'\'+cp_ToStr(_Link_.OPCODOPE,1)
      cp_ShowWarn(i_cKey,this.OPE_RATO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   +" and VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIENTE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACV',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_CLIENTE))
          select CLCODCLI,CLDESCRI,CLDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIENTE)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStr(trim(this.w_CLIENTE)+"%");

            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIENTE) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oCLIENTE_2_1'),i_cWhere,'GSPS_ACV',"Clienti negozi",'CLIPOS.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_CLIENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_CLIENTE)
            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIENTE = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESCLI = NVL(_Link_.CLDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CLDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIENTE = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
        endif
        this.w_CLIENTE = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PAGAM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAGAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_PAGAM)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_PAGAM))
          select PACODICE,PADTOBSO,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAGAM)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PAGAM) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPAGAM_2_2'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADTOBSO,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAGAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PAGAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PAGAM)
            select PACODICE,PADTOBSO,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAGAM = NVL(_Link_.PACODICE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PAGAM = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_PAGAM = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESPAG = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAGAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_2_3'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODNEG_1_2.value==this.w_CODNEG)
      this.oPgFrm.Page1.oPag.oCODNEG_1_2.value=this.w_CODNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPVEN_1_3.value==this.w_TIPVEN)
      this.oPgFrm.Page1.oPag.oTIPVEN_1_3.value=this.w_TIPVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCHI_1_4.RadioValue()==this.w_TIPCHI)
      this.oPgFrm.Page1.oPag.oTIPCHI_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODOPE_1_6.value==this.w_CODOPE)
      this.oPgFrm.Page1.oPag.oCODOPE_1_6.value=this.w_CODOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_7.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_7.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oADATA_1_8.value==this.w_ADATA)
      this.oPgFrm.Page1.oPag.oADATA_1_8.value=this.w_ADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_9.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_9.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_10.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_10.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_11.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_11.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_12.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_12.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCOINI_1_13.value==this.w_SCOINI)
      this.oPgFrm.Page1.oPag.oSCOINI_1_13.value=this.w_SCOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCOFIN_1_14.value==this.w_SCOFIN)
      this.oPgFrm.Page1.oPag.oSCOFIN_1_14.value=this.w_SCOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMATSCO_1_15.value==this.w_MATSCO)
      this.oPgFrm.Page1.oPag.oMATSCO_1_15.value=this.w_MATSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNEG_1_32.value==this.w_DESNEG)
      this.oPgFrm.Page1.oPag.oDESNEG_1_32.value=this.w_DESNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVEN_1_38.value==this.w_DESVEN)
      this.oPgFrm.Page1.oPag.oDESVEN_1_38.value=this.w_DESVEN
    endif
    if not(this.oPgFrm.Page2.oPag.oCLIENTE_2_1.value==this.w_CLIENTE)
      this.oPgFrm.Page2.oPag.oCLIENTE_2_1.value=this.w_CLIENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGAM_2_2.value==this.w_PAGAM)
      this.oPgFrm.Page2.oPag.oPAGAM_2_2.value=this.w_PAGAM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAG_2_3.value==this.w_CODMAG)
      this.oPgFrm.Page2.oPag.oCODMAG_2_3.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOLUN_2_4.RadioValue()==this.w_GIOLUN)
      this.oPgFrm.Page2.oPag.oGIOLUN_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOMAR_2_5.RadioValue()==this.w_GIOMAR)
      this.oPgFrm.Page2.oPag.oGIOMAR_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOMER_2_6.RadioValue()==this.w_GIOMER)
      this.oPgFrm.Page2.oPag.oGIOMER_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOGIO_2_7.RadioValue()==this.w_GIOGIO)
      this.oPgFrm.Page2.oPag.oGIOGIO_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOVEN_2_8.RadioValue()==this.w_GIOVEN)
      this.oPgFrm.Page2.oPag.oGIOVEN_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOSAB_2_9.RadioValue()==this.w_GIOSAB)
      this.oPgFrm.Page2.oPag.oGIOSAB_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIODOM_2_10.RadioValue()==this.w_GIODOM)
      this.oPgFrm.Page2.oPag.oGIODOM_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPAG_2_12.value==this.w_DESPAG)
      this.oPgFrm.Page2.oPag.oDESPAG_2_12.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_2_13.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_2_13.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAG_2_15.value==this.w_DESMAG)
      this.oPgFrm.Page2.oPag.oDESMAG_2_15.value=this.w_DESMAG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_adata)) OR (.w_adata>=.w_dadata))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATA_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   ((empty(.w_ADATA)) or not((.w_ADATA>=.w_DADATA) OR  ( empty(.w_ADATA))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oADATA_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ADATA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not(VAL(.w_ORAINI)<24 And Not Empty( .w_ORAINI  ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora inizio validit� non corretta specificare da 00 a 23")
          case   not(VAL(.w_MININI)<60)  and (NOT EMPTY(.w_ORAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti inizio validit� non corretti, specificare da 00 a 59")
          case   not(VAL(.w_ORAFIN)<24 And Not Empty(.w_ORAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora fine validit� non corretta, specificare da 00 a 23")
          case   not(VAL(.w_MINFIN)<60)  and (NOT EMPTY(.w_ORAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti fine validit� non corretti, specificare da 00 a 59")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_CLIENTE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLIENTE_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PAGAM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPAGAM_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_sbd
      * --- Disabilita tasto F10
         i_bRes=.f.
      
      
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCHI = this.w_TIPCHI
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_ORAFIN = this.w_ORAFIN
    this.o_MINFIN = this.w_MINFIN
    return

enddefine

* --- Define pages as container
define class tgsps_sbdPag1 as StdContainer
  Width  = 513
  height = 210
  stdWidth  = 513
  stdheight = 210
  resizeXpos=340
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODNEG_1_2 as StdField with uid="ADRORXSOVO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODNEG", cQueryName = "CODNEG",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice negozio selezionato",;
    HelpContextID = 180798682,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=104, Top=14, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSPS_KAN", oKey_1_1="BUCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODNEG"

  func oCODNEG_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNEG_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNEG_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODNEG_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_KAN',"Negozi azienda",'',this.parent.oContained
  endproc
  proc oCODNEG_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSPS_KAN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_AZIENDA
     i_obj.w_BUCODICE=this.parent.oContained.w_CODNEG
     i_obj.ecpSave()
  endproc

  add object oTIPVEN_1_3 as StdField with uid="UTWGDVFAZR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TIPVEN", cQueryName = "TIPVEN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Modalit� di vendita",;
    HelpContextID = 62785994,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=104, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MOD_VEND", cZoomOnZoom="GSPS_AMV", oKey_1_1="MOCODICE", oKey_1_2="this.w_TIPVEN"

  func oTIPVEN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPVEN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPVEN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_VEND','*','MOCODICE',cp_AbsName(this.parent,'oTIPVEN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_AMV',"Modalit� di vendita",'',this.parent.oContained
  endproc
  proc oTIPVEN_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSPS_AMV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_TIPVEN
     i_obj.ecpSave()
  endproc


  add object oTIPCHI_1_4 as StdCombo with uid="FHMQYOWKXT",rtseq=4,rtrep=.f.,left=104,top=69,width=177,height=21;
    , ToolTipText = "Tipo chiusura";
    , HelpContextID = 144771530;
    , cFormVar="w_TIPCHI",RowSource=""+"Tutti,"+"Nessuna stampa,"+"Emissione scontrino,"+"Brogliaccio di vendita,"+"Ricevuta fiscale,"+"Fattura immediata,"+"Fattura fiscale,"+"Ricevuta segue fattura,"+"Documento di trasporto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCHI_1_4.RadioValue()
    return(iif(this.value =1,'TT',;
    iif(this.value =2,'NS',;
    iif(this.value =3,'ES',;
    iif(this.value =4,'BV',;
    iif(this.value =5,'RF',;
    iif(this.value =6,'FI',;
    iif(this.value =7,'FF',;
    iif(this.value =8,'RS',;
    iif(this.value =9,'DT',;
    space(2)))))))))))
  endfunc
  func oTIPCHI_1_4.GetRadio()
    this.Parent.oContained.w_TIPCHI = this.RadioValue()
    return .t.
  endfunc

  func oTIPCHI_1_4.SetRadio()
    this.Parent.oContained.w_TIPCHI=trim(this.Parent.oContained.w_TIPCHI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCHI=='TT',1,;
      iif(this.Parent.oContained.w_TIPCHI=='NS',2,;
      iif(this.Parent.oContained.w_TIPCHI=='ES',3,;
      iif(this.Parent.oContained.w_TIPCHI=='BV',4,;
      iif(this.Parent.oContained.w_TIPCHI=='RF',5,;
      iif(this.Parent.oContained.w_TIPCHI=='FI',6,;
      iif(this.Parent.oContained.w_TIPCHI=='FF',7,;
      iif(this.Parent.oContained.w_TIPCHI=='RS',8,;
      iif(this.Parent.oContained.w_TIPCHI=='DT',9,;
      0)))))))))
  endfunc

  add object oCODOPE_1_6 as StdField with uid="CXTSJGBOLH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODOPE", cQueryName = "CODOPE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore selezionato",;
    HelpContextID = 202753242,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=448, Top=69, bHasZoom = .t. , cLinkFile="OPE_RATO", cZoomOnZoom="GSPS_AOP", oKey_1_1="OPCODOPE", oKey_1_2="this.w_CODOPE"

  func oCODOPE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODOPE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODOPE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OPE_RATO','*','OPCODOPE',cp_AbsName(this.parent,'oCODOPE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_AOP',"Operatori",'',this.parent.oContained
  endproc
  proc oCODOPE_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSPS_AOP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OPCODOPE=this.parent.oContained.w_CODOPE
     i_obj.ecpSave()
  endproc

  add object oDADATA_1_7 as StdField with uid="KSMMEHAABE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data di inizio selezione delle vendite da considerare",;
    HelpContextID = 266588874,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=96

  func oDADATA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_adata)) OR (.w_adata>=.w_dadata))
    endwith
    return bRes
  endfunc

  add object oADATA_1_8 as StdField with uid="TMLQZZAHGL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ADATA", cQueryName = "ADATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data di fine selezione delle vendite da considerare",;
    HelpContextID = 33619962,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=123

  func oADATA_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ADATA>=.w_DADATA) OR  ( empty(.w_ADATA)))
    endwith
    return bRes
  endfunc

  add object oORAINI_1_9 as StdField with uid="DDNWXMUKSP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora inizio validit� non corretta specificare da 00 a 23",;
    ToolTipText = "Ora vendita inizio selezione",;
    HelpContextID = 138146074,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=260, Top=96, InputMask=replicate('X',2)

  func oORAINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI)<24 And Not Empty( .w_ORAINI  ))
    endwith
    return bRes
  endfunc

  add object oMININI_1_10 as StdField with uid="BDZMDUHVRY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti inizio validit� non corretti, specificare da 00 a 59",;
    ToolTipText = "Minuti vendita inizio selezione",;
    HelpContextID = 138095162,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=296, Top=96, InputMask=replicate('X',2)

  func oMININI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ORAINI))
    endwith
   endif
  endfunc

  func oMININI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI)<60)
    endwith
    return bRes
  endfunc

  add object oORAFIN_1_11 as StdField with uid="HIKYATJZHB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora fine validit� non corretta, specificare da 00 a 23",;
    ToolTipText = "Ora vendita fine selezione",;
    HelpContextID = 59699482,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=260, Top=123, InputMask=replicate('X',2)

  func oORAFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN)<24 And Not Empty(.w_ORAFIN))
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_12 as StdField with uid="PJAPJOVQDA",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti fine validit� non corretti, specificare da 00 a 59",;
    ToolTipText = "Minuti vendita fine selezione",;
    HelpContextID = 59648570,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=296, Top=123, InputMask=replicate('X',2)

  func oMINFIN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ORAFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN)<60)
    endwith
    return bRes
  endfunc

  add object oSCOINI_1_13 as StdField with uid="GXDKBEINAA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SCOINI", cQueryName = "SCOINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero scontrino di inizio selezione",;
    HelpContextID = 138092506,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=448, Top=96

  add object oSCOFIN_1_14 as StdField with uid="RNPMJTDVII",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SCOFIN", cQueryName = "SCOFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero scontrino di fine selezione",;
    HelpContextID = 59645914,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=448, Top=125

  add object oMATSCO_1_15 as StdField with uid="TQVHOCAABU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MATSCO", cQueryName = "MATSCO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Matricola cassa",;
    HelpContextID = 48288314,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=152, InputMask=replicate('X',10)


  add object oBtn_1_26 as StdButton with uid="XAOFRYAIID",left=404, top=161, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 107538458;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSPS_BBD(this.Parent.oContained,.w_SERIAL,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_27 as StdButton with uid="KYQESPJULE",left=460, top=161, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 100249786;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESNEG_1_32 as StdField with uid="JWGIIKSIAV",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESNEG", cQueryName = "DESNEG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione negozio",;
    HelpContextID = 180739786,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=157, Top=14, InputMask=replicate('X',40)

  add object oDESVEN_1_38 as StdField with uid="TDOAMIKMZT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESVEN", cQueryName = "DESVEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione modalit� di vendita",;
    HelpContextID = 62774986,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=157, Top=41, InputMask=replicate('X',40)

  add object oStr_1_16 as StdString with uid="IOVTSCSLRF",Visible=.t., Left=5, Top=97,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ICFOIHRQGF",Visible=.t., Left=5, Top=124,;
    Alignment=1, Width=99, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ZJNBAGKORH",Visible=.t., Left=195, Top=97,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="HCCEZDLRZY",Visible=.t., Left=195, Top=124,;
    Alignment=1, Width=62, Height=18,;
    Caption="A ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="OBFHHCGQQV",Visible=.t., Left=287, Top=99,;
    Alignment=1, Width=10, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="MKODBEZTPZ",Visible=.t., Left=287, Top=125,;
    Alignment=1, Width=10, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="BSEDNZTUNR",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=99, Height=18,;
    Caption="Negozio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="JTMFGGQVQN",Visible=.t., Left=356, Top=69,;
    Alignment=1, Width=89, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="RNBQCYGXWX",Visible=.t., Left=5, Top=42,;
    Alignment=1, Width=99, Height=18,;
    Caption="Vendita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="OSYUHCZWIA",Visible=.t., Left=356, Top=97,;
    Alignment=1, Width=89, Height=18,;
    Caption="Da scontrino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="BFRICPZOCG",Visible=.t., Left=356, Top=126,;
    Alignment=1, Width=89, Height=18,;
    Caption="A scontrino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="KXRCAOBAQN",Visible=.t., Left=5, Top=153,;
    Alignment=1, Width=99, Height=18,;
    Caption="Matricola cassa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="BPFPXKIDIY",Visible=.t., Left=5, Top=69,;
    Alignment=1, Width=99, Height=18,;
    Caption="Chiusura:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsps_sbdPag2 as StdContainer
  Width  = 513
  height = 210
  stdWidth  = 513
  stdheight = 210
  resizeXpos=462
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLIENTE_2_1 as StdField with uid="DNZWFJCDJL",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CLIENTE", cQueryName = "CLIENTE",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice cliente � inesistente oppure obsoleto",;
    ToolTipText = "Codice cliente selezionato",;
    HelpContextID = 46172198,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=81, Top=16, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", cZoomOnZoom="GSPS_ACV", oKey_1_1="CLCODCLI", oKey_1_2="this.w_CLIENTE"

  func oCLIENTE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIENTE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIENTE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oCLIENTE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACV',"Clienti negozi",'CLIPOS.CLI_VEND_VZM',this.parent.oContained
  endproc
  proc oCLIENTE_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLCODCLI=this.parent.oContained.w_CLIENTE
     i_obj.ecpSave()
  endproc

  add object oPAGAM_2_2 as StdField with uid="TGIYBOLWES",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PAGAM", cQueryName = "PAGAM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento selezionato",;
    HelpContextID = 22258186,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=81, Top=43, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_PAGAM"

  func oPAGAM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAGAM_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAGAM_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPAGAM_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPAGAM_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_PAGAM
     i_obj.ecpSave()
  endproc

  add object oCODMAG_2_3 as StdField with uid="ARVFREAYMP",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino selezionato",;
    HelpContextID = 185058522,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=81, Top=70, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oGIOLUN_2_4 as StdCheck with uid="YLFBHHZYJJ",rtseq=32,rtrep=.f.,left=89, top=102, caption="Luned�",;
    ToolTipText = "Vendita eseguita il luned�",;
    HelpContextID = 46668442,;
    cFormVar="w_GIOLUN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOLUN_2_4.RadioValue()
    return(iif(this.value =1,'2',;
    ' '))
  endfunc
  func oGIOLUN_2_4.GetRadio()
    this.Parent.oContained.w_GIOLUN = this.RadioValue()
    return .t.
  endfunc

  func oGIOLUN_2_4.SetRadio()
    this.Parent.oContained.w_GIOLUN=trim(this.Parent.oContained.w_GIOLUN)
    this.value = ;
      iif(this.Parent.oContained.w_GIOLUN=='2',1,;
      0)
  endfunc

  add object oGIOMAR_2_5 as StdCheck with uid="BPOTZCVRPB",rtseq=33,rtrep=.f.,left=186, top=102, caption="Marted�",;
    ToolTipText = "Vendita eseguita il marted�",;
    HelpContextID = 465562,;
    cFormVar="w_GIOMAR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOMAR_2_5.RadioValue()
    return(iif(this.value =1,'3',;
    ' '))
  endfunc
  func oGIOMAR_2_5.GetRadio()
    this.Parent.oContained.w_GIOMAR = this.RadioValue()
    return .t.
  endfunc

  func oGIOMAR_2_5.SetRadio()
    this.Parent.oContained.w_GIOMAR=trim(this.Parent.oContained.w_GIOMAR)
    this.value = ;
      iif(this.Parent.oContained.w_GIOMAR=='3',1,;
      0)
  endfunc

  add object oGIOMER_2_6 as StdCheck with uid="PLMVJQUSYV",rtseq=34,rtrep=.f.,left=282, top=102, caption="Mercoled�",;
    ToolTipText = "Vendita eseguita il mercoled�",;
    HelpContextID = 3728742,;
    cFormVar="w_GIOMER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOMER_2_6.RadioValue()
    return(iif(this.value =1,'4',;
    ' '))
  endfunc
  func oGIOMER_2_6.GetRadio()
    this.Parent.oContained.w_GIOMER = this.RadioValue()
    return .t.
  endfunc

  func oGIOMER_2_6.SetRadio()
    this.Parent.oContained.w_GIOMER=trim(this.Parent.oContained.w_GIOMER)
    this.value = ;
      iif(this.Parent.oContained.w_GIOMER=='4',1,;
      0)
  endfunc

  add object oGIOGIO_2_7 as StdCheck with uid="VBNKFXJXZF",rtseq=35,rtrep=.f.,left=89, top=133, caption="Gioved�",;
    ToolTipText = "Vendita eseguita il gioved�",;
    HelpContextID = 42801818,;
    cFormVar="w_GIOGIO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOGIO_2_7.RadioValue()
    return(iif(this.value =1,'5',;
    ' '))
  endfunc
  func oGIOGIO_2_7.GetRadio()
    this.Parent.oContained.w_GIOGIO = this.RadioValue()
    return .t.
  endfunc

  func oGIOGIO_2_7.SetRadio()
    this.Parent.oContained.w_GIOGIO=trim(this.Parent.oContained.w_GIOGIO)
    this.value = ;
      iif(this.Parent.oContained.w_GIOGIO=='5',1,;
      0)
  endfunc

  add object oGIOVEN_2_8 as StdCheck with uid="EPAPSYXMPA",rtseq=36,rtrep=.f.,left=186, top=133, caption="Venerd�",;
    ToolTipText = "Vendita eseguita il venerd�",;
    HelpContextID = 62790298,;
    cFormVar="w_GIOVEN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOVEN_2_8.RadioValue()
    return(iif(this.value =1,'6',;
    ' '))
  endfunc
  func oGIOVEN_2_8.GetRadio()
    this.Parent.oContained.w_GIOVEN = this.RadioValue()
    return .t.
  endfunc

  func oGIOVEN_2_8.SetRadio()
    this.Parent.oContained.w_GIOVEN=trim(this.Parent.oContained.w_GIOVEN)
    this.value = ;
      iif(this.Parent.oContained.w_GIOVEN=='6',1,;
      0)
  endfunc

  add object oGIOSAB_2_9 as StdCheck with uid="PEJMLNCTOC",rtseq=37,rtrep=.f.,left=282, top=133, caption="Sabato",;
    ToolTipText = "Vendita eseguita il sabato",;
    HelpContextID = 72346,;
    cFormVar="w_GIOSAB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOSAB_2_9.RadioValue()
    return(iif(this.value =1,'7',;
    ' '))
  endfunc
  func oGIOSAB_2_9.GetRadio()
    this.Parent.oContained.w_GIOSAB = this.RadioValue()
    return .t.
  endfunc

  func oGIOSAB_2_9.SetRadio()
    this.Parent.oContained.w_GIOSAB=trim(this.Parent.oContained.w_GIOSAB)
    this.value = ;
      iif(this.Parent.oContained.w_GIOSAB=='7',1,;
      0)
  endfunc

  add object oGIODOM_2_10 as StdCheck with uid="IDKNYMLIXI",rtseq=38,rtrep=.f.,left=386, top=133, caption="Domenica",;
    ToolTipText = "Vendita eseguita domenica",;
    HelpContextID = 70261402,;
    cFormVar="w_GIODOM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIODOM_2_10.RadioValue()
    return(iif(this.value =1,'1',;
    ' '))
  endfunc
  func oGIODOM_2_10.GetRadio()
    this.Parent.oContained.w_GIODOM = this.RadioValue()
    return .t.
  endfunc

  func oGIODOM_2_10.SetRadio()
    this.Parent.oContained.w_GIODOM=trim(this.Parent.oContained.w_GIODOM)
    this.value = ;
      iif(this.Parent.oContained.w_GIODOM=='1',1,;
      0)
  endfunc

  add object oDESPAG_2_12 as StdField with uid="CIURVSUYUU",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione pagamento",;
    HelpContextID = 184803018,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=212, Top=43, InputMask=replicate('X',30)

  add object oDESCLI_2_13 as StdField with uid="TWSXVINGFB",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cliente pos",;
    HelpContextID = 140566218,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=212, Top=16, InputMask=replicate('X',40)

  add object oDESMAG_2_15 as StdField with uid="EJUWTMEEDX",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione magazzino",;
    HelpContextID = 184999626,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=212, Top=70, InputMask=replicate('X',30)

  add object oStr_2_11 as StdString with uid="ZWRRXYXROA",Visible=.t., Left=5, Top=44,;
    Alignment=1, Width=76, Height=15,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="GLXRFLHNHN",Visible=.t., Left=5, Top=17,;
    Alignment=1, Width=76, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="MOTKUDJLHM",Visible=.t., Left=5, Top=71,;
    Alignment=1, Width=76, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="UQTLRKORWG",Visible=.t., Left=5, Top=99,;
    Alignment=1, Width=76, Height=18,;
    Caption="Giorno:"  ;
  , bGlobalFont=.t.

  add object oBox_2_17 as StdBox with uid="EYWJGVWPHO",left=83, top=98, width=421,height=59
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_sbd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
