* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_aca                                                        *
*              Carte di credito                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_17]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2009-12-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_aca"))

* --- Class definition
define class tgsps_aca as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 574
  Height = 107+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-29"
  HelpContextID=158771095
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  CAR_CRED_IDX = 0
  CONTI_IDX = 0
  cFile = "CAR_CRED"
  cKeySelect = "CCCODICE"
  cKeyWhere  = "CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyWhereODBCqualified = '"CAR_CRED.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cPrg = "gsps_aca"
  cComment = "Carte di credito"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_CCCONCAR = space(20)
  w_DESCON = space(40)
  w_TIPCON = space(1)
  w_DTOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAR_CRED','gsps_aca')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_acaPag1","gsps_aca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Carta di credito / finanziamento")
      .Pages(1).HelpContextID = 110467717
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAR_CRED'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAR_CRED_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAR_CRED_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CCCODICE = NVL(CCCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAR_CRED where CCCODICE=KeySet.CCCODICE
    *
    i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAR_CRED')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAR_CRED.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAR_CRED '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCON = space(40)
        .w_DTOBSO = ctod("  /  /  ")
        .w_CCCODICE = NVL(CCCODICE,space(5))
        .w_CCDESCRI = NVL(CCDESCRI,space(40))
        .w_CCCONCAR = NVL(CCCONCAR,space(20))
          .link_1_5('Load')
        .w_TIPCON = 'G'
        cp_LoadRecExtFlds(this,'CAR_CRED')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CCCODICE = space(5)
      .w_CCDESCRI = space(40)
      .w_CCCONCAR = space(20)
      .w_DESCON = space(40)
      .w_TIPCON = space(1)
      .w_DTOBSO = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_CCCONCAR))
          .link_1_5('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_TIPCON = 'G'
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAR_CRED')
    this.DoRTCalc(6,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCCDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCCCONCAR_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCCCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCCCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAR_CRED',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESCRI,"CCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCONCAR,"CCCONCAR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
    i_lTable = "CAR_CRED"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAR_CRED_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("..\GPOS\EXE\QUERY\GSPS_ACA.VQR,..\GPOS\EXE\QUERY\GSPS_ACA.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAR_CRED_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAR_CRED
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAR_CRED')
        i_extval=cp_InsertValODBCExtFlds(this,'CAR_CRED')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CCCODICE,CCDESCRI,CCCONCAR "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CCCODICE)+;
                  ","+cp_ToStrODBC(this.w_CCDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_CCCONCAR)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAR_CRED')
        i_extval=cp_InsertValVFPExtFlds(this,'CAR_CRED')
        cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable);
              (CCCODICE,CCDESCRI,CCCONCAR  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CCCODICE;
                  ,this.w_CCDESCRI;
                  ,this.w_CCCONCAR;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAR_CRED_IDX,i_nConn)
      *
      * update CAR_CRED
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAR_CRED')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
             ",CCCONCAR="+cp_ToStrODBCNull(this.w_CCCONCAR)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAR_CRED')
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        UPDATE (i_cTable) SET;
              CCDESCRI=this.w_CCDESCRI;
             ,CCCONCAR=this.w_CCCONCAR;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAR_CRED_IDX,i_nConn)
      *
      * delete CAR_CRED
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_TIPCON = 'G'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCCONCAR
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCONCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CCCONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CCCONCAR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCONCAR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CCCONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CCCONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCCONCAR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCCCONCAR_1_5'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCONCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CCCONCAR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CCCONCAR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCONCAR = NVL(_Link_.ANCODICE,space(20))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CCCONCAR = space(20)
      endif
      this.w_DESCON = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOBSO Or Empty(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_CCCONCAR = space(20)
        this.w_DESCON = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCONCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_1.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_1.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCONCAR_1_5.value==this.w_CCCONCAR)
      this.oPgFrm.Page1.oPag.oCCCONCAR_1_5.value=this.w_CCCONCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_7.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_7.value=this.w_DESCON
    endif
    cp_SetControlsValueExtFlds(this,'CAR_CRED')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(i_DATSYS < .w_DTOBSO Or Empty(.w_DTOBSO))  and not(empty(.w_CCCONCAR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCONCAR_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_acaPag1 as StdContainer
  Width  = 570
  height = 107
  stdWidth  = 570
  stdheight = 107
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_1 as StdField with uid="KCIWXUXDOI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice carta di credito/finanziamento",;
    HelpContextID = 150331541,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=103, Top=19, InputMask=replicate('X',5)

  add object oCCDESCRI_1_3 as StdField with uid="YEXYLOFWIH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 235917457,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=103, Top=47, InputMask=replicate('X',40)

  add object oCCCONCAR_1_5 as StdField with uid="HDCOUQUJLS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCCONCAR", cQueryName = "CCCONCAR",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata nella contabilizzazione del documento generato contenente questo tipo di pagamento",;
    HelpContextID = 240509064,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=103, Top=78, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CCCONCAR"

  func oCCCONCAR_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCONCAR_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCONCAR_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCCCONCAR_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCCCONCAR_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CCCONCAR
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_7 as StdField with uid="KBYQEKCVTZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55631562,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=259, Top=78, InputMask=replicate('X',40)

  add object oStr_1_2 as StdString with uid="JFZPZYDDDJ",Visible=.t., Left=10, Top=23,;
    Alignment=1, Width=93, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="JUQRNOXKQF",Visible=.t., Left=12, Top=51,;
    Alignment=1, Width=91, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VVDUQYNLYE",Visible=.t., Left=16, Top=80,;
    Alignment=1, Width=87, Height=18,;
    Caption="Contropartita:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_aca','CAR_CRED','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCCODICE=CAR_CRED.CCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
