* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bgd                                                        *
*              Generazione documenti da vendita negozio                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_388]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-11                                                      *
* Last revis.: 2013-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bgd",oParentObject)
return(i_retval)

define class tgsps_bgd as StdBatch
  * --- Local variables
  w_MVFLSCOM = space(1)
  w_COCINC = space(15)
  DR = space(10)
  w_DBRK = space(10)
  w_OBRK = space(10)
  w_DBRK2 = space(10)
  w_OBRK2 = space(10)
  w_FLRAGG = space(1)
  w_MESS = space(10)
  w_FLANAL = space(1)
  w_FLELAN = space(1)
  w_CODNEG = space(3)
  w_FLFAFI = space(1)
  w_MATRSN = space(1)
  w_TOTPRE = 0
  w_TOTCON = 0
  w_TOTASS = 0
  w_TOTCAR = 0
  w_TOTFIN = 0
  w_CONPRE = space(15)
  w_CONCON = space(15)
  w_CONASS = space(15)
  w_CONCAR = space(20)
  w_CONFIN = space(20)
  w_AZFLUNIV = space(1)
  w_BPAGCLI = .f.
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  w_NUMCOR = space(25)
  w_FLERR = .f.
  w_NEWTES = .f.
  w_MVTIPDOC = space(5)
  w_MVANNPRO = space(4)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVFLOFFE = space(1)
  w_MVPRP = space(2)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVPRD = space(2)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTCAMAG = space(5)
  w_MVTIPCON = space(1)
  w_MVANNDOC = space(4)
  w_MVTFRAGG = space(1)
  w_MVCODESE = space(4)
  w_MVFLPROV = space(1)
  w_MVSERIAL = space(10)
  w_MVCODUTE = 0
  w_MVCAUCON = space(5)
  w_MVFLACCO = space(1)
  w_MVNUMREG = 0
  w_NUMSCO = 0
  w_MVFLINTE = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVDATPLA = ctod("  /  /  ")
  w_MVCODMAG = space(5)
  w_MVLOTMAG = space(5)
  w_MVDATEVA = ctod("  /  /  ")
  w_MVASPEST = space(30)
  w_MVDATTRA = ctod("  /  /  ")
  w_MVORATRA = space(2)
  w_MVMINTRA = space(2)
  w_CPROWORD = 0
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCLADOC = space(2)
  w_MVCONCON = space(1)
  w_MVCONTRA = space(15)
  w_MVSERRIF = space(10)
  w_MV__NOTE = space(10)
  w_MVIVAINC = space(5)
  w_MVSPEINC = 0
  w_MVSCOVEN = 0
  w_MVNUMRIF = 0
  w_MVCONIND = space(15)
  w_MVROWRIF = 0
  w_MVFLRINC = space(1)
  w_MVFLSALD = space(1)
  w_CPROWNUM = 0
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVSPEIMB = 0
  w_MVFLCASC = space(1)
  w_MVFLRISE = space(1)
  w_MVFLORDI = space(1)
  w_MVCODICE = space(20)
  w_MVQTAMOV = 0
  w_MVTINCOM = ctod("  /  /  ")
  w_MVIVAIMB = space(5)
  w_MVFLIMPE = space(1)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MDIMPABB = 0
  w_MVFLRIMB = space(1)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVACCSUC = 0
  w_MVVOCCEN = space(15)
  w_MVFLELAN = space(1)
  w_MVDESART = space(40)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVCODCEN = space(15)
  w_MVDESSUP = space(10)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVCODCOM = space(15)
  w_MVUNIMIS = space(3)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVCATCON = space(5)
  w_MVSCONT4 = 0
  w_MVMOLSUP = 0
  w_MVIVABOL = space(5)
  w_MVFLSCOR = space(1)
  w_MVCODCLA = space(3)
  w_MVFLOMAG = space(1)
  w_MVSCONTI = 0
  w_MVCODCON = space(15)
  w_MVCODIVA = space(5)
  w_MVCODIVE = space(5)
  w_MVCODPAG = space(5)
  w_MVKEYSAL = space(20)
  w_MVVALNAZ = space(3)
  w_MVCODAGE = space(5)
  w_MVCODAG2 = space(5)
  w_MVCODBAN = space(10)
  w_MVCODBA2 = space(10)
  w_MVCODPOR = space(1)
  w_MVCAOVAL = 0
  w_MVTCONTR = space(15)
  w_MVCODVAL = space(3)
  w_MVCODVET = space(5)
  w_MVVALRIG = 0
  w_MVFLELGM = space(1)
  w_MVPESNET = 0
  w_MVCODSPE = space(3)
  w_MVIMPSCO = 0
  w_MVRIFDIC = space(10)
  w_MVFLTRAS = space(1)
  w_MVTIPATT = space(1)
  w_MVVALMAG = 0
  w_MVPESNET = 0
  w_MVTCOLIS = space(5)
  w_MVNOMENC = space(8)
  w_MVCODATT = space(15)
  w_MVIMPNAZ = 0
  w_MVVALULT = 0
  w_MVACIVA1 = space(5)
  w_MVAIMPN1 = 0
  w_MVAFLOM1 = space(1)
  w_MVAIMPS1 = 0
  w_MVUMSUPP = space(3)
  w_MVACIVA2 = space(5)
  w_MVAIMPN2 = 0
  w_MVAFLOM2 = space(1)
  w_MVAIMPS2 = 0
  w_MVFLVEAC = space(1)
  w_MVFLULPV = space(1)
  w_MVACIVA3 = space(5)
  w_MVAIMPN3 = 0
  w_MVAFLOM3 = space(1)
  w_MVAIMPS3 = 0
  w_MVCODDES = space(5)
  w_MVACIVA4 = space(5)
  w_MVAIMPN4 = 0
  w_CAOCON = 0
  w_VALCON = space(3)
  w_MVAFLOM4 = space(1)
  w_MVAIMPS4 = 0
  w_MVIMPARR = 0
  w_MVACIVA5 = space(5)
  w_MVAIMPN5 = 0
  w_MVAFLOM5 = space(1)
  w_MVAIMPS5 = 0
  w_MVACIVA6 = space(5)
  w_MVAIMPN6 = 0
  w_MVAFLOM6 = space(1)
  w_MVAIMPS6 = 0
  w_MVACCPRE = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVIMPACC = 0
  w_MVVOCCOS = space(15)
  w_MVRITPRE = 0
  w_MVFLFOSC = space(1)
  w_MVNOTAGG = space(40)
  w_CAOVAL = 0
  w_BOLARR = 0
  w_PERIVA = 0
  w_PEIINC = 0
  w_MFLCASC = space(1)
  w_IMPARR = 0
  w_BOLMIN = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_MFLORDI = space(1)
  w_RSIMPRAT = 0
  w_TOTMERCE = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_MFLIMPE = space(1)
  w_DECTOT = 0
  w_TOTALE = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_MFLRISE = space(1)
  w_BOLESE = 0
  w_TOTIMPON = 0
  w_GIORN1 = 0
  w_FLINCA = space(1)
  w_PEITRA = 0
  w_MFLELGM = space(1)
  w_BOLSUP = 0
  w_TOTIMPOS = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_MFLCOMM = space(1)
  w_BOLCAM = 0
  w_TOTFATTU = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_PERIVE = 0
  w_CAONAZ = 0
  w_CLBOLFAT = space(1)
  w_BOLIVE = space(1)
  w_RSMODPAG = space(10)
  w_RSNUMRAT = 0
  w_RSDATRAT = ctod("  /  /  ")
  w_CODESE = space(4)
  w_ACCPRE = 0
  w_ORDNUM = 0
  w_CODNAZ = space(3)
  w_APPO = space(10)
  w_DETNUM = 0
  w_FLFOBO = space(1)
  w_UNMIS1 = space(3)
  w_IVALIS = space(1)
  w_UNMIS2 = space(3)
  w_QUALIS = space(1)
  w_UNMIS3 = space(3)
  w_DECUNI = 0
  w_MOLTIP = 0
  w_DATREG = ctod("  /  /  ")
  w_GRUMER = space(5)
  w_QUAN = 0
  w_MOLTI3 = 0
  w_CODVAL = space(3)
  w_QTAUM1 = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_ARRSUP = 0
  w_ACQINT = space(1)
  w_OK_ANALI = .f.
  w_OK_COMM = .f.
  w_SCOLIS = space(1)
  w_FLSERA = space(1)
  w_INDIVE = 0
  w_INDIVA = 0
  w_APPART = space(10)
  w_FLPPRO = space(1)
  w_MDSERIAL = space(10)
  w_FLRESO = space(1)
  w_RFLCASC = space(1)
  w_RFLORDI = space(1)
  w_RFLIMPE = space(1)
  w_RFLRISE = space(1)
  w_RFLELGM = space(1)
  w_RFLCOMM = space(1)
  w_MDTIPCHI = space(2)
  w_MDCODCLI = space(5)
  w_MDTIPDOC = space(5)
  w_MDCODPAG = space(5)
  w_ULTSCA = ctod("  /  /  ")
  w_MDSTOPRO = 0
  w_CODCONF = space(3)
  w_CODCOL = space(5)
  w_FLPACK = space(1)
  w_MVQTACOL = 0
  w_MVQTAPES = 0
  w_MVQTALOR = 0
  w_PZCONF = 0
  w_NUMCOL = 0
  w_MVTIPPRO = space(2)
  w_MVPERPRO = 0
  w_MVIMPPRO = 0
  w_IMPPRO = 0
  w_AGSCOPAG = space(1)
  w_CODPRO = space(15)
  w_DIFFER = 0
  w_OKDATDIV = .f.
  w_TIPREG = space(1)
  w_MV_SEGNO = space(1)
  w_MVIMPCOM = 0
  w_CODAZI = space(5)
  w_CODCEN = space(15)
  w_MVCODLOT = space(20)
  w_MVCODUBI = space(20)
  w_MVFLLOTT = space(1)
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_RECNO = 0
  w_MVTIPPR2 = space(2)
  w_MVPROCAP = 0
  w_ARUTISER = space(1)
  w_ARDATINT = space(1)
  w_DATCOM = ctod("  /  /  ")
  w_GENPOS = space(1)
  w_AZPLADOC = space(1)
  w_PPCALPRO = space(2)
  w_OLDCALPRO = space(2)
  w_PPCALSCO = space(1)
  w_MASSGEN = space(1)
  w_SERPRO = space(10)
  w_PPLISRIF = space(5)
  w_MRESTO = 0
  w_ERESTO = 0
  w_TRESTO = 0
  w_TOTDOC = 0
  w_MINRATA = 0
  w_MSGRET = space(254)
  w_NUMDOC = 0
  w_LOOP = 0
  w_FLINSE = .f.
  w_UPDA = .f.
  w_INSDOC = .f.
  w_MVSERIAL_C = space(10)
  w_MVNUMDOC_C = 0
  w_MVDATDOC_C = ctod("  /  /  ")
  w_MVANNDOC_C = space(4)
  w_MVALFDOC_C = space(10)
  w_MVTIPDOC_C = space(5)
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  DOC_RATE_idx=0
  SALDIART_idx=0
  COR_RISP_idx=0
  CAM_AGAZ_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  LISTINI_idx=0
  TIP_DOCU_idx=0
  KEY_ARTI_idx=0
  DES_DIVE_idx=0
  PAR_VDET_idx=0
  ART_ICOL_idx=0
  AGENTI_idx=0
  PAG_2AME_idx=0
  CAU_CONT_idx=0
  CON_PAGA_idx=0
  TMP_MATR_idx=0
  MOVIMATR_idx=0
  MATR_POS_idx=0
  PAR_PROV_idx=0
  CONTI_idx=0
  CONTROPA_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documenti da Vendita Negozio (da GSPS_BSV, GSPS_BSC, GSPS_BGF)
    * --- w_tipope: Origine - I=Stampa Immediata ; D=Differita ; F=Fattura Fiscale ; G=Fattura Fiscale generata da Corrispettivi
    * --- Generando i document da POS devo sempre considerare disattivo il flag 
    *     Calcola Sconti su Omaggi poich� sul pos gli sconti sono considerati sul totale
    this.w_MVFLSCOM = "N"
    * --- Leggo la contropartita INCASSI da utilizzare come default per la contabilizzazione
    *     degli incassi...
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCOCINC"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCOCINC;
        from (i_cTable) where;
            COCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COCINC = NVL(cp_ToDate(_read_.COCOCINC),cp_NullValue(_read_.COCOCINC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Creazione Tabella Temporanea per Generazione Matricole
    * --- Create temporary table TMP_MATR
    i_nIdx=cp_AddTableDef('TMP_MATR') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\gpos\exe\query\gsps_qtt',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_MATR_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_MVFLPROV = "N"
    this.w_MVVALNAZ = g_PERVAL
    this.w_MVFLVEAC = "V"
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_CODESE = this.w_MVCODESE
    this.w_FLFOBO = " "
    this.w_FLINCA = " "
    this.w_ACQINT = "N"
    this.w_MVDATTRA = cp_CharToDate("  -  -  ")
    this.w_MVORATRA = left(time(),2)
    this.w_MVMINTRA = substr(time(),4,2)
    this.w_MVNOTAGG = " "
    this.w_CODAZI = i_CODAZI
    DIMENSION DR[1000, 9]
    * --- Causale per gestione resi
    this.w_RFLCASC = " "
    this.w_RFLORDI = " "
    this.w_RFLIMPE = " "
    this.w_RFLRISE = " "
    this.w_RFLELGM = " "
    this.w_RFLCOMM = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMFLCOMM"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAURES);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMFLCOMM;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_CAURES;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_RFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_RFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_RFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_RFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.w_RFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      this.w_RFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Legge Informazioni di Riga di Default
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Inizio Aggiornamento
    ah_Msg("Inizio fase di generazione...",.T.)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Drop temporary table TMP_MATR
    i_nIdx=cp_GetTableDefIdx('TMP_MATR')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_MATR')
    endif
    * --- Chiude i Cursori
    USE IN SELECT("GENEORDI")
    USE IN SELECT("GeneApp")
    USE IN SELECT("RifReg")
    USE IN SELECT("AppRagg")
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti 
    * --- Testa il Cambio Documento
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZFLUNIV"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZFLUNIV;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZFLUNIV = NVL(cp_ToDate(_read_.AZFLUNIV),cp_NullValue(_read_.AZFLUNIV))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FLRAGG = "N"
    this.w_DBRK = "##zz##"
    SELECT GENEORDI
    * --- Creo un corrispettivo per ogni vendita negozio se
    *     1) Origine =Differita e  Importo rimanente da pagare a saldo (C./cliente) valorizzato
    *     2) ho flag singolo Corrispettivo e ho intestatario nella vendita negozio
    *     3) ho il flag contabilizz. separata (importo > 3000 euro) nella causale contabile
    GO TOP
    SCAN FOR (this.oParentObject.w_TIPOPE<>"D" OR NVL(FLPAGCLI, "S")="S") AND (Nvl(PAFLCONT,"N")<>"S") and nvl(CCFLCOSE,"N")<>"S"
    REPLACE SERDOC WITH space(10) 
    ENDSCAN
    SELECT * from GENEORDI order by MDCODNEG, MDDATREG, MDTIPDOC, MDCODCLI, MDSCOCL1, ; 
 MDSCOCL2, MDSCOPAG, MDFLFOSC, SERDOC, MDCODMAG into cursor _ord_ NOFILTER
    USE IN GENEORDI
    SELECT from _ord_ * into cursor GENEORDI READWRITE
    USE IN _ORD_
    SELECT GENEORDI
    GO TOP
    SCAN FOR NOT EMPTY(NVL(MDSERIAL," ")) AND NOT EMPTY(NVL(MDTIPDOC," ")) 
    * --- Testa Cambio Documento
    this.w_OBRK = NVL(MDCODNEG, SPACE(3)) + DTOS(CP_TODATE(MDDATREG)) + NVL(MDTIPDOC, SPACE(5)) + NVL(MDCODCLI, SPACE(15))
    this.w_OBRK = this.w_OBRK + STR(NVL(MDSCOCL1,0), 6, 2) + STR(NVL(MDSCOCL2,0), 6, 2) + STR(NVL(MDSCOPAG,0), 6, 2)+NVL(MDFLFOSC, " ")
    * --- Ulteriori Campi di Rottura (se Corrispettivo e Parzialmente Incassato)
    this.w_OBRK = this.w_OBRK + SERDOC + NVL(MDCODMAG, SPACE(5))
    this.w_BPAGCLI = NVL(FLPAGCLI, "S")<>"S"
    * --- Contropartite di pagamento legate alla Carta di Credito/Finanziamento
    *     Nel caso siano diverse devo dividere i documenti
    this.w_OBRK = this.w_OBRK + Nvl(CONCAR,Space(15)) + Nvl(CONFIN,Space(15))
    this.w_OBRK2 = this.w_OBRK + MDSERIAL
    this.w_NEWTES = .F.
    if this.w_DBRK2<>this.w_OBRK2
      * --- Testa Cambio Registrazione (di Vendita Negozio)
      this.w_FLERR = .F.
      if this.w_DBRK<>this.w_OBRK
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Delete from TMP_MATR
        i_nConn=i_TableProp[this.TMP_MATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_MATR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        SELECT GENEORDI
        * --- Dati della vendita
        this.w_MDTIPCHI = NVL(MDTIPCHI, "  ")
        this.w_MDCODCLI = NVL(MDCODCLI, SPACE(5))
        this.w_MDTIPDOC = NVL(MDTIPDOC, SPACE(5))
        this.w_MDCODPAG = NVL(MDCODPAG, SPACE(5))
        this.w_MDSTOPRO = NVL(MDSTOPRO,0)
        * --- Test Raggruppamento per Articolo (solo se corrispettivi totalmente incassati)
        this.w_FLRAGG = IIF(this.oParentObject.w_TIPOPE="D" AND NVL(FLPAGCLI, "S")="S" , "S", "N")
        this.w_MVTIPDOC = MDTIPDOC
        this.w_FLPPRO = " "
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDCATDOC,TDFLINTE,TDNUMSCO,TDFLACCO,TDFLPPRO,TDPRODOC,TFFLRAGG,TDCAUCON,TDASPETT,TDCAUMAG,TDFLPACK,TDFLANAL,TDFLELAN,TD_SEGNO"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDCATDOC,TDFLINTE,TDNUMSCO,TDFLACCO,TDFLPPRO,TDPRODOC,TFFLRAGG,TDCAUCON,TDASPETT,TDCAUMAG,TDFLPACK,TDFLANAL,TDFLELAN,TD_SEGNO;
            from (i_cTable) where;
                TDTIPDOC = this.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
          this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_NUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
          this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
          this.w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
          this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
          this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
          this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
          this.w_MVASPEST = NVL(cp_ToDate(_read_.TDASPETT),cp_NullValue(_read_.TDASPETT))
          this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          this.w_FLPACK = NVL(cp_ToDate(_read_.TDFLPACK),cp_NullValue(_read_.TDFLPACK))
          this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          this.w_FLELAN = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
          this.w_MV_SEGNO = NVL(cp_ToDate(_read_.TD_SEGNO),cp_NullValue(_read_.TD_SEGNO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_TIPREG = ""
        if Not Empty(this.w_MVCAUCON)
          * --- Read from CAU_CONT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAU_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCTIPREG"+;
              " from "+i_cTable+" CAU_CONT where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.w_MVCAUCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCTIPREG;
              from (i_cTable) where;
                  CCCODICE = this.w_MVCAUCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_MVTIPCON = "C"
        this.w_MVCODCON = IIF(this.w_MVFLINTE="C" AND (this.w_MVCLADOC<>"RF" OR CCFLCOSE="S"), ANCODICE, SPACE(15))
        * --- Attenzione: queste w_MV... vanno lasciate Locali (lette in GSAR_BFA)
        this.w_MVDATDOC = CP_TODATE(MDDATREG)
        this.w_MVDATREG = this.w_MVDATDOC
        this.w_MVDATCIV = this.w_MVDATDOC
        this.w_OKDATDIV = .F.
        * --- Select from PAG_2AME
        i_nConn=i_TableProp[this.PAG_2AME_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2],.t.,this.PAG_2AME_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select P2TIPSCA  from "+i_cTable+" PAG_2AME ";
              +" where P2CODICE="+cp_ToStrODBC(this.w_MDCODPAG)+"";
               ,"_Curs_PAG_2AME")
        else
          select P2TIPSCA from (i_cTable);
           where P2CODICE=this.w_MDCODPAG;
            into cursor _Curs_PAG_2AME
        endif
        if used('_Curs_PAG_2AME')
          select _Curs_PAG_2AME
          locate for 1=1
          do while not(eof())
          * --- Controllo se nel Dettaglio del pagamento esiste almeno un record 
          *     con Inizio Scadenza impostato a 'Data Diversa'
          this.w_OKDATDIV = this.w_OKDATDIV Or _Curs_PAG_2AME.P2TIPSCA="DD"
            select _Curs_PAG_2AME
            continue
          enddo
          use
        endif
        SELECT GENEORDI
        this.w_GENPOS = Nvl(GENEORDI.PAFLCONT,"N")="S"
        * --- Se esiste il pagamento prevede Data Diversa 
        *     assegno al campo MVDATDIV la data documento altrimenti vuoto
        this.w_MVDATDIV = IIF(this.w_OKDATDIV,this.w_MVDATDOC,cp_CharToDate("  /  /    "))
        this.w_MVDATTRA = this.w_MVDATDOC
        this.w_MVDATEVA = this.w_MVDATDOC
        this.w_MVNUMREG = 0
        this.w_MVALFEST = Space(10)
        this.w_MVNUMEST = 0
        this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
        this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
        this.w_MVCODESE = g_CODESE
        this.w_MVANNPRO = CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO)
        this.w_MVPRP = "NN"
        this.w_MVORATRA = left(time(),2)
        this.w_MVMINTRA = substr(time(),4,2)
        this.w_MVNUMDOC = NVL(MDNUMDOC, 0)
        this.w_MVALFDOC = NVL(MDALFDOC, "  ")
        this.w_MVCODMAG = NVL(MDCODMAG, SPACE(5))
        this.w_TOTPRE = 0
        this.w_TOTCON = 0
        this.w_TOTASS = 0
        this.w_TOTCAR = 0
        this.w_TOTFIN = 0
        this.w_MV__NOTE = ""
        * --- Lettura Centro di Costo e flag Esplicita Sconti Riga da Parametri Vendita negozio
        this.w_CODNEG = Nvl(MDCODNEG,Space(3))
        this.w_CODCEN = Space(15)
        * --- Read from PAR_VDET
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_VDET_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PACODCEN,PAFLFAFI"+;
            " from "+i_cTable+" PAR_VDET where ";
                +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and PACODNEG = "+cp_ToStrODBC(this.w_CODNEG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PACODCEN,PAFLFAFI;
            from (i_cTable) where;
                PACODAZI = this.w_CODAZI;
                and PACODNEG = this.w_CODNEG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODCEN = NVL(cp_ToDate(_read_.PACODCEN),cp_NullValue(_read_.PACODCEN))
          this.w_FLFAFI = NVL(cp_ToDate(_read_.PAFLFAFI),cp_NullValue(_read_.PAFLFAFI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Inizializza i dati di Testata del Nuovo Documento
        this.w_MVCAUMAG = this.w_MVTCAMAG
        this.w_MFLCASC = " "
        this.w_MFLORDI = " "
        this.w_MFLIMPE = " "
        this.w_MFLRISE = " "
        this.w_MFLELGM = " "
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMFLCOMM"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.w_MVTCAMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMFLCOMM;
            from (i_cTable) where;
                CMCODICE = this.w_MVTCAMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
          this.w_MFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MFLCASC = IIF(this.w_MVFLPROV="S"," ",this.w_MFLCASC)
        this.w_MFLORDI = IIF(this.w_MVFLPROV="S"," ",this.w_MFLORDI)
        this.w_MFLIMPE = IIF(this.w_MVFLPROV="S"," ",this.w_MFLIMPE)
        this.w_MFLRISE = IIF(this.w_MVFLPROV="S"," ",this.w_MFLRISE)
        this.w_MVFLORCO = IIF(g_COMM="S" ,iif(this.w_MFLCOMM="I","+",IIF(this.w_MFLCOMM="D","-"," "))," ")
        this.w_MVFLCOCO = IIF(g_COMM="S" , iif(this.w_MFLCOMM="C","+",IIF(this.w_MFLCOMM="S","-"," "))," ")
        * --- Viene considerato sempre scorporo piede fattura
        this.w_MVFLSCOR = "S"
        * --- Nel caso di corrispettivi riporto nelle note di testata il cliente negozio
        *     Solo se ho corrispettivo non incassato totalmente e quindi ho un conto cliente
        if Empty( this.w_MVCODCON ) And Not Empty(Nvl(CLDESCRI,"")) And this.w_BPAGCLI
          this.w_MV__NOTE = LEFT(CLDESCRI+SPACE(40),40)+LEFT(CLINDIRI+SPACE(35),35)+LEFT(CL___CAP+ SPACE(8), 8) +LEFT(CLLOCALI+SPACE(30), 30) +LEFT(CLPROVIN+SPACE(2), 2) +LEFT(CLCODFIS+SPACE(16), 16)
        endif
        this.w_MVCODIVE = SPACE(5)
        this.w_MVCODBAN = SPACE(10)
        this.w_MVCODBA2 = SPACE(10)
        this.w_MVCONCON = " "
        this.w_GIORN1 = 0
        this.w_GIORN2 = 0
        this.w_MESE1 = 0
        this.w_MESE2 = 0
        this.w_GIOFIS = 0
        this.w_CLBOLFAT = " "
        this.w_CODNAZ = g_CODNAZ
        this.w_MVCODPOR = NVL(MDCODPOR, " ")
        this.w_MVCODSPE = NVL(MDCODSPE, SPACE(3))
        this.w_MVCODAGE = NVL(MDCODAGE, SPACE(5))
        if NOT EMPTY(this.w_MVCODCON)
          this.w_MVCODBAN = NVL(CODBAN, SPACE(10))
          this.w_MVCODBA2 = ANCODBA2
          this.w_MVCONCON = NVL(ANCONCON, " ")
          this.w_GIORN1 = NVL(ANGIOSC1, 0)
          this.w_GIORN2 = NVL(ANGIOSC2, 0)
          this.w_MESE1 = NVL(AN1MESCL, 0)
          this.w_MESE2 = NVL(AN2MESCL, 0)
          this.w_GIOFIS = NVL(ANGIOFIS, 0)
          this.w_CLBOLFAT = NVL(ANBOLFAT, " ")
          this.w_CODNAZ = NVL(ANNAZION, g_CODNAZ)
          this.w_MVFLSCOR = NVL(FLSCOR, "N")
          this.w_MVCODVET = SPACE(5)
          * --- Leggo anche il conto corrente se � piena la banca
          *     ANNUMCOR viene sempre valorizzato con il conto corrente di default della tabella BAN_CONTI
          if Not Empty(this.w_MVCODBAN)
            this.w_NUMCOR = Space(25)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANNUMCOR"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANNUMCOR;
                from (i_cTable) where;
                    ANTIPCON = this.w_MVTIPCON;
                    and ANCODICE = this.w_MVCODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Select from DES_DIVE
          i_nConn=i_TableProp[this.DES_DIVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
                +" where DDTIPCON= "+cp_ToStrODBC(this.w_MVTIPCON)+" AND DDCODICE="+cp_ToStrODBC(this.w_MVCODCON)+" AND DDTIPRIF='CO' AND DDPREDEF='S' ";
                 ,"_Curs_DES_DIVE")
          else
            select * from (i_cTable);
             where DDTIPCON= this.w_MVTIPCON AND DDCODICE=this.w_MVCODCON AND DDTIPRIF="CO" AND DDPREDEF="S" ;
              into cursor _Curs_DES_DIVE
          endif
          if used('_Curs_DES_DIVE')
            select _Curs_DES_DIVE
            locate for 1=1
            do while not(eof())
            if _Curs_DES_DIVE.DDDTOBSO > this.w_MVDATREG OR EMPTY(NVL(_Curs_DES_DIVE.DDDTOBSO,cp_CharToDate("  -  -    ")))
              this.w_MVCODDES = _Curs_DES_DIVE.DDCODDES
              this.w_MVCODVET = _Curs_DES_DIVE.DDCODVET
              this.w_MVCODPOR = IIF(NOT EMPTY(NVL(_Curs_DES_DIVE.DDCODPOR,SPACE(10))),_Curs_DES_DIVE.DDCODPOR,this.w_MVCODPOR)
              this.w_MVCODSPE = IIF(NOT EMPTY(NVL(_Curs_DES_DIVE.DDCODSPE,SPACE(10))),_Curs_DES_DIVE.DDCODSPE,this.w_MVCODSPE)
              this.w_MVCODAGE = IIF(NOT EMPTY(NVL(_Curs_DES_DIVE.DDCODAGE,SPACE(10))),_Curs_DES_DIVE.DDCODAGE,this.w_MVCODAGE)
            endif
              select _Curs_DES_DIVE
              continue
            enddo
            use
          endif
          SELECT GENEORDI
        endif
        if Not Empty(this.w_MVCODAGE)
          * --- Read from AGENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AGCZOAGE"+;
              " from "+i_cTable+" AGENTI where ";
                  +"AGCODAGE = "+cp_ToStrODBC(this.w_MVCODAGE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AGCZOAGE;
              from (i_cTable) where;
                  AGCODAGE = this.w_MVCODAGE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVCODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Contropartite per Pagamenti
        this.w_CONPRE = Nvl(PACONPRE,Space(15))
        this.w_CONCON = Nvl(PACONCON,Space(15))
        this.w_CONASS = Nvl(PACONASS,Space(15))
        * --- Per Carte di Credito E Finanziamenti se � stato definita una contropartita a
        *     livello di anagrafica Carte Di Credito, Prendo questa
        this.w_CONCAR = IIF(Not Empty(Nvl(CONCAR," ")) , CONCAR, Nvl(PACONCAR,Space(15) ) )
        this.w_CONFIN = IIF(Not Empty(Nvl(CONFIN," ")) , CONFIN, Nvl(PACONFIN,Space(15) ) )
        this.w_MVCODPAG = NVL(MDCODPAG, SPACE(5))
        this.w_MVCODVAL = NVL(MDCODVAL, SPACE(5))
        this.w_MVSPEINC = NVL(MDSPEINC, 0)
        this.w_MVSPEIMB = NVL(MDSPEIMB, 0)
        this.w_MVSPETRA = NVL(MDSPETRA, 0)
        this.w_MVSPEBOL = 0
        this.w_MVIMPARR = 0
        this.w_MVTCONTR = SPACE(15)
        this.w_MVCODVAL = IIF(EMPTY(this.w_MVCODVAL), this.w_MVVALNAZ, this.w_MVCODVAL)
        this.w_MVCAOVAL = this.w_CAONAZ
        this.w_MVDATCIV = this.w_MVDATDOC
        this.w_MVDATREG = this.w_MVDATDOC
        if this.w_MVCODVAL<>this.w_MVVALNAZ
          this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL, this.w_MVDATDOC, 0)
        endif
        this.w_MVCAOVAL = IIF(this.w_MVCAOVAL=0, g_CAOVAL, this.w_MVCAOVAL)
        this.w_CAOCON = this.w_MVCAOVAL
        this.w_VALCON = this.w_MVCODVAL
        * --- Legge Dati Associati alla Valuta
        this.w_DECTOT = 0
        this.w_DECUNI = 0
        this.w_BOLESE = 0
        this.w_BOLSUP = 0
        this.w_BOLCAM = 0
        this.w_BOLARR = 0
        this.w_BOLMIN = 0
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI;
            from (i_cTable) where;
                VACODVAL = this.w_MVCODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
          this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
          this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
          this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
          this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
          this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- calcola le Spese di Incasso
        *     MVCODIVE non � pi� gestito
        *     Non � pi� gestito il codice Iva legato al Cliente o Lettera di Intento.
        *     L'Iva � sempre e solo quella legata all'articolo
        this.w_MVIVAINC = IIF(NOT EMPTY(this.w_MVCODIVE), this.w_MVCODIVE, g_COIINC)
        this.w_MVIVAIMB = IIF(NOT EMPTY(this.w_MVCODIVE), this.w_MVCODIVE, g_COIIMB)
        this.w_MVIVATRA = IIF(NOT EMPTY(this.w_MVCODIVE), this.w_MVCODIVE, g_COITRA)
        this.w_MVIVABOL = SPACE(5)
        this.w_BOLBOL = " "
        this.w_PEIINC = 0
        this.w_BOLINC = " "
        if NOT EMPTY(this.w_MVIVAINC)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVBOLIVA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAINC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVBOLIVA;
              from (i_cTable) where;
                  IVCODIVA = this.w_MVIVAINC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PEIINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_BOLINC = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_PEIIMB = 0
        this.w_BOLIMB = " "
        if NOT EMPTY(this.w_MVIVAIMB)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVBOLIVA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAIMB);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVBOLIVA;
              from (i_cTable) where;
                  IVCODIVA = this.w_MVIVAIMB;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PEIIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_BOLIMB = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_PEITRA = 0
        this.w_BOLTRA = " "
        if NOT EMPTY(this.w_MVIVATRA)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVBOLIVA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVATRA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVBOLIVA;
              from (i_cTable) where;
                  IVCODIVA = this.w_MVIVATRA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PEITRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_BOLTRA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_PERIVE = 0
        this.w_INDIVE = 0
        this.w_BOLIVE = " "
        this.w_MVTCOLIS = NVL(MDCODLIS, SPACE(5))
        this.w_IVALIS = " "
        this.w_QUALIS = " "
        if NOT EMPTY(this.w_MVTCOLIS)
          * --- Read from LISTINI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LISTINI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LSIVALIS,LSQUANTI"+;
              " from "+i_cTable+" LISTINI where ";
                  +"LSCODLIS = "+cp_ToStrODBC(this.w_MVTCOLIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LSIVALIS,LSQUANTI;
              from (i_cTable) where;
                  LSCODLIS = this.w_MVTCOLIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
            this.w_QUALIS = NVL(cp_ToDate(_read_.LSQUANTI),cp_NullValue(_read_.LSQUANTI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if (this.w_IVALIS="L" AND this.w_MVFLSCOR<>"S") OR (this.w_IVALIS<>"L" AND this.w_MVFLSCOR="S")
            * --- Azzera se Listino Incongruente
            this.w_MVTCOLIS = SPACE(5)
          endif
        endif
        * --- Se sono presenti sconti per promozioni metto sempre sconto forzato perch� effettivamente
        *     � come se lo fosse visto che la promozione � rappresentata da uno sconto a valore
        this.w_MVFLFOSC = IIF(this.w_MDSTOPRO<>0,"S",NVL(MDFLFOSC, " ") )
        this.w_MVSCOCL1 = NVL(MDSCOCL1,0)
        this.w_MVSCOCL2 = NVL(MDSCOCL2,0)
        this.w_MVSCOPAG = NVL(MDSCOPAG,0)
        * --- Con No Fattura Fidelity anche se gli sconti sono in percentuale 
        *     vengono tradotti in sconto a valore forzato
        if this.w_FLFAFI = "S"
          this.w_MVFLFOSC = IIF(NVL(MDSCONTI, 0) <>0,"S",this.w_MVFLFOSC )
          this.w_MVSCOCL1 = 0
          this.w_MVSCOCL2 = 0
          this.w_MVSCOPAG = 0
        endif
        this.w_MVACCPRE = 0
        this.w_MVACCSUC = 0
        this.w_MVACCONT = 0
        this.w_MDIMPABB = 0
        this.w_MRESTO = 0
        this.w_MVSCONTI = 0
        this.w_DBRK = this.w_OBRK
        this.w_NEWTES = .T.
      endif
      SELECT GENEORDI
      this.w_MVFLSALD = IIF(this.w_MVFLSALD="S", "S", NVL(MDFLSALD, " "))
      this.w_MVACCONT = this.w_MVACCONT + NVL(MDPAGCON, 0) + NVL(MDPAGASS, 0) + NVL(MDPAGCAR, 0) + NVL(MDPAGFIN, 0) + IIF(this.w_FLFAFI = "S", 0,NVL(MDIMPPRE, 0))
      * --- Valorizzo i Totalizzatori dei Pagamenti
      if this.w_FLFAFI<>"S"
        * --- Prepagato fidelilty, solo se non ho attivo il flag No Fattura Fidelity
        this.w_TOTPRE = this.w_TOTPRE + NVL(MDIMPPRE, 0)
      endif
      this.w_TOTCON = this.w_TOTCON + Nvl(MDPAGCON,0)
      this.w_TOTASS = this.w_TOTASS + Nvl(MDPAGASS,0)
      this.w_TOTCAR = this.w_TOTCAR + Nvl(MDPAGCAR,0)
      this.w_TOTFIN = this.w_TOTFIN + Nvl(MDPAGFIN,0)
      if this.w_MVCLADOC="RF"
        this.w_MVACCSUC = this.w_MVACCSUC + NVL(MDACCPRE, 0)
      else
        this.w_MVACCPRE = this.w_MVACCPRE + NVL(MDACCPRE, 0)
      endif
      * --- Storna eventuale Resto
      this.w_MVACCONT = this.w_MVACCONT + IIF(NVL(MDIMPABB, 0)<0, NVL(MDIMPABB, 0), 0)
      * --- Memorizzo l'abbuono (solo se effettivamente abbuono cio� > 0 altrimenti Resto)
      this.w_MDIMPABB = this.w_MDIMPABB + IIF(NVL(MDIMPABB, 0)>0, NVL(MDIMPABB, 0), 0)
      this.w_MRESTO = this.w_MRESTO + IIF(NVL(MDIMPABB, 0)<0, Abs(NVL(MDIMPABB, 0)), 0)
      this.w_MVSCONTI = this.w_MVSCONTI + NVL(MDSCONTI, 0) + NVL(MDSTOPRO,0)
      * --- riferimenti registrazioni da Flaggare
      this.w_MDSERIAL = MDSERIAL
      this.oParentObject.w_NUREG = this.oParentObject.w_NUREG + 1
      INSERT INTO RifReg (MDSERIAL) VALUES (this.w_MDSERIAL)
      this.w_DBRK2 = this.w_OBRK2
    endif
    if Not Empty(CHKCONS(this.w_MVFLVEAC+IIF(this.w_FLANAL="S","C","")+IIF(Not empty(this.w_MVCODMAG),"M",""),this.w_MVDATREG,"B","N"))
      this.w_FLERR = .T.
      this.oParentObject.w_ERRORI = .T.
      if this.w_NEWTES
        * --- Se nuova Testata Inserisco messaggio di Errore
        this.oParentObject.w_oERRORLOG.AddMsgLog("Verificare doc.n.: %1 del %2 %3", Alltrim(Str(this.w_MVNUMDOC,15))+IIF(EMPTY(this.w_MVALFDOC),"","/"+Alltrim(this.w_MVALFDOC)), DTOC(this.w_MVDATDOC), CHKCONS(this.w_MVFLVEAC+IIF(this.w_FLANAL="S","C","")+IIF(Not empty(this.w_MVCODMAG),"M",""),this.w_MVDATREG,"B","N"))     
      endif
    endif
    SELECT GENEORDI
    if Not this.w_FLERR
      * --- Scrive nuova Riga sul Temporaneo di Appoggio
      this.w_MVTIPRIG = NVL(MDTIPRIG, "R")
      this.w_MVCODICE = NVL(MDCODICE, SPACE(20))
      this.w_MVCODART = NVL(MDCODART, SPACE(20))
      this.w_MVDESART = NVL(CADESART, SPACE(40))
      this.w_MVDESSUP = NVL(MDNOTAGG, " ")
      this.w_MVUNIMIS = NVL(MDUNIMIS, SPACE(3))
      this.w_MVCATCON = NVL(ARCATCON, SPACE(5))
      this.w_MVCODCLA = NVL(ARCODCLA, SPACE(3))
      this.w_GRUMER = NVL(ARGRUMER, SPACE(5))
      this.w_MVCODLIS = this.w_MVTCOLIS
      this.w_MVQTAMOV = NVL(MDQTAMOV, 0)
      this.w_MVQTAUM1 = NVL(MDQTAUM1, 0)
      this.w_MVPREZZO = NVL(MDPREZZO, 0)
      this.w_MVSCONT1 = IIF(g_FLESSC="S" And this.w_MVDATDOC >= g_DATESC, 0, NVL(MDSCONT1, 0))
      this.w_MVSCONT2 = IIF(g_FLESSC="S" And this.w_MVDATDOC >= g_DATESC, 0, NVL(MDSCONT2, 0))
      this.w_MVSCONT3 = IIF(g_FLESSC="S" And this.w_MVDATDOC >= g_DATESC, 0, NVL(MDSCONT3, 0))
      this.w_MVSCONT4 = IIF(g_FLESSC="S" And this.w_MVDATDOC >= g_DATESC, 0, NVL(MDSCONT4, 0))
      this.w_MVSCOVEN = NVL(MDSCOVEN, 0)
      this.w_MVFLOMAG = NVL(MDFLOMAG, "X")
      this.w_MVCODIVA = NVL(ARCODIVA, SPACE(5))
      this.w_FLRESO = NVL(MDFLRESO, " ")
      this.w_PERIVA = NVL(IVPERIVA, 0)
      this.w_BOLIVA = NVL(IVBOLIVA, " ")
      this.w_UNMIS1 = NVL(ARUNMIS1, SPACE(3))
      this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
      this.w_UNMIS3 = NVL(CAUNIMIS, SPACE(3))
      this.w_MOLTIP = NVL(ARMOLTIP, 0)
      this.w_MOLTI3 = NVL(CAMOLTIP, 0)
      this.w_OPERAT = NVL(AROPERAT, " ")
      this.w_OPERA3 = NVL(CAOPERAT, " ")
      this.w_FLSERA = NVL(ARTIPSER,SPACE(1))
      this.w_CODPRO = NVL(MDCODPRO, SPACE(15))
      this.w_MVCODLOT = IIF(this.w_MDTIPCHI="FF",Space(20),Nvl(MDCODLOT,Space(20)) )
      this.w_MVCODUBI = IIF(this.w_MDTIPCHI="FF",Space(20),Nvl(MDCODUBI,Space(20)) )
      this.w_MVFLLOTT = IIF(this.w_MDTIPCHI="FF"," ",Nvl(MDFLLOTT," ") )
      this.w_MVLOTMAG = NVL(MDLOTMAG, SPACE(5))
      this.w_MVIMPACC = 0
      this.w_MVIMPSCO = 0
      this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
      * --- Al posto dell'Iva di Esenzione passo sempre 0 poich� visto che sul POS i
      *     prezzi sono sempre al lordo, calcolo il valore fiscale sempre al netto dell'Iva.
      *     Anche so ho quindi una lettera di intento o un codice Iva esenzione in Testata
      this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR, this.w_MVVALRIG, this.w_MVIMPSCO, this.w_MVIMPACC, this.w_PERIVA, this.w_DECTOT, "", 0 )
      this.w_INDIVA = IIF(this.w_PERIVA<>0 AND this.w_PERIVE=0, IVPERIND, 0)
      this.w_MVPESNET = NVL(ARPESNET, 0)
      this.w_MVNOMENC = NVL(ARNOMENC, SPACE(8))
      this.w_MVUMSUPP = NVL(ARUMSUPP, SPACE(3))
      this.w_MVMOLSUP = NVL(ARMOLSUP, 0)
      this.w_MVCODCEN = SPACE(15)
      this.w_MVVOCCOS = NVL(MVVOCCOS, SPACE(15))
      this.w_MVCODCOM = Nvl(MDCODCOM,Space(15))
      this.w_MVIMPCOM = Nvl(MDIMPCOM, 0)
      this.w_MVSERRIF = NVL(MDSERRIF, SPACE(10))
      this.w_MVROWRIF = NVL(MDROWRIF, 0)
      this.w_CODCONF = SPACE(3)
      this.w_CODCOL = SPACE(5)
      this.w_NUMCOL = 0
      * --- Se attivo 'Provvigioni in Generazione Docuemnti' su paramentri provvigioni
      *     devo impostare il tipo provvigione come se caricassi a mano un documento
      *     Verr� poi calcolato da GSVE_BPP
      this.w_MVTIPPRO = IIF(this.w_MVFLOMAG<>"X","ES",IIF(g_PROGEN = "S", IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")) ,NVL(MDTIPPRO,"DC")))
      this.w_MVPERPRO = NVL(MDPERPRO,0)
      this.w_MVTIPPR2 = IIF(this.w_MVFLOMAG<>"X","ES",IIF(g_PROGEN = "S", IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")) ,NVL(MDTIPPR2,"DC")))
      this.w_MVPROCAP = NVL(MDPROCAP,0)
      this.w_SERIAL = Nvl(MDSERIAL,Space(10))
      this.w_ROWNUM = Nvl(CPROWNUM,0)
      this.w_MATRSN = NVL(MATRSN,"N")
      if this.w_FLPACK="S"
        do case
          case this.w_MVUNIMIS=this.w_UNMIS3 AND NOT EMPTY(this.w_UNMIS3)
            * --- UM. del Codice di Ricerca
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CATPCON3,CATIPCO3,CAPZCON3"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CATPCON3,CATIPCO3,CAPZCON3;
                from (i_cTable) where;
                    CACODICE = this.w_MVCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCONF = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
              this.w_CODCOL = NVL(cp_ToDate(_read_.CATIPCO3),cp_NullValue(_read_.CATIPCO3))
              this.w_PZCONF = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          case this.w_MVUNIMIS=this.w_UNMIS2 AND NOT EMPTY(this.w_UNMIS2) AND NOT EMPTY(this.w_MVCODART)
            * --- UM della 2^UM Articolo
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARTPCON2,ARTIPCO2,ARPZCON2"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARTPCON2,ARTIPCO2,ARPZCON2;
                from (i_cTable) where;
                    ARCODART = this.w_MVCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCONF = NVL(cp_ToDate(_read_.ARTPCON2),cp_NullValue(_read_.ARTPCON2))
              this.w_CODCOL = NVL(cp_ToDate(_read_.ARTIPCO2),cp_NullValue(_read_.ARTIPCO2))
              this.w_PZCONF = NVL(cp_ToDate(_read_.ARPZCON2),cp_NullValue(_read_.ARPZCON2))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          case this.w_MVUNIMIS=this.w_UNMIS1 AND NOT EMPTY(this.w_UNMIS1) AND NOT EMPTY(this.w_MVCODART)
            * --- UM della 1^UM Articolo
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARTPCONF,ARTIPCO1,ARPZCONF"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARTPCONF,ARTIPCO1,ARPZCONF;
                from (i_cTable) where;
                    ARCODART = this.w_MVCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCONF = NVL(cp_ToDate(_read_.ARTPCONF),cp_NullValue(_read_.ARTPCONF))
              this.w_CODCOL = NVL(cp_ToDate(_read_.ARTIPCO1),cp_NullValue(_read_.ARTIPCO1))
              this.w_PZCONF = NVL(cp_ToDate(_read_.ARPZCONF),cp_NullValue(_read_.ARPZCONF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
        endcase
        if NOT EMPTY(this.w_CODCONF) AND NOT EMPTY(this.w_CODCOL) AND this.w_PZCONF<>0
          this.w_NUMCOL = cp_ROUND((this.w_MVQTAMOV / this.w_PZCONF)+ .499, 0)
        endif
      endif
      this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, "", 0, 0, this.w_PERIVA, this.w_INDIVA )
      this.w_MVTIPATT = "A"
      this.w_MVCODATT = SPACE(15)
      * --- Totalizzatori
      this.w_TOTALE = this.w_TOTALE + this.w_MVVALRIG
      this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X" AND this.w_FLSERA<>"S", this.w_MVVALRIG, 0)
      INSERT INTO GeneApp ;
      (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ;
      t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ;
      t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ;
      t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ;
      t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ;
      t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, t_MVSCOVEN, ;
      t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ;
      t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ;
      t_MVCODMAG,t_MVLOTMAG, t_MVCODCEN, t_MVVOCCOS, t_MVSERRIF, t_MVROWRIF, ;
      t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, t_INDIVA, t_FLSERA, t_FLRESO, ;
      t_UNMIS1, t_UNMIS2, t_UNMIS3, t_CPROWORD, CPROWNUM, t_CODCONF, t_MVNUMCOL, t_MVCODCOL, ;
      t_MVTIPPRO, t_MVPERPRO, t_CODPRO, t_MVIMPCOM, t_MVIMPAC2, ;
      t_MVCODLOT, t_MVCODUBI, t_MVFLLOTT, t_POSSER, t_POSROW, t_MVTIPPR2, t_MVPROCAP, t_MVFLORCO, t_MVFLCOCO, t_MATRSN ) ;
      VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ;
      this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ;
      this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ;
      this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ;
      this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ;
      this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, this.w_MVSCOVEN, ;
      this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ;
      this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ;
      this.w_MVCODMAG,this.w_MVLOTMAG,this.w_MVCODCEN, this.w_MVVOCCOS, this.w_MVSERRIF, this.w_MVROWRIF, ;
      this.w_MVCODCOM, this.w_MVTIPATT, this.w_MVCODATT, this.w_MVDATEVA, this.w_INDIVA, this.w_FLSERA, this.w_FLRESO, ;
      this.w_UNMIS1, this.w_UNMIS2, this.w_UNMIS3, 1, 1, this.w_CODCONF, this.w_NUMCOL , this.w_CODCOL, ;
      this.w_MVTIPPRO, this.w_MVPERPRO, this.w_CODPRO, this.w_MVIMPCOM, 0, ;
      this.w_MVCODLOT, this.w_MVCODUBI, this.w_MVFLLOTT, this.w_SERIAL, this.w_ROWNUM, this.w_MVTIPPR2, this.w_MVPROCAP, this.w_MVFLORCO, this.w_MVFLCOCO, this.w_MATRSN)
      if Not this.w_FLRAGG="S" And this.w_MDTIPCHI<>"FF"
        this.w_RECNO = RecNo("GeneApp")
        * --- Insert into TMP_MATR
        i_nConn=i_TableProp[this.TMP_MATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_MATR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_MATR_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"POSSER"+",POSROW"+",DOCSER"+",DOCROW"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMP_MATR','POSSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'TMP_MATR','POSROW');
          +","+cp_NullLink(cp_ToStrODBC("XXXXX"),'TMP_MATR','DOCSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RECNO),'TMP_MATR','DOCROW');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'POSSER',this.w_SERIAL,'POSROW',this.w_ROWNUM,'DOCSER',"XXXXX",'DOCROW',this.w_RECNO)
          insert into (i_cTable) (POSSER,POSROW,DOCSER,DOCROW &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,this.w_ROWNUM;
               ,"XXXXX";
               ,this.w_RECNO;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      SELECT GENEORDI
    endif
    ENDSCAN
    * --- Testa l'Ultima Uscita
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizioni Variabili Locali
    * --- Inizializza le Variabili di Lavoro
    this.w_TOTMERCE = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVACCSUC = 0
    this.w_MVACCPRE = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVACCPRE = 0
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTOTRIT = 0
    this.w_MVTOTENA = 0
    this.w_MVTOTRIT = 0
    this.w_MDSTOPRO = 0
    this.w_MVIMPPRO = 0
    this.w_IMPPRO = 0
    this.w_CODCONF = SPACE(3)
    this.w_CODCOL = SPACE(5)
    this.w_MVQTACOL = 0
    this.w_MVQTAPES = 0
    this.w_MVQTALOR = 0
    this.w_PZCONF = 0
    this.w_NUMCOL = 0
    * --- Se attivo 'Provvigioni in Generazione Docuemnti' su paramentri provvigioni
    *     devo impostare il tipo provvigione come se caricassi a mano un documento
    *     Verr� poi calcolato da GSVE_BPP
    this.w_MVTIPPRO = IIF(g_PROGEN = "S", IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")) ,"DC")
    this.w_MVPERPRO = 0
    this.w_MVTIPPR2 = IIF(g_PROGEN = "S", IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")) ,"DC")
    this.w_MVPROCAP = 0
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_DBRK <> "##zz##" AND RECCOUNT("GeneApp") > 0
      * --- Cancello il contenuto della Tabella Temporanea per inserimento matricole
      if this.w_FLRAGG="S"
        * --- Delete from TMP_MATR
        i_nConn=i_TableProp[this.TMP_MATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_MATR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Lancia Batch per ripartizione spese accessorie
      *     Il 5� e l'ultimo parametro sono le spese accessorie che dal POS non esistono
      this.w_DATCOM = IIF(Empty(this.w_MVDATDOC),this.w_MVDATREG, this.w_MVDATDOC)
      GSAR_BRS(this,"B", "GeneApp", this.w_MVFLVEAC, this.w_MVFLSCOR, 0, this.w_MVSCONTI, this.w_TOTMERCE, "", this.w_MVCAOVAL, this.w_DATCOM, this.w_CAONAZ, this.w_MVVALNAZ, this.w_MVCODVAL, 0)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcoli Finali
      GSAR_BFA(this,"DP")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_FLRAGG="S"
        * --- MVACCSUC = incassi successivi (solo per corrispettivi )
        *     MVACCPRE = acconti precedenti ( per gli altri documenti )
        this.w_DIFFER = (this.w_MVACCONT + this.w_MDIMPABB + this.w_MVACCSUC + this.w_MVACCPRE ) - this.w_TOTFATTU 
        * --- Verifico eventuale differenza tra totale documento e totale incassato. 
        *     La verifica � necessaria solo se raggruppo pi� righe nel documento.
        *     Se, per motivi di calcolo, � diversa da zero allora modifico il totale sconti 
        *     di piede e aumento il valore della prima riga valida (non descrittiva ne forfettaria)
        if this.w_DIFFER <> 0
          this.w_MVSCONTI = this.w_MVSCONTI + this.w_DIFFER
           
 SELECT GeneApp 
 LOCATE FOR Not t_MVTIPRIG$ "FD"
          REPLACE t_MVIMPSCO WITH t_MVIMPSCO + this.w_DIFFER
          REPLACE t_MVVALMAG WITH t_MVVALMAG + this.w_DIFFER
          REPLACE t_MVIMPNAZ WITH ; 
 CAIMPNAZ(this.w_MVFLVEAC, t_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, t_PERIVA, t_INDIVA ) 
 Go Top
          GSAR_BFA(this,"DP")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      this.w_MVSERIAL = SPACE(10)
      this.w_MVNUMEST = 0
      this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_MVNUMRIF = -20
      this.w_MVFLOFFE = IIF(this.oParentObject.w_TIPOPE="G", "F", this.oParentObject.w_TIPOPE)
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_INSDOC
        * --- Try
        local bErr_04838408
        bErr_04838408=bTrsErr
        this.Try_04838408()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if this.oparentobject.oparentobject.cfunction="Edit"
            * --- ripristino i progressivi se la generazione documento di una vendita in modifica non va a buon fine 
            this.w_NUMDOC = 0
            i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
            cp_askTableProg(this, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_NUMDOC")
            if this.w_NUMDOC - 1 = this.w_MVNUMDOC
              GSAR_BRP(this,this.w_MVFLVEAC, this.w_MVANNDOC, this.w_MVPRD, this.w_MVNUMDOC, this.w_MVALFDOC, this.w_MVPRP, this.w_MVNUMEST, this.w_MVALFEST, this.w_MVANNPRO, this.w_MVCLADOC)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          ah_ErrorMsg(i_ErrMsg)
        endif
        bTrsErr=bTrsErr or bErr_04838408
        * --- End
      endif
    endif
    * --- Inizializza il Temporaneo di Appoggio
    * --- I campi t_UNMIS1, t_UNMIS2, t_UNMIS3, t_CPROWORD, CPROWNUM, t_CODCONF, t_MVNUMCOL, t_MVCODCOL
    *     sono stati inseriti poich� nel caso di documento con packing list il cursore viene utilizzato nel batch GSVE_BC2 al posto del transitorio 
    *     per il calcolo dei colli e dei pesi quindi il cursore deve essere congruente con il cTrsName dei documenti
    CREATE CURSOR GeneApp ;
    (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
    t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ;
    t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ;
    t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ;
    t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), t_MVSCOVEN N(18,4), ;
    t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ;
    t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ;
    t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ;
    t_MVCODMAG C(5), t_MVLOTMAG C(5), t_MVCODCEN C(15), t_MVVOCCOS C(15), t_MVSERRIF C(10), t_MVROWRIF N(4,0), ;
    t_MVCODCOM C(15), t_MVTIPATT C(1), t_MVCODATT C(15), t_MVDATEVA D(8), t_INDIVA N(3,0), t_FLSERA C(1), t_FLRESO C(1), ;
    t_UNMIS1 C(3), t_UNMIS2 C(3), t_UNMIS3 C(3), t_CPROWORD N(5), CPROWNUM N(4), t_CODCONF C(3), t_MVNUMCOL N(5), t_MVCODCOL C(5), ;
    t_MVTIPPRO C(2), t_MVPERPRO N(5,2), t_CODPRO C(15), t_MVIMPCOM N(18,4), t_MVIMPAC2 N(18,4), ;
    t_MVCODLOT C(20), t_MVCODUBI C(20), t_MVFLLOTT C(1), t_POSSER C(10), t_POSROW N(4), t_MVTIPPR2 C(2), t_MVPROCAP N(5,2), t_MVFLORCO C(1), t_MVFLCOCO C(1), t_MATRSN C(1))
    * --- riferimenti registrazioni da Flaggare
    CREATE CURSOR RifReg (MDSERIAL C(10))
    * --- Inizializza le Variabili di Lavoro
    this.w_TOTMERCE = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVACCPRE = 0
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTOTRIT = 0
    this.w_MVTOTENA = 0
    this.w_MVTOTRIT = 0
    this.w_MVFLSALD = " "
    * --- Rate Scadenze
    this.w_LOOP = 1
    do while this.w_LOOP <= 1000
      DR[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ")
      DR[ this.w_LOOP , 2] = 0
      DR[ this.w_LOOP , 3] = "  "
      DR[ this.w_LOOP , 4] = "  "
      DR[ this.w_LOOP , 5] = "  "
      DR[ this.w_LOOP , 6] = "  "
      DR[ this.w_LOOP , 7] = " "
      DR[ this.w_LOOP , 8] = " "
      DR[ this.w_LOOP , 9] = " "
      this.w_LOOP = this.w_LOOP + 1
    enddo
  endproc
  proc Try_04838408()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Calcola MVSERIAL
    this.w_MVSERIAL = cp_GetProg("DOC_MAST", "SEDOC", this.w_MVSERIAL, i_codazi)
    * --- Ricalcolo il numero documento solo se la causale documento gestisce i progressjvi
    *     e se non sono in modifica. In questo caso MVNUMDOC � gi� valorizzato dalla precedente
    *     generazione. Quindi mantengo quello.
    if this.w_MVPRD<>"NN" AND EMPTY(this.w_MVNUMDOC)
      this.w_MVNUMDOC = 0
      this.w_MVNUMDOC = cp_GetProg("DOC_MAST", "PRDOC", this.w_MVNUMDOC, i_codazi, this.w_MVANNDOC, this.w_MVPRD, this.w_MVALFDOC)
    endif
    if this.oParentObject.w_TIPOPE="G" 
      * --- Se generazione Documento da Corrispettivi Aggiorna Ultimo Numero Documento per stampa
      this.oParentObject.w_NUMFIN = this.w_MVNUMDOC
    endif
    * --- MVNUMREG, MVNUMDOC sono gia' stati definiti sul documento di Vendita Negozio
    this.w_MVNUMREG = this.w_MVNUMDOC
    * --- Inserisce la datga Plafond
    * --- Scrive la Testata
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZPLADOC"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZPLADOC;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZPLADOC = NVL(cp_ToDate(_read_.AZPLADOC),cp_NullValue(_read_.AZPLADOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVDATPLA = IIF(this.w_AZPLADOC="S",this.w_MVDATDOC,this.w_MVDATREG)
    * --- Try
    local bErr_0483E3B8
    bErr_0483E3B8=bTrsErr
    this.Try_0483E3B8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_MsgFormat("Errore in inserimento DOC_MAST%0%1", MESSAGE())
      return
    endif
    bTrsErr=bTrsErr or bErr_0483E3B8
    * --- End
    * --- Occorrono 3 frasi per problema lunghezza stringa INSERT
    * --- Try
    local bErr_0483EB38
    bErr_0483EB38=bTrsErr
    this.Try_0483EB38()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_MsgFormat("Errore in scrittura DOC_MAST%0%1", MESSAGE())
      return
    endif
    bTrsErr=bTrsErr or bErr_0483EB38
    * --- End
    * --- Try
    local bErr_0483F4C8
    bErr_0483F4C8=bTrsErr
    this.Try_0483F4C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_MsgFormat("Errore in scrittura DOC_MAST(2)%0%1", MESSAGE())
      return
    endif
    bTrsErr=bTrsErr or bErr_0483F4C8
    * --- End
    * --- Cicla sulle Righe Documento
    SELECT GeneApp
    GO TOP
    SCAN FOR t_MVTIPRIG<>" " AND NOT EMPTY(t_MVCODICE)
    * --- Scrive il Detail
    this.w_MVTIPRIG = t_MVTIPRIG
    this.w_MVCODICE = t_MVCODICE
    this.w_MVCODART = t_MVCODART
    this.w_MVDESART = t_MVDESART
    this.w_MVDESSUP = t_MVDESSUP
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_NUMCOL = t_MVNUMCOL
    this.w_MVCATCON = t_MVCATCON
    this.w_MVCODCLA = t_MVCODCLA
    this.w_MVCONTRA = t_MVCONTRA
    this.w_MVCODLIS = t_MVCODLIS
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVPREZZO = t_MVPREZZO
    this.w_MVIMPACC = t_MVIMPACC
    this.w_MVIMPSCO = t_MVIMPSCO
    this.w_MVVALMAG = t_MVVALMAG
    this.w_MVIMPNAZ = t_MVIMPNAZ
    this.w_MVSCONT1 = IIF(g_FLESSC="S" And this.w_MVDATDOC >= g_DATESC, 0, t_MVSCONT1)
    this.w_MVSCONT2 = IIF(g_FLESSC="S" And this.w_MVDATDOC >= g_DATESC, 0, t_MVSCONT2)
    this.w_MVSCONT3 = IIF(g_FLESSC="S" And this.w_MVDATDOC >= g_DATESC, 0, t_MVSCONT3)
    this.w_MVSCONT4 = IIF(g_FLESSC="S" And this.w_MVDATDOC >= g_DATESC, 0, t_MVSCONT4)
    this.w_MVFLOMAG = t_MVFLOMAG
    this.w_MVCODIVA = t_MVCODIVA
    this.w_MVVALRIG = t_MVVALRIG
    this.w_MVPESNET = t_MVPESNET
    this.w_MVNOMENC = t_MVNOMENC
    this.w_MVUMSUPP = t_MVUMSUPP
    this.w_MVMOLSUP = t_MVMOLSUP
    this.w_MVCODMAG = t_MVCODMAG
    this.w_MVCODCEN = t_MVCODCEN
    this.w_MVVOCCOS = t_MVVOCCOS
    this.w_MVCODCOM = t_MVCODCOM
    this.w_MVTIPATT = t_MVTIPATT
    this.w_MVCODATT = t_MVCODATT
    this.w_MVDATEVA = t_MVDATEVA
    this.w_FLRESO = t_FLRESO
    this.w_CODCOL = t_MVCODCOL
    this.w_MVTIPPRO = t_MVTIPPRO
    this.w_MVPERPRO = t_MVPERPRO
    this.w_MVTIPPR2 = t_MVTIPPR2
    this.w_MVPROCAP = t_MVPROCAP
    this.w_MVCODLOT = t_MVCODLOT
    this.w_MVCODUBI = t_MVCODUBI
    this.w_MVFLLOTT = t_MVFLLOTT
    * --- Se reso e no Fattura Fiscale Utilizza la causale magazino dei Resi
    this.w_MVCAUMAG = IIF(this.w_FLRESO="S" AND NOT this.oParentObject.w_TIPOPE $ "GF", this.oParentObject.w_CAURES, this.w_MVTCAMAG)
    this.w_MVFLCASC = IIF(this.w_FLRESO="S" AND NOT this.oParentObject.w_TIPOPE $ "GF", this.w_RFLCASC, this.w_MFLCASC)
    this.w_MVFLRISE = IIF(this.w_FLRESO="S" AND NOT this.oParentObject.w_TIPOPE $ "GF", this.w_RFLRISE, this.w_MFLRISE)
    this.w_MVFLORDI = IIF(this.w_FLRESO="S" AND NOT this.oParentObject.w_TIPOPE $ "GF", this.w_RFLORDI, this.w_MFLORDI)
    this.w_MVFLIMPE = IIF(this.w_FLRESO="S" AND NOT this.oParentObject.w_TIPOPE $ "GF", this.w_RFLIMPE, this.w_MFLIMPE)
    this.w_MVFLELGM = IIF(this.w_FLRESO="S" AND NOT this.oParentObject.w_TIPOPE $ "GF", this.w_RFLELGM, this.w_MFLELGM)
    this.w_MVKEYSAL = IIF(this.w_MVTIPRIG="R" AND NOT EMPTY(this.w_MVFLCASC+this.w_MVFLRISE+this.w_MVFLORDI+this.w_MVFLIMPE), this.w_MVCODART, SPACE(20))
    this.w_MVCODMAG = IIF(EMPTY(this.w_MVKEYSAL), SPACE(5), this.w_MVCODMAG)
    this.w_MVLOTMAG = t_MVLOTMAG
    this.w_MVFLULPV = " "
    this.w_MVVALULT = IIF(this.w_MVQTAUM1=0, 0, cp_ROUND(this.w_MVIMPNAZ/this.w_MVQTAUM1, g_PERPUL))
    this.w_ULTSCA = cp_CharToDate("  -  -  ")
    if NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_MVCODMAG) AND this.w_MVFLCASC="-" AND this.w_MVFLOMAG<>"S" AND (this.w_MVCLADOC<>"DT" OR this.w_MVPREZZO<>0)
      * --- Read from SALDIART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SLDATUPV"+;
          " from "+i_cTable+" SALDIART where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SLDATUPV;
          from (i_cTable) where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_MVCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ULTSCA = NVL(cp_ToDate(_read_.SLDATUPV),cp_NullValue(_read_.SLDATUPV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVFLULPV = IIF(this.w_MVDATDOC>=this.w_ULTSCA, "=", " ")
    endif
    * --- Leggo dati INTRA
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUTISER,ARDATINT"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUTISER,ARDATINT;
        from (i_cTable) where;
            ARCODART = this.w_MVCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ARUTISER = NVL(cp_ToDate(_read_.ARUTISER),cp_NullValue(_read_.ARUTISER))
      this.w_ARDATINT = NVL(cp_ToDate(_read_.ARDATINT),cp_NullValue(_read_.ARDATINT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ARUTISER = "S"
      this.w_MVFLTRAS = "S"
    else
      do case
        case this.w_ARDATINT = "N"
          this.w_MVFLTRAS = "S"
        case this.w_ARDATINT = "S"
          this.w_MVFLTRAS = "Z"
        case this.w_ARDATINT = "F"
          this.w_MVFLTRAS = " "
        case this.w_ARDATINT = "I"
          this.w_MVFLTRAS = "I"
      endcase
    endif
    * --- Inseriti solo per gestire la Tracciabilita' il controllo e' sempre sul Doc. di Vendita Negozio (MVFLARIF=' ')
    this.w_MVSERRIF = t_MVSERRIF
    this.w_MVROWRIF = t_MVROWRIF
    * --- Gestione Analitica
    this.w_MVVOCCEN = SPACE(15)
    this.w_MVFLELAN = " "
    this.w_OK_ANALI = .T.
    this.w_OK_COMM = .T.
    if g_PERCCR="S" AND this.w_MVTIPRIG<>"D" AND this.w_FLANAL="S"
      * --- Voce di Ricavo (campo ARVOCRIC degli Articoli)
      this.w_MVVOCCEN = this.w_MVVOCCOS
      this.w_MVFLELAN = this.w_FLELAN
      if NOT EMPTY(this.w_MVVOCCEN) OR this.w_MVTIPRIG $ "MF"
        * --- Lettura dai Parametri Vendita del Centro di Costo
        this.w_MVCODCEN = this.w_CODCEN
      endif
      this.w_MVCODCOM = IIF(g_COMM="S", t_MVCODCOM, SPACE(15))
      this.w_MVIMPCOM = IIF(g_COMM="S", t_MVIMPCOM, 0)
      this.w_MVCODATT = IIF(g_COMM="S", t_MVCODATT, SPACE(15))
      this.w_MVFLORCO = t_MVFLORCO
      this.w_MVFLCOCO = t_MVFLCOCO
      this.w_OK_ANALI = NOT EMPTY(this.w_MVCODCEN) AND NOT EMPTY(this.w_MVVOCCEN)
      if NOT (this.w_OK_ANALI)
        * --- Raise
        i_Error=ah_MsgFormat("Specificare voce di ricavo per articolo %1 e centro di ricavo nei parametri P.O.S.", ALLTRIM(this.w_MVCODART) )
        return
      endif
      this.w_OK_COMM = g_COMM<>"S" or this.w_MFLCOMM<>"S" or not empty(this.w_MVCODCOM) and not empty(this.w_MVCODATT)
    endif
    if NOT EMPTY(this.w_MVCODAGE)
      * --- Se siamo nei calcoli finali calcola l'importo provvigione
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGSCOPAG"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_MVCODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGSCOPAG;
          from (i_cTable) where;
              AGCODAGE = this.w_MVCODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AGSCOPAG = NVL(cp_ToDate(_read_.AGSCOPAG),cp_NullValue(_read_.AGSCOPAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IMPPRO = this.w_MVVALMAG
      * --- Se il flag Sconto Pagamento  non � attivo calcola al lordo
      if this.w_IMPPRO<>0 AND this.w_AGSCOPAG<>"S"
        this.w_IMPPRO = (100 * this.w_IMPPRO) / (100 + this.w_MVSCOPAG)
        this.w_MVIMPPRO = cp_ROUND((this.w_IMPPRO*this.w_MVPERPRO/100),this.w_DECTOT)
      endif
    endif
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    * --- Rivalorizzo TMP_MATR con le chiavi effettive del documento generato
    *     solo se mivimenta il Magazzino
    if this.w_MDTIPCHI<>"FF"
      this.w_RECNO = RecNo("GeneApp")
      * --- Write into TMP_MATR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMP_MATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_MATR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_MATR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DOCROW ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TMP_MATR','DOCROW');
        +",DOCSER ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'TMP_MATR','DOCSER');
            +i_ccchkf ;
        +" where ";
            +"DOCROW = "+cp_ToStrODBC(this.w_RECNO);
            +" and DOCSER = "+cp_ToStrODBC("XXXXX");
               )
      else
        update (i_cTable) set;
            DOCROW = this.w_CPROWNUM;
            ,DOCSER = this.w_MVSERIAL;
            &i_ccchkf. ;
         where;
            DOCROW = this.w_RECNO;
            and DOCSER = "XXXXX";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Try
    local bErr_04883488
    bErr_04883488=bTrsErr
    this.Try_04883488()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_MsgFormat("Errore in inserimento DOC_DETT%0%1", MESSAGE())
      return
    endif
    bTrsErr=bTrsErr or bErr_04883488
    * --- End
    * --- Try
    local bErr_04883E48
    bErr_04883E48=bTrsErr
    this.Try_04883E48()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_MsgFormat("Errore in scrittura DOC_DETT%0%1", MESSAGE())
      return
    endif
    bTrsErr=bTrsErr or bErr_04883E48
    * --- End
    * --- Modifica chiave tabella matricole che si sposta dal POS ai documenti solo 
    *     se mivimenta il Magazzino
    if this.w_MDTIPCHI<>"FF"
      * --- Insert into MATR_POS
      i_nConn=i_TableProp[this.MATR_POS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MATR_POS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsps_qim",this.MATR_POS_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into MOVIMATR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MTSERIAL,MTROWNUM,MTNUMRIF"
        do vq_exec with 'gsps_qam',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MTSERIAL = _t2.DOCSER";
            +",MTROWNUM = _t2.DOCROW";
        +",MTNUMRIF ="+cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTNUMRIF');
            +i_ccchkf;
            +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
            +"MOVIMATR.MTSERIAL = _t2.DOCSER";
            +",MOVIMATR.MTROWNUM = _t2.DOCROW";
        +",MOVIMATR.MTNUMRIF ="+cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTNUMRIF');
            +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = t2.MTNUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
            +"MTSERIAL,";
            +"MTROWNUM,";
            +"MTNUMRIF";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DOCSER,";
            +"t2.DOCROW,";
            +cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTNUMRIF')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
            +"MTSERIAL = _t2.DOCSER";
            +",MTROWNUM = _t2.DOCROW";
        +",MTNUMRIF ="+cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTNUMRIF');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
                +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
                +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MTSERIAL = (select DOCSER from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MTROWNUM = (select DOCROW from "+i_cQueryTable+" where "+i_cWhere+")";
        +",MTNUMRIF ="+cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTNUMRIF');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into MOVIMATR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MTSERRIF,MTROWRIF,MTRIFNUM"
        do vq_exec with 'gsps1qam',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MOVIMATR.MTSERRIF = _t2.MTSERRIF";
                +" and "+"MOVIMATR.MTROWRIF = _t2.MTROWRIF";
                +" and "+"MOVIMATR.MTRIFNUM = _t2.MTRIFNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MTSERRIF = _t2.DOCSER";
            +",MTROWRIF = _t2.DOCROW";
        +",MTRIFNUM ="+cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTRIFNUM');
            +i_ccchkf;
            +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MOVIMATR.MTSERRIF = _t2.MTSERRIF";
                +" and "+"MOVIMATR.MTROWRIF = _t2.MTROWRIF";
                +" and "+"MOVIMATR.MTRIFNUM = _t2.MTRIFNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
            +"MOVIMATR.MTSERRIF = _t2.DOCSER";
            +",MOVIMATR.MTROWRIF = _t2.DOCROW";
        +",MOVIMATR.MTRIFNUM ="+cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTRIFNUM');
            +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MOVIMATR.MTSERRIF = t2.MTSERRIF";
                +" and "+"MOVIMATR.MTROWRIF = t2.MTROWRIF";
                +" and "+"MOVIMATR.MTRIFNUM = t2.MTRIFNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
            +"MTSERRIF,";
            +"MTROWRIF,";
            +"MTRIFNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DOCSER,";
            +"t2.DOCROW,";
            +cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTRIFNUM')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MOVIMATR.MTSERRIF = _t2.MTSERRIF";
                +" and "+"MOVIMATR.MTROWRIF = _t2.MTROWRIF";
                +" and "+"MOVIMATR.MTRIFNUM = _t2.MTRIFNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
            +"MTSERRIF = _t2.DOCSER";
            +",MTROWRIF = _t2.DOCROW";
        +",MTRIFNUM ="+cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTRIFNUM');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MTSERRIF = "+i_cQueryTable+".MTSERRIF";
                +" and "+i_cTable+".MTROWRIF = "+i_cQueryTable+".MTROWRIF";
                +" and "+i_cTable+".MTRIFNUM = "+i_cQueryTable+".MTRIFNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MTSERRIF = (select DOCSER from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MTROWRIF = (select DOCROW from "+i_cQueryTable+" where "+i_cWhere+")";
        +",MTRIFNUM ="+cp_NullLink(cp_ToStrODBC(-20),'MOVIMATR','MTRIFNUM');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    SELECT GeneApp
    * --- I Saldi vengono aggiornati solo per U.P.V. aggiornati (Agg.to Esistenza gia' eseguito dalla Vendita negozio).
    if this.w_MVFLULPV = "="
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLVALUPV ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALULT),'SALDIART','SLVALUPV');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
               )
      else
        update (i_cTable) set;
            SLVALUPV = this.w_MVVALULT;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_MVKEYSAL;
            and SLCODMAG = this.w_MVCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_FLPACK="S"
      * --- Per gestione Packing List aggiorno nel cursore GeneApp i campi CPROWNUM e t _CPROWORD 
      *     con quelli effettivamente inseriti nel documento
       
 REPLACE CPROWNUM WITH this.w_CPROWNUM 
 REPLACE t_CPROWORD WITH this.w_CPROWORD
    endif
    SELECT GeneApp
    ENDSCAN
    if this.w_FLPACK="S"
      * --- Se causale con attivo il check Packing list, lancio lo stesso batch utilizzato dai documenti per il calcolo colli
      *     In questo caso l'elaborazione verr� effettuata sul cursore GENEAPP invece che sul transitorio
      GSVE_BC2(this,"V")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiorno i Colli e i pesi sul documento generato
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVQTAPES ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAPES),'DOC_MAST','MVQTAPES');
        +",MVQTALOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTALOR),'DOC_MAST','MVQTALOR');
        +",MVQTACOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTACOL),'DOC_MAST','MVQTACOL');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVQTAPES = this.w_MVQTAPES;
            ,MVQTALOR = this.w_MVQTALOR;
            ,MVQTACOL = this.w_MVQTACOL;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if g_PROGEN= "S"
      this.w_SERPRO = this.w_MVSERIAL
      this.w_MASSGEN = "S"
      * --- Considero sempre come se precedentemente avesi avuto disattivo
      *     In questo modo mi aggiorna il documento generato nel modo corretto
      this.w_OLDCALPRO = "DI"
      * --- Read from PAR_PROV
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PPCALPRO,PPCALSCO,PPLISRIF"+;
          " from "+i_cTable+" PAR_PROV where ";
              +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PPCALPRO,PPCALSCO,PPLISRIF;
          from (i_cTable) where;
              PPCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PPCALPRO = NVL(cp_ToDate(_read_.PPCALPRO),cp_NullValue(_read_.PPCALPRO))
        this.w_PPCALSCO = NVL(cp_ToDate(_read_.PPCALSCO),cp_NullValue(_read_.PPCALSCO))
        this.w_PPLISRIF = NVL(cp_ToDate(_read_.PPLISRIF),cp_NullValue(_read_.PPLISRIF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do GSVE_BPP with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_CPROWNUM=0
      * --- Nessuna Riga nel Detail
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    else
      * --- Inserisco le contropartite pagamenti
      if Not Empty(this.w_CONPRE + this.w_CONCON + this.w_CONASS + this.w_CONCAR + this.w_CONFIN)
        if Not Empty(this.w_CONPRE + this.w_CONCON + this.w_CONASS + this.w_CONCAR + this.w_CONFIN)
          * --- Se ho del resto lo detraggo dal contante, se ho ulteriore resto lo ripartisco 
          *     tra gli altri incassi...
          *     Questo caso gestisce il caso in cui incasso pi� di quanto dovrei.
          if this.w_MRESTO>0
            this.w_ERESTO = this.w_MRESTO
            if this.w_TOTCON<>0
              if this.w_TOTCON>=this.w_ERESTO
                this.w_TOTCON = this.w_TOTCON - this.w_ERESTO 
                this.w_ERESTO = 0
              else
                this.w_ERESTO = this.w_ERESTO - this.w_TOTCON 
                this.w_TOTCON = 0
              endif
            endif
            * --- Se c'� ancora resto passo alla ripartizione...
            if this.w_ERESTO>0
              this.w_TRESTO = this.w_TOTASS + this.w_TOTCAR + this.w_TOTFIN
              if this.w_TRESTO <>0
                this.w_TOTASS = this.w_TOTASS - cp_Round( ( this.w_ERESTO * this.w_TOTASS / this.w_TRESTO ) , this.w_DECTOT )
                this.w_TOTCAR = this.w_TOTCAR - cp_Round( ( this.w_ERESTO * this.w_TOTCAR / this.w_TRESTO ) , this.w_DECTOT )
                this.w_TOTFIN = this.w_TOTFIN - cp_Round( ( this.w_ERESTO * this.w_TOTFIN / this.w_TRESTO ) , this.w_DECTOT )
                * --- Se c'� del resto lo metto sull primo importo diverso da 0
                if this.w_TRESTO - ( this.w_TOTASS + this.w_TOTCAR + this.w_TOTFIN ) <> this.w_ERESTO 
                  this.w_TOTASS = this.w_TOTASS + iif( this.w_TOTASS=0 , 0 , this.w_TRESTO - ( this.w_TOTASS + this.w_TOTCAR + this.w_TOTFIN ) - this.w_ERESTO )
                  if this.w_TRESTO - ( this.w_TOTASS + this.w_TOTCAR + this.w_TOTFIN ) <> this.w_ERESTO 
                    this.w_TOTCAR = this.w_TOTCAR + iif( this.w_TOTCAR=0 ,0 , this.w_TRESTO - ( this.w_TOTASS + this.w_TOTCAR + this.w_TOTFIN ) - this.w_ERESTO )
                    if this.w_TRESTO - ( this.w_TOTASS + this.w_TOTCAR + this.w_TOTFIN ) <> this.w_ERESTO 
                      this.w_TOTFIN = this.w_TOTFIN + iif( this.w_TOTFIN=0, 0 , this.w_TRESTO - ( this.w_TOTASS + this.w_TOTCAR + this.w_TOTFIN ) - this.w_ERESTO )
                    endif
                  endif
                endif
              endif
            endif
          endif
          * --- Inserisco la Contropartita solo se c'� un importo relativo
          *     Se la contropartita � vuota � utilizzata la contropartita Conto Acconti
          *     Incassati nella contropartite vendite
          this.w_CONPRE = IIF(this.w_TOTPRE<>0, IIF( Empty( this.w_CONPRE ) , this.w_COCINC , this.w_CONPRE ), Space(15) )
          this.w_CONCON = IIF(this.w_TOTCON<>0, IIF( Empty( this.w_CONCON ) , this.w_COCINC , this.w_CONCON ), Space(15) )
          this.w_CONASS = IIF(this.w_TOTASS<>0, IIF( Empty( this.w_CONASS ) , this.w_COCINC , this.w_CONASS ), Space(15) )
          this.w_CONCAR = IIF(this.w_TOTCAR<>0, IIF( Empty( this.w_CONCAR ) , this.w_COCINC , this.w_CONCAR ) , Space(15) )
          this.w_CONFIN = IIF(this.w_TOTFIN<>0, IIF( Empty( this.w_CONFIN ) , this.w_COCINC , this.w_CONFIN ), Space(15) )
          * --- Insert into CON_PAGA
          i_nConn=i_TableProp[this.CON_PAGA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_PAGA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_PAGA_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CPSERIAL"+",CPCONPRE"+",CPIMPPRE"+",CPCONCON"+",CPIMPCON"+",CPCONASS"+",CPIMPASS"+",CPCONCAR"+",CPIMPCAR"+",CPCONFIN"+",CPIMPFIN"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'CON_PAGA','CPSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CONPRE),'CON_PAGA','CPCONPRE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTPRE),'CON_PAGA','CPIMPPRE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CONCON),'CON_PAGA','CPCONCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTCON),'CON_PAGA','CPIMPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CONASS),'CON_PAGA','CPCONASS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTASS),'CON_PAGA','CPIMPASS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CONCAR),'CON_PAGA','CPCONCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTCAR),'CON_PAGA','CPIMPCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CONFIN),'CON_PAGA','CPCONFIN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTFIN),'CON_PAGA','CPIMPFIN');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CPSERIAL',this.w_MVSERIAL,'CPCONPRE',this.w_CONPRE,'CPIMPPRE',this.w_TOTPRE,'CPCONCON',this.w_CONCON,'CPIMPCON',this.w_TOTCON,'CPCONASS',this.w_CONASS,'CPIMPASS',this.w_TOTASS,'CPCONCAR',this.w_CONCAR,'CPIMPCAR',this.w_TOTCAR,'CPCONFIN',this.w_CONFIN,'CPIMPFIN',this.w_TOTFIN)
            insert into (i_cTable) (CPSERIAL,CPCONPRE,CPIMPPRE,CPCONCON,CPIMPCON,CPCONASS,CPIMPASS,CPCONCAR,CPIMPCAR,CPCONFIN,CPIMPFIN &i_ccchkf. );
               values (;
                 this.w_MVSERIAL;
                 ,this.w_CONPRE;
                 ,this.w_TOTPRE;
                 ,this.w_CONCON;
                 ,this.w_TOTCON;
                 ,this.w_CONASS;
                 ,this.w_TOTASS;
                 ,this.w_CONCAR;
                 ,this.w_TOTCAR;
                 ,this.w_CONFIN;
                 ,this.w_TOTFIN;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
      this.w_TOTDOC = 0
      this.w_MINRATA = 0
      * --- Cicla Sul Vettore delle Rate (Calcolato in GSAR_BFA)
      FOR l_i=1 TO 999
      if NOT EMPTY(DR[l_i, 1]) AND NOT EMPTY(DR[l_i, 2])
        this.w_RSNUMRAT = l_i
        this.w_RSDATRAT = DR[l_i, 1]
        this.w_RSIMPRAT = DR[l_i, 2]
        this.w_RSMODPAG = DR[l_i, 3]
        this.w_TOTDOC = this.w_TOTDOC + this.w_RSIMPRAT
        this.w_MINRATA = MIN(this.w_MINRATA, this.w_RSIMPRAT)
        * --- Scrive Doc_Rate
        * --- Try
        local bErr_048D6660
        bErr_048D6660=bTrsErr
        this.Try_048D6660()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error=ah_MsgFormat("Errore in inserimento DOC_RATE%0%1", MESSAGE())
          return
        endif
        bTrsErr=bTrsErr or bErr_048D6660
        * --- End
      endif
      NEXT
      if this.w_TOTDOC<0 && oppure verifica per singola rata: this.w_MINRATA<0
        * --- Documento non Accettato
        if this.w_TOTDOC<0
          this.w_MSGRET = "importo totale negativo"
        else
          this.w_MSGRET = "rate con importo negativo"
        endif
        * --- Raise
        i_Error=ah_msgformat("Errore inserimento rate: %1", this.w_MSGRET)
        return
      endif
      * --- Aggiorna Riferimento sul Documento di Vendita Negozio
      if USED("RifReg")
        SELECT RifReg
        GO TOP
        SCAN FOR NOT EMPTY(MDSERIAL)
        this.oParentObject.w_OKREG = this.oParentObject.w_OKREG + 1
        this.w_MDSERIAL = MDSERIAL
        if this.oParentObject.w_TIPOPE="D" Or (this.oParentObject.w_TIPOPE="I" And this.w_MDTIPCHI="RF")
          * --- Differita o Immediata (Corrispettivi): Aggiorna MDRIFCOR
          if this.w_MDTIPCHI="FF"
            * --- Se Fattura Fiscale non scrive il Numero DOcumento (gia' inserito in Immediata)
            * --- Write into COR_RISP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COR_RISP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MDRIFCOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'COR_RISP','MDRIFCOR');
                  +i_ccchkf ;
              +" where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.w_MDSERIAL);
                     )
            else
              update (i_cTable) set;
                  MDRIFCOR = this.w_MVSERIAL;
                  &i_ccchkf. ;
               where;
                  MDSERIAL = this.w_MDSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into COR_RISP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COR_RISP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MDRIFCOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'COR_RISP','MDRIFCOR');
              +",MDNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'COR_RISP','MDNUMDOC');
              +",MDALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'COR_RISP','MDALFDOC');
                  +i_ccchkf ;
              +" where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.w_MDSERIAL);
                     )
            else
              update (i_cTable) set;
                  MDRIFCOR = this.w_MVSERIAL;
                  ,MDNUMDOC = this.w_MVNUMDOC;
                  ,MDALFDOC = this.w_MVALFDOC;
                  &i_ccchkf. ;
               where;
                  MDSERIAL = this.w_MDSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          * --- Immediata: Aggiorna MDRIFDOC e Riferimenti Documento
          if this.oParentObject.w_TIPOPE="G" 
            * --- Se generazione Documento da Corrispettivi Aggiorna anche i Riferimenti al Documento e Cliente ...
            * --- Write into COR_RISP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COR_RISP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MDRIFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'COR_RISP','MDRIFDOC');
              +",MDNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'COR_RISP','MDNUMDOC');
              +",MDALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'COR_RISP','MDALFDOC');
              +",MDTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MDTIPDOC),'COR_RISP','MDTIPDOC');
              +",MDCODCLI ="+cp_NullLink(cp_ToStrODBC(this.w_MDCODCLI),'COR_RISP','MDCODCLI');
              +",MDCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MDCODPAG),'COR_RISP','MDCODPAG');
              +",MDTIPCHI ="+cp_NullLink(cp_ToStrODBC(this.w_MDTIPCHI),'COR_RISP','MDTIPCHI');
              +",MDPRD ="+cp_NullLink(cp_ToStrODBC(this.w_MVPRD),'COR_RISP','MDPRD');
                  +i_ccchkf ;
              +" where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.w_MDSERIAL);
                     )
            else
              update (i_cTable) set;
                  MDRIFDOC = this.w_MVSERIAL;
                  ,MDNUMDOC = this.w_MVNUMDOC;
                  ,MDALFDOC = this.w_MVALFDOC;
                  ,MDTIPDOC = this.w_MDTIPDOC;
                  ,MDCODCLI = this.w_MDCODCLI;
                  ,MDCODPAG = this.w_MDCODPAG;
                  ,MDTIPCHI = this.w_MDTIPCHI;
                  ,MDPRD = this.w_MVPRD;
                  &i_ccchkf. ;
               where;
                  MDSERIAL = this.w_MDSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into COR_RISP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COR_RISP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MDRIFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'COR_RISP','MDRIFDOC');
              +",MDNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'COR_RISP','MDNUMDOC');
              +",MDALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'COR_RISP','MDALFDOC');
                  +i_ccchkf ;
              +" where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.w_MDSERIAL);
                     )
            else
              update (i_cTable) set;
                  MDRIFDOC = this.w_MVSERIAL;
                  ,MDNUMDOC = this.w_MVNUMDOC;
                  ,MDALFDOC = this.w_MVALFDOC;
                  &i_ccchkf. ;
               where;
                  MDSERIAL = this.w_MDSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        ENDSCAN
      endif
      this.oParentObject.w_NUDOC = this.oParentObject.w_NUDOC + 1
      * --- commit
      cp_EndTrs(.t.)
    endif
    return
  proc Try_0483E3B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVALFDOC"+",MVANNDOC"+",MVCAUCON"+",MVCLADOC"+",MVCODAGE"+",MVCODCON"+",MVCODDES"+",MVCODESE"+",MVCODPOR"+",MVCODSPE"+",MVCODUTE"+",MVCODVET"+",MVCONCON"+",MVDATCIV"+",MVDATDOC"+",MVDATREG"+",MVFLACCO"+",MVFLCONT"+",MVFLGIOM"+",MVFLINTE"+",MVFLPROV"+",MVFLSALD"+",MVFLSCOR"+",MVFLVEAC"+",MVEMERIC"+",MVGENEFF"+",MVGENPRO"+",MVNUMDOC"+",MVNUMREG"+",MVPRD"+",MVRIFDIC"+",MVSERIAL"+",MVTCAMAG"+",MVTCOLIS"+",MVTCONTR"+",MVTFRAGG"+",MVTIPCON"+",MVTIPDOC"+",MVTIPORN"+",MVDATPLA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DOC_MAST','MVALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVANNDOC),'DOC_MAST','MVANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCON),'DOC_MAST','MVCAUCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODAGE),'DOC_MAST','MVCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DOC_MAST','MVCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODDES),'DOC_MAST','MVCODDES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODESE),'DOC_MAST','MVCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODPOR),'DOC_MAST','MVCODPOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODSPE),'DOC_MAST','MVCODSPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUTE),'DOC_MAST','MVCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVET),'DOC_MAST','MVCODVET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONCON),'DOC_MAST','MVCONCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATCIV),'DOC_MAST','MVDATCIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATREG),'DOC_MAST','MVDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLACCO),'DOC_MAST','MVFLACCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLINTE),'DOC_MAST','MVFLINTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLPROV),'DOC_MAST','MVFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSALD),'DOC_MAST','MVFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVEMERIC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENEFF');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMREG),'DOC_MAST','MVNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPRD),'DOC_MAST','MVPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DOC_MAST','MVRIFDIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_MAST','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCONTR),'DOC_MAST','MVTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'DOC_MAST','MVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'DOC_MAST','MVTIPORN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATPLA),'DOC_MAST','MVDATPLA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVALFDOC',this.w_MVALFDOC,'MVANNDOC',this.w_MVANNDOC,'MVCAUCON',this.w_MVCAUCON,'MVCLADOC',this.w_MVCLADOC,'MVCODAGE',this.w_MVCODAGE,'MVCODCON',this.w_MVCODCON,'MVCODDES',this.w_MVCODDES,'MVCODESE',this.w_MVCODESE,'MVCODPOR',this.w_MVCODPOR,'MVCODSPE',this.w_MVCODSPE,'MVCODUTE',this.w_MVCODUTE,'MVCODVET',this.w_MVCODVET)
      insert into (i_cTable) (MVALFDOC,MVANNDOC,MVCAUCON,MVCLADOC,MVCODAGE,MVCODCON,MVCODDES,MVCODESE,MVCODPOR,MVCODSPE,MVCODUTE,MVCODVET,MVCONCON,MVDATCIV,MVDATDOC,MVDATREG,MVFLACCO,MVFLCONT,MVFLGIOM,MVFLINTE,MVFLPROV,MVFLSALD,MVFLSCOR,MVFLVEAC,MVEMERIC,MVGENEFF,MVGENPRO,MVNUMDOC,MVNUMREG,MVPRD,MVRIFDIC,MVSERIAL,MVTCAMAG,MVTCOLIS,MVTCONTR,MVTFRAGG,MVTIPCON,MVTIPDOC,MVTIPORN,MVDATPLA &i_ccchkf. );
         values (;
           this.w_MVALFDOC;
           ,this.w_MVANNDOC;
           ,this.w_MVCAUCON;
           ,this.w_MVCLADOC;
           ,this.w_MVCODAGE;
           ,this.w_MVCODCON;
           ,this.w_MVCODDES;
           ,this.w_MVCODESE;
           ,this.w_MVCODPOR;
           ,this.w_MVCODSPE;
           ,this.w_MVCODUTE;
           ,this.w_MVCODVET;
           ,this.w_MVCONCON;
           ,this.w_MVDATCIV;
           ,this.w_MVDATDOC;
           ,this.w_MVDATREG;
           ,this.w_MVFLACCO;
           ," ";
           ," ";
           ,this.w_MVFLINTE;
           ,this.w_MVFLPROV;
           ,this.w_MVFLSALD;
           ,this.w_MVFLSCOR;
           ,this.w_MVFLVEAC;
           ,this.w_MVFLVEAC;
           ," ";
           ," ";
           ,this.w_MVNUMDOC;
           ,this.w_MVNUMREG;
           ,this.w_MVPRD;
           ,this.w_MVRIFDIC;
           ,this.w_MVSERIAL;
           ,this.w_MVTCAMAG;
           ,this.w_MVTCOLIS;
           ,this.w_MVTCONTR;
           ,this.w_MVTFRAGG;
           ,this.w_MVTIPCON;
           ,this.w_MVTIPDOC;
           ,this.w_MVTIPCON;
           ,this.w_MVDATPLA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0483EB38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVNOTAGG');
      +",MVFLFOSC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLFOSC),'DOC_MAST','MVFLFOSC');
      +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFEST),'DOC_MAST','MVALFEST');
      +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVANNPRO),'DOC_MAST','MVANNPRO');
      +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
      +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBA2),'DOC_MAST','MVCODBA2');
      +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
      +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
      +",MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
      +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
      +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
      +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
      +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
      +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
      +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
      +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
      +",MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMEST),'DOC_MAST','MVNUMEST');
      +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPRP),'DOC_MAST','MVPRP');
      +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
      +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
      +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
      +",MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
      +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
      +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
      +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
      +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPETRA),'DOC_MAST','MVSPETRA');
      +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVNOTAGG = " ";
          ,MVFLFOSC = this.w_MVFLFOSC;
          ,MVALFEST = this.w_MVALFEST;
          ,MVANNPRO = this.w_MVANNPRO;
          ,MVCAOVAL = this.w_MVCAOVAL;
          ,MVCODBA2 = this.w_MVCODBA2;
          ,MVCODBAN = this.w_MVCODBAN;
          ,MVCODIVE = this.w_MVCODIVE;
          ,MVCODPAG = this.w_MVCODPAG;
          ,MVCODVAL = this.w_MVCODVAL;
          ,MVFLRIMB = this.w_MVFLRIMB;
          ,MVFLRINC = this.w_MVFLRINC;
          ,MVFLRTRA = this.w_MVFLRTRA;
          ,MVIVAIMB = this.w_MVIVAIMB;
          ,MVIVAINC = this.w_MVIVAINC;
          ,MVIVATRA = this.w_MVIVATRA;
          ,MVNUMEST = this.w_MVNUMEST;
          ,MVPRP = this.w_MVPRP;
          ,MVSCOCL1 = this.w_MVSCOCL1;
          ,MVSCOCL2 = this.w_MVSCOCL2;
          ,MVSCONTI = this.w_MVSCONTI;
          ,MVSCOPAG = this.w_MVSCOPAG;
          ,MVSPEBOL = this.w_MVSPEBOL;
          ,MVSPEIMB = this.w_MVSPEIMB;
          ,MVSPEINC = this.w_MVSPEINC;
          ,MVSPETRA = this.w_MVSPETRA;
          ,MVVALNAZ = this.w_MVVALNAZ;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0483F4C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLOFFE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLOFFE),'DOC_MAST','MVFLOFFE');
      +",MVORATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVORATRA),'DOC_MAST','MVORATRA');
      +",MVMINTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVMINTRA),'DOC_MAST','MVMINTRA');
      +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
      +",MVIMPARR ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPARR),'DOC_MAST','MVIMPARR');
      +",MVDATTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATTRA),'DOC_MAST','MVDATTRA');
      +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDIV),'DOC_MAST','MVDATDIV');
      +",MVCODAG2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODAG2),'DOC_MAST','MVCODAG2');
      +",MVASPEST ="+cp_NullLink(cp_ToStrODBC(this.w_MVASPEST),'DOC_MAST','MVASPEST');
      +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
      +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
      +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
      +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
      +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
      +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
      +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
      +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
      +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
      +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
      +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
      +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
      +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
      +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
      +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
      +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
      +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
      +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
      +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA6),'DOC_MAST','MVACIVA6');
      +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA5),'DOC_MAST','MVACIVA5');
      +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA4),'DOC_MAST','MVACIVA4');
      +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA3),'DOC_MAST','MVACIVA3');
      +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA2),'DOC_MAST','MVACIVA2');
      +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA1),'DOC_MAST','MVACIVA1');
      +",MVACCSUC ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCSUC),'DOC_MAST','MVACCSUC');
      +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
      +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCONT),'DOC_MAST','MVACCONT');
      +",MV__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_MV__NOTE),'DOC_MAST','MV__NOTE');
      +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDC');
      +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCC');
      +",MVDESDOC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVDESDOC');
      +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'DOC_MAST','MVNUMCOR');
      +",MVGENPOS ="+cp_NullLink(cp_ToStrODBC(this.w_GENPOS),'DOC_MAST','MVGENPOS');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVFLOFFE = this.w_MVFLOFFE;
          ,MVORATRA = this.w_MVORATRA;
          ,MVMINTRA = this.w_MVMINTRA;
          ,MVIVABOL = this.w_MVIVABOL;
          ,MVIMPARR = this.w_MVIMPARR;
          ,MVDATTRA = this.w_MVDATTRA;
          ,MVDATDIV = this.w_MVDATDIV;
          ,MVCODAG2 = this.w_MVCODAG2;
          ,MVASPEST = this.w_MVASPEST;
          ,MVAIMPS6 = this.w_MVAIMPS6;
          ,MVAIMPS5 = this.w_MVAIMPS5;
          ,MVAIMPS4 = this.w_MVAIMPS4;
          ,MVAIMPS3 = this.w_MVAIMPS3;
          ,MVAIMPS2 = this.w_MVAIMPS2;
          ,MVAIMPS1 = this.w_MVAIMPS1;
          ,MVAIMPN6 = this.w_MVAIMPN6;
          ,MVAIMPN5 = this.w_MVAIMPN5;
          ,MVAIMPN4 = this.w_MVAIMPN4;
          ,MVAIMPN3 = this.w_MVAIMPN3;
          ,MVAIMPN2 = this.w_MVAIMPN2;
          ,MVAIMPN1 = this.w_MVAIMPN1;
          ,MVAFLOM6 = this.w_MVAFLOM6;
          ,MVAFLOM5 = this.w_MVAFLOM5;
          ,MVAFLOM4 = this.w_MVAFLOM4;
          ,MVAFLOM3 = this.w_MVAFLOM3;
          ,MVAFLOM2 = this.w_MVAFLOM2;
          ,MVAFLOM1 = this.w_MVAFLOM1;
          ,MVACIVA6 = this.w_MVACIVA6;
          ,MVACIVA5 = this.w_MVACIVA5;
          ,MVACIVA4 = this.w_MVACIVA4;
          ,MVACIVA3 = this.w_MVACIVA3;
          ,MVACIVA2 = this.w_MVACIVA2;
          ,MVACIVA1 = this.w_MVACIVA1;
          ,MVACCSUC = this.w_MVACCSUC;
          ,MVACCPRE = this.w_MVACCPRE;
          ,MVACCONT = this.w_MVACCONT;
          ,MV__NOTE = this.w_MV__NOTE;
          ,UTDC = SetInfoDate( g_CALUTD );
          ,UTCC = i_CODUTE;
          ,MVDESDOC = " ";
          ,MVNUMCOR = this.w_NUMCOR;
          ,MVGENPOS = this.w_GENPOS;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04883488()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CPROWNUM"+",CPROWORD"+",MVCATCON"+",MVCAUMAG"+",MVCODART"+",MVCODCLA"+",MVCODICE"+",MVCODIVA"+",MVCODLIS"+",MVCONIND"+",MVCONTRA"+",MVCONTRO"+",MVDESART"+",MVDESSUP"+",MVF2LOTT"+",MVFLARIF"+",MVFLLOTT"+",MVFLOMAG"+",MVFLRAGG"+",MVFLTRAS"+",MVIMPACC"+",MVIMPNAZ"+",MVIMPPRO"+",MVIMPSCO"+",MVMOLSUP"+",MVNOMENC"+",MVNUMCOL"+",MVNUMRIF"+",MVPERPRO"+",MVPESNET"+",MVPREZZO"+",MVQTAMOV"+",MVQTASAL"+",MVQTAUM1"+",MVROWRIF"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVSERIAL"+",MVSERRIF"+",MVTIPRIG"+",MVUNIMIS"+",MVVALMAG"+",MVVALRIG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCONTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2LOTT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOL),'DOC_DETT','MVNUMCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'DOC_DETT','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'DOC_DETT','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MVCATCON',this.w_MVCATCON,'MVCAUMAG',this.w_MVCAUMAG,'MVCODART',this.w_MVCODART,'MVCODCLA',this.w_MVCODCLA,'MVCODICE',this.w_MVCODICE,'MVCODIVA',this.w_MVCODIVA,'MVCODLIS',this.w_MVCODLIS,'MVCONIND',this.w_MVCONIND,'MVCONTRA',this.w_MVCONTRA,'MVCONTRO',SPACE(15))
      insert into (i_cTable) (CPROWNUM,CPROWORD,MVCATCON,MVCAUMAG,MVCODART,MVCODCLA,MVCODICE,MVCODIVA,MVCODLIS,MVCONIND,MVCONTRA,MVCONTRO,MVDESART,MVDESSUP,MVF2LOTT,MVFLARIF,MVFLLOTT,MVFLOMAG,MVFLRAGG,MVFLTRAS,MVIMPACC,MVIMPNAZ,MVIMPPRO,MVIMPSCO,MVMOLSUP,MVNOMENC,MVNUMCOL,MVNUMRIF,MVPERPRO,MVPESNET,MVPREZZO,MVQTAMOV,MVQTASAL,MVQTAUM1,MVROWRIF,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVSERIAL,MVSERRIF,MVTIPRIG,MVUNIMIS,MVVALMAG,MVVALRIG &i_ccchkf. );
         values (;
           this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_MVCATCON;
           ,this.w_MVCAUMAG;
           ,this.w_MVCODART;
           ,this.w_MVCODCLA;
           ,this.w_MVCODICE;
           ,this.w_MVCODIVA;
           ,this.w_MVCODLIS;
           ,this.w_MVCONIND;
           ,this.w_MVCONTRA;
           ,SPACE(15);
           ,this.w_MVDESART;
           ,this.w_MVDESSUP;
           ," ";
           ," ";
           ,this.w_MVFLLOTT;
           ,this.w_MVFLOMAG;
           ,this.w_MVTFRAGG;
           ,this.w_MVFLTRAS;
           ,this.w_MVIMPACC;
           ,this.w_MVIMPNAZ;
           ,this.w_MVIMPPRO;
           ,this.w_MVIMPSCO;
           ,this.w_MVMOLSUP;
           ,this.w_MVNOMENC;
           ,this.w_NUMCOL;
           ,this.w_MVNUMRIF;
           ,this.w_MVPERPRO;
           ,this.w_MVPESNET;
           ,this.w_MVPREZZO;
           ,this.w_MVQTAMOV;
           ,this.w_MVQTAUM1;
           ,this.w_MVQTAUM1;
           ,this.w_MVROWRIF;
           ,this.w_MVSCONT1;
           ,this.w_MVSCONT2;
           ,this.w_MVSCONT3;
           ,this.w_MVSCONT4;
           ,this.w_MVSERIAL;
           ,this.w_MVSERRIF;
           ,this.w_MVTIPRIG;
           ,this.w_MVUNIMIS;
           ,this.w_MVVALMAG;
           ,this.w_MVVALRIG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='"+ah_msgformat("ERRORE IN INSERIMENTO DOC_DETT ")+CHR(13) +MESSAGE () +"'
      return
    endif
    return
  proc Try_04883E48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
      +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
      +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2CASC');
      +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2ORDI');
      +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2IMPE');
      +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2RISE');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
      +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
      +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
      +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
      +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
      +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVCODUB2');
      +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
      +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCAUCOL');
      +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
      +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELGM),'DOC_DETT','MVFLELGM');
      +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
      +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
      +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
      +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
      +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'DOC_DETT','MVFLCASC');
      +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLIMPE),'DOC_DETT','MVFLIMPE');
      +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORDI),'DOC_DETT','MVFLORDI');
      +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'DOC_DETT','MVFLRISE');
      +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
      +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
      +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
      +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALULT),'DOC_DETT','MVVALULT');
      +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLULPV),'DOC_DETT','MVFLULPV');
      +",MVCODCOL ="+cp_NullLink(cp_ToStrODBC(this.w_CODCOL),'DOC_DETT','MVCODCOL');
      +",MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
      +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELAN),'DOC_DETT','MVFLELAN');
      +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
      +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
      +",MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
      +",MVPROCAP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPROCAP),'DOC_DETT','MVPROCAP');
      +",MVFLORCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORCO),'DOC_DETT','MVFLORCO');
      +",MVFLCOCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCOCO),'DOC_DETT','MVFLCOCO');
      +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
             )
    else
      update (i_cTable) set;
          MVFLARIF = " ";
          ,MVFLERIF = " ";
          ,MVF2CASC = " ";
          ,MVFLRIPA = " ";
          ,MVFLEVAS = " ";
          ,MVF2ORDI = " ";
          ,MVF2IMPE = " ";
          ,MVF2RISE = " ";
          ,MVQTAEV1 = 0;
          ,MVQTAEVA = 0;
          ,MVQTAIMP = 0;
          ,MVIMPEVA = 0;
          ,MVQTAIM1 = 0;
          ,MVCODLOT = this.w_MVCODLOT;
          ,MVCODUB2 = SPACE(20);
          ,MVCODUBI = this.w_MVCODUBI;
          ,MVCAUCOL = SPACE(5);
          ,MVCODMAT = SPACE(5);
          ,MVFLELGM = this.w_MVFLELGM;
          ,MVCAUMAG = this.w_MVCAUMAG;
          ,MVCODATT = this.w_MVCODATT;
          ,MVCODCEN = this.w_MVCODCEN;
          ,MVCODCOM = this.w_MVCODCOM;
          ,MVCODMAG = this.w_MVCODMAG;
          ,MVDATEVA = this.w_MVDATEVA;
          ,MVFLCASC = this.w_MVFLCASC;
          ,MVFLIMPE = this.w_MVFLIMPE;
          ,MVFLORDI = this.w_MVFLORDI;
          ,MVFLRISE = this.w_MVFLRISE;
          ,MVKEYSAL = this.w_MVKEYSAL;
          ,MVTIPATT = this.w_MVTIPATT;
          ,MVVOCCEN = this.w_MVVOCCEN;
          ,MVVALULT = this.w_MVVALULT;
          ,MVFLULPV = this.w_MVFLULPV;
          ,MVCODCOL = this.w_CODCOL;
          ,MVTIPPRO = this.w_MVTIPPRO;
          ,MVFLELAN = this.w_MVFLELAN;
          ,MV_SEGNO = this.w_MV_SEGNO;
          ,MVIMPCOM = this.w_MVIMPCOM;
          ,MVTIPPR2 = this.w_MVTIPPR2;
          ,MVPROCAP = this.w_MVPROCAP;
          ,MVFLORCO = this.w_MVFLORCO;
          ,MVFLCOCO = this.w_MVFLCOCO;
          ,MVLOTMAG = this.w_MVLOTMAG;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;
          and CPROWNUM = this.w_CPROWNUM;
          and MVNUMRIF = this.w_MVNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='"+ah_msgformat(ERRORE IN SCRITTURA DOC_DETT")+CHR(13) +MESSAGE () +"'
      return
    endif
    return
  proc Try_048D6660()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_RATE
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_RATE','RSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSFLSOSP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.w_MVSERIAL,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP'," ")
      insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP &i_ccchkf. );
         values (;
           this.w_MVSERIAL;
           ,this.w_RSNUMRAT;
           ,this.w_RSDATRAT;
           ,this.w_RSIMPRAT;
           ,this.w_RSMODPAG;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Raggruppa per Articolo
    * --- Salva il Contenuto in Cursore appoggio
    if USED("AppRagg")
      SELECT AppRagg
      USE
    endif
    SELECT * FROM GeneApp INTO CURSOR AppRagg
    Cur = WrCursor ("AppRagg")
    * --- Azzera il Temporaneo di Appoggio
    * --- I campi t_UNMIS1, t_UNMIS2, t_UNMIS3, t_CPROWORD, CPROWNUM, t_CODCONF, t_MVNUMCOL, t_MVCODCOL
    *     sono stati inseriti poich� nel caso di documento con packing list il cursore viene utilizzato nel batch GSVE_BC2 al posto del transitorio 
    *     per il calcolo dei colli e dei pesi quindi il cursore deve essere congruente con il cTrsName dei documenti
    CREATE CURSOR GeneApp ;
    (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
    t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ;
    t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ;
    t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ;
    t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), t_MVSCOVEN N(18,4), ;
    t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ;
    t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ;
    t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ;
    t_MVCODMAG C(5),t_MVLOTMAG C(5), t_MVCODCEN C(15), t_MVVOCCOS C(15), t_MVSERRIF C(10), t_MVROWRIF N(4,0), ;
    t_MVCODCOM C(15), t_MVTIPATT C(1), t_MVCODATT C(15), t_MVDATEVA D(8), t_INDIVA N(3,0), t_FLSERA C(1), t_FLRESO C(1), ;
    t_UNMIS1 C(3), t_UNMIS2 C(3), t_UNMIS3 C(3), t_CPROWORD N(5), CPROWNUM N(4), t_CODCONF C(3), t_MVNUMCOL N(5), t_MVCODCOL C(5), ;
    t_MVTIPPRO C(2), t_MVPERPRO N(5,2), t_CODPRO C(15) , t_MVIMPCOM N(18,4), t_MVIMPAC2 N(18,4), ;
    t_MVCODLOT C(20), t_MVCODUBI C(20), t_MVFLLOTT C(1), t_POSSER C(10), t_POSROW N(4), t_MVTIPPR2 C(2), t_MVPROCAP N(5,2), t_MVFLORCO C(1) , t_MVFLCOCO C(1), t_MATRSN C(1) )
    * --- Quindi lo Ricostruisce raggruppando tutti gi Articoli Congruenti
    this.w_UPDA = .F.
    FOR GIRO=1 TO 2
    * --- Prima Aggiorna le Righe descrittive, poi tutte le altre
    SELECT AppRagg
    GO TOP
    SCAN FOR NOT EMPTY(t_MVTIPRIG)
    this.w_FLINSE = .T.
    if (t_MVTIPRIG="D" AND GIRO=1) OR (t_MVTIPRIG<>"D" AND GIRO=2)
      this.w_MVTIPRIG = t_MVTIPRIG
      this.w_MVCODICE = t_MVCODICE
      this.w_MVCODART = t_MVCODART
      this.w_MVDESART = t_MVDESART
      this.w_MVDESSUP = t_MVDESSUP
      this.w_MVUNIMIS = t_MVUNIMIS
      this.w_UNMIS1 = t_UNMIS1
      this.w_UNMIS2 = t_UNMIS2
      this.w_UNMIS3 = t_UNMIS3
      this.w_NUMCOL = t_MVNUMCOL
      this.w_CODCONF = t_CODCONF
      this.w_MVCATCON = t_MVCATCON
      this.w_MVCODCLA = t_MVCODCLA
      this.w_MVCONTRA = t_MVCONTRA
      this.w_MVCODLIS = t_MVCODLIS
      this.w_MVQTAMOV = t_MVQTAMOV
      this.w_MVQTAUM1 = t_MVQTAUM1
      this.w_MVPREZZO = t_MVPREZZO
      this.w_MVSCONT1 = t_MVSCONT1
      this.w_MVSCONT2 = t_MVSCONT2
      this.w_MVSCONT3 = t_MVSCONT3
      this.w_MVSCONT4 = t_MVSCONT4
      this.w_MVSCOVEN = t_MVSCOVEN
      this.w_MVFLOMAG = t_MVFLOMAG
      this.w_MVCODIVA = t_MVCODIVA
      this.w_PERIVA = t_PERIVA
      this.w_BOLIVA = t_BOLIVA
      this.w_MVVALRIG = t_MVVALRIG
      this.w_MVPESNET = t_MVPESNET
      this.w_MVNOMENC = t_MVNOMENC
      this.w_MVUMSUPP = t_MVUMSUPP
      this.w_MVMOLSUP = t_MVMOLSUP
      this.w_MVIMPACC = t_MVIMPACC
      this.w_MVIMPSCO = t_MVIMPSCO
      this.w_MVVALMAG = t_MVVALMAG
      this.w_MVIMPNAZ = t_MVIMPNAZ
      this.w_MVCODMAG = t_MVCODMAG
      this.w_MVLOTMAG = t_MVLOTMAG
      this.w_MVCODCEN = t_MVCODCEN
      this.w_MVVOCCOS = t_MVVOCCOS
      this.w_MVSERRIF = t_MVSERRIF
      this.w_MVROWRIF = t_MVROWRIF
      this.w_MVCODCOM = t_MVCODCOM
      this.w_MVIMPCOM = t_MVIMPCOM
      this.w_MVTIPATT = t_MVTIPATT
      this.w_MVCODATT = t_MVCODATT
      this.w_MVFLORCO = t_MVFLORCO
      this.w_MVFLCOCO = t_MVFLCOCO
      this.w_MVDATEVA = t_MVDATEVA
      this.w_INDIVA = t_INDIVA
      this.w_FLSERA = t_FLSERA
      this.w_FLRESO = t_FLRESO
      this.w_MVTIPPRO = t_MVTIPPRO
      this.w_MVPERPRO = t_MVPERPRO
      this.w_CODPRO = t_CODPRO
      this.w_MVCODLOT = t_MVCODLOT
      this.w_MVCODUBI = t_MVCODUBI
      this.w_MVFLLOTT = t_MVFLLOTT
      this.w_SERIAL = t_POSSER
      this.w_ROWNUM = t_POSROW
      this.w_MVTIPPR2 = t_MVTIPPR2
      this.w_MVPROCAP = t_MVPROCAP
      this.w_MATRSN = t_MATRSN
      do case
        case this.w_MVTIPRIG $ "RM"
          SELECT GeneApp
          GO TOP
          LOCATE FOR t_MVTIPRIG=this.w_MVTIPRIG AND t_MVCODICE=this.w_MVCODICE AND t_MVCODART=this.w_MVCODART AND ;
          t_MVUNIMIS=this.w_MVUNIMIS AND t_MVCATCON=this.w_MVCATCON AND ;
          t_MVCODCLA=this.w_MVCODCLA AND t_MVPREZZO=this.w_MVPREZZO AND t_FLRESO=this.w_FLRESO AND ;
          t_MVSCONT1=this.w_MVSCONT1 AND t_MVSCONT2=this.w_MVSCONT2 AND t_MVSCONT3=this.w_MVSCONT3 AND t_MVSCONT4=this.w_MVSCONT4 AND ;
          t_MVFLOMAG=this.w_MVFLOMAG AND t_MVCODIVA=this.w_MVCODIVA AND ;
          t_MVCODCEN=this.w_MVCODCEN AND t_MVCODCOM=this.w_MVCODCOM AND t_MVSERRIF=this.w_MVSERRIF AND t_MVROWRIF=this.w_MVROWRIF AND ;
          t_MVCODLOT=this.w_MVCODLOT AND t_MVCODUBI=this.w_MVCODUBI AND t_MVFLLOTT = this.w_MVFLLOTT AND t_MATRSN="N"
          this.w_FLINSE = NOT FOUND()
        case this.w_MVTIPRIG = "D" AND NOT EMPTY(this.w_CODPRO)
          SELECT GeneApp
          GO TOP
          LOCATE FOR t_MVTIPRIG=this.w_MVTIPRIG AND t_CODPRO=this.w_CODPRO AND t_MVSERRIF=this.w_MVSERRIF AND t_MVROWRIF=this.w_MVROWRIF
          this.w_FLINSE = NOT FOUND()
      endcase
      if this.w_FLINSE
        * --- Inserisce nuova Riga
        INSERT INTO GeneApp ;
        (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ;
        t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ;
        t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ;
        t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ;
        t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, t_MVSCOVEN, ;
        t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ;
        t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ;
        t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ;
        t_MVCODMAG,t_MVLOTMAG, t_MVCODCEN, t_MVVOCCOS, t_MVSERRIF, t_MVROWRIF, ;
        t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, t_INDIVA, t_FLSERA, t_FLRESO, ;
        t_UNMIS1, t_UNMIS2, t_UNMIS3, t_CPROWORD, CPROWNUM, t_CODCONF, t_MVNUMCOL, t_MVCODCOL, ;
        t_MVTIPPRO, t_MVPERPRO, t_CODPRO, t_MVIMPCOM, t_MVIMPAC2, ;
        t_MVCODLOT, t_MVCODUBI, t_MVFLLOTT, t_POSSER, t_POSROW, t_MVTIPPR2, t_MVPROCAP, t_MATRSN) ;
        VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ;
        this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ;
        this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ;
        this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ;
        this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, this.w_MVSCOVEN, ;
        this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ;
        this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ;
        this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ;
        this.w_MVCODMAG, this.w_MVLOTMAG,this.w_MVCODCEN, this.w_MVVOCCOS, this.w_MVSERRIF, this.w_MVROWRIF, ;
        this.w_MVCODCOM, this.w_MVTIPATT, this.w_MVCODATT, this.w_MVDATEVA, this.w_INDIVA, this.w_FLSERA, this.w_FLRESO, ;
        this.w_UNMIS1, this.w_UNMIS2, this.w_UNMIS3, 1, 1, this.w_CODCONF, 0 , this.w_CODCOL , ;
        this.w_MVTIPPRO, this.w_MVPERPRO, this.w_CODPRO, this.w_MVIMPCOM, 0, ;
        this.w_MVCODLOT, this.w_MVCODUBI, this.w_MVFLLOTT, this.w_SERIAL, this.w_ROWNUM, this.w_MVTIPPR2, this.w_MVPROCAP, this.w_MATRSN)
      else
        * --- Aggiunge alla Riga
        if this.w_MVTIPRIG<>"D"
          REPLACE t_MVQTAMOV WITH (t_MVQTAMOV + this.w_MVQTAMOV), t_MVQTAUM1 WITH (t_MVQTAUM1 + this.w_MVQTAUM1)
          REPLACE t_MVSCOVEN WITH (t_MVSCOVEN + this.w_MVSCOVEN)
          this.w_UPDA = .T.
        endif
      endif
      if this.w_MVTIPRIG<>"D" And this.w_MDTIPCHI <>"FF"
        this.w_RECNO = RecNo("GeneApp")
        * --- Insert into TMP_MATR
        i_nConn=i_TableProp[this.TMP_MATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_MATR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_MATR_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"POSSER"+",POSROW"+",DOCSER"+",DOCROW"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMP_MATR','POSSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'TMP_MATR','POSROW');
          +","+cp_NullLink(cp_ToStrODBC("XXXXX"),'TMP_MATR','DOCSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RECNO),'TMP_MATR','DOCROW');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'POSSER',this.w_SERIAL,'POSROW',this.w_ROWNUM,'DOCSER',"XXXXX",'DOCROW',this.w_RECNO)
          insert into (i_cTable) (POSSER,POSROW,DOCSER,DOCROW &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,this.w_ROWNUM;
               ,"XXXXX";
               ,this.w_RECNO;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    SELECT AppRagg
    ENDSCAN
    ENDFOR
    if this.w_UPDA
      * --- Se raggruppate Rige, Riaggiorna MVVALRIG
      SELECT GeneApp
      GO TOP
      SCAN FOR t_MVTIPRIG $ "RM"
      this.w_MVVALRIG = CAVALRIG(t_MVPREZZO,t_MVQTAMOV, t_MVSCONT1,t_MVSCONT2,t_MVSCONT3,t_MVSCONT4,this.w_DECTOT)
      this.w_MVIMPNAZ = cp_ROUND(t_MVQTAMOV*t_MVIMPNAZ, this.w_DECTOT)
      this.w_MVIMPACC = cp_ROUND(t_MVQTAMOV*t_MVIMPACC, this.w_DECTOT)
      this.w_MVIMPSCO = cp_ROUND(t_MVQTAMOV*t_MVIMPSCO, this.w_DECTOT)
      this.w_MVIMPCOM = cp_ROUND(t_MVQTAMOV*t_MVIMPCOM, this.w_DECTOT)
      REPLACE t_MVVALRIG WITH this.w_MVVALRIG
      REPLACE t_MVIMPNAZ WITH this.w_MVIMPNAZ
      REPLACE t_MVIMPACC WITH this.w_MVIMPACC
      REPLACE t_MVIMPSCO WITH this.w_MVIMPSCO
      REPLACE t_MVIMPCOM WITH this.w_MVIMPCOM
      ENDSCAN
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo se devo inserire il documento (E8196)
    this.w_MVNUMDOC_C = this.w_MVNUMDOC
    if this.w_MVPRD<>"NN" And !EMPTY(this.w_MVNUMDOC_C)
      * --- Controllo se il numero docuemnto � gi� stato utilizzato
      this.w_MVDATDOC_C = this.w_MVDATDOC
      this.w_MVANNDOC_C = this.w_MVANNDOC
      this.w_MVALFDOC_C = this.w_MVALFDOC
      this.w_MVTIPDOC_C = this.w_MVTIPDOC
      this.w_MVSERIAL_C = ""
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERIAL,MVDATDOC"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVNUMDOC = "+cp_ToStrODBC(this.w_MVNUMDOC_C);
              +" and MVALFDOC = "+cp_ToStrODBC(this.w_MVALFDOC_C);
              +" and MVANNDOC = "+cp_ToStrODBC(this.w_MVANNDOC_C);
              +" and MVPRD = "+cp_ToStrODBC(this.w_MVPRD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERIAL,MVDATDOC;
          from (i_cTable) where;
              MVNUMDOC = this.w_MVNUMDOC_C;
              and MVALFDOC = this.w_MVALFDOC_C;
              and MVANNDOC = this.w_MVANNDOC_C;
              and MVPRD = this.w_MVPRD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVSERIAL_C = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
        this.w_MVDATDOC_C = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_MVSERIAL_C)
        * --- Numero documento vuoto, non ho problemi di numerazione
        this.w_INSDOC = .T.
      else
        * --- Messaggio problema numerazione
        if this.w_AZFLUNIV = "S"
          Ah_ErrorMsg("Numero/alfa documento gi� inserito%0Documento n.%1 del %2 %0Impossibile generare", , , Transform(this.w_MVNUMDOC_C)+IIF(EMPTY(this.w_MVALFDOC_C),"","/"+Alltrim(this.w_MVALFDOC_C)), DTOC(this.w_MVDATDOC_C))
          this.w_INSDOC = .F.
        else
          this.w_INSDOC = Ah_YesNo("Numero/alfa documento gi� inserito%0Documento n.%1 del %2 %0Confermi generazione?", , Transform(this.w_MVNUMDOC_C)+IIF(EMPTY(this.w_MVALFDOC_C),"","/"+Alltrim(this.w_MVALFDOC_C)), DTOC(this.w_MVDATDOC_C))
        endif
      endif
    else
      * --- Numero documento vuoto, non ho problemi di numerazione
      this.w_INSDOC = .T.
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,25)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_RATE'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='COR_RISP'
    this.cWorkTables[6]='CAM_AGAZ'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='VOCIIVA'
    this.cWorkTables[9]='LISTINI'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='KEY_ARTI'
    this.cWorkTables[12]='DES_DIVE'
    this.cWorkTables[13]='PAR_VDET'
    this.cWorkTables[14]='ART_ICOL'
    this.cWorkTables[15]='AGENTI'
    this.cWorkTables[16]='PAG_2AME'
    this.cWorkTables[17]='CAU_CONT'
    this.cWorkTables[18]='CON_PAGA'
    this.cWorkTables[19]='*TMP_MATR'
    this.cWorkTables[20]='MOVIMATR'
    this.cWorkTables[21]='MATR_POS'
    this.cWorkTables[22]='PAR_PROV'
    this.cWorkTables[23]='CONTI'
    this.cWorkTables[24]='CONTROPA'
    this.cWorkTables[25]='AZIENDA'
    return(this.OpenAllTables(25))

  proc CloseCursors()
    if used('_Curs_PAG_2AME')
      use in _Curs_PAG_2AME
    endif
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
