* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_kfc                                                        *
*              Situazione Fidelity Card                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_158]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-13                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_kfc",oParentObject))

* --- Class definition
define class tgsps_kfc as StdForm
  Top    = 82
  Left   = 41

  * --- Standard Properties
  Width  = 565
  Height = 182
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=48846953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  FID_CARD_IDX = 0
  cPrg = "gsps_kfc"
  cComment = "Situazione Fidelity Card"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MDCODCLI = space(15)
  w_DESCLI = space(40)
  w_TOTIMP = 0
  w_IMPPRE = 0
  w_IMPRES = 0
  w_PUNFID = 0
  w_NUMVIS = 0
  w_DATATT = ctod('  /  /  ')
  w_DATSCA = ctod('  /  /  ')
  w_DATUAC = ctod('  /  /  ')
  w_CODFID = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_kfcPag1","gsps_kfc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FID_CARD'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MDCODCLI=space(15)
      .w_DESCLI=space(40)
      .w_TOTIMP=0
      .w_IMPPRE=0
      .w_IMPRES=0
      .w_PUNFID=0
      .w_NUMVIS=0
      .w_DATATT=ctod("  /  /  ")
      .w_DATSCA=ctod("  /  /  ")
      .w_DATUAC=ctod("  /  /  ")
      .w_CODFID=space(20)
      .w_MDCODCLI=oParentObject.w_MDCODCLI
      .w_DESCLI=oParentObject.w_DESCLI
          .DoRTCalc(1,10,.f.)
        .w_CODFID = this.oParentObject .w_MDCODFID
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODFID))
          .link_1_21('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MDCODCLI=.w_MDCODCLI
      .oParentObject.w_DESCLI=.w_DESCLI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
          .link_1_21('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODFID
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_lTable = "FID_CARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2], .t., this.FID_CARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFID) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFID)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATUAC,FCDATATT,FCDATSCA";
                   +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(this.w_CODFID);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',this.w_CODFID)
            select FCCODFID,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATUAC,FCDATATT,FCDATSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFID = NVL(_Link_.FCCODFID,space(20))
      this.w_IMPPRE = NVL(_Link_.FCIMPPRE,0)
      this.w_IMPRES = NVL(_Link_.FCIMPRES,0)
      this.w_TOTIMP = NVL(_Link_.FCTOTIMP,0)
      this.w_PUNFID = NVL(_Link_.FCPUNFID,0)
      this.w_NUMVIS = NVL(_Link_.FCNUMVIS,0)
      this.w_DATUAC = NVL(cp_ToDate(_Link_.FCDATUAC),ctod("  /  /  "))
      this.w_DATATT = NVL(cp_ToDate(_Link_.FCDATATT),ctod("  /  /  "))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.FCDATSCA),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFID = space(20)
      endif
      this.w_IMPPRE = 0
      this.w_IMPRES = 0
      this.w_TOTIMP = 0
      this.w_PUNFID = 0
      this.w_NUMVIS = 0
      this.w_DATUAC = ctod("  /  /  ")
      this.w_DATATT = ctod("  /  /  ")
      this.w_DATSCA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])+'\'+cp_ToStr(_Link_.FCCODFID,1)
      cp_ShowWarn(i_cKey,this.FID_CARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFID Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMDCODCLI_1_1.value==this.w_MDCODCLI)
      this.oPgFrm.Page1.oPag.oMDCODCLI_1_1.value=this.w_MDCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_2.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_2.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_1_3.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_1_3.value=this.w_TOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPPRE_1_14.value==this.w_IMPPRE)
      this.oPgFrm.Page1.oPag.oIMPPRE_1_14.value=this.w_IMPPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPRES_1_15.value==this.w_IMPRES)
      this.oPgFrm.Page1.oPag.oIMPRES_1_15.value=this.w_IMPRES
    endif
    if not(this.oPgFrm.Page1.oPag.oPUNFID_1_16.value==this.w_PUNFID)
      this.oPgFrm.Page1.oPag.oPUNFID_1_16.value=this.w_PUNFID
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMVIS_1_17.value==this.w_NUMVIS)
      this.oPgFrm.Page1.oPag.oNUMVIS_1_17.value=this.w_NUMVIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDATATT_1_18.value==this.w_DATATT)
      this.oPgFrm.Page1.oPag.oDATATT_1_18.value=this.w_DATATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCA_1_19.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oDATSCA_1_19.value=this.w_DATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATUAC_1_20.value==this.w_DATUAC)
      this.oPgFrm.Page1.oPag.oDATUAC_1_20.value=this.w_DATUAC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_kfcPag1 as StdContainer
  Width  = 561
  height = 182
  stdWidth  = 561
  stdheight = 182
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMDCODCLI_1_1 as StdField with uid="MGRROWXJYO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MDCODCLI", cQueryName = "MDCODCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 190177009,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=120, Top=12, InputMask=replicate('X',15)

  add object oDESCLI_1_2 as StdField with uid="EULVYAQRAA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 81845962,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=257, Top=12, InputMask=replicate('X',40)

  add object oTOTIMP_1_3 as StdField with uid="DCJCZFEZUB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TOTIMP", cQueryName = "TOTIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231392202,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=411, Top=73, cSayPict="v_PV(18)", cGetPict="v_PV(18)"


  add object oBtn_1_6 as StdButton with uid="KYQESPJULE",left=505, top=132, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 48818202;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oIMPPRE_1_14 as StdField with uid="UXTAAJRCXA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IMPPRE", cQueryName = "IMPPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 141821562,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=120, Top=73, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  add object oIMPRES_1_15 as StdField with uid="IZSGWYVRSW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IMPRES", cQueryName = "IMPRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 188876410,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=120, Top=98, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  add object oPUNFID_1_16 as StdField with uid="DFIRTCYKPQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PUNFID", cQueryName = "PUNFID",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168697354,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=411, Top=123, cSayPict='"999999"', cGetPict='"999999"'

  add object oNUMVIS_1_17 as StdField with uid="VKSJZIQUTL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMVIS", cQueryName = "NUMVIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 184430122,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=120, Top=123, cSayPict='"999999"', cGetPict='"999999"'

  add object oDATATT_1_18 as StdField with uid="XIPVUWKIIY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATATT", cQueryName = "DATATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 157471434,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=257, Top=37

  add object oDATSCA_1_19 as StdField with uid="KOYXCKTZVA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATSCA", cQueryName = "DATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 224449226,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=474, Top=38

  add object oDATUAC_1_20 as StdField with uid="IGEHPAIPEV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATUAC", cQueryName = "DATUAC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 192860874,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=411, Top=98

  add object oStr_1_4 as StdString with uid="MSDPIFIXFZ",Visible=.t., Left=46, Top=12,;
    Alignment=1, Width=73, Height=18,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="SIHESKLJJG",Visible=.t., Left=286, Top=74,;
    Alignment=1, Width=124, Height=18,;
    Caption="Totale acquisti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="GDCKJSEBNX",Visible=.t., Left=286, Top=124,;
    Alignment=1, Width=124, Height=18,;
    Caption="Punti Fidelity:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="UADZQBQQFM",Visible=.t., Left=7, Top=124,;
    Alignment=1, Width=112, Height=18,;
    Caption="Contatore visite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="MHJRCGSSUT",Visible=.t., Left=119, Top=38,;
    Alignment=1, Width=133, Height=18,;
    Caption="Data attivazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="IPRSVAKRLX",Visible=.t., Left=386, Top=39,;
    Alignment=1, Width=85, Height=18,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="JEDWXJIHTA",Visible=.t., Left=286, Top=99,;
    Alignment=1, Width=124, Height=18,;
    Caption="Ultimo acquisto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NRNSNRZGKM",Visible=.t., Left=7, Top=74,;
    Alignment=1, Width=112, Height=18,;
    Caption="Importo prepagato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="ACEYAJLQSG",Visible=.t., Left=7, Top=99,;
    Alignment=1, Width=112, Height=18,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_22 as StdBox with uid="ACKDVHEZVY",left=14, top=64, width=536,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_kfc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
