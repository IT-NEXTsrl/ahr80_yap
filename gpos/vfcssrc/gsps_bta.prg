* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bta                                                        *
*              Aggiornamento saldi commessa                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-02-25                                                      *
* Last revis.: 2016-08-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bta",oParentObject,m.pOPER)
return(i_retval)

define class tgsps_bta as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_OLDCOMME = space(15)
  w_OLDQTAUM1 = 0
  w_FLCASC = space(1)
  w_OLDCODMAG = space(5)
  w_OLDCODUBI = space(20)
  w_OLDCODLOT = space(20)
  w_CODART = space(20)
  w_OLDFCM = space(1)
  w_COMMDEFA = space(15)
  w_PunPAD = .NULL.
  * --- WorkFile variables
  DOC_DETT_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  SALOTCOM_idx=0
  CORDRISP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pOper = B (Aggiornamento Saldi Commessa)
    * --- Caller
    * --- Locali
    do case
      case this.pOPER="B"
        * --- Aggiornamento Saldi Commessa
        * --- Leggo il vecchio valore dal Documento
        * --- Read from CORDRISP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CORDRISP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CORDRISP_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MDCODCOM,MDQTAUM1,MDFLCASC,MDMAGSAL,MDCODART,MDCODLOT,MDCODUBI"+;
            " from "+i_cTable+" CORDRISP where ";
                +"MDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MDCODCOM,MDQTAUM1,MDFLCASC,MDMAGSAL,MDCODART,MDCODLOT,MDCODUBI;
            from (i_cTable) where;
                MDSERIAL = this.oParentObject.w_MDSERIAL;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDCOMME = NVL(cp_ToDate(_read_.MDCODCOM),cp_NullValue(_read_.MDCODCOM))
          this.w_OLDQTAUM1 = NVL(cp_ToDate(_read_.MDQTAUM1),cp_NullValue(_read_.MDQTAUM1))
          this.w_FLCASC = NVL(cp_ToDate(_read_.MDFLCASC),cp_NullValue(_read_.MDFLCASC))
          this.w_OLDCODMAG = NVL(cp_ToDate(_read_.MDMAGSAL),cp_NullValue(_read_.MDMAGSAL))
          this.w_CODART = NVL(cp_ToDate(_read_.MDCODART),cp_NullValue(_read_.MDCODART))
          this.w_OLDCODLOT = NVL(cp_ToDate(_read_.MDCODLOT),cp_NullValue(_read_.MDCODLOT))
          this.w_OLDCODUBI = NVL(cp_ToDate(_read_.MDCODUBI),cp_NullValue(_read_.MDCODUBI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDFCM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PunPAD = this.oParentObject
        * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
        this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
        if NVL(this.oParentObject.w_FLCOM1,"N")="S" or NVL(this.w_OLDFCM,"N")="S"
          if empty (nvl(this.oParentObject.w_MDCODCOM," "))
            this.oParentObject.w_MDCODCOM = this.w_COMMDEFA
          endif
          if empty (nvl(this.w_OLDCOMME," ")) 
            this.w_OLDCOMME = this.w_COMMDEFA
          endif
          * --- Inverto i valori dei flag letti per effettuare lo storno
          this.w_FLCASC = IIF(this.w_FLCASC="+", "-", IIF(this.w_FLCASC="-", "+", " "))
          if (nvl(this.oParentObject.w_MDCODCOM,"               "))=(nvl(this.w_OLDCOMME,"               ")) and NVL(this.w_OLDFCM,"N")="S" 
            if inlist(this.w_PunPAD.cFunction, "Query", "Edit")
              * --- Storno i Saldi Commessa
              if NOT EMPTY(this.w_FLCASC)
                * --- Magazzino principale
                * --- Try
                local bErr_02EC7870
                bErr_02EC7870=bTrsErr
                this.Try_02EC7870()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_02EC7870
                * --- End
                if g_MADV="S"
                  if (g_PERUBI = "S" OR g_PERLOT = "S") and (not empty(this.oParentObject.w_MDCODUBI) or not empty(this.oParentObject.w_MDCODLOT))
                    * --- Try
                    local bErr_02EAD710
                    bErr_02EAD710=bTrsErr
                    this.Try_02EAD710()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- accept error
                      bTrsErr=.f.
                    endif
                    bTrsErr=bTrsErr or bErr_02EAD710
                    * --- End
                  endif
                endif
              endif
            endif
          endif
          if NVL(this.oParentObject.w_FLCOM1,"N")="S" 
            if this.w_PunPAD.cFunction <> "Query" and this.w_PunPAD.rowstatus()<>"D"
              * --- Caso in cui non sono in interrogazione , quindi non st� cancellando
              * --- Aggiorno i saldi commessa con i nuovi valori.
              if NOT EMPTY(this.oParentObject.w_MDFLCASC)
                * --- Magazzino principale
                * --- Try
                local bErr_02EC70C0
                bErr_02EC70C0=bTrsErr
                this.Try_02EC70C0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_02EC70C0
                * --- End
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.oParentObject.w_MDFLCASC,'SCQTAPER','this.oParentObject.w_MDQTAUM1',this.oParentObject.w_MDQTAUM1,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MDCODART);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MDMAGSAL);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MDCODCOM);
                         )
                else
                  update (i_cTable) set;
                      SCQTAPER = &i_cOp1.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.oParentObject.w_MDCODART;
                      and SCCODMAG = this.oParentObject.w_MDMAGSAL;
                      and SCCODCAN = this.oParentObject.w_MDCODCOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa'
                  return
                endif
                if g_MADV="S"
                  if (g_PERUBI = "S" OR g_PERLOT = "S") and (not empty(this.oParentObject.w_MDCODUBI) or not empty(this.oParentObject.w_MDCODLOT))
                    * --- Try
                    local bErr_02EAD5F0
                    bErr_02EAD5F0=bTrsErr
                    this.Try_02EAD5F0()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- accept error
                      bTrsErr=.f.
                    endif
                    bTrsErr=bTrsErr or bErr_02EAD5F0
                    * --- End
                    * --- Write into SALOTCOM
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
                      i_cOp1=cp_SetTrsOp(this.oParentObject.w_MDFLCASC,'SMQTAPER','this.oParentObject.w_MDQTAUM1',this.oParentObject.w_MDQTAUM1,'update',i_nConn)
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
                          +i_ccchkf ;
                      +" where ";
                          +"SMCODART = "+cp_ToStrODBC(this.oParentObject.w_MDCODART);
                          +" and SMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MDMAGSAL);
                          +" and SMCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MDCODCOM);
                          +" and SMCODUBI = "+cp_ToStrODBC(this.oParentObject.w_MDCODUBI);
                          +" and SMCODLOT = "+cp_ToStrODBC(this.oParentObject.w_MDCODLOT);
                             )
                    else
                      update (i_cTable) set;
                          SMQTAPER = &i_cOp1.;
                          &i_ccchkf. ;
                       where;
                          SMCODART = this.oParentObject.w_MDCODART;
                          and SMCODMAG = this.oParentObject.w_MDMAGSAL;
                          and SMCODCAN = this.oParentObject.w_MDCODCOM;
                          and SMCODUBI = this.oParentObject.w_MDCODUBI;
                          and SMCODLOT = this.oParentObject.w_MDCODLOT;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                endif
              endif
            endif
          endif
          if (nvl(this.oParentObject.w_MDCODCOM,"               "))<>(nvl(this.w_OLDCOMME,"               ")) and NVL(this.w_OLDFCM,"N")="S" 
            if inlist(this.w_PunPAD.cFunction, "Query", "Edit")
              * --- Storno i Saldi Commessa
              if NOT EMPTY(this.w_FLCASC)
                * --- Magazzino principale
                * --- Try
                local bErr_02EC32E0
                bErr_02EC32E0=bTrsErr
                this.Try_02EC32E0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_02EC32E0
                * --- End
                if g_MADV="S"
                  if (g_PERUBI = "S" OR g_PERLOT = "S") and (not empty(this.w_OLDCODUBI) or not empty(this.w_OLDCODLOT))
                    * --- Try
                    local bErr_02EC8650
                    bErr_02EC8650=bTrsErr
                    this.Try_02EC8650()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- accept error
                      bTrsErr=.f.
                    endif
                    bTrsErr=bTrsErr or bErr_02EC8650
                    * --- End
                  endif
                endif
              endif
            endif
          endif
        endif
        * --- Evito la chiamata alla mCalc comunque fatta dopo la Valid dei vari campi
        this.bUpdateParentObject=.F.
    endcase
  endproc
  proc Try_02EC7870()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SCQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_CODART);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLDCODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MDCODCOM);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_CODART;
          and SCCODMAG = this.w_OLDCODMAG;
          and SCCODCAN = this.oParentObject.w_MDCODCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_02EAD710()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALOTCOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SMQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
          +i_ccchkf ;
      +" where ";
          +"SMCODART = "+cp_ToStrODBC(this.w_CODART);
          +" and SMCODMAG = "+cp_ToStrODBC(this.w_OLDCODMAG);
          +" and SMCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MDCODCOM);
          +" and SMCODUBI = "+cp_ToStrODBC(this.oParentObject.w_MDCODUBI);
          +" and SMCODLOT = "+cp_ToStrODBC(this.oParentObject.w_MDCODLOT);
             )
    else
      update (i_cTable) set;
          SMQTAPER = &i_cOp1.;
          &i_ccchkf. ;
       where;
          SMCODART = this.w_CODART;
          and SMCODMAG = this.w_OLDCODMAG;
          and SMCODCAN = this.oParentObject.w_MDCODCOM;
          and SMCODUBI = this.oParentObject.w_MDCODUBI;
          and SMCODLOT = this.oParentObject.w_MDCODLOT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_02EC70C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDCODART),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDMAGSAL),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDCODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.oParentObject.w_MDCODART,'SCCODMAG',this.oParentObject.w_MDMAGSAL,'SCCODCAN',this.oParentObject.w_MDCODCOM,'SCCODART',this.oParentObject.w_MDCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.oParentObject.w_MDCODART;
           ,this.oParentObject.w_MDMAGSAL;
           ,this.oParentObject.w_MDCODCOM;
           ,this.oParentObject.w_MDCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02EAD5F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALOTCOM
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SMCODART"+",SMCODMAG"+",SMCODCAN"+",SMCODUBI"+",SMCODLOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDCODART),'SALOTCOM','SMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDMAGSAL),'SALOTCOM','SMCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDCODCOM),'SALOTCOM','SMCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDCODUBI),'SALOTCOM','SMCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDCODLOT),'SALOTCOM','SMCODLOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SMCODART',this.oParentObject.w_MDCODART,'SMCODMAG',this.oParentObject.w_MDMAGSAL,'SMCODCAN',this.oParentObject.w_MDCODCOM,'SMCODUBI',this.oParentObject.w_MDCODUBI,'SMCODLOT',this.oParentObject.w_MDCODLOT)
      insert into (i_cTable) (SMCODART,SMCODMAG,SMCODCAN,SMCODUBI,SMCODLOT &i_ccchkf. );
         values (;
           this.oParentObject.w_MDCODART;
           ,this.oParentObject.w_MDMAGSAL;
           ,this.oParentObject.w_MDCODCOM;
           ,this.oParentObject.w_MDCODUBI;
           ,this.oParentObject.w_MDCODLOT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02EC32E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SCQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_CODART);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLDCODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_CODART;
          and SCCODMAG = this.w_OLDCODMAG;
          and SCCODCAN = this.w_OLDCOMME;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_02EC8650()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALOTCOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SMQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
          +i_ccchkf ;
      +" where ";
          +"SMCODART = "+cp_ToStrODBC(this.w_CODART);
          +" and SMCODMAG = "+cp_ToStrODBC(this.w_OLDCODMAG);
          +" and SMCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
          +" and SMCODUBI = "+cp_ToStrODBC(this.w_OLDCODUBI);
          +" and SMCODLOT = "+cp_ToStrODBC(this.w_OLDCODLOT);
             )
    else
      update (i_cTable) set;
          SMQTAPER = &i_cOp1.;
          &i_ccchkf. ;
       where;
          SMCODART = this.w_CODART;
          and SMCODMAG = this.w_OLDCODMAG;
          and SMCODCAN = this.w_OLDCOMME;
          and SMCODUBI = this.w_OLDCODUBI;
          and SMCODLOT = this.w_OLDCODLOT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='SALDICOM'
    this.cWorkTables[4]='SALOTCOM'
    this.cWorkTables[5]='CORDRISP'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
