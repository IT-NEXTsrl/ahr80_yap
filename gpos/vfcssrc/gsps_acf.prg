* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_acf                                                        *
*              Causali movimenti Fidelity                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_12]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-19                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_acf"))

* --- Class definition
define class tgsps_acf as StdForm
  Top    = 48
  Left   = 165

  * --- Standard Properties
  Width  = 466
  Height = 73+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=109664361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  CAU_FIDE_IDX = 0
  cFile = "CAU_FIDE"
  cKeySelect = "CFCODICE"
  cKeyWhere  = "CFCODICE=this.w_CFCODICE"
  cKeyWhereODBC = '"CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';

  cKeyWhereODBCqualified = '"CAU_FIDE.CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';

  cPrg = "gsps_acf"
  cComment = "Causali movimenti Fidelity"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CFCODICE = space(5)
  w_CFDESCRI = space(40)
  w_CFTIPMOV = space(1)
  w_CFPUNPRE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAU_FIDE','gsps_acf')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_acfPag1","gsps_acf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Causale movimento fidelity")
      .Pages(1).HelpContextID = 66206499
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCFCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAU_FIDE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAU_FIDE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAU_FIDE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CFCODICE = NVL(CFCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAU_FIDE where CFCODICE=KeySet.CFCODICE
    *
    i_nConn = i_TableProp[this.CAU_FIDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAU_FIDE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAU_FIDE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAU_FIDE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CFCODICE = NVL(CFCODICE,space(5))
        .w_CFDESCRI = NVL(CFDESCRI,space(40))
        .w_CFTIPMOV = NVL(CFTIPMOV,space(1))
        .w_CFPUNPRE = NVL(CFPUNPRE,space(1))
        cp_LoadRecExtFlds(this,'CAU_FIDE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CFCODICE = space(5)
      .w_CFDESCRI = space(40)
      .w_CFTIPMOV = space(1)
      .w_CFPUNPRE = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CFTIPMOV = ' '
        .w_CFPUNPRE = ' '
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAU_FIDE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCFCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCFDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCFTIPMOV_1_4.enabled = i_bVal
      .Page1.oPag.oCFPUNPRE_1_6.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCFCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCFCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAU_FIDE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAU_FIDE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODICE,"CFCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDESCRI,"CFDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFTIPMOV,"CFTIPMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPUNPRE,"CFPUNPRE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAU_FIDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])
    i_lTable = "CAU_FIDE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAU_FIDE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAU_FIDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAU_FIDE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAU_FIDE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAU_FIDE')
        i_extval=cp_InsertValODBCExtFlds(this,'CAU_FIDE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CFCODICE)+;
                  ","+cp_ToStrODBC(this.w_CFDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CFTIPMOV)+;
                  ","+cp_ToStrODBC(this.w_CFPUNPRE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAU_FIDE')
        i_extval=cp_InsertValVFPExtFlds(this,'CAU_FIDE')
        cp_CheckDeletedKey(i_cTable,0,'CFCODICE',this.w_CFCODICE)
        INSERT INTO (i_cTable);
              (CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CFCODICE;
                  ,this.w_CFDESCRI;
                  ,this.w_CFTIPMOV;
                  ,this.w_CFPUNPRE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAU_FIDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAU_FIDE_IDX,i_nConn)
      *
      * update CAU_FIDE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAU_FIDE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CFDESCRI="+cp_ToStrODBC(this.w_CFDESCRI)+;
             ",CFTIPMOV="+cp_ToStrODBC(this.w_CFTIPMOV)+;
             ",CFPUNPRE="+cp_ToStrODBC(this.w_CFPUNPRE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAU_FIDE')
        i_cWhere = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
        UPDATE (i_cTable) SET;
              CFDESCRI=this.w_CFDESCRI;
             ,CFTIPMOV=this.w_CFTIPMOV;
             ,CFPUNPRE=this.w_CFPUNPRE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAU_FIDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAU_FIDE_IDX,i_nConn)
      *
      * delete CAU_FIDE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAU_FIDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCFCODICE_1_1.value==this.w_CFCODICE)
      this.oPgFrm.Page1.oPag.oCFCODICE_1_1.value=this.w_CFCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDESCRI_1_3.value==this.w_CFDESCRI)
      this.oPgFrm.Page1.oPag.oCFDESCRI_1_3.value=this.w_CFDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCFTIPMOV_1_4.RadioValue()==this.w_CFTIPMOV)
      this.oPgFrm.Page1.oPag.oCFTIPMOV_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPUNPRE_1_6.RadioValue()==this.w_CFPUNPRE)
      this.oPgFrm.Page1.oPag.oCFPUNPRE_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CAU_FIDE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_acfPag1 as StdContainer
  Width  = 462
  height = 73
  stdWidth  = 462
  stdheight = 73
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCFCODICE_1_1 as StdField with uid="DHSHQRTAYE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CFCODICE", cQueryName = "CFCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 118104683,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=17, InputMask=replicate('X',5)

  add object oCFDESCRI_1_3 as StdField with uid="UMPGDVBSVI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CFDESCRI", cQueryName = "CFDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32518767,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=170, Top=17, InputMask=replicate('X',40)


  add object oCFTIPMOV_1_4 as StdCombo with uid="QFNYHIURMD",value=1,rtseq=3,rtrep=.f.,left=107,top=48,width=139,height=21;
    , ToolTipText = "Definisce la tipologia del movimento Fidelity";
    , HelpContextID = 197472892;
    , cFormVar="w_CFTIPMOV",RowSource=""+"Invariato,"+"Aumenta,"+"Diminuisce", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCFTIPMOV_1_4.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'+',;
    iif(this.value =3,'-',;
    space(1)))))
  endfunc
  func oCFTIPMOV_1_4.GetRadio()
    this.Parent.oContained.w_CFTIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oCFTIPMOV_1_4.SetRadio()
    this.Parent.oContained.w_CFTIPMOV=trim(this.Parent.oContained.w_CFTIPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_CFTIPMOV=='',1,;
      iif(this.Parent.oContained.w_CFTIPMOV=='+',2,;
      iif(this.Parent.oContained.w_CFTIPMOV=='-',3,;
      0)))
  endfunc


  add object oCFPUNPRE_1_6 as StdCombo with uid="UXVFZUMTBR",value=1,rtseq=4,rtrep=.f.,left=253,top=48,width=139,height=21;
    , ToolTipText = "Indica se il movimento riguarderą i punti o l'importo prepagato";
    , HelpContextID = 246477419;
    , cFormVar="w_CFPUNPRE",RowSource=""+"Nullo,"+"Punti,"+"Importo residuo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCFPUNPRE_1_6.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'P',;
    iif(this.value =3,'I',;
    space(1)))))
  endfunc
  func oCFPUNPRE_1_6.GetRadio()
    this.Parent.oContained.w_CFPUNPRE = this.RadioValue()
    return .t.
  endfunc

  func oCFPUNPRE_1_6.SetRadio()
    this.Parent.oContained.w_CFPUNPRE=trim(this.Parent.oContained.w_CFPUNPRE)
    this.value = ;
      iif(this.Parent.oContained.w_CFPUNPRE=='',1,;
      iif(this.Parent.oContained.w_CFPUNPRE=='P',2,;
      iif(this.Parent.oContained.w_CFPUNPRE=='I',3,;
      0)))
  endfunc

  add object oStr_1_2 as StdString with uid="QXDUYGDZKP",Visible=.t., Left=23, Top=19,;
    Alignment=1, Width=81, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="PBBFABSDXP",Visible=.t., Left=4, Top=47,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo movimento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_acf','CAU_FIDE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CFCODICE=CAU_FIDE.CFCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
