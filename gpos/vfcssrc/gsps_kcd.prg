* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_kcd                                                        *
*              Controllo dati                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-20                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_kcd",oParentObject))

* --- Class definition
define class tgsps_kcd as StdForm
  Top    = 92
  Left   = 187

  * --- Standard Properties
  Width  = 339
  Height = 172
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=99178601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsps_kcd"
  cComment = "Controllo dati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATREG = ctod('  /  /  ')
  w_ORAVEN = space(2)
  w_MINVEN = space(2)
  w_MATCAS = space(20)
  w_NUMSCO = 0
  w_OK_AGG = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_kcdPag1","gsps_kcd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATREG_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATREG=ctod("  /  /  ")
      .w_ORAVEN=space(2)
      .w_MINVEN=space(2)
      .w_MATCAS=space(20)
      .w_NUMSCO=0
      .w_OK_AGG=.f.
      .w_DATREG=oParentObject.w_DATREG
      .w_ORAVEN=oParentObject.w_ORAVEN
      .w_MINVEN=oParentObject.w_MINVEN
      .w_MATCAS=oParentObject.w_MATCAS
      .w_NUMSCO=oParentObject.w_NUMSCO
      .w_OK_AGG=oParentObject.w_OK_AGG
          .DoRTCalc(1,5,.f.)
        .w_OK_AGG = .T.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATREG=.w_DATREG
      .oParentObject.w_ORAVEN=.w_ORAVEN
      .oParentObject.w_MINVEN=.w_MINVEN
      .oParentObject.w_MATCAS=.w_MATCAS
      .oParentObject.w_NUMSCO=.w_NUMSCO
      .oParentObject.w_OK_AGG=.w_OK_AGG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_3.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_3.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oORAVEN_1_5.value==this.w_ORAVEN)
      this.oPgFrm.Page1.oPag.oORAVEN_1_5.value=this.w_ORAVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINVEN_1_7.value==this.w_MINVEN)
      this.oPgFrm.Page1.oPag.oMINVEN_1_7.value=this.w_MINVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oMATCAS_1_9.value==this.w_MATCAS)
      this.oPgFrm.Page1.oPag.oMATCAS_1_9.value=this.w_MATCAS
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMSCO_1_10.value==this.w_NUMSCO)
      this.oPgFrm.Page1.oPag.oNUMSCO_1_10.value=this.w_NUMSCO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_kcdPag1 as StdContainer
  Width  = 335
  height = 172
  stdWidth  = 335
  stdheight = 172
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATREG_1_3 as StdField with uid="UFPVNHKJCI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 172085962,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=121, Top=33, bHasZoom = .t. 

  proc oDATREG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oDATREG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oORAVEN_1_5 as StdField with uid="SRZZINZNMK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ORAVEN", cQueryName = "ORAVEN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Ora vendita",;
    HelpContextID = 54456602,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=250, Top=33, InputMask=replicate('X',2)

  add object oMINVEN_1_7 as StdField with uid="GIRRBXMCWN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MINVEN", cQueryName = "MINVEN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Minuti vendita",;
    HelpContextID = 54405690,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=292, Top=33, InputMask=replicate('X',2)

  add object oMATCAS_1_9 as StdField with uid="GOZGYBYTQZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MATCAS", cQueryName = "MATCAS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Matricola cassa",;
    HelpContextID = 24063430,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=121, Top=92, InputMask=replicate('X',20)

  add object oNUMSCO_1_10 as StdField with uid="JZYXMNSHAB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMSCO", cQueryName = "NUMSCO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero scontrino",;
    HelpContextID = 39923242,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=121, Top=62


  add object oBtn_1_13 as StdButton with uid="YSRFFHTDMW",left=224, top=121, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare la vendita con i dati indicati";
    , HelpContextID = 99149850;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="OJYROTBZFC",left=280, top=121, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per non aggiornare la vendita con i dati indicati. Verranno mantenuti i dati iniziali";
    , HelpContextID = 91861178;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="CXGSTOJCOI",Visible=.t., Left=11, Top=8,;
    Alignment=0, Width=317, Height=18,;
    Caption="Di seguito i dati con cui verr� aggiornata la vendita"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="ZODWRJLOTB",Visible=.t., Left=26, Top=34,;
    Alignment=1, Width=94, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="IZAZALQXOS",Visible=.t., Left=204, Top=34,;
    Alignment=1, Width=42, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VTOUWPBANS",Visible=.t., Left=282, Top=34,;
    Alignment=1, Width=11, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="QNINGWSQLV",Visible=.t., Left=6, Top=92,;
    Alignment=1, Width=114, Height=18,;
    Caption="Matricola cassa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BHFVMDHGOW",Visible=.t., Left=18, Top=63,;
    Alignment=1, Width=102, Height=18,;
    Caption="Scontrino n.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_kcd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
