* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bi1                                                        *
*              Eventi da import documenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_71]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-02                                                      *
* Last revis.: 2003-04-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bi1",oParentObject,m.pEvent)
return(i_retval)

define class tgsps_bi1 as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_CHKMAST = 0
  w_SERDETT = space(10)
  w_MESS = space(10)
  w_APPO = space(10)
  w_APPO2 = space(10)
  w_APPO1 = space(10)
  w_CHIAVE = space(30)
  w_LOOP = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue eventi da Selezione/Spostamento Riga Master (da GSPS_KIM)
    * --- Evento: MC - Master Row Checked , MU - Master Row Checked
    * --- BQD - Before Query Detail , AQD - After Query Detail
    this.w_CHKMAST = -1
    ND = this.oParentObject.w_ZoomDett.cCursor
    NM = this.oParentObject.w_ZoomMast.cCursor
    do case
      case this.pEvent = "MRC"
        * --- Eseguito il Check sul Master
        * --- Inserisce il Flag Selezionato sulle righe del Detail (Se non Trasferito)
        do case
          case &NM..FLDOCU = "S"
            this.oParentObject.w_ZOOMMAST.grd.Columns[ this.oparentobject.w_ZOOMMAST.grd.ColumnCount  ].chk.Value  = 0
            this.w_MESS = "Impossibile selezionare un documento gi� importato%0Eliminare tutti i riferimenti sul documento"
            ah_Msg(this.w_MESS,.T.)
          case &NM..FLCONG <> " "
            this.oParentObject.w_ZOOMMAST.grd.Columns[ this.oparentobject.w_ZOOMMAST.grd.ColumnCount  ].chk.Value  = 0
            do case
              case &NM..FLCONG="3"
                this.w_MESS = "Documento di origine; non selezionabile%0Dati pagamento incongruenti"
              case &NM..FLCONG="4"
                this.w_MESS = "Documento di origine; non selezionabile%0Sconti di piede incongruenti"
              otherwise
                this.w_MESS = "Documento di origine; non selezionabile"
            endcase
            ah_Msg(this.w_MESS,.T.)
          otherwise
            this.oParentObject.NotifyEvent("CalcRig")
            UPDATE (ND) SET XCHK=IIF(NVL(MVTIPRIG," ")="D",1, 0), MVQTAEVA=IIF(NVL(MVTIPRIG," ")="D",1, MVQTAMOV), ;
            VISPRE=cp_ROUND(VISPRE, 2), MVIMPEVA=cp_ROUND(MVPREZZO, 2) ;
            WHERE FLDOCU<>"S" AND (MVTIPRIG<>"F" OR MVFLSCOR="S")
            this.w_CHKMAST = 1
        endcase
      case this.pEvent = "MRU"
        * --- Eseguito l' Uncheck sul Master
        * --- Toglie il Flag Selezionato sulle righe del Detail (Se non Trasferito)
        do case
          case &NM..FLDOCU = "S"
            this.oParentObject.w_ZOOMMAST.grd.Columns[ this.oparentobject.w_ZOOMMAST.grd.ColumnCount  ].chk.Value  = 1
            this.w_MESS = "Impossibile deselezionare un documento gi� importato%0Eliminare tutti i riferimenti sul documento"
            ah_Msg(this.w_MESS,.T.)
          otherwise
            UPDATE (ND) SET XCHK=0, MVQTAEVA=0, MVIMPEVA=0 WHERE FLDOCU<>"S"
            this.w_CHKMAST = 0
        endcase
      case this.pEvent = "DRC"
        * --- Eseguito il Check sul Detail
        * --- Inserisce il Flag Selezionato sulla riga del Detail (se non proveniente dal Documento)
        do case
          case &ND..FLDOCU="S"
            this.oParentObject.w_ZOOMDETT.grd.Columns[ this.oparentobject.w_ZOOMDETT.grd.ColumnCount  ].chk.Value  = 1
            this.w_MESS = "Impossibile selezionare una riga gi� importata%0Eliminare la riga corrispondente sul documento"
            ah_Msg(this.w_MESS,.T.)
          case &NM..FLCONG <> " "
            this.oParentObject.w_ZOOMDETT.grd.Columns[ this.oparentobject.w_ZOOMDETT.grd.ColumnCount  ].chk.Value  = 0
            do case
              case &NM..FLCONG="3"
                this.w_MESS = "Documento di origine; non selezionabile%0Dati pagamento incongruenti"
              case &NM..FLCONG="4"
                this.w_MESS = "Documento di origine; non selezionabile%0Sconti di piede incongruenti"
              otherwise
                this.w_MESS = "Documento di origine; non selezionabile"
            endcase
            ah_Msg(this.w_MESS,.T.)
          case &ND..MVTIPRIG="F" AND &ND..MVFLSCOR<>"S"
            this.oParentObject.w_ZOOMDETT.grd.Columns[ this.oparentobject.w_ZOOMDETT.grd.ColumnCount  ].chk.Value  = 0
            this.w_MESS = "Impossibile selezionare una riga forfettaria da un documento con importi al netto"
            ah_Msg(this.w_MESS,.T.)
          otherwise
            REPLACE &ND..MVQTAEVA WITH IIF(NVL(&ND..MVTIPRIG," ")="D", 1, &ND..MVQTAMOV)
            REPLACE &ND..MVIMPEVA WITH cp_ROUND(&ND..MVPREZZO, 2)
        endcase
      case this.pEvent = "DRU"
        * --- Eseguito l' Uncheck sul Detail
        * --- Toglie il Flag Selezionato sulle riga del Detail (se non proveniente dal Documento)
        if &ND..FLDOCU = "S"
          this.oParentObject.w_ZOOMDETT.grd.Columns[ this.oparentobject.w_ZOOMDETT.grd.ColumnCount  ].chk.Value  = 1
          this.w_MESS = "Impossibile deselezionare una riga gi� importata%0Eliminare la riga corrispondente sul documento"
          ah_Msg(this.w_MESS,.T.)
        else
          this.w_APPO = 0
          if &ND..MVTIPRIG="D"
            this.w_APPO = IIF(ah_YesNo("Importo comunque la riga descrittiva nel documento di destinazione?"),1,0)
          endif
          REPLACE &ND..MVQTAEVA WITH this.w_APPO
          REPLACE &ND..MVIMPEVA WITH 0
        endif
      case this.pEvent = "DBQ"
        * --- Evento Before Query sul Detail
        * --- Salva in RigheDoc le Variazioni Riportate sul Detail
        if USED( this.oParentObject.w_ZoomDett.cCursor ) AND USED("RigheDoc")
          this.w_SERDETT = Nvl(this.oParentObject.w_ZoomDett.GetVar("MVSERIAL"),Space(10))
          SELECT * FROM RigheDoc WHERE MVSERIAL<>this.w_SERDETT UNION ;
          SELECT XCHK, MVQTAMOV, MVQTAEVA, MVIMPEVA, MVSERIAL, CPROWNUM, CPROWORD, FLDOCU, MVTIPRIG FROM (ND) ;
          WHERE XCHK = 1 OR MVQTAEVA<>0 OR (MVIMPEVA<>0 AND MVTIPRIG="F") ;
          INTO CURSOR RigheDoc
          SELECT (ND)
        endif
      case this.pEvent = "DAQ"
        * --- Evento After Query sul Detail
        * --- Riporta sul Detail le Variazioni precedentemente definite e salvate in RigheDoc
        if USED( this.oParentObject.w_ZoomDett.cCursor ) AND USED("RigheDoc")
          this.w_CHKMAST = this.oParentObject.w_ZoomMast.GetVar("XCHK")
          SELECT (ND)
          SCAN
          SELECT * FROM RigheDoc WHERE RigheDoc.MVSERIAL = &ND..MVSERIAL ;
          AND RigheDoc.CPROWNUM = &ND..CPROWNUM INTO CURSOR APPCUR
          SELECT (ND)
          if RECCOUNT("APPCUR")<>0
            REPLACE XCHK WITH APPCUR.XCHK, ;
            MVQTAEVA WITH IIF(MVTIPRIG="D", 1, APPCUR.MVQTAEVA), ;
            VISPRE WITH cp_ROUND(VISPRE, 2), ;
            MVIMPEVA WITH APPCUR.MVIMPEVA, ;
            FLDOCU WITH APPCUR.FLDOCU
          else
            REPLACE VISPRE WITH cp_ROUND(VISPRE, 2)
          endif
          ENDSCAN
          if USED("APPCUR")
            SELECT APPCUR
            USE
            SELECT (ND)
          endif
        endif
      case this.pEvent = "SCH"
        * --- Evento w_SELEZI Changed
        * --- Seleziona/Deselezione la Righe Detail (se consentito)
        if USED( this.oParentObject.w_ZoomDett.cCursor )
          if this.oParentObject.w_ZoomMast.GetVar("XCHK") = 1
            * --- Selezionato il Documento Master
            if this.oParentObject.w_SELEZI = "S"
              * --- Seleziona Tutto
              UPDATE (ND) SET XCHK=IIF(NVL(MVTIPRIG," ")="D", 1, 0), MVQTAEVA=IIF(NVL(MVTIPRIG," ")="D", 1, MVQTAMOV), ;
              MVIMPEVA=cp_ROUND(MVPREZZO, 2) WHERE FLDOCU<>"S"
            else
              * --- deseleziona Tutto
              UPDATE (ND) SET XCHK=0, MVQTAEVA=0, MVIMPEVA=0 WHERE FLDOCU<>"S"
            endif
          else
            this.w_MESS = "Documento da importare non selezionato"
            ah_Msg(this.w_MESS,.T.)
            this.oParentObject.w_SELEZI = IIF(this.oParentObject.w_SELEZI = "S", "D", "S")
          endif
        endif
    endcase
    * --- Setta Proprieta' Campi del Cursore
    if this.w_CHKMAST <> -1
      this.w_APPO1 = "XCHK-MVQTAEVA-CPROWORD-MVCODICE-MVUNIMIS-MVQTAMOV-MVDATEVA"
      this.w_APPO1 = this.w_APPO1 + "-FLEVAS-MVTIPRIG-MVPREZZO-MVIMPEVA-VISPRE"
      SELECT (ND)
      this.w_LOOP = 1
      do while this.w_LOOP<=this.oParentObject.w_ZoomDett.grd.ColumnCount
        if Upper( this.oParentObject.w_ZoomDett.grd.Columns[ this.w_LOOP ].ControlSource) $ this.w_APPO1
          if this.w_APPO <> "XCHK"
            this.w_APPO2 = "IIF(XCHK=0 AND MVQTAEVA=0, RGB(0,0,0), "
            this.w_APPO2 = this.w_APPO2 + 'IIF(FLDOCU="S", RGB(0,0,255), RGB(255,0,0)))'
            this.oParentObject.w_ZOOMDETT.grd.Columns[ this.w_LOOP ].DynamicForeColor = this.w_APPO2
            if this.w_APPO = "MVQTAEVA" OR this.w_APPO = "MVIMPEVA"
              this.oParentObject.w_ZOOMDETT.grd.Columns[ this.w_LOOP ].Format = "KZR"
              this.oParentObject.w_ZOOMDETT.grd.Columns[ this.w_LOOP ].Enabled = IIF(this.w_CHKMAST=1, .T., .F.)
            endif
          else
            this.oParentObject.w_ZOOMDETT.grd.Columns[ this.w_LOOP ].Width = 40
            this.oParentObject.w_ZOOMDETT.grd.Columns[ this.w_LOOP ].Header1.caption = "Evasa"
            this.oParentObject.w_ZOOMDETT.grd.Columns[ this.w_LOOP ].chk.caption = ""
            this.oParentObject.w_ZOOMDETT.grd.Columns[ this.w_LOOP ].Enabled =  IIF(this.w_CHKMAST=1, .T., .F.)
          endif
        else
          this.oParentObject.w_ZOOMDETT.grd.Columns[ this.w_LOOP ].Visible = .F.
        endif
        this.w_LOOP = this.w_LOOP + 1
      enddo
    endif
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    *     perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
