* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bpz                                                        *
*              Calcolo promozioni                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_362]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-04                                                      *
* Last revis.: 2015-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bpz",oParentObject,m.pExec)
return(i_retval)

define class tgsps_bpz as StdBatch
  * --- Local variables
  pExec = space(1)
  w_OK = .f.
  w_DOCPOS = .f.
  w_CODPRO = space(15)
  w_ORAMIN = space(4)
  w_ORA = 0
  w_ROWORD = 0
  w_RIGA = 0
  w_MSG = space(200)
  w_SCONT1 = space(6)
  w_SCONT2 = space(6)
  w_SCONT3 = space(6)
  w_SCONT4 = space(6)
  w_TESTFID = space(1)
  w_GIORNO = space(1)
  w_IMPON = 0
  w_CODART = space(20)
  w_LGRUMER = space(5)
  w_QTAMOV = 0
  w_ARTDES = space(20)
  w_DESPRO = space(40)
  w_FLESCU = space(1)
  w_TIPPRO = space(1)
  w_ARTOMA = space(20)
  w_DESOMA = space(40)
  w_QTAOMA = 0
  w_GRUOMA = space(5)
  w_CODIVA = space(5)
  w_ARTPOS = space(1)
  w_DESART = space(40)
  w_UNIMIS = space(5)
  w_ARMOLTIP = 0
  w_ARMOLTI3 = 0
  w_ARUNMIS1 = space(5)
  w_ARUNMIS2 = space(5)
  w_ARUNMIS3 = space(5)
  w_AROPERAT = space(1)
  w_REPOMA = space(3)
  w_ARUMCAL = space(14)
  w_TOTIMP = 0
  w_TOTPZ = 0
  w_TOTBOL = 0
  w_MINIMP = 0
  w_MINPZ = 0
  w_MINBOL = 0
  w_SCPER = 0
  w_SCVAL = 0
  w_PRZUNI = 0
  w_PRZTOT = 0
  w_QTAN = 0
  w_QTAM = 0
  w_MIXMAT = space(1)
  w_TIPSCO = space(3)
  w_SCONTO = 0
  w_PUNEXT = 0
  w_PUNTI = 0
  w_TIPFID = space(3)
  w_TIPOMA = space(3)
  w_TIPVAQ = space(1)
  w_CATSCOA = space(3)
  w_FLNOAR = space(1)
  w_TOTVEN = 0
  w_SCOVEN = 0
  w_TSCOVEN = 0
  w_LOCATE = 0
  w_SCOPRO = 0
  w_STOPRO = 0
  w_FLOMAG = space(1)
  w_FUNC = space(1)
  w_LFLFRAZ = space(1)
  w_PADRE = .NULL.
  w_RECO = 0
  w_AbsRow = 0
  w_nRelRow = 0
  w_RECPOS = 0
  w_LPOSIZ = 0
  w_TEST = .f.
  w_NUMREC = 0
  w_LROWNUM = 0
  w_WRITETRS = .f.
  w_UNISEP = space(1)
  w_NOFRAZ = space(1)
  w_MODUM2 = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  REP_ARTI_idx=0
  VOCIIVA_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione cursore per cercare tutte le Promozioni valide (da GSPS_MVD)
    * --- pExec
    *     'P' = Calcolo Promozioni alla conferma della prima pagina: vengono calcolate tutte le promozioni possibili
    *     'C' = Se modificato il Cliente in pag.1 vengono riazzerate le righe derivanti da Promozioni
    this.w_PADRE = this.oParentObject
    this.w_FUNC = this.w_PADRE.cFunction
    this.oParentObject.w_PROMOK = .F.
    this.w_GIORNO = ALLTRIM(STR(DOW(this.oParentObject.w_MDDATREG)))
    this.w_ORAMIN = RIGHT("00"+this.oParentObject.w_MDORAVEN,2) + RIGHT("00"+this.oParentObject.w_MDMINVEN,2)
    this.w_TESTFID = "X"
    this.w_IMPON = 0
    this.oParentObject.w_MDSCOPRO = 0
    this.oParentObject.w_MDSTOPRO = 0
    this.w_FLESCU = " "
    this.w_FLOMAG = " "
    this.w_CODPRO = SPACE(15)
    this.w_ARTDES = IIF(NOT EMPTY(g_ARTDES),g_ARTDES,SPACE(20))
    this.w_ARTPOS = " "
    this.w_SCONTO = 0
    this.oParentObject.w_MDPUNFID = IIF(this.w_FUNC="Edit",this.oParentObject.w_MDPUNFID,0)
    * --- Articolo descrittivo
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARARTPOS"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_ARTDES);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARARTPOS;
        from (i_cTable) where;
            ARCODART = this.w_ARTDES;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ARTPOS = NVL(cp_ToDate(_read_.ARARTPOS),cp_NullValue(_read_.ARARTPOS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_ARTDES) OR this.w_ARTPOS<>"S"
      this.w_MSG = "Deve essere impostato obbligatoriamente l'articolo per riferimenti nelle contropartite di vendita%0Il servizio descrittivo deve essere un servizio P.O.S."
      ah_ErrorMsg(this.w_MSG,,"")
      i_retcode = 'stop'
      return
    endif
    this.w_CODIVA = SPACE(5)
    if NOT EMPTY(this.oParentObject.w_MDCODFID)
      * --- Per considerare anche le Promozioni su fidelity se � stato
      *     specificato un codice fidelity nella vendita
      this.w_TESTFID = "F"
    endif
    this.w_SCONT1 = " "
    this.w_SCONT2 = " "
    this.w_SCONT3 = " "
    this.w_SCONT4 = " "
    this.w_ROWORD = 0
    * --- Eliminazione dal temporaneo delle righe inserite per una precedente elaborazione di Promozione
    *     e cancellazione degli sconti ventilati
    this.w_PADRE.MarkPos()     
    * --- Esamino il transitorio per determinare eventuali righe promozioni gi� calcolate. 
    *     Se ci� accade le elimino (Delete sulle righe promozioni aggiunte)  
    *     e ripulisco il transitorio sulle altre righe (quelle che determinano la creazione di promozioni)
    *     Non cancello le righe tipo: 
    *     *    Riga Articolo Omaggio da Promozione
    *     *    Riga Forfettaria per Storno Importo Fidelity
    this.w_PADRE.FirstRow()     
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if this.w_PADRE.FullRow() And NOT EMPTY(this.oParentObject.w_MDCODART) AND this.oParentObject.w_MDFLRESO<>"S"
        if NOT EMPTY(this.oParentObject.w_MDCODPRO) OR this.oParentObject.w_MDFLROMA="S" 
          * --- Riga Omaggio Promozione o derivante da Promozione
          this.w_PADRE.DeleteRow()     
        else
          if NOT EMPTY(this.oParentObject.w_MDFLESCL)
            * --- Elimino flag Promozione esclusiva o comulativa
            this.oParentObject.w_MDFLESCL = ""
          endif
          if NOT EMPTY(this.oParentObject.w_MDSCOVEN)
            * --- Riazzero sconto ventilato
            this.oParentObject.w_MDSCOVEN = 0
          endif
          this.w_PADRE.SaveRow()     
        endif
      endif
      this.w_PADRE.NextRow()     
    enddo
    if this.pExec = "P"
      this.w_PADRE.FirstRow()     
      * --- Azzero campi di sistema per ricalcolare le promozioni
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        if this.w_PADRE.FullRow() 
          this.oParentObject.w_MDPROMOZ = 0
          this.oParentObject.w_MDESPSCO = 0
          this.w_PADRE.SaveRow()     
        endif
        this.w_PADRE.NextRow()     
      enddo
      * --- Esamino eventuali righe con sconto e se trovate genero una nuova riga descrittiva
      this.w_PADRE.Exec_Select("RigheSconti","*","((ABS(t_MDSCONT1)+ABS(t_MDSCONT2)+ABS(t_MDSCONT3)+ABS(t_MDSCONT4)<>0) OR t_MDFLOMAG<>1) AND t_MDFLRESO<>1 AND EMPTY(NVL(t_MDCODPRO,SPACE(10))) AND NOT DELETED()","","","")     
      * --- Scorro il cursore (aggiungo righe sul transitorio)
      do while Not Eof( "RigheSconti" )
        this.w_TEST = .F.
        do case
          case t_MDFLOMAG=1
            * --- Costruisco una stringa con gli sconti di riga.
            this.w_SCONT1 = ALLTRIM(STR(NVL(t_MDSCONT1,0),6,2))
            this.w_SCONT2 = IIF(NOT EMPTY(NVL(t_MDSCONT2,0)),ALLTRIM(STR(NVL(t_MDSCONT2,0),6,2))," ")
            this.w_SCONT3 = IIF(NOT EMPTY(NVL(t_MDSCONT3,0)),ALLTRIM(STR(NVL(t_MDSCONT3,0),6,2))," ")
            this.w_SCONT4 = IIF(NOT EMPTY(NVL(t_MDSCONT4,0)),ALLTRIM(STR(NVL(t_MDSCONT4,0),6,2))," ")
            this.w_DESART = ah_Msgformat("Sconti %: %1 %2 %3 %4",this.w_SCONT1, this.w_SCONT2, this.w_SCONT3, this.w_SCONT4)
            * --- Controllo flag Esplicita Sconti Riga nei parametri.
            *     Nel caso � uguale a 'S' gli sconti di riga devono essere trattati come Promozioni 
            *     e arrotondati a 2 decimali fissi, come avviene direttamente sul Registratore di Cassa.
            *     Deve essere inoltre aggiornato lo sconto Ventilato (MVSCOVEN) sulla riga che ha generato lo sconto
            if g_FLESSC = "S" And this.oParentObject.w_MDDATREG >= g_DATESC
              this.w_IMPON = - cp_Round(( NVL(t_MDPREZZO,0)*NVL(t_MDQTAMOV,0) ) * cp_Round((1 - (1+t_MDSCONT1/100.0)*(1+t_MDSCONT2/100.0)*(1+t_MDSCONT3/100.0)*(1+t_MDSCONT4/100.0)),10),2)
              this.w_LROWNUM = CPROWNUM
              this.w_NUMREC = this.w_PADRE.Search("CPROWNUM="+ALLTRIM(STR(this.w_LROWNUM) ) )
              this.w_PADRE.SetRow(this.w_NUMREC)     
              this.oParentObject.w_MDSCOVEN = this.oParentObject.w_MDSCOVEN + this.w_IMPON
              this.oParentObject.w_MDESPSCO = this.oParentObject.w_MDESPSCO + this.w_IMPON
              this.w_PADRE.SaveRow()     
              * --- Riseleziono il cursore di appoggio
              SELECT("RigheSconti")
              this.w_TEST = .T.
            else
              this.w_IMPON = t_VALRIG-(NVL(t_MDPREZZO,0)*NVL(t_MDQTAMOV,0))
            endif
          case t_MDFLOMAG=2
            * --- Calcolo l'importo di riga
            this.w_IMPON = - NVL(t_VALRIG,0)
            this.w_DESART = ah_Msgformat("Sconto merce")
          case t_MDFLOMAG=3
            * --- Calcolo l'importo di riga: abbuono pari alla riga
            this.w_IMPON = - NVL(t_VALRIG,0)
            this.w_DESART = ah_Msgformat("Omaggio")
        endcase
        this.w_ROWORD = t_CPROWORD
        * --- Creo una nuova riga descrittiva
        this.w_PADRE.AddRow()     
        * --- Inserisco una riga descrittiva per ogni riga esaminata
        this.oParentObject.w_MDCODICE = this.w_ARTDES
        this.oParentObject.w_MDCODART = this.w_ARTDES
        this.oParentObject.w_MDDESART = this.w_DESART
        this.oParentObject.w_MDSCOPRO = this.w_IMPON
        this.oParentObject.w_MDQTAMOV = 0
        this.oParentObject.w_MDTIPRIG = "D"
        * --- Nel caso di sconto con il flag Esplicita Sconti di Riga attivato,
        *     diversifico il codice promozione per distinguerlo 
        if g_FLESSC = "S" And this.oParentObject.w_MDDATREG >= g_DATESC
          this.oParentObject.w_MDCODPRO = "#########X"
          if this.w_TEST
            * --- Con questa impostazione gli sconti di riga sono equipartati alle promozioni
            *     Nella generazione documenti, gli sconti di riga verranno azzerati.
            *     w_TEST: solo riga normale, non sconto merce o omaggio imponibile
            this.oParentObject.w_MDSTOPRO = this.oParentObject.w_MDSTOPRO + this.w_IMPON
          endif
        else
          this.oParentObject.w_MDCODPRO = "##########"
        endif
        this.oParentObject.w_CPROWORD = this.w_ROWORD
        this.w_PADRE.SaveRow()     
        * --- Vado alla riga successiva
        if Not Eof("RigheSconti")
           
 SELECT("RigheSconti") 
 SKIP
        endif
      enddo
      * --- Rimuovo il cursore
      USE IN SELECT("RigheSconti")
      * --- Ricerca delle Promozioni valide alla data, ora e giorno del documento
      VQ_EXEC("..\GPOS\EXE\QUERY\GSPS2QAP.VQR",this,"PROMO")
      if RECCOUNT("PROMO") >0
        * --- PANIERI:Considera solo le Promozioni MixMatch poich� nell'altro caso esiste una promozione per ogni articolo
        *     del paniere e quindi Articolo e Gru.Merc sono gi� presenti in PROMO
        VQ_EXEC("..\GPOS\EXE\QUERY\GSPS1QAP.VQR",this,"PANIERI")
        * --- Elaboro le Promozioni
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      USE IN SELECT("PROMO")
      USE IN SELECT("PANIERI")
    endif
    * --- Riposizionamento sul Trs aggionamento valori
    this.w_PADRE.RePos()     
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione cursore PROMO contentente le Promozioni valide
    * --- Ciclo sulle Promozioni
    SELECT PANIERI
    INDEX ON PRCODICE TAG TAG1PAN
    this.w_DOCPOS = .T.
    SELECT PROMO
    GO TOP
    SCAN
    * --- w_DOCPOS : Se il totale documento, dopo lo sconto promozione, 
    *     rimane positivo continuo ad elaborare le promozioni successive.
    if this.w_DOCPOS
      this.w_OK = .F.
      this.w_RIGA = 0
      this.w_PUNTI = 0
      this.w_CODPRO = PRCODICE
      this.w_MIXMAT = NVL(PRMIXMAT," ")
      this.w_CODART = SPACE(20)
      this.w_LGRUMER = SPACE(5)
      this.w_FLNOAR = NVL(FLNOAR," ")
      this.w_QTAMOV = 0
      this.w_IMPON = 0
      this.w_SCOPRO = 0
      this.w_STOPRO = 0
      this.w_ARUMCAL = SPACE(14)
      this.w_ARMOLTIP = NVL(ARMOLTIP,0)
      this.w_ARMOLTI3 = 0
      this.w_ARUNMIS1 = NVL(ARUNMIS1,SPACE(3))
      this.w_ARUNMIS2 = NVL(ARUNMIS2,SPACE(3))
      this.w_ARUNMIS3 = this.w_ARUNMIS1
      this.w_AROPERAT = NVL(AROPERAT," ")
      this.oParentObject.w_MDQTAUM1 = 0
      this.w_ARTDES = NVL(PRARTDES,SPACE(20))
      this.w_DESPRO = NVL(PRDESPRO,SPACE(40))
      this.w_FLESCU = NVL(PRFLESCU," ")
      this.w_TIPPRO = NVL(PRTIPPRO," ")
      this.w_TIPSCO = NVL(PRTIPSCO,SPACE(3))
      this.w_TIPVAQ = NVL(PRTIPVAQ," ")
      this.w_ARTDES = IIF(EMPTY(NVL(PRARTDES,SPACE(20))),g_ARTDES,NVL(PRARTDES,SPACE(20)))
      this.w_DESPRO = NVL(PRDESPRO,SPACE(40))
      this.w_ARTOMA = NVL(PRARTOMA,SPACE(20))
      this.w_DESOMA = NVL(ARDESART,SPACE(40))
      this.w_GRUOMA = NVL(ARGRUMER,SPACE(5))
      this.w_QTAOMA = NVL(PRQTOMAG,0)
      this.w_REPOMA = NVL(ARCODREP,SPACE(3))
      this.w_CODIVA = NVL(RECODIVA, SPACE(5))
      this.w_CATSCOA = NVL(ARCATSCM,SPACE(5))
      this.w_UNIMIS = NVL(PRUMIOMA,SPACE(3))
      this.w_LFLFRAZ = Nvl(FLFRAZ," ")
      this.w_MINIMP = NVL(PRMINIMP,0)
      this.w_MINPZ = NVL(PRPEZMIN,0)
      this.w_MINBOL = NVL(PRMINBOL,0)
      this.w_SCPER = NVL(PRPERSCO,0)
      this.w_SCVAL = NVL(PRVALSCU,0)
      this.w_PRZUNI = NVL(PRPRUFIS,0)
      this.w_PRZTOT = NVL(PRPRTFIS,0)
      this.w_QTAN = NVL(PRQTENNE,0)
      this.w_QTAM = NVL(PRQTEMME,0)
      this.w_PUNEXT = NVL(PRPUNEXT,0)
      this.w_TIPFID = NVL(PRTIPFID,"XXX")
      this.w_TIPOMA = NVL(PRTIPOMA,"XXX")
      this.w_FLOMAG = NVL(PRFLOMAG," ")
      this.w_TOTBOL = 0
      this.w_TOTPZ = 0
      this.w_TOTIMP = 0
      * --- Ciclo sulle Righe della Vendita
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        * --- Ciclo sulle righe della vendita dove non � gi� stata applicata una Promozione Esclusiva, di Tipo Normale (No sconto merce o altro),
        *     Non Descrittive, Non vuote, Non di Reso, Non servizi accessori, e non Servizio Per Storno Importo Fidelity
        if this.oParentObject.w_MDFLESCL<>"E" AND (this.oParentObject.w_MDFLESCL<>"C" OR this.w_FLESCU="C") AND this.oParentObject.w_MDFLOMAG="X" AND this.oParentObject.w_MDTIPRIG<>"D" AND NOT EMPTY(this.oParentObject.w_MDCODICE) AND this.oParentObject.w_MDFLRESO<>"S" AND this.oParentObject.w_FLSERA<>"S" And this.oParentObject.w_MDCODPRO <>"#########F"
          * --- Segno le variabili per aggiornamento cursori
          this.w_CODART = this.oParentObject.w_MDCODART
          this.w_LGRUMER = NVL(this.oParentObject.w_GRUMER,Repl("#",10 ) )
          this.w_QTAMOV = this.oParentObject.w_MDQTAUM1
          * --- Imponibile considerato: prezzo x q.t� - sconti di riga - sconto ventilato
          this.w_ROWORD = this.oParentObject.w_CPROWORD
          this.w_IMPON = this.oParentObject.w_VALRIG + this.oParentObject.w_MDSCOVEN
          this.w_WRITETRS = .F.
          if this.w_FLNOAR="S"
            * --- Flag tutti gli articoli attivato: non devo controllare la congruit� dell'articolo della vendita
            this.w_WRITETRS = .T.
          else
            * --- Tutti gli articoli disattivato: controllo se presente nel paniere
            if this.w_MIXMAT="S"
              * --- Verifico se l'articolo della vendita � presente nel paniere della promozione esaminata
               
 SELECT PANIERI 
 GO TOP
              if SEEK(this.w_CODPRO)
                LOCATE FOR PRCODICE=this.w_CODPRO AND (PRCODART= this.w_CODART OR PRGRUMER = this.w_LGRUMER) 
                this.w_WRITETRS = FOUND()
              endif
            else
              * --- Se non Mixmatch l'articolo � presente nella riga del cursore PROMO
              *     poich� in questo caso � come se esistesse una promozione per ogni 
              *     articolo definito nella promozione
              SELECT PROMO
              this.w_WRITETRS = this.w_CODART=NVL(PRCODART,Space(43)) OR this.w_LGRUMER=NVL(PRGRUMER,Space(13))
            endif
          endif
          if this.w_WRITETRS
            this.w_TOTPZ = this.w_TOTPZ+this.w_QTAMOV
            this.w_TOTIMP = this.w_TOTIMP+this.w_IMPON
            this.w_RIGA = this.w_ROWORD
            this.w_OK = .T.
            * --- In ogni caso indico che la riga appartiene ad una promozione
            this.oParentObject.w_FLPAN = "S"
            this.w_PADRE.SaveRow()     
          endif
        endif
        * --- EndScan sul transitorio
        this.w_PADRE.NextRow()     
      enddo
      if this.w_OK
        SELECT PROMO
        do case
          case PRTIPVAQ="Q"
            if NVL(PRTIPSCO,0)="NXM"
              if this.w_TOTPZ>=NVL(PRQTENNE,0) AND (this.w_TOTPZ>=NVL(PRPEZMIN,0) OR NVL(PRPEZMIN,0)=0) 
                * --- Se raggiunti i requisiti minimi indico che la promozione viene applicata
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              if this.w_TOTPZ>=NVL(PRPEZMIN,0) 
                * --- Se raggiunti i requisiti minimi indico che la promozione viene applicata
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          case PRTIPVAQ="V"
            if this.w_TOTIMP>=NVL(PRMINIMP,0) 
              * --- Se raggiunti i requisiti minimi indico che la promozione viene applicata
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          case PRTIPVAQ="B"
            * --- Se raggiunti i requisiti minimi indico che la promozione viene applicata
            if this.oParentObject.w_MDPUNFID>= this.w_MINBOL
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          otherwise
            if NVL(PRTIPPRO," ")="S" AND NVL(PRTIPSCO,0)="NXM"
              if this.w_TOTPZ>=NVL(PRQTENNE,0) AND (this.w_TOTPZ>=NVL(PRPEZMIN,0) OR NVL(PRPEZMIN,0)=0) 
                * --- Se raggiunti i requisiti minimi indico che la promozione viene applicata
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
        endcase
      endif
    endif
    * --- Smarco le righe che hanno partecipato alla promozione esaminata
    *     NON ESISTE UN METODO SUI TRANSITORI PER EFFETTUARE UNA REPLACE MASSIVA
     
 SELECT (this.w_PADRE.cTrsName) 
 REPLACE t_FLPAN WITH " ", i_Srv WITH IIF(i_Srv="A",i_Srv,"U") FOR t_FLPAN="S"
     
 SELECT("PROMO")
    ENDSCAN
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- In questa pagina viene eseguito l'aggiornamento del temporaneo di riga inserendo la riga Promozionale
    if this.w_TIPPRO="O"
      * --- Nel caso di promozione con Omaggio aggiungo la riga dell'articolo omaggiato
      SELECT (this.w_PADRE.cTrsName)
      * --- Aggiunge una riga in append
      this.w_PADRE.AddRow()     
      this.oParentObject.w_MDCODICE = this.w_ARTOMA
      this.oParentObject.w_MDCODART = this.w_ARTOMA
      this.oParentObject.w_MDDESART = this.w_DESOMA
      this.oParentObject.w_MDCODPRO = this.w_CODPRO
      this.oParentObject.w_GRUMER = this.w_GRUOMA
      * --- Tipo Omaggio definito dall'utente
      this.oParentObject.w_MDFLOMAG = this.w_FLOMAG
      this.oParentObject.w_MDFLROMA = "S"
      if this.w_TIPOMA="QAF"
        * --- Se raggiunto il minimo
        this.oParentObject.w_MDQTAMOV = this.w_QTAOMA
      else
        * --- Ad ogni multiplo
        do case
          case this.w_TIPVAQ="Q"
            * --- Minimo Pezzi
            this.oParentObject.w_MDQTAMOV = (this.w_QTAOMA * (INT(this.w_TOTPZ / this.w_MINPZ)))
          case this.w_TIPVAQ="V"
            * --- Minimo Valore
            this.oParentObject.w_MDQTAMOV = (this.w_QTAOMA * (INT(this.w_TOTIMP / this.w_MINIMP)))
          case this.w_TIPVAQ="B"
            * --- Minimo Bollini
            this.oParentObject.w_MDQTAMOV = (this.w_QTAOMA * (INT(this.oParentObject.w_MDPUNFID / this.w_MINBOL)))
        endcase
      endif
      this.oParentObject.w_MDCODREP = this.w_REPOMA
      * --- Read from REP_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.REP_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.REP_ARTI_idx,2],.t.,this.REP_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "REDESREP"+;
          " from "+i_cTable+" REP_ARTI where ";
              +"RECODREP = "+cp_ToStrODBC(this.w_REPOMA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          REDESREP;
          from (i_cTable) where;
              RECODREP = this.w_REPOMA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DESREP = NVL(cp_ToDate(_read_.REDESREP),cp_NullValue(_read_.REDESREP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLUSEP"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARTOMA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLUSEP;
          from (i_cTable) where;
              ARCODART = this.w_ARTOMA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_UNISEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from UNIMIS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UNIMIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UMFLFRAZ,UMMODUM2"+;
          " from "+i_cTable+" UNIMIS where ";
              +"UMCODICE = "+cp_ToStrODBC(this.w_ARUNMIS1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UMFLFRAZ,UMMODUM2;
          from (i_cTable) where;
              UMCODICE = this.w_ARUNMIS1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
        this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_MDCODIVA = this.w_CODIVA
      this.oParentObject.w_MDFLCASC = "-"
      this.oParentObject.w_MDTIPRIG = "R"
      this.oParentObject.w_MDUNIMIS = this.w_UNIMIS
      this.oParentObject.w_FLFRAZ = this.w_LFLFRAZ
      this.w_ARUMCAL = this.w_ARUNMIS1+this.w_ARUNMIS2+this.w_ARUNMIS3+this.oParentObject.w_MDUNIMIS+this.w_AROPERAT
      this.oParentObject.w_MDQTAUM1 = CALQTAADV(this.oParentObject.w_MDQTAMOV, this.oParentObject.w_MDUNIMIS, this.w_ARUNMIS2, this.w_AROPERAT, this.w_ARMOLTIP, this.w_UNISEP, this.w_NOFRAZ, this.w_MODUM2,"", this.w_ARUNMIS3, 1,this.w_ARMOLTI3,,, this.oparentobject, "MDQTAMOV")
      this.oParentObject.w_MOLTIP = this.w_ARMOLTIP
      this.oParentObject.w_MOLTI3 = this.w_ARMOLTI3
      this.oParentObject.w_OPERAT = this.w_AROPERAT
      this.oParentObject.w_UNMIS1 = this.w_ARUNMIS1
      this.oParentObject.w_UNMIS2 = this.w_ARUNMIS2
      this.oParentObject.w_UNMIS3 = this.w_ARUNMIS3
      this.oParentObject.w_MDMAGSAL = this.oParentObject.w_MDCODMAG
      this.oParentObject.w_CATSCA = this.w_CATSCOA
      this.oParentObject.w_CPROWORD = this.w_RIGA
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MDCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.oParentObject.w_MDCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Mi appoggio all'evento per ricalcolare il prezzo da Listino Contratto
      this.w_PADRE.SaveRow()     
      this.w_PADRE.NotifyEvent("w_MDQTAMOV Changed")     
      * --- Memorizzo il prezzo in negativo che andr� a valorizzare lo sconto promozione
      *     nella riga descrittiva inserita successivamente
      this.w_SCOPRO = -this.oParentObject.w_VALRIG
    endif
    * --- Calcolo promozione
    do case
      case this.w_TIPPRO="S"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_SCOPRO = cp_ROUND( - this.w_SCONTO,this.oParentObject.w_DECTOT)
        this.w_STOPRO = cp_ROUND(this.w_SCONTO,this.oParentObject.w_DECTOT)
      case this.w_TIPPRO="F"
        if this.w_TIPFID="NPF"
          this.w_PUNTI = this.w_PUNEXT
        else
          do case
            case this.w_TIPVAQ="Q"
              this.w_PUNTI = (this.w_PUNEXT * (INT(this.w_TOTPZ / this.w_MINPZ)))
            case this.w_TIPVAQ="V"
              this.w_PUNTI = (this.w_PUNEXT * (INT(this.w_TOTIMP / this.w_MINIMP)))
          endcase
        endif
        this.w_DESPRO = ALLTRIM(LEFT(this.w_DESPRO,25))+ " " + ah_Msgformat("N. punti: %1", ALLTRIM(STR(this.w_PUNTI)) )
    endcase
    * --- La Promozione viene applicata
    if this.w_SCONTO<>0 OR this.w_PUNTI<>0 OR this.w_TIPPRO="O"
      if this.w_FLESCU="E"
        * --- Nel caso di Promozione Esclusiva valorizzo il campo dedicato a 'E'
        *     per tutti gli articoli della promozione
         
 SELECT (this.w_PADRE.cTrsName) 
 REPLACE t_MDFLESCL WITH "E" , i_Srv WITH IIF(i_Srv="A",i_Srv,"U") FOR t_FLPAN="S" 
 
      else
        * --- Nel caso di Promozione Comulativa valorizzo il campo dedicato a 'C'
        *     per tutti gli articoli della promozione
         
 SELECT (this.w_PADRE.cTrsName) 
 REPLACE t_MDFLESCL WITH "C" , i_Srv WITH IIF(i_Srv="A",i_Srv,"U") FOR t_FLPAN="S"
      endif
      * --- Aggiungo una riga in append
      this.w_PADRE.AddRow()     
      this.oParentObject.w_MDCODICE = this.w_ARTDES
      this.oParentObject.w_MDCODART = this.w_ARTDES
      this.oParentObject.w_MDDESART = this.w_DESPRO
      this.oParentObject.w_MDCODPRO = this.w_CODPRO
      this.oParentObject.w_MDQTAMOV = 0
      this.oParentObject.w_MDQTAUM1 = 0
      this.oParentObject.w_MDTIPRIG = "D"
      this.oParentObject.w_MDSCOPRO = this.w_SCOPRO
      this.oParentObject.w_MDSTOPRO = this.oParentObject.w_MDSTOPRO - this.w_STOPRO
      this.oParentObject.w_MDPUNFID = this.oParentObject.w_MDPUNFID+this.w_PUNTI
      this.oParentObject.w_CPROWORD = this.w_RIGA
      this.w_PADRE.SaveRow()     
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Promozioni con effetto sconto 
    do case
      case this.w_TIPSCO="SPT"
        this.w_SCONTO = (this.w_TOTIMP * this.w_SCPER)/ 100
      case this.w_TIPSCO="SPE"
        if this.w_TIPVAQ="Q"
          this.w_SCONTO = ((((this.w_TOTPZ- this.w_MINPZ) * (this.w_TOTIMP / this.w_TOTPZ)) * this.w_SCPER) / 100)
        else
          this.w_SCONTO = (((this.w_TOTIMP - this.w_MINIMP) * this.w_SCPER) / 100)
        endif
      case this.w_TIPSCO="SVF"
        this.w_SCONTO = this.w_SCVAL
      case this.w_TIPSCO="SVM"
        do case
          case this.w_TIPVAQ="Q"
            this.w_SCONTO = (INT(this.w_TOTPZ/this.w_MINPZ) * this.w_SCVAL)
          case this.w_TIPVAQ="V"
            this.w_SCONTO = (INT(this.w_TOTIMP/this.w_MINIMP) * this.w_SCVAL)
          case this.w_TIPVAQ="B"
            this.w_SCONTO = (INT(this.oParentObject.w_MDPUNFID/this.w_MINBOL) * this.w_SCVAL)
        endcase
      case this.w_TIPSCO="SVP"
        this.w_SCONTO = this.w_TOTPZ * this.w_SCVAL
      case this.w_TIPSCO="SVE"
        do case
          case this.w_TIPVAQ="Q"
            this.w_SCONTO = (((this.w_TOTPZ/this.w_MINPZ) * this.w_SCVAL))
          case this.w_TIPVAQ="V"
            this.w_SCONTO = ((this.w_TOTIMP - this.w_MINIMP) / (this.w_TOTIMP * this.w_SCVAL * this.w_TOTPZ))
        endcase
      case this.w_TIPSCO="PUP"
        this.w_SCONTO = (this.w_TOTIMP - (this.w_PRZUNI * this.w_TOTPZ))
      case this.w_TIPSCO="PTF"
        this.w_SCONTO = (this.w_TOTIMP - (this.w_PRZTOT + ((this.w_TOTPZ - this.w_MINPZ) * (this.w_TOTIMP / this.w_TOTPZ))))
      case this.w_TIPSCO="PTM"
        this.w_SCONTO = (this.w_TOTIMP - ((INT(this.w_TOTPZ / this.w_MINPZ) * this.w_PRZTOT) + (MOD(this.w_TOTPZ,this.w_MINPZ) * this.w_TOTIMP / this.w_TOTPZ)))
      case this.w_TIPSCO="NXM"
        this.w_SCONTO = ((INT(this.w_TOTPZ / this.w_QTAN)) * (this.w_QTAN - this.w_QTAM) * (this.w_TOTIMP / this.w_TOTPZ))
    endcase
    this.w_SCONTO = cp_Round(this.w_SCONTO,this.oParentObject.w_DECTOT)
    this.w_LOCATE = 0
    this.w_TSCOVEN = 0
    this.w_SCOVEN = 0
    if this.w_SCONTO > (this.oParentObject.w_MDTOTVEN - this.oParentObject.w_MDSCONTI - this.oParentObject.w_MDSTOPRO)
      * --- Se lo sconto promozione � maggiore del totale documento gi� scontato,
      *     imposto il suo valore al massimo per arrivare al totale documento 0
      *     in modo da non avere un totale documento negativo.
      *     Inoltre aggiorno la variabile w_DOCPOS a .F. poich� non devono essere 
      *     elaborate altre promozioni.
      this.w_SCONTO = (this.oParentObject.w_MDTOTVEN - this.oParentObject.w_MDSCONTI - this.oParentObject.w_MDSTOPRO)
      this.w_DOCPOS = .F.
    endif
    * --- Eseguo ciclo su TrsName per ripartizione sconto ventilato
    this.w_PADRE.FirstRow()     
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if this.w_PADRE.FullRow() And this.oParentObject.w_FLPAN="S" AND this.oParentObject.w_VALRIG<>0 AND this.oParentObject.w_FLSERA <>"S"
        * --- Segno posizione primo record su cui effettuo ripartizione per eventuale differenza finale
        if this.w_LOCATE=0
          this.w_LOCATE = this.w_PADRE.RowIndex()
        endif
        * --- t_MDSCOVEN � Negativo quindi sommo
        this.w_SCOVEN = - cp_ROUND(((this.w_SCONTO * ((NVL(this.oParentObject.w_VALRIG,0)) + NVL(this.oParentObject.w_MDSCOVEN,0))) / this.w_TOTIMP ),this.oParentObject.w_DECTOT)
        * --- Mantengo il totale ripartito per eventuali differenze: eseguo sottrazione poich�
        *     w_SCOVEN � negativo
        this.w_TSCOVEN = this.w_TSCOVEN + this.w_SCOVEN
        * --- Sommo al precedente sconto ventilato quello derivante dalla promozione esaminata
        this.oParentObject.w_MDSCOVEN = this.oParentObject.w_MDSCOVEN + this.w_SCOVEN
        this.oParentObject.w_MDPROMOZ = this.oParentObject.w_MDPROMOZ + this.w_SCOVEN
        this.w_PADRE.SaveRow()     
      endif
      this.w_PADRE.NextRow()     
    enddo
    * --- Se il totale ripartito � diverso dallo sconto effettivo modifico il primo record per la differenza
    if this.w_TSCOVEN<>this.w_SCONTO AND this.w_LOCATE<>0
      this.w_PADRE.SetRow(this.w_LOCATE)     
      this.w_SCOVEN = this.w_SCONTO+ this.w_TSCOVEN
      this.oParentObject.w_MDSCOVEN = this.oParentObject.w_MDSCOVEN - this.w_SCOVEN
      this.w_PADRE.SaveRow()     
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='REP_ARTI'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='UNIMIS'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
