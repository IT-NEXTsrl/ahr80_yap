* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bim                                                        *
*              Import da documenti collegati                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_84]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-02                                                      *
* Last revis.: 2003-04-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bim",oParentObject)
return(i_retval)

define class tgsps_bim as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  w_OREC = 0
  w_APPO2 = space(10)
  w_RECTRS = 0
  w_FLIMPA = space(1)
  w_MVCODPAG = space(5)
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCOPAG = 0
  w_OKSCF = .f.
  w_OKACO = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Ricerca da Import Documenti Collegati (da GSPS_KIM)
    * --- Variabili dal Documento di Destinazione
    * --- Pagamento
    * --- Accompagnatori
    WITH this.oParentObject.oParentObject
    this.w_FLIMPA = .w_FLIMPA
    this.w_MVCODPAG = .w_MDCODPAG
    this.w_MVSCOCL1 = .w_MDSCOCL1
    this.w_MVSCOCL2 = .w_MDSCOCL2
    this.w_MVSCOPAG = .w_MDSCOPAG
    ENDWITH
    SELECT (this.oParentObject.oParentObject.cTrsName)
    this.w_RECTRS = reccount()
    if this.w_RECTRS>0
      this.w_OREC = RECNO()
      GOTO TOP
      this.w_RECTRS = IIF(EMPTY(NVL(t_MDCODICE," ")), 0, 1)
      if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
        GOTO this.w_OREC
      else
        GO TOP
      endif
    endif
    this.w_ZOOM = this.oParentObject.w_ZoomMast
    * --- Lancia la Query
    ah_Msg("Ricerca documenti da importare...",.T.)
    this.oParentObject.NotifyEvent("Calcola")
    WAIT CLEAR
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if UPPER(This.w_Zoom.grd.Column&NC..ControlSource)<>"XCHK"
      this.w_APPO2 = 'IIF(FLCONG<>" " AND XCHK<>1, RGB(255,0,255), IIF(XCHK=0, RGB(0,0,0), IIF(FLDOCU="S", RGB(0,0,255), RGB(255,0,0))))'
      This.w_Zoom.grd.Column&NC..DynamicForeColor = this.w_APPO2
    endif
    ENDFOR
    * --- Crea il Cursore di Appoggio precaricato dei dati del Temporaneo documenti
    CREATE CURSOR RigheDoc ;
    (XCHK N(1), MVQTAMOV N(12,3), MVQTAEVA N(12,3), MVIMPEVA N(18,5), MVSERIAL C(10), CPROWNUM N(4,0), ;
    CPROWORD N(5,0), FLDOCU C(1), MVTIPRIG C(1))
    * --- Lettura Righe Documento
    NC = this.oParentObject.oParentObject.cTrsName
    SELECT (NC)
    this.w_OREC = RECNO()
    GO TOP
    SCAN FOR (NOT EMPTY(NVL(t_MDSERRIF," "))) AND NVL(t_MDROWRIF, 0)<>0 AND NOT DELETED()
    INSERT INTO RigheDoc ;
    (XCHK, MVQTAMOV, MVQTAEVA, MVIMPEVA, MVSERIAL, CPROWNUM, CPROWORD, FLDOCU, MVTIPRIG) ;
    VALUES ;
    (IIF(&NC..t_MDFLERIF="S", 1, 0), &NC..t_MDQTAMOV, &NC..t_MDQTAIMP, ;
    &NC..t_MDPREZZO, &NC..t_MDSERRIF, &NC..t_MDROWRIF, ;
    &NC..t_CPROWORD, "S", &NC..t_MDTIPRIG)
    SELECT (NC)
    ENDSCAN
    if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
      GOTO this.w_OREC
    else
      GO TOP
    endif
    NC = this.w_Zoom.cCursor
    * --- Aggiorna sul Master le Righe gia' presenti sul Documento (non deve modificarle)
    UPDATE (NC) SET XCHK = 1, FLDOCU = "S" WHERE EXIST ;
    (SELECT * FROM RigheDoc WHERE &NC..MVSERIAL = RigheDoc.MVSERIAL)
    SELECT (NC)
    this.w_OKSCF = .F.
    this.w_OKACO = .F.
    if this.w_RECTRS<>0
      GO TOP
      SCAN FOR XCHK=1 AND FLDOCU="S" 
      * --- Verifica Presenza Documenti Sconti Forzati
      this.w_OKSCF = IIF(NVL(MVFLFOSC, " ")="S", .T., this.w_OKSCF)
      * --- Verifica Presenza Documenti Acconti Contestuali gia' Importati
      this.w_OKACO = IIF(NVL(MVFLCAPA, " ")="S" AND NVL(MVACCONT, 0)<>0, .T., this.w_OKACO)
      ENDSCAN
    endif
    * --- Controlla la Congruenza tra i Doc di Origine e Destinazione
    GO TOP
    SCAN FOR NOT EMPTY(MVSERIAL)
    if this.w_RECTRS<>0
      if this.w_FLIMPA="S" AND FLCONG=" " AND NOT EMPTY(NVL(MVCODCON,""))
        do case
          case NVL(MVCODPAG, SPACE(5))<>this.w_MVCODPAG
            REPLACE FLCONG WITH "3"
          case NVL(MVSCOCL1, 0)<>this.w_MVSCOCL1 OR NVL(MVSCOCL2, 0)<>this.w_MVSCOCL2 OR NVL(MVSCOPAG, 0)<>this.w_MVSCOPAG
            REPLACE FLCONG WITH "4"
        endcase
      endif
      if (this.w_OKSCF=.T. OR this.w_OKACO=.T.) AND (XCHK<>1 OR FLDOCU<>"S") AND FLCONG=" "
        do case
          case this.w_OKSCF=.T.
            REPLACE FLCONG WITH "8"
          case this.w_OKACO=.T. AND NVL(MVFLCAPA, " ")="S" AND NVL(MVACCONT, 0)<>0
            REPLACE FLCONG WITH "8"
        endcase
      endif
    endif
    ENDSCAN
    GO TOP
    this.oParentObject.NotifyEvent("CalcRig")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
