* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_amv                                                        *
*              Modalit� di vendita                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_148]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-04                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_amv"))

* --- Class definition
define class tgsps_amv as StdForm
  Top    = 10
  Left   = 8

  * --- Standard Properties
  Width  = 589
  Height = 349+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=210327657
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=84

  * --- Constant Properties
  MOD_VEND_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  CLI_VEND_IDX = 0
  LISTINI_IDX = 0
  TIP_DOCU_IDX = 0
  PAG_AMEN_IDX = 0
  PAR_VDET_IDX = 0
  CAU_CONT_IDX = 0
  cFile = "MOD_VEND"
  cKeySelect = "MOCODICE"
  cKeyWhere  = "MOCODICE=this.w_MOCODICE"
  cKeyWhereODBC = '"MOCODICE="+cp_ToStrODBC(this.w_MOCODICE)';

  cKeyWhereODBCqualified = '"MOD_VEND.MOCODICE="+cp_ToStrODBC(this.w_MOCODICE)';

  cPrg = "gsps_amv"
  cComment = "Modalit� di vendita"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZIENDA = space(5)
  w_FLVEAC = space(1)
  w_FILDT = space(2)
  w_FILRF = space(2)
  w_FILFA = space(2)
  w_MOCODICE = space(5)
  o_MOCODICE = space(5)
  w_MODESCRI = space(35)
  w_READPAR = space(3)
  w_DOCCOR = space(5)
  w_DOCRIC = space(5)
  w_DOCRIF = space(5)
  w_DOCDDT = space(5)
  w_DOCFAT = space(5)
  w_DOCFAF = space(5)
  w_MOCODMAG = space(5)
  w_MOCODCLI = space(15)
  w_MOCODLIS = space(5)
  w_MOTIPCHI = space(2)
  w_MOQTADEF = 0
  w_MONUMSCO = 0
  w_MOFINASS = space(1)
  w_MOPRZVAC = space(1)
  w_MOPRZDES = space(1)
  w_MOFLGDES = space(1)
  w_MOFLUNIM = space(1)
  w_MOFLPREZ = space(1)
  w_MOFLSCON = space(1)
  w_DESMAG = space(30)
  w_CLDESCRI = space(40)
  w_DESLIS = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MODOCCOR = space(5)
  w_MODOCRIC = space(5)
  w_MODOCFAF = space(5)
  o_MODOCFAF = space(5)
  w_DESFAF = space(35)
  w_MODOCFAT = space(5)
  o_MODOCFAT = space(5)
  w_MODOCRIF = space(5)
  w_MODOCDDT = space(5)
  w_DESCOR = space(35)
  w_DESRIC = space(35)
  w_DESFAT = space(35)
  w_DESRIF = space(35)
  w_DESDDT = space(35)
  w_DESPA1 = space(30)
  w_MODOCCO1 = space(5)
  w_MODOCCO2 = space(5)
  w_MODOCCO3 = space(5)
  w_MOCODPAG = space(5)
  w_DESCO1 = space(35)
  w_DESCO2 = space(35)
  w_DESCO3 = space(35)
  w_LISVAL = space(3)
  w_CATCOR = space(2)
  w_CATRIC = space(2)
  w_CATFAF = space(2)
  w_CATRIF = space(2)
  w_CATFAT = space(2)
  w_CATDDT = space(2)
  w_FLRIF = space(1)
  w_FLDDT = space(1)
  w_FLFAT = space(1)
  w_FLFAF = space(1)
  w_CATDO1 = space(2)
  w_FLINT1 = space(1)
  w_CATDO2 = space(2)
  w_FLINT2 = space(1)
  w_CATDO3 = space(2)
  w_FLINT3 = space(1)
  w_FLRI1 = space(1)
  w_FLRI2 = space(1)
  w_CAUCO1 = space(5)
  w_CAUCO2 = space(5)
  w_TIPDO1 = space(2)
  w_TIPDO2 = space(2)
  w_CODMAT = space(5)
  w_CAUFAF = space(5)
  w_CASFAF = space(1)
  w_FLRISC = space(1)
  w_RESCHK = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_VEND','gsps_amv')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_amvPag1","gsps_amv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(1).HelpContextID = 52173591
      .Pages(2).addobject("oPag","tgsps_amvPag2","gsps_amv",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Documenti")
      .Pages(2).HelpContextID = 264393274
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMOCODICE_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CLI_VEND'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='PAG_AMEN'
    this.cWorkTables[7]='PAR_VDET'
    this.cWorkTables[8]='CAU_CONT'
    this.cWorkTables[9]='MOD_VEND'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_VEND_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_VEND_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MOCODICE = NVL(MOCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_19_joined
    link_2_19_joined=.f.
    local link_2_21_joined
    link_2_21_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_VEND where MOCODICE=KeySet.MOCODICE
    *
    i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_VEND')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_VEND.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_VEND '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_19_joined=this.AddJoinedLink_2_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_21_joined=this.AddJoinedLink_2_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZIENDA = i_codazi
        .w_FLVEAC = 'V'
        .w_DOCCOR = space(5)
        .w_DOCRIC = space(5)
        .w_DOCRIF = space(5)
        .w_DOCDDT = space(5)
        .w_DOCFAT = space(5)
        .w_DOCFAF = space(5)
        .w_DESMAG = space(30)
        .w_CLDESCRI = space(40)
        .w_DESLIS = space(40)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESFAF = space(35)
        .w_DESCOR = space(35)
        .w_DESRIC = space(35)
        .w_DESFAT = space(35)
        .w_DESRIF = space(35)
        .w_DESDDT = space(35)
        .w_DESPA1 = space(30)
        .w_DESCO1 = space(35)
        .w_DESCO2 = space(35)
        .w_DESCO3 = space(35)
        .w_LISVAL = space(3)
        .w_CATCOR = space(2)
        .w_CATRIC = space(2)
        .w_CATFAF = space(2)
        .w_CATRIF = space(2)
        .w_CATFAT = space(2)
        .w_CATDDT = space(2)
        .w_FLRIF = space(1)
        .w_FLDDT = space(1)
        .w_FLFAT = space(1)
        .w_FLFAF = space(1)
        .w_CATDO1 = space(2)
        .w_FLINT1 = space(1)
        .w_CATDO2 = space(2)
        .w_FLINT2 = space(1)
        .w_CATDO3 = space(2)
        .w_FLINT3 = space(1)
        .w_FLRI1 = space(1)
        .w_FLRI2 = space(1)
        .w_CAUCO1 = space(5)
        .w_CAUCO2 = space(5)
        .w_TIPDO1 = space(2)
        .w_TIPDO2 = space(2)
        .w_CODMAT = SPACE(5)
        .w_CAUFAF = space(5)
        .w_CASFAF = space(1)
        .w_FLRISC = space(1)
        .w_RESCHK = 0
        .w_FILDT = 'DT'
        .w_FILRF = 'RF'
        .w_FILFA = 'FA'
        .w_MOCODICE = NVL(MOCODICE,space(5))
        .w_MODESCRI = NVL(MODESCRI,space(35))
        .w_READPAR = g_CODNEG
          .link_1_9('Load')
        .w_MOCODMAG = NVL(MOCODMAG,space(5))
          if link_1_16_joined
            this.w_MOCODMAG = NVL(MGCODMAG116,NVL(this.w_MOCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG116,space(30))
          else
          .link_1_16('Load')
          endif
        .w_MOCODCLI = NVL(MOCODCLI,space(15))
          if link_1_18_joined
            this.w_MOCODCLI = NVL(CLCODCLI118,NVL(this.w_MOCODCLI,space(15)))
            this.w_CLDESCRI = NVL(CLDESCRI118,space(40))
          else
          .link_1_18('Load')
          endif
        .w_MOCODLIS = NVL(MOCODLIS,space(5))
          if link_1_20_joined
            this.w_MOCODLIS = NVL(LSCODLIS120,NVL(this.w_MOCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS120,space(40))
            this.w_LISVAL = NVL(LSVALLIS120,space(3))
          else
          .link_1_20('Load')
          endif
        .w_MOTIPCHI = NVL(MOTIPCHI,space(2))
        .w_MOQTADEF = NVL(MOQTADEF,0)
        .w_MONUMSCO = NVL(MONUMSCO,0)
        .w_MOFINASS = NVL(MOFINASS,space(1))
        .w_MOPRZVAC = NVL(MOPRZVAC,space(1))
        .w_MOPRZDES = NVL(MOPRZDES,space(1))
        .w_MOFLGDES = NVL(MOFLGDES,space(1))
        .w_MOFLUNIM = NVL(MOFLUNIM,space(1))
        .w_MOFLPREZ = NVL(MOFLPREZ,space(1))
        .w_MOFLSCON = NVL(MOFLSCON,space(1))
        .w_MODOCCOR = NVL(MODOCCOR,space(5))
          .link_2_1('Load')
        .w_MODOCRIC = NVL(MODOCRIC,space(5))
          .link_2_2('Load')
        .w_MODOCFAF = NVL(MODOCFAF,space(5))
          .link_2_3('Load')
        .w_MODOCFAT = NVL(MODOCFAT,space(5))
          .link_2_5('Load')
        .w_MODOCRIF = NVL(MODOCRIF,space(5))
          .link_2_6('Load')
        .w_MODOCDDT = NVL(MODOCDDT,space(5))
          .link_2_7('Load')
        .w_MODOCCO1 = NVL(MODOCCO1,space(5))
          if link_2_15_joined
            this.w_MODOCCO1 = NVL(TDTIPDOC215,NVL(this.w_MODOCCO1,space(5)))
            this.w_DESCO1 = NVL(TDDESDOC215,space(35))
            this.w_CATDO1 = NVL(TDCATDOC215,space(2))
            this.w_FLINT1 = NVL(TDFLINTE215,space(1))
            this.w_FLRISC = NVL(TDFLRISC215,space(1))
          else
          .link_2_15('Load')
          endif
        .w_MODOCCO2 = NVL(MODOCCO2,space(5))
          if link_2_17_joined
            this.w_MODOCCO2 = NVL(TDTIPDOC217,NVL(this.w_MODOCCO2,space(5)))
            this.w_DESCO2 = NVL(TDDESDOC217,space(35))
            this.w_CATDO2 = NVL(TDCATDOC217,space(2))
            this.w_FLINT2 = NVL(TDFLINTE217,space(1))
            this.w_FLRISC = NVL(TDFLRISC217,space(1))
          else
          .link_2_17('Load')
          endif
        .w_MODOCCO3 = NVL(MODOCCO3,space(5))
          if link_2_19_joined
            this.w_MODOCCO3 = NVL(TDTIPDOC219,NVL(this.w_MODOCCO3,space(5)))
            this.w_DESCO3 = NVL(TDDESDOC219,space(35))
            this.w_CATDO3 = NVL(TDCATDOC219,space(2))
            this.w_FLINT3 = NVL(TDFLINTE219,space(1))
            this.w_FLRISC = NVL(TDFLRISC219,space(1))
          else
          .link_2_19('Load')
          endif
        .w_MOCODPAG = NVL(MOCODPAG,space(5))
          if link_2_21_joined
            this.w_MOCODPAG = NVL(PACODICE221,NVL(this.w_MOCODPAG,space(5)))
            this.w_DESPA1 = NVL(PADESCRI221,space(30))
          else
          .link_2_21('Load')
          endif
          .link_2_55('Load')
          .link_2_56('Load')
          .link_2_60('Load')
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'MOD_VEND')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA = space(5)
      .w_FLVEAC = space(1)
      .w_FILDT = space(2)
      .w_FILRF = space(2)
      .w_FILFA = space(2)
      .w_MOCODICE = space(5)
      .w_MODESCRI = space(35)
      .w_READPAR = space(3)
      .w_DOCCOR = space(5)
      .w_DOCRIC = space(5)
      .w_DOCRIF = space(5)
      .w_DOCDDT = space(5)
      .w_DOCFAT = space(5)
      .w_DOCFAF = space(5)
      .w_MOCODMAG = space(5)
      .w_MOCODCLI = space(15)
      .w_MOCODLIS = space(5)
      .w_MOTIPCHI = space(2)
      .w_MOQTADEF = 0
      .w_MONUMSCO = 0
      .w_MOFINASS = space(1)
      .w_MOPRZVAC = space(1)
      .w_MOPRZDES = space(1)
      .w_MOFLGDES = space(1)
      .w_MOFLUNIM = space(1)
      .w_MOFLPREZ = space(1)
      .w_MOFLSCON = space(1)
      .w_DESMAG = space(30)
      .w_CLDESCRI = space(40)
      .w_DESLIS = space(40)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_MODOCCOR = space(5)
      .w_MODOCRIC = space(5)
      .w_MODOCFAF = space(5)
      .w_DESFAF = space(35)
      .w_MODOCFAT = space(5)
      .w_MODOCRIF = space(5)
      .w_MODOCDDT = space(5)
      .w_DESCOR = space(35)
      .w_DESRIC = space(35)
      .w_DESFAT = space(35)
      .w_DESRIF = space(35)
      .w_DESDDT = space(35)
      .w_DESPA1 = space(30)
      .w_MODOCCO1 = space(5)
      .w_MODOCCO2 = space(5)
      .w_MODOCCO3 = space(5)
      .w_MOCODPAG = space(5)
      .w_DESCO1 = space(35)
      .w_DESCO2 = space(35)
      .w_DESCO3 = space(35)
      .w_LISVAL = space(3)
      .w_CATCOR = space(2)
      .w_CATRIC = space(2)
      .w_CATFAF = space(2)
      .w_CATRIF = space(2)
      .w_CATFAT = space(2)
      .w_CATDDT = space(2)
      .w_FLRIF = space(1)
      .w_FLDDT = space(1)
      .w_FLFAT = space(1)
      .w_FLFAF = space(1)
      .w_CATDO1 = space(2)
      .w_FLINT1 = space(1)
      .w_CATDO2 = space(2)
      .w_FLINT2 = space(1)
      .w_CATDO3 = space(2)
      .w_FLINT3 = space(1)
      .w_FLRI1 = space(1)
      .w_FLRI2 = space(1)
      .w_CAUCO1 = space(5)
      .w_CAUCO2 = space(5)
      .w_TIPDO1 = space(2)
      .w_TIPDO2 = space(2)
      .w_CODMAT = space(5)
      .w_CAUFAF = space(5)
      .w_CASFAF = space(1)
      .w_FLRISC = space(1)
      .w_RESCHK = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_AZIENDA = i_codazi
        .w_FLVEAC = 'V'
        .w_FILDT = 'DT'
        .w_FILRF = 'RF'
        .w_FILFA = 'FA'
          .DoRTCalc(6,7,.f.)
        .w_READPAR = g_CODNEG
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_READPAR))
          .link_1_9('Full')
          endif
        .DoRTCalc(9,15,.f.)
          if not(empty(.w_MOCODMAG))
          .link_1_16('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_MOCODCLI))
          .link_1_18('Full')
          endif
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_MOCODLIS))
          .link_1_20('Full')
          endif
        .w_MOTIPCHI = 'ES'
          .DoRTCalc(19,19,.f.)
        .w_MONUMSCO = g_NUMSCO
          .DoRTCalc(21,22,.f.)
        .w_MOPRZDES = ' '
          .DoRTCalc(24,30,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(32,32,.f.)
        .w_MODOCCOR = IIF(not empty(.w_MOCODICE),.w_DOCCOR,SPACE(5))
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_MODOCCOR))
          .link_2_1('Full')
          endif
        .w_MODOCRIC = IIF(not empty(.w_MOCODICE),.w_DOCRIC,SPACE(5))
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_MODOCRIC))
          .link_2_2('Full')
          endif
        .w_MODOCFAF = IIF(not empty(.w_MOCODICE),.w_DOCFAF,SPACE(5))
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_MODOCFAF))
          .link_2_3('Full')
          endif
          .DoRTCalc(36,36,.f.)
        .w_MODOCFAT = IIF(not empty(.w_MOCODICE),.w_DOCFAT,SPACE(5))
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_MODOCFAT))
          .link_2_5('Full')
          endif
        .w_MODOCRIF = IIF(not empty(.w_MOCODICE),.w_DOCRIF,SPACE(5))
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_MODOCRIF))
          .link_2_6('Full')
          endif
        .w_MODOCDDT = IIF(not empty(.w_MOCODICE),.w_DOCDDT,SPACE(5))
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_MODOCDDT))
          .link_2_7('Full')
          endif
        .DoRTCalc(40,46,.f.)
          if not(empty(.w_MODOCCO1))
          .link_2_15('Full')
          endif
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_MODOCCO2))
          .link_2_17('Full')
          endif
        .DoRTCalc(48,48,.f.)
          if not(empty(.w_MODOCCO3))
          .link_2_19('Full')
          endif
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_MOCODPAG))
          .link_2_21('Full')
          endif
        .DoRTCalc(50,72,.f.)
          if not(empty(.w_CAUCO1))
          .link_2_55('Full')
          endif
        .DoRTCalc(73,73,.f.)
          if not(empty(.w_CAUCO2))
          .link_2_56('Full')
          endif
          .DoRTCalc(74,75,.f.)
        .w_CODMAT = SPACE(5)
        .DoRTCalc(77,77,.f.)
          if not(empty(.w_CAUFAF))
          .link_2_60('Full')
          endif
          .DoRTCalc(78,79,.f.)
        .w_RESCHK = 0
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_VEND')
    this.DoRTCalc(81,84,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMOCODICE_1_6.enabled = i_bVal
      .Page1.oPag.oMODESCRI_1_7.enabled = i_bVal
      .Page1.oPag.oMOCODMAG_1_16.enabled = i_bVal
      .Page1.oPag.oMOCODCLI_1_18.enabled = i_bVal
      .Page1.oPag.oMOCODLIS_1_20.enabled = i_bVal
      .Page1.oPag.oMOTIPCHI_1_21.enabled = i_bVal
      .Page1.oPag.oMOQTADEF_1_22.enabled = i_bVal
      .Page1.oPag.oMONUMSCO_1_23.enabled = i_bVal
      .Page1.oPag.oMOFINASS_1_24.enabled = i_bVal
      .Page1.oPag.oMOPRZVAC_1_25.enabled = i_bVal
      .Page1.oPag.oMOPRZDES_1_26.enabled = i_bVal
      .Page1.oPag.oMOFLGDES_1_28.enabled = i_bVal
      .Page1.oPag.oMOFLUNIM_1_29.enabled = i_bVal
      .Page1.oPag.oMOFLPREZ_1_31.enabled = i_bVal
      .Page1.oPag.oMOFLSCON_1_32.enabled = i_bVal
      .Page2.oPag.oMODOCCOR_2_1.enabled = i_bVal
      .Page2.oPag.oMODOCRIC_2_2.enabled = i_bVal
      .Page2.oPag.oMODOCFAF_2_3.enabled = i_bVal
      .Page2.oPag.oMODOCFAT_2_5.enabled = i_bVal
      .Page2.oPag.oMODOCRIF_2_6.enabled = i_bVal
      .Page2.oPag.oMODOCDDT_2_7.enabled = i_bVal
      .Page2.oPag.oMODOCCO1_2_15.enabled = i_bVal
      .Page2.oPag.oMODOCCO2_2_17.enabled = i_bVal
      .Page2.oPag.oMODOCCO3_2_19.enabled = i_bVal
      .Page2.oPag.oMOCODPAG_2_21.enabled = i_bVal
      .Page2.oPag.oObj_2_64.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMOCODICE_1_6.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMOCODICE_1_6.enabled = .t.
        .Page1.oPag.oMODESCRI_1_7.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOD_VEND',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODICE,"MOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODESCRI,"MODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODMAG,"MOCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODCLI,"MOCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODLIS,"MOCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOTIPCHI,"MOTIPCHI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOQTADEF,"MOQTADEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MONUMSCO,"MONUMSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOFINASS,"MOFINASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPRZVAC,"MOPRZVAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPRZDES,"MOPRZDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOFLGDES,"MOFLGDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOFLUNIM,"MOFLUNIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOFLPREZ,"MOFLPREZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOFLSCON,"MOFLSCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCCOR,"MODOCCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCRIC,"MODOCRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCFAF,"MODOCFAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCFAT,"MODOCFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCRIF,"MODOCRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCDDT,"MODOCDDT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCCO1,"MODOCCO1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCCO2,"MODOCCO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODOCCO3,"MODOCCO3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODPAG,"MOCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
    i_lTable = "MOD_VEND"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_VEND_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSPS_SMV with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_VEND_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_VEND
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_VEND')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_VEND')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MOCODICE,MODESCRI,MOCODMAG,MOCODCLI,MOCODLIS"+;
                  ",MOTIPCHI,MOQTADEF,MONUMSCO,MOFINASS,MOPRZVAC"+;
                  ",MOPRZDES,MOFLGDES,MOFLUNIM,MOFLPREZ,MOFLSCON"+;
                  ",MODOCCOR,MODOCRIC,MODOCFAF,MODOCFAT,MODOCRIF"+;
                  ",MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOCODPAG"+;
                  ",UTCC,UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MOCODICE)+;
                  ","+cp_ToStrODBC(this.w_MODESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODMAG)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODCLI)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODLIS)+;
                  ","+cp_ToStrODBC(this.w_MOTIPCHI)+;
                  ","+cp_ToStrODBC(this.w_MOQTADEF)+;
                  ","+cp_ToStrODBC(this.w_MONUMSCO)+;
                  ","+cp_ToStrODBC(this.w_MOFINASS)+;
                  ","+cp_ToStrODBC(this.w_MOPRZVAC)+;
                  ","+cp_ToStrODBC(this.w_MOPRZDES)+;
                  ","+cp_ToStrODBC(this.w_MOFLGDES)+;
                  ","+cp_ToStrODBC(this.w_MOFLUNIM)+;
                  ","+cp_ToStrODBC(this.w_MOFLPREZ)+;
                  ","+cp_ToStrODBC(this.w_MOFLSCON)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCCOR)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCRIC)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCFAF)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCFAT)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCRIF)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCDDT)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCCO1)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCCO2)+;
                  ","+cp_ToStrODBCNull(this.w_MODOCCO3)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODPAG)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_VEND')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_VEND')
        cp_CheckDeletedKey(i_cTable,0,'MOCODICE',this.w_MOCODICE)
        INSERT INTO (i_cTable);
              (MOCODICE,MODESCRI,MOCODMAG,MOCODCLI,MOCODLIS,MOTIPCHI,MOQTADEF,MONUMSCO,MOFINASS,MOPRZVAC,MOPRZDES,MOFLGDES,MOFLUNIM,MOFLPREZ,MOFLSCON,MODOCCOR,MODOCRIC,MODOCFAF,MODOCFAT,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOCODPAG,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MOCODICE;
                  ,this.w_MODESCRI;
                  ,this.w_MOCODMAG;
                  ,this.w_MOCODCLI;
                  ,this.w_MOCODLIS;
                  ,this.w_MOTIPCHI;
                  ,this.w_MOQTADEF;
                  ,this.w_MONUMSCO;
                  ,this.w_MOFINASS;
                  ,this.w_MOPRZVAC;
                  ,this.w_MOPRZDES;
                  ,this.w_MOFLGDES;
                  ,this.w_MOFLUNIM;
                  ,this.w_MOFLPREZ;
                  ,this.w_MOFLSCON;
                  ,this.w_MODOCCOR;
                  ,this.w_MODOCRIC;
                  ,this.w_MODOCFAF;
                  ,this.w_MODOCFAT;
                  ,this.w_MODOCRIF;
                  ,this.w_MODOCDDT;
                  ,this.w_MODOCCO1;
                  ,this.w_MODOCCO2;
                  ,this.w_MODOCCO3;
                  ,this.w_MOCODPAG;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_VEND_IDX,i_nConn)
      *
      * update MOD_VEND
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_VEND')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MODESCRI="+cp_ToStrODBC(this.w_MODESCRI)+;
             ",MOCODMAG="+cp_ToStrODBCNull(this.w_MOCODMAG)+;
             ",MOCODCLI="+cp_ToStrODBCNull(this.w_MOCODCLI)+;
             ",MOCODLIS="+cp_ToStrODBCNull(this.w_MOCODLIS)+;
             ",MOTIPCHI="+cp_ToStrODBC(this.w_MOTIPCHI)+;
             ",MOQTADEF="+cp_ToStrODBC(this.w_MOQTADEF)+;
             ",MONUMSCO="+cp_ToStrODBC(this.w_MONUMSCO)+;
             ",MOFINASS="+cp_ToStrODBC(this.w_MOFINASS)+;
             ",MOPRZVAC="+cp_ToStrODBC(this.w_MOPRZVAC)+;
             ",MOPRZDES="+cp_ToStrODBC(this.w_MOPRZDES)+;
             ",MOFLGDES="+cp_ToStrODBC(this.w_MOFLGDES)+;
             ",MOFLUNIM="+cp_ToStrODBC(this.w_MOFLUNIM)+;
             ",MOFLPREZ="+cp_ToStrODBC(this.w_MOFLPREZ)+;
             ",MOFLSCON="+cp_ToStrODBC(this.w_MOFLSCON)+;
             ",MODOCCOR="+cp_ToStrODBCNull(this.w_MODOCCOR)+;
             ",MODOCRIC="+cp_ToStrODBCNull(this.w_MODOCRIC)+;
             ",MODOCFAF="+cp_ToStrODBCNull(this.w_MODOCFAF)+;
             ",MODOCFAT="+cp_ToStrODBCNull(this.w_MODOCFAT)+;
             ",MODOCRIF="+cp_ToStrODBCNull(this.w_MODOCRIF)+;
             ",MODOCDDT="+cp_ToStrODBCNull(this.w_MODOCDDT)+;
             ",MODOCCO1="+cp_ToStrODBCNull(this.w_MODOCCO1)+;
             ",MODOCCO2="+cp_ToStrODBCNull(this.w_MODOCCO2)+;
             ",MODOCCO3="+cp_ToStrODBCNull(this.w_MODOCCO3)+;
             ",MOCODPAG="+cp_ToStrODBCNull(this.w_MOCODPAG)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_VEND')
        i_cWhere = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
        UPDATE (i_cTable) SET;
              MODESCRI=this.w_MODESCRI;
             ,MOCODMAG=this.w_MOCODMAG;
             ,MOCODCLI=this.w_MOCODCLI;
             ,MOCODLIS=this.w_MOCODLIS;
             ,MOTIPCHI=this.w_MOTIPCHI;
             ,MOQTADEF=this.w_MOQTADEF;
             ,MONUMSCO=this.w_MONUMSCO;
             ,MOFINASS=this.w_MOFINASS;
             ,MOPRZVAC=this.w_MOPRZVAC;
             ,MOPRZDES=this.w_MOPRZDES;
             ,MOFLGDES=this.w_MOFLGDES;
             ,MOFLUNIM=this.w_MOFLUNIM;
             ,MOFLPREZ=this.w_MOFLPREZ;
             ,MOFLSCON=this.w_MOFLSCON;
             ,MODOCCOR=this.w_MODOCCOR;
             ,MODOCRIC=this.w_MODOCRIC;
             ,MODOCFAF=this.w_MODOCFAF;
             ,MODOCFAT=this.w_MODOCFAT;
             ,MODOCRIF=this.w_MODOCRIF;
             ,MODOCDDT=this.w_MODOCDDT;
             ,MODOCCO1=this.w_MODOCCO1;
             ,MODOCCO2=this.w_MODOCCO2;
             ,MODOCCO3=this.w_MODOCCO3;
             ,MOCODPAG=this.w_MOCODPAG;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_VEND_IDX,i_nConn)
      *
      * delete MOD_VEND
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_FILDT = 'DT'
            .w_FILRF = 'RF'
            .w_FILFA = 'FA'
        .DoRTCalc(6,7,.t.)
        if .o_MOCODICE<>.w_MOCODICE
            .w_READPAR = g_CODNEG
          .link_1_9('Full')
        endif
        .DoRTCalc(9,32,.t.)
        if .o_MOCODICE<>.w_MOCODICE
            .w_MODOCCOR = IIF(not empty(.w_MOCODICE),.w_DOCCOR,SPACE(5))
          .link_2_1('Full')
        endif
        if .o_MOCODICE<>.w_MOCODICE
            .w_MODOCRIC = IIF(not empty(.w_MOCODICE),.w_DOCRIC,SPACE(5))
          .link_2_2('Full')
        endif
        if .o_MOCODICE<>.w_MOCODICE
            .w_MODOCFAF = IIF(not empty(.w_MOCODICE),.w_DOCFAF,SPACE(5))
          .link_2_3('Full')
        endif
        .DoRTCalc(36,36,.t.)
        if .o_MOCODICE<>.w_MOCODICE
            .w_MODOCFAT = IIF(not empty(.w_MOCODICE),.w_DOCFAT,SPACE(5))
          .link_2_5('Full')
        endif
        if .o_MOCODICE<>.w_MOCODICE
            .w_MODOCRIF = IIF(not empty(.w_MOCODICE),.w_DOCRIF,SPACE(5))
          .link_2_6('Full')
        endif
        if .o_MOCODICE<>.w_MOCODICE
            .w_MODOCDDT = IIF(not empty(.w_MOCODICE),.w_DOCDDT,SPACE(5))
          .link_2_7('Full')
        endif
        .DoRTCalc(40,71,.t.)
        if .o_MODOCFAF<>.w_MODOCFAF
          .link_2_55('Full')
        endif
        if .o_MODOCFAT<>.w_MODOCFAT
          .link_2_56('Full')
        endif
        .DoRTCalc(74,76,.t.)
        if .o_MODOCFAF<>.w_MODOCFAF
          .link_2_60('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(78,84,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_64.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
    i_lTable = "PAR_VDET"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2], .t., this.PAR_VDET_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACODNEG,PADOCCOR,PADOCRIC,PADOCFAT,PADOCFAF,PADOCRIF,PADOCDDT";
                   +" from "+i_cTable+" "+i_lTable+" where PACODNEG="+cp_ToStrODBC(this.w_READPAR);
                   +" and PACODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_AZIENDA;
                       ,'PACODNEG',this.w_READPAR)
            select PACODAZI,PACODNEG,PADOCCOR,PADOCRIC,PADOCFAT,PADOCFAF,PADOCRIF,PADOCDDT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PACODNEG,space(3))
      this.w_DOCCOR = NVL(_Link_.PADOCCOR,space(5))
      this.w_DOCRIC = NVL(_Link_.PADOCRIC,space(5))
      this.w_DOCFAT = NVL(_Link_.PADOCFAT,space(5))
      this.w_DOCFAF = NVL(_Link_.PADOCFAF,space(5))
      this.w_DOCRIF = NVL(_Link_.PADOCRIF,space(5))
      this.w_DOCDDT = NVL(_Link_.PADOCDDT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(3)
      endif
      this.w_DOCCOR = space(5)
      this.w_DOCRIC = space(5)
      this.w_DOCFAT = space(5)
      this.w_DOCFAF = space(5)
      this.w_DOCRIF = space(5)
      this.w_DOCDDT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)+'\'+cp_ToStr(_Link_.PACODNEG,1)
      cp_ShowWarn(i_cKey,this.PAR_VDET_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCODMAG
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MOCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MOCODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMOCODMAG_1_16'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MOCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MOCODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.MGCODMAG as MGCODMAG116"+ ",link_1_16.MGDESMAG as MGDESMAG116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on MOD_VEND.MOCODMAG=link_1_16.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and MOD_VEND.MOCODMAG=link_1_16.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODCLI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACV',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_MOCODCLI)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_MOCODCLI))
          select CLCODCLI,CLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODCLI)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oMOCODCLI_1_18'),i_cWhere,'GSPS_ACV',"Clienti negozio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_MOCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_MOCODCLI)
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODCLI = NVL(_Link_.CLCODCLI,space(15))
      this.w_CLDESCRI = NVL(_Link_.CLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODCLI = space(15)
      endif
      this.w_CLDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLI_VEND_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.CLCODCLI as CLCODCLI118"+ ",link_1_18.CLDESCRI as CLDESCRI118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on MOD_VEND.MOCODCLI=link_1_18.CLCODCLI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and MOD_VEND.MOCODCLI=link_1_18.CLCODCLI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODLIS
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_MOCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_MOCODLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oMOCODLIS_1_20'),i_cWhere,'GSAR_ALI',"Listini articoli",'GSPS_MVD.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_MOCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_MOCODLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_LISVAL = NVL(_Link_.LSVALLIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_LISVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LISVAL=g_PERVAL
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_LISVAL = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.LSCODLIS as LSCODLIS120"+ ",link_1_20.LSDESLIS as LSDESLIS120"+ ",link_1_20.LSVALLIS as LSVALLIS120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on MOD_VEND.MOCODLIS=link_1_20.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and MOD_VEND.MOCODLIS=link_1_20.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MODOCCOR
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCCOR)+"%");
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);

          i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDCATDOC',this.w_FILRF;
                     ,'TDTIPDOC',trim(this.w_MODOCCOR))
          select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCCOR)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCCOR) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCCOR_2_1'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FILRF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',oSource.xKey(1);
                       ,'TDTIPDOC',oSource.xKey(2))
            select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCCOR);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',this.w_FILRF;
                       ,'TDTIPDOC',this.w_MODOCCOR)
            select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCCOR = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCOR = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATCOR = NVL(_Link_.TDCATDOC,space(2))
      this.w_CODMAT = NVL(_Link_.TDCODMAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCCOR = space(5)
      endif
      this.w_DESCOR = space(35)
      this.w_CATCOR = space(2)
      this.w_CODMAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATCOR='RF' AND EMPTY(.w_CODMAT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        endif
        this.w_MODOCCOR = space(5)
        this.w_DESCOR = space(35)
        this.w_CATCOR = space(2)
        this.w_CODMAT = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODOCRIC
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCRIC)+"%");
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);

          i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDCATDOC',this.w_FILRF;
                     ,'TDTIPDOC',trim(this.w_MODOCRIC))
          select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCRIC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCRIC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCRIC_2_2'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FILRF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',oSource.xKey(1);
                       ,'TDTIPDOC',oSource.xKey(2))
            select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCRIC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILRF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',this.w_FILRF;
                       ,'TDTIPDOC',this.w_MODOCRIC)
            select TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCRIC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESRIC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATRIC = NVL(_Link_.TDCATDOC,space(2))
      this.w_CODMAT = NVL(_Link_.TDCODMAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCRIC = space(5)
      endif
      this.w_DESRIC = space(35)
      this.w_CATRIC = space(2)
      this.w_CODMAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATRIC='RF' AND EMPTY(.w_CODMAT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        endif
        this.w_MODOCRIC = space(5)
        this.w_DESRIC = space(35)
        this.w_CATRIC = space(2)
        this.w_CODMAT = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODOCFAF
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCFAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCFAF)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT,TDCAUMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILFA;
                     ,'TDTIPDOC',trim(this.w_MODOCFAF))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT,TDCAUMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCFAF)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCFAF) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCFAF_2_3'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILFA<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCFAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCFAF);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILFA;
                       ,'TDTIPDOC',this.w_MODOCFAF)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCFAF = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESFAF = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATFAF = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLFAF = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLRI1 = NVL(_Link_.TDFLRICE,space(1))
      this.w_CAUCO1 = NVL(_Link_.TDCAUCON,space(5))
      this.w_CODMAT = NVL(_Link_.TDCODMAT,space(5))
      this.w_CAUFAF = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCFAF = space(5)
      endif
      this.w_DESFAF = space(35)
      this.w_CATFAF = space(2)
      this.w_FLFAF = space(1)
      this.w_FLRI1 = space(1)
      this.w_CAUCO1 = space(5)
      this.w_CODMAT = space(5)
      this.w_CAUFAF = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATFAF='FA' AND .w_FLFAF='V' AND EMPTY(.w_CODMAT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        endif
        this.w_MODOCFAF = space(5)
        this.w_DESFAF = space(35)
        this.w_CATFAF = space(2)
        this.w_FLFAF = space(1)
        this.w_FLRI1 = space(1)
        this.w_CAUCO1 = space(5)
        this.w_CODMAT = space(5)
        this.w_CAUFAF = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCFAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODOCFAT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCFAT)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILFA;
                     ,'TDTIPDOC',trim(this.w_MODOCFAT))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCFAT)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCFAT) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCFAT_2_5'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILFA<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCFAT);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILFA;
                       ,'TDTIPDOC',this.w_MODOCFAT)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCFAT = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESFAT = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATFAT = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLFAT = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLRI2 = NVL(_Link_.TDFLRICE,space(1))
      this.w_CAUCO2 = NVL(_Link_.TDCAUCON,space(5))
      this.w_CODMAT = NVL(_Link_.TDCODMAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCFAT = space(5)
      endif
      this.w_DESFAT = space(35)
      this.w_CATFAT = space(2)
      this.w_FLFAT = space(1)
      this.w_FLRI2 = space(1)
      this.w_CAUCO2 = space(5)
      this.w_CODMAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATFAT='FA' AND .w_FLFAT='V' AND EMPTY(.w_CODMAT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        endif
        this.w_MODOCFAT = space(5)
        this.w_DESFAT = space(35)
        this.w_CATFAT = space(2)
        this.w_FLFAT = space(1)
        this.w_FLRI2 = space(1)
        this.w_CAUCO2 = space(5)
        this.w_CODMAT = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODOCRIF
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCRIF)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILDT;
                     ,'TDTIPDOC',trim(this.w_MODOCRIF))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCRIF)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCRIF) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCRIF_2_6'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILDT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCRIF);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILDT;
                       ,'TDTIPDOC',this.w_MODOCRIF)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCRIF = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESRIF = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATRIF = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLRIF = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CODMAT = NVL(_Link_.TDCODMAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCRIF = space(5)
      endif
      this.w_DESRIF = space(35)
      this.w_CATRIF = space(2)
      this.w_FLRIF = space(1)
      this.w_CODMAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATRIF='DT' AND .w_FLRIF='V' AND EMPTY(.w_CODMAT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        endif
        this.w_MODOCRIF = space(5)
        this.w_DESRIF = space(35)
        this.w_CATRIF = space(2)
        this.w_FLRIF = space(1)
        this.w_CODMAT = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODOCDDT
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCDDT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCDDT)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILDT;
                     ,'TDTIPDOC',trim(this.w_MODOCDDT))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCDDT)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCDDT) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCDDT_2_7'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILDT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCDDT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCDDT);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILDT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILDT;
                       ,'TDTIPDOC',this.w_MODOCDDT)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDCODMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCDDT = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDDT = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDDT = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLDDT = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CODMAT = NVL(_Link_.TDCODMAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCDDT = space(5)
      endif
      this.w_DESDDT = space(35)
      this.w_CATDDT = space(2)
      this.w_FLDDT = space(1)
      this.w_CODMAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDDT='DT' AND .w_FLDDT='V' AND EMPTY(.w_CODMAT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
        endif
        this.w_MODOCDDT = space(5)
        this.w_DESDDT = space(35)
        this.w_CATDDT = space(2)
        this.w_FLDDT = space(1)
        this.w_CODMAT = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCDDT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODOCCO1
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCCO1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_BZC',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCCO1)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MODOCCO1))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCCO1)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCCO1) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCCO1_2_15'),i_cWhere,'GSPS_BZC',"Causali documenti",'GSPS_MDC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCCO1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCCO1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MODOCCO1)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCCO1 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCO1 = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDO1 = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINT1 = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLRISC = NVL(_Link_.TDFLRISC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCCO1 = space(5)
      endif
      this.w_DESCO1 = space(35)
      this.w_CATDO1 = space(2)
      this.w_FLINT1 = space(1)
      this.w_FLRISC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MODOCCO1) OR (.w_CATDO1 $ 'DI-OR' AND .w_FLINT1<>'F' AND Not .w_FLRISC$'SD')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento di origine inesistente o che partecipa al calcolo del rischio")
        endif
        this.w_MODOCCO1 = space(5)
        this.w_DESCO1 = space(35)
        this.w_CATDO1 = space(2)
        this.w_FLINT1 = space(1)
        this.w_FLRISC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCCO1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.TDTIPDOC as TDTIPDOC215"+ ",link_2_15.TDDESDOC as TDDESDOC215"+ ",link_2_15.TDCATDOC as TDCATDOC215"+ ",link_2_15.TDFLINTE as TDFLINTE215"+ ",link_2_15.TDFLRISC as TDFLRISC215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on MOD_VEND.MODOCCO1=link_2_15.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and MOD_VEND.MODOCCO1=link_2_15.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MODOCCO2
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCCO2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_BZC',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCCO2)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MODOCCO2))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCCO2)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCCO2) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCCO2_2_17'),i_cWhere,'GSPS_BZC',"Causali documenti",'GSPS_MDC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCCO2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCCO2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MODOCCO2)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCCO2 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCO2 = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDO2 = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINT2 = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLRISC = NVL(_Link_.TDFLRISC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCCO2 = space(5)
      endif
      this.w_DESCO2 = space(35)
      this.w_CATDO2 = space(2)
      this.w_FLINT2 = space(1)
      this.w_FLRISC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MODOCCO2) OR (.w_CATDO2 $ 'DI-OR' AND .w_FLINT2<>'F'  AND Not .w_FLRISC$'SD')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento di origine inesistente o che partecipa al calcolo del rischio")
        endif
        this.w_MODOCCO2 = space(5)
        this.w_DESCO2 = space(35)
        this.w_CATDO2 = space(2)
        this.w_FLINT2 = space(1)
        this.w_FLRISC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCCO2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.TDTIPDOC as TDTIPDOC217"+ ",link_2_17.TDDESDOC as TDDESDOC217"+ ",link_2_17.TDCATDOC as TDCATDOC217"+ ",link_2_17.TDFLINTE as TDFLINTE217"+ ",link_2_17.TDFLRISC as TDFLRISC217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on MOD_VEND.MODOCCO2=link_2_17.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and MOD_VEND.MODOCCO2=link_2_17.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MODOCCO3
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODOCCO3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_BZC',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MODOCCO3)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MODOCCO3))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODOCCO3)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODOCCO3) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMODOCCO3_2_19'),i_cWhere,'GSPS_BZC',"Causali documenti",'GSPS_MDC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODOCCO3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MODOCCO3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MODOCCO3)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLRISC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODOCCO3 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCO3 = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDO3 = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINT3 = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLRISC = NVL(_Link_.TDFLRISC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MODOCCO3 = space(5)
      endif
      this.w_DESCO3 = space(35)
      this.w_CATDO3 = space(2)
      this.w_FLINT3 = space(1)
      this.w_FLRISC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MODOCCO3) OR (.w_CATDO3 $ 'DI-OR' AND .w_FLINT3<>'F' AND Not .w_FLRISC$'SD')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Documento di origine inesistente o che partecipa al calcolo del rischio")
        endif
        this.w_MODOCCO3 = space(5)
        this.w_DESCO3 = space(35)
        this.w_CATDO3 = space(2)
        this.w_FLINT3 = space(1)
        this.w_FLRISC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODOCCO3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_19.TDTIPDOC as TDTIPDOC219"+ ",link_2_19.TDDESDOC as TDDESDOC219"+ ",link_2_19.TDCATDOC as TDCATDOC219"+ ",link_2_19.TDFLINTE as TDFLINTE219"+ ",link_2_19.TDFLRISC as TDFLRISC219"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_19 on MOD_VEND.MODOCCO3=link_2_19.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_19"
          i_cKey=i_cKey+'+" and MOD_VEND.MODOCCO3=link_2_19.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODPAG
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_MOCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_MOCODPAG))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oMOCODPAG_2_21'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_MOCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_MOCODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPA1 = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODPAG = space(5)
      endif
      this.w_DESPA1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_21.PACODICE as PACODICE221"+ ","+cp_TransLinkFldName('link_2_21.PADESCRI')+" as PADESCRI221"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_21 on MOD_VEND.MOCODPAG=link_2_21.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_21"
          i_cKey=i_cKey+'+" and MOD_VEND.MOCODPAG=link_2_21.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CAUCO1
  func Link_2_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCO1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCO1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCO1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCO1)
            select CCCODICE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCO1 = NVL(_Link_.CCCODICE,space(5))
      this.w_TIPDO1 = NVL(_Link_.CCTIPDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCO1 = space(5)
      endif
      this.w_TIPDO1 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCO1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCO2
  func Link_2_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCO2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCO2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCO2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCO2)
            select CCCODICE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCO2 = NVL(_Link_.CCCODICE,space(5))
      this.w_TIPDO2 = NVL(_Link_.CCTIPDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCO2 = space(5)
      endif
      this.w_TIPDO2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCO2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUFAF
  func Link_2_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUFAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUFAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLCASC";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUFAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUFAF)
            select CMCODICE,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUFAF = NVL(_Link_.CMCODICE,space(5))
      this.w_CASFAF = NVL(_Link_.CMFLCASC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUFAF = space(5)
      endif
      this.w_CASFAF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUFAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMOCODICE_1_6.value==this.w_MOCODICE)
      this.oPgFrm.Page1.oPag.oMOCODICE_1_6.value=this.w_MOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_7.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_7.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODMAG_1_16.value==this.w_MOCODMAG)
      this.oPgFrm.Page1.oPag.oMOCODMAG_1_16.value=this.w_MOCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODCLI_1_18.value==this.w_MOCODCLI)
      this.oPgFrm.Page1.oPag.oMOCODCLI_1_18.value=this.w_MOCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODLIS_1_20.value==this.w_MOCODLIS)
      this.oPgFrm.Page1.oPag.oMOCODLIS_1_20.value=this.w_MOCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPCHI_1_21.RadioValue()==this.w_MOTIPCHI)
      this.oPgFrm.Page1.oPag.oMOTIPCHI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOQTADEF_1_22.value==this.w_MOQTADEF)
      this.oPgFrm.Page1.oPag.oMOQTADEF_1_22.value=this.w_MOQTADEF
    endif
    if not(this.oPgFrm.Page1.oPag.oMONUMSCO_1_23.value==this.w_MONUMSCO)
      this.oPgFrm.Page1.oPag.oMONUMSCO_1_23.value=this.w_MONUMSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oMOFINASS_1_24.RadioValue()==this.w_MOFINASS)
      this.oPgFrm.Page1.oPag.oMOFINASS_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPRZVAC_1_25.RadioValue()==this.w_MOPRZVAC)
      this.oPgFrm.Page1.oPag.oMOPRZVAC_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPRZDES_1_26.RadioValue()==this.w_MOPRZDES)
      this.oPgFrm.Page1.oPag.oMOPRZDES_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOFLGDES_1_28.RadioValue()==this.w_MOFLGDES)
      this.oPgFrm.Page1.oPag.oMOFLGDES_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOFLUNIM_1_29.RadioValue()==this.w_MOFLUNIM)
      this.oPgFrm.Page1.oPag.oMOFLUNIM_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOFLPREZ_1_31.RadioValue()==this.w_MOFLPREZ)
      this.oPgFrm.Page1.oPag.oMOFLPREZ_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOFLSCON_1_32.RadioValue()==this.w_MOFLSCON)
      this.oPgFrm.Page1.oPag.oMOFLSCON_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_34.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_34.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDESCRI_1_35.value==this.w_CLDESCRI)
      this.oPgFrm.Page1.oPag.oCLDESCRI_1_35.value=this.w_CLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_36.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_36.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCCOR_2_1.value==this.w_MODOCCOR)
      this.oPgFrm.Page2.oPag.oMODOCCOR_2_1.value=this.w_MODOCCOR
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCRIC_2_2.value==this.w_MODOCRIC)
      this.oPgFrm.Page2.oPag.oMODOCRIC_2_2.value=this.w_MODOCRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCFAF_2_3.value==this.w_MODOCFAF)
      this.oPgFrm.Page2.oPag.oMODOCFAF_2_3.value=this.w_MODOCFAF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAF_2_4.value==this.w_DESFAF)
      this.oPgFrm.Page2.oPag.oDESFAF_2_4.value=this.w_DESFAF
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCFAT_2_5.value==this.w_MODOCFAT)
      this.oPgFrm.Page2.oPag.oMODOCFAT_2_5.value=this.w_MODOCFAT
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCRIF_2_6.value==this.w_MODOCRIF)
      this.oPgFrm.Page2.oPag.oMODOCRIF_2_6.value=this.w_MODOCRIF
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCDDT_2_7.value==this.w_MODOCDDT)
      this.oPgFrm.Page2.oPag.oMODOCDDT_2_7.value=this.w_MODOCDDT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOR_2_8.value==this.w_DESCOR)
      this.oPgFrm.Page2.oPag.oDESCOR_2_8.value=this.w_DESCOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIC_2_9.value==this.w_DESRIC)
      this.oPgFrm.Page2.oPag.oDESRIC_2_9.value=this.w_DESRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAT_2_10.value==this.w_DESFAT)
      this.oPgFrm.Page2.oPag.oDESFAT_2_10.value=this.w_DESFAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIF_2_11.value==this.w_DESRIF)
      this.oPgFrm.Page2.oPag.oDESRIF_2_11.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDDT_2_12.value==this.w_DESDDT)
      this.oPgFrm.Page2.oPag.oDESDDT_2_12.value=this.w_DESDDT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPA1_2_14.value==this.w_DESPA1)
      this.oPgFrm.Page2.oPag.oDESPA1_2_14.value=this.w_DESPA1
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCCO1_2_15.value==this.w_MODOCCO1)
      this.oPgFrm.Page2.oPag.oMODOCCO1_2_15.value=this.w_MODOCCO1
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCCO2_2_17.value==this.w_MODOCCO2)
      this.oPgFrm.Page2.oPag.oMODOCCO2_2_17.value=this.w_MODOCCO2
    endif
    if not(this.oPgFrm.Page2.oPag.oMODOCCO3_2_19.value==this.w_MODOCCO3)
      this.oPgFrm.Page2.oPag.oMODOCCO3_2_19.value=this.w_MODOCCO3
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODPAG_2_21.value==this.w_MOCODPAG)
      this.oPgFrm.Page2.oPag.oMOCODPAG_2_21.value=this.w_MOCODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCO1_2_22.value==this.w_DESCO1)
      this.oPgFrm.Page2.oPag.oDESCO1_2_22.value=this.w_DESCO1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCO2_2_23.value==this.w_DESCO2)
      this.oPgFrm.Page2.oPag.oDESCO2_2_23.value=this.w_DESCO2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCO3_2_24.value==this.w_DESCO3)
      this.oPgFrm.Page2.oPag.oDESCO3_2_24.value=this.w_DESCO3
    endif
    cp_SetControlsValueExtFlds(this,'MOD_VEND')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_LISVAL=g_PERVAL)  and not(empty(.w_MOCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODLIS_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MONUMSCO<g_NUMSCO+1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMONUMSCO_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire al massimo il num. sconti utilizzati definito nei dati azienda")
          case   not(.w_CATCOR='RF' AND EMPTY(.w_CODMAT))  and not(empty(.w_MODOCCOR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCCOR_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
          case   not(.w_CATRIC='RF' AND EMPTY(.w_CODMAT))  and not(empty(.w_MODOCRIC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCRIC_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
          case   not(.w_CATFAF='FA' AND .w_FLFAF='V' AND EMPTY(.w_CODMAT))  and not(empty(.w_MODOCFAF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCFAF_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
          case   not(.w_CATFAT='FA' AND .w_FLFAT='V' AND EMPTY(.w_CODMAT))  and not(empty(.w_MODOCFAT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCFAT_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
          case   not(.w_CATRIF='DT' AND .w_FLRIF='V' AND EMPTY(.w_CODMAT))  and not(empty(.w_MODOCRIF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCRIF_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
          case   not(.w_CATDDT='DT' AND .w_FLDDT='V' AND EMPTY(.w_CODMAT))  and not(empty(.w_MODOCDDT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCDDT_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento inesistente o con causale magazzino collegata")
          case   not(EMPTY(.w_MODOCCO1) OR (.w_CATDO1 $ 'DI-OR' AND .w_FLINT1<>'F' AND Not .w_FLRISC$'SD'))  and not(empty(.w_MODOCCO1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCCO1_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento di origine inesistente o che partecipa al calcolo del rischio")
          case   not(EMPTY(.w_MODOCCO2) OR (.w_CATDO2 $ 'DI-OR' AND .w_FLINT2<>'F'  AND Not .w_FLRISC$'SD'))  and not(empty(.w_MODOCCO2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCCO2_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento di origine inesistente o che partecipa al calcolo del rischio")
          case   not(EMPTY(.w_MODOCCO3) OR (.w_CATDO3 $ 'DI-OR' AND .w_FLINT3<>'F' AND Not .w_FLRISC$'SD'))  and not(empty(.w_MODOCCO3))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODOCCO3_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Documento di origine inesistente o che partecipa al calcolo del rischio")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_amv
      if i_bRes=.t.
        * --- Verifica congruenza causali
        if not empty(.w_MODOCFAF)
          if (g_COGE<>'S' and .w_FLRI1<>'S') OR (g_COGE='S' and .w_TIPDO1<>'FC')
            i_bRes = .f.
            i_bnoChk = .f.		
      	    i_cErrorMsg = Ah_MsgFormat("Causale fattura fiscale incongruente (no compresa in corrispettivi)")
          else
            if not empty(.w_CASFAF)
              i_bRes = .f.
              i_bnoChk = .f.		
      	      i_cErrorMsg = Ah_MsgFormat("Causale fattura fiscale incongruente (aggiorna il magazzino)")
            endif
          endif
        endif
        if not empty(.w_MODOCFAT)
          if (g_COGE<>'S' and .w_FLRI2='S') OR (g_COGE='S' and .w_TIPDO2='FC')
            i_bRes = .f.
            i_bnoChk = .f.		
      	    i_cErrorMsg = Ah_MsgFormat("Causale fattura immediata incongruente (compresa in corrispettivi)")
          endif
        endif
      endif
      if i_bRes=.t.
        * --- Verifica Presenza Documento associato alla Chiusura
        do case
          case .w_MOTIPCHI $ "NS-ES-BV" and empty(.w_MODOCCOR)
            i_bRes = .f.
          case .w_MOTIPCHI = "RF" and empty(.w_MODOCRIC)
            i_bRes = .f.
          case .w_MOTIPCHI = "FI" and empty(.w_MODOCFAT)
            i_bRes = .f.
          case .w_MOTIPCHI = "FF" and (empty(.w_MODOCFAF) or empty(.w_MODOCCOR))
            i_bRes = .f.
          case .w_MOTIPCHI = "RS" and empty(.w_MODOCRIF)
            i_bRes = .f.
          case .w_MOTIPCHI = "DT" and empty(.w_MODOCDDT)
            i_bRes = .f.
        endcase
        if i_bRes=.f.
          i_bnoChk = .f.		
          i_cErrorMsg = Ah_MsgFormat("Non esiste un documento associato alla tipologia di chiusura impostata")
          if .w_MOTIPCHI = "FF"
            if empty(.w_MODOCFAF) and not empty(.w_MODOCCOR)
              i_cErrorMsg = Ah_MsgFormat("Non esiste un documento di tipo fattura fiscale associato alla tipologia di chiusura impostata")
            endif
            if empty(.w_MODOCFAF) or empty(.w_MODOCCOR)
              i_cErrorMsg = Ah_MsgFormat("Non esiste un documento di tipo corrispettivo associato alla tipologia di chiusura impostata")
            endif
          endif
        endif
      endif
      
      if i_bRes
          Ah_Msg('Check finali...',.T.)
           .NotifyEvent('ChkFinali')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MOCODICE = this.w_MOCODICE
    this.o_MODOCFAF = this.w_MODOCFAF
    this.o_MODOCFAT = this.w_MODOCFAT
    return

enddefine

* --- Define pages as container
define class tgsps_amvPag1 as StdContainer
  Width  = 585
  height = 349
  stdWidth  = 585
  stdheight = 349
  resizeXpos=388
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOCODICE_1_6 as StdField with uid="TFXDGPTSOG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MOCODICE", cQueryName = "MOCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 17443851,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=155, Top=15, InputMask=replicate('X',5)

  add object oMODESCRI_1_7 as StdField with uid="PORGIFJIGL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 200293391,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=221, Top=15, InputMask=replicate('X',35)

  add object oMOCODMAG_1_16 as StdField with uid="FQZIYWKQCK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MOCODMAG", cQueryName = "MOCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino predefinito associato alla vendita",;
    HelpContextID = 84552717,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=155, Top=45, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MOCODMAG"

  func oMOCODMAG_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODMAG_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODMAG_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMOCODMAG_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oMOCODMAG_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MOCODMAG
     i_obj.ecpSave()
  endproc

  add object oMOCODCLI_1_18 as StdField with uid="QWXYCYLVBW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MOCODCLI", cQueryName = "MOCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente predefinito associato alla vendita",;
    HelpContextID = 83219441,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=155, Top=74, cSayPict="g_MASPOS", cGetPict="g_MASPOS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", cZoomOnZoom="GSPS_ACV", oKey_1_1="CLCODCLI", oKey_1_2="this.w_MOCODCLI"

  proc oMOCODCLI_1_18.mAfter
    with this.Parent.oContained
      .w_MOCODCLI=PSCALZER(.w_MOCODCLI, "XXX")
    endwith
  endproc

  func oMOCODCLI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODCLI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODCLI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oMOCODCLI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACV',"Clienti negozio",'',this.parent.oContained
  endproc
  proc oMOCODCLI_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLCODCLI=this.parent.oContained.w_MOCODCLI
     i_obj.ecpSave()
  endproc

  add object oMOCODLIS_1_20 as StdField with uid="MCNGFPMXGX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MOCODLIS", cQueryName = "MOCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino predefinito associato alla vendita",;
    HelpContextID = 200659943,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=155, Top=103, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_MOCODLIS"

  func oMOCODLIS_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODLIS_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODLIS_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oMOCODLIS_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini articoli",'GSPS_MVD.LISTINI_VZM',this.parent.oContained
  endproc
  proc oMOCODLIS_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_MOCODLIS
     i_obj.ecpSave()
  endproc


  add object oMOTIPCHI_1_21 as StdCombo with uid="YAQMAIZKIQ",rtseq=18,rtrep=.f.,left=155,top=133,width=156,height=21;
    , ToolTipText = "Tipo di documento generato alla chiusura della vendita (predefinito)";
    , HelpContextID = 70960113;
    , cFormVar="w_MOTIPCHI",RowSource=""+"Nessuna stampa,"+"Emissione scontrino,"+"Brogliaccio di vendita,"+"Ricevute fiscale,"+"Fattura immediata,"+"Fattura fiscale,"+"Ricevuta segue fattura,"+"Documento di trasporto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOTIPCHI_1_21.RadioValue()
    return(iif(this.value =1,'NS',;
    iif(this.value =2,'ES',;
    iif(this.value =3,'BV',;
    iif(this.value =4,'RF',;
    iif(this.value =5,'FI',;
    iif(this.value =6,'FF',;
    iif(this.value =7,'RS',;
    iif(this.value =8,'DT',;
    space(2))))))))))
  endfunc
  func oMOTIPCHI_1_21.GetRadio()
    this.Parent.oContained.w_MOTIPCHI = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPCHI_1_21.SetRadio()
    this.Parent.oContained.w_MOTIPCHI=trim(this.Parent.oContained.w_MOTIPCHI)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPCHI=='NS',1,;
      iif(this.Parent.oContained.w_MOTIPCHI=='ES',2,;
      iif(this.Parent.oContained.w_MOTIPCHI=='BV',3,;
      iif(this.Parent.oContained.w_MOTIPCHI=='RF',4,;
      iif(this.Parent.oContained.w_MOTIPCHI=='FI',5,;
      iif(this.Parent.oContained.w_MOTIPCHI=='FF',6,;
      iif(this.Parent.oContained.w_MOTIPCHI=='RS',7,;
      iif(this.Parent.oContained.w_MOTIPCHI=='DT',8,;
      0))))))))
  endfunc

  add object oMOQTADEF_1_22 as StdField with uid="TAIZLYFJGW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MOQTADEF", cQueryName = "MOQTADEF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� predefinita proposta sulla vendita",;
    HelpContextID = 199232524,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=155, Top=162, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  add object oMONUMSCO_1_23 as StdField with uid="KBHLQLNZQN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MONUMSCO", cQueryName = "MONUMSCO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire al massimo il num. sconti utilizzati definito nei dati azienda",;
    ToolTipText = "Numero massimo di sconti/maggiorazioni utilizzate (max. 4)",;
    HelpContextID = 195091477,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=155, Top=192, cSayPict='"9"', cGetPict='"9"'

  func oMONUMSCO_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MONUMSCO<g_NUMSCO+1)
    endwith
    return bRes
  endfunc


  add object oMOFINASS_1_24 as StdCombo with uid="DKLGMRHYCK",rtseq=21,rtrep=.f.,left=155,top=222,width=233,height=21;
    , ToolTipText = "Questa combo permette di definire l'associazione tra l'importo del campo finanziamento e la voce che verr� riportata nello scontrino";
    , HelpContextID = 161766425;
    , cFormVar="w_MOFINASS",RowSource=""+"Associato a contanti,"+"Associato a c/assegno,"+"Associato a c/credito/bancomat", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOFINASS_1_24.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oMOFINASS_1_24.GetRadio()
    this.Parent.oContained.w_MOFINASS = this.RadioValue()
    return .t.
  endfunc

  func oMOFINASS_1_24.SetRadio()
    this.Parent.oContained.w_MOFINASS=trim(this.Parent.oContained.w_MOFINASS)
    this.value = ;
      iif(this.Parent.oContained.w_MOFINASS=='C',1,;
      iif(this.Parent.oContained.w_MOFINASS=='A',2,;
      iif(this.Parent.oContained.w_MOFINASS=='B',3,;
      0)))
  endfunc

  add object oMOPRZVAC_1_25 as StdCheck with uid="VKXJWUHCJW",rtseq=22,rtrep=.f.,left=430, top=133, caption="Prezzo default U.P.V.",;
    HelpContextID = 258866185,;
    cFormVar="w_MOPRZVAC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOPRZVAC_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOPRZVAC_1_25.GetRadio()
    this.Parent.oContained.w_MOPRZVAC = this.RadioValue()
    return .t.
  endfunc

  func oMOPRZVAC_1_25.SetRadio()
    this.Parent.oContained.w_MOPRZVAC=trim(this.Parent.oContained.w_MOPRZVAC)
    this.value = ;
      iif(this.Parent.oContained.w_MOPRZVAC=='S',1,;
      0)
  endfunc

  add object oMOPRZDES_1_26 as StdCheck with uid="VKVTSSEGQX",rtseq=23,rtrep=.f.,left=430, top=162, caption="Ricalcola prezzo",;
    ToolTipText = "Se attivo, in importazione viene ricalcolato il prezzo in base ai dati del documento di destinazione",;
    HelpContextID = 225311769,;
    cFormVar="w_MOPRZDES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOPRZDES_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOPRZDES_1_26.GetRadio()
    this.Parent.oContained.w_MOPRZDES = this.RadioValue()
    return .t.
  endfunc

  func oMOPRZDES_1_26.SetRadio()
    this.Parent.oContained.w_MOPRZDES=trim(this.Parent.oContained.w_MOPRZDES)
    this.value = ;
      iif(this.Parent.oContained.w_MOPRZDES=='S',1,;
      0)
  endfunc

  add object oMOFLGDES_1_28 as StdCheck with uid="VZZWAFHZSS",rtseq=24,rtrep=.f.,left=430, top=251, caption="Descrizione",;
    ToolTipText = "Se attivo elimina dalla sequenza di tabulazione la descrizione articolo sulla vendita",;
    HelpContextID = 204954649,;
    cFormVar="w_MOFLGDES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOFLGDES_1_28.RadioValue()
    return(iif(this.value =1,"S",;
    space(1)))
  endfunc
  func oMOFLGDES_1_28.GetRadio()
    this.Parent.oContained.w_MOFLGDES = this.RadioValue()
    return .t.
  endfunc

  func oMOFLGDES_1_28.SetRadio()
    this.Parent.oContained.w_MOFLGDES=trim(this.Parent.oContained.w_MOFLGDES)
    this.value = ;
      iif(this.Parent.oContained.w_MOFLGDES=="S",1,;
      0)
  endfunc

  add object oMOFLUNIM_1_29 as StdCheck with uid="GCIREVHAYS",rtseq=25,rtrep=.f.,left=430, top=274, caption="Unit� di misura",;
    ToolTipText = "Se attivo elimina dalla sequenza di tabulazione l'unit� di misura sulla vendita",;
    HelpContextID = 149464045,;
    cFormVar="w_MOFLUNIM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOFLUNIM_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMOFLUNIM_1_29.GetRadio()
    this.Parent.oContained.w_MOFLUNIM = this.RadioValue()
    return .t.
  endfunc

  func oMOFLUNIM_1_29.SetRadio()
    this.Parent.oContained.w_MOFLUNIM=trim(this.Parent.oContained.w_MOFLUNIM)
    this.value = ;
      iif(this.Parent.oContained.w_MOFLUNIM=='S',1,;
      0)
  endfunc

  add object oMOFLPREZ_1_31 as StdCheck with uid="MNFROFDXZD",rtseq=26,rtrep=.f.,left=430, top=297, caption="Prezzo",;
    ToolTipText = "Se attivo elimina dalla sequenza di tabulazione il prezzo sulla vendita",;
    HelpContextID = 180837408,;
    cFormVar="w_MOFLPREZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOFLPREZ_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMOFLPREZ_1_31.GetRadio()
    this.Parent.oContained.w_MOFLPREZ = this.RadioValue()
    return .t.
  endfunc

  func oMOFLPREZ_1_31.SetRadio()
    this.Parent.oContained.w_MOFLPREZ=trim(this.Parent.oContained.w_MOFLPREZ)
    this.value = ;
      iif(this.Parent.oContained.w_MOFLPREZ=='S',1,;
      0)
  endfunc

  add object oMOFLSCON_1_32 as StdCheck with uid="VYFQYVOPVQ",rtseq=27,rtrep=.f.,left=430, top=320, caption="Sconto",;
    ToolTipText = "Se attivo elimina dalla sequenza di tabulazione lo sconto sulla vendita",;
    HelpContextID = 200760340,;
    cFormVar="w_MOFLSCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOFLSCON_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMOFLSCON_1_32.GetRadio()
    this.Parent.oContained.w_MOFLSCON = this.RadioValue()
    return .t.
  endfunc

  func oMOFLSCON_1_32.SetRadio()
    this.Parent.oContained.w_MOFLSCON=trim(this.Parent.oContained.w_MOFLSCON)
    this.value = ;
      iif(this.Parent.oContained.w_MOFLSCON=='S',1,;
      0)
  endfunc

  add object oDESMAG_1_34 as StdField with uid="RYCPINGWVJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 249110838,;
   bGlobalFont=.t.,;
    Height=21, Width=262, Left=221, Top=45, InputMask=replicate('X',30)

  add object oCLDESCRI_1_35 as StdField with uid="DZSQEHWZTT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CLDESCRI", cQueryName = "CLDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200292463,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=74, InputMask=replicate('X',40)

  add object oDESLIS_1_36 as StdField with uid="VDLJMGYQEX",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 190325046,;
   bGlobalFont=.t.,;
    Height=21, Width=347, Left=222, Top=103, InputMask=replicate('X',40)

  add object oStr_1_8 as StdString with uid="JBARZYEFGC",Visible=.t., Left=6, Top=17,;
    Alignment=1, Width=145, Height=18,;
    Caption="Codice modalit� vendita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="YBTROJVOHX",Visible=.t., Left=6, Top=45,;
    Alignment=1, Width=145, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GPVSPQXKRC",Visible=.t., Left=6, Top=74,;
    Alignment=1, Width=145, Height=18,;
    Caption="Cliente predefinito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ODRFPITJKW",Visible=.t., Left=6, Top=103,;
    Alignment=1, Width=145, Height=18,;
    Caption="Listino predefinito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="OHAIFCCRET",Visible=.t., Left=6, Top=162,;
    Alignment=1, Width=145, Height=18,;
    Caption="Qt� proposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="IUPCALLZLL",Visible=.t., Left=6, Top=133,;
    Alignment=1, Width=145, Height=18,;
    Caption="Tipo chiusura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="MYDCPQTCWW",Visible=.t., Left=428, Top=229,;
    Alignment=0, Width=137, Height=18,;
    Caption="Automatismi"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="GDHHWGEPPR",Visible=.t., Left=6, Top=193,;
    Alignment=1, Width=145, Height=18,;
    Caption="Sconti/magg. utilizzati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="SAQTLLYDVA",Visible=.t., Left=16, Top=223,;
    Alignment=1, Width=135, Height=18,;
    Caption="Finanziamento:"  ;
  , bGlobalFont=.t.

  add object oBox_1_42 as StdBox with uid="KFFLGUXIEH",left=426, top=247, width=155,height=3
enddefine
define class tgsps_amvPag2 as StdContainer
  Width  = 585
  height = 349
  stdWidth  = 585
  stdheight = 349
  resizeXpos=411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMODOCCOR_2_1 as StdField with uid="JECTTCWCRD",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MODOCCOR", cQueryName = "MODOCCOR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento inesistente o con causale magazzino collegata",;
    ToolTipText = "Causale per documenti corrispettivi (chiusure: nessuna stampa, emiss. scontrino, brogliaccio)",;
    HelpContextID = 184171544,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDCATDOC", oKey_1_2="this.w_FILRF", oKey_2_1="TDTIPDOC", oKey_2_2="this.w_MODOCCOR"

  func oMODOCCOR_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCCOR_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCCOR_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILRF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILRF)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oMODOCCOR_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oMODOCCOR_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDCATDOC=w_FILRF
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCCOR
     i_obj.ecpSave()
  endproc

  add object oMODOCRIC_2_2 as StdField with uid="PGJSFCYPLJ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MODOCRIC", cQueryName = "MODOCRIC",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento inesistente o con causale magazzino collegata",;
    ToolTipText = "Causale per la generazione documenti di tipo ricevuta fiscale",;
    HelpContextID = 101041143,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=68, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDCATDOC", oKey_1_2="this.w_FILRF", oKey_2_1="TDTIPDOC", oKey_2_2="this.w_MODOCRIC"

  func oMODOCRIC_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCRIC_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCRIC_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILRF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILRF)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oMODOCRIC_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oMODOCRIC_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDCATDOC=w_FILRF
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCRIC
     i_obj.ecpSave()
  endproc

  add object oMODOCFAF_2_3 as StdField with uid="MWKPJLLMUU",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MODOCFAF", cQueryName = "MODOCFAF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento inesistente o con causale magazzino collegata",;
    ToolTipText = "Causale per la generazione documenti di tipo fattura fiscale (compresa nei corrispettivi)",;
    HelpContextID = 234503180,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=94, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILFA", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_MODOCFAF"

  func oMODOCFAF_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCFAF_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCFAF_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILFA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILFA)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oMODOCFAF_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oMODOCFAF_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILFA
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCFAF
     i_obj.ecpSave()
  endproc

  add object oDESFAF_2_4 as StdField with uid="LHJMHOZCTT",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESFAF", cQueryName = "DESFAF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 231874870,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=94, InputMask=replicate('X',35)

  add object oMODOCFAT_2_5 as StdField with uid="SGGYFPGWVU",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MODOCFAT", cQueryName = "MODOCFAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento inesistente o con causale magazzino collegata",;
    ToolTipText = "Causale per la generazione documenti di tipo fattura immediata",;
    HelpContextID = 234503194,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=120, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILFA", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_MODOCFAT"

  func oMODOCFAT_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCFAT_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCFAT_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILFA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILFA)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oMODOCFAT_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oMODOCFAT_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILFA
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCFAT
     i_obj.ecpSave()
  endproc

  add object oMODOCRIF_2_6 as StdField with uid="KBOOQPEPJX",rtseq=38,rtrep=.f.,;
    cFormVar = "w_MODOCRIF", cQueryName = "MODOCRIF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento inesistente o con causale magazzino collegata",;
    ToolTipText = "Causale per la generazione documenti di tipo ric.segue fattura (DDT)",;
    HelpContextID = 101041140,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=146, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILDT", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_MODOCRIF"

  func oMODOCRIF_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCRIF_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCRIF_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILDT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILDT)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oMODOCRIF_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oMODOCRIF_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILDT
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCRIF
     i_obj.ecpSave()
  endproc

  add object oMODOCDDT_2_7 as StdField with uid="OFCIYINDNH",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MODOCDDT", cQueryName = "MODOCDDT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento inesistente o con causale magazzino collegata",;
    ToolTipText = "Causale per la generazione documenti di tipo doc. di trasprto",;
    HelpContextID = 200948762,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=172, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILDT", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_MODOCDDT"

  func oMODOCDDT_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCDDT_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCDDT_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILDT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILDT)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oMODOCDDT_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oMODOCDDT_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILDT
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCDDT
     i_obj.ecpSave()
  endproc

  add object oDESCOR_2_8 as StdField with uid="CZKYGBSEPO",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESCOR", cQueryName = "DESCOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 179249462,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=42, InputMask=replicate('X',35)

  add object oDESRIC_2_9 as StdField with uid="FTWKOJGDVQ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESRIC", cQueryName = "DESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 190718262,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=68, InputMask=replicate('X',35)

  add object oDESFAT_2_10 as StdField with uid="FNGZVBSLYL",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESFAT", cQueryName = "DESFAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 198320438,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=120, InputMask=replicate('X',35)

  add object oDESRIF_2_11 as StdField with uid="EOJQLXVABJ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 241049910,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=146, InputMask=replicate('X',35)

  add object oDESDDT_2_12 as StdField with uid="HLGVCUROJI",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESDDT", cQueryName = "DESDDT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 201335094,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=172, InputMask=replicate('X',35)

  add object oDESPA1_2_14 as StdField with uid="BAKENLVPUK",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESPA1", cQueryName = "DESPA1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 148644150,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=327, InputMask=replicate('X',30)

  add object oMODOCCO1_2_15 as StdField with uid="BMSDSDPXFI",rtseq=46,rtrep=.f.,;
    cFormVar = "w_MODOCCO1", cQueryName = "MODOCCO1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento di origine inesistente o che partecipa al calcolo del rischio",;
    ToolTipText = "Causale 1^documento di origine",;
    HelpContextID = 184171511,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=224, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSPS_BZC", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MODOCCO1"

  func oMODOCCO1_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCCO1_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCCO1_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMODOCCO1_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_BZC',"Causali documenti",'GSPS_MDC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMODOCCO1_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSPS_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCCO1
     i_obj.ecpSave()
  endproc

  add object oMODOCCO2_2_17 as StdField with uid="IWXZCLWCPA",rtseq=47,rtrep=.f.,;
    cFormVar = "w_MODOCCO2", cQueryName = "MODOCCO2",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento di origine inesistente o che partecipa al calcolo del rischio",;
    ToolTipText = "Causale 2^documento di origine",;
    HelpContextID = 184171512,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=251, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSPS_BZC", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MODOCCO2"

  func oMODOCCO2_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCCO2_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCCO2_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMODOCCO2_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_BZC',"Causali documenti",'GSPS_MDC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMODOCCO2_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSPS_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCCO2
     i_obj.ecpSave()
  endproc

  add object oMODOCCO3_2_19 as StdField with uid="JAYTOAEOXF",rtseq=48,rtrep=.f.,;
    cFormVar = "w_MODOCCO3", cQueryName = "MODOCCO3",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Documento di origine inesistente o che partecipa al calcolo del rischio",;
    ToolTipText = "Causale 3^documento di origine",;
    HelpContextID = 184171513,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=278, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSPS_BZC", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MODOCCO3"

  func oMODOCCO3_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODOCCO3_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODOCCO3_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMODOCCO3_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_BZC',"Causali documenti",'GSPS_MDC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMODOCCO3_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSPS_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MODOCCO3
     i_obj.ecpSave()
  endproc

  add object oMOCODPAG_2_21 as StdField with uid="CYOXWUAAOC",rtseq=49,rtrep=.f.,;
    cFormVar = "w_MOCODPAG", cQueryName = "MOCODPAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice pagamento predefinito sulla vendita negozio",;
    HelpContextID = 134884365,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=327, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_MOCODPAG"

  func oMOCODPAG_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODPAG_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODPAG_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oMOCODPAG_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oMOCODPAG_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_MOCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESCO1_2_22 as StdField with uid="COCRQXIUVJ",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESCO1", cQueryName = "DESCO1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 162472246,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=224, InputMask=replicate('X',35)

  add object oDESCO2_2_23 as StdField with uid="VEAQBVZGQO",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESCO2", cQueryName = "DESCO2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 179249462,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=251, InputMask=replicate('X',35)

  add object oDESCO3_2_24 as StdField with uid="FAVCAOQAMC",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESCO3", cQueryName = "DESCO3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 196026678,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=228, Top=278, InputMask=replicate('X',35)


  add object oObj_2_64 as cp_runprogram with uid="VHTQAEYMAL",left=0, top=363, width=692,height=19,;
    caption='GSPS_BMM(w_MODOCCOR, w_MODOCRIC, w_MODOCFAF, w_MODOCFAT, w_MODOCRIF, w_MODOCDDT)',;
   bGlobalFont=.t.,;
    prg="GSPS_BMM(w_MODOCCOR, w_MODOCRIC, w_MODOCFAF, w_MODOCFAT, w_MODOCRIF, w_MODOCDDT)",;
    cEvent = "ChkFinali",;
    nPag=2;
    , HelpContextID = 263499492

  add object oStr_2_13 as StdString with uid="JMFZNAKTRA",Visible=.t., Left=48, Top=327,;
    Alignment=1, Width=108, Height=18,;
    Caption="Pagam.cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="VGSOSIGHWN",Visible=.t., Left=72, Top=224,;
    Alignment=1, Width=84, Height=18,;
    Caption="1�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="DCBYUWIJHT",Visible=.t., Left=72, Top=251,;
    Alignment=1, Width=84, Height=18,;
    Caption="2�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="OFYHKPSZRK",Visible=.t., Left=72, Top=278,;
    Alignment=1, Width=84, Height=18,;
    Caption="3�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="EMVMLLHBUL",Visible=.t., Left=10, Top=198,;
    Alignment=0, Width=210, Height=18,;
    Caption="Documenti di origine"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_27 as StdString with uid="ZJECEEKBXP",Visible=.t., Left=10, Top=14,;
    Alignment=0, Width=299, Height=18,;
    Caption="Causali per generazione documenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_29 as StdString with uid="UHZHLFAFWS",Visible=.t., Left=10, Top=302,;
    Alignment=0, Width=210, Height=18,;
    Caption="Pagamento predefinito"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_47 as StdString with uid="ITOYUYNQMG",Visible=.t., Left=7, Top=94,;
    Alignment=1, Width=149, Height=18,;
    Caption="Fattura fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="ZNNAMOSESY",Visible=.t., Left=7, Top=172,;
    Alignment=1, Width=149, Height=18,;
    Caption="Doc. di trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="BOKISVVCPW",Visible=.t., Left=7, Top=146,;
    Alignment=1, Width=149, Height=18,;
    Caption="Ricevuta segue fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="JBDAPAISJU",Visible=.t., Left=7, Top=42,;
    Alignment=1, Width=149, Height=18,;
    Caption="Corrispettivo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="PALDRIRNQW",Visible=.t., Left=7, Top=68,;
    Alignment=1, Width=149, Height=18,;
    Caption="Ricevuta fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="ZTBQAHDSOX",Visible=.t., Left=7, Top=120,;
    Alignment=1, Width=149, Height=18,;
    Caption="Fattura immediata:"  ;
  , bGlobalFont=.t.

  add object oBox_2_26 as StdBox with uid="FBRXJAVCLJ",left=8, top=216, width=568,height=3

  add object oBox_2_28 as StdBox with uid="ZTRZJIERJD",left=10, top=32, width=568,height=3

  add object oBox_2_30 as StdBox with uid="FXEPNIBTAF",left=10, top=320, width=568,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_amv','MOD_VEND','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MOCODICE=MOD_VEND.MOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
