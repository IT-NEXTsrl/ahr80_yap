* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bcg                                                        *
*              Stampa chiusura giornaliera                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_117]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-06                                                      *
* Last revis.: 2015-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bcg",oParentObject)
return(i_retval)

define class tgsps_bcg as StdBatch
  * --- Local variables
  w_IMPORTO = 0
  w_CODICE = space(35)
  * --- WorkFile variables
  PAR_VDET_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Chiusure Giornaliere (da GSPS_SCG)
    L_DATA = this.oParentObject.w_DatEla
    L_BUDESCRI = this.oParentObject.w_DESNEG
    CREATE CURSOR __tmp__ (CHIAVE C(35), IMP N(18,4), TIPO C(2))
    vq_exec("..\GPOS\EXE\query\GSPSMSCG.VQR",this,"Curs1")
    * --- Totali per Pagamento Prepagato
    SELECT MDDATREG, SUM(MDIMPPRE) AS IMPPRE FROM Curs1 GROUP BY MDDATREG INTO CURSOR APPO
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Prepagati")+ SPACE(1)
    this.w_IMPORTO = IMPPRE
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "02")
    select APPO
    endscan
    * --- Totali per Pagamento Contanti
    SELECT MDDATREG, SUM(MDPAGCON) AS PAGCON, SUM(IIF(MDIMPABB<=0, ABS(MDIMPABB), 0)) AS RESTO ;
    FROM Curs1 GROUP BY MDDATREG INTO CURSOR APPO
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Contanti") + SPACE(1)
    this.w_IMPORTO = PAGCON-RESTO
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "03")
    select APPO
    endscan
    * --- Totali per Pagamento Assegni
    SELECT MDDATREG, SUM(MDPAGASS) AS PAGASS FROM Curs1 GROUP BY MDDATREG INTO CURSOR APPO
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Assegni") + SPACE(1)
    this.w_IMPORTO = PAGASS
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "04")
    select APPO
    endscan
    * --- Totali IMPORTI NON PAGATI
    SELECT MDDATREG, SUM(MDPAGCLI) AS PAGCLI FROM Curs1 GROUP BY MDDATREG INTO CURSOR APPO
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Vendite non pagate") + SPACE(1)
    this.w_IMPORTO = PAGCLI
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "05")
    select APPO
    endscan
    * --- Totali per Pagamento Carte
    SELECT MDDATREG, SUM(MDPAGCAR) AS PAGCAR, MDCODCAR,CCDESCRI ;
    FROM Curs1 GROUP BY MDDATREG,MDCODCAR INTO CURSOR APPO
    select APPO
    go top
    scan
    if EMPTY(Nvl(CCDESCRI,""))
      this.w_CODICE = ah_Msgformat("Carte non specificate")
    else
      this.w_CODICE = ah_Msgformat("Carte %1", CCDESCRI)
    endif
    this.w_IMPORTO = PAGCAR
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "06")
    select APPO
    endscan
    * --- Totali per Pagamento con finanziamento
    SELECT MDDATREG, SUM(MDPAGFIN) AS PAGFIN FROM Curs1 GROUP BY MDDATREG INTO CURSOR APPO
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Finanziamenti") + SPACE(1)
    this.w_IMPORTO = PAGFIN
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "07")
    select APPO
    endscan
    vq_exec("..\GPOS\EXE\query\GSPSDSCG.VQR",this,"Curs2")
    * --- Totali per Iva
    if g_FLESSC = "S" And this.oParentObject.w_DATELA >= g_DATESC
      SELECT NVL(IVPERIVA, 0) AS IVPERIVA, ;
      SUM(cp_round(MDQTAMOV*nvl(MDPREZZO,0) ,VADECTOT)+NVL(MDSCVENT,0)) AS PAGIVA ;
      WHERE NOT EMPTY(NVL(IVCODIVA,"")) FROM Curs2 GROUP BY IVPERIVA INTO CURSOR APPO
    else
      SELECT NVL(IVPERIVA, 0) AS IVPERIVA, ;
      SUM(cp_round(MDQTAMOV*(cp_ROUND(nvl(MDPREZZO,0) * (1+nvl(MDSCONT1,0)/100)*(1+nvl(MDSCONT2,0)/100)*(1+nvl(MDSCONT3,0)/100)*(1+nvl(MDSCONT4,0)/100),5)),VADECTOT)+NVL(MDSCVENT,0)) AS PAGIVA ;
      WHERE NOT EMPTY(NVL(IVCODIVA,"")) FROM Curs2 GROUP BY IVPERIVA INTO CURSOR APPO
    endif
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Aliquota IVA %1 %",str(IVPERIVA,3,0) )
    this.w_IMPORTO = PAGIVA
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "08")
    select APPO
    endscan
    * --- Totali per Reparto
    if g_FLESSC = "S" And this.oParentObject.w_DATELA >= g_DATESC
      SELECT NVL(PLCODREP, SPACE(3)) AS PLCODREP, ;
      SUM(cp_round(MDQTAMOV*nvl(MDPREZZO,0) ,VADECTOT)+NVL(MDSCVENT,0)) AS PAGREP ;
      WHERE NOT EMPTY(NVL(PLCODREP,"")) FROM Curs2 GROUP BY PLCODREP INTO CURSOR APPO
    else
      SELECT NVL(PLCODREP, SPACE(3)) AS PLCODREP, ;
      SUM(cp_round(MDQTAMOV*(cp_ROUND(nvl(MDPREZZO,0) * (1+nvl(MDSCONT1,0)/100)*(1+nvl(MDSCONT2,0)/100)*(1+nvl(MDSCONT3,0)/100)*(1+nvl(MDSCONT4,0)/100),5)),VADECTOT)+NVL(MDSCVENT,0)) AS PAGREP ;
      WHERE NOT EMPTY(NVL(PLCODREP,"")) FROM Curs2 GROUP BY PLCODREP INTO CURSOR APPO
    endif
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Reparto %1", PLCODREP)
    this.w_IMPORTO = PAGREP
    if this.w_IMPORTO <> 0
      * --- Pu� essere zero se omaggio imponib
      INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "09")
    endif
    select APPO
    endscan
    * --- Numero Vendite per tipo chiusura
    SELECT MDTIPCHI, COUNT(MDSERIAL) AS TOTGIO;
    FROM Curs1 GROUP BY MDTIPCHI ORDER BY MDTIPCHI INTO CURSOR APPO
    select APPO
    go top
    scan
    do case
      case MDTIPCHI="BV"
        this.w_CODICE = SPACE(1) + ah_Msgformat("Brogliaccio vendite") + SPACE(1)
      case MDTIPCHI="RF"
        this.w_CODICE = SPACE(1) +ah_Msgformat("Ricevuta fiscale") + SPACE(1)
      case MDTIPCHI="ES"
        this.w_CODICE = SPACE(1)+ah_Msgformat("Scontrino fiscale") + SPACE(1)
      case MDTIPCHI="FI"
        this.w_CODICE = SPACE(1)+ ah_Msgformat("Fattura immediata") + SPACE(1)
      case MDTIPCHI="FF"
        this.w_CODICE = SPACE(1) + ah_Msgformat("Fattura fiscale") + SPACE(1)
      case MDTIPCHI="RS"
        this.w_CODICE = SPACE(1)+ ah_Msgformat("Ricevuta segue fattura") + SPACE(1)
      case MDTIPCHI="DT"
        this.w_CODICE = SPACE(1)+ ah_Msgformat("Documento di trasporto") + SPACE(1)
      case MDTIPCHI="NS"
        this.w_CODICE = SPACE(1)+ ah_Msgformat("Nessuna stampa") + SPACE(1)
    endcase
    this.w_IMPORTO = nvl(TOTGIO,0)
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "10")
    select APPO
    endscan
    * --- Totali per Operatore
    SELECT MDCODOPE, SUM(MDTOTDOC) AS TOTOPE;
    FROM Curs1 GROUP BY MDCODOPE INTO CURSOR APPO
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Operatore %1", str(MDCODOPE,4) )
    this.w_IMPORTO = NVL(TOTOPE,0)
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "11")
    select APPO
    endscan
    * --- Totale Giornaliero e Acconti precedenti
    SELECT MDDATREG, SUM(MDTOTDOC) AS TOTGIO,SUM(MDACCPRE) AS ACCPRE;
    FROM Curs1 GROUP BY MDDATREG INTO CURSOR APPO
    select APPO
    go top
    scan
    this.w_CODICE = ah_Msgformat("Acconti precedenti")
    this.w_IMPORTO = nvl(ACCPRE,0)
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "01")
    this.w_CODICE = ah_Msgformat("Totale giornaliero del %1",dtoc(MDDATREG) )
    this.w_IMPORTO = nvl(TOTGIO,0)
    INSERT INTO __TMP__ (CHIAVE, IMP, TIPO) VALUES (this.w_CODICE, this.w_IMPORTO, "12")
    select APPO
    endscan
    select __TMP__
    SELECT CHIAVE, SUM(IMP) as IMP , TIPO FROM __TMP__ GROUP BY CHIAVE, TIPO INTO CURSOR __TMP__ order by TIPO DESC
    select __TMP__
    if used("__TMP__")
      SELECT __TMP__
      GO TOP
      cp_chprn("..\GPOS\EXE\QUERY\gsps3scg.FRX", " ", this)
    endif
    if used( "__tmp__" )
      select __tmp__
      use
    endif
    if used( "Curs1" )
      select Curs1
      use
    endif
    if used( "Curs2" )
      select Curs2
      use
    endif
    if used( "APPO")
      select Appo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_VDET'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
