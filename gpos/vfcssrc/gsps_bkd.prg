* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bkd                                                        *
*              Elimina riferimento - documento                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_29]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-20                                                      *
* Last revis.: 2003-03-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pFLORIG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bkd",oParentObject,m.pSERIAL,m.pFLORIG)
return(i_retval)

define class tgsps_bkd as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pFLORIG = space(1)
  w_SERIALE = space(10)
  w_ORIGIN = space(1)
  w_MDSERIAL = space(10)
  w_MDTIPCHI = space(2)
  w_MDTIPCOR = space(5)
  w_MDTIPDOC = space(5)
  w_MDTIPVEN = space(5)
  * --- WorkFile variables
  COR_RISP_idx=0
  MOD_VEND_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Riferimento al Documento di Vendita Negozio a seguito della cancellazione del Documento generato (da GSVE_BMK)
    * --- ATTENZIONE: Sono eliminabili solo i Documenti generati in Differita (MDRIFCOR)
    this.w_SERIALE = this.pSERIAL
    this.w_ORIGIN = this.pFLORIG
    if NOT EMPTY(this.w_SERIALE)
      * --- Cicla sulle Vendite associate al documento da eliminare
      if this.w_ORIGIN="D"
        * --- Corrispettivi
        * --- Select from COR_RISP
        i_nConn=i_TableProp[this.COR_RISP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" COR_RISP ";
              +" where MDRIFCOR="+cp_ToStrODBC(this.w_SERIALE)+"";
               ,"_Curs_COR_RISP")
        else
          select * from (i_cTable);
           where MDRIFCOR=this.w_SERIALE;
            into cursor _Curs_COR_RISP
        endif
        if used('_Curs_COR_RISP')
          select _Curs_COR_RISP
          locate for 1=1
          do while not(eof())
          if NOT EMPTY(NVL(_Curs_COR_RISP.MDRIFCOR,""))
            this.w_MDSERIAL = NVL(_Curs_COR_RISP.MDSERIAL,"")
            * --- Documento associato a vendite Corrispettivi (differita) Elimina Riferimento
            * --- Write into COR_RISP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COR_RISP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MDRIFCOR ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'COR_RISP','MDRIFCOR');
                  +i_ccchkf ;
              +" where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.w_MDSERIAL);
                     )
            else
              update (i_cTable) set;
                  MDRIFCOR = SPACE(10);
                  &i_ccchkf. ;
               where;
                  MDSERIAL = this.w_MDSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_COR_RISP
            continue
          enddo
          use
        endif
      else
        * --- Fatture Fiscali
        * --- Select from COR_RISP
        i_nConn=i_TableProp[this.COR_RISP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" COR_RISP ";
              +" where MDRIFDOC="+cp_ToStrODBC(this.w_SERIALE)+"";
               ,"_Curs_COR_RISP")
        else
          select * from (i_cTable);
           where MDRIFDOC=this.w_SERIALE;
            into cursor _Curs_COR_RISP
        endif
        if used('_Curs_COR_RISP')
          select _Curs_COR_RISP
          locate for 1=1
          do while not(eof())
          if NOT EMPTY(NVL(_Curs_COR_RISP.MDRIFDOC,""))
            this.w_MDSERIAL = NVL(_Curs_COR_RISP.MDSERIAL,"")
            this.w_MDTIPCOR = NVL(_Curs_COR_RISP.MDTIPCOR, "")
            this.w_MDTIPDOC = NVL(_Curs_COR_RISP.MDTIPDOC,"")
            this.w_MDTIPVEN = NVL(_Curs_COR_RISP.MDTIPVEN,"")
            if EMPTY(this.w_MDTIPCOR)
              * --- Read from MOD_VEND
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MOD_VEND_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MOD_VEND_idx,2],.t.,this.MOD_VEND_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MODOCCOR"+;
                  " from "+i_cTable+" MOD_VEND where ";
                      +"MOCODICE = "+cp_ToStrODBC(this.w_MDTIPVEN);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MODOCCOR;
                  from (i_cTable) where;
                      MOCODICE = this.w_MDTIPVEN;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MDTIPCOR = NVL(cp_ToDate(_read_.MODOCCOR),cp_NullValue(_read_.MODOCCOR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_MDTIPCOR = IIF(EMPTY(this.w_MDTIPCOR), this.w_MDTIPDOC, this.w_MDTIPCOR)
            endif
            * --- Documento associato a Vattura Fiscale Elimina Riferimento
            * --- Resetta anche la Tipologia di Chiusura
            * --- Write into COR_RISP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COR_RISP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MDRIFDOC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'COR_RISP','MDRIFDOC');
              +",MDTIPCHI ="+cp_NullLink(cp_ToStrODBC("ES"),'COR_RISP','MDTIPCHI');
              +",MDTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MDTIPCOR),'COR_RISP','MDTIPDOC');
              +",MDNUMDOC ="+cp_NullLink(cp_ToStrODBC(0),'COR_RISP','MDNUMDOC');
                  +i_ccchkf ;
              +" where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.w_MDSERIAL);
                     )
            else
              update (i_cTable) set;
                  MDRIFDOC = SPACE(10);
                  ,MDTIPCHI = "ES";
                  ,MDTIPDOC = this.w_MDTIPCOR;
                  ,MDNUMDOC = 0;
                  &i_ccchkf. ;
               where;
                  MDSERIAL = this.w_MDSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_COR_RISP
            continue
          enddo
          use
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pSERIAL,pFLORIG)
    this.pSERIAL=pSERIAL
    this.pFLORIG=pFLORIG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='COR_RISP'
    this.cWorkTables[2]='MOD_VEND'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_COR_RISP')
      use in _Curs_COR_RISP
    endif
    if used('_Curs_COR_RISP')
      use in _Curs_COR_RISP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pFLORIG"
endproc
