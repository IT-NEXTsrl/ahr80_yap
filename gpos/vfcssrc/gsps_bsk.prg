* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bsk                                                        *
*              Stampa del documento                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_289]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-19                                                      *
* Last revis.: 2014-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bsk",oParentObject,m.pTipo)
return(i_retval)

define class tgsps_bsk as StdBatch
  * --- Local variables
  pTipo = 0
  w_MVSERIAL = space(10)
  w_MVFLVEAC = space(1)
  w_MVCLADOC = space(2)
  w_MVTIPCON = space(1)
  w_MVTIPDOC = space(5)
  w_TELFAX = space(18)
  w_MVCODCON = space(15)
  w_EMAIL = space(10)
  w_EMPEC = space(10)
  w_MVCODIVE = space(5)
  w_MVRIFFAD = space(10)
  w_MVRIFODL = space(10)
  w_MVTCAMAG = space(5)
  w_MVCODVAL = space(3)
  w_REP = 0
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_FLNSTA = space(1)
  w_FLINCA = space(1)
  w_OREP = space(50)
  w_OQRY = space(50)
  w_OTES = space(1)
  w_FLCOMM = space(1)
  w_FLANAL = space(10)
  w_MVCODDES = space(5)
  w_NOMDES = space(36)
  w_DESCLF = space(40)
  w_CATDOC = space(2)
  w_EMAIL1 = space(10)
  w_EMPEC1 = space(10)
  w_EMAIL2 = space(10)
  w_EMPEC2 = space(10)
  w_FUNC = space(1)
  w_SERIAL = space(10)
  w_MVSERRIF = space(10)
  w_OK = .f.
  w_TIPO = space(1)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_LINGUA = space(3)
  w_codiva = space(5)
  w_APPO = space(10)
  w_PRGSTA = space(30)
  w_contiva = 0
  w_NUMER = 0
  w_CODICE = space(15)
  w_DATAIN = ctod("  /  /  ")
  w_TRADLIN = space(3)
  w_SERDOC = space(10)
  w_contrate = 0
  w_FC = space(1)
  w_MESS = space(1)
  w_DATAFI = ctod("  /  /  ")
  w_TIPDOC = space(5)
  w_CAMPO = space(10)
  w_CATCOM = space(3)
  w_CLIFOR = space(15)
  w_CODCON = space(15)
  w_DATA = ctod("  /  /  ")
  w_TIPOIN = space(5)
  w_BIANCO = .f.
  w_CODCLA = space(3)
  w_NOTE = space(0)
  w_SERIE1 = space(2)
  w_SERIE2 = space(2)
  w_CODESE = space(4)
  w_DT = space(35)
  w_OLDMAGA = space(5)
  w_CODSTA = space(3)
  w_OLDCAUSA = space(5)
  w_COND = .f.
  w_CODCAU = space(5)
  w_CAUMAG = space(5)
  w_CODART = space(20)
  w_DESART = space(40)
  w_TIPCON = space(1)
  w_CODMAG = space(5)
  w_CODICE1 = space(20)
  w_DESSUP = space(0)
  w_TRPRGST = 0
  w_OKMAG = .f.
  w_PRIMAG = space(5)
  w_KEYOBS = ctod("  /  /  ")
  w_TIPCON1 = space(1)
  w_CODCON1 = space(15)
  w_DTOBS0 = ctod("  /  /  ")
  w_DTOBS1 = ctod("  /  /  ")
  w_codic = space(15)
  w_nmdes = space(40)
  w_ind = space(40)
  w_cap = space(5)
  w_loca = space(30)
  w_prov = space(2)
  w_nazi = space(3)
  w_rif = space(2)
  w_DATORIG = ctod("  /  /  ")
  w_CODDES = space(15)
  w_PRIMAT = space(5)
  w_DESART1 = space(40)
  w_DESSUP1 = space(0)
  w_MVCODICE = space(20)
  w_OLDSUBJECT = space(254)
  w_CODNEG = space(3)
  w_DADATA = ctod("  /  /  ")
  w_ADATA = ctod("  /  /  ")
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_ORAFIN = space(2)
  w_MINFIN = space(2)
  w_CODOPE = 0
  w_CLIENTE = space(15)
  w_PAGAM = space(5)
  w_GIOLUN = space(1)
  w_GIOMAR = space(1)
  w_GIOMER = space(1)
  w_GIOGIO = space(1)
  w_GIOVEN = space(1)
  w_GIOSAB = space(1)
  w_GIODOM = space(1)
  w_SCOINI = 0
  w_SCOFIN = 0
  w_MATSCO = space(10)
  w_TIPVEN = space(5)
  w_TIPCHI1 = space(2)
  w_DATA1 = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_CODPAG = space(5)
  w_CATEGO = space(2)
  w_CODZON = space(3)
  w_CODVET = space(5)
  w_NOSBAN = space(5)
  w_CODAGE = space(5)
  * --- WorkFile variables
  DES_DIVE_idx=0
  KEY_ARTI_idx=0
  OUT_PUTS_idx=0
  TIP_DOCU_idx=0
  TRADDOCU_idx=0
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  TRADARTI_idx=0
  COR_RISP_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la Stampa del Documento (da GSPS_MVD)
    * --- 1= Record Inserted ; 2=Record Updated ; 3=F2 - Stampa
    * --- Parametri passati alla query per compatibilit� con ristampa doc.
    this.w_FUNC = this.oParentObject.cFunction
    this.w_OK = .T.
    this.w_FLCOMM = " "
    this.w_MVRIFFAD = SPACE(10)
    this.w_MVRIFODL = SPACE(10)
    * --- Genera Documenti Dai Kit
    if this.pTipo<>3 AND NOT EMPTY(this.oParentObject.w_CARKIT) AND NOT EMPTY(this.oParentObject.w_SCAKIT)
      this.oParentObject.NotifyEvent("GeneraKit")
    endif
    if EMPTY(this.oParentObject.w_MDSERIAL) AND this.oParentObject.w_MDTIPCHI <> "NS"
      ah_ErrorMsg("Codice vendita negozio non definito","!","")
      i_retcode = 'stop'
      return
    endif
    do case
      case this.oParentObject.w_MDTIPCHI $ "RF-FI-FF-RS-DT"
        * --- Ric. Fiscale; Fattura ; DDT
        if this.pTipo<>3
          * --- Elimina preventivamente il Riferimento (per rigenerarlo successivamente)
          * --- Write into COR_RISP
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.COR_RISP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MDRIFDOC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'COR_RISP','MDRIFDOC');
                +i_ccchkf ;
            +" where ";
                +"MDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDSERIAL);
                   )
          else
            update (i_cTable) set;
                MDRIFDOC = SPACE(10);
                &i_ccchkf. ;
             where;
                MDSERIAL = this.oParentObject.w_MDSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Generazione Immediata del Documento Allegato
          this.oParentObject.NotifyEvent("AggiornaDocumento")
        endif
        if this.oParentObject.w_MDTIPCHI ="RF"
          * --- Se ricevuta Fiscale leggo il campo MDRIFCOR....
          * --- Read from COR_RISP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COR_RISP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MDRIFCOR"+;
              " from "+i_cTable+" COR_RISP where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MDRIFCOR;
              from (i_cTable) where;
                  MDSERIAL = this.oParentObject.w_MDSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVSERIAL = NVL(cp_ToDate(_read_.MDRIFCOR),cp_NullValue(_read_.MDRIFCOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- ...altrimenti MDRIFDOC
          * --- Read from COR_RISP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COR_RISP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MDRIFDOC"+;
              " from "+i_cTable+" COR_RISP where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MDRIFDOC;
              from (i_cTable) where;
                  MDSERIAL = this.oParentObject.w_MDSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVSERIAL = NVL(cp_ToDate(_read_.MDRIFDOC),cp_NullValue(_read_.MDRIFDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if EMPTY(this.w_MVSERIAL)
          ah_ErrorMsg("Documento di vendita negozio non generato","!","")
          i_retcode = 'stop'
          return
        else
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLVEAC,MVCLADOC,MVTIPCON,MVTIPDOC,MVCODCON,MVCODIVE,MVDATDOC,MVALFDOC,MVNUMDOC,MVCODDES,MVTCAMAG,MVCODVAL"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLVEAC,MVCLADOC,MVTIPCON,MVTIPDOC,MVCODCON,MVCODIVE,MVDATDOC,MVALFDOC,MVNUMDOC,MVCODDES,MVTCAMAG,MVCODVAL;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
            this.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
            this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
            this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
            this.w_MVCODIVE = NVL(cp_ToDate(_read_.MVCODIVE),cp_NullValue(_read_.MVCODIVE))
            this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_MVCODDES = NVL(cp_ToDate(_read_.MVCODDES),cp_NullValue(_read_.MVCODDES))
            this.w_MVTCAMAG = NVL(cp_ToDate(_read_.MVTCAMAG),cp_NullValue(_read_.MVTCAMAG))
            this.w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLANAL,TDFLNSTA,TDPRGSTA"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLANAL,TDFLNSTA,TDPRGSTA;
              from (i_cTable) where;
                  TDTIPDOC = this.w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
            this.w_FLNSTA = NVL(cp_ToDate(_read_.TDFLNSTA),cp_NullValue(_read_.TDFLNSTA))
            this.w_REP = NVL(cp_ToDate(_read_.TDPRGSTA),cp_NullValue(_read_.TDPRGSTA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_NOMDES = ""
          this.w_DESCLF = ""
          this.w_TELFAX = " "
          this.w_EMAIL1 = ""
          this.w_EMPEC1 = ""
          this.w_EMAIL2 = ""
          this.w_EMPEC2 = ""
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANDESCRI,ANTELFAX,AN_EMAIL,AN_EMPEC"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANDESCRI,ANTELFAX,AN_EMAIL,AN_EMPEC;
              from (i_cTable) where;
                  ANTIPCON = this.w_MVTIPCON;
                  and ANCODICE = this.w_MVCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCLF = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
            this.w_TELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
            this.w_EMAIL1 = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
            this.w_EMPEC1 = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_MVCODDES)
            * --- Read from DES_DIVE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DES_DIVE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DDNOMDES,DD_EMAIL,DD_EMPEC"+;
                " from "+i_cTable+" DES_DIVE where ";
                    +"DDTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                    +" and DDCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                    +" and DDCODDES = "+cp_ToStrODBC(this.w_MVCODDES);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DDNOMDES,DD_EMAIL,DD_EMPEC;
                from (i_cTable) where;
                    DDTIPCON = this.w_MVTIPCON;
                    and DDCODICE = this.w_MVCODCON;
                    and DDCODDES = this.w_MVCODDES;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NOMDES = NVL(cp_ToDate(_read_.DDNOMDES),cp_NullValue(_read_.DDNOMDES))
              this.w_EMAIL2 = NVL(cp_ToDate(_read_.DD_EMAIL),cp_NullValue(_read_.DD_EMAIL))
              this.w_EMPEC2 = NVL(cp_ToDate(_read_.DD_EMPEC),cp_NullValue(_read_.DD_EMPEC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_EMAIL = IIF(EMPTY(this.w_EMAIL2), this.w_EMAIL1, this.w_EMAIL2)
          this.w_EMPEC = IIF(EMPTY(this.w_EMPEC2), this.w_EMPEC1, this.w_EMPEC2)
          this.w_CATDOC = this.w_MVCLADOC
          * --- Se acconti precedenti o abbuono � uguale a totale documento Flag Totalmente incassato = ' '
          *     Se valorizzato il conto cliente, Flag Totalmente incassato = ' '
          *     Negli altri casi il corrispettivo � stato pagato interamente quindi flag totalmente incassato = 'S'
          this.w_FLINCA = IIF((this.oParentObject.w_MDTOTDOC=this.oParentObject.w_MDACCPRE) OR (this.oParentObject.w_MDTOTDOC=this.oParentObject.w_MDIMPABB)," ",IIF(this.oParentObject.w_MDPAGCLI<>0," ","S"))
          if NOT ((this.w_FLNSTA="S" AND this.pTipo<>0) OR (EMPTY(this.w_MVSERIAL) AND this.pTipo=0))
            * --- Documento Abilitato per le Stampe o selezionato
            * --- I vettori vanno definiti fuori dalla funzione
            dimension iva(6,2)
            iva=""
            dimension doc_rate(6,3)
            doc_rate=""
            * --- Cambio della Valuta in LIRE utilizzato in talune stampe
            l_cambio = g_CAOEUR
            * --- Passo al report Flag per incasso Corrispettivi
            L_FLINCA=this.w_FLINCA
            * --- SETTO VARIABILI PER FAX E EMAIL
            if not empty(this.w_MVCODDES)
              i_DEST = ALLTRIM(this.w_NOMDES)
            else
              i_DEST = ALLTRIM(this.w_DESCLF)
            endif
            i_FAXNO = this.w_TELFAX
            i_EMAIL = this.w_EMAIL
            i_EMAIL_PEC = this.w_EMPEC
            i_WEDEST = this.w_MVTIPCON+this.w_MVCODCON
            i_WEALLENAME = this.w_CATDOC+this.w_MVFLVEAC+"_"+dtos(this.w_MVDATDOC)+"_"+Alltrim(this.w_MVALFDOC)+IIF(EMPTY(this.w_MVALFDOC),"","_")+alltrim(STR(this.w_MVNUMDOC,15,0))
            do case
              case this.w_CATDOC="DI"
                i_WEALLETITLE = ah_Msgformat("Doc.interno n. %1 del %2",alltrim(STR(this.w_MVNUMDOC,15,0))+IIF(EMPTY(this.w_MVALFDOC),"","/")+alltrim(this.w_MVALFDOC), dtoc(this.w_MVDATDOC) )
              case this.w_CATDOC="OR"
                i_WEALLETITLE = ah_Msgformat("Ordine n. %1 del %2",alltrim(STR(this.w_MVNUMDOC,15,0))+IIF(EMPTY(this.w_MVALFDOC),"","/")+alltrim(this.w_MVALFDOC), dtoc(this.w_MVDATDOC) )
              case this.w_CATDOC="DT"
                i_WEALLETITLE = ah_Msgformat("Doc.trasporto n. %1 del %2",alltrim(STR(this.w_MVNUMDOC,15,0))+IIF(EMPTY(this.w_MVALFDOC),"","/")+alltrim(this.w_MVALFDOC), dtoc(this.w_MVDATDOC) ) 
              case this.w_CATDOC="FA"
                i_WEALLETITLE = ah_Msgformat("Fattura n. %1 del %2",alltrim(STR(this.w_MVNUMDOC,15,0))+IIF(EMPTY(this.w_MVALFDOC),"","/")+alltrim(this.w_MVALFDOC), dtoc(this.w_MVDATDOC) )
              case this.w_CATDOC="NC"
                i_WEALLETITLE = ah_Msgformat("Nota di credito n. %1 del %2",alltrim(STR(this.w_MVNUMDOC,15,0))+IIF(EMPTY(this.w_MVALFDOC),"","/")+alltrim(this.w_MVALFDOC), dtoc(this.w_MVDATDOC) )
              case this.w_CATDOC="OP"
                i_WEALLETITLE = ah_Msgformat("Ordine previsionale n. %1 del %2",alltrim(STR(this.w_MVNUMDOC,15,0))+IIF(EMPTY(this.w_MVALFDOC),"","/")+alltrim(this.w_MVALFDOC), dtoc(this.w_MVDATDOC) )
              case this.w_CATDOC="RF"
                i_WEALLETITLE = ah_Msgformat("Corrispettivo n. %1 del %2",alltrim(STR(this.w_MVNUMDOC,15,0))+IIF(EMPTY(this.w_MVALFDOC),"","/")+alltrim(this.w_MVALFDOC), dtoc(this.w_MVDATDOC) )
              otherwise
                i_WEALLETITLE = ah_Msgformat("Documento n. %1 del %2",alltrim(STR(this.w_MVNUMDOC,15,0))+IIF(EMPTY(this.w_MVALFDOC),"","/")+alltrim(this.w_MVALFDOC), dtoc(this.w_MVDATDOC) )
            endcase
            this.w_OLDSUBJECT = i_EMAILSUBJECT
            i_EMAILSUBJECT=i_WEALLETITLE
            if this.w_OK
              ah_Msg("Stampa in corso...",.T.)
              * --- Controllo i documenti di trasporto
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          * --- Resetta Fax/Email
          i_FAXNO = ""
          i_EMAIL = ""
          i_EMAIL_PEC = ""
          i_DEST = " "
          i_WEDEST = " "
          i_WEALLENAME = " "
          i_EMAILSUBJECT = this.w_OLDSUBJECT
        endif
      case this.oParentObject.w_MDTIPCHI = "ES"
        if this.w_FUNC="Load" OR (this.oParentObject.w_OTIPCHI<> this.oParentObject.w_MDTIPCHI)
          this.w_SERIAL = this.oParentObject.w_MDSERIAL
          * --- Emissione Scontrino
          do GSPS_KES with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_OTIPCHI = this.oParentObject.w_MDTIPCHI
        endif
      case this.oParentObject.w_MDTIPCHI = "BV"
        * --- Brogliaccio di Vendita
        this.w_CODNEG = SPACE(3)
        this.w_DADATA = this.oParentObject.w_MDDATREG
        this.w_ADATA = this.oParentObject.w_MDDATREG
        this.w_ORAINI = "00"
        this.w_MININI = "01"
        this.w_ORAFIN = "23"
        this.w_MINFIN = "59"
        this.w_CODOPE = SPACE(2)
        this.w_CLIENTE = SPACE(15)
        this.w_PAGAM = SPACE(5)
        this.w_CODMAG = SPACE(5)
        this.w_GIOLUN = "2"
        this.w_GIOMAR = "3"
        this.w_GIOMER = "4"
        this.w_GIOGIO = "5"
        this.w_GIOVEN = "6"
        this.w_GIOSAB = "7"
        this.w_GIODOM = "1"
        this.w_SCOINI = 0
        this.w_SCOFIN = 999999
        this.w_MATSCO = SPACE(10)
        this.w_TIPCHI1 = " "
        this.w_TIPVEN = " "
        GSPS_BBD(this,this.oParentObject.w_MDSERIAL,"A")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa documento
    this.w_TIPOIN = this.w_MVTIPDOC
    GSVE_BRD(this,"F")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='DES_DIVE'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='OUT_PUTS'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='TRADDOCU'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='DOC_DETT'
    this.cWorkTables[8]='TRADARTI'
    this.cWorkTables[9]='COR_RISP'
    this.cWorkTables[10]='CONTI'
    return(this.OpenAllTables(10))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
