* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_amf                                                        *
*              Movimenti Fidelity                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_19]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-19                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_amf"))

* --- Class definition
define class tgsps_amf as StdForm
  Top    = 25
  Left   = 83

  * --- Standard Properties
  Width  = 583
  Height = 242+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=210327657
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  MOV_FIDE_IDX = 0
  CAU_FIDE_IDX = 0
  FID_CARD_IDX = 0
  CONTI_IDX = 0
  cFile = "MOV_FIDE"
  cKeySelect = "MFSERIAL"
  cKeyWhere  = "MFSERIAL=this.w_MFSERIAL"
  cKeyWhereODBC = '"MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cKeyWhereODBCqualified = '"MOV_FIDE.MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cPrg = "gsps_amf"
  cComment = "Movimenti Fidelity"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MFSERIAL = space(10)
  w_MFCAUFID = space(5)
  o_MFCAUFID = space(5)
  w_PUNPRE = space(1)
  w_TIPMOV = space(1)
  w_MFFLPUNT = space(1)
  w_MFFLRESI = space(1)
  w_MFCODFID = space(20)
  w_MF_PUNTI = 0
  w_MF_PREPA = 0
  w_MFDATMOV = ctod('  /  /  ')
  w_MFDESMOV = space(0)
  w_DESCAU = space(40)
  w_DESFID = space(40)
  w_CODCLI = space(15)
  w_DESCLI = space(40)
  w_TIPCON = space(1)
  w_MAXFID = 0
  w_IMPRES = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MFSERIAL = this.W_MFSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOV_FIDE','gsps_amf')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_amfPag1","gsps_amf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimento fidelity")
      .Pages(1).HelpContextID = 191333331
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAU_FIDE'
    this.cWorkTables[2]='FID_CARD'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='MOV_FIDE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOV_FIDE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOV_FIDE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MFSERIAL = NVL(MFSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_8_joined
    link_1_8_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOV_FIDE where MFSERIAL=KeySet.MFSERIAL
    *
    i_nConn = i_TableProp[this.MOV_FIDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_FIDE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOV_FIDE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOV_FIDE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOV_FIDE '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PUNPRE = space(1)
        .w_TIPMOV = space(1)
        .w_DESCAU = space(40)
        .w_DESFID = space(40)
        .w_CODCLI = space(15)
        .w_DESCLI = space(40)
        .w_TIPCON = 'C'
        .w_MAXFID = 0
        .w_IMPRES = 0
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCAUFID = NVL(MFCAUFID,space(5))
          if link_1_2_joined
            this.w_MFCAUFID = NVL(CFCODICE102,NVL(this.w_MFCAUFID,space(5)))
            this.w_DESCAU = NVL(CFDESCRI102,space(40))
            this.w_TIPMOV = NVL(CFTIPMOV102,space(1))
            this.w_PUNPRE = NVL(CFPUNPRE102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_MFFLPUNT = NVL(MFFLPUNT,space(1))
        .w_MFFLRESI = NVL(MFFLRESI,space(1))
        .w_MFCODFID = NVL(MFCODFID,space(20))
          if link_1_8_joined
            this.w_MFCODFID = NVL(FCCODFID108,NVL(this.w_MFCODFID,space(20)))
            this.w_DESFID = NVL(FCDESFID108,space(40))
            this.w_CODCLI = NVL(FCCODCLI108,space(15))
            this.w_MAXFID = NVL(FCIMPPRE108,0)
            this.w_IMPRES = NVL(FCIMPRES108,0)
          else
          .link_1_8('Load')
          endif
        .w_MF_PUNTI = NVL(MF_PUNTI,0)
        .w_MF_PREPA = NVL(MF_PREPA,0)
        .w_MFDATMOV = NVL(cp_ToDate(MFDATMOV),ctod("  /  /  "))
        .w_MFDESMOV = NVL(MFDESMOV,space(0))
          .link_1_20('Load')
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOV_FIDE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MFSERIAL = space(10)
      .w_MFCAUFID = space(5)
      .w_PUNPRE = space(1)
      .w_TIPMOV = space(1)
      .w_MFFLPUNT = space(1)
      .w_MFFLRESI = space(1)
      .w_MFCODFID = space(20)
      .w_MF_PUNTI = 0
      .w_MF_PREPA = 0
      .w_MFDATMOV = ctod("  /  /  ")
      .w_MFDESMOV = space(0)
      .w_DESCAU = space(40)
      .w_DESFID = space(40)
      .w_CODCLI = space(15)
      .w_DESCLI = space(40)
      .w_TIPCON = space(1)
      .w_MAXFID = 0
      .w_IMPRES = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
          if not(empty(.w_MFCAUFID))
          .link_1_2('Full')
          endif
          .DoRTCalc(3,4,.f.)
        .w_MFFLPUNT = IIF(.w_PUNPRE='P',.w_TIPMOV,' ')
        .w_MFFLRESI = IIF(.w_PUNPRE='I',.w_TIPMOV,' ')
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_MFCODFID))
          .link_1_8('Full')
          endif
          .DoRTCalc(8,9,.f.)
        .w_MFDATMOV = i_DATSYS
        .DoRTCalc(11,14,.f.)
          if not(empty(.w_CODCLI))
          .link_1_20('Full')
          endif
          .DoRTCalc(15,15,.f.)
        .w_TIPCON = 'C'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOV_FIDE')
    this.DoRTCalc(17,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOV_FIDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_FIDE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERMOVFID","i_codazi,w_MFSERIAL")
      .op_codazi = .w_codazi
      .op_MFSERIAL = .w_MFSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMFCAUFID_1_2.enabled = i_bVal
      .Page1.oPag.oMFCODFID_1_8.enabled = i_bVal
      .Page1.oPag.oMF_PUNTI_1_10.enabled = i_bVal
      .Page1.oPag.oMF_PREPA_1_12.enabled = i_bVal
      .Page1.oPag.oMFDATMOV_1_14.enabled = i_bVal
      .Page1.oPag.oMFDESMOV_1_16.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oMFCAUFID_1_2.enabled = .t.
        .Page1.oPag.oMFCODFID_1_8.enabled = .t.
        .Page1.oPag.oMFDATMOV_1_14.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOV_FIDE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOV_FIDE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUFID,"MFCAUFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFFLPUNT,"MFFLPUNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFFLRESI,"MFFLRESI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODFID,"MFCODFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PUNTI,"MF_PUNTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PREPA,"MF_PREPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDATMOV,"MFDATMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDESMOV,"MFDESMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOV_FIDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_FIDE_IDX,2])
    i_lTable = "MOV_FIDE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOV_FIDE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOV_FIDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_FIDE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOV_FIDE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SERMOVFID","i_codazi,w_MFSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOV_FIDE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOV_FIDE')
        i_extval=cp_InsertValODBCExtFlds(this,'MOV_FIDE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MFSERIAL,MFCAUFID,MFFLPUNT,MFFLRESI,MFCODFID"+;
                  ",MF_PUNTI,MF_PREPA,MFDATMOV,MFDESMOV,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MFSERIAL)+;
                  ","+cp_ToStrODBCNull(this.w_MFCAUFID)+;
                  ","+cp_ToStrODBC(this.w_MFFLPUNT)+;
                  ","+cp_ToStrODBC(this.w_MFFLRESI)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODFID)+;
                  ","+cp_ToStrODBC(this.w_MF_PUNTI)+;
                  ","+cp_ToStrODBC(this.w_MF_PREPA)+;
                  ","+cp_ToStrODBC(this.w_MFDATMOV)+;
                  ","+cp_ToStrODBC(this.w_MFDESMOV)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOV_FIDE')
        i_extval=cp_InsertValVFPExtFlds(this,'MOV_FIDE')
        cp_CheckDeletedKey(i_cTable,0,'MFSERIAL',this.w_MFSERIAL)
        INSERT INTO (i_cTable);
              (MFSERIAL,MFCAUFID,MFFLPUNT,MFFLRESI,MFCODFID,MF_PUNTI,MF_PREPA,MFDATMOV,MFDESMOV,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MFSERIAL;
                  ,this.w_MFCAUFID;
                  ,this.w_MFFLPUNT;
                  ,this.w_MFFLRESI;
                  ,this.w_MFCODFID;
                  ,this.w_MF_PUNTI;
                  ,this.w_MF_PREPA;
                  ,this.w_MFDATMOV;
                  ,this.w_MFDESMOV;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOV_FIDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_FIDE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOV_FIDE_IDX,i_nConn)
      *
      * update MOV_FIDE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOV_FIDE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MFCAUFID="+cp_ToStrODBCNull(this.w_MFCAUFID)+;
             ",MFFLPUNT="+cp_ToStrODBC(this.w_MFFLPUNT)+;
             ",MFFLRESI="+cp_ToStrODBC(this.w_MFFLRESI)+;
             ",MFCODFID="+cp_ToStrODBCNull(this.w_MFCODFID)+;
             ",MF_PUNTI="+cp_ToStrODBC(this.w_MF_PUNTI)+;
             ",MF_PREPA="+cp_ToStrODBC(this.w_MF_PREPA)+;
             ",MFDATMOV="+cp_ToStrODBC(this.w_MFDATMOV)+;
             ",MFDESMOV="+cp_ToStrODBC(this.w_MFDESMOV)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOV_FIDE')
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        UPDATE (i_cTable) SET;
              MFCAUFID=this.w_MFCAUFID;
             ,MFFLPUNT=this.w_MFFLPUNT;
             ,MFFLRESI=this.w_MFFLRESI;
             ,MFCODFID=this.w_MFCODFID;
             ,MF_PUNTI=this.w_MF_PUNTI;
             ,MF_PREPA=this.w_MF_PREPA;
             ,MFDATMOV=this.w_MFDATMOV;
             ,MFDESMOV=this.w_MFDESMOV;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Transaction
  proc mRestoreTrs(i_bCanSkip)
    local i_cWhere,i_cF,i_nConn,i_cTable
    local i_cOp1,i_cOp2

    i_cF=this.cCursor
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MFFLPUNT,space(1))==this.w_MFFLPUNT;
              and NVL(&i_cF..MF_PUNTI,0)==this.w_MF_PUNTI;
              and NVL(&i_cF..MFFLRESI,space(1))==this.w_MFFLRESI;
              and NVL(&i_cF..MF_PREPA,0)==this.w_MF_PREPA;
              and NVL(&i_cF..MFCODFID,space(20))==this.w_MFCODFID;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MFFLPUNT,space(1)),'FCPUNFID','',NVL(&i_cF..MF_PUNTI,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..MFFLRESI,space(1)),'FCIMPRES','',NVL(&i_cF..MF_PREPA,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MFCODFID,space(20))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" FCPUNFID="+i_cOp1+","           +" FCIMPRES="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE FCCODFID="+cp_ToStrODBC(NVL(&i_cF..MFCODFID,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MFFLPUNT,'FCPUNFID',i_cF+'.MF_PUNTI',&i_cF..MF_PUNTI,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..MFFLRESI,'FCIMPRES',i_cF+'.MF_PREPA',&i_cF..MF_PREPA,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'FCCODFID',&i_cF..MFCODFID)
      UPDATE (i_cTable) SET ;
           FCPUNFID=&i_cOp1.  ,;
           FCIMPRES=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Transaction
  proc mUpdateTrs(i_bCanSkip)
    local i_cWhere,i_cF,i_nModRow,i_nConn,i_cTable
    local i_cOp1,i_cOp2

    i_cF=this.cCursor
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MFFLPUNT,space(1))==this.w_MFFLPUNT;
              and NVL(&i_cF..MF_PUNTI,0)==this.w_MF_PUNTI;
              and NVL(&i_cF..MFFLRESI,space(1))==this.w_MFFLRESI;
              and NVL(&i_cF..MF_PREPA,0)==this.w_MF_PREPA;
              and NVL(&i_cF..MFCODFID,space(20))==this.w_MFCODFID;

      i_cOp1=cp_SetTrsOp(this.w_MFFLPUNT,'FCPUNFID','this.w_MF_PUNTI',this.w_MF_PUNTI,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MFFLRESI,'FCIMPRES','this.w_MF_PREPA',this.w_MF_PREPA,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MFCODFID)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" FCPUNFID="+i_cOp1  +",";
         +" FCIMPRES="+i_cOp2  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE FCCODFID="+cp_ToStrODBC(this.w_MFCODFID);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MFFLPUNT,'FCPUNFID','this.w_MF_PUNTI',this.w_MF_PUNTI,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_MFFLRESI,'FCIMPRES','this.w_MF_PREPA',this.w_MF_PREPA,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'FCCODFID',this.w_MFCODFID)
      UPDATE (i_cTable) SET;
           FCPUNFID=&i_cOp1.  ,;
           FCIMPRES=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOV_FIDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_FIDE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOV_FIDE_IDX,i_nConn)
      *
      * delete MOV_FIDE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOV_FIDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_FIDE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_MFCAUFID<>.w_MFCAUFID
            .w_MFFLPUNT = IIF(.w_PUNPRE='P',.w_TIPMOV,' ')
        endif
            .w_MFFLRESI = IIF(.w_PUNPRE='I',.w_TIPMOV,' ')
        .DoRTCalc(7,13,.t.)
          .link_1_20('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SERMOVFID","i_codazi,w_MFSERIAL")
          .op_MFSERIAL = .w_MFSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(15,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMF_PUNTI_1_10.enabled = this.oPgFrm.Page1.oPag.oMF_PUNTI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oMF_PREPA_1_12.enabled = this.oPgFrm.Page1.oPag.oMF_PREPA_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MFCAUFID
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_FIDE_IDX,3]
    i_lTable = "CAU_FIDE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2], .t., this.CAU_FIDE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCAUFID) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACF',True,'CAU_FIDE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_MFCAUFID)+"%");

          i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFCODICE',trim(this.w_MFCAUFID))
          select CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCAUFID)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCAUFID) and !this.bDontReportError
            deferred_cp_zoom('CAU_FIDE','*','CFCODICE',cp_AbsName(oSource.parent,'oMFCAUFID_1_2'),i_cWhere,'GSPS_ACF',"Causali movimenti Fidelity",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFCODICE',oSource.xKey(1))
            select CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCAUFID)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_MFCAUFID);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFCODICE',this.w_MFCAUFID)
            select CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCAUFID = NVL(_Link_.CFCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CFDESCRI,space(40))
      this.w_TIPMOV = NVL(_Link_.CFTIPMOV,space(1))
      this.w_PUNPRE = NVL(_Link_.CFPUNPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MFCAUFID = space(5)
      endif
      this.w_DESCAU = space(40)
      this.w_TIPMOV = space(1)
      this.w_PUNPRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_FIDE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCAUFID Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_FIDE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_FIDE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.CFCODICE as CFCODICE102"+ ",link_1_2.CFDESCRI as CFDESCRI102"+ ",link_1_2.CFTIPMOV as CFTIPMOV102"+ ",link_1_2.CFPUNPRE as CFPUNPRE102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on MOV_FIDE.MFCAUFID=link_1_2.CFCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and MOV_FIDE.MFCAUFID=link_1_2.CFCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODFID
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_lTable = "FID_CARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2], .t., this.FID_CARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODFID) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_AFC',True,'FID_CARD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FCCODFID like "+cp_ToStrODBC(trim(this.w_MFCODFID)+"%");

          i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FCCODFID","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FCCODFID',trim(this.w_MFCODFID))
          select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FCCODFID into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODFID)==trim(_Link_.FCCODFID) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODFID) and !this.bDontReportError
            deferred_cp_zoom('FID_CARD','*','FCCODFID',cp_AbsName(oSource.parent,'oMFCODFID_1_8'),i_cWhere,'GSPS_AFC',"Fidelity Card",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES";
                     +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',oSource.xKey(1))
            select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODFID)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES";
                   +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(this.w_MFCODFID);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',this.w_MFCODFID)
            select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODFID = NVL(_Link_.FCCODFID,space(20))
      this.w_DESFID = NVL(_Link_.FCDESFID,space(40))
      this.w_CODCLI = NVL(_Link_.FCCODCLI,space(15))
      this.w_MAXFID = NVL(_Link_.FCIMPPRE,0)
      this.w_IMPRES = NVL(_Link_.FCIMPRES,0)
    else
      if i_cCtrl<>'Load'
        this.w_MFCODFID = space(20)
      endif
      this.w_DESFID = space(40)
      this.w_CODCLI = space(15)
      this.w_MAXFID = 0
      this.w_IMPRES = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])+'\'+cp_ToStr(_Link_.FCCODFID,1)
      cp_ShowWarn(i_cKey,this.FID_CARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODFID Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FID_CARD_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.FCCODFID as FCCODFID108"+ ",link_1_8.FCDESFID as FCDESFID108"+ ",link_1_8.FCCODCLI as FCCODCLI108"+ ",link_1_8.FCIMPPRE as FCIMPPRE108"+ ",link_1_8.FCIMPRES as FCIMPRES108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on MOV_FIDE.MFCODFID=link_1_8.FCCODFID"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and MOV_FIDE.MFCODFID=link_1_8.FCCODFID(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMFCAUFID_1_2.value==this.w_MFCAUFID)
      this.oPgFrm.Page1.oPag.oMFCAUFID_1_2.value=this.w_MFCAUFID
    endif
    if not(this.oPgFrm.Page1.oPag.oMFCODFID_1_8.value==this.w_MFCODFID)
      this.oPgFrm.Page1.oPag.oMFCODFID_1_8.value=this.w_MFCODFID
    endif
    if not(this.oPgFrm.Page1.oPag.oMF_PUNTI_1_10.value==this.w_MF_PUNTI)
      this.oPgFrm.Page1.oPag.oMF_PUNTI_1_10.value=this.w_MF_PUNTI
    endif
    if not(this.oPgFrm.Page1.oPag.oMF_PREPA_1_12.value==this.w_MF_PREPA)
      this.oPgFrm.Page1.oPag.oMF_PREPA_1_12.value=this.w_MF_PREPA
    endif
    if not(this.oPgFrm.Page1.oPag.oMFDATMOV_1_14.value==this.w_MFDATMOV)
      this.oPgFrm.Page1.oPag.oMFDATMOV_1_14.value=this.w_MFDATMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oMFDESMOV_1_16.value==this.w_MFDESMOV)
      this.oPgFrm.Page1.oPag.oMFDESMOV_1_16.value=this.w_MFDESMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_18.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_18.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFID_1_19.value==this.w_DESFID)
      this.oPgFrm.Page1.oPag.oDESFID_1_19.value=this.w_DESFID
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLI_1_20.value==this.w_CODCLI)
      this.oPgFrm.Page1.oPag.oCODCLI_1_20.value=this.w_CODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_22.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_22.value=this.w_DESCLI
    endif
    cp_SetControlsValueExtFlds(this,'MOV_FIDE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MFCAUFID))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFCAUFID_1_2.SetFocus()
            i_bnoObbl = !empty(.w_MFCAUFID)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MFCODFID))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFCODFID_1_8.SetFocus()
            i_bnoObbl = !empty(.w_MFCODFID)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MF_PUNTI))  and (.w_PUNPRE = 'P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMF_PUNTI_1_10.SetFocus()
            i_bnoObbl = !empty(.w_MF_PUNTI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MF_PREPA))  and (.w_PUNPRE = 'I')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMF_PREPA_1_12.SetFocus()
            i_bnoObbl = !empty(.w_MF_PREPA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MFDATMOV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFDATMOV_1_14.SetFocus()
            i_bnoObbl = !empty(.w_MFDATMOV)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_amf
       * --- Controllo Movimento Residuo superiore a Importo Massimo Fidelity
       If i_bRes And .w_PUNPRE = 'I' And .w_MFFLRESI = '+' And ( .w_MF_PREPA + .w_IMPRES ) > .w_MAXFID
           i_cErrorMsg = Ah_MsgFormat("La ricarica supera l'importo massimo impostato")
           i_bRes = .f.
           i_bnoChk = .f.
       Endif
      
       If i_bRes And .w_PUNPRE = 'I' And .w_MFFLRESI = '-' And ( .w_IMPRES - .w_MF_PREPA ) < 0
           i_cErrorMsg = Ah_MsgFormat("Lo scarico imposta il residuo a valore negativo")
           i_bRes = .f.
           i_bnoChk = .f.
       Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MFCAUFID = this.w_MFCAUFID
    return

enddefine

* --- Define pages as container
define class tgsps_amfPag1 as StdContainer
  Width  = 579
  height = 242
  stdWidth  = 579
  stdheight = 242
  resizeXpos=301
  resizeYpos=85
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFCAUFID_1_2 as StdField with uid="IOZDUMBDNO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MFCAUFID", cQueryName = "MFCAUFID",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale del movimento",;
    HelpContextID = 15981814,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=83, Top=17, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_FIDE", cZoomOnZoom="GSPS_ACF", oKey_1_1="CFCODICE", oKey_1_2="this.w_MFCAUFID"

  func oMFCAUFID_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCAUFID_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCAUFID_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_FIDE','*','CFCODICE',cp_AbsName(this.parent,'oMFCAUFID_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACF',"Causali movimenti Fidelity",'',this.parent.oContained
  endproc
  proc oMFCAUFID_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CFCODICE=this.parent.oContained.w_MFCAUFID
     i_obj.ecpSave()
  endproc

  add object oMFCODFID_1_8 as StdField with uid="KWYFESNVRS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MFCODFID", cQueryName = "MFCODFID",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice Fidelity movimentata",;
    HelpContextID = 32890102,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=83, Top=156, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FID_CARD", cZoomOnZoom="GSPS_AFC", oKey_1_1="FCCODFID", oKey_1_2="this.w_MFCODFID"

  func oMFCODFID_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODFID_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODFID_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FID_CARD','*','FCCODFID',cp_AbsName(this.parent,'oMFCODFID_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_AFC',"Fidelity Card",'',this.parent.oContained
  endproc
  proc oMFCODFID_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSPS_AFC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FCCODFID=this.parent.oContained.w_MFCODFID
     i_obj.ecpSave()
  endproc

  add object oMF_PUNTI_1_10 as StdField with uid="AOWAFNBIZJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MF_PUNTI", cQueryName = "MF_PUNTI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Punti movimentati",;
    HelpContextID = 119333647,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=83, Top=216, cSayPict='"999999"', cGetPict='"999999"'

  func oMF_PUNTI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUNPRE = 'P')
    endwith
   endif
  endfunc

  add object oMF_PREPA_1_12 as StdField with uid="NNDADIANSF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MF_PREPA", cQueryName = "MF_PREPA",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo prepagato movimentato",;
    HelpContextID = 233628423,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=437, Top=216, cSayPict='"9999999999999.9999"', cGetPict='"9999999999999.9999"'

  func oMF_PREPA_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUNPRE = 'I')
    endwith
   endif
  endfunc

  add object oMFDATMOV_1_14 as StdField with uid="UMOPKEDSBX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MFDATMOV", cQueryName = "MFDATMOV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data movimento",;
    HelpContextID = 100414236,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=500, Top=17

  add object oMFDESMOV_1_16 as StdMemo with uid="NNGSFAFSMJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MFDESMOV", cQueryName = "MFDESMOV",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione movimento",;
    HelpContextID = 99627804,;
   bGlobalFont=.t.,;
    Height=84, Width=494, Left=83, Top=51

  add object oDESCAU_1_18 as StdField with uid="SCKXPHRLZQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 214901046,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=145, Top=17, InputMask=replicate('X',40)

  add object oDESFID_1_19 as StdField with uid="YYXVHTKZLO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESFID", cQueryName = "DESFID",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 206709046,;
   bGlobalFont=.t.,;
    Height=21, Width=329, Left=239, Top=156, InputMask=replicate('X',40)

  add object oCODCLI_1_20 as StdField with uid="PVXOHAYGZS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 25049894,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=83, Top=183, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLI"

  func oCODCLI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCLI_1_22 as StdField with uid="LYNZZKJWYN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 25108790,;
   bGlobalFont=.t.,;
    Height=21, Width=329, Left=204, Top=183, InputMask=replicate('X',40)

  add object oStr_1_7 as StdString with uid="OKPWVTEVMJ",Visible=.t., Left=5, Top=18,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="HTRJRCEJPW",Visible=.t., Left=9, Top=156,;
    Alignment=1, Width=72, Height=18,;
    Caption="Fidelity:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="JFMRDDJVLB",Visible=.t., Left=9, Top=216,;
    Alignment=1, Width=72, Height=18,;
    Caption="Punti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="HUCCWJOQCB",Visible=.t., Left=301, Top=217,;
    Alignment=1, Width=133, Height=18,;
    Caption="Importo prepagato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NEHWJGEVIU",Visible=.t., Left=442, Top=19,;
    Alignment=1, Width=57, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="QTCYVUAYYK",Visible=.t., Left=7, Top=50,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="PXNQPODMKN",Visible=.t., Left=9, Top=183,;
    Alignment=1, Width=72, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_amf','MOV_FIDE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MFSERIAL=MOV_FIDE.MFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
