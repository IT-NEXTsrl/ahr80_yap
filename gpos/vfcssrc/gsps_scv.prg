* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_scv                                                        *
*              Stampa clienti negozio                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_17]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-14                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_scv",oParentObject))

* --- Class definition
define class tgsps_scv as StdForm
  Top    = 110
  Left   = 168

  * --- Standard Properties
  Width  = 515
  Height = 149
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=90789993
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  CLI_VEND_IDX = 0
  cPrg = "gsps_scv"
  cComment = "Stampa clienti negozio"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODNEG = space(3)
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_ONUME = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_scvPag1","gsps_scv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CLI_VEND'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODNEG=space(3)
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_ONUME=0
        .w_CODNEG = iif(type('g_CODNEG')<>'U',g_CODNEG,' ')
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODINI))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODFIN))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
    this.DoRTCalc(4,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_CODINI))
          select CLCODCLI,CLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oCODINI_1_2'),i_cWhere,'',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_CODINI)
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESINI = NVL(_Link_.CLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_codfin)) OR  (.w_codini<=.w_codfin))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore a quello finale o inesistente o non valido")
        endif
        this.w_CODINI = space(15)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_CODFIN))
          select CLCODCLI,CLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oCODFIN_1_3'),i_cWhere,'',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_CODFIN)
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESFIN = NVL(_Link_.CLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_codfin>=.w_codini) or (empty(.w_codini)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice finale inferiore a quello iniziale o inesistente o non valido")
        endif
        this.w_CODFIN = space(15)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_2.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_2.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_3.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_4.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_4.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_5.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_5.value=this.w_DESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_codfin)) OR  (.w_codini<=.w_codfin)))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore a quello finale o inesistente o non valido")
          case   not(((.w_codfin>=.w_codini) or (empty(.w_codini))))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice finale inferiore a quello iniziale o inesistente o non valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_scvPag1 as StdContainer
  Width  = 511
  height = 149
  stdWidth  = 511
  stdheight = 149
  resizeXpos=325
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_2 as StdField with uid="JRKJOGCJQI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore a quello finale o inesistente o non valido",;
    ToolTipText = "Cliente di inizio selezione",;
    HelpContextID = 147077926,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=86, Top=14, cSayPict="REPL('X',15)", cGetPict="g_MASPOS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", oKey_1_1="CLCODCLI", oKey_1_2="this.w_CODINI"

  proc oCODINI_1_2.mAfter
    with this.Parent.oContained
      .w_CODINI=PSCALZER(.w_CODINI, 'LOAD')
    endwith
  endproc

  func oCODINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oCODINI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this.parent.oContained
  endproc

  add object oCODFIN_1_3 as StdField with uid="FXEIYJWHZM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice finale inferiore a quello iniziale o inesistente o non valido",;
    ToolTipText = "Cliente di fine selezione",;
    HelpContextID = 225524518,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=86, Top=40, cSayPict="REPL('X',15)", cGetPict="g_MASPOS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", oKey_1_1="CLCODCLI", oKey_1_2="this.w_CODFIN"

  proc oCODFIN_1_3.mAfter
    with this.Parent.oContained
      .w_CODFIN=PSCALZER(.w_CODFIN, 'LOAD')
    endwith
  endproc

  func oCODFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oCODFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this.parent.oContained
  endproc

  add object oDESINI_1_4 as StdField with uid="YJDNNQTGSR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 147136822,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=208, Top=14, InputMask=replicate('X',40)

  add object oDESFIN_1_5 as StdField with uid="YTVOGAYYZO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 225583414,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=208, Top=40, InputMask=replicate('X',40)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=86, top=72, width=415,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87207654


  add object oBtn_1_9 as StdButton with uid="VCZMCHUPGB",left=397, top=99, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare la stampa";
    , HelpContextID = 50999590;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="EFNLDLJGWV",left=453, top=99, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 83472570;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_6 as StdString with uid="ZVGIMBXPIO",Visible=.t., Left=20, Top=15,;
    Alignment=1, Width=64, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="HMPDNJIDNF",Visible=.t., Left=23, Top=41,;
    Alignment=1, Width=61, Height=18,;
    Caption="a codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="DYGLJWSYXZ",Visible=.t., Left=23, Top=72,;
    Alignment=1, Width=61, Height=18,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_scv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
