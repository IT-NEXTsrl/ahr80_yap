* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bk2                                                        *
*              Crea anagrafica clienti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_57]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-11                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bk2",oParentObject)
return(i_retval)

define class tgsps_bk2 as StdBatch
  * --- Local variables
  w_CLDESCRI = space(40)
  w_CLINDIRI = space(35)
  w_CL___CAP = space(8)
  w_CLLOCALI = space(30)
  w_CLPROVIN = space(2)
  w_CLNAZION = space(3)
  w_CLTELEFO = space(18)
  w_CLTELFAX = space(18)
  w_CLNUMCEL = space(18)
  w_CL_EMAIL = space(50)
  w_CL_EMPEC = space(50)
  w_CLPARIVA = space(12)
  w_CLCODFIS = space(16)
  w_CLCODPAG = space(5)
  w_CLCATCOM = space(3)
  w_CLCATSCM = space(5)
  w_CLSCONT1 = 0
  w_CLSCONT2 = 0
  w_CLCODLIS = space(5)
  w_CLFLFIDO = space(1)
  w_CLVALFID = 0
  w_CLFIDUTI = 0
  w_CLDTINVA = ctod("  /  /  ")
  w_CLDTOBSO = ctod("  /  /  ")
  w_ANFLFIDO = space(1)
  w_ANCLIPOS = space(1)
  w_CODICE = space(15)
  w_AUTN = 0
  w_CODN = space(15)
  w_LENC = 0
  w_MESS = space(15)
  w_OKTROV = .f.
  w_ANCODSTU = space(5)
  w_MCPROSTU = space(5)
  w_CODSTU = space(5)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_CONCON = space(1)
  * --- WorkFile variables
  CLI_VEND_idx=0
  CONTI_idx=0
  CLI_VEND_idx=0
  MASTRI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea L'anagrafica Clienti (da GSPS_KCC)
    * --- w_CHKERR, Se .T. Notifica alla .M. CheckForm il Fallimento dell'operazione, riportando l'input sulla Maschera
    * --- Read from CLI_VEND
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CLI_VEND_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CLDESCRI,CLINDIRI,CL___CAP,CLLOCALI,CLPROVIN,CLNAZION,CLTELEFO,CLTELFAX,CLNUMCEL,CL_EMAIL,CL_EMPEC,CLPARIVA,CLCODFIS,CLCODPAG,CLCATCOM,CLCATSCM,CLSCONT1,CLSCONT2,CLVALFID,CLCODLIS,CLDTINVA,CLDTOBSO,CLFIDUTI,CLFLFIDO"+;
        " from "+i_cTable+" CLI_VEND where ";
            +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_MDCODCLI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CLDESCRI,CLINDIRI,CL___CAP,CLLOCALI,CLPROVIN,CLNAZION,CLTELEFO,CLTELFAX,CLNUMCEL,CL_EMAIL,CL_EMPEC,CLPARIVA,CLCODFIS,CLCODPAG,CLCATCOM,CLCATSCM,CLSCONT1,CLSCONT2,CLVALFID,CLCODLIS,CLDTINVA,CLDTOBSO,CLFIDUTI,CLFLFIDO;
        from (i_cTable) where;
            CLCODCLI = this.oParentObject.w_MDCODCLI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CLDESCRI = NVL(cp_ToDate(_read_.CLDESCRI),cp_NullValue(_read_.CLDESCRI))
      this.w_CLINDIRI = NVL(cp_ToDate(_read_.CLINDIRI),cp_NullValue(_read_.CLINDIRI))
      this.w_CL___CAP = NVL(cp_ToDate(_read_.CL___CAP),cp_NullValue(_read_.CL___CAP))
      this.w_CLLOCALI = NVL(cp_ToDate(_read_.CLLOCALI),cp_NullValue(_read_.CLLOCALI))
      this.w_CLPROVIN = NVL(cp_ToDate(_read_.CLPROVIN),cp_NullValue(_read_.CLPROVIN))
      this.w_CLNAZION = NVL(cp_ToDate(_read_.CLNAZION),cp_NullValue(_read_.CLNAZION))
      this.w_CLTELEFO = NVL(cp_ToDate(_read_.CLTELEFO),cp_NullValue(_read_.CLTELEFO))
      this.w_CLTELFAX = NVL(cp_ToDate(_read_.CLTELFAX),cp_NullValue(_read_.CLTELFAX))
      this.w_CLNUMCEL = NVL(cp_ToDate(_read_.CLNUMCEL),cp_NullValue(_read_.CLNUMCEL))
      this.w_CL_EMAIL = NVL(cp_ToDate(_read_.CL_EMAIL),cp_NullValue(_read_.CL_EMAIL))
      this.w_CL_EMPEC = NVL(cp_ToDate(_read_.CL_EMPEC),cp_NullValue(_read_.CL_EMPEC))
      this.w_CLPARIVA = NVL(cp_ToDate(_read_.CLPARIVA),cp_NullValue(_read_.CLPARIVA))
      this.w_CLCODFIS = NVL(cp_ToDate(_read_.CLCODFIS),cp_NullValue(_read_.CLCODFIS))
      this.w_CLCODPAG = NVL(cp_ToDate(_read_.CLCODPAG),cp_NullValue(_read_.CLCODPAG))
      this.w_CLCATCOM = NVL(cp_ToDate(_read_.CLCATCOM),cp_NullValue(_read_.CLCATCOM))
      this.w_CLCATSCM = NVL(cp_ToDate(_read_.CLCATSCM),cp_NullValue(_read_.CLCATSCM))
      this.w_CLSCONT1 = NVL(cp_ToDate(_read_.CLSCONT1),cp_NullValue(_read_.CLSCONT1))
      this.w_CLSCONT2 = NVL(cp_ToDate(_read_.CLSCONT2),cp_NullValue(_read_.CLSCONT2))
      this.w_CLVALFID = NVL(cp_ToDate(_read_.CLVALFID),cp_NullValue(_read_.CLVALFID))
      this.w_CLCODLIS = NVL(cp_ToDate(_read_.CLCODLIS),cp_NullValue(_read_.CLCODLIS))
      this.w_CLDTINVA = NVL(cp_ToDate(_read_.CLDTINVA),cp_NullValue(_read_.CLDTINVA))
      this.w_CLDTOBSO = NVL(cp_ToDate(_read_.CLDTOBSO),cp_NullValue(_read_.CLDTOBSO))
      this.w_CLFIDUTI = NVL(cp_ToDate(_read_.CLFIDUTI),cp_NullValue(_read_.CLFIDUTI))
      this.w_CLFLFIDO = NVL(cp_ToDate(_read_.CLFLFIDO),cp_NullValue(_read_.CLFLFIDO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CLLOCALI = LEFT(this.w_CLLOCALI, 30)
    this.w_CLNAZION = LEFT(this.w_CLNAZION, 3)
    this.w_LENC = MIN(LEN(ALLTRIM(p_CLF)), 15)
    this.w_ANCLIPOS = " "
    * --- Controlli del Form
    this.oParentObject.w_CHKERR = .F.
    this.w_OKTROV = .F.
    if EMPTY(this.oParentObject.w_CLMASCON)
      ah_ErrorMsg("Mastro contabile obbligatorio",,"")
      this.oParentObject.w_CHKERR = .T.
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_CLCATCON)
      ah_ErrorMsg("Categoria contabile obbligatoria",,"")
      this.oParentObject.w_CHKERR = .T.
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_CLCODLIN)
      ah_ErrorMsg("Codice lingua obbligatorio",,"")
      this.oParentObject.w_CHKERR = .T.
      i_retcode = 'stop'
      return
    endif
    if g_LEMC="S"
      * --- Calcola il valore di ANCODSTU.
      *     Prima leggiamo dal mastro se c'� un conto studio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCCODSTU,MCPROSTU"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CLMASCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCCODSTU,MCPROSTU;
          from (i_cTable) where;
              MCCODICE = this.oParentObject.w_CLMASCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODSTU = NVL(cp_ToDate(_read_.MCCODSTU),cp_NullValue(_read_.MCCODSTU))
        this.w_MCPROSTU = NVL(cp_ToDate(_read_.MCPROSTU),cp_NullValue(_read_.MCPROSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Istanzio Oggetto Messaggio Incrementale
    this.w_oMESS=createobject("ah_message")
    if NOT EMPTY(this.w_CLPARIVA)
      this.w_oMESS.AddMsgPartNL("Esiste gi� un cliente con la stessa partita IVA")     
      * --- Select from CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ANCODICE,ANPARIVA,ANDESCRI,ANCLIPOS,ANTIPCON  from "+i_cTable+" CONTI ";
            +" where ANPARIVA="+cp_ToStrODBC(this.w_CLPARIVA)+" ";
             ,"_Curs_CONTI")
      else
        select ANCODICE,ANPARIVA,ANDESCRI,ANCLIPOS,ANTIPCON from (i_cTable);
         where ANPARIVA=this.w_CLPARIVA ;
          into cursor _Curs_CONTI
      endif
      if used('_Curs_CONTI')
        select _Curs_CONTI
        locate for 1=1
        do while not(eof())
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_CONTI
          continue
        enddo
        use
      endif
    endif
    if NOT EMPTY(this.w_CLCODFIS) and NOT this.w_OKTROV
      this.w_oMESS.AddMsgPartNL("Esiste gi� un cliente con lo stesso codice fiscale")     
      * --- Select from CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ANCODICE,ANPARIVA,ANDESCRI,ANCLIPOS,ANTIPCON  from "+i_cTable+" CONTI ";
            +" where ANCODFIS="+cp_ToStrODBC(this.w_CLCODFIS)+"";
             ,"_Curs_CONTI")
      else
        select ANCODICE,ANPARIVA,ANDESCRI,ANCLIPOS,ANTIPCON from (i_cTable);
         where ANCODFIS=this.w_CLCODFIS;
          into cursor _Curs_CONTI
      endif
      if used('_Curs_CONTI')
        select _Curs_CONTI
        locate for 1=1
        do while not(eof())
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_CONTI
          continue
        enddo
        use
      endif
    endif
    if this.w_OKTROV=.F. AND this.oParentObject.w_CHKERR=.F.
      * --- Select from CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ANCODICE,ANDESCRI,ANCLIPOS  from "+i_cTable+" CONTI ";
            +" where ANTIPCON='C' AND ANCODICE="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
             ,"_Curs_CONTI")
      else
        select ANCODICE,ANDESCRI,ANCLIPOS from (i_cTable);
         where ANTIPCON="C" AND ANCODICE=this.oParentObject.w_ANCODICE;
          into cursor _Curs_CONTI
      endif
      if used('_Curs_CONTI')
        select _Curs_CONTI
        locate for 1=1
        do while not(eof())
        if this.w_OKTROV=.F.
          this.w_CODICE = ANCODICE
          this.w_ANCLIPOS = NVL(ANCLIPOS, " ")
          if ah_YesNo("Codice cliente gi� presente in anagrafica%0(%1-%2)%0Associo il cliente negozio a quest'ultimo?","", ALLTRIM(this.w_CODICE), ALLTRIM(NVL(ANDESCRI,"")) )
            this.oParentObject.w_ANCODICE = this.w_CODICE
            this.w_OKTROV = .T.
            if this.w_ANCLIPOS<>"S"
              * --- Se Cliente no POS inserisce il Flag
              * --- Write into CONTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
                    +i_ccchkf ;
                +" where ";
                    +"ANTIPCON = "+cp_ToStrODBC("C");
                    +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_ANCODICE);
                       )
              else
                update (i_cTable) set;
                    ANCLIPOS = "S";
                    &i_ccchkf. ;
                 where;
                    ANTIPCON = "C";
                    and ANCODICE = this.oParentObject.w_ANCODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          else
            this.oParentObject.w_CHKERR = .T.
            i_retcode = 'stop'
            return
          endif
        endif
          select _Curs_CONTI
          continue
        enddo
        use
      endif
    endif
    * --- Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
    if this.oParentObject.w_CHKERR=.F.
      this.w_AUTN = 0
      this.w_CODN = ALLTRIM(this.oParentObject.w_ANCODICE)
      FOR p_POS=1 TO LEN(this.w_CODN)
      this.w_AUTN = IIF(SUBSTR(this.w_CODN, p_POS, 1) $ "0123456789", this.w_AUTN, 1)
      ENDFOR
      * --- Try
      local bErr_03818C68
      bErr_03818C68=bTrsErr
      this.Try_03818C68()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore: il cliente non pu� essere creato%0%1",,"", MESSAGE() )
        this.oParentObject.w_CHKERR = .T.
      endif
      bTrsErr=bTrsErr or bErr_03818C68
      * --- End
    endif
  endproc
  proc Try_03818C68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorna il progressivo cliente (se codifica numerica)
    * --- begin transaction
    cp_BeginTrs()
    if this.w_OKTROV=.F.
      if g_CFNUME="S" AND this.w_AUTN=0
        this.w_CODICE = this.oParentObject.w_ANCODICE
        i_nConn = i_TableProp[this.CONTI_IDX,3]
        cp_NextTableProg(this,i_nConn,"PRNUCL","i_CODAZI,w_CODICE") 
        this.oParentObject.w_ANCODICE = RIGHT(this.w_CODICE, this.w_LENC)
      endif
      this.w_CONCON = IIF(g_ISONAZ="ITA","E","1")
      this.w_ANFLFIDO = IIF(g_PERFID="S", this.w_CLFLFIDO, " ")
      if g_LEMC="S"
        this.w_ANCODSTU = IIF(!EMPTY(this.w_CODSTU),PADL(ALLTRIM(GSLM_BCF(this, "C", this.oParentObject.w_ANCODICE, this.w_CODSTU, this.oParentObject.w_CLMASCON , , this.w_MCPROSTU)),6,"0")," ")
      endif
      * --- Insert into CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"AN___CAP"+",ANCATCOM"+",ANCATSCM"+",ANCODICE"+",ANCODPAG"+",ANDESCR2"+",ANDESCRI"+",ANINDIR2"+",ANINDIRI"+",ANLOCALI"+",ANNAZION"+",ANNUMCEL"+",ANNUMLIS"+",ANPROVIN"+",ANTIPCON"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CL___CAP),'CONTI','AN___CAP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLCATCOM),'CONTI','ANCATCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLCATSCM),'CONTI','ANCATSCM');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANCODICE),'CONTI','ANCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODPAG),'CONTI','ANCODPAG');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANDESCR2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLDESCRI),'CONTI','ANDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANINDIR2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLINDIRI),'CONTI','ANINDIRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLLOCALI),'CONTI','ANLOCALI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLNAZION),'CONTI','ANNAZION');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLNUMCEL),'CONTI','ANNUMCEL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODLIS),'CONTI','ANNUMLIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLPROVIN),'CONTI','ANPROVIN');
        +","+cp_NullLink(cp_ToStrODBC("C"),'CONTI','ANTIPCON');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'AN___CAP',this.w_CL___CAP,'ANCATCOM',this.w_CLCATCOM,'ANCATSCM',this.w_CLCATSCM,'ANCODICE',this.oParentObject.w_ANCODICE,'ANCODPAG',this.w_CLCODPAG,'ANDESCR2'," ",'ANDESCRI',this.w_CLDESCRI,'ANINDIR2'," ",'ANINDIRI',this.w_CLINDIRI,'ANLOCALI',this.w_CLLOCALI,'ANNAZION',this.w_CLNAZION,'ANNUMCEL',this.w_CLNUMCEL)
        insert into (i_cTable) (AN___CAP,ANCATCOM,ANCATSCM,ANCODICE,ANCODPAG,ANDESCR2,ANDESCRI,ANINDIR2,ANINDIRI,ANLOCALI,ANNAZION,ANNUMCEL,ANNUMLIS,ANPROVIN,ANTIPCON &i_ccchkf. );
           values (;
             this.w_CL___CAP;
             ,this.w_CLCATCOM;
             ,this.w_CLCATSCM;
             ,this.oParentObject.w_ANCODICE;
             ,this.w_CLCODPAG;
             ," ";
             ,this.w_CLDESCRI;
             ," ";
             ,this.w_CLINDIRI;
             ,this.w_CLLOCALI;
             ,this.w_CLNAZION;
             ,this.w_CLNUMCEL;
             ,this.w_CLCODLIS;
             ,this.w_CLPROVIN;
             ,"C";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into CONTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AFFLINTR ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','AFFLINTR');
        +",AN1SCONT ="+cp_NullLink(cp_ToStrODBC(this.w_CLSCONT1),'CONTI','AN1SCONT');
        +",AN2SCONT ="+cp_NullLink(cp_ToStrODBC(this.w_CLSCONT2),'CONTI','AN2SCONT');
        +",AN__NOTE ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','AN__NOTE');
        +",AN_EMAIL ="+cp_NullLink(cp_ToStrODBC(this.w_CL_EMAIL),'CONTI','AN_EMAIL');
        +",AN_EMPEC ="+cp_NullLink(cp_ToStrODBC(this.w_CL_EMPEC),'CONTI','AN_EMPEC');
        +",AN_SESSO ="+cp_NullLink(cp_ToStrODBC("M"),'CONTI','AN_SESSO');
        +",ANBOLFAT ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANBOLFAT');
        +",ANCATCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLCATCON),'CONTI','ANCATCON');
        +",ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
        +",ANCODBAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLCODBAN),'CONTI','ANCODBAN');
        +",ANCODFIS ="+cp_NullLink(cp_ToStrODBC(this.w_CLCODFIS),'CONTI','ANCODFIS');
        +",ANCODIVA ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'CONTI','ANCODIVA');
        +",ANCODLIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLCODLIN),'CONTI','ANCODLIN');
        +",ANCODSTU ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODSTU),'CONTI','ANCODSTU');
        +",ANCONCON ="+cp_NullLink(cp_ToStrODBC(this.w_CONCON),'CONTI','ANCONCON');
        +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLMASCON),'CONTI','ANCONSUP');
        +",ANDTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CLDTINVA),'CONTI','ANDTINVA');
        +",ANDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CLDTOBSO),'CONTI','ANDTOBSO');
        +",ANFLAACC ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLAACC');
        +",ANFLBLVE ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLBLVE');
        +",ANFLCODI ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLCODI');
        +",ANFLCONA ="+cp_NullLink(cp_ToStrODBC("U"),'CONTI','ANFLCONA');
        +",ANFLESIG ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANFLESIG');
        +",ANFLFIDO ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLFIDO),'CONTI','ANFLFIDO');
        +",ANFLGAVV ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANFLGAVV');
        +",ANFLGCON ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLGCON');
        +",ANFLRAGG ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLRAGG');
        +",ANGESCON ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANGESCON');
        +",ANIBARID ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANIBARID');
        +",ANINDWEB ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANINDWEB');
        +",ANLOCNAS ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANLOCNAS');
        +",ANNUMCOR ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANNUMCOR');
        +",ANPARIVA ="+cp_NullLink(cp_ToStrODBC(this.w_CLPARIVA),'CONTI','ANPARIVA');
        +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANPARTSN');
        +",ANPERFIS ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANPERFIS');
        +",ANPREBOL ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANPREBOL');
        +",ANPRONAS ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANPRONAS');
        +",ANSCORPO ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANSCORPO');
        +",ANTELEFO ="+cp_NullLink(cp_ToStrODBC(this.w_CLTELEFO),'CONTI','ANTELEFO');
        +",ANTELFAX ="+cp_NullLink(cp_ToStrODBC(this.w_CLTELFAX),'CONTI','ANTELFAX');
        +",ANTIPFAT ="+cp_NullLink(cp_ToStrODBC("R"),'CONTI','ANTIPFAT');
        +",ANTIPRIF ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANTIPRIF');
        +",ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANTIPSOT');
        +",ANVALFID ="+cp_NullLink(cp_ToStrODBC(this.w_CLVALFID),'CONTI','ANVALFID');
        +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CONTI','UTCC');
        +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'CONTI','UTDC');
            +i_ccchkf ;
        +" where ";
            +"ANTIPCON = "+cp_ToStrODBC("C");
            +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_ANCODICE);
               )
      else
        update (i_cTable) set;
            AFFLINTR = " ";
            ,AN1SCONT = this.w_CLSCONT1;
            ,AN2SCONT = this.w_CLSCONT2;
            ,AN__NOTE = " ";
            ,AN_EMAIL = this.w_CL_EMAIL;
            ,AN_EMPEC = this.w_CL_EMPEC;
            ,AN_SESSO = "M";
            ,ANBOLFAT = "N";
            ,ANCATCON = this.oParentObject.w_CLCATCON;
            ,ANCLIPOS = "S";
            ,ANCODBAN = this.oParentObject.w_CLCODBAN;
            ,ANCODFIS = this.w_CLCODFIS;
            ,ANCODIVA = SPACE(5);
            ,ANCODLIN = this.oParentObject.w_CLCODLIN;
            ,ANCODSTU = this.w_ANCODSTU;
            ,ANCONCON = this.w_CONCON;
            ,ANCONSUP = this.oParentObject.w_CLMASCON;
            ,ANDTINVA = this.w_CLDTINVA;
            ,ANDTOBSO = this.w_CLDTOBSO;
            ,ANFLAACC = " ";
            ,ANFLBLVE = " ";
            ,ANFLCODI = " ";
            ,ANFLCONA = "U";
            ,ANFLESIG = "N";
            ,ANFLFIDO = this.w_ANFLFIDO;
            ,ANFLGAVV = "N";
            ,ANFLGCON = " ";
            ,ANFLRAGG = " ";
            ,ANGESCON = "N";
            ,ANIBARID = " ";
            ,ANINDWEB = " ";
            ,ANLOCNAS = " ";
            ,ANNUMCOR = " ";
            ,ANPARIVA = this.w_CLPARIVA;
            ,ANPARTSN = "S";
            ,ANPERFIS = "N";
            ,ANPREBOL = "N";
            ,ANPRONAS = " ";
            ,ANSCORPO = "S";
            ,ANTELEFO = this.w_CLTELEFO;
            ,ANTELFAX = this.w_CLTELFAX;
            ,ANTIPFAT = "R";
            ,ANTIPRIF = " ";
            ,ANTIPSOT = " ";
            ,ANVALFID = this.w_CLVALFID;
            ,UTCC = i_CODUTE;
            ,UTDC = SetInfoDate( g_CALUTD );
            &i_ccchkf. ;
         where;
            ANTIPCON = "C";
            and ANCODICE = this.oParentObject.w_ANCODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_MDCODCLI)
      * --- Write into CLI_VEND
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLTIPCON ="+cp_NullLink(cp_ToStrODBC("C"),'CLI_VEND','CLTIPCON');
        +",CLCODCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANCODICE),'CLI_VEND','CLCODCON');
            +i_ccchkf ;
        +" where ";
            +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_MDCODCLI);
               )
      else
        update (i_cTable) set;
            CLTIPCON = "C";
            ,CLCODCON = this.oParentObject.w_ANCODICE;
            &i_ccchkf. ;
         where;
            CLCODCLI = this.oParentObject.w_MDCODCLI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Notifica il corretto inserimento Cliente
    this.oparentobject.oparentobject.w_NOTIFICA = "S"
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Cliente AHR con Flag POS
    if NVL(ANTIPCON, " ")="C"
      this.w_CODICE = ANCODICE
      this.w_ANCLIPOS = NVL(ANCLIPOS, " ")
      if this.w_OKTROV=.F.
        this.w_oPART = this.w_oMESS.addmsgpartNL("(%1-%2)%0Il cliente negozio verr� associato a quest'ultimo")
        this.w_oPART.addParam(ALLTRIM(this.w_CODICE))     
        this.w_oPART.addParam(ALLTRIM(NVL(ANDESCRI,"")))     
        this.w_oMESS.ah_ErrorMsg("!")     
        this.oParentObject.w_ANCODICE = this.w_CODICE
        this.w_OKTROV = .T.
        if this.w_ANCLIPOS<>"S"
          * --- Se Cliente no POS inserisce il Flag
          * --- Write into CONTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
                +i_ccchkf ;
            +" where ";
                +"ANTIPCON = "+cp_ToStrODBC("C");
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_ANCODICE);
                   )
          else
            update (i_cTable) set;
                ANCLIPOS = "S";
                &i_ccchkf. ;
             where;
                ANTIPCON = "C";
                and ANCODICE = this.oParentObject.w_ANCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CLI_VEND'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CLI_VEND'
    this.cWorkTables[4]='MASTRI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
