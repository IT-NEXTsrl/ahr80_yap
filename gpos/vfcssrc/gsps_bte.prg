* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bte                                                        *
*              Vendita negozio - dati testata                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_179]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-14                                                      *
* Last revis.: 2013-09-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bte",oParentObject,m.pOPER)
return(i_retval)

define class tgsps_bte as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_RECO = 0
  w_MESS = space(10)
  w_AGGRIG = .f.
  w_VARLIS = .f.
  w_VARCLI = .f.
  w_VARSCO = .f.
  w_NabsRow = 0
  w_NRelRow = 0
  w_AGGSCO = space(1)
  w_APPO = space(10)
  w_APPO1 = space(10)
  w_TOTDOC = 0
  w_GSPS_MVD = .NULL.
  w_PRETOTDOC = 0
  w_MESSAGE = space(10)
  w_PA3KCORR = space(5)
  w_PA3KRICF = space(5)
  w_PA3K_CAU = space(5)
  w_PA3KIMPO = space(1)
  w_PA3KDFIS = space(1)
  w_IAMINFAT = 0
  w_IAMINCOR = 0
  w_IAMINFCO = 0
  w_IALORFCO = space(1)
  w_ANNO = space(4)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  CLI_VEND_idx=0
  LISTINI_idx=0
  TAB_SCON_idx=0
  PAG_AMEN_idx=0
  SALDIART_idx=0
  DAT_IVAN_idx=0
  PAR_VDET_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna dati Vendita Negozio al cambio dei dati di Testata (da GSPS_MVD)
    * --- pOPER - Cambio: 
    *     D = Data Registrazione ; 
    *     V = Causale di Vendita ; 
    *     F = Fidelity Card ; 
    *     C = Cliente ; 
    *     L = Listino ;
    *     M = Magazzino
    this.w_GSPS_MVD = this.oParentObject
    this.w_GSPS_MVD.Exec_Select("_Tmp_Righe","Count(*) As Conta ","Not Empty(Nvl(t_MDCODICE,' ')) And Not Deleted()","","","")     
    this.w_RECO = Nvl(_Tmp_Righe.Conta,0)
    Use in _Tmp_Righe
    if this.pOper = "D" And this.oParentObject.o_MDDATREG < g_DATESC And this.oParentObject.w_MDDATREG >= g_DATESC And g_FLESSC = "S" And (this.w_GSPS_MVD.cFunction="Edit" Or this.w_RECO<>0)
      if this.w_GSPS_MVD.cFunction="Edit"
        this.w_MESS = "Documento registrato in data antecedente all'attivazione del check esplicita sconti%0Impossibile impostare una data maggiore o uguale a %1"
      else
        this.w_MESS = "Impostata data maggiore a data attivazione del check esplicita sconti%0Impossibile impostare una data maggiore o uguale a %1 se gi� presenti righe sul dettaglio"
      endif
      ah_ErrorMsg(this.w_MESS,"!","", DTOC(g_DATESC) )
      this.oParentObject.w_MDDATREG = this.oParentObject.o_MDDATREG
      i_retcode = 'stop'
      return
    endif
    this.w_AGGRIG = .F.
    this.w_VARLIS = .F.
    this.w_VARCLI = .F.
    this.w_VARSCO = .F.
    this.w_AGGSCO = " "
    do case
      case this.pOPER="L"
        * --- Cambio Listino
        if this.oParentObject.o_MDCODLIS = this.oParentObject.w_MDCODLIS 
          * --- Nel caso in cui riseleziono lo stesso codice listino dallo zoom 
          *     (in questo caso verrebbe notificato comunque w_MDCODICE Changed) mi fermo
          i_retcode = 'stop'
          return
        endif
        if NOT EMPTY(this.oParentObject.w_MDCODLIS)
          this.w_AGGRIG = .T.
        else
          if NOT EMPTY(this.oParentObject.o_MDCODLIS) AND (this.w_RECO>1 OR (this.w_RECO=1 AND NOT EMPTY(this.oParentObject.w_MDCODICE))) AND NOT EMPTY(this.oParentObject.w_MDCODCLI)
            this.w_APPO = this.oParentObject.o_MDCODLIS
            this.w_APPO1 = " "
            * --- Read from LISTINI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LISTINI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LSFLSCON"+;
                " from "+i_cTable+" LISTINI where ";
                    +"LSCODLIS = "+cp_ToStrODBC(this.w_APPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LSFLSCON;
                from (i_cTable) where;
                    LSCODLIS = this.w_APPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO1 = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_APPO1="S"
              this.w_MESS = "Aggiorno gli sconti secondo la tabella sconti/maggiorazioni?%0Se le nuove impostazioni non trovano sconti validi,%0verranno mantenuti quelli precedentemente calcolati"
              this.w_VARSCO = ah_YesNo(this.w_MESS)
              this.w_AGGRIG = this.w_VARSCO
            endif
          endif
        endif
      case this.pOPER="V"
        * --- Cambio Modalita' di Vendita
        this.oParentObject.w_MDTIPDOC = IIF(this.oParentObject.w_MDTIPCHI="DT",this.oParentObject.w_DOCDDT,IIF(this.oParentObject.w_MDTIPCHI="RS", this.oParentObject.w_DOCRIF, IIF(this.oParentObject.w_MDTIPCHI="FF", this.oParentObject.w_DOCFAF,IIF(this.oParentObject.w_MDTIPCHI="FI",this.oParentObject.w_DOCFAT,IIF(this.oParentObject.w_MDTIPCHI="RF",this.oParentObject.w_DOCRIC,this.oParentObject.w_DOCCOR)))))
        * --- Solo per Fattura Fiscale / Corrispettivi
        this.oParentObject.w_MDTIPCOR = IIF(this.oParentObject.w_MDTIPCHI = "FF", this.oParentObject.w_DOCCOR, IIF(this.oParentObject.w_MDTIPCHI $ "NS-ES-BV", this.oParentObject.w_MDTIPDOC, SPACE(5)))
        if NOT EMPTY(this.oParentObject.w_MDTIPDOC) AND this.oParentObject.o_MDTIPDOC<>this.oParentObject.w_MDTIPDOC
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDPRODOC,TDFLIMPA,TDFLRISC,TDFLANAL,TDFLCOMM"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MDTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDPRODOC,TDFLIMPA,TDFLRISC,TDFLANAL,TDFLCOMM;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_MDTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_MDPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
            this.oParentObject.w_FLIMPA = NVL(cp_ToDate(_read_.TDFLIMPA),cp_NullValue(_read_.TDFLIMPA))
            this.oParentObject.w_FLRISC = NVL(cp_ToDate(_read_.TDFLRISC),cp_NullValue(_read_.TDFLRISC))
            this.oParentObject.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
            this.oParentObject.w_FLGCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_CLIVEN) AND EMPTY(this.oParentObject.w_MDCODFID) AND this.oParentObject.w_MDCODCLI<>this.oParentObject.w_CLIVEN
          * --- Se Vendita associata a Cliente imposta il nuovo Cliente
          this.oParentObject.w_MDCODCLI = this.oParentObject.w_CLIVEN
          * --- Read from CLI_VEND
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO"+;
              " from "+i_cTable+" CLI_VEND where ";
                  +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_MDCODCLI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO;
              from (i_cTable) where;
                  CLCODCLI = this.oParentObject.w_MDCODCLI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCLI = NVL(cp_ToDate(_read_.CLDESCRI),cp_NullValue(_read_.CLDESCRI))
            this.oParentObject.w_SCOCL1 = NVL(cp_ToDate(_read_.CLSCONT1),cp_NullValue(_read_.CLSCONT1))
            this.oParentObject.w_SCOCL2 = NVL(cp_ToDate(_read_.CLSCONT2),cp_NullValue(_read_.CLSCONT2))
            this.oParentObject.w_PAGCLI = NVL(cp_ToDate(_read_.CLCODPAG),cp_NullValue(_read_.CLCODPAG))
            this.oParentObject.w_CATSCC = NVL(cp_ToDate(_read_.CLCATSCM),cp_NullValue(_read_.CLCATSCM))
            this.oParentObject.w_VALFID = NVL(cp_ToDate(_read_.CLVALFID),cp_NullValue(_read_.CLVALFID))
            this.oParentObject.w_FIDUTI = NVL(cp_ToDate(_read_.CLFIDUTI),cp_NullValue(_read_.CLFIDUTI))
            this.oParentObject.w_CATCOM = NVL(cp_ToDate(_read_.CLCATCOM),cp_NullValue(_read_.CLCATCOM))
            this.oParentObject.w_LISCLI = NVL(cp_ToDate(_read_.CLCODLIS),cp_NullValue(_read_.CLCODLIS))
            this.oParentObject.w_TELFAX = NVL(cp_ToDate(_read_.CLTELFAX),cp_NullValue(_read_.CLTELFAX))
            this.oParentObject.w_EMAIL = NVL(cp_ToDate(_read_.CL_EMAIL),cp_NullValue(_read_.CL_EMAIL))
            this.oParentObject.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCON),cp_NullValue(_read_.CLCODCON))
            this.oParentObject.w_FLFIDO = NVL(cp_ToDate(_read_.CLFLFIDO),cp_NullValue(_read_.CLFLFIDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_VARCLI = .T.
        else
          this.oParentObject.w_MDCODLIS = IIF(NOT EMPTY(this.oParentObject.w_LISVEN), this.oParentObject.w_LISVEN, this.oParentObject.w_MDCODLIS)
          this.oParentObject.w_MDCODPAG = IIF(NOT EMPTY(this.oParentObject.w_PAGVEN), this.oParentObject.w_PAGVEN, this.oParentObject.w_MDCODPAG)
        endif
        if this.w_GSPS_MVD.cFunction="Load" AND NOT (this.oParentObject.w_MDPRD<>"NN" AND this.oParentObject.w_MDTIPCHI $ "RF-FI-FF-RS-DT") 
 
          * --- Se corrispettivo Azzera Numero Documento (vene assegnato in differita)
          this.oParentObject.w_MDNUMDOC = 0
        endif
        this.w_AGGRIG = .T.
        * --- Al cambio della modalit� di vendit� cmq chiedo 
        *     se si vuole aggioranre i prezzi di riga
        this.w_VARLIS = .T.
      case this.pOPER="D"
        * --- Cambio data Registrazione
        if NOT EMPTY(this.oParentObject.w_MDCODCLI)
          this.w_AGGRIG = .T.
        endif
      case this.pOPER="F"
        * --- Cambio Fidelity Card
        this.w_AGGRIG = .T.
        this.oParentObject.w_MDCODCLI = IIF(EMPTY(this.oParentObject.w_MDCODFID), Space(15), this.oParentObject.w_CLIFID)
        if NOT EMPTY(this.oParentObject.w_MDCODCLI)
          * --- Read from CLI_VEND
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLSCONT1,CLSCONT2,CLCODPAG,CLCODCON,CLCODLIS"+;
              " from "+i_cTable+" CLI_VEND where ";
                  +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_MDCODCLI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLSCONT1,CLSCONT2,CLCODPAG,CLCODCON,CLCODLIS;
              from (i_cTable) where;
                  CLCODCLI = this.oParentObject.w_MDCODCLI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_SCOCL1 = NVL(cp_ToDate(_read_.CLSCONT1),cp_NullValue(_read_.CLSCONT1))
            this.oParentObject.w_SCOCL2 = NVL(cp_ToDate(_read_.CLSCONT2),cp_NullValue(_read_.CLSCONT2))
            this.oParentObject.w_PAGCLI = NVL(cp_ToDate(_read_.CLCODPAG),cp_NullValue(_read_.CLCODPAG))
            this.oParentObject.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCON),cp_NullValue(_read_.CLCODCON))
            this.oParentObject.w_LISCLI = NVL(cp_ToDate(_read_.CLCODLIS),cp_NullValue(_read_.CLCODLIS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_MDSCOCL1 = this.oParentObject.w_SCOCL1
          this.oParentObject.w_MDSCOCL2 = this.oParentObject.w_SCOCL2
          this.w_VARCLI = .T.
        endif
      case this.pOPER="C"
        * --- Cambio Cliente
        * --- Lancio batch GSPS_BPZ('C') per pulitura transitorio delle righe derivanti da promozioni
        * --- Memorizza totale documento prima delle promozioni
        this.w_PRETOTDOC = this.oParentObject.w_MDTOTDOC
        if this.w_GSPS_MVD.oPgFrm.ActivePage=2
          this.w_GSPS_MVD.NotifyEvent("Promozioni")     
        else
          this.w_GSPS_MVD.NotifyEvent("PromCli")     
        endif
        this.oParentObject.w_MDSCOCL1 = this.oParentObject.w_SCOCL1
        this.oParentObject.w_MDSCOCL2 = this.oParentObject.w_SCOCL2
        this.w_AGGRIG = .T.
        this.w_VARCLI = .T.
        if g_AIFT="S"
          * --- Se l'importo � maggiore di 3000 euro, cambia la causale
          if inlist(this.oParentObject.w_MDTIPCHI, "NS", "BV", "ES", "RF")
            * --- Nessuna stampa, Brogliaccio di vendita, Emissione scontrino
            this.w_ANNO = Alltrim(str(year(this.oParentObject.w_MDDATREG)))
            * --- Read from DAT_IVAN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "IAMINFAT,IAMINCOR,IAMINFCO,IALORFCO"+;
                " from "+i_cTable+" DAT_IVAN where ";
                    +"IACODAZI = "+cp_ToStrODBC(i_codazi);
                    +" and IA__ANNO = "+cp_ToStrODBC(this.w_ANNO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                IAMINFAT,IAMINCOR,IAMINFCO,IALORFCO;
                from (i_cTable) where;
                    IACODAZI = i_codazi;
                    and IA__ANNO = this.w_ANNO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_IAMINFAT = NVL(cp_ToDate(_read_.IAMINFAT),cp_NullValue(_read_.IAMINFAT))
              this.w_IAMINCOR = NVL(cp_ToDate(_read_.IAMINCOR),cp_NullValue(_read_.IAMINCOR))
              this.w_IAMINFCO = NVL(cp_ToDate(_read_.IAMINFCO),cp_NullValue(_read_.IAMINFCO))
              this.w_IALORFCO = NVL(cp_ToDate(_read_.IALORFCO),cp_NullValue(_read_.IALORFCO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from PAR_VDET
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_VDET_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PA3KCORR,PA3KRICF,PA3KIMPO,PA3KDFIS"+;
                " from "+i_cTable+" PAR_VDET where ";
                    +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                    +" and PACODNEG = "+cp_ToStrODBC(this.oParentObject.w_MDCODNEG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PA3KCORR,PA3KRICF,PA3KIMPO,PA3KDFIS;
                from (i_cTable) where;
                    PACODAZI = i_codazi;
                    and PACODNEG = this.oParentObject.w_MDCODNEG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PA3KCORR = NVL(cp_ToDate(_read_.PA3KCORR),cp_NullValue(_read_.PA3KCORR))
              this.w_PA3KRICF = NVL(cp_ToDate(_read_.PA3KRICF),cp_NullValue(_read_.PA3KRICF))
              this.w_PA3KIMPO = NVL(cp_ToDate(_read_.PA3KIMPO),cp_NullValue(_read_.PA3KIMPO))
              this.w_PA3KDFIS = NVL(cp_ToDate(_read_.PA3KDFIS),cp_NullValue(_read_.PA3KDFIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_PA3KIMPO $ "WB" and this.oParentObject.w_MDTOTDOC >= this.w_IAMINCOR
              this.w_PA3K_CAU = IIF(this.oParentObject.w_MDTIPCHI="RF", this.w_PA3KRICF, this.w_PA3KCORR)
              if EMPTY(this.oParentObject.w_MDCODCLI)
                =setvaluelinked("M", this.w_GSPS_MVD, "w_MDTIPDOC", this.oParentObject.w_zMDTIPDOC)
                this.oParentObject.w_EDTIPDOC = False
              else
                this.oParentObject.w_EDTIPDOC = True
                if this.oParentObject.w_MDTIPDOC<>this.w_PA3K_CAU and not empty(nvl(this.w_PA3K_CAU,""))
                  this.oParentObject.w_zMDTIPDOC = this.oParentObject.w_MDTIPDOC
                  this.oParentObject.w_oMDTIPDOC = this.w_PA3K_CAU
                  this.w_GSPS_MVD.mEnableControls()     
                  =setvaluelinked("M", this.w_GSPS_MVD, "w_MDTIPDOC", this.w_PA3K_CAU)
                  this.w_MESSAGE = "Operazione maggiori di 3.000 euro%0Il tipo documento e stato modificato."
                  ah_errormsg(this.w_MESSAGE)
                endif
              endif
            endif
            this.oParentObject.w_MDTIPCOR = IIF(this.oParentObject.w_MDTIPCHI = "FF", this.oParentObject.w_DOCCOR, IIF(this.oParentObject.w_MDTIPCHI $ "NS-ES-BV", this.oParentObject.w_MDTIPDOC, SPACE(5)))
          endif
        endif
      case this.pOPER="M"
        this.w_GSPS_MVD.MarkPos()     
        * --- Se cambio il magazzino in testata lo devo cambiare su tutte le righe
        this.w_GSPS_MVD.FirstRow()     
        do while Not this.w_GSPS_MVD.Eof_Trs()
          this.w_GSPS_MVD.SetRow()     
          this.w_GSPS_MVD.Set("w_MDMAGRIG" , this.oParentObject.w_MDCODMAG , .f. , .t.)     
          this.w_GSPS_MVD.Set("w_MDMAGSAL" , IIF(this.oParentObject.w_MDTIPRIG="R" AND this.oParentObject.w_MDFLCASC<>" ", this.oParentObject.w_MDCODMAG, SPACE(5)) , .f. , .t.)     
          * --- Svuoto l'Ubiacazione.
          *     ATTENZIONE: il campo non legge nessuna variabile nel Link.
          *     Nel caso si vadano ad aggiungere letture nel Link di MDCODUBI, 
          *     devono essere gestite in questo punto (con EcpDrop)
          this.w_GSPS_MVD.Set("w_MDCODUBI" , Space(20), .f., .t.)     
          * --- ripetiamo il calcolo svolto sulla gestione, anche se codubi � sempre vuota..
          this.w_GSPS_MVD.Set("w_MDLOTMAG" , iif( Empty( this.oParentObject.w_MDCODLOT ) And Empty( this.oParentObject.w_MDCODUBI ) , SPACE(5) , this.oParentObject.w_MDMAGSAL ))     
          * --- Passo alla prossima riga non cancellata..
          this.w_GSPS_MVD.NextRow()     
        enddo
        * --- Se cambio il magazzino in testata visto che l'aggiorno anche sulle righe mi riposiziono
        this.w_GSPS_MVD.RePos()     
    endcase
    if this.w_VARCLI
      * --- Se variato Cliente, ricalcola il Pagamento e il Listino in testata
      this.oParentObject.w_MDCODLIS = IIF(NOT EMPTY(this.oParentObject.w_LISCLI), this.oParentObject.w_LISCLI, IIF(EMPTY(this.oParentObject.w_LISVEN), this.oParentObject.w_MDCODLIS, this.oParentObject.w_LISVEN))
      this.oParentObject.w_MDCODPAG = IIF(NOT EMPTY(this.oParentObject.w_PAGCLI), this.oParentObject.w_PAGCLI, IIF(EMPTY(this.oParentObject.w_PAGVEN), this.oParentObject.w_MDCODPAG, this.oParentObject.w_PAGVEN))
    endif
    if this.oParentObject.w_MDCODLIS<>this.oParentObject.o_MDCODLIS AND NOT EMPTY(this.oParentObject.w_MDCODLIS)
      * --- Read from LISTINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LISTINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LSIVALIS,LSVALLIS,LSFLSCON"+;
          " from "+i_cTable+" LISTINI where ";
              +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_MDCODLIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LSIVALIS,LSVALLIS,LSFLSCON;
          from (i_cTable) where;
              LSCODLIS = this.oParentObject.w_MDCODLIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_LISIVA = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
        this.oParentObject.w_LISVAL = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
        this.oParentObject.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_VARLIS = .T.
    endif
    if this.oParentObject.w_MDCODPAG<>this.oParentObject.o_MDCODPAG AND NOT EMPTY(this.oParentObject.w_MDCODPAG)
      * --- Read from PAG_AMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PASCONTO"+;
          " from "+i_cTable+" PAG_AMEN where ";
              +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_MDCODPAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PASCONTO;
          from (i_cTable) where;
              PACODICE = this.oParentObject.w_MDCODPAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_SCOPAG = NVL(cp_ToDate(_read_.PASCONTO),cp_NullValue(_read_.PASCONTO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Per tenere il valore del totale documento prima del ricalcolo dovuto alla mCalc
    this.w_TOTDOC = this.oParentObject.w_MDTOTDOC
    * --- Lettura dei Link
    this.w_GSPS_MVD.mCalc(.T.)     
    * --- Se in caricamento, se modifico il cliente, se la mCalc qui sopra modifica il totale documento, se il totale � variato dopo l'applicazione delle promozioni
    *     riporto tutto sui contanti
    if this.w_GSPS_MVD.cFunction="Load" AND this.pOPER="C" AND (this.oParentObject.w_MDTOTDOC<>this.w_TOTDOC OR this.oParentObject.w_MDTOTDOC<>this.w_PRETOTDOC)
      * --- Riazzero i Pagamenti e riporto il totale sui contanti solo se in caricamento
      *     poich� la mCalc qui sopra fa ricalcolare il totale documento.
      this.oParentObject.w_MDIMPPRE = 0
      this.oParentObject.w_MDACCPRE = 0
      this.oParentObject.w_MDPAGCON = cp_Round(this.oParentObject.w_MDTOTDOC, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MDPAGASS = 0
      this.oParentObject.w_MDPAGCAR = 0
      this.oParentObject.w_MDPAGFIN = 0
      this.oParentObject.w_MDPAGCLI = 0
      this.oParentObject.w_MDIMPABB = 0
    endif
    if this.w_AGGRIG AND this.w_RECO>0
      do case
        case this.w_VARLIS
          this.w_MESS = "Aggiorno i prezzi sulle righe in base al listino impostato?%0Nel caso il listino non sia gestito a sconti,%0questi verranno ricalcolati da tabella sconti/maggiorazioni"
          this.w_AGGRIG = ah_YesNo(this.w_MESS)
          if this.w_AGGRIG AND this.oParentObject.w_SCOLIS="S"
            this.w_AGGSCO = "S"
          endif
        case this.pOPER $ "CDF"
          * --- Solo se modifico la data registrazione o il cliente.
          this.w_MESS = "Aggiorno anche i prezzi sulle righe?"
          this.w_AGGRIG = ah_YesNo(this.w_MESS)
      endcase
      if this.w_AGGRIG
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna dati di Riga
    this.w_GSPS_MVD.MarkPos()     
    this.w_GSPS_MVD.FirstRow()     
    do while Not this.w_GSPS_MVD.Eof_Trs()
      if this.w_GSPS_MVD.FullRow()
        this.w_GSPS_MVD.SetRow()     
        this.w_GSPS_MVD.SaveDependsOn()     
        if this.w_VARSCO=.F.
          * --- Forzo il ricalcolo se nel GSVE_BCD viene trovato un prezzo
          this.oParentObject.w_LIPREZZO = 0
          this.oParentObject.o_LIPREZZO = 0
          if this.pOPER $ "CDF"
            * --- Il contratto potrebbe non esere piu' valido
            this.oParentObject.w_MDCONTRA = SPACE(15)
            * --- Aggiorna anche gli Sconti
            this.oParentObject.w_OFLSCO = "C"
          endif
          this.oParentObject.w_OFLSCO = IIF(this.w_AGGSCO="S", "C", this.oParentObject.w_OFLSCO)
          this.oParentObject.w_MDFLCASC = IIF(this.oParentObject.w_MDFLRESO="S", "+", "-")
          this.oParentObject.w_MDMAGSAL = IIF(this.oParentObject.w_MDTIPRIG="R" AND this.oParentObject.w_MDFLCASC<>" ", this.oParentObject.w_MDCODMAG, SPACE(5))
          this.oParentObject.w_MDMAGRIG = IIF(this.oParentObject.w_MDTIPRIG="R",this.oParentObject.w_MDCODMAG,Space(20) )
          this.w_GSPS_MVD.NotifyEvent("Ricalcola")     
        else
          * --- Varia solo gli Sconti se non presente un contratto su riga
          if this.oParentObject.w_LISCON<>1
            * --- Select from TAB_SCON
            i_nConn=i_TableProp[this.TAB_SCON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TAB_SCON_idx,2],.t.,this.TAB_SCON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_SCON ";
                  +" where TSCATCLI="+cp_ToStrODBC(this.oParentObject.w_CATSCC)+" AND TSCATARR="+cp_ToStrODBC(this.oParentObject.w_CATSCA)+" AND TSDATINI<="+cp_ToStrODBC(this.oParentObject.w_MDDATREG)+"";
                  +" order by TSDATINI";
                   ,"_Curs_TAB_SCON")
            else
              select * from (i_cTable);
               where TSCATCLI=this.oParentObject.w_CATSCC AND TSCATARR=this.oParentObject.w_CATSCA AND TSDATINI<=this.oParentObject.w_MDDATREG;
               order by TSDATINI;
                into cursor _Curs_TAB_SCON
            endif
            if used('_Curs_TAB_SCON')
              select _Curs_TAB_SCON
              locate for 1=1
              do while not(eof())
              if CP_TODATE(_Curs_TAB_SCON.TSDATFIN)>=this.oParentObject.w_MDDATREG OR EMPTY(CP_TODATE(_Curs_TAB_SCON.TSDATFIN))
                this.oParentObject.w_MDSCONT1 = IIF(this.oParentObject.w_NUMSCO>0, _Curs_TAB_SCON.TSSCONT1, 0)
                this.oParentObject.w_MDSCONT2 = IIF(this.oParentObject.w_NUMSCO>1, _Curs_TAB_SCON.TSSCONT2, 0)
                this.oParentObject.w_MDSCONT3 = IIF(this.oParentObject.w_NUMSCO>2, _Curs_TAB_SCON.TSSCONT3, 0)
                this.oParentObject.w_MDSCONT4 = IIF(this.oParentObject.w_NUMSCO>3, _Curs_TAB_SCON.TSSCONT4, 0)
              endif
                select _Curs_TAB_SCON
                continue
              enddo
              use
            endif
          endif
          this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE - this.oParentObject.w_VALRIG
          this.oParentObject.w_MDTOTVEN = this.oParentObject.w_MDTOTVEN - this.oParentObject.w_RIGNET
          this.oParentObject.w_VALRIG = IIF(g_FLESSC="S" And this.oParentObject.w_MDDATREG >= g_DATESC,CAVALRIG(this.oParentObject.w_MDPREZZO,this.oParentObject.w_MDQTAMOV, 0, 0, 0, 0,this.oParentObject.w_DECTOT), CAVALRIG(this.oParentObject.w_MDPREZZO,this.oParentObject.w_MDQTAMOV, this.oParentObject.w_MDSCONT1,this.oParentObject.w_MDSCONT2,this.oParentObject.w_MDSCONT3,this.oParentObject.w_MDSCONT4,this.oParentObject.w_DECTOT) )
          this.oParentObject.w_RIGNET = IIF(this.oParentObject.w_MDFLOMAG="X", this.oParentObject.w_VALRIG,IIF(this.oParentObject.w_MDFLOMAG="I", this.oParentObject.w_VALRIG - CALNET(this.oParentObject.w_VALRIG,this.oParentObject.w_PERIVA,this.oParentObject.w_DECTOT,Space(3),0),0))
          this.oParentObject.w_MDTOTVEN = this.oParentObject.w_MDTOTVEN + this.oParentObject.w_RIGNET
          this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE + this.oParentObject.w_VALRIG
          * --- Notifica che la Riga e' Stata Variata
          SELECT (this.w_GSPS_MVD.cTrsName)
          if EMPTY(i_SRV) AND NOT DELETED()
            REPLACE i_SRV WITH "U"
          endif
          this.oParentObject.o_MDSCONT1 = this.oParentObject.w_MDSCONT1
          this.oParentObject.o_MDSCONT2 = this.oParentObject.w_MDSCONT2
          this.oParentObject.o_MDSCONT3 = this.oParentObject.w_MDSCONT3
        endif
        * --- Risalvo le modifiche sul temporaneo
        *     La SaveRow deve rimanere all'interno dell'If w_GSPS_MVD.FullRow() altrimenti
        *     duplica l'ultima riga piena sulla prima riga in append
        this.w_GSPS_MVD.SaveRow()     
      endif
      * --- Skippo alla riga successiva
      this.w_GSPS_MVD.NextRow()     
    enddo
    * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
    this.w_GSPS_MVD.RePos()     
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CLI_VEND'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='TAB_SCON'
    this.cWorkTables[5]='PAG_AMEN'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='DAT_IVAN'
    this.cWorkTables[8]='PAR_VDET'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_TAB_SCON')
      use in _Curs_TAB_SCON
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
