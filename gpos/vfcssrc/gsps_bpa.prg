* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bpa                                                        *
*              Lettura parametri                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_19]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2002-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bpa",oParentObject)
return(i_retval)

define class tgsps_bpa as StdBatch
  * --- Local variables
  * --- WorkFile variables
  PAR_VDET_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura Parametri (da GSPS_MVD)
    * --- Read from PAR_VDET
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_VDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAALFDOC,PADOCCOR,PADOCRIC,PADOCFAT,PADOCFAF,PADOCRIF,PADOCDDT,PACAURES,PACARKIT,PASCAKIT,PASERFID,PAFLFAFI"+;
        " from "+i_cTable+" PAR_VDET where ";
            +"PACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
            +" and PACODNEG = "+cp_ToStrODBC(this.oParentObject.w_MDCODNEG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAALFDOC,PADOCCOR,PADOCRIC,PADOCFAT,PADOCFAF,PADOCRIF,PADOCDDT,PACAURES,PACARKIT,PASCAKIT,PASERFID,PAFLFAFI;
        from (i_cTable) where;
            PACODAZI = this.oParentObject.w_CODAZI;
            and PACODNEG = this.oParentObject.w_MDCODNEG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_ALFDOC = NVL(cp_ToDate(_read_.PAALFDOC),cp_NullValue(_read_.PAALFDOC))
      this.oParentObject.w_PDOCCOR = NVL(cp_ToDate(_read_.PADOCCOR),cp_NullValue(_read_.PADOCCOR))
      this.oParentObject.w_PDOCRIC = NVL(cp_ToDate(_read_.PADOCRIC),cp_NullValue(_read_.PADOCRIC))
      this.oParentObject.w_PDOCFAT = NVL(cp_ToDate(_read_.PADOCFAT),cp_NullValue(_read_.PADOCFAT))
      this.oParentObject.w_PDOCFAF = NVL(cp_ToDate(_read_.PADOCFAF),cp_NullValue(_read_.PADOCFAF))
      this.oParentObject.w_PDOCRIF = NVL(cp_ToDate(_read_.PADOCRIF),cp_NullValue(_read_.PADOCRIF))
      this.oParentObject.w_PDOCDDT = NVL(cp_ToDate(_read_.PADOCDDT),cp_NullValue(_read_.PADOCDDT))
      this.oParentObject.w_CAURES = NVL(cp_ToDate(_read_.PACAURES),cp_NullValue(_read_.PACAURES))
      this.oParentObject.w_CARKIT = NVL(cp_ToDate(_read_.PACARKIT),cp_NullValue(_read_.PACARKIT))
      this.oParentObject.w_SCAKIT = NVL(cp_ToDate(_read_.PASCAKIT),cp_NullValue(_read_.PASCAKIT))
      this.oParentObject.w_SERFID = NVL(cp_ToDate(_read_.PASERFID),cp_NullValue(_read_.PASERFID))
      this.oParentObject.w_FLFAFI = NVL(cp_ToDate(_read_.PAFLFAFI),cp_NullValue(_read_.PAFLFAFI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_VDET'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
