* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bgf                                                        *
*              Generazione fatture da corrispettivi                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_90]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-16                                                      *
* Last revis.: 2012-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bgf",oParentObject,m.pOper)
return(i_retval)

define class tgsps_bgf as StdBatch
  * --- Local variables
  pOper = space(2)
  Padre = .NULL.
  NC = space(10)
  w_MVANNDOC = space(4)
  w_MVPRD = space(2)
  w_MVALFDOC = space(2)
  w_MVNUMDOC = 0
  w_MDSERIAL = space(10)
  w_MDCODNEG = space(3)
  w_MDDATREG = ctod("  /  /  ")
  w_MDTIPDOC = space(5)
  w_MDCODCLI = space(5)
  w_MDCODPAG = space(5)
  w_MDPRD = space(2)
  w_MDNUMDOC = 0
  w_MDALFDOC = space(2)
  w_MDANNDOC = space(4)
  w_MDCODESE = space(4)
  w_ERRORE = .f.
  w_nRecSel = 0
  w_TIPOPE = space(1)
  w_NUREG = 0
  w_OKREG = 0
  w_NUDOC = 0
  w_CAURES = space(5)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_TCLADOC = space(2)
  w_TANNDOC = space(4)
  w_TPRODOC = space(2)
  w_TALFDOC = space(2)
  w_TDATDOC = ctod("  /  /  ")
  w_ERRORI = .f.
  w_APPO = space(10)
  w_oERRORLOG = .NULL.
  w_MVSERIAL = space(10)
  w_MVRIFODL = space(10)
  w_MVRIFFAD = space(10)
  w_OLDSER = space(10)
  w_STAMPA1 = space(30)
  w_MVCODICE = space(20)
  w_KEYOBS = ctod("  /  /  ")
  w_QUERY = space(50)
  w_REPORT = space(50)
  w_TESTO = space(1)
  w_CODART = space(20)
  w_DESART1 = space(20)
  w_DESSUP1 = space(20)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_TIPCON1 = space(1)
  w_CODCON1 = space(15)
  w_CODICE1 = space(15)
  w_DTOBS1 = ctod("  /  /  ")
  w_codic = space(15)
  w_nmdes = space(40)
  w_ind = space(40)
  w_cap = space(8)
  w_loca = space(30)
  w_prov = space(2)
  w_rif = space(2)
  w_LINGUA = space(3)
  w_TRPRGST = 0
  w_DATA1 = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_serie1 = space(2)
  w_serie2 = space(2)
  w_FLVEAC = space(1)
  w_CATDOC = space(2)
  w_CODESE = space(4)
  w_TIPOIN = space(5)
  w_CLIFOR = space(15)
  w_DATAIN = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_OTES = space(1)
  w_CATCOM = space(3)
  w_CODPAG = space(5)
  w_CATEGO = space(2)
  w_CODZON = space(3)
  w_CODVET = space(5)
  w_NOSBAN = space(5)
  w_CATCON = space(5)
  w_CODDES = space(5)
  w_CODAGE = space(5)
  * --- WorkFile variables
  DOC_MAST_idx=0
  POSTMP_idx=0
  DES_DIVE_idx=0
  KEY_ARTI_idx=0
  OUT_PUTS_idx=0
  CLI_VEND_idx=0
  CONTI_idx=0
  TRADDOCU_idx=0
  TRADARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Generazione Fatture da Corrispettivi (da GSPS_KGF)
    this.w_TIPOPE = "G"
    this.w_NUREG = 0
    this.w_OKREG = 0
    this.w_NUDOC = 0
    this.w_CAURES = this.oParentObject.w_RESCAU
    this.w_ERRORI = .F.
    this.w_CODPAG = this.oParentObject.w_CODPAG
    if EMPTY(this.w_CAURES)
      ah_ErrorMsg("Causale per gestione resi non definita nei parametri P.O.S.%0Impossibile continuare","!","")
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_ALLNSC = IIF(this.oParentObject.w_NUMSC1+this.oParentObject.w_NUMSC2+this.oParentObject.w_NUMSC3+this.oParentObject.w_NUMSC4+this.oParentObject.w_NUMSC5=0, "S", "N")
    this.oParentObject.w_ALLDSC = IIF(EMPTY(this.oParentObject.w_DATSC1) AND EMPTY(this.oParentObject.w_DATSC2) AND EMPTY(this.oParentObject.w_DATSC3) AND EMPTY(this.oParentObject.w_DATSC4) AND EMPTY(this.oParentObject.w_DATSC5), "S", "N")
    this.oParentObject.w_ALLMSC = IIF(EMPTY(this.oParentObject.w_MATSC1) AND EMPTY(this.oParentObject.w_MATSC2) AND EMPTY(this.oParentObject.w_MATSC3) AND EMPTY(this.oParentObject.w_MATSC4) AND EMPTY(this.oParentObject.w_MATSC5), "S", "N")
    this.oParentObject.w_ORMIN1 = IIF(EMPTY(this.oParentObject.w_ORAINI), "00", RIGHT("00"+ALLTRIM(this.oParentObject.w_ORAINI),2))+IIF(EMPTY(this.oParentObject.w_MININI), "00", RIGHT("00"+ALLTRIM(this.oParentObject.w_MININI),2))
    this.oParentObject.w_ORMFI1 = IIF(EMPTY(this.oParentObject.w_ORAFIN), "24", RIGHT("00"+ALLTRIM(this.oParentObject.w_ORAFIN),2))+IIF(EMPTY(this.oParentObject.w_MINFIN), "00", RIGHT("00"+ALLTRIM(this.oParentObject.w_MINFIN),2))
    this.oParentObject.w_ORMINI = IIF(this.oParentObject.w_ORMIN1="0000", SPACE(4), this.oParentObject.w_ORMIN1)
    this.oParentObject.w_ORMFIN = IIF(this.oParentObject.w_ORMFI1="2400", SPACE(4), this.oParentObject.w_ORMFI1)
    this.Padre = this.oParentObject
    * --- Nome cursore collegato allo zoom
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pOper $ "ND-NX"
        * --- Setta Numero Documento
        this.w_MVANNDOC = this.oParentObject.w_ANNDOC
        this.w_MVPRD = this.oParentObject.w_PRODOC
        this.w_MVALFDOC = this.oParentObject.w_ALFDOC
        this.w_MVNUMDOC = 0
        i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
        cp_AskTableProg(this, i_Conn, "PRDOC", "i_CODAZI,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
        this.oParentObject.w_NUMDOC = this.w_MVNUMDOC
      case this.pOper="SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pOper="VZ"
        * --- Visualizza Zoom 
        if EMPTY(this.oParentObject.w_CODCLI)
          ah_ErrorMsg("Codice cliente non definito",,"")
          this.oParentObject.oPgFrm.ActivePage = 1
        else
          this.Padre.NotifyEvent("Interroga")     
          * --- Attiva la pagina 2 automaticamente
          this.oParentObject.oPgFrm.ActivePage = 2
          this.oParentObject.w_SELEZI = "D"
        endif
      case this.pOper $ "CO-AG"
        * --- Generazione Fattura
        if this.pOper $ "AG"
          if NOT this.oParentObject.CheckForm()
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_MDCODNEG = g_CODNEG
        this.w_MDDATREG = this.oParentObject.w_DATDOC
        this.w_MDTIPDOC = this.oParentObject.w_TIPDOC
        this.w_MDCODCLI = this.oParentObject.w_CODCLI
        this.w_MDCODPAG = this.w_CODPAG
        this.w_MDPRD = this.oParentObject.w_PRODOC
        this.w_MDNUMDOC = 0
        this.w_MDALFDOC = this.oParentObject.w_ALFDOC
        this.w_MDANNDOC = this.oParentObject.w_ANNDOC
        this.w_MDCODESE = g_CODESE
        * --- Verifica Esistenza Codice Cliente
        this.w_ERRORE = .F.
        do GSPS_BKC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_ERRORE=.T.
          ah_ErrorMsg("Codice cliente non inserito in anagrafica",,"")
          i_retcode = 'stop'
          return
        endif
        * --- Inizio generazione
        * --- Legge cursore di selezione ...
        * --- Crea tabella Temporanea 
        * --- Create temporary table POSTMP
        i_nIdx=cp_AddTableDef('POSTMP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\GPOS\EXE\QUERY\GSPSTQGF',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.POSTMP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Cicla sui codici selezionati ...
        this.w_nRecSel = 0
        SELECT (this.NC)
        GO TOP
        COUNT FOR xChk=1 TO this.w_nRecSel
        if this.w_nRecSel>0
          SELECT (this.NC)
          GO TOP
          SCAN FOR xChk=1 AND NOT EMPTY(NVL(MDSERIAL,"")) 
          this.w_MDSERIAL = MDSERIAL
          * --- Try
          local bErr_0398AE70
          bErr_0398AE70=bTrsErr
          this.Try_0398AE70()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg("Errore inserimento tabella postmp%0%1",,"",MESSAGE())
          endif
          bTrsErr=bTrsErr or bErr_0398AE70
          * --- End
          SELECT (this.NC)
          ENDSCAN
          VQ_EXEC("..\GPOS\EXE\QUERY\GSPS1QGF.VQR",this,"GENEORDI")
          * --- Elimina Tabella temporanea
          * --- Drop temporary table POSTMP
          i_nIdx=cp_GetTableDefIdx('POSTMP')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('POSTMP')
          endif
          this.w_ERRORE = .F.
          if USED("GENEORDI")
            * --- Generazione Documenti
            SELECT GENEORDI
            if RECCOUNT("GENEORDI") > 0
              this.w_NUREG = 0
              this.w_OKREG = 0
              this.w_NUDOC = 0
              this.w_NUMINI = this.oParentObject.w_NUMDOC
              this.w_NUMFIN = this.oParentObject.w_NUMDOC
              * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
              this.w_oERRORLOG=createobject("AH_ErrorLog")
              do GSPS_BGD with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_OKREG>0
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.w_APPO = "Operazione completata%0N. %1 registrazioni generate%0su %2 registrazioni da generare%0%0Totale %3 documenti generati"
              ah_ErrorMsg(this.w_APPO,"!","", ALLTRIM(STR(this.w_OKREG)), ALLTRIM(STR(this.w_NUREG)), ALLTRIM(STR(this.w_NUDOC)) )
              if this.w_ERRORI
                this.w_oERRORLOG.PrintLog(this,"Errori riscontrati")     
              endif
            endif
          endif
          if USED("GENEORDI")
            SELECT GENEORDI
            USE
          endif
          if used( "__tmp__" )
            select __tmp__
            use
          endif
          * --- Refresh nuovo Progressivo Ordine e Zoom
          this.Padre.NotifyEvent("NewProg")     
          this.Padre.NotifyEvent("Interroga")     
        else
          ah_ErrorMsg("Nessun corrispettivo selezionato",,"")
        endif
    endcase
    if this.pOper $ "ND-NU-CF"
      * --- Verifica se Esistono Registrazioni con Data Maggiore e N.Documento Minore o Uguale
      this.w_TCLADOC = this.oParentObject.w_CLADOC
      this.w_TANNDOC = STR(YEAR(this.oParentObject.w_DATDOC), 4, 0)
      this.w_TPRODOC = this.oParentObject.w_PRODOC
      this.w_TALFDOC = this.oParentObject.w_ALFDOC
      this.w_TDATDOC = this.oParentObject.w_DATDOC
      vq_exec("..\GPOS\EXE\QUERY\GSPS_BS3.VQR",this,"CONTADOC")
      if USED("CONTADOC")
        if RECCOUNT("CONTADOC")>0
          SELECT CONTADOC
          GO TOP
          if NVL(NUMDOC,0)>0 AND NVL(NUMDOC,0)<=this.oParentObject.w_NUMDOC
            ah_ErrorMsg("Esistono documenti con data maggiore e numero minore o uguale",,"")
            this.oParentObject.w_ERRORE=.T.
          endif
        endif
        SELECT CONTADOC
        USE
      endif
      vq_exec("..\GPOS\EXE\QUERY\GSPS1BS3.VQR",this,"CONTADOC")
      if USED("CONTADOC")
        if RECCOUNT("CONTADOC")>0
          SELECT CONTADOC
          GO TOP
          if NVL(NUMDOC,0)>0 AND NVL(NUMDOC,0)>=this.oParentObject.w_NUMDOC
            ah_ErrorMsg("Esistono documenti con data minore o uguale e numero maggiore o uguale",,"")
            this.oParentObject.w_ERRORE=.T.
          endif
        endif
        SELECT CONTADOC
        USE
      endif
    endif
  endproc
  proc Try_0398AE70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into POSTMP
    i_nConn=i_TableProp[this.POSTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.POSTMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.POSTMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MDSERIAL"+",MDDATREG"+",MDCODNEG"+",MDTIPDOC"+",MDCODCLI"+",MDTIPCHI"+",MDPRD"+",MDCODPAG"+",MDNUMDOC"+",MDALFDOC"+",MDANNDOC"+",MDCODESE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MDSERIAL),'POSTMP','MDSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDDATREG),'POSTMP','MDDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDCODNEG),'POSTMP','MDCODNEG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDTIPDOC),'POSTMP','MDTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDCODCLI),'POSTMP','MDCODCLI');
      +","+cp_NullLink(cp_ToStrODBC("FF"),'POSTMP','MDTIPCHI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDPRD),'POSTMP','MDPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDCODPAG),'POSTMP','MDCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDNUMDOC),'POSTMP','MDNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDALFDOC),'POSTMP','MDALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDANNDOC),'POSTMP','MDANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDCODESE),'POSTMP','MDCODESE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MDSERIAL',this.w_MDSERIAL,'MDDATREG',this.w_MDDATREG,'MDCODNEG',this.w_MDCODNEG,'MDTIPDOC',this.w_MDTIPDOC,'MDCODCLI',this.w_MDCODCLI,'MDTIPCHI',"FF",'MDPRD',this.w_MDPRD,'MDCODPAG',this.w_MDCODPAG,'MDNUMDOC',this.w_MDNUMDOC,'MDALFDOC',this.w_MDALFDOC,'MDANNDOC',this.w_MDANNDOC,'MDCODESE',this.w_MDCODESE)
      insert into (i_cTable) (MDSERIAL,MDDATREG,MDCODNEG,MDTIPDOC,MDCODCLI,MDTIPCHI,MDPRD,MDCODPAG,MDNUMDOC,MDALFDOC,MDANNDOC,MDCODESE &i_ccchkf. );
         values (;
           this.w_MDSERIAL;
           ,this.w_MDDATREG;
           ,this.w_MDCODNEG;
           ,this.w_MDTIPDOC;
           ,this.w_MDCODCLI;
           ,"FF";
           ,this.w_MDPRD;
           ,this.w_MDCODPAG;
           ,this.w_MDNUMDOC;
           ,this.w_MDALFDOC;
           ,this.w_MDANNDOC;
           ,this.w_MDCODESE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Fatture
    this.w_MVSERIAL = ""
    this.w_TESTO = " "
    this.w_TIPOIN = this.oParentObject.w_TIPDOC
    * --- Read from CLI_VEND
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CLI_VEND_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CLCODCON"+;
        " from "+i_cTable+" CLI_VEND where ";
            +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_CODCLI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CLCODCON;
        from (i_cTable) where;
            CLCODCLI = this.oParentObject.w_CODCLI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CLIFOR = NVL(cp_ToDate(_read_.CLCODCON),cp_NullValue(_read_.CLCODCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVRIFODL = SPACE(10)
    this.w_MVRIFFAD = SPACE(10)
    this.w_DATA1 = this.oParentObject.w_DATDOC
    this.w_DATA2 = this.oParentObject.w_DATDOC
    this.w_serie1 = this.oParentObject.w_ALFDOC
    this.w_serie2 = this.oParentObject.w_ALFDOC
    this.w_FLVEAC = "V"
    this.w_CATDOC = "FA"
    this.w_CODESE = g_CODESE
    this.w_CODCON = this.w_CLIFOR
    this.w_TIPCON = "C"
    this.w_DATAIN = this.oParentObject.w_DATDOC
    this.w_DATAFI = this.oParentObject.w_DATDOC
    GSVE_BRD(this,"F")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='*POSTMP'
    this.cWorkTables[3]='DES_DIVE'
    this.cWorkTables[4]='KEY_ARTI'
    this.cWorkTables[5]='OUT_PUTS'
    this.cWorkTables[6]='CLI_VEND'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='TRADDOCU'
    this.cWorkTables[9]='TRADARTI'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
