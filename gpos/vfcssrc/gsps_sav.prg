* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_sav                                                        *
*              Analisi del venduto                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_558]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2013-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_sav",oParentObject))

* --- Class definition
define class tgsps_sav as StdForm
  Top    = 7
  Left   = 35

  * --- Standard Properties
  Width  = 609
  Height = 336+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-15"
  HelpContextID=124344425
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=75

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  ART_ICOL_IDX = 0
  LISTINI_IDX = 0
  CONTI_IDX = 0
  PAG_AMEN_IDX = 0
  VALUTE_IDX = 0
  INVENTAR_IDX = 0
  GRUMERC_IDX = 0
  REP_ARTI_IDX = 0
  MAGAZZIN_IDX = 0
  BUSIUNIT_IDX = 0
  CLI_VEND_IDX = 0
  OPE_RATO_IDX = 0
  cPrg = "gsps_sav"
  cComment = "Analisi del venduto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATA = ctod('  /  /  ')
  o_DATA = ctod('  /  /  ')
  w_AZIENDA = space(5)
  w_CODNEG = space(3)
  w_ONUME = 0
  w_DADATA = ctod('  /  /  ')
  w_ADATA = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_ORAFIN = space(2)
  o_ORAFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_DATSTA = ctod('  /  /  ')
  w_costmarg = space(1)
  o_costmarg = space(1)
  w_flscoiva = space(1)
  w_FLORDI = space(1)
  w_AGGIORN = space(27)
  o_AGGIORN = space(27)
  w_NUMERO = space(6)
  o_NUMERO = space(6)
  w_FLPUNT = space(1)
  o_FLPUNT = space(1)
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_CODESE = space(4)
  w_DATINV = ctod('  /  /  ')
  w_LISTINO = space(8)
  w_DECTOT = 0
  w_TIPOLN = space(1)
  w_LISTINO = space(8)
  w_TIPOCON = space(1)
  w_VALUTA = space(3)
  w_CONTRNU = space(20)
  w_CAMBIO = 0
  w_CONTRLI = space(20)
  w_CAOVAL1 = 0
  w_CAMBIO1 = 0
  w_CODART = space(20)
  w_CLIENTE = space(15)
  w_CODREP = space(3)
  w_GRUMER = space(5)
  w_PAGAM = space(5)
  w_CODMAG = space(5)
  w_CODOPE = 0
  w_INTERVAL = 0
  w_GIOLUN = space(1)
  w_GIOMAR = space(1)
  w_GIOMER = space(1)
  w_GIOGIO = space(1)
  w_GIOVEN = space(1)
  w_GIOSAB = space(1)
  w_GIODOM = space(1)
  w_STVAL = space(10)
  o_STVAL = space(10)
  w_VALU2 = space(3)
  o_VALU2 = space(3)
  w_DESPAG = space(30)
  w_CAMBIO2 = 0
  w_DESCVALU2 = space(35)
  w_CAOVAL2 = 0
  w_DESCLI = space(40)
  w_TIPO = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CLIENT = space(1)
  w_DECTOT2 = 0
  w_VALESE = space(3)
  w_CAOESE = 0
  w_DESART = space(40)
  w_DESGRU = space(35)
  w_CALCPICT = space(1)
  w_OBTEST1 = ctod('  /  /  ')
  w_SIMVAL = space(5)
  w_SIMVAL2 = space(5)
  w_SIMVAL = space(5)
  w_DESREP = space(35)
  w_IVAREP = space(3)
  w_DESMAG = space(30)
  w_DESNEG = space(40)
  w_DTOBS1 = ctod('  /  /  ')
  w_TIPART = space(2)
  w_DTOBSO = ctod('  /  /  ')
  w_ARTPOS = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_savPag1","gsps_sav",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsps_savPag2","gsps_sav",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODNEG_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='PAG_AMEN'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='INVENTAR'
    this.cWorkTables[8]='GRUMERC'
    this.cWorkTables[9]='REP_ARTI'
    this.cWorkTables[10]='MAGAZZIN'
    this.cWorkTables[11]='BUSIUNIT'
    this.cWorkTables[12]='CLI_VEND'
    this.cWorkTables[13]='OPE_RATO'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATA=ctod("  /  /  ")
      .w_AZIENDA=space(5)
      .w_CODNEG=space(3)
      .w_ONUME=0
      .w_DADATA=ctod("  /  /  ")
      .w_ADATA=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_ORAFIN=space(2)
      .w_MINFIN=space(2)
      .w_DATSTA=ctod("  /  /  ")
      .w_costmarg=space(1)
      .w_flscoiva=space(1)
      .w_FLORDI=space(1)
      .w_AGGIORN=space(27)
      .w_NUMERO=space(6)
      .w_FLPUNT=space(1)
      .w_VALUTA=space(3)
      .w_CODESE=space(4)
      .w_DATINV=ctod("  /  /  ")
      .w_LISTINO=space(8)
      .w_DECTOT=0
      .w_TIPOLN=space(1)
      .w_LISTINO=space(8)
      .w_TIPOCON=space(1)
      .w_VALUTA=space(3)
      .w_CONTRNU=space(20)
      .w_CAMBIO=0
      .w_CONTRLI=space(20)
      .w_CAOVAL1=0
      .w_CAMBIO1=0
      .w_CODART=space(20)
      .w_CLIENTE=space(15)
      .w_CODREP=space(3)
      .w_GRUMER=space(5)
      .w_PAGAM=space(5)
      .w_CODMAG=space(5)
      .w_CODOPE=0
      .w_INTERVAL=0
      .w_GIOLUN=space(1)
      .w_GIOMAR=space(1)
      .w_GIOMER=space(1)
      .w_GIOGIO=space(1)
      .w_GIOVEN=space(1)
      .w_GIOSAB=space(1)
      .w_GIODOM=space(1)
      .w_STVAL=space(10)
      .w_VALU2=space(3)
      .w_DESPAG=space(30)
      .w_CAMBIO2=0
      .w_DESCVALU2=space(35)
      .w_CAOVAL2=0
      .w_DESCLI=space(40)
      .w_TIPO=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CLIENT=space(1)
      .w_DECTOT2=0
      .w_VALESE=space(3)
      .w_CAOESE=0
      .w_DESART=space(40)
      .w_DESGRU=space(35)
      .w_CALCPICT=space(1)
      .w_OBTEST1=ctod("  /  /  ")
      .w_SIMVAL=space(5)
      .w_SIMVAL2=space(5)
      .w_SIMVAL=space(5)
      .w_DESREP=space(35)
      .w_IVAREP=space(3)
      .w_DESMAG=space(30)
      .w_DESNEG=space(40)
      .w_DTOBS1=ctod("  /  /  ")
      .w_TIPART=space(2)
      .w_DTOBSO=ctod("  /  /  ")
      .w_ARTPOS=space(20)
        .w_DATA = IIF(.w_COSTMARG='S' AND .w_AGGIORN$'LICO-FICO' AND .w_FLPUNT='S' AND NOT EMPTY(.w_NUMERO), .w_DATINV +1, cp_CharToDate('    -  -  '))
        .w_AZIENDA = i_CODAZI
        .w_CODNEG = g_CODNEG
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODNEG))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_DADATA = .w_DATA
        .w_ADATA = i_datsys
        .w_ORAINI = IIF(EMPTY(.w_ORAINI),'  ',RIGHT('00'+.w_ORAINI,2))
        .w_MININI = IIF(EMPTY(.w_ORAINI) OR EMPTY(.w_MININI),'00',RIGHT('00'+.w_MININI,2))
        .w_ORAFIN = IIF(EMPTY(.w_ORAFIN),'  ',RIGHT('00'+.w_ORAFIN,2))
        .w_MINFIN = IIF(EMPTY(.w_ORAFIN) OR EMPTY(.w_MINFIN),'59',RIGHT('00'+.w_MINFIN,2))
        .w_DATSTA = i_DATSYS
        .w_costmarg = 'N'
        .w_flscoiva = 'N'
        .w_FLORDI = 'N'
        .w_AGGIORN = 'CMPA'
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_NUMERO))
          .link_1_21('Full')
        endif
        .w_FLPUNT = 'N'
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_VALUTA))
          .link_1_23('Full')
        endif
        .w_CODESE = g_CODESE
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODESE))
          .link_1_24('Full')
        endif
        .DoRTCalc(20,21,.f.)
        if not(empty(.w_LISTINO))
          .link_1_29('Full')
        endif
        .DoRTCalc(22,24,.f.)
        if not(empty(.w_LISTINO))
          .link_1_33('Full')
        endif
        .w_TIPOCON = 'C'
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_VALUTA))
          .link_1_35('Full')
        endif
        .w_CONTRNU = IIF(.w_COSTMARG='S',IIF(.w_AGGIORN<>'LIST' AND (NOT EMPTY(.w_NUMERO)),'N',' '),'X')
        .w_CAMBIO = IIF(.w_CAOVAL1<>0,.w_CAOVAL1,GETCAM(.w_VALUTA, .w_Adata, 7))
        .w_CONTRLI = IIF(.w_COSTMARG='S',IIF(.w_AGGIORN='LIST' AND (NOT EMPTY(.w_LISTINO)),'L',' '),'X')
          .DoRTCalc(30,30,.f.)
        .w_CAMBIO1 = IIF(.w_CAOVAL1<>0,.w_CAOVAL1,GETCAM(.w_VALUTA, .w_Adata, 7))
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_CODART))
          .link_2_1('Full')
        endif
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_CLIENTE))
          .link_2_2('Full')
        endif
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_CODREP))
          .link_2_3('Full')
        endif
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_GRUMER))
          .link_2_4('Full')
        endif
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_PAGAM))
          .link_2_5('Full')
        endif
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_CODMAG))
          .link_2_6('Full')
        endif
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_CODOPE))
          .link_2_7('Full')
        endif
          .DoRTCalc(39,39,.f.)
        .w_GIOLUN = '2'
        .w_GIOMAR = '3'
        .w_GIOMER = '4'
        .w_GIOGIO = '5'
        .w_GIOVEN = '6'
        .w_GIOSAB = '7'
        .w_GIODOM = '1'
        .w_STVAL = 'C'
        .w_VALU2 = g_PERVAL
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_VALU2))
          .link_2_17('Full')
        endif
          .DoRTCalc(49,49,.f.)
        .w_CAMBIO2 = GETCAM(.w_VALU2, .w_Adata, 7)
          .DoRTCalc(51,53,.f.)
        .w_TIPO = 'C'
        .w_OBTEST = i_INIDAT
          .DoRTCalc(56,56,.f.)
        .w_CLIENT = 'C'
        .DoRTCalc(58,59,.f.)
        if not(empty(.w_VALESE))
          .link_1_49('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
          .DoRTCalc(60,62,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT2)
        .w_OBTEST1 = i_DATSYS
        .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
        .w_SIMVAL2 = IIF(EMPTY(.w_VALU2),' ',.w_SIMVAL2)
        .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
          .DoRTCalc(68,73,.f.)
        .w_DTOBSO = .w_DATSTA
    endwith
    this.DoRTCalc(75,75,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_DATA = IIF(.w_COSTMARG='S' AND .w_AGGIORN$'LICO-FICO' AND .w_FLPUNT='S' AND NOT EMPTY(.w_NUMERO), .w_DATINV +1, cp_CharToDate('    -  -  '))
        .DoRTCalc(2,4,.t.)
        if .o_AGGIORN<>.w_AGGIORN.or. .o_COSTMARG<>.w_COSTMARG.or. .o_FLPUNT<>.w_FLPUNT.or. .o_DATA<>.w_DATA
            .w_DADATA = .w_DATA
        endif
        .DoRTCalc(6,6,.t.)
        if .o_ORAINI<>.w_ORAINI
            .w_ORAINI = IIF(EMPTY(.w_ORAINI),'  ',RIGHT('00'+.w_ORAINI,2))
        endif
        if .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
            .w_MININI = IIF(EMPTY(.w_ORAINI) OR EMPTY(.w_MININI),'00',RIGHT('00'+.w_MININI,2))
        endif
        if .o_ORAFIN<>.w_ORAFIN
            .w_ORAFIN = IIF(EMPTY(.w_ORAFIN),'  ',RIGHT('00'+.w_ORAFIN,2))
        endif
        if .o_ORAINI<>.w_ORAINI.or. .o_MINFIN<>.w_MINFIN
            .w_MINFIN = IIF(EMPTY(.w_ORAFIN) OR EMPTY(.w_MINFIN),'59',RIGHT('00'+.w_MINFIN,2))
        endif
        .DoRTCalc(11,16,.t.)
        if .o_AGGIORN<>.w_AGGIORN
            .w_FLPUNT = 'N'
        endif
          .link_1_23('Full')
        if .o_Aggiorn<>.w_Aggiorn
          .link_1_24('Full')
        endif
        .DoRTCalc(20,25,.t.)
          .link_1_35('Full')
            .w_CONTRNU = IIF(.w_COSTMARG='S',IIF(.w_AGGIORN<>'LIST' AND (NOT EMPTY(.w_NUMERO)),'N',' '),'X')
        if .o_VALUTA<>.w_VALUTA
            .w_CAMBIO = IIF(.w_CAOVAL1<>0,.w_CAOVAL1,GETCAM(.w_VALUTA, .w_Adata, 7))
        endif
            .w_CONTRLI = IIF(.w_COSTMARG='S',IIF(.w_AGGIORN='LIST' AND (NOT EMPTY(.w_LISTINO)),'L',' '),'X')
        .DoRTCalc(30,30,.t.)
        if .o_VALUTA<>.w_VALUTA
            .w_CAMBIO1 = IIF(.w_CAOVAL1<>0,.w_CAOVAL1,GETCAM(.w_VALUTA, .w_Adata, 7))
        endif
        .DoRTCalc(32,47,.t.)
        if .o_stval<>.w_stval
            .w_VALU2 = g_PERVAL
          .link_2_17('Full')
        endif
        .DoRTCalc(49,49,.t.)
        if .o_valu2<>.w_valu2
            .w_CAMBIO2 = GETCAM(.w_VALU2, .w_Adata, 7)
        endif
        .DoRTCalc(51,58,.t.)
          .link_1_49('Full')
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .DoRTCalc(60,62,.t.)
        if .o_valu2<>.w_valu2
            .w_CALCPICT = DEFPIC(.w_DECTOT2)
        endif
        .DoRTCalc(64,64,.t.)
            .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
            .w_SIMVAL2 = IIF(EMPTY(.w_VALU2),' ',.w_SIMVAL2)
            .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
        .DoRTCalc(68,73,.t.)
            .w_DTOBSO = .w_DATSTA
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(75,75,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDADATA_1_5.enabled = this.oPgFrm.Page1.oPag.oDADATA_1_5.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_8.enabled = this.oPgFrm.Page1.oPag.oMININI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_10.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_10.mCond()
    this.oPgFrm.Page1.oPag.oFLORDI_1_19.enabled = this.oPgFrm.Page1.oPag.oFLORDI_1_19.mCond()
    this.oPgFrm.Page1.oPag.oAGGIORN_1_20.enabled = this.oPgFrm.Page1.oPag.oAGGIORN_1_20.mCond()
    this.oPgFrm.Page1.oPag.oNUMERO_1_21.enabled = this.oPgFrm.Page1.oPag.oNUMERO_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCODESE_1_24.enabled = this.oPgFrm.Page1.oPag.oCODESE_1_24.mCond()
    this.oPgFrm.Page1.oPag.oLISTINO_1_29.enabled = this.oPgFrm.Page1.oPag.oLISTINO_1_29.mCond()
    this.oPgFrm.Page1.oPag.oLISTINO_1_33.enabled = this.oPgFrm.Page1.oPag.oLISTINO_1_33.mCond()
    this.oPgFrm.Page1.oPag.oCAMBIO_1_37.enabled = this.oPgFrm.Page1.oPag.oCAMBIO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oCAMBIO1_1_44.enabled = this.oPgFrm.Page1.oPag.oCAMBIO1_1_44.mCond()
    this.oPgFrm.Page2.oPag.oVALU2_2_17.enabled = this.oPgFrm.Page2.oPag.oVALU2_2_17.mCond()
    this.oPgFrm.Page2.oPag.oCAMBIO2_2_25.enabled = this.oPgFrm.Page2.oPag.oCAMBIO2_2_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.ocostmarg_1_12.visible=!this.oPgFrm.Page1.oPag.ocostmarg_1_12.mHide()
    this.oPgFrm.Page1.oPag.oflscoiva_1_13.visible=!this.oPgFrm.Page1.oPag.oflscoiva_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oNUMERO_1_21.visible=!this.oPgFrm.Page1.oPag.oNUMERO_1_21.mHide()
    this.oPgFrm.Page1.oPag.oFLPUNT_1_22.visible=!this.oPgFrm.Page1.oPag.oFLPUNT_1_22.mHide()
    this.oPgFrm.Page1.oPag.oVALUTA_1_23.visible=!this.oPgFrm.Page1.oPag.oVALUTA_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCODESE_1_24.visible=!this.oPgFrm.Page1.oPag.oCODESE_1_24.mHide()
    this.oPgFrm.Page1.oPag.oLISTINO_1_29.visible=!this.oPgFrm.Page1.oPag.oLISTINO_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oCAMBIO_1_37.visible=!this.oPgFrm.Page1.oPag.oCAMBIO_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oCAMBIO1_1_44.visible=!this.oPgFrm.Page1.oPag.oCAMBIO1_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page2.oPag.oINTERVAL_2_8.visible=!this.oPgFrm.Page2.oPag.oINTERVAL_2_8.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_55.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODNEG
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_KAN',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODNEG)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_AZIENDA;
                     ,'BUCODICE',trim(this.w_CODNEG))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNEG)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODNEG) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODNEG_1_3'),i_cWhere,'GSPS_KAN',"Negozi azienda",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODNEG);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_AZIENDA;
                       ,'BUCODICE',this.w_CODNEG)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNEG = NVL(_Link_.BUCODICE,space(3))
      this.w_DESNEG = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODNEG = space(3)
      endif
      this.w_DESNEG = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMERO
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMERO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AIN',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMERO)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_CODESE;
                     ,'INNUMINV',trim(this.w_NUMERO))
          select INCODESE,INNUMINV,INDATINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMERO)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMERO) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMERO_1_21'),i_cWhere,'GSMA_AIN',"Inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Numero inventario errato o mancante")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMERO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMERO);
                   +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_CODESE;
                       ,'INNUMINV',this.w_NUMERO)
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMERO = NVL(_Link_.INNUMINV,space(6))
      this.w_CODESE = NVL(_Link_.INCODESE,space(4))
      this.w_DATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NUMERO = space(6)
      endif
      this.w_CODESE = space(4)
      this.w_DATINV = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMERO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL1 = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_CAOVAL1 = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_24'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_VALESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTINO
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTINO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTINO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTINO))
          select LSCODLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTINO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTINO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTINO_1_29'),i_cWhere,'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTINO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTINO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTINO)
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTINO = NVL(_Link_.LSCODLIS,space(8))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LISTINO = space(8)
      endif
      this.w_VALUTA = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTINO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTINO
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTINO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTINO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTINO))
          select LSCODLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTINO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTINO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTINO_1_33'),i_cWhere,'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTINO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTINO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTINO)
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTINO = NVL(_Link_.LSCODLIS,space(8))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LISTINO = space(8)
      endif
      this.w_VALUTA = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTINO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL1 = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_CAOVAL1 = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART,ARARTPOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART,ARARTPOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_2_1'),i_cWhere,'GSMA_BZA',"Articoli/servizi",'GSPS_APR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART,ARARTPOS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART,ARARTPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART,ARARTPOS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART,ARARTPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_ARTPOS = NVL(_Link_.ARARTPOS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_TIPART = space(2)
      this.w_ARTPOS = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBS1>.w_DATSTA OR EMPTY(.w_DTOBS1)) AND .w_ARTPOS='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Impossibile selezionare articoli non pos o obsoleti")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_DTOBS1 = ctod("  /  /  ")
        this.w_TIPART = space(2)
        this.w_ARTPOS = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIENTE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACV',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_CLIENTE))
          select CLCODCLI,CLDESCRI,CLDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIENTE)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStr(trim(this.w_CLIENTE)+"%");

            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIENTE) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oCLIENTE_2_2'),i_cWhere,'GSPS_ACV',"Clienti negozi",'CLIPOS.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_CLIENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_CLIENTE)
            select CLCODCLI,CLDESCRI,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIENTE = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESCLI = NVL(_Link_.CLDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CLDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIENTE = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
        endif
        this.w_CLIENTE = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODREP
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_CODREP)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_CODREP))
          select RECODREP,REDESREP,RECODIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODREP)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODREP) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oCODREP_2_3'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP,RECODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_CODREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_CODREP)
            select RECODREP,REDESREP,RECODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODREP = NVL(_Link_.RECODREP,space(3))
      this.w_DESREP = NVL(_Link_.REDESREP,space(35))
      this.w_IVAREP = NVL(_Link_.RECODIVA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODREP = space(3)
      endif
      this.w_DESREP = space(35)
      this.w_IVAREP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMER
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER_2_4'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PAGAM
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAGAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_PAGAM)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_PAGAM))
          select PACODICE,PADTOBSO,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAGAM)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PAGAM) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPAGAM_2_5'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADTOBSO,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAGAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PAGAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PAGAM)
            select PACODICE,PADTOBSO,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAGAM = NVL(_Link_.PACODICE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PAGAM = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_PAGAM = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESPAG = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAGAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_2_6'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODOPE
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OPE_RATO_IDX,3]
    i_lTable = "OPE_RATO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2], .t., this.OPE_RATO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_AOP',True,'OPE_RATO')
        if i_nConn<>0
          i_cWhere = " OPCODOPE="+cp_ToStrODBC(this.w_CODOPE);

          i_ret=cp_SQL(i_nConn,"select OPCODOPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OPCODOPE',this.w_CODOPE)
          select OPCODOPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODOPE) and !this.bDontReportError
            deferred_cp_zoom('OPE_RATO','*','OPCODOPE',cp_AbsName(oSource.parent,'oCODOPE_2_7'),i_cWhere,'GSPS_AOP',"Operatori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OPCODOPE";
                     +" from "+i_cTable+" "+i_lTable+" where OPCODOPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OPCODOPE',oSource.xKey(1))
            select OPCODOPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OPCODOPE";
                   +" from "+i_cTable+" "+i_lTable+" where OPCODOPE="+cp_ToStrODBC(this.w_CODOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OPCODOPE',this.w_CODOPE)
            select OPCODOPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODOPE = NVL(_Link_.OPCODOPE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODOPE = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2])+'\'+cp_ToStr(_Link_.OPCODOPE,1)
      cp_ShowWarn(i_cKey,this.OPE_RATO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALU2
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALU2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALU2)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALU2))
          select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALU2)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VALU2) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALU2_2_17'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALU2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALU2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALU2)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALU2 = NVL(_Link_.VACODVAL,space(3))
      this.w_descvalu2 = NVL(_Link_.VADESVAL,space(35))
      this.w_dectot2 = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL2 = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL2 = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALU2 = space(3)
      endif
      this.w_descvalu2 = space(35)
      this.w_dectot2 = 0
      this.w_CAOVAL2 = 0
      this.w_SIMVAL2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALU2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   +" and VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODNEG_1_3.value==this.w_CODNEG)
      this.oPgFrm.Page1.oPag.oCODNEG_1_3.value=this.w_CODNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_5.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_5.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oADATA_1_6.value==this.w_ADATA)
      this.oPgFrm.Page1.oPag.oADATA_1_6.value=this.w_ADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_7.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_7.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_8.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_8.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_9.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_9.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_10.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_10.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_11.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_11.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.ocostmarg_1_12.RadioValue()==this.w_costmarg)
      this.oPgFrm.Page1.oPag.ocostmarg_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oflscoiva_1_13.RadioValue()==this.w_flscoiva)
      this.oPgFrm.Page1.oPag.oflscoiva_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLORDI_1_19.RadioValue()==this.w_FLORDI)
      this.oPgFrm.Page1.oPag.oFLORDI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGIORN_1_20.RadioValue()==this.w_AGGIORN)
      this.oPgFrm.Page1.oPag.oAGGIORN_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_21.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_21.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPUNT_1_22.RadioValue()==this.w_FLPUNT)
      this.oPgFrm.Page1.oPag.oFLPUNT_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_23.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_23.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_24.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_24.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oLISTINO_1_29.value==this.w_LISTINO)
      this.oPgFrm.Page1.oPag.oLISTINO_1_29.value=this.w_LISTINO
    endif
    if not(this.oPgFrm.Page1.oPag.oLISTINO_1_33.value==this.w_LISTINO)
      this.oPgFrm.Page1.oPag.oLISTINO_1_33.value=this.w_LISTINO
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_35.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_35.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO_1_37.value==this.w_CAMBIO)
      this.oPgFrm.Page1.oPag.oCAMBIO_1_37.value=this.w_CAMBIO
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO1_1_44.value==this.w_CAMBIO1)
      this.oPgFrm.Page1.oPag.oCAMBIO1_1_44.value=this.w_CAMBIO1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODART_2_1.value==this.w_CODART)
      this.oPgFrm.Page2.oPag.oCODART_2_1.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page2.oPag.oCLIENTE_2_2.value==this.w_CLIENTE)
      this.oPgFrm.Page2.oPag.oCLIENTE_2_2.value=this.w_CLIENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oCODREP_2_3.value==this.w_CODREP)
      this.oPgFrm.Page2.oPag.oCODREP_2_3.value=this.w_CODREP
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMER_2_4.value==this.w_GRUMER)
      this.oPgFrm.Page2.oPag.oGRUMER_2_4.value=this.w_GRUMER
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGAM_2_5.value==this.w_PAGAM)
      this.oPgFrm.Page2.oPag.oPAGAM_2_5.value=this.w_PAGAM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAG_2_6.value==this.w_CODMAG)
      this.oPgFrm.Page2.oPag.oCODMAG_2_6.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oCODOPE_2_7.value==this.w_CODOPE)
      this.oPgFrm.Page2.oPag.oCODOPE_2_7.value=this.w_CODOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oINTERVAL_2_8.value==this.w_INTERVAL)
      this.oPgFrm.Page2.oPag.oINTERVAL_2_8.value=this.w_INTERVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOLUN_2_9.RadioValue()==this.w_GIOLUN)
      this.oPgFrm.Page2.oPag.oGIOLUN_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOMAR_2_10.RadioValue()==this.w_GIOMAR)
      this.oPgFrm.Page2.oPag.oGIOMAR_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOMER_2_11.RadioValue()==this.w_GIOMER)
      this.oPgFrm.Page2.oPag.oGIOMER_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOGIO_2_12.RadioValue()==this.w_GIOGIO)
      this.oPgFrm.Page2.oPag.oGIOGIO_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOVEN_2_13.RadioValue()==this.w_GIOVEN)
      this.oPgFrm.Page2.oPag.oGIOVEN_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOSAB_2_14.RadioValue()==this.w_GIOSAB)
      this.oPgFrm.Page2.oPag.oGIOSAB_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGIODOM_2_15.RadioValue()==this.w_GIODOM)
      this.oPgFrm.Page2.oPag.oGIODOM_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSTVAL_2_16.RadioValue()==this.w_STVAL)
      this.oPgFrm.Page2.oPag.oSTVAL_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVALU2_2_17.value==this.w_VALU2)
      this.oPgFrm.Page2.oPag.oVALU2_2_17.value=this.w_VALU2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPAG_2_19.value==this.w_DESPAG)
      this.oPgFrm.Page2.oPag.oDESPAG_2_19.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oCAMBIO2_2_25.value==this.w_CAMBIO2)
      this.oPgFrm.Page2.oPag.oCAMBIO2_2_25.value=this.w_CAMBIO2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_2_28.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_2_28.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART_2_35.value==this.w_DESART)
      this.oPgFrm.Page2.oPag.oDESART_2_35.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU_2_36.value==this.w_DESGRU)
      this.oPgFrm.Page2.oPag.oDESGRU_2_36.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_54.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_54.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oSIMVAL2_2_41.value==this.w_SIMVAL2)
      this.oPgFrm.Page2.oPag.oSIMVAL2_2_41.value=this.w_SIMVAL2
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_55.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_55.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESREP_2_42.value==this.w_DESREP)
      this.oPgFrm.Page2.oPag.oDESREP_2_42.value=this.w_DESREP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAG_2_45.value==this.w_DESMAG)
      this.oPgFrm.Page2.oPag.oDESMAG_2_45.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNEG_1_60.value==this.w_DESNEG)
      this.oPgFrm.Page1.oPag.oDESNEG_1_60.value=this.w_DESNEG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_adata)) OR (.w_adata>=.w_dadata))  and (EMPTY(.w_DATA)  OR .w_FLPUNT <>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATA_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   ((empty(.w_ADATA)) or not((.w_ADATA>=.w_DADATA) OR  ( empty(.w_ADATA))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oADATA_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ADATA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not(val(.w_ORAINI)<24 AND ((empty(.w_ORAFIN)) OR (.w_ORAFIN>=.w_ORAINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora inizio selezione non corretta")
          case   not(VAL(.w_MININI)<60)  and (NOT EMPTY(.w_ORAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti inizio selezione non corretti")
          case   not(val(.w_ORAFIN)=<24 AND ((.w_ORAFIN>=.w_ORAINI) OR  ( empty(.w_ORAFIN))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora fine selezione non corretta")
          case   not(VAL(.w_MINFIN)<60)  and (NOT EMPTY(.w_ORAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti fine selezione non corretti")
          case   (empty(.w_DATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODESE))  and not(.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')  and (.w_costmarg='S' AND .w_Aggiorn<>'LIST' AND .w_ONUME<>11)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_24.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAMBIO<>0)  and not(.w_CAOVAL1<>0 or Empty(nvl(.w_VALUTA,' ')))  and (.w_CAOVAL1=0  AND .w_ONUME<>11)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
          case   not(.w_CAMBIO1<>0)  and not(.w_CAOVAL1<>0 OR .w_Aggiorn<>'LIST' OR Empty(nvl(.w_VALUTA,' ')) OR .w_Aggiorn='UCST')  and (.w_CAOVAL1=0 AND .w_costmarg='S' AND .w_ONUME<>11)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO1_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
          case   not((.w_DTOBS1>.w_DATSTA OR EMPTY(.w_DTOBS1)) AND .w_ARTPOS='S')  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODART_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile selezionare articoli non pos o obsoleti")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_CLIENTE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLIENTE_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PAGAM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPAGAM_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   (empty(.w_VALU2))  and (.w_STVAL='A')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVALU2_2_17.SetFocus()
            i_bnoObbl = !empty(.w_VALU2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMBIO2))  and (.w_CAOVAL2=0 AND .w_VALU2<>' ')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCAMBIO2_2_25.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_sav
      * --- Disabilita tasto F10
         i_bRes=.f.
      
      
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATA = this.w_DATA
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_ORAFIN = this.w_ORAFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_costmarg = this.w_costmarg
    this.o_AGGIORN = this.w_AGGIORN
    this.o_NUMERO = this.w_NUMERO
    this.o_FLPUNT = this.w_FLPUNT
    this.o_VALUTA = this.w_VALUTA
    this.o_STVAL = this.w_STVAL
    this.o_VALU2 = this.w_VALU2
    return

enddefine

* --- Define pages as container
define class tgsps_savPag1 as StdContainer
  Width  = 605
  height = 336
  stdWidth  = 605
  stdheight = 336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODNEG_1_3 as StdField with uid="ADRORXSOVO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODNEG", cQueryName = "CODNEG",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 70859558,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=88, Top=14, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSPS_KAN", oKey_1_1="BUCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODNEG"

  func oCODNEG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNEG_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNEG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODNEG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_KAN',"Negozi azienda",'',this.parent.oContained
  endproc
  proc oCODNEG_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSPS_KAN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_AZIENDA
     i_obj.w_BUCODICE=this.parent.oContained.w_CODNEG
     i_obj.ecpSave()
  endproc

  add object oDADATA_1_5 as StdField with uid="KSMMEHAABE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data di inizio dei documenti di vendita da considerare",;
    HelpContextID = 253504822,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=88, Top=44

  func oDADATA_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DATA)  OR .w_FLPUNT <>'S')
    endwith
   endif
  endfunc

  func oDADATA_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_adata)) OR (.w_adata>=.w_dadata))
    endwith
    return bRes
  endfunc

  add object oADATA_1_6 as StdField with uid="TMLQZZAHGL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ADATA", cQueryName = "ADATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data di termine dei documenti di vendita da considerare",;
    HelpContextID = 50397178,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=88, Top=70

  func oADATA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ADATA>=.w_DADATA) OR  ( empty(.w_ADATA)))
    endwith
    return bRes
  endfunc

  add object oORAINI_1_7 as StdField with uid="DDNWXMUKSP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora inizio selezione non corretta",;
    ToolTipText = "Ora vendita inizio selezione",;
    HelpContextID = 113512166,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=248, Top=44, InputMask=replicate('X',2)

  func oORAINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_ORAINI)<24 AND ((empty(.w_ORAFIN)) OR (.w_ORAFIN>=.w_ORAINI)))
    endwith
    return bRes
  endfunc

  add object oMININI_1_8 as StdField with uid="BDZMDUHVRY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti inizio selezione non corretti",;
    ToolTipText = "Minuti vendita inizio selezione",;
    HelpContextID = 113563078,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=284, Top=44, InputMask=replicate('X',2)

  func oMININI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ORAINI))
    endwith
   endif
  endfunc

  func oMININI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI)<60)
    endwith
    return bRes
  endfunc

  add object oORAFIN_1_9 as StdField with uid="HIKYATJZHB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora fine selezione non corretta",;
    ToolTipText = "Ora vendita fine selezione",;
    HelpContextID = 191958758,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=248, Top=70, InputMask=replicate('X',2)

  func oORAFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_ORAFIN)=<24 AND ((.w_ORAFIN>=.w_ORAINI) OR  ( empty(.w_ORAFIN))))
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_10 as StdField with uid="PJAPJOVQDA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti fine selezione non corretti",;
    ToolTipText = "Minuti vendita fine selezione",;
    HelpContextID = 192009670,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=284, Top=70, InputMask=replicate('X',2)

  func oMINFIN_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ORAFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN)<60)
    endwith
    return bRes
  endfunc

  add object oDATSTA_1_11 as StdField with uid="XKCTIADZZD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di elaborazione, usata come riferimento per il listino",;
    HelpContextID = 254750006,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=523, Top=44

  add object ocostmarg_1_12 as StdCheck with uid="UZCGPCZDBF",rtseq=12,rtrep=.f.,left=481, top=70, caption="Costi e margine",;
    ToolTipText = "Se attivo riporta nella stampa i campi costo acquisto, margine lordo e %M.L.",;
    HelpContextID = 14830989,;
    cFormVar="w_costmarg", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func ocostmarg_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func ocostmarg_1_12.GetRadio()
    this.Parent.oContained.w_costmarg = this.RadioValue()
    return .t.
  endfunc

  func ocostmarg_1_12.SetRadio()
    this.Parent.oContained.w_costmarg=trim(this.Parent.oContained.w_costmarg)
    this.value = ;
      iif(this.Parent.oContained.w_costmarg=='S',1,;
      0)
  endfunc

  func ocostmarg_1_12.mHide()
    with this.Parent.oContained
      return (.w_ONUME=11)
    endwith
  endfunc

  add object oflscoiva_1_13 as StdCheck with uid="YPCLBXVKBM",rtseq=13,rtrep=.f.,left=481, top=90, caption="Scorporo IVA",;
    ToolTipText = "Se attivo le vendite vengono scorporate dall'IVA",;
    HelpContextID = 118404425,;
    cFormVar="w_flscoiva", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oflscoiva_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oflscoiva_1_13.GetRadio()
    this.Parent.oContained.w_flscoiva = this.RadioValue()
    return .t.
  endfunc

  func oflscoiva_1_13.SetRadio()
    this.Parent.oContained.w_flscoiva=trim(this.Parent.oContained.w_flscoiva)
    this.value = ;
      iif(this.Parent.oContained.w_flscoiva=='S',1,;
      0)
  endfunc

  func oflscoiva_1_13.mHide()
    with this.Parent.oContained
      return (.w_ONUME=11)
    endwith
  endfunc

  add object oFLORDI_1_19 as StdCheck with uid="BIKLOQVLMQ",rtseq=14,rtrep=.f.,left=321, top=140, caption="Ord. per venduto",;
    ToolTipText = "Se attivo ordina per totale venduto altrimenti mantiene l'ordinamento specifico della stampa",;
    HelpContextID = 103671894,;
    cFormVar="w_FLORDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLORDI_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLORDI_1_19.GetRadio()
    this.Parent.oContained.w_FLORDI = this.RadioValue()
    return .t.
  endfunc

  func oFLORDI_1_19.SetRadio()
    this.Parent.oContained.w_FLORDI=trim(this.Parent.oContained.w_FLORDI)
    this.value = ;
      iif(this.Parent.oContained.w_FLORDI=='S',1,;
      0)
  endfunc

  func oFLORDI_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME<>11)
    endwith
   endif
  endfunc


  add object oAGGIORN_1_20 as StdCombo with uid="SVYFDQOGHY",rtseq=15,rtrep=.f.,left=85,top=137,width=230,height=21;
    , ToolTipText = "Criterio di valorizzazione degli articoli di magazzino";
    , HelpContextID = 2858234;
    , cFormVar="w_AGGIORN",RowSource=""+"Costo medio ponderato annuo,"+"Costo medio ponderato periodico,"+"Costo ultimo,"+"Costo standard,"+"LIFO continuo,"+"FIFO continuo,"+"LIFO scatti,"+"Ultimo costo standard,"+"Listino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAGGIORN_1_20.RadioValue()
    return(iif(this.value =1,'CMPA',;
    iif(this.value =2,'CMPP',;
    iif(this.value =3,'COUL',;
    iif(this.value =4,'COST',;
    iif(this.value =5,'LICO',;
    iif(this.value =6,'FICO',;
    iif(this.value =7,'LISC',;
    iif(this.value =8,'UCST',;
    iif(this.value =9,'LIST',;
    space(27)))))))))))
  endfunc
  func oAGGIORN_1_20.GetRadio()
    this.Parent.oContained.w_AGGIORN = this.RadioValue()
    return .t.
  endfunc

  func oAGGIORN_1_20.SetRadio()
    this.Parent.oContained.w_AGGIORN=trim(this.Parent.oContained.w_AGGIORN)
    this.value = ;
      iif(this.Parent.oContained.w_AGGIORN=='CMPA',1,;
      iif(this.Parent.oContained.w_AGGIORN=='CMPP',2,;
      iif(this.Parent.oContained.w_AGGIORN=='COUL',3,;
      iif(this.Parent.oContained.w_AGGIORN=='COST',4,;
      iif(this.Parent.oContained.w_AGGIORN=='LICO',5,;
      iif(this.Parent.oContained.w_AGGIORN=='FICO',6,;
      iif(this.Parent.oContained.w_AGGIORN=='LISC',7,;
      iif(this.Parent.oContained.w_AGGIORN=='UCST',8,;
      iif(this.Parent.oContained.w_AGGIORN=='LIST',9,;
      0)))))))))
  endfunc

  func oAGGIORN_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COSTMARG='S' AND .w_ONUME<>11)
    endwith
   endif
  endfunc

  add object oNUMERO_1_21 as StdField with uid="TETFPVTIKO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Numero inventario errato o mancante",;
    ToolTipText = "Numero dell'inventario da visualizzare",;
    HelpContextID = 218157526,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=218, Top=172, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="GSMA_AIN", oKey_1_1="INCODESE", oKey_1_2="this.w_CODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMERO"

  func oNUMERO_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COSTMARG='S' AND .w_AGGIORN<>'LIST' AND NOT EMPTY(.w_CODESE) AND .w_ONUME<>11)
    endwith
   endif
  endfunc

  func oNUMERO_1_21.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN='LIST' OR .w_AGGIORN='UCST')
    endwith
  endfunc

  func oNUMERO_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMERO_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMERO_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_CODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_CODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMERO_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AIN',"Inventari",'',this.parent.oContained
  endproc
  proc oNUMERO_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AIN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_CODESE
     i_obj.w_INNUMINV=this.parent.oContained.w_NUMERO
     i_obj.ecpSave()
  endproc

  add object oFLPUNT_1_22 as StdCheck with uid="LBRMVGCFGH",rtseq=17,rtrep=.f.,left=445, top=140, caption="Elab. Fifo\Lifo puntuale",;
    ToolTipText = "Se attivo esegue elaborazione FIFO-LIFO puntuale",;
    HelpContextID = 30472278,;
    cFormVar="w_FLPUNT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPUNT_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLPUNT_1_22.GetRadio()
    this.Parent.oContained.w_FLPUNT = this.RadioValue()
    return .t.
  endfunc

  func oFLPUNT_1_22.SetRadio()
    this.Parent.oContained.w_FLPUNT=trim(this.Parent.oContained.w_FLPUNT)
    this.value = ;
      iif(this.Parent.oContained.w_FLPUNT=='S',1,;
      0)
  endfunc

  func oFLPUNT_1_22.mHide()
    with this.Parent.oContained
      return (NOT(.w_AGGIORN $ 'FICO-LICO'))
    endwith
  endfunc

  add object oVALUTA_1_23 as StdField with uid="WYWPJCWGRG",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta associata al listino",;
    HelpContextID = 254848598,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=218, Top=172, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_23.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST' OR .w_Aggiorn ='UCST')
    endwith
  endfunc

  func oVALUTA_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODESE_1_24 as StdField with uid="TYVLWVSSGB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 51395366,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=85, Top=172, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_costmarg='S' AND .w_Aggiorn<>'LIST' AND .w_ONUME<>11)
    endwith
   endif
  endfunc

  func oCODESE_1_24.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')
    endwith
  endfunc

  func oCODESE_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
      if .not. empty(.w_NUMERO)
        bRes2=.link_1_21('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODESE_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oLISTINO_1_29 as StdField with uid="KINQNTIZDJ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_LISTINO", cQueryName = "LISTINO",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino di riferimento",;
    HelpContextID = 192947638,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=85, Top=172, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTINO"

  func oLISTINO_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_costmarg='S')
    endwith
   endif
  endfunc

  func oLISTINO_1_29.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST' OR .w_Aggiorn='UCST' OR .w_ONUME=11)
    endwith
  endfunc

  func oLISTINO_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTINO_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTINO_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTINO_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTINO_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTINO
     i_obj.ecpSave()
  endproc

  add object oLISTINO_1_33 as StdField with uid="XUOTALSCCK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_LISTINO", cQueryName = "LISTINO",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Listino di valorizzazzione dei servizi",;
    HelpContextID = 192947638,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=85, Top=227, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTINO"

  func oLISTINO_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_costmarg='S'  AND .w_ONUME<>11)
    endwith
   endif
  endfunc

  func oLISTINO_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTINO_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTINO_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTINO_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTINO_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTINO
     i_obj.ecpSave()
  endproc

  add object oVALUTA_1_35 as StdField with uid="SFXVRNVBHA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta associata al listino",;
    HelpContextID = 254848598,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=218, Top=227, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCAMBIO_1_37 as StdField with uid="EPKAJJECXB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CAMBIO", cQueryName = "CAMBIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 208518438,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=391, Top=227

  func oCAMBIO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL1=0  AND .w_ONUME<>11)
    endwith
   endif
  endfunc

  func oCAMBIO_1_37.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 or Empty(nvl(.w_VALUTA,' ')))
    endwith
  endfunc

  func oCAMBIO_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAMBIO<>0)
    endwith
    return bRes
  endfunc

  add object oCAMBIO1_1_44 as StdField with uid="SYXEXKZMQO",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CAMBIO1", cQueryName = "CAMBIO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 208518438,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=391, Top=172

  func oCAMBIO1_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL1=0 AND .w_costmarg='S' AND .w_ONUME<>11)
    endwith
   endif
  endfunc

  func oCAMBIO1_1_44.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 OR .w_Aggiorn<>'LIST' OR Empty(nvl(.w_VALUTA,' ')) OR .w_Aggiorn='UCST')
    endwith
  endfunc

  func oCAMBIO1_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAMBIO1<>0)
    endwith
    return bRes
  endfunc


  add object oBtn_1_51 as StdButton with uid="XAOFRYAIID",left=497, top=290, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare la stampa";
    , HelpContextID = 17445158;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      with this.Parent.oContained
        do GSPS_BPV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ODES) AND (NOT EMPTY(.w_CONTRNU) OR NOT EMPTY(.w_CONTRLI)) OR .w_AGGIORN='UCST')
      endwith
    endif
  endfunc


  add object oObj_1_52 as cp_outputCombo with uid="ZLERXIVPWT",left=129, top=257, width=470,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 53653222


  add object oBtn_1_53 as StdButton with uid="KYQESPJULE",left=551, top=290, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 117027002;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSIMVAL_1_54 as StdField with uid="ECTOAJKETF",rtseq=65,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 151111206,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=263, Top=227, InputMask=replicate('X',5)

  add object oSIMVAL_1_55 as StdField with uid="EEZHHDKHAS",rtseq=67,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 151111206,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=263, Top=172, InputMask=replicate('X',5)

  func oSIMVAL_1_55.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST' OR .w_Aggiorn ='UCST')
    endwith
  endfunc

  add object oDESNEG_1_60 as StdField with uid="JWGIIKSIAV",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DESNEG", cQueryName = "DESNEG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 70918454,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=141, Top=14, InputMask=replicate('X',40)

  add object oStr_1_14 as StdString with uid="IOVTSCSLRF",Visible=.t., Left=4, Top=44,;
    Alignment=1, Width=80, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ICFOIHRQGF",Visible=.t., Left=4, Top=73,;
    Alignment=1, Width=80, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="YHYTTKORAT",Visible=.t., Left=10, Top=173,;
    Alignment=1, Width=72, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="MFIMKBSCEY",Visible=.t., Left=156, Top=173,;
    Alignment=1, Width=59, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="HNGIIZNRAB",Visible=.t., Left=28, Top=110,;
    Alignment=0, Width=592, Height=15,;
    Caption="Valorizzazione costi articoli"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="DYGLJWSYXZ",Visible=.t., Left=7, Top=259,;
    Alignment=1, Width=120, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ACIRBGYASN",Visible=.t., Left=28, Top=202,;
    Alignment=0, Width=597, Height=15,;
    Caption="Valorizzazione costi servizi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="KPFIKXAECL",Visible=.t., Left=156, Top=227,;
    Alignment=1, Width=59, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="KXXCYMYEKH",Visible=.t., Left=324, Top=227,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 OR Empty(nvl(.w_VALUTA,' ')))
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="ZTTIZZOTZG",Visible=.t., Left=10, Top=138,;
    Alignment=1, Width=72, Height=15,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="SCNSLAHCGY",Visible=.t., Left=19, Top=227,;
    Alignment=1, Width=64, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="YXOQTYNOIN",Visible=.t., Left=156, Top=174,;
    Alignment=1, Width=59, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST')
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="KPNQOYPNED",Visible=.t., Left=322, Top=173,;
    Alignment=1, Width=67, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 OR .w_Aggiorn<>'LIST' OR Empty(nvl(.w_VALUTA,' ')))
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="ZTUSXTFWWM",Visible=.t., Left=10, Top=174,;
    Alignment=1, Width=72, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="AMTALZPSCH",Visible=.t., Left=374, Top=44,;
    Alignment=1, Width=145, Height=18,;
    Caption="Data di elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="ZJNBAGKORH",Visible=.t., Left=183, Top=44,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="HCCEZDLRZY",Visible=.t., Left=183, Top=70,;
    Alignment=1, Width=62, Height=18,;
    Caption="A ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="OBFHHCGQQV",Visible=.t., Left=275, Top=44,;
    Alignment=1, Width=10, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="MKODBEZTPZ",Visible=.t., Left=275, Top=70,;
    Alignment=1, Width=10, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="BSEDNZTUNR",Visible=.t., Left=4, Top=14,;
    Alignment=1, Width=80, Height=18,;
    Caption="Negozio:"  ;
  , bGlobalFont=.t.

  add object oBox_1_46 as StdBox with uid="CONANCLOVL",left=26, top=128, width=572,height=1

  add object oBox_1_47 as StdBox with uid="SSXAOYKQYU",left=26, top=221, width=572,height=1
enddefine
define class tgsps_savPag2 as StdContainer
  Width  = 605
  height = 336
  stdWidth  = 605
  stdheight = 336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODART_2_1 as StdField with uid="GDOWHDUCOT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile selezionare articoli non pos o obsoleti",;
    ToolTipText = "Codice articolo selezionato",;
    HelpContextID = 33307430,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=108, Top=13, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli/servizi",'GSPS_APR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oCLIENTE_2_2 as StdField with uid="DNZWFJCDJL",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CLIENTE", cQueryName = "CLIENTE",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice cliente � inesistente oppure obsoleto",;
    ToolTipText = "Codice cliente selezionato",;
    HelpContextID = 29394982,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=108, Top=39, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", cZoomOnZoom="GSPS_ACV", oKey_1_1="CLCODCLI", oKey_1_2="this.w_CLIENTE"

  func oCLIENTE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIENTE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIENTE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oCLIENTE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACV',"Clienti negozi",'CLIPOS.CLI_VEND_VZM',this.parent.oContained
  endproc
  proc oCLIENTE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLCODCLI=this.parent.oContained.w_CLIENTE
     i_obj.ecpSave()
  endproc

  add object oCODREP_2_3 as StdField with uid="GZXWZQKQZY",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODREP", cQueryName = "CODREP",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 222116646,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=65, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_CODREP"

  func oCODREP_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODREP_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODREP_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oCODREP_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oCODREP_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_CODREP
     i_obj.ecpSave()
  endproc

  add object oGRUMER_2_4 as StdField with uid="GMEHYOKZIG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_GRUMER", cQueryName = "GRUMER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico selezionato",;
    HelpContextID = 255413862,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=91, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER"

  func oGRUMER_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRUMER_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMER
     i_obj.ecpSave()
  endproc

  add object oPAGAM_2_5 as StdField with uid="TGIYBOLWES",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PAGAM", cQueryName = "PAGAM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento selezionato",;
    HelpContextID = 39035402,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=117, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_PAGAM"

  func oPAGAM_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAGAM_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAGAM_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPAGAM_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPAGAM_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_PAGAM
     i_obj.ecpSave()
  endproc

  add object oCODMAG_2_6 as StdField with uid="ARVFREAYMP",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 66599718,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oCODOPE_2_7 as StdField with uid="CXTSJGBOLH",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CODOPE", cQueryName = "CODOPE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 48904998,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=108, Top=169, cSayPict='"99"', cGetPict='"99"', bHasZoom = .t. , cLinkFile="OPE_RATO", cZoomOnZoom="GSPS_AOP", oKey_1_1="OPCODOPE", oKey_1_2="this.w_CODOPE"

  func oCODOPE_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODOPE_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODOPE_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OPE_RATO','*','OPCODOPE',cp_AbsName(this.parent,'oCODOPE_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_AOP',"Operatori",'',this.parent.oContained
  endproc
  proc oCODOPE_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSPS_AOP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OPCODOPE=this.parent.oContained.w_CODOPE
     i_obj.ecpSave()
  endproc

  add object oINTERVAL_2_8 as StdField with uid="PSFSZEMPUO",rtseq=39,rtrep=.f.,;
    cFormVar = "w_INTERVAL", cQueryName = "INTERVAL",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Intervallo di elaborazione per stampa fascia oraria",;
    HelpContextID = 67189458,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=321, Top=169

  func oINTERVAL_2_8.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>9)
    endwith
  endfunc

  add object oGIOLUN_2_9 as StdCheck with uid="YLFBHHZYJJ",rtseq=40,rtrep=.f.,left=13, top=201, caption="Luned�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 204989798,;
    cFormVar="w_GIOLUN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOLUN_2_9.RadioValue()
    return(iif(this.value =1,'2',;
    ' '))
  endfunc
  func oGIOLUN_2_9.GetRadio()
    this.Parent.oContained.w_GIOLUN = this.RadioValue()
    return .t.
  endfunc

  func oGIOLUN_2_9.SetRadio()
    this.Parent.oContained.w_GIOLUN=trim(this.Parent.oContained.w_GIOLUN)
    this.value = ;
      iif(this.Parent.oContained.w_GIOLUN=='2',1,;
      0)
  endfunc

  add object oGIOMAR_2_10 as StdCheck with uid="BPOTZCVRPB",rtseq=41,rtrep=.f.,left=85, top=201, caption="Marted�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 251192678,;
    cFormVar="w_GIOMAR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOMAR_2_10.RadioValue()
    return(iif(this.value =1,'3',;
    ' '))
  endfunc
  func oGIOMAR_2_10.GetRadio()
    this.Parent.oContained.w_GIOMAR = this.RadioValue()
    return .t.
  endfunc

  func oGIOMAR_2_10.SetRadio()
    this.Parent.oContained.w_GIOMAR=trim(this.Parent.oContained.w_GIOMAR)
    this.value = ;
      iif(this.Parent.oContained.w_GIOMAR=='3',1,;
      0)
  endfunc

  add object oGIOMER_2_11 as StdCheck with uid="PLMVJQUSYV",rtseq=42,rtrep=.f.,left=170, top=201, caption="Mercoled�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 255386982,;
    cFormVar="w_GIOMER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOMER_2_11.RadioValue()
    return(iif(this.value =1,'4',;
    ' '))
  endfunc
  func oGIOMER_2_11.GetRadio()
    this.Parent.oContained.w_GIOMER = this.RadioValue()
    return .t.
  endfunc

  func oGIOMER_2_11.SetRadio()
    this.Parent.oContained.w_GIOMER=trim(this.Parent.oContained.w_GIOMER)
    this.value = ;
      iif(this.Parent.oContained.w_GIOMER=='4',1,;
      0)
  endfunc

  add object oGIOGIO_2_12 as StdCheck with uid="VBNKFXJXZF",rtseq=43,rtrep=.f.,left=263, top=201, caption="Gioved�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 208856422,;
    cFormVar="w_GIOGIO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOGIO_2_12.RadioValue()
    return(iif(this.value =1,'5',;
    ' '))
  endfunc
  func oGIOGIO_2_12.GetRadio()
    this.Parent.oContained.w_GIOGIO = this.RadioValue()
    return .t.
  endfunc

  func oGIOGIO_2_12.SetRadio()
    this.Parent.oContained.w_GIOGIO=trim(this.Parent.oContained.w_GIOGIO)
    this.value = ;
      iif(this.Parent.oContained.w_GIOGIO=='5',1,;
      0)
  endfunc

  add object oGIOVEN_2_13 as StdCheck with uid="EPAPSYXMPA",rtseq=44,rtrep=.f.,left=356, top=201, caption="Venerd�",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 188867942,;
    cFormVar="w_GIOVEN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOVEN_2_13.RadioValue()
    return(iif(this.value =1,'6',;
    ' '))
  endfunc
  func oGIOVEN_2_13.GetRadio()
    this.Parent.oContained.w_GIOVEN = this.RadioValue()
    return .t.
  endfunc

  func oGIOVEN_2_13.SetRadio()
    this.Parent.oContained.w_GIOVEN=trim(this.Parent.oContained.w_GIOVEN)
    this.value = ;
      iif(this.Parent.oContained.w_GIOVEN=='6',1,;
      0)
  endfunc

  add object oGIOSAB_2_14 as StdCheck with uid="PEJMLNCTOC",rtseq=45,rtrep=.f.,left=437, top=201, caption="Sabato",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 251585894,;
    cFormVar="w_GIOSAB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIOSAB_2_14.RadioValue()
    return(iif(this.value =1,'7',;
    ' '))
  endfunc
  func oGIOSAB_2_14.GetRadio()
    this.Parent.oContained.w_GIOSAB = this.RadioValue()
    return .t.
  endfunc

  func oGIOSAB_2_14.SetRadio()
    this.Parent.oContained.w_GIOSAB=trim(this.Parent.oContained.w_GIOSAB)
    this.value = ;
      iif(this.Parent.oContained.w_GIOSAB=='7',1,;
      0)
  endfunc

  add object oGIODOM_2_15 as StdCheck with uid="IDKNYMLIXI",rtseq=46,rtrep=.f.,left=517, top=201, caption="Domenica",;
    ToolTipText = "Se attivo: filtra per il relativo giorno della settimana",;
    HelpContextID = 181396838,;
    cFormVar="w_GIODOM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIODOM_2_15.RadioValue()
    return(iif(this.value =1,'1',;
    ' '))
  endfunc
  func oGIODOM_2_15.GetRadio()
    this.Parent.oContained.w_GIODOM = this.RadioValue()
    return .t.
  endfunc

  func oGIODOM_2_15.SetRadio()
    this.Parent.oContained.w_GIODOM=trim(this.Parent.oContained.w_GIODOM)
    this.value = ;
      iif(this.Parent.oContained.w_GIODOM=='1',1,;
      0)
  endfunc


  add object oSTVAL_2_16 as StdCombo with uid="VIZQGRQXYY",rtseq=47,rtrep=.f.,left=108,top=270,width=113,height=21;
    , ToolTipText = "Valuta di valorizzazione dei totali sulla stampa";
    , HelpContextID = 40017626;
    , cFormVar="w_STVAL",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oSTVAL_2_16.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(10))))
  endfunc
  func oSTVAL_2_16.GetRadio()
    this.Parent.oContained.w_STVAL = this.RadioValue()
    return .t.
  endfunc

  func oSTVAL_2_16.SetRadio()
    this.Parent.oContained.w_STVAL=trim(this.Parent.oContained.w_STVAL)
    this.value = ;
      iif(this.Parent.oContained.w_STVAL=='C',1,;
      iif(this.Parent.oContained.w_STVAL=='A',2,;
      0))
  endfunc

  add object oVALU2_2_17 as StdField with uid="DWHGNCVMZI",rtseq=48,rtrep=.f.,;
    cFormVar = "w_VALU2", cQueryName = "VALU2",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 66015658,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=291, Top=270, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALU2"

  func oVALU2_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STVAL='A')
    endwith
   endif
  endfunc

  func oVALU2_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALU2_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALU2_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALU2_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALU2_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALU2
     i_obj.ecpSave()
  endproc

  add object oDESPAG_2_19 as StdField with uid="CIURVSUYUU",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 66855222,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=117, InputMask=replicate('X',30)

  add object oCAMBIO2_2_25 as StdField with uid="JFHQPTZNMO",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CAMBIO2", cQueryName = "CAMBIO2",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 208518438,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=491, Top=270

  func oCAMBIO2_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL2=0 AND .w_VALU2<>' ')
    endwith
   endif
  endfunc

  add object oDESCLI_2_28 as StdField with uid="TWSXVINGFB",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 111092022,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=39, InputMask=replicate('X',40)

  add object oDESART_2_35 as StdField with uid="XTEYJOVCSQ",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 33366326,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=13, InputMask=replicate('X',40)

  add object oDESGRU_2_36 as StdField with uid="OXDFLLORMT",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 50536758,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=91, InputMask=replicate('X',35)

  add object oSIMVAL2_2_41 as StdField with uid="YJEYKYZTGR",rtseq=66,rtrep=.f.,;
    cFormVar = "w_SIMVAL2", cQueryName = "SIMVAL2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 151111206,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=329, Top=270, InputMask=replicate('X',5)

  add object oDESREP_2_42 as StdField with uid="GHTWVYCUZX",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESREP", cQueryName = "DESREP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 222175542,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=65, InputMask=replicate('X',35)

  add object oDESMAG_2_45 as StdField with uid="EJUWTMEEDX",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 66658614,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=143, InputMask=replicate('X',30)

  add object oStr_2_18 as StdString with uid="ZWRRXYXROA",Visible=.t., Left=1, Top=121,;
    Alignment=1, Width=104, Height=15,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="SXOGKGKNEX",Visible=.t., Left=9, Top=239,;
    Alignment=0, Width=565, Height=15,;
    Caption="Valuta totali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_22 as StdString with uid="QKUPPYRXXA",Visible=.t., Left=12, Top=270,;
    Alignment=1, Width=92, Height=15,;
    Caption="Stampa in:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="KHAUGDCEMY",Visible=.t., Left=236, Top=270,;
    Alignment=1, Width=53, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="PZHPGKGVAV",Visible=.t., Left=384, Top=270,;
    Alignment=1, Width=104, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="GLXRFLHNHN",Visible=.t., Left=1, Top=39,;
    Alignment=1, Width=104, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="SNFXLXSTPF",Visible=.t., Left=1, Top=13,;
    Alignment=1, Width=104, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="XYUGNJUKOO",Visible=.t., Left=1, Top=94,;
    Alignment=1, Width=104, Height=18,;
    Caption="Gru. Merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="CJOVTSIEVF",Visible=.t., Left=1, Top=67,;
    Alignment=1, Width=104, Height=18,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="MOTKUDJLHM",Visible=.t., Left=1, Top=146,;
    Alignment=1, Width=104, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="JTMFGGQVQN",Visible=.t., Left=1, Top=171,;
    Alignment=1, Width=104, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="SZGRLOHSDU",Visible=.t., Left=242, Top=171,;
    Alignment=1, Width=78, Height=18,;
    Caption="Intervallo:"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>9)
    endwith
  endfunc

  add object oBox_2_20 as StdBox with uid="ILBOQDWCSU",left=9, top=257, width=588,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_sav','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
