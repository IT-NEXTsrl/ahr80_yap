* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_apd                                                        *
*              Parametri dispositivi                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_23]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_apd"))

* --- Class definition
define class tgsps_apd as StdForm
  Top    = 6
  Left   = 14

  * --- Standard Properties
  Width  = 519
  Height = 307+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=159996009
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  DIS_HARD_IDX = 0
  cFile = "DIS_HARD"
  cKeySelect = "DHCODICE"
  cKeyWhere  = "DHCODICE=this.w_DHCODICE"
  cKeyWhereODBC = '"DHCODICE="+cp_ToStrODBC(this.w_DHCODICE)';

  cKeyWhereODBCqualified = '"DIS_HARD.DHCODICE="+cp_ToStrODBC(this.w_DHCODICE)';

  cPrg = "gsps_apd"
  cComment = "Parametri dispositivi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DHCODICE = space(5)
  w_DHDESCRI = space(40)
  w_DHTIPDIS = space(1)
  w_DHPROGRA = space(18)
  w_DHRITARD = 0
  w_DHBUFRIC = 0
  w_DHBUFTRA = 0
  w_DH__BAUD = 0
  w_DHPARITA = space(1)
  w_DHBITSTP = 0
  w_DHBITDAT = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIS_HARD','gsps_apd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_apdPag1","gsps_apd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri dispositivo")
      .Pages(1).HelpContextID = 239362053
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDHCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIS_HARD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_HARD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_HARD_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DHCODICE = NVL(DHCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIS_HARD where DHCODICE=KeySet.DHCODICE
    *
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_HARD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_HARD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_HARD '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DHCODICE',this.w_DHCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DHCODICE = NVL(DHCODICE,space(5))
        .w_DHDESCRI = NVL(DHDESCRI,space(40))
        .w_DHTIPDIS = NVL(DHTIPDIS,space(1))
        .w_DHPROGRA = NVL(DHPROGRA,space(18))
        .w_DHRITARD = NVL(DHRITARD,0)
        .w_DHBUFRIC = NVL(DHBUFRIC,0)
        .w_DHBUFTRA = NVL(DHBUFTRA,0)
        .w_DH__BAUD = NVL(DH__BAUD,0)
        .w_DHPARITA = NVL(DHPARITA,space(1))
        .w_DHBITSTP = NVL(DHBITSTP,0)
        .w_DHBITDAT = NVL(DHBITDAT,0)
        cp_LoadRecExtFlds(this,'DIS_HARD')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DHCODICE = space(5)
      .w_DHDESCRI = space(40)
      .w_DHTIPDIS = space(1)
      .w_DHPROGRA = space(18)
      .w_DHRITARD = 0
      .w_DHBUFRIC = 0
      .w_DHBUFTRA = 0
      .w_DH__BAUD = 0
      .w_DHPARITA = space(1)
      .w_DHBITSTP = 0
      .w_DHBITDAT = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_DHTIPDIS = 'R'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_HARD')
    this.DoRTCalc(4,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDHCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oDHDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oDHTIPDIS_1_3.enabled = i_bVal
      .Page1.oPag.oDHPROGRA_1_5.enabled = i_bVal
      .Page1.oPag.oDHRITARD_1_6.enabled = i_bVal
      .Page1.oPag.oDHBUFRIC_1_7.enabled = i_bVal
      .Page1.oPag.oDHBUFTRA_1_8.enabled = i_bVal
      .Page1.oPag.oDH__BAUD_1_10.enabled_(i_bVal)
      .Page1.oPag.oDHPARITA_1_12.enabled_(i_bVal)
      .Page1.oPag.oDHBITSTP_1_14.enabled_(i_bVal)
      .Page1.oPag.oDHBITDAT_1_16.enabled_(i_bVal)
      if i_cOp = "Edit"
        .Page1.oPag.oDHCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDHCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DIS_HARD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHCODICE,"DHCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHDESCRI,"DHDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHTIPDIS,"DHTIPDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPROGRA,"DHPROGRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHRITARD,"DHRITARD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHBUFRIC,"DHBUFRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHBUFTRA,"DHBUFTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DH__BAUD,"DH__BAUD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPARITA,"DHPARITA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHBITSTP,"DHBITSTP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHBITDAT,"DHBITDAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    i_lTable = "DIS_HARD"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIS_HARD_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIS_HARD_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIS_HARD
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_HARD')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_HARD')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DHCODICE,DHDESCRI,DHTIPDIS,DHPROGRA,DHRITARD"+;
                  ",DHBUFRIC,DHBUFTRA,DH__BAUD,DHPARITA,DHBITSTP"+;
                  ",DHBITDAT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DHCODICE)+;
                  ","+cp_ToStrODBC(this.w_DHDESCRI)+;
                  ","+cp_ToStrODBC(this.w_DHTIPDIS)+;
                  ","+cp_ToStrODBC(this.w_DHPROGRA)+;
                  ","+cp_ToStrODBC(this.w_DHRITARD)+;
                  ","+cp_ToStrODBC(this.w_DHBUFRIC)+;
                  ","+cp_ToStrODBC(this.w_DHBUFTRA)+;
                  ","+cp_ToStrODBC(this.w_DH__BAUD)+;
                  ","+cp_ToStrODBC(this.w_DHPARITA)+;
                  ","+cp_ToStrODBC(this.w_DHBITSTP)+;
                  ","+cp_ToStrODBC(this.w_DHBITDAT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_HARD')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_HARD')
        cp_CheckDeletedKey(i_cTable,0,'DHCODICE',this.w_DHCODICE)
        INSERT INTO (i_cTable);
              (DHCODICE,DHDESCRI,DHTIPDIS,DHPROGRA,DHRITARD,DHBUFRIC,DHBUFTRA,DH__BAUD,DHPARITA,DHBITSTP,DHBITDAT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DHCODICE;
                  ,this.w_DHDESCRI;
                  ,this.w_DHTIPDIS;
                  ,this.w_DHPROGRA;
                  ,this.w_DHRITARD;
                  ,this.w_DHBUFRIC;
                  ,this.w_DHBUFTRA;
                  ,this.w_DH__BAUD;
                  ,this.w_DHPARITA;
                  ,this.w_DHBITSTP;
                  ,this.w_DHBITDAT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIS_HARD_IDX,i_nConn)
      *
      * update DIS_HARD
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_HARD')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DHDESCRI="+cp_ToStrODBC(this.w_DHDESCRI)+;
             ",DHTIPDIS="+cp_ToStrODBC(this.w_DHTIPDIS)+;
             ",DHPROGRA="+cp_ToStrODBC(this.w_DHPROGRA)+;
             ",DHRITARD="+cp_ToStrODBC(this.w_DHRITARD)+;
             ",DHBUFRIC="+cp_ToStrODBC(this.w_DHBUFRIC)+;
             ",DHBUFTRA="+cp_ToStrODBC(this.w_DHBUFTRA)+;
             ",DH__BAUD="+cp_ToStrODBC(this.w_DH__BAUD)+;
             ",DHPARITA="+cp_ToStrODBC(this.w_DHPARITA)+;
             ",DHBITSTP="+cp_ToStrODBC(this.w_DHBITSTP)+;
             ",DHBITDAT="+cp_ToStrODBC(this.w_DHBITDAT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_HARD')
        i_cWhere = cp_PKFox(i_cTable  ,'DHCODICE',this.w_DHCODICE  )
        UPDATE (i_cTable) SET;
              DHDESCRI=this.w_DHDESCRI;
             ,DHTIPDIS=this.w_DHTIPDIS;
             ,DHPROGRA=this.w_DHPROGRA;
             ,DHRITARD=this.w_DHRITARD;
             ,DHBUFRIC=this.w_DHBUFRIC;
             ,DHBUFTRA=this.w_DHBUFTRA;
             ,DH__BAUD=this.w_DH__BAUD;
             ,DHPARITA=this.w_DHPARITA;
             ,DHBITSTP=this.w_DHBITSTP;
             ,DHBITDAT=this.w_DHBITDAT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIS_HARD_IDX,i_nConn)
      *
      * delete DIS_HARD
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DHCODICE',this.w_DHCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDHCODICE_1_1.value==this.w_DHCODICE)
      this.oPgFrm.Page1.oPag.oDHCODICE_1_1.value=this.w_DHCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDHDESCRI_1_2.value==this.w_DHDESCRI)
      this.oPgFrm.Page1.oPag.oDHDESCRI_1_2.value=this.w_DHDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDHTIPDIS_1_3.RadioValue()==this.w_DHTIPDIS)
      this.oPgFrm.Page1.oPag.oDHTIPDIS_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHPROGRA_1_5.value==this.w_DHPROGRA)
      this.oPgFrm.Page1.oPag.oDHPROGRA_1_5.value=this.w_DHPROGRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDHRITARD_1_6.value==this.w_DHRITARD)
      this.oPgFrm.Page1.oPag.oDHRITARD_1_6.value=this.w_DHRITARD
    endif
    if not(this.oPgFrm.Page1.oPag.oDHBUFRIC_1_7.value==this.w_DHBUFRIC)
      this.oPgFrm.Page1.oPag.oDHBUFRIC_1_7.value=this.w_DHBUFRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDHBUFTRA_1_8.value==this.w_DHBUFTRA)
      this.oPgFrm.Page1.oPag.oDHBUFTRA_1_8.value=this.w_DHBUFTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDH__BAUD_1_10.RadioValue()==this.w_DH__BAUD)
      this.oPgFrm.Page1.oPag.oDH__BAUD_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHPARITA_1_12.RadioValue()==this.w_DHPARITA)
      this.oPgFrm.Page1.oPag.oDHPARITA_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHBITSTP_1_14.RadioValue()==this.w_DHBITSTP)
      this.oPgFrm.Page1.oPag.oDHBITSTP_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHBITDAT_1_16.RadioValue()==this.w_DHBITDAT)
      this.oPgFrm.Page1.oPag.oDHBITDAT_1_16.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DIS_HARD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_apdPag1 as StdContainer
  Width  = 515
  height = 307
  stdWidth  = 515
  stdheight = 307
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDHCODICE_1_1 as StdField with uid="DMDPBTYLYO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DHCODICE", cQueryName = "DHCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 67773563,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=137, Top=12, InputMask=replicate('X',5)

  add object oDHDESCRI_1_2 as StdField with uid="XGGPBLXWJI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DHDESCRI", cQueryName = "DHDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del dispositivo",;
    HelpContextID = 250623103,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=202, Top=13, InputMask=replicate('X',40)


  add object oDHTIPDIS_1_3 as StdCombo with uid="AWSMWWHZKE",rtseq=3,rtrep=.f.,left=137,top=44,width=139,height=21;
    , HelpContextID = 3853175;
    , cFormVar="w_DHTIPDIS",RowSource=""+"Registratore di cassa,"+"Penna ottica,"+"Bilancia", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDHTIPDIS_1_3.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'P',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oDHTIPDIS_1_3.GetRadio()
    this.Parent.oContained.w_DHTIPDIS = this.RadioValue()
    return .t.
  endfunc

  func oDHTIPDIS_1_3.SetRadio()
    this.Parent.oContained.w_DHTIPDIS=trim(this.Parent.oContained.w_DHTIPDIS)
    this.value = ;
      iif(this.Parent.oContained.w_DHTIPDIS=='R',1,;
      iif(this.Parent.oContained.w_DHTIPDIS=='P',2,;
      iif(this.Parent.oContained.w_DHTIPDIS=='B',3,;
      0)))
  endfunc

  add object oDHPROGRA_1_5 as StdField with uid="FCBSRBFYAD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DHPROGRA", cQueryName = "DHPROGRA",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 46003319,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=137, Top=72, InputMask=replicate('X',18)

  add object oDHRITARD_1_6 as StdField with uid="UBHBBYCQSJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DHRITARD", cQueryName = "DHRITARD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritardo in (1/10 secondi)",;
    HelpContextID = 218436730,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=137, Top=100, cSayPict='"999"', cGetPict='"999"'

  add object oDHBUFRIC_1_7 as StdField with uid="QHGCISQKIU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DHBUFRIC", cQueryName = "DHBUFRIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione Buffer di ricezione (1030..65535 bytes)",;
    HelpContextID = 47180679,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=137, Top=130, cSayPict='"999999"', cGetPict='"999999"'

  add object oDHBUFTRA_1_8 as StdField with uid="TNZRJGADFX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DHBUFTRA", cQueryName = "DHBUFTRA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione Buffer di trasmissione (1030..65535 bytes)",;
    HelpContextID = 254809207,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=137, Top=160

  add object oDH__BAUD_1_10 as StdRadio with uid="GRTKWBNLVO",rtseq=8,rtrep=.f.,left=137, top=190, width=370,height=23;
    , cFormVar="w_DH__BAUD", ButtonCount=6, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDH__BAUD_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1200"
      this.Buttons(1).HelpContextID = 201057402
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("1200","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="2400"
      this.Buttons(2).HelpContextID = 201057402
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("2400","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="4800"
      this.Buttons(3).HelpContextID = 201057402
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("4800","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.Buttons(4).Caption="9600"
      this.Buttons(4).HelpContextID = 201057402
      this.Buttons(4).Left=i_coord
      this.Buttons(4).Width=(TxtWidth("9600","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(4).Width
      this.Buttons(5).Caption="19200"
      this.Buttons(5).HelpContextID = 201057402
      this.Buttons(5).Left=i_coord
      this.Buttons(5).Width=(TxtWidth("19200","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(5).Width
      this.Buttons(6).Caption="38400"
      this.Buttons(6).HelpContextID = 201057402
      this.Buttons(6).Left=i_coord
      this.Buttons(6).Width=(TxtWidth("38400","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(6).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oDH__BAUD_1_10.RadioValue()
    return(iif(this.value =1,1200,;
    iif(this.value =2,2400,;
    iif(this.value =3,4800,;
    iif(this.value =4,9600,;
    iif(this.value =5,19200,;
    iif(this.value =6,38400,;
    0)))))))
  endfunc
  func oDH__BAUD_1_10.GetRadio()
    this.Parent.oContained.w_DH__BAUD = this.RadioValue()
    return .t.
  endfunc

  func oDH__BAUD_1_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DH__BAUD==1200,1,;
      iif(this.Parent.oContained.w_DH__BAUD==2400,2,;
      iif(this.Parent.oContained.w_DH__BAUD==4800,3,;
      iif(this.Parent.oContained.w_DH__BAUD==9600,4,;
      iif(this.Parent.oContained.w_DH__BAUD==19200,5,;
      iif(this.Parent.oContained.w_DH__BAUD==38400,6,;
      0))))))
  endfunc

  add object oDHPARITA_1_12 as StdRadio with uid="PBRYONJIBE",rtseq=9,rtrep=.f.,left=137, top=219, width=343,height=23;
    , cFormVar="w_DHPARITA", ButtonCount=5, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDHPARITA_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="No"
      this.Buttons(1).HelpContextID = 81589367
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("No","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Odd"
      this.Buttons(2).HelpContextID = 81589367
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Odd","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Even"
      this.Buttons(3).HelpContextID = 81589367
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Even","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.Buttons(4).Caption="Mark"
      this.Buttons(4).HelpContextID = 81589367
      this.Buttons(4).Left=i_coord
      this.Buttons(4).Width=(TxtWidth("Mark","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(4).Width
      this.Buttons(5).Caption="Space"
      this.Buttons(5).HelpContextID = 81589367
      this.Buttons(5).Left=i_coord
      this.Buttons(5).Width=(TxtWidth("Space","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(5).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oDHPARITA_1_12.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'4',;
    space(1)))))))
  endfunc
  func oDHPARITA_1_12.GetRadio()
    this.Parent.oContained.w_DHPARITA = this.RadioValue()
    return .t.
  endfunc

  func oDHPARITA_1_12.SetRadio()
    this.Parent.oContained.w_DHPARITA=trim(this.Parent.oContained.w_DHPARITA)
    this.value = ;
      iif(this.Parent.oContained.w_DHPARITA=='0',1,;
      iif(this.Parent.oContained.w_DHPARITA=='1',2,;
      iif(this.Parent.oContained.w_DHPARITA=='2',3,;
      iif(this.Parent.oContained.w_DHPARITA=='3',4,;
      iif(this.Parent.oContained.w_DHPARITA=='4',5,;
      0)))))
  endfunc

  add object oDHBITSTP_1_14 as StdRadio with uid="BEZIYAJGMB",rtseq=10,rtrep=.f.,left=137, top=248, width=78,height=23;
    , cFormVar="w_DHBITSTP", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDHBITSTP_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1"
      this.Buttons(1).HelpContextID = 251925638
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("1","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="2"
      this.Buttons(2).HelpContextID = 251925638
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("2","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oDHBITSTP_1_14.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oDHBITSTP_1_14.GetRadio()
    this.Parent.oContained.w_DHBITSTP = this.RadioValue()
    return .t.
  endfunc

  func oDHBITSTP_1_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DHBITSTP==1,1,;
      iif(this.Parent.oContained.w_DHBITSTP==2,2,;
      0))
  endfunc

  add object oDHBITDAT_1_16 as StdRadio with uid="LUYWUSWWVQ",rtseq=11,rtrep=.f.,left=137, top=277, width=78,height=23;
    , cFormVar="w_DHBITDAT", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDHBITDAT_1_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="7"
      this.Buttons(1).HelpContextID = 267402
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("7","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="8"
      this.Buttons(2).HelpContextID = 267402
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("8","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oDHBITDAT_1_16.RadioValue()
    return(iif(this.value =1,7,;
    iif(this.value =2,8,;
    0)))
  endfunc
  func oDHBITDAT_1_16.GetRadio()
    this.Parent.oContained.w_DHBITDAT = this.RadioValue()
    return .t.
  endfunc

  func oDHBITDAT_1_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DHBITDAT==7,1,;
      iif(this.Parent.oContained.w_DHBITDAT==8,2,;
      0))
  endfunc

  add object oStr_1_4 as StdString with uid="MSSZTUZZZB",Visible=.t., Left=5, Top=12,;
    Alignment=1, Width=123, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="HUXKZAVHEI",Visible=.t., Left=5, Top=72,;
    Alignment=1, Width=123, Height=18,;
    Caption="Prg di gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="ULHIMPQXZZ",Visible=.t., Left=5, Top=190,;
    Alignment=1, Width=123, Height=18,;
    Caption="Baud:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="JQLNIGLMCV",Visible=.t., Left=5, Top=219,;
    Alignment=1, Width=123, Height=18,;
    Caption="Parit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="HEANDCPVKK",Visible=.t., Left=5, Top=248,;
    Alignment=1, Width=123, Height=18,;
    Caption="Bit stop:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="BYSBISWUOO",Visible=.t., Left=5, Top=277,;
    Alignment=1, Width=123, Height=18,;
    Caption="Bit dati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="MDLAPBSYIS",Visible=.t., Left=5, Top=46,;
    Alignment=1, Width=123, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ZZBAFVJBJQ",Visible=.t., Left=5, Top=100,;
    Alignment=1, Width=123, Height=18,;
    Caption="Ritardo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="HDHNDAYITF",Visible=.t., Left=5, Top=130,;
    Alignment=1, Width=123, Height=18,;
    Caption="Buffer ricezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="MQYKYNIHNZ",Visible=.t., Left=5, Top=160,;
    Alignment=1, Width=123, Height=18,;
    Caption="Buffer trasmissione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_apd','DIS_HARD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DHCODICE=DIS_HARD.DHCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
