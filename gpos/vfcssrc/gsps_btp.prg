* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_btp                                                        *
*              Test su promozioni                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_65]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-02                                                      *
* Last revis.: 2003-04-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_btp",oParentObject,m.pExec)
return(i_retval)

define class tgsps_btp as StdBatch
  * --- Local variables
  pExec = space(1)
  w_MSG = space(1)
  w_OK = .f.
  w_OKRIGHE = .f.
  w_GRUMER = space(5)
  w_CODART = space(20)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  * --- WorkFile variables
  PRM_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato delle Promozioni (GSPS_MPR) per i controlli sulle valorizzazioni dei campi
    * --- pExec
    *     'A' = Valorizzazione variabili TST... per visualizzazione campi relativi alle selezioni effettuate
    *     'B' = Evento 'Controlli' al salvataggio. Impedisce il salvataggio in caso di impostazioni incongruenti
    * --- Istanzio Oggetto Messaggio Incrementale
    this.w_oMESS=createobject("ah_message")
    this.w_OK = .T.
    this.w_OKRIGHE = .T.
    if  this.pExec="A"
      * --- Metto tutte le variabili a 'N'. Nel CASE vado a valorizzare a 'S' quelle che
      *     mi rendono obbligatori o visibili i campi necessari per la selezione
      *     Non al Salvataggio
      this.oParentObject.w_TSTSCP = "N"
      this.oParentObject.w_TSTSVT = "N"
      this.oParentObject.w_TSTSVU = "N"
      this.oParentObject.w_TSTPUF = "N"
      this.oParentObject.w_TSTPTF = "N"
      this.oParentObject.w_TSTMPZ = "N"
      this.oParentObject.w_TSTMBL = "N"
      this.oParentObject.w_TSTMVL = "N"
      this.oParentObject.w_TSTQTA = "N"
    endif
    * --- Nel caso di Effetto Promozione = Sconto valorizzo le variabili TST..
    *     per la visualizzazione a seconda del tipo si sconto selezionato
    do case
      case this.oParentObject.w_PRTIPPRO="S"
        do case
          case this.oParentObject.w_PRTIPSCO="SPT"
            this.oParentObject.w_TSTSCP = "S"
          case this.oParentObject.w_PRTIPSCO="SPE"
            if (this.oParentObject.w_PRTIPVAQ="N" OR this.oParentObject.w_PRTIPVAQ="B") AND this.pExec="B"
              this.w_OK = .F.
              this.w_oMESS.AddMsgPartNL("I requisiti minimi devono essere impostati a minimo pezzi o minimo valore")     
              this.oParentObject.w_PRTIPVAQ = "Q"
            else
              this.oParentObject.w_TSTSCP = "S"
              this.oParentObject.w_TSTMPZ = "S"
              this.oParentObject.w_TSTMVL = "S"
            endif
          case this.oParentObject.w_PRTIPSCO="SVF"
            this.oParentObject.w_TSTSVU = "S"
          case this.oParentObject.w_PRTIPSCO="SVM"
            if this.oParentObject.w_PRTIPVAQ="N" AND this.pExec="B"
              this.w_OK = .F.
              this.w_oMESS.AddMsgPartNL("I requisiti minimi devono essere gestiti")     
              this.oParentObject.w_PRTIPVAQ = "Q"
            else
              this.oParentObject.w_TSTSVU = "S"
              this.oParentObject.w_TSTMPZ = "S"
              this.oParentObject.w_TSTMBL = "S"
              this.oParentObject.w_TSTMVL = "S"
            endif
          case this.oParentObject.w_PRTIPSCO="SVP"
            this.oParentObject.w_TSTSVU = "S"
          case this.oParentObject.w_PRTIPSCO="SVE"
            if (this.oParentObject.w_PRTIPVAQ="N" OR this.oParentObject.w_PRTIPVAQ="B") AND this.pExec="B"
              this.w_OK = .F.
              this.w_oMESS.AddMsgPartNL("I requisiti minimi devono essere impostati a minimo pezzi o minimo valore")     
              this.oParentObject.w_PRTIPVAQ = "Q"
            else
              this.oParentObject.w_TSTSVU = "S"
              this.oParentObject.w_TSTMPZ = "S"
              this.oParentObject.w_TSTMVL = "S"
            endif
          case this.oParentObject.w_PRTIPSCO="PUP"
            this.oParentObject.w_TSTPUF = "S"
          case this.oParentObject.w_PRTIPSCO="PTF"
            if this.oParentObject.w_PRTIPVAQ<>"Q" AND this.pExec="B"
              this.w_OK = .F.
              this.w_oMESS.AddMsgPartNL("I requisiti minimi devono essere impostati a minimo pezzi")     
              this.oParentObject.w_PRTIPVAQ = "Q"
            else
              this.oParentObject.w_TSTPTF = "S"
              this.oParentObject.w_TSTMPZ = "S"
            endif
          case this.oParentObject.w_PRTIPSCO="PTM"
            if this.oParentObject.w_PRTIPVAQ<>"Q" AND this.pExec="B"
              this.w_OK = .F.
              this.w_oMESS.AddMsgPartNL("I requisiti minimi devono essere impostati a minimo pezzi")     
              this.oParentObject.w_PRTIPVAQ = "Q"
            else
              this.oParentObject.w_TSTPTF = "S"
              this.oParentObject.w_TSTMPZ = "S"
            endif
          case this.oParentObject.w_PRTIPSCO="NXM"
            if this.oParentObject.w_PRTIPVAQ="B" AND this.pExec="B"
              this.w_OK = .F.
              this.w_oMESS.AddMsgPartNL("I requisiti minimi non possono essere impostati a minimo bollini")     
              this.oParentObject.w_PRTIPVAQ = "N"
            else
              this.oParentObject.w_TSTQTA = "S"
            endif
        endcase
      case this.oParentObject.w_PRTIPPRO="O"
        if this.oParentObject.w_PRQTOMAG=0 AND this.pExec="B"
          this.w_OK = .F.
          this.w_oMESS.AddMsgPartNL("Definire la quantit� articolo da omaggiare")     
        endif
      case this.oParentObject.w_PRTIPPRO="F"
        if (this.oParentObject.w_PRTIPVAQ="B" OR this.oParentObject.w_PRTIPVAQ="N") AND this.pExec="B"
          this.w_OK = .F.
          this.w_oMESS.AddMsgPartNL("I requisiti minimi devono essere impostati a minimo pezzi o minimo valore")     
          this.oParentObject.w_PRTIPVAQ = "Q"
        endif
    endcase
    if this.pExec="B"
      this.w_OKRIGHE = .F.
      * --- Controllo se ci sono righe di dettaglio
      * --- Select from PRM_DETT
      i_nConn=i_TableProp[this.PRM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRM_DETT_idx,2],.t.,this.PRM_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PRGRUMER,PRCODART  from "+i_cTable+" PRM_DETT ";
            +" where PRCODICE = "+cp_ToStrODBC(this.oParentObject.w_PRCODICE)+"";
             ,"_Curs_PRM_DETT")
      else
        select PRGRUMER,PRCODART from (i_cTable);
         where PRCODICE = this.oParentObject.w_PRCODICE;
          into cursor _Curs_PRM_DETT
      endif
      if used('_Curs_PRM_DETT')
        select _Curs_PRM_DETT
        locate for 1=1
        do while not(eof())
        this.w_OKRIGHE = NOT EMPTY(NVL(_Curs_PRM_DETT.PRGRUMER,SPACE(5))) OR NOT EMPTY(NVL(_Curs_PRM_DETT.PRCODART,SPACE(20)))
        if this.w_OKRIGHE
          Exit
        endif
          select _Curs_PRM_DETT
          continue
        enddo
        use
      endif
      if this.oParentObject.w_PRFLNOAR<>"S"
        * --- Se il flag tutti gli articoli � disattivato devono essere presenti righe di dettagio.....
        if this.w_OKRIGHE=.F.
          this.w_oMESS.AddMsgPartNL("Deve essere caricata almeno una riga di dettaglio")     
        endif
      else
        * --- ........altrimenti non ci deve essere dettaglio
        if this.w_OKRIGHE=.T.
          this.w_oMESS.AddMsgPartNL("Con flag tutti gli articoli attivato non devono essere presenti righe di dettaglio")     
        endif
      endif
    endif
    this.w_MSG = this.w_oMESS.ComposeMessage()
    this.oParentObject.mHideControls()
    if this.w_OK=.F. OR (this.w_OKRIGHE=.F. And this.oParentObject.w_PRFLNOAR<>"S") Or (this.w_OKRIGHE=.T. And this.oParentObject.w_PRFLNOAR="S")
      * --- Errore impostazione dati
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MSG
    endif
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PRM_DETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PRM_DETT')
      use in _Curs_PRM_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
