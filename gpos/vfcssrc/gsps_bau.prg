* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bau                                                        *
*              Autonumber cli pos                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2001-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bau",oParentObject,m.pOper)
return(i_retval)

define class tgsps_bau as StdBatch
  * --- Local variables
  pOper = space(1)
  w_MESS = space(10)
  w_VISITE = 0
  w_TOTVEN = 0
  w_ULTVEN = ctod("  /  /  ")
  * --- WorkFile variables
  CONTI_idx=0
  COR_RISP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiusta il Codice Cliente calcolato dall'Autonumber alla effettiva lunghezza della Picture Parametrica g_MASPOS
    * --- Chiamato all'evento New record da (GSPS_ACV)
    do case
      case this.pOper="C"
        if g_FLCPOS="S"
          if LEN(ALLTRIM(g_MASPOS))<>0
            this.oParentObject.w_CLCODCLI = RIGHT(this.oParentObject.w_CLCODCLI, MIN(LEN(ALLTRIM(g_MASPOS)), 15))
          endif
        else
          * --- Anche se non gestito, l'autonumber ritorna sempre una stringa di zeri in questo caso, deve essere svuotata
          this.oParentObject.w_CLCODCLI = SPACE(15)
        endif
        this.oParentObject.op_CLCODCLI = this.oParentObject.w_CLCODCLI
      case this.pOper="D"
        * --- Esegue la Write solo se al al Delete Start (pOper='E') non viene trovato un documento con il cliente
        *     Altrimenti poich� viene notificato il Transaction Error viene emesso 'Errore Nella Scrittura'
        if Not Empty(this.oParentObject.w_CLCODCON) And Not(this.oParentObject.w_Trov)
          * --- Cancellazione Cliente Pos: tolgo il Flag di Cliente POS dal Cliente Azienda
          * --- Write into CONTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANCLIPOS');
                +i_ccchkf ;
            +" where ";
                +"ANTIPCON = "+cp_ToStrODBC("C");
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CLCODCON);
                   )
          else
            update (i_cTable) set;
                ANCLIPOS = " ";
                &i_ccchkf. ;
             where;
                ANTIPCON = "C";
                and ANCODICE = this.oParentObject.w_CLCODCON;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pOper="E"
        this.oParentObject.w_Trov = .F.
        * --- Select from COR_RISP
        i_nConn=i_TableProp[this.COR_RISP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" COR_RISP ";
              +" where MDCODCLI="+cp_ToStrODBC(this.oParentObject.w_CLCODCLI)+"";
               ,"_Curs_COR_RISP")
        else
          select * from (i_cTable);
           where MDCODCLI=this.oParentObject.w_CLCODCLI;
            into cursor _Curs_COR_RISP
        endif
        if used('_Curs_COR_RISP')
          select _Curs_COR_RISP
          locate for 1=1
          do while not(eof())
          * --- Cerco documenti con il cliente che sto cancellando:
          *     se ne trovo, non permetto la cancellazione
          this.oParentObject.w_Trov = .T.
          EXIT
            select _Curs_COR_RISP
            continue
          enddo
          use
        endif
        if this.oParentObject.w_Trov
          this.w_MESS = ah_Msgformat("Cliente movimentato nella vendita negozio. Impossibile eliminare")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pOper="V"
        VQ_EXEC("..\GPOS\EXE\QUERY\GSPS_QAC.VQR",this,"DATI")
        if Used("DATI")
           
 Select DATI 
 Go Top
          this.w_VISITE = Nvl(VISITE,0)
          this.w_TOTVEN = Nvl(TOTVEN,0)
          this.w_ULTVEN = CP_TODATE(ULTVEN)
        endif
        do GSPS_KAC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='COR_RISP'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_COR_RISP')
      use in _Curs_COR_RISP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
