* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bgs                                                        *
*              Emissione scontrino                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_342]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-22                                                      *
* Last revis.: 2014-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec,pSerial
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bgs",oParentObject,m.pExec,m.pSerial)
return(i_retval)

define class tgsps_bgs as StdBatch
  * --- Local variables
  pExec = space(1)
  pSerial = space(10)
  w_SERIAL = space(10)
  w_MESS = space(10)
  w_OK = .f.
  w_RIGHE = 0
  w_DATREG = ctod("  /  /  ")
  w_ORAVEN = space(2)
  w_MINVEN = space(2)
  w_NUMSCO = 0
  w_MATSCO = space(20)
  w_CODOPE = 0
  w_CODCAS = space(5)
  w_PORTA = 0
  w_NUMCAR = 0
  w_STADES = space(1)
  w_RECORD = space(100)
  w_Separa = space(2)
  w_CMD = space(80)
  w_Ret = 0
  w_VALRIG = 0
  w_POSCOM = space(3)
  w_POSIZ = 0
  w_RECTEST = space(5)
  w_PSW = space(10)
  w_POSPAT = space(254)
  w_MATCAS = space(20)
  w_RECTEST = space(100)
  w_RECTEST1 = space(100)
  w_DATREG = ctod("  /  /  ")
  w_ORAVEN = space(2)
  w_MINVEN = space(2)
  w_NUMSCO = 0
  w_MATCAS = space(20)
  w_FAMCAS = space(1)
  w_TESTTMP = 0
  w_TMP = 0
  w_TMPOUT = 0
  w_FINESCO = space(254)
  w_FINESCO2 = space(254)
  w_SR_ERR = space(254)
  w_SR_DATI = space(254)
  w_SR_OUT = space(254)
  w_SR_MATR = space(254)
  w_DIRCAS = space(254)
  w_TMPPAT = space(254)
  w_AHRPOS = space(254)
  w_POSEXE = space(254)
  w_NOCART = .f.
  w_DIVIDI = .f.
  w_StrDaIns = space(0)
  w_MDIMPABB = 0
  w_MDPAGCLI = 0
  w_MDTOTDOC = 0
  w_BATSCO = space(250)
  w_ARTMAG = space(20)
  w_NUMFIS = 0
  w_SR_ERR_C = space(254)
  w_SR_DATI_C = space(254)
  w_SR_OUT_C = space(254)
  w_PATFIL = space(10)
  w_CARDAT = space(1)
  w_OK_AGG = .f.
  w_DDATREG = ctod("  /  /  ")
  w_DORAVEN = space(2)
  w_DMINVEN = space(2)
  w_DNUMSCO = 0
  w_DMATCAS = space(20)
  w_PASQTA = space(1)
  w_BAUD = 0
  w_FISCALE = .f.
  w_FLMBRG = space(1)
  w_SR_CMD = space(254)
  w_TMP_CMD = space(254)
  w_MDTIPVEN = space(5)
  w_MOFINASS = space(1)
  w_MDPAGCAR = 0
  w_MDPAGASS = 0
  w_MDPAGFIN = 0
  w_PAGEST = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_GEST = .NULL.
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MDTIPRIG = space(1)
  w_MDCODICE = space(20)
  w_MDCODART = space(20)
  w_MDDESART = space(40)
  w_MDUNIMIS = space(3)
  w_MDQTAMOV = 0
  w_MDPREZZO = 0
  w_MDCODIVA = space(5)
  w_MDSCONT1 = 0
  w_MDSCONT2 = 0
  w_MDSCONT3 = 0
  w_MDSCONT4 = 0
  w_MDFLOMAG = space(1)
  w_MDQTAUM1 = 0
  w_MDCONTRA = space(15)
  w_MDFLCASC = space(1)
  w_MDMAGSAL = space(5)
  w_MDSCOVEN = 0
  w_MDSCOVEN_OLD = 0
  w_MDSERRIF = space(10)
  w_MDNUMRIF = 0
  w_MDROWRIF = 0
  w_MDFLARIF = space(1)
  w_MDFLERIF = space(1)
  w_MDQTAIMP = 0
  w_MDQTAIM1 = 0
  w_MDCODPRO = space(15)
  w_MDFLESCL = space(1)
  w_MDFLROMA = space(1)
  w_MDSCOPRO = 0
  w_MDFLRESO = space(1)
  w_MDCODREP = space(3)
  w_MDFLULPV = space(1)
  w_MDVALULT = 0
  w_MDTIPPRO = space(2)
  w_MDPERPRO = 0
  w_DESART = space(40)
  w_PAFLPROM = space(1)
  w_SR_DATI_TEXT = space(0)
  w_TEXT_PLUD = space(0)
  w_POS_CHAR = 0
  w_QTA = space(20)
  w_NUMRIGHEDES = 0
  w_CLIENTPAT = space(10)
  w_UNITA = space(1)
  w_CLIENTEXE = space(10)
  w_CLIENTPOS = space(10)
  w_CONNECT = .f.
  w_LENTMPATT = 0
  w_PAGFINASS = 0
  w_PAGFINCAR = 0
  * --- WorkFile variables
  COR_RISP_idx=0
  DIS_HARD_idx=0
  PAR_VDET_idx=0
  ART_ICOL_idx=0
  MOD_VEND_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione File TXT da Vendita negozio (lanciato da maschera GSPS_KES al salvataggio della vendita)
    *     per Emissione Scontrino
    * --- Parametri:
    *     pExec : 'A' Generaizone scontrino
    *                  'B' Conferma Vendita anche se in errore (da pulsante OK su maschera GSPS_KES)
    *                  'C' Chiusura cassa
    *     
    *     pSerial : MDSERIAL Seriale della Vendita Negozio
    *                   Se vale TEST significa che la gestione � stata lanciata dal bottone TEST nei dispositivi Hardware GSAR_APD
    *                   Se vale CHIUSURA significa che la gestione � stata lanciata da menu Chiusura Cassa
    * --- Usata nella maschera GSPS_KCD
    this.w_OK_AGG = .F.
    * --- Istanzio Oggetto Messaggio Incrementale
    this.w_oMESS=createobject("ah_message")
    this.oParentObject.w_EDIT = .F.
    this.w_Separa = chr(13)+chr(10)
    this.w_SERIAL = this.pSerial
    this.w_FISCALE = Alltrim(this.w_SERIAL) <> "TEST"
    * --- FISCALE: se .T. significa che effettuo una vendita. Se .F. � un test
    if this.w_FISCALE And !this.oParentObject.w_CHIUSURA
      * --- Read from COR_RISP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COR_RISP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MDCODOPE,MDTOTDOC,MDIMPABB,MDPAGCLI,MDDATREG,MDORAVEN,MDMINVEN,MDNUMSCO,MDMATSCO,MDTIPVEN,MDPAGASS,MDPAGCAR,MDPAGFIN"+;
          " from "+i_cTable+" COR_RISP where ";
              +"MDSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MDCODOPE,MDTOTDOC,MDIMPABB,MDPAGCLI,MDDATREG,MDORAVEN,MDMINVEN,MDNUMSCO,MDMATSCO,MDTIPVEN,MDPAGASS,MDPAGCAR,MDPAGFIN;
          from (i_cTable) where;
              MDSERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODOPE = NVL(cp_ToDate(_read_.MDCODOPE),cp_NullValue(_read_.MDCODOPE))
        this.w_MDTOTDOC = NVL(cp_ToDate(_read_.MDTOTDOC),cp_NullValue(_read_.MDTOTDOC))
        this.w_MDIMPABB = NVL(cp_ToDate(_read_.MDIMPABB),cp_NullValue(_read_.MDIMPABB))
        this.w_MDPAGCLI = NVL(cp_ToDate(_read_.MDPAGCLI),cp_NullValue(_read_.MDPAGCLI))
        this.w_DDATREG = NVL(cp_ToDate(_read_.MDDATREG),cp_NullValue(_read_.MDDATREG))
        this.w_DORAVEN = NVL(cp_ToDate(_read_.MDORAVEN),cp_NullValue(_read_.MDORAVEN))
        this.w_DMINVEN = NVL(cp_ToDate(_read_.MDMINVEN),cp_NullValue(_read_.MDMINVEN))
        this.w_DNUMSCO = NVL(cp_ToDate(_read_.MDNUMSCO),cp_NullValue(_read_.MDNUMSCO))
        this.w_DMATCAS = NVL(cp_ToDate(_read_.MDMATSCO),cp_NullValue(_read_.MDMATSCO))
        this.w_MDTIPVEN = NVL(cp_ToDate(_read_.MDTIPVEN),cp_NullValue(_read_.MDTIPVEN))
        this.w_MDPAGASS = NVL(cp_ToDate(_read_.MDPAGASS),cp_NullValue(_read_.MDPAGASS))
        this.w_MDPAGCAR = NVL(cp_ToDate(_read_.MDPAGCAR),cp_NullValue(_read_.MDPAGCAR))
        this.w_MDPAGFIN = NVL(cp_ToDate(_read_.MDPAGFIN),cp_NullValue(_read_.MDPAGFIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Associazione finanziamento
      * --- Read from MOD_VEND
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_VEND_idx,2],.t.,this.MOD_VEND_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MOFINASS"+;
          " from "+i_cTable+" MOD_VEND where ";
              +"MOCODICE = "+cp_ToStrODBC(this.w_MDTIPVEN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MOFINASS;
          from (i_cTable) where;
              MOCODICE = this.w_MDTIPVEN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MOFINASS = NVL(cp_ToDate(_read_.MOFINASS),cp_NullValue(_read_.MOFINASS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    do case
      case this.pExec="A" Or this.pExec="C"
        if this.w_FISCALE
          this.w_OK = .T.
          if Type("g_CNFDISP")<>"U" Or FILE(tempadhoc()+"\AHR_DISP.CNF")
            * --- Lettura variabili pubblicahe per generazione TXT
            GSUT_BLD(this,"L")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CODCAS = IIF( TYPE("g_CODCAS")="C",g_CODCAS, SPACE(5))
            if EMPTY(this.w_CODCAS)
              AddMsgNL("Definire il registratore di cassa installato dalla maschera di gestione nei parametri%0%1",this,REPL("-",30))
              this.oParentObject.w_EDIT = .T.
            endif
          else
            AddMsgNL("'Definire il dispositivo installato dalla maschera di gestione nei parametri%0%1",this,REPL("-",30))
            this.oParentObject.w_EDIT = .T.
          endif
          * --- Read from DIS_HARD
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIS_HARD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DH__BAUD,DHCARDAT,DHFAMCAS,DHFLMBRG,DHMATCAS,DHNUMCAR,DHNUMFIS,DHNUMPOR,DHPASQTA,DHPATFIL,DHPOSPAT,DHPSWPOS,DHSTADES,DHPAGEST"+;
              " from "+i_cTable+" DIS_HARD where ";
                  +"DHCODICE = "+cp_ToStrODBC(this.w_CODCAS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DH__BAUD,DHCARDAT,DHFAMCAS,DHFLMBRG,DHMATCAS,DHNUMCAR,DHNUMFIS,DHNUMPOR,DHPASQTA,DHPATFIL,DHPOSPAT,DHPSWPOS,DHSTADES,DHPAGEST;
              from (i_cTable) where;
                  DHCODICE = this.w_CODCAS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BAUD = NVL(cp_ToDate(_read_.DH__BAUD),cp_NullValue(_read_.DH__BAUD))
            this.w_CARDAT = NVL(cp_ToDate(_read_.DHCARDAT),cp_NullValue(_read_.DHCARDAT))
            this.w_FAMCAS = NVL(cp_ToDate(_read_.DHFAMCAS),cp_NullValue(_read_.DHFAMCAS))
            this.w_FLMBRG = NVL(cp_ToDate(_read_.DHFLMBRG),cp_NullValue(_read_.DHFLMBRG))
            this.w_MATCAS = NVL(cp_ToDate(_read_.DHMATCAS),cp_NullValue(_read_.DHMATCAS))
            this.w_NUMCAR = NVL(cp_ToDate(_read_.DHNUMCAR),cp_NullValue(_read_.DHNUMCAR))
            this.w_NUMFIS = NVL(cp_ToDate(_read_.DHNUMFIS),cp_NullValue(_read_.DHNUMFIS))
            this.w_PORTA = NVL(cp_ToDate(_read_.DHNUMPOR),cp_NullValue(_read_.DHNUMPOR))
            this.w_PASQTA = NVL(cp_ToDate(_read_.DHPASQTA),cp_NullValue(_read_.DHPASQTA))
            this.w_PATFIL = NVL(cp_ToDate(_read_.DHPATFIL),cp_NullValue(_read_.DHPATFIL))
            this.w_POSPAT = NVL(cp_ToDate(_read_.DHPOSPAT),cp_NullValue(_read_.DHPOSPAT))
            this.w_PSW = NVL(cp_ToDate(_read_.DHPSWPOS),cp_NullValue(_read_.DHPSWPOS))
            this.w_STADES = NVL(cp_ToDate(_read_.DHSTADES),cp_NullValue(_read_.DHSTADES))
            this.w_PAGEST = NVL(cp_ToDate(_read_.DHPAGEST),cp_NullValue(_read_.DHPAGEST))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0
            if this.w_BAUD = 0
              this.w_BAUD = 9600
            endif
            this.w_POSPAT = ADDBS(this.w_POSPAT)
            this.w_PATFIL = Alltrim(this.w_PATFIL)
            this.w_PSW = ALLTRIM(this.w_PSW)
            if this.w_PORTA<>1 AND this.w_PORTA<>2
              AddMsgNL("'La porta di connessione dal Registratore di Cassa deve essere impostata su COM1 o COM2%0%1",this,REPL("-",30))
              this.oParentObject.w_EDIT = .T.
            endif
            if (EMPTY(this.w_POSPAT) or EMPTY(this.w_PSW)) and NOT this.oParentObject.w_EDIT
              AddMsgNL("Definire nella maschera dispositivi installati,%0il path per la generazione del file txt per emissione scontrino%0e la chiave del registratore di cassa%0%1",this, REPL("-",30))
              this.oParentObject.w_EDIT = .T.
            endif
            if NOT EMPTY(this.w_SERIAL)
              AddMsgNL("Elaborazione iniziata alle: %1%0%2",this,Time(),REPL("-",30) )
              if this.pExec="A"
                * --- Emissione scontrino
                VQ_EXEC("..\GPOS\EXE\QUERY\GSPS_QES.VQR",this,"GENETXT")
                if USED("GENETXT")
                  this.Pag2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              else
                * --- Chiusura cassa
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              AddMsgNL("Scontrino non generato per errore su vendita%0%1",this,REPL("-",30) )
              this.oParentObject.w_EDIT = .T.
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
          else
            AddMsgNL("Dispositivo installato inesistente: reinstallare dalla maschera dispositivi installati%0%1",this,REPL("-",30) )
            this.oParentObject.w_EDIT = .T.
          endif
        else
          this.w_GEST = This.oParentObject.oParentObject
          this.w_FAMCAS = this.w_GEST.w_DHFAMCAS
          this.w_PORTA = this.w_GEST.w_DHNUMPOR
          this.w_POSPAT = Alltrim(this.w_GEST.w_DHPOSPAT)
          this.w_PATFIL = Alltrim(this.w_GEST.w_DHPATFIL)
          this.w_TMP = 30
          this.w_TMPOUT = 1
          if this.w_PORTA<>1 AND this.w_PORTA<>2
            AddMsgNL("'La porta di connessione dal Registratore di Cassa deve essere impostata su COM1 o COM2%0%1",this,REPL("-",30))
            this.oParentObject.w_EDIT = .T.
            i_retcode = 'stop'
            return
          endif
          AddMsgNL("Test in corso%0%1",this,REPL("-",30))
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pExec="B"
        if this.w_FISCALE
          if ah_YesNo("Si desidera rigenerare lo scontrino in seguito?")
            * --- Write into COR_RISP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COR_RISP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MD_ERROR ="+cp_NullLink(cp_ToStrODBC("S"),'COR_RISP','MD_ERROR');
                  +i_ccchkf ;
              +" where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                     )
            else
              update (i_cTable) set;
                  MD_ERROR = "S";
                  &i_ccchkf. ;
               where;
                  MDSERIAL = this.w_SERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into COR_RISP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COR_RISP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MD_ERROR ="+cp_NullLink(cp_ToStrODBC(" "),'COR_RISP','MD_ERROR');
                  +i_ccchkf ;
              +" where ";
                  +"MDSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                     )
            else
              update (i_cTable) set;
                  MD_ERROR = " ";
                  &i_ccchkf. ;
               where;
                  MDSERIAL = this.w_SERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Rileggo la gestione vendita negozio se in interroga (Record Modificato da un'altro utente)
          if Type("this.oParentObject.oParentObject.cFunction")="C" And this.oParentObject.oParentObject.cFunction="Query"
            this.oParentObject.oParentObject.Loadrec()
          endif
        endif
        this.oParentObject.ecpQuit()
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Cursore delle Righe di dettaglio per creazione TXT
    * --- Leggo parametri negozio, promozioni
    * --- Read from PAR_VDET
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_VDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAFLPROM"+;
        " from "+i_cTable+" PAR_VDET where ";
            +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and PACODNEG = "+cp_ToStrODBC(g_CODNEG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAFLPROM;
        from (i_cTable) where;
            PACODAZI = i_CODAZI;
            and PACODNEG = g_CODNEG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PAFLPROM = NVL(cp_ToDate(_read_.PAFLPROM),cp_NullValue(_read_.PAFLPROM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- In Terminal Serve si tratta di cartelle sul client quindi \\tsclinet\c\...
    this.w_TMPPAT = IIF(Empty(this.w_PATFIL),tempadhoc()+"\", this.w_PATFIL)
    * --- Per lancio eseguibile in terminal
    this.w_POSCOM = IIF(this.w_PORTA=1,"1",IIF(this.w_PORTA=2,"2",SPACE(3)))
    this.w_SR_DATI = this.w_TMPPAT+"SR_DATI.TXT"
    this.w_SR_OUT = this.w_TMPPAT+"SR_OUT.TXT"
    this.w_SR_ERR = this.w_TMPPAT+"SR_ERR.TXT"
    this.w_AHRPOS = this.w_TMPPAT+"AHRPOS.TXT" 
    this.w_SR_MATR = this.w_TMPPAT+"SR_MATR.TXT"
    if Type("g_ZBRGFILE")="C" And Not Empty(g_ZBRGFILE)
      * --- Se definisco la variabile pubblica g_ZBRGFILE nel Cnf posso indicare il nome del
      *     file da creare per il comando. Stesso nome che indico come parametro al 
      *     Zucchetti Bridge sul client
      this.w_SR_CMD = this.w_TMPPAT+Alltrim(g_ZBRGFILE)
      this.w_TMP_CMD = tempadhoc()+"\"+Alltrim(g_ZBRGFILE)
    else
      this.w_SR_CMD = this.w_TMPPAT+"COMANDO.TXT"
      this.w_TMP_CMD = tempadhoc()+"\"+"COMANDO.TXT"
    endif
    this.w_NOCART = .F.
    if NOT this.oParentObject.w_EDIT And this.w_FISCALE
      this.w_TMP = 180
      this.w_TMPOUT = 1
      this.w_RIGHE = 0
      this.w_SR_DATI_TEXT = ""
      if this.pExec="A"
        SELECT GENETXT
        if RECCOUNT() >0
          this.w_RECORD = "OPER,C"+ALLTRIM(RIGHT(STR(this.w_CODOPE,2),2))+";"
          this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
          * --- Scansione del temporaneo delle righe e assegnamento alle variabili locali
          *     w_RECORD : accodamento dei dati del temporaneo
          GO TOP
          SCAN FOR CPROWNUM<>0
          this.w_MDTIPRIG = NVL(MDTIPRIG," ")
          this.w_MDCODICE = NVL(MDCODICE,SPACE(20))
          this.w_MDDESART = ALLTRIM(NVL(MDDESART,SPACE(40)))
          this.w_MDFLRESO = NVL(MDFLRESO," ")
          this.w_MDSCONT1 = Nvl(MDSCONT1, 0)
          this.w_MDSCONT2 = Nvl(MDSCONT2, 0)
          this.w_MDSCONT3 = Nvl(MDSCONT3, 0)
          this.w_MDSCONT4 = Nvl(MDSCONT4, 0)
          this.w_MDFLOMAG = Nvl(MDFLOMAG, " ")
          this.w_MDUNIMIS = MDUNIMIS
          if this.w_MDTIPRIG="D"
            * --- Riga Descrittiva
            if NOT EMPTY(NVL(MDCODPRO,SPACE(10)))
              this.w_MDSCOPRO = NVL(MDSCOPRO,0)
              if this.w_MDSCOPRO<0
                * --- Riga sconti promozioni o sconti
                if Alltrim(MDCODPRO) = "##########"
                  * --- Se non � gi� presente nella riga precendente il comando COUP lo inserisco altrimenti devo sommare gli sconti
                  * --- Estraggo la porzione di testo dell'ultimo plud
                  *     Es:
                  *     OPER,C1;
                  *     PLUD,C[1],N4,P500,:;
                  *     PRNT,:aaaaaaaaaaaaaaaaaaa
                  *     PLUD,C[1],N4,P600,:;
                  *     COUP,V1000; 
                  *     PRNT,:eeeeeeeeeeeeeeeeeee
                  *     Prelevo 
                  *     PLUD,C[1],N4,P600,:;
                  *     COUP,V1000; 
                  *     PRNT,:eeeeeeeeeeeeeeeeeee
                  this.w_POS_CHAR = RAT(this.w_Separa + "PLUD", this.w_SR_DATI_TEXT)+LEN(this.w_Separa)
                  this.w_TEXT_PLUD = SUBSTR(this.w_SR_DATI_TEXT, this.w_POS_CHAR)
                  * --- Controllo la presenza del comando COUP
                  if AT(this.w_Separa + "COUP,", this.w_TEXT_PLUD) <> 0
                    this.w_RECORD = "COUP,V"+ALLTRIM(STR((ABS(this.w_MDSCOPRO)+ABS(this.w_MDSCOVEN_OLD))*100,20,0))+";"
                    this.w_SR_DATI_TEXT = STUFF(this.w_SR_DATI_TEXT, this.w_POS_CHAR + AT(this.w_Separa + "COUP,", this.w_TEXT_PLUD) + LEN(this.w_Separa) - 1, AT(this.w_Separa, this.w_TEXT_PLUD, 2) - AT(this.w_Separa + "COUP,", this.w_TEXT_PLUD), this.w_RECORD+ this.w_Separa)
                  else
                    * --- Sconto inserito a mano dall'utente sulla riga articolo
                    *     Passo comunque una riga di sconto a valore
                    this.w_RECORD = "COUP,V"+ALLTRIM(STR(ABS(this.w_MDSCOPRO)*100,20,0))+";"
                    * --- Se Cassa Ditron non posso eseguire il comando COUP dopo delle PRNT
                    if this.w_FAMCAS = "D"
                      this.w_SR_DATI_TEXT = STUFF(this.w_SR_DATI_TEXT, this.w_POS_CHAR + AT(this.w_Separa, this.w_TEXT_PLUD) + LEN(this.w_Separa) -1, 0, this.w_Record+this.w_Separa)
                    else
                      this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                    endif
                  endif
                else
                  if this.w_PAFLPROM<>"S"
                    * --- Sconto globale sulla vendita o sconto dovuto a promozione 
                    this.w_RECORD = "COST,V"+ALLTRIM(STR(ABS(this.w_MDSCOPRO)*100,20,0))+";"
                    this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                  endif
                endif
              else
                * --- Riga Maggiorazioni
                if this.w_MDSCOPRO>0
                  * --- Read from PAR_VDET
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PAR_VDET_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "PAARTMAG"+;
                      " from "+i_cTable+" PAR_VDET where ";
                          +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
                          +" and PACODNEG = "+cp_ToStrODBC(g_CODNEG);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      PAARTMAG;
                      from (i_cTable) where;
                          PACODAZI = i_CODAZI;
                          and PACODNEG = g_CODNEG;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_ARTMAG = NVL(cp_ToDate(_read_.PAARTMAG),cp_NullValue(_read_.PAARTMAG))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if NOT EMPTY(this.w_ARTMAG)
                    * --- Solo se definito articolo x Maggiorazioni nei Parametri Negozio
                    * --- Read from ART_ICOL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ARCODREP,ARDESART"+;
                        " from "+i_cTable+" ART_ICOL where ";
                            +"ARCODART = "+cp_ToStrODBC(this.w_ARTMAG);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ARCODREP,ARDESART;
                        from (i_cTable) where;
                            ARCODART = this.w_ARTMAG;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_MDCODREP = NVL(cp_ToDate(_read_.ARCODREP),cp_NullValue(_read_.ARCODREP))
                      this.w_DESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.w_RECORD = "PLUD,C[1],"
                    this.w_RECORD = this.w_RECORD+"N"+ALLTRIM(this.w_MDCODREP)+","
                    this.w_VALRIG = cp_ROUND(this.w_MDSCOPRO*100,0)
                    this.w_RECORD = this.w_RECORD+"P"+ALLTRIM(STR(ABS(this.w_VALRIG),20))+","
                    this.w_MDCODART = LEFT(IIF(this.w_STADES$"CT",this.w_ARTMAG,this.w_DESART),this.w_NUMFIS)
                    this.w_RECTEST = this.w_MDCODART
                    * --- Ricerca dei caratteri proibiti:  , ; : [ ]
                    this.Pag3()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    * --- Riga Fiscale :Numero reparto + Valore Fiscale + Codice Articolo
                    this.w_RECORD = this.w_RECORD+":"+UPPER(ALLTRIM(this.w_RECTEST))+";"
                    if NOT EMPTY(this.w_RECORD)
                      * --- Scrittura dei dati di riga nel TXT
                      this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                      this.w_RIGHE = this.w_RIGHE + 1
                    endif
                    if this.w_STADES="T"
                      * --- Riga descrittiva con descrizione articolo nel caso di riga con Codice + Descrizione
                      this.w_RECTEST = this.w_DESART
                      this.Pag3()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                      this.w_RECORD = "PRNT,:"+LEFT(ALLTRIM(this.w_RECTEST),this.w_NUMCAR)+";"
                      this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                    endif
                  else
                    AddMsgNL("Attenzione, impossibile generare lo scontrino%0Non � stato definito articolo per maggiorazioni nei parametri negozio%0%1",this, REPL("-",30) )
                    this.oParentObject.w_EDIT = .T.
                    this.Pag4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    i_retcode = 'stop'
                    return
                  endif
                endif
              endif
            endif
            this.w_RECTEST = this.w_MDDESART
            * --- Ricerca dei caratteri proibiti:  , ; : [ ]
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_RECORD = PSRIGDES(this.w_RECTEST,this.w_NUMCAR,"PRNT,:")
          else
            this.w_MDQTAMOV = NVL(MDQTAMOV,0)
            this.w_MDPREZZO = NVL(MDPREZZO,0)
            if this.w_MDQTAMOV<>1 And this.w_PASQTA<> "S"
              if this.w_MDFLOMAG<>"X"
                * --- Creo una riga descrittiva del Tipo PZ 3 Sconto Merce/Omaggio
                this.w_RECTEST = Alltrim(this.w_MDUNIMIS) + " " +Alltrim(STR(ABS(this.w_MDQTAMOV),12,3))+" "+ IIF(this.w_MDFLOMAG="S", ah_Msgformat("Sconto"), ah_Msgformat("Omaggio"))
                this.Pag3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_RECORD = this.w_RECTEST
                this.w_RECTEST = ""
              else
                this.w_RECTEST = STR(ABS(this.w_MDQTAMOV),12,g_PERPQT)
                * --- Ricerca dei caratteri proibiti:  , ; : [ ]
                this.Pag3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_RECORD = SPACE(3)+ALLTRIM(this.w_RECTEST)+"X"
                this.w_RECTEST = STR(ABS(this.w_MDPREZZO),20,g_PERPUL)
                * --- Ricerca dei caratteri proibiti:  , ; : [ ]
                this.Pag3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              * --- Riga descrittiva Quantit� x Prezzo
              this.w_RECORD = "PRNT,:"+LEFT(this.w_RECORD+ALLTRIM(this.w_RECTEST),this.w_NUMCAR)+";"
              this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
            endif
            this.w_RECORD = "PLUD,C[1],"
            this.w_MDCODREP = NVL(MDCODREP,SPACE(3))
            this.w_RECORD = this.w_RECORD+"N"+ALLTRIM(this.w_MDCODREP)+","
            if this.w_MDFLOMAG="S"
              * --- Se riga di tipo sconto merce, sullo scontrino riporto il netto riga
              this.w_VALRIG = CAVALRIG(this.w_MDPREZZO,this.w_MDQTAMOV, this.w_MDSCONT1,this.w_MDSCONT2,this.w_MDSCONT3,this.w_MDSCONT4,g_PERPVL) * 100
            else
              * --- Se riga normale o Omaggio Imp + Iva riporto il totale riga Lordo
              if this.w_PASQTA = "S"
                * --- Se devo passare le quantit�, passo solo il prezzo arrotondato a 2 cifre decimali
                this.w_VALRIG = cp_ROUND(this.w_MDPREZZO,2)*100
              else
                this.w_VALRIG = cp_ROUND(this.w_MDPREZZO*this.w_MDQTAMOV*100,0)
              endif
            endif
            this.w_RECORD = this.w_RECORD+"P"+ALLTRIM(STR(ABS(this.w_VALRIG),20))+","
            this.w_MDCODART = NVL(MDCODART,SPACE(20))
            this.w_MDCODART = LEFT(IIF(this.w_STADES$"CT",this.w_MDCODART,this.w_MDDESART),this.w_NUMFIS)
            this.w_RECTEST = this.w_MDCODART
            * --- Ricerca dei caratteri proibiti:  , ; : [ ]
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Riga Fiscale :Numero reparto + Valore Fiscale + Codice Articolo
            this.w_RECORD = this.w_RECORD+":"+UPPER(ALLTRIM(this.w_RECTEST))
            if this.w_MDFLRESO="S" Or Alltrim(MDCODPRO) = "#########F"
              * --- Nel caso di Reso aggiungo ,R
              *     Anche nel caso di servizio per Storno Importo Fidelity
              this.w_RECORD = this.w_RECORD+",R"
            endif
            if this.w_PASQTA= "S" And this.w_MDFLOMAG<>"S" And this.w_MDQTAMOV>1
              * --- Calcolo la quantit� da passare arrotondata a 2 cifre decimali e sostituisco la virgola con il punto
              this.w_QTA = STRTRAN(STR(cp_Round(this.w_MDQTAMOV,2),12,2),",",".")
              if Int(this.w_MDQTAMOV) = this.w_MDQTAMOV
                * --- Se non ho decimali passo solo la parte intera per problemi di velocit� sul registratore
                this.w_QTA = Alltrim(Left( this.w_QTA, At( ".", this.w_QTA ) -1 ) )
              endif
              this.w_RECORD = this.w_RECORD+",Q"+Alltrim(this.w_QTA)
            endif
            this.w_RECORD = this.w_RECORD+";"
          endif
          if NOT EMPTY(this.w_RECORD)
            * --- Scrittura dei dati di riga nel TXT
            this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record + IIF(this.w_MDTIPRIG="D", "", this.w_Separa)
            this.w_RIGHE = this.w_RIGHE + 1
            this.w_MDSCOVEN = MDSCOVEN
            if this.w_PAFLPROM="S" AND this.w_MDSCOVEN<0
              * --- Salvo il valore dello sconto, servir� nel caso in cui abbia due coup uno di seguito all'altro per avere il vecchio valore dello sconto
              *     (Problema sconto a livello di riga)
              this.w_MDSCOVEN_OLD = this.w_MDSCOVEN
              this.w_RECORD = "COUP,V"+ALLTRIM(STR(ABS(this.w_MDSCOVEN)*100,20,0))+";"
              this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
            endif
          endif
          if this.w_MDTIPRIG<>"D" And this.w_STADES="T"
            * --- Riga descrittiva con descrizione articolo
            *     solo se si sta scrivendo una riga di vendita e non una riga descrittiva altrimenti verrebbe ripetuta
            *     la riga descrittiva due volte
            * --- Se impostato codice + Descrizione, oltre al codice sulla riga di vendita, 
            *     creo una riga descrittiva con la descrizione.
            *     Codice + Descrizione
            this.w_RECTEST = Alltrim(Nvl(MDDESART,""))
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Divisione riga descrittiva su pi� righe secondo il numero di caratteri definito sul dispositivo
            this.w_RECORD = PSRIGDES(this.w_RECTEST,this.w_NUMCAR,"PRNT,:")
            this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record
          endif
          ENDSCAN
          * --- Chiusura scontrino
          do case
            case this.w_MDIMPABB > 0
              * --- Abbuono e contanti
              this.w_RECORD = "COST,V"+ALLTRIM(STR(ABS(this.w_MDIMPABB)*100,20,0))+",M;"
              this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
              this.w_RECORD = "CASH;"
              this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
            case this.w_MDIMPABB = 0
              * --- Contanti + pagamenti e conto cliente
              if this.w_MDTOTDOC - this.w_MDPAGCLI=0
                * --- Se chiusura scontrino a 0 .....
                this.w_RECORD = "CASH;"
                * --- questa parte per� non deve andare nello scontrino , tranne il caso di totale(MDTOTDOC)=0,  poich� significa che � tutto 
                *     a credito verso cliente e quindi non devo indicare il CASH ma solo il CRED
                if this.w_MDTOTDOC=0
                  this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                endif
              else
                * --- .....altrimenti indico il valore della chiusura
                this.Pag5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Il resto del totale lo metto sempre in contanti
                if (this.w_MDTOTDOC-this.w_MDPAGCLI-this.w_MDPAGASS-this.w_PAGFINASS-this.w_MDPAGCAR-this.w_PAGFINCAR)>0
                  this.w_RECORD = "CASH,V"+ALLTRIM(STR(ABS(this.w_MDTOTDOC-this.w_MDPAGCLI-this.w_MDPAGASS-this.w_PAGFINASS-this.w_MDPAGCAR-this.w_PAGFINCAR)*100,20,0))+";"
                  this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                endif
              endif
              if this.w_MDPAGCLI <> 0
                this.w_RECORD = "CRED,C1;"
                this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
              endif
            case this.w_MDIMPABB < 0
              if this.w_MDPAGCLI <> 0
                * --- Questa parte non viene mai eseguita poich� non � possibile inserire un 
                *     conto cliente e avere il resto contemporaneamente
                *     Lasciata per sicurezza e correttezza di copertura di ogni casistica
                if this.w_MDTOTDOC - this.w_MDPAGCLI=0
                  * --- Se chiusura scontrino a 0 .....
                  this.w_RECORD = "CASH;"
                  this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                else
                  * --- .....altrimenti indico il valore della chiusura
                  this.Pag5()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  * --- Il resto del totale lo metto sempre in contanti
                  if (this.w_MDTOTDOC-this.w_MDPAGCLI-this.w_MDPAGASS-this.w_PAGFINASS-this.w_MDPAGCAR-this.w_PAGFINCAR) > 0
                    this.w_RECORD = "CASH,V"+ALLTRIM(STR(ABS(this.w_MDTOTDOC-this.w_MDPAGCLI-this.w_MDPAGASS-this.w_PAGFINASS-this.w_MDPAGCAR-this.w_PAGFINCAR)*100,20,0))+";"
                    this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                  endif
                endif
                this.w_RECORD = "CRED,C1;"
                this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
              else
                * --- Contanti + Pagamenti con Resto
                if this.w_MDTOTDOC - this.w_MDIMPABB=0
                  * --- Se chiusura scontrino a 0 .....
                  this.w_RECORD = "CASH;"
                  this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                else
                  * --- .....altrimenti indico il valore della chiusura
                  this.Pag5()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  * --- Il resto del totale lo metto sempre in contanti
                  if (this.w_MDTOTDOC-this.w_MDIMPABB-this.w_MDPAGASS-this.w_PAGFINASS-this.w_MDPAGCAR-this.w_PAGFINCAR) > 0
                    this.w_RECORD = "CASH,V"+ALLTRIM(STR(ABS(this.w_MDTOTDOC-this.w_MDIMPABB-this.w_MDPAGASS-this.w_PAGFINASS-this.w_MDPAGCAR-this.w_PAGFINCAR)*100,20,0))+";"
                    this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
                  endif
                endif
              endif
          endcase
        endif
      else
        this.w_RECORD = "ZKEY;"
        this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
        this.w_RECORD = "FUNC,N1;"
        this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
        this.w_RECORD = "REGK;"
        this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
        this.w_RIGHE = 3
      endif
      if this.w_RIGHE>0
        * --- Il file � stato generato con successo
        if this.pExec="A"
          * --- Sbianco il campo MD_ERROR per eventuali errori precedenti
          * --- Write into COR_RISP
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.COR_RISP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MD_ERROR ="+cp_NullLink(cp_ToStrODBC(" "),'COR_RISP','MD_ERROR');
                +i_ccchkf ;
            +" where ";
                +"MDSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MD_ERROR = " ";
                &i_ccchkf. ;
             where;
                MDSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if STRTOFILE(this.w_SR_DATI_TEXT, this.w_SR_DATI) = 0
          AddMsgNL("Attenzione, non � possibile creare il file .TXT%0%1",this, REPL("-",30) )
          this.oParentObject.w_EDIT = .T.
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        * --- Controllo se devo eliminare righe descrittive consecutive (Micropos)
        if this.w_FAMCAS = "M"
          * --- Se Micropos non posso stampare pi� di 2 righe descrittive consecutive
          this.w_NUMRIGHEDES = 2
          This.CancellaRigheDesConsecutive(ALLTRIM(this.w_SR_DATI), this.w_NUMRIGHEDES)
        endif
      else
        AddMsgNL("Attenzione, non � stata inserita nessuna riga nel .TXT%0%1",this, REPL("-",30) )
        this.oParentObject.w_EDIT = .T.
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Costruisco le path che mi servono per il controllo file
      this.w_RECORD = " "
      this.w_POSIZ = 0
      if FILE(this.w_SR_ERR)
        ERASE(this.w_SR_ERR)
      endif
      if FILE(this.w_SR_OUT)
        ERASE(this.w_SR_OUT)
      endif
    endif
    * --- Clientpat conterr� ad esempio \\TSCLIENT\C\
    *     e UNITA invece C:\
    this.w_CLIENTPAT = Upper(SUBSTR(Upper(Alltrim(this.w_PATFIL)),1,AT("\",Upper(Alltrim(this.w_PATFIL)),3)+2))
    this.w_UNITA = Upper(Left(Right(Alltrim(this.w_CLIENTPAT),2),1))+":\"
    w_Handle = fcreate(ALLTRIM(this.w_AHRPOS), 0)
    * --- Creazione File di configurazione
    if w_Handle = -1
      AddMsgNL("Attenzione, non � possibile creare il file .TXT%0%1",this, REPL("-",30) )
      this.oParentObject.w_EDIT = .T.
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    else
      * --- COM: numero porta di connessione (1,2)
      *     TST: 0 scontrino fiscale, 1 scontrino di prova
      *     INP: path del file dati 
      *     OUT: path di creazione del file di output
      *     ERR: path di creazione del file di errori
      *     PWD: password del registratore di cassa
      *     QTA: 0 non passo qt� allo scontrino, 1 passo le qt� alla riga fiscale
      *     BDR: Velocit� di trasmissione sulla porta
      *     
      *     Se terminal server devo inserire le path che puntano a cartelle del client
      if g_TERMINALSS
        this.w_SR_ERR_C = Upper(Strtran(Upper(Alltrim(this.w_SR_ERR)),Upper(Alltrim(this.w_CLIENTPAT)),Upper(Alltrim(this.w_UNITA))))
        this.w_SR_DATI_C = Upper(StrTran(Upper(Alltrim(this.w_SR_DATI)),Upper(Alltrim(this.w_CLIENTPAT)),Upper(Alltrim(this.w_UNITA))))
        this.w_SR_OUT_C = Upper(StrTran(Upper(Alltrim(this.w_SR_OUT)),Upper(Alltrim(this.w_CLIENTPAT)),Upper(Alltrim(this.w_UNITA))))
         
 w_t = fwrite(w_Handle, "COM:"+this.w_POSCOM+this.w_Separa) 
 w_t = fwrite(w_Handle, "TST:"+IIF(this.w_FISCALE,"0","1")+this.w_Separa) 
 w_t = fwrite(w_Handle, "INP:"+IIF(this.w_FISCALE,this.w_SR_DATI_C,"")+this.w_Separa) 
 w_t = fwrite(w_Handle, "OUT:"+this.w_SR_OUT_C+this.w_Separa) 
 w_t = fwrite(w_Handle, "ERR:"+this.w_SR_ERR_C+this.w_Separa) 
 w_t = fwrite(w_Handle, "PWD:"+this.w_PSW+this.w_Separa)
        if this.w_FAMCAS="H"
          * --- Se si seleziona la famiglia Custom e si attiva il check Pagamenti estesi si aggiunge il parametro EXP:1/0 (Se il check � attivo, si mette il valore 1=gestisce nuovi pagamenti / altrimenti 0 = utilizza le attuali forma di pagamento) 
          *     In questo modo il driver personalizzato permette il passaggio alla cassa del dettaglio pagamenti
           
 w_t = fwrite(w_Handle, "EXP:"+IIF( this.w_PAGEST="S","1","0")+this.w_Separa)
        endif
        * --- Solo se scontrino fiscale aggiungo quantit� e velocit� di trasmissione
        *     Nel caso di test non ce n'� bisogno
        if this.w_FISCALE
           
 w_t = fwrite(w_Handle, "QTA:"+IIF(this.w_PASQTA="S","1","0")+this.w_Separa) 
 w_t = fwrite(w_Handle, "BDR:"+Alltrim(STR(this.w_BAUD,5,0))+this.w_Separa)
        endif
      else
         
 w_t = fwrite(w_Handle, "COM:"+this.w_POSCOM+this.w_Separa) 
 w_t = fwrite(w_Handle, "TST:"+IIF(this.w_FISCALE,"0","1")+this.w_Separa) 
 w_t = fwrite(w_Handle, "INP:"+IIF(this.w_FISCALE,this.w_SR_DATI,"")+this.w_Separa) 
 w_t = fwrite(w_Handle, "OUT:"+this.w_SR_OUT+this.w_Separa) 
 w_t = fwrite(w_Handle, "ERR:"+this.w_SR_ERR+this.w_Separa) 
 w_t = fwrite(w_Handle, "PWD:"+IIF(this.w_FISCALE,this.w_PSW,"")+this.w_Separa)
        if this.w_FAMCAS="H"
          * --- Se si seleziona la famiglia Custom e si attiva il check Pagamenti estesi si aggiunge il parametro EXP:1/0 (Se il check � attivo, si mette il valore 1=gestisce nuovi pagamenti / altrimenti 0 = utilizza le attuali forma di pagamento) 
          *     In questo modo il driver personalizzato permette il passaggio alla cassa del dettaglio pagamenti
           
 w_t = fwrite(w_Handle, "EXP:"+IIF( this.w_PAGEST="S","1","0")+this.w_Separa)
        endif
        * --- Solo se scontrino fiscale aggiungo quantit� e velocit� di trasmissione
        *     Nel caso di test non ce n'� bisogno
        if this.w_FISCALE
           
 w_t = fwrite(w_Handle, "QTA:"+IIF(this.w_PASQTA="S","1","0")+this.w_Separa) 
 w_t = fwrite(w_Handle, "BDR:"+Alltrim(STR(this.w_BAUD,5,0))+this.w_Separa)
        endif
      endif
    endif
    w_t = FCLOSE(w_Handle)
    * --- Creazione della path corretta con l'eseguibile di ogni famiglia di registratori interfacciata
    do case
      case this.w_FAMCAS="S"
        * --- Compatibile Sarema
        this.w_DIRCAS = "Sarema\"
      case this.w_FAMCAS="M"
        * --- Compatibile Sweda/Micropos
        this.w_DIRCAS = "Micropos\"
      case this.w_FAMCAS="N"
        * --- Nucleo RCH
        this.w_DIRCAS = "Nucleo\"
      case this.w_FAMCAS="O" Or this.w_FAMCAS="Q"
        * --- Olivetti CRF 4080
        *     Olivetti mod. gestionale
        this.w_DIRCAS = "Olivetti\"
      case this.w_FAMCAS="P"
        * --- Sarema Protocollo Controllato
        this.w_DIRCAS = "SF1000\"
      case this.w_FAMCAS="Z"
        * --- Sweda con Sarema Language
        this.w_DIRCAS = "Labor\"
      case this.w_FAMCAS="R"
        * --- RCH
        this.w_DIRCAS = "G2000\"
      case this.w_FAMCAS="V"
        * --- Vandoni
        this.w_DIRCAS = "Vandoni\"
      case this.w_FAMCAS="W"
        * --- Sweda Master
        this.w_DIRCAS = "Master\"
      case this.w_FAMCAS="A"
        * --- Mizar
        this.w_DIRCAS = "MIZAR\"
      case this.w_FAMCAS="B"
        * --- G3000
        this.w_DIRCAS = "G3000\"
      case this.w_FAMCAS="C"
        * --- SW204
        this.w_DIRCAS = "SW204\"
      case this.w_FAMCAS="D"
        * --- DITRON
        this.w_DIRCAS = "DITRON\"
      case this.w_FAMCAS="E"
        * --- S5000
        this.w_DIRCAS = "S5000\"
      case this.w_FAMCAS="F"
        * --- NCR2215
        this.w_DIRCAS = "NCR\"
      case this.w_FAMCAS="G"
        * --- Epson
        this.w_DIRCAS = "Epson\"
      case this.w_FAMCAS="H"
        * --- Custom Engineering
        this.w_DIRCAS = "CustomEng\"
      case this.w_FAMCAS="I"
        * --- Fasy2WincorN2
        this.w_DIRCAS = "FasyScirocco_WincorOpportunityEJ\"
      case this.w_FAMCAS="L"
        * --- FasyMistral
        this.w_DIRCAS = "FasyMistral_WincorButterfly\"
      case this.w_FAMCAS="T"
        * --- Coris
        this.w_DIRCAS = "Coris\"
    endcase
    this.w_POSEXE = CHR(34)+(this.w_POSPAT)+(this.w_DIRCAS)+"adhocEmettitoreScontrino.exe"+CHR(34)+" -C"+(this.w_AHRPOS)
    if NOT FILE(ALLTRIM((this.w_POSPAT)+(this.w_DIRCAS)+"adhocEmettitoreScontrino.exe"))
      * --- Errore: eseguibile adhocemettitorescontrino.exe non trovato
      this.w_oPART = this.w_oMESS.addmsgpartNL("Attenzione, impossibile trovare adhocemettitorescontrino.Exe%0Controllare il path emettitore scontrino nel dispositivo hardware %1")
      this.w_oPART.addParam(this.w_CODCAS)     
      this.w_oMESS.AddMsgPartNL("Se non si � ancora installato ad hoc emettitore scontrino%0eseguire il download dello zip congruente con il proprio registratore di cassa%0dal sito Internet www.zucchettitam.it")     
      this.w_oPART = this.w_oMESS.addmsgpartNL("Ctrl + V per copiare l'indirizzo sul browser%0%1")
      this.w_oPART.addParam(REPL("-",30))     
      this.w_MESS = this.w_oMESS.ComposeMessage()
      _CLIPTEXT="www.zucchettitam.it"
      AddMsgNL("%1",this, this.w_MESS)
      this.oParentObject.w_EDIT = .T.
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    if g_TERMINALSS
      this.w_CLIENTEXE = Upper(StrTran(Upper(Alltrim(this.w_POSPAT)),Upper(Alltrim(this.w_CLIENTPAT)),Upper(Alltrim(this.w_UNITA)))) + this.w_DIRCAS
      this.w_CLIENTPOS = Upper(StrTran(Upper(Alltrim(this.w_AHRPOS)),Upper(Alltrim(this.w_CLIENTPAT)),Upper(Alltrim(this.w_UNITA))))
      * --- EXE: Path di esecuzione AdHocEmettitorescontrino.Exe
      *     POS: Path di posizionamento AHRPOS.TXT
      cCommand = "! /N" + CHR(34)+Alltrim(this.w_CLIENTEXE)+"adhocEmettitoreScontrino.exe" +CHR(34)+ " -C"+(this.w_CLIENTPOS)
      if this.w_FLMBRG = "P"
        * --- Trasmetto al Bridge Service di eseguire adhocEmettitoreScontrino
        this.w_CONNECT = ZBrgCli( cCommand , GETENV("clientname"), IIF(type("g_ZBRGPORT")="N", g_ZBRGPORT, 63333))
        if this.w_CONNECT
          AddMsgNL("Eseguita connessione a Zucchetti Bridge%0%1",this, REPL("-",30) )
        else
          AddMsgNL("Impossibile effettuare la connessione a Zucchetti Bridge%0%1",this, REPL("-",30) )
          this.oParentObject.w_EDIT = .T.
          i_retcode = 'stop'
          return
        endif
      else
        * --- Nel caso in cui seleziono 'Timer' come modalit� dello Zucchetti Bridge creo prima
        *     il file nella cartella temp dell'utente sul server e una volta creato lo copio nella cartella
        *     del client dove lo zucchetti bridge lo aspetta. Appena viene copiato sar� eseguito il comando
        *     al suo interno
        this.w_CONNECT = ZBrgCli( cCommand , GETENV("clientname"), IIF(type("g_ZBRGPORT")="N", g_ZBRGPORT, 63333), this.w_TMP_CMD, this.w_SR_CMD)
        if this.w_CONNECT
          AddMsgNL("Creato file di comando per Zucchetti Bridge%0%1",this, REPL("-",30) )
        else
          AddMsgNL("Impossibile creare il file di comando sul client%0%1",this, REPL("-",30) )
          this.oParentObject.w_EDIT = .T.
          i_retcode = 'stop'
          return
        endif
      endif
    else
      macro = this.w_POSEXE
      * --- Lancio l'eseguibile 
      ! /N &macro
    endif
    this.w_FINESCO = (this.w_POSPAT)+(this.w_DIRCAS)+"FINESCO."+IIF(this.w_PORTA=1,"001",IIF(this.w_PORTA=2,"002",SPACE(3)))
    this.w_FINESCO2 = (this.w_TMPPAT)+"FINESCO."+IIF(this.w_PORTA=1,"001",IIF(this.w_PORTA=2,"002",SPACE(3)))
    * --- Se � stata definita la variabile g_POSTIME nel cnf risetto il tempo di attesa di ogni ricerca
    *     del file finesco. In questo modo riesco a diminuire i tempi morti. Nel caso di test aspetto sempre 3 secondi
    if Type("g_POSTIME")="N" And g_POSTIME<>0
      this.w_TESTTMP = IIF(this.w_FISCALE,g_POSTIME,3)
    else
      this.w_TESTTMP = IIF(this.w_FISCALE,10,3)
    endif
    do while this.w_TMPOUT<this.w_TMP 
      * --- Per tre minuti controllo se esiste il file FINESCO altrimenti emissione scontrino in errore
      if NOT this.w_NOCART
        AddMsgNL("Emissione scontrino in corso%0%1",this, REPL("-",30) )
      endif
      WAIT "" TIMEOUT this.w_TESTTMP
      * --- Se non ho definito la variabile g_POSTIME nel cnf la prima volta TESTTMP vale 10
      *     ma dalla seconda ricerca diminuisco il tempo di ricerca a 2 secondi
      this.w_TESTTMP = IIF(this.w_TESTTMP=10,2,this.w_TESTTMP)
      if FILE(ALLTRIM(this.w_FINESCO)) OR FILE(ALLTRIM(this.w_FINESCO2))
        this.oParentObject.w_EDIT = .F.
        EXIT
      else
        if NOT this.w_NOCART
          AddMsgNL("Attenzione, il registratore di cassa non ha rilasciato nessun risultato%0%1",this,REPL("-",30) )
          this.oParentObject.w_EDIT = .T.
        endif
      endif
      * --- Controllo in contemporanea se � stato emesso il file SR_ERR
      if FILE(ALLTRIM(this.w_SR_ERR)) AND NOT this.w_NOCART
        AddMsgNL("Attenzione, il registratore di cassa ha segnalato errori%0%1",this, REPL("-",30) )
        w_Handle3 = fopen(ALLTRIM(this.w_SR_ERR), 0)
        if w_Handle3 <> -1
          this.w_RECORD = fgets(w_Handle3)
           
 AddMsgNL(this.w_RECORD,this) 
 AddMsgNL("%1",this, REPL("-",30) )
          if left(this.w_RECORD,15) = "Errore generico"
            * --- Errore di sintassi nella creazione file TXT: potrebbero essere anche i dati errati
            AddMsgNL("Attenzione, controllare che il codice operatore e il codice reparto%0siano nei limiti consentiti dalla cassa%0%1",this, REPL("-",30) )
          endif
          if left(this.w_RECORD,10) = "Fine carta"
            * --- Fine carta
            this.oParentObject.w_EDIT = .F.
            this.w_NOCART = .T.
            this.w_TMP = 300
          endif
        endif
        w_t = FCLOSE(w_Handle3)
        if NOT this.w_NOCART
          EXIT
        endif
      endif
      if this.w_NOCART
        if this.w_LENTMPATT<>0
          this.w_StrDaIns = LEFT(This.oParentObject.w_Msg, this.w_LENTMPATT)
          This.oParentObject.w_MSG=""
          this.w_LENTMPATT = 0
        else
          this.w_StrDaIns = ""
        endif
        this.w_LENTMPATT = len(ALLTRIM(This.oParentObject.w_MSG)) + len(ALLTRIM(this.w_StrDaIns))
        AddMsgNL("%1Tempo di attesa per cambio carta: %2 secondi%0%3",this,this.w_StrDaIns, STR(this.w_TMP-this.w_TMPOUT,3,0), REPL("-",30) )
      endif
      this.w_TMPOUT = this.w_TMPOUT+3
    enddo
    if NOT this.oParentObject.w_EDIT And this.w_CARDAT <> "N" And this.w_FISCALE And !this.oParentObject.w_CHIUSURA
      if FILE(ALLTRIM(this.w_SR_OUT))
        * --- Assegnamento delle variabili con i dati di ritorno dal registratore di Cassa
        w_Handle = fopen(ALLTRIM(this.w_SR_OUT), 0)
        if w_Handle<> - 1
          if Feof(w_Handle)
            * --- SR_DATI restituito vuoto
            AddMsgNL("La cassa non restituisce risultati. Attivare l'opzione di restituzione dell''ECO 792 subtotale%0%1",this, REPL("-",30) )
            AddMsgNL("Non verr� aggiornato il numero scontrino sulla vendita%0%1",this, REPL("-",30) )
          else
            * --- Lettura Ora Vendita
            do while LEFT(this.w_RECORD,4)<>"TIME" and Not(Feof(w_Handle))
              this.w_RECORD = fgets(w_Handle)
            enddo
            this.w_ORAVEN = ALLTRIM(SUBSTR(this.w_RECORD,AT(":",this.w_RECORD)-2,2))
            this.w_MINVEN = ALLTRIM(SUBSTR(this.w_RECORD,AT(":",this.w_RECORD)+1,2))
            * --- Lettura Data Registrazione
            do while LEFT(this.w_RECORD,4)<>"DATE" and Not(Feof(w_Handle))
              this.w_RECORD = fgets(w_Handle)
            enddo
            this.w_DATREG = cp_CharToDate(ALLTRIM(SUBSTR(this.w_RECORD,AT("DATE",this.w_RECORD)+5,8)))
            * --- Lettura Numero Scontrino
            this.w_NUMSCO = INT(VAL(ALLTRIM(SUBSTR(this.w_RECORD,AT("SCONTR. N.",this.w_RECORD)+10,4))))
            w_t = FCLOSE(w_Handle)
            * --- Lettura Matricola Cassa
            w_Handle = fopen(ALLTRIM(this.w_SR_MATR), 0)
            if w_Handle <> -1
              this.w_RECORD = fgets(w_Handle)
              this.w_MATCAS = ALLTRIM(LEFT(this.w_RECORD,8))
            else
              AddMsgNL("Impossibile leggere il numero matricola rilasciato dalla cassa%0%1",this, REPL("-",30) )
            endif
          endif
          w_t = FCLOSE(w_Handle)
        else
          AddMsgNL("Impossibile leggere i dati rilasciati dalla cassa%0%1",this, REPL("-",30) )
        endif
        AddMsgNL("Memorizzazione dati da registratore di cassa%0%1",this, REPL("-",30) )
      endif
      * --- Controllo ritorno dati da registratore
      if this.w_CARDAT = "C"
        do GSPS_KCD with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not this.w_OK_AGG
          * --- Se non aggiorno i dati scelti devo mantenere quelli inizializzati sulla maschera di vendita
          *     tranne numero scontrino
          this.w_DATREG = this.w_DDATREG
          this.w_ORAVEN = this.w_DORAVEN
          this.w_MINVEN = this.w_DMINVEN
          this.w_MATCAS = this.w_DMATCAS
          this.w_NUMSCO = 0
        endif
      else
        this.w_DATREG = IIF(Not Empty(this.w_DATREG),this.w_DATREG,this.w_DDATREG)
        this.w_ORAVEN = IIF(Not Empty(this.w_ORAVEN),this.w_ORAVEN,this.w_DORAVEN)
        this.w_MINVEN = IIF(Not Empty(this.w_MINVEN),this.w_MINVEN,this.w_DMINVEN)
        this.w_MATCAS = IIF(Not Empty(this.w_MATCAS),this.w_MATCAS,this.w_DMATCAS)
        this.w_NUMSCO = IIF(Not Empty(this.w_NUMSCO),this.w_NUMSCO, 0)
      endif
      * --- Scrittura Dati Ricevuti dal Registratore
      *     Numero Scontrino, Matricola, Data Registrazione, Ora
      * --- Try
      local bErr_03184D50
      bErr_03184D50=bTrsErr
      this.Try_03184D50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03184D50
      * --- End
    endif
    if Type("this.oParentObject") = "O" And Not this.oParentObject.w_EDIT
      * --- Chiusura maschera di Controllo Emissione Scontrino GSPS_KES
      this.oParentObject.ecpQuit()
    endif
  endproc
  proc Try_03184D50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into COR_RISP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COR_RISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MDDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'COR_RISP','MDDATREG');
      +",MDORAVEN ="+cp_NullLink(cp_ToStrODBC(this.w_ORAVEN),'COR_RISP','MDORAVEN');
      +",MDMINVEN ="+cp_NullLink(cp_ToStrODBC(this.w_MINVEN),'COR_RISP','MDMINVEN');
      +",MDNUMSCO ="+cp_NullLink(cp_ToStrODBC(this.w_NUMSCO),'COR_RISP','MDNUMSCO');
      +",MDMATSCO ="+cp_NullLink(cp_ToStrODBC(this.w_MATCAS),'COR_RISP','MDMATSCO');
          +i_ccchkf ;
      +" where ";
          +"MDSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MDDATREG = this.w_DATREG;
          ,MDORAVEN = this.w_ORAVEN;
          ,MDMINVEN = this.w_MINVEN;
          ,MDNUMSCO = this.w_NUMSCO;
          ,MDMATSCO = this.w_MATCAS;
          &i_ccchkf. ;
       where;
          MDSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pagina Eseguita per il controllo dei caratteri proibiti nelle stringhe passate al registratore di cassa
    this.w_RECTEST = STRTRAN(this.w_RECTEST,"[","(")
    this.w_RECTEST = STRTRAN(this.w_RECTEST,"]",")")
    this.w_RECTEST = STRTRAN(this.w_RECTEST,";"," ")
    this.w_RECTEST = STRTRAN(this.w_RECTEST,":"," ")
    this.w_RECTEST = STRTRAN(this.w_RECTEST,",",".")
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiude cursore
    USE IN SELECT("GENETXT")
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- .....altrimenti indico il valore della chiusura
    this.w_PAGFINASS = 0
    this.w_PAGFINCAR = 0
    do case
      case this.w_MOFINASS="A"
        this.w_PAGFINASS = this.w_MDPAGFIN
      case this.w_MOFINASS="B"
        this.w_PAGFINCAR = this.w_MDPAGFIN
    endcase
    * --- Assegno
    if this.w_MDPAGASS>0 Or this.w_PAGFINASS>0
      this.w_RECORD = "CHEQ,V"+ALLTRIM(STR(ABS(this.w_MDPAGASS+this.w_PAGFINASS)*100,20,0))+";"
      this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
    endif
    * --- Carta/Bancomat
    if this.w_MDPAGCAR>0 Or this.w_PAGFINCAR>0
      this.w_RECORD = "CARD,V"+ALLTRIM(STR(ABS(this.w_MDPAGCAR+this.w_PAGFINCAR)*100,20,0))+";"
      this.w_SR_DATI_TEXT = this.w_SR_DATI_TEXT + this.w_Record+this.w_Separa
    endif
  endproc


  proc Init(oParentObject,pExec,pSerial)
    this.pExec=pExec
    this.pSerial=pSerial
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='COR_RISP'
    this.cWorkTables[2]='DIS_HARD'
    this.cWorkTables[3]='PAR_VDET'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='MOD_VEND'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- gsps_bgs
  *--- Legge il file al contrario e ritorna la stringa letta finch� non 
  *--- trova cSearch
  Func FReadReverse(pFile, cSearch, pOccurs)
     Local cStr, nPos, bNoExit, cRevSearch, nOccurs
     nOccurs = IIF(Type("pOccurs")<>'L', pOccurs, 1)
     bNoExit = .t.
     cStr = ""
     cRevSearch = This.ReverseStr(cSearch)
     Do while bNoExit Or nOccurs>0
       nPos = FSeek(pFile, -1, 1)
       cStr = cStr + FRead(pFile, 1)
       nPos = FSeek(pFile, -1, 1)
       bNoExit = At(cRevSearch, cStr)=0
       nOccurs = IIF(!bNoExit, nOccurs - 1, nOccurs)     
     EndDo
     Return This.ReverseStr(cStr)
  Endfunc
  
  Func ReverseStr(cStr)
     *--- Rigiro la stringa da cercare
     Local cRevSearch, i
     cRevSearch = ""
     For i = len(cStr) To 1 Step -1
       cRevSearch = cRevSearch + SubStr(cStr, i ,1)
     Next
     Return cRevSearch
  EndFunc
  
  Function FGetSReverse(pFile)
     Local cStr
     cStr = This.FReadReverse(pFile, CHR(13)+CHR(10))
     Return cStr
  Endfunc
  *---- CancellaRigheDescrittiveConsecutive
  Proc CancellaRigheDesConsecutive(cFile, NumMaxRig)
   local strFile, l_SAFETY, nLines, prntCount, strNewFile
   local aFile(1)
   strNewFile = ''
   strFile = FileToStr(cFile)
   nLines = ALINES(aFile, strFile, 16)
   prntCount = 0
   For l_i=1 to nLines
     If Left(aFile[l_i], 4) = 'PRNT'
       prntCount = prntCount + 1
       If prntCount <= NumMaxRig
         strNewFile = strNewFile + aFile[l_i]
       Endif
     Else
       prntCount = 0
       strNewFile = strNewFile + aFile[l_i]
     Endif
   Endfor
   *--- Salvo il file
   l_SAFETY=SET('SAFETY')
   SET SAFETY OFF
   StrToFile(strNewFile, cFile)
   SET SAFETY &l_SAFETY
  Endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec,pSerial"
endproc
