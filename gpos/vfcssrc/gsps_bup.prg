* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bup                                                        *
*              Ult.vendite/acquisti per C/F                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-07                                                      *
* Last revis.: 2003-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bup",oParentObject)
return(i_retval)

define class tgsps_bup as StdBatch
  * --- Local variables
  w_CODART = space(20)
  w_DATDOC = ctod("  /  /  ")
  w_FLULPV = space(1)
  w_FLULCA = space(1)
  w_ZOOM = space(10)
  w_DATREG = ctod("  /  /  ")
  w_CODVAL = space(3)
  w_VALULT = 0
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_SERIAL = space(10)
  w_CPROWNUM = 0
  w_NUMRIF = 0
  w_PREZZO = 0
  w_PREZZO1 = 0
  w_LORDO = space(1)
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_FLOMAG = space(20)
  w_PREZZOSM = 0
  w_CODICE = space(20)
  w_VALUNI = 0
  w_TIPDOC = space(5)
  w_OCAOVAL = 0
  w_VALUN1 = 0
  w_QTAUM1 = 0
  w_UNMIS1 = space(3)
  w_CODCON = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora la situazione degli Ultimi Prezzi a Cliente o Costi da Fornitore (da GSPS_KUP) 
    * --- Al termine della Elaborazione Popola il Cursore dello Zoom della Maschera chiamante
    * --- da GSPS_KUP (Vendite)
    this.w_ZOOM = this.oParentObject.w_ZoomPRE
    ND = this.oParentObject.w_ZoomPRE.cCursor
    This.OparentObject.NotifyEvent("Ricerca")
    WAIT WINDOW " Lettura Documenti...  " NOWAIT
    this.w_CODART = this.oParentObject.w_MDCODART
    this.w_CODCON = this.oParentObject.w_MDCODCLI
    vq_exec("..\GPOS\EXE\QUERY\GSPSVKCA",this, "TOTDOC")
    if USED("TOTDOC")
      SELECT TOTDOC
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MVDATREG,""))
      this.w_DATREG = CP_TODATE(MVDATREG)
      this.w_CODVAL = this.oParentObject.w_MDCODVAL
      this.w_OCAOVAL = MVCAOVAL
      this.w_TIPDOC = MVTIPDOC
      this.w_UNIMIS = MVUNIMIS
      this.w_UNMIS1 = ARUNMIS1
      this.w_QTAMOV = MVQTAMOV
      this.w_QTAUM1 = MVQTAUM1
      this.w_PREZZO = MVPREZZO
      this.w_PREZZO1 = (this.w_PREZZO/this.w_OCAOVAL)*this.oParentObject.w_MDCAOVAL
      this.w_PREZZO = cp_ROUND(this.w_PREZZO1,this.oParentObject.w_DECUNI)
      this.w_LORDO = "S"
      this.w_SCONT1 = MVSCONT1
      this.w_SCONT2 = MVSCONT2
      this.w_SCONT3 = MVSCONT3
      this.w_SCONT4 = MVSCONT4
      this.w_FLOMAG = MVFLOMAG
      this.w_VALUNI = cp_ROUND((this.w_PREZZO1 * (1+this.w_SCONT1/100)*(1+this.w_SCONT2/100)*(1+this.w_SCONT3/100)*(1+this.w_SCONT4/100)),this.oParentObject.w_DECUNI)
      this.w_VALUN1 = IIF(this.w_QTAMOV=this.w_QTAUM1 OR this.w_QTAUM1=0, this.w_VALUNI, cp_ROUND((this.w_VALUNI * this.w_QTAMOV) / this.w_QTAUM1, this.oParentObject.w_DECUNI))
      this.w_SERIAL = MVSERIAL
      this.w_CPROWNUM = CPROWNUM
      this.w_NUMRIF = MVNUMRIF
      this.w_VALULT = MVVALULT
      this.w_CODICE = IIF(this.w_FLOMAG="X"," ","OMAGGIO")
      INSERT INTO (ND) ;
      (MVDATREG, MVCODVAL, MVPREZZO, MVUNIMIS, MVQTAMOV, OMAGGIO, MVSCONT1, MVSCONT2, MVSCONT3, MVSCONT4, ;
      MVPREZZOSM, LORDO, ARUNMIS1, MVQTAUM1, MVPREZZOU1, MVSERIAL, MVNUMRIF, CPROWNUM, MVTIPDOC) VALUES ;
      (this.w_DATREG, this.w_CODVAL, this.w_PREZZO, this.w_UNIMIS, this.w_QTAMOV, this.w_CODICE, this.w_SCONT1, this.w_SCONT2, this.w_SCONT3, this.w_SCONT4, ;
      this.w_VALUNI, this.w_LORDO, this.w_UNMIS1, this.w_QTAUM1, this.w_VALUN1, this.w_SERIAL, this.w_NUMRIF, this.w_CPROWNUM, this.w_TIPDOC)
      ENDSCAN
      * --- chiude il cursore
      if USED("TOTDOC")
        SELECT TOTDOC
        USE
      endif
      select (ND)
      go top
      this.w_ZOOM = this.oParentObject.w_ZoomPRE.refresh()
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
