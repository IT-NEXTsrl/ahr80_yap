* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_kim                                                        *
*              Importa documenti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_129]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-02                                                      *
* Last revis.: 2012-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_kim",oParentObject))

* --- Class definition
define class tgsps_kim as StdForm
  Top    = 0
  Left   = 16

  * --- Standard Properties
  Width  = 791
  Height = 449+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-23"
  HelpContextID=1484695
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  MOD_VEND_IDX = 0
  CLI_VEND_IDX = 0
  cPrg = "gsps_kim"
  cComment = "Importa documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MDDATREG = ctod('  /  /  ')
  w_MDTIPVEN = space(5)
  w_MDCODCLI = space(15)
  w_DOCCO1 = space(5)
  w_DOCCO2 = space(5)
  w_DOCCO3 = space(5)
  w_CODCON = space(15)
  w_TESTERR = .F.
  w_OBTEST = ctod('  /  /  ')
  w_FLNSRI = space(1)
  w_FLVSRI = space(1)
  w_CALSER = space(10)
  w_SERIAL = space(10)
  w_TIPART = space(1)
  w_DESART = space(40)
  w_SELEZI = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_EVAINI = ctod('  /  /  ')
  w_EVAFIN = ctod('  /  /  ')
  w_FILART = space(20)
  w_FILARM = space(20)
  w_DESFAR = space(40)
  w_DESARM = space(40)
  w_FILCLI = space(15)
  w_DESCLI = space(40)
  w_FLVEAC1 = space(1)
  w_DESCAU = space(35)
  w_FILCAU = space(5)
  w_FLFOM = space(1)
  w_FLRICIVA = .F.
  o_FLRICIVA = .F.
  w_RIIVDTIN = ctod('  /  /  ')
  o_RIIVDTIN = ctod('  /  /  ')
  w_RIIVDTFI = ctod('  /  /  ')
  w_RIIVLSIV = space(0)
  w_ZoomMast = .NULL.
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsps_kim
  cVqrCheck = '' && Se piena Query utilizzata per check testate su conferma, alternativa al cursore sullo zoom di sinistra
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_kimPag1","gsps_kim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Documenti")
      .Pages(2).addobject("oPag","tgsps_kimPag2","gsps_kim",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Ulteriori selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLNSRI_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMast = this.oPgFrm.Pages(1).oPag.ZoomMast
    this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
    DoDefault()
    proc Destroy()
      this.w_ZoomMast = .NULL.
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='MOD_VEND'
    this.cWorkTables[6]='CLI_VEND'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MDDATREG=ctod("  /  /  ")
      .w_MDTIPVEN=space(5)
      .w_MDCODCLI=space(15)
      .w_DOCCO1=space(5)
      .w_DOCCO2=space(5)
      .w_DOCCO3=space(5)
      .w_CODCON=space(15)
      .w_TESTERR=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_FLNSRI=space(1)
      .w_FLVSRI=space(1)
      .w_CALSER=space(10)
      .w_SERIAL=space(10)
      .w_TIPART=space(1)
      .w_DESART=space(40)
      .w_SELEZI=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_EVAINI=ctod("  /  /  ")
      .w_EVAFIN=ctod("  /  /  ")
      .w_FILART=space(20)
      .w_FILARM=space(20)
      .w_DESFAR=space(40)
      .w_DESARM=space(40)
      .w_FILCLI=space(15)
      .w_DESCLI=space(40)
      .w_FLVEAC1=space(1)
      .w_DESCAU=space(35)
      .w_FILCAU=space(5)
      .w_FLFOM=space(1)
      .w_FLRICIVA=.f.
      .w_RIIVDTIN=ctod("  /  /  ")
      .w_RIIVDTFI=ctod("  /  /  ")
      .w_RIIVLSIV=space(0)
      .w_MDDATREG=oParentObject.w_MDDATREG
      .w_MDTIPVEN=oParentObject.w_MDTIPVEN
      .w_MDCODCLI=oParentObject.w_MDCODCLI
      .w_FLRICIVA=oParentObject.w_FLRICIVA
      .w_RIIVDTIN=oParentObject.w_RIIVDTIN
      .w_RIIVDTFI=oParentObject.w_RIIVDTFI
      .w_RIIVLSIV=oParentObject.w_RIIVLSIV
          .DoRTCalc(1,1,.f.)
        .w_MDTIPVEN = .w_MDTIPVEN
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_MDTIPVEN))
          .link_1_2('Full')
        endif
        .w_MDCODCLI = .w_MDCODCLI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MDCODCLI))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,7,.f.)
        .w_TESTERR = .T.
        .w_OBTEST = .w_MDDATREG
      .oPgFrm.Page1.oPag.ZoomMast.Calculate()
          .DoRTCalc(10,11,.f.)
        .w_CALSER = Nvl(.w_ZoomMast.getVar('MVSERIAL'),Space(10))
        .w_SERIAL = IIF(EMPTY(NVL(.w_CALSER,' ')),'zzzzzzzzzz', .w_CALSER)
      .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .w_TIPART = Nvl(.w_ZoomDett.getVar('MVTIPRIG'),Space(1))
        .w_DESART = Nvl(.w_ZoomDett.getVar('MVDESART'),Space(40))
        .w_SELEZI = 'S'
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
          .DoRTCalc(17,17,.f.)
        .w_DATINI = .w_MDDATREG - g_DTINIM
        .w_DATFIN = .w_MDDATREG + g_DTFIIM
        .DoRTCalc(20,22,.f.)
        if not(empty(.w_FILART))
          .link_2_5('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_FILARM))
          .link_2_6('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .DoRTCalc(24,26,.f.)
        if not(empty(.w_FILCLI))
          .link_2_16('Full')
        endif
          .DoRTCalc(27,27,.f.)
        .w_FLVEAC1 = 'C'
        .DoRTCalc(29,30,.f.)
        if not(empty(.w_FILCAU))
          .link_2_21('Full')
        endif
        .w_FLFOM = 'M'
    endwith
    this.DoRTCalc(32,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MDDATREG=.w_MDDATREG
      .oParentObject.w_MDTIPVEN=.w_MDTIPVEN
      .oParentObject.w_MDCODCLI=.w_MDCODCLI
      .oParentObject.w_FLRICIVA=.w_FLRICIVA
      .oParentObject.w_RIIVDTIN=.w_RIIVDTIN
      .oParentObject.w_RIIVDTFI=.w_RIIVDTFI
      .oParentObject.w_RIIVLSIV=.w_RIIVLSIV
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
          .link_1_3('Full')
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .DoRTCalc(4,11,.t.)
            .w_CALSER = Nvl(.w_ZoomMast.getVar('MVSERIAL'),Space(10))
            .w_SERIAL = IIF(EMPTY(NVL(.w_CALSER,' ')),'zzzzzzzzzz', .w_CALSER)
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
            .w_TIPART = Nvl(.w_ZoomDett.getVar('MVTIPRIG'),Space(1))
            .w_DESART = Nvl(.w_ZoomDett.getVar('MVDESART'),Space(40))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .DoRTCalc(16,27,.t.)
            .w_FLVEAC1 = 'C'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLNSRI_1_10.enabled = this.oPgFrm.Page1.oPag.oFLNSRI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oFLVSRI_1_11.enabled = this.oPgFrm.Page1.oPag.oFLVSRI_1_11.mCond()
    this.oPgFrm.Page2.oPag.oFILCLI_2_16.enabled = this.oPgFrm.Page2.oPag.oFILCLI_2_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oStr_2_17.visible=!this.oPgFrm.Page2.oPag.oStr_2_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMast.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MDTIPVEN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
    i_lTable = "MOD_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2], .t., this.MOD_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDTIPVEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDTIPVEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODOCCO1,MODOCCO2,MODOCCO3";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_MDTIPVEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_MDTIPVEN)
            select MOCODICE,MODOCCO1,MODOCCO2,MODOCCO3;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDTIPVEN = NVL(_Link_.MOCODICE,space(5))
      this.w_DOCCO1 = NVL(_Link_.MODOCCO1,space(5))
      this.w_DOCCO2 = NVL(_Link_.MODOCCO2,space(5))
      this.w_DOCCO3 = NVL(_Link_.MODOCCO3,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MDTIPVEN = space(5)
      endif
      this.w_DOCCO1 = space(5)
      this.w_DOCCO2 = space(5)
      this.w_DOCCO3 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDTIPVEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODCLI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_MDCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_MDCODCLI)
            select CLCODCLI,CLCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODCLI = NVL(_Link_.CLCODCLI,space(15))
      this.w_CODCON = NVL(_Link_.CLCODCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODCLI = space(15)
      endif
      this.w_CODCON = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILART
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_FILART)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_FILART))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILART)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILART) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oFILART_2_5'),i_cWhere,'GSMA_ACA',"Articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_FILART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_FILART)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILART = NVL(_Link_.CACODICE,space(20))
      this.w_DESFAR = NVL(_Link_.CADESART,space(40))
      this.w_FILARM = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_FILART = space(20)
      endif
      this.w_DESFAR = space(40)
      this.w_FILARM = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILARM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILARM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_FILARM)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_FILARM))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILARM)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_FILARM)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_FILARM)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FILARM) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oFILARM_2_6'),i_cWhere,'GSMA_BZA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILARM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_FILARM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_FILARM)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILARM = NVL(_Link_.ARCODART,space(20))
      this.w_DESARM = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILARM = space(20)
      endif
      this.w_DESARM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILARM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILCLI
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FILCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_FLVEAC1;
                     ,'ANCODICE',trim(this.w_FILCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFILCLI_2_16'),i_cWhere,'GSAR_BZC',"Anagrafica clienti",'GSCG1MPN.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FILCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_FLVEAC1;
                       ,'ANCODICE',this.w_FILCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILCLI = space(15)
      endif
      this.w_DESCLI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILCAU
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_FILCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_FILCAU))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILCAU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILCAU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oFILCAU_2_21'),i_cWhere,'',"Documenti importabili",'GSPS_MDC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_FILCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_FILCAU)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILCAU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FILCAU = space(5)
      endif
      this.w_DESCAU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLNSRI_1_10.RadioValue()==this.w_FLNSRI)
      this.oPgFrm.Page1.oPag.oFLNSRI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLVSRI_1_11.RadioValue()==this.w_FLVSRI)
      this.oPgFrm.Page1.oPag.oFLVSRI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPART_1_21.RadioValue()==this.w_TIPART)
      this.oPgFrm.Page1.oPag.oTIPART_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_22.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_22.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_25.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_2_1.value==this.w_DATINI)
      this.oPgFrm.Page2.oPag.oDATINI_2_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_2_2.value==this.w_DATFIN)
      this.oPgFrm.Page2.oPag.oDATFIN_2_2.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oEVAINI_2_3.value==this.w_EVAINI)
      this.oPgFrm.Page2.oPag.oEVAINI_2_3.value=this.w_EVAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oEVAFIN_2_4.value==this.w_EVAFIN)
      this.oPgFrm.Page2.oPag.oEVAFIN_2_4.value=this.w_EVAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFILART_2_5.value==this.w_FILART)
      this.oPgFrm.Page2.oPag.oFILART_2_5.value=this.w_FILART
    endif
    if not(this.oPgFrm.Page2.oPag.oFILARM_2_6.value==this.w_FILARM)
      this.oPgFrm.Page2.oPag.oFILARM_2_6.value=this.w_FILARM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAR_2_12.value==this.w_DESFAR)
      this.oPgFrm.Page2.oPag.oDESFAR_2_12.value=this.w_DESFAR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESARM_2_14.value==this.w_DESARM)
      this.oPgFrm.Page2.oPag.oDESARM_2_14.value=this.w_DESARM
    endif
    if not(this.oPgFrm.Page2.oPag.oFILCLI_2_16.value==this.w_FILCLI)
      this.oPgFrm.Page2.oPag.oFILCLI_2_16.value=this.w_FILCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_2_18.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_2_18.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_20.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_20.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oFILCAU_2_21.value==this.w_FILCAU)
      this.oPgFrm.Page2.oPag.oFILCAU_2_21.value=this.w_FILCAU
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsps_kim
      * --- Controlli Finali
      this.w_TESTERR=.T.
      this.NotifyEvent('ControlliFinali')
      return (this.w_TESTERR)
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLRICIVA = this.w_FLRICIVA
    this.o_RIIVDTIN = this.w_RIIVDTIN
    return

enddefine

* --- Define pages as container
define class tgsps_kimPag1 as StdContainer
  Width  = 787
  height = 449
  stdWidth  = 787
  stdheight = 449
  resizeXpos=313
  resizeYpos=277
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLNSRI_1_10 as StdCheck with uid="QNJYIHULOC",rtseq=10,rtrep=.f.,left=16, top=7, caption="Nostro riferimento",;
    ToolTipText = "Se attivo: aggiunge una riga descrittiva di nostro riferimento ad ogni nuovo doc",;
    HelpContextID = 24192938,;
    cFormVar="w_FLNSRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNSRI_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLNSRI_1_10.GetRadio()
    this.Parent.oContained.w_FLNSRI = this.RadioValue()
    return .t.
  endfunc

  func oFLNSRI_1_10.SetRadio()
    this.Parent.oContained.w_FLNSRI=trim(this.Parent.oContained.w_FLNSRI)
    this.value = ;
      iif(this.Parent.oContained.w_FLNSRI=='S',1,;
      0)
  endfunc

  func oFLNSRI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES))
    endwith
   endif
  endfunc

  add object oFLVSRI_1_11 as StdCheck with uid="JTVUITDNDV",rtseq=11,rtrep=.f.,left=16, top=26, caption="Vostro riferimento",;
    ToolTipText = "Se attivo: aggiunge una riga descrittiva di vostro riferimento ad ogni nuovo doc",;
    HelpContextID = 24160170,;
    cFormVar="w_FLVSRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLVSRI_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLVSRI_1_11.GetRadio()
    this.Parent.oContained.w_FLVSRI = this.RadioValue()
    return .t.
  endfunc

  func oFLVSRI_1_11.SetRadio()
    this.Parent.oContained.w_FLVSRI=trim(this.Parent.oContained.w_FLVSRI)
    this.value = ;
      iif(this.Parent.oContained.w_FLVSRI=='S',1,;
      0)
  endfunc

  func oFLVSRI_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES))
    endwith
   endif
  endfunc


  add object ZoomMast as cp_szoombox with uid="VJRPDOWVIW",left=-3, top=73, width=269,height=314,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSPS_KIM",bOptions=.f.,bQueryOnLoad=.f.,;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 88953114


  add object oBtn_1_17 as StdButton with uid="AKOERXBELZ",left=685, top=403, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 1513446;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="VAYMUUHAYT",left=737, top=403, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8802118;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="YZZNYCOXRM",left=737, top=19, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inizio ricerca documenti da importare";
    , HelpContextID = 90028522;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        do GSPS_BIM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomDett as cp_szoombox with uid="NEMWPYAYDC",left=257, top=73, width=533,height=314,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_DETT",cZoomFile="GSPSDKIM",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,;
    cEvent = "CalcRig",;
    nPag=1;
    , HelpContextID = 88953114


  add object oTIPART_1_21 as StdCombo with uid="HRTJDWODQY",rtseq=14,rtrep=.f.,left=390,top=395,width=134,height=21, enabled=.f.;
    , ToolTipText = "Tipo codifica articolo";
    , HelpContextID = 109251018;
    , cFormVar="w_TIPART",RowSource=""+"A valore,"+"Interno,"+"A qt� e valore,"+"Descrittivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPART_1_21.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'R',;
    iif(this.value =3,'M',;
    iif(this.value =4,'D',;
    space(1))))))
  endfunc
  func oTIPART_1_21.GetRadio()
    this.Parent.oContained.w_TIPART = this.RadioValue()
    return .t.
  endfunc

  func oTIPART_1_21.SetRadio()
    this.Parent.oContained.w_TIPART=trim(this.Parent.oContained.w_TIPART)
    this.value = ;
      iif(this.Parent.oContained.w_TIPART=='F',1,;
      iif(this.Parent.oContained.w_TIPART=='R',2,;
      iif(this.Parent.oContained.w_TIPART=='M',3,;
      iif(this.Parent.oContained.w_TIPART=='D',4,;
      0))))
  endfunc

  add object oDESART_1_22 as StdField with uid="MOXDQLXEXW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 109240010,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=55, Top=395, InputMask=replicate('X',40)

  add object oSELEZI_1_25 as StdRadio with uid="VNICEXLLTR",rtseq=16,rtrep=.f.,left=529, top=389, width=156,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_25.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 16731610
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 16731610
      this.Buttons(2).Top=15
      this.SetAll("Width",154)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_25.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_25.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_26 as cp_runprogram with uid="OOKNFUPYOH",left=1, top=482, width=168,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPS_BI1('MRC')",;
    cEvent = "ZOOMMAST row checked",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_27 as cp_runprogram with uid="RJSXBWSNSU",left=172, top=481, width=169,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPS_BI1('MRU')",;
    cEvent = "ZOOMMAST row unchecked",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_28 as cp_runprogram with uid="EDMDVWMAYW",left=346, top=482, width=168,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPS_BI1('DBQ')",;
    cEvent = "ZOOMDETT before query",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_29 as cp_runprogram with uid="EDSNJGRTZW",left=518, top=482, width=168,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPS_BI1('DAQ')",;
    cEvent = "ZOOMDETT after query",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_30 as cp_runprogram with uid="SNFOGKYDRY",left=0, top=503, width=168,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPS_BI1('DRC')",;
    cEvent = "ZOOMDETT row checked",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_31 as cp_runprogram with uid="FHFZMDWAGY",left=351, top=505, width=141,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPS_BI1('SCH')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_32 as cp_runprogram with uid="UJYRBPRZYT",left=173, top=504, width=175,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPS_BI1('DRU')",;
    cEvent = "ZOOMDETT row unchecked",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_33 as cp_runprogram with uid="PWUVEHENLH",left=694, top=484, width=105,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPS_BIM",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_41 as cp_runprogram with uid="AFDENMLHZT",left=508, top=504, width=104,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BI2('A')",;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 88953114


  add object oObj_1_45 as cp_runprogram with uid="YHDJKTNFOV",left=632, top=503, width=154,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BI2('U')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 88953114

  add object oStr_1_12 as StdString with uid="BNCNHVKGYN",Visible=.t., Left=4, Top=57,;
    Alignment=0, Width=127, Height=15,;
    Caption="Elenco documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="DCXZNKWDRF",Visible=.t., Left=266, Top=57,;
    Alignment=0, Width=122, Height=15,;
    Caption="Dettaglio righe"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="WSUTWREUHW",Visible=.t., Left=0, Top=395,;
    Alignment=1, Width=53, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PGOISVAEYP",Visible=.t., Left=339, Top=395,;
    Alignment=1, Width=48, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="VUNFMVNLKY",Visible=.t., Left=461, Top=35,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Blu)"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="JLPTGYJBLO",Visible=.t., Left=461, Top=20,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Rosso)"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="IGJFRFLBPY",Visible=.t., Left=461, Top=5,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Nero)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="SNHXGJYXDR",Visible=.t., Left=524, Top=5,;
    Alignment=0, Width=178, Height=17,;
    Caption="Non selezionata, non trasferita."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="KPEGKTKNKF",Visible=.t., Left=524, Top=20,;
    Alignment=0, Width=178, Height=17,;
    Caption="Selezionata, non trasferita."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="NHQJGSHCLY",Visible=.t., Left=524, Top=35,;
    Alignment=0, Width=178, Height=17,;
    Caption="Gi� trasferita, non selezionabile."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="EPYXVQIDXX",Visible=.t., Left=339, Top=7,;
    Alignment=1, Width=117, Height=15,;
    Caption="Legenda righe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="TMZNCGZDVI",Visible=.t., Left=461, Top=50,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Magenta)"    , ForeColor=RGB(255,0,255);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_44 as StdString with uid="BSAUUOJAIY",Visible=.t., Left=524, Top=50,;
    Alignment=0, Width=178, Height=17,;
    Caption="Incongruente, non selezionabile."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsps_kimPag2 as StdContainer
  Width  = 787
  height = 449
  stdWidth  = 787
  stdheight = 449
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_2_1 as StdField with uid="PHKYWKFGDT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione (vuoto =no selezione)",;
    HelpContextID = 29020874,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=11

  add object oDATFIN_2_2 as StdField with uid="UETOVWEXGF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione (vuoto =no selezione)",;
    HelpContextID = 219009738,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=39

  add object oEVAINI_2_3 as StdField with uid="AYUGUTBVKR",rtseq=20,rtrep=.f.,;
    cFormVar = "w_EVAINI", cQueryName = "EVAINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prevista evasione di inizio selezione (vuoto =no selezione)",;
    HelpContextID = 29093306,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=447, Top=11

  add object oEVAFIN_2_4 as StdField with uid="NZPEFWQYAO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_EVAFIN", cQueryName = "EVAFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prevista evasione di fine selezione (vuoto =no selezione)",;
    HelpContextID = 219082170,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=447, Top=39

  add object oFILART_2_5 as StdField with uid="QFAEHIGQLO",rtseq=22,rtrep=.f.,;
    cFormVar = "w_FILART", cQueryName = "FILART",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale codice di ricerca di selezione (vuoto = no selezione)",;
    HelpContextID = 109267626,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=80, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_FILART"

  func oFILART_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILART_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILART_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oFILART_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Articoli/servizi",'',this.parent.oContained
  endproc
  proc oFILART_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_FILART
     i_obj.ecpSave()
  endproc

  add object oFILARM_2_6 as StdField with uid="WQUBQWVMAU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_FILARM", cQueryName = "FILARM",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale codice articolo di selezione (vuoto = no selezione)",;
    HelpContextID = 226708138,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=108, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_FILARM"

  func oFILARM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILARM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILARM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oFILARM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'',this.parent.oContained
  endproc
  proc oFILARM_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_FILARM
     i_obj.ecpSave()
  endproc

  add object oDESFAR_2_12 as StdField with uid="FIMCEDYFOD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESFAR", cQueryName = "DESFAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 160292554,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=80, InputMask=replicate('X',40)

  add object oDESARM_2_14 as StdField with uid="PIOAUNZZKO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESARM", cQueryName = "DESARM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 226680522,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=108, InputMask=replicate('X',40)

  add object oFILCLI_2_16 as StdField with uid="VXCHAHINBV",rtseq=26,rtrep=.f.,;
    cFormVar = "w_FILCLI", cQueryName = "FILCLI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 31541930,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=137, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_FLVEAC1", oKey_2_1="ANCODICE", oKey_2_2="this.w_FILCLI"

  func oFILCLI_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON))
    endwith
   endif
  endfunc

  func oFILCLI_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILCLI_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILCLI_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_FLVEAC1)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFILCLI_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti",'GSCG1MPN.CONTI_VZM',this.parent.oContained
  endproc
  proc oFILCLI_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_FLVEAC1
     i_obj.w_ANCODICE=this.parent.oContained.w_FILCLI
     i_obj.ecpSave()
  endproc

  add object oDESCLI_2_18 as StdField with uid="VCQGTDKOCN",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 31514314,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=137, InputMask=replicate('X',40)

  add object oDESCAU_2_20 as StdField with uid="HPXFEEAYSW",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 110157514,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=165, InputMask=replicate('X',35)

  add object oFILCAU_2_21 as StdField with uid="WJROWXMCTI",rtseq=30,rtrep=.f.,;
    cFormVar = "w_FILCAU", cQueryName = "FILCAU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale documento selezione (vuoto = no selezione)",;
    HelpContextID = 110185130,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=148, Top=166, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_FILCAU"

  func oFILCAU_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILCAU_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILCAU_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oFILCAU_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Documenti importabili",'GSPS_MDC.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oStr_2_7 as StdString with uid="FYWPQOXBEE",Visible=.t., Left=7, Top=11,;
    Alignment=1, Width=139, Height=15,;
    Caption="Da data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="JGLWZGKDER",Visible=.t., Left=19, Top=39,;
    Alignment=1, Width=127, Height=15,;
    Caption="A data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="XSSBOAYRYS",Visible=.t., Left=294, Top=11,;
    Alignment=1, Width=151, Height=15,;
    Caption="Da prevista evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="FNYXEXORXF",Visible=.t., Left=294, Top=39,;
    Alignment=1, Width=151, Height=15,;
    Caption="A prevista evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="MDONVSXVZG",Visible=.t., Left=33, Top=165,;
    Alignment=1, Width=113, Height=18,;
    Caption="Tipo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="UOAYZZHOIG",Visible=.t., Left=33, Top=80,;
    Alignment=1, Width=113, Height=15,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="WCEYASNNGX",Visible=.t., Left=33, Top=108,;
    Alignment=1, Width=113, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="REIBAUJZMI",Visible=.t., Left=33, Top=137,;
    Alignment=1, Width=113, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_2_17.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC1='F')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_kim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
