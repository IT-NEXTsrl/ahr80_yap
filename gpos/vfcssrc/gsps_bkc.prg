* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bkc                                                        *
*              Verifica cliente in documenti di vendita                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_406]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-14                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bkc",oParentObject)
return(i_retval)

define class tgsps_bkc as StdBatch
  * --- Local variables
  w_MDCODCLI = space(15)
  w_CODAZI = space(5)
  w_LENC = 0
  w_NOTIFICA = space(1)
  w_CODICE = space(15)
  w_ANCODICE = space(15)
  w_CLPARIVA = space(12)
  w_AUTN = 0
  w_CODN = space(15)
  w_APPO = space(10)
  w_APPO1 = space(10)
  w_FLAUTC = space(1)
  w_MASCON = space(15)
  w_CATCON = space(5)
  w_CODLIN = space(3)
  w_OKTROV = .f.
  w_CLTIPCON = space(1)
  w_CLCODCON = space(15)
  w_CLDESCRI = space(40)
  w_CLINDIRI = space(35)
  w_CL___CAP = space(8)
  w_CLLOCALI = space(30)
  w_CLPROVIN = space(2)
  w_CLNAZION = space(3)
  w_CLTELEFO = space(18)
  w_CLTELFAX = space(18)
  w_CLNUMCEL = space(18)
  w_CL_EMAIL = space(50)
  w_CL_EMPEC = space(50)
  w_CLPARIVA = space(12)
  w_CLCODFIS = space(16)
  w_CLCODPAG = space(5)
  w_CLCATCOM = space(3)
  w_CLCATSCM = space(5)
  w_CLSCONT1 = 0
  w_CLSCONT2 = 0
  w_CLCODLIS = space(5)
  w_CLFLFIDO = space(1)
  w_CLVALFID = 0
  w_CLFIDUTI = 0
  w_CLDTINVA = ctod("  /  /  ")
  w_CLDTOBSO = ctod("  /  /  ")
  w_ANFLFIDO = space(1)
  w_ANCLIPOS = space(1)
  w_CONCON = space(1)
  w_ANCODSTU = space(5)
  w_CODSTU = space(5)
  * --- WorkFile variables
  PAR_VDET_idx=0
  CONTI_idx=0
  CLI_VEND_idx=0
  MASTRI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se il Cliente e' stato inserito in Anagrafica (da GSPS_MVD e GSPS_BGF)
    * --- Chiamato in caso di Generazione Automatica Documenti (w_MDTIPCHI: FI ; FF ; RS ; DT)
    * --- Nonche' in Generazione Fatture da Corrispettivi
    * --- Lettura parametri 
    this.w_CODAZI = i_CODAZI
    this.w_FLAUTC = " "
    this.w_ANCLIPOS = " "
    this.w_MASCON = SPACE(15)
    this.w_CATCON = SPACE(5)
    this.w_CODLIN = SPACE(3)
    * --- Lasciare locale (utilizzato in GSPS_KCC)
    this.w_MDCODCLI = this.oParentObject.w_MDCODCLI
    * --- Read from PAR_VDET
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_VDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAFLAUTC,PAMASCON,PACATCON,PACODLIN"+;
        " from "+i_cTable+" PAR_VDET where ";
            +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and PACODNEG = "+cp_ToStrODBC(this.oParentObject.w_MDCODNEG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAFLAUTC,PAMASCON,PACATCON,PACODLIN;
        from (i_cTable) where;
            PACODAZI = this.w_CODAZI;
            and PACODNEG = this.oParentObject.w_MDCODNEG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLAUTC = NVL(cp_ToDate(_read_.PAFLAUTC),cp_NullValue(_read_.PAFLAUTC))
      this.w_MASCON = NVL(cp_ToDate(_read_.PAMASCON),cp_NullValue(_read_.PAMASCON))
      this.w_CATCON = NVL(cp_ToDate(_read_.PACATCON),cp_NullValue(_read_.PACATCON))
      this.w_CODLIN = NVL(cp_ToDate(_read_.PACODLIN),cp_NullValue(_read_.PACODLIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_ERRORE = .T.
    this.w_LENC = MIN(LEN(ALLTRIM(p_CLF)), 15)
    * --- Lettura dei Clienti Negozio da Inserire 
    this.w_CLTIPCON = " "
    this.w_CLCODCON = SPACE(15)
    * --- Read from CLI_VEND
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CLI_VEND_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CL___CAP,CL_EMAIL,CL_EMPEC,CLCATCOM,CLCATSCM,CLCODCON,CLCODFIS,CLCODLIS,CLCODPAG,CLDESCRI,CLDTINVA,CLDTOBSO,CLFIDUTI,CLFLFIDO,CLINDIRI,CLLOCALI,CLNAZION,CLNUMCEL,CLPARIVA,CLPROVIN,CLSCONT1,CLSCONT2,CLTELEFO,CLTELFAX,CLTIPCON,CLVALFID"+;
        " from "+i_cTable+" CLI_VEND where ";
            +"CLCODCLI = "+cp_ToStrODBC(this.w_MDCODCLI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CL___CAP,CL_EMAIL,CL_EMPEC,CLCATCOM,CLCATSCM,CLCODCON,CLCODFIS,CLCODLIS,CLCODPAG,CLDESCRI,CLDTINVA,CLDTOBSO,CLFIDUTI,CLFLFIDO,CLINDIRI,CLLOCALI,CLNAZION,CLNUMCEL,CLPARIVA,CLPROVIN,CLSCONT1,CLSCONT2,CLTELEFO,CLTELFAX,CLTIPCON,CLVALFID;
        from (i_cTable) where;
            CLCODCLI = this.w_MDCODCLI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CL___CAP = NVL(cp_ToDate(_read_.CL___CAP),cp_NullValue(_read_.CL___CAP))
      this.w_CL_EMAIL = NVL(cp_ToDate(_read_.CL_EMAIL),cp_NullValue(_read_.CL_EMAIL))
      this.w_CL_EMPEC = NVL(cp_ToDate(_read_.CL_EMPEC),cp_NullValue(_read_.CL_EMPEC))
      this.w_CLCATCOM = NVL(cp_ToDate(_read_.CLCATCOM),cp_NullValue(_read_.CLCATCOM))
      this.w_CLCATSCM = NVL(cp_ToDate(_read_.CLCATSCM),cp_NullValue(_read_.CLCATSCM))
      this.w_CLCODCON = NVL(cp_ToDate(_read_.CLCODCON),cp_NullValue(_read_.CLCODCON))
      this.w_CLCODFIS = NVL(cp_ToDate(_read_.CLCODFIS),cp_NullValue(_read_.CLCODFIS))
      this.w_CLCODLIS = NVL(cp_ToDate(_read_.CLCODLIS),cp_NullValue(_read_.CLCODLIS))
      this.w_CLCODPAG = NVL(cp_ToDate(_read_.CLCODPAG),cp_NullValue(_read_.CLCODPAG))
      this.w_CLDESCRI = NVL(cp_ToDate(_read_.CLDESCRI),cp_NullValue(_read_.CLDESCRI))
      this.w_CLDTINVA = NVL(cp_ToDate(_read_.CLDTINVA),cp_NullValue(_read_.CLDTINVA))
      this.w_CLDTOBSO = NVL(cp_ToDate(_read_.CLDTOBSO),cp_NullValue(_read_.CLDTOBSO))
      this.w_CLFIDUTI = NVL(cp_ToDate(_read_.CLFIDUTI),cp_NullValue(_read_.CLFIDUTI))
      this.w_CLFLFIDO = NVL(cp_ToDate(_read_.CLFLFIDO),cp_NullValue(_read_.CLFLFIDO))
      this.w_CLINDIRI = NVL(cp_ToDate(_read_.CLINDIRI),cp_NullValue(_read_.CLINDIRI))
      this.w_CLLOCALI = NVL(cp_ToDate(_read_.CLLOCALI),cp_NullValue(_read_.CLLOCALI))
      this.w_CLNAZION = NVL(cp_ToDate(_read_.CLNAZION),cp_NullValue(_read_.CLNAZION))
      this.w_CLNUMCEL = NVL(cp_ToDate(_read_.CLNUMCEL),cp_NullValue(_read_.CLNUMCEL))
      this.w_CLPARIVA = NVL(cp_ToDate(_read_.CLPARIVA),cp_NullValue(_read_.CLPARIVA))
      this.w_CLPROVIN = NVL(cp_ToDate(_read_.CLPROVIN),cp_NullValue(_read_.CLPROVIN))
      this.w_CLSCONT1 = NVL(cp_ToDate(_read_.CLSCONT1),cp_NullValue(_read_.CLSCONT1))
      this.w_CLSCONT2 = NVL(cp_ToDate(_read_.CLSCONT2),cp_NullValue(_read_.CLSCONT2))
      this.w_CLTELEFO = NVL(cp_ToDate(_read_.CLTELEFO),cp_NullValue(_read_.CLTELEFO))
      this.w_CLTELFAX = NVL(cp_ToDate(_read_.CLTELFAX),cp_NullValue(_read_.CLTELFAX))
      this.w_CLTIPCON = NVL(cp_ToDate(_read_.CLTIPCON),cp_NullValue(_read_.CLTIPCON))
      this.w_CLVALFID = NVL(cp_ToDate(_read_.CLVALFID),cp_NullValue(_read_.CLVALFID))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_CLTIPCON="C" AND NOT EMPTY(this.w_CLCODCON)
      this.w_APPO = SPACE(15)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODICE,ANCLIPOS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_CLTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CLCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODICE,ANCLIPOS;
          from (i_cTable) where;
              ANTIPCON = this.w_CLTIPCON;
              and ANCODICE = this.w_CLCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_APPO = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
        this.w_ANCLIPOS = NVL(cp_ToDate(_read_.ANCLIPOS),cp_NullValue(_read_.ANCLIPOS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_ERRORE = IIF(this.w_APPO=this.w_CLCODCON, .F., .T.)
      if this.oParentObject.w_ERRORE=.F. AND this.w_ANCLIPOS<>"S"
        * --- Se Cliente no POS inserisce il Flag
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_CLTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CLCODCON);
                 )
        else
          update (i_cTable) set;
              ANCLIPOS = "S";
              &i_ccchkf. ;
           where;
              ANTIPCON = this.w_CLTIPCON;
              and ANCODICE = this.w_CLCODCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    if this.oParentObject.w_ERRORE=.T.
      * --- Generazione Nuovo Cliente
      * --- Se � stata definita la codifica numerica sui Clienti.
      if g_CFNUME="S"
        * --- Propone il Primo Progressivo Cliente Disponibile
        this.w_CODICE = LEFT(SPACE(15), LEN(ALLTRIM(p_CLF)))
        i_nConn = i_TableProp[this.CONTI_IDX,3]
        cp_AskTableProg(this,i_nConn,"PRNUCL","i_CODAZI,w_CODICE") 
        this.w_ANCODICE = RIGHT(this.w_CODICE, this.w_LENC)
      else
        * --- Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
        this.w_AUTN = 0
        this.w_CODN = ALLTRIM(this.w_MDCODCLI)
        FOR p_POS=1 TO LEN(this.w_CODN)
        this.w_AUTN = IIF(SUBSTR(this.w_CODN, p_POS, 1) $ "0123456789", this.w_AUTN, 1)
        ENDFOR
        if g_FLCPOS="S" AND this.w_AUTN=0
          this.w_ANCODICE = RIGHT(ALLTRIM(this.w_MDCODCLI), this.w_LENC)
        else
          this.w_ANCODICE = LEFT(ALLTRIM(this.w_MDCODCLI), this.w_LENC)
        endif
      endif
      * --- Notifica Se Aggiornamento Cliente OK viene valorizzato a 'S'
      if this.w_FLAUTC="S" AND NOT EMPTY(this.w_MASCON) AND NOT EMPTY(this.w_CATCON) AND NOT EMPTY(this.w_CODLIN) AND NOT EMPTY(this.w_MDCODCLI)
        * --- Inserisce in automatico il Cliente
        this.w_OKTROV = .F.
        if NOT EMPTY(this.w_CLPARIVA)
          * --- Select from CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONTI ";
                +" where ANPARIVA="+cp_ToStrODBC(this.w_CLPARIVA)+" AND ANCLIPOS<>'S'";
                 ,"_Curs_CONTI")
          else
            select * from (i_cTable);
             where ANPARIVA=this.w_CLPARIVA AND ANCLIPOS<>"S";
              into cursor _Curs_CONTI
          endif
          if used('_Curs_CONTI')
            select _Curs_CONTI
            locate for 1=1
            do while not(eof())
            * --- Cerca se esiste un codice con stessa Partita IVA
            if NVL(ANTIPCON, " ")="C"
              if this.w_OKTROV=.F.
                this.w_ANCODICE = ANCODICE
                this.w_ANCLIPOS = NVL(ANCLIPOS, " ")
                this.w_OKTROV = .T.
                this.oParentObject.w_ERRORE = .F.
                * --- Write into CLI_VEND
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CLI_VEND_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"CLINDIRI ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANINDIRI),'CLI_VEND','CLINDIRI');
                  +",CL___CAP ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN___CAP),'CLI_VEND','CL___CAP');
                  +",CLLOCALI ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANLOCALI),'CLI_VEND','CLLOCALI');
                  +",CLPROVIN ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANPROVIN),'CLI_VEND','CLPROVIN');
                  +",CLNAZION ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANNAZION),'CLI_VEND','CLNAZION');
                  +",CLTELEFO ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANTELEFO),'CLI_VEND','CLTELEFO');
                  +",CLTELFAX ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANTELFAX),'CLI_VEND','CLTELFAX');
                  +",CLNUMCEL ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANNUMCEL),'CLI_VEND','CLNUMCEL');
                  +",CL_EMAIL ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN_EMAIL),'CLI_VEND','CL_EMAIL');
                  +",CL_EMPEC ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN_EMPEC),'CLI_VEND','CL_EMPEC');
                  +",CLPARIVA ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANPARIVA),'CLI_VEND','CLPARIVA');
                  +",CLCODFIS ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANCODFIS),'CLI_VEND','CLCODFIS');
                  +",CLCODPAG ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANCODPAG),'CLI_VEND','CLCODPAG');
                  +",CLCATCOM ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANCATCOM),'CLI_VEND','CLCATCOM');
                  +",CLCATSCM ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANCATSCM),'CLI_VEND','CLCATSCM');
                  +",CLSCONT1 ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN1SCONT),'CLI_VEND','CLSCONT1');
                  +",CLSCONT2 ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN2SCONT),'CLI_VEND','CLSCONT2');
                  +",CLCODLIS ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANNUMLIS),'CLI_VEND','CLCODLIS');
                  +",CLDTINVA ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANDTINVA),'CLI_VEND','CLDTINVA');
                  +",CLDTOBSO ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANDTOBSO),'CLI_VEND','CLDTOBSO');
                  +",CLVALFID ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANVALFID),'CLI_VEND','CLVALFID');
                  +",CLTIPCON ="+cp_NullLink(cp_ToStrODBC("C"),'CLI_VEND','CLTIPCON');
                  +",CLCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CLI_VEND','CLCODCON');
                      +i_ccchkf ;
                  +" where ";
                      +"CLCODCLI = "+cp_ToStrODBC(this.w_MDCODCLI);
                         )
                else
                  update (i_cTable) set;
                      CLINDIRI = _Curs_CONTI.ANINDIRI;
                      ,CL___CAP = _Curs_CONTI.AN___CAP;
                      ,CLLOCALI = _Curs_CONTI.ANLOCALI;
                      ,CLPROVIN = _Curs_CONTI.ANPROVIN;
                      ,CLNAZION = _Curs_CONTI.ANNAZION;
                      ,CLTELEFO = _Curs_CONTI.ANTELEFO;
                      ,CLTELFAX = _Curs_CONTI.ANTELFAX;
                      ,CLNUMCEL = _Curs_CONTI.ANNUMCEL;
                      ,CL_EMAIL = _Curs_CONTI.AN_EMAIL;
                      ,CL_EMPEC = _Curs_CONTI.AN_EMPEC;
                      ,CLPARIVA = _Curs_CONTI.ANPARIVA;
                      ,CLCODFIS = _Curs_CONTI.ANCODFIS;
                      ,CLCODPAG = _Curs_CONTI.ANCODPAG;
                      ,CLCATCOM = _Curs_CONTI.ANCATCOM;
                      ,CLCATSCM = _Curs_CONTI.ANCATSCM;
                      ,CLSCONT1 = _Curs_CONTI.AN1SCONT;
                      ,CLSCONT2 = _Curs_CONTI.AN2SCONT;
                      ,CLCODLIS = _Curs_CONTI.ANNUMLIS;
                      ,CLDTINVA = _Curs_CONTI.ANDTINVA;
                      ,CLDTOBSO = _Curs_CONTI.ANDTOBSO;
                      ,CLVALFID = _Curs_CONTI.ANVALFID;
                      ,CLTIPCON = "C";
                      ,CLCODCON = this.w_ANCODICE;
                      &i_ccchkf. ;
                   where;
                      CLCODCLI = this.w_MDCODCLI;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                if this.w_ANCLIPOS<>"S"
                  * --- Se Cliente no POS inserisce il Flag
                  * --- Write into CONTI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CONTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
                        +i_ccchkf ;
                    +" where ";
                        +"ANTIPCON = "+cp_ToStrODBC("C");
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                           )
                  else
                    update (i_cTable) set;
                        ANCLIPOS = "S";
                        &i_ccchkf. ;
                     where;
                        ANTIPCON = "C";
                        and ANCODICE = this.w_ANCODICE;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
              select _Curs_CONTI
              continue
            enddo
            use
          endif
        endif
        if NOT EMPTY(this.w_CLCODFIS) AND this.w_OKTROV=.F.
          * --- Select from CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONTI ";
                +" where ANCODFIS="+cp_ToStrODBC(this.w_CLCODFIS)+" AND ANCLIPOS<>'S'";
                 ,"_Curs_CONTI")
          else
            select * from (i_cTable);
             where ANCODFIS=this.w_CLCODFIS AND ANCLIPOS<>"S";
              into cursor _Curs_CONTI
          endif
          if used('_Curs_CONTI')
            select _Curs_CONTI
            locate for 1=1
            do while not(eof())
            * --- Cerca se esiste un codice con stessa Partita IVA
            if NVL(ANTIPCON, " ")="C"
              if this.w_OKTROV=.F.
                this.w_ANCODICE = ANCODICE
                this.w_ANCLIPOS = NVL(ANCLIPOS, " ")
                this.w_OKTROV = .T.
                this.oParentObject.w_ERRORE = .F.
                * --- Write into CLI_VEND
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CLI_VEND_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"CLINDIRI ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANINDIRI),'CLI_VEND','CLINDIRI');
                  +",CL___CAP ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN___CAP),'CLI_VEND','CL___CAP');
                  +",CLLOCALI ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANLOCALI),'CLI_VEND','CLLOCALI');
                  +",CLPROVIN ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANPROVIN),'CLI_VEND','CLPROVIN');
                  +",CLNAZION ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANNAZION),'CLI_VEND','CLNAZION');
                  +",CLTELEFO ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANTELEFO),'CLI_VEND','CLTELEFO');
                  +",CLTELFAX ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANTELFAX),'CLI_VEND','CLTELFAX');
                  +",CLNUMCEL ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANNUMCEL),'CLI_VEND','CLNUMCEL');
                  +",CL_EMAIL ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN_EMAIL),'CLI_VEND','CL_EMAIL');
                  +",CL_EMPEC ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN_EMPEC),'CLI_VEND','CL_EMPEC');
                  +",CLPARIVA ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANPARIVA),'CLI_VEND','CLPARIVA');
                  +",CLCODFIS ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANCODFIS),'CLI_VEND','CLCODFIS');
                  +",CLCODPAG ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANCODPAG),'CLI_VEND','CLCODPAG');
                  +",CLCATCOM ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANCATCOM),'CLI_VEND','CLCATCOM');
                  +",CLCATSCM ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANCATSCM),'CLI_VEND','CLCATSCM');
                  +",CLSCONT1 ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN1SCONT),'CLI_VEND','CLSCONT1');
                  +",CLSCONT2 ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.AN2SCONT),'CLI_VEND','CLSCONT2');
                  +",CLCODLIS ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANNUMLIS),'CLI_VEND','CLCODLIS');
                  +",CLDTINVA ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANDTINVA),'CLI_VEND','CLDTINVA');
                  +",CLDTOBSO ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANDTOBSO),'CLI_VEND','CLDTOBSO');
                  +",CLVALFID ="+cp_NullLink(cp_ToStrODBC(_Curs_CONTI.ANVALFID),'CLI_VEND','CLVALFID');
                  +",CLTIPCON ="+cp_NullLink(cp_ToStrODBC("C"),'CLI_VEND','CLTIPCON');
                  +",CLCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CLI_VEND','CLCODCON');
                      +i_ccchkf ;
                  +" where ";
                      +"CLCODCLI = "+cp_ToStrODBC(this.w_MDCODCLI);
                         )
                else
                  update (i_cTable) set;
                      CLINDIRI = _Curs_CONTI.ANINDIRI;
                      ,CL___CAP = _Curs_CONTI.AN___CAP;
                      ,CLLOCALI = _Curs_CONTI.ANLOCALI;
                      ,CLPROVIN = _Curs_CONTI.ANPROVIN;
                      ,CLNAZION = _Curs_CONTI.ANNAZION;
                      ,CLTELEFO = _Curs_CONTI.ANTELEFO;
                      ,CLTELFAX = _Curs_CONTI.ANTELFAX;
                      ,CLNUMCEL = _Curs_CONTI.ANNUMCEL;
                      ,CL_EMAIL = _Curs_CONTI.AN_EMAIL;
                      ,CL_EMPEC = _Curs_CONTI.AN_EMPEC;
                      ,CLPARIVA = _Curs_CONTI.ANPARIVA;
                      ,CLCODFIS = _Curs_CONTI.ANCODFIS;
                      ,CLCODPAG = _Curs_CONTI.ANCODPAG;
                      ,CLCATCOM = _Curs_CONTI.ANCATCOM;
                      ,CLCATSCM = _Curs_CONTI.ANCATSCM;
                      ,CLSCONT1 = _Curs_CONTI.AN1SCONT;
                      ,CLSCONT2 = _Curs_CONTI.AN2SCONT;
                      ,CLCODLIS = _Curs_CONTI.ANNUMLIS;
                      ,CLDTINVA = _Curs_CONTI.ANDTINVA;
                      ,CLDTOBSO = _Curs_CONTI.ANDTOBSO;
                      ,CLVALFID = _Curs_CONTI.ANVALFID;
                      ,CLTIPCON = "C";
                      ,CLCODCON = this.w_ANCODICE;
                      &i_ccchkf. ;
                   where;
                      CLCODCLI = this.w_MDCODCLI;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                if this.w_ANCLIPOS<>"S"
                  * --- Se Cliente no POS inserisce il Flag
                  * --- Write into CONTI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CONTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"ANCLIPOS ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANCLIPOS');
                        +i_ccchkf ;
                    +" where ";
                        +"ANTIPCON = "+cp_ToStrODBC("C");
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                           )
                  else
                    update (i_cTable) set;
                        ANCLIPOS = "S";
                        &i_ccchkf. ;
                     where;
                        ANTIPCON = "C";
                        and ANCODICE = this.w_ANCODICE;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
              select _Curs_CONTI
              continue
            enddo
            use
          endif
        endif
        if this.w_OKTROV=.F.
          * --- Select from CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ANCODICE,ANCLIPOS  from "+i_cTable+" CONTI ";
                +" where ANTIPCON='C' AND ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)+"";
                 ,"_Curs_CONTI")
          else
            select ANCODICE,ANCLIPOS from (i_cTable);
             where ANTIPCON="C" AND ANCODICE=this.w_ANCODICE;
              into cursor _Curs_CONTI
          endif
          if used('_Curs_CONTI')
            select _Curs_CONTI
            locate for 1=1
            do while not(eof())
            * --- Verifica se esiste un codice uguale a quello da Inserire (se esiste deve inserirlo a mano)
            this.w_OKTROV = .T.
              select _Curs_CONTI
              continue
            enddo
            use
          endif
          * --- Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
          if this.w_OKTROV=.F.
            * --- Aggiorna il progressivo cliente (se codifica numerica)
            if g_CFNUME="S"
              * --- Aggiorna il Progressivo del Cliente
              i_nConn = i_TableProp[this.CONTI_IDX,3]
              cp_NextTableProg(this,i_nConn,"PRNUCL","i_CODAZI,w_CODICE") 
              this.w_ANCODICE = RIGHT(this.w_CODICE, this.w_LENC)
            endif
            this.w_CLLOCALI = LEFT(this.w_CLLOCALI, 30)
            this.w_CLNAZION = LEFT(this.w_CLNAZION, 3)
            this.w_ANFLFIDO = IIF(g_PERFID="S", this.w_CLFLFIDO, " ")
            this.w_ANCLIPOS = "S"
            this.w_CONCON = IIF(g_ISONAZ="ITA","E","1")
            if g_LEMC="S"
              * --- Calcola il valore di ANCODSTU.
              *     Prima leggiamo dal mastro se c'� un conto studio
              * --- Read from MASTRI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MASTRI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MCCODSTU,MCPROSTU"+;
                  " from "+i_cTable+" MASTRI where ";
                      +"MCCODICE = "+cp_ToStrODBC(this.w_MASCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MCCODSTU,MCPROSTU;
                  from (i_cTable) where;
                      MCCODICE = this.w_MASCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODSTU = NVL(cp_ToDate(_read_.MCCODSTU),cp_NullValue(_read_.MCCODSTU))
                w_MCPROSTU = NVL(cp_ToDate(_read_.MCPROSTU),cp_NullValue(_read_.MCPROSTU))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_ANCODSTU = IIF(!EMPTY(this.w_CODSTU),PADL(ALLTRIM(GSLM_BCF(this, "C", this.w_ANCODICE, this.w_CODSTU, this.w_MASCON, , w_MCPROSTU)),6,"0")," ")
            else
              this.w_ANCODSTU = " "
            endif
            * --- Insert into CONTI
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ANTIPCON"+",ANCODICE"+",ANDESCRI"+",ANDESCR2"+",AN___CAP"+",ANPROVIN"+",ANINDIRI"+",ANINDIR2"+",ANLOCALI"+",ANNAZION"+",ANTELEFO"+",ANTELFAX"+",ANNUMCEL"+",ANCODPAG"+",ANCATCOM"+",ANCATSCM"+",AN1SCONT"+",AN2SCONT"+",ANNUMLIS"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC("C"),'CONTI','ANTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CONTI','ANCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLDESCRI),'CONTI','ANDESCRI');
              +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANDESCR2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CL___CAP),'CONTI','AN___CAP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLPROVIN),'CONTI','ANPROVIN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLINDIRI),'CONTI','ANINDIRI');
              +","+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANINDIR2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLLOCALI),'CONTI','ANLOCALI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLNAZION),'CONTI','ANNAZION');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLTELEFO),'CONTI','ANTELEFO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLTELFAX),'CONTI','ANTELFAX');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLNUMCEL),'CONTI','ANNUMCEL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODPAG),'CONTI','ANCODPAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLCATCOM),'CONTI','ANCATCOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLCATSCM),'CONTI','ANCATSCM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLSCONT1),'CONTI','AN1SCONT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLSCONT2),'CONTI','AN2SCONT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CLCODLIS),'CONTI','ANNUMLIS');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',"C",'ANCODICE',this.w_ANCODICE,'ANDESCRI',this.w_CLDESCRI,'ANDESCR2'," ",'AN___CAP',this.w_CL___CAP,'ANPROVIN',this.w_CLPROVIN,'ANINDIRI',this.w_CLINDIRI,'ANINDIR2'," ",'ANLOCALI',this.w_CLLOCALI,'ANNAZION',this.w_CLNAZION,'ANTELEFO',this.w_CLTELEFO,'ANTELFAX',this.w_CLTELFAX)
              insert into (i_cTable) (ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,AN___CAP,ANPROVIN,ANINDIRI,ANINDIR2,ANLOCALI,ANNAZION,ANTELEFO,ANTELFAX,ANNUMCEL,ANCODPAG,ANCATCOM,ANCATSCM,AN1SCONT,AN2SCONT,ANNUMLIS &i_ccchkf. );
                 values (;
                   "C";
                   ,this.w_ANCODICE;
                   ,this.w_CLDESCRI;
                   ," ";
                   ,this.w_CL___CAP;
                   ,this.w_CLPROVIN;
                   ,this.w_CLINDIRI;
                   ," ";
                   ,this.w_CLLOCALI;
                   ,this.w_CLNAZION;
                   ,this.w_CLTELEFO;
                   ,this.w_CLTELFAX;
                   ,this.w_CLNUMCEL;
                   ,this.w_CLCODPAG;
                   ,this.w_CLCATCOM;
                   ,this.w_CLCATSCM;
                   ,this.w_CLSCONT1;
                   ,this.w_CLSCONT2;
                   ,this.w_CLCODLIS;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Write into CONTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AFFLINTR ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','AFFLINTR');
              +",AN__NOTE ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','AN__NOTE');
              +",AN_EMAIL ="+cp_NullLink(cp_ToStrODBC(this.w_CL_EMAIL),'CONTI','AN_EMAIL');
              +",AN_EMPEC ="+cp_NullLink(cp_ToStrODBC(this.w_CL_EMPEC),'CONTI','AN_EMPEC');
              +",AN_SESSO ="+cp_NullLink(cp_ToStrODBC("M"),'CONTI','AN_SESSO');
              +",ANBOLFAT ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANBOLFAT');
              +",ANCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_CATCON),'CONTI','ANCATCON');
              +",ANCLIPOS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCLIPOS),'CONTI','ANCLIPOS');
              +",ANCODFIS ="+cp_NullLink(cp_ToStrODBC(this.w_CLCODFIS),'CONTI','ANCODFIS');
              +",ANCODIVA ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'CONTI','ANCODIVA');
              +",ANCODLIN ="+cp_NullLink(cp_ToStrODBC(this.w_CODLIN),'CONTI','ANCODLIN');
              +",ANCODSTU ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODSTU),'CONTI','ANCODSTU');
              +",ANCONCON ="+cp_NullLink(cp_ToStrODBC(this.w_CONCON),'CONTI','ANCONCON');
              +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MASCON),'CONTI','ANCONSUP');
              +",ANDTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CLDTINVA),'CONTI','ANDTINVA');
              +",ANDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CLDTOBSO),'CONTI','ANDTOBSO');
              +",ANFLAACC ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLAACC');
              +",ANFLBLVE ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLBLVE');
              +",ANFLCODI ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLCODI');
              +",ANFLCONA ="+cp_NullLink(cp_ToStrODBC("U"),'CONTI','ANFLCONA');
              +",ANFLESIG ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANFLESIG');
              +",ANFLFIDO ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLFIDO),'CONTI','ANFLFIDO');
              +",ANFLGAVV ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANFLGAVV');
              +",ANFLGCON ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLGCON');
              +",ANFLRAGG ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANFLRAGG');
              +",ANGESCON ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANGESCON');
              +",ANIBARID ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANIBARID');
              +",ANINDWEB ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANINDWEB');
              +",ANLOCNAS ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANLOCNAS');
              +",ANNUMCOR ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANNUMCOR');
              +",ANPARIVA ="+cp_NullLink(cp_ToStrODBC(this.w_CLPARIVA),'CONTI','ANPARIVA');
              +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANPARTSN');
              +",ANPERFIS ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANPERFIS');
              +",ANPREBOL ="+cp_NullLink(cp_ToStrODBC("N"),'CONTI','ANPREBOL');
              +",ANPRONAS ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANPRONAS');
              +",ANSCORPO ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANSCORPO');
              +",ANTIPFAT ="+cp_NullLink(cp_ToStrODBC("R"),'CONTI','ANTIPFAT');
              +",ANTIPRIF ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANTIPRIF');
              +",ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(" "),'CONTI','ANTIPSOT');
              +",ANVALFID ="+cp_NullLink(cp_ToStrODBC(this.w_CLVALFID),'CONTI','ANVALFID');
              +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CONTI','UTCC');
              +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'CONTI','UTDC');
                  +i_ccchkf ;
              +" where ";
                  +"ANTIPCON = "+cp_ToStrODBC("C");
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                     )
            else
              update (i_cTable) set;
                  AFFLINTR = " ";
                  ,AN__NOTE = " ";
                  ,AN_EMAIL = this.w_CL_EMAIL;
                  ,AN_EMPEC = this.w_CL_EMPEC;
                  ,AN_SESSO = "M";
                  ,ANBOLFAT = "N";
                  ,ANCATCON = this.w_CATCON;
                  ,ANCLIPOS = this.w_ANCLIPOS;
                  ,ANCODFIS = this.w_CLCODFIS;
                  ,ANCODIVA = SPACE(5);
                  ,ANCODLIN = this.w_CODLIN;
                  ,ANCODSTU = this.w_ANCODSTU;
                  ,ANCONCON = this.w_CONCON;
                  ,ANCONSUP = this.w_MASCON;
                  ,ANDTINVA = this.w_CLDTINVA;
                  ,ANDTOBSO = this.w_CLDTOBSO;
                  ,ANFLAACC = " ";
                  ,ANFLBLVE = " ";
                  ,ANFLCODI = " ";
                  ,ANFLCONA = "U";
                  ,ANFLESIG = "N";
                  ,ANFLFIDO = this.w_ANFLFIDO;
                  ,ANFLGAVV = "N";
                  ,ANFLGCON = " ";
                  ,ANFLRAGG = " ";
                  ,ANGESCON = "N";
                  ,ANIBARID = " ";
                  ,ANINDWEB = " ";
                  ,ANLOCNAS = " ";
                  ,ANNUMCOR = " ";
                  ,ANPARIVA = this.w_CLPARIVA;
                  ,ANPARTSN = "S";
                  ,ANPERFIS = "N";
                  ,ANPREBOL = "N";
                  ,ANPRONAS = " ";
                  ,ANSCORPO = "S";
                  ,ANTIPFAT = "R";
                  ,ANTIPRIF = " ";
                  ,ANTIPSOT = " ";
                  ,ANVALFID = this.w_CLVALFID;
                  ,UTCC = i_CODUTE;
                  ,UTDC = SetInfoDate( g_CALUTD );
                  &i_ccchkf. ;
               where;
                  ANTIPCON = "C";
                  and ANCODICE = this.w_ANCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Write into CLI_VEND
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CLI_VEND_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLTIPCON ="+cp_NullLink(cp_ToStrODBC("C"),'CLI_VEND','CLTIPCON');
              +",CLCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CLI_VEND','CLCODCON');
                  +i_ccchkf ;
              +" where ";
                  +"CLCODCLI = "+cp_ToStrODBC(this.w_MDCODCLI);
                     )
            else
              update (i_cTable) set;
                  CLTIPCON = "C";
                  ,CLCODCON = this.w_ANCODICE;
                  &i_ccchkf. ;
               where;
                  CLCODCLI = this.w_MDCODCLI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Notifica il corretto inserimento Cliente
            this.oParentObject.w_ERRORE = .F.
          endif
        endif
      endif
    endif
    * --- Apre masch.CONFERMA CLIENTE
    if this.oParentObject.w_ERRORE=.T.
      this.w_NOTIFICA = "N"
      do GSPS_KCC with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_ERRORE = IIF(this.w_NOTIFICA="S", .F., .T.)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PAR_VDET'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CLI_VEND'
    this.cWorkTables[4]='MASTRI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
