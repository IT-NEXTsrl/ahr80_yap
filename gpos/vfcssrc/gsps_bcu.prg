* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bcu                                                        *
*              Controllo negozio principale univoco                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_32]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-07                                                      *
* Last revis.: 2003-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bcu",oParentObject)
return(i_retval)

define class tgsps_bcu as StdBatch
  * --- Local variables
  w_CODNEG = space(3)
  w_CODAZI = space(5)
  w_MESS = space(10)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  * --- WorkFile variables
  PAR_VDET_idx=0
  COR_RISP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dai parametri POS GSPS_APA per controllo unico negozio principale
    * --- Per controllo univocit�:'F': esiste gi� un negozio princ. 'T': OK
    this.w_CODNEG = SPACE(3)
    this.w_CODAZI = i_CODAZI
    this.oParentObject.w_UNITST = "T"
    this.w_MESS = ""
    * --- istanzio oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    * --- se attivo il check Esplicita sconti di riga avviso l'utente
    *     Se la data attivazione � pi� alta dell'ultima registrazione blocco il salvataggio
    if this.oParentObject.w_PAFLESSC="S" And this.oParentObject.w_OFLESSC <>"S"
      * --- Select from COR_RISP
      i_nConn=i_TableProp[this.COR_RISP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX(MDDATREG) AS DATAMAX  from "+i_cTable+" COR_RISP ";
            +" where MDDATREG >= "+cp_ToStrODBC(this.oParentObject.w_PADATESC)+" And MDCODNEG = "+cp_ToStrODBC(this.oParentObject.w_PACODNEG)+"";
             ,"_Curs_COR_RISP")
      else
        select MAX(MDDATREG) AS DATAMAX from (i_cTable);
         where MDDATREG >= this.oParentObject.w_PADATESC And MDCODNEG = this.oParentObject.w_PACODNEG;
          into cursor _Curs_COR_RISP
      endif
      if used('_Curs_COR_RISP')
        select _Curs_COR_RISP
        locate for 1=1
        do while not(eof())
        if Not Empty(Cp_Todate(_Curs_COR_RISP.DATAMAX))
          this.w_oPART = this.w_oMESS.addmsgpartNL("Attivato check esplicita sconti di riga%0Esistono registrazioni di vendita con data %1%0Impostare una data attivazione successiva all'ultima registrazione effettuata")
          this.w_oPART.addParam(DTOC(_Curs_COR_RISP.DATAMAX))     
          this.oParentObject.w_UNITST = "F"
        endif
          select _Curs_COR_RISP
          continue
        enddo
        use
      endif
      Vq_Exec("..\GPOS\EXE\QUERY\GSPS_BCU.VQR",this,"GENCOR")
      if USED("GENCOR") And Reccount("GENCOR")>0
        this.w_oMESS.AddMsgPart("Esistono vendite che non hanno ancora generato i corrispettivi%0Prima di attivare il check esplicita sconti di riga,%0devono essere generati i corrispettivi")     
        this.oParentObject.w_UNITST = "F"
      endif
      if this.oParentObject.w_UNITST = "F"
        this.w_oMESS.ah_ErrorMsg()
        i_retcode = 'stop'
        return
      else
        this.w_MESS = "Attivato check esplicita sconti di riga%0La generazione fatture da corrispettivi generati con%0check esplicita sconti di riga disattivo,%0pu� causare incongruenze nel calcolo dei totali documento"
        ah_ErrorMsg(this.w_MESS,"!","")
      endif
    else
      if this.oParentObject.w_PAFLESSC<>"S" And this.oParentObject.w_OFLESSC ="S"
        Vq_Exec("..\GPOS\EXE\QUERY\GSPS_BCU.VQR",this,"GENCOR")
        if USED("GENCOR") And Reccount("GENCOR")>0
          this.w_MESS = "Esistono vendite che non hanno ancora generato i corrispettivi%0Prima di disattivare il check esplicita sconti di riga,%0devono essere generati i corrispettivi"
          this.oParentObject.w_UNITST = "F"
        endif
        if this.oParentObject.w_UNITST = "F"
          ah_ErrorMsg(this.w_MESS,"!","")
          i_retcode = 'stop'
          return
        else
          this.w_MESS = "Disattivato check esplicita sconti di riga%0La generazione fatture da corrispettivi generati con%0check esplicita sconti di riga attivo,%0pu� causare incongruenze nel calcolo dei totali documento"
          ah_ErrorMsg(this.w_MESS,"!","")
        endif
      endif
    endif
    if this.oParentObject.w_PAPRINEG="S"
      * --- Read from PAR_VDET
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_VDET_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PACODNEG"+;
          " from "+i_cTable+" PAR_VDET where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and PAPRINEG = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PACODNEG;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
              and PAPRINEG = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODNEG = NVL(cp_ToDate(_read_.PACODNEG),cp_NullValue(_read_.PACODNEG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0 AND this.w_CODNEG<>this.oParentObject.w_PACODNEG
        this.w_MESS = "Negozio principale gi� presente (%1)%0Considero %2 come nuovo negozio principale?"
        if ah_YesNo(this.w_MESS, "", ALLTRIM(this.w_CODNEG), ALLTRIM(this.oParentObject.w_PACODNEG) )
          * --- Select from PAR_VDET
          i_nConn=i_TableProp[this.PAR_VDET_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select PACODNEG  from "+i_cTable+" PAR_VDET ";
                +" where PACODAZI="+cp_ToStrODBC(this.w_CODAZI)+" AND PACODNEG<>"+cp_ToStrODBC(this.oParentObject.w_PACODNEG)+"";
                 ,"_Curs_PAR_VDET")
          else
            select PACODNEG from (i_cTable);
             where PACODAZI=this.w_CODAZI AND PACODNEG<>this.oParentObject.w_PACODNEG;
              into cursor _Curs_PAR_VDET
          endif
          if used('_Curs_PAR_VDET')
            select _Curs_PAR_VDET
            locate for 1=1
            do while not(eof())
            this.w_CODNEG = NVL(_Curs_PAR_VDET.PACODNEG, "")
            if NOT EMPTY(this.w_CODNEG)
              * --- Write into PAR_VDET
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_VDET_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_VDET_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PAPRINEG ="+cp_NullLink(cp_ToStrODBC(" "),'PAR_VDET','PAPRINEG');
                    +i_ccchkf ;
                +" where ";
                    +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                    +" and PACODNEG = "+cp_ToStrODBC(this.w_CODNEG);
                       )
              else
                update (i_cTable) set;
                    PAPRINEG = " ";
                    &i_ccchkf. ;
                 where;
                    PACODAZI = this.w_CODAZI;
                    and PACODNEG = this.w_CODNEG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
              select _Curs_PAR_VDET
              continue
            enddo
            use
          endif
          this.oParentObject.w_UNITST = "T"
        else
          this.oParentObject.w_UNITST = "F"
        endif
      endif
    else
      * --- Read from PAR_VDET
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_VDET_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PACODNEG"+;
          " from "+i_cTable+" PAR_VDET where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and PAPRINEG = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PACODNEG;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
              and PAPRINEG = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODNEG = NVL(cp_ToDate(_read_.PACODNEG),cp_NullValue(_read_.PACODNEG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_CODNEG) OR this.oParentObject.w_PACODNEG=this.w_CODNEG
        this.w_MESS = "Nessun negozio principale impostato%0Per disattivare il flag negozio principale occorre abilitare un altro negozio principale"
        ah_ErrorMsg(this.w_MESS,,"")
        this.oParentObject.w_UNITST = "F"
      endif
    endif
    if Used("GENCOR")
       
 Select GENCOR 
 Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_VDET'
    this.cWorkTables[2]='COR_RISP'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_COR_RISP')
      use in _Curs_COR_RISP
    endif
    if used('_Curs_PAR_VDET')
      use in _Curs_PAR_VDET
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
