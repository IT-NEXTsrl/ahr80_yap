* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bfi                                                        *
*              Visualizza Fidelity                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-02-21                                                      *
* Last revis.: 2005-02-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIALE,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bfi",oParentObject,m.pSERIALE,m.pTIPO)
return(i_retval)

define class tgsps_bfi as StdBatch
  * --- Local variables
  pSERIALE = space(10)
  pTIPO = space(1)
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Bottone Dettagli da GSPS_SFI
    if this.pTipo="R"
      * --- Premuto bottone 'Ricerca'
      this.oParentObject.NotifyEvent("Esegui")
      Cursor = this.oParentObject.w_Zoom.cCursor
      Select (Cursor) 
 Go Top
      SUM MDIMPPRE, MDPUNFID TO this.oParentObject.w_TOTPRE, this.oParentObject.w_TOTPUN
      Select (Cursor) 
 Go Top
    else
      if this.pTIPO="V"
        * --- vendita negozio
        this.w_PROG = GSPS_MVD()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.Ecpfilter()     
        this.w_PROG.w_MDSERIAL = this.pSERIALE
        this.w_PROG.Ecpsave()     
        * --- carico il record
        this.w_PROG.LoadRec()     
      else
        * --- Movimenti fidelity
        this.w_PROG = GSPS_AMF()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.Ecpfilter()     
        this.w_PROG.w_MFSERIAL = this.pSERIALE
        this.w_PROG.Ecpsave()     
        * --- carico il record
        this.w_PROG.LoadRec()     
      endif
    endif
  endproc


  proc Init(oParentObject,pSERIALE,pTIPO)
    this.pSERIALE=pSERIALE
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIALE,pTIPO"
endproc
