* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bcc                                                        *
*              Controllo flag causale magazzino                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_51]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2002-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bcc",oParentObject)
return(i_retval)

define class tgsps_bcc as StdBatch
  * --- Local variables
  w_MSG = space(200)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Flag causale Magazzino (da GSPS_MVD)
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MDDCMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_MDDCMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_CMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.oParentObject.w_CMFLCASC="+" AND this.oParentObject.w_MDPREZZO>0
      this.oParentObject.w_MDPREZZO = -this.oParentObject.w_MDPREZZO
    endif
    if this.oParentObject.w_MDPREZZO < 0
      if this.oParentObject.w_TOTALE < 0
        this.w_MSG = "IL TOTALE PARZIALE RISULTA NEGATIVO%0LA STAMPA DELLO SCONTRINO POTREBBE RITORNARE ERRORI%0INSERIRE DOPO LA RIGA CON IL RESO"
        ah_ErrorMsg(this.w_MSG,,"")
      endif
    endif
    if used( "DETAIL")
      select DETAIL
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAM_AGAZ'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
