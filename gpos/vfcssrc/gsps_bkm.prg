* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bkm                                                        *
*              Elimina riferimento - mov.magazzino                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_24]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-15                                                      *
* Last revis.: 2003-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bkm",oParentObject,m.pSERIAL)
return(i_retval)

define class tgsps_bkm as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  w_SERIAL = space(10)
  * --- WorkFile variables
  COR_RISP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se il Movimento Magazzino e' comunque eliminabile (da GSMA_BMK)
    this.w_SERIAL = this.pSERIAL
    this.oParentObject.w_OK = .T.
    * --- Select from COR_RISP
    i_nConn=i_TableProp[this.COR_RISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" COR_RISP ";
          +" where MDSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
           ,"_Curs_COR_RISP")
    else
      select * from (i_cTable);
       where MDSERIAL=this.w_SERIAL;
        into cursor _Curs_COR_RISP
    endif
    if used('_Curs_COR_RISP')
      select _Curs_COR_RISP
      locate for 1=1
      do while not(eof())
      this.oParentObject.w_OK = .F.
        select _Curs_COR_RISP
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,pSERIAL)
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COR_RISP'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_COR_RISP')
      use in _Curs_COR_RISP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL"
endproc
