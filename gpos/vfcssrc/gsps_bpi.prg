* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bpi                                                        *
*              Controllo p.IVA/C.fiscale                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_41]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2002-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODI,w_PICF,w_CODICE,w_CODNAZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bpi",oParentObject,m.w_CODI,m.w_PICF,m.w_CODICE,m.w_CODNAZ)
return(i_retval)

define class tgsps_bpi as StdBatch
  * --- Local variables
  w_CODI = space(20)
  w_PICF = space(3)
  w_CODICE = space(20)
  w_CODNAZ = space(5)
  w_CODISO = space(2)
  w_ERRF = .f.
  w_ERR = .f.
  w_MESS = space(20)
  w_PRCAR = space(1)
  w_LENCF = space(2)
  w_ICF = 0
  w_totcf = 0
  w_ASCCA = 0
  w_RISCF = 0
  w_CLCODCLI = space(15)
  w_valdisp = space(40)
  w_ca = space(2)
  w_NRIGHE = 0
  w_APPO = space(15)
  * --- WorkFile variables
  CLI_VEND_idx=0
  NAZIONI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Partita IVA / Cod.Fiscale (ds GSPS_ACV)
    * --- TOLTO w_TIPCLF in quanto � sempre e solo cliente
    this.w_APPO = space(15)
    this.w_CODISO = space(2)
    this.w_ERRF = .T.
    this.w_ERR = .F.
    this.w_MESS = ""
    * --- Controllo Partita IVA / Codice Fiscale
    *     w_CODI: Codice da Verificare
    *     w_PICF: PI=Partita IVA; CF=Codice Fiscale
    if EMPTY(NVL(this.w_CODICE,""))
      this.w_CODICE = ""
    endif
    if EMPTY(NVL(this.w_CODNAZ,""))
      this.w_CODNAZ = g_CODNAZ
    endif
    * ---  Per le Societa' di Capitali (Test C.Fiscale = P.IVA)
    if this.w_PICF="CF" AND LEN(ALLTRIM(this.w_CODI))=11
      this.w_PICF = "PI"
    endif
    this.w_PRCAR = ASC(LEFT(this.w_CODI,1))
    this.w_LENCF = LEN(ALLTRIM(this.w_CODI))
    this.w_ICF = 1
    this.w_ASCCA = 0
    this.w_RISCF = 0
    do case
      case this.w_PICF="PI"
        * --- Controllo Partita Iva
        * ---  Testo se la partita IVA � gi� stata utilizzata - SE VUOTA VA BENE
        if this.w_lencf = 0
          this.w_MESS = ah_Msgformat("Partita IVA non inserita")
          this.w_ERR = .T.
        endif
        * --- Leggo il codice ISO
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NACODISO"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_CODNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NACODISO;
            from (i_cTable) where;
                NACODNAZ = this.w_CODNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_ERR=.F. AND this.w_PICF="PI" AND ( NOT EMPTY(this.w_CODI))
          * --- Read from CLI_VEND
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLCODCLI"+;
              " from "+i_cTable+" CLI_VEND where ";
                  +"CLPARIVA = "+cp_ToStrODBC(this.w_CODI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLCODCLI;
              from (i_cTable) where;
                  CLPARIVA = this.w_CODI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.CLCODCLI),cp_NullValue(_read_.CLCODCLI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if alltrim(this.w_APPO)<>alltrim(this.w_CODICE) AND len(alltrim(this.w_APPO))>0
            this.w_MESS = ah_Msgformat("Partita IVA gia utilizzata dal cliente: %1",this.w_APPO)
            this.w_ERRF = .F.
            this.w_ERR = .T.
          endif
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_PICF="CF"
        * --- Controllo Codice Fiscale
        if this.w_PICF="CF" AND (NOT EMPTY(this.w_CODI))
          * --- Read from CLI_VEND
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLCODCLI"+;
              " from "+i_cTable+" CLI_VEND where ";
                  +"CLCODFIS = "+cp_ToStrODBC(this.w_CODI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLCODCLI;
              from (i_cTable) where;
                  CLCODFIS = this.w_CODI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.CLCODCLI),cp_NullValue(_read_.CLCODCLI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if alltrim(this.w_APPO)<>alltrim(this.w_CODICE) AND len(alltrim(this.w_APPO))>0
            this.w_MESS = ah_Msgformat("Codice fiscale gia utilizzato dal cliente: %1", this.w_APPO)
            this.w_ERRF = .F.
            this.w_ERR = .T.
          endif
          * --- Controllo Codice Fiscale
          do case
            case this.w_lencf=0 AND NOT this.w_ERR
              this.w_MESS = ah_Msgformat("Codice fiscale non inserito")
              this.w_ERR = .T.
            case this.w_lencf <> 16 AND NOT this.w_ERR
              this.w_MESS = ah_Msgformat("Lunghezza codice fiscale errata")
              this.w_ERR = .T.
            case this.w_prcar < 65 OR this.w_prcar > 90 AND NOT this.w_ERR
              this.w_MESS = ah_Msgformat("Primo carattere del codice fiscale errato")
              this.w_ERR = .T.
          endcase
          * --- Se test lunghezza codice OK
          if this.w_ERR=.F.
            * --- Dati per la decodifica del codice Fiscale
            this.w_valdisp = "1 |0 |5 |7 |9 |13|15|17|19|21|2 |4 |18|20|11|3 |6 |8 |12|14|16|10|22|25|24|23|"
            * --- Decodifica dei Caratteri
            do while this.w_icf < this.w_lencf
              this.w_ca = SUBSTR(this.w_CODI, this.w_icf, 1)
              this.w_ASCCA = ASC(this.w_ca) - 65
              if this.w_ascca < 0
                this.w_ASCCA = this.w_ascca + 17
              endif
              * --- Nella stringa e' presente un carattere non ammesso
              if this.w_ascca < 0 OR this.w_ascca > 25
                this.w_MESS = ah_Msgformat("Codice fiscale errato ai sensi di legge")
                this.w_ERR = .T.
                exit
              endif
              * --- Assegna la posizione nella stringa
              if (this.w_icf - INT(this.w_icf/2)*2) <> 0
                * --- assegna posizioni Dispari
                this.w_totcf = this.w_totcf + VAL(SUBSTR(this.w_valdisp, this.w_ascca*3+1,2))
              else
                * ---  assegna posizioni Pari
                this.w_totcf = this.w_totcf + this.w_ascca
              endif
              this.w_ICF = this.w_icf + 1
            enddo
            if NOT this.w_ERR
              this.w_RISCF = CHR(this.w_totcf - INT(this.w_totcf/26)*26 + 65)
              if this.w_riscf <> RIGHT(this.w_codi, 1)
                this.w_MESS = ah_Msgformat("Codice fiscale errato ai sensi di legge")
                this.w_ERR = .T.
              endif
            endif
          endif
        endif
    endcase
    if this.w_ERR = .T.
      if this.w_ERRF=.F.
        this.w_ERRF = ah_YesNo("%1%0Confermi ugualmente?","", this.w_MESS)
      else
        ah_ErrorMsg("%1",,"",this.w_MESS)
      endif
      * --- Codice di errore ritornato
      this.w_PICF = "ER"
    endif
    i_retcode = "stop" 
 i_retval = this.w_ERRF
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_CODNAZ = g_CODNAZ OR this.w_CODISO = "IT"
        * --- ITALIA
        if this.w_ERR=.F. AND (this.w_lencf<>11 OR this.w_lencf<11)
          this.w_MESS = ah_Msgformat("Lunghezza codice partita IVA errata%0Codice ISO 'it' codice IVA di lungh. 11")
          this.w_ERR = .t.
        endif
        * --- Se test lunghezza codice OK
        if this.w_ERR=.F.
          do while this.w_icf<11
            this.w_ca = SUBSTR(this.w_CODI, this.w_icf, 1)
            this.w_ASCCA = VAL(this.w_ca)
            * --- Assegna la posizione nella stringa
            if (this.w_icf - INT(this.w_icf/2)*2) <> 0
              * --- assegna posizioni Dispari
              this.w_totcf = this.w_totcf + this.w_ascca
            else
              * --- assegna posizioni Pari
              this.w_ASCCA = this.w_ascca * 2
              if this.w_ascca > 9
                this.w_ASCCA = this.w_ascca - 9
              endif
              this.w_totcf = this.w_totcf + this.w_ascca
            endif
            this.w_icf = this.w_icf + 1
          enddo
          * --- CONTROLLO FINALE
          *             * --- Le Partite IVA che iniziano per 8 appartengono ai comuni
          *             * --- Trattasi di Codici Fiscali che possono essere usati anche come Partita IVA
          *             * --- Le Partite IVA che iniziano per 9 appartengono ai Condomini
          *             * --- in tutti i casi vale lo stesso tipo di controllo.
          this.w_RISCF = STR(100-this.w_totcf, 3)
          if RIGHT(this.w_riscf,1) <> SUBSTR(this.w_CODI, 11, 1)
            this.w_MESS = ah_Msgformat("Partita IVA errata ai sensi di legge")
            this.w_ERR = .t.
          endif
        endif
      case this.w_CODISO="AT"
        * --- AUSTRIA
        if this.w_ERR=.F. AND NOT (this.w_lencf=9 AND SUBSTR(this.w_CODI,1,1)="U")
          this.w_MESS = "Codice partita IVA errata%0Codice ISO 'at' codice IVA lett. 'U' + lungh. 8"
          this.w_ERR = .t.
        endif
      case this.w_CODISO="NL" OR this.w_CODISO="SE"
        * --- OLANDA, SVEZIA
        if this.w_ERR=.F. AND NOT (this.w_lencf=12)
          this.w_MESS = ah_Msgformat("Lunghezza codice partita IVA errata%0Codice ISO %1 codice IVA di lungh. 12", this.w_CODISO )
          this.w_ERR = .t.
        endif
      case this.w_CODISO="BE" OR this.w_CODISO="DE" OR this.w_CODISO="PT" OR this.w_CODISO="ES"
        * ---  BELGIO,  GERMANIA,  PORTOGALLO,  SPAGNA
        if this.w_ERR=.F. AND NOT (this.w_lencf=9)
          this.w_MESS = ah_Msgformat("Lunghezza codice partita IVA errata%0Codice ISO %1 codice IVA di lungh. 9", this.w_CODISO )
          this.w_ERR = .t.
        endif
      case this.w_CODISO="DK" OR this.w_CODISO="FI" OR this.w_CODISO="EL" OR this.w_CODISO="IE" OR this.w_CODISO="LU"
        * --- DANIMARCA,  FINLANDIA, GRECIA, IRLANDA, LUSSEMBURGO
        if this.w_ERR=.F. AND NOT (this.w_lencf=8)
          this.w_MESS = ah_Msgformat("Lunghezza codice partita IVA errata%0Codice ISO %1 codice IVA di lungh. 8", this.w_CODISO )
          this.w_ERR = .t.
        endif
      case this.w_CODISO="FR"
        * --- FRANCIA
        if this.w_ERR=.F. AND NOT (this.w_lencf=11)
          this.w_MESS = ah_Msgformat("Lunghezza codice partita IVA errata%0Codice ISO %1 codice IVA di lungh. 11", this.w_CODISO )
          this.w_ERR = .t.
        endif
      case this.w_CODISO="SM"
        * --- SAN MARINO
        if this.w_ERR=.F. AND NOT (this.w_lencf=5)
          this.w_MESS = ah_Msgformat("Lunghezza codice partita IVA errata%0Codice ISO %1 codice IVA di lungh. 5", this.w_CODISO )
          this.w_ERR = .t.
        endif
      case this.w_CODISO="GB"
        * --- GRAN BRETAGNA
        if this.w_ERR=.F. AND NOT (this.w_lencf=12 OR this.w_lencf=9 OR this.w_lencf=5)
          this.w_MESS = ah_Msgformat("Lunghezza codice partita IVA errata%0Codice ISO %1 codice IVA di lungh. 12 o 9 o 5", this.w_CODISO )
          this.w_ERR = .t.
        endif
    endcase
  endproc


  proc Init(oParentObject,w_CODI,w_PICF,w_CODICE,w_CODNAZ)
    this.w_CODI=w_CODI
    this.w_PICF=w_PICF
    this.w_CODICE=w_CODICE
    this.w_CODNAZ=w_CODNAZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CLI_VEND'
    this.cWorkTables[2]='NAZIONI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODI,w_PICF,w_CODICE,w_CODNAZ"
endproc
