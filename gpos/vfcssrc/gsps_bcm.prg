* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bcm                                                        *
*              Ripristina matricole                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-29                                                      *
* Last revis.: 2005-04-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSerial
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bcm",oParentObject,m.pSerial)
return(i_retval)

define class tgsps_bcm as StdBatch
  * --- Local variables
  pSerial = space(10)
  w_SERIAL = space(10)
  * --- WorkFile variables
  MOVIMATR_idx=0
  MATR_POS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripristino matricole sul POS dopo cancellazione documento. Da GSVE_BMK
    this.w_SERIAL = this.pSerial
    * --- Ripristino le chiavi dei documenti POS sulle Matricole.
    * --- Write into MOVIMATR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MTSERIAL,MTROWNUM,MTNUMRIF,MTCODMAT"
      do vq_exec with 'GSPS2QAM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
              +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
              +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
              +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MTSERIAL = _t2.POSSER";
          +",MTROWNUM = _t2.POSROW";
      +",MTNUMRIF ="+cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTNUMRIF');
          +i_ccchkf;
          +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
              +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
              +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
              +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
          +"MOVIMATR.MTSERIAL = _t2.POSSER";
          +",MOVIMATR.MTROWNUM = _t2.POSROW";
      +",MOVIMATR.MTNUMRIF ="+cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTNUMRIF');
          +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
              +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
              +" and "+"MOVIMATR.MTNUMRIF = t2.MTNUMRIF";
              +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
          +"MTSERIAL,";
          +"MTROWNUM,";
          +"MTNUMRIF";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.POSSER,";
          +"t2.POSROW,";
          +cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTNUMRIF')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
              +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
              +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
              +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
          +"MTSERIAL = _t2.POSSER";
          +",MTROWNUM = _t2.POSROW";
      +",MTNUMRIF ="+cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTNUMRIF');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
              +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
              +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
              +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MTSERIAL = (select POSSER from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MTROWNUM = (select POSROW from "+i_cQueryTable+" where "+i_cWhere+")";
      +",MTNUMRIF ="+cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTNUMRIF');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Rivalorizzo MVSERRIF ROWRIF e NUMRIF di tutti i Movimenti Matricole che 
    *     contengono il documento che sto cancellando come riferimento.
    *     Rimetto i riferimenti della vendita POS
    * --- Write into MOVIMATR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MTSERRIF,MTROWRIF,MTRIFNUM"
      do vq_exec with 'gsps3qam',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MOVIMATR.MTSERRIF = _t2.MTSERRIF";
              +" and "+"MOVIMATR.MTROWRIF = _t2.MTROWRIF";
              +" and "+"MOVIMATR.MTRIFNUM = _t2.MTRIFNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MTSERRIF = _t2.POSSER";
          +",MTROWRIF = _t2.POSROW";
      +",MTRIFNUM ="+cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTRIFNUM');
          +i_ccchkf;
          +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MOVIMATR.MTSERRIF = _t2.MTSERRIF";
              +" and "+"MOVIMATR.MTROWRIF = _t2.MTROWRIF";
              +" and "+"MOVIMATR.MTRIFNUM = _t2.MTRIFNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
          +"MOVIMATR.MTSERRIF = _t2.POSSER";
          +",MOVIMATR.MTROWRIF = _t2.POSROW";
      +",MOVIMATR.MTRIFNUM ="+cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTRIFNUM');
          +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MOVIMATR.MTSERRIF = t2.MTSERRIF";
              +" and "+"MOVIMATR.MTROWRIF = t2.MTROWRIF";
              +" and "+"MOVIMATR.MTRIFNUM = t2.MTRIFNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
          +"MTSERRIF,";
          +"MTROWRIF,";
          +"MTRIFNUM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.POSSER,";
          +"t2.POSROW,";
          +cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTRIFNUM')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MOVIMATR.MTSERRIF = _t2.MTSERRIF";
              +" and "+"MOVIMATR.MTROWRIF = _t2.MTROWRIF";
              +" and "+"MOVIMATR.MTRIFNUM = _t2.MTRIFNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
          +"MTSERRIF = _t2.POSSER";
          +",MTROWRIF = _t2.POSROW";
      +",MTRIFNUM ="+cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTRIFNUM');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MTSERRIF = "+i_cQueryTable+".MTSERRIF";
              +" and "+i_cTable+".MTROWRIF = "+i_cQueryTable+".MTROWRIF";
              +" and "+i_cTable+".MTRIFNUM = "+i_cQueryTable+".MTRIFNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MTSERRIF = (select POSSER from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MTROWRIF = (select POSROW from "+i_cQueryTable+" where "+i_cWhere+")";
      +",MTRIFNUM ="+cp_NullLink(cp_ToStrODBC(-30),'MOVIMATR','MTRIFNUM');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Cancello i riferimenti delle matricole ripristinate
    * --- Delete from MATR_POS
    i_nConn=i_TableProp[this.MATR_POS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MATR_POS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MPDOCSER = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      delete from (i_cTable) where;
            MPDOCSER = this.w_SERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,pSerial)
    this.pSerial=pSerial
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOVIMATR'
    this.cWorkTables[2]='MATR_POS'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSerial"
endproc
