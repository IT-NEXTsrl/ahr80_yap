* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bck                                                        *
*              Check finali                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-08                                                      *
* Last revis.: 2012-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bck",oParentObject)
return(i_retval)

define class tgsps_bck as StdBatch
  * --- Local variables
  w_OK = .f.
  w_PADRE = .NULL.
  w_MDCODNEG = space(3)
  w_MDCODCLI = space(15)
  w_ERRORE = .f.
  w_FLDEL = .f.
  w_MESS = space(200)
  w_FLAGLO = space(1)
  w_COART = space(20)
  w_NR = space(4)
  w_TIPRIG = space(1)
  w_CODLOT = space(20)
  w_QTAUM1 = 0
  w_CODUBI = space(20)
  w_CODMAG = space(5)
  w_CFUNC = space(10)
  w_PA3KCORR = space(5)
  w_PA3KRICF = space(5)
  w_PA3K_CAU = space(5)
  w_PA3KIMPO = space(1)
  w_PA3KDFIS = space(1)
  w_PAACCPRE = space(1)
  w_PAPAGCON = space(1)
  w_PAPAGASS = space(1)
  w_PAPAGCAR = space(1)
  w_PAPAGFIN = space(1)
  w_PAPAGCLI = space(1)
  w_IAMINFAT = 0
  w_IAMINCOR = 0
  w_IAMINFCO = 0
  w_IALORFCO = space(1)
  w_CLPARIVA = space(10)
  w_CLCODFIS = space(10)
  w_CLNAZION = space(3)
  w_NACODISO = space(3)
  w_CLCODCON = space(15)
  w_MESSAGE = space(10)
  w_TOTDOC3K = 0
  w_ANNO = space(4)
  * --- WorkFile variables
  PAR_VDET_idx=0
  DAT_IVAN_idx=0
  CLI_VEND_idx=0
  NAZIONI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli finali fuori transazione lanciato dalla manual Block Check Form della maschera GSPS_MVD con Evento ChkFinali
    this.w_OK = .T.
    this.w_PADRE = this.oParentObject
    this.w_CFUNC = this.w_PADRE.cFunction
    this.w_FLDEL = this.w_CFUNC="Query"
    * --- Controllo dati di testata
    if this.w_CFUNC="Load"
      this.w_MDCODNEG = this.w_PADRE.w_MDCODNEG
      this.w_MDCODCLI = this.w_PADRE.w_MDCODCLI
      this.oParentObject.w_RESCHK = 0
      if empty(nvl(this.w_MDCODNEG,""))
        ah_ErrorMsg("Negozio non definito")
        this.oParentObject.w_RESCHK = -1
      else
        if g_AIFT="S"
          * --- Se l'importo � maggiore di 3000 euro, dal 01/07/2011 � obbligatorio per legge indicare l'intestatario
          this.w_ANNO = Alltrim(str(year(this.oParentObject.w_MDDATREG)))
          * --- Read from DAT_IVAN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IAMINFAT,IAMINCOR,IAMINFCO,IALORFCO"+;
              " from "+i_cTable+" DAT_IVAN where ";
                  +"IACODAZI = "+cp_ToStrODBC(i_codazi);
                  +" and IA__ANNO = "+cp_ToStrODBC(this.w_ANNO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IAMINFAT,IAMINCOR,IAMINFCO,IALORFCO;
              from (i_cTable) where;
                  IACODAZI = i_codazi;
                  and IA__ANNO = this.w_ANNO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IAMINFAT = NVL(cp_ToDate(_read_.IAMINFAT),cp_NullValue(_read_.IAMINFAT))
            this.w_IAMINCOR = NVL(cp_ToDate(_read_.IAMINCOR),cp_NullValue(_read_.IAMINCOR))
            this.w_IAMINFCO = NVL(cp_ToDate(_read_.IAMINFCO),cp_NullValue(_read_.IAMINFCO))
            this.w_IALORFCO = NVL(cp_ToDate(_read_.IALORFCO),cp_NullValue(_read_.IALORFCO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from PAR_VDET
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_VDET_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PA3KCORR,PA3KRICF,PA3KIMPO,PA3KDFIS,PAACCPRE,PAPAGCON,PAPAGASS,PAPAGCAR,PAPAGFIN,PAPAGCLI"+;
              " from "+i_cTable+" PAR_VDET where ";
                  +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                  +" and PACODNEG = "+cp_ToStrODBC(this.w_MDCODNEG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PA3KCORR,PA3KRICF,PA3KIMPO,PA3KDFIS,PAACCPRE,PAPAGCON,PAPAGASS,PAPAGCAR,PAPAGFIN,PAPAGCLI;
              from (i_cTable) where;
                  PACODAZI = i_codazi;
                  and PACODNEG = this.w_MDCODNEG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PA3KCORR = NVL(cp_ToDate(_read_.PA3KCORR),cp_NullValue(_read_.PA3KCORR))
            this.w_PA3KRICF = NVL(cp_ToDate(_read_.PA3KRICF),cp_NullValue(_read_.PA3KRICF))
            this.w_PA3KIMPO = NVL(cp_ToDate(_read_.PA3KIMPO),cp_NullValue(_read_.PA3KIMPO))
            this.w_PA3KDFIS = NVL(cp_ToDate(_read_.PA3KDFIS),cp_NullValue(_read_.PA3KDFIS))
            this.w_PAACCPRE = NVL(cp_ToDate(_read_.PAACCPRE),cp_NullValue(_read_.PAACCPRE))
            this.w_PAPAGCON = NVL(cp_ToDate(_read_.PAPAGCON),cp_NullValue(_read_.PAPAGCON))
            this.w_PAPAGASS = NVL(cp_ToDate(_read_.PAPAGASS),cp_NullValue(_read_.PAPAGASS))
            this.w_PAPAGCAR = NVL(cp_ToDate(_read_.PAPAGCAR),cp_NullValue(_read_.PAPAGCAR))
            this.w_PAPAGFIN = NVL(cp_ToDate(_read_.PAPAGFIN),cp_NullValue(_read_.PAPAGFIN))
            this.w_PAPAGCLI = NVL(cp_ToDate(_read_.PAPAGCLI),cp_NullValue(_read_.PAPAGCLI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_PA3KIMPO $ "WB"
            this.w_TOTDOC3K = this.oParentObject.w_MDIMPABB
            this.w_TOTDOC3K = this.w_TOTDOC3K + IIF(this.w_PAACCPRE="S", this.oParentObject.w_MDACCPRE, 0)
            this.w_TOTDOC3K = this.w_TOTDOC3K + IIF(this.w_PAPAGCON="S", this.oParentObject.w_MDPAGCON, 0)
            this.w_TOTDOC3K = this.w_TOTDOC3K + IIF(this.w_PAPAGASS="S", this.oParentObject.w_MDPAGASS, 0)
            this.w_TOTDOC3K = this.w_TOTDOC3K + IIF(this.w_PAPAGCAR="S", this.oParentObject.w_MDPAGCAR, 0)
            this.w_TOTDOC3K = this.w_TOTDOC3K + IIF(this.w_PAPAGFIN="S", this.oParentObject.w_MDPAGFIN, 0)
            this.w_TOTDOC3K = this.w_TOTDOC3K + IIF(this.w_PAPAGCLI="S", this.oParentObject.w_MDPAGCLI, 0)
            do case
              case inlist(this.oParentObject.w_MDTIPCHI, "NS", "BV", "ES", "RF")
                * --- Nessuna stampa, Brogliaccio di vendita, Emissione scontrino
                if this.w_TOTDOC3K >= this.w_IAMINCOR
                  this.oParentObject.w_EDCL3KEU = .T.
                  this.w_PA3K_CAU = IIF(this.oParentObject.w_MDTIPCHI="RF", this.w_PA3KRICF, this.w_PA3KCORR)
                  if EMPTY(this.w_MDCODCLI)
                    this.w_MESSAGE = "Controllo operazioni maggiori di 3.000 euro%0Codice cliente non impostato."
                    if this.w_PA3KIMPO="B"
                      ah_errormsg(this.w_MESSAGE)
                      this.oParentObject.w_RESCHK = -1
                    else
                      this.w_MESSAGE = this.w_MESSAGE + "%0Si desidera proseguire comunque?"
                      if NOT AH_YESNO(this.w_MESSAGE)
                        this.oParentObject.w_RESCHK = -1
                      endif
                    endif
                  else
                    this.Pag3()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                endif
              case inlist(this.oParentObject.w_MDTIPCHI, "FF")
                * --- Fattura fiscale
                if this.w_TOTDOC3K >= this.w_IAMINFCO
                  if EMPTY(this.w_MDCODCLI)
                    this.w_MESSAGE = "Controllo operazioni maggiori di 3.000 euro%0Codice cliente non impostato."
                    ah_errormsg(this.w_MESSAGE)
                    this.oParentObject.w_RESCHK = -1
                  else
                    this.Pag3()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                endif
              case inlist(this.oParentObject.w_MDTIPCHI, "RF")
                * --- Ricevuta fiscale
            endcase
          endif
        endif
      endif
    endif
    if this.oParentObject.w_RESCHK<>-1
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      * --- Primo giro sulle righe non cancellate...
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        if this.w_PADRE.FullRow()
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Se tutto ok passo alla prossima riga altrimenti esco...
        if this.w_OK
          this.w_PADRE.NextRow()     
        else
          if not Empty( this.w_MESS )
            ah_ErrorMsg("%1",,"", this.w_MESS)
          endif
          Exit
        endif
      enddo
      * --- Secondo giro, se tutto Ok sulle righe cancellate...
      if this.w_OK
        this.w_PADRE.FirstRowDel()     
        * --- Test Se riga Eliminata
        this.w_FLDEL = .T.
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Se tutto ok passo alla prossima riga altrimenti esco...
          if this.w_OK
            this.w_PADRE.NextRowDel()     
          else
            if not Empty( this.w_MESS )
              ah_ErrorMsg("%1",,"", this.w_MESS)
            endif
            Exit
          endif
        enddo
      endif
      this.w_PADRE.RePos(.F.)     
    endif
    if Not this.w_OK
      this.oParentObject.w_RESCHK = -1
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ciclo sulle righe
    if this.w_PADRE.FullRow()
      this.w_TIPRIG = NVL(this.oParentObject.w_MDTIPRIG," ")
      this.w_FLAGLO = NVL(this.oParentObject.w_MDFLLOTT," ")
      this.w_CODLOT = NVL(this.oParentObject.w_MDCODLOT,Space(20))
      this.w_QTAUM1 = NVL(this.oParentObject.w_MDQTAUM1, 0)
      this.w_CODUBI = NVL(this.oParentObject.w_MDCODUBI,Space(20))
      this.w_CODMAG = NVL(this.oParentObject.w_MDMAGRIG, " ")
      this.w_COART = NVL(this.oParentObject.w_MDCODART, Space(20))
      this.w_NR = ALLTRIM(STR(this.oParentObject.w_CPROWORD))
      if this.w_OK And ((g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" ) OR g_PERUBI="S") AND this.w_TIPRIG $ "RM" 
        do case
          case EMPTY(this.w_CODLOT) AND this.w_FLAGLO $ "+-" AND this.w_QTAUM1<>0 AND g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" AND Not this.oParentObject.w_TESLOT
            this.w_MESS = ah_Msgformat("Riga movimento: %1%0Articolo: %2%0Inserire codice lotto",this.w_NR, ALLTRIM(this.w_COART) )
            this.w_OK = .F.
          case g_PERUBI="S" And EMPTY(this.w_CODUBI) AND NOT EMPTY(this.w_CODMAG) AND this.oParentObject.w_FLUBIC="S" AND this.w_FLAGLO $ "+-"
            * --- Controllo Disponibilita' Lotti/Ubicazioni
            this.w_MESS = ah_Msgformat("Riga movimento: %1%0Magazzino: %2%0Inserire codice ubicazione",this.w_NR, alltrim(this.w_CODMAG) )
            this.w_OK = .F.
        endcase
      endif
      if this.oParentObject.w_PASQTA = "S" And (this.oParentObject.w_MDQTAMOV <> cp_Round(this.oParentObject.w_MDQTAMOV, 2) Or this.oParentObject.w_MDPREZZO <> cp_Round(this.oParentObject.w_MDPREZZO, 2)) And this.oParentObject.w_MDTIPRIG <>"D" And this.oParentObject.w_MDTIPCHI = "ES"
        this.w_MESS = ah_Msgformat("Impossibile utilizzare prezzo o quantit� con pi� di due decimali se attivo invio quantit� sul dispositivo cassa installato")
        this.w_OK = .F.
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica i dati fiscali del cliente
    if this.w_PA3KDFIS $ "WB"
      * --- Read from CLI_VEND
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLPARIVA,CLCODFIS,CLNAZION,CLCODCON"+;
          " from "+i_cTable+" CLI_VEND where ";
              +"CLCODCLI = "+cp_ToStrODBC(this.w_MDCODCLI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLPARIVA,CLCODFIS,CLNAZION,CLCODCON;
          from (i_cTable) where;
              CLCODCLI = this.w_MDCODCLI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CLPARIVA = NVL(cp_ToDate(_read_.CLPARIVA),cp_NullValue(_read_.CLPARIVA))
        this.w_CLCODFIS = NVL(cp_ToDate(_read_.CLCODFIS),cp_NullValue(_read_.CLCODFIS))
        this.w_CLNAZION = NVL(cp_ToDate(_read_.CLNAZION),cp_NullValue(_read_.CLNAZION))
        this.w_CLCODCON = NVL(cp_ToDate(_read_.CLCODCON),cp_NullValue(_read_.CLCODCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from NAZIONI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.NAZIONI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NACODISO"+;
          " from "+i_cTable+" NAZIONI where ";
              +"NACODNAZ = "+cp_ToStrODBC(this.w_CLNAZION);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NACODISO;
          from (i_cTable) where;
              NACODNAZ = this.w_CLNAZION;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NACODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_NACODISO="IT " or empty(this.w_NACODISO)
        * --- Cliente nazionale, deve avere almeno il codice fiscale
        if EMPTY(this.w_CLCODFIS) and EMPTY(this.w_CLPARIVA)
          this.w_MESSAGE = "Controllo operazioni maggiori di 3.000 euro%0Codice Fiscale o Partita IVA cliente non impostati."
          if this.w_PA3KDFIS="B"
            ah_errormsg(this.w_MESSAGE)
            this.oParentObject.w_RESCHK = -1
          else
            this.w_MESSAGE = this.w_MESSAGE + "%0Si desidera proseguire comunque?"
            if NOT AH_YESNO(this.w_MESSAGE)
              this.oParentObject.w_RESCHK = -1
            endif
          endif
        endif
        if empty(nvl(this.w_CLCODCON,""))
          * --- Genero il cliente
          this.w_ERRORE = False
          do gsps_bkc with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_ERRORE
            ah_errormsg("Cliente negozio non registrato")
            this.oParentObject.w_RESCHK = -1
          endif
        endif
      else
        * --- Cliente estero, ancora non si sa quali siano i controlli da eseguire
      endif
    endif
    if this.oParentObject.w_RESCHK=>0
      * --- Se l'importo � maggiore di 3000 euro, cambia la causale
      this.w_MESSAGE = ah_msgformat("Operazione maggiore di 3.000 euro%0Modifico il tipo documento?")
      if this.oParentObject.w_MDTIPDOC<>this.w_PA3K_CAU and not empty(nvl(this.w_PA3K_CAU,"")) and cp_YesNo(this.w_MESSAGE)
        this.oParentObject.w_EDTIPDOC = True
        this.oParentObject.w_zMDTIPDOC = this.oParentObject.w_MDTIPDOC
        this.oParentObject.w_oMDTIPDOC = this.w_PA3K_CAU
        this.w_PADRE.mEnableControls()     
        =setvaluelinked("M", this.w_PADRE, "w_MDTIPDOC", this.w_PA3K_CAU)
        this.oParentObject.w_MDTIPCOR = IIF(this.oParentObject.w_MDTIPCHI = "FF", this.oParentObject.w_DOCCOR, IIF(this.oParentObject.w_MDTIPCHI $ "NS-ES-BV", this.oParentObject.w_MDTIPDOC, SPACE(5)))
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PAR_VDET'
    this.cWorkTables[2]='DAT_IVAN'
    this.cWorkTables[3]='CLI_VEND'
    this.cWorkTables[4]='NAZIONI'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
