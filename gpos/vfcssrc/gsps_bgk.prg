* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bgk                                                        *
*              Generazione movimenti da kit                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_58]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-15                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bgk",oParentObject)
return(i_retval)

define class tgsps_bgk as StdBatch
  * --- Local variables
  w_MMSERIAL = space(10)
  w_MMNUMREG = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MMTCAMAG = space(5)
  w_MMCAUMAG = space(5)
  w_MMFLCASC = space(1)
  w_MMFLORDI = space(1)
  w_MMFLIMPE = space(1)
  w_MMFLRISE = space(1)
  w_MMFLELGM = space(1)
  w_FLAVA1 = space(1)
  w_MMCODMAG = space(5)
  w_MMCODESE = space(4)
  w_MMVALNAZ = space(3)
  w_MMCODUTE = 0
  w_MMDATREG = ctod("  /  /  ")
  w_MMTCOLIS = space(5)
  w_MMNUMDOC = 0
  w_MMALFDOC = space(10)
  w_MMDATDOC = ctod("  /  /  ")
  w_MMDESSUP = space(40)
  w_MMCODVAL = space(3)
  w_MMCAOVAL = 0
  w_FLLOTT = space(1)
  w_SERSCA = space(10)
  w_SERCAR = space(10)
  w_CONTA = 0
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_SERIALE = space(0)
  w_MMCODICE = space(20)
  w_MMCODART = space(20)
  w_MMUNIMIS = space(3)
  w_MMQTAMOV = 0
  w_MMQTAUM1 = 0
  w_MMNUMRIF = 0
  w_MMCODLIS = space(5)
  w_MMKEYSAL = space(20)
  w_MMFLULCA = space(1)
  w_MMFLULPV = space(1)
  w_MMFLLOTT = space(1)
  w_MMPREZZO = 0
  w_MMVALMAG = 0
  w_MMIMPNAZ = 0
  w_MMVALULT = 0
  w_CAUTES = space(5)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  MVM_MAST_idx=0
  MVM_DETT_idx=0
  SALDIART_idx=0
  COR_RISP_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Movimenti di Magazzino da KIT (da GSPS_MVD)
    if NOT EMPTY(this.oParentObject.w_CARKIT) AND NOT EMPTY(this.oParentObject.w_SCAKIT)
      * --- Valorizzo variabili per creazione testata movimenti di magazzino
      * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
      this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
      this.w_MMCODESE = g_CODESE
      this.w_MMVALNAZ = g_PERVAL
      this.w_MMCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
      this.w_MMDATREG = this.oParentObject.w_MDDATREG
      this.w_MMTCOLIS = this.oParentObject.w_MDCODLIS
      this.w_MMNUMDOC = this.oParentObject.w_MDNUMDOC
      this.w_MMALFDOC = this.oParentObject.w_MDALFDOC
      this.w_MMDATDOC = this.oParentObject.w_MDDATREG
      this.w_MMDESSUP = " "
      this.w_MMCODVAL = g_PERVAL
      this.w_MMCAOVAL = 1
      VQ_EXEC("..\GPOS\EXE\QUERY\GSPS_QCK.VQR",this,"GENEKIT")
      if USED("GENEKIT")
        SELECT "GENEKIT"
        if RECCOUNT()>0
          this.w_CONTA = 0
          SELECT "GENEKIT"
          GO TOP
          COUNT FOR NOT EMPTY(KICODCOM) AND !(ARTIPART$"FM-FO-DE") TO this.w_CONTA
          GO TOP
          if this.w_CONTA>0
            * --- Raggruppa gli Scarichi
            SELECT KICODCOM, KIARTCOM, KIUNIMIS, MAX(NVL(KIFLLOTT, " ")) AS FLLOTT, MAX(NVL(ARTIPART,"  ")) AS TIPART, ;
            SUM(KIQTAMOV*MDQTAUM1) AS QTAMOV, SUM(KIQTAUM1*MDQTAUM1) AS QTAUM1, MAX(NVL(PRCOSSTA,0)) AS COSSTA ;
            FROM GENEKIT GROUP BY KICODCOM, KIARTCOM, KIUNIMIS ORDER BY 1 INTO CURSOR "APPO"
            * --- Scrive testata movimento di magazzino
            this.w_CAUTES = this.oParentObject.w_SCAKIT
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_SERSCA = this.w_MMSERIAL
            this.w_CPROWNUM = 0
            this.w_CPROWORD = 0
            SELECT "APPO"
            GO TOP
            SCAN FOR NOT EMPTY(KICODCOM) AND !(TIPART$"FM-FO-DE")
            this.w_MMCODICE = KICODCOM
            this.w_MMCODART = KIARTCOM
            this.w_MMUNIMIS = KIUNIMIS
            this.w_MMQTAMOV = QTAMOV
            this.w_MMQTAUM1 = QTAUM1
            this.w_MMPREZZO = COSSTA
            this.w_FLLOTT = IIF(g_PERLOT="S", FLLOTT, " ")
            if this.w_MMQTAMOV<>this.w_MMQTAUM1
              * --- Se U.M. Differenti riporta il prezzo alla U.M. di Riga
              this.w_MMPREZZO = cp_ROUND((this.w_MMPREZZO * this.w_MMQTAUM1) / this.w_MMQTAMOV, g_PERPUL)
            endif
            ah_Msg("Scrittura movimento di scarico componenti da kit; articolo: %1",.T.,.F.,.F.,ALLTRIM(this.w_MMCODART) )
            this.w_SERIALE = this.w_SERSCA
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT APPO
            ENDSCAN 
            USE IN SELECT("APPO")
          endif
          * --- Raggruppa i Carichi
          SELECT CPROWNUM, CPROWORD, MAX(NVL(MDFLLOTT, " ")) AS FLLOTT, ;
          MAX(MDCODICE) AS MDCODICE, MAX(MDCODART) AS MDCODART, MAX(MDUNIMIS) AS MDUNIMIS, ;
          MAX(MDQTAMOV) AS QTAMOV, MAX(MDQTAUM1) AS QTAUM1, SUM(KIQTAUM1*NVL(PRCOSSTA,0)) AS COSSTA ;
          FROM GENEKIT GROUP BY CPROWNUM, CPROWORD ORDER BY 2 INTO CURSOR APPO
          * --- Scrive testata movimento di magazzino
          this.w_CAUTES = this.oParentObject.w_CARKIT
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_SERCAR = this.w_MMSERIAL
          this.w_CPROWNUM = 0
          this.w_CPROWORD = 0
          SELECT "APPO"
          GO TOP
          SCAN FOR NOT EMPTY(MDCODICE) 
          this.w_MMCODICE = MDCODICE
          this.w_MMCODART = MDCODART
          this.w_MMUNIMIS = MDUNIMIS
          this.w_MMQTAMOV = QTAMOV
          this.w_MMQTAUM1 = QTAUM1
          this.w_MMPREZZO = COSSTA
          this.w_FLLOTT = IIF(g_PERLOT="S", FLLOTT, " ")
          if this.w_MMQTAMOV<>this.w_MMQTAUM1
            * --- Se U.M. Differenti riporta il prezzo alla U.M. di Riga
            this.w_MMPREZZO = cp_ROUND((this.w_MMPREZZO * this.w_MMQTAUM1) / this.w_MMQTAMOV, g_PERPUL)
          endif
          ah_Msg("Scrittura movimento di carico da kit; articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_MMCODART) )
          this.w_SERIALE = this.w_SERCAR
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT "APPO"
          ENDSCAN 
          USE IN SELECT("APPO")
        endif
      endif
      USE IN SELECT("GENEKIT")
      VQ_EXEC("..\GPOS\EXE\QUERY\GSPS1QCK.VQR",this,"GENEKIT")
      if USED("GENEKIT")
        SELECT "GENEKIT"
        if RECCOUNT()>0
          this.w_CONTA = 0
          SELECT "GENEKIT"
          GO TOP
          COUNT FOR NOT EMPTY(KICODCOM) AND !(ARTIPART$"FM-FO-DE") TO this.w_CONTA
          GO TOP
          if this.w_CONTA>0
            * --- Raggruppa i componenti per Resi
            SELECT KICODCOM, KIARTCOM, KIUNIMIS, MAX(NVL(KIFLLOTT, " ")) AS FLLOTT, MAX(NVL(ARTIPART,"  ")) AS TIPART, ;
            SUM(KIQTAMOV*MDQTAUM1) AS QTAMOV, SUM(KIQTAUM1*MDQTAUM1) AS QTAUM1, MAX(NVL(PRCOSSTA,0)) AS COSSTA ;
            FROM GENEKIT GROUP BY KICODCOM, KIARTCOM, KIUNIMIS ORDER BY 1 INTO CURSOR "APPO"
            if Empty( this.w_SERCAR )
              * --- Scrive testata movimento di magazzino
              this.w_CAUTES = this.oParentObject.w_CARKIT
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_SERCAR = this.w_MMSERIAL
            else
              * --- Read from CAM_AGAZ
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM"+;
                  " from "+i_cTable+" CAM_AGAZ where ";
                      +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CARKIT);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM;
                  from (i_cTable) where;
                      CMCODICE = this.oParentObject.w_CARKIT;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
                this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
                this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
                this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
                this.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
                this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- In caso vi siano pi� kit nel solito movimento sia di reso che di vendita
              *     il cprownum potrebbe disallinearsi tra carichi e scarichi e dare errore in 
              *     scrittura del movimento di magazzino
              * --- Select from MVM_DETT
              i_nConn=i_TableProp[this.MVM_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS LAST_ROWN,MAX(CPROWORD) AS LAST_ROWO  from "+i_cTable+" MVM_DETT ";
                    +" where MMSERIAL = "+cp_ToStrODBC(this.w_SERCAR)+"";
                    +" group by MMSERIAL";
                     ,"_Curs_MVM_DETT")
              else
                select MAX(CPROWNUM) AS LAST_ROWN,MAX(CPROWORD) AS LAST_ROWO from (i_cTable);
                 where MMSERIAL = this.w_SERCAR;
                 group by MMSERIAL;
                  into cursor _Curs_MVM_DETT
              endif
              if used('_Curs_MVM_DETT')
                select _Curs_MVM_DETT
                locate for 1=1
                do while not(eof())
                this.w_CPROWNUM = NVL(_Curs_MVM_DETT.LAST_ROWN, 0)
                this.w_CPROWORD = NVL(_Curs_MVM_DETT.LAST_ROWO, 0)
                  select _Curs_MVM_DETT
                  continue
                enddo
                use
              endif
              this.w_MMCAUMAG = this.oParentObject.w_CARKIT
            endif
            SELECT "APPO"
            GO TOP
            SCAN FOR NOT EMPTY(KICODCOM) AND !(TIPART$"FM-FO-DE")
            this.w_MMCODICE = KICODCOM
            this.w_MMCODART = KIARTCOM
            this.w_MMUNIMIS = KIUNIMIS
            this.w_MMQTAMOV = QTAMOV
            this.w_MMQTAUM1 = QTAUM1
            this.w_MMPREZZO = COSSTA
            this.w_FLLOTT = IIF(g_PERLOT="S", FLLOTT, " ")
            if this.w_MMQTAMOV<>this.w_MMQTAUM1
              * --- Se U.M. Differenti riporta il prezzo alla U.M. di Riga
              this.w_MMPREZZO = cp_ROUND((this.w_MMPREZZO * this.w_MMQTAUM1) / this.w_MMQTAMOV, g_PERPUL)
            endif
            ah_Msg("Scrittura movimento di carico componenti da kit; articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_MMCODART))
            this.w_SERIALE = this.w_SERCAR
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT "APPO"
            ENDSCAN 
            USE IN SELECT("APPO")
          endif
          * --- Raggruppa i Kit di Resi
          SELECT CPROWNUM, CPROWORD, MAX(NVL(MDFLLOTT, " ")) AS FLLOTT, ;
          MAX(MDCODICE) AS MDCODICE, MAX(MDCODART) AS MDCODART, MAX(MDUNIMIS) AS MDUNIMIS, ;
          MAX(MDQTAMOV) AS QTAMOV, MAX(MDQTAUM1) AS QTAUM1, SUM(KIQTAUM1*NVL(PRCOSSTA,0)) AS COSSTA ;
          FROM GENEKIT GROUP BY CPROWNUM, CPROWORD ORDER BY 2 INTO CURSOR APPO
          if Empty( this.w_SERSCA )
            * --- Scrive testata movimento di magazzino
            this.w_CAUTES = this.oParentObject.w_SCAKIT
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_SERSCA = this.w_MMSERIAL
          else
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_SCAKIT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM;
                from (i_cTable) where;
                    CMCODICE = this.oParentObject.w_SCAKIT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
              this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
              this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
              this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
              this.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
              this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- In caso vi siano pi� kit nel solito movimento sia di reso che di vendita
            *     il cprownum potrebbe disallinearsi tra carichi e scarichi e dare errore in 
            *     scrittura del movimento di magazzino
            * --- Select from MVM_DETT
            i_nConn=i_TableProp[this.MVM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS LAST_ROWN,MAX(CPROWORD) AS LAST_ROWO  from "+i_cTable+" MVM_DETT ";
                  +" where MMSERIAL = "+cp_ToStrODBC(this.w_SERSCA)+"";
                  +" group by MMSERIAL";
                   ,"_Curs_MVM_DETT")
            else
              select MAX(CPROWNUM) AS LAST_ROWN,MAX(CPROWORD) AS LAST_ROWO from (i_cTable);
               where MMSERIAL = this.w_SERSCA;
               group by MMSERIAL;
                into cursor _Curs_MVM_DETT
            endif
            if used('_Curs_MVM_DETT')
              select _Curs_MVM_DETT
              locate for 1=1
              do while not(eof())
              this.w_CPROWNUM = NVL(_Curs_MVM_DETT.LAST_ROWN, 0)
              this.w_CPROWORD = NVL(_Curs_MVM_DETT.LAST_ROWO, 0)
                select _Curs_MVM_DETT
                continue
              enddo
              use
            endif
            this.w_MMCAUMAG = this.oParentObject.w_SCAKIT
          endif
          SELECT "APPO"
          GO TOP
          SCAN FOR NOT EMPTY(MDCODICE) 
          this.w_MMCODICE = MDCODICE
          this.w_MMCODART = MDCODART
          this.w_MMUNIMIS = MDUNIMIS
          this.w_MMQTAMOV = QTAMOV
          this.w_MMQTAUM1 = QTAUM1
          this.w_MMPREZZO = COSSTA
          this.w_FLLOTT = IIF(g_PERLOT="S", FLLOTT, " ")
          if this.w_MMQTAMOV<>this.w_MMQTAUM1
            * --- Se U.M. Differenti riporta il prezzo alla U.M. di Riga
            this.w_MMPREZZO = cp_ROUND((this.w_MMPREZZO * this.w_MMQTAUM1) / this.w_MMQTAMOV, g_PERPUL)
          endif
          ah_Msg("Scrittura movimento di scarico da kit; articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_MMCODART))
          this.w_SERIALE = this.w_SERSCA
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT "APPO"
          ENDSCAN 
          USE IN SELECT("APPO")
        endif
      endif
      * --- Scrive Riferimento Scarico Componenti da KIT
      * --- Try
      local bErr_04067888
      bErr_04067888=bTrsErr
      this.Try_04067888()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Errore scrittura riferimento carichi da KIT")
      endif
      bTrsErr=bTrsErr or bErr_04067888
      * --- End
      USE IN SELECT("GENEKIT")
    endif
  endproc
  proc Try_04067888()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into COR_RISP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COR_RISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.COR_RISP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MDSCAKIT ="+cp_NullLink(cp_ToStrODBC(this.w_SERSCA),'COR_RISP','MDSCAKIT');
      +",MDCARKIT ="+cp_NullLink(cp_ToStrODBC(this.w_SERCAR),'COR_RISP','MDCARKIT');
          +i_ccchkf ;
      +" where ";
          +"MDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDSERIAL);
             )
    else
      update (i_cTable) set;
          MDSCAKIT = this.w_SERSCA;
          ,MDCARKIT = this.w_SERCAR;
          &i_ccchkf. ;
       where;
          MDSERIAL = this.oParentObject.w_MDSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Detail Movimento Magazzino
    *     w_SERIALE identifica il SERIALE della testata alla quale le righe saranno 
    *     collegate
    * --- Aggiorna Detail Movimento di Magazzino
    this.w_MMNUMRIF = -10
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_MMCODLIS = this.w_MMTCOLIS
    this.w_MMKEYSAL = this.w_MMCODART
    this.w_MMFLULCA = " "
    this.w_MMFLULPV = " "
    this.w_MMVALMAG = cp_ROUND((this.w_MMQTAMOV*this.w_MMPREZZO), 2)
    this.w_MMIMPNAZ = this.w_MMVALMAG
    this.w_MMVALULT = IIF(this.w_MMQTAUM1=0, 0, cp_ROUND(this.w_MMIMPNAZ/this.w_MMQTAUM1, g_PERPUL))
    this.w_MMFLLOTT = " "
    if (g_PERLOT="S" AND this.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.oParentObject.w_FLUBIC="S")
      this.w_MMFLLOTT = LEFT(ALLTRIM(this.w_MMFLCASC)+IIF(this.w_MMFLRISE="+", "-", IIF(this.w_MMFLRISE="-", "+", " ")), 1)
    endif
    * --- Try
    local bErr_0417B540
    bErr_0417B540=bTrsErr
    this.Try_0417B540()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile inserire dettaglio movimenti di magazzino%0%1",,"",message())
    endif
    bTrsErr=bTrsErr or bErr_0417B540
    * --- End
    * --- Aggiorna Saldi
    if this.w_MMQTAUM1<>0 AND NOT EMPTY(this.w_MMKEYSAL) AND NOT EMPTY(this.w_MMCODMAG)
      if NOT EMPTY(this.w_MMFLCASC+this.w_MMFLRISE+this.w_MMFLORDI+this.w_MMFLIMPE)
        * --- Try
        local bErr_0416D0B0
        bErr_0416D0B0=bTrsErr
        this.Try_0416D0B0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0416D0B0
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MMKEYSAL;
              and SLCODMAG = this.w_MMCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MMCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_MMCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          * --- Try
          local bErr_0416F450
          bErr_0416F450=bTrsErr
          this.Try_0416F450()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0416F450
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SCQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SCQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SCQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SCQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MMKEYSAL;
                and SCCODMAG = this.w_MMCODMAG;
                and SCCODCAN = this.w_COMMDEFA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
  endproc
  proc Try_0417B540()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MVM_DETT
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+",CPROWNUM"+",MMNUMRIF"+",CPROWORD"+",MMCAUMAG"+",MMCAUCOL"+",MMCODMAG"+",MMCODMAT"+",MMCODLIS"+",MMCODICE"+",MMCODART"+",MMUNIMIS"+",MMQTAMOV"+",MMQTAUM1"+",MMPREZZO"+",MMSCONT1"+",MMSCONT2"+",MMSCONT3"+",MMSCONT4"+",MMVALMAG"+",MMIMPNAZ"+",MMKEYSAL"+",MMFLELGM"+",MMFLCASC"+",MMFLORDI"+",MMFLIMPE"+",MMFLRISE"+",MMF2CASC"+",MMF2ORDI"+",MMF2IMPE"+",MMF2RISE"+",MMFLOMAG"+",MMCODCOM"+",MMCODATT"+",MMTIPATT"+",MMCODCOS"+",MMFLORCO"+",MMFLCOCO"+",MMIMPCOM"+",MMFLULCA"+",MMFLULPV"+",MMVALULT"+",MMFLLOTT"+",MMF2LOTT"+",MMCODLOT"+",MMCODUBI"+",MMCODUB2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'MVM_DETT','MMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MVM_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMRIF),'MVM_DETT','MMNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MVM_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUMAG),'MVM_DETT','MMCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'MVM_DETT','MMCAUCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'MVM_DETT','MMCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'MVM_DETT','MMCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODLIS),'MVM_DETT','MMCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODICE),'MVM_DETT','MMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'MVM_DETT','MMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMUNIMIS),'MVM_DETT','MMUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAMOV),'MVM_DETT','MMQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAUM1),'MVM_DETT','MMQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMPREZZO),'MVM_DETT','MMPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT1');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT2');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT3');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALMAG),'MVM_DETT','MMVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMIMPNAZ),'MVM_DETT','MMIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MVM_DETT','MMKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLELGM),'MVM_DETT','MMFLELGM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLCASC),'MVM_DETT','MMFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLORDI),'MVM_DETT','MMFLORDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLIMPE),'MVM_DETT','MMFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLRISE),'MVM_DETT','MMFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2CASC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2ORDI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2IMPE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2RISE');
      +","+cp_NullLink(cp_ToStrODBC("X"),'MVM_DETT','MMFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMCODATT');
      +","+cp_NullLink(cp_ToStrODBC("A"),'MVM_DETT','MMTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMCODCOS');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLORCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLCOCO');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMIMPCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLULCA),'MVM_DETT','MMFLULCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLULPV),'MVM_DETT','MMFLULPV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALULT),'MVM_DETT','MMVALULT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLLOTT),'MVM_DETT','MMFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2LOTT');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'MVM_DETT','MMCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'MVM_DETT','MMCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'MVM_DETT','MMCODUB2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_SERIALE,'CPROWNUM',this.w_CPROWNUM,'MMNUMRIF',this.w_MMNUMRIF,'CPROWORD',this.w_CPROWORD,'MMCAUMAG',this.w_MMCAUMAG,'MMCAUCOL',SPACE(5),'MMCODMAG',this.w_MMCODMAG,'MMCODMAT',SPACE(5),'MMCODLIS',this.w_MMCODLIS,'MMCODICE',this.w_MMCODICE,'MMCODART',this.w_MMCODART,'MMUNIMIS',this.w_MMUNIMIS)
      insert into (i_cTable) (MMSERIAL,CPROWNUM,MMNUMRIF,CPROWORD,MMCAUMAG,MMCAUCOL,MMCODMAG,MMCODMAT,MMCODLIS,MMCODICE,MMCODART,MMUNIMIS,MMQTAMOV,MMQTAUM1,MMPREZZO,MMSCONT1,MMSCONT2,MMSCONT3,MMSCONT4,MMVALMAG,MMIMPNAZ,MMKEYSAL,MMFLELGM,MMFLCASC,MMFLORDI,MMFLIMPE,MMFLRISE,MMF2CASC,MMF2ORDI,MMF2IMPE,MMF2RISE,MMFLOMAG,MMCODCOM,MMCODATT,MMTIPATT,MMCODCOS,MMFLORCO,MMFLCOCO,MMIMPCOM,MMFLULCA,MMFLULPV,MMVALULT,MMFLLOTT,MMF2LOTT,MMCODLOT,MMCODUBI,MMCODUB2 &i_ccchkf. );
         values (;
           this.w_SERIALE;
           ,this.w_CPROWNUM;
           ,this.w_MMNUMRIF;
           ,this.w_CPROWORD;
           ,this.w_MMCAUMAG;
           ,SPACE(5);
           ,this.w_MMCODMAG;
           ,SPACE(5);
           ,this.w_MMCODLIS;
           ,this.w_MMCODICE;
           ,this.w_MMCODART;
           ,this.w_MMUNIMIS;
           ,this.w_MMQTAMOV;
           ,this.w_MMQTAUM1;
           ,this.w_MMPREZZO;
           ,0;
           ,0;
           ,0;
           ,0;
           ,this.w_MMVALMAG;
           ,this.w_MMIMPNAZ;
           ,this.w_MMKEYSAL;
           ,this.w_MMFLELGM;
           ,this.w_MMFLCASC;
           ,this.w_MMFLORDI;
           ,this.w_MMFLIMPE;
           ,this.w_MMFLRISE;
           ," ";
           ," ";
           ," ";
           ," ";
           ,"X";
           ," ";
           ," ";
           ,"A";
           ," ";
           ," ";
           ," ";
           ,0;
           ,this.w_MMFLULCA;
           ,this.w_MMFLULPV;
           ,this.w_MMVALULT;
           ,this.w_MMFLLOTT;
           ," ";
           ,SPACE(20);
           ,SPACE(20);
           ,SPACE(20);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0416D0B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MMKEYSAL,'SLCODMAG',this.w_MMCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MMKEYSAL;
           ,this.w_MMCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0416F450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMDEFA),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MMKEYSAL,'SCCODMAG',this.w_MMCODMAG,'SCCODCAN',this.w_COMMDEFA,'SCCODART',this.w_MMCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MMKEYSAL;
           ,this.w_MMCODMAG;
           ,this.w_COMMDEFA;
           ,this.w_MMCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea testata movimento di magazzino ( Con causale w_CAUTES )
    *     Inizializza inoltre i campi di riga per l'aggiornamento del magazzino
    this.w_MMSERIAL = SPACE(10)
    this.w_MMNUMREG = 0
    this.w_MMTCAMAG = this.w_CAUTES
    this.w_MMCAUMAG = this.w_CAUTES
    this.w_MMFLCASC = " "
    this.w_MMFLORDI = " "
    this.w_MMFLIMPE = " "
    this.w_MMFLRISE = " "
    this.w_MMFLELGM = " "
    this.w_FLAVA1 = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_CAUTES);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM;
        from (i_cTable) where;
            CMCODICE = this.w_CAUTES;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
      this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Inserisce il Master
    this.w_MMCODMAG = this.oParentObject.w_MDCODMAG
    i_Conn=i_TableProp[this.MVM_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEMVM", "i_codazi,w_MMSERIAL")
    cp_NextTableProg(this, i_Conn, "PRMVM", "i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG")
    * --- Try
    local bErr_04198620
    bErr_04198620=bTrsErr
    this.Try_04198620()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_Errormsg("Impossibile inserire carico KIT (tabella Master)%0%1",,"",message () )
    endif
    bTrsErr=bTrsErr or bErr_04198620
    * --- End
  endproc
  proc Try_04198620()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MVM_MAST
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+",MMCODESE"+",MMVALNAZ"+",MMCODUTE"+",MMNUMREG"+",MMDATREG"+",MMTCAMAG"+",MMTCOLIS"+",MMNUMDOC"+",MMALFDOC"+",MMDATDOC"+",MMFLCLFR"+",MMTIPCON"+",MMCODCON"+",MMDESSUP"+",MMCODVAL"+",MMCAOVAL"+",MMSCOCL1"+",MMSCOCL2"+",MMSCOPAG"+",MMFLGIOM"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",MMSERPOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODESE),'MVM_MAST','MMCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODUTE),'MVM_MAST','MMCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMREG),'MVM_MAST','MMNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'MVM_MAST','MMDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCOLIS),'MVM_MAST','MMTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMDOC),'MVM_MAST','MMNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMALFDOC),'MVM_MAST','MMALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATDOC),'MVM_MAST','MMDATDOC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'MVM_MAST','MMFLCLFR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'MVM_MAST','MMCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDESSUP),'MVM_MAST','MMDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAL),'MVM_MAST','MMCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOCL1');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOCL2');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOPAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MVM_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MVM_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MVM_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDSERIAL),'MVM_MAST','MMSERPOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'MMCODESE',this.w_MMCODESE,'MMVALNAZ',this.w_MMVALNAZ,'MMCODUTE',this.w_MMCODUTE,'MMNUMREG',this.w_MMNUMREG,'MMDATREG',this.w_MMDATREG,'MMTCAMAG',this.w_MMTCAMAG,'MMTCOLIS',this.w_MMTCOLIS,'MMNUMDOC',this.w_MMNUMDOC,'MMALFDOC',this.w_MMALFDOC,'MMDATDOC',this.w_MMDATDOC,'MMFLCLFR',"N")
      insert into (i_cTable) (MMSERIAL,MMCODESE,MMVALNAZ,MMCODUTE,MMNUMREG,MMDATREG,MMTCAMAG,MMTCOLIS,MMNUMDOC,MMALFDOC,MMDATDOC,MMFLCLFR,MMTIPCON,MMCODCON,MMDESSUP,MMCODVAL,MMCAOVAL,MMSCOCL1,MMSCOCL2,MMSCOPAG,MMFLGIOM,UTCC,UTDC,UTCV,UTDV,MMSERPOS &i_ccchkf. );
         values (;
           this.w_MMSERIAL;
           ,this.w_MMCODESE;
           ,this.w_MMVALNAZ;
           ,this.w_MMCODUTE;
           ,this.w_MMNUMREG;
           ,this.w_MMDATREG;
           ,this.w_MMTCAMAG;
           ,this.w_MMTCOLIS;
           ,this.w_MMNUMDOC;
           ,this.w_MMALFDOC;
           ,this.w_MMDATDOC;
           ,"N";
           ," ";
           ,SPACE(15);
           ,this.w_MMDESSUP;
           ,this.w_MMCODVAL;
           ,this.w_MMCAOVAL;
           ,0;
           ,0;
           ,0;
           ," ";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.oParentObject.w_MDSERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='MVM_MAST'
    this.cWorkTables[3]='MVM_DETT'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='COR_RISP'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='SALDICOM'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
