* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_mvd                                                        *
*              Vendita negozio                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_1080]                                                *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-02                                                      *
* Last revis.: 2016-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_mvd"))

* --- Class definition
define class tgsps_mvd as StdTrsForm
  Top    = 4
  Left   = 10

  * --- Standard Properties
  Width  = 795
  Height = 485+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-02-04"
  HelpContextID=46749801
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=301

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  COR_RISP_IDX = 0
  CORDRISP_IDX = 0
  ART_ICOL_IDX = 0
  CAM_AGAZ_IDX = 0
  CAR_CRED_IDX = 0
  CLI_VEND_IDX = 0
  CPUSERS_IDX = 0
  DIS_HARD_IDX = 0
  DOC_DETT_IDX = 0
  FID_CARD_IDX = 0
  KEY_ARTI_IDX = 0
  LISTINI_IDX = 0
  MAGAZZIN_IDX = 0
  MOD_VEND_IDX = 0
  OPE_RATO_IDX = 0
  OUT_PUTS_IDX = 0
  PAG_AMEN_IDX = 0
  PAR_VDET_IDX = 0
  REP_ARTI_IDX = 0
  SALDIART_IDX = 0
  TIP_DOCU_IDX = 0
  UNIMIS_IDX = 0
  VALUTE_IDX = 0
  VOCIIVA_IDX = 0
  CAN_TIER_IDX = 0
  CONTI_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  SALDILOT_IDX = 0
  CAU_CONT_IDX = 0
  cFile = "COR_RISP"
  cFileDetail = "CORDRISP"
  cKeySelect = "MDSERIAL"
  cKeyWhere  = "MDSERIAL=this.w_MDSERIAL"
  cKeyDetail  = "MDSERIAL=this.w_MDSERIAL"
  cKeyWhereODBC = '"MDSERIAL="+cp_ToStrODBC(this.w_MDSERIAL)';

  cKeyDetailWhereODBC = '"MDSERIAL="+cp_ToStrODBC(this.w_MDSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CORDRISP.MDSERIAL="+cp_ToStrODBC(this.w_MDSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CORDRISP.CPROWORD,CORDRISP.CPROWNUM'
  cPrg = "gsps_mvd"
  cComment = "Vendita negozio"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODCAS = space(5)
  w_MATSCO = space(20)
  o_MATSCO = space(20)
  w_CODAZI = space(5)
  w_DECUNI = 0
  w_DECTOT = 0
  w_CALCPICT = 0
  w_CALCPICU = 0
  w_MDCODVAL = space(3)
  w_MDCAOVAL = 0
  w_MDSERIAL = space(10)
  o_MDSERIAL = space(10)
  w_MDDATREG = ctod('  /  /  ')
  o_MDDATREG = ctod('  /  /  ')
  w_MDORAVEN = space(2)
  w_MDMINVEN = space(2)
  w_MDCODUTE = 0
  w_MDCODESE = space(4)
  w_MDCODNEG = space(3)
  w_MDTIPVEN = space(5)
  o_MDTIPVEN = space(5)
  w_DESVEN = space(35)
  w_LISVEN = space(5)
  w_CLIVEN = space(15)
  o_CLIVEN = space(15)
  w_FLSCON = space(1)
  w_QTADEF = 0
  w_FLPREZ = space(1)
  w_FLGDES = space(1)
  w_FLUNIM = space(1)
  w_READPAR = space(10)
  w_ALFDOC = space(10)
  w_MDCODMAG = space(5)
  o_MDCODMAG = space(5)
  w_MDCODFID = space(20)
  o_MDCODFID = space(20)
  w_CLIFID = space(15)
  w_CODNEG = space(10)
  w_MDCODCLI = space(15)
  o_MDCODCLI = space(15)
  w_DESCLI = space(40)
  w_MDCODLIS = space(5)
  o_MDCODLIS = space(5)
  w_MDNUMSCO = 0
  w_MDMATSCO = space(20)
  w_LISIVA = space(1)
  w_LISVAL = space(3)
  w_MDFLTRAS = space(1)
  w_NUMSCO = 0
  w_PAGVEN = space(5)
  w_PAGCLI = space(5)
  w_MDANNDOC = space(4)
  w_DOCCOR = space(5)
  w_DOCRIC = space(5)
  w_DOCFAF = space(5)
  w_DOCRIF = space(5)
  w_DOCDDT = space(5)
  w_DOCFAT = space(5)
  w_CATSCC = space(5)
  w_VALFID = 0
  w_FIDUTI = 0
  w_SCOLIS = space(1)
  w_CATCOM = space(3)
  w_FIMPPRE = 0
  w_FIMPRES = 0
  w_FTOTIMP = 0
  w_FPUNFID = 0
  w_FNUMVIS = 0
  w_FDATATT = ctod('  /  /  ')
  w_FDATSCA = ctod('  /  /  ')
  w_FDATUAC = ctod('  /  /  ')
  w_LISCLI = space(5)
  w_PDOCCOR = space(5)
  w_PDOCRIC = space(5)
  w_PDOCFAF = space(5)
  w_PDOCRIF = space(5)
  w_PDOCDDT = space(5)
  w_PDOCFAT = space(5)
  w_DOCCO1 = space(5)
  w_DOCCO2 = space(5)
  w_DOCCO3 = space(5)
  w_SCOCL1 = 0
  w_SCOCL2 = 0
  w_GIAIMP = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_PROMOK = .F.
  w_CODCLI = space(15)
  w_NULLA = space(10)
  w_ARTDES = space(10)
  w_DESPOS = space(1)
  w_MDCODOPE = 0
  w_ERRORE = .F.
  w_CAURES = space(5)
  w_CARKIT = space(5)
  w_SCAKIT = space(5)
  w_FLUBIC = space(1)
  w_DESFID = space(40)
  w_FLFIDO = space(1)
  w_CODVAA = space(3)
  w_PRZVAC = space(1)
  w_PRZDES = space(1)
  w_FLGCOM = space(1)
  w_FLANAL = space(1)
  w_DTOCOM = ctod('  /  /  ')
  w_CODESC = space(5)
  w_CLOBSO = ctod('  /  /  ')
  w_SIMVAL = space(5)
  w_SERFID = space(20)
  w_FLFAFI = space(1)
  w_LOTDIF = space(1)
  w_NUMRIF = 0
  w_FLPRG = space(1)
  w_SHOWMMT = space(1)
  w_TESLOT = .F.
  w_HASEVENT = .F.
  w_HASEVCOP = space(50)
  w_READHARD = space(5)
  w_PASQTA = space(1)
  w_FLFOM = space(1)
  w_DEFCODREP = space(3)
  w_DA1 = space(1)
  w_UM1 = space(1)
  w_QT1 = space(1)
  w_PZ1 = space(1)
  w_SC1 = space(1)
  w_MDCODICE = space(20)
  o_MDCODICE = space(20)
  w_MDDESART = space(40)
  w_MDCODART = space(20)
  o_MDCODART = space(20)
  w_FLSERG = space(1)
  w_CPROWORD = 0
  w_MDTIPRIG = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MDQTAUM1 = 0
  w_MDUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_MDQTAMOV = 0
  o_MDQTAMOV = 0
  w_LIPREZZO = 0
  o_LIPREZZO = 0
  w_MDCONTRA = space(15)
  w_MDPREZZO = 0
  o_MDPREZZO = 0
  w_MDSCONT1 = 0
  o_MDSCONT1 = 0
  w_MDFLRESO = space(1)
  o_MDFLRESO = space(1)
  w_MDCODREP = space(3)
  o_MDCODREP = space(3)
  w_MDCODIVA = space(5)
  w_MDSCONT2 = 0
  o_MDSCONT2 = 0
  w_MDSCONT3 = 0
  o_MDSCONT3 = 0
  w_MDSCONT4 = 0
  o_MDSCONT4 = 0
  w_MDFLOMAG = space(1)
  o_MDFLOMAG = space(1)
  w_MDFLCASC = space(1)
  w_MDMAGSAL = space(5)
  o_MDMAGSAL = space(5)
  w_ULTSCA = ctod('  /  /  ')
  w_MDSCOVEN = 0
  w_ARCODART = space(20)
  w_CATSCA = space(5)
  w_GRUMER = space(5)
  w_OFLSCO = space(1)
  w_IMPNAZ = 0
  w_VALRIG = 0
  o_VALRIG = 0
  w_RIGNET = 0
  w_PERIVA = 0
  o_PERIVA = 0
  w_MDSERRIF = space(10)
  w_MDNUMRIF = 0
  w_MDROWRIF = 0
  w_MDFLARIF = space(1)
  w_MDFLERIF = space(1)
  w_MDQTAIMP = 0
  w_MDQTAIM1 = 0
  w_ORDEVA = 0
  w_OLDEVAS = space(1)
  w_MDCODPRO = space(15)
  w_MDFLESCL = space(1)
  w_MDFLROMA = space(1)
  w_MDSCOPRO = 0
  w_ARTPOS = space(1)
  w_VALUCA = 0
  w_QTAPER = 0
  w_QTRPER = 0
  w_FLPAN = space(1)
  w_OQTAIMP = 0
  w_OQTAIM1 = 0
  w_OFLERIF = space(1)
  w_OPREZZO = 0
  w_DESREP = space(40)
  w_QTDISP = 0
  w_MDFLULPV = space(1)
  w_MDVALULT = 0
  w_FLSERA = space(1)
  w_MDTIPPRO = space(2)
  w_MDPERPRO = 0
  w_NOCALQTA = .F.
  w_LISCON = 0
  w_GESMAT = space(1)
  w_OCOM = space(15)
  w_COCODVAL = space(3)
  w_DECCOM = 0
  w_MDIMPCOM = 0
  w_GIACON = space(1)
  w_FLSTAT = space(1)
  w_DATLOT = ctod('  /  /  ')
  w_FLLOTT = space(1)
  w_MDFLLOTT = space(1)
  w_LOTZOOM = .F.
  w_MDCODLOT = space(20)
  o_MDCODLOT = space(20)
  w_MDCODUBI = space(20)
  o_MDCODUBI = space(20)
  w_GESMAT = space(1)
  w_MDCODCOM = space(15)
  o_MDCODCOM = space(15)
  w_UBIZOOM = .F.
  w_MTCARI = space(1)
  w_MDMAGRIG = space(5)
  w_OLDQTA = 0
  w_ARTDIS = space(1)
  w_FLDISP = space(1)
  w_TOTMAT = 0
  w_FLRRIF = space(1)
  w_ORQTAEV1 = 0
  w_DISLOT = space(1)
  w_MDTIPPR2 = space(2)
  w_MDPROCAP = 0
  w_MDLOTMAG = space(5)
  w_NOFRAZ = space(1)
  w_MODUM2 = space(1)
  w_UNISEP = space(1)
  w_PREZUM = space(1)
  w_CLUNIMIS = space(1)
  w_CODREP = space(3)
  w_CODIVA = space(5)
  w_TOTALE = 0
  w_MDTOTVEN = 0
  o_MDTOTVEN = 0
  w_MDPUNFID = 0
  w_MDSTOPRO = 0
  o_MDSTOPRO = 0
  w_MDSCOCL1 = 0
  o_MDSCOCL1 = 0
  w_MDSCOCL2 = 0
  o_MDSCOCL2 = 0
  w_MDSCOPAG = 0
  o_MDSCOPAG = 0
  w_MDSCONTI = 0
  o_MDSCONTI = 0
  w_MDFLFOSC = space(1)
  o_MDFLFOSC = space(1)
  w_MDCODPAG = space(5)
  o_MDCODPAG = space(5)
  w_SCOPAG = 0
  w_MDIMPPRE = 0
  w_MDTOTDOC = 0
  o_MDTOTDOC = 0
  w_MDACCPRE = 0
  w_MDPAGCON = 0
  w_MDPAGASS = 0
  o_MDPAGASS = 0
  w_MDRIFASS = space(40)
  w_MDPAGCAR = 0
  o_MDPAGCAR = 0
  w_MDCODCAR = space(5)
  w_MDPAGFIN = 0
  o_MDPAGFIN = 0
  w_CCDESCRI = space(40)
  w_MDCODFIN = space(5)
  w_DESFIN = space(40)
  w_MDPAGCLI = 0
  w_MDIMPABB = 0
  w_MDTIPCHI = space(2)
  o_MDTIPCHI = space(2)
  w_MDCODCLI = space(15)
  w_DESCLI = space(40)
  w_MDTIPDOC = space(5)
  o_MDTIPDOC = space(5)
  w_MDPRD = space(2)
  w_MDALFDOC = space(10)
  w_MDNUMDOC = 0
  w_MDFLFATT = space(1)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_TELFAX = space(18)
  w_EMAIL = space(50)
  w_OTES = space(1)
  w_MDFLSALD = space(1)
  w_FLIMPA = space(1)
  w_FLNSRI = space(1)
  w_FLVSRI = space(1)
  w_MDRIFDOC = space(10)
  w_MDRIFCOR = space(10)
  w_MDCARKIT = space(10)
  w_MDSCAKIT = space(10)
  w_MDTIPCOR = space(5)
  o_MDTIPCOR = space(5)
  w_OTOTDOC = 0
  w_OIMPPRE = 0
  w_OPUNFID = 0
  w_MD_ERROR = space(1)
  w_SERIAL = space(10)
  w_FLRISC = space(1)
  w_OPAGCLI = 0
  w_OFLRISC = space(1)
  w_OTIPCHI = space(1)
  w_RESCHK = 0
  w_EDCL3KEU = .F.
  w_EDTIPDOC = .F.
  w_oMDTIPDOC = space(5)
  w_TDFLINTE = space(1)
  w_zMDTIPDOC = space(5)
  w_TDCAUCON = space(5)
  w_CCFLRIFE = space(1)
  w_DELNOMSG = .F.
  w_FLRICIVA = .F.
  o_FLRICIVA = .F.
  w_RIIVDTIN = ctod('  /  /  ')
  o_RIIVDTIN = ctod('  /  /  ')
  w_RIIVDTFI = ctod('  /  /  ')
  w_RIIVLSIV = space(0)
  w_ERRORCOD = space(200)
  w_FLCOM1 = space(1)
  w_ARCLAMAT = space(5)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0
  w_MDPROMOZ = 0
  w_MDESPSCO = 0
  w_NUMDEQ = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MDSERIAL = this.W_MDSERIAL

  * --- Children pointers
  GSVE_MMT = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsps_mvd
  * --- Per riposizionarmi alla fine del caricamento sul codice articolo
  * --- per essere pronto ad un nuovo scontrino. La testata viene ricaricata
  * --- in automatico in base alla vendita precedente
  Proc ECPSAVE
  Local tcFunc
    tcFunc=this.cFunction
    dodefault()
    if this.w_bRes And this.oPgFrm.ActivePage=1 And inlist(tcFunc,'Load','Edit')
       this.oPgFrm.Page1.oPag.oBody.SetFullFocus()
       Local CodCtrl
       CodCtrl=this.GetBodyCtrl("w_MDCODICE")
       CodCtrl.SetFocus()
    endif
  endproc
  
  Proc ecpF6
    If Inlist(This.cFunction,'Edit','Load') And Type('this.ActiveControl.parent.oContained')='O'
      this.w_DELNOMSG=.T.
    endif
    DoDefault()
  endproc
  
  
    * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
    * --- Lanciato solo se in stato di Query per prevenire il controllo quando si
    * --- ri preme il tasto in modifica / cariacamento
    Func ah_HasCPEvents(i_cOp)
  	     This.w_HASEVCOP=i_cop
         This.w_HASEvent=.t.
         If (Upper(This.cFunction)="QUERY" And (Upper(i_cop)='ECPEDIT' or Upper(i_cop)='ECPLOAD'))
            this.w_bRes=.F.
            if This.oPgFrm.ActivePage=2
               this.oPgFrm.ActivePage=1
            endif
         Endif
         * Controllo matricole
         If (Upper(This.cFunction)="QUERY" And (Upper(i_cop)='ECPEDIT' Or Upper(i_cop)='ECPDELETE'))
            this.NotifyEvent('HasEvent')
         Endif
  	Return(this.w_HASEVENT)
    EndFunc
  
  * --- Variabile utilizzata per caricamento fidelity tramite penna Ottica.
  * --- Utilizzata in GSAR_BGP per far si che il focus rimanga su MDCODICE
  w_Lost_Cod=.F.
  w_Old_bDRE=.F.
  * --- Variabile per evitare di eseguire la Setfocus su MDCODICE al salvataggio
  * --- se fallisce la checkform o checkrow
  w_bRes=.F.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'COR_RISP','gsps_mvd')
    stdPageFrame::Init()
    *set procedure to GSVE_MMT additive
    with this
      .Pages(1).addobject("oPag","tgsps_mvdPag1","gsps_mvd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(1).HelpContextID = 215751447
      .Pages(2).addobject("oPag","tgsps_mvdPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati chiusura")
      .Pages(2).HelpContextID = 114223121
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMDSERIAL_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVE_MMT
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[30]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CAR_CRED'
    this.cWorkTables[4]='CLI_VEND'
    this.cWorkTables[5]='CPUSERS'
    this.cWorkTables[6]='DIS_HARD'
    this.cWorkTables[7]='DOC_DETT'
    this.cWorkTables[8]='FID_CARD'
    this.cWorkTables[9]='KEY_ARTI'
    this.cWorkTables[10]='LISTINI'
    this.cWorkTables[11]='MAGAZZIN'
    this.cWorkTables[12]='MOD_VEND'
    this.cWorkTables[13]='OPE_RATO'
    this.cWorkTables[14]='OUT_PUTS'
    this.cWorkTables[15]='PAG_AMEN'
    this.cWorkTables[16]='PAR_VDET'
    this.cWorkTables[17]='REP_ARTI'
    this.cWorkTables[18]='SALDIART'
    this.cWorkTables[19]='TIP_DOCU'
    this.cWorkTables[20]='UNIMIS'
    this.cWorkTables[21]='VALUTE'
    this.cWorkTables[22]='VOCIIVA'
    this.cWorkTables[23]='CAN_TIER'
    this.cWorkTables[24]='CONTI'
    this.cWorkTables[25]='LOTTIART'
    this.cWorkTables[26]='UBICAZIO'
    this.cWorkTables[27]='SALDILOT'
    this.cWorkTables[28]='CAU_CONT'
    this.cWorkTables[29]='COR_RISP'
    this.cWorkTables[30]='CORDRISP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(30))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COR_RISP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COR_RISP_IDX,3]
  return

  function CreateChildren()
    this.GSVE_MMT = CREATEOBJECT('stdLazyChild',this,'GSVE_MMT')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVE_MMT)
      this.GSVE_MMT.DestroyChildrenChain()
      this.GSVE_MMT=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVE_MMT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVE_MMT.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVE_MMT.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSVE_MMT.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_MDSERIAL,"MTSERIAL";
             ,.w_CPROWNUM,"MTROWNUM";
             ,.w_MDNUMRIF,"MTNUMRIF";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MDSERIAL = NVL(MDSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_34_joined
    link_1_34_joined=.f.
    local link_1_35_joined
    link_1_35_joined=.f.
    local link_1_41_joined
    link_1_41_joined=.f.
    local link_1_44_joined
    link_1_44_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_2_20_joined
    link_2_20_joined=.f.
    local link_2_28_joined
    link_2_28_joined=.f.
    local link_2_29_joined
    link_2_29_joined=.f.
    local link_2_104_joined
    link_2_104_joined=.f.
    local link_4_9_joined
    link_4_9_joined=.f.
    local link_4_19_joined
    link_4_19_joined=.f.
    local link_4_22_joined
    link_4_22_joined=.f.
    local link_4_35_joined
    link_4_35_joined=.f.
    local link_4_37_joined
    link_4_37_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from COR_RISP where MDSERIAL=KeySet.MDSERIAL
    *
    i_nConn = i_TableProp[this.COR_RISP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COR_RISP_IDX,2],this.bLoadRecFilter,this.COR_RISP_IDX,"gsps_mvd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COR_RISP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COR_RISP.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CORDRISP.","COR_RISP.")
      i_cTable = i_cTable+' COR_RISP '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_34_joined=this.AddJoinedLink_1_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_35_joined=this.AddJoinedLink_1_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_41_joined=this.AddJoinedLink_1_41(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_44_joined=this.AddJoinedLink_1_44(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_9_joined=this.AddJoinedLink_4_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_19_joined=this.AddJoinedLink_4_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_22_joined=this.AddJoinedLink_4_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_35_joined=this.AddJoinedLink_4_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_37_joined=this.AddJoinedLink_4_37(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MDSERIAL',this.w_MDSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODCAS = IIF( Type('g_CODCAS')='C' , g_CODCAS, space(5))
        .w_MATSCO = space(20)
        .w_DESVEN = space(35)
        .w_LISVEN = space(5)
        .w_CLIVEN = space(15)
        .w_FLSCON = space(1)
        .w_QTADEF = 0
        .w_FLPREZ = space(1)
        .w_FLGDES = space(1)
        .w_FLUNIM = space(1)
        .w_ALFDOC = space(10)
        .w_CLIFID = space(15)
        .w_CODNEG = iif(type('g_CODNEG')<>'U',g_CODNEG,' ')
        .w_DESCLI = space(40)
        .w_LISIVA = space(1)
        .w_LISVAL = space(3)
        .w_NUMSCO = 0
        .w_PAGVEN = space(5)
        .w_PAGCLI = space(5)
        .w_DOCCOR = space(5)
        .w_DOCRIC = space(5)
        .w_DOCFAF = space(5)
        .w_DOCRIF = space(5)
        .w_DOCDDT = space(5)
        .w_DOCFAT = space(5)
        .w_CATSCC = space(5)
        .w_VALFID = 0
        .w_FIDUTI = 0
        .w_SCOLIS = space(1)
        .w_CATCOM = space(3)
        .w_FIMPPRE = 0
        .w_FIMPRES = 0
        .w_FTOTIMP = 0
        .w_FPUNFID = 0
        .w_FNUMVIS = 0
        .w_FDATATT = ctod("  /  /  ")
        .w_FDATSCA = ctod("  /  /  ")
        .w_FDATUAC = ctod("  /  /  ")
        .w_LISCLI = space(5)
        .w_PDOCCOR = space(5)
        .w_PDOCRIC = space(5)
        .w_PDOCFAF = space(5)
        .w_PDOCRIF = space(5)
        .w_PDOCDDT = space(5)
        .w_PDOCFAT = space(5)
        .w_DOCCO1 = space(5)
        .w_DOCCO2 = space(5)
        .w_DOCCO3 = space(5)
        .w_SCOCL1 = 0
        .w_SCOCL2 = 0
        .w_GIAIMP = ' '
        .w_PROMOK = .F.
        .w_CODCLI = space(15)
        .w_ARTDES = g_ARTDES
        .w_ERRORE = .F.
        .w_CAURES = space(5)
        .w_CARKIT = space(5)
        .w_SCAKIT = space(5)
        .w_FLUBIC = space(1)
        .w_DESFID = space(40)
        .w_FLFIDO = space(1)
        .w_PRZVAC = space(1)
        .w_PRZDES = space(1)
        .w_FLGCOM = space(1)
        .w_FLANAL = space(1)
        .w_DTOCOM = ctod("  /  /  ")
        .w_CODESC = space(5)
        .w_CLOBSO = ctod("  /  /  ")
        .w_SIMVAL = space(5)
        .w_SERFID = space(20)
        .w_FLFAFI = space(1)
        .w_NUMRIF = -30
        .w_SHOWMMT = 'S'
        .w_HASEVENT = .f.
        .w_HASEVCOP = space(50)
        .w_PASQTA = space(1)
        .w_FLFOM = space(1)
        .w_DEFCODREP = space(3)
        .w_DECCOM = 0
        .w_TOTALE = 0
        .w_SCOPAG = 0
        .w_CCDESCRI = space(40)
        .w_DESFIN = space(40)
        .w_DESCLI = space(40)
        .w_OQRY = space(50)
        .w_OREP = space(50)
        .w_TELFAX = space(18)
        .w_EMAIL = space(50)
        .w_OTES = space(1)
        .w_FLIMPA = space(1)
        .w_FLNSRI = ' '
        .w_FLVSRI = ' '
        .w_OTOTDOC = 0
        .w_OIMPPRE = 0
        .w_OPUNFID = 0
        .w_SERIAL = .w_MDSERIAL
        .w_FLRISC = space(1)
        .w_RESCHK = 0
        .w_EDCL3KEU = .f.
        .w_EDTIPDOC = .F.
        .w_TDFLINTE = space(1)
        .w_zMDTIPDOC = space(5)
        .w_TDCAUCON = space(5)
        .w_CCFLRIFE = space(1)
        .w_FLRICIVA = .f.
        .w_RIIVDTIN = ctod("  /  /  ")
        .w_RIIVDTFI = ctod("  /  /  ")
        .w_RIIVLSIV = space(0)
          .link_1_1('Load')
        .w_CODAZI = i_CODAZI
        .w_DECUNI = g_PERPUL
        .w_DECTOT = g_PERPVL
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_MDCODVAL = NVL(MDCODVAL,space(3))
          if link_1_8_joined
            this.w_MDCODVAL = NVL(VACODVAL108,NVL(this.w_MDCODVAL,space(3)))
            this.w_SIMVAL = NVL(VASIMVAL108,space(5))
          else
          .link_1_8('Load')
          endif
        .w_MDCAOVAL = NVL(MDCAOVAL,0)
        .w_MDSERIAL = NVL(MDSERIAL,space(10))
        .op_MDSERIAL = .w_MDSERIAL
        .w_MDDATREG = NVL(cp_ToDate(MDDATREG),ctod("  /  /  "))
        .w_MDORAVEN = NVL(MDORAVEN,space(2))
        .w_MDMINVEN = NVL(MDMINVEN,space(2))
        .w_MDCODUTE = NVL(MDCODUTE,0)
          * evitabile
          *.link_1_17('Load')
        .w_MDCODESE = NVL(MDCODESE,space(4))
        .w_MDCODNEG = NVL(MDCODNEG,space(3))
        .w_MDTIPVEN = NVL(MDTIPVEN,space(5))
          if link_1_21_joined
            this.w_MDTIPVEN = NVL(MOCODICE121,NVL(this.w_MDTIPVEN,space(5)))
            this.w_DESVEN = NVL(MODESCRI121,space(35))
            this.w_MDCODMAG = NVL(MOCODMAG121,space(5))
            this.w_LISVEN = NVL(MOCODLIS121,space(5))
            this.w_MDTIPCHI = NVL(MOTIPCHI121,space(2))
            this.w_CLIVEN = NVL(MOCODCLI121,space(15))
            this.w_FLUNIM = NVL(MOFLUNIM121,space(1))
            this.w_FLPREZ = NVL(MOFLPREZ121,space(1))
            this.w_QTADEF = NVL(MOQTADEF121,0)
            this.w_FLSCON = NVL(MOFLSCON121,space(1))
            this.w_FLGDES = NVL(MOFLGDES121,space(1))
            this.w_NUMSCO = NVL(MONUMSCO121,0)
            this.w_PAGVEN = NVL(MOCODPAG121,space(5))
            this.w_DOCCOR = NVL(MODOCCOR121,space(5))
            this.w_DOCRIC = NVL(MODOCRIC121,space(5))
            this.w_DOCFAT = NVL(MODOCFAT121,space(5))
            this.w_DOCFAF = NVL(MODOCFAF121,space(5))
            this.w_DOCRIF = NVL(MODOCRIF121,space(5))
            this.w_DOCDDT = NVL(MODOCDDT121,space(5))
            this.w_DOCCO1 = NVL(MODOCCO1121,space(5))
            this.w_DOCCO2 = NVL(MODOCCO2121,space(5))
            this.w_DOCCO3 = NVL(MODOCCO3121,space(5))
            this.w_PRZVAC = NVL(MOPRZVAC121,space(1))
            this.w_PRZDES = NVL(MOPRZDES121,space(1))
          else
          .link_1_21('Load')
          endif
        .w_READPAR = .w_MDCODNEG
          .link_1_31('Load')
        .w_MDCODMAG = NVL(MDCODMAG,space(5))
          if link_1_34_joined
            this.w_MDCODMAG = NVL(MGCODMAG134,NVL(this.w_MDCODMAG,space(5)))
            this.w_FLUBIC = NVL(MGFLUBIC134,space(1))
          else
          .link_1_34('Load')
          endif
        .w_MDCODFID = NVL(MDCODFID,space(20))
          if link_1_35_joined
            this.w_MDCODFID = NVL(FCCODFID135,NVL(this.w_MDCODFID,space(20)))
            this.w_DESFID = NVL(FCDESFID135,space(40))
            this.w_CLIFID = NVL(FCCODCLI135,space(15))
            this.w_FIMPPRE = NVL(FCIMPPRE135,0)
            this.w_FIMPRES = NVL(FCIMPRES135,0)
            this.w_FTOTIMP = NVL(FCTOTIMP135,0)
            this.w_FPUNFID = NVL(FCPUNFID135,0)
            this.w_FNUMVIS = NVL(FCNUMVIS135,0)
            this.w_FDATATT = NVL(cp_ToDate(FCDATATT135),ctod("  /  /  "))
            this.w_FDATSCA = NVL(cp_ToDate(FCDATSCA135),ctod("  /  /  "))
            this.w_FDATUAC = NVL(cp_ToDate(FCDATUAC135),ctod("  /  /  "))
          else
          .link_1_35('Load')
          endif
        .w_MDCODCLI = NVL(MDCODCLI,space(15))
          if link_1_41_joined
            this.w_MDCODCLI = NVL(CLCODCLI141,NVL(this.w_MDCODCLI,space(15)))
            this.w_DESCLI = NVL(CLDESCRI141,space(40))
            this.w_SCOCL1 = NVL(CLSCONT1141,0)
            this.w_SCOCL2 = NVL(CLSCONT2141,0)
            this.w_PAGCLI = NVL(CLCODPAG141,space(5))
            this.w_CATSCC = NVL(CLCATSCM141,space(5))
            this.w_VALFID = NVL(CLVALFID141,0)
            this.w_FIDUTI = NVL(CLFIDUTI141,0)
            this.w_CATCOM = NVL(CLCATCOM141,space(3))
            this.w_LISCLI = NVL(CLCODLIS141,space(5))
            this.w_TELFAX = NVL(CLTELFAX141,space(18))
            this.w_EMAIL = NVL(CL_EMAIL141,space(50))
            this.w_CODCLI = NVL(CLCODCON141,space(15))
            this.w_FLFIDO = NVL(CLFLFIDO141,space(1))
            this.w_CLOBSO = NVL(cp_ToDate(CLDTOBSO141),ctod("  /  /  "))
          else
          .link_1_41('Load')
          endif
        .w_MDCODLIS = NVL(MDCODLIS,space(5))
          if link_1_44_joined
            this.w_MDCODLIS = NVL(LSCODLIS144,NVL(this.w_MDCODLIS,space(5)))
            this.w_LISIVA = NVL(LSIVALIS144,space(1))
            this.w_LISVAL = NVL(LSVALLIS144,space(3))
            this.w_SCOLIS = NVL(LSFLSCON144,space(1))
          else
          .link_1_44('Load')
          endif
        .w_MDNUMSCO = NVL(MDNUMSCO,0)
        .w_MDMATSCO = NVL(MDMATSCO,space(20))
        .w_MDFLTRAS = NVL(MDFLTRAS,space(1))
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(IIF(.w_MDFLTRAS='S', '*** Vendita Trasferita', ''))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_MDANNDOC = NVL(MDANNDOC,space(4))
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .w_OBTEST = .w_MDDATREG
          .link_1_102('Load')
        .w_NULLA = ' '
          .link_1_104('Load')
        .w_MDCODOPE = NVL(MDCODOPE,0)
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .w_FLPRG = 'V'
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .w_TESLOT = .F.
        .oPgFrm.Page1.oPag.oObj_1_148.Calculate()
        .w_READHARD = IIF( TYPE('g_CODCAS')='C',g_CODCAS, SPACE(5))
          .link_1_149('Load')
        .w_OCOM = IIF(.w_MDTIPRIG<>'D', .w_MDCODCOM, .w_OCOM)
        .w_MDTOTVEN = NVL(MDTOTVEN,0)
        .w_MDPUNFID = NVL(MDPUNFID,0)
        .w_MDSTOPRO = NVL(MDSTOPRO,0)
        .w_MDSCOCL1 = NVL(MDSCOCL1,0)
        .w_MDSCOCL2 = NVL(MDSCOCL2,0)
        .w_MDSCOPAG = NVL(MDSCOPAG,0)
        .w_MDSCONTI = NVL(MDSCONTI,0)
        .w_MDFLFOSC = NVL(MDFLFOSC,space(1))
        .w_MDCODPAG = NVL(MDCODPAG,space(5))
          if link_4_9_joined
            this.w_MDCODPAG = NVL(PACODICE409,NVL(this.w_MDCODPAG,space(5)))
            this.w_SCOPAG = NVL(PASCONTO409,0)
          else
          .link_4_9('Load')
          endif
        .w_MDIMPPRE = NVL(MDIMPPRE,0)
        .w_MDTOTDOC = NVL(MDTOTDOC,0)
        .w_MDACCPRE = NVL(MDACCPRE,0)
        .w_MDPAGCON = NVL(MDPAGCON,0)
        .w_MDPAGASS = NVL(MDPAGASS,0)
        .w_MDRIFASS = NVL(MDRIFASS,space(40))
        .w_MDPAGCAR = NVL(MDPAGCAR,0)
        .w_MDCODCAR = NVL(MDCODCAR,space(5))
          if link_4_19_joined
            this.w_MDCODCAR = NVL(CCCODICE419,NVL(this.w_MDCODCAR,space(5)))
            this.w_CCDESCRI = NVL(CCDESCRI419,space(40))
          else
          .link_4_19('Load')
          endif
        .w_MDPAGFIN = NVL(MDPAGFIN,0)
        .w_MDCODFIN = NVL(MDCODFIN,space(5))
          if link_4_22_joined
            this.w_MDCODFIN = NVL(CCCODICE422,NVL(this.w_MDCODFIN,space(5)))
            this.w_DESFIN = NVL(CCDESCRI422,space(40))
          else
          .link_4_22('Load')
          endif
        .w_MDPAGCLI = NVL(MDPAGCLI,0)
        .w_MDIMPABB = NVL(MDIMPABB,0)
        .w_MDTIPCHI = NVL(MDTIPCHI,space(2))
        .w_MDCODCLI = NVL(MDCODCLI,space(15))
          if link_4_35_joined
            this.w_MDCODCLI = NVL(CLCODCLI435,NVL(this.w_MDCODCLI,space(15)))
            this.w_DESCLI = NVL(CLDESCRI435,space(40))
            this.w_SCOCL1 = NVL(CLSCONT1435,0)
            this.w_SCOCL2 = NVL(CLSCONT2435,0)
            this.w_PAGCLI = NVL(CLCODPAG435,space(5))
            this.w_CATSCC = NVL(CLCATSCM435,space(5))
            this.w_VALFID = NVL(CLVALFID435,0)
            this.w_FIDUTI = NVL(CLFIDUTI435,0)
            this.w_CATCOM = NVL(CLCATCOM435,space(3))
            this.w_LISCLI = NVL(CLCODLIS435,space(5))
            this.w_CODCLI = NVL(CLCODCON435,space(15))
            this.w_TELFAX = NVL(CLTELFAX435,space(18))
            this.w_EMAIL = NVL(CL_EMAIL435,space(50))
            this.w_FLFIDO = NVL(CLFLFIDO435,space(1))
          else
          .link_4_35('Load')
          endif
        .w_MDTIPDOC = NVL(MDTIPDOC,space(5))
          if link_4_37_joined
            this.w_MDTIPDOC = NVL(TDTIPDOC437,NVL(this.w_MDTIPDOC,space(5)))
            this.w_MDPRD = NVL(TDPRODOC437,space(2))
            this.w_FLIMPA = NVL(TDFLIMPA437,space(1))
            this.w_FLRISC = NVL(TDFLCRIS437,space(1))
            this.w_FLANAL = NVL(TDFLANAL437,space(1))
            this.w_FLGCOM = NVL(TDFLCOMM437,space(1))
            this.w_LOTDIF = NVL(TDLOTDIF437,space(1))
            this.w_TDFLINTE = NVL(TDFLINTE437,space(1))
            this.w_TDCAUCON = NVL(TDCAUCON437,space(5))
          else
          .link_4_37('Load')
          endif
        .w_MDPRD = NVL(MDPRD,space(2))
        .w_MDALFDOC = NVL(MDALFDOC,space(10))
        .w_MDNUMDOC = NVL(MDNUMDOC,0)
        .w_MDFLFATT = NVL(MDFLFATT,space(1))
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate()
        .w_MDFLSALD = NVL(MDFLSALD,space(1))
        .oPgFrm.Page2.oPag.oObj_4_65.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_66.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_67.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_68.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_69.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_70.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_71.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_72.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_73.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_79.Calculate()
        .w_MDRIFDOC = NVL(MDRIFDOC,space(10))
        .w_MDRIFCOR = NVL(MDRIFCOR,space(10))
        .w_MDCARKIT = NVL(MDCARKIT,space(10))
        .w_MDSCAKIT = NVL(MDSCAKIT,space(10))
        .w_MDTIPCOR = NVL(MDTIPCOR,space(5))
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_87.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_88.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_90.Calculate()
        .w_MD_ERROR = NVL(MD_ERROR,space(1))
        .w_OPAGCLI = .w_MDPAGCLI
        .w_OFLRISC = .w_FLRISC
        .w_OTIPCHI = .w_MDTIPCHI
        .oPgFrm.Page2.oPag.oObj_4_104.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_105.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_106.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_107.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_oMDTIPDOC = iif(.w_EDTIPDOC, .w_oMDTIPDOC, space(5))
          .link_4_114('Load')
        .w_ERRORCOD = 'Articolo inesistente'
        .oPgFrm.Page2.oPag.oObj_4_116.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'COR_RISP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CORDRISP where MDSERIAL=KeySet.MDSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CORDRISP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CORDRISP_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CORDRISP')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CORDRISP.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CORDRISP"
        link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_20_joined=this.AddJoinedLink_2_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_28_joined=this.AddJoinedLink_2_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_29_joined=this.AddJoinedLink_2_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_104_joined=this.AddJoinedLink_2_104(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MDSERIAL',this.w_MDSERIAL  )
        select * from (i_cTable) CORDRISP where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTALE = 0
      this.w_MDTOTVEN = 0
      scan
        with this
          .w_DESPOS = space(1)
          .w_CODVAA = space(3)
          .w_LOTDIF = space(1)
          .w_FLSERG = space(1)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_UNMIS3 = space(3)
          .w_MOLTIP = 0
          .w_MOLTI3 = 0
          .w_OPERAT = space(1)
          .w_OPERA3 = space(1)
          .w_FLFRAZ = space(1)
          .w_LIPREZZO = 0
          .w_ULTSCA = ctod("  /  /  ")
          .w_CATSCA = space(5)
          .w_GRUMER = space(5)
          .w_PERIVA = 0
          .w_ORDEVA = 0
          .w_OLDEVAS = space(1)
          .w_ARTPOS = space(1)
          .w_VALUCA = 0
          .w_QTAPER = 0
          .w_QTRPER = 0
        .w_FLPAN = ' '
          .w_DESREP = space(40)
        .w_FLSERA = ' '
        .w_NOCALQTA = .F.
          .w_LISCON = 0
          .w_GESMAT = space(1)
        .w_GIACON = IIF(.cFunction='Load', ' ', 'X')
          .w_FLSTAT = space(1)
          .w_DATLOT = ctod("  /  /  ")
          .w_FLLOTT = space(1)
        .w_LOTZOOM = .T.
          .w_GESMAT = space(1)
        .w_UBIZOOM = .T.
        .w_MTCARI = 'S'
          .w_ARTDIS = space(1)
        .w_TOTMAT = -1
          .w_FLRRIF = space(1)
          .w_ORQTAEV1 = 0
          .w_DISLOT = space(1)
          .w_NOFRAZ = space(1)
          .w_MODUM2 = space(1)
          .w_UNISEP = space(1)
          .w_PREZUM = space(1)
          .w_CLUNIMIS = space(1)
          .w_CODREP = space(3)
          .w_CODIVA = space(5)
        .w_DELNOMSG = .F.
          .w_FLCOM1 = space(1)
          .w_ARCLAMAT = space(5)
          .w_NUMDEQ = space(1)
          .w_CPROWNUM = CPROWNUM
        .w_DA1 = IIF(.cFunction='Load', .w_FLGDES, ' ')
        .w_UM1 = IIF(.cFunction='Load', .w_FLUNIM, ' ')
        .w_QT1 = IIF(.cFunction='Load', iif(.w_QTADEF<>0,'S',' '), ' ')
        .w_PZ1 = IIF(.cFunction='Load', .w_FLPREZ, ' ')
        .w_SC1 = IIF(.cFunction='Load', .w_FLSCON, ' ')
          .w_MDCODICE = NVL(MDCODICE,space(20))
          if link_2_6_joined
            this.w_MDCODICE = NVL(CACODICE206,NVL(this.w_MDCODICE,space(20)))
            this.w_MDDESART = NVL(CADESART206,space(40))
            this.w_MDCODART = NVL(CACODART206,space(20))
            this.w_MDTIPRIG = NVL(CA__TIPO206,space(1))
            this.w_UNMIS3 = NVL(CAUNIMIS206,space(3))
            this.w_MOLTI3 = NVL(CAMOLTIP206,0)
            this.w_OPERA3 = NVL(CAOPERAT206,space(1))
            this.w_NUMDEQ = NVL(CANUMDEQ206,space(1))
          else
          .link_2_6('Load')
          endif
          .w_MDDESART = NVL(MDDESART,space(40))
          .w_MDCODART = NVL(MDCODART,space(20))
          if link_2_8_joined
            this.w_MDCODART = NVL(ARCODART208,NVL(this.w_MDCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1208,space(3))
            this.w_OPERAT = NVL(AROPERAT208,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP208,0)
            this.w_UNMIS2 = NVL(ARUNMIS2208,space(3))
            this.w_CATSCA = NVL(ARCATSCM208,space(5))
            this.w_GRUMER = NVL(ARGRUMER208,space(5))
            this.w_FLSERG = NVL(ARFLSERG208,space(1))
            this.w_ARTPOS = NVL(ARARTPOS208,space(1))
            this.w_CODREP = NVL(ARCODREP208,space(3))
            this.w_FLSERA = NVL(ARTIPSER208,space(1))
            this.w_FLLOTT = NVL(ARFLLOTT208,space(1))
            this.w_GESMAT = NVL(ARGESMAT208,space(1))
            this.w_ARTDIS = NVL(ARFLDISP208,space(1))
            this.w_DISLOT = NVL(ARDISLOT208,space(1))
            this.w_UNISEP = NVL(ARFLUSEP208,space(1))
            this.w_PREZUM = NVL(ARPREZUM208,space(1))
            this.w_FLCOM1 = NVL(ARSALCOM208,space(1))
            this.w_ARCLAMAT = NVL(ARCLAMAT208,space(5))
          else
          .link_2_8('Load')
          endif
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MDTIPRIG = NVL(MDTIPRIG,space(1))
          .link_2_12('Load')
          .w_MDQTAUM1 = NVL(MDQTAUM1,0)
          .w_MDUNIMIS = NVL(MDUNIMIS,space(3))
          if link_2_20_joined
            this.w_MDUNIMIS = NVL(UMCODICE220,NVL(this.w_MDUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ220,space(1))
          else
          .link_2_20('Load')
          endif
          .w_MDQTAMOV = NVL(MDQTAMOV,0)
          .w_MDCONTRA = NVL(MDCONTRA,space(15))
          .w_MDPREZZO = NVL(MDPREZZO,0)
          .w_MDSCONT1 = NVL(MDSCONT1,0)
          .w_MDFLRESO = NVL(MDFLRESO,space(1))
          .w_MDCODREP = NVL(MDCODREP,space(3))
          if link_2_28_joined
            this.w_MDCODREP = NVL(RECODREP228,NVL(this.w_MDCODREP,space(3)))
            this.w_DESREP = NVL(REDESREP228,space(40))
            this.w_MDCODIVA = NVL(RECODIVA228,space(5))
          else
          .link_2_28('Load')
          endif
          .w_MDCODIVA = NVL(MDCODIVA,space(5))
          if link_2_29_joined
            this.w_MDCODIVA = NVL(IVCODIVA229,NVL(this.w_MDCODIVA,space(5)))
            this.w_PERIVA = NVL(IVPERIVA229,0)
          else
          .link_2_29('Load')
          endif
          .w_MDSCONT2 = NVL(MDSCONT2,0)
          .w_MDSCONT3 = NVL(MDSCONT3,0)
          .w_MDSCONT4 = NVL(MDSCONT4,0)
          .w_MDFLOMAG = NVL(MDFLOMAG,space(1))
          .w_MDFLCASC = NVL(MDFLCASC,space(1))
          .w_MDMAGSAL = NVL(MDMAGSAL,space(5))
          .link_2_35('Load')
          .w_MDSCOVEN = NVL(MDSCOVEN,0)
        .w_ARCODART = .w_MDCODART
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_46.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_47.Calculate()
        .w_OFLSCO = ' '
        .w_IMPNAZ = IIF(.w_VALRIG=0 Or .w_MDQTAUM1=0,0,cp_ROUND(.w_VALRIG/.w_MDQTAUM1,.w_DECTOT))
        .w_VALRIG = IIF(g_FLESSC='S' And .w_MDDATREG >= g_DATESC,CAVALRIG(.w_MDPREZZO,.w_MDQTAMOV, 0, 0, 0, 0,.w_DECTOT), CAVALRIG(.w_MDPREZZO,.w_MDQTAMOV, .w_MDSCONT1,.w_MDSCONT2,.w_MDSCONT3,.w_MDSCONT4,.w_DECTOT) )
        .w_RIGNET = IIF(.w_MDCODICE = .w_SERFID And .w_FLFAFI = 'S', 0, IIF(.w_MDFLOMAG='X', .w_VALRIG,IIF(.w_MDFLOMAG='I', .w_VALRIG - CALNET(.w_VALRIG,.w_PERIVA,.w_DECTOT,Space(3),0),0) ) )
          .w_MDSERRIF = NVL(MDSERRIF,space(10))
          .w_MDNUMRIF = NVL(MDNUMRIF,0)
          .w_MDROWRIF = NVL(MDROWRIF,0)
          .link_2_56('Load')
          .w_MDFLARIF = NVL(MDFLARIF,space(1))
          .w_MDFLERIF = NVL(MDFLERIF,space(1))
          .w_MDQTAIMP = NVL(MDQTAIMP,0)
          .w_MDQTAIM1 = NVL(MDQTAIM1,0)
          .w_MDCODPRO = NVL(MDCODPRO,space(15))
          .w_MDFLESCL = NVL(MDFLESCL,space(1))
          .w_MDFLROMA = NVL(MDFLROMA,space(1))
          .w_MDSCOPRO = NVL(MDSCOPRO,0)
        .w_OQTAIMP = .w_MDQTAIMP
        .w_OQTAIM1 = .w_MDQTAIM1
        .w_OFLERIF = .w_MDFLERIF
        .w_OPREZZO = .w_MDPREZZO
        .w_QTDISP = .w_QTAPER-.w_QTRPER
        .oPgFrm.Page1.oPag.oObj_2_78.Calculate()
          .w_MDFLULPV = NVL(MDFLULPV,space(1))
          .w_MDVALULT = NVL(MDVALULT,0)
        .oPgFrm.Page1.oPag.oObj_2_81.Calculate()
          .w_MDTIPPRO = NVL(MDTIPPRO,space(2))
          .w_MDPERPRO = NVL(MDPERPRO,0)
        .oPgFrm.Page1.oPag.oObj_2_86.Calculate(IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)<>'#########' AND NOT EMPTY(.w_MDCODPRO),'Sc. Promozione:',IIF(.w_MDTIPRIG<>'D','Sc. Ventilato:',IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)='#########' AND NOT EMPTY(.w_MDCODPRO),'Sconto:',''))))
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))=.T., 'Commessa:',''))
        .w_COCODVAL = IIF(EMPTY(.w_COCODVAL), g_PERVAL, .w_COCODVAL)
          .link_2_91('Load')
          .w_MDIMPCOM = NVL(MDIMPCOM,0)
          .w_MDFLLOTT = NVL(MDFLLOTT,space(1))
          .w_MDCODLOT = NVL(MDCODLOT,space(20))
          .link_2_100('Load')
          .w_MDCODUBI = NVL(MDCODUBI,space(20))
          * evitabile
          *.link_2_101('Load')
          .w_MDCODCOM = NVL(MDCODCOM,space(15))
          if link_2_104_joined
            this.w_MDCODCOM = NVL(CNCODCAN304,NVL(this.w_MDCODCOM,space(15)))
            this.w_COCODVAL = NVL(CNCODVAL304,space(3))
            this.w_DTOCOM = NVL(cp_ToDate(CNDTOBSO304),ctod("  /  /  "))
          else
          .link_2_104('Load')
          endif
          .w_MDMAGRIG = NVL(MDMAGRIG,space(5))
        .w_OLDQTA = .w_MDQTAUM1
        .w_FLDISP = IIF(g_PERDIS='S', .w_ARTDIS, ' ')
          .w_MDTIPPR2 = NVL(MDTIPPR2,space(2))
          .w_MDPROCAP = NVL(MDPROCAP,0)
          .w_MDLOTMAG = NVL(MDLOTMAG,space(5))
          * evitabile
          *.link_2_117('Load')
          .w_MDPROMOZ = NVL(MDPROMOZ,0)
          .w_MDESPSCO = NVL(MDESPSCO,0)
          select (this.cTrsName)
          append blank
          replace MDCODVAL with .w_MDCODVAL
          replace MDDATREG with .w_MDDATREG
          replace MDCODART with .w_MDCODART
          replace MDQTAUM1 with .w_MDQTAUM1
          replace MDFLCASC with .w_MDFLCASC
          replace MDMAGSAL with .w_MDMAGSAL
          replace MDFLULPV with .w_MDFLULPV
          replace MDVALULT with .w_MDVALULT
          replace MDCODLOT with .w_MDCODLOT
          replace MDCODUBI with .w_MDCODUBI
          replace MDLOTMAG with .w_MDLOTMAG
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALE = .w_TOTALE+.w_VALRIG
          .w_MDTOTVEN = .w_MDTOTVEN+.w_RIGNET
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CODAZI = i_CODAZI
        .w_DECUNI = g_PERPUL
        .w_DECTOT = g_PERPVL
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_READPAR = .w_MDCODNEG
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(IIF(.w_MDFLTRAS='S', '*** Vendita Trasferita', ''))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .w_OBTEST = .w_MDDATREG
        .w_NULLA = ' '
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .w_FLPRG = 'V'
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .w_TESLOT = .F.
        .oPgFrm.Page1.oPag.oObj_1_148.Calculate()
        .w_READHARD = IIF( TYPE('g_CODCAS')='C',g_CODCAS, SPACE(5))
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_65.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_66.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_67.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_68.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_69.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_70.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_71.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_72.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_73.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_79.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_87.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_88.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_90.Calculate()
        .w_OPAGCLI = .w_MDPAGCLI
        .w_OFLRISC = .w_FLRISC
        .w_OTIPCHI = .w_MDTIPCHI
        .oPgFrm.Page2.oPag.oObj_4_104.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_105.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_106.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_107.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_oMDTIPDOC = iif(.w_EDTIPDOC, .w_oMDTIPDOC, space(5))
        .w_ERRORCOD = 'Articolo inesistente'
        .oPgFrm.Page2.oPag.oObj_4_116.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_40.enabled = .oPgFrm.Page1.oPag.oBtn_1_40.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_43.enabled = .oPgFrm.Page1.oPag.oBtn_1_43.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_38.enabled = .oPgFrm.Page1.oPag.oBtn_2_38.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_39.enabled = .oPgFrm.Page1.oPag.oBtn_2_39.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_40.enabled = .oPgFrm.Page1.oPag.oBtn_2_40.mCond()
        .oPgFrm.Page2.oPag.oBtn_4_89.enabled = .oPgFrm.Page2.oPag.oBtn_4_89.mCond()
        .oPgFrm.Page2.oPag.oBtn_4_91.enabled = .oPgFrm.Page2.oPag.oBtn_4_91.mCond()
        .oPgFrm.Page2.oPag.oBtn_4_95.enabled = .oPgFrm.Page2.oPag.oBtn_4_95.mCond()
        .oPgFrm.Page2.oPag.oBtn_4_96.enabled = .oPgFrm.Page2.oPag.oBtn_4_96.mCond()
        .oPgFrm.Page2.oPag.oBtn_4_98.enabled = .oPgFrm.Page2.oPag.oBtn_4_98.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
    this.Calculate_AYUGABETUN()
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsps_mvd
    * --- Memorizza la chiave per automatizzare i dati di testata
    g_SERIAL=this.w_MDSERIAL
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CODCAS=space(5)
      .w_MATSCO=space(20)
      .w_CODAZI=space(5)
      .w_DECUNI=0
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_CALCPICU=0
      .w_MDCODVAL=space(3)
      .w_MDCAOVAL=0
      .w_MDSERIAL=space(10)
      .w_MDDATREG=ctod("  /  /  ")
      .w_MDORAVEN=space(2)
      .w_MDMINVEN=space(2)
      .w_MDCODUTE=0
      .w_MDCODESE=space(4)
      .w_MDCODNEG=space(3)
      .w_MDTIPVEN=space(5)
      .w_DESVEN=space(35)
      .w_LISVEN=space(5)
      .w_CLIVEN=space(15)
      .w_FLSCON=space(1)
      .w_QTADEF=0
      .w_FLPREZ=space(1)
      .w_FLGDES=space(1)
      .w_FLUNIM=space(1)
      .w_READPAR=space(10)
      .w_ALFDOC=space(10)
      .w_MDCODMAG=space(5)
      .w_MDCODFID=space(20)
      .w_CLIFID=space(15)
      .w_CODNEG=space(10)
      .w_MDCODCLI=space(15)
      .w_DESCLI=space(40)
      .w_MDCODLIS=space(5)
      .w_MDNUMSCO=0
      .w_MDMATSCO=space(20)
      .w_LISIVA=space(1)
      .w_LISVAL=space(3)
      .w_MDFLTRAS=space(1)
      .w_NUMSCO=0
      .w_PAGVEN=space(5)
      .w_PAGCLI=space(5)
      .w_MDANNDOC=space(4)
      .w_DOCCOR=space(5)
      .w_DOCRIC=space(5)
      .w_DOCFAF=space(5)
      .w_DOCRIF=space(5)
      .w_DOCDDT=space(5)
      .w_DOCFAT=space(5)
      .w_CATSCC=space(5)
      .w_VALFID=0
      .w_FIDUTI=0
      .w_SCOLIS=space(1)
      .w_CATCOM=space(3)
      .w_FIMPPRE=0
      .w_FIMPRES=0
      .w_FTOTIMP=0
      .w_FPUNFID=0
      .w_FNUMVIS=0
      .w_FDATATT=ctod("  /  /  ")
      .w_FDATSCA=ctod("  /  /  ")
      .w_FDATUAC=ctod("  /  /  ")
      .w_LISCLI=space(5)
      .w_PDOCCOR=space(5)
      .w_PDOCRIC=space(5)
      .w_PDOCFAF=space(5)
      .w_PDOCRIF=space(5)
      .w_PDOCDDT=space(5)
      .w_PDOCFAT=space(5)
      .w_DOCCO1=space(5)
      .w_DOCCO2=space(5)
      .w_DOCCO3=space(5)
      .w_SCOCL1=0
      .w_SCOCL2=0
      .w_GIAIMP=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_PROMOK=.f.
      .w_CODCLI=space(15)
      .w_NULLA=space(10)
      .w_ARTDES=space(10)
      .w_DESPOS=space(1)
      .w_MDCODOPE=0
      .w_ERRORE=.f.
      .w_CAURES=space(5)
      .w_CARKIT=space(5)
      .w_SCAKIT=space(5)
      .w_FLUBIC=space(1)
      .w_DESFID=space(40)
      .w_FLFIDO=space(1)
      .w_CODVAA=space(3)
      .w_PRZVAC=space(1)
      .w_PRZDES=space(1)
      .w_FLGCOM=space(1)
      .w_FLANAL=space(1)
      .w_DTOCOM=ctod("  /  /  ")
      .w_CODESC=space(5)
      .w_CLOBSO=ctod("  /  /  ")
      .w_SIMVAL=space(5)
      .w_SERFID=space(20)
      .w_FLFAFI=space(1)
      .w_LOTDIF=space(1)
      .w_NUMRIF=0
      .w_FLPRG=space(1)
      .w_SHOWMMT=space(1)
      .w_TESLOT=.f.
      .w_HASEVENT=.f.
      .w_HASEVCOP=space(50)
      .w_READHARD=space(5)
      .w_PASQTA=space(1)
      .w_FLFOM=space(1)
      .w_DEFCODREP=space(3)
      .w_DA1=space(1)
      .w_UM1=space(1)
      .w_QT1=space(1)
      .w_PZ1=space(1)
      .w_SC1=space(1)
      .w_MDCODICE=space(20)
      .w_MDDESART=space(40)
      .w_MDCODART=space(20)
      .w_FLSERG=space(1)
      .w_CPROWORD=10
      .w_MDTIPRIG=space(1)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_MOLTIP=0
      .w_MOLTI3=0
      .w_OPERAT=space(1)
      .w_OPERA3=space(1)
      .w_MDQTAUM1=0
      .w_MDUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_MDQTAMOV=0
      .w_LIPREZZO=0
      .w_MDCONTRA=space(15)
      .w_MDPREZZO=0
      .w_MDSCONT1=0
      .w_MDFLRESO=space(1)
      .w_MDCODREP=space(3)
      .w_MDCODIVA=space(5)
      .w_MDSCONT2=0
      .w_MDSCONT3=0
      .w_MDSCONT4=0
      .w_MDFLOMAG=space(1)
      .w_MDFLCASC=space(1)
      .w_MDMAGSAL=space(5)
      .w_ULTSCA=ctod("  /  /  ")
      .w_MDSCOVEN=0
      .w_ARCODART=space(20)
      .w_CATSCA=space(5)
      .w_GRUMER=space(5)
      .w_OFLSCO=space(1)
      .w_IMPNAZ=0
      .w_VALRIG=0
      .w_RIGNET=0
      .w_PERIVA=0
      .w_MDSERRIF=space(10)
      .w_MDNUMRIF=0
      .w_MDROWRIF=0
      .w_MDFLARIF=space(1)
      .w_MDFLERIF=space(1)
      .w_MDQTAIMP=0
      .w_MDQTAIM1=0
      .w_ORDEVA=0
      .w_OLDEVAS=space(1)
      .w_MDCODPRO=space(15)
      .w_MDFLESCL=space(1)
      .w_MDFLROMA=space(1)
      .w_MDSCOPRO=0
      .w_ARTPOS=space(1)
      .w_VALUCA=0
      .w_QTAPER=0
      .w_QTRPER=0
      .w_FLPAN=space(1)
      .w_OQTAIMP=0
      .w_OQTAIM1=0
      .w_OFLERIF=space(1)
      .w_OPREZZO=0
      .w_DESREP=space(40)
      .w_QTDISP=0
      .w_MDFLULPV=space(1)
      .w_MDVALULT=0
      .w_FLSERA=space(1)
      .w_MDTIPPRO=space(2)
      .w_MDPERPRO=0
      .w_NOCALQTA=.f.
      .w_LISCON=0
      .w_GESMAT=space(1)
      .w_OCOM=space(15)
      .w_COCODVAL=space(3)
      .w_DECCOM=0
      .w_MDIMPCOM=0
      .w_GIACON=space(1)
      .w_FLSTAT=space(1)
      .w_DATLOT=ctod("  /  /  ")
      .w_FLLOTT=space(1)
      .w_MDFLLOTT=space(1)
      .w_LOTZOOM=.f.
      .w_MDCODLOT=space(20)
      .w_MDCODUBI=space(20)
      .w_GESMAT=space(1)
      .w_MDCODCOM=space(15)
      .w_UBIZOOM=.f.
      .w_MTCARI=space(1)
      .w_MDMAGRIG=space(5)
      .w_OLDQTA=0
      .w_ARTDIS=space(1)
      .w_FLDISP=space(1)
      .w_TOTMAT=0
      .w_FLRRIF=space(1)
      .w_ORQTAEV1=0
      .w_DISLOT=space(1)
      .w_MDTIPPR2=space(2)
      .w_MDPROCAP=0
      .w_MDLOTMAG=space(5)
      .w_NOFRAZ=space(1)
      .w_MODUM2=space(1)
      .w_UNISEP=space(1)
      .w_PREZUM=space(1)
      .w_CLUNIMIS=space(1)
      .w_CODREP=space(3)
      .w_CODIVA=space(5)
      .w_TOTALE=0
      .w_MDTOTVEN=0
      .w_MDPUNFID=0
      .w_MDSTOPRO=0
      .w_MDSCOCL1=0
      .w_MDSCOCL2=0
      .w_MDSCOPAG=0
      .w_MDSCONTI=0
      .w_MDFLFOSC=space(1)
      .w_MDCODPAG=space(5)
      .w_SCOPAG=0
      .w_MDIMPPRE=0
      .w_MDTOTDOC=0
      .w_MDACCPRE=0
      .w_MDPAGCON=0
      .w_MDPAGASS=0
      .w_MDRIFASS=space(40)
      .w_MDPAGCAR=0
      .w_MDCODCAR=space(5)
      .w_MDPAGFIN=0
      .w_CCDESCRI=space(40)
      .w_MDCODFIN=space(5)
      .w_DESFIN=space(40)
      .w_MDPAGCLI=0
      .w_MDIMPABB=0
      .w_MDTIPCHI=space(2)
      .w_MDCODCLI=space(15)
      .w_DESCLI=space(40)
      .w_MDTIPDOC=space(5)
      .w_MDPRD=space(2)
      .w_MDALFDOC=space(10)
      .w_MDNUMDOC=0
      .w_MDFLFATT=space(1)
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_TELFAX=space(18)
      .w_EMAIL=space(50)
      .w_OTES=space(1)
      .w_MDFLSALD=space(1)
      .w_FLIMPA=space(1)
      .w_FLNSRI=space(1)
      .w_FLVSRI=space(1)
      .w_MDRIFDOC=space(10)
      .w_MDRIFCOR=space(10)
      .w_MDCARKIT=space(10)
      .w_MDSCAKIT=space(10)
      .w_MDTIPCOR=space(5)
      .w_OTOTDOC=0
      .w_OIMPPRE=0
      .w_OPUNFID=0
      .w_MD_ERROR=space(1)
      .w_SERIAL=space(10)
      .w_FLRISC=space(1)
      .w_OPAGCLI=0
      .w_OFLRISC=space(1)
      .w_OTIPCHI=space(1)
      .w_RESCHK=0
      .w_EDCL3KEU=.f.
      .w_EDTIPDOC=.f.
      .w_oMDTIPDOC=space(5)
      .w_TDFLINTE=space(1)
      .w_zMDTIPDOC=space(5)
      .w_TDCAUCON=space(5)
      .w_CCFLRIFE=space(1)
      .w_DELNOMSG=.f.
      .w_FLRICIVA=.f.
      .w_RIIVDTIN=ctod("  /  /  ")
      .w_RIIVDTFI=ctod("  /  /  ")
      .w_RIIVLSIV=space(0)
      .w_ERRORCOD=space(200)
      .w_FLCOM1=space(1)
      .w_ARCLAMAT=space(5)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      .w_MDPROMOZ=0
      .w_MDESPSCO=0
      .w_NUMDEQ=space(1)
      if .cFunction<>"Filter"
        .w_CODCAS = IIF( Type('g_CODCAS')='C' , g_CODCAS, space(5))
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODCAS))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        .w_CODAZI = i_CODAZI
        .w_DECUNI = g_PERPUL
        .w_DECTOT = g_PERPVL
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_MDCODVAL = g_PERVAL
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_MDCODVAL))
         .link_1_8('Full')
        endif
        .w_MDCAOVAL = GETCAM(.w_MDCODVAL, I_DATSYS)
        .DoRTCalc(10,10,.f.)
        .w_MDDATREG = DATE()
        .w_MDORAVEN = left(time(),2)
        .w_MDMINVEN = substr(time(),4,2)
        .w_MDCODUTE = i_CODUTE
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_MDCODUTE))
         .link_1_17('Full')
        endif
        .w_MDCODESE = g_CODESE
        .w_MDCODNEG = g_CODNEG
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_MDTIPVEN))
         .link_1_21('Full')
        endif
        .DoRTCalc(18,25,.f.)
        .w_READPAR = .w_MDCODNEG
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_READPAR))
         .link_1_31('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_MDCODMAG))
         .link_1_34('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_MDCODFID))
         .link_1_35('Full')
        endif
        .DoRTCalc(30,30,.f.)
        .w_CODNEG = iif(type('g_CODNEG')<>'U',g_CODNEG,' ')
        .w_MDCODCLI = IIF(EMPTY(.w_MDCODFID), .w_CLIVEN, .w_CLIFID)
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_MDCODCLI))
         .link_1_41('Full')
        endif
        .DoRTCalc(33,34,.f.)
        if not(empty(.w_MDCODLIS))
         .link_1_44('Full')
        endif
        .DoRTCalc(35,35,.f.)
        .w_MDMATSCO = IIF(.cFunction='Load',.w_MATSCO,.w_MDMATSCO)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(IIF(.w_MDFLTRAS='S', '*** Vendita Trasferita', ''))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(37,42,.f.)
        .w_MDANNDOC = STR(YEAR(.w_MDDATREG), 4, 0)
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .DoRTCalc(44,74,.f.)
        .w_GIAIMP = ' '
        .w_OBTEST = .w_MDDATREG
        .w_PROMOK = .F.
        .DoRTCalc(78,78,.f.)
        if not(empty(.w_CODCLI))
         .link_1_102('Full')
        endif
        .w_NULLA = ' '
        .w_ARTDES = g_ARTDES
        .DoRTCalc(80,80,.f.)
        if not(empty(.w_ARTDES))
         .link_1_104('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .DoRTCalc(81,82,.f.)
        .w_ERRORE = .F.
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .DoRTCalc(84,101,.f.)
        .w_NUMRIF = -30
        .w_FLPRG = 'V'
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .w_SHOWMMT = 'S'
        .w_TESLOT = .F.
        .oPgFrm.Page1.oPag.oObj_1_148.Calculate()
        .DoRTCalc(106,107,.f.)
        .w_READHARD = IIF( TYPE('g_CODCAS')='C',g_CODCAS, SPACE(5))
        .DoRTCalc(108,108,.f.)
        if not(empty(.w_READHARD))
         .link_1_149('Full')
        endif
        .DoRTCalc(109,111,.f.)
        .w_DA1 = IIF(.cFunction='Load', .w_FLGDES, ' ')
        .w_UM1 = IIF(.cFunction='Load', .w_FLUNIM, ' ')
        .w_QT1 = IIF(.cFunction='Load', iif(.w_QTADEF<>0,'S',' '), ' ')
        .w_PZ1 = IIF(.cFunction='Load', .w_FLPREZ, ' ')
        .w_SC1 = IIF(.cFunction='Load', .w_FLSCON, ' ')
        .DoRTCalc(117,117,.f.)
        if not(empty(.w_MDCODICE))
         .link_2_6('Full')
        endif
        .DoRTCalc(118,119,.f.)
        if not(empty(.w_MDCODART))
         .link_2_8('Full')
        endif
        .DoRTCalc(120,123,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_12('Full')
        endif
        .DoRTCalc(124,129,.f.)
        .w_MDQTAUM1 = IIF(.w_MDTIPRIG='F', 1, CALQTA(.w_MDQTAMOV,.w_MDUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_UNISEP, .w_NOFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        .w_MDUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(131,131,.f.)
        if not(empty(.w_MDUNIMIS))
         .link_2_20('Full')
        endif
        .DoRTCalc(132,132,.f.)
        .w_MDQTAMOV = IIF(.w_MDQTAMOV=0 and not empty(.w_MDCODICE) And Not .w_NOCALQTA,IIF(.w_MDTIPRIG='F', 1, IIF(.w_MDTIPRIG='D', 0, .w_QTADEF)),.w_MDQTAMOV)
        .DoRTCalc(134,135,.f.)
        .w_MDPREZZO = IIF((.w_MDTIPRIG $ 'RFMA'), CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MDUNIMIS+.w_OPERAT+.w_OPERA3+IIF(.w_LISCON=2, .w_LISIVA, 'N')+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1), 0), 0)
        .DoRTCalc(137,138,.f.)
        .w_MDCODREP = IIF(Empty(.w_DEFCODREP), .w_CODREP, .w_DEFCODREP)
        .DoRTCalc(139,139,.f.)
        if not(empty(.w_MDCODREP))
         .link_2_28('Full')
        endif
        .DoRTCalc(140,140,.f.)
        if not(empty(.w_MDCODIVA))
         .link_2_29('Full')
        endif
        .w_MDSCONT2 = 0
        .w_MDSCONT3 = 0
        .w_MDSCONT4 = 0
        .w_MDFLOMAG = 'X'
        .w_MDFLCASC = IIF(.w_MDFLRESO='S', '+', '-')
        .w_MDMAGSAL = IIF(.w_MDTIPRIG='R' AND .w_MDFLCASC<>' ', .w_MDCODMAG, SPACE(5))
        .DoRTCalc(146,146,.f.)
        if not(empty(.w_MDMAGSAL))
         .link_2_35('Full')
        endif
        .DoRTCalc(147,148,.f.)
        .w_ARCODART = .w_MDCODART
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_46.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_47.Calculate()
        .DoRTCalc(150,151,.f.)
        .w_OFLSCO = ' '
        .w_IMPNAZ = IIF(.w_VALRIG=0 Or .w_MDQTAUM1=0,0,cp_ROUND(.w_VALRIG/.w_MDQTAUM1,.w_DECTOT))
        .w_VALRIG = IIF(g_FLESSC='S' And .w_MDDATREG >= g_DATESC,CAVALRIG(.w_MDPREZZO,.w_MDQTAMOV, 0, 0, 0, 0,.w_DECTOT), CAVALRIG(.w_MDPREZZO,.w_MDQTAMOV, .w_MDSCONT1,.w_MDSCONT2,.w_MDSCONT3,.w_MDSCONT4,.w_DECTOT) )
        .w_RIGNET = IIF(.w_MDCODICE = .w_SERFID And .w_FLFAFI = 'S', 0, IIF(.w_MDFLOMAG='X', .w_VALRIG,IIF(.w_MDFLOMAG='I', .w_VALRIG - CALNET(.w_VALRIG,.w_PERIVA,.w_DECTOT,Space(3),0),0) ) )
        .DoRTCalc(156,157,.f.)
        .w_MDNUMRIF = -30
        .DoRTCalc(159,159,.f.)
        if not(empty(.w_MDROWRIF))
         .link_2_56('Full')
        endif
        .DoRTCalc(160,173,.f.)
        .w_FLPAN = ' '
        .w_OQTAIMP = .w_MDQTAIMP
        .w_OQTAIM1 = .w_MDQTAIM1
        .w_OFLERIF = .w_MDFLERIF
        .w_OPREZZO = .w_MDPREZZO
        .DoRTCalc(179,179,.f.)
        .w_QTDISP = .w_QTAPER-.w_QTRPER
        .oPgFrm.Page1.oPag.oObj_2_78.Calculate()
        .w_MDFLULPV = IIF(.w_MDFLCASC='-' AND .w_MDDATREG>=.w_ULTSCA AND .w_MDFLOMAG<>'S', '=', ' ')
        .w_MDVALULT = CALNET(.w_IMPNAZ,.w_PERIVA,.w_DECTOT,SPACE(5), 0)
        .oPgFrm.Page1.oPag.oObj_2_81.Calculate()
        .w_FLSERA = ' '
        .w_MDTIPPRO = IIF(.w_MDFLOMAG='X','DC','ES')
        .DoRTCalc(185,185,.f.)
        .w_NOCALQTA = .F.
        .oPgFrm.Page1.oPag.oObj_2_86.Calculate(IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)<>'#########' AND NOT EMPTY(.w_MDCODPRO),'Sc. Promozione:',IIF(.w_MDTIPRIG<>'D','Sc. Ventilato:',IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)='#########' AND NOT EMPTY(.w_MDCODPRO),'Sconto:',''))))
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))=.T., 'Commessa:',''))
        .DoRTCalc(187,188,.f.)
        .w_OCOM = IIF(.w_MDTIPRIG<>'D', .w_MDCODCOM, .w_OCOM)
        .w_COCODVAL = IIF(EMPTY(.w_COCODVAL), g_PERVAL, .w_COCODVAL)
        .DoRTCalc(190,190,.f.)
        if not(empty(.w_COCODVAL))
         .link_2_91('Full')
        endif
        .DoRTCalc(191,191,.f.)
        .w_MDIMPCOM = CAIMPCOM( IIF(Empty(.w_MDCODCOM),'S', ' '), g_PERVAL, .w_COCODVAL, .w_VALRIG, g_CAOEUR, .w_MDDATREG, .w_DECCOM )
        .w_GIACON = IIF(.cFunction='Load', ' ', 'X')
        .DoRTCalc(194,196,.f.)
        .w_MDFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_MDFLCASC),1),' ')
        .w_LOTZOOM = .T.
        .DoRTCalc(199,199,.f.)
        if not(empty(.w_MDCODLOT))
         .link_2_100('Full')
        endif
        .DoRTCalc(200,200,.f.)
        if not(empty(.w_MDCODUBI))
         .link_2_101('Full')
        endif
        .DoRTCalc(201,201,.f.)
        .w_MDCODCOM = IIF(.w_MDTIPRIG<>'D' And .w_FLANAL='S' And .w_FLGCOM='S', .w_OCOM, SPACE(15))
        .DoRTCalc(202,202,.f.)
        if not(empty(.w_MDCODCOM))
         .link_2_104('Full')
        endif
        .w_UBIZOOM = .T.
        .w_MTCARI = 'S'
        .w_MDMAGRIG = IIF(.w_MDTIPRIG='R',.w_MDCODMAG,Space(20) )
        .w_OLDQTA = .w_MDQTAUM1
        .DoRTCalc(207,207,.f.)
        .w_FLDISP = IIF(g_PERDIS='S', .w_ARTDIS, ' ')
        .w_TOTMAT = -1
        .DoRTCalc(210,212,.f.)
        .w_MDTIPPR2 = IIF(.w_MDFLOMAG='X','DC','ES')
        .DoRTCalc(214,214,.f.)
        .w_MDLOTMAG = iif( Empty( .w_MDCODLOT ) And Empty( .w_MDCODUBI ) , SPACE(5) , LEFT(.w_MDMAGSAL,5) )
        .DoRTCalc(215,215,.f.)
        if not(empty(.w_MDLOTMAG))
         .link_2_117('Full')
        endif
        .DoRTCalc(216,227,.f.)
        .w_MDSCOCL2 = IIF(.w_MDSCOCL1=0, 0, .w_MDSCOCL2)
        .w_MDSCOPAG = .w_SCOPAG
        .w_MDSCONTI = cp_Round( IIF(.w_MDFLFOSC<>'S', - (.w_MDTOTVEN + .w_MDSTOPRO) * cp_Round((1 - (1+.w_MDSCOCL1/100)*(1+.w_MDSCOCL2/100)*(1+.w_MDSCOPAG/100)), 10) ,.w_MDSCONTI) , .w_DECTOT )
        .DoRTCalc(231,232,.f.)
        if not(empty(.w_MDCODPAG))
         .link_4_9('Full')
        endif
        .DoRTCalc(233,233,.f.)
        .w_MDIMPPRE = IIF(.w_FLFAFI ='S', .w_MDIMPPRE, IIF(NOT EMPTY(.w_MDCODFID) AND .w_MDTOTDOC<>0,IIF(.w_FIMPRES>0,MIN(.w_FIMPRES, ( .w_MDTOTVEN + .w_MDSTOPRO + .w_MDSCONTI ) ),0),0) )
        .w_MDTOTDOC = cp_ROUND(.w_MDTOTVEN + (.w_MDSCONTI+ .w_MDSTOPRO - IIF(.w_FLFAFI ='S' And Not Empty(.w_SERFID),.w_MDIMPPRE,0)), .w_DECTOT+1)
        .DoRTCalc(236,238,.f.)
        .w_MDRIFASS = IIF(.w_MDPAGASS=0,' ',.w_MDRIFASS)
        .DoRTCalc(240,240,.f.)
        .w_MDCODCAR = IIF(.w_MDPAGCAR=0,'',.w_MDCODCAR)
        .DoRTCalc(241,241,.f.)
        if not(empty(.w_MDCODCAR))
         .link_4_19('Full')
        endif
        .DoRTCalc(242,243,.f.)
        .w_MDCODFIN = IIF(.w_MDPAGFIN=0,'',.w_MDCODFIN)
        .DoRTCalc(244,244,.f.)
        if not(empty(.w_MDCODFIN))
         .link_4_22('Full')
        endif
        .DoRTCalc(245,248,.f.)
        .w_MDCODCLI = IIF(EMPTY(.w_MDCODFID), .w_CLIVEN, .w_CLIFID)
        .DoRTCalc(249,249,.f.)
        if not(empty(.w_MDCODCLI))
         .link_4_35('Full')
        endif
        .DoRTCalc(250,251,.f.)
        if not(empty(.w_MDTIPDOC))
         .link_4_37('Full')
        endif
        .DoRTCalc(252,252,.f.)
        .w_MDALFDOC = .w_ALFDOC
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate()
        .DoRTCalc(254,260,.f.)
        .w_MDFLSALD = ' '
        .oPgFrm.Page2.oPag.oObj_4_65.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_66.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_67.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_68.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_69.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_70.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_71.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_72.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_73.Calculate()
        .DoRTCalc(262,262,.f.)
        .w_FLNSRI = ' '
        .w_FLVSRI = ' '
        .oPgFrm.Page2.oPag.oObj_4_79.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_87.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_88.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_90.Calculate()
        .DoRTCalc(265,272,.f.)
        .w_MD_ERROR = ' '
        .w_SERIAL = .w_MDSERIAL
        .DoRTCalc(275,275,.f.)
        .w_OPAGCLI = .w_MDPAGCLI
        .w_OFLRISC = .w_FLRISC
        .w_OTIPCHI = .w_MDTIPCHI
        .oPgFrm.Page2.oPag.oObj_4_104.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_105.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_106.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_107.Calculate()
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(280,280,.f.)
        .w_EDTIPDOC = .F.
        .w_oMDTIPDOC = iif(.w_EDTIPDOC, .w_oMDTIPDOC, space(5))
        .DoRTCalc(283,285,.f.)
        if not(empty(.w_TDCAUCON))
         .link_4_114('Full')
        endif
        .DoRTCalc(286,286,.f.)
        .w_DELNOMSG = .F.
        .w_FLRICIVA = .f.
        .DoRTCalc(289,291,.f.)
        .w_ERRORCOD = 'Articolo inesistente'
        .oPgFrm.Page2.oPag.oObj_4_116.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'COR_RISP')
    this.DoRTCalc(293,301,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_38.enabled = this.oPgFrm.Page1.oPag.oBtn_2_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_39.enabled = this.oPgFrm.Page1.oPag.oBtn_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_40.enabled = this.oPgFrm.Page1.oPag.oBtn_2_40.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_89.enabled = this.oPgFrm.Page2.oPag.oBtn_4_89.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_91.enabled = this.oPgFrm.Page2.oPag.oBtn_4_91.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_95.enabled = this.oPgFrm.Page2.oPag.oBtn_4_95.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_96.enabled = this.oPgFrm.Page2.oPag.oBtn_4_96.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_98.enabled = this.oPgFrm.Page2.oPag.oBtn_4_98.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsps_mvd
    * --- Caricamento Rapido ed avvisi preliminari al caricamento
    if this.cFunction='Load' and not empty(g_SERIAL)
       this.NotifyEvent('CaricaRapido')
    
      ** Controllo Articoli descrittivo POS obbligatorio
      if empty(this.w_ARTDES) or this.w_DESPOS<>'S'
        Ah_ErrorMsg("Articolo per riferimenti descrittivi inesistente o incongruente (no P.O.S.)%0Non sar� possibile confermare la vendita")
      endif
    
      ** Controllo Causali Resi/KIT
      if empty(this.w_CAURES) or empty(this.w_CARKIT) or empty(this.w_SCAKIT)
        Ah_ErrorMsg("Causali magazzino per resi/kit non definite nei parametri P.O.S.%0Non sar� possibile confermare la vendita")
      endif
    
    endif
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMDSERIAL_1_10.enabled = !i_bVal
      .Page1.oPag.oMDDATREG_1_12.enabled = i_bVal
      .Page1.oPag.oMDORAVEN_1_13.enabled = i_bVal
      .Page1.oPag.oMDMINVEN_1_14.enabled = i_bVal
      .Page1.oPag.oMDCODUTE_1_17.enabled = i_bVal
      .Page1.oPag.oMDTIPVEN_1_21.enabled = i_bVal
      .Page1.oPag.oMDCODMAG_1_34.enabled = i_bVal
      .Page1.oPag.oMDCODFID_1_35.enabled = i_bVal
      .Page1.oPag.oMDCODCLI_1_41.enabled = i_bVal
      .Page1.oPag.oMDCODLIS_1_44.enabled = i_bVal
      .Page1.oPag.oMDNUMSCO_1_47.enabled = i_bVal
      .Page1.oPag.oMDMATSCO_1_48.enabled = i_bVal
      .Page1.oPag.oMDCODREP_2_28.enabled = i_bVal
      .Page1.oPag.oMDSCONT2_2_30.enabled = i_bVal
      .Page1.oPag.oMDSCONT3_2_31.enabled = i_bVal
      .Page1.oPag.oMDSCONT4_2_32.enabled = i_bVal
      .Page1.oPag.oMDFLOMAG_2_33.enabled = i_bVal
      .Page1.oPag.oMDCODLOT_2_100.enabled = i_bVal
      .Page1.oPag.oMDCODUBI_2_101.enabled = i_bVal
      .Page1.oPag.oMDCODCOM_2_104.enabled = i_bVal
      .Page2.oPag.oMDPUNFID_4_2.enabled = i_bVal
      .Page2.oPag.oMDSCOCL1_4_4.enabled = i_bVal
      .Page2.oPag.oMDSCOCL2_4_5.enabled = i_bVal
      .Page2.oPag.oMDSCOPAG_4_6.enabled = i_bVal
      .Page2.oPag.oMDSCONTI_4_7.enabled = i_bVal
      .Page2.oPag.oMDFLFOSC_4_8.enabled = i_bVal
      .Page2.oPag.oMDCODPAG_4_9.enabled = i_bVal
      .Page2.oPag.oMDIMPPRE_4_11.enabled = i_bVal
      .Page2.oPag.oMDACCPRE_4_13.enabled = i_bVal
      .Page2.oPag.oMDPAGCON_4_15.enabled = i_bVal
      .Page2.oPag.oMDPAGASS_4_16.enabled = i_bVal
      .Page2.oPag.oMDRIFASS_4_17.enabled = i_bVal
      .Page2.oPag.oMDPAGCAR_4_18.enabled = i_bVal
      .Page2.oPag.oMDCODCAR_4_19.enabled = i_bVal
      .Page2.oPag.oMDPAGFIN_4_20.enabled = i_bVal
      .Page2.oPag.oMDCODFIN_4_22.enabled = i_bVal
      .Page2.oPag.oMDPAGCLI_4_28.enabled = i_bVal
      .Page2.oPag.oMDTIPCHI_4_31.enabled = i_bVal
      .Page2.oPag.oMDCODCLI_4_35.enabled = i_bVal
      .Page2.oPag.oMDTIPDOC_4_37.enabled = i_bVal
      .Page2.oPag.oMDFLSALD_4_60.enabled = i_bVal
      .Page1.oPag.oBtn_1_38.enabled = i_bVal
      .Page1.oPag.oBtn_1_39.enabled = i_bVal
      .Page1.oPag.oBtn_1_40.enabled = .Page1.oPag.oBtn_1_40.mCond()
      .Page1.oPag.oBtn_1_43.enabled = .Page1.oPag.oBtn_1_43.mCond()
      .Page1.oPag.oBtn_2_38.enabled = .Page1.oPag.oBtn_2_38.mCond()
      .Page1.oPag.oBtn_2_39.enabled = .Page1.oPag.oBtn_2_39.mCond()
      .Page1.oPag.oBtn_2_40.enabled = .Page1.oPag.oBtn_2_40.mCond()
      .Page1.oPag.oBtn_2_41.enabled = i_bVal
      .Page2.oPag.oBtn_4_89.enabled = .Page2.oPag.oBtn_4_89.mCond()
      .Page2.oPag.oBtn_4_91.enabled = .Page2.oPag.oBtn_4_91.mCond()
      .Page2.oPag.oBtn_4_95.enabled = .Page2.oPag.oBtn_4_95.mCond()
      .Page2.oPag.oBtn_4_96.enabled = .Page2.oPag.oBtn_4_96.mCond()
      .Page2.oPag.oBtn_4_98.enabled = .Page2.oPag.oBtn_4_98.mCond()
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.oObj_1_91.enabled = i_bVal
      .Page1.oPag.oObj_1_92.enabled = i_bVal
      .Page1.oPag.oObj_1_93.enabled = i_bVal
      .Page1.oPag.oObj_1_94.enabled = i_bVal
      .Page1.oPag.oObj_1_95.enabled = i_bVal
      .Page1.oPag.oObj_1_98.enabled = i_bVal
      .Page1.oPag.oObj_1_107.enabled = i_bVal
      .Page1.oPag.oObj_1_116.enabled = i_bVal
      .Page1.oPag.oObj_1_142.enabled = i_bVal
      .Page1.oPag.oObj_1_143.enabled = i_bVal
      .Page1.oPag.oObj_1_148.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_44.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_45.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_46.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_47.enabled = i_bVal
      .Page1.oPag.oObj_2_78.enabled = i_bVal
      .Page2.oPag.oObj_4_53.enabled = i_bVal
      .Page2.oPag.oObj_4_65.enabled = i_bVal
      .Page2.oPag.oObj_4_66.enabled = i_bVal
      .Page2.oPag.oObj_4_67.enabled = i_bVal
      .Page2.oPag.oObj_4_68.enabled = i_bVal
      .Page2.oPag.oObj_4_69.enabled = i_bVal
      .Page2.oPag.oObj_4_70.enabled = i_bVal
      .Page2.oPag.oObj_4_71.enabled = i_bVal
      .Page2.oPag.oObj_4_72.enabled = i_bVal
      .Page2.oPag.oObj_4_73.enabled = i_bVal
      .Page2.oPag.oObj_4_79.enabled = i_bVal
      .Page2.oPag.oObj_4_86.enabled = i_bVal
      .Page2.oPag.oObj_4_87.enabled = i_bVal
      .Page2.oPag.oObj_4_88.enabled = i_bVal
      .Page2.oPag.oObj_4_90.enabled = i_bVal
      .Page2.oPag.oObj_4_104.enabled = i_bVal
      .Page2.oPag.oObj_4_105.enabled = i_bVal
      .Page2.oPag.oObj_4_106.enabled = i_bVal
      .Page2.oPag.oObj_4_107.enabled = i_bVal
      .Page2.oPag.oObj_4_116.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oMDNUMSCO_1_47.enabled = .t.
        .Page1.oPag.oMDMATSCO_1_48.enabled = .t.
      endif
    endwith
    this.GSVE_MMT.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'COR_RISP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COR_RISP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COR_RISP_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERPLU","i_codazi,w_MDSERIAL")
      .op_codazi = .w_codazi
      .op_MDSERIAL = .w_MDSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSVE_MMT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COR_RISP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODVAL,"MDCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCAOVAL,"MDCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDSERIAL,"MDSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDDATREG,"MDDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDORAVEN,"MDORAVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDMINVEN,"MDMINVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODUTE,"MDCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODESE,"MDCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODNEG,"MDCODNEG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDTIPVEN,"MDTIPVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODMAG,"MDCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODFID,"MDCODFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODCLI,"MDCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODLIS,"MDCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDNUMSCO,"MDNUMSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDMATSCO,"MDMATSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDFLTRAS,"MDFLTRAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDANNDOC,"MDANNDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODOPE,"MDCODOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDTOTVEN,"MDTOTVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDPUNFID,"MDPUNFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDSTOPRO,"MDSTOPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDSCOCL1,"MDSCOCL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDSCOCL2,"MDSCOCL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDSCOPAG,"MDSCOPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDSCONTI,"MDSCONTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDFLFOSC,"MDFLFOSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODPAG,"MDCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDIMPPRE,"MDIMPPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDTOTDOC,"MDTOTDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDACCPRE,"MDACCPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDPAGCON,"MDPAGCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDPAGASS,"MDPAGASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDRIFASS,"MDRIFASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDPAGCAR,"MDPAGCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODCAR,"MDCODCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDPAGFIN,"MDPAGFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODFIN,"MDCODFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDPAGCLI,"MDPAGCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDIMPABB,"MDIMPABB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDTIPCHI,"MDTIPCHI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODCLI,"MDCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDTIPDOC,"MDTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDPRD,"MDPRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDALFDOC,"MDALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDNUMDOC,"MDNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDFLFATT,"MDFLFATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDFLSALD,"MDFLSALD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDRIFDOC,"MDRIFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDRIFCOR,"MDRIFCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCARKIT,"MDCARKIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDSCAKIT,"MDSCAKIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDTIPCOR,"MDTIPCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MD_ERROR,"MD_ERROR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COR_RISP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COR_RISP_IDX,2])
    i_lTable = "COR_RISP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.COR_RISP_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GSPS_BSK(this,3)
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MDCODICE C(20);
      ,t_MDDESART C(40);
      ,t_MDCODART C(20);
      ,t_CPROWORD N(5);
      ,t_MDUNIMIS C(3);
      ,t_MDQTAMOV N(12,3);
      ,t_MDPREZZO N(12,5);
      ,t_MDSCONT1 N(6,2);
      ,t_MDFLRESO N(3);
      ,t_MDCODREP C(3);
      ,t_MDCODIVA C(5);
      ,t_MDSCONT2 N(6,2);
      ,t_MDSCONT3 N(6,2);
      ,t_MDSCONT4 N(6,2);
      ,t_MDFLOMAG N(3);
      ,t_MDSCOVEN N(18,5);
      ,t_VALRIG N(18,4);
      ,t_PERIVA N(5,2);
      ,t_MDSCOPRO N(18,5);
      ,t_DESREP C(40);
      ,t_QTDISP N(12,3);
      ,t_MDCODLOT C(20);
      ,t_MDCODUBI C(20);
      ,t_MDCODCOM C(15);
      ,MDCODVAL C(3);
      ,MDDATREG D(8);
      ,MDCODART C(20);
      ,MDQTAUM1 N(12,3);
      ,MDFLCASC C(1);
      ,MDMAGSAL C(5);
      ,MDFLULPV C(1);
      ,MDVALULT N(18,4);
      ,MDCODLOT C(20);
      ,MDCODUBI C(20);
      ,MDLOTMAG C(5);
      ,CPROWNUM N(10);
      ,t_DESPOS C(1);
      ,t_CODVAA C(3);
      ,t_LOTDIF C(1);
      ,t_DA1 C(1);
      ,t_UM1 C(1);
      ,t_QT1 C(1);
      ,t_PZ1 C(1);
      ,t_SC1 C(1);
      ,t_FLSERG C(1);
      ,t_MDTIPRIG C(1);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_UNMIS3 C(3);
      ,t_MOLTIP N(10,4);
      ,t_MOLTI3 N(10,4);
      ,t_OPERAT C(1);
      ,t_OPERA3 C(1);
      ,t_MDQTAUM1 N(12,3);
      ,t_FLFRAZ C(1);
      ,t_LIPREZZO N(18,5);
      ,t_MDCONTRA C(15);
      ,t_MDFLCASC C(1);
      ,t_MDMAGSAL C(5);
      ,t_ULTSCA D(8);
      ,t_ARCODART C(20);
      ,t_CATSCA C(5);
      ,t_GRUMER C(5);
      ,t_OFLSCO C(1);
      ,t_IMPNAZ N(18,5);
      ,t_RIGNET N(18,4);
      ,t_MDSERRIF C(10);
      ,t_MDNUMRIF N(3);
      ,t_MDROWRIF N(4);
      ,t_MDFLARIF C(1);
      ,t_MDFLERIF C(1);
      ,t_MDQTAIMP N(12,3);
      ,t_MDQTAIM1 N(12,3);
      ,t_ORDEVA N(18,4);
      ,t_OLDEVAS C(1);
      ,t_MDCODPRO C(15);
      ,t_MDFLESCL C(1);
      ,t_MDFLROMA C(1);
      ,t_ARTPOS C(1);
      ,t_VALUCA N(18,5);
      ,t_QTAPER N(12,3);
      ,t_QTRPER N(12,3);
      ,t_FLPAN C(1);
      ,t_OQTAIMP N(12,3);
      ,t_OQTAIM1 N(12,3);
      ,t_OFLERIF C(1);
      ,t_OPREZZO N(18,4);
      ,t_MDFLULPV C(1);
      ,t_MDVALULT N(18,4);
      ,t_FLSERA C(1);
      ,t_MDTIPPRO C(2);
      ,t_MDPERPRO N(5,2);
      ,t_NOCALQTA L(1);
      ,t_LISCON N(1);
      ,t_GESMAT C(1);
      ,t_COCODVAL C(3);
      ,t_MDIMPCOM N(18,4);
      ,t_GIACON C(1);
      ,t_FLSTAT C(1);
      ,t_DATLOT D(8);
      ,t_FLLOTT C(1);
      ,t_MDFLLOTT C(1);
      ,t_LOTZOOM L(1);
      ,t_UBIZOOM L(1);
      ,t_MTCARI C(1);
      ,t_MDMAGRIG C(5);
      ,t_OLDQTA N(12,3);
      ,t_ARTDIS C(1);
      ,t_FLDISP C(1);
      ,t_TOTMAT N(10);
      ,t_FLRRIF C(1);
      ,t_ORQTAEV1 N(12,3);
      ,t_DISLOT C(1);
      ,t_MDTIPPR2 C(2);
      ,t_MDPROCAP N(5,2);
      ,t_MDLOTMAG C(5);
      ,t_NOFRAZ C(1);
      ,t_MODUM2 C(1);
      ,t_UNISEP C(1);
      ,t_PREZUM C(1);
      ,t_CLUNIMIS C(1);
      ,t_CODREP C(3);
      ,t_CODIVA C(5);
      ,t_DELNOMSG L(1);
      ,t_FLCOM1 C(1);
      ,t_ARCLAMAT C(5);
      ,t_MDPROMOZ N(18,5);
      ,t_MDESPSCO N(18,5);
      ,t_NUMDEQ C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsps_mvdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODICE_2_6.controlsource=this.cTrsName+'.t_MDCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDDESART_2_7.controlsource=this.cTrsName+'.t_MDDESART'
    this.oPgFRm.Page1.oPag.oMDCODART_2_8.controlsource=this.cTrsName+'.t_MDCODART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_10.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDUNIMIS_2_20.controlsource=this.cTrsName+'.t_MDUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDQTAMOV_2_22.controlsource=this.cTrsName+'.t_MDQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDPREZZO_2_25.controlsource=this.cTrsName+'.t_MDPREZZO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDSCONT1_2_26.controlsource=this.cTrsName+'.t_MDSCONT1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDFLRESO_2_27.controlsource=this.cTrsName+'.t_MDFLRESO'
    this.oPgFRm.Page1.oPag.oMDCODREP_2_28.controlsource=this.cTrsName+'.t_MDCODREP'
    this.oPgFRm.Page1.oPag.oMDCODIVA_2_29.controlsource=this.cTrsName+'.t_MDCODIVA'
    this.oPgFRm.Page1.oPag.oMDSCONT2_2_30.controlsource=this.cTrsName+'.t_MDSCONT2'
    this.oPgFRm.Page1.oPag.oMDSCONT3_2_31.controlsource=this.cTrsName+'.t_MDSCONT3'
    this.oPgFRm.Page1.oPag.oMDSCONT4_2_32.controlsource=this.cTrsName+'.t_MDSCONT4'
    this.oPgFRm.Page1.oPag.oMDFLOMAG_2_33.controlsource=this.cTrsName+'.t_MDFLOMAG'
    this.oPgFRm.Page1.oPag.oMDSCOVEN_2_37.controlsource=this.cTrsName+'.t_MDSCOVEN'
    this.oPgFRm.Page1.oPag.oVALRIG_2_51.controlsource=this.cTrsName+'.t_VALRIG'
    this.oPgFRm.Page1.oPag.oPERIVA_2_53.controlsource=this.cTrsName+'.t_PERIVA'
    this.oPgFRm.Page1.oPag.oMDSCOPRO_2_66.controlsource=this.cTrsName+'.t_MDSCOPRO'
    this.oPgFRm.Page1.oPag.oDESREP_2_76.controlsource=this.cTrsName+'.t_DESREP'
    this.oPgFRm.Page1.oPag.oQTDISP_2_77.controlsource=this.cTrsName+'.t_QTDISP'
    this.oPgFRm.Page1.oPag.oMDCODLOT_2_100.controlsource=this.cTrsName+'.t_MDCODLOT'
    this.oPgFRm.Page1.oPag.oMDCODUBI_2_101.controlsource=this.cTrsName+'.t_MDCODUBI'
    this.oPgFRm.Page1.oPag.oMDCODCOM_2_104.controlsource=this.cTrsName+'.t_MDCODCOM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(54)
    this.AddVLine(205)
    this.AddVLine(445)
    this.AddVLine(568)
    this.AddVLine(695)
    this.AddVLine(755)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODICE_2_6
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- gsps_mvd
    * - Per Ordinare per numero riga e chiave di riga (CPROUNUM)
    SELECT (this.cTrsName)
    INDEX ON STR(t_CPROWORD,5,0)+STR(CPROWNUM,4,0) TAG TAGX
    
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COR_RISP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COR_RISP_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SERPLU","i_codazi,w_MDSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into COR_RISP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COR_RISP')
        i_extval=cp_InsertValODBCExtFlds(this,'COR_RISP')
        local i_cFld
        i_cFld=" "+;
                  "(MDCODVAL,MDCAOVAL,MDSERIAL,MDDATREG,MDORAVEN"+;
                  ",MDMINVEN,MDCODUTE,MDCODESE,MDCODNEG,MDTIPVEN"+;
                  ",MDCODMAG,MDCODFID,MDCODCLI,MDCODLIS,MDNUMSCO"+;
                  ",MDMATSCO,MDFLTRAS,MDANNDOC,MDCODOPE,MDTOTVEN"+;
                  ",MDPUNFID,MDSTOPRO,MDSCOCL1,MDSCOCL2,MDSCOPAG"+;
                  ",MDSCONTI,MDFLFOSC,MDCODPAG,MDIMPPRE,MDTOTDOC"+;
                  ",MDACCPRE,MDPAGCON,MDPAGASS,MDRIFASS,MDPAGCAR"+;
                  ",MDCODCAR,MDPAGFIN,MDCODFIN,MDPAGCLI,MDIMPABB"+;
                  ",MDTIPCHI,MDTIPDOC,MDPRD,MDALFDOC,MDNUMDOC"+;
                  ",MDFLFATT,MDFLSALD,MDRIFDOC,MDRIFCOR,MDCARKIT"+;
                  ",MDSCAKIT,MDTIPCOR,MD_ERROR,UTCC,UTDC"+;
                  ",UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBCNull(this.w_MDCODVAL)+;
                    ","+cp_ToStrODBC(this.w_MDCAOVAL)+;
                    ","+cp_ToStrODBC(this.w_MDSERIAL)+;
                    ","+cp_ToStrODBC(this.w_MDDATREG)+;
                    ","+cp_ToStrODBC(this.w_MDORAVEN)+;
                    ","+cp_ToStrODBC(this.w_MDMINVEN)+;
                    ","+cp_ToStrODBCNull(this.w_MDCODUTE)+;
                    ","+cp_ToStrODBC(this.w_MDCODESE)+;
                    ","+cp_ToStrODBC(this.w_MDCODNEG)+;
                    ","+cp_ToStrODBCNull(this.w_MDTIPVEN)+;
                    ","+cp_ToStrODBCNull(this.w_MDCODMAG)+;
                    ","+cp_ToStrODBCNull(this.w_MDCODFID)+;
                    ","+cp_ToStrODBCNull(this.w_MDCODCLI)+;
                    ","+cp_ToStrODBCNull(this.w_MDCODLIS)+;
                    ","+cp_ToStrODBC(this.w_MDNUMSCO)+;
                    ","+cp_ToStrODBC(this.w_MDMATSCO)+;
                    ","+cp_ToStrODBC(this.w_MDFLTRAS)+;
                    ","+cp_ToStrODBC(this.w_MDANNDOC)+;
                    ","+cp_ToStrODBC(this.w_MDCODOPE)+;
                    ","+cp_ToStrODBC(this.w_MDTOTVEN)+;
                    ","+cp_ToStrODBC(this.w_MDPUNFID)+;
                    ","+cp_ToStrODBC(this.w_MDSTOPRO)+;
                    ","+cp_ToStrODBC(this.w_MDSCOCL1)+;
                    ","+cp_ToStrODBC(this.w_MDSCOCL2)+;
                    ","+cp_ToStrODBC(this.w_MDSCOPAG)+;
                    ","+cp_ToStrODBC(this.w_MDSCONTI)+;
                    ","+cp_ToStrODBC(this.w_MDFLFOSC)+;
                    ","+cp_ToStrODBCNull(this.w_MDCODPAG)+;
                    ","+cp_ToStrODBC(this.w_MDIMPPRE)+;
                    ","+cp_ToStrODBC(this.w_MDTOTDOC)+;
                    ","+cp_ToStrODBC(this.w_MDACCPRE)+;
                    ","+cp_ToStrODBC(this.w_MDPAGCON)+;
                    ","+cp_ToStrODBC(this.w_MDPAGASS)+;
                    ","+cp_ToStrODBC(this.w_MDRIFASS)+;
                    ","+cp_ToStrODBC(this.w_MDPAGCAR)+;
                    ","+cp_ToStrODBCNull(this.w_MDCODCAR)+;
                    ","+cp_ToStrODBC(this.w_MDPAGFIN)+;
                    ","+cp_ToStrODBCNull(this.w_MDCODFIN)+;
                    ","+cp_ToStrODBC(this.w_MDPAGCLI)+;
                    ","+cp_ToStrODBC(this.w_MDIMPABB)+;
                    ","+cp_ToStrODBC(this.w_MDTIPCHI)+;
                    ","+cp_ToStrODBCNull(this.w_MDTIPDOC)+;
                    ","+cp_ToStrODBC(this.w_MDPRD)+;
                    ","+cp_ToStrODBC(this.w_MDALFDOC)+;
                    ","+cp_ToStrODBC(this.w_MDNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_MDFLFATT)+;
                    ","+cp_ToStrODBC(this.w_MDFLSALD)+;
                    ","+cp_ToStrODBC(this.w_MDRIFDOC)+;
                    ","+cp_ToStrODBC(this.w_MDRIFCOR)+;
                    ","+cp_ToStrODBC(this.w_MDCARKIT)+;
                    ","+cp_ToStrODBC(this.w_MDSCAKIT)+;
                    ","+cp_ToStrODBC(this.w_MDTIPCOR)+;
                    ","+cp_ToStrODBC(this.w_MD_ERROR)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COR_RISP')
        i_extval=cp_InsertValVFPExtFlds(this,'COR_RISP')
        cp_CheckDeletedKey(i_cTable,0,'MDSERIAL',this.w_MDSERIAL)
        INSERT INTO (i_cTable);
              (MDCODVAL,MDCAOVAL,MDSERIAL,MDDATREG,MDORAVEN,MDMINVEN,MDCODUTE,MDCODESE,MDCODNEG,MDTIPVEN,MDCODMAG,MDCODFID,MDCODCLI,MDCODLIS,MDNUMSCO,MDMATSCO,MDFLTRAS,MDANNDOC,MDCODOPE,MDTOTVEN,MDPUNFID,MDSTOPRO,MDSCOCL1,MDSCOCL2,MDSCOPAG,MDSCONTI,MDFLFOSC,MDCODPAG,MDIMPPRE,MDTOTDOC,MDACCPRE,MDPAGCON,MDPAGASS,MDRIFASS,MDPAGCAR,MDCODCAR,MDPAGFIN,MDCODFIN,MDPAGCLI,MDIMPABB,MDTIPCHI,MDTIPDOC,MDPRD,MDALFDOC,MDNUMDOC,MDFLFATT,MDFLSALD,MDRIFDOC,MDRIFCOR,MDCARKIT,MDSCAKIT,MDTIPCOR,MD_ERROR,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MDCODVAL;
                  ,this.w_MDCAOVAL;
                  ,this.w_MDSERIAL;
                  ,this.w_MDDATREG;
                  ,this.w_MDORAVEN;
                  ,this.w_MDMINVEN;
                  ,this.w_MDCODUTE;
                  ,this.w_MDCODESE;
                  ,this.w_MDCODNEG;
                  ,this.w_MDTIPVEN;
                  ,this.w_MDCODMAG;
                  ,this.w_MDCODFID;
                  ,this.w_MDCODCLI;
                  ,this.w_MDCODLIS;
                  ,this.w_MDNUMSCO;
                  ,this.w_MDMATSCO;
                  ,this.w_MDFLTRAS;
                  ,this.w_MDANNDOC;
                  ,this.w_MDCODOPE;
                  ,this.w_MDTOTVEN;
                  ,this.w_MDPUNFID;
                  ,this.w_MDSTOPRO;
                  ,this.w_MDSCOCL1;
                  ,this.w_MDSCOCL2;
                  ,this.w_MDSCOPAG;
                  ,this.w_MDSCONTI;
                  ,this.w_MDFLFOSC;
                  ,this.w_MDCODPAG;
                  ,this.w_MDIMPPRE;
                  ,this.w_MDTOTDOC;
                  ,this.w_MDACCPRE;
                  ,this.w_MDPAGCON;
                  ,this.w_MDPAGASS;
                  ,this.w_MDRIFASS;
                  ,this.w_MDPAGCAR;
                  ,this.w_MDCODCAR;
                  ,this.w_MDPAGFIN;
                  ,this.w_MDCODFIN;
                  ,this.w_MDPAGCLI;
                  ,this.w_MDIMPABB;
                  ,this.w_MDTIPCHI;
                  ,this.w_MDTIPDOC;
                  ,this.w_MDPRD;
                  ,this.w_MDALFDOC;
                  ,this.w_MDNUMDOC;
                  ,this.w_MDFLFATT;
                  ,this.w_MDFLSALD;
                  ,this.w_MDRIFDOC;
                  ,this.w_MDRIFCOR;
                  ,this.w_MDCARKIT;
                  ,this.w_MDSCAKIT;
                  ,this.w_MDTIPCOR;
                  ,this.w_MD_ERROR;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CORDRISP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CORDRISP_IDX,2])
      *
      * insert into CORDRISP
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MDSERIAL,MDCODICE,MDDESART,MDCODART,CPROWORD"+;
                  ",MDTIPRIG,MDQTAUM1,MDUNIMIS,MDQTAMOV,MDCONTRA"+;
                  ",MDPREZZO,MDSCONT1,MDFLRESO,MDCODREP,MDCODIVA"+;
                  ",MDSCONT2,MDSCONT3,MDSCONT4,MDFLOMAG,MDFLCASC"+;
                  ",MDMAGSAL,MDSCOVEN,MDSERRIF,MDNUMRIF,MDROWRIF"+;
                  ",MDFLARIF,MDFLERIF,MDQTAIMP,MDQTAIM1,MDCODPRO"+;
                  ",MDFLESCL,MDFLROMA,MDSCOPRO,MDFLULPV,MDVALULT"+;
                  ",MDTIPPRO,MDPERPRO,MDIMPCOM,MDFLLOTT,MDCODLOT"+;
                  ",MDCODUBI,MDCODCOM,MDMAGRIG,MDTIPPR2,MDPROCAP"+;
                  ",MDLOTMAG,MDPROMOZ,MDESPSCO,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MDSERIAL)+","+cp_ToStrODBCNull(this.w_MDCODICE)+","+cp_ToStrODBC(this.w_MDDESART)+","+cp_ToStrODBCNull(this.w_MDCODART)+","+cp_ToStrODBC(this.w_CPROWORD)+;
             ","+cp_ToStrODBC(this.w_MDTIPRIG)+","+cp_ToStrODBC(this.w_MDQTAUM1)+","+cp_ToStrODBCNull(this.w_MDUNIMIS)+","+cp_ToStrODBC(this.w_MDQTAMOV)+","+cp_ToStrODBC(this.w_MDCONTRA)+;
             ","+cp_ToStrODBC(this.w_MDPREZZO)+","+cp_ToStrODBC(this.w_MDSCONT1)+","+cp_ToStrODBC(this.w_MDFLRESO)+","+cp_ToStrODBCNull(this.w_MDCODREP)+","+cp_ToStrODBCNull(this.w_MDCODIVA)+;
             ","+cp_ToStrODBC(this.w_MDSCONT2)+","+cp_ToStrODBC(this.w_MDSCONT3)+","+cp_ToStrODBC(this.w_MDSCONT4)+","+cp_ToStrODBC(this.w_MDFLOMAG)+","+cp_ToStrODBC(this.w_MDFLCASC)+;
             ","+cp_ToStrODBCNull(this.w_MDMAGSAL)+","+cp_ToStrODBC(this.w_MDSCOVEN)+","+cp_ToStrODBC(this.w_MDSERRIF)+","+cp_ToStrODBC(this.w_MDNUMRIF)+","+cp_ToStrODBCNull(this.w_MDROWRIF)+;
             ","+cp_ToStrODBC(this.w_MDFLARIF)+","+cp_ToStrODBC(this.w_MDFLERIF)+","+cp_ToStrODBC(this.w_MDQTAIMP)+","+cp_ToStrODBC(this.w_MDQTAIM1)+","+cp_ToStrODBC(this.w_MDCODPRO)+;
             ","+cp_ToStrODBC(this.w_MDFLESCL)+","+cp_ToStrODBC(this.w_MDFLROMA)+","+cp_ToStrODBC(this.w_MDSCOPRO)+","+cp_ToStrODBC(this.w_MDFLULPV)+","+cp_ToStrODBC(this.w_MDVALULT)+;
             ","+cp_ToStrODBC(this.w_MDTIPPRO)+","+cp_ToStrODBC(this.w_MDPERPRO)+","+cp_ToStrODBC(this.w_MDIMPCOM)+","+cp_ToStrODBC(this.w_MDFLLOTT)+","+cp_ToStrODBCNull(this.w_MDCODLOT)+;
             ","+cp_ToStrODBCNull(this.w_MDCODUBI)+","+cp_ToStrODBCNull(this.w_MDCODCOM)+","+cp_ToStrODBC(this.w_MDMAGRIG)+","+cp_ToStrODBC(this.w_MDTIPPR2)+","+cp_ToStrODBC(this.w_MDPROCAP)+;
             ","+cp_ToStrODBCNull(this.w_MDLOTMAG)+","+cp_ToStrODBC(this.w_MDPROMOZ)+","+cp_ToStrODBC(this.w_MDESPSCO)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MDSERIAL',this.w_MDSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MDSERIAL,this.w_MDCODICE,this.w_MDDESART,this.w_MDCODART,this.w_CPROWORD"+;
                ",this.w_MDTIPRIG,this.w_MDQTAUM1,this.w_MDUNIMIS,this.w_MDQTAMOV,this.w_MDCONTRA"+;
                ",this.w_MDPREZZO,this.w_MDSCONT1,this.w_MDFLRESO,this.w_MDCODREP,this.w_MDCODIVA"+;
                ",this.w_MDSCONT2,this.w_MDSCONT3,this.w_MDSCONT4,this.w_MDFLOMAG,this.w_MDFLCASC"+;
                ",this.w_MDMAGSAL,this.w_MDSCOVEN,this.w_MDSERRIF,this.w_MDNUMRIF,this.w_MDROWRIF"+;
                ",this.w_MDFLARIF,this.w_MDFLERIF,this.w_MDQTAIMP,this.w_MDQTAIM1,this.w_MDCODPRO"+;
                ",this.w_MDFLESCL,this.w_MDFLROMA,this.w_MDSCOPRO,this.w_MDFLULPV,this.w_MDVALULT"+;
                ",this.w_MDTIPPRO,this.w_MDPERPRO,this.w_MDIMPCOM,this.w_MDFLLOTT,this.w_MDCODLOT"+;
                ",this.w_MDCODUBI,this.w_MDCODCOM,this.w_MDMAGRIG,this.w_MDTIPPR2,this.w_MDPROCAP"+;
                ",this.w_MDLOTMAG,this.w_MDPROMOZ,this.w_MDESPSCO,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- gsps_mvd
    ** Forza sempre aggiornamento della Testata
    this.bUpdated=.T.
    
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.COR_RISP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COR_RISP_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update COR_RISP
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'COR_RISP')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MDCODVAL="+cp_ToStrODBCNull(this.w_MDCODVAL)+;
             ",MDCAOVAL="+cp_ToStrODBC(this.w_MDCAOVAL)+;
             ",MDDATREG="+cp_ToStrODBC(this.w_MDDATREG)+;
             ",MDORAVEN="+cp_ToStrODBC(this.w_MDORAVEN)+;
             ",MDMINVEN="+cp_ToStrODBC(this.w_MDMINVEN)+;
             ",MDCODUTE="+cp_ToStrODBCNull(this.w_MDCODUTE)+;
             ",MDCODESE="+cp_ToStrODBC(this.w_MDCODESE)+;
             ",MDCODNEG="+cp_ToStrODBC(this.w_MDCODNEG)+;
             ",MDTIPVEN="+cp_ToStrODBCNull(this.w_MDTIPVEN)+;
             ",MDCODMAG="+cp_ToStrODBCNull(this.w_MDCODMAG)+;
             ",MDCODFID="+cp_ToStrODBCNull(this.w_MDCODFID)+;
             ",MDCODCLI="+cp_ToStrODBCNull(this.w_MDCODCLI)+;
             ",MDCODLIS="+cp_ToStrODBCNull(this.w_MDCODLIS)+;
             ",MDNUMSCO="+cp_ToStrODBC(this.w_MDNUMSCO)+;
             ",MDMATSCO="+cp_ToStrODBC(this.w_MDMATSCO)+;
             ",MDFLTRAS="+cp_ToStrODBC(this.w_MDFLTRAS)+;
             ",MDANNDOC="+cp_ToStrODBC(this.w_MDANNDOC)+;
             ",MDCODOPE="+cp_ToStrODBC(this.w_MDCODOPE)+;
             ",MDTOTVEN="+cp_ToStrODBC(this.w_MDTOTVEN)+;
             ",MDPUNFID="+cp_ToStrODBC(this.w_MDPUNFID)+;
             ",MDSTOPRO="+cp_ToStrODBC(this.w_MDSTOPRO)+;
             ",MDSCOCL1="+cp_ToStrODBC(this.w_MDSCOCL1)+;
             ",MDSCOCL2="+cp_ToStrODBC(this.w_MDSCOCL2)+;
             ",MDSCOPAG="+cp_ToStrODBC(this.w_MDSCOPAG)+;
             ",MDSCONTI="+cp_ToStrODBC(this.w_MDSCONTI)+;
             ",MDFLFOSC="+cp_ToStrODBC(this.w_MDFLFOSC)+;
             ",MDCODPAG="+cp_ToStrODBCNull(this.w_MDCODPAG)+;
             ",MDIMPPRE="+cp_ToStrODBC(this.w_MDIMPPRE)+;
             ",MDTOTDOC="+cp_ToStrODBC(this.w_MDTOTDOC)+;
             ",MDACCPRE="+cp_ToStrODBC(this.w_MDACCPRE)+;
             ",MDPAGCON="+cp_ToStrODBC(this.w_MDPAGCON)+;
             ",MDPAGASS="+cp_ToStrODBC(this.w_MDPAGASS)+;
             ",MDRIFASS="+cp_ToStrODBC(this.w_MDRIFASS)+;
             ",MDPAGCAR="+cp_ToStrODBC(this.w_MDPAGCAR)+;
             ",MDCODCAR="+cp_ToStrODBCNull(this.w_MDCODCAR)+;
             ",MDPAGFIN="+cp_ToStrODBC(this.w_MDPAGFIN)+;
             ",MDCODFIN="+cp_ToStrODBCNull(this.w_MDCODFIN)+;
             ",MDPAGCLI="+cp_ToStrODBC(this.w_MDPAGCLI)+;
             ",MDIMPABB="+cp_ToStrODBC(this.w_MDIMPABB)+;
             ",MDTIPCHI="+cp_ToStrODBC(this.w_MDTIPCHI)+;
             ",MDTIPDOC="+cp_ToStrODBCNull(this.w_MDTIPDOC)+;
             ",MDPRD="+cp_ToStrODBC(this.w_MDPRD)+;
             ",MDALFDOC="+cp_ToStrODBC(this.w_MDALFDOC)+;
             ",MDNUMDOC="+cp_ToStrODBC(this.w_MDNUMDOC)+;
             ",MDFLFATT="+cp_ToStrODBC(this.w_MDFLFATT)+;
             ",MDFLSALD="+cp_ToStrODBC(this.w_MDFLSALD)+;
             ",MDRIFDOC="+cp_ToStrODBC(this.w_MDRIFDOC)+;
             ",MDRIFCOR="+cp_ToStrODBC(this.w_MDRIFCOR)+;
             ",MDCARKIT="+cp_ToStrODBC(this.w_MDCARKIT)+;
             ",MDSCAKIT="+cp_ToStrODBC(this.w_MDSCAKIT)+;
             ",MDTIPCOR="+cp_ToStrODBC(this.w_MDTIPCOR)+;
             ",MD_ERROR="+cp_ToStrODBC(this.w_MD_ERROR)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'COR_RISP')
          i_cWhere = cp_PKFox(i_cTable  ,'MDSERIAL',this.w_MDSERIAL  )
          UPDATE (i_cTable) SET;
              MDCODVAL=this.w_MDCODVAL;
             ,MDCAOVAL=this.w_MDCAOVAL;
             ,MDDATREG=this.w_MDDATREG;
             ,MDORAVEN=this.w_MDORAVEN;
             ,MDMINVEN=this.w_MDMINVEN;
             ,MDCODUTE=this.w_MDCODUTE;
             ,MDCODESE=this.w_MDCODESE;
             ,MDCODNEG=this.w_MDCODNEG;
             ,MDTIPVEN=this.w_MDTIPVEN;
             ,MDCODMAG=this.w_MDCODMAG;
             ,MDCODFID=this.w_MDCODFID;
             ,MDCODCLI=this.w_MDCODCLI;
             ,MDCODLIS=this.w_MDCODLIS;
             ,MDNUMSCO=this.w_MDNUMSCO;
             ,MDMATSCO=this.w_MDMATSCO;
             ,MDFLTRAS=this.w_MDFLTRAS;
             ,MDANNDOC=this.w_MDANNDOC;
             ,MDCODOPE=this.w_MDCODOPE;
             ,MDTOTVEN=this.w_MDTOTVEN;
             ,MDPUNFID=this.w_MDPUNFID;
             ,MDSTOPRO=this.w_MDSTOPRO;
             ,MDSCOCL1=this.w_MDSCOCL1;
             ,MDSCOCL2=this.w_MDSCOCL2;
             ,MDSCOPAG=this.w_MDSCOPAG;
             ,MDSCONTI=this.w_MDSCONTI;
             ,MDFLFOSC=this.w_MDFLFOSC;
             ,MDCODPAG=this.w_MDCODPAG;
             ,MDIMPPRE=this.w_MDIMPPRE;
             ,MDTOTDOC=this.w_MDTOTDOC;
             ,MDACCPRE=this.w_MDACCPRE;
             ,MDPAGCON=this.w_MDPAGCON;
             ,MDPAGASS=this.w_MDPAGASS;
             ,MDRIFASS=this.w_MDRIFASS;
             ,MDPAGCAR=this.w_MDPAGCAR;
             ,MDCODCAR=this.w_MDCODCAR;
             ,MDPAGFIN=this.w_MDPAGFIN;
             ,MDCODFIN=this.w_MDCODFIN;
             ,MDPAGCLI=this.w_MDPAGCLI;
             ,MDIMPABB=this.w_MDIMPABB;
             ,MDTIPCHI=this.w_MDTIPCHI;
             ,MDTIPDOC=this.w_MDTIPDOC;
             ,MDPRD=this.w_MDPRD;
             ,MDALFDOC=this.w_MDALFDOC;
             ,MDNUMDOC=this.w_MDNUMDOC;
             ,MDFLFATT=this.w_MDFLFATT;
             ,MDFLSALD=this.w_MDFLSALD;
             ,MDRIFDOC=this.w_MDRIFDOC;
             ,MDRIFCOR=this.w_MDRIFCOR;
             ,MDCARKIT=this.w_MDCARKIT;
             ,MDSCAKIT=this.w_MDSCAKIT;
             ,MDTIPCOR=this.w_MDTIPCOR;
             ,MD_ERROR=this.w_MD_ERROR;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_MDCODVAL<>MDCODVAL
            i_bUpdAll = .t.
          endif
          if this.w_MDDATREG<>MDDATREG
            i_bUpdAll = .t.
          endif
        endif
        scan for (NOT EMPTY(t_MDCODART)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CORDRISP_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CORDRISP_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSVE_MMT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_MDSERIAL,"MTSERIAL";
                     ,this.w_CPROWNUM,"MTROWNUM";
                     ,this.w_MDNUMRIF,"MTNUMRIF";
                     )
              this.GSVE_MMT.mDelete()
              *
              * delete from CORDRISP
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CORDRISP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MDCODICE="+cp_ToStrODBCNull(this.w_MDCODICE)+;
                     ",MDDESART="+cp_ToStrODBC(this.w_MDDESART)+;
                     ",MDCODART="+cp_ToStrODBCNull(this.w_MDCODART)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MDTIPRIG="+cp_ToStrODBC(this.w_MDTIPRIG)+;
                     ",MDQTAUM1="+cp_ToStrODBC(this.w_MDQTAUM1)+;
                     ",MDUNIMIS="+cp_ToStrODBCNull(this.w_MDUNIMIS)+;
                     ",MDQTAMOV="+cp_ToStrODBC(this.w_MDQTAMOV)+;
                     ",MDCONTRA="+cp_ToStrODBC(this.w_MDCONTRA)+;
                     ",MDPREZZO="+cp_ToStrODBC(this.w_MDPREZZO)+;
                     ",MDSCONT1="+cp_ToStrODBC(this.w_MDSCONT1)+;
                     ",MDFLRESO="+cp_ToStrODBC(this.w_MDFLRESO)+;
                     ",MDCODREP="+cp_ToStrODBCNull(this.w_MDCODREP)+;
                     ",MDCODIVA="+cp_ToStrODBCNull(this.w_MDCODIVA)+;
                     ",MDSCONT2="+cp_ToStrODBC(this.w_MDSCONT2)+;
                     ",MDSCONT3="+cp_ToStrODBC(this.w_MDSCONT3)+;
                     ",MDSCONT4="+cp_ToStrODBC(this.w_MDSCONT4)+;
                     ",MDFLOMAG="+cp_ToStrODBC(this.w_MDFLOMAG)+;
                     ",MDFLCASC="+cp_ToStrODBC(this.w_MDFLCASC)+;
                     ",MDMAGSAL="+cp_ToStrODBCNull(this.w_MDMAGSAL)+;
                     ",MDSCOVEN="+cp_ToStrODBC(this.w_MDSCOVEN)+;
                     ",MDSERRIF="+cp_ToStrODBC(this.w_MDSERRIF)+;
                     ",MDNUMRIF="+cp_ToStrODBC(this.w_MDNUMRIF)+;
                     ",MDROWRIF="+cp_ToStrODBCNull(this.w_MDROWRIF)+;
                     ",MDFLARIF="+cp_ToStrODBC(this.w_MDFLARIF)+;
                     ",MDFLERIF="+cp_ToStrODBC(this.w_MDFLERIF)+;
                     ",MDQTAIMP="+cp_ToStrODBC(this.w_MDQTAIMP)+;
                     ",MDQTAIM1="+cp_ToStrODBC(this.w_MDQTAIM1)+;
                     ",MDCODPRO="+cp_ToStrODBC(this.w_MDCODPRO)+;
                     ",MDFLESCL="+cp_ToStrODBC(this.w_MDFLESCL)+;
                     ",MDFLROMA="+cp_ToStrODBC(this.w_MDFLROMA)+;
                     ",MDSCOPRO="+cp_ToStrODBC(this.w_MDSCOPRO)+;
                     ",MDFLULPV="+cp_ToStrODBC(this.w_MDFLULPV)+;
                     ",MDVALULT="+cp_ToStrODBC(this.w_MDVALULT)+;
                     ",MDTIPPRO="+cp_ToStrODBC(this.w_MDTIPPRO)+;
                     ",MDPERPRO="+cp_ToStrODBC(this.w_MDPERPRO)+;
                     ",MDIMPCOM="+cp_ToStrODBC(this.w_MDIMPCOM)+;
                     ",MDFLLOTT="+cp_ToStrODBC(this.w_MDFLLOTT)+;
                     ",MDCODLOT="+cp_ToStrODBCNull(this.w_MDCODLOT)+;
                     ",MDCODUBI="+cp_ToStrODBCNull(this.w_MDCODUBI)+;
                     ",MDCODCOM="+cp_ToStrODBCNull(this.w_MDCODCOM)+;
                     ",MDMAGRIG="+cp_ToStrODBC(this.w_MDMAGRIG)+;
                     ",MDTIPPR2="+cp_ToStrODBC(this.w_MDTIPPR2)+;
                     ",MDPROCAP="+cp_ToStrODBC(this.w_MDPROCAP)+;
                     ",MDLOTMAG="+cp_ToStrODBCNull(this.w_MDLOTMAG)+;
                     ",MDPROMOZ="+cp_ToStrODBC(this.w_MDPROMOZ)+;
                     ",MDESPSCO="+cp_ToStrODBC(this.w_MDESPSCO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MDCODICE=this.w_MDCODICE;
                     ,MDDESART=this.w_MDDESART;
                     ,MDCODART=this.w_MDCODART;
                     ,CPROWORD=this.w_CPROWORD;
                     ,MDTIPRIG=this.w_MDTIPRIG;
                     ,MDQTAUM1=this.w_MDQTAUM1;
                     ,MDUNIMIS=this.w_MDUNIMIS;
                     ,MDQTAMOV=this.w_MDQTAMOV;
                     ,MDCONTRA=this.w_MDCONTRA;
                     ,MDPREZZO=this.w_MDPREZZO;
                     ,MDSCONT1=this.w_MDSCONT1;
                     ,MDFLRESO=this.w_MDFLRESO;
                     ,MDCODREP=this.w_MDCODREP;
                     ,MDCODIVA=this.w_MDCODIVA;
                     ,MDSCONT2=this.w_MDSCONT2;
                     ,MDSCONT3=this.w_MDSCONT3;
                     ,MDSCONT4=this.w_MDSCONT4;
                     ,MDFLOMAG=this.w_MDFLOMAG;
                     ,MDFLCASC=this.w_MDFLCASC;
                     ,MDMAGSAL=this.w_MDMAGSAL;
                     ,MDSCOVEN=this.w_MDSCOVEN;
                     ,MDSERRIF=this.w_MDSERRIF;
                     ,MDNUMRIF=this.w_MDNUMRIF;
                     ,MDROWRIF=this.w_MDROWRIF;
                     ,MDFLARIF=this.w_MDFLARIF;
                     ,MDFLERIF=this.w_MDFLERIF;
                     ,MDQTAIMP=this.w_MDQTAIMP;
                     ,MDQTAIM1=this.w_MDQTAIM1;
                     ,MDCODPRO=this.w_MDCODPRO;
                     ,MDFLESCL=this.w_MDFLESCL;
                     ,MDFLROMA=this.w_MDFLROMA;
                     ,MDSCOPRO=this.w_MDSCOPRO;
                     ,MDFLULPV=this.w_MDFLULPV;
                     ,MDVALULT=this.w_MDVALULT;
                     ,MDTIPPRO=this.w_MDTIPPRO;
                     ,MDPERPRO=this.w_MDPERPRO;
                     ,MDIMPCOM=this.w_MDIMPCOM;
                     ,MDFLLOTT=this.w_MDFLLOTT;
                     ,MDCODLOT=this.w_MDCODLOT;
                     ,MDCODUBI=this.w_MDCODUBI;
                     ,MDCODCOM=this.w_MDCODCOM;
                     ,MDMAGRIG=this.w_MDMAGRIG;
                     ,MDTIPPR2=this.w_MDTIPPR2;
                     ,MDPROCAP=this.w_MDPROCAP;
                     ,MDLOTMAG=this.w_MDLOTMAG;
                     ,MDPROMOZ=this.w_MDPROMOZ;
                     ,MDESPSCO=this.w_MDESPSCO;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (NOT EMPTY(t_MDCODART))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSVE_MMT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_MDSERIAL,"MTSERIAL";
             ,this.w_CPROWNUM,"MTROWNUM";
             ,this.w_MDNUMRIF,"MTNUMRIF";
             )
        this.GSVE_MMT.mReplace()
        this.GSVE_MMT.bSaveContext=.f.
      endscan
     this.GSVE_MMT.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsps_mvd
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        this.SaveDependsOn()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * --- Riposiziona sul Primo record del Temporaneo dei Documenti
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        this.SaveDependsOn()
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4,i_cOp5,i_cOp6

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MDFLCASC,space(1))==this.w_MDFLCASC;
              and NVL(&i_cF..MDQTAUM1,0)==this.w_MDQTAUM1;
              and NVL(&i_cF..MDFLULPV,space(1))==this.w_MDFLULPV;
              and NVL(&i_cF..MDDATREG,ctod("  /  /  "))==this.w_MDDATREG;
              and NVL(&i_cF..MDFLULPV,space(1))==this.w_MDFLULPV;
              and NVL(&i_cF..MDVALULT,0)==this.w_MDVALULT;
              and NVL(&i_cF..MDFLULPV,space(1))==this.w_MDFLULPV;
              and NVL(&i_cF..MDCODVAL,space(3))==this.w_MDCODVAL;
              and NVL(&i_cF..MDMAGSAL,space(5))==this.w_MDMAGSAL;
              and NVL(&i_cF..MDCODART,space(20))==this.w_MDCODART;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MDFLCASC,space(1)),'SLQTAPER','',NVL(&i_cF..MDQTAUM1,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..MDFLULPV,space(1)),'SLDATUPV','',NVL(&i_cF..MDDATREG,ctod("  /  /  ")),'restore',i_nConn)
      i_cOp3=cp_SetTrsOp(NVL(&i_cF..MDFLULPV,space(1)),'SLVALUPV','',NVL(&i_cF..MDVALULT,0),'restore',i_nConn)
      i_cOp4=cp_SetTrsOp(NVL(&i_cF..MDFLULPV,space(1)),'SLCODVAV','',NVL(&i_cF..MDCODVAL,space(3)),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MDMAGSAL,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SLQTAPER="+i_cOp1+","           +" SLDATUPV="+i_cOp2+","           +" SLVALUPV="+i_cOp3+","           +" SLCODVAV="+i_cOp4+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODMAG="+cp_ToStrODBC(NVL(&i_cF..MDMAGSAL,space(5)));
             +" AND SLCODICE="+cp_ToStrODBC(NVL(&i_cF..MDCODART,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MDFLCASC,'SLQTAPER',i_cF+'.MDQTAUM1',&i_cF..MDQTAUM1,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..MDFLULPV,'SLDATUPV',i_cF+'.MDDATREG',&i_cF..MDDATREG,'restore',0)
      i_cOp3=cp_SetTrsOp(&i_cF..MDFLULPV,'SLVALUPV',i_cF+'.MDVALULT',&i_cF..MDVALULT,'restore',0)
      i_cOp4=cp_SetTrsOp(&i_cF..MDFLULPV,'SLCODVAV',i_cF+'.MDCODVAL',&i_cF..MDCODVAL,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLCODICE',&i_cF..MDCODART;
                 ,'SLCODMAG',&i_cF..MDMAGSAL)
      UPDATE (i_cTable) SET ;
           SLQTAPER=&i_cOp1.  ,;
           SLDATUPV=&i_cOp2.  ,;
           SLVALUPV=&i_cOp3.  ,;
           SLCODVAV=&i_cOp4.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MDFLCASC,space(1))==this.w_MDFLCASC;
              and NVL(&i_cF..MDQTAUM1,0)==this.w_MDQTAUM1;
              and NVL(&i_cF..MDLOTMAG,space(5))==this.w_MDLOTMAG;
              and NVL(&i_cF..MDCODART,space(20))==this.w_MDCODART;
              and NVL(&i_cF..MDCODUBI,space(20))==this.w_MDCODUBI;
              and NVL(&i_cF..MDCODLOT,space(20))==this.w_MDCODLOT;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MDFLCASC,space(1)),'SUQTAPER','',NVL(&i_cF..MDQTAUM1,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MDLOTMAG,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SUQTAPER="+i_cOp1+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SUCODMAG="+cp_ToStrODBC(NVL(&i_cF..MDLOTMAG,space(5)));
             +" AND SUCODART="+cp_ToStrODBC(NVL(&i_cF..MDCODART,space(20)));
             +" AND SUCODUBI="+cp_ToStrODBC(NVL(&i_cF..MDCODUBI,space(20)));
             +" AND SUCODLOT="+cp_ToStrODBC(NVL(&i_cF..MDCODLOT,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MDFLCASC,'SUQTAPER',i_cF+'.MDQTAUM1',&i_cF..MDQTAUM1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SUCODART',&i_cF..MDCODART;
                 ,'SUCODUBI',&i_cF..MDCODUBI;
                 ,'SUCODLOT',&i_cF..MDCODLOT;
                 ,'SUCODMAG',&i_cF..MDLOTMAG)
      UPDATE (i_cTable) SET ;
           SUQTAPER=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4,i_cOp5,i_cOp6

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MDFLCASC,space(1))==this.w_MDFLCASC;
              and NVL(&i_cF..MDQTAUM1,0)==this.w_MDQTAUM1;
              and NVL(&i_cF..MDFLULPV,space(1))==this.w_MDFLULPV;
              and NVL(&i_cF..MDDATREG,ctod("  /  /  "))==this.w_MDDATREG;
              and NVL(&i_cF..MDFLULPV,space(1))==this.w_MDFLULPV;
              and NVL(&i_cF..MDVALULT,0)==this.w_MDVALULT;
              and NVL(&i_cF..MDFLULPV,space(1))==this.w_MDFLULPV;
              and NVL(&i_cF..MDCODVAL,space(3))==this.w_MDCODVAL;
              and NVL(&i_cF..MDMAGSAL,space(5))==this.w_MDMAGSAL;
              and NVL(&i_cF..MDCODART,space(20))==this.w_MDCODART;

      i_cOp1=cp_SetTrsOp(this.w_MDFLCASC,'SLQTAPER','this.w_MDQTAUM1',this.w_MDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MDFLULPV,'SLDATUPV','this.w_MDDATREG',this.w_MDDATREG,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MDFLULPV,'SLVALUPV','this.w_MDVALULT',this.w_MDVALULT,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MDFLULPV,'SLCODVAV','this.w_MDCODVAL',this.w_MDCODVAL,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MDMAGSAL)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SLQTAPER="+i_cOp1  +",";
         +" SLDATUPV="+i_cOp2  +",";
         +" SLVALUPV="+i_cOp3  +",";
         +" SLCODVAV="+i_cOp4  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODMAG="+cp_ToStrODBC(this.w_MDMAGSAL);
           +" AND SLCODICE="+cp_ToStrODBC(this.w_MDCODART);
           )
        if i_nModRow<1 .and. .not. empty(this.w_MDMAGSAL)
        i_cOp1=cp_SetTrsOp(this.w_MDFLCASC,'SLQTAPER','this.w_MDQTAUM1',this.w_MDQTAUM1,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MDFLULPV,'SLDATUPV','this.w_MDDATREG',this.w_MDDATREG,'insert',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MDFLULPV,'SLVALUPV','this.w_MDVALULT',this.w_MDVALULT,'insert',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MDFLULPV,'SLCODVAV','this.w_MDCODVAL',this.w_MDCODVAL,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SLCODMAG,SLCODICE  ;
             ,SLQTAPER"+",SLDATUPV"+",SLVALUPV"+",SLCODVAV"+",UTCV"+",UTDV,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_MDMAGSAL)+","+cp_ToStrODBC(this.w_MDCODART)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+i_cOp3;
             +","+i_cOp4;
             +","+cp_ToStrODBC(i_codute);
             +","+cp_ToStrODBC(SetInfoDate(This.cCalUtd));
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MDFLCASC,'SLQTAPER','this.w_MDQTAUM1',this.w_MDQTAUM1,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_MDFLULPV,'SLDATUPV','this.w_MDDATREG',this.w_MDDATREG,'update',0)
      i_cOp3=cp_SetTrsOp(this.w_MDFLULPV,'SLVALUPV','this.w_MDVALULT',this.w_MDVALULT,'update',0)
      i_cOp4=cp_SetTrsOp(this.w_MDFLULPV,'SLCODVAV','this.w_MDCODVAL',this.w_MDCODVAL,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLCODICE',this.w_MDCODART;
                 ,'SLCODMAG',this.w_MDMAGSAL)
      UPDATE (i_cTable) SET;
           SLQTAPER=&i_cOp1.  ,;
           SLDATUPV=&i_cOp2.  ,;
           SLVALUPV=&i_cOp3.  ,;
           SLCODVAV=&i_cOp4.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_MDMAGSAL)
        i_cOp1=cp_SetTrsOp(this.w_MDFLCASC,'SLQTAPER','this.w_MDQTAUM1',this.w_MDQTAUM1,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_MDFLULPV,'SLDATUPV','this.w_MDDATREG',this.w_MDDATREG,'insert',0)
        i_cOp3=cp_SetTrsOp(this.w_MDFLULPV,'SLVALUPV','this.w_MDVALULT',this.w_MDVALULT,'insert',0)
        i_cOp4=cp_SetTrsOp(this.w_MDFLULPV,'SLCODVAV','this.w_MDCODVAL',this.w_MDCODVAL,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MDCODART,'SLCODMAG',this.w_MDMAGSAL)
        INSERT INTO (i_cTable) (SLCODMAG,SLCODICE  ;
         ,SLQTAPER,SLDATUPV,SLVALUPV,SLCODVAV,UTCV,UTDV,CPCCCHK) VALUES (this.w_MDMAGSAL,this.w_MDCODART  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ;
           ,&i_cOp3.  ;
           ,&i_cOp4.  ;
           ,i_codute  ;
           ,SetInfoDate(This.cCalUtd)  ,cp_NewCCChk())
      endif
    endif
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MDFLCASC,space(1))==this.w_MDFLCASC;
              and NVL(&i_cF..MDQTAUM1,0)==this.w_MDQTAUM1;
              and NVL(&i_cF..MDLOTMAG,space(5))==this.w_MDLOTMAG;
              and NVL(&i_cF..MDCODART,space(20))==this.w_MDCODART;
              and NVL(&i_cF..MDCODUBI,space(20))==this.w_MDCODUBI;
              and NVL(&i_cF..MDCODLOT,space(20))==this.w_MDCODLOT;

      i_cOp1=cp_SetTrsOp(this.w_MDFLCASC,'SUQTAPER','this.w_MDQTAUM1',this.w_MDQTAUM1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MDLOTMAG)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SUQTAPER="+i_cOp1  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SUCODMAG="+cp_ToStrODBC(this.w_MDLOTMAG);
           +" AND SUCODART="+cp_ToStrODBC(this.w_MDCODART);
           +" AND SUCODUBI="+cp_ToStrODBC(this.w_MDCODUBI);
           +" AND SUCODLOT="+cp_ToStrODBC(this.w_MDCODLOT);
           )
        if i_nModRow<1 .and. .not. empty(this.w_MDLOTMAG)
        i_cOp1=cp_SetTrsOp(this.w_MDFLCASC,'SUQTAPER','this.w_MDQTAUM1',this.w_MDQTAUM1,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SUCODMAG,SUCODART,SUCODUBI,SUCODLOT  ;
             ,SUQTAPER"+",UTCV"+",UTDV,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_MDLOTMAG)+","+cp_ToStrODBC(this.w_MDCODART)+","+cp_ToStrODBC(this.w_MDCODUBI)+","+cp_ToStrODBC(this.w_MDCODLOT)  ;
             +","+i_cOp1;
             +","+cp_ToStrODBC(i_codute);
             +","+cp_ToStrODBC(SetInfoDate(This.cCalUtd));
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MDFLCASC,'SUQTAPER','this.w_MDQTAUM1',this.w_MDQTAUM1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SUCODART',this.w_MDCODART;
                 ,'SUCODUBI',this.w_MDCODUBI;
                 ,'SUCODLOT',this.w_MDCODLOT;
                 ,'SUCODMAG',this.w_MDLOTMAG)
      UPDATE (i_cTable) SET;
           SUQTAPER=&i_cOp1.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_MDLOTMAG)
        i_cOp1=cp_SetTrsOp(this.w_MDFLCASC,'SUQTAPER','this.w_MDQTAUM1',this.w_MDQTAUM1,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SUCODART',this.w_MDCODART,'SUCODUBI',this.w_MDCODUBI,'SUCODLOT',this.w_MDCODLOT,'SUCODMAG',this.w_MDLOTMAG)
        INSERT INTO (i_cTable) (SUCODMAG,SUCODART,SUCODUBI,SUCODLOT  ;
         ,SUQTAPER,UTCV,UTDV,CPCCCHK) VALUES (this.w_MDLOTMAG,this.w_MDCODART,this.w_MDCODUBI,this.w_MDCODLOT  ;
           ,&i_cOp1.  ;
           ,i_codute  ;
           ,SetInfoDate(This.cCalUtd)  ,cp_NewCCChk())
      endif
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_MDCODART)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSVE_MMT : Deleting
        this.GSVE_MMT.bSaveContext=.f.
        this.GSVE_MMT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MDSERIAL,"MTSERIAL";
               ,this.w_CPROWNUM,"MTROWNUM";
               ,this.w_MDNUMRIF,"MTNUMRIF";
               )
        this.GSVE_MMT.bSaveContext=.t.
        this.GSVE_MMT.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CORDRISP_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CORDRISP_IDX,2])
        *
        * delete CORDRISP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.COR_RISP_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.COR_RISP_IDX,2])
        *
        * delete COR_RISP
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_MDCODART)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsps_mvd
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * --- Riposiziona sul Primo record del Temporaneo dei Documenti
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        if not(bTrsErr)
            select (this.cTrsName)
            go top
            this.WorkFromTrs()
        endif
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COR_RISP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COR_RISP_IDX,2])
    if i_bUpd
      this.bHeaderUpdated=.t. && Totalized fields
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .w_CODAZI = i_CODAZI
          .w_DECUNI = g_PERPUL
          .w_DECTOT = g_PERPVL
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
          .link_1_8('Full')
        .DoRTCalc(9,14,.t.)
          .w_MDCODESE = g_CODESE
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_MDCODNEG = g_CODNEG
        endif
        .DoRTCalc(17,25,.t.)
        if .o_MDTIPVEN<>.w_MDTIPVEN
          .w_READPAR = .w_MDCODNEG
          .link_1_31('Full')
        endif
        .DoRTCalc(27,31,.t.)
        if .o_MDCODFID<>.w_MDCODFID.or. .o_CLIVEN<>.w_CLIVEN
          .w_MDCODCLI = IIF(EMPTY(.w_MDCODFID), .w_CLIVEN, .w_CLIFID)
          .link_1_41('Full')
        endif
        .DoRTCalc(33,35,.t.)
        if .o_MATSCO<>.w_MATSCO
          .w_MDMATSCO = IIF(.cFunction='Load',.w_MATSCO,.w_MDMATSCO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(IIF(.w_MDFLTRAS='S', '*** Vendita Trasferita', ''))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(37,42,.t.)
        if .o_MDDATREG<>.w_MDDATREG
          .w_MDANNDOC = STR(YEAR(.w_MDDATREG), 4, 0)
        endif
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .DoRTCalc(44,75,.t.)
          .w_OBTEST = .w_MDDATREG
        .DoRTCalc(77,77,.t.)
          .link_1_102('Full')
        if .o_MDTIPDOC<>.w_MDTIPDOC.or. .o_MDTIPCOR<>.w_MDTIPCOR.or. .o_MDCODLIS<>.w_MDCODLIS
          .w_NULLA = ' '
        endif
          .link_1_104('Full')
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .DoRTCalc(81,102,.t.)
          .w_FLPRG = 'V'
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .DoRTCalc(104,104,.t.)
          .w_TESLOT = .F.
        .oPgFrm.Page1.oPag.oObj_1_148.Calculate()
        .DoRTCalc(106,107,.t.)
          .w_READHARD = IIF( TYPE('g_CODCAS')='C',g_CODCAS, SPACE(5))
          .link_1_149('Full')
        .DoRTCalc(109,111,.t.)
        if .o_MDCODICE<>.w_MDCODICE
          .w_DA1 = IIF(.cFunction='Load', .w_FLGDES, ' ')
        endif
        if .o_MDCODICE<>.w_MDCODICE
          .w_UM1 = IIF(.cFunction='Load', .w_FLUNIM, ' ')
        endif
        if .o_MDCODICE<>.w_MDCODICE
          .w_QT1 = IIF(.cFunction='Load', iif(.w_QTADEF<>0,'S',' '), ' ')
        endif
        if .o_MDCODICE<>.w_MDCODICE
          .w_PZ1 = IIF(.cFunction='Load', .w_FLPREZ, ' ')
        endif
        if .o_MDCODICE<>.w_MDCODICE
          .w_SC1 = IIF(.cFunction='Load', .w_FLSCON, ' ')
        endif
        .DoRTCalc(117,118,.t.)
        if .o_MDCODICE<>.w_MDCODICE
          .link_2_8('Full')
        endif
        .DoRTCalc(120,122,.t.)
          .link_2_12('Full')
        .DoRTCalc(124,129,.t.)
          .w_MDQTAUM1 = IIF(.w_MDTIPRIG='F', 1, CALQTA(.w_MDQTAMOV,.w_MDUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_UNISEP, .w_NOFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        if .o_MDCODICE<>.w_MDCODICE
          .w_MDUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_2_20('Full')
        endif
        .DoRTCalc(132,132,.t.)
        if .o_MDCODICE<>.w_MDCODICE
          .w_MDQTAMOV = IIF(.w_MDQTAMOV=0 and not empty(.w_MDCODICE) And Not .w_NOCALQTA,IIF(.w_MDTIPRIG='F', 1, IIF(.w_MDTIPRIG='D', 0, .w_QTADEF)),.w_MDQTAMOV)
        endif
        .DoRTCalc(134,135,.t.)
        if .o_LIPREZZO<>.w_LIPREZZO
          .w_MDPREZZO = IIF((.w_MDTIPRIG $ 'RFMA'), CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MDUNIMIS+.w_OPERAT+.w_OPERA3+IIF(.w_LISCON=2, .w_LISIVA, 'N')+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1), 0), 0)
        endif
        .DoRTCalc(137,138,.t.)
        if .o_MDCODICE<>.w_MDCODICE
          .link_2_28('Full')
        endif
        if .o_MDCODICE<>.w_MDCODICE.or. .o_MDCODREP<>.w_MDCODREP
          .link_2_29('Full')
        endif
        if .o_MDSCONT1<>.w_MDSCONT1
          .w_MDSCONT2 = 0
        endif
        if .o_MDSCONT2<>.w_MDSCONT2
          .w_MDSCONT3 = 0
        endif
        if .o_MDSCONT3<>.w_MDSCONT3
          .w_MDSCONT4 = 0
        endif
        .DoRTCalc(144,144,.t.)
        if .o_MDFLRESO<>.w_MDFLRESO
          .w_MDFLCASC = IIF(.w_MDFLRESO='S', '+', '-')
        endif
        if .o_MDCODMAG<>.w_MDCODMAG.or. .o_MDCODICE<>.w_MDCODICE.or. .o_MDMAGSAL<>.w_MDMAGSAL
          .w_MDMAGSAL = IIF(.w_MDTIPRIG='R' AND .w_MDFLCASC<>' ', .w_MDCODMAG, SPACE(5))
          .link_2_35('Full')
        endif
        .DoRTCalc(147,148,.t.)
          .w_ARCODART = .w_MDCODART
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_46.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_47.Calculate()
        .DoRTCalc(150,151,.t.)
        if .o_MDCODART<>.w_MDCODART
          .w_OFLSCO = ' '
        endif
        if .o_VALRIG<>.w_VALRIG
          .w_IMPNAZ = IIF(.w_VALRIG=0 Or .w_MDQTAUM1=0,0,cp_ROUND(.w_VALRIG/.w_MDQTAUM1,.w_DECTOT))
        endif
        if .o_MDQTAMOV<>.w_MDQTAMOV.or. .o_MDPREZZO<>.w_MDPREZZO.or. .o_MDSCONT1<>.w_MDSCONT1.or. .o_MDSCONT2<>.w_MDSCONT2.or. .o_MDSCONT3<>.w_MDSCONT3.or. .o_MDSCONT4<>.w_MDSCONT4
          .w_TOTALE = .w_TOTALE-.w_valrig
          .w_VALRIG = IIF(g_FLESSC='S' And .w_MDDATREG >= g_DATESC,CAVALRIG(.w_MDPREZZO,.w_MDQTAMOV, 0, 0, 0, 0,.w_DECTOT), CAVALRIG(.w_MDPREZZO,.w_MDQTAMOV, .w_MDSCONT1,.w_MDSCONT2,.w_MDSCONT3,.w_MDSCONT4,.w_DECTOT) )
          .w_TOTALE = .w_TOTALE+.w_valrig
        endif
          .w_MDTOTVEN = .w_MDTOTVEN-.w_rignet
          .w_RIGNET = IIF(.w_MDCODICE = .w_SERFID And .w_FLFAFI = 'S', 0, IIF(.w_MDFLOMAG='X', .w_VALRIG,IIF(.w_MDFLOMAG='I', .w_VALRIG - CALNET(.w_VALRIG,.w_PERIVA,.w_DECTOT,Space(3),0),0) ) )
          .w_MDTOTVEN = .w_MDTOTVEN+.w_rignet
        .DoRTCalc(156,157,.t.)
          .w_MDNUMRIF = -30
          .link_2_56('Full')
        .DoRTCalc(160,174,.t.)
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_OQTAIMP = .w_MDQTAIMP
        endif
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_OQTAIM1 = .w_MDQTAIM1
        endif
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_OFLERIF = .w_MDFLERIF
        endif
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_OPREZZO = .w_MDPREZZO
        endif
        .DoRTCalc(179,179,.t.)
          .w_QTDISP = .w_QTAPER-.w_QTRPER
        .oPgFrm.Page1.oPag.oObj_2_78.Calculate()
          .w_MDFLULPV = IIF(.w_MDFLCASC='-' AND .w_MDDATREG>=.w_ULTSCA AND .w_MDFLOMAG<>'S', '=', ' ')
        if .o_PERIVA<>.w_PERIVA.or. .o_VALRIG<>.w_VALRIG
          .w_MDVALULT = CALNET(.w_IMPNAZ,.w_PERIVA,.w_DECTOT,SPACE(5), 0)
        endif
        .oPgFrm.Page1.oPag.oObj_2_81.Calculate()
        .DoRTCalc(183,183,.t.)
        if .o_MDFLOMAG<>.w_MDFLOMAG
          .w_MDTIPPRO = IIF(.w_MDFLOMAG='X','DC','ES')
        endif
        .oPgFrm.Page1.oPag.oObj_2_86.Calculate(IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)<>'#########' AND NOT EMPTY(.w_MDCODPRO),'Sc. Promozione:',IIF(.w_MDTIPRIG<>'D','Sc. Ventilato:',IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)='#########' AND NOT EMPTY(.w_MDCODPRO),'Sconto:',''))))
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))=.T., 'Commessa:',''))
        .DoRTCalc(185,188,.t.)
          .w_OCOM = IIF(.w_MDTIPRIG<>'D', .w_MDCODCOM, .w_OCOM)
        if .o_MDCODCOM<>.w_MDCODCOM
          .w_COCODVAL = IIF(EMPTY(.w_COCODVAL), g_PERVAL, .w_COCODVAL)
          .link_2_91('Full')
        endif
        .DoRTCalc(191,191,.t.)
          .w_MDIMPCOM = CAIMPCOM( IIF(Empty(.w_MDCODCOM),'S', ' '), g_PERVAL, .w_COCODVAL, .w_VALRIG, g_CAOEUR, .w_MDDATREG, .w_DECCOM )
        .DoRTCalc(193,196,.t.)
          .w_MDFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_MDFLCASC),1),' ')
        .DoRTCalc(198,201,.t.)
        if .o_MDCODICE<>.w_MDCODICE.or. .o_MDTIPCHI<>.w_MDTIPCHI.or. .o_MDTIPDOC<>.w_MDTIPDOC
          .w_MDCODCOM = IIF(.w_MDTIPRIG<>'D' And .w_FLANAL='S' And .w_FLGCOM='S', .w_OCOM, SPACE(15))
          .link_2_104('Full')
        endif
        .DoRTCalc(203,204,.t.)
        if .o_MDCODMAG<>.w_MDCODMAG.or. .o_MDCODICE<>.w_MDCODICE
          .w_MDMAGRIG = IIF(.w_MDTIPRIG='R',.w_MDCODMAG,Space(20) )
        endif
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_OLDQTA = .w_MDQTAUM1
        endif
        .DoRTCalc(207,207,.t.)
          .w_FLDISP = IIF(g_PERDIS='S', .w_ARTDIS, ' ')
        .DoRTCalc(209,212,.t.)
        if .o_MDFLOMAG<>.w_MDFLOMAG
          .w_MDTIPPR2 = IIF(.w_MDFLOMAG='X','DC','ES')
        endif
        .DoRTCalc(214,214,.t.)
        if .o_MDMAGSAL<>.w_MDMAGSAL.or. .o_MDCODART<>.w_MDCODART.or. .o_MDCODLOT<>.w_MDCODLOT.or. .o_MDCODUBI<>.w_MDCODUBI
          .w_MDLOTMAG = iif( Empty( .w_MDCODLOT ) And Empty( .w_MDCODUBI ) , SPACE(5) , LEFT(.w_MDMAGSAL,5) )
          .link_2_117('Full')
        endif
        .DoRTCalc(216,227,.t.)
        if .o_MDSCOCL1<>.w_MDSCOCL1
          .w_MDSCOCL2 = IIF(.w_MDSCOCL1=0, 0, .w_MDSCOCL2)
        endif
        if .o_MDCODPAG<>.w_MDCODPAG
          .w_MDSCOPAG = .w_SCOPAG
        endif
        Local l_Dep1,l_Dep2
        l_Dep1= .o_MDSCOCL1<>.w_MDSCOCL1 .or. .o_MDSCOCL2<>.w_MDSCOCL2 .or. .o_MDSCOPAG<>.w_MDSCOPAG .or. .o_MDCODPAG<>.w_MDCODPAG .or. .o_MDCODCLI<>.w_MDCODCLI        l_Dep2= .o_MDTOTVEN<>.w_MDTOTVEN .or. .o_MDFLFOSC<>.w_MDFLFOSC
        if m.l_Dep1 .or. m.l_Dep2
          .w_MDSCONTI = cp_Round( IIF(.w_MDFLFOSC<>'S', - (.w_MDTOTVEN + .w_MDSTOPRO) * cp_Round((1 - (1+.w_MDSCOCL1/100)*(1+.w_MDSCOCL2/100)*(1+.w_MDSCOPAG/100)), 10) ,.w_MDSCONTI) , .w_DECTOT )
        endif
        .DoRTCalc(231,233,.t.)
        if .o_MDSCONTI<>.w_MDSCONTI.or. .o_MDSTOPRO<>.w_MDSTOPRO.or. .o_MDTOTDOC<>.w_MDTOTDOC.or. .o_MDCODFID<>.w_MDCODFID
          .w_MDIMPPRE = IIF(.w_FLFAFI ='S', .w_MDIMPPRE, IIF(NOT EMPTY(.w_MDCODFID) AND .w_MDTOTDOC<>0,IIF(.w_FIMPRES>0,MIN(.w_FIMPRES, ( .w_MDTOTVEN + .w_MDSTOPRO + .w_MDSCONTI ) ),0),0) )
        endif
          .w_MDTOTDOC = cp_ROUND(.w_MDTOTVEN + (.w_MDSCONTI+ .w_MDSTOPRO - IIF(.w_FLFAFI ='S' And Not Empty(.w_SERFID),.w_MDIMPPRE,0)), .w_DECTOT+1)
        .DoRTCalc(236,238,.t.)
        if .o_MDPAGASS<>.w_MDPAGASS
          .w_MDRIFASS = IIF(.w_MDPAGASS=0,' ',.w_MDRIFASS)
        endif
        .DoRTCalc(240,240,.t.)
        if .o_MDPAGCAR<>.w_MDPAGCAR
          .w_MDCODCAR = IIF(.w_MDPAGCAR=0,'',.w_MDCODCAR)
          .link_4_19('Full')
        endif
        .DoRTCalc(242,243,.t.)
        if .o_MDPAGFIN<>.w_MDPAGFIN
          .w_MDCODFIN = IIF(.w_MDPAGFIN=0,'',.w_MDCODFIN)
          .link_4_22('Full')
        endif
        .DoRTCalc(245,248,.t.)
        if .o_MDCODFID<>.w_MDCODFID
          .w_MDCODCLI = IIF(EMPTY(.w_MDCODFID), .w_CLIVEN, .w_CLIFID)
          .link_4_35('Full')
        endif
        .DoRTCalc(250,252,.t.)
          .w_MDALFDOC = .w_ALFDOC
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate()
        .DoRTCalc(254,260,.t.)
        if .o_MDTIPCHI<>.w_MDTIPCHI.or. .o_MDCODPAG<>.w_MDCODPAG
          .w_MDFLSALD = ' '
        endif
        .oPgFrm.Page2.oPag.oObj_4_65.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_66.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_67.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_68.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_69.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_70.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_71.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_72.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_73.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_79.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_87.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_88.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_90.Calculate()
        .DoRTCalc(262,275,.t.)
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_OPAGCLI = .w_MDPAGCLI
        endif
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_OFLRISC = .w_FLRISC
        endif
        if .o_MDSERIAL<>.w_MDSERIAL
          .w_OTIPCHI = .w_MDTIPCHI
        endif
        .oPgFrm.Page2.oPag.oObj_4_104.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_105.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_106.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_107.Calculate()
        if .o_MDTIPVEN<>.w_MDTIPVEN
          .Calculate_ZSWTKXGTYU()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(279,281,.t.)
          .w_oMDTIPDOC = iif(.w_EDTIPDOC, .w_oMDTIPDOC, space(5))
        .DoRTCalc(283,284,.t.)
          .link_4_114('Full')
        .DoRTCalc(286,291,.t.)
          .w_ERRORCOD = 'Articolo inesistente'
        .oPgFrm.Page2.oPag.oObj_4_116.Calculate()
        * --- Area Manuale = Calculate
        * --- gsps_mvd
        * - Se eseguita la mCalc in pag.1 viene valorizzata a .T.
        * - in modo da calcolare le promozioni per evitare
        * - il continuo ricalcolo ad ogni cambio pagina.
        *IF .oPgFrm.ActivePage=1
        *   this.w_PROMOK=.T.
        *ENDIF
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SERPLU","i_codazi,w_MDSERIAL")
          .op_MDSERIAL = .w_MDSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(293,301,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESPOS with this.w_DESPOS
      replace t_CODVAA with this.w_CODVAA
      replace t_LOTDIF with this.w_LOTDIF
      replace t_DA1 with this.w_DA1
      replace t_UM1 with this.w_UM1
      replace t_QT1 with this.w_QT1
      replace t_PZ1 with this.w_PZ1
      replace t_SC1 with this.w_SC1
      replace t_FLSERG with this.w_FLSERG
      replace t_MDTIPRIG with this.w_MDTIPRIG
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTIP with this.w_MOLTIP
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_OPERAT with this.w_OPERAT
      replace t_OPERA3 with this.w_OPERA3
      replace t_MDQTAUM1 with this.w_MDQTAUM1
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_LIPREZZO with this.w_LIPREZZO
      replace t_MDCONTRA with this.w_MDCONTRA
      replace t_MDFLCASC with this.w_MDFLCASC
      replace t_MDMAGSAL with this.w_MDMAGSAL
      replace t_ULTSCA with this.w_ULTSCA
      replace t_ARCODART with this.w_ARCODART
      replace t_CATSCA with this.w_CATSCA
      replace t_GRUMER with this.w_GRUMER
      replace t_OFLSCO with this.w_OFLSCO
      replace t_IMPNAZ with this.w_IMPNAZ
      replace t_RIGNET with this.w_RIGNET
      replace t_MDSERRIF with this.w_MDSERRIF
      replace t_MDNUMRIF with this.w_MDNUMRIF
      replace t_MDROWRIF with this.w_MDROWRIF
      replace t_MDFLARIF with this.w_MDFLARIF
      replace t_MDFLERIF with this.w_MDFLERIF
      replace t_MDQTAIMP with this.w_MDQTAIMP
      replace t_MDQTAIM1 with this.w_MDQTAIM1
      replace t_ORDEVA with this.w_ORDEVA
      replace t_OLDEVAS with this.w_OLDEVAS
      replace t_MDCODPRO with this.w_MDCODPRO
      replace t_MDFLESCL with this.w_MDFLESCL
      replace t_MDFLROMA with this.w_MDFLROMA
      replace t_ARTPOS with this.w_ARTPOS
      replace t_VALUCA with this.w_VALUCA
      replace t_QTAPER with this.w_QTAPER
      replace t_QTRPER with this.w_QTRPER
      replace t_FLPAN with this.w_FLPAN
      replace t_OQTAIMP with this.w_OQTAIMP
      replace t_OQTAIM1 with this.w_OQTAIM1
      replace t_OFLERIF with this.w_OFLERIF
      replace t_OPREZZO with this.w_OPREZZO
      replace t_MDFLULPV with this.w_MDFLULPV
      replace t_MDVALULT with this.w_MDVALULT
      replace t_FLSERA with this.w_FLSERA
      replace t_MDTIPPRO with this.w_MDTIPPRO
      replace t_MDPERPRO with this.w_MDPERPRO
      replace t_NOCALQTA with this.w_NOCALQTA
      replace t_LISCON with this.w_LISCON
      replace t_GESMAT with this.w_GESMAT
      replace t_COCODVAL with this.w_COCODVAL
      replace t_MDIMPCOM with this.w_MDIMPCOM
      replace t_GIACON with this.w_GIACON
      replace t_FLSTAT with this.w_FLSTAT
      replace t_DATLOT with this.w_DATLOT
      replace t_FLLOTT with this.w_FLLOTT
      replace t_MDFLLOTT with this.w_MDFLLOTT
      replace t_LOTZOOM with this.w_LOTZOOM
      replace t_GESMAT with this.w_GESMAT
      replace t_UBIZOOM with this.w_UBIZOOM
      replace t_MTCARI with this.w_MTCARI
      replace t_MDMAGRIG with this.w_MDMAGRIG
      replace t_OLDQTA with this.w_OLDQTA
      replace t_ARTDIS with this.w_ARTDIS
      replace t_FLDISP with this.w_FLDISP
      replace t_TOTMAT with this.w_TOTMAT
      replace t_FLRRIF with this.w_FLRRIF
      replace t_ORQTAEV1 with this.w_ORQTAEV1
      replace t_DISLOT with this.w_DISLOT
      replace t_MDTIPPR2 with this.w_MDTIPPR2
      replace t_MDPROCAP with this.w_MDPROCAP
      replace t_MDLOTMAG with this.w_MDLOTMAG
      replace t_NOFRAZ with this.w_NOFRAZ
      replace t_MODUM2 with this.w_MODUM2
      replace t_UNISEP with this.w_UNISEP
      replace t_PREZUM with this.w_PREZUM
      replace t_CLUNIMIS with this.w_CLUNIMIS
      replace t_CODREP with this.w_CODREP
      replace t_CODIVA with this.w_CODIVA
      replace t_DELNOMSG with this.w_DELNOMSG
      replace t_FLCOM1 with this.w_FLCOM1
      replace t_ARCLAMAT with this.w_ARCLAMAT
      replace t_MDPROMOZ with this.w_MDPROMOZ
      replace t_MDESPSCO with this.w_MDESPSCO
      replace t_NUMDEQ with this.w_NUMDEQ
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(IIF(.w_MDFLTRAS='S', '*** Vendita Trasferita', ''))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_148.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_46.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_86.Calculate(IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)<>'#########' AND NOT EMPTY(.w_MDCODPRO),'Sc. Promozione:',IIF(.w_MDTIPRIG<>'D','Sc. Ventilato:',IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)='#########' AND NOT EMPTY(.w_MDCODPRO),'Sconto:',''))))
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))=.T., 'Commessa:',''))
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_65.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_66.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_67.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_68.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_69.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_70.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_71.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_72.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_73.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_79.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_87.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_88.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_90.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_104.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_105.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_106.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_107.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_116.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_46.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_86.Calculate(IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)<>'#########' AND NOT EMPTY(.w_MDCODPRO),'Sc. Promozione:',IIF(.w_MDTIPRIG<>'D','Sc. Ventilato:',IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)='#########' AND NOT EMPTY(.w_MDCODPRO),'Sconto:',''))))
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))=.T., 'Commessa:',''))
    endwith
  return
  proc Calculate_AYUGABETUN()
    with this
          * --- Memorizzo valori originali
          .w_OIMPPRE = .w_MDIMPPRE
          .w_OTOTDOC = .w_MDTOTDOC
          .w_OPUNFID = .w_MDPUNFID
    endwith
  endproc
  proc Calculate_EKZUIATNLY()
    with this
          * --- SetCodRep - w_MDCODREP Changed
          SetCodRep(this;
              ,.w_MDCODREP;
              ,.w_DESREP;
              ,.w_MDCODIVA;
             )
    endwith
  endproc
  proc Calculate_FSXMTGLBDI()
    with this
          * --- Azzero promozioni riga al cambio articolo
          BlackPromo(this;
             )
    endwith
  endproc
  proc Calculate_ZSWTKXGTYU()
    with this
          * --- Forzo link MDCODMAG
          .w_MDCODMAG = .w_MDCODMAG
          .link_1_34('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMDTIPVEN_1_21.enabled = this.oPgFrm.Page1.oPag.oMDTIPVEN_1_21.mCond()
    this.oPgFrm.Page1.oPag.oMDCODFID_1_35.enabled = this.oPgFrm.Page1.oPag.oMDCODFID_1_35.mCond()
    this.oPgFrm.Page1.oPag.oMDCODCLI_1_41.enabled = this.oPgFrm.Page1.oPag.oMDCODCLI_1_41.mCond()
    this.oPgFrm.Page1.oPag.oMDMATSCO_1_48.enabled = this.oPgFrm.Page1.oPag.oMDMATSCO_1_48.mCond()
    this.oPgFrm.Page2.oPag.oMDSCOCL2_4_5.enabled = this.oPgFrm.Page2.oPag.oMDSCOCL2_4_5.mCond()
    this.oPgFrm.Page2.oPag.oMDSCONTI_4_7.enabled = this.oPgFrm.Page2.oPag.oMDSCONTI_4_7.mCond()
    this.oPgFrm.Page2.oPag.oMDCODPAG_4_9.enabled = this.oPgFrm.Page2.oPag.oMDCODPAG_4_9.mCond()
    this.oPgFrm.Page2.oPag.oMDIMPPRE_4_11.enabled = this.oPgFrm.Page2.oPag.oMDIMPPRE_4_11.mCond()
    this.oPgFrm.Page2.oPag.oMDRIFASS_4_17.enabled = this.oPgFrm.Page2.oPag.oMDRIFASS_4_17.mCond()
    this.oPgFrm.Page2.oPag.oMDCODCAR_4_19.enabled = this.oPgFrm.Page2.oPag.oMDCODCAR_4_19.mCond()
    this.oPgFrm.Page2.oPag.oMDCODFIN_4_22.enabled = this.oPgFrm.Page2.oPag.oMDCODFIN_4_22.mCond()
    this.oPgFrm.Page2.oPag.oMDPAGCLI_4_28.enabled = this.oPgFrm.Page2.oPag.oMDPAGCLI_4_28.mCond()
    this.oPgFrm.Page2.oPag.oMDTIPCHI_4_31.enabled = this.oPgFrm.Page2.oPag.oMDTIPCHI_4_31.mCond()
    this.oPgFrm.Page2.oPag.oMDCODCLI_4_35.enabled = this.oPgFrm.Page2.oPag.oMDCODCLI_4_35.mCond()
    this.oPgFrm.Page2.oPag.oMDTIPDOC_4_37.enabled = this.oPgFrm.Page2.oPag.oMDTIPDOC_4_37.mCond()
    this.oPgFrm.Page2.oPag.oMDFLSALD_4_60.enabled = this.oPgFrm.Page2.oPag.oMDFLSALD_4_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_89.enabled = this.oPgFrm.Page2.oPag.oBtn_4_89.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_91.enabled = this.oPgFrm.Page2.oPag.oBtn_4_91.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_95.enabled = this.oPgFrm.Page2.oPag.oBtn_4_95.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_96.enabled = this.oPgFrm.Page2.oPag.oBtn_4_96.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_98.enabled = this.oPgFrm.Page2.oPag.oBtn_4_98.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDUNIMIS_2_20
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDQTAMOV_2_22
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDDESART_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDDESART_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDUNIMIS_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDUNIMIS_2_20.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDQTAMOV_2_22.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDQTAMOV_2_22.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDPREZZO_2_25.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDPREZZO_2_25.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDSCONT1_2_26.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMDSCONT1_2_26.mCond()
    this.oPgFrm.Page1.oPag.oMDCODREP_2_28.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMDCODREP_2_28.mCond()
    this.oPgFrm.Page1.oPag.oMDSCONT2_2_30.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMDSCONT2_2_30.mCond()
    this.oPgFrm.Page1.oPag.oMDSCONT3_2_31.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMDSCONT3_2_31.mCond()
    this.oPgFrm.Page1.oPag.oMDSCONT4_2_32.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMDSCONT4_2_32.mCond()
    this.oPgFrm.Page1.oPag.oMDCODLOT_2_100.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMDCODLOT_2_100.mCond()
    this.oPgFrm.Page1.oPag.oMDCODUBI_2_101.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMDCODUBI_2_101.mCond()
    this.oPgFrm.Page1.oPag.oMDCODCOM_2_104.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMDCODCOM_2_104.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_38.enabled =this.oPgFrm.Page1.oPag.oBtn_2_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_39.enabled =this.oPgFrm.Page1.oPag.oBtn_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_40.enabled =this.oPgFrm.Page1.oPag.oBtn_2_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_41.enabled =this.oPgFrm.Page1.oPag.oBtn_2_41.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_2_103.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_103.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSVE_MMT.visible")=='L' And this.GSVE_MMT.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_103.enabled
      this.GSVE_MMT.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_39.visible=!this.oPgFrm.Page1.oPag.oBtn_1_39.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_43.visible=!this.oPgFrm.Page1.oPag.oBtn_1_43.mHide()
    this.oPgFrm.Page1.oPag.oMDNUMSCO_1_47.visible=!this.oPgFrm.Page1.oPag.oMDNUMSCO_1_47.mHide()
    this.oPgFrm.Page1.oPag.oMDMATSCO_1_48.visible=!this.oPgFrm.Page1.oPag.oMDMATSCO_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_113.visible=!this.oPgFrm.Page1.oPag.oStr_1_113.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_134.visible=!this.oPgFrm.Page1.oPag.oStr_1_134.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_135.visible=!this.oPgFrm.Page1.oPag.oStr_1_135.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_136.visible=!this.oPgFrm.Page1.oPag.oStr_1_136.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_137.visible=!this.oPgFrm.Page1.oPag.oStr_1_137.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_138.visible=!this.oPgFrm.Page1.oPag.oStr_1_138.mHide()
    this.oPgFrm.Page2.oPag.oMDIMPABB_4_29.visible=!this.oPgFrm.Page2.oPag.oMDIMPABB_4_29.mHide()
    this.oPgFrm.Page2.oPag.oMDFLSALD_4_60.visible=!this.oPgFrm.Page2.oPag.oMDFLSALD_4_60.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_63.visible=!this.oPgFrm.Page2.oPag.oStr_4_63.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_64.visible=!this.oPgFrm.Page2.oPag.oStr_4_64.mHide()
    this.oPgFrm.Page2.oPag.oBtn_4_89.visible=!this.oPgFrm.Page2.oPag.oBtn_4_89.mHide()
    this.oPgFrm.Page2.oPag.oBtn_4_91.visible=!this.oPgFrm.Page2.oPag.oBtn_4_91.mHide()
    this.oPgFrm.Page2.oPag.oBtn_4_95.visible=!this.oPgFrm.Page2.oPag.oBtn_4_95.mHide()
    this.oPgFrm.Page2.oPag.oBtn_4_96.visible=!this.oPgFrm.Page2.oPag.oBtn_4_96.mHide()
    this.oPgFrm.Page2.oPag.oBtn_4_98.visible=!this.oPgFrm.Page2.oPag.oBtn_4_98.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oMDCODART_2_8.visible=!this.oPgFrm.Page1.oPag.oMDCODART_2_8.mHide()
    this.oPgFrm.Page1.oPag.oMDSCONT2_2_30.visible=!this.oPgFrm.Page1.oPag.oMDSCONT2_2_30.mHide()
    this.oPgFrm.Page1.oPag.oMDSCONT3_2_31.visible=!this.oPgFrm.Page1.oPag.oMDSCONT3_2_31.mHide()
    this.oPgFrm.Page1.oPag.oMDSCONT4_2_32.visible=!this.oPgFrm.Page1.oPag.oMDSCONT4_2_32.mHide()
    this.oPgFrm.Page1.oPag.oMDSCOVEN_2_37.visible=!this.oPgFrm.Page1.oPag.oMDSCOVEN_2_37.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_39.visible=!this.oPgFrm.Page1.oPag.oBtn_2_39.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_40.visible=!this.oPgFrm.Page1.oPag.oBtn_2_40.mHide()
    this.oPgFrm.Page1.oPag.oMDSCOPRO_2_66.visible=!this.oPgFrm.Page1.oPag.oMDSCOPRO_2_66.mHide()
    this.oPgFrm.Page1.oPag.oMDCODLOT_2_100.visible=!this.oPgFrm.Page1.oPag.oMDCODLOT_2_100.mHide()
    this.oPgFrm.Page1.oPag.oMDCODUBI_2_101.visible=!this.oPgFrm.Page1.oPag.oMDCODUBI_2_101.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_2_103.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_103.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSVE_MMT.visible")=='L' And this.GSVE_MMT.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_103.visible
      this.GSVE_MMT.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oMDCODCOM_2_104.visible=!this.oPgFrm.Page1.oPag.oMDCODCOM_2_104.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_91.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_92.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_93.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_94.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_95.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_98.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_107.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_116.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_142.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_143.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_148.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_47.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_86.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_89.Event(cEvent)
        if lower(cEvent)==lower("w_MDCODREP Changed")
          .Calculate_EKZUIATNLY()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_4_53.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_65.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_66.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_67.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_68.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_69.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_70.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_71.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_72.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_73.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_79.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_86.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_87.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_88.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_90.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_104.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_105.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_106.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_107.Event(cEvent)
        if lower(cEvent)==lower("w_MDCODICE Changed")
          .Calculate_FSXMTGLBDI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_116.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCAS
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_lTable = "DIS_HARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2], .t., this.DIS_HARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DHCODICE,DHMATCAS";
                   +" from "+i_cTable+" "+i_lTable+" where DHCODICE="+cp_ToStrODBC(this.w_CODCAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DHCODICE',this.w_CODCAS)
            select DHCODICE,DHMATCAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAS = NVL(_Link_.DHCODICE,space(5))
      this.w_MATSCO = NVL(_Link_.DHMATCAS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAS = space(5)
      endif
      this.w_MATSCO = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])+'\'+cp_ToStr(_Link_.DHCODICE,1)
      cp_ShowWarn(i_cKey,this.DIS_HARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODVAL
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MDCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MDCODVAL)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.VACODVAL as VACODVAL108"+ ",link_1_8.VASIMVAL as VASIMVAL108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on COR_RISP.MDCODVAL=link_1_8.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODVAL=link_1_8.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODUTE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OPE_RATO_IDX,3]
    i_lTable = "OPE_RATO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2], .t., this.OPE_RATO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_AOP',True,'OPE_RATO')
        if i_nConn<>0
          i_cWhere = " OPCODUTE="+cp_ToStrODBC(this.w_MDCODUTE);

          i_ret=cp_SQL(i_nConn,"select OPCODUTE,OPCODOPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OPCODUTE',this.w_MDCODUTE)
          select OPCODUTE,OPCODOPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_MDCODUTE) and !this.bDontReportError
            deferred_cp_zoom('OPE_RATO','*','OPCODUTE',cp_AbsName(oSource.parent,'oMDCODUTE_1_17'),i_cWhere,'GSPS_AOP',"Elenco operatori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OPCODUTE,OPCODOPE";
                     +" from "+i_cTable+" "+i_lTable+" where OPCODUTE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OPCODUTE',oSource.xKey(1))
            select OPCODUTE,OPCODOPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OPCODUTE,OPCODOPE";
                   +" from "+i_cTable+" "+i_lTable+" where OPCODUTE="+cp_ToStrODBC(this.w_MDCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OPCODUTE',this.w_MDCODUTE)
            select OPCODUTE,OPCODOPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODUTE = NVL(_Link_.OPCODUTE,0)
      this.w_MDCODOPE = NVL(_Link_.OPCODOPE,0)
    else
      if i_cCtrl<>'Load'
        this.w_MDCODUTE = 0
      endif
      this.w_MDCODOPE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MDCODOPE<>0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice utente non definito")
        endif
        this.w_MDCODUTE = 0
        this.w_MDCODOPE = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OPE_RATO_IDX,2])+'\'+cp_ToStr(_Link_.OPCODUTE,1)
      cp_ShowWarn(i_cKey,this.OPE_RATO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDTIPVEN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_VEND_IDX,3]
    i_lTable = "MOD_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2], .t., this.MOD_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDTIPVEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_AMV',True,'MOD_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_MDTIPVEN)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOCODMAG,MOCODLIS,MOTIPCHI,MOCODCLI,MOFLUNIM,MOFLPREZ,MOQTADEF,MOFLSCON,MOFLGDES,MONUMSCO,MOCODPAG,MODOCCOR,MODOCRIC,MODOCFAT,MODOCFAF,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOPRZVAC,MOPRZDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_MDTIPVEN))
          select MOCODICE,MODESCRI,MOCODMAG,MOCODLIS,MOTIPCHI,MOCODCLI,MOFLUNIM,MOFLPREZ,MOQTADEF,MOFLSCON,MOFLGDES,MONUMSCO,MOCODPAG,MODOCCOR,MODOCRIC,MODOCFAT,MODOCFAF,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOPRZVAC,MOPRZDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDTIPVEN)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStrODBC(trim(this.w_MDTIPVEN)+"%");

            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOCODMAG,MOCODLIS,MOTIPCHI,MOCODCLI,MOFLUNIM,MOFLPREZ,MOQTADEF,MOFLSCON,MOFLGDES,MONUMSCO,MOCODPAG,MODOCCOR,MODOCRIC,MODOCFAT,MODOCFAF,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOPRZVAC,MOPRZDES";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStr(trim(this.w_MDTIPVEN)+"%");

            select MOCODICE,MODESCRI,MOCODMAG,MOCODLIS,MOTIPCHI,MOCODCLI,MOFLUNIM,MOFLPREZ,MOQTADEF,MOFLSCON,MOFLGDES,MONUMSCO,MOCODPAG,MODOCCOR,MODOCRIC,MODOCFAT,MODOCFAF,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOPRZVAC,MOPRZDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MDTIPVEN) and !this.bDontReportError
            deferred_cp_zoom('MOD_VEND','*','MOCODICE',cp_AbsName(oSource.parent,'oMDTIPVEN_1_21'),i_cWhere,'GSPS_AMV',"Modalit� di vendita",'GSPS_MVD.MOD_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOCODMAG,MOCODLIS,MOTIPCHI,MOCODCLI,MOFLUNIM,MOFLPREZ,MOQTADEF,MOFLSCON,MOFLGDES,MONUMSCO,MOCODPAG,MODOCCOR,MODOCRIC,MODOCFAT,MODOCFAF,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOPRZVAC,MOPRZDES";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI,MOCODMAG,MOCODLIS,MOTIPCHI,MOCODCLI,MOFLUNIM,MOFLPREZ,MOQTADEF,MOFLSCON,MOFLGDES,MONUMSCO,MOCODPAG,MODOCCOR,MODOCRIC,MODOCFAT,MODOCFAF,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOPRZVAC,MOPRZDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDTIPVEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOCODMAG,MOCODLIS,MOTIPCHI,MOCODCLI,MOFLUNIM,MOFLPREZ,MOQTADEF,MOFLSCON,MOFLGDES,MONUMSCO,MOCODPAG,MODOCCOR,MODOCRIC,MODOCFAT,MODOCFAF,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOPRZVAC,MOPRZDES";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_MDTIPVEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_MDTIPVEN)
            select MOCODICE,MODESCRI,MOCODMAG,MOCODLIS,MOTIPCHI,MOCODCLI,MOFLUNIM,MOFLPREZ,MOQTADEF,MOFLSCON,MOFLGDES,MONUMSCO,MOCODPAG,MODOCCOR,MODOCRIC,MODOCFAT,MODOCFAF,MODOCRIF,MODOCDDT,MODOCCO1,MODOCCO2,MODOCCO3,MOPRZVAC,MOPRZDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDTIPVEN = NVL(_Link_.MOCODICE,space(5))
      this.w_DESVEN = NVL(_Link_.MODESCRI,space(35))
      this.w_MDCODMAG = NVL(_Link_.MOCODMAG,space(5))
      this.w_LISVEN = NVL(_Link_.MOCODLIS,space(5))
      this.w_MDTIPCHI = NVL(_Link_.MOTIPCHI,space(2))
      this.w_CLIVEN = NVL(_Link_.MOCODCLI,space(15))
      this.w_FLUNIM = NVL(_Link_.MOFLUNIM,space(1))
      this.w_FLPREZ = NVL(_Link_.MOFLPREZ,space(1))
      this.w_QTADEF = NVL(_Link_.MOQTADEF,0)
      this.w_FLSCON = NVL(_Link_.MOFLSCON,space(1))
      this.w_FLGDES = NVL(_Link_.MOFLGDES,space(1))
      this.w_NUMSCO = NVL(_Link_.MONUMSCO,0)
      this.w_PAGVEN = NVL(_Link_.MOCODPAG,space(5))
      this.w_DOCCOR = NVL(_Link_.MODOCCOR,space(5))
      this.w_DOCRIC = NVL(_Link_.MODOCRIC,space(5))
      this.w_DOCFAT = NVL(_Link_.MODOCFAT,space(5))
      this.w_DOCFAF = NVL(_Link_.MODOCFAF,space(5))
      this.w_DOCRIF = NVL(_Link_.MODOCRIF,space(5))
      this.w_DOCDDT = NVL(_Link_.MODOCDDT,space(5))
      this.w_DOCCO1 = NVL(_Link_.MODOCCO1,space(5))
      this.w_DOCCO2 = NVL(_Link_.MODOCCO2,space(5))
      this.w_DOCCO3 = NVL(_Link_.MODOCCO3,space(5))
      this.w_PRZVAC = NVL(_Link_.MOPRZVAC,space(1))
      this.w_PRZDES = NVL(_Link_.MOPRZDES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MDTIPVEN = space(5)
      endif
      this.w_DESVEN = space(35)
      this.w_MDCODMAG = space(5)
      this.w_LISVEN = space(5)
      this.w_MDTIPCHI = space(2)
      this.w_CLIVEN = space(15)
      this.w_FLUNIM = space(1)
      this.w_FLPREZ = space(1)
      this.w_QTADEF = 0
      this.w_FLSCON = space(1)
      this.w_FLGDES = space(1)
      this.w_NUMSCO = 0
      this.w_PAGVEN = space(5)
      this.w_DOCCOR = space(5)
      this.w_DOCRIC = space(5)
      this.w_DOCFAT = space(5)
      this.w_DOCFAF = space(5)
      this.w_DOCRIF = space(5)
      this.w_DOCDDT = space(5)
      this.w_DOCCO1 = space(5)
      this.w_DOCCO2 = space(5)
      this.w_DOCCO3 = space(5)
      this.w_PRZVAC = space(1)
      this.w_PRZDES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MDCODNEG=g_CODNEG AND PSCHKMOD(.w_DOCDDT, .w_MDTIPCHI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Modalit� di vendita inesistente o incongruente")
        endif
        this.w_MDTIPVEN = space(5)
        this.w_DESVEN = space(35)
        this.w_MDCODMAG = space(5)
        this.w_LISVEN = space(5)
        this.w_MDTIPCHI = space(2)
        this.w_CLIVEN = space(15)
        this.w_FLUNIM = space(1)
        this.w_FLPREZ = space(1)
        this.w_QTADEF = 0
        this.w_FLSCON = space(1)
        this.w_FLGDES = space(1)
        this.w_NUMSCO = 0
        this.w_PAGVEN = space(5)
        this.w_DOCCOR = space(5)
        this.w_DOCRIC = space(5)
        this.w_DOCFAT = space(5)
        this.w_DOCFAF = space(5)
        this.w_DOCRIF = space(5)
        this.w_DOCDDT = space(5)
        this.w_DOCCO1 = space(5)
        this.w_DOCCO2 = space(5)
        this.w_DOCCO3 = space(5)
        this.w_PRZVAC = space(1)
        this.w_PRZDES = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDTIPVEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 24 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_VEND_IDX,3] and i_nFlds+24<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_VEND_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.MOCODICE as MOCODICE121"+ ",link_1_21.MODESCRI as MODESCRI121"+ ",link_1_21.MOCODMAG as MOCODMAG121"+ ",link_1_21.MOCODLIS as MOCODLIS121"+ ",link_1_21.MOTIPCHI as MOTIPCHI121"+ ",link_1_21.MOCODCLI as MOCODCLI121"+ ",link_1_21.MOFLUNIM as MOFLUNIM121"+ ",link_1_21.MOFLPREZ as MOFLPREZ121"+ ",link_1_21.MOQTADEF as MOQTADEF121"+ ",link_1_21.MOFLSCON as MOFLSCON121"+ ",link_1_21.MOFLGDES as MOFLGDES121"+ ",link_1_21.MONUMSCO as MONUMSCO121"+ ",link_1_21.MOCODPAG as MOCODPAG121"+ ",link_1_21.MODOCCOR as MODOCCOR121"+ ",link_1_21.MODOCRIC as MODOCRIC121"+ ",link_1_21.MODOCFAT as MODOCFAT121"+ ",link_1_21.MODOCFAF as MODOCFAF121"+ ",link_1_21.MODOCRIF as MODOCRIF121"+ ",link_1_21.MODOCDDT as MODOCDDT121"+ ",link_1_21.MODOCCO1 as MODOCCO1121"+ ",link_1_21.MODOCCO2 as MODOCCO2121"+ ",link_1_21.MODOCCO3 as MODOCCO3121"+ ",link_1_21.MOPRZVAC as MOPRZVAC121"+ ",link_1_21.MOPRZDES as MOPRZDES121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on COR_RISP.MDTIPVEN=link_1_21.MOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+24
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and COR_RISP.MDTIPVEN=link_1_21.MOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+24
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
    i_lTable = "PAR_VDET"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2], .t., this.PAR_VDET_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACODNEG,PAALFDOC,PACAURES,PACARKIT,PASCAKIT,PASERFID,PAFLFAFI";
                   +" from "+i_cTable+" "+i_lTable+" where PACODNEG="+cp_ToStrODBC(this.w_READPAR);
                   +" and PACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   +" and PACODNEG="+cp_ToStrODBC(this.w_MDCODNEG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_CODAZI;
                       ,'PACODNEG',this.w_MDCODNEG;
                       ,'PACODNEG',this.w_READPAR)
            select PACODAZI,PACODNEG,PAALFDOC,PACAURES,PACARKIT,PASCAKIT,PASERFID,PAFLFAFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PACODNEG,space(10))
      this.w_ALFDOC = NVL(_Link_.PAALFDOC,space(10))
      this.w_CAURES = NVL(_Link_.PACAURES,space(5))
      this.w_CARKIT = NVL(_Link_.PACARKIT,space(5))
      this.w_SCAKIT = NVL(_Link_.PASCAKIT,space(5))
      this.w_SERFID = NVL(_Link_.PASERFID,space(20))
      this.w_FLFAFI = NVL(_Link_.PAFLFAFI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_ALFDOC = space(10)
      this.w_CAURES = space(5)
      this.w_CARKIT = space(5)
      this.w_SCAKIT = space(5)
      this.w_SERFID = space(20)
      this.w_FLFAFI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)+'\'+cp_ToStr(_Link_.PACODNEG,1)+'\'+cp_ToStr(_Link_.PACODNEG,1)
      cp_ShowWarn(i_cKey,this.PAR_VDET_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODMAG
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MDCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MDCODMAG))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMDCODMAG_1_34'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MDCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MDCODMAG)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODMAG = space(5)
      endif
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_34.MGCODMAG as MGCODMAG134"+ ",link_1_34.MGFLUBIC as MGFLUBIC134"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_34 on COR_RISP.MDCODMAG=link_1_34.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_34"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODMAG=link_1_34.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODFID
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_lTable = "FID_CARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2], .t., this.FID_CARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODFID) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_AFC',True,'FID_CARD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FCCODFID like "+cp_ToStrODBC(trim(this.w_MDCODFID)+"%");

          i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATATT,FCDATSCA,FCDATUAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FCCODFID","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FCCODFID',trim(this.w_MDCODFID))
          select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATATT,FCDATSCA,FCDATUAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FCCODFID into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODFID)==trim(_Link_.FCCODFID) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FCDESFID like "+cp_ToStrODBC(trim(this.w_MDCODFID)+"%");

            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATATT,FCDATSCA,FCDATUAC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FCDESFID like "+cp_ToStr(trim(this.w_MDCODFID)+"%");

            select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATATT,FCDATSCA,FCDATUAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MDCODFID) and !this.bDontReportError
            deferred_cp_zoom('FID_CARD','*','FCCODFID',cp_AbsName(oSource.parent,'oMDCODFID_1_35'),i_cWhere,'GSPS_AFC',"Fidelity Card",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATATT,FCDATSCA,FCDATUAC";
                     +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',oSource.xKey(1))
            select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATATT,FCDATSCA,FCDATUAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODFID)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATATT,FCDATSCA,FCDATUAC";
                   +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(this.w_MDCODFID);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',this.w_MDCODFID)
            select FCCODFID,FCDESFID,FCCODCLI,FCIMPPRE,FCIMPRES,FCTOTIMP,FCPUNFID,FCNUMVIS,FCDATATT,FCDATSCA,FCDATUAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODFID = NVL(_Link_.FCCODFID,space(20))
      this.w_DESFID = NVL(_Link_.FCDESFID,space(40))
      this.w_CLIFID = NVL(_Link_.FCCODCLI,space(15))
      this.w_FIMPPRE = NVL(_Link_.FCIMPPRE,0)
      this.w_FIMPRES = NVL(_Link_.FCIMPRES,0)
      this.w_FTOTIMP = NVL(_Link_.FCTOTIMP,0)
      this.w_FPUNFID = NVL(_Link_.FCPUNFID,0)
      this.w_FNUMVIS = NVL(_Link_.FCNUMVIS,0)
      this.w_FDATATT = NVL(cp_ToDate(_Link_.FCDATATT),ctod("  /  /  "))
      this.w_FDATSCA = NVL(cp_ToDate(_Link_.FCDATSCA),ctod("  /  /  "))
      this.w_FDATUAC = NVL(cp_ToDate(_Link_.FCDATUAC),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODFID = space(20)
      endif
      this.w_DESFID = space(40)
      this.w_CLIFID = space(15)
      this.w_FIMPPRE = 0
      this.w_FIMPRES = 0
      this.w_FTOTIMP = 0
      this.w_FPUNFID = 0
      this.w_FNUMVIS = 0
      this.w_FDATATT = ctod("  /  /  ")
      this.w_FDATSCA = ctod("  /  /  ")
      this.w_FDATUAC = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MDCODFID) OR ((.w_MDDATREG>=.w_FDATATT OR EMPTY(.w_FDATATT)) AND (.w_MDDATREG<=.w_FDATSCA OR EMPTY(.w_FDATSCA)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Fidelity Card inesistente o fuori validit�")
        endif
        this.w_MDCODFID = space(20)
        this.w_DESFID = space(40)
        this.w_CLIFID = space(15)
        this.w_FIMPPRE = 0
        this.w_FIMPRES = 0
        this.w_FTOTIMP = 0
        this.w_FPUNFID = 0
        this.w_FNUMVIS = 0
        this.w_FDATATT = ctod("  /  /  ")
        this.w_FDATSCA = ctod("  /  /  ")
        this.w_FDATUAC = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])+'\'+cp_ToStr(_Link_.FCCODFID,1)
      cp_ShowWarn(i_cKey,this.FID_CARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODFID Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 11 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FID_CARD_IDX,3] and i_nFlds+11<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_35.FCCODFID as FCCODFID135"+ ",link_1_35.FCDESFID as FCDESFID135"+ ",link_1_35.FCCODCLI as FCCODCLI135"+ ",link_1_35.FCIMPPRE as FCIMPPRE135"+ ",link_1_35.FCIMPRES as FCIMPRES135"+ ",link_1_35.FCTOTIMP as FCTOTIMP135"+ ",link_1_35.FCPUNFID as FCPUNFID135"+ ",link_1_35.FCNUMVIS as FCNUMVIS135"+ ",link_1_35.FCDATATT as FCDATATT135"+ ",link_1_35.FCDATSCA as FCDATSCA135"+ ",link_1_35.FCDATUAC as FCDATUAC135"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_35 on COR_RISP.MDCODFID=link_1_35.FCCODFID"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_35"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODFID=link_1_35.FCCODFID(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODCLI
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACV',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_MDCODCLI)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO,CLDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_MDCODCLI))
          select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO,CLDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODCLI)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStrODBC(trim(this.w_MDCODCLI)+"%");

            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO,CLDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStr(trim(this.w_MDCODCLI)+"%");

            select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MDCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oMDCODCLI_1_41'),i_cWhere,'GSPS_ACV',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO,CLDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO,CLDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_MDCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_MDCODCLI)
            select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLTELFAX,CL_EMAIL,CLCODCON,CLFLFIDO,CLDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODCLI = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESCLI = NVL(_Link_.CLDESCRI,space(40))
      this.w_SCOCL1 = NVL(_Link_.CLSCONT1,0)
      this.w_SCOCL2 = NVL(_Link_.CLSCONT2,0)
      this.w_PAGCLI = NVL(_Link_.CLCODPAG,space(5))
      this.w_CATSCC = NVL(_Link_.CLCATSCM,space(5))
      this.w_VALFID = NVL(_Link_.CLVALFID,0)
      this.w_FIDUTI = NVL(_Link_.CLFIDUTI,0)
      this.w_CATCOM = NVL(_Link_.CLCATCOM,space(3))
      this.w_LISCLI = NVL(_Link_.CLCODLIS,space(5))
      this.w_TELFAX = NVL(_Link_.CLTELFAX,space(18))
      this.w_EMAIL = NVL(_Link_.CL_EMAIL,space(50))
      this.w_CODCLI = NVL(_Link_.CLCODCON,space(15))
      this.w_FLFIDO = NVL(_Link_.CLFLFIDO,space(1))
      this.w_CLOBSO = NVL(cp_ToDate(_Link_.CLDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_SCOCL1 = 0
      this.w_SCOCL2 = 0
      this.w_PAGCLI = space(5)
      this.w_CATSCC = space(5)
      this.w_VALFID = 0
      this.w_FIDUTI = 0
      this.w_CATCOM = space(3)
      this.w_LISCLI = space(5)
      this.w_TELFAX = space(18)
      this.w_EMAIL = space(50)
      this.w_CODCLI = space(15)
      this.w_FLFIDO = space(1)
      this.w_CLOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CLOBSO>.w_MDDATREG OR EMPTY(.w_CLOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cliente inesistente, obsoleto o incongruente")
        endif
        this.w_MDCODCLI = space(15)
        this.w_DESCLI = space(40)
        this.w_SCOCL1 = 0
        this.w_SCOCL2 = 0
        this.w_PAGCLI = space(5)
        this.w_CATSCC = space(5)
        this.w_VALFID = 0
        this.w_FIDUTI = 0
        this.w_CATCOM = space(3)
        this.w_LISCLI = space(5)
        this.w_TELFAX = space(18)
        this.w_EMAIL = space(50)
        this.w_CODCLI = space(15)
        this.w_FLFIDO = space(1)
        this.w_CLOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_41(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 15 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLI_VEND_IDX,3] and i_nFlds+15<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_41.CLCODCLI as CLCODCLI141"+ ",link_1_41.CLDESCRI as CLDESCRI141"+ ",link_1_41.CLSCONT1 as CLSCONT1141"+ ",link_1_41.CLSCONT2 as CLSCONT2141"+ ",link_1_41.CLCODPAG as CLCODPAG141"+ ",link_1_41.CLCATSCM as CLCATSCM141"+ ",link_1_41.CLVALFID as CLVALFID141"+ ",link_1_41.CLFIDUTI as CLFIDUTI141"+ ",link_1_41.CLCATCOM as CLCATCOM141"+ ",link_1_41.CLCODLIS as CLCODLIS141"+ ",link_1_41.CLTELFAX as CLTELFAX141"+ ",link_1_41.CL_EMAIL as CL_EMAIL141"+ ",link_1_41.CLCODCON as CLCODCON141"+ ",link_1_41.CLFLFIDO as CLFLFIDO141"+ ",link_1_41.CLDTOBSO as CLDTOBSO141"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_41 on COR_RISP.MDCODCLI=link_1_41.CLCODCLI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_41"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODCLI=link_1_41.CLCODCLI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODLIS
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_MDCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_MDCODLIS))
          select LSCODLIS,LSIVALIS,LSVALLIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oMDCODLIS_1_44'),i_cWhere,'',"Listini",'GSPS_MVD.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSIVALIS,LSVALLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_MDCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_MDCODLIS)
            select LSCODLIS,LSIVALIS,LSVALLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_LISIVA = NVL(_Link_.LSIVALIS,space(1))
      this.w_LISVAL = NVL(_Link_.LSVALLIS,space(3))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODLIS = space(5)
      endif
      this.w_LISIVA = space(1)
      this.w_LISVAL = space(3)
      this.w_SCOLIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LISVAL=.w_MDCODVAL
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino inesistente o valuta incongruente")
        endif
        this.w_MDCODLIS = space(5)
        this.w_LISIVA = space(1)
        this.w_LISVAL = space(3)
        this.w_SCOLIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_44(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_44.LSCODLIS as LSCODLIS144"+ ",link_1_44.LSIVALIS as LSIVALIS144"+ ",link_1_44.LSVALLIS as LSVALLIS144"+ ",link_1_44.LSFLSCON as LSFLSCON144"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_44 on COR_RISP.MDCODLIS=link_1_44.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_44"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODLIS=link_1_44.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_102(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC('C');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON','C';
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_CODESC = NVL(_Link_.ANCODESC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_CODESC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTDES
  func Link_1_104(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARARTPOS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTDES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTDES)
            select ARCODART,ARARTPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTDES = NVL(_Link_.ARCODART,space(10))
      this.w_DESPOS = NVL(_Link_.ARARTPOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARTDES = space(10)
      endif
      this.w_DESPOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READHARD
  func Link_1_149(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_lTable = "DIS_HARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2], .t., this.DIS_HARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READHARD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READHARD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DHCODICE,DHPASQTA";
                   +" from "+i_cTable+" "+i_lTable+" where DHCODICE="+cp_ToStrODBC(this.w_READHARD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DHCODICE',this.w_READHARD)
            select DHCODICE,DHPASQTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READHARD = NVL(_Link_.DHCODICE,space(5))
      this.w_PASQTA = NVL(_Link_.DHPASQTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READHARD = space(5)
      endif
      this.w_PASQTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])+'\'+cp_ToStr(_Link_.DHCODICE,1)
      cp_ShowWarn(i_cKey,this.DIS_HARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READHARD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODICE
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MDCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CA__TIPO,CAUNIMIS,CAMOLTIP,CAOPERAT,CANUMDEQ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MDCODICE))
          select CACODICE,CADESART,CACODART,CA__TIPO,CAUNIMIS,CAMOLTIP,CAOPERAT,CANUMDEQ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MDCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CA__TIPO,CAUNIMIS,CAMOLTIP,CAOPERAT,CANUMDEQ";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MDCODICE)+"%");

            select CACODICE,CADESART,CACODART,CA__TIPO,CAUNIMIS,CAMOLTIP,CAOPERAT,CANUMDEQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MDCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMDCODICE_2_6'),i_cWhere,'GSMA_BZA',"Articoli pos",'GSPS_MVD.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CA__TIPO,CAUNIMIS,CAMOLTIP,CAOPERAT,CANUMDEQ";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CA__TIPO,CAUNIMIS,CAMOLTIP,CAOPERAT,CANUMDEQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CA__TIPO,CAUNIMIS,CAMOLTIP,CAOPERAT,CANUMDEQ";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MDCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MDCODICE)
            select CACODICE,CADESART,CACODART,CA__TIPO,CAUNIMIS,CAMOLTIP,CAOPERAT,CANUMDEQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_MDDESART = NVL(_Link_.CADESART,space(40))
      this.w_MDCODART = NVL(_Link_.CACODART,space(20))
      this.w_MDTIPRIG = NVL(_Link_.CA__TIPO,space(1))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_NUMDEQ = NVL(_Link_.CANUMDEQ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODICE = space(20)
      endif
      this.w_MDDESART = space(40)
      this.w_MDCODART = space(20)
      this.w_MDTIPRIG = space(1)
      this.w_UNMIS3 = space(3)
      this.w_MOLTI3 = 0
      this.w_OPERA3 = space(1)
      this.w_NUMDEQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=PSCHKART(.w_MDCODART, .w_MDCODICE,  .w_MDDATREG, .w_CODESC, .w_CODCLI, THIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("%w_ERRORCOD%")
        endif
        this.w_MDCODICE = space(20)
        this.w_MDDESART = space(40)
        this.w_MDCODART = space(20)
        this.w_MDTIPRIG = space(1)
        this.w_UNMIS3 = space(3)
        this.w_MOLTI3 = 0
        this.w_OPERA3 = space(1)
        this.w_NUMDEQ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.CACODICE as CACODICE206"+ ",link_2_6.CADESART as CADESART206"+ ",link_2_6.CACODART as CACODART206"+ ",link_2_6.CA__TIPO as CA__TIPO206"+ ",link_2_6.CAUNIMIS as CAUNIMIS206"+ ",link_2_6.CAMOLTIP as CAMOLTIP206"+ ",link_2_6.CAOPERAT as CAOPERAT206"+ ",link_2_6.CANUMDEQ as CANUMDEQ206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on CORDRISP.MDCODICE=link_2_6.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and CORDRISP.MDCODICE=link_2_6.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODART
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARCATSCM,ARGRUMER,ARFLSERG,ARARTPOS,ARCODREP,ARTIPSER,ARFLLOTT,ARGESMAT,ARFLDISP,ARDISLOT,ARFLUSEP,ARPREZUM,ARSALCOM,ARCLAMAT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MDCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MDCODART)
            select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARCATSCM,ARGRUMER,ARFLSERG,ARARTPOS,ARCODREP,ARTIPSER,ARFLLOTT,ARGESMAT,ARFLDISP,ARDISLOT,ARFLUSEP,ARPREZUM,ARSALCOM,ARCLAMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_CATSCA = NVL(_Link_.ARCATSCM,space(5))
      this.w_GRUMER = NVL(_Link_.ARGRUMER,space(5))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_ARTPOS = NVL(_Link_.ARARTPOS,space(1))
      this.w_CODREP = NVL(_Link_.ARCODREP,space(3))
      this.w_FLSERA = NVL(_Link_.ARTIPSER,space(1))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_GESMAT = NVL(_Link_.ARGESMAT,space(1))
      this.w_ARTDIS = NVL(_Link_.ARFLDISP,space(1))
      this.w_DISLOT = NVL(_Link_.ARDISLOT,space(1))
      this.w_UNISEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
      this.w_FLCOM1 = NVL(_Link_.ARSALCOM,space(1))
      this.w_ARCLAMAT = NVL(_Link_.ARCLAMAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_CATSCA = space(5)
      this.w_GRUMER = space(5)
      this.w_FLSERG = space(1)
      this.w_ARTPOS = space(1)
      this.w_CODREP = space(3)
      this.w_FLSERA = space(1)
      this.w_FLLOTT = space(1)
      this.w_GESMAT = space(1)
      this.w_ARTDIS = space(1)
      this.w_DISLOT = space(1)
      this.w_UNISEP = space(1)
      this.w_PREZUM = space(1)
      this.w_FLCOM1 = space(1)
      this.w_ARCLAMAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 19 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+19<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.ARCODART as ARCODART208"+ ",link_2_8.ARUNMIS1 as ARUNMIS1208"+ ",link_2_8.AROPERAT as AROPERAT208"+ ",link_2_8.ARMOLTIP as ARMOLTIP208"+ ",link_2_8.ARUNMIS2 as ARUNMIS2208"+ ",link_2_8.ARCATSCM as ARCATSCM208"+ ",link_2_8.ARGRUMER as ARGRUMER208"+ ",link_2_8.ARFLSERG as ARFLSERG208"+ ",link_2_8.ARARTPOS as ARARTPOS208"+ ",link_2_8.ARCODREP as ARCODREP208"+ ",link_2_8.ARTIPSER as ARTIPSER208"+ ",link_2_8.ARFLLOTT as ARFLLOTT208"+ ",link_2_8.ARGESMAT as ARGESMAT208"+ ",link_2_8.ARFLDISP as ARFLDISP208"+ ",link_2_8.ARDISLOT as ARDISLOT208"+ ",link_2_8.ARFLUSEP as ARFLUSEP208"+ ",link_2_8.ARPREZUM as ARPREZUM208"+ ",link_2_8.ARSALCOM as ARSALCOM208"+ ",link_2_8.ARCLAMAT as ARCLAMAT208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on CORDRISP.MDCODART=link_2_8.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+19
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and CORDRISP.MDCODART=link_2_8.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+19
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_NOFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_NOFRAZ = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDUNIMIS
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_MDUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_MDUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oMDUNIMIS_2_20'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_MDUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_MDUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MDUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(IIF((.w_MDTIPRIG='M' AND .w_MDUNIMIS<>'   ') or .w_FLSERG='S','***',.w_MDUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_MDUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_MDUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_20.UMCODICE as UMCODICE220"+ ",link_2_20.UMFLFRAZ as UMFLFRAZ220"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_20 on CORDRISP.MDUNIMIS=link_2_20.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_20"
          i_cKey=i_cKey+'+" and CORDRISP.MDUNIMIS=link_2_20.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODREP
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_MDCODREP)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_MDCODREP))
          select RECODREP,REDESREP,RECODIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODREP)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODREP) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oMDCODREP_2_28'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP,RECODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_MDCODREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_MDCODREP)
            select RECODREP,REDESREP,RECODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODREP = NVL(_Link_.RECODREP,space(3))
      this.w_DESREP = NVL(_Link_.REDESREP,space(40))
      this.w_MDCODIVA = NVL(_Link_.RECODIVA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODREP = space(3)
      endif
      this.w_DESREP = space(40)
      this.w_MDCODIVA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MDTIPRIG='D' OR NOT EMPTY(.w_MDCODIVA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice reparto inesistente o codice IVA non definito")
        endif
        this.w_MDCODREP = space(3)
        this.w_DESREP = space(40)
        this.w_MDCODIVA = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REP_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_28.RECODREP as RECODREP228"+ ",link_2_28.REDESREP as REDESREP228"+ ",link_2_28.RECODIVA as RECODIVA228"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_28 on CORDRISP.MDCODREP=link_2_28.RECODREP"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_28"
          i_cKey=i_cKey+'+" and CORDRISP.MDCODREP=link_2_28.RECODREP(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODIVA
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MDCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MDCODIVA)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_MDCODIVA = space(5)
      endif
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_29.IVCODIVA as IVCODIVA229"+ ",link_2_29.IVPERIVA as IVPERIVA229"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_29 on CORDRISP.MDCODIVA=link_2_29.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_29"
          i_cKey=i_cKey+'+" and CORDRISP.MDCODIVA=link_2_29.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDMAGSAL
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_lTable = "SALDIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2], .t., this.SALDIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDMAGSAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDMAGSAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODICE,SLCODMAG,SLVALUCA,SLQTAPER,SLQTRPER,SLDATUPV,SLCODVAA";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODMAG="+cp_ToStrODBC(this.w_MDMAGSAL);
                   +" and SLCODICE="+cp_ToStrODBC(this.w_MDCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODICE',this.w_MDCODART;
                       ,'SLCODMAG',this.w_MDMAGSAL)
            select SLCODICE,SLCODMAG,SLVALUCA,SLQTAPER,SLQTRPER,SLDATUPV,SLCODVAA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUCA = NVL(_Link_.SLVALUCA,0)
      this.w_QTAPER = NVL(_Link_.SLQTAPER,0)
      this.w_QTRPER = NVL(_Link_.SLQTRPER,0)
      this.w_ULTSCA = NVL(cp_ToDate(_Link_.SLDATUPV),ctod("  /  /  "))
      this.w_CODVAA = NVL(_Link_.SLCODVAA,space(3))
    else
      this.w_VALUCA = 0
      this.w_QTAPER = 0
      this.w_QTRPER = 0
      this.w_ULTSCA = ctod("  /  /  ")
      this.w_CODVAA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])+'\'+cp_ToStr(_Link_.SLCODICE,1)+'\'+cp_ToStr(_Link_.SLCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDMAGSAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDROWRIF
  func Link_2_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
    i_lTable = "DOC_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2], .t., this.DOC_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDROWRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDROWRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,CPROWNUM,MVFLRISE,MVQTAEV1";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_MDROWRIF);
                   +" and MVSERIAL="+cp_ToStrODBC(this.w_MDSERRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_MDSERRIF;
                       ,'CPROWNUM',this.w_MDROWRIF)
            select MVSERIAL,CPROWNUM,MVFLRISE,MVQTAEV1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDROWRIF = NVL(_Link_.CPROWNUM,0)
      this.w_FLRRIF = NVL(_Link_.MVFLRISE,space(1))
      this.w_ORQTAEV1 = NVL(_Link_.MVQTAEV1,0)
    else
      if i_cCtrl<>'Load'
        this.w_MDROWRIF = 0
      endif
      this.w_FLRRIF = space(1)
      this.w_ORQTAEV1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.DOC_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDROWRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODVAL
  func Link_2_91(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_COCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_COCODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECCOM = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_COCODVAL = space(3)
      endif
      this.w_DECCOM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODLOT
  func Link_2_100(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_MDCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_MDCODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_MDCODART;
                     ,'LOCODICE',trim(this.w_MDCODLOT))
          select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oMDCODLOT_2_100'),i_cWhere,'GSMD_ALO',"Lotti articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MDCODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_MDCODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_MDCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_MDCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_MDCODART;
                       ,'LOCODICE',this.w_MDCODLOT)
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_FLSTAT = NVL(_Link_.LOFLSTAT,space(1))
      this.w_DATLOT = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODLOT = space(20)
      endif
      this.w_FLSTAT = space(1)
      this.w_DATLOT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_FLSTAT<>'S' AND (Empty(.w_DATLOT) OR .w_DATLOT >.w_MDDATREG)) OR CHKLOTUB(.w_MDCODLOT,Space(10),0,.w_MDCODMAG,.w_MDCODART,Space(15),.w_MDFLLOTT, .w_FLSTAT,.w_LOTZOOM,.w_MDDATREG,' ','L','@@@@@@',' ',.F.,.w_DISLOT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        endif
        this.w_MDCODLOT = space(20)
        this.w_FLSTAT = space(1)
        this.w_DATLOT = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODUBI
  func Link_2_101(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_MDCODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MDMAGRIG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_MDMAGRIG;
                     ,'UBCODICE',trim(this.w_MDCODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oMDCODUBI_2_101'),i_cWhere,'GSMD_MUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MDMAGRIG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_MDMAGRIG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_MDCODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MDMAGRIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MDMAGRIG;
                       ,'UBCODICE',this.w_MDCODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MDFLLOTT='+') OR CHKLOTUB(.w_MDCODUBI,'',0,.w_MDCODMAG,.w_MDCODART,Space(15),.w_MDFLLOTT, .w_FLSTAT,.w_LOTZOOM,.w_MDDATREG,' ','U','     ','     ',.T.,.w_DISLOT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        endif
        this.w_MDCODUBI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODCOM
  func Link_2_104(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MDCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNCODVAL,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MDCODCOM))
          select CNCODCAN,CNCODVAL,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMDCODCOM_2_104'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNCODVAL,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNCODVAL,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNCODVAL,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MDCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MDCODCOM)
            select CNCODCAN,CNCODVAL,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_COCODVAL = NVL(_Link_.CNCODVAL,space(3))
      this.w_DTOCOM = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODCOM = space(15)
      endif
      this.w_COCODVAL = space(3)
      this.w_DTOCOM = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOCOM,.w_OBTEST,"Codice Commessa obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_MDCODCOM = space(15)
        this.w_COCODVAL = space(3)
        this.w_DTOCOM = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_104(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_104.CNCODCAN as CNCODCAN304"+ ",link_2_104.CNCODVAL as CNCODVAL304"+ ",link_2_104.CNDTOBSO as CNDTOBSO304"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_104 on CORDRISP.MDCODCOM=link_2_104.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_104"
          i_cKey=i_cKey+'+" and CORDRISP.MDCODCOM=link_2_104.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDLOTMAG
  func Link_2_117(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_lTable = "SALDILOT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2], .t., this.SALDILOT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDLOTMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDLOTMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SUCODART,SUCODUBI,SUCODLOT,SUCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where SUCODMAG="+cp_ToStrODBC(this.w_MDLOTMAG);
                   +" and SUCODART="+cp_ToStrODBC(this.w_MDCODART);
                   +" and SUCODUBI="+cp_ToStrODBC(this.w_MDCODUBI);
                   +" and SUCODLOT="+cp_ToStrODBC(this.w_MDCODLOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SUCODART',this.w_MDCODART;
                       ,'SUCODUBI',this.w_MDCODUBI;
                       ,'SUCODLOT',this.w_MDCODLOT;
                       ,'SUCODMAG',this.w_MDLOTMAG)
            select SUCODART,SUCODUBI,SUCODLOT,SUCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
    else
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])+'\'+cp_ToStr(_Link_.SUCODART,1)+'\'+cp_ToStr(_Link_.SUCODUBI,1)+'\'+cp_ToStr(_Link_.SUCODLOT,1)+'\'+cp_ToStr(_Link_.SUCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDILOT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDLOTMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODPAG
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_MDCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PASCONTO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_MDCODPAG))
          select PACODICE,PASCONTO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oMDCODPAG_4_9'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PASCONTO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PASCONTO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PASCONTO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_MDCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_MDCODPAG)
            select PACODICE,PASCONTO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_SCOPAG = NVL(_Link_.PASCONTO,0)
    else
      if i_cCtrl<>'Load'
        this.w_MDCODPAG = space(5)
      endif
      this.w_SCOPAG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_9.PACODICE as PACODICE409"+ ",link_4_9.PASCONTO as PASCONTO409"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_9 on COR_RISP.MDCODPAG=link_4_9.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_9"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODPAG=link_4_9.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODCAR
  func Link_4_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
    i_lTable = "CAR_CRED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2], .t., this.CAR_CRED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACA',True,'CAR_CRED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_MDCODCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_MDCODCAR))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODCAR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODCAR) and !this.bDontReportError
            deferred_cp_zoom('CAR_CRED','*','CCCODICE',cp_AbsName(oSource.parent,'oMDCODCAR_4_19'),i_cWhere,'GSPS_ACA',"Carte di credito",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_MDCODCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_MDCODCAR)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODCAR = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODCAR = space(5)
      endif
      this.w_CCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAR_CRED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAR_CRED_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_19.CCCODICE as CCCODICE419"+ ",link_4_19.CCDESCRI as CCDESCRI419"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_19 on COR_RISP.MDCODCAR=link_4_19.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_19"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODCAR=link_4_19.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODFIN
  func Link_4_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAR_CRED_IDX,3]
    i_lTable = "CAR_CRED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2], .t., this.CAR_CRED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACA',True,'CAR_CRED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_MDCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_MDCODFIN))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODFIN)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODFIN) and !this.bDontReportError
            deferred_cp_zoom('CAR_CRED','*','CCCODICE',cp_AbsName(oSource.parent,'oMDCODFIN_4_22'),i_cWhere,'GSPS_ACA',"Carte di credito/finanziamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_MDCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_MDCODFIN)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODFIN = NVL(_Link_.CCCODICE,space(5))
      this.w_DESFIN = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODFIN = space(5)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAR_CRED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAR_CRED_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAR_CRED_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_22.CCCODICE as CCCODICE422"+ ",link_4_22.CCDESCRI as CCDESCRI422"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_22 on COR_RISP.MDCODFIN=link_4_22.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_22"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODFIN=link_4_22.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCODCLI
  func Link_4_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACV',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_MDCODCLI)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLCODCON,CLTELFAX,CL_EMAIL,CLFLFIDO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_MDCODCLI))
          select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLCODCON,CLTELFAX,CL_EMAIL,CLFLFIDO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCODCLI)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oMDCODCLI_4_35'),i_cWhere,'GSPS_ACV',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLCODCON,CLTELFAX,CL_EMAIL,CLFLFIDO";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLCODCON,CLTELFAX,CL_EMAIL,CLFLFIDO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLCODCON,CLTELFAX,CL_EMAIL,CLFLFIDO";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_MDCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_MDCODCLI)
            select CLCODCLI,CLDESCRI,CLSCONT1,CLSCONT2,CLCODPAG,CLCATSCM,CLVALFID,CLFIDUTI,CLCATCOM,CLCODLIS,CLCODCON,CLTELFAX,CL_EMAIL,CLFLFIDO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODCLI = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESCLI = NVL(_Link_.CLDESCRI,space(40))
      this.w_SCOCL1 = NVL(_Link_.CLSCONT1,0)
      this.w_SCOCL2 = NVL(_Link_.CLSCONT2,0)
      this.w_PAGCLI = NVL(_Link_.CLCODPAG,space(5))
      this.w_CATSCC = NVL(_Link_.CLCATSCM,space(5))
      this.w_VALFID = NVL(_Link_.CLVALFID,0)
      this.w_FIDUTI = NVL(_Link_.CLFIDUTI,0)
      this.w_CATCOM = NVL(_Link_.CLCATCOM,space(3))
      this.w_LISCLI = NVL(_Link_.CLCODLIS,space(5))
      this.w_CODCLI = NVL(_Link_.CLCODCON,space(15))
      this.w_TELFAX = NVL(_Link_.CLTELFAX,space(18))
      this.w_EMAIL = NVL(_Link_.CL_EMAIL,space(50))
      this.w_FLFIDO = NVL(_Link_.CLFLFIDO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_SCOCL1 = 0
      this.w_SCOCL2 = 0
      this.w_PAGCLI = space(5)
      this.w_CATSCC = space(5)
      this.w_VALFID = 0
      this.w_FIDUTI = 0
      this.w_CATCOM = space(3)
      this.w_LISCLI = space(5)
      this.w_CODCLI = space(15)
      this.w_TELFAX = space(18)
      this.w_EMAIL = space(50)
      this.w_FLFIDO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLINTE<>"C" or not empty(.w_MDCODCLI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Impostare il cliente")
        endif
        this.w_MDCODCLI = space(15)
        this.w_DESCLI = space(40)
        this.w_SCOCL1 = 0
        this.w_SCOCL2 = 0
        this.w_PAGCLI = space(5)
        this.w_CATSCC = space(5)
        this.w_VALFID = 0
        this.w_FIDUTI = 0
        this.w_CATCOM = space(3)
        this.w_LISCLI = space(5)
        this.w_CODCLI = space(15)
        this.w_TELFAX = space(18)
        this.w_EMAIL = space(50)
        this.w_FLFIDO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLI_VEND_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_35.CLCODCLI as CLCODCLI435"+ ",link_4_35.CLDESCRI as CLDESCRI435"+ ",link_4_35.CLSCONT1 as CLSCONT1435"+ ",link_4_35.CLSCONT2 as CLSCONT2435"+ ",link_4_35.CLCODPAG as CLCODPAG435"+ ",link_4_35.CLCATSCM as CLCATSCM435"+ ",link_4_35.CLVALFID as CLVALFID435"+ ",link_4_35.CLFIDUTI as CLFIDUTI435"+ ",link_4_35.CLCATCOM as CLCATCOM435"+ ",link_4_35.CLCODLIS as CLCODLIS435"+ ",link_4_35.CLCODCON as CLCODCON435"+ ",link_4_35.CLTELFAX as CLTELFAX435"+ ",link_4_35.CL_EMAIL as CL_EMAIL435"+ ",link_4_35.CLFLFIDO as CLFLFIDO435"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_35 on COR_RISP.MDCODCLI=link_4_35.CLCODCLI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_35"
          i_cKey=i_cKey+'+" and COR_RISP.MDCODCLI=link_4_35.CLCODCLI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDTIPDOC
  func Link_4_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsve_atd',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MDTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDPRODOC,TDFLIMPA,TDFLCRIS,TDFLANAL,TDFLCOMM,TDLOTDIF,TDFLINTE,TDCAUCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MDTIPDOC))
          select TDTIPDOC,TDPRODOC,TDFLIMPA,TDFLCRIS,TDFLANAL,TDFLCOMM,TDLOTDIF,TDFLINTE,TDCAUCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDPRODOC like "+cp_ToStrODBC(trim(this.w_MDTIPDOC)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDPRODOC,TDFLIMPA,TDFLCRIS,TDFLANAL,TDFLCOMM,TDLOTDIF,TDFLINTE,TDCAUCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDPRODOC like "+cp_ToStr(trim(this.w_MDTIPDOC)+"%");

            select TDTIPDOC,TDPRODOC,TDFLIMPA,TDFLCRIS,TDFLANAL,TDFLCOMM,TDLOTDIF,TDFLINTE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MDTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMDTIPDOC_4_37'),i_cWhere,'gsve_atd',"Tipo documento",'gsps0mdv.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDPRODOC,TDFLIMPA,TDFLCRIS,TDFLANAL,TDFLCOMM,TDLOTDIF,TDFLINTE,TDCAUCON";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDPRODOC,TDFLIMPA,TDFLCRIS,TDFLANAL,TDFLCOMM,TDLOTDIF,TDFLINTE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDPRODOC,TDFLIMPA,TDFLCRIS,TDFLANAL,TDFLCOMM,TDLOTDIF,TDFLINTE,TDCAUCON";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MDTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MDTIPDOC)
            select TDTIPDOC,TDPRODOC,TDFLIMPA,TDFLCRIS,TDFLANAL,TDFLCOMM,TDLOTDIF,TDFLINTE,TDCAUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_MDPRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_FLIMPA = NVL(_Link_.TDFLIMPA,space(1))
      this.w_FLRISC = NVL(_Link_.TDFLCRIS,space(1))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLGCOM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_LOTDIF = NVL(_Link_.TDLOTDIF,space(1))
      this.w_TDFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_TDCAUCON = NVL(_Link_.TDCAUCON,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MDTIPDOC = space(5)
      endif
      this.w_MDPRD = space(2)
      this.w_FLIMPA = space(1)
      this.w_FLRISC = space(1)
      this.w_FLANAL = space(1)
      this.w_FLGCOM = space(1)
      this.w_LOTDIF = space(1)
      this.w_TDFLINTE = space(1)
      this.w_TDCAUCON = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=not .w_EDTIPDOC OR EMPTY(.w_zMDTIPDOC) OR inlist(.w_MDTIPDOC, .w_oMDTIPDOC, .w_zMDTIPDOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo documento non coerente")
        endif
        this.w_MDTIPDOC = space(5)
        this.w_MDPRD = space(2)
        this.w_FLIMPA = space(1)
        this.w_FLRISC = space(1)
        this.w_FLANAL = space(1)
        this.w_FLGCOM = space(1)
        this.w_LOTDIF = space(1)
        this.w_TDFLINTE = space(1)
        this.w_TDCAUCON = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_37(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_37.TDTIPDOC as TDTIPDOC437"+ ",link_4_37.TDPRODOC as TDPRODOC437"+ ",link_4_37.TDFLIMPA as TDFLIMPA437"+ ",link_4_37.TDFLCRIS as TDFLCRIS437"+ ",link_4_37.TDFLANAL as TDFLANAL437"+ ",link_4_37.TDFLCOMM as TDFLCOMM437"+ ",link_4_37.TDLOTDIF as TDLOTDIF437"+ ",link_4_37.TDFLINTE as TDFLINTE437"+ ",link_4_37.TDCAUCON as TDCAUCON437"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_37 on COR_RISP.MDTIPDOC=link_4_37.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_37"
          i_cKey=i_cKey+'+" and COR_RISP.MDTIPDOC=link_4_37.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUCON
  func Link_4_114(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLRIFE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_TDCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_TDCAUCON)
            select CCCODICE,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_CCFLRIFE = NVL(_Link_.CCFLRIFE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCON = space(5)
      endif
      this.w_CCFLRIFE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMDSERIAL_1_10.value==this.w_MDSERIAL)
      this.oPgFrm.Page1.oPag.oMDSERIAL_1_10.value=this.w_MDSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDATREG_1_12.value==this.w_MDDATREG)
      this.oPgFrm.Page1.oPag.oMDDATREG_1_12.value=this.w_MDDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMDORAVEN_1_13.value==this.w_MDORAVEN)
      this.oPgFrm.Page1.oPag.oMDORAVEN_1_13.value=this.w_MDORAVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oMDMINVEN_1_14.value==this.w_MDMINVEN)
      this.oPgFrm.Page1.oPag.oMDMINVEN_1_14.value=this.w_MDMINVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODUTE_1_17.value==this.w_MDCODUTE)
      this.oPgFrm.Page1.oPag.oMDCODUTE_1_17.value=this.w_MDCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODNEG_1_20.value==this.w_MDCODNEG)
      this.oPgFrm.Page1.oPag.oMDCODNEG_1_20.value=this.w_MDCODNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oMDTIPVEN_1_21.value==this.w_MDTIPVEN)
      this.oPgFrm.Page1.oPag.oMDTIPVEN_1_21.value=this.w_MDTIPVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVEN_1_22.value==this.w_DESVEN)
      this.oPgFrm.Page1.oPag.oDESVEN_1_22.value=this.w_DESVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODMAG_1_34.value==this.w_MDCODMAG)
      this.oPgFrm.Page1.oPag.oMDCODMAG_1_34.value=this.w_MDCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODFID_1_35.value==this.w_MDCODFID)
      this.oPgFrm.Page1.oPag.oMDCODFID_1_35.value=this.w_MDCODFID
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODCLI_1_41.value==this.w_MDCODCLI)
      this.oPgFrm.Page1.oPag.oMDCODCLI_1_41.value=this.w_MDCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_42.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_42.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODLIS_1_44.value==this.w_MDCODLIS)
      this.oPgFrm.Page1.oPag.oMDCODLIS_1_44.value=this.w_MDCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMDNUMSCO_1_47.value==this.w_MDNUMSCO)
      this.oPgFrm.Page1.oPag.oMDNUMSCO_1_47.value=this.w_MDNUMSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oMDMATSCO_1_48.value==this.w_MDMATSCO)
      this.oPgFrm.Page1.oPag.oMDMATSCO_1_48.value=this.w_MDMATSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFID_1_115.value==this.w_DESFID)
      this.oPgFrm.Page1.oPag.oDESFID_1_115.value=this.w_DESFID
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODART_2_8.value==this.w_MDCODART)
      this.oPgFrm.Page1.oPag.oMDCODART_2_8.value=this.w_MDCODART
      replace t_MDCODART with this.oPgFrm.Page1.oPag.oMDCODART_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODREP_2_28.value==this.w_MDCODREP)
      this.oPgFrm.Page1.oPag.oMDCODREP_2_28.value=this.w_MDCODREP
      replace t_MDCODREP with this.oPgFrm.Page1.oPag.oMDCODREP_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODIVA_2_29.value==this.w_MDCODIVA)
      this.oPgFrm.Page1.oPag.oMDCODIVA_2_29.value=this.w_MDCODIVA
      replace t_MDCODIVA with this.oPgFrm.Page1.oPag.oMDCODIVA_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDSCONT2_2_30.value==this.w_MDSCONT2)
      this.oPgFrm.Page1.oPag.oMDSCONT2_2_30.value=this.w_MDSCONT2
      replace t_MDSCONT2 with this.oPgFrm.Page1.oPag.oMDSCONT2_2_30.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDSCONT3_2_31.value==this.w_MDSCONT3)
      this.oPgFrm.Page1.oPag.oMDSCONT3_2_31.value=this.w_MDSCONT3
      replace t_MDSCONT3 with this.oPgFrm.Page1.oPag.oMDSCONT3_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDSCONT4_2_32.value==this.w_MDSCONT4)
      this.oPgFrm.Page1.oPag.oMDSCONT4_2_32.value=this.w_MDSCONT4
      replace t_MDSCONT4 with this.oPgFrm.Page1.oPag.oMDSCONT4_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDFLOMAG_2_33.RadioValue()==this.w_MDFLOMAG)
      this.oPgFrm.Page1.oPag.oMDFLOMAG_2_33.SetRadio()
      replace t_MDFLOMAG with this.oPgFrm.Page1.oPag.oMDFLOMAG_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDSCOVEN_2_37.value==this.w_MDSCOVEN)
      this.oPgFrm.Page1.oPag.oMDSCOVEN_2_37.value=this.w_MDSCOVEN
      replace t_MDSCOVEN with this.oPgFrm.Page1.oPag.oMDSCOVEN_2_37.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVALRIG_2_51.value==this.w_VALRIG)
      this.oPgFrm.Page1.oPag.oVALRIG_2_51.value=this.w_VALRIG
      replace t_VALRIG with this.oPgFrm.Page1.oPag.oVALRIG_2_51.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIVA_2_53.value==this.w_PERIVA)
      this.oPgFrm.Page1.oPag.oPERIVA_2_53.value=this.w_PERIVA
      replace t_PERIVA with this.oPgFrm.Page1.oPag.oPERIVA_2_53.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDSCOPRO_2_66.value==this.w_MDSCOPRO)
      this.oPgFrm.Page1.oPag.oMDSCOPRO_2_66.value=this.w_MDSCOPRO
      replace t_MDSCOPRO with this.oPgFrm.Page1.oPag.oMDSCOPRO_2_66.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREP_2_76.value==this.w_DESREP)
      this.oPgFrm.Page1.oPag.oDESREP_2_76.value=this.w_DESREP
      replace t_DESREP with this.oPgFrm.Page1.oPag.oDESREP_2_76.value
    endif
    if not(this.oPgFrm.Page1.oPag.oQTDISP_2_77.value==this.w_QTDISP)
      this.oPgFrm.Page1.oPag.oQTDISP_2_77.value=this.w_QTDISP
      replace t_QTDISP with this.oPgFrm.Page1.oPag.oQTDISP_2_77.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODLOT_2_100.value==this.w_MDCODLOT)
      this.oPgFrm.Page1.oPag.oMDCODLOT_2_100.value=this.w_MDCODLOT
      replace t_MDCODLOT with this.oPgFrm.Page1.oPag.oMDCODLOT_2_100.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODUBI_2_101.value==this.w_MDCODUBI)
      this.oPgFrm.Page1.oPag.oMDCODUBI_2_101.value=this.w_MDCODUBI
      replace t_MDCODUBI with this.oPgFrm.Page1.oPag.oMDCODUBI_2_101.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCODCOM_2_104.value==this.w_MDCODCOM)
      this.oPgFrm.Page1.oPag.oMDCODCOM_2_104.value=this.w_MDCODCOM
      replace t_MDCODCOM with this.oPgFrm.Page1.oPag.oMDCODCOM_2_104.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_1.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_1.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page2.oPag.oMDTOTVEN_4_1.value==this.w_MDTOTVEN)
      this.oPgFrm.Page2.oPag.oMDTOTVEN_4_1.value=this.w_MDTOTVEN
    endif
    if not(this.oPgFrm.Page2.oPag.oMDPUNFID_4_2.value==this.w_MDPUNFID)
      this.oPgFrm.Page2.oPag.oMDPUNFID_4_2.value=this.w_MDPUNFID
    endif
    if not(this.oPgFrm.Page2.oPag.oMDSTOPRO_4_3.value==this.w_MDSTOPRO)
      this.oPgFrm.Page2.oPag.oMDSTOPRO_4_3.value=this.w_MDSTOPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oMDSCOCL1_4_4.value==this.w_MDSCOCL1)
      this.oPgFrm.Page2.oPag.oMDSCOCL1_4_4.value=this.w_MDSCOCL1
    endif
    if not(this.oPgFrm.Page2.oPag.oMDSCOCL2_4_5.value==this.w_MDSCOCL2)
      this.oPgFrm.Page2.oPag.oMDSCOCL2_4_5.value=this.w_MDSCOCL2
    endif
    if not(this.oPgFrm.Page2.oPag.oMDSCOPAG_4_6.value==this.w_MDSCOPAG)
      this.oPgFrm.Page2.oPag.oMDSCOPAG_4_6.value=this.w_MDSCOPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oMDSCONTI_4_7.value==this.w_MDSCONTI)
      this.oPgFrm.Page2.oPag.oMDSCONTI_4_7.value=this.w_MDSCONTI
    endif
    if not(this.oPgFrm.Page2.oPag.oMDFLFOSC_4_8.RadioValue()==this.w_MDFLFOSC)
      this.oPgFrm.Page2.oPag.oMDFLFOSC_4_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMDCODPAG_4_9.value==this.w_MDCODPAG)
      this.oPgFrm.Page2.oPag.oMDCODPAG_4_9.value=this.w_MDCODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oMDIMPPRE_4_11.value==this.w_MDIMPPRE)
      this.oPgFrm.Page2.oPag.oMDIMPPRE_4_11.value=this.w_MDIMPPRE
    endif
    if not(this.oPgFrm.Page2.oPag.oMDTOTDOC_4_12.value==this.w_MDTOTDOC)
      this.oPgFrm.Page2.oPag.oMDTOTDOC_4_12.value=this.w_MDTOTDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oMDACCPRE_4_13.value==this.w_MDACCPRE)
      this.oPgFrm.Page2.oPag.oMDACCPRE_4_13.value=this.w_MDACCPRE
    endif
    if not(this.oPgFrm.Page2.oPag.oMDPAGCON_4_15.value==this.w_MDPAGCON)
      this.oPgFrm.Page2.oPag.oMDPAGCON_4_15.value=this.w_MDPAGCON
    endif
    if not(this.oPgFrm.Page2.oPag.oMDPAGASS_4_16.value==this.w_MDPAGASS)
      this.oPgFrm.Page2.oPag.oMDPAGASS_4_16.value=this.w_MDPAGASS
    endif
    if not(this.oPgFrm.Page2.oPag.oMDRIFASS_4_17.value==this.w_MDRIFASS)
      this.oPgFrm.Page2.oPag.oMDRIFASS_4_17.value=this.w_MDRIFASS
    endif
    if not(this.oPgFrm.Page2.oPag.oMDPAGCAR_4_18.value==this.w_MDPAGCAR)
      this.oPgFrm.Page2.oPag.oMDPAGCAR_4_18.value=this.w_MDPAGCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oMDCODCAR_4_19.value==this.w_MDCODCAR)
      this.oPgFrm.Page2.oPag.oMDCODCAR_4_19.value=this.w_MDCODCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oMDPAGFIN_4_20.value==this.w_MDPAGFIN)
      this.oPgFrm.Page2.oPag.oMDPAGFIN_4_20.value=this.w_MDPAGFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESCRI_4_21.value==this.w_CCDESCRI)
      this.oPgFrm.Page2.oPag.oCCDESCRI_4_21.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oMDCODFIN_4_22.value==this.w_MDCODFIN)
      this.oPgFrm.Page2.oPag.oMDCODFIN_4_22.value=this.w_MDCODFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFIN_4_23.value==this.w_DESFIN)
      this.oPgFrm.Page2.oPag.oDESFIN_4_23.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMDPAGCLI_4_28.value==this.w_MDPAGCLI)
      this.oPgFrm.Page2.oPag.oMDPAGCLI_4_28.value=this.w_MDPAGCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oMDIMPABB_4_29.value==this.w_MDIMPABB)
      this.oPgFrm.Page2.oPag.oMDIMPABB_4_29.value=this.w_MDIMPABB
    endif
    if not(this.oPgFrm.Page2.oPag.oMDTIPCHI_4_31.RadioValue()==this.w_MDTIPCHI)
      this.oPgFrm.Page2.oPag.oMDTIPCHI_4_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMDCODCLI_4_35.value==this.w_MDCODCLI)
      this.oPgFrm.Page2.oPag.oMDCODCLI_4_35.value=this.w_MDCODCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_4_36.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_4_36.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oMDTIPDOC_4_37.value==this.w_MDTIPDOC)
      this.oPgFrm.Page2.oPag.oMDTIPDOC_4_37.value=this.w_MDTIPDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oMDALFDOC_4_42.value==this.w_MDALFDOC)
      this.oPgFrm.Page2.oPag.oMDALFDOC_4_42.value=this.w_MDALFDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oMDFLSALD_4_60.RadioValue()==this.w_MDFLSALD)
      this.oPgFrm.Page2.oPag.oMDFLSALD_4_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODICE_2_6.value==this.w_MDCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODICE_2_6.value=this.w_MDCODICE
      replace t_MDCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODICE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDDESART_2_7.value==this.w_MDDESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDDESART_2_7.value=this.w_MDDESART
      replace t_MDDESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDDESART_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_10.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_10.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDUNIMIS_2_20.value==this.w_MDUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDUNIMIS_2_20.value=this.w_MDUNIMIS
      replace t_MDUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDUNIMIS_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDQTAMOV_2_22.value==this.w_MDQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDQTAMOV_2_22.value=this.w_MDQTAMOV
      replace t_MDQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDQTAMOV_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDPREZZO_2_25.value==this.w_MDPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDPREZZO_2_25.value=this.w_MDPREZZO
      replace t_MDPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDPREZZO_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDSCONT1_2_26.value==this.w_MDSCONT1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDSCONT1_2_26.value=this.w_MDSCONT1
      replace t_MDSCONT1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDSCONT1_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDFLRESO_2_27.RadioValue()==this.w_MDFLRESO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDFLRESO_2_27.SetRadio()
      replace t_MDFLRESO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDFLRESO_2_27.value
    endif
    cp_SetControlsValueExtFlds(this,'COR_RISP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MDDATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDDATREG_1_12.SetFocus()
            i_bnoObbl = !empty(.w_MDDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MDORAVEN) or not(VAL(.w_MDORAVEN)<24))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDORAVEN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_MDORAVEN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora vendita non corretta")
          case   (empty(.w_MDMINVEN) or not(VAL(.w_MDMINVEN)<60))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDMINVEN_1_14.SetFocus()
            i_bnoObbl = !empty(.w_MDMINVEN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuto vendita non corretto")
          case   (empty(.w_MDCODUTE) or not(.w_MDCODOPE<>0))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDCODUTE_1_17.SetFocus()
            i_bnoObbl = !empty(.w_MDCODUTE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice utente non definito")
          case   (empty(.w_MDTIPVEN) or not(.w_MDCODNEG=g_CODNEG AND PSCHKMOD(.w_DOCDDT, .w_MDTIPCHI)))  and (EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDTIPVEN_1_21.SetFocus()
            i_bnoObbl = !empty(.w_MDTIPVEN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Modalit� di vendita inesistente o incongruente")
          case   not(EMPTY(.w_MDCODFID) OR ((.w_MDDATREG>=.w_FDATATT OR EMPTY(.w_FDATATT)) AND (.w_MDDATREG<=.w_FDATSCA OR EMPTY(.w_FDATSCA))))  and (EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR))  and not(empty(.w_MDCODFID))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDCODFID_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Fidelity Card inesistente o fuori validit�")
          case   not((.w_CLOBSO>.w_MDDATREG OR EMPTY(.w_CLOBSO)))  and (EMPTY(.w_MDCODFID) AND EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR))  and not(empty(.w_MDCODCLI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDCODCLI_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cliente inesistente, obsoleto o incongruente")
          case   not(.w_LISVAL=.w_MDCODVAL)  and not(empty(.w_MDCODLIS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDCODLIS_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino inesistente o valuta incongruente")
          case   (empty(.w_MDCODPAG))  and (.w_MDPAGCLI<>0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oMDCODPAG_4_9.SetFocus()
            i_bnoObbl = !empty(.w_MDCODPAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente")
          case   not(.w_MDIMPPRE <= .w_FIMPRES Or .cFunction = 'Query')  and (.w_FLFAFI<>'S' Or .cFunction = 'Load')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oMDIMPPRE_4_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Superato importo residuo")
          case   not(.w_TDFLINTE<>"C" or not empty(.w_MDCODCLI))  and (.w_EDCL3KEU OR EMPTY(.w_MDCODFID) AND .w_MDTIPCHI $ 'RF-FI-FF-RS-DT' AND EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR))  and not(empty(.w_MDCODCLI))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oMDCODCLI_4_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare il cliente")
          case   not(not .w_EDTIPDOC OR EMPTY(.w_zMDTIPDOC) OR inlist(.w_MDTIPDOC, .w_oMDTIPDOC, .w_zMDTIPDOC))  and (.w_EDTIPDOC)  and not(empty(.w_MDTIPDOC))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oMDTIPDOC_4_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo documento non coerente")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_mvd
      if i_bRes AND .oPgFrm.ActivePage<>2
         *** Salta alla Pagina Dati Chiusura
         i_bnoChk = .t.
         i_bRes = .f.
         .oPgFrm.ActivePage=2
         .oPgFrm.Click()
         Local controllo
         controllo = this.GETCTRL('w_MDSCOCL1')
         controllo.SetFocus()
      endif
      if i_bRes AND (EMPTY(.w_MDTIPDOC) OR .w_EDTIPDOC AND NOT inlist(.w_MDTIPDOC, .w_oMDTIPDOC, .w_zMDTIPDOC))
         i_cErrorMsg =Ah_MsgFormat("Tipo documento non definito oppure non coerente")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if i_bRes AND .w_MDTOTVEN<0
         i_cErrorMsg = Ah_MsgFormat("Totale negativo. %0Impossibile chiudere la vendita")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if i_bRes AND .w_MDTOTVEN=0 AND .w_MDTOTDOC<>0
         i_cErrorMsg = Ah_MsgFormat("Impossibile confermare la vendita: sconti/maggiorazioni su totale a 0")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if i_bRes AND .w_MDIMPABB>0 AND .w_MDFLSALD<>'S'
         i_cErrorMsg =Ah_MsgFormat( "Esiste un importo residuo. Specificare se a saldo (no pag.c./cliente) o riportare nei pagamenti")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if i_bRes AND EMPTY(.w_MDCODUTE)
         i_cErrorMsg = Ah_MsgFormat("Codice operatore non definito")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if i_bRes AND .cFunction='Load' AND NOT (.w_MDPRD<>'NN' AND .w_MDTIPCHI $ 'RF-FI-FF-RS-DT')
        .w_MDNUMDOC=0
      endif
      if i_bRes AND EMPTY(.w_MDCODMAG)
         i_cErrorMsg =Ah_MsgFormat( "Codice magazzino non definito")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if i_bRes AND EMPTY(.w_MDCODPAG) And .w_MDPAGCLI<>0
         i_cErrorMsg = Ah_MsgFormat("Codice pagamento non definito")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if i_bRes AND .w_MDTIPCHI $ 'RF-FI-FF-RS-DT'
         do case
           case empty(.w_MDCODCLI)
             i_cErrorMsg =Ah_MsgFormat("Codice cliente non definito")
             i_bRes = .f.
             i_bnoChk = .f.
           case .w_MDTIPCHI $ 'FI-FF-RS-DT'
             *** Verifica Inserimento Cliente in Anagrafica
             .w_ERRORE=.f.
             .NotifyEvent('VerificaCliente')
             if .w_ERRORE
               i_cErrorMsg = Ah_MsgFormat("Cliente negozio non registrato")
               i_bRes = .f.
               i_bnoChk = .f.
             endif
         endcase
      endif
      if i_bRes AND .w_CCFLRIFE="C" and empty(.w_MDCODCLI)
         i_cErrorMsg = Ah_MsgFormat("La causale contabile richiede un codice cliente")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      
      if i_bRes
          ah_Msg('Check finali...',.T.)
           .NotifyEvent('ChkFinali')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      
      * --- Memorizzo il risultato della checkform e checkrow
      this.w_bRes = i_bRes
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(PSCHKART(.w_MDCODART, .w_MDCODICE,  .w_MDDATREG, .w_CODESC, .w_CODCLI, THIS)) and not(empty(.w_MDCODICE)) and (NOT EMPTY(.w_MDCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODICE_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("%w_ERRORCOD%")
        case   (empty(.w_MDUNIMIS) or not(CHKUNIMI(IIF((.w_MDTIPRIG='M' AND .w_MDUNIMIS<>'   ') or .w_FLSERG='S','***',.w_MDUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_MDUNIMIS))) and (.w_MDTIPRIG $ 'RM' AND NOT EMPTY(.w_MDCODICE)  AND (EMPTY(.w_MDUNIMIS) OR (EMPTY(.w_MDSERRIF) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S') AND .w_UM1<>'S'))) and (NOT EMPTY(.w_MDCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDUNIMIS_2_20
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   (empty(.w_MDQTAMOV) or not((.w_MDQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_MDQTAMOV=INT(.w_MDQTAMOV))) OR NOT .w_MDTIPRIG $ 'RM')) and (.w_MDTIPRIG $ 'RMA'  AND NOT EMPTY(.w_MDCODICE) AND (EMPTY(.w_MDQTAMOV) OR .w_QT1<>'S')) and (NOT EMPTY(.w_MDCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDQTAMOV_2_22
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        case   (empty(.w_MDCODREP) or not(.w_MDTIPRIG='D' OR NOT EMPTY(.w_MDCODIVA))) and (.w_MDTIPRIG<>'D') and (NOT EMPTY(.w_MDCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oMDCODREP_2_28
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice reparto inesistente o codice IVA non definito")
        case   not((.w_FLSTAT<>'S' AND (Empty(.w_DATLOT) OR .w_DATLOT >.w_MDDATREG)) OR CHKLOTUB(.w_MDCODLOT,Space(10),0,.w_MDCODMAG,.w_MDCODART,Space(15),.w_MDFLLOTT, .w_FLSTAT,.w_LOTZOOM,.w_MDDATREG,' ','L','@@@@@@',' ',.F.,.w_DISLOT)) and (g_PERLOT='S' AND NOT EMPTY(.w_MDCODART) AND .w_FLLOTT$ 'SC' AND .w_MDFLLOTT $ '+-'  AND .w_MDQTAMOV<>0) and not(empty(.w_MDCODLOT)) and (NOT EMPTY(.w_MDCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oMDCODLOT_2_100
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        case   not((.w_MDFLLOTT='+') OR CHKLOTUB(.w_MDCODUBI,'',0,.w_MDCODMAG,.w_MDCODART,Space(15),.w_MDFLLOTT, .w_FLSTAT,.w_LOTZOOM,.w_MDDATREG,' ','U','     ','     ',.T.,.w_DISLOT)) and (g_PERUBI='S' AND NOT EMPTY(.w_MDCODART) AND NOT EMPTY(.w_MDCODMAG) AND .w_FLUBIC='S' AND (.w_MDFLLOTT $ '+-' Or NOT EMPTY(ALLTRIM(.w_MDFLCASC)) ) AND .w_MDQTAMOV<>0) and not(empty(.w_MDCODUBI)) and (NOT EMPTY(.w_MDCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oMDCODUBI_2_101
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        case   not(CHKDTOBS(.w_DTOCOM,.w_OBTEST,"Codice Commessa obsoleto!",.F.)) and (.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))) and not(empty(.w_MDCODCOM)) and (NOT EMPTY(.w_MDCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oMDCODCOM_2_104
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
      endcase
      i_bRes = i_bRes .and. .GSVE_MMT.CheckForm()
      if NOT EMPTY(.w_MDCODART)
        * --- Area Manuale = Check Row
        * --- gsps_mvd
        * --- Aggiorna Automatismi
        this.w_UM1=' '
        this.w_PZ1=' '
        this.w_QT1=' '
        this.w_SC1=' '
        this.w_DA1=' '
        select (this.cTrsname)
        replace t_UM1 with ' '
        replace t_PZ1 with ' '
        replace t_QT1 with ' '
        replace t_SC1 with ' '
        replace t_DA1 with ' '
        * -Controllo commessa
        if i_bRes AND ((g_PERCCR='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MDTIPRIG $ 'MRF'
           If EMPTY(.w_MDCODCOM) AND g_COMM='S' AND .w_FLGCOM='S'
              i_bRes = .f.
              i_bnoChk = .f.
        	    i_cErrorMsg = Ah_MsgFormat("Commessa non definita")
              *Local CtrlCommessa
              *CtrlCommessa=this.GetCtrl('w_MDCODCOM')
              *CtrlCommessa.SetFocus()
           endif
        endif
        
        IF i_bRes AND ((g_PERLOT='S' AND .w_FLLOTT $ 'SC') OR (g_PERUBI='S'AND .w_FLUBIC='S' ))AND(.w_MDFLCASC $'-+';
        AND ((.cFunction='Load') OR (.cFunction='Edit')) AND .w_FLDISP$ 'SC') AND .w_DISLOT ='S'
           i_bRes = CHKMOVLO(.w_MDCODLOT,.w_MDCODUBI,Space(20),.w_MDMAGRIG,Space(20),.w_MDFLLOTT,' ',;
           .w_MDQTAUM1-.w_OLDQTA,.w_MDCODART,.w_FLLOTT,.w_FLUBIC,' ',.w_FLSTAT,.w_MDSERIAL,'D',Space(10),NOT.w_TESLOT)
           i_bnoChk = .t.	
           i_cErrorMsg = ""
        ENDIF
        
        * -- Controllo: nel caso in cui viene passata al registratore la quantit�
        * -- non � possibile inserire prezzi e quantit� con pi� di due decimali
        * -- poich� i registratori ne gestiscono solo 2 e si potrebbero creare
        * -- differenze tra scontrino e vendita negozio
        If i_bRes And .w_PASQTA = 'S' And (.w_MDQTAMOV <> cp_Round(.w_MDQTAMOV, 2) Or .w_MDPREZZO <> cp_Round(.w_MDPREZZO, 2)) And .w_MDTIPRIG <>'D' And .w_MDTIPCHI = 'ES'
              i_bRes = .f.
              i_bnoChk = .f.
        	    i_cErrorMsg = Ah_MsgFormat("Impossibile utilizzare prezzo o quantit� con pi� di due decimali se attivo invio quantit� sul dispositivo cassa installato")
        Endif
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MATSCO = this.w_MATSCO
    this.o_MDSERIAL = this.w_MDSERIAL
    this.o_MDDATREG = this.w_MDDATREG
    this.o_MDTIPVEN = this.w_MDTIPVEN
    this.o_CLIVEN = this.w_CLIVEN
    this.o_MDCODMAG = this.w_MDCODMAG
    this.o_MDCODFID = this.w_MDCODFID
    this.o_MDCODCLI = this.w_MDCODCLI
    this.o_MDCODLIS = this.w_MDCODLIS
    this.o_MDCODICE = this.w_MDCODICE
    this.o_MDCODART = this.w_MDCODART
    this.o_MDQTAMOV = this.w_MDQTAMOV
    this.o_LIPREZZO = this.w_LIPREZZO
    this.o_MDPREZZO = this.w_MDPREZZO
    this.o_MDSCONT1 = this.w_MDSCONT1
    this.o_MDFLRESO = this.w_MDFLRESO
    this.o_MDCODREP = this.w_MDCODREP
    this.o_MDSCONT2 = this.w_MDSCONT2
    this.o_MDSCONT3 = this.w_MDSCONT3
    this.o_MDSCONT4 = this.w_MDSCONT4
    this.o_MDFLOMAG = this.w_MDFLOMAG
    this.o_MDMAGSAL = this.w_MDMAGSAL
    this.o_VALRIG = this.w_VALRIG
    this.o_PERIVA = this.w_PERIVA
    this.o_MDCODLOT = this.w_MDCODLOT
    this.o_MDCODUBI = this.w_MDCODUBI
    this.o_MDCODCOM = this.w_MDCODCOM
    this.o_MDTOTVEN = this.w_MDTOTVEN
    this.o_MDSTOPRO = this.w_MDSTOPRO
    this.o_MDSCOCL1 = this.w_MDSCOCL1
    this.o_MDSCOCL2 = this.w_MDSCOCL2
    this.o_MDSCOPAG = this.w_MDSCOPAG
    this.o_MDSCONTI = this.w_MDSCONTI
    this.o_MDFLFOSC = this.w_MDFLFOSC
    this.o_MDCODPAG = this.w_MDCODPAG
    this.o_MDTOTDOC = this.w_MDTOTDOC
    this.o_MDPAGASS = this.w_MDPAGASS
    this.o_MDPAGCAR = this.w_MDPAGCAR
    this.o_MDPAGFIN = this.w_MDPAGFIN
    this.o_MDTIPCHI = this.w_MDTIPCHI
    this.o_MDTIPDOC = this.w_MDTIPDOC
    this.o_MDTIPCOR = this.w_MDTIPCOR
    this.o_FLRICIVA = this.w_FLRICIVA
    this.o_RIIVDTIN = this.w_RIIVDTIN
    * --- GSVE_MMT : Depends On
    this.GSVE_MMT.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_MDCODART))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DESPOS=space(1)
      .w_CODVAA=space(3)
      .w_LOTDIF=space(1)
      .w_DA1=space(1)
      .w_UM1=space(1)
      .w_QT1=space(1)
      .w_PZ1=space(1)
      .w_SC1=space(1)
      .w_MDCODICE=space(20)
      .w_MDDESART=space(40)
      .w_MDCODART=space(20)
      .w_FLSERG=space(1)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MDTIPRIG=space(1)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_MOLTIP=0
      .w_MOLTI3=0
      .w_OPERAT=space(1)
      .w_OPERA3=space(1)
      .w_MDQTAUM1=0
      .w_MDUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_MDQTAMOV=0
      .w_LIPREZZO=0
      .w_MDCONTRA=space(15)
      .w_MDPREZZO=0
      .w_MDSCONT1=0
      .w_MDFLRESO=space(1)
      .w_MDCODREP=space(3)
      .w_MDCODIVA=space(5)
      .w_MDSCONT2=0
      .w_MDSCONT3=0
      .w_MDSCONT4=0
      .w_MDFLOMAG=space(1)
      .w_MDFLCASC=space(1)
      .w_MDMAGSAL=space(5)
      .w_ULTSCA=ctod("  /  /  ")
      .w_MDSCOVEN=0
      .w_ARCODART=space(20)
      .w_CATSCA=space(5)
      .w_GRUMER=space(5)
      .w_OFLSCO=space(1)
      .w_IMPNAZ=0
      .w_VALRIG=0
      .w_RIGNET=0
      .w_PERIVA=0
      .w_MDSERRIF=space(10)
      .w_MDNUMRIF=0
      .w_MDROWRIF=0
      .w_MDFLARIF=space(1)
      .w_MDFLERIF=space(1)
      .w_MDQTAIMP=0
      .w_MDQTAIM1=0
      .w_ORDEVA=0
      .w_OLDEVAS=space(1)
      .w_MDCODPRO=space(15)
      .w_MDFLESCL=space(1)
      .w_MDFLROMA=space(1)
      .w_MDSCOPRO=0
      .w_ARTPOS=space(1)
      .w_VALUCA=0
      .w_QTAPER=0
      .w_QTRPER=0
      .w_FLPAN=space(1)
      .w_OQTAIMP=0
      .w_OQTAIM1=0
      .w_OFLERIF=space(1)
      .w_OPREZZO=0
      .w_DESREP=space(40)
      .w_QTDISP=0
      .w_MDFLULPV=space(1)
      .w_MDVALULT=0
      .w_FLSERA=space(1)
      .w_MDTIPPRO=space(2)
      .w_MDPERPRO=0
      .w_NOCALQTA=.f.
      .w_LISCON=0
      .w_GESMAT=space(1)
      .w_COCODVAL=space(3)
      .w_MDIMPCOM=0
      .w_GIACON=space(1)
      .w_FLSTAT=space(1)
      .w_DATLOT=ctod("  /  /  ")
      .w_FLLOTT=space(1)
      .w_MDFLLOTT=space(1)
      .w_LOTZOOM=.f.
      .w_MDCODLOT=space(20)
      .w_MDCODUBI=space(20)
      .w_GESMAT=space(1)
      .w_MDCODCOM=space(15)
      .w_UBIZOOM=.f.
      .w_MTCARI=space(1)
      .w_MDMAGRIG=space(5)
      .w_OLDQTA=0
      .w_ARTDIS=space(1)
      .w_FLDISP=space(1)
      .w_TOTMAT=0
      .w_FLRRIF=space(1)
      .w_ORQTAEV1=0
      .w_DISLOT=space(1)
      .w_MDTIPPR2=space(2)
      .w_MDPROCAP=0
      .w_MDLOTMAG=space(5)
      .w_NOFRAZ=space(1)
      .w_MODUM2=space(1)
      .w_UNISEP=space(1)
      .w_PREZUM=space(1)
      .w_CLUNIMIS=space(1)
      .w_CODREP=space(3)
      .w_CODIVA=space(5)
      .w_DELNOMSG=.f.
      .w_FLCOM1=space(1)
      .w_ARCLAMAT=space(5)
      .w_MDPROMOZ=0
      .w_MDESPSCO=0
      .w_NUMDEQ=space(1)
      .DoRTCalc(1,111,.f.)
        .w_DA1 = IIF(.cFunction='Load', .w_FLGDES, ' ')
        .w_UM1 = IIF(.cFunction='Load', .w_FLUNIM, ' ')
        .w_QT1 = IIF(.cFunction='Load', iif(.w_QTADEF<>0,'S',' '), ' ')
        .w_PZ1 = IIF(.cFunction='Load', .w_FLPREZ, ' ')
        .w_SC1 = IIF(.cFunction='Load', .w_FLSCON, ' ')
      .DoRTCalc(117,117,.f.)
      if not(empty(.w_MDCODICE))
        .link_2_6('Full')
      endif
      .DoRTCalc(118,119,.f.)
      if not(empty(.w_MDCODART))
        .link_2_8('Full')
      endif
      .DoRTCalc(120,123,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_12('Full')
      endif
      .DoRTCalc(124,129,.f.)
        .w_MDQTAUM1 = IIF(.w_MDTIPRIG='F', 1, CALQTA(.w_MDQTAMOV,.w_MDUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_UNISEP, .w_NOFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        .w_MDUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
      .DoRTCalc(131,131,.f.)
      if not(empty(.w_MDUNIMIS))
        .link_2_20('Full')
      endif
      .DoRTCalc(132,132,.f.)
        .w_MDQTAMOV = IIF(.w_MDQTAMOV=0 and not empty(.w_MDCODICE) And Not .w_NOCALQTA,IIF(.w_MDTIPRIG='F', 1, IIF(.w_MDTIPRIG='D', 0, .w_QTADEF)),.w_MDQTAMOV)
      .DoRTCalc(134,135,.f.)
        .w_MDPREZZO = IIF((.w_MDTIPRIG $ 'RFMA'), CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MDUNIMIS+.w_OPERAT+.w_OPERA3+IIF(.w_LISCON=2, .w_LISIVA, 'N')+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1), 0), 0)
      .DoRTCalc(137,138,.f.)
        .w_MDCODREP = IIF(Empty(.w_DEFCODREP), .w_CODREP, .w_DEFCODREP)
      .DoRTCalc(139,139,.f.)
      if not(empty(.w_MDCODREP))
        .link_2_28('Full')
      endif
      .DoRTCalc(140,140,.f.)
      if not(empty(.w_MDCODIVA))
        .link_2_29('Full')
      endif
        .w_MDSCONT2 = 0
        .w_MDSCONT3 = 0
        .w_MDSCONT4 = 0
        .w_MDFLOMAG = 'X'
        .w_MDFLCASC = IIF(.w_MDFLRESO='S', '+', '-')
        .w_MDMAGSAL = IIF(.w_MDTIPRIG='R' AND .w_MDFLCASC<>' ', .w_MDCODMAG, SPACE(5))
      .DoRTCalc(146,146,.f.)
      if not(empty(.w_MDMAGSAL))
        .link_2_35('Full')
      endif
      .DoRTCalc(147,148,.f.)
        .w_ARCODART = .w_MDCODART
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_46.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_47.Calculate()
      .DoRTCalc(150,151,.f.)
        .w_OFLSCO = ' '
        .w_IMPNAZ = IIF(.w_VALRIG=0 Or .w_MDQTAUM1=0,0,cp_ROUND(.w_VALRIG/.w_MDQTAUM1,.w_DECTOT))
        .w_VALRIG = IIF(g_FLESSC='S' And .w_MDDATREG >= g_DATESC,CAVALRIG(.w_MDPREZZO,.w_MDQTAMOV, 0, 0, 0, 0,.w_DECTOT), CAVALRIG(.w_MDPREZZO,.w_MDQTAMOV, .w_MDSCONT1,.w_MDSCONT2,.w_MDSCONT3,.w_MDSCONT4,.w_DECTOT) )
        .w_RIGNET = IIF(.w_MDCODICE = .w_SERFID And .w_FLFAFI = 'S', 0, IIF(.w_MDFLOMAG='X', .w_VALRIG,IIF(.w_MDFLOMAG='I', .w_VALRIG - CALNET(.w_VALRIG,.w_PERIVA,.w_DECTOT,Space(3),0),0) ) )
      .DoRTCalc(156,157,.f.)
        .w_MDNUMRIF = -30
      .DoRTCalc(159,159,.f.)
      if not(empty(.w_MDROWRIF))
        .link_2_56('Full')
      endif
      .DoRTCalc(160,173,.f.)
        .w_FLPAN = ' '
        .w_OQTAIMP = .w_MDQTAIMP
        .w_OQTAIM1 = .w_MDQTAIM1
        .w_OFLERIF = .w_MDFLERIF
        .w_OPREZZO = .w_MDPREZZO
      .DoRTCalc(179,179,.f.)
        .w_QTDISP = .w_QTAPER-.w_QTRPER
        .oPgFrm.Page1.oPag.oObj_2_78.Calculate()
        .w_MDFLULPV = IIF(.w_MDFLCASC='-' AND .w_MDDATREG>=.w_ULTSCA AND .w_MDFLOMAG<>'S', '=', ' ')
        .w_MDVALULT = CALNET(.w_IMPNAZ,.w_PERIVA,.w_DECTOT,SPACE(5), 0)
        .oPgFrm.Page1.oPag.oObj_2_81.Calculate()
        .w_FLSERA = ' '
        .w_MDTIPPRO = IIF(.w_MDFLOMAG='X','DC','ES')
      .DoRTCalc(185,185,.f.)
        .w_NOCALQTA = .F.
        .oPgFrm.Page1.oPag.oObj_2_86.Calculate(IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)<>'#########' AND NOT EMPTY(.w_MDCODPRO),'Sc. Promozione:',IIF(.w_MDTIPRIG<>'D','Sc. Ventilato:',IIF(.w_MDTIPRIG='D' AND LEFT(.w_MDCODPRO,9)='#########' AND NOT EMPTY(.w_MDCODPRO),'Sconto:',''))))
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))=.T., 'Commessa:',''))
      .DoRTCalc(187,189,.f.)
        .w_COCODVAL = IIF(EMPTY(.w_COCODVAL), g_PERVAL, .w_COCODVAL)
      .DoRTCalc(190,190,.f.)
      if not(empty(.w_COCODVAL))
        .link_2_91('Full')
      endif
      .DoRTCalc(191,191,.f.)
        .w_MDIMPCOM = CAIMPCOM( IIF(Empty(.w_MDCODCOM),'S', ' '), g_PERVAL, .w_COCODVAL, .w_VALRIG, g_CAOEUR, .w_MDDATREG, .w_DECCOM )
        .w_GIACON = IIF(.cFunction='Load', ' ', 'X')
      .DoRTCalc(194,196,.f.)
        .w_MDFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_MDFLCASC),1),' ')
        .w_LOTZOOM = .T.
      .DoRTCalc(199,199,.f.)
      if not(empty(.w_MDCODLOT))
        .link_2_100('Full')
      endif
      .DoRTCalc(200,200,.f.)
      if not(empty(.w_MDCODUBI))
        .link_2_101('Full')
      endif
      .DoRTCalc(201,201,.f.)
        .w_MDCODCOM = IIF(.w_MDTIPRIG<>'D' And .w_FLANAL='S' And .w_FLGCOM='S', .w_OCOM, SPACE(15))
      .DoRTCalc(202,202,.f.)
      if not(empty(.w_MDCODCOM))
        .link_2_104('Full')
      endif
        .w_UBIZOOM = .T.
        .w_MTCARI = 'S'
        .w_MDMAGRIG = IIF(.w_MDTIPRIG='R',.w_MDCODMAG,Space(20) )
        .w_OLDQTA = .w_MDQTAUM1
      .DoRTCalc(207,207,.f.)
        .w_FLDISP = IIF(g_PERDIS='S', .w_ARTDIS, ' ')
        .w_TOTMAT = -1
      .DoRTCalc(210,212,.f.)
        .w_MDTIPPR2 = IIF(.w_MDFLOMAG='X','DC','ES')
      .DoRTCalc(214,214,.f.)
        .w_MDLOTMAG = iif( Empty( .w_MDCODLOT ) And Empty( .w_MDCODUBI ) , SPACE(5) , LEFT(.w_MDMAGSAL,5) )
      .DoRTCalc(215,215,.f.)
      if not(empty(.w_MDLOTMAG))
        .link_2_117('Full')
      endif
      .DoRTCalc(216,286,.f.)
        .w_DELNOMSG = .F.
    endwith
    this.DoRTCalc(288,301,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DESPOS = t_DESPOS
    this.w_CODVAA = t_CODVAA
    this.w_LOTDIF = t_LOTDIF
    this.w_DA1 = t_DA1
    this.w_UM1 = t_UM1
    this.w_QT1 = t_QT1
    this.w_PZ1 = t_PZ1
    this.w_SC1 = t_SC1
    this.w_MDCODICE = t_MDCODICE
    this.w_MDDESART = t_MDDESART
    this.w_MDCODART = t_MDCODART
    this.w_FLSERG = t_FLSERG
    this.w_CPROWORD = t_CPROWORD
    this.w_MDTIPRIG = t_MDTIPRIG
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTIP = t_MOLTIP
    this.w_MOLTI3 = t_MOLTI3
    this.w_OPERAT = t_OPERAT
    this.w_OPERA3 = t_OPERA3
    this.w_MDQTAUM1 = t_MDQTAUM1
    this.w_MDUNIMIS = t_MDUNIMIS
    this.w_FLFRAZ = t_FLFRAZ
    this.w_MDQTAMOV = t_MDQTAMOV
    this.w_LIPREZZO = t_LIPREZZO
    this.w_MDCONTRA = t_MDCONTRA
    this.w_MDPREZZO = t_MDPREZZO
    this.w_MDSCONT1 = t_MDSCONT1
    this.w_MDFLRESO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDFLRESO_2_27.RadioValue(.t.)
    this.w_MDCODREP = t_MDCODREP
    this.w_MDCODIVA = t_MDCODIVA
    this.w_MDSCONT2 = t_MDSCONT2
    this.w_MDSCONT3 = t_MDSCONT3
    this.w_MDSCONT4 = t_MDSCONT4
    this.w_MDFLOMAG = this.oPgFrm.Page1.oPag.oMDFLOMAG_2_33.RadioValue(.t.)
    this.w_MDFLCASC = t_MDFLCASC
    this.w_MDMAGSAL = t_MDMAGSAL
    this.w_ULTSCA = t_ULTSCA
    this.w_MDSCOVEN = t_MDSCOVEN
    this.w_ARCODART = t_ARCODART
    this.w_CATSCA = t_CATSCA
    this.w_GRUMER = t_GRUMER
    this.w_OFLSCO = t_OFLSCO
    this.w_IMPNAZ = t_IMPNAZ
    this.w_VALRIG = t_VALRIG
    this.w_RIGNET = t_RIGNET
    this.w_PERIVA = t_PERIVA
    this.w_MDSERRIF = t_MDSERRIF
    this.w_MDNUMRIF = t_MDNUMRIF
    this.w_MDROWRIF = t_MDROWRIF
    this.w_MDFLARIF = t_MDFLARIF
    this.w_MDFLERIF = t_MDFLERIF
    this.w_MDQTAIMP = t_MDQTAIMP
    this.w_MDQTAIM1 = t_MDQTAIM1
    this.w_ORDEVA = t_ORDEVA
    this.w_OLDEVAS = t_OLDEVAS
    this.w_MDCODPRO = t_MDCODPRO
    this.w_MDFLESCL = t_MDFLESCL
    this.w_MDFLROMA = t_MDFLROMA
    this.w_MDSCOPRO = t_MDSCOPRO
    this.w_ARTPOS = t_ARTPOS
    this.w_VALUCA = t_VALUCA
    this.w_QTAPER = t_QTAPER
    this.w_QTRPER = t_QTRPER
    this.w_FLPAN = t_FLPAN
    this.w_OQTAIMP = t_OQTAIMP
    this.w_OQTAIM1 = t_OQTAIM1
    this.w_OFLERIF = t_OFLERIF
    this.w_OPREZZO = t_OPREZZO
    this.w_DESREP = t_DESREP
    this.w_QTDISP = t_QTDISP
    this.w_MDFLULPV = t_MDFLULPV
    this.w_MDVALULT = t_MDVALULT
    this.w_FLSERA = t_FLSERA
    this.w_MDTIPPRO = t_MDTIPPRO
    this.w_MDPERPRO = t_MDPERPRO
    this.w_NOCALQTA = t_NOCALQTA
    this.w_LISCON = t_LISCON
    this.w_GESMAT = t_GESMAT
    this.w_COCODVAL = t_COCODVAL
    this.w_MDIMPCOM = t_MDIMPCOM
    this.w_GIACON = t_GIACON
    this.w_FLSTAT = t_FLSTAT
    this.w_DATLOT = t_DATLOT
    this.w_FLLOTT = t_FLLOTT
    this.w_MDFLLOTT = t_MDFLLOTT
    this.w_LOTZOOM = t_LOTZOOM
    this.w_MDCODLOT = t_MDCODLOT
    this.w_MDCODUBI = t_MDCODUBI
    this.w_GESMAT = t_GESMAT
    this.w_MDCODCOM = t_MDCODCOM
    this.w_UBIZOOM = t_UBIZOOM
    this.w_MTCARI = t_MTCARI
    this.w_MDMAGRIG = t_MDMAGRIG
    this.w_OLDQTA = t_OLDQTA
    this.w_ARTDIS = t_ARTDIS
    this.w_FLDISP = t_FLDISP
    this.w_TOTMAT = t_TOTMAT
    this.w_FLRRIF = t_FLRRIF
    this.w_ORQTAEV1 = t_ORQTAEV1
    this.w_DISLOT = t_DISLOT
    this.w_MDTIPPR2 = t_MDTIPPR2
    this.w_MDPROCAP = t_MDPROCAP
    this.w_MDLOTMAG = t_MDLOTMAG
    this.w_NOFRAZ = t_NOFRAZ
    this.w_MODUM2 = t_MODUM2
    this.w_UNISEP = t_UNISEP
    this.w_PREZUM = t_PREZUM
    this.w_CLUNIMIS = t_CLUNIMIS
    this.w_CODREP = t_CODREP
    this.w_CODIVA = t_CODIVA
    this.w_DELNOMSG = t_DELNOMSG
    this.w_FLCOM1 = t_FLCOM1
    this.w_ARCLAMAT = t_ARCLAMAT
    this.w_MDPROMOZ = t_MDPROMOZ
    this.w_MDESPSCO = t_MDESPSCO
    this.w_NUMDEQ = t_NUMDEQ
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DESPOS with this.w_DESPOS
    replace t_CODVAA with this.w_CODVAA
    replace t_LOTDIF with this.w_LOTDIF
    replace t_DA1 with this.w_DA1
    replace t_UM1 with this.w_UM1
    replace t_QT1 with this.w_QT1
    replace t_PZ1 with this.w_PZ1
    replace t_SC1 with this.w_SC1
    replace t_MDCODICE with this.w_MDCODICE
    replace t_MDDESART with this.w_MDDESART
    replace t_MDCODART with this.w_MDCODART
    replace t_FLSERG with this.w_FLSERG
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MDTIPRIG with this.w_MDTIPRIG
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTIP with this.w_MOLTIP
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_OPERAT with this.w_OPERAT
    replace t_OPERA3 with this.w_OPERA3
    replace t_MDQTAUM1 with this.w_MDQTAUM1
    replace t_MDUNIMIS with this.w_MDUNIMIS
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_MDQTAMOV with this.w_MDQTAMOV
    replace t_LIPREZZO with this.w_LIPREZZO
    replace t_MDCONTRA with this.w_MDCONTRA
    replace t_MDPREZZO with this.w_MDPREZZO
    replace t_MDSCONT1 with this.w_MDSCONT1
    replace t_MDFLRESO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDFLRESO_2_27.ToRadio()
    replace t_MDCODREP with this.w_MDCODREP
    replace t_MDCODIVA with this.w_MDCODIVA
    replace t_MDSCONT2 with this.w_MDSCONT2
    replace t_MDSCONT3 with this.w_MDSCONT3
    replace t_MDSCONT4 with this.w_MDSCONT4
    replace t_MDFLOMAG with this.oPgFrm.Page1.oPag.oMDFLOMAG_2_33.ToRadio()
    replace t_MDFLCASC with this.w_MDFLCASC
    replace t_MDMAGSAL with this.w_MDMAGSAL
    replace t_ULTSCA with this.w_ULTSCA
    replace t_MDSCOVEN with this.w_MDSCOVEN
    replace t_ARCODART with this.w_ARCODART
    replace t_CATSCA with this.w_CATSCA
    replace t_GRUMER with this.w_GRUMER
    replace t_OFLSCO with this.w_OFLSCO
    replace t_IMPNAZ with this.w_IMPNAZ
    replace t_VALRIG with this.w_VALRIG
    replace t_RIGNET with this.w_RIGNET
    replace t_PERIVA with this.w_PERIVA
    replace t_MDSERRIF with this.w_MDSERRIF
    replace t_MDNUMRIF with this.w_MDNUMRIF
    replace t_MDROWRIF with this.w_MDROWRIF
    replace t_MDFLARIF with this.w_MDFLARIF
    replace t_MDFLERIF with this.w_MDFLERIF
    replace t_MDQTAIMP with this.w_MDQTAIMP
    replace t_MDQTAIM1 with this.w_MDQTAIM1
    replace t_ORDEVA with this.w_ORDEVA
    replace t_OLDEVAS with this.w_OLDEVAS
    replace t_MDCODPRO with this.w_MDCODPRO
    replace t_MDFLESCL with this.w_MDFLESCL
    replace t_MDFLROMA with this.w_MDFLROMA
    replace t_MDSCOPRO with this.w_MDSCOPRO
    replace t_ARTPOS with this.w_ARTPOS
    replace t_VALUCA with this.w_VALUCA
    replace t_QTAPER with this.w_QTAPER
    replace t_QTRPER with this.w_QTRPER
    replace t_FLPAN with this.w_FLPAN
    replace t_OQTAIMP with this.w_OQTAIMP
    replace t_OQTAIM1 with this.w_OQTAIM1
    replace t_OFLERIF with this.w_OFLERIF
    replace t_OPREZZO with this.w_OPREZZO
    replace t_DESREP with this.w_DESREP
    replace t_QTDISP with this.w_QTDISP
    replace t_MDFLULPV with this.w_MDFLULPV
    replace t_MDVALULT with this.w_MDVALULT
    replace t_FLSERA with this.w_FLSERA
    replace t_MDTIPPRO with this.w_MDTIPPRO
    replace t_MDPERPRO with this.w_MDPERPRO
    replace t_NOCALQTA with this.w_NOCALQTA
    replace t_LISCON with this.w_LISCON
    replace t_GESMAT with this.w_GESMAT
    replace t_COCODVAL with this.w_COCODVAL
    replace t_MDIMPCOM with this.w_MDIMPCOM
    replace t_GIACON with this.w_GIACON
    replace t_FLSTAT with this.w_FLSTAT
    replace t_DATLOT with this.w_DATLOT
    replace t_FLLOTT with this.w_FLLOTT
    replace t_MDFLLOTT with this.w_MDFLLOTT
    replace t_LOTZOOM with this.w_LOTZOOM
    replace t_MDCODLOT with this.w_MDCODLOT
    replace t_MDCODUBI with this.w_MDCODUBI
    replace t_GESMAT with this.w_GESMAT
    replace t_MDCODCOM with this.w_MDCODCOM
    replace t_UBIZOOM with this.w_UBIZOOM
    replace t_MTCARI with this.w_MTCARI
    replace t_MDMAGRIG with this.w_MDMAGRIG
    replace t_OLDQTA with this.w_OLDQTA
    replace t_ARTDIS with this.w_ARTDIS
    replace t_FLDISP with this.w_FLDISP
    replace t_TOTMAT with this.w_TOTMAT
    replace t_FLRRIF with this.w_FLRRIF
    replace t_ORQTAEV1 with this.w_ORQTAEV1
    replace t_DISLOT with this.w_DISLOT
    replace t_MDTIPPR2 with this.w_MDTIPPR2
    replace t_MDPROCAP with this.w_MDPROCAP
    replace t_MDLOTMAG with this.w_MDLOTMAG
    replace t_NOFRAZ with this.w_NOFRAZ
    replace t_MODUM2 with this.w_MODUM2
    replace t_UNISEP with this.w_UNISEP
    replace t_PREZUM with this.w_PREZUM
    replace t_CLUNIMIS with this.w_CLUNIMIS
    replace t_CODREP with this.w_CODREP
    replace t_CODIVA with this.w_CODIVA
    replace t_DELNOMSG with this.w_DELNOMSG
    replace t_FLCOM1 with this.w_FLCOM1
    replace t_ARCLAMAT with this.w_ARCLAMAT
    replace t_MDPROMOZ with this.w_MDPROMOZ
    replace t_MDESPSCO with this.w_MDESPSCO
    replace t_NUMDEQ with this.w_NUMDEQ
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_valrig
        .w_MDTOTVEN = .w_MDTOTVEN-.w_rignet
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsps_mvdPag1 as StdContainer
  Width  = 791
  height = 485
  stdWidth  = 791
  stdheight = 485
  resizeXpos=396
  resizeYpos=323
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMDSERIAL_1_10 as StdField with uid="VTGROTOWCD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MDSERIAL", cQueryName = "MDSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo numero vendita",;
    HelpContextID = 195109138,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=695, Top=17, InputMask=replicate('X',10)

  add object oMDDATREG_1_12 as StdField with uid="ILKIKPKNLZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MDDATREG", cQueryName = "MDDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione della vendita",;
    HelpContextID = 79442189,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=144, Top=17

  add object oMDORAVEN_1_13 as StdField with uid="OPWQPGYKNK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MDORAVEN", cQueryName = "MDORAVEN",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora vendita non corretta",;
    ToolTipText = "Ora di registrazione della vendita",;
    HelpContextID = 127787284,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=282, Top=17, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMDORAVEN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MDORAVEN)<24)
    endwith
    return bRes
  endfunc

  add object oMDMINVEN_1_14 as StdField with uid="XBZTBTTUQI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MDMINVEN", cQueryName = "MDMINVEN",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuto vendita non corretto",;
    ToolTipText = "Minuto di registrazione della vendita",;
    HelpContextID = 140820756,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=314, Top=17, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMDMINVEN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MDMINVEN)<60)
    endwith
    return bRes
  endfunc

  add object oMDCODUTE_1_17 as StdField with uid="OBYLPPHNPA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MDCODUTE", cQueryName = "MDCODUTE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice utente non definito",;
    ToolTipText = "Codice dell'utente che ha effettuato la vendita",;
    HelpContextID = 113910027,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=422, Top=17, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="OPE_RATO", cZoomOnZoom="GSPS_AOP", oKey_1_1="OPCODUTE", oKey_1_2="this.w_MDCODUTE"

  func oMDCODUTE_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODUTE_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODUTE_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OPE_RATO','*','OPCODUTE',cp_AbsName(this.parent,'oMDCODUTE_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_AOP',"Elenco operatori",'',this.parent.oContained
  endproc
  proc oMDCODUTE_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSPS_AOP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OPCODUTE=this.parent.oContained.w_MDCODUTE
    i_obj.ecpSave()
  endproc

  add object oMDCODNEG_1_20 as StdField with uid="RESZHPAUPZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MDCODNEG", cQueryName = "MDCODNEG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice negozio a cui si riferisce la vendita",;
    HelpContextID = 264904973,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=568, Top=17, InputMask=replicate('X',3)

  func oMDCODNEG_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_READPAR)
        bRes2=.link_1_31('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oMDTIPVEN_1_21 as StdField with uid="MBJEZDGCVB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MDTIPVEN", cQueryName = "MDTIPVEN",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Modalit� di vendita inesistente o incongruente",;
    ToolTipText = "Codice tipo modalit� di vendita",;
    HelpContextID = 142946580,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=144, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MOD_VEND", cZoomOnZoom="GSPS_AMV", oKey_1_1="MOCODICE", oKey_1_2="this.w_MDTIPVEN"

  func oMDTIPVEN_1_21.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR))
    endwith
  endfunc

  func oMDTIPVEN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDTIPVEN_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDTIPVEN_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_VEND','*','MOCODICE',cp_AbsName(this.parent,'oMDTIPVEN_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_AMV',"Modalit� di vendita",'GSPS_MVD.MOD_VEND_VZM',this.parent.oContained
  endproc
  proc oMDTIPVEN_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSPS_AMV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_MDTIPVEN
    i_obj.ecpSave()
  endproc

  add object oDESVEN_1_22 as StdField with uid="GQICIKPITG",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESVEN", cQueryName = "DESVEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 266477878,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=207, Top=42, InputMask=replicate('X',35)

  add object oMDCODMAG_1_34 as StdField with uid="THGSHWACAR",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MDCODMAG", cQueryName = "MDCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino associato alla vendita",;
    HelpContextID = 248127757,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=568, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , tabstop=.f., cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MDCODMAG"

  func oMDCODMAG_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODMAG_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODMAG_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMDCODMAG_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMDCODFID_1_35 as StdField with uid="YQXJRLXDJO",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MDCODFID", cQueryName = "MDCODFID",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Fidelity Card inesistente o fuori validit�",;
    ToolTipText = "Codice della eventuale Fidelity Card",;
    HelpContextID = 137748214,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=144, Top=67, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FID_CARD", cZoomOnZoom="GSPS_AFC", oKey_1_1="FCCODFID", oKey_1_2="this.w_MDCODFID"

  func oMDCODFID_1_35.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR))
    endwith
  endfunc

  func oMDCODFID_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODFID_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODFID_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FID_CARD','*','FCCODFID',cp_AbsName(this.parent,'oMDCODFID_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_AFC',"Fidelity Card",'',this.parent.oContained
  endproc
  proc oMDCODFID_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSPS_AFC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FCCODFID=this.parent.oContained.w_MDCODFID
    i_obj.ecpSave()
  endproc


  add object oBtn_1_38 as StdButton with uid="JQMDTPIOHL",left=593, top=70, width=48,height=45,;
    CpPicture="BMP\EVASIONE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per importare manualmente i dati di vendita";
    , HelpContextID = 147659142;
    , TabStop=.f.,Caption='\<Import';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      do GSPS_KIM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    with this.Parent.oContained
      return (NOT (EMPTY(.w_DOCCO1) AND EMPTY(.w_DOCCO2) AND EMPTY(.w_DOCCO3)) AND .cFunction = 'Load')
    endwith
  endfunc

  func oBtn_1_38.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load'  OR (EMPTY(.w_DOCCO1) AND EMPTY(.w_DOCCO2) AND EMPTY(.w_DOCCO3)))
    endwith
   endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="IYGKPXTGUG",left=643, top=70, width=48,height=45,;
    CpPicture="bmp\SCARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare automaticamente la vendita da penna con memoria";
    , HelpContextID = 93102799;
    , TabStop=.f.,Caption='\<Car.Rapido';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GSAR_MPG(thisform)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MDTIPVEN) AND .cFunction='Load')
    endwith
  endfunc

  func oBtn_1_39.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDTIPVEN))
    endwith
   endif
  endfunc


  add object oBtn_1_40 as StdButton with uid="TLKIVQYGTJ",left=693, top=70, width=48,height=45,;
    CpPicture="BMP\log_pwd.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la situazione Fidelity Card";
    , HelpContextID = 68856527;
    , TabStop=.f.,Caption='\<Fid.Card';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      do GSPS_KFC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MDCODFID))
    endwith
  endfunc

  func oBtn_1_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDCODFID))
    endwith
   endif
  endfunc

  add object oMDCODCLI_1_41 as StdField with uid="ADQWIFWXWQ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MDCODCLI", cQueryName = "MDCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Cliente inesistente, obsoleto o incongruente",;
    ToolTipText = "Codice cliente associato alla vendita",;
    HelpContextID = 188079857,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=144, Top=92, cSayPict="g_MASPOS", cGetPict="g_MASPOS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", cZoomOnZoom="GSPS_ACV", oKey_1_1="CLCODCLI", oKey_1_2="this.w_MDCODCLI"

  func oMDCODCLI_1_41.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_MDCODFID) AND EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR))
    endwith
  endfunc

  proc oMDCODCLI_1_41.mAfter
    with this.Parent.oContained
      .w_MDCODCLI=PSCALZER(.w_MDCODCLI, "XXX")
    endwith
  endproc

  func oMDCODCLI_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODCLI_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODCLI_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oMDCODCLI_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACV',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this.parent.oContained
  endproc
  proc oMDCODCLI_1_41.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLCODCLI=this.parent.oContained.w_MDCODCLI
    i_obj.ecpSave()
  endproc

  add object oDESCLI_1_42 as StdField with uid="WYQVMQBSFS",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 188686646,;
   bGlobalFont=.t.,;
    Height=21, Width=311, Left=279, Top=92, InputMask=replicate('X',40)


  add object oBtn_1_43 as StdButton with uid="PAHRABYTEB",left=743, top=70, width=48,height=45,;
    CpPicture="BMP\rischio.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla situazione del rischio cliente";
    , HelpContextID = 220284438;
    , TabStop=.f.,Caption='\<Rischio';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      do GSPS_KRI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    with this.Parent.oContained
      return (g_PERFID='S' AND NOT EMPTY(.w_MDCODCLI) AND .w_FLFIDO='S')
    endwith
  endfunc

  func oBtn_1_43.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (g_PERFID='S' AND NOT EMPTY(.w_MDCODCLI) AND .w_FLFIDO='S'))
    endwith
   endif
  endfunc

  add object oMDCODLIS_1_44 as StdField with uid="SXRVIXXNNE",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MDCODLIS", cQueryName = "MDCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino inesistente o valuta incongruente",;
    ToolTipText = "Codice listino associato alla vendita",;
    HelpContextID = 37084903,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=144, Top=117, InputMask=replicate('X',5), bHasZoom = .t. , tabstop=.f., cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_MDCODLIS"

  func oMDCODLIS_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODLIS_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODLIS_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oMDCODLIS_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'GSPS_MVD.LISTINI_VZM',this.parent.oContained
  endproc

  add object oMDNUMSCO_1_47 as StdField with uid="ZXMIDIYSDB",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MDNUMSCO", cQueryName = "MDNUMSCO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di emissione scontrino",;
    HelpContextID = 90231061,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=300, Top=117, cSayPict='"999999"', cGetPict='"999999"', tabstop=.f.

  func oMDNUMSCO_1_47.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_MDTIPCHI $ 'ES-NS-BV' or .cFunction='Query'))
    endwith
    endif
  endfunc

  add object oMDMATSCO_1_48 as StdField with uid="ALASPBOWXL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MDMATSCO", cQueryName = "MDNUMSCO,MDMATSCO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Matricola cassa",;
    HelpContextID = 96256277,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=438, Top=117, InputMask=replicate('X',20), tabstop=.f.

  func oMDMATSCO_1_48.mCond()
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
  endfunc

  func oMDMATSCO_1_48.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_MDTIPCHI $ 'ES-NS-BV' or .cFunction='Query'))
    endwith
    endif
  endfunc


  add object oObj_1_55 as cp_calclbl with uid="AVZNHXXSSC",left=628, top=47, width=157,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=0,;
    nPag=1;
    , HelpContextID = 131247846


  add object oObj_1_56 as cp_runprogram with uid="HJIOARKACQ",left=250, top=538, width=229,height=18,;
    caption='GSPS_BCR',;
   bGlobalFont=.t.,;
    prg="GSPS_BCR",;
    cEvent = "CaricaRapido",;
    nPag=1;
    , HelpContextID = 92209080


  add object oObj_1_57 as cp_runprogram with uid="ZZLECZWYGG",left=250, top=521, width=229,height=18,;
    caption='GSPS_BPA',;
   bGlobalFont=.t.,;
    prg="GSPS_BPA",;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 92209063


  add object oObj_1_91 as cp_runprogram with uid="ZYBLHRYOGD",left=1, top=515, width=229,height=18,;
    caption='GSPS_BTE(D)',;
   bGlobalFont=.t.,;
    prg="GSPS_BTE('D')",;
    cEvent = "w_MDDATREG Changed",;
    nPag=1;
    , HelpContextID = 92395051


  add object oObj_1_92 as cp_runprogram with uid="LXVHVHYIZN",left=1, top=532, width=229,height=18,;
    caption='GSPS_BTE(V)',;
   bGlobalFont=.t.,;
    prg="GSPS_BTE('V')",;
    cEvent = "w_MDTIPVEN Changed",;
    nPag=1;
    , HelpContextID = 92399659


  add object oObj_1_93 as cp_runprogram with uid="ELIFIMVJWZ",left=1, top=549, width=229,height=18,;
    caption='GSPS_BTE(F)',;
   bGlobalFont=.t.,;
    prg="GSPS_BTE('F')",;
    cEvent = "w_MDCODFID Changed",;
    nPag=1;
    , HelpContextID = 92395563


  add object oObj_1_94 as cp_runprogram with uid="LOCJVJVBVW",left=1, top=566, width=229,height=18,;
    caption='GSPS_BTE(C)',;
   bGlobalFont=.t.,;
    prg="GSPS_BTE('C')",;
    cEvent = "w_MDCODCLI Changed",;
    nPag=1;
    , HelpContextID = 92394795


  add object oObj_1_95 as cp_runprogram with uid="XPGILZKFVW",left=1, top=583, width=229,height=18,;
    caption='GSPS_BTE(L)',;
   bGlobalFont=.t.,;
    prg="GSPS_BTE('L')",;
    cEvent = "w_MDCODLIS Changed",;
    nPag=1;
    , HelpContextID = 92397099


  add object oObj_1_98 as cp_runprogram with uid="KJGCBGOTBV",left=250, top=555, width=229,height=18,;
    caption='GSPS_BI3',;
   bGlobalFont=.t.,;
    prg="GSPS_BI3",;
    cEvent = "ImportaDoc",;
    nPag=1;
    , HelpContextID = 176226407


  add object oObj_1_107 as cp_runprogram with uid="QKYHFOFNFB",left=250, top=572, width=229,height=18,;
    caption='GSPS_BKC',;
   bGlobalFont=.t.,;
    prg="GSPS_BKC",;
    cEvent = "VerificaCliente",;
    nPag=1;
    , HelpContextID = 176226391

  add object oDESFID_1_115 as StdField with uid="BQFQELJSNQ",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESFID", cQueryName = "DESFID",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 101851446,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=299, Top=67, InputMask=replicate('X',40)


  add object oObj_1_116 as cp_runprogram with uid="JUBTCUTYVJ",left=1, top=601, width=228,height=20,;
    caption='GSPS_BPZ(C)',;
   bGlobalFont=.t.,;
    prg="GSPS_BPZ('C')",;
    cEvent = "PromCli",;
    nPag=1;
    , HelpContextID = 92394816


  add object oObj_1_142 as cp_runprogram with uid="UPYRKCKSLG",left=1, top=621, width=229,height=18,;
    caption='GSPS_BTE(M)',;
   bGlobalFont=.t.,;
    prg="GSPS_BTE('M')",;
    cEvent = "w_MDCODMAG Changed",;
    nPag=1;
    , HelpContextID = 92397355


  add object oObj_1_143 as cp_runprogram with uid="TDPREWBSWX",left=251, top=619, width=222,height=23,;
    caption='GSPS_BSF',;
   bGlobalFont=.t.,;
    prg="GSPS_BSF",;
    cEvent = "w_MDCODICE LostFocus",;
    nPag=1;
    , HelpContextID = 92209068


  add object oObj_1_148 as cp_runprogram with uid="QPOPHYNYWQ",left=0, top=638, width=229,height=18,;
    caption='GSPS_BNR',;
   bGlobalFont=.t.,;
    prg="GSPS_BNR",;
    cEvent = "HasEvent",;
    nPag=1;
    , HelpContextID = 176226376


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=149, width=784,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="MDCODICE",Label2="Articolo",Field3="MDDESART",Label3="Descrizione",Field4="MDUNIMIS",Label4="U.M.",Field5="MDQTAMOV",Label5="Quantit�",Field6="MDPREZZO",Label6="Prezzo",Field7="MDSCONT1",Label7="Sconto",Field8="MDFLRESO",Label8="Reso",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 905594

  add object oStr_1_11 as StdString with uid="WXFYEVYZIK",Visible=.t., Left=629, Top=18,;
    Alignment=1, Width=64, Height=17,;
    Caption="Progr.:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="TJXGJKJRXQ",Visible=.t., Left=16, Top=18,;
    Alignment=1, Width=125, Height=17,;
    Caption="Registrazione del:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="ZIWLLYOBBA",Visible=.t., Left=234, Top=18,;
    Alignment=1, Width=45, Height=17,;
    Caption="Ora:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="SDQYKQYYDT",Visible=.t., Left=349, Top=18,;
    Alignment=1, Width=69, Height=17,;
    Caption="Utente:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="XKKGELQSKI",Visible=.t., Left=16, Top=42,;
    Alignment=1, Width=125, Height=18,;
    Caption="Modalit� di vendita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="KFMDVVZRMH",Visible=.t., Left=484, Top=18,;
    Alignment=1, Width=82, Height=17,;
    Caption="Negozio:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="EODORCLFFI",Visible=.t., Left=484, Top=42,;
    Alignment=1, Width=82, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="QJPDHXPMGF",Visible=.t., Left=65, Top=117,;
    Alignment=1, Width=76, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="NLBDSJGXHX",Visible=.t., Left=211, Top=117,;
    Alignment=1, Width=87, Height=18,;
    Caption="Scontrino n.:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (NOT( .w_MDTIPCHI $ 'ES-NS-BV' or .cFunction='Query'))
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="NGLODKSQVR",Visible=.t., Left=16, Top=67,;
    Alignment=1, Width=125, Height=18,;
    Caption="Cod. Fidelity Card:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="CKXDCESZYT",Visible=.t., Left=64, Top=92,;
    Alignment=1, Width=77, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="BOPPOJCQQL",Visible=.t., Left=370, Top=117,;
    Alignment=1, Width=66, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  func oStr_1_113.mHide()
    with this.Parent.oContained
      return (NOT( .w_MDTIPCHI $ 'ES-NS-BV' or .cFunction='Query'))
    endwith
  endfunc

  add object oStr_1_114 as StdString with uid="LISSNVNRRL",Visible=.t., Left=306, Top=17,;
    Alignment=1, Width=9, Height=15,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="BURXLBBFME",Visible=.t., Left=573, Top=413,;
    Alignment=1, Width=70, Height=18,;
    Caption="Totale riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_118 as StdString with uid="FPJLJHXDNZ",Visible=.t., Left=573, Top=439,;
    Alignment=1, Width=70, Height=18,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_132 as StdString with uid="EYHUQLCWHT",Visible=.t., Left=1, Top=363,;
    Alignment=1, Width=70, Height=18,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_133 as StdString with uid="IXBBCZOIRP",Visible=.t., Left=398, Top=363,;
    Alignment=1, Width=43, Height=18,;
    Caption="Disp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_134 as StdString with uid="UJNMEIWEEQ",Visible=.t., Left=663, Top=363,;
    Alignment=2, Width=15, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_134.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<3)
    endwith
  endfunc

  add object oStr_1_135 as StdString with uid="VKJNOUCJHL",Visible=.t., Left=726, Top=363,;
    Alignment=2, Width=12, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_135.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<4)
    endwith
  endfunc

  add object oStr_1_136 as StdString with uid="EZGMCBTSTB",Visible=.t., Left=530, Top=363,;
    Alignment=1, Width=87, Height=18,;
    Caption="Sconti/magg.:"  ;
  , bGlobalFont=.t.

  func oStr_1_136.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<2)
    endwith
  endfunc

  add object oStr_1_137 as StdString with uid="BXPVXWDEDF",Visible=.t., Left=1, Top=389,;
    Alignment=1, Width=70, Height=18,;
    Caption="Lotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_137.mHide()
    with this.Parent.oContained
      return (g_PERLOT<>'S')
    endwith
  endfunc

  add object oStr_1_138 as StdString with uid="IWQAPHFPTQ",Visible=.t., Left=1, Top=412,;
    Alignment=1, Width=70, Height=18,;
    Caption="Ubic.:"  ;
  , bGlobalFont=.t.

  func oStr_1_138.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=167,;
    width=780+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=168,width=779+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oMDCODART_2_8.Refresh()
      this.Parent.oMDCODREP_2_28.Refresh()
      this.Parent.oMDCODIVA_2_29.Refresh()
      this.Parent.oMDSCONT2_2_30.Refresh()
      this.Parent.oMDSCONT3_2_31.Refresh()
      this.Parent.oMDSCONT4_2_32.Refresh()
      this.Parent.oMDFLOMAG_2_33.Refresh()
      this.Parent.oMDSCOVEN_2_37.Refresh()
      this.Parent.oVALRIG_2_51.Refresh()
      this.Parent.oPERIVA_2_53.Refresh()
      this.Parent.oMDSCOPRO_2_66.Refresh()
      this.Parent.oDESREP_2_76.Refresh()
      this.Parent.oQTDISP_2_77.Refresh()
      this.Parent.oMDCODLOT_2_100.Refresh()
      this.Parent.oMDCODUBI_2_101.Refresh()
      this.Parent.oMDCODCOM_2_104.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oMDCODICE_2_6
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oMDUNIMIS_2_20
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oMDCODART_2_8 as StdTrsField with uid="YVKEDUFEGG",rtseq=119,rtrep=.t.,;
    cFormVar="w_MDCODART",value=space(20),enabled=.f.,;
    HelpContextID = 46801178,;
    cTotal="", bFixedPos=.t., cQueryName = "MDCODART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=23, Width=79, Left=1049, Top=61, InputMask=replicate('X',20), cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_MDCODART"

  func oMDCODART_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.T.)
    endwith
    endif
  endfunc

  func oMDCODART_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_MDMAGSAL)
        bRes2=.link_2_35('Full')
      endif
      if .not. empty(.w_MDCODLOT)
        bRes2=.link_2_100('Full')
      endif
      if .not. empty(.w_MDLOTMAG)
        bRes2=.link_2_117('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oMDCODREP_2_28 as StdTrsField with uid="YYBZRITRGG",rtseq=139,rtrep=.t.,;
    cFormVar="w_MDCODREP",value=space(3),;
    ToolTipText = "Codice reparto di vendita",;
    HelpContextID = 63578390,;
    cTotal="", bFixedPos=.t., cQueryName = "MDCODREP",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice reparto inesistente o codice IVA non definito",;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=74, Top=362, cSayPict=["99"], cGetPict=["99"], InputMask=replicate('X',3), bHasZoom = .t. , tabstop=.f., cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_MDCODREP"

  func oMDCODREP_2_28.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG<>'D')
    endwith
  endfunc

  func oMDCODREP_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODREP_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMDCODREP_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oMDCODREP_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oMDCODREP_2_28.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_MDCODREP
    i_obj.ecpSave()
  endproc

  add object oMDCODIVA_2_29 as StdTrsField with uid="QEZBVHJZQP",rtseq=140,rtrep=.t.,;
    cFormVar="w_MDCODIVA",value=space(5),enabled=.f.,;
    ToolTipText = "Codice IVA legato al reparto",;
    HelpContextID = 181018887,;
    cTotal="", bFixedPos=.t., cQueryName = "MDCODIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=353, Top=362, InputMask=replicate('X',5), cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MDCODIVA"

  func oMDCODIVA_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMDSCONT2_2_30 as StdTrsField with uid="XXKRTSCPYE",rtseq=141,rtrep=.t.,;
    cFormVar="w_MDSCONT2",value=0,;
    ToolTipText = "2^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 7282936,;
    cTotal="", bFixedPos=.t., cQueryName = "MDSCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=615, Top=362, cSayPict=["999.99"], cGetPict=["999.99"]

  func oMDSCONT2_2_30.mCond()
    with this.Parent.oContained
      return (.w_MDSCONT1<>0 AND .w_NUMSCO>1 AND .w_MDTIPRIG $ 'RFMA' AND .w_SC1<>'S')
    endwith
  endfunc

  func oMDSCONT2_2_30.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<2)
    endwith
    endif
  endfunc

  add object oMDSCONT3_2_31 as StdTrsField with uid="QVYJBIZMKJ",rtseq=142,rtrep=.t.,;
    cFormVar="w_MDSCONT3",value=0,;
    ToolTipText = "3^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 7282937,;
    cTotal="", bFixedPos=.t., cQueryName = "MDSCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=677, Top=362, cSayPict=["999.99"], cGetPict=["999.99"]

  func oMDSCONT3_2_31.mCond()
    with this.Parent.oContained
      return (.w_MDSCONT2<>0 AND .w_NUMSCO>2 AND .w_MDTIPRIG $ 'RFMA' AND .w_SC1<>'S')
    endwith
  endfunc

  func oMDSCONT3_2_31.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<3)
    endwith
    endif
  endfunc

  add object oMDSCONT4_2_32 as StdTrsField with uid="PACTYPSGBU",rtseq=143,rtrep=.t.,;
    cFormVar="w_MDSCONT4",value=0,;
    ToolTipText = "4^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 7282938,;
    cTotal="", bFixedPos=.t., cQueryName = "MDSCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=738, Top=362, cSayPict=["999.99"], cGetPict=["999.99"]

  func oMDSCONT4_2_32.mCond()
    with this.Parent.oContained
      return (.w_MDSCONT3<>0 AND .w_NUMSCO>3 AND .w_MDTIPRIG $ 'RFMA' AND .w_SC1<>'S')
    endwith
  endfunc

  func oMDSCONT4_2_32.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<4)
    endwith
    endif
  endfunc

  add object oMDFLOMAG_2_33 as StdTrsCombo with uid="GHARYYSMXD",rtrep=.t.,;
    cFormVar="w_MDFLOMAG", RowSource=""+"Normale,"+"Sconto merce,"+"Omaggio imp. + IVA" , ;
    ToolTipText = "Test riga omaggio/sconto merce o normale",;
    HelpContextID = 259477773,;
    Height=25, Width=126, Left=660, Top=388,;
    cTotal="", cQueryName = "MDFLOMAG",;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMDFLOMAG_2_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MDFLOMAG,&i_cF..t_MDFLOMAG),this.value)
    return(iif(xVal =1,'X',;
    iif(xVal =2,'S',;
    iif(xVal =3,'E',;
    space(1)))))
  endfunc
  func oMDFLOMAG_2_33.GetRadio()
    this.Parent.oContained.w_MDFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oMDFLOMAG_2_33.ToRadio()
    this.Parent.oContained.w_MDFLOMAG=trim(this.Parent.oContained.w_MDFLOMAG)
    return(;
      iif(this.Parent.oContained.w_MDFLOMAG=='X',1,;
      iif(this.Parent.oContained.w_MDFLOMAG=='S',2,;
      iif(this.Parent.oContained.w_MDFLOMAG=='E',3,;
      0))))
  endfunc

  func oMDFLOMAG_2_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMDSCOVEN_2_37 as StdTrsField with uid="QTEDKJYPHS",rtseq=148,rtrep=.t.,;
    cFormVar="w_MDSCOVEN",value=0,enabled=.f.,;
    HelpContextID = 141500692,;
    cTotal="", bFixedPos=.t., cQueryName = "MDSCOVEN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=126, Left=444, Top=388, cSayPict=[v_PV(18)], cGetPict=[v_GV(18)]

  func oMDSCOVEN_2_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MDTIPRIG='D')
    endwith
    endif
  endfunc

  add object oBtn_2_38 as StdButton with uid="MTECMOHYUA",width=48,height=45,;
   left=158, top=439,;
    CpPicture="BMP\SALDI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai saldi dell'articolo selezionato";
    , HelpContextID = 198062554;
    , TabStop=.f.,Caption='\<Saldi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_38.Click()
      do GSPS_KCS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_38.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG='R' AND NOT EMPTY(.w_MDCODART))
    endwith
  endfunc

  add object oBtn_2_39 as StdButton with uid="BGSAEWIFZZ",width=48,height=45,;
   left=2, top=439,;
    CpPicture="BMP\Costi.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli ultimi prezzi al cliente";
    , HelpContextID = 46748346;
    , tabstop=.f., Caption='\<Ult.prezzi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_39.Click()
      with this.Parent.oContained
        GSAR_BLU(this.Parent.oContained,"GU")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_39.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG='R' AND NOT EMPTY(.w_MDCODART))
    endwith
  endfunc

  func oBtn_2_39.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDSERIAL))
    endwith
   endif
  endfunc

  add object oBtn_2_40 as StdButton with uid="XVKPEQEJSB",width=48,height=45,;
   left=54, top=439,;
    CpPicture="BMP\ULTACQ.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli ultimi costi";
    , HelpContextID = 46748634;
    , tabstop=.f., Caption='\<Costi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_40.Click()
      with this.Parent.oContained
        GSAR_BLU(this.Parent.oContained,"GC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_40.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG='R' AND NOT EMPTY(.w_MDCODART))
    endwith
  endfunc

  func oBtn_2_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDSERIAL))
    endwith
   endif
  endfunc

  add object oBtn_2_41 as StdButton with uid="NNVHEFBBUC",width=48,height=45,;
   left=106, top=439,;
    CpPicture="BMP\ULTVEN.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli articoli/prezzi alternativi";
    , HelpContextID = 46748426;
    , tabstop=.f., Caption='\<Art. alt.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_41.Click()
      do GSAR_KCA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_41.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG='R' AND NOT EMPTY(.w_MDCODART) AND NOT EMPTY(.w_MDCODMAG))
    endwith
  endfunc

  add object oVALRIG_2_51 as StdTrsField with uid="TSWNFLPDKH",rtseq=154,rtrep=.t.,;
    cFormVar="w_VALRIG",value=0,enabled=.f.,;
    ToolTipText = "Totale riga vendita (esclusi gli sconti globali e sconti da promozione)",;
    HelpContextID = 152940118,;
    cTotal="this.Parent.oContained.w_totale", bFixedPos=.t., cQueryName = "VALRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=10, FontBold=.t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=22, Width=137, Left=647, Top=412, cSayPict=[v_PV(18)], cGetPict=[v_GV(18)]

  add object oPERIVA_2_53 as StdTrsField with uid="ERDBFUGOAH",rtseq=156,rtrep=.t.,;
    cFormVar="w_PERIVA",value=0,enabled=.f.,;
    HelpContextID = 65343990,;
    cTotal="", bFixedPos=.t., cQueryName = "PERIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=23, Width=79, Left=1127, Top=21

  add object oMDSCOPRO_2_66 as StdTrsField with uid="FVYZJNYTED",rtseq=169,rtrep=.t.,;
    cFormVar="w_MDSCOPRO",value=0,enabled=.f.,;
    ToolTipText = "Sconto a valore da promozione",;
    HelpContextID = 40837397,;
    cTotal="", bFixedPos=.t., cQueryName = "MDSCOPRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=126, Left=444, Top=388, cSayPict=[v_PV(18)], cGetPict=[v_GV(18)]

  func oMDSCOPRO_2_66.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MDTIPRIG<>'D' OR EMPTY(.w_MDCODPRO))
    endwith
    endif
  endfunc

  add object oDESREP_2_76 as StdTrsField with uid="KKIOOXOPCW",rtseq=179,rtrep=.t.,;
    cFormVar="w_DESREP",value=space(40),enabled=.f.,;
    HelpContextID = 31334710,;
    cTotal="", bFixedPos=.t., cQueryName = "DESREP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=122, Top=362, InputMask=replicate('X',40)

  add object oQTDISP_2_77 as StdTrsField with uid="NGYQXPCVER",rtseq=180,rtrep=.t.,;
    cFormVar="w_QTDISP",value=0,enabled=.f.,;
    ToolTipText = "Disponibilit� articolo relativa al magazzino principale",;
    HelpContextID = 45367558,;
    cTotal="", bFixedPos=.t., cQueryName = "QTDISP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=444, Top=362, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  add object oObj_2_78 as cp_runprogram with uid="ZACFTUAIOU",width=229,height=18,;
   left=489, top=589,;
    caption='GSPS_BCD(E)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCD('E')",;
    cEvent = "w_MDFLRESO Changed,w_MDPREZZO Changed",;
    nPag=2;
    , HelpContextID = 92395306

  add object oObj_2_81 as cp_runprogram with uid="RSGQAJFLPW",width=229,height=18,;
   left=489, top=606,;
    caption='GSPS_BIP',;
   bGlobalFont=.t.,;
    prg="GSPS_BIP",;
    cEvent = "w_MDPREZZO Changed",;
    nPag=2;
    , HelpContextID = 176226378

  add object oObj_2_86 as cp_calclbl with uid="QNLXFPTGKT",width=110,height=17,;
   left=331, top=390,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 131247846

  add object oObj_2_89 as cp_calclbl with uid="BQOFVBRGLZ",width=110,height=17,;
   left=331, top=415,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 131247846

  add object oMDCODLOT_2_100 as StdTrsField with uid="TFKWSSZOUA",rtseq=199,rtrep=.t.,;
    cFormVar="w_MDCODLOT",value=space(20),;
    ToolTipText = "Codice lotto",;
    HelpContextID = 37084902,;
    cTotal="", bFixedPos=.t., cQueryName = "MDCODLOT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto inesistente, incongruente o sospeso",;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=74, Top=388, cSayPict=[p_LOT], cGetPict=[p_LOT], InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_MDCODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_MDCODLOT"

  func oMDCODLOT_2_100.mCond()
    with this.Parent.oContained
      return (g_PERLOT='S' AND NOT EMPTY(.w_MDCODART) AND .w_FLLOTT$ 'SC' AND .w_MDFLLOTT $ '+-'  AND .w_MDQTAMOV<>0)
    endwith
  endfunc

  func oMDCODLOT_2_100.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT<>'S')
    endwith
    endif
  endfunc

  func oMDCODLOT_2_100.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_100('Part',this)
      if .not. empty(.w_MDLOTMAG)
        bRes2=.link_2_117('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMDCODLOT_2_100.ecpDrop(oSource)
    this.Parent.oContained.link_2_100('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMDCODLOT_2_100.mZoom
      with this.Parent.oContained
        do GSMD_BZL with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oMDCODLOT_2_100.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_MDCODART
     i_obj.w_LOCODICE=this.parent.oContained.w_MDCODLOT
    i_obj.ecpSave()
  endproc

  add object oMDCODUBI_2_101 as StdTrsField with uid="RMSPWQREBY",rtseq=200,rtrep=.t.,;
    cFormVar="w_MDCODUBI",value=space(20),;
    ToolTipText = "Codice ubicazione associata al magazzino principale",;
    HelpContextID = 113910031,;
    cTotal="", bFixedPos=.t., cQueryName = "MDCODUBI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice ubicazione inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=74, Top=413, cSayPict=[p_UBI], cGetPict=[p_UBI], InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MDMAGRIG", oKey_2_1="UBCODICE", oKey_2_2="this.w_MDCODUBI"

  func oMDCODUBI_2_101.mCond()
    with this.Parent.oContained
      return (g_PERUBI='S' AND NOT EMPTY(.w_MDCODART) AND NOT EMPTY(.w_MDCODMAG) AND .w_FLUBIC='S' AND (.w_MDFLLOTT $ '+-' Or NOT EMPTY(ALLTRIM(.w_MDFLCASC)) ) AND .w_MDQTAMOV<>0)
    endwith
  endfunc

  func oMDCODUBI_2_101.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
    endif
  endfunc

  func oMDCODUBI_2_101.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_101('Part',this)
      if .not. empty(.w_MDLOTMAG)
        bRes2=.link_2_117('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMDCODUBI_2_101.ecpDrop(oSource)
    this.Parent.oContained.link_2_101('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMDCODUBI_2_101.mZoom
      with this.Parent.oContained
        GSMD_BZU(this.Parent.oContained,"A")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oMDCODUBI_2_101.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_MDMAGRIG
     i_obj.w_UBCODICE=this.parent.oContained.w_MDCODUBI
    i_obj.ecpSave()
  endproc

  add object oLinkPC_2_103 as StdButton with uid="SUOYSFVRLN",width=48,height=45,;
   left=210, top=439,;
    CpPicture="bmp\matricole.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla gestione delle matricole";
    , HelpContextID = 121656450;
    , Caption='\<Matricole';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_103.Click()
      this.Parent.oContained.GSVE_MMT.LinkPCClick()
    endproc

  func oLinkPC_2_103.mCond()
    with this.Parent.oContained
      return (g_MATR='S'  And .w_MDDATREG>=nvl(g_DATMAT,cp_CharToDate('  /  /    ')) And .w_MDQTAMOV<>0 And (.w_MDFLCASC $ '+-') AND ((.w_FLLOTT$ 'S-C' AND NOT EMPTY(.w_MDCODLOT)) OR .w_FLLOTT='N' OR g_PERLOT<>'S') AND ((.w_FLUBIC='S' AND NOT EMPTY(.w_MDCODUBI)) OR EMPTY(.w_FLUBIC) OR g_PERUBI<>'S'))
    endwith
  endfunc

  func oLinkPC_2_103.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESMAT<>'S' Or g_MATR<>'S' or .w_MDDATREG<nvl(g_DATMAT,cp_CharToDate('  /  /    ')))
    endwith
   endif
  endfunc

  add object oMDCODCOM_2_104 as StdTrsField with uid="BNUFPDJHXB",rtseq=202,rtrep=.t.,;
    cFormVar="w_MDCODCOM",value=space(15),;
    HelpContextID = 188079853,;
    cTotal="", bFixedPos=.t., cQueryName = "MDCODCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
   bGlobalFont=.t.,;
    Height=21, Width=126, Left=444, Top=413, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MDCODCOM"

  func oMDCODCOM_2_104.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S')))
    endwith
  endfunc

  func oMDCODCOM_2_104.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(.w_MDTIPRIG<>'D' AND ((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))))
    endwith
    endif
  endfunc

  func oMDCODCOM_2_104.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_104('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODCOM_2_104.ecpDrop(oSource)
    this.Parent.oContained.link_2_104('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMDCODCOM_2_104.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMDCODCOM_2_104'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oMDCODCOM_2_104.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MDCODCOM
    i_obj.ecpSave()
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_1 as StdField with uid="MJICJUQYIN",rtseq=223,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    ToolTipText = "Somma importi di riga (compresi omaggi e sconti merce, esclusi sconti globali e da promozione)",;
    HelpContextID = 121453622,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=647, Top=438, cSayPict=[v_PV(18)], cGetPict=[v_GV(18)]
enddefine

  define class tgsps_mvdPag2 as StdContainer
    Width  = 791
    height = 485
    stdWidth  = 791
    stdheight = 485
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMDTOTVEN_4_1 as StdField with uid="PZCQBPVLLL",rtseq=224,rtrep=.f.,;
    cFormVar = "w_MDTOTVEN", cQueryName = "MDTOTVEN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale vendita (esclusi storno promozioni e sconti globali)",;
    HelpContextID = 147534100,;
    FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=139, Left=553, Top=13, cSayPict="v_PV(18)", cGetPict="v_PV(18)", tabstop=.f.

  add object oMDPUNFID_4_2 as StdField with uid="OHYVFCTUOR",rtseq=225,rtrep=.f.,;
    cFormVar = "w_MDPUNFID", cQueryName = "MDPUNFID",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Punti Fidelity maturati nella vendita",;
    HelpContextID = 126815990,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=348, Top=41, tabstop=.f.

  add object oMDSTOPRO_4_3 as StdField with uid="OQXYJGOPVT",rtseq=226,rtrep=.f.,;
    cFormVar = "w_MDSTOPRO", cQueryName = "MDSTOPRO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Storno promozione",;
    HelpContextID = 41951509,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=41, cSayPict="v_PV(18)", cGetPict="v_PV(18)", tabstop=.f.

  add object oMDSCOCL1_4_4 as StdField with uid="NRWHSKXDIR",rtseq=227,rtrep=.f.,;
    cFormVar = "w_MDSCOCL1", cQueryName = "MDSCOCL1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^Maggiorazione (se positiva) o sconto (se negativa) applicata al cliente",;
    HelpContextID = 177266441,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=110, Top=70, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oMDSCOCL2_4_5 as StdField with uid="VVUXXNTYBT",rtseq=228,rtrep=.f.,;
    cFormVar = "w_MDSCOCL2", cQueryName = "MDSCOCL2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^ Maggiorazione (se positiva) o sconto (se negativa) applicata al cliente",;
    HelpContextID = 177266440,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=183, Top=70, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMDSCOCL2_4_5.mCond()
    with this.Parent.oContained
      return (.w_MDSCOCL1<>0 AND .cFunction<>'Query')
    endwith
  endfunc

  add object oMDSCOPAG_4_6 as StdField with uid="JESUECURNQ",rtseq=229,rtrep=.f.,;
    cFormVar = "w_MDSCOPAG", cQueryName = "MDSCOPAG",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Maggiorazione (se positiva) o sconto (se negativa) applicata al pagamento",;
    HelpContextID = 40837389,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=348, Top=70, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oMDSCONTI_4_7 as StdField with uid="MPCCYMTBWP",rtseq=230,rtrep=.f.,;
    cFormVar = "w_MDSCONTI", cQueryName = "MDSCONTI",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale degli sconti globali (sconti cliente + pagamento)",;
    HelpContextID = 7282959,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=70, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oMDSCONTI_4_7.mCond()
    with this.Parent.oContained
      return (.w_MDFLFOSC='S' AND .cFunction<>'Query')
    endwith
  endfunc

  add object oMDFLFOSC_4_8 as StdCheck with uid="KCNOSAMCEI",rtseq=231,rtrep=.f.,left=696, top=72, caption="Forza sc.",;
    ToolTipText = "Se attivo: consente di forzare lo sconto/maggiorazione di piede documento",;
    HelpContextID = 15159561,;
    cFormVar="w_MDFLFOSC", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oMDFLFOSC_4_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MDFLFOSC,&i_cF..t_MDFLFOSC),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oMDFLFOSC_4_8.GetRadio()
    this.Parent.oContained.w_MDFLFOSC = this.RadioValue()
    return .t.
  endfunc

  func oMDFLFOSC_4_8.ToRadio()
    this.Parent.oContained.w_MDFLFOSC=trim(this.Parent.oContained.w_MDFLFOSC)
    return(;
      iif(this.Parent.oContained.w_MDFLFOSC=='S',1,;
      0))
  endfunc

  func oMDFLFOSC_4_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMDCODPAG_4_9 as StdField with uid="UZIPNGOCCW",rtseq=232,rtrep=.f.,;
    cFormVar = "w_MDCODPAG", cQueryName = "MDCODPAG",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente",;
    ToolTipText = "Codice pagamento legata al documento",;
    HelpContextID = 30023949,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=477, Top=430, InputMask=replicate('X',5), bHasZoom = .t. , tabstop=.f., cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_MDCODPAG"

  func oMDCODPAG_4_9.mCond()
    with this.Parent.oContained
      return (.w_MDPAGCLI<>0)
    endwith
  endfunc

  func oMDCODPAG_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODPAG_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODPAG_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oMDCODPAG_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oMDCODPAG_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_MDCODPAG
    i_obj.ecpSave()
  endproc

  add object oMDIMPPRE_4_11 as StdField with uid="STCVIOOLKK",rtseq=234,rtrep=.f.,;
    cFormVar = "w_MDIMPPRE", cQueryName = "MDIMPPRE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Superato importo residuo",;
    ToolTipText = "Importo prepagato o Fidelity Card",;
    HelpContextID = 42500363,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=264, Top=110, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oMDIMPPRE_4_11.mCond()
    with this.Parent.oContained
      return (.w_FLFAFI<>'S' Or .cFunction = 'Load')
    endwith
  endfunc

  func oMDIMPPRE_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MDIMPPRE <= .w_FIMPRES Or .cFunction = 'Query')
    endwith
    return bRes
  endfunc

  add object oMDTOTDOC_4_12 as StdField with uid="GIKBCEHQHN",rtseq=235,rtrep=.f.,;
    cFormVar = "w_MDTOTDOC", cQueryName = "MDTOTDOC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale vendita",;
    HelpContextID = 154455799,;
    FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=139, Left=553, Top=111, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  add object oMDACCPRE_4_13 as StdField with uid="NRTECQZASZ",rtseq=236,rtrep=.f.,;
    cFormVar = "w_MDACCPRE", cQueryName = "MDACCPRE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acconto precedente",;
    HelpContextID = 28180747,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=194, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oMDPAGCON_4_15 as StdField with uid="AYUQZXAKWD",rtseq=237,rtrep=.f.,;
    cFormVar = "w_MDPAGCON", cQueryName = "MDPAGCON",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo pagato in contanti",;
    HelpContextID = 185798380,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=220, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oMDPAGASS_4_16 as StdField with uid="MZBCPRDNAY",rtseq=238,rtrep=.f.,;
    cFormVar = "w_MDPAGASS", cQueryName = "MDPAGASS",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo pagato tramite assegno",;
    HelpContextID = 49082649,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=246, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oMDRIFASS_4_17 as StdField with uid="CTORSJXXWS",rtseq=239,rtrep=.f.,;
    cFormVar = "w_MDRIFASS", cQueryName = "MDRIFASS",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento n.assegno",;
    HelpContextID = 48566553,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=110, Top=246, InputMask=replicate('X',40)

  func oMDRIFASS_4_17.mCond()
    with this.Parent.oContained
      return (.w_MDPAGASS<>0)
    endwith
  endfunc

  add object oMDPAGCAR_4_18 as StdField with uid="XTKQVJKZCX",rtseq=240,rtrep=.f.,;
    cFormVar = "w_MDPAGCAR", cQueryName = "MDPAGCAR",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^ Importo pagato tramite carta di credito o debito",;
    HelpContextID = 82637080,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=272, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oMDCODCAR_4_19 as StdField with uid="AGCTGOIOUV",rtseq=241,rtrep=.f.,;
    cFormVar = "w_MDCODCAR", cQueryName = "MDCODCAR",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "1^ Codice carta credito/bancomat",;
    HelpContextID = 80355608,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=110, Top=272, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAR_CRED", cZoomOnZoom="GSPS_ACA", oKey_1_1="CCCODICE", oKey_1_2="this.w_MDCODCAR"

  func oMDCODCAR_4_19.mCond()
    with this.Parent.oContained
      return (.w_MDPAGCAR<>0)
    endwith
  endfunc

  func oMDCODCAR_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODCAR_4_19.ecpDrop(oSource)
    this.Parent.oContained.link_4_19('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODCAR_4_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAR_CRED','*','CCCODICE',cp_AbsName(this.parent,'oMDCODCAR_4_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACA',"Carte di credito",'',this.parent.oContained
  endproc
  proc oMDCODCAR_4_19.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_MDCODCAR
    i_obj.ecpSave()
  endproc

  add object oMDPAGFIN_4_20 as StdField with uid="IWFRMLCVIP",rtseq=242,rtrep=.f.,;
    cFormVar = "w_MDPAGFIN", cQueryName = "MDPAGFIN",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo pagato attraverso finanziamento",;
    HelpContextID = 135466732,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=298, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oCCDESCRI_4_21 as StdField with uid="NMEPZVKYUB",rtseq=243,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 95432559,;
   bGlobalFont=.t.,;
    Height=21, Width=239, Left=164, Top=272, InputMask=replicate('X',40)

  add object oMDCODFIN_4_22 as StdField with uid="NTKYONEVUB",rtseq=244,rtrep=.f.,;
    cFormVar = "w_MDCODFIN", cQueryName = "MDCODFIN",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice finanziamento",;
    HelpContextID = 137748204,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=110, Top=298, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAR_CRED", cZoomOnZoom="GSPS_ACA", oKey_1_1="CCCODICE", oKey_1_2="this.w_MDCODFIN"

  func oMDCODFIN_4_22.mCond()
    with this.Parent.oContained
      return (.w_MDPAGFIN<>0)
    endwith
  endfunc

  func oMDCODFIN_4_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODFIN_4_22.ecpDrop(oSource)
    this.Parent.oContained.link_4_22('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODFIN_4_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAR_CRED','*','CCCODICE',cp_AbsName(this.parent,'oMDCODFIN_4_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACA',"Carte di credito/finanziamento",'',this.parent.oContained
  endproc
  proc oMDCODFIN_4_22.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_MDCODFIN
    i_obj.ecpSave()
  endproc

  add object oDESFIN_4_23 as StdField with uid="UYNFMCGACY",rtseq=245,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 1188150,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=165, Top=298, InputMask=replicate('X',40)

  add object oMDPAGCLI_4_28 as StdField with uid="YNWYZYNSTU",rtseq=246,rtrep=.f.,;
    cFormVar = "w_MDPAGCLI", cQueryName = "MDPAGCLI",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo rimanente da pagare a saldo (C./cliente)",;
    HelpContextID = 185798385,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=324, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oMDPAGCLI_4_28.mCond()
    with this.Parent.oContained
      return (.w_MDFLSALD<>'S' AND .cFunction<>'Query')
    endwith
  endfunc

  add object oMDIMPABB_4_29 as StdField with uid="CROKYJUOVW",rtseq=247,rtrep=.f.,;
    cFormVar = "w_MDIMPABB", cQueryName = "MDIMPABB",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale abbuono/resto",;
    HelpContextID = 59277576,;
    FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=139, Left=553, Top=351, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oMDIMPABB_4_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MDIMPABB>0 AND .w_MDFLSALD<>'S')
    endwith
    endif
  endfunc


  add object oMDTIPCHI_4_31 as StdCombo with uid="ZDEENQYIAL",rtseq=248,rtrep=.f.,left=110,top=361,width=242,height=26;
    , tabstop=.F., bGlobalFont=.f.;
    , ToolTipText = "Tipo chiusura vendita";
    , HelpContextID = 175820529;
    , cFormVar="w_MDTIPCHI",RowSource=""+"Nessuna stampa,"+"Emissione scontrino,"+"Brogliaccio di vendita,"+"Ricevuta fiscale,"+"Fattura immediata,"+"Fattura fiscale,"+"Ricevuta segue fattura,"+"Documento di trasporto", bObbl = .f. , nPag = 4;
    , FontName = "Tahoma", FontSize = 12, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oMDTIPCHI_4_31.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MDTIPCHI,&i_cF..t_MDTIPCHI),this.value)
    return(iif(xVal =1,'NS',;
    iif(xVal =2,'ES',;
    iif(xVal =3,'BV',;
    iif(xVal =4,'RF',;
    iif(xVal =5,'FI',;
    iif(xVal =6,'FF',;
    iif(xVal =7,'RS',;
    iif(xVal =8,'DT',;
    space(2))))))))))
  endfunc
  func oMDTIPCHI_4_31.GetRadio()
    this.Parent.oContained.w_MDTIPCHI = this.RadioValue()
    return .t.
  endfunc

  func oMDTIPCHI_4_31.ToRadio()
    this.Parent.oContained.w_MDTIPCHI=trim(this.Parent.oContained.w_MDTIPCHI)
    return(;
      iif(this.Parent.oContained.w_MDTIPCHI=='NS',1,;
      iif(this.Parent.oContained.w_MDTIPCHI=='ES',2,;
      iif(this.Parent.oContained.w_MDTIPCHI=='BV',3,;
      iif(this.Parent.oContained.w_MDTIPCHI=='RF',4,;
      iif(this.Parent.oContained.w_MDTIPCHI=='FI',5,;
      iif(this.Parent.oContained.w_MDTIPCHI=='FF',6,;
      iif(this.Parent.oContained.w_MDTIPCHI=='RS',7,;
      iif(this.Parent.oContained.w_MDTIPCHI=='DT',8,;
      0)))))))))
  endfunc

  func oMDTIPCHI_4_31.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMDTIPCHI_4_31.mCond()
    with this.Parent.oContained
      return ((((EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR)) and (.w_MD_ERROR='S' or .w_MDTIPCHI<>'ES')) AND .cFunction<>'Query') or .cFunction='Load')
    endwith
  endfunc

  add object oMDCODCLI_4_35 as StdField with uid="VJXUHTEJZM",rtseq=249,rtrep=.f.,;
    cFormVar = "w_MDCODCLI", cQueryName = "MDCODCLI",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare il cliente",;
    ToolTipText = "Codice cliente associato alla vendita",;
    HelpContextID = 188079857,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=110, Top=402, InputMask=replicate('X',15), bHasZoom = .t. , tabstop=.F., cLinkFile="CLI_VEND", cZoomOnZoom="GSPS_ACV", oKey_1_1="CLCODCLI", oKey_1_2="this.w_MDCODCLI"

  func oMDCODCLI_4_35.mCond()
    with this.Parent.oContained
      return (.w_EDCL3KEU OR EMPTY(.w_MDCODFID) AND .w_MDTIPCHI $ 'RF-FI-FF-RS-DT' AND EMPTY(.w_MDRIFDOC) AND EMPTY(.w_MDRIFCOR))
    endwith
  endfunc

  proc oMDCODCLI_4_35.mAfter
    with this.Parent.oContained
      .w_MDCODCLI=PSCALZER(.w_MDCODCLI, "XXX")
    endwith
  endproc

  func oMDCODCLI_4_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODCLI_4_35.ecpDrop(oSource)
    this.Parent.oContained.link_4_35('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCODCLI_4_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oMDCODCLI_4_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACV',"Clienti negozio",'CLIPOS.CLI_VEND_VZM',this.parent.oContained
  endproc
  proc oMDCODCLI_4_35.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLCODCLI=this.parent.oContained.w_MDCODCLI
    i_obj.ecpSave()
  endproc

  add object oDESCLI_4_36 as StdField with uid="NUERDQFSJR",rtseq=250,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 188686646,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=246, Top=402, InputMask=replicate('X',40)

  add object oMDTIPDOC_4_37 as StdField with uid="IXGJSXFGFL",rtseq=251,rtrep=.f.,;
    cFormVar = "w_MDTIPDOC", cQueryName = "MDTIPDOC",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo documento non coerente",;
    ToolTipText = "Tipologia di documento generata",;
    HelpContextID = 159043319,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=430, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="gsve_atd", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MDTIPDOC"

  func oMDTIPDOC_4_37.mCond()
    with this.Parent.oContained
      return (.w_EDTIPDOC)
    endwith
  endfunc

  func oMDTIPDOC_4_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDTIPDOC_4_37.ecpDrop(oSource)
    this.Parent.oContained.link_4_37('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDTIPDOC_4_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMDTIPDOC_4_37'),iif(empty(i_cWhere),.f.,i_cWhere),'gsve_atd',"Tipo documento",'gsps0mdv.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMDTIPDOC_4_37.mZoomOnZoom
    local i_obj
    i_obj=gsve_atd()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MDTIPDOC
    i_obj.ecpSave()
  endproc

  add object oMDALFDOC_4_42 as StdField with uid="EFBUTPQJPO",rtseq=253,rtrep=.f.,;
    cFormVar = "w_MDALFDOC", cQueryName = "MDALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 169410295,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=288, Top=430, InputMask=replicate('X',10), tabstop=.F.


  add object oObj_4_53 as cp_runprogram with uid="PYVGECLKHV",left=-3, top=473, width=193,height=20,;
    caption='GSPS_BCS',;
   bGlobalFont=.t.,;
    prg="GSPS_BCS",;
    cEvent = "ActivatePage 2",;
    nPag=4;
    , ToolTipText = "Inizializza dati chiusura e lancia calcolo promozioni";
    , HelpContextID = 92209081

  add object oMDFLSALD_4_60 as StdCheck with uid="PPACLIHSFR",rtseq=261,rtrep=.f.,left=696, top=323, caption="a saldo",;
    ToolTipText = "Se attivo: corrispettivo totalmente incassato (differenza in abbuono)",;
    HelpContextID = 206089974,;
    cFormVar="w_MDFLSALD", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oMDFLSALD_4_60.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MDFLSALD,&i_cF..t_MDFLSALD),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oMDFLSALD_4_60.GetRadio()
    this.Parent.oContained.w_MDFLSALD = this.RadioValue()
    return .t.
  endfunc

  func oMDFLSALD_4_60.ToRadio()
    this.Parent.oContained.w_MDFLSALD=trim(this.Parent.oContained.w_MDFLSALD)
    return(;
      iif(this.Parent.oContained.w_MDFLSALD=='S',1,;
      0))
  endfunc

  func oMDFLSALD_4_60.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMDFLSALD_4_60.mCond()
    with this.Parent.oContained
      return (.w_MDTIPCHI $ 'NS-ES-BV-RF' AND .w_MDIMPABB>0 AND .w_MDPAGCLI=0 AND .cFunction<>'Query')
    endwith
  endfunc

  func oMDFLSALD_4_60.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (.w_MDTIPCHI $ 'NS-ES-BV-RF' AND .w_MDIMPABB>0))
    endwith
    endif
  endfunc


  add object oObj_4_65 as cp_runprogram with uid="WQSKILQGQP",left=193, top=473, width=209,height=20,;
    caption='GSPS_BSV',;
   bGlobalFont=.t.,;
    prg="GSPS_BSV",;
    cEvent = "AggiornaDocumento",;
    nPag=4;
    , HelpContextID = 92209084


  add object oObj_4_66 as cp_runprogram with uid="RWOCOSVQPD",left=575, top=492, width=193,height=20,;
    caption='GSPS_BSK(1)',;
   bGlobalFont=.t.,;
    prg="GSPS_BSK(1)",;
    cEvent = "Record Updated",;
    nPag=4;
    , ToolTipText = "Stampa del documento";
    , HelpContextID = 92390193


  add object oObj_4_67 as cp_runprogram with uid="VLXJZPNQAB",left=575, top=511, width=193,height=20,;
    caption='GSPS_BSK(2)',;
   bGlobalFont=.t.,;
    prg="GSPS_BSK(2)",;
    cEvent = "Record Inserted",;
    nPag=4;
    , ToolTipText = "Stampa del documento";
    , HelpContextID = 92390449


  add object oObj_4_68 as cp_runprogram with uid="TFMKQBVBRV",left=193, top=492, width=209,height=20,;
    caption='GSPS_BMK',;
   bGlobalFont=.t.,;
    prg="GSPS_BMK",;
    cEvent = "ControlliFinali",;
    nPag=4;
    , HelpContextID = 176226383


  add object oObj_4_69 as cp_runprogram with uid="SEFMWXJPDF",left=-3, top=511, width=573,height=20,;
    caption='GSPS_BCZ(S)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('S')",;
    cEvent = "w_MDSCOCL1 Changed,w_MDSCOCL2 Changed,w_MDSCOPAG Changed,w_MDCODPAG Changed",;
    nPag=4;
    , HelpContextID = 92398912


  add object oObj_4_70 as cp_runprogram with uid="VSCUGQNMAS",left=-3, top=530, width=573,height=20,;
    caption='GSPS_BCZ(P)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('P')",;
    cEvent = "w_MDPAGASS Changed,w_MDPAGCAR Changed,w_MDPAGFIN Changed",;
    nPag=4;
    , HelpContextID = 92398144


  add object oObj_4_71 as cp_runprogram with uid="WWYJDCTOTG",left=-3, top=549, width=238,height=20,;
    caption='GSPS_BCZ(C)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('C')",;
    cEvent = "w_MDPAGCLI Changed",;
    nPag=4;
    , HelpContextID = 92394816


  add object oObj_4_72 as cp_runprogram with uid="OIWQJSAXZB",left=238, top=549, width=238,height=20,;
    caption='GSPS_BCZ(A)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('A')",;
    cEvent = "w_MDIMPABB Changed",;
    nPag=4;
    , HelpContextID = 92394304


  add object oObj_4_73 as cp_runprogram with uid="BXVMEMTTJS",left=575, top=530, width=238,height=20,;
    caption='GSPS_BCZ(T)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('T')",;
    cEvent = "w_MDTIPCHI Changed",;
    nPag=4;
    , HelpContextID = 92399168


  add object oObj_4_79 as cp_runprogram with uid="ZNJZWXRWJK",left=-3, top=492, width=193,height=20,;
    caption='GSPS_BPZ(P)',;
   bGlobalFont=.t.,;
    prg="GSPS_BPZ('P')",;
    cEvent = "Promozioni",;
    nPag=4;
    , ToolTipText = "Crea righe descrittive promozioni e calcola mdscoven sulle righe presenti";
    , HelpContextID = 92398144


  add object oObj_4_86 as cp_runprogram with uid="XNHCEBPLCJ",left=-3, top=568, width=209,height=20,;
    caption='GSPS_BGK',;
   bGlobalFont=.t.,;
    prg="GSPS_BGK",;
    cEvent = "GeneraKit",;
    nPag=4;
    , HelpContextID = 92209073


  add object oObj_4_87 as cp_runprogram with uid="ATCMLGCAQH",left=238, top=569, width=229,height=20,;
    caption='GSPS_BCZ(F)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('F')",;
    cEvent = "w_MDSCONTI Changed",;
    nPag=4;
    , HelpContextID = 92395584


  add object oObj_4_88 as cp_runprogram with uid="OKSIULWXRX",left=575, top=473, width=209,height=20,;
    caption='GSPS1BPZ',;
   bGlobalFont=.t.,;
    prg="GSPS1BPZ",;
    cEvent = "Insert start,Update start",;
    nPag=4;
    , ToolTipText = "Ripartisce gli sconti di piede sulle righe (mdscoven)";
    , HelpContextID = 43974592


  add object oBtn_4_89 as StdButton with uid="ZSFNDSXFJZ",left=548, top=407, width=48,height=45,;
    CpPicture="BMP\fattura.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per accedere al documento generato";
    , HelpContextID = 159535770;
    , TabStop=.f.,Caption='\<Documento';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_89.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_MDRIFDOC,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_89.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MDRIFDOC))
    endwith
  endfunc

  func oBtn_4_89.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDRIFDOC))
    endwith
   endif
  endfunc


  add object oObj_4_90 as cp_runprogram with uid="ZKJDLURAMF",left=575, top=549, width=229,height=20,;
    caption='GSPS_BCZ(O)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('O')",;
    cEvent = "w_MDFLFOSC Changed",;
    nPag=4;
    , HelpContextID = 92397888


  add object oBtn_4_91 as StdButton with uid="MUVNASTUXP",left=602, top=407, width=48,height=45,;
    CpPicture="BMP\doc.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per accedere al corrispettivo generato";
    , HelpContextID = 246236875;
    , TabStop=.f.,Caption='\<Corrispettivo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_91.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_MDRIFCOR,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_91.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MDRIFCOR))
    endwith
  endfunc

  func oBtn_4_91.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDRIFCOR))
    endwith
   endif
  endfunc


  add object oBtn_4_95 as StdButton with uid="BHKYZAXGJA",left=657, top=407, width=48,height=45,;
    CpPicture="BMP\verifica.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per accedere al movimento di magazzino di carico dei kit";
    , HelpContextID = 47687713;
    , TabStop=.f.,Caption='Ca\<rico Kit';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_95.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_MDCARKIT,-10)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_95.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MDCARKIT))
    endwith
  endfunc

  func oBtn_4_95.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDCARKIT))
    endwith
   endif
  endfunc


  add object oBtn_4_96 as StdButton with uid="YUQKZPUEBG",left=713, top=407, width=48,height=45,;
    CpPicture="BMP\verifica.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per accedere al movimento di magazzino di scarico dei kit";
    , HelpContextID = 122081270;
    , TabStop=.f.,Caption='Scarico \<Kit';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_96.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_MDSCAKIT,-10)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_96.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MDSCAKIT))
    endwith
  endfunc

  func oBtn_4_96.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDSCAKIT))
    endwith
   endif
  endfunc


  add object oBtn_4_98 as StdButton with uid="IDQEFXFSFW",left=355, top=342, width=48,height=45,;
    CpPicture="BMP\SCONTRINO.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per generare lo scontrino";
    , HelpContextID = 152303740;
    , TabStop=.f.,Caption='\<Scontrino';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_98.Click()
      do GSPS_KES with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_98.mCond()
    with this.Parent.oContained
      return (.w_MD_ERROR='S' AND .w_MDTIPCHI='ES' and .cFunction <>'Edit')
    endwith
  endfunc

  func oBtn_4_98.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MD_ERROR<>'S' OR .w_MDTIPCHI<>'ES')
    endwith
   endif
  endfunc


  add object oObj_4_104 as cp_runprogram with uid="TGYVNRGLWJ",left=238, top=587, width=306,height=20,;
    caption='GSPS_BCZ(I)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('I')",;
    cEvent = "w_MDIMPPRE Changed,w_MDACCPRE Changed",;
    nPag=4;
    , HelpContextID = 92396352


  add object oObj_4_105 as cp_runprogram with uid="MPFQJWQGFM",left=-3, top=587, width=218,height=20,;
    caption='GSPS_BCZ(Z)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('Z')",;
    cEvent = "w_MDPAGCON Changed",;
    nPag=4;
    , HelpContextID = 92400704


  add object oObj_4_106 as cp_runprogram with uid="CARVQZAHED",left=575, top=570, width=274,height=20,;
    caption='GSPS_BCZ(R)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCZ('R')",;
    cEvent = "w_MDCODPAG Changed",;
    nPag=4;
    , HelpContextID = 92398656


  add object oObj_4_107 as cp_runprogram with uid="VHTQAEYMAL",left=404, top=474, width=168,height=18,;
    caption='GSPS_BCK',;
   bGlobalFont=.t.,;
    prg="GSPS_BCK",;
    cEvent = "ChkFinali",;
    nPag=4;
    , HelpContextID = 92209073


  add object oObj_4_116 as cp_runprogram with uid="LBFTWELFBM",left=-3, top=608, width=247,height=17,;
    caption='GSPS_BTA',;
   bGlobalFont=.t.,;
    prg="GSPS_BTA('B')",;
    cEvent = "Delete row start,Insert row start,Update row start",;
    nPag=4;
    , HelpContextID = 92209063

  add object oStr_4_14 as StdString with uid="NDXAIKFQAH",Visible=.t., Left=419, Top=14,;
    Alignment=1, Width=132, Height=17,;
    Caption="Importo totale:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_24 as StdString with uid="NHVDNFENID",Visible=.t., Left=408, Top=221,;
    Alignment=1, Width=143, Height=18,;
    Caption="Contanti:"  ;
  , bGlobalFont=.t.

  add object oStr_4_25 as StdString with uid="KHRETZTWHU",Visible=.t., Left=408, Top=247,;
    Alignment=1, Width=143, Height=18,;
    Caption="C/assegno:"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="YQSZUZEVQB",Visible=.t., Left=5, Top=247,;
    Alignment=1, Width=102, Height=18,;
    Caption="Riferim.assegno:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="MPAEGONTQZ",Visible=.t., Left=408, Top=273,;
    Alignment=1, Width=143, Height=18,;
    Caption="C/credito/bancomat:"  ;
  , bGlobalFont=.t.

  add object oStr_4_30 as StdString with uid="BTFMCQMVPV",Visible=.t., Left=5, Top=273,;
    Alignment=1, Width=102, Height=18,;
    Caption="Riferim.carta:"  ;
  , bGlobalFont=.t.

  add object oStr_4_32 as StdString with uid="UXSOZRUQIW",Visible=.t., Left=408, Top=325,;
    Alignment=1, Width=143, Height=18,;
    Caption="C/cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="PTKJIMCKPZ",Visible=.t., Left=408, Top=299,;
    Alignment=1, Width=143, Height=18,;
    Caption="Finanziamento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_34 as StdString with uid="SFSLLYLJZS",Visible=.t., Left=5, Top=299,;
    Alignment=1, Width=102, Height=18,;
    Caption="Finanziamento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_39 as StdString with uid="ISCNWNNRIM",Visible=.t., Left=4, Top=363,;
    Alignment=1, Width=103, Height=22,;
    Caption="Chiusura:"  ;
    , FontName = "Tahoma", FontSize = 12, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_40 as StdString with uid="WHVGIZZBBO",Visible=.t., Left=5, Top=430,;
    Alignment=1, Width=102, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_41 as StdString with uid="ZAXYCTBXIQ",Visible=.t., Left=385, Top=430,;
    Alignment=1, Width=90, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_45 as StdString with uid="SJQAYMJOFU",Visible=.t., Left=113, Top=111,;
    Alignment=1, Width=149, Height=18,;
    Caption="Prepagato/Fidelity Card:"  ;
  , bGlobalFont=.t.

  add object oStr_4_47 as StdString with uid="WZXPMWHQWX",Visible=.t., Left=34, Top=402,;
    Alignment=1, Width=73, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_4_48 as StdString with uid="QUTOWFEMCF",Visible=.t., Left=419, Top=71,;
    Alignment=1, Width=132, Height=18,;
    Caption="Totale sconti/magg.:"  ;
  , bGlobalFont=.t.

  add object oStr_4_49 as StdString with uid="BHROZOVPEY",Visible=.t., Left=419, Top=112,;
    Alignment=1, Width=132, Height=17,;
    Caption="Totale scontato:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_50 as StdString with uid="MMARSLBFDE",Visible=.t., Left=166, Top=72,;
    Alignment=2, Width=17, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_4_51 as StdString with uid="JBRHXQOSHZ",Visible=.t., Left=250, Top=71,;
    Alignment=1, Width=96, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_52 as StdString with uid="YINGYMIXAU",Visible=.t., Left=9, Top=71,;
    Alignment=1, Width=97, Height=15,;
    Caption="Magg./sconti:"  ;
  , bGlobalFont=.t.

  add object oStr_4_59 as StdString with uid="ELAMQVYLPV",Visible=.t., Left=419, Top=41,;
    Alignment=1, Width=132, Height=18,;
    Caption="Storno promozioni:"  ;
  , bGlobalFont=.t.

  add object oStr_4_62 as StdString with uid="MQXZASEUMK",Visible=.t., Left=415, Top=172,;
    Alignment=0, Width=73, Height=17,;
    Caption="Pagamenti"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_63 as StdString with uid="NWIKMQATNV",Visible=.t., Left=408, Top=352,;
    Alignment=1, Width=143, Height=17,;
    Caption="Abbuono:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_63.mHide()
    with this.Parent.oContained
      return (.w_MDIMPABB<=0 OR (.w_MDIMPABB >0 AND .w_MDFLSALD<>'S'))
    endwith
  endfunc

  add object oStr_4_64 as StdString with uid="GHNVHLOQUM",Visible=.t., Left=408, Top=352,;
    Alignment=1, Width=143, Height=17,;
    Caption="Resto:"  ;
    , FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_64.mHide()
    with this.Parent.oContained
      return (.w_MDIMPABB>0)
    endwith
  endfunc

  add object oStr_4_74 as StdString with uid="ILRBSGVVVY",Visible=.t., Left=175, Top=430,;
    Alignment=1, Width=111, Height=18,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  add object oStr_4_78 as StdString with uid="EDQSFYVMYW",Visible=.t., Left=408, Top=195,;
    Alignment=1, Width=143, Height=18,;
    Caption="Acconti precedenti:"  ;
  , bGlobalFont=.t.

  add object oStr_4_80 as StdString with uid="DMZBFXNFDW",Visible=.t., Left=250, Top=41,;
    Alignment=1, Width=96, Height=18,;
    Caption="Punti Fidelity:"  ;
  , bGlobalFont=.t.

  add object oBox_4_44 as StdBox with uid="QPEYWXZTCN",left=6, top=395, width=742,height=2

  add object oBox_4_61 as StdBox with uid="ZRSQZRVNIG",left=414, top=186, width=356,height=2
enddefine

* --- Defining Body row
define class tgsps_mvdBodyRow as CPBodyRowCnt
  Width=770
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMDCODICE_2_6 as StdTrsField with uid="XTIKXLJUBR",rtseq=117,rtrep=.t.,;
    cFormVar="w_MDCODICE",value=space(20),;
    ToolTipText = "Codice articolo",;
    HelpContextID = 181018891,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "%w_ERRORCOD%",;
    FontName="Arial", FontSize=10, FontBold=.t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=18, Width=149, Left=49, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_MDCODICE"

  proc oMDCODICE_2_6.mAfter
      with this.Parent.oContained
        GSAR_BGP(this.Parent.oContained,this.value)
      endwith
  endproc

  func oMDCODICE_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCODICE_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMDCODICE_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMDCODICE_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli pos",'GSPS_MVD.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oMDCODICE_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_MDCODICE
    i_obj.ecpSave()
  endproc

  add object oMDDESART_2_7 as StdTrsField with uid="JOAARLETCV",rtseq=118,rtrep=.t.,;
    cFormVar="w_MDDESART",value=space(40),;
    ToolTipText = "Descrizione articolo (F9 = accede alle descrizioni aggiuntive)",;
    HelpContextID = 61878554,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Tahoma", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=15, Width=239, Left=198, Top=2, InputMask=replicate('X',40)

  func oMDDESART_2_7.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MDCODART) AND (EMPTY(.w_MDDESART) OR .w_DA1<>'S'))
    endwith
  endfunc

  add object oCPROWORD_2_10 as StdTrsField with uid="XSYGVHLMCQ",rtseq=121,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 33234026,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=1, cSayPict=["99999"], cGetPict=["99999"], tabstop=.f.

  add object oMDUNIMIS_2_20 as StdTrsField with uid="ZYNBYUERWA",rtseq=131,rtrep=.t.,;
    cFormVar="w_MDUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 15056615,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=439, Top=1, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_MDUNIMIS"

  func oMDUNIMIS_2_20.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG $ 'RM' AND NOT EMPTY(.w_MDCODICE)  AND (EMPTY(.w_MDUNIMIS) OR (EMPTY(.w_MDSERRIF) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S') AND .w_UM1<>'S')))
    endwith
  endfunc

  func oMDUNIMIS_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  func oMDUNIMIS_2_20.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=!.w_DELNOMSG
    endwith
    return i_bres
  endfunc

  proc oMDUNIMIS_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMDUNIMIS_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oMDUNIMIS_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oMDUNIMIS_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_MDUNIMIS
    i_obj.ecpSave()
  endproc

  add object oMDQTAMOV_2_22 as StdTrsField with uid="QFMFSKHDFP",rtseq=133,rtrep=.t.,;
    cFormVar="w_MDQTAMOV",value=0,;
    HelpContextID = 23068388,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=478, Top=1, cSayPict=['@Z '+v_PQ(10)], cGetPict=['@Z '+v_GQ(10)]

  func oMDQTAMOV_2_22.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG $ 'RMA'  AND NOT EMPTY(.w_MDCODICE) AND (EMPTY(.w_MDQTAMOV) OR .w_QT1<>'S'))
    endwith
  endfunc

  func oMDQTAMOV_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_MDQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_MDQTAMOV=INT(.w_MDQTAMOV))) OR NOT .w_MDTIPRIG $ 'RM')
    endwith
    return bRes
  endfunc

  func oMDQTAMOV_2_22.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=!.w_DELNOMSG
    endwith
    return i_bres
  endfunc

  add object oMDPREZZO_2_25 as StdTrsField with uid="NHPUQAXPGQ",rtseq=136,rtrep=.t.,;
    cFormVar="w_MDPREZZO",value=0,;
    HelpContextID = 69340907,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=562, Top=1, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]

  func oMDPREZZO_2_25.mCond()
    with this.Parent.oContained
      return (.w_MDTIPRIG $ 'RFMA' AND NOT EMPTY(.w_MDCODICE) AND (EMPTY(.w_MDPREZZO) OR .w_PZ1<>'S'))
    endwith
  endfunc

  add object oMDSCONT1_2_26 as StdTrsField with uid="OMPMYHALXZ",rtseq=137,rtrep=.t.,;
    cFormVar="w_MDSCONT1",value=0,;
    HelpContextID = 7282935,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=690, Top=1, cSayPict=["999.99"], cGetPict=["999.99"]

  func oMDSCONT1_2_26.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MDCODICE) AND .w_NUMSCO>0 AND .w_MDTIPRIG $ 'RFMA' AND .w_SC1<>'S')
    endwith
  endfunc

  add object oMDFLRESO_2_27 as StdTrsCheck with uid="TOHSLHKBOG",rtrep=.t.,;
    cFormVar="w_MDFLRESO",  caption="",;
    ToolTipText = "Se attivo: riga di reso",;
    HelpContextID = 128405781,;
    Left=751, Top=1, Width=14,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.,Left=748,AutoSize=.F.;
   , bGlobalFont=.t.


  func oMDFLRESO_2_27.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MDFLRESO,&i_cF..t_MDFLRESO),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oMDFLRESO_2_27.GetRadio()
    this.Parent.oContained.w_MDFLRESO = this.RadioValue()
    return .t.
  endfunc

  func oMDFLRESO_2_27.ToRadio()
    this.Parent.oContained.w_MDFLRESO=trim(this.Parent.oContained.w_MDFLRESO)
    return(;
      iif(this.Parent.oContained.w_MDFLRESO=='S',1,;
      0))
  endfunc

  func oMDFLRESO_2_27.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oObj_2_44 as cp_runprogram with uid="DLLLSRVRDR",width=229,height=18,;
   left=482, top=353,;
    caption='GSPS_BCD(M)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCD('M')",;
    cEvent = "w_MDCODICE Changed",;
    nPag=2;
    , HelpContextID = 92397354

  add object oObj_2_45 as cp_runprogram with uid="RWOOCLJRXS",width=229,height=18,;
   left=482, top=370,;
    caption='GSPS_BCD(A)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCD('A')",;
    cEvent = "w_MDQTAMOV Changed",;
    nPag=2;
    , HelpContextID = 92394282

  add object oObj_2_46 as cp_runprogram with uid="PTUWCRJUBD",width=229,height=18,;
   left=482, top=387,;
    caption='GSPS_BCD(R)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCD('R')",;
    cEvent = "Ricalcola",;
    nPag=2;
    , HelpContextID = 92398634

  add object oObj_2_47 as cp_runprogram with uid="XYECVWMYAX",width=229,height=18,;
   left=482, top=404,;
    caption='GSPS_BCD(U)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCD('U')",;
    cEvent = "w_MDUNIMIS Changed",;
    nPag=2;
    , HelpContextID = 92399402
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oMDCODICE_2_6.When()
    return(.t.)
  proc oMDCODICE_2_6.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMDCODICE_2_6.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_mvd','COR_RISP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MDSERIAL=COR_RISP.MDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsps_mvd
Proc SetCodRep(pParent, pCodRep, pDesRep, pCodIva)
   local l_recNum, l_recNumFirst
   l_recNum = pParent.RowIndex()
   l_recNumFirst = pParent.Search("t_MDTIPRIG<>'D'")
   If l_recNumFirst = l_recNum  And Ah_YesNo("Si desidera aggiornare il codice reparto anche per le righe successive?")
    pParent.MarkPos()
    pParent.w_DEFCODREP = m.pCodRep
    pParent.SetRow(l_recNumFirst)
    local l_oCtrl, L_Method_Name, pPerIva
    l_oCtrl = pParent.GetCtrl("w_MDCODIVA")
    L_Method_Name='pParent.Link'+Right(l_oCtrl.Name, LEN(l_oCtrl.Name) - rat('_', l_oCtrl.Name, 2)+1)+"('Full',pParent)"
    &L_Method_Name
    l_oCtrl = .NULL.     
    pPerIva = pParent.w_PERIVA
    pParent.SaveRow()
    do while !pParent.Eof_Trs()
      pParent.SetRow()
      If pParent.w_MDTIPRIG<>'D'
        pParent.w_MDCODREP = m.pCodRep
        pParent.w_DESREP = m.pDesRep
        pParent.w_MDCODIVA = m.pCodIva
        pParent.w_PERIVA = m.pPerIva
        pParent.SaveRow()
      EndIf
      pParent.NextRow()
    enddo
    pParent.RePos()
   EndIf
EndProc

Proc BlackPromo(pParent)
   With pParent
     *--- Riga Promozione
     If !Empty(.w_MDCODPRO) Or (.w_MDFLROMA='S')
       .w_MDCODPRO = Space(15)
       .w_MDFLROMA=' '
       local l_oldArea
       l_oldArea = Select()
       Select (pParent.cTrsName)
       .w_CPROWORD = cp_maxroword() + 10
       Select(l_oldArea)
     EndIf
   EndWith
EndProc
* --- Fine Area Manuale
