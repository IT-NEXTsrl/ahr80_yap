* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bi3                                                        *
*              Import da documenti collegati                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_625]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-03                                                      *
* Last revis.: 2013-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bi3",oParentObject)
return(i_retval)

define class tgsps_bi3 as StdBatch
  * --- Local variables
  w_INDICE = 0
  w_DATREG = ctod("  /  /  ")
  w_OKRIG = .f.
  w_NORIG = .f.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_IMPLOT = 0
  w_TOTEFF = 0
  w_RESIDUO = 0
  w_TOTMOV = 0
  w_TOTIMP = 0
  w_QTAIMP = 0
  w_RESIM1 = 0
  w_RESIMP = 0
  w_RAPUM = 0
  w_OKDEC = .f.
  w_CAAUT = space(1)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_CODLOT = space(20)
  w_CASC = space(1)
  w_ORDI = space(1)
  w_IMPE = space(1)
  w_RISE = space(1)
  w_UNIMIS = space(3)
  w_QTAEV1 = 0
  w_LFLFRAZ = space(1)
  w_FLLMAG = space(1)
  w_CODMAG = space(5)
  w_RESOCON = space(0)
  w_OLDMAG = space(5)
  w_CODUBI = space(20)
  w_LODATSCA = ctod("  /  /  ")
  w_QTAIMPLO = 0
  w_QTAMOVLO = 0
  w_QTALOT = 0
  w_OKRIF = .f.
  w_RIFER = space(10)
  w_RIGA = 0
  w_LOOP = 0
  w_LFLRRIF = space(1)
  w_TMPCARI = space(1)
  w_GSVE_MMT = .NULL.
  w_ORCASC = space(1)
  w_NUMREC = 0
  w_ANNULLA = .f.
  w_PADRE = .NULL.
  w_RECTRS = 0
  w_NEWDOC = space(10)
  w_ARTDES = space(20)
  w_NEWDOC = space(10)
  w_DCFLINTE = space(1)
  w_CNT = 0
  w_MCAORIF = 0
  w_MDOCRIF = space(1)
  w_APPOSER = space(10)
  w_APPO = space(3)
  w_DESRIF = space(18)
  w_ARIF = space(10)
  w_NRIF = 0
  w_DRIF = ctod("  /  /  ")
  w_NEST = 0
  w_AEST = space(10)
  w_DEST = ctod("  /  /  ")
  w_APPART = space(10)
  w_VISEVA = 0
  w_APPO1 = 0
  w_APPO2 = 0
  w_APPO4 = 0
  w_APPO5 = 0
  w_TCF = space(15)
  w_SCPAG = 0
  w_SCOFIS = 0
  w_SCCL1 = 0
  w_SCCL2 = 0
  w_RICON = space(15)
  w_FLFOSC = space(1)
  w_FLCAPA = space(1)
  w_COPAG = space(5)
  w_ODOC = space(5)
  w_CLADOC = space(2)
  w_QTAUM1 = 0
  w_ACCONT = 0
  w_ACCPRE = 0
  w_FLSCOR = space(1)
  w_MESS = space(8)
  w_CONTRA = space(15)
  w_NOBLOK = .f.
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_DATIDOC = space(0)
  w_DOCBLOK = .f.
  w_DOCWARN = .f.
  w_TEST = space(0)
  w_TESBLOK = space(0)
  w_CODLIN = space(3)
  w_MODRIF = space(5)
  w_FLUSEP = space(1)
  w_DTOBSO = ctod("  /  /  ")
  w_MODUM2 = space(1)
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = ctod("  /  /  ")
  w_CF = ctod("  /  /  ")
  w_CV = space(5)
  w_IVACON = space(1)
  w_OKCONTR = .f.
  w_NOFRAZ = space(1)
  w_TmpN = 0
  w_LFLUBIC = space(1)
  w_FLLOTT1 = space(1)
  w_QTATEST = 0
  w_QTATEST1 = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  CON_TRAM_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  KEY_ARTI_idx=0
  LISTINI_idx=0
  LOTTIART_idx=0
  MAGAZZIN_idx=0
  PAG_AMEN_idx=0
  REP_ARTI_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  UNIMIS_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  MAGAZZIN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- La Causale Magazzino viene letta solo la prima volta in testata in quanto ripete quella di Testata Documento
    * --- Import da Documenti Collegati da (GSPS_KIM)
    * --- Aggiorna il Documento Destinazione dall'Import (da GSPS_MDV evento: ImportaDoc)
    this.w_PADRE = this.oParentObject
    if USED("RigheDoc")
      * --- Se w_RECTRS=0 allora non e' ancora stato aggiornato il Corpo del Documento, 
      * --- quindi i dati di testata del Primo Doc. Importato verranno inseriti nella Testata del Doc. Destinazione.
      SELECT (this.w_PADRE.cTrsName) 
      this.w_RECTRS = reccount()
      if this.w_RECTRS=1
        GOTO 1
        this.w_RECTRS = IIF(EMPTY(NVL(t_MDCODICE," ")), 0, 1)
      endif
      * --- Scarica nel Cursore Ordinato...
      SELECT * FROM RigheDoc ;
      WHERE (XCHK=1 OR MVQTAEVA<>0 OR (MVTIPRIG="F" AND MVIMPEVA<>0)) AND FLDOCU<>"S" AND CPROWNUM<>0 ;
      INTO CURSOR RigheSort ORDER BY MVSERIAL, CPROWORD
      * --- w_OKRIG  .T.  se una riga non descrittiva del documento di origine �
      *                              correttamente inserita nel documento di destinazione
      *     w_NORIG .T.  se una riga non descrittiva del documento di origine 
      *                             nella quale � gestito lo split dei lotti non � inserita per
      *                             mancanza di relativi movimenti di carico lotti
      this.w_NORIG = .F.
      this.w_INDICE = 0
      * --- Array righe descrittive da eliminare
       
 DIMENSION Des_Row[1, 2] 
 Des_Row[1, 1] = Space(10) 
 Des_Row[1, 2] = 0
      * --- Articolo per Descrizioni
      this.w_ARTDES = SPACE(20)
      if NOT EMPTY(g_ARTDES)
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(g_ARTDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODART;
            from (i_cTable) where;
                CACODICE = g_ARTDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTDES = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_NEWDOC = "zzzzzzzzzz"
      this.w_DCFLINTE = " "
      this.w_CNT = 0
      this.w_NOBLOK = .F.
      this.w_DOCWARN = .F.
      this.w_MCAORIF = 0
      this.w_MDOCRIF = " "
      this.w_FLSCOR = " "
      this.w_RESOCON1 = ""
      this.w_TEST = ""
      this.w_MESBLOK = ""
      this.w_OLDMAG = "#####"
      this.w_PADRE.MarkPos()     
      * --- istanzio oggetti per mess. incrementali
      this.w_oMESS=createobject("ah_message")
      SELECT RigheSort
      GO TOP
      SCAN FOR CPROWNUM<>0
      * --- Nuovo Documento
      if this.w_NEWDOC<>MVSERIAL
        * --- w_APPOSER = Serial Documento usato nella Pagina 4
        this.w_APPOSER = MVSERIAL
        * --- Prima di esaminare un nuovo documento, controllo il resoconto errori di quello precedente.
        *     Al primo record non fa niente
        if this.w_DOCWARN
          this.w_NOBLOK = .T.
        endif
        * --- Variabile Controllo Errori per ogni documento esaminato
        this.w_DOCWARN = .F.
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT RigheSort
        this.w_NEWDOC = MVSERIAL
        * --- Se abilitato scrive riga Descrittiva Riferimenti
        if NOT EMPTY(g_ARTDES)
          * --- leggo il codice lingua
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODLIN"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_TCF);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_RICON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODLIN;
              from (i_cTable) where;
                  ANTIPCON = this.w_TCF;
                  and ANCODICE = this.w_RICON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDMODRIF"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_ODOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDMODRIF;
              from (i_cTable) where;
                  TDTIPDOC = this.w_ODOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MODRIF = NVL(cp_ToDate(_read_.TDMODRIF),cp_NullValue(_read_.TDMODRIF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
           
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_NRIF,15,0)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=Alltrim(this.w_ARIF) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_DRIF) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=Alltrim(this.w_AEST) 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]=DTOC(this.w_DEST) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_RICON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= ALLTRIM(STR(this.w_NEST,15,0)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
          * --- Nostro Riferimento
          if this.oParentObject.w_FLNSRI="S"
            if this.w_NRIF<>0
               
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]=ALLTRIM(this.w_ODOC) 
 ARPARAM2[2]=ALLTRIM(STR(this.w_NRIF,15,0))+IIF(EMPTY(this.w_ARIF),"", "/"+Alltrim(this.w_ARIF)) +SPACE(1)+DTOC(this.w_DRIF) 
              this.w_APPART = LEFT(CALRIFDES("nsd",this.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_DESRIF, @ARPARAM2),40)
              this.w_RIFER = this.w_APPART
              this.w_OKRIF = .T.
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          * --- Vostro Riferimento
          if this.oParentObject.w_FLVSRI="S" 
            if this.w_NEST<>0
               
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]="N" 
 ARPARAM2[2]=ALLTRIM(STR(this.w_NEST,15,0))+IIF(EMPTY(this.w_AEST),"", "/"+Alltrim(this.w_AEST)) +SPACE(1)+DTOC(this.w_DEST) 
              this.w_APPART = LEFT(CALRIFDES("vsd",this.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_DESRIF, @ARPARAM2),40)
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
        SELECT RigheSort
      endif
      * --- Nuova Riga del Temporaneo
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_SERRIF = MVSERIAL
      this.w_ROWRIF = CPROWNUM
      this.w_DATREG = this.oParentObject.w_MDDATREG
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODLOT,MVCODART,MVQTAUM1,CPROWORD,MVQTAEV1,MVUNIMIS,MVQTAMOV,MVCODUBI,MVCODUB2,MVCODMAG"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODLOT,MVCODART,MVQTAUM1,CPROWORD,MVQTAEV1,MVUNIMIS,MVQTAMOV,MVCODUBI,MVCODUB2,MVCODMAG;
          from (i_cTable) where;
              MVSERIAL = this.w_SERRIF;
              and CPROWNUM = this.w_ROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODLOT = NVL(cp_ToDate(_read_.MVCODLOT),cp_NullValue(_read_.MVCODLOT))
        w_CODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
        this.w_QTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
        this.w_RIGA = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
        this.oParentObject.w_ORQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
        this.w_UNIMIS = NVL(cp_ToDate(_read_.MVUNIMIS),cp_NullValue(_read_.MVUNIMIS))
        w_QTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
        this.w_CODUBI = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
        w_CODUB2 = NVL(cp_ToDate(_read_.MVCODUB2),cp_NullValue(_read_.MVCODUB2))
        w_ORIMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLLOTT,ARUNMIS1,ARMAGPRE,ARFLLMAG"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLLOTT,ARUNMIS1,ARMAGPRE,ARFLLMAG;
          from (i_cTable) where;
              ARCODART = w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAAUT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
        this.w_FLLMAG = NVL(cp_ToDate(_read_.ARFLLMAG),cp_NullValue(_read_.ARFLLMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- U.M. Frazionabile
      if NOT EMPTY(NVL(this.w_UNIMIS," "))
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLFRAZ"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLFRAZ;
            from (i_cTable) where;
                UMCODICE = this.w_UNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LFLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_QTALOT = 0
      this.w_RESIM1 = 0
      this.w_RESIMP = 0
      this.w_RAPUM = 0
      if this.w_CAAUT= "C" AND EMPTY(this.w_CODLOT) AND g_PERLOT="S" And .f.
        * --- Ricalcolo la quantit� 1 U.M. effettivamente evasa in base al rapporto 
        *     nel documento di origine
        if this.w_UNIMIS <>this.oParentObject.w_UNMIS1
          * --- Ricalcolo quantit� specificata nello zoom come qta evasa nella 1 U.M.
          this.w_QTAEV1 = (this.w_QTAUM1*NVL(MVQTAEVA,0))/w_QTAMOV
          this.w_QTATRA = IIF(this.w_QTAUM1=this.w_QTAEV1,this.w_QTAUM1-this.oParentObject.w_ORQTAEV1,this.w_QTAEV1)
          this.w_QTAIMP = (this.w_QTAUM1*MIN(NVL(MVQTAEVA,0),(NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0))))/w_QTAMOV
          this.w_RESIM1 = (this.w_QTAUM1*(NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0)))/w_QTAMOV
          this.w_RAPUM = IIF(w_QTAMOV>0 AND this.w_LFLFRAZ="S",this.w_QTAUM1/w_QTAMOV,0)
        else
          this.w_QTATRA = NVL(MVQTAEVA,0)
          this.w_RESIM1 = (NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0))
          this.w_QTAIMP = MIN(NVL(MVQTAEVA,0),(NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0)))
        endif
        this.w_RESIMP = (NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0))
        * --- Variabili Utilizzate:
        *     w_IMPLOT: quantit� importata nelle righe dello split dei lotti
        *     w_TOTIMP: totale qta importata utilizzata per calcolare proporzionalmente la qta importata
        *     w_TOTMOV: totale quantit� utilizzabile dei lotti associati all'articolo in analisi decrementata a ogni iterazione
        *     w_RESIDUO: residuo decimale della qta importata calcolata proporzionalmente da assegnare all'ultima riga dello split
        *     w_TOTEFF: quantit� totale effettivamente utilizzata MIN(w_QTATRA,w_TOTEFF)
        *     w_QTAIMP: totale quantit� importata decrementata ad ogni iterazione
        *     w_QTATRA: totale quanti� trasferibile decrementata ad ogni iterazione 
        *     w_QTALOT: qta movimentata di riga del lotto
        *     w_RESIM1: qta residua da evadere 
        *     w_RAPUM: rapporto tra le unit� di misura
        this.w_IMPLOT = 0
        this.w_TOTEFF = 0
        this.w_TOTMOV = 0
        this.w_TOTIMP = 0
        this.w_RESIDUO = 0
        if this.w_QTATRA>0
          * --- Magazzino Utilizzato Come Filtro
          * --- Nel POS forzo sempre il magazzino come filtro 
          *     poich� non � gestito magazzino di riga come modificabile. 
          *     Deve essere sempre uguale a quello di testata
          this.w_CODMAG = this.oParentObject.w_MDCODMAG
          * --- Eseguo una volta la query per determinare i lotti disponibili
          this.w_PADRE.Exec_Select("Appo","t_MDCODART, t_MDCODUBI,t_MDCODLOT,SUM(NVL(t_MDQTAUM1,0)-NVL(MDQTAUM1,0)) AS t_MDQTAUM1","t_MDCODART="+Cp_ToStrODBC( w_CODART ),"t_MDCODART,t_MDCODLOT,t_MDCODUBI","t_MDCODART,t_MDCODLOT,t_MDCODUBI","")     
          vq_exec("..\MADV\EXE\QUERY\GSMD_QCA.VQR",this,"LOTTI")
           
 Select LOCODICE,LOCODUBI,LODATCRE,LODATSCA,(LOQTAESI-NVL(t_MVQTAUM1,0)) As LOQTAESI,CODMAG,CODART,UTDC; 
 From LOTTI Left outer Join APPO On (APPO.t_MDCODART=LOTTI.CODART And APPO.t_MDCODUBI=LOTTI.LOCODUBI ; 
 And APPO.t_MDCODLOT=LOTTI.LOCODICE) Into Cursor Lotti NoFilter
           
 Select Appo 
 Use 
 Select Lotti 
 Sum LOQTAESI To this.w_TOTEFF For LOQTAESI>=this.w_RAPUM
          * --- Quantit� effettiva su cui eseguire proporzione quantit� importata
          this.w_TOTMOV = this.w_TOTEFF
          this.w_TOTIMP = this.w_QTAIMP
          this.w_TOTEFF = MIN(this.w_TOTEFF,this.w_QTATRA)
          =WRCURSOR("LOTTI")
          do while this.w_QTATRA>0
            this.w_CODLOT = CERCLOT(this.w_DATREG,w_CODART,"LOTTI")
            if NOT EMPTY(this.w_CODLOT)
              SELECT LOTTI
              GO TOP
              LOCATE FOR w_CODART=NVL(CODART,SPACE(20)) AND this.w_CODLOT=NVL(LOTTI.LOCODICE,SPACE(20))
              if FOUND()
                * --- Assegno informazioni della riga documento  che variano 
                this.w_OKDEC = .T.
                this.w_QTALOT = NVL(LOTTI.LOQTAESI,0)
                this.w_CODLOT = NVL(LOTTI.LOCODICE,SPACE(20))
                this.w_CODUBI = NVL(LOTTI.LOCODUBI,SPACE(20))
                this.w_CODMAG = NVL(LOTTI.CODMAG,SPACE(5))
                if this.w_QTATRA>this.w_QTALOT
                  * --- Consumo per intero la quantit� del lotto se unit� di misura frazionabile
                  * --- Ricalcolo in proporzione la quantit� da memorizzare in MVQTAIMP
                  this.w_IMPLOT = IIF(this.w_TOTIMP>this.w_TOTMOV,this.w_QTALOT,cp_ROUND((this.w_TOTIMP*this.w_QTALOT)/this.w_TOTEFF,3))
                  if this.w_LFLFRAZ="S"
                    * --- Se l'unit� di misura movimentata  � non frazionabile posso creare la riga del documento 
                    *     solo per la parte intera della quantit� movimentata
                    this.w_RESIDUO = this.w_RESIDUO+ (this.w_IMPLOT-INT(this.w_IMPLOT))
                    if this.w_UNIMIS <>this.oParentObject.w_UNMIS1
                      * --- Variabili Utilizzate:
                      *     w_QTAMOVLO: qta del lotto espressa nella U.M. movimentata
                      *     w_QTAIMPLO:  qta importata  del lotto espressa nella U.M. movimentata
                      this.w_QTAMOVLO = (this.w_QTALOT*w_QTAMOV)/this.w_QTAUM1
                      this.w_QTAIMPLO = (this.w_IMPLOT*w_QTAMOV)/this.w_QTAUM1
                      this.w_QTALOT = (INT(this.w_QTAMOVLO)*this.w_QTAUM1)/w_QTAMOV
                      this.w_IMPLOT = (INT(this.w_QTAIMPLO)*this.w_QTAUM1)/w_QTAMOV
                    else
                      this.w_QTALOT = INT(this.w_QTALOT)
                      this.w_IMPLOT = INT(this.w_IMPLOT)
                    endif
                    if INT(this.w_QTALOT)>0 AND this.w_IMPLOT>0
                      * --- Se la parte intera della qta ricalcolata � >0 decremento contatore quantit� importata
                      this.w_OKDEC = .T.
                    else
                      * --- Se la parte intera della qta ricalcolata � =0 non ho consumato il lotto ma devo eliminarlo dal 
                      *     cursore LOTTI per non essere riconsiderato nello split della riga corrente.
                      this.w_OKDEC = .F.
                    endif
                  else
                    * --- U.M. frazionabile devo decrementare contatatore qta importata
                    this.w_OKDEC = .T.
                  endif
                  * --- Elimino il lotto per intero
                  SELECT LOTTI
                  DELETE
                  if this.w_OKDEC
                    * --- Decremento Totalizzatori
                    this.w_TOTMOV = this.w_TOTMOV-this.w_QTALOT
                    this.w_QTATRA = this.w_QTATRA-this.w_QTALOT
                    this.w_RESIM1 = this.w_RESIM1-this.w_IMPLOT
                    if this.w_RESIM1<0
                      * --- Riassegno qta importata dell' ultima riga se differenze di arrotondamento
                      this.w_IMPLOT = this.w_IMPLOT- ABS(this.w_RESIM1)
                      this.w_RESIM1 = 0
                    endif
                    this.w_QTAIMP = this.w_QTAIMP-this.w_IMPLOT
                    this.w_OKRIF = IIF(this.w_QTATRA=0,.F.,.T.)
                  endif
                  if this.w_RESIDUO>0 AND this.w_TOTMOV=0
                    * --- Se esiste residuo e sono nell'ultima riga possibile aggiungo residuo qta importata
                    this.w_IMPLOT = INT(this.w_IMPLOT+this.w_RESIDUO)
                    this.w_RESIDUO = 0
                  endif
                else
                  * --- Consumo in parte la quantit� del lotto quindi creo una riga pari
                  *     alla quantit� del documento di origine
                  this.w_QTALOT = this.w_QTATRA
                  this.w_IMPLOT = this.w_QTAIMP
                  this.w_RESIM1 = this.w_RESIM1-this.w_IMPLOT
                  this.w_QTATRA = 0
                  this.w_QTAIMP = 0
                endif
                if this.w_QTALOT>0 AND this.w_IMPLOT>0
                  * --- Creo riga del lotto se quantit� significativa
                  this.oParentObject.w_FLLOTT = this.w_CAAUT
                  this.Pag6()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              this.w_OKRIG = .T.
            else
              * --- Creo Riga con lotto vuoto
              this.w_OKRIG = .F.
              this.w_NORIG = .T.
              this.w_CODLOT = SPACE(20)
              this.w_CODUBI = SPACE(20)
              this.w_CODMAG = SPACE(5)
              if this.w_OKRIF
                this.w_oPART = this.w_oMESS.addmsgpartNL("%0%1%0Riga n.%2 articolo %3 quantit� 1� U.M. mancante %4")
                this.w_oPART.addParam(this.w_RIFER)     
              else
                this.w_oPART = this.w_oMESS.addmsgpartNL("Riga n.%1 articolo %2 quantit� 1� U.M. mancante %3")
              endif
              this.w_oPART.addParam(alltrim(str(this.w_RIGA)) )     
              this.w_oPART.addParam(alltrim(w_CODART))     
              this.w_oPART.addParam(alltrim(this.oParentObject.w_UNMIS1)+" "+alltrim(str(this.w_QTATRA)))     
              this.w_QTATRA = 0
              this.w_OKRIF = .F.
              this.w_NOBLOK = .T.
            endif
          enddo
        endif
      else
        this.w_OKRIG = .T.
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Elimino Riga Descrittiva Dall'array
      if this.w_OKRIG AND g_PERLOT="S" AND this.w_INDICE>0
        this.w_LOOP = 0
        do while this.w_LOOP <this.w_INDICE
          this.w_LOOP = this.w_LOOP + 1
          if Not EMpty(Nvl(Des_Row[ this.w_LOOP , 1] ," ")) and this.oParentObject.w_MDSERRIF=Des_Row[ this.w_LOOP , 1] 
             
 Des_Row[ this.w_LOOP, 1] = SPACE(10) 
 Des_Row[ this.w_LOOP ,2] = 0
          endif
        enddo
      endif
      if g_MATR="S" And this.oParentObject.w_GESMAT="S" And this.oParentObject.w_MDDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    ")) And this.oParentObject.w_MDQTAMOV<>0 And this.oParentObject.w_MDFLCASC $ "+-" 
        this.w_LFLRRIF = this.oParentObject.w_FLRRIF
        * --- Nel caso di import da caricamento rapido vince questo rispetto alla query sulle
        *     matricole del documento di origine
        if Type("RigheDoc.cMATR")<>"U"
          ah_Msg("Inserimento matricole riga: %1",.T.,.F.,.F., ALLTRIM(STR(this.oParentObject.w_CPROWORD)))
          GSAR_BDM(this, this.oParentObject.w_MDCODICE , this.oParentObject.w_MDCODLOT , this.oParentObject.w_MDCODUBI , this.oParentObject.w_MDMAGRIG , this.oParentObject.w_MDPREZZO , this.oParentObject.w_MDUNIMIS , this.oParentObject.w_MDSERRIF , this.oParentObject.w_MDROWRIF , SPACE( 15 ), this.oParentObject.w_MDCODCOM, SPACE( 15 ), SPACE(15), SPACE( 15 ), SPACE(15),this.w_PADRE, L_GSAR_MPG_TRSNAME)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if (this.w_LFLRRIF="+" OR this.w_ORCASC="-" ) and this.oParentObject.w_MDTIPRIG="R"
            ah_Msg("Inserimento matricole riga: %1",.T.,.F.,.F., ALLTRIM(STR(this.oParentObject.w_CPROWORD)))
            GSAR_BIM(this,this.w_PADRE, this.oParentObject.w_MDSERRIF, this.oParentObject.w_MDROWRIF)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      SELECT RigheSort
      ENDSCAN
      WAIT CLEAR
      * --- Uscito dallo Ciclo, controllo il resoconto errori dell'ultimo record esaminato.
      *     Questo poich� se l'ultimo documento non avesse errori, la variabile w_RESOCON1 
      *     conterrebbe ancora i riferimenti all'ultimo documento. Deve essere riportata al record precedente
      if this.w_DOCWARN
        * --- Inserisco Warning attivo
        this.w_NOBLOK = .T.
      endif
      * --- Chiude i Cursori
      SELECT RigheDoc
      USE
      SELECT RigheSort
      USE
      if USED("APPCUR")
        SELECT APPCUR
        USE
      endif
      if this.w_NORIG AND g_PERLOT="S" AND this.w_INDICE>0
        * --- Elimino Righe Descrittive
        this.w_LOOP = 0
        do while this.w_LOOP < this.w_INDICE
          this.w_LOOP = this.w_LOOP + 1
          * --- Ricerco la riga..
          this.w_NUMREC = this.w_PADRE.Search( "CPROWNUM=Nvl("+cp_ToStrODBC(Des_Row[ this.w_LOOP , 2])+",0) And Not Deleted()",0 )
          * --- Se la trovo la elimino
          if this.w_NUMREC>=0
            * --- Mi posiziono sulla riga e la cancello...
            this.w_PADRE.SetRow(this.w_NUMREC)     
            this.w_PADRE.DeleteRow()     
          endif
        enddo
      endif
      * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
      this.w_PADRE.RePos(.F.)     
      if this.w_NOBLOK
        this.w_RESOCON1 = this.w_oMESS.ComposeMessage()
        * --- ANNULLA: poich� nella maschera � definita come caller. In questo caso inutilizzata
        *     Gestita nel GSVE_BI2
        * --- Errori
        * --- Lancio la Maschera dei Log Errori
        do GSVE_KLG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Campi e Variabili
    this.oParentObject.w_CPROWNUM = 0
    this.oParentObject.w_CPROWORD = 0
    this.oParentObject.w_MDSERRIF = SPACE(10)
    this.oParentObject.w_MDROWRIF = 0
    this.oParentObject.w_MDFLOMAG = " "
    this.oParentObject.w_MDCODICE = SPACE(20)
    this.oParentObject.w_MDDESART = SPACE(40)
    this.oParentObject.w_MDCODIVA = SPACE(5)
    this.oParentObject.w_MDCODART = SPACE(20)
    this.oParentObject.w_FLFRAZ = " "
    this.oParentObject.w_MDTIPRIG = " "
    this.oParentObject.w_MDUNIMIS = SPACE(3)
    this.oParentObject.w_UNMIS1 = SPACE(3)
    this.oParentObject.w_UNMIS2 = SPACE(3)
    this.oParentObject.w_UNMIS3 = SPACE(3)
    this.oParentObject.w_OPERAT = " "
    this.oParentObject.w_OPERA3 = " "
    this.oParentObject.w_MOLTIP = 0
    this.oParentObject.w_MOLTI3 = 0
    this.oParentObject.w_MDQTAMOV = 0
    this.oParentObject.w_MDQTAUM1 = 0
    this.oParentObject.w_MDQTAIMP = 0
    this.oParentObject.w_MDQTAIM1 = 0
    this.oParentObject.w_MDFLERIF = " "
    this.oParentObject.w_MDFLRESO = " "
    this.oParentObject.w_MDPREZZO = 0
    this.oParentObject.w_VALRIG = 0
    this.oParentObject.w_MDCONTRA = SPACE(15)
    this.w_CONTRA = Space(15)
    this.oParentObject.w_MDSCONT1 = 0
    this.oParentObject.w_MDSCONT2 = 0
    this.oParentObject.w_MDSCONT3 = 0
    this.oParentObject.w_MDSCONT4 = 0
    this.oParentObject.w_GRUMER = SPACE(5)
    this.oParentObject.w_CATSCA = SPACE(5)
    this.oParentObject.w_PERIVA = 0
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive riga Riferimenti
    * --- Nuova Riga del Temporaneo
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiungo una riga sul Temporaneo
    this.w_PADRE.AddRow()     
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    if g_PERLOT="S"
      if this.w_INDICE=0
         
 Des_Row[1, 1] = this.w_NEWDOC 
 Des_Row[1, 2] = this.oParentObject.w_CPROWNUM
        this.w_INDICE = 1
      else
        this.w_INDICE = this.w_INDICE +1
         
 DIMENSION Des_Row[this.w_INDICE, 2] 
 Des_Row[this.w_INDICE, 1] = this.w_NEWDOC 
 Des_Row[this.w_INDICE, 2] = this.oParentObject.w_CPROWNUM
      endif
    endif
    this.oParentObject.w_MDCODICE = g_ARTDES
    this.oParentObject.w_MDTIPRIG = "D"
    this.oParentObject.w_MDCODART = this.w_ARTDES
    this.oParentObject.w_MDDESART = this.w_APPART
    this.oParentObject.w_MDFLRESO = " "
    this.oParentObject.w_MDFLCASC = "-"
    this.oParentObject.w_ARCODART = this.oParentObject.w_MDCODART
    this.oParentObject.w_MDMAGSAL = SPACE(5)
    this.oParentObject.w_MDLOTMAG = SPACE(5)
    this.oParentObject.w_MDPREZZO = 0
    this.oParentObject.w_LIPREZZO = 0
    this.oParentObject.w_MDQTAMOV = 0
    this.oParentObject.w_MDQTAUM1 = 0
    this.oParentObject.w_MDQTAIMP = 0
    this.oParentObject.w_MDQTAIM1 = 0
    this.oParentObject.w_VALRIG = 0
    this.oParentObject.w_RIGNET = 0
    * --- Carica il Temporaneo
    this.w_PADRE.TrsFromWork()     
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge Testata Documento di Origine
    * --- Inizializza dati Testata
    this.w_TCF = " "
    this.w_RICON = SPACE(15)
    this.w_SCPAG = 0
    this.w_SCOFIS = 0
    this.w_SCCL1 = 0
    this.w_SCCL2 = 0
    this.w_NRIF = 0
    this.w_NEST = 0
    this.w_FLFOSC = " "
    this.w_FLCAPA = " "
    this.w_DRIF = cp_CharToDate("  -  -  ")
    this.w_DEST = cp_CharToDate("  -  -  ")
    this.w_COPAG = SPACE(5)
    this.w_CLADOC = "  "
    this.w_ARIF = Space(10)
    this.w_AEST = Space(10)
    this.w_ACCONT = 0
    this.w_ACCPRE = 0
    this.w_FLSCOR = " "
    this.w_ODOC = SPACE(5)
    * --- Attenzione a w_APPOSER, proviene dalla pagina precedente
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVTIPCON,MVSCOPAG,MVSCONTI,MVSCOCL2,MVSCOCL1,MVCODCON,MVNUMDOC,MVNUMEST,MVFLFOSC,MVFLCAPA,MVDATDOC,MVDATEST,MVCODPAG,MVCLADOC,MVALFDOC,MVALFEST,MVACCPRE,MVACCONT,MVFLSCOR,MVTIPDOC"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_APPOSER);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVTIPCON,MVSCOPAG,MVSCONTI,MVSCOCL2,MVSCOCL1,MVCODCON,MVNUMDOC,MVNUMEST,MVFLFOSC,MVFLCAPA,MVDATDOC,MVDATEST,MVCODPAG,MVCLADOC,MVALFDOC,MVALFEST,MVACCPRE,MVACCONT,MVFLSCOR,MVTIPDOC;
        from (i_cTable) where;
            MVSERIAL = this.w_APPOSER;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TCF = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
      this.w_SCPAG = NVL(cp_ToDate(_read_.MVSCOPAG),cp_NullValue(_read_.MVSCOPAG))
      this.w_SCOFIS = NVL(cp_ToDate(_read_.MVSCONTI),cp_NullValue(_read_.MVSCONTI))
      this.w_SCCL2 = NVL(cp_ToDate(_read_.MVSCOCL2),cp_NullValue(_read_.MVSCOCL2))
      this.w_SCCL1 = NVL(cp_ToDate(_read_.MVSCOCL1),cp_NullValue(_read_.MVSCOCL1))
      this.w_RICON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      this.w_NRIF = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
      this.w_NEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
      this.w_FLFOSC = NVL(cp_ToDate(_read_.MVFLFOSC),cp_NullValue(_read_.MVFLFOSC))
      this.w_FLCAPA = NVL(cp_ToDate(_read_.MVFLCAPA),cp_NullValue(_read_.MVFLCAPA))
      this.w_DRIF = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_DEST = NVL(cp_ToDate(_read_.MVDATEST),cp_NullValue(_read_.MVDATEST))
      this.w_COPAG = NVL(cp_ToDate(_read_.MVCODPAG),cp_NullValue(_read_.MVCODPAG))
      this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
      this.w_ARIF = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
      this.w_AEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
      this.w_ACCPRE = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
      this.w_ACCONT = NVL(cp_ToDate(_read_.MVACCONT),cp_NullValue(_read_.MVACCONT))
      this.w_FLSCOR = NVL(cp_ToDate(_read_.MVFLSCOR),cp_NullValue(_read_.MVFLSCOR))
      this.w_ODOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Variabili contenenti gli errori bloccanti e solo warning di tutti i documenti esaminati
    this.w_DATIDOC = ah_Msgformat("Documento di origine: n. %1 del %2%0",ALLTR(STR(this.w_NRIF,15,0)) + IIF(EMPTY(this.w_ARIF), "", "/"+Alltrim(this.w_ARIF)), DTOC(this.w_DRIF) )
    * --- Se ordine riporta acconto solo se caparra
    this.w_ACCONT = IIF(this.w_CLADOC="OR" AND this.w_FLCAPA<>"S", 0, this.w_ACCONT)
    * --- Primo Documento Selezionato (solo in Caricamento Documento)
    if this.w_PADRE.cFunction="Load"
      if this.oParentObject.w_GIAIMP<>"S" 
        this.oParentObject.w_GIAIMP = "S"
        * --- Importa i Dati di testata del Primo Doc. di Origine sul Doc. di Destinazione
        if NOT EMPTY(this.w_COPAG) AND this.w_RECTRS=0
          this.oParentObject.w_MDSCOCL1 = this.w_SCCL1
          this.oParentObject.w_MDSCOCL2 = this.w_SCCL2
          this.oParentObject.w_MDSCOPAG = this.w_SCPAG
          this.oParentObject.w_MDCODPAG = this.w_COPAG
        endif
      endif
    endif
    * --- Importa le Spese Accessorie e/o Sconti Forzati e/o Acconti
    if this.w_FLFOSC="S" OR this.w_ACCONT<>0 OR this.w_ACCPRE<>0
      * --- ATTENZIONE: Si presume che sia Importabile un solo Documento con Spese Accessorie/Sconti Forzati (non importa se uno solo o il primo dell'elenco)
      this.oParentObject.w_MDACCPRE = this.oParentObject.w_MDACCPRE + (this.w_ACCONT+this.w_ACCPRE)
      if this.w_FLFOSC="S"
        this.oParentObject.w_MDFLFOSC = "S"
        this.oParentObject.w_MDSCONTI = this.oParentObject.w_MDSCONTI + this.w_SCOFIS
      endif
      if this.w_ACCONT<>0
        * --- Testa che sono stati importati acconti per dare avviso
        this.w_oPART = this.w_oMESS.addmsgpartNL("%1%0Sul documento importato � presente un acconto:%0Assicurarsi che non sia gi� stato considerato in precedenti importazioni")
        this.w_oPART.addParam(this.w_DATIDOC)     
        this.w_DOCWARN = .T.
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizioni Variabili Globali
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiungo una riga sul Temporaneo
    this.w_PADRE.AddRow()     
    this.w_PADRE.ChildrenChangeRow()     
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.oParentObject.w_MDSERRIF = MVSERIAL
    this.oParentObject.w_MDROWRIF = CPROWNUM
    this.w_VISEVA = MVIMPEVA
    this.oParentObject.w_MDFLCASC = IIF(this.oParentObject.w_MDFLRESO="S", "+", "-")
    this.w_APPO1 = 0
    this.w_APPO2 = 0
    this.w_APPO4 = 0
    this.w_APPO5 = 0
    * --- Read from DOC_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVQTAMOV,MVQTAEVA,MVQTAUM1,MVQTAEV1,MVCODICE,MVCODIVA,MVDESART,MVFLOMAG,MVPREZZO,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVTIPRIG,MVUNIMIS,MVFLEVAS,MVIMPEVA,MVCONTRA,MVCODLOT,MVCODUBI,MVFLLOTT,MVFLCASC,MVFLRISE"+;
        " from "+i_cTable+" DOC_DETT where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDSERRIF);
            +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MDROWRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVQTAMOV,MVQTAEVA,MVQTAUM1,MVQTAEV1,MVCODICE,MVCODIVA,MVDESART,MVFLOMAG,MVPREZZO,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVTIPRIG,MVUNIMIS,MVFLEVAS,MVIMPEVA,MVCONTRA,MVCODLOT,MVCODUBI,MVFLLOTT,MVFLCASC,MVFLRISE;
        from (i_cTable) where;
            MVSERIAL = this.oParentObject.w_MDSERRIF;
            and CPROWNUM = this.oParentObject.w_MDROWRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APPO1 = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
      this.w_APPO2 = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
      this.w_APPO4 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
      this.w_APPO5 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
      this.oParentObject.w_MDCODICE = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
      this.oParentObject.w_MDCODIVA = NVL(cp_ToDate(_read_.MVCODIVA),cp_NullValue(_read_.MVCODIVA))
      this.oParentObject.w_MDDESART = NVL(cp_ToDate(_read_.MVDESART),cp_NullValue(_read_.MVDESART))
      this.oParentObject.w_MDFLOMAG = NVL(cp_ToDate(_read_.MVFLOMAG),cp_NullValue(_read_.MVFLOMAG))
      this.oParentObject.w_MDPREZZO = NVL(cp_ToDate(_read_.MVPREZZO),cp_NullValue(_read_.MVPREZZO))
      this.oParentObject.w_MDSCONT1 = NVL(cp_ToDate(_read_.MVSCONT1),cp_NullValue(_read_.MVSCONT1))
      this.oParentObject.w_MDSCONT2 = NVL(cp_ToDate(_read_.MVSCONT2),cp_NullValue(_read_.MVSCONT2))
      this.oParentObject.w_MDSCONT3 = NVL(cp_ToDate(_read_.MVSCONT3),cp_NullValue(_read_.MVSCONT3))
      this.oParentObject.w_MDSCONT4 = NVL(cp_ToDate(_read_.MVSCONT4),cp_NullValue(_read_.MVSCONT4))
      this.oParentObject.w_MDTIPRIG = NVL(cp_ToDate(_read_.MVTIPRIG),cp_NullValue(_read_.MVTIPRIG))
      this.oParentObject.w_MDUNIMIS = NVL(cp_ToDate(_read_.MVUNIMIS),cp_NullValue(_read_.MVUNIMIS))
      this.oParentObject.w_OLDEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
      this.oParentObject.w_ORDEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
      this.w_CONTRA = NVL(cp_ToDate(_read_.MVCONTRA),cp_NullValue(_read_.MVCONTRA))
      this.oParentObject.w_MDCODLOT = NVL(cp_ToDate(_read_.MVCODLOT),cp_NullValue(_read_.MVCODLOT))
      this.oParentObject.w_MDCODUBI = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
      this.oParentObject.w_MDFLLOTT = NVL(cp_ToDate(_read_.MVFLLOTT),cp_NullValue(_read_.MVFLLOTT))
      this.w_ORCASC = NVL(cp_ToDate(_read_.MVFLCASC),cp_NullValue(_read_.MVFLCASC))
      this.oParentObject.w_FLRRIF = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.oParentObject.w_MDFLOMAG = "I"
      * --- Righe Omaggio Imponibile.
      *     Non sono gestite quindi le tramuto in Omaggio Imponibile + Iva.
      this.oParentObject.w_MDFLOMAG = "E"
      * --- Se w_docwarn � gi� true non inserisco i dati di testata del documento perch� per quel documento ci sono gi�
      if ! this.w_DOCWARN
        this.w_oPART = this.w_oMESS.addmsgpartNL("%1Riga %2 riga ad omaggio imponibile%0Questa riga verr� importata come omaggio imponibile + IVA")
        this.w_oPART.addParam(this.w_DATIDOC)     
        this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MDROWRIF,6,0)))     
      else
        this.w_oPART = this.w_oMESS.addmsgpartNL("Riga %1 riga ad omaggio imponibile%0Questa riga verr� importata come omaggio imponibile + IVA")
        this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MDROWRIF,6,0)))     
      endif
      this.w_DOCWARN = .T.
    endif
    SELECT RigheSort
    do case
      case this.oParentObject.w_MDTIPRIG="F"
        this.oParentObject.w_MDQTAMOV = 1
        this.oParentObject.w_MDQTAIMP = 1
        this.w_VISEVA = IIF(NVL(MVIMPEVA, 0)<>0, MVIMPEVA, IIF(MVQTAEVA<>0, this.oParentObject.w_MDPREZZO-this.oParentObject.w_ORDEVA, this.w_VISEVA))
      case this.oParentObject.w_MDTIPRIG="D"
        this.w_VISEVA = 0
        this.oParentObject.w_MDQTAMOV = 0
        this.oParentObject.w_MDQTAIMP = 0
      otherwise
        this.w_VISEVA = NVL(MVIMPEVA,0)
        this.oParentObject.w_MDQTAMOV = NVL(MVQTAEVA,0)
        this.oParentObject.w_MDQTAIMP = MIN(MVQTAMOV, MVQTAEVA)
    endcase
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_MDCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
        from (i_cTable) where;
            CACODICE = this.oParentObject.w_MDCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_MDCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      this.oParentObject.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      this.oParentObject.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      this.oParentObject.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS2,ARUNMIS1,AROPERAT,ARMOLTIP,ARGRUMER,ARFLSERG,ARCATSCM,ARARTPOS,ARCODREP,ARDTOBSO,ARTIPSER,ARFLLOTT,ARGESMAT,ARFLDISP,ARFLUSEP"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MDCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS2,ARUNMIS1,AROPERAT,ARMOLTIP,ARGRUMER,ARFLSERG,ARCATSCM,ARARTPOS,ARCODREP,ARDTOBSO,ARTIPSER,ARFLLOTT,ARGESMAT,ARFLDISP,ARFLUSEP;
        from (i_cTable) where;
            ARCODART = this.oParentObject.w_MDCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.oParentObject.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      this.oParentObject.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      this.oParentObject.w_GRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
      this.oParentObject.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
      this.oParentObject.w_CATSCA = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
      this.oParentObject.w_ARTPOS = NVL(cp_ToDate(_read_.ARARTPOS),cp_NullValue(_read_.ARARTPOS))
      this.oParentObject.w_MDCODREP = NVL(cp_ToDate(_read_.ARCODREP),cp_NullValue(_read_.ARCODREP))
      this.w_DTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
      this.oParentObject.w_FLSERA = NVL(cp_ToDate(_read_.ARTIPSER),cp_NullValue(_read_.ARTIPSER))
      this.oParentObject.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
      this.oParentObject.w_GESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
      this.oParentObject.w_ARTDIS = NVL(cp_ToDate(_read_.ARFLDISP),cp_NullValue(_read_.ARFLDISP))
      this.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo obsolescenza articolo
    if NOT EMPTY(this.w_DTOBSO) AND this.w_DTOBSO<=this.oParentObject.w_MDDATREG
      this.w_MESS = "Attenzione: articolo %1 obsoleto dal %2"
      ah_ErrorMsg(this.w_MESS,,"", ALLTRIM(this.oParentObject.w_MDCODART), DTOC(this.w_DTOBSO) )
    endif
    * --- Legge Cod.IVA Articolo
    if NOT EMPTY(this.oParentObject.w_MDCODREP)
      * --- Read from REP_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.REP_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.REP_ARTI_idx,2],.t.,this.REP_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "RECODIVA,REDESREP"+;
          " from "+i_cTable+" REP_ARTI where ";
              +"RECODREP = "+cp_ToStrODBC(this.oParentObject.w_MDCODREP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          RECODIVA,REDESREP;
          from (i_cTable) where;
              RECODREP = this.oParentObject.w_MDCODREP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_MDCODIVA = NVL(cp_ToDate(_read_.RECODIVA),cp_NullValue(_read_.RECODIVA))
        this.oParentObject.w_DESREP = NVL(cp_ToDate(_read_.REDESREP),cp_NullValue(_read_.REDESREP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if NOT EMPTY(NVL(this.oParentObject.w_MDCODIVA," "))
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MDCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.oParentObject.w_MDCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- U.M. Frazionabile
    if NOT EMPTY(NVL(this.oParentObject.w_MDUNIMIS," "))
      * --- Read from UNIMIS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UNIMIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UMFLFRAZ"+;
          " from "+i_cTable+" UNIMIS where ";
              +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MDUNIMIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UMFLFRAZ;
          from (i_cTable) where;
              UMCODICE = this.oParentObject.w_MDUNIMIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.oParentObject.w_PRZDES="S"
      * --- Ricalcolo Prezzo attivato sulla Modalit� di Vendita.
      *     I prezzi verranno ricalcolati in base ai listini/contratti validi sulla vendita.
      this.w_PADRE.NotifyEvent("Ricalcola")     
    else
      if (g_GESCON="S" and not Empty( this.oParentObject.w_CODCLI ) and not Empty( this.w_CONTRA ))
        * --- Verifico se il contratto impostato nel documento di origine � ancora valido
        * --- Controllo se il contratto di origine � valido per il documento di destinazione.
        *     Altrimenti lo svuoto.
        * --- Read from CON_TRAM
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CON_TRAM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2],.t.,this.CON_TRAM_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS"+;
            " from "+i_cTable+" CON_TRAM where ";
                +"CONUMERO = "+cp_ToStrODBC(this.w_CONTRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
            from (i_cTable) where;
                CONUMERO = this.w_CONTRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CT = NVL(cp_ToDate(_read_.COTIPCLF),cp_NullValue(_read_.COTIPCLF))
          this.w_CC = NVL(cp_ToDate(_read_.COCODCLF),cp_NullValue(_read_.COCODCLF))
          this.w_CM = NVL(cp_ToDate(_read_.COCATCOM),cp_NullValue(_read_.COCATCOM))
          this.w_CI = NVL(cp_ToDate(_read_.CODATINI),cp_NullValue(_read_.CODATINI))
          this.w_CF = NVL(cp_ToDate(_read_.CODATFIN),cp_NullValue(_read_.CODATFIN))
          this.w_CV = NVL(cp_ToDate(_read_.COCODVAL),cp_NullValue(_read_.COCODVAL))
          this.w_IVACON = NVL(cp_ToDate(_read_.COIVALIS),cp_NullValue(_read_.COIVALIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if CHKCONTR(this.w_CONTRA,"C",this.oParentObject.w_CODCLI,this.oParentObject.w_CATCOM,this.w_FLSCOR,g_CODEUR,this.oParentObject.w_MDDATREG,this.w_CT,this.w_CC,this.w_CM,this.w_CV,this.w_CI,this.w_CF,this.w_IVACON,.T.)
          this.oParentObject.w_MDCONTRA = this.w_CONTRA
        else
          * --- Contratto incongruente
          * --- Se w_docwarn � gi� true non inserisco i dati di testata del documento perch� per quel documento ci sono gi�
          if ! this.w_DOCWARN
            this.w_oPART = this.w_oMESS.addmsgpartNL("%1Riga %2 Contratto %3 incongruente")
            this.w_oPART.addParam(this.w_DATIDOC)     
            this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MDROWRIF,6,0)))     
            this.w_oPART.addParam(Alltrim(this.w_CONTRA))     
          else
            this.w_oPART = this.w_oMESS.addmsgpartNL("Riga %1 Contratto %2 incongruente")
            this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MDROWRIF,6,0)))     
            this.w_oPART.addParam(Alltrim(this.w_CONTRA))     
          endif
          this.w_DOCWARN = .T.
          this.oParentObject.w_MDCONTRA = SPACE(15)
        endif
      else
        this.oParentObject.w_MDCONTRA = SPACE(15)
      endif
    endif
    SELECT RigheSort
    * --- Per eliminare eventuali differenze di arrotondamento sugli importi evasi
    this.w_VISEVA = IIF(this.w_VISEVA=cp_ROUND(this.oParentObject.w_MDPREZZO-this.oParentObject.w_ORDEVA, 2), this.oParentObject.w_MDPREZZO-this.oParentObject.w_ORDEVA, this.w_VISEVA)
    this.w_VISEVA = cp_ROUND(this.w_VISEVA, this.oParentObject.w_DECUNI)
    if this.oParentObject.w_MDTIPRIG="D"
      * --- Nel caso di righe descrittive ho due situazioni
      *     a) Riga da evadere  (con check attivo), in questo caso valorizzo MVFLARIF
      *     b) Riga da riportare che non evade (MVQTAEVA<>0) sbianco MVFLARIF per invalidre il meccanismo dell'evasione
      this.oParentObject.w_MDFLARIF = IIF( XCHK=1 , "+" , " " ) 
    else
      this.oParentObject.w_MDFLARIF = "+"
    endif
    do case
      case XCHK=1
        this.oParentObject.w_MDFLERIF = "S"
      case this.oParentObject.w_MDTIPRIG="F" AND this.w_VISEVA>=(this.oParentObject.w_MDPREZZO-this.oParentObject.w_ORDEVA)
        this.oParentObject.w_MDFLERIF = "S"
      case NOT this.oParentObject.w_MDTIPRIG $ "DF" AND this.oParentObject.w_MDQTAIMP>=(this.w_APPO1-this.w_APPO2)
        this.oParentObject.w_MDFLERIF = "S"
      otherwise
        this.oParentObject.w_MDFLERIF = " "
    endcase
    * --- Se riga Forfettaria Importa x valore (Prende l'importo del Cursore)
    this.oParentObject.w_MDPREZZO = IIF(this.oParentObject.w_MDTIPRIG="F", this.w_VISEVA, this.oParentObject.w_MDPREZZO)
    * --- Per non ricalcolare gli Sconti
    this.oParentObject.w_OFLSCO = "S"
    if this.w_FLSCOR<>"S" AND this.oParentObject.w_PERIVA<>0 AND this.oParentObject.w_MDPREZZO<>0 AND this.oParentObject.w_PRZDES<>"S"
      * --- Se documento di Origine con prezzi al Netto Calcola il Prezzo al Lordo 
      *     solo se disattivo il flag PRZDES(Ricalcolo prezzo) nella modalit� di vendit�, 
      *     poich� questo calcolo viene gi� effettuato nel GSPS_BCD (Evento 'Ricalcola')
      this.oParentObject.w_MDPREZZO = this.oParentObject.w_MDPREZZO + cp_ROUND(((this.oParentObject.w_MDPREZZO * this.oParentObject.w_PERIVA) / 100), g_PERPUL)
    endif
    this.oParentObject.w_ARCODART = this.oParentObject.w_MDCODART
    * --- Unita' di Misura
    this.oParentObject.w_UNMIS1 = LEFT(this.oParentObject.w_UNMIS1+SPACE(3), 3)
    * --- Leggo flag no frazionalbile e modifica 2a UM della 1a UM
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMFLFRAZ,UMMODUM2"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMFLFRAZ,UMMODUM2;
        from (i_cTable) where;
            UMCODICE = this.oParentObject.w_UNMIS1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_UNMIS2 = LEFT(this.oParentObject.w_UNMIS2+SPACE(3), 3)
    this.oParentObject.w_UNMIS3 = LEFT(this.oParentObject.w_UNMIS3+SPACE(3), 3)
    this.oParentObject.w_MDMAGSAL = IIF(this.oParentObject.w_MDTIPRIG="R" AND this.oParentObject.w_MDFLCASC<>"N", this.oParentObject.w_MDCODMAG, SPACE(5))
    this.oParentObject.w_MDMAGRIG = this.oParentObject.w_MDMAGSAL
    if NOT EMPTY(this.oParentObject.w_MDMAGSAL) AND NOT EMPTY(this.oParentObject.w_MDCODART)
      * --- Read from SALDIART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SLQTAPER,SLQTRPER,SLVALUCA"+;
          " from "+i_cTable+" SALDIART where ";
              +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MDCODART);
              +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MDMAGSAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SLQTAPER,SLQTRPER,SLVALUCA;
          from (i_cTable) where;
              SLCODICE = this.oParentObject.w_MDCODART;
              and SLCODMAG = this.oParentObject.w_MDMAGSAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_QTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
        this.oParentObject.w_QTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
        this.oParentObject.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_QTDISP = this.oParentObject.w_QTAPER-this.oParentObject.w_QTRPER
    endif
    this.w_APPO = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MDUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+" Q"
    msg=""
    this.oParentObject.w_MDQTAUM1 = CALQTA(this.oParentObject.w_MDQTAMOV,this.oParentObject.w_MDUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
    * --- Qt� evasa nella prima unit� di misura, utilizzata per stornare la qt� evasa sul documento di origine
    *     (MVQTAUM1,MVQTAIM1)
    if this.oParentObject.w_MDTIPRIG$"F-D"
      this.oParentObject.w_MDQTAUM1 = IIF( this.oParentObject.w_MDTIPRIG="F" ,1,0 )
      this.oParentObject.w_MDQTAIM1 = IIF( this.oParentObject.w_MDTIPRIG="F" ,1,0 )
    else
      if this.oParentObject.w_MDUNIMIS<>this.oParentObject.w_UNMIS1
        * --- Ricalcolo la qt� movimentata del documento di origine nella prima unit� di misura
        *     e la confronto con la qt� nella prima unit� di misura sempre del doc
        *     di origine. Se diverse allora l'utente ha modificato la qt� nella prima unit� di misura 
        *     quindi nel doc. di destinazione calcolo MVQTAUM1 come rapporto dato da:
        *     
        *     ( Qt� 1 Mis. Origine / Qt� movimenta Origine ) * Qta movimentata Destinazione
        *     
        *     Ricalcolo la qt� nella prima unit� di misura del doc. di origine
        this.w_TmpN = CALMMLIS(this.w_APPO1,this.w_APPO,this.oParentObject.w_MOLTIP,this.oParentObject.w_MOLTI3,0)
        if this.w_TmpN<>this.w_QTAUM1
          * --- Eseguo la proporzione..
          this.oParentObject.w_MDQTAUM1 = ( this.w_QTAUM1 / this.w_APPO1 ) * this.oParentObject.w_MDQTAMOV
          this.oParentObject.w_MDQTAIM1 = ( this.w_QTAUM1 / this.w_APPO1 ) * this.oParentObject.w_MDQTAIMP
        else
          this.oParentObject.w_MDQTAIM1 = CALQTA(this.oParentObject.w_MDQTAIMP,this.oParentObject.w_MDUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
        endif
      else
        this.oParentObject.w_MDQTAIM1 = CALQTA(this.oParentObject.w_MDQTAIMP,this.oParentObject.w_MDUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
      endif
      if this.w_NOFRAZ="S" AND this.oParentObject.w_MDQTAUM1<>INT(this.oParentObject.w_MDQTAUM1)
        if this.w_MODUM2="S" 
          * --- Se sulla 1 UM ho il flag forza UM secondarie e le UM non sono separate arrotondo la 1UM all'intero superiore  
          *     e riproporziono l'UM movimentata
          *     altrimenti arrotondo all'intero 
          if this.w_FLUSEP=" "
            this.Pag7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Se UM separate arrotondo la qt� della 1UM ma lascio invariata la riga 
            this.oParentObject.w_MDQTAUM1 = cp_ROUND(this.oParentObject.w_MDQTAUM1,0)
            this.oParentObject.w_MDQTAIM1 = cp_ROUND(this.oParentObject.w_MDQTAIM1,0)
          endif
        else
          if this.w_FLUSEP<>" "
            * --- Se UM separate arrotondo la qt� della 1UM
            this.oParentObject.w_MDQTAUM1 = cp_ROUND(this.oParentObject.w_MDQTAUM1,0)
            this.oParentObject.w_MDQTAIM1 = cp_ROUND(this.oParentObject.w_MDQTAIM1,0)
          else
            * --- segnalo errore
            msg="Attenzione: articolo privo di unit� di misura separate e prima unit� di misura non frazionabile%0La quantit� nella 1a UM non pu� essere arrotondata all'intero"
            this.oParentObject.w_MDQTAUM1 = 0
            this.oParentObject.w_MDQTAIM1 = 0
          endif
        endif
      endif
    endif
    if NOT EMPTY(msg)
      * --- Errore relativo alla quantit� della 1a UM
      Ah_ErrorMsg(msg)
    endif
    this.oParentObject.w_MDSCOVEN = 0
    * --- ----------
    if this.oParentObject.w_PRZDES<>"S"
      * --- Solo se disattivo il flag PRZDES(Ricalcolo prezzo) nella modalit� di vendit�, 
      *     poich� altrimenti queste variabili vengono gi� calcolate nel GSPS_BCD (Evento 'Ricalcola')
      this.oParentObject.w_VALRIG = IIF(g_FLESSC="S" And this.oParentObject.w_MDDATREG >= g_DATESC ,CAVALRIG(this.oParentObject.w_MDPREZZO,this.oParentObject.w_MDQTAMOV, 0, 0, 0, 0,this.oParentObject.w_DECTOT), CAVALRIG(this.oParentObject.w_MDPREZZO,this.oParentObject.w_MDQTAMOV, this.oParentObject.w_MDSCONT1,this.oParentObject.w_MDSCONT2,this.oParentObject.w_MDSCONT3,this.oParentObject.w_MDSCONT4,this.oParentObject.w_DECTOT) )
      this.oParentObject.w_RIGNET = IIF(this.oParentObject.w_MDFLOMAG="X", this.oParentObject.w_VALRIG,IIF(this.oParentObject.w_MDFLOMAG="I", this.oParentObject.w_VALRIG - CALNET(this.oParentObject.w_VALRIG,this.oParentObject.w_PERIVA,this.oParentObject.w_DECTOT,Space(3),0),0))
      this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE + this.oParentObject.w_VALRIG
      this.oParentObject.w_MDTOTVEN = this.oParentObject.w_MDTOTVEN + this.oParentObject.w_RIGNET
    endif
    * --- Gestione Lotti
    this.oParentObject.w_MDFLLOTT = " "
    if EMPTY(this.oParentObject.w_MDMAGRIG)
      this.w_LFLUBIC = " "
    else
      if this.w_OLDMAG<>this.oParentObject.w_MDMAGRIG
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGFLUBIC"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MDMAGRIG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGFLUBIC;
            from (i_cTable) where;
                MGCODMAG = this.oParentObject.w_MDMAGRIG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LFLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Assegna qui per evitare problemi con l'inizializzazione dei Magazzini (InitRiw)
    this.oParentObject.w_FLUBIC = this.w_LFLUBIC
    this.w_OLDMAG = this.oParentObject.w_MDMAGRIG
    if this.oParentObject.w_MDTIPRIG="R" AND ((this.oParentObject.w_FLLOTT $ "SC" AND g_PERLOT="S") OR (g_PERUBI="S" AND this.w_LFLUBIC="S"))
      * --- Importo Informazioni relative ai lotti
      this.w_FLLOTT1 = IIF((this.oParentObject.w_FLLOTT $ "SC" AND g_PERLOT="S") OR (this.w_LFLUBIC="S" AND g_PERUBI="S") , Alltrim(this.oParentObject.w_MDFLCASC)," ")
      this.oParentObject.w_MDFLLOTT = this.w_FLLOTT1
      this.oParentObject.w_MDCODUBI = IIF(g_PERUBI="S" AND this.oParentObject.w_MDFLLOTT $ "+-" AND this.w_LFLUBIC="S" ,this.w_CODUBI,SPACE(20))
      this.oParentObject.w_MDCODLOT = IIF(g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" AND this.oParentObject.w_MDFLLOTT $ "+-" ,this.w_CODLOT,SPACE(20))
      if NOT EMPTY(this.oParentObject.w_MDCODLOT)
        * --- Read from LOTTIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LOTTIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LOFLSTAT,LODATSCA"+;
            " from "+i_cTable+" LOTTIART where ";
                +"LOCODICE = "+cp_ToStrODBC(this.oParentObject.w_MDCODLOT);
                +" and LOCODART = "+cp_ToStrODBC(this.oParentObject.w_MDCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LOFLSTAT,LODATSCA;
            from (i_cTable) where;
                LOCODICE = this.oParentObject.w_MDCODLOT;
                and LOCODART = this.oParentObject.w_MDCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_FLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
          this.w_LODATSCA = NVL(cp_ToDate(_read_.LODATSCA),cp_NullValue(_read_.LODATSCA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if (this.w_LODATSCA<this.oParentObject.w_MDDATREG AND NOT EMPTY(this.w_LODATSCA)) 
          * --- Se lotto Scaduto o non Disponibile non viene importato
          this.oParentObject.w_MDCODLOT = SPACE(20)
        endif
      endif
      this.oParentObject.w_MDLOTMAG = iif( Empty( this.oParentObject.w_MDCODLOT ) And Empty( this.oParentObject.w_MDCODUBI ) , SPACE(5) , LEFT(this.oParentObject.w_MDMAGSAL,5) )
    endif
    * --- ---------
    * --- Carica il Temporaneo
    this.w_CNT = this.w_CNT + 1
    ah_Msg("Inserimento riga: %1",.T.,.F.,.F., ALLTRIM(STR(this.w_CNT)) )
    * --- Carica il Temporaneo dei Dati
    SELECT (this.w_PADRE.cTrsName)
    this.w_PADRE.TrsFromWork()     
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se la prima U.M. non � frazionabile ed � attivo il flag Forza seconda U.M. e la Qt� nella Prima U.M. ha decimali
    *     ricalcolo la qt� nella Prima U.M. arrotondata alla unit� superiore
    * --- memorizzo il valore iniziale di mvqtaum1
    this.w_QTATEST = this.oParentObject.w_MDQTAUM1
    this.w_QTATEST1 = this.oParentObject.w_MDQTAIM1
    this.oParentObject.w_MDQTAUM1 = INT(this.w_QTATEST)+1
    this.oParentObject.w_MDQTAIM1 = INT(this.w_QTATEST1)+1
    * --- Ricalcolo la quantit� su riga in base alla nuova Qt� nella prima U.M.
    this.oParentObject.w_MDQTAMOV = cp_Round((this.oParentObject.w_MDQTAMOV*this.oParentObject.w_MDQTAUM1)/this.w_QTATEST,g_PERPQT)
    * --- verificare se corretto
    this.oParentObject.w_MDQTAIMP = this.oParentObject.w_MDQTAMOV
    this.oParentObject.w_MDQTAIM1 = this.oParentObject.w_MDQTAUM1
    * --- --
    * --- Per evitare di nuovo il ricalcolo della Qt� nella prima U.M. dovuta al calculate sul campo nei documenti
    this.oParentObject.o_MDQTAMOV = this.oParentObject.w_MDQTAMOV
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,20)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CON_TRAM'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='DES_DIVE'
    this.cWorkTables[6]='DOC_DETT'
    this.cWorkTables[7]='DOC_MAST'
    this.cWorkTables[8]='DOC_RATE'
    this.cWorkTables[9]='KEY_ARTI'
    this.cWorkTables[10]='LISTINI'
    this.cWorkTables[11]='LOTTIART'
    this.cWorkTables[12]='MAGAZZIN'
    this.cWorkTables[13]='PAG_AMEN'
    this.cWorkTables[14]='REP_ARTI'
    this.cWorkTables[15]='SALDIART'
    this.cWorkTables[16]='TIP_DOCU'
    this.cWorkTables[17]='UNIMIS'
    this.cWorkTables[18]='VALUTE'
    this.cWorkTables[19]='VOCIIVA'
    this.cWorkTables[20]='MAGAZZIN'
    return(this.OpenAllTables(20))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
