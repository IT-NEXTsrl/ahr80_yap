* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bcd                                                        *
*              Calcola prezzo                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_622]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-08                                                      *
* Last revis.: 2012-11-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bcd",oParentObject,m.pExec)
return(i_retval)

define class tgsps_bcd as StdBatch
  * --- Local variables
  pExec = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_LENSCF = 0
  w_MESS = space(1)
  w_ARTCLI = space(1)
  w_cKey = space(20)
  w_CODREP = space(3)
  w_FLLMAG = space(1)
  w_OKSCA = space(1)
  w_FLAVAL = space(1)
  w_CODMAG = space(5)
  w_MLCODICE = space(20)
  w_OBJCTRL = .NULL.
  w_CHECK = .f.
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_QTALIS = 0
  w_PADRE = .NULL.
  w_OK = .f.
  w_APPO = space(12)
  w_QTAUM1 = 0
  w_CODART = space(20)
  w_DATREG = ctod("  /  /  ")
  w_CODLIS = space(5)
  w_CODCON = space(15)
  w_CATCLI = space(5)
  w_CATART = space(5)
  w_APPO1 = 0
  w_CODVAL = space(3)
  w_SCOCON = .f.
  w_CODGRU = space(5)
  w_OLIPREZ = 0
  w_OCONTRA = space(15)
  w_IVACON = space(1)
  w_OPREZZO = 0
  w_OSCONT1 = 0
  w_OSCONT2 = 0
  w_OSCONT3 = 0
  w_OSCONT4 = 0
  w_ROWNUM = 0
  w_PREZZO = 0
  w_QTAMOV = 0
  w_CODICE = space(5)
  w_CALPRZ = 0
  w_RICSCO = .f.
  w_RICPRE = .f.
  w_ORIF = space(1)
  w_MESS = space(1)
  w_SRV = space(1)
  w_QTAORI = 0
  w_QTAEVA = 0
  w_QTARIM = 0
  w_DTOBSO = ctod("  /  /  ")
  w_QTATEST = 0
  w_MAGSAL = space(5)
  w_TESTPRZ = 0
  w_ORIGINE = space(10)
  w_CONTA = 0
  w_OLDSCHED = space(1)
  * --- WorkFile variables
  VOCIIVA_idx=0
  ART_ICOL_idx=0
  REP_ARTI_idx=0
  DOC_DETT_idx=0
  UNIMIS_idx=0
  SALDIART_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Listino da vendita Negozio (da GSPS_MVD)
    * --- Lanciato da:
    * --- M = Cambio Articolo; A = Cambio Quantita'; R = Automatismi (ev. Ricalcola); U = Unita' di Misura; E = Flag Reso
    * --- w_PADRE = GSPS_MVD
    this.w_PADRE = this.oParentObject
    * --- istanzio oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    * --- Ricalcolo la qt� nella prima U.M poich� non � ancora stata calcolata dall'mCalc
    msg=""
    this.oParentObject.w_MDQTAUM1 = IIF(this.oParentObject.w_MDTIPRIG="F", 1, CALQTA(this.oParentObject.w_MDQTAMOV,this.oParentObject.w_MDUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_UNISEP, this.oParentObject.w_NOFRAZ, this.oParentObject.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
    * --- Modifica della quantit� con Qt�<>0 e non movimentata la prima U.M. non per Servizi a valore
    if this.pExec="U" and empty(this.oParentObject.w_MDUNIMIS)
      * --- Cambio unit� di misura da cp_zoom, il campo � ancora vuoto e bisogna ancora selezionare l'unit�.
      i_retcode = 'stop'
      return
    endif
    if this.pExec$"AU" And this.oParentObject.w_MDQTAMOV<>0 And this.oParentObject.w_MDUNIMIS<>this.oParentObject.w_UNMIS1 And this.oParentObject.w_MDTIPRIG<>"F"
      * --- Lettura dei flag U.M. frazionabile e Forza 2^ U.M.
      * --- Ricalcolo della Qt� nella Prima U.M. poich� al lancio del batch non � ancora stata calcolata
      this.w_QTAUM1 = CALQTAADV(this.oParentObject.w_MDQTAMOV,this.oParentObject.w_MDUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_UNISEP, this.oParentObject.w_NOFRAZ, this.oParentObject.w_MODUM2, , this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3,,,This.oparentobject, "MDQTAMOV", .T. )
      * --- Se la prima U.M. non � frazionabile ed � attivo il flag Forza seconda U.M. e la Qt� nella Prima U.M. ha decimali
      *     ricalcolo la qt� nella Prima U.M. arrotondata alla unit� superiore
    endif
    if this.pExec="A" and NOT EMPTY(this.oParentObject.w_MDSERRIF) AND this.oParentObject.w_MDROWRIF<>0
      * --- Cambio quantit� in una riga importata da altro documento
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Rimetto a 0 le variabili che vengono valorizzate in pagina 3 e 4 e poi riutilizzate in pag 1
      if this.oParentObject.w_MDFLERIF<>"S"
        * --- Solo in Importazione documenti.
        *     Se non forzo l'evasione della riga del documento di Origine non devo rifare i calcoli.
        *     In questo caso non deve nemmeno lanciare la mCalc()
        *     
        *     w_MDFLERIF = 'S' se 
        *     -  Evado per una quantit� maggiore rispetto al documento di origine: non fa la domanda
        *               'Evado la Quantita Rimanente sul Documento di Origine?' ma potrebbero esserci condizioni pi� vantaggiose
        *               (altro scaglione sul contratto o listino) 
        *     -  Evdo per una quantit� minore rispetto al documento di origine e rispondo di si alla domanda
        *               'Evado la Quantita Rimanente sul Documento di Origine?' 
        *     
        *     In questi casi devo ricalcolare il Prezzo/Sconti
        * --- Non esegue la mCalc
        this.bUpdateParentObject = .F.
        i_retcode = 'stop'
        return
      endif
    endif
    if this.pExec="E"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Controllo obsolescenza articolo
    if this.pExec="M" AND NOT EMPTY(this.oParentObject.w_MDCODICE)
      this.oParentObject.w_MDFLRESO = " "
    endif
    * --- Attiva il Post-In relativo al Codice Articolo associato al Codice di Ricerca
    if this.pExec="M" AND NOT EMPTY(this.oParentObject.w_MDCODART)
      this.w_cKey = cp_setAzi(i_TableProp[this.w_PADRE.ART_ICOL_IDX, 2]) + "\" + cp_ToStr(this.oParentObject.w_MDCODART, 1)
      cp_ShowWarn(this.w_cKey, this.w_PADRE.ART_ICOL_IDX)
    endif
    if this.oParentObject.w_MDTIPRIG $ "RFMA" AND (this.pExec="M" OR this.oParentObject.w_MDQTAMOV=0)
      * --- Eventuale Default da Causale Documento
      this.oParentObject.w_MDQTAMOV = IIF(this.oParentObject.w_MDQTAMOV=0, this.oParentObject.w_QTADEF, this.oParentObject.w_MDQTAMOV)
      * --- Devo leggere i dati dell'articolo in caso di quantita' proposta in automatico 
      * --- Lasciando invariato l'input sulla qta non verrebbero inizializzati gli sconti ecc.
      if this.pExec="M" AND NOT EMPTY(this.oParentObject.w_MDCODART) AND this.oParentObject.w_MDQTAMOV<>0 
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARGRUMER,ARCATSCM,ARFLSERG,ARARTPOS,ARCODREP,ARFLLOTT,ARPREZUM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MDCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARGRUMER,ARCATSCM,ARFLSERG,ARARTPOS,ARCODREP,ARFLLOTT,ARPREZUM;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_MDCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.oParentObject.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.oParentObject.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.oParentObject.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.oParentObject.w_GRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
          this.oParentObject.w_CATSCA = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
          this.oParentObject.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
          this.oParentObject.w_ARTPOS = NVL(cp_ToDate(_read_.ARARTPOS),cp_NullValue(_read_.ARARTPOS))
          this.w_CODREP = NVL(cp_ToDate(_read_.ARCODREP),cp_NullValue(_read_.ARCODREP))
          this.oParentObject.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
          this.oParentObject.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se ho definito un reparto di deafult modificando la prima riga utilizzo quello
        if Empty(this.oParentObject.w_DEFCODREP)
          this.oParentObject.w_MDCODREP = this.w_CODREP
        else
          this.oParentObject.w_MDCODREP = this.oParentObject.w_DEFCODREP
        endif
        * --- La lettura di un valore carattere null restituisce una stringa lunga
        *     un carattere. Questa variabile � utilizzata per costruire un parametro
        *     come somma di strringhe (CALMMLIS)
        this.oParentObject.w_UNMIS1 = IIF( Empty( this.oParentObject.w_UNMIS1) , Space(3) ,this.oParentObject.w_UNMIS1 )
        this.oParentObject.w_UNMIS2 = IIF( Empty( this.oParentObject.w_UNMIS2) , Space(3) ,this.oParentObject.w_UNMIS2 )
        this.oParentObject.w_MDUNIMIS = IIF(NOT EMPTY(this.oParentObject.w_UNMIS3) AND this.oParentObject.w_MOLTI3<>0, this.oParentObject.w_UNMIS3, this.oParentObject.w_UNMIS1)
        * --- Rileggo anche l'ultimo costo di acquisto che nel caso di quantit� proposta in automatico 
        *     non sarebbe ancora valorizzato
        this.w_MAGSAL = IIF(this.oParentObject.w_MDTIPRIG="R", this.oParentObject.w_MDCODMAG, SPACE(5))
        * --- Read from SALDIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLVALUCA,SLCODVAA"+;
            " from "+i_cTable+" SALDIART where ";
                +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MDCODART);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGSAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLVALUCA,SLCODVAA;
            from (i_cTable) where;
                SLCODICE = this.oParentObject.w_MDCODART;
                and SLCODMAG = this.w_MAGSAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
          this.oParentObject.w_CODVAA = NVL(cp_ToDate(_read_.SLCODVAA),cp_NullValue(_read_.SLCODVAA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.pExec="M" AND NOT EMPTY(this.oParentObject.w_MDCODART) AND this.oParentObject.w_MDQTAMOV=0 
        * --- Se ho definito un reparto di deafult modificando la prima riga utilizzo quello
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODREP,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARGRUMER,ARCATSCM,ARFLSERG,ARARTPOS,ARFLLOTT,ARPREZUM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MDCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODREP,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARGRUMER,ARCATSCM,ARFLSERG,ARARTPOS,ARFLLOTT,ARPREZUM;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_MDCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODREP = NVL(cp_ToDate(_read_.ARCODREP),cp_NullValue(_read_.ARCODREP))
          this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.oParentObject.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.oParentObject.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.oParentObject.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.oParentObject.w_GRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
          this.oParentObject.w_CATSCA = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
          this.oParentObject.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
          this.oParentObject.w_ARTPOS = NVL(cp_ToDate(_read_.ARARTPOS),cp_NullValue(_read_.ARARTPOS))
          this.w_CODREP = NVL(cp_ToDate(_read_.ARCODREP),cp_NullValue(_read_.ARCODREP))
          this.oParentObject.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
          this.oParentObject.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Empty(this.oParentObject.w_DEFCODREP)
          this.oParentObject.w_MDCODREP = this.w_CODREP
        else
          this.oParentObject.w_MDCODREP = this.oParentObject.w_DEFCODREP
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_MDCODREP)
        * --- Read from REP_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.REP_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.REP_ARTI_idx,2],.t.,this.REP_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "RECODIVA"+;
            " from "+i_cTable+" REP_ARTI where ";
                +"RECODREP = "+cp_ToStrODBC(this.oParentObject.w_MDCODREP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            RECODIVA;
            from (i_cTable) where;
                RECODREP = this.oParentObject.w_MDCODREP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_MDCODIVA = NVL(cp_ToDate(_read_.RECODIVA),cp_NullValue(_read_.RECODIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if NOT EMPTY(this.oParentObject.w_MDCODIVA)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MDCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = this.oParentObject.w_MDCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    this.w_CODART = this.oParentObject.w_MDCODART
    if this.pExec="M" AND NOT EMPTY(this.oParentObject.w_MDCODICE)
      * --- Consumo Automatico
      if g_PERLOT="S" AND this.oParentObject.w_FLLOTT="C" AND (this.oParentObject.w_MDFLCASC="-") AND this.oParentObject.w_MDTIPRIG $ "RMA"
        * --- Lotti a Consumo Automatico
        this.w_OKSCA = "F"
        this.w_OKSCA = IIF(this.oParentObject.w_MDFLLOTT="+","T","F")
        * --- Se No Forza Magazzino Lotto cerco movimenti di carico del Lotto con il magazzino
        *     impostato di Default
        *     Nel POS il magazzino deve essere per forza quello di Testata
        this.w_CODMAG = this.oParentObject.w_MDCODMAG
        vq_exec("..\MADV\EXE\QUERY\GSMD_QCA.VQR",this,"LOTTI")
        * --- Cerco il Lotto con data scadenza pi� prossima alla data registrazione e 
        *     a parit� di  data scadenza con data creazione pi� vecchia.
        this.oParentObject.w_MDCODLOT = CERCLOT(this.oParentObject.w_MDDATREG,this.w_CODART,"LOTTI")
        if NOT EMPTY(this.oParentObject.w_MDCODLOT)
          * --- Assegno Magazzino e Ubicazione congruenti al lotto se consumo automatico
          if RECCOUNT("LOTTI")>0
            SELECT LOTTI
            LOCATE FOR LOTTI.LOCODICE = this.oParentObject.w_MDCODLOT
            if FOUND()
              * --- Assegno Magazzino e Ubicazione congruenti al lotto se consumo automatico
              this.oParentObject.w_MDCODUBI = NVL(LOTTI.LOCODUBI,SPACE(20))
              this.oParentObject.w_DATLOT = CP_TODATE(LOTTI.LODATSCA)
              this.oParentObject.w_FLSTAT = "D"
            endif
            SELECT LOTTI
            USE
          endif
        endif
      endif
      if this.oParentObject.w_FLLOTT <>"C" OR this.oParentObject.w_MDTIPRIG $ "DF"
        this.oParentObject.w_MDCODLOT = SPACE(20)
      endif
    endif
    this.w_DATREG = this.oParentObject.w_MDDATREG
    this.w_CODLIS = this.oParentObject.w_MDCODLIS
    this.w_CODCON = this.oParentObject.w_MDCONTRA
    this.w_CODVAL = this.oParentObject.w_MDCODVAL
    this.w_CATCLI = this.oParentObject.w_CATSCC
    this.w_CATART = this.oParentObject.w_CATSCA
    this.w_CODGRU = this.oParentObject.w_GRUMER
    this.w_SCOCON = .F.
    this.w_OLIPREZ = this.oParentObject.w_LIPREZZO
    this.w_OCONTRA = this.oParentObject.w_MDCONTRA
    this.w_OSCONT1 = this.oParentObject.w_MDSCONT1
    this.w_OSCONT2 = this.oParentObject.w_MDSCONT2
    this.w_OSCONT3 = this.oParentObject.w_MDSCONT3
    this.w_OSCONT4 = this.oParentObject.w_MDSCONT4
    this.w_OPREZZO = -9876
    if (this.pExec="A" OR this.pExec="U" Or this.pExec="M") AND this.oParentObject.w_MDPREZZO<>0 AND (NOT EMPTY(this.w_CODLIS) OR NOT EMPTY(this.w_CODCON))
      * --- Segna il Prezzo di origine per eventuale non Conferma finale
      this.w_OPREZZO = this.oParentObject.w_MDPREZZO
    endif
    if (this.oParentObject.w_MDQTAMOV=0 AND this.oParentObject.w_MDTIPRIG $ "RMA") OR EMPTY(this.w_CODART)
      * --- Se non Specifico la Quantita' o l'Articolo esce
      this.oParentObject.w_MDPREZZO = 0
      i_retcode = 'stop'
      return
    endif
    * --- Calcola quantit� nella prima unit� di misura
    * --- Svolgo il Link su MDCODART (On Changed MDCODICE non aggiorna Link collegati)
    if this.oParentObject.w_MDTIPRIG="F"
      this.w_OBJCTRL = this.w_PADRE.GetCtrl( "w_MDCODART" )
      L_Method_Name="this.w_PADRE.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
      this.w_CHECK = &L_Method_Name
      * --- Link su MDCODREP
      this.w_PADRE.w_MDCODREP = IIF(Empty(this.oParentObject.w_DEFCODREP), This.oParentObject.w_CODREP, this.oParentObject.w_DEFCODREP)
      this.w_OBJCTRL = this.w_PADRE.GetCtrl( "w_MDCODREP" )
      L_Method_Name="this.w_PADRE.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
      this.w_CHECK = &L_Method_Name
      this.w_QTAUM1 = this.oParentObject.w_MDQTAUM1
      this.oParentObject.w_MDQTAMOV = IIF(this.oParentObject.w_MDQTAMOV=0, 1, this.oParentObject.w_MDQTAMOV)
    endif
    this.w_APPO = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MDUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_LISIVA, "N")+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI))
    this.w_QTAUM1 = IIF(this.w_QTAUM1=0, CALQTA(this.oParentObject.w_MDQTAMOV,this.oParentObject.w_MDUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_UNISEP, this.oParentObject.w_NOFRAZ, this.oParentObject.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3) ,this.w_QTAUM1)
    DECLARE ARRCALC (16,1)
    * --- Azzero l'Array che verr� riempito dalla Funzione
    ARRCALC(1)=0
    this.w_QTAUM3 = CALQTA(this.w_QTAUM1,this.oParentObject.w_UNMIS3, Space(3),IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
    this.w_QTAUM2 = CALQTA(this.w_QTAUM1,this.oParentObject.w_UNMIS2, this.oParentObject.w_UNMIS2,IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
    DIMENSION pArrUm[9]
    pArrUm [1] = this.oParentObject.w_PREZUM 
 pArrUm [2] = this.oParentObject.w_MDUNIMIS 
 pArrUm [3] = this.oParentObject.w_MDQTAMOV 
 pArrUm [4] = this.oParentObject.w_UNMIS1 
 pArrUm [5] = this.w_QTAUM1 
 pArrUm [6] = this.oParentObject.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.oParentObject.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
    this.w_CALPRZ = CalPrzli( this.oParentObject.w_MDCONTRA , "C" , this.oParentObject.w_MDCODLIS , this.oParentObject.w_MDCODART , this.oParentObject.w_GRUMER , this.w_QTAUM1 , this.oParentObject.w_MDCODVAL , this.oParentObject.w_MDCAOVAL , this.w_DatReg , this.w_CATCLI , this.w_CATART, this.oParentObject.w_CODVAA, this.oParentObject.w_CODCLI, this.oParentObject.w_CATCOM, " ", this.oParentObject.w_SCOLIS, this.oParentObject.w_VALUCA, "P", @ARRCALC, this.oParentObject.w_PRZVAC, "V","N",@pArrUm )
    this.oParentObject.w_CLUNIMIS = ARRCALC(16)
    this.oParentObject.w_LIPREZZO = ARRCALC(5)
    this.w_OK = ARRCALC(7)=2 OR ARRCALC(8)=1
    this.oParentObject.w_LISCON = ARRCALC(7)
    if this.oParentObject.w_MDUNIMIS<>this.oParentObject.w_CLUNIMIS AND NOT EMPTY(this.oParentObject.w_CLUNIMIS)
      this.w_QTALIS = IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS1, this.w_QTAUM1, IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
      if EMPTY(this.oParentObject.w_UNISEP) AND this.oParentObject.w_PREZUM="N"
        * --- Se in anagrafica articolo la combo  U.M. Separate � impostata a No, 
        *     per determinare il prezzo di riga nella seconda, o terza u.m. si utilizza il fattore di conversione tra prima e seconda \ terza u.m..
        this.oParentObject.w_LIPREZZO = CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO, this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, 0)
      else
        this.oParentObject.w_LIPREZZO = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_MDQTAMOV, this.w_QTALIS, "N", 0, this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
      endif
    endif
    * --- Aggiorno gli sconti solo se arrivano da Listino/Contratto/Tabella ScontiMagg.
    this.oParentObject.w_MDSCONT1 = ARRCALC(1)
    this.oParentObject.w_MDSCONT2 = ARRCALC(2)
    this.oParentObject.w_MDSCONT3 = ARRCALC(3)
    this.oParentObject.w_MDSCONT4 = ARRCALC(4)
    * --- Impedisco il ricalcolo degli sconti
    this.oParentObject.o_MDSCONT1 = this.oParentObject.w_MDSCONT1
    this.oParentObject.o_MDSCONT2 = this.oParentObject.w_MDSCONT2
    this.oParentObject.o_MDSCONT3 = this.oParentObject.w_MDSCONT3
    this.oParentObject.w_MDCONTRA = ARRCALC(9)
    this.w_IVACON = ARRCALC(12)
    this.w_APPO = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MDUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_LISIVA, "N")+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI))
    if (this.oParentObject.w_LISIVA<>"L" AND ARRCALC(7)=2) or (this.w_IVACON<>"L" AND ARRCALC(7)=1) AND this.oParentObject.w_PERIVA<>0 AND this.oParentObject.w_LIPREZZO<>0 AND this.w_OK
      * --- Calcola il Prezzo al Lordo
      this.oParentObject.w_LIPREZZO = this.oParentObject.w_LIPREZZO + cp_ROUND(((this.oParentObject.w_LIPREZZO * this.oParentObject.w_PERIVA) / 100), g_PERPUL)
    endif
    if this.oParentObject.w_MDTIPRIG $ "RFMA" AND g_PERAGE="S" AND this.oParentObject.w_MDTIPPRO$"DC-CC"
      if ARRCALC(6)<>0
        this.oParentObject.w_MDPERPRO = ARRCALC(6)
        this.oParentObject.w_MDTIPPRO = "CC"
        SELECT (this.w_PADRE.cTrsName)
        REPLACE t_MDTIPPRO WITH this.oParentObject.w_MDTIPPRO
      endif
    endif
    if this.oParentObject.w_MDTIPRIG $ "RFMA" AND g_PERAGE="S" AND this.oParentObject.w_MDTIPPR2$"DC-CC"
      if ARRCALC(15)<>0
        this.oParentObject.w_MDPROCAP = ARRCALC(15)
        this.oParentObject.w_MDTIPPR2 = "CC"
        SELECT (this.w_PADRE.cTrsName)
        REPLACE t_MDTIPPR2 WITH this.oParentObject.w_MDTIPPR2
      endif
    endif
    * --- Controllo se sono stati modificati prezzo o sconti
    this.w_RICSCO = IIF(this.oParentObject.w_NUMSCO>0, this.oParentObject.w_MDSCONT1, 0)<>this.w_OSCONT1 And this.w_OSCONT1<>0
    this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>1, this.oParentObject.w_MDSCONT2, 0)<>this.w_OSCONT2 And this.w_OSCONT2<>0)
    this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>2, this.oParentObject.w_MDSCONT3, 0)<>this.w_OSCONT3 And this.w_OSCONT3<>0)
    this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>3, this.oParentObject.w_MDSCONT4, 0)<>this.w_OSCONT4 And this.w_OSCONT4<>0)
    if EMPTY(this.oParentObject.w_CLUNIMIS)
      this.w_TESTPRZ = CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO, this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, 0)
    else
      this.w_TESTPRZ = this.oParentObject.w_LIPREZZO
    endif
    this.w_RICPRE = this.w_TESTPRZ <>this.oParentObject.w_MDPREZZO
    * --- Al cambio della quantit� se modificato il prezzo e il Listino/Contratto sono gestiti a sconti 
    *     oppure gli sonti derivano dalla tabella Sconti Maggiorazioni chiedo se si vogliono modificare
    if (this.pExec="A" OR this.pExec="U" Or this.pExec="M") AND ((this.w_OPREZZO<>-9876 AND this.w_RICPRE) Or this.w_RICSCO )
      * --- Nella domanda di ricalcolo prezzi evidenzio anche il nuovo prezzo e sconti e l'origine di questi
      this.w_ORIGINE = IIF(Arrcalc(7)=1,ah_Msgformat("Calcolato da contratto"),IIF(Arrcalc(7)=2,ah_Msgformat("Calcolato da listino"),IIF(Arrcalc(7)=3,ah_Msgformat("Calcolato da U.C.A., U.P.V"),ah_Msgformat("Nessuna origine") )))
      this.w_oMESS.AddMsgPartNL("Ricalcolo il prezzo e gli sconti in base al listino/contratto/tabella sconti maggiorazioni?")     
      this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1")
      this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5))+" "+this.w_ORIGINE)     
      if this.oParentObject.w_NUMSCO>0
        this.w_ORIGINE = IIF(Arrcalc(8)=1,ah_Msgformat("Calcolati da contratto"),IIF(Arrcalc(8)=3,ah_Msgformat("Calcolati da listino"),IIF(Arrcalc(8)=4,"Calcolati da U.C.A., U.P.V",IIF(Arrcalc(8)=2,ah_Msgformat("Calcolati da tab. sconti/magg."),ah_Msgformat("Nessuna origine") ))))
        this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1")
        this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MDSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_MDSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_MDSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_MDSCONT4,6,2)),"")+" "+this.w_ORIGINE)     
      endif
      if NOT this.w_oMESS.ah_YesNo()
        * --- Ripristina
        this.oParentObject.w_LIPREZZO = this.w_OLIPREZ
        this.oParentObject.w_MDCONTRA = this.w_OCONTRA
        this.oParentObject.w_MDSCONT1 = this.w_OSCONT1
        this.oParentObject.w_MDSCONT2 = this.w_OSCONT2
        this.oParentObject.w_MDSCONT3 = this.w_OSCONT3
        this.oParentObject.w_MDSCONT4 = this.w_OSCONT4
        * --- Impedisco il ricalcolo degli sconti
        this.oParentObject.o_MDSCONT1 = this.oParentObject.w_MDSCONT1
        this.oParentObject.o_MDSCONT2 = this.oParentObject.w_MDSCONT2
        this.oParentObject.o_MDSCONT3 = this.oParentObject.w_MDSCONT3
        i_retcode = 'stop'
        return
      else
        if (this.pExec="A" OR this.pExec="U" Or this.pExec="M") AND this.oParentObject.o_LIPREZZO=this.oParentObject.w_LIPREZZO
          * --- Forza il Ricalcolo se variate QTA
          this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO + 1
        endif
      endif
    endif
    * --- w_OFLSCO alla fine deve valere solo ' ' o 'S'
    this.oParentObject.w_OFLSCO = IIF(this.oParentObject.w_OFLSCO="C", " ", this.oParentObject.w_OFLSCO)
    this.w_APPO = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MDUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_LISIVA, "N")+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI))
    * --- Se modifico quantit� (pExec='A') se non mi arriva prezzo da listini/contratti
    *     allora non ricalcolo il prezzo in base a liprezzo. Questo per evitare
    *     l'azzeramento del prezzo se scritto a mano, a seguito del cambio di 
    *     quantit�.
    if Not ( this.pExec="A" And (Not(this.w_OK)) and (this.oParentObject.w_PRZVAC<>"S" AND this.oParentObject.w_LIPREZZO=0) )
      if EMPTY(this.oParentObject.w_CLUNIMIS)
        this.oParentObject.w_MDPREZZO = CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO, this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, 0)
      else
        this.oParentObject.w_MDPREZZO = this.oParentObject.w_LIPREZZO
      endif
    endif
    if this.oParentObject.w_MDFLRESO="S" AND this.oParentObject.w_MDPREZZO>0
      * --- Se Reso setta il Prezzo Negativo e lo nettifica azzerando gli sconti
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
    this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE - this.oParentObject.w_VALRIG
    this.oParentObject.w_MDTOTVEN = this.oParentObject.w_MDTOTVEN - this.oParentObject.w_RIGNET
    this.oParentObject.w_VALRIG = IIF(g_FLESSC="S",CAVALRIG(this.oParentObject.w_MDPREZZO,this.oParentObject.w_MDQTAMOV, 0, 0, 0, 0,this.oParentObject.w_DECTOT), CAVALRIG(this.oParentObject.w_MDPREZZO,this.oParentObject.w_MDQTAMOV, this.oParentObject.w_MDSCONT1,this.oParentObject.w_MDSCONT2,this.oParentObject.w_MDSCONT3,this.oParentObject.w_MDSCONT4,this.oParentObject.w_DECTOT) )
    this.oParentObject.w_RIGNET = IIF(this.oParentObject.w_MDFLOMAG="X", this.oParentObject.w_VALRIG,IIF(this.oParentObject.w_MDFLOMAG="I", this.oParentObject.w_VALRIG - CALNET(this.oParentObject.w_VALRIG,this.oParentObject.w_PERIVA,this.oParentObject.w_DECTOT,Space(3),0),0))
    this.oParentObject.w_MDTOTVEN = this.oParentObject.w_MDTOTVEN + this.oParentObject.w_RIGNET
    this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE + this.oParentObject.w_VALRIG
    * --- Notifica che la Riga e' Stata Variata
    SELECT (this.w_PADRE.cTrsName)
    if EMPTY(i_SRV) AND NOT DELETED()
      REPLACE i_SRV WITH "U"
    endif
    * --- Num. Max. Sconti Consentiti
    this.oParentObject.w_MDSCONT1 = IIF(this.oParentObject.w_NUMSCO>0, this.oParentObject.w_MDSCONT1, 0)
    this.oParentObject.w_MDSCONT2 = IIF(this.oParentObject.w_NUMSCO>1, this.oParentObject.w_MDSCONT2, 0)
    this.oParentObject.w_MDSCONT3 = IIF(this.oParentObject.w_NUMSCO>2, this.oParentObject.w_MDSCONT3, 0)
    this.oParentObject.w_MDSCONT4 = IIF(this.oParentObject.w_NUMSCO>3, this.oParentObject.w_MDSCONT4, 0)
    * --- Impedisco il ricalcolo degli sconti
    this.oParentObject.o_MDSCONT1 = this.oParentObject.w_MDSCONT1
    this.oParentObject.o_MDSCONT2 = this.oParentObject.w_MDSCONT2
    this.oParentObject.o_MDSCONT3 = this.oParentObject.w_MDSCONT3
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazione Variabili Locali e Caller
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli al Cambio Quantita' sulle Righe (da GSPS_MVD)
    if this.oParentObject.w_MDFLARIF<>" " AND this.w_PADRE.cFunction<>"Query"
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVQTAMOV,MVQTAEVA"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MDROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVQTAMOV,MVQTAEVA;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_MDSERRIF;
              and CPROWNUM = this.oParentObject.w_MDROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_QTAORI = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
        this.w_QTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Qta Importabile sul Documento escludendo quella gia' presente sulla riga
      SELECT (this.w_PADRE.cTrsName)
      this.w_SRV = i_SRV
      this.w_QTARIM = (this.w_QTAORI-this.w_QTAEVA) + IIF(this.w_SRV="A", 0, this.oParentObject.w_MDQTAIMP)
      this.w_ORIF = this.oParentObject.w_MDFLERIF
      this.oParentObject.w_MDQTAIMP = MIN(this.oParentObject.w_MDQTAMOV, this.w_QTARIM)
      this.oParentObject.w_MDQTAIM1 = IIF(this.oParentObject.w_MDTIPRIG="F", 1, CALQTA(this.oParentObject.w_MDQTAIMP,this.oParentObject.w_MDUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_UNISEP, this.oParentObject.w_NOFRAZ, this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
      if this.oParentObject.w_MDQTAIMP<this.w_QTARIM
        if this.w_PADRE.cFunction="Load"
          this.oParentObject.w_MDFLERIF = IIF(ah_YesNo("Evado la quantit� rimanente sul documento di origine?")=.T.,"S"," ")
        else
          if this.oParentObject.w_OLDEVAS $ "SE"
            if ah_YesNo("Devo considerare da evadere la quantit� rimanente sul documento di origine?")
              this.oParentObject.w_MDFLERIF = " "
              * --- Forzata Riapertura in Variazione
              this.oParentObject.w_OLDEVAS = "R"
            endif
          else
            if ah_YesNo("Evado la quantit� rimanente sul documento di origine?")
              this.oParentObject.w_MDFLERIF = "S"
              * --- Forzata Chiusura in Variazione
              this.oParentObject.w_OLDEVAS = "E"
            else
              this.oParentObject.w_MDFLERIF = " "
              * --- Forzata Riapertura in Variazione
              this.oParentObject.w_OLDEVAS = "R"
            endif
          endif
        endif
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcolo prezzo al netto degli sconti se attivo flag reso
    if g_MATR="S" And this.oParentObject.o_MDFLRESO <> this.oParentObject.w_MDFLRESO
      * --- Controllo se sono gi� state inserite delle Matricole sulla Riga.
      *     In questo caso non posso modificare il Check Reso
      if Type("This.oParentObject.GSVE_MMT.cnt") = "O"
        this.w_OLDSCHED = IIF(TYPE("g_SCHEDULER")="C", g_SCHEDULER,"")
        if TYPE("g_SCHEDULER")="C"
          g_SCHEDULER="S"
        else
          Public g_SCHEDULER 
 g_SCHEDULER="S"
        endif
        this.w_PADRE.GSVE_MMT.LINKPCCLICK()     
        g_SCHEDULER=this.w_OLDSCHED
        this.w_PADRE.GSVE_MMT.LINKPCCLICK()     
        * --- solo se ho gi� instanziato le matricole
        this.w_PADRE.GSVE_MMT.cnt.Exec_Select("_Tmp_MATR","Count(*) As Conta ","Not Deleted() And Not Empty(Nvl(t_MTCODMAT,' ')) And Nvl(t_MTKEYSAL,' ') = "+cp_ToStrODBC(this.oParentObject.w_MDCODART)+" And Nvl(t_MTCODLOT,' ') ="+ cp_ToStrODBC(this.oParentObject.w_MDCODLOT))     
        if NVL(_Tmp_MATR.Conta,0)>0
          ah_ErrorMsg("Sulla riga sono presenti matricole. Impossibile modificare check reso",,"")
          this.oParentObject.w_MDFLRESO = IIF(this.oParentObject.w_MDFLRESO="S"," ","S")
          USE IN SELECT("_Tmp_MATR")
          i_retcode = 'stop'
          return
        endif
        USE IN SELECT("_Tmp_MATR")
      endif
    endif
    this.oParentObject.w_MDPREZZO = ABS(this.oParentObject.w_MDPREZZO)
    if this.oParentObject.w_MDFLRESO="S"
      * --- Se Reso setta il Prezzo Negativo
      if this.oParentObject.w_MDSCONT1<>0 Or this.oParentObject.w_MDSCONT2 <>0 Or this.oParentObject.w_MDSCONT3<>0 Or this.oParentObject.w_MDSCONT4<>0
        ah_Msg("Ricalcolato il prezzo al netto degli sconti",.T.)
        this.oParentObject.w_MDPREZZO = -this.oParentObject.w_MDPREZZO * (1+this.oParentObject.w_MDSCONT1/100)*(1+this.oParentObject.w_MDSCONT2/100)*(1+this.oParentObject.w_MDSCONT3/100)*(1+this.oParentObject.w_MDSCONT4/100) 
        this.oParentObject.w_MDSCONT1 = 0
        this.oParentObject.w_MDSCONT2 = 0
        this.oParentObject.w_MDSCONT3 = 0
        this.oParentObject.w_MDSCONT4 = 0
        * --- Impedisco il ricalcolo degli sconti
        this.oParentObject.o_MDSCONT1 = this.oParentObject.w_MDSCONT1
        this.oParentObject.o_MDSCONT2 = this.oParentObject.w_MDSCONT2
        this.oParentObject.o_MDSCONT3 = this.oParentObject.w_MDSCONT3
      else
        this.oParentObject.w_MDPREZZO = -this.oParentObject.w_MDPREZZO
      endif
    endif
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='REP_ARTI'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='KEY_ARTI'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
