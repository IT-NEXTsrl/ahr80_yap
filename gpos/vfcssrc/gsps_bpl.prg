* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bpl                                                        *
*              Avvia la sincronizzazione archivi                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_204]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-06                                                      *
* Last revis.: 2002-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bpl",oParentObject)
return(i_retval)

define class tgsps_bpl as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_PATIPINS = space(1)
  * --- WorkFile variables
  PAR_VDET_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Sincronizzazione Archivi Remota (da GSPS_KPL)
    this.w_CODAZI = i_CODAZI
    * --- Read from PAR_VDET
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_VDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PATIPINS"+;
        " from "+i_cTable+" PAR_VDET where ";
            +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and PABUSUNI = "+cp_ToStrODBC(this.oParentObject.w_CODBUN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PATIPINS;
        from (i_cTable) where;
            PACODAZI = this.w_CODAZI;
            and PABUSUNI = this.oParentObject.w_CODBUN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PATIPINS = NVL(cp_ToDate(_read_.PATIPINS),cp_NullValue(_read_.PATIPINS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se configurata installazione Remota, aggiorna 
    *     le tabelle POS delle varie connessioni ODBC
    do GSPS_BSR with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Opzione Trasferimento Venduto (Solo Remoto)
    if this.oParentObject.w_VENDUTO="S"
      do GSPS_BTV with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    ah_ErrorMsg("Sincronizzazione remota archivi terminata",,"")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_VDET'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
