* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bsv                                                        *
*              Generazione documenti di vendita                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_361]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-07                                                      *
* Last revis.: 2002-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bsv",oParentObject)
return(i_retval)

define class tgsps_bsv as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_NEGFIL = space(3)
  w_SERDOC = space(10)
  w_OK = .f.
  w_ERRORI = .f.
  w_NUREG = 0
  w_OKREG = 0
  w_NUDOC = 0
  w_APPO = space(10)
  w_APPO1 = space(10)
  w_TIPOPE = space(1)
  w_CAURES = space(5)
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documenti da Vendita Negozio (da GSPS_MVD)
    * --- Lettura vendita in COR_RISP e CORDRISP e Scrittura movimenti in DOC_MAST e DOC_DETT
    * --- Se Fattura Fiscale il documento puo' essere eliminato e rigenerato successivamente
    this.w_TIPOPE = IIF(this.oParentObject.w_MDTIPCHI="FF", "F", "I")
    * --- Lettura parametri 
    this.w_CODAZI = i_CODAZI
    this.w_OK = .T.
    this.w_ERRORI = .F.
    this.w_NUREG = 0
    this.w_OKREG = 0
    this.w_NUDOC = 0
    * --- Causale Resi
    this.w_CAURES = this.oParentObject.w_CAURES
    if EMPTY(this.w_CAURES)
      ah_ErrorMsg("Causale per gestione resi non definita nei parametri P.O.S.%0Impossibile continuare","!","")
      i_retcode = 'stop'
      return
    endif
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Leggo i record di COR_RISP e CORDRISP da trasferire
    this.w_APPO = "Non esistono documenti da generare"
    * --- Lanciato dalla Conferma Vendita Negozio (Filtro sulla singola Vendita)
    this.w_SERDOC = this.oParentObject.w_MDSERIAL
    this.w_NEGFIL = this.oParentObject.w_MDCODNEG
    VQ_EXEC("..\GPOS\EXE\QUERY\GSPS_QGI.VQR",this,"GENEORDI")
    if USED("GENEORDI")
      * --- Generazione Documenti
      SELECT GENEORDI
      if RECCOUNT("GENEORDI") > 0
        do GSPS_BGD with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_APPO = "Operazione completata%0N. %1 registrazioni generate%0su %2 registrazioni da generare%0%0Totale %3 documenti generati"
        ah_ErrorMsg(this.w_APPO,"!","", ALLTRIM(STR(this.w_OKREG)), ALLTRIM(STR(this.w_NUREG)), ALLTRIM(STR(this.w_NUDOC)) )
      endif
    endif
    if this.w_ERRORI
      this.w_oERRORLOG.PrintLog(this,"Errori riscontrati")     
    endif
    if USED("GENEORDI")
      Select GENEORDI
      GO TOP
      USE
    endif
    if used( "__tmp__" )
      select __tmp__
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
