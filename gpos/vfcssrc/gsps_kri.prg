* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_kri                                                        *
*              Situazione rischio cliente                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_153]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-08                                                      *
* Last revis.: 2009-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_kri",oParentObject))

* --- Class definition
define class tgsps_kri as StdForm
  Top    = 135
  Left   = 18

  * --- Standard Properties
  Width  = 512
  Height = 184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-03-10"
  HelpContextID=152479639
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsps_kri"
  cComment = "Situazione rischio cliente"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MDCODCLI = space(15)
  w_DESCLI = space(40)
  w_VALFID = 0
  w_FIDUTI = 0
  w_FIDRES1 = 0
  w_FIDRES2 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_kriPag1","gsps_kri",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MDCODCLI=space(15)
      .w_DESCLI=space(40)
      .w_VALFID=0
      .w_FIDUTI=0
      .w_FIDRES1=0
      .w_FIDRES2=0
      .w_MDCODCLI=oParentObject.w_MDCODCLI
      .w_DESCLI=oParentObject.w_DESCLI
      .w_VALFID=oParentObject.w_VALFID
      .w_FIDUTI=oParentObject.w_FIDUTI
          .DoRTCalc(1,4,.f.)
        .w_FIDRES1 = .w_VALFID - .w_FIDUTI
        .w_FIDRES2 = .w_VALFID - .w_FIDUTI
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MDCODCLI=.w_MDCODCLI
      .oParentObject.w_DESCLI=.w_DESCLI
      .oParentObject.w_VALFID=.w_VALFID
      .oParentObject.w_FIDUTI=.w_FIDUTI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_FIDRES1 = .w_VALFID - .w_FIDUTI
            .w_FIDRES2 = .w_VALFID - .w_FIDUTI
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFIDRES1_1_10.visible=!this.oPgFrm.Page1.oPag.oFIDRES1_1_10.mHide()
    this.oPgFrm.Page1.oPag.oFIDRES2_1_11.visible=!this.oPgFrm.Page1.oPag.oFIDRES2_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMDCODCLI_1_1.value==this.w_MDCODCLI)
      this.oPgFrm.Page1.oPag.oMDCODCLI_1_1.value=this.w_MDCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_2.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_2.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oVALFID_1_3.value==this.w_VALFID)
      this.oPgFrm.Page1.oPag.oVALFID_1_3.value=this.w_VALFID
    endif
    if not(this.oPgFrm.Page1.oPag.oFIDUTI_1_7.value==this.w_FIDUTI)
      this.oPgFrm.Page1.oPag.oFIDUTI_1_7.value=this.w_FIDUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oFIDRES1_1_10.value==this.w_FIDRES1)
      this.oPgFrm.Page1.oPag.oFIDRES1_1_10.value=this.w_FIDRES1
    endif
    if not(this.oPgFrm.Page1.oPag.oFIDRES2_1_11.value==this.w_FIDRES2)
      this.oPgFrm.Page1.oPag.oFIDRES2_1_11.value=this.w_FIDRES2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_kriPag1 as StdContainer
  Width  = 508
  height = 184
  stdWidth  = 508
  stdheight = 184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMDCODCLI_1_1 as StdField with uid="MGRROWXJYO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MDCODCLI", cQueryName = "MDCODCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 11149583,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=70, Top=13, InputMask=replicate('X',15)

  add object oDESCLI_1_2 as StdField with uid="EULVYAQRAA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 119480630,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=205, Top=13, InputMask=replicate('X',40)

  add object oVALFID_1_3 as StdField with uid="DCJCZFEZUB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VALFID", cQueryName = "VALFID",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 32616022,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=359, Top=46, cSayPict="v_PV(18)", cGetPict="v_PV(18)"


  add object oBtn_1_6 as StdButton with uid="KYQESPJULE",left=452, top=134, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 152508390;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFIDUTI_1_7 as StdField with uid="DECNNIRPAC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FIDUTI", cQueryName = "FIDUTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 128988502,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=359, Top=73, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  add object oFIDRES1_1_10 as StdField with uid="SKKYXZRFZS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FIDRES1", cQueryName = "FIDRES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente ancora disponibile",;
    HelpContextID = 12399958,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=359, Top=100, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  func oFIDRES1_1_10.mHide()
    with this.Parent.oContained
      return (.w_FIDRES1<0)
    endwith
  endfunc

  add object oFIDRES2_1_11 as StdField with uid="OXLOYMKTTW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FIDRES2", cQueryName = "FIDRES2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente ancora disponibile",;
    HelpContextID = 256035498,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=359, Top=100, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  func oFIDRES2_1_11.mHide()
    with this.Parent.oContained
      return (.w_FIDRES2>=0)
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="MSDPIFIXFZ",Visible=.t., Left=7, Top=13,;
    Alignment=1, Width=61, Height=18,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="SIHESKLJJG",Visible=.t., Left=225, Top=46,;
    Alignment=1, Width=131, Height=18,;
    Caption="Fido acceditato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="GDCKJSEBNX",Visible=.t., Left=225, Top=73,;
    Alignment=1, Width=131, Height=18,;
    Caption="Fido utilizzato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="TMKZGCYRER",Visible=.t., Left=225, Top=100,;
    Alignment=1, Width=131, Height=18,;
    Caption="Fido disponibile:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_kri','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
