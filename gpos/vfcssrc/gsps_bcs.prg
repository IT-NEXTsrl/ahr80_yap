* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bcs                                                        *
*              Ricalcolo variabili vendita negozio                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_98]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-23                                                      *
* Last revis.: 2008-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bcs",oParentObject)
return(i_retval)

define class tgsps_bcs as StdBatch
  * --- Local variables
  w_TIPOPE = space(1)
  w_MSG = space(200)
  w_TESTIMP = 0
  w_TESTRES = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per ricalcolare le variabili della pagine Dati Chiusura nella Vendita Negozio 
    *     Lanciato all'evento ActivatePage 2 (nel momento in cui si va nella seconda pagina)
    * --- PROMOK : se sono gi� state calcolate le promozioni � uguale a .F.
    *     Nel Batch GSPS_BPZ viene valorizzata a .F.
    *     Nell'area manuale CALCULATE di GSPS_MVD viene messa a .T.
    this.w_TIPOPE = this.oParentObject.cFunction
    if this.w_TIPOPE="Query"
      * --- In interrogazione non devo fare niente
      i_retcode = 'stop'
      return
    endif
    * --- Esegue Calcoli associati alle Promozioni
    if this.w_TIPOPE<>"Query"
      if !this.oParentObject.w_PROMOK
        this.oParentObject.w_MDSTOPRO = 0
        this.oParentObject.w_MDPUNFID = 0
        * --- Non sono ancora state calcolate le Promozioni o � stato variato 
        *     qualcosa nella vendita
        this.oParentObject.NotifyEvent("Promozioni")
      endif
    endif
    this.oParentObject.w_MDIMPPRE = IIF( this.w_TIPOPE="Load", 0 , this.oParentObject.w_MDIMPPRE)
    * --- Sconti Globali
    this.oParentObject.w_MDSCONTI = cp_Round( IIF(this.oParentObject.w_MDFLFOSC<>"S", - (this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSTOPRO) * cp_Round((1 - (1+this.oParentObject.w_MDSCOCL1/100)*(1+this.oParentObject.w_MDSCOCL2/100)*(1+this.oParentObject.w_MDSCOPAG/100)), 10) ,this.oParentObject.w_MDSCONTI) , this.oParentObject.w_DECTOT )
    * --- In modifica devo ricalcolare il prepagato in base al totale vendita prima del totale documento
    *     solo se � attivo il flag No Fattura Fidelity
    if this.w_TIPOPE="Edit" And this.oParentObject.w_FLFAFI = "S"
      if NOT EMPTY(this.oParentObject.w_MDCODFID)
        * --- In modifica controllo se ho aumentato il totale documento tanto da superare il residuo della fidelity
        *     In questo caso aggiungo all'import prepagato fino al massimo del residuo
        this.w_TESTIMP = (this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSCONTI + this.oParentObject.w_MDSTOPRO) - (this.oParentObject.w_MDACCPRE+this.oParentObject.w_MDPAGASS+this.oParentObject.w_MDPAGCAR+this.oParentObject.w_MDPAGFIN+this.oParentObject.w_MDPAGCLI+this.oParentObject.w_MDIMPABB+this.oParentObject.w_MDPAGCON)
        this.w_TESTRES = this.w_TESTIMP - this.oParentObject.w_MDIMPPRE
        if this.w_TESTRES >this.oParentObject.w_FIMPRES
          this.oParentObject.w_MDIMPPRE = this.oParentObject.w_MDIMPPRE + this.oParentObject.w_FIMPRES
        else
          * --- Se l'incremento (o decremento) � minore del residuo, ricalcolo il prepagato fino al nuovo totale documento
          this.oParentObject.w_MDIMPPRE = (this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSCONTI + this.oParentObject.w_MDSTOPRO) - (this.oParentObject.w_MDACCPRE+this.oParentObject.w_MDPAGASS+this.oParentObject.w_MDPAGCAR+this.oParentObject.w_MDPAGFIN+this.oParentObject.w_MDPAGCLI+this.oParentObject.w_MDIMPABB+this.oParentObject.w_MDPAGCON)
        endif
      endif
    endif
    * --- Totale Documento
    this.oParentObject.w_MDTOTDOC = this.oParentObject.w_MDTOTVEN + (this.oParentObject.w_MDSCONTI+ this.oParentObject.w_MDSTOPRO - IIF(this.oParentObject.w_FLFAFI ="S" And Not Empty(this.oParentObject.w_SERFID),this.oParentObject.w_MDIMPPRE,0) )
    * --- Esegue Calcoli Prepagato/Fidelity Card
    if this.w_TIPOPE="Load"
      if NOT EMPTY(this.oParentObject.w_MDCODFID)
        this.oParentObject.w_MDIMPPRE = IIF(this.oParentObject.w_FIMPRES>0,this.oParentObject.w_FIMPRES,0)
        * --- Prepagato al massimo uguale al documento
        this.oParentObject.w_MDIMPPRE = MIN(this.oParentObject.w_FIMPRES, ( this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSTOPRO + this.oParentObject.w_MDSCONTI ) )
      endif
    endif
    if this.w_TIPOPE="Load"
      this.oParentObject.w_MDPAGCON = cp_Round(this.oParentObject.w_MDTOTDOC - (this.oParentObject.w_MDIMPPRE+this.oParentObject.w_MDACCPRE), this.oParentObject.w_DECTOT)
      this.oParentObject.w_MDPAGASS = 0
      this.oParentObject.w_MDPAGCAR = 0
      this.oParentObject.w_MDPAGFIN = 0
      this.oParentObject.w_MDPAGCLI = 0
      this.oParentObject.w_MDIMPABB = 0
    else
      if this.w_TIPOPE="Edit"
        * --- In modifica ricalcolo i contanti in base al nuovo totale documento
        *     Metto la differenza fra il valore originale e attuale nei Contanti
        if this.oParentObject.w_FLFAFI="S"
          * --- Nel caso ho attivo il No Fattura Fidelity nel contante metto direttamente il Totale documento
          this.oParentObject.w_MDPAGCON = cp_Round(this.oParentObject.w_MDTOTDOC - (this.oParentObject.w_MDACCPRE+this.oParentObject.w_MDPAGASS+this.oParentObject.w_MDPAGCAR+this.oParentObject.w_MDPAGFIN+this.oParentObject.w_MDPAGCLI+this.oParentObject.w_MDIMPABB), this.oParentObject.w_DECTOT)
        else
          this.oParentObject.w_MDPAGCON = cp_Round(this.oParentObject.w_MDTOTDOC - (this.oParentObject.w_MDIMPPRE+this.oParentObject.w_MDACCPRE+this.oParentObject.w_MDPAGASS+this.oParentObject.w_MDPAGCAR+this.oParentObject.w_MDPAGFIN+this.oParentObject.w_MDPAGCLI+this.oParentObject.w_MDIMPABB), this.oParentObject.w_DECTOT)
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
