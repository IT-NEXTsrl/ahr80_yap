* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_spr                                                        *
*              Stampa promozioni                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_22]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-17                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_spr",oParentObject))

* --- Class definition
define class tgsps_spr as StdForm
  Top    = 105
  Left   = 148

  * --- Standard Properties
  Width  = 537
  Height = 241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=141121641
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  PRM_MAST_IDX = 0
  cPrg = "gsps_spr"
  cComment = "Stampa promozioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PROINI = space(15)
  w_DESINI = space(40)
  w_PROFIN = space(15)
  w_DESFIN = space(40)
  w_FLESCU = space(1)
  w_DATSTA = ctod('  /  /  ')
  w_GIOLUN = space(1)
  w_GIOMAR = space(1)
  w_GIOMER = space(1)
  w_GIOGIO = space(1)
  w_GIOVEN = space(1)
  w_GIOSAB = space(1)
  w_GIODOM = space(1)
  w_ONUME = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_sprPag1","gsps_spr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPROINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PRM_MAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PROINI=space(15)
      .w_DESINI=space(40)
      .w_PROFIN=space(15)
      .w_DESFIN=space(40)
      .w_FLESCU=space(1)
      .w_DATSTA=ctod("  /  /  ")
      .w_GIOLUN=space(1)
      .w_GIOMAR=space(1)
      .w_GIOMER=space(1)
      .w_GIOGIO=space(1)
      .w_GIOVEN=space(1)
      .w_GIOSAB=space(1)
      .w_GIODOM=space(1)
      .w_ONUME=0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PROINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_PROFIN))
          .link_1_5('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_FLESCU = 'C'
        .w_DATSTA = i_DATSYS
        .w_GIOLUN = '2'
        .w_GIOMAR = '3'
        .w_GIOMER = '4'
        .w_GIOGIO = '5'
        .w_GIOVEN = '6'
        .w_GIOSAB = '7'
        .w_GIODOM = '1'
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PROINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
    i_lTable = "PRM_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2], .t., this.PRM_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRM_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRCODICE like "+cp_ToStrODBC(trim(this.w_PROINI)+"%");

          i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRCODICE',trim(this.w_PROINI))
          select PRCODICE,PRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROINI)==trim(_Link_.PRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROINI) and !this.bDontReportError
            deferred_cp_zoom('PRM_MAST','*','PRCODICE',cp_AbsName(oSource.parent,'oPROINI_1_3'),i_cWhere,'',"Codice promozione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODICE',oSource.xKey(1))
            select PRCODICE,PRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODICE="+cp_ToStrODBC(this.w_PROINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODICE',this.w_PROINI)
            select PRCODICE,PRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROINI = NVL(_Link_.PRCODICE,space(15))
      this.w_DESINI = NVL(_Link_.PRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PROINI = space(15)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PROFIN)) OR  (.w_PROINI<=.w_PROFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PROINI = space(15)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PRCODICE,1)
      cp_ShowWarn(i_cKey,this.PRM_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROFIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
    i_lTable = "PRM_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2], .t., this.PRM_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRM_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRCODICE like "+cp_ToStrODBC(trim(this.w_PROFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRCODICE',trim(this.w_PROFIN))
          select PRCODICE,PRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROFIN)==trim(_Link_.PRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROFIN) and !this.bDontReportError
            deferred_cp_zoom('PRM_MAST','*','PRCODICE',cp_AbsName(oSource.parent,'oPROFIN_1_5'),i_cWhere,'',"Codice promozione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODICE',oSource.xKey(1))
            select PRCODICE,PRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODICE="+cp_ToStrODBC(this.w_PROFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODICE',this.w_PROFIN)
            select PRCODICE,PRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROFIN = NVL(_Link_.PRCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.PRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PROFIN = space(15)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PROFIN>=.w_PROINI) or (empty(.w_PROINI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PROFIN = space(15)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PRCODICE,1)
      cp_ShowWarn(i_cKey,this.PRM_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPROINI_1_3.value==this.w_PROINI)
      this.oPgFrm.Page1.oPag.oPROINI_1_3.value=this.w_PROINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_4.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_4.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPROFIN_1_5.value==this.w_PROFIN)
      this.oPgFrm.Page1.oPag.oPROFIN_1_5.value=this.w_PROFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_6.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_6.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLESCU_1_7.RadioValue()==this.w_FLESCU)
      this.oPgFrm.Page1.oPag.oFLESCU_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_8.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_8.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOLUN_1_9.RadioValue()==this.w_GIOLUN)
      this.oPgFrm.Page1.oPag.oGIOLUN_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOMAR_1_10.RadioValue()==this.w_GIOMAR)
      this.oPgFrm.Page1.oPag.oGIOMAR_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOMER_1_11.RadioValue()==this.w_GIOMER)
      this.oPgFrm.Page1.oPag.oGIOMER_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOGIO_1_12.RadioValue()==this.w_GIOGIO)
      this.oPgFrm.Page1.oPag.oGIOGIO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOVEN_1_13.RadioValue()==this.w_GIOVEN)
      this.oPgFrm.Page1.oPag.oGIOVEN_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOSAB_1_14.RadioValue()==this.w_GIOSAB)
      this.oPgFrm.Page1.oPag.oGIOSAB_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIODOM_1_15.RadioValue()==this.w_GIODOM)
      this.oPgFrm.Page1.oPag.oGIODOM_1_15.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_PROFIN)) OR  (.w_PROINI<=.w_PROFIN)))  and not(empty(.w_PROINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPROINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_PROFIN>=.w_PROINI) or (empty(.w_PROINI))))  and not(empty(.w_PROFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPROFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_sprPag1 as StdContainer
  Width  = 533
  height = 241
  stdWidth  = 533
  stdheight = 241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPROINI_1_3 as StdField with uid="XJFBPISASI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PROINI", cQueryName = "PROINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice promozione di inizio selezione",;
    HelpContextID = 171643146,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=104, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PRM_MAST", oKey_1_1="PRCODICE", oKey_1_2="this.w_PROINI"

  func oPROINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRM_MAST','*','PRCODICE',cp_AbsName(this.parent,'oPROINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice promozione",'',this.parent.oContained
  endproc

  add object oDESINI_1_4 as StdField with uid="PBJKGWORUU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 171630282,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=232, Top=14, InputMask=replicate('X',40)

  add object oPROFIN_1_5 as StdField with uid="BKKJYBBXHY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PROFIN", cQueryName = "PROFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice pomozione di fine selezione",;
    HelpContextID = 93196554,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=104, Top=42, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PRM_MAST", oKey_1_1="PRCODICE", oKey_1_2="this.w_PROFIN"

  func oPROFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROFIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROFIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRM_MAST','*','PRCODICE',cp_AbsName(this.parent,'oPROFIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice promozione",'',this.parent.oContained
  endproc

  add object oDESFIN_1_6 as StdField with uid="RTNZBOKPQG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 93183690,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=232, Top=42, InputMask=replicate('X',40)


  add object oFLESCU_1_7 as StdCombo with uid="UUMSNRPZCS",rtseq=5,rtrep=.f.,left=104,top=74,width=123,height=21;
    , ToolTipText = "Tipologia: esclusiva, comulativa";
    , HelpContextID = 249673642;
    , cFormVar="w_FLESCU",RowSource=""+"Esclusiva,"+"Cumulativa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLESCU_1_7.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oFLESCU_1_7.GetRadio()
    this.Parent.oContained.w_FLESCU = this.RadioValue()
    return .t.
  endfunc

  func oFLESCU_1_7.SetRadio()
    this.Parent.oContained.w_FLESCU=trim(this.Parent.oContained.w_FLESCU)
    this.value = ;
      iif(this.Parent.oContained.w_FLESCU=='E',1,;
      iif(this.Parent.oContained.w_FLESCU=='C',2,;
      0))
  endfunc

  add object oDATSTA_1_8 as StdField with uid="CCWDHBOSYP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data stampa: utilizzata come filtro di validit� delle righe",;
    HelpContextID = 30462666,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=449, Top=72

  add object oGIOLUN_1_9 as StdCheck with uid="YLFBHHZYJJ",rtseq=7,rtrep=.f.,left=123, top=105, caption="Luned�",;
    ToolTipText = "Valida luned�",;
    HelpContextID = 80222874,;
    cFormVar="w_GIOLUN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOLUN_1_9.RadioValue()
    return(iif(this.value =1,'2',;
    '9'))
  endfunc
  func oGIOLUN_1_9.GetRadio()
    this.Parent.oContained.w_GIOLUN = this.RadioValue()
    return .t.
  endfunc

  func oGIOLUN_1_9.SetRadio()
    this.Parent.oContained.w_GIOLUN=trim(this.Parent.oContained.w_GIOLUN)
    this.value = ;
      iif(this.Parent.oContained.w_GIOLUN=='2',1,;
      0)
  endfunc

  add object oGIOMAR_1_10 as StdCheck with uid="BPOTZCVRPB",rtseq=8,rtrep=.f.,left=212, top=105, caption="Marted�",;
    ToolTipText = "Valida marted�",;
    HelpContextID = 34019994,;
    cFormVar="w_GIOMAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOMAR_1_10.RadioValue()
    return(iif(this.value =1,'3',;
    '9'))
  endfunc
  func oGIOMAR_1_10.GetRadio()
    this.Parent.oContained.w_GIOMAR = this.RadioValue()
    return .t.
  endfunc

  func oGIOMAR_1_10.SetRadio()
    this.Parent.oContained.w_GIOMAR=trim(this.Parent.oContained.w_GIOMAR)
    this.value = ;
      iif(this.Parent.oContained.w_GIOMAR=='3',1,;
      0)
  endfunc

  add object oGIOMER_1_11 as StdCheck with uid="PLMVJQUSYV",rtseq=9,rtrep=.f.,left=305, top=105, caption="Mercoled�",;
    ToolTipText = "Valida mercoled�",;
    HelpContextID = 29825690,;
    cFormVar="w_GIOMER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOMER_1_11.RadioValue()
    return(iif(this.value =1,'4',;
    '9'))
  endfunc
  func oGIOMER_1_11.GetRadio()
    this.Parent.oContained.w_GIOMER = this.RadioValue()
    return .t.
  endfunc

  func oGIOMER_1_11.SetRadio()
    this.Parent.oContained.w_GIOMER=trim(this.Parent.oContained.w_GIOMER)
    this.value = ;
      iif(this.Parent.oContained.w_GIOMER=='4',1,;
      0)
  endfunc

  add object oGIOGIO_1_12 as StdCheck with uid="VBNKFXJXZF",rtseq=10,rtrep=.f.,left=413, top=105, caption="Gioved�",;
    ToolTipText = "Valida gioved�",;
    HelpContextID = 76356250,;
    cFormVar="w_GIOGIO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOGIO_1_12.RadioValue()
    return(iif(this.value =1,'5',;
    '9'))
  endfunc
  func oGIOGIO_1_12.GetRadio()
    this.Parent.oContained.w_GIOGIO = this.RadioValue()
    return .t.
  endfunc

  func oGIOGIO_1_12.SetRadio()
    this.Parent.oContained.w_GIOGIO=trim(this.Parent.oContained.w_GIOGIO)
    this.value = ;
      iif(this.Parent.oContained.w_GIOGIO=='5',1,;
      0)
  endfunc

  add object oGIOVEN_1_13 as StdCheck with uid="EPAPSYXMPA",rtseq=11,rtrep=.f.,left=123, top=128, caption="Venerd�",;
    ToolTipText = "Valida venerd�",;
    HelpContextID = 96344730,;
    cFormVar="w_GIOVEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOVEN_1_13.RadioValue()
    return(iif(this.value =1,'6',;
    '9'))
  endfunc
  func oGIOVEN_1_13.GetRadio()
    this.Parent.oContained.w_GIOVEN = this.RadioValue()
    return .t.
  endfunc

  func oGIOVEN_1_13.SetRadio()
    this.Parent.oContained.w_GIOVEN=trim(this.Parent.oContained.w_GIOVEN)
    this.value = ;
      iif(this.Parent.oContained.w_GIOVEN=='6',1,;
      0)
  endfunc

  add object oGIOSAB_1_14 as StdCheck with uid="PEJMLNCTOC",rtseq=12,rtrep=.f.,left=212, top=128, caption="Sabato",;
    ToolTipText = "Valida sabato",;
    HelpContextID = 33626778,;
    cFormVar="w_GIOSAB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIOSAB_1_14.RadioValue()
    return(iif(this.value =1,'7',;
    '9'))
  endfunc
  func oGIOSAB_1_14.GetRadio()
    this.Parent.oContained.w_GIOSAB = this.RadioValue()
    return .t.
  endfunc

  func oGIOSAB_1_14.SetRadio()
    this.Parent.oContained.w_GIOSAB=trim(this.Parent.oContained.w_GIOSAB)
    this.value = ;
      iif(this.Parent.oContained.w_GIOSAB=='7',1,;
      0)
  endfunc

  add object oGIODOM_1_15 as StdCheck with uid="IDKNYMLIXI",rtseq=13,rtrep=.f.,left=305, top=128, caption="Domenica",;
    ToolTipText = "Valida domenica",;
    HelpContextID = 103815834,;
    cFormVar="w_GIODOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGIODOM_1_15.RadioValue()
    return(iif(this.value =1,'1',;
    '9'))
  endfunc
  func oGIODOM_1_15.GetRadio()
    this.Parent.oContained.w_GIODOM = this.RadioValue()
    return .t.
  endfunc

  func oGIODOM_1_15.SetRadio()
    this.Parent.oContained.w_GIODOM=trim(this.Parent.oContained.w_GIODOM)
    this.value = ;
      iif(this.Parent.oContained.w_GIODOM=='1',1,;
      0)
  endfunc


  add object oObj_1_16 as cp_outputCombo with uid="ZLERXIVPWT",left=104, top=162, width=420,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 36876006


  add object oBtn_1_17 as StdButton with uid="VCZMCHUPGB",left=421, top=189, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare la stampa";
    , HelpContextID = 667942;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="EFNLDLJGWV",left=478, top=189, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 133804218;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="ERCPYOQOEY",Visible=.t., Left=7, Top=15,;
    Alignment=1, Width=96, Height=18,;
    Caption="Da promozione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="QPBSPNNISV",Visible=.t., Left=7, Top=43,;
    Alignment=1, Width=96, Height=18,;
    Caption="A promozione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TVVCWTJUID",Visible=.t., Left=7, Top=74,;
    Alignment=1, Width=96, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YALLPNBSDL",Visible=.t., Left=7, Top=104,;
    Alignment=1, Width=96, Height=18,;
    Caption="Giorni validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="SFVMPZMENT",Visible=.t., Left=389, Top=74,;
    Alignment=1, Width=59, Height=18,;
    Caption="Valide il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="DYGLJWSYXZ",Visible=.t., Left=7, Top=162,;
    Alignment=1, Width=96, Height=18,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_21 as StdBox with uid="PTDKIKVGHK",left=104, top=99, width=417,height=54
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_spr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
