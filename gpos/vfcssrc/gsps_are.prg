* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_are                                                        *
*              Reparti                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_27]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_are"))

* --- Class definition
define class tgsps_are as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 432
  Height = 104+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=126441577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  REP_ARTI_IDX = 0
  VOCIIVA_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "REP_ARTI"
  cKeySelect = "RECODREP"
  cKeyWhere  = "RECODREP=this.w_RECODREP"
  cKeyWhereODBC = '"RECODREP="+cp_ToStrODBC(this.w_RECODREP)';

  cKeyWhereODBCqualified = '"REP_ARTI.RECODREP="+cp_ToStrODBC(this.w_RECODREP)';

  cPrg = "gsps_are"
  cComment = "Reparti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_RECODREP = space(3)
  w_REDESREP = space(40)
  w_RECODIVA = space(5)
  w_DESIVA = space(35)
  w_PERIVA = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'REP_ARTI','gsps_are')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_arePag1","gsps_are",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Reparti")
      .Pages(1).HelpContextID = 201387498
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRECODREP_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='REP_ARTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.REP_ARTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.REP_ARTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RECODREP = NVL(RECODREP,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from REP_ARTI where RECODREP=KeySet.RECODREP
    *
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('REP_ARTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "REP_ARTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' REP_ARTI '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RECODREP',this.w_RECODREP  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_DESIVA = space(35)
        .w_PERIVA = 0
        .w_RECODREP = NVL(RECODREP,space(3))
        .w_REDESREP = NVL(REDESREP,space(40))
        .w_RECODIVA = NVL(RECODIVA,space(5))
          if link_1_6_joined
            this.w_RECODIVA = NVL(IVCODIVA106,NVL(this.w_RECODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA106,space(35))
            this.w_PERIVA = NVL(IVPERIVA106,0)
          else
          .link_1_6('Load')
          endif
        cp_LoadRecExtFlds(this,'REP_ARTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST = ctod("  /  /  ")
      .w_RECODREP = space(3)
      .w_REDESREP = space(40)
      .w_RECODIVA = space(5)
      .w_DESIVA = space(35)
      .w_PERIVA = 0
      if .cFunction<>"Filter"
        .w_OBTEST = i_DATSYS
        .DoRTCalc(2,4,.f.)
          if not(empty(.w_RECODIVA))
          .link_1_6('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'REP_ARTI')
    this.DoRTCalc(5,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRECODREP_1_2.enabled = i_bVal
      .Page1.oPag.oREDESREP_1_4.enabled = i_bVal
      .Page1.oPag.oRECODIVA_1_6.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRECODREP_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRECODREP_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'REP_ARTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RECODREP,"RECODREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REDESREP,"REDESREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RECODIVA,"RECODIVA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    i_lTable = "REP_ARTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.REP_ARTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("..\GPOS\EXE\QUERY\GSPS_ARE.VQR,..\GPOS\EXE\QUERY\GSPS_ARE.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.REP_ARTI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into REP_ARTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'REP_ARTI')
        i_extval=cp_InsertValODBCExtFlds(this,'REP_ARTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RECODREP,REDESREP,RECODIVA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RECODREP)+;
                  ","+cp_ToStrODBC(this.w_REDESREP)+;
                  ","+cp_ToStrODBCNull(this.w_RECODIVA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'REP_ARTI')
        i_extval=cp_InsertValVFPExtFlds(this,'REP_ARTI')
        cp_CheckDeletedKey(i_cTable,0,'RECODREP',this.w_RECODREP)
        INSERT INTO (i_cTable);
              (RECODREP,REDESREP,RECODIVA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RECODREP;
                  ,this.w_REDESREP;
                  ,this.w_RECODIVA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.REP_ARTI_IDX,i_nConn)
      *
      * update REP_ARTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'REP_ARTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " REDESREP="+cp_ToStrODBC(this.w_REDESREP)+;
             ",RECODIVA="+cp_ToStrODBCNull(this.w_RECODIVA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'REP_ARTI')
        i_cWhere = cp_PKFox(i_cTable  ,'RECODREP',this.w_RECODREP  )
        UPDATE (i_cTable) SET;
              REDESREP=this.w_REDESREP;
             ,RECODIVA=this.w_RECODIVA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.REP_ARTI_IDX,i_nConn)
      *
      * delete REP_ARTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RECODREP',this.w_RECODREP  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRECODIVA_1_6.enabled = this.oPgFrm.Page1.oPag.oRECODIVA_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RECODIVA
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RECODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_RECODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_RECODIVA))
          select IVCODIVA,IVDESIVA,IVPERIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RECODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RECODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oRECODIVA_1_6'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RECODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_RECODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_RECODIVA)
            select IVCODIVA,IVDESIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RECODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_RECODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RECODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.IVCODIVA as IVCODIVA106"+ ",link_1_6.IVDESIVA as IVDESIVA106"+ ",link_1_6.IVPERIVA as IVPERIVA106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on REP_ARTI.RECODIVA=link_1_6.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and REP_ARTI.RECODIVA=link_1_6.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRECODREP_1_2.value==this.w_RECODREP)
      this.oPgFrm.Page1.oPag.oRECODREP_1_2.value=this.w_RECODREP
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESREP_1_4.value==this.w_REDESREP)
      this.oPgFrm.Page1.oPag.oREDESREP_1_4.value=this.w_REDESREP
    endif
    if not(this.oPgFrm.Page1.oPag.oRECODIVA_1_6.value==this.w_RECODIVA)
      this.oPgFrm.Page1.oPag.oRECODIVA_1_6.value=this.w_RECODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_7.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_7.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIVA_1_9.value==this.w_PERIVA)
      this.oPgFrm.Page1.oPag.oPERIVA_1_9.value=this.w_PERIVA
    endif
    cp_SetControlsValueExtFlds(this,'REP_ARTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(VAL( .w_RECODREP )<>0)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice reparto non pu� essere 0")
          case   (empty(.w_RECODIVA))  and (psedtiva( .w_RECODREP ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRECODIVA_1_6.SetFocus()
            i_bnoObbl = !empty(.w_RECODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_arePag1 as StdContainer
  Width  = 428
  height = 104
  stdWidth  = 428
  stdheight = 104
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRECODREP_1_2 as StdField with uid="ZPVIVWSFYR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RECODREP", cQueryName = "RECODREP",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice reparto",;
    HelpContextID = 252322406,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=83, Top=12, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',3)

  add object oREDESREP_1_4 as StdField with uid="XLIZEOMPHB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_REDESREP", cQueryName = "REDESREP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 267399782,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=132, Top=12, InputMask=replicate('X',40)

  add object oRECODIVA_1_6 as StdField with uid="BDLVJYCHKR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RECODIVA", cQueryName = "RECODIVA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA associato al reparto",;
    HelpContextID = 101327447,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=83, Top=47, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_RECODIVA"

  func oRECODIVA_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (psedtiva( .w_RECODREP ))
    endwith
   endif
  endfunc

  func oRECODIVA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oRECODIVA_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRECODIVA_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oRECODIVA_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oRECODIVA_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_RECODIVA
     i_obj.ecpSave()
  endproc

  add object oDESIVA_1_7 as StdField with uid="EZEXKYSJMF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 254091574,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=153, Top=47, InputMask=replicate('X',35)

  add object oPERIVA_1_9 as StdField with uid="AAXBHBRZIS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PERIVA", cQueryName = "PERIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 254087670,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=375, Top=77, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oStr_1_3 as StdString with uid="GETKCXAMBT",Visible=.t., Left=3, Top=12,;
    Alignment=1, Width=76, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="IZDMBSMZTW",Visible=.t., Left=3, Top=47,;
    Alignment=1, Width=76, Height=18,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="GFBTNKSLIZ",Visible=.t., Left=286, Top=77,;
    Alignment=1, Width=85, Height=18,;
    Caption="Aliquota:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_are','REP_ARTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RECODREP=REP_ARTI.RECODREP";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
