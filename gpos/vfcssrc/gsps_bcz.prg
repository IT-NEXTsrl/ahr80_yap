* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bcz                                                        *
*              Ricalcolo variabili vendita negozio                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_146]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-23                                                      *
* Last revis.: 2012-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bcz",oParentObject,m.pTIPOPE)
return(i_retval)

define class tgsps_bcz as StdBatch
  * --- Local variables
  pTIPOPE = space(1)
  w_TOTDAP = 0
  w_TOTPAG = 0
  w_RESTO = 0
  w_MESS = space(10)
  w_NUREC = 0
  w_APPO = 0
  w_PADRE = .NULL.
  * --- WorkFile variables
  COR_RISP_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per ricalcolare le variabili della pagine Dati Chiusura nella Vendita Negozio al Cambio di uno dei valori (da GSPS_MVD)
    * --- pTIPOPE: S=Sconti ; P=Pagamenti (tranne Cliente) ; C=Pag.Cliente ; A=Abbuono ; T=Tipo Chiusura
    this.w_PADRE = this.oParentObject
    if this.pTIPOPE="T"
      * --- Cambio Tipo Chiusura
      * --- Riaggiorna il Progressivo
      * --- Propone il Primo Progressivo Documento Disponibile
      if PSCHKMOD(this.oParentObject.w_DOCDDT, this.oParentObject.w_MDTIPCHI)
        this.oParentObject.w_MDTIPDOC = IIF(this.oParentObject.w_MDTIPCHI="DT",this.oParentObject.w_DOCDDT,IIF(this.oParentObject.w_MDTIPCHI="RS", this.oParentObject.w_DOCRIF, IIF(this.oParentObject.w_MDTIPCHI="FF", this.oParentObject.w_DOCFAF,IIF(this.oParentObject.w_MDTIPCHI="FI",this.oParentObject.w_DOCFAT,IIF(this.oParentObject.w_MDTIPCHI="RF",this.oParentObject.w_DOCRIC,this.oParentObject.w_DOCCOR)))))
        * --- Solo per Fattura Fiscale / Corrispettivi
        this.oParentObject.w_MDTIPCOR = IIF(this.oParentObject.w_MDTIPCHI = "FF", this.oParentObject.w_DOCCOR, IIF(this.oParentObject.w_MDTIPCHI $ "NS-ES-BV", this.oParentObject.w_MDTIPDOC, SPACE(5)))
        if EMPTY(this.oParentObject.w_MDTIPDOC)
          this.w_MESS = "Non esiste un documento associato alla chiusura selezionata"
          ah_ErrorMsg(this.w_MESS,,"")
          this.oParentObject.w_MDTIPCHI = this.oParentObject.o_MDTIPCHI
          this.oParentObject.w_MDTIPDOC = this.oParentObject.o_MDTIPDOC
          this.oParentObject.w_MDTIPCOR = this.oParentObject.o_MDTIPCOR
          i_retcode = 'stop'
          return
        else
          * --- Solo per Fattura Fiscale 
          if this.oParentObject.o_MDTIPDOC<>this.oParentObject.w_MDTIPDOC
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDPRODOC,TDFLIMPA,TDFLRISC,TDFLANAL,TDFLCOMM"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MDTIPDOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDPRODOC,TDFLIMPA,TDFLRISC,TDFLANAL,TDFLCOMM;
                from (i_cTable) where;
                    TDTIPDOC = this.oParentObject.w_MDTIPDOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_MDPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
              this.oParentObject.w_FLIMPA = NVL(cp_ToDate(_read_.TDFLIMPA),cp_NullValue(_read_.TDFLIMPA))
              this.oParentObject.w_FLRISC = NVL(cp_ToDate(_read_.TDFLRISC),cp_NullValue(_read_.TDFLRISC))
              this.oParentObject.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
              this.oParentObject.w_FLGCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if this.oParentObject.w_FLGCOM<>"S"
            * --- Se la nuova causale non gestisce la commessa Svuoto i campi relativi sulle righe
            this.w_PADRE.MarkPos()     
            this.w_PADRE.FirstRow()     
            do while Not this.w_PADRE.Eof_Trs()
              this.w_PADRE.SetRow()     
              this.w_PADRE.Set("w_MDCODCOM" , Space(15))     
              this.w_PADRE.Set("w_MDIMPCOM" , 0)     
              this.w_PADRE.NextRow()     
            enddo
            this.w_PADRE.RePos(.T.)     
          endif
        endif
        * --- Riazzero i Pagamenti e riporto il totale sui contanti solo se in caricamento.
        if this.w_PADRE.cFunction="Load"
          this.oParentObject.w_MDIMPPRE = IIF(this.oParentObject.w_FLFAFI ="S", this.oParentObject.w_MDIMPPRE,0)
          this.oParentObject.w_MDACCPRE = 0
          this.oParentObject.w_MDPAGCON = cp_ROUND(IIF(this.oParentObject.w_FLFAFI ="S", this.oParentObject.w_MDPAGCON,this.oParentObject.w_MDTOTDOC), this.oParentObject.w_DECTOT)
          this.oParentObject.w_MDPAGASS = 0
          this.oParentObject.w_MDPAGCAR = 0
          this.oParentObject.w_MDPAGFIN = 0
          this.oParentObject.w_MDPAGCLI = 0
          this.oParentObject.w_MDIMPABB = 0
        else
          * --- In modifica Azzero tutto tranne l'acconto precedente.
          *     In contanti: totale documento - acconto precedente
          this.oParentObject.w_MDIMPPRE = IIF(this.oParentObject.w_FLFAFI ="S", this.oParentObject.w_MDIMPPRE,0)
          this.oParentObject.w_MDPAGCON = cp_ROUND(IIF(this.oParentObject.w_FLFAFI ="S", this.oParentObject.w_MDPAGCON,this.oParentObject.w_MDTOTDOC - this.oParentObject.w_MDACCPRE), this.oParentObject.w_DECTOT)
          this.oParentObject.w_MDPAGASS = 0
          this.oParentObject.w_MDPAGCAR = 0
          this.oParentObject.w_MDPAGFIN = 0
          this.oParentObject.w_MDPAGCLI = 0
          this.oParentObject.w_MDIMPABB = 0
        endif
      else
        ah_ErrorMsg("Tipo chiusura non selezionabile: la causale documento prevede controlli su quantit� e/o importi",,"")
        this.oParentObject.w_MDTIPCHI = this.oParentObject.o_MDTIPCHI
        this.oParentObject.w_MDTIPDOC = this.oParentObject.o_MDTIPDOC
        this.oParentObject.w_MDTIPCOR = this.oParentObject.o_MDTIPCOR
        i_retcode = 'stop'
        return
      endif
    else
      if this.pTIPOPE="O"
        * --- Check Forza Sconti
        this.w_APPO = this.oParentObject.w_MDSCONTI
        if this.oParentObject.w_MDFLFOSC<>"S"
          * --- Se toglie check Ricalcola Sconti Globali
          this.oParentObject.w_MDSCONTI = cp_Round( - (this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSTOPRO) * cp_Round((1 - (1+this.oParentObject.w_MDSCOCL1/100)*(1+this.oParentObject.w_MDSCOCL2/100)*(1+this.oParentObject.w_MDSCOPAG/100)), 10), this.oParentObject.w_DECTOT )
        endif
        if this.oParentObject.w_MDSCONTI=this.w_APPO
          * --- Se gli sconti restano uguali non deve fare nulla
          this.bUpdateParentObject=.f.
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Se azzero il primo sconto deve azzerare subito anche il secondo
      this.oParentObject.w_MDSCOCL2 = IIF(this.oParentObject.w_MDSCOCL1=0, 0, this.oParentObject.w_MDSCOCL2)
      * --- Sconti Globali
      if this.oParentObject.w_MDFLFOSC<>"S" AND this.pTIPOPE$"SR"
        * --- Ricalcolo solo se non sono forzati
        if this.pTIPOPE="S"
          * --- Modifico gli sconti globali
          this.oParentObject.w_MDSCONTI = cp_Round( - (this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSTOPRO) * cp_Round((1 - (1+this.oParentObject.w_MDSCOCL1/100)*(1+this.oParentObject.w_MDSCOCL2/100)*(1+this.oParentObject.w_MDSCOPAG/100)), 10), this.oParentObject.w_DECTOT )
        else
          * --- Modifico il codice pagamento: ricalcolo gli sconti poich� potrebbe esserci uno sconto sul pagamento
          *     Utilizzo la variabile SCOPAG poich� � quella letta nel link sul codice pagamento
          this.oParentObject.w_MDSCONTI = cp_Round( - (this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSTOPRO) * cp_Round((1 - (1+this.oParentObject.w_MDSCOCL1/100)*(1+this.oParentObject.w_MDSCOCL2/100)*(1+this.oParentObject.w_SCOPAG/100)), 10), this.oParentObject.w_DECTOT )
        endif
      endif
      if NOT this.pTIPOPE$"ICPZ" Or (this.pTIPOPE="I" And this.oParentObject.w_FLFAFI = "S" And Not Empty(this.oParentObject.w_SERFID))
        * --- Ricalcolo l'importo prepagato nel caso di cambio sconti
        if Not(this.pTIPOPE="I" And this.oParentObject.w_FLFAFI = "S" And Not Empty(this.oParentObject.w_SERFID))
          this.oParentObject.w_MDIMPPRE = MIN(this.oParentObject.w_FIMPRES, ( this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSTOPRO + this.oParentObject.w_MDSCONTI ) )
        endif
        * --- Totale Documento
        this.oParentObject.w_MDTOTDOC = this.oParentObject.w_MDTOTVEN + (this.oParentObject.w_MDSCONTI+ this.oParentObject.w_MDSTOPRO - IIF(this.oParentObject.w_FLFAFI ="S" And Not Empty(this.oParentObject.w_SERFID),this.oParentObject.w_MDIMPPRE,0) )
        if this.oParentObject.w_MDTOTDOC < 0
          this.w_MESS = "Totale documento negativo%0Gli sconti verranno forzati al massimo possibile"
          ah_ErrorMsg(this.w_MESS,"!","")
          this.oParentObject.w_MDSCONTI = - (this.oParentObject.w_MDTOTVEN +this.oParentObject.w_MDSTOPRO)
          this.oParentObject.w_MDFLFOSC = "S"
        endif
        if NOT EMPTY(this.oParentObject.w_MDCODFID) And ( this.oParentObject.w_FLFAFI <> "S" Or this.pTipope <> "I" )
          * --- Prepagato al massimo uguale al documento
          this.oParentObject.w_MDIMPPRE = MIN(this.oParentObject.w_FIMPRES, ( this.oParentObject.w_MDTOTVEN + this.oParentObject.w_MDSTOPRO + this.oParentObject.w_MDSCONTI ) )
        endif
      endif
      this.w_TOTDAP = this.oParentObject.w_MDTOTDOC - (IIF(this.oParentObject.w_FLFAFI="S" And Not Empty(this.oParentObject.w_SERFID), 0, this.oParentObject.w_MDIMPPRE) + this.oParentObject.w_MDACCPRE)
      this.w_TOTPAG = this.oParentObject.w_MDPAGASS+this.oParentObject.w_MDPAGCAR+this.oParentObject.w_MDPAGFIN
    endif
    do case
      case this.pTIPOPE $ "SRFOI"
        * --- Cambia il Totale da Pagare mette la Differenza sul Contante
        this.oParentObject.w_MDPAGCON = cp_ROUND(this.w_TOTDAP - (this.w_TOTPAG + this.oParentObject.w_MDPAGCLI), this.oParentObject.w_DECTOT)
        this.oParentObject.w_MDIMPABB = 0
        if this.oParentObject.w_MDPAGCON<0
          * --- Se contante Negativo, sposta su Resto
          *     Se negativo poich� il totale documento � negativo, metto tutto a 0
          * --- ATTENZIONE:Gli arrotondamenti su MDIMPABB sono stati effettuati per un errore di Fox
          this.oParentObject.w_MDIMPABB = cp_ROUND(IIF(this.oParentObject.w_MDTOTDOC=0,0,this.oParentObject.w_MDPAGCON),this.oParentObject.w_DECTOT)
          this.oParentObject.w_MDPAGCON = 0
        endif
      case this.pTIPOPE$ "PZ"
        * --- Cambia il Totale Pagato mette la differenza sul Cliente
        if this.pTIPOPE="P"
          this.oParentObject.w_MDPAGCON = cp_ROUND(this.w_TOTDAP - (this.w_TOTPAG + this.oParentObject.w_MDPAGCLI), this.oParentObject.w_DECTOT)
          this.oParentObject.w_MDPAGCON = cp_ROUND(IIF(this.oParentObject.w_MDPAGCON<0,0,this.oParentObject.w_MDPAGCON), this.oParentObject.w_DECTOT)
        endif
        this.oParentObject.w_MDIMPABB = 0
        this.oParentObject.w_MDPAGCLI = this.w_TOTDAP - (this.w_TOTPAG+this.oParentObject.w_MDPAGCON)
        if this.oParentObject.w_MDPAGCLI<0
          * --- Se Cliente Negativo, sposta su Resto
          * --- ATTENZIONE:Gli arrotondamenti su MDIMPABB sono stati effettuati per un errore di Fox
          this.oParentObject.w_MDIMPABB = cp_ROUND(this.oParentObject.w_MDPAGCLI,this.oParentObject.w_DECTOT)
          this.oParentObject.w_MDPAGCLI = 0
        endif
      case this.pTIPOPE="C"
        * --- Cambia il Totale Cliente mette la Differenza sul Contante
        this.oParentObject.w_MDIMPABB = 0
        if this.oParentObject.w_MDPAGCLI > this.w_TOTDAP - (this.w_TOTPAG + this.oParentObject.w_MDPAGCON)
          this.oParentObject.w_MDPAGCON = cp_ROUND(this.w_TOTDAP - (this.w_TOTPAG + this.oParentObject.w_MDPAGCLI), this.oParentObject.w_DECTOT)
        else
          if this.oParentObject.w_MDTIPCHI $ "NS-ES-BV-RF"
            * --- ATTENZIONE:Gli arrotondamenti su MDIMPABB sono stati effettuati per un errore di Fox
            this.oParentObject.w_MDIMPABB = cp_ROUND(this.w_TOTDAP - (this.w_TOTPAG + this.oParentObject.w_MDPAGCLI + this.oParentObject.w_MDPAGCON),this.oParentObject.w_DECTOT)
          else
            this.oParentObject.w_MDPAGCON = cp_ROUND(this.w_TOTDAP - (this.w_TOTPAG + this.oParentObject.w_MDPAGCLI), this.oParentObject.w_DECTOT)
          endif
        endif
      case this.pTIPOPE="A"
        * --- Cambia l'Abbuono mette la differenza sul Cliente
        if EMPTY(this.oParentObject.w_MDCODCLI)
          this.oParentObject.w_MDPAGCLI = 0
        endif
        this.w_RESTO = this.w_TOTDAP - (this.w_TOTPAG+this.oParentObject.w_MDPAGCON+this.oParentObject.w_MDPAGCLI+this.oParentObject.w_MDIMPABB)
        if this.w_RESTO<>0
          if NOT EMPTY(this.oParentObject.w_MDCODCLI)
            this.oParentObject.w_MDPAGCLI = this.oParentObject.w_MDPAGCLI+this.w_RESTO
            this.w_RESTO = 0
            if this.oParentObject.w_MDPAGCLI<0
              this.w_RESTO = this.oParentObject.w_MDPAGCLI
              this.oParentObject.w_MDPAGCLI = 0
            endif
          endif
          if this.w_RESTO<>0
            this.oParentObject.w_MDPAGCON = cp_ROUND(this.oParentObject.w_MDPAGCON + this.w_RESTO, this.oParentObject.w_DECTOT)
            this.w_RESTO = 0
            if this.oParentObject.w_MDPAGCON<0
              this.w_RESTO = this.oParentObject.w_MDPAGCON
              this.oParentObject.w_MDPAGCON = 0
            endif
          endif
        endif
        * --- ATTENZIONE:Gli arrotondamenti su MDIMPABB sono stati effettuati per un errore di Fox
        this.oParentObject.w_MDIMPABB = cp_ROUND(this.w_TOTDAP - (this.w_TOTPAG+this.oParentObject.w_MDPAGCON+this.oParentObject.w_MDPAGCLI),this.oParentObject.w_DECTOT)
    endcase
    this.oParentObject.w_MDFLSALD = IIF(this.oParentObject.w_MDIMPABB=0," ",this.oParentObject.w_MDFLSALD)
  endproc


  proc Init(oParentObject,pTIPOPE)
    this.pTIPOPE=pTIPOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='COR_RISP'
    this.cWorkTables[2]='TIP_DOCU'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE"
endproc
