* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bsf                                                        *
*              Set focus alla lost focus                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-19                                                      *
* Last revis.: 2005-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bsf",oParentObject)
return(i_retval)

define class tgsps_bsf as StdBatch
  * --- Local variables
  w_Padre = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato alla Lost Focus di MDCODICE
    *     Se su riga carico il codice Fidelity da emulazione tastiera,
    *     nel GSAR_BGP Imposto 
    *     bDontReportError
    *     w_Lost_Cod
    *     a .T. per impedire che venga fatta la SetFocus sul campo successivo al codice articolo
    *     
    *     Quindi, in questo caso, alla LostFocus reimposto le variabili al valore Originale
    this.w_Padre = this.oParentObject
    if this.w_Padre.w_Lost_Cod
      * --- Ripristino Vecchio valore salvato in GSAR_BGP
      this.w_Padre.bDontReportError = this.w_Padre.w_Old_bDRE
      this.w_Padre.w_Lost_Cod = .F.
    endif
    * --- cmq non esegue mai la mCalc
    this.bUpdateParentObject=.f.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
