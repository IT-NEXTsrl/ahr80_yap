* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bpv                                                        *
*              Stampa analisi del venduto                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_394]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-03                                                      *
* Last revis.: 2016-08-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bpv",oParentObject)
return(i_retval)

define class tgsps_bpv as StdBatch
  * --- Local variables
  w_CAMBIOL = 0
  w_VALCOS = space(3)
  w_CAOCOS = 0
  w_CAOESE1 = 0
  w_CAOVAL = 0
  w_DECTOT = 0
  w_CAMBIO2L = 0
  w_VALU = space(3)
  w_SIMVAL = space(5)
  w_DATAIN = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_QTAMOV = 0
  w_COUNT = 0
  w_COSTO = 0
  w_SERIALE = space(10)
  w_RIFRIGA = 0
  w_ARTICOLO = space(20)
  w_SERVEN = space(10)
  w_ROWVEN = 0
  w_SERIAL = space(0)
  w_ORADOC = space(4)
  w_GIODOC = space(2)
  w_OMINI = space(4)
  w_OMFIN = space(4)
  w_TIPRIG = space(1)
  w_TIPRIG1 = space(1)
  * --- WorkFile variables
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  TMPVEND3_idx=0
  VALUTE_idx=0
  TMP_ACQU_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch elaborazione stampa Analisi  del venduto lanciato dalla maschera  GSPS_SAV
    * --- Variabili Elaborazione LIFO\FIFO Puntuale
    if this.oParentObject.w_COSTMARG="S" and (this.oParentObject.w_AGGIORN<>"UCST" AND this.oParentObject.w_AGGIORN<>"LIST")
      if EMPTY(NVL(this.oParentObject.w_NUMERO,""))
        ah_ErrorMsg("Inserire numero inventario",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_CAMBIO <> this.oParentObject.w_CAMBIO1
      ah_ErrorMsg("I cambi devono essere uguali",,"")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_INTERVAL=0 AND this.oParentObject.w_ONUME=9
      ah_ErrorMsg("Inserire intervallo fascia oraria",,"")
      i_retcode = 'stop'
      return
    endif
    if  EMPTY(this.oParentObject.w_VALU2)
      ah_ErrorMsg("Inserire codice valuta",,"")
      i_retcode = 'stop'
      return
    endif
    this.w_OMINI = ALLTRIM(this.oParentObject.w_ORAINI)+ALLTRIM(this.oParentObject.w_MININI)
    this.w_OMFIN = ALLTRIM(this.oParentObject.w_ORAFIN)+ALLTRIM(this.oParentObject.w_MINFIN)
    this.w_VALU = this.oParentObject.w_VALU2
    this.w_CAOESE1 = this.oParentObject.w_CAOESE
    this.w_CAOVAL = GETCAM(g_PERVAL,i_DATSYS)
    this.w_CAMBIOL = this.oParentObject.w_CAMBIO1
    this.w_CAMBIO2L = this.oParentObject.w_CAMBIO2
    this.w_DECTOT = this.oParentObject.w_DECTOT2
    this.w_VALCOS = this.oParentObject.w_VALUTA
    this.w_CAOCOS = this.oParentObject.w_CAOVAL1
    this.w_DATAFIN = this.oParentObject.w_ADATA
    this.w_DATAIN = this.oParentObject.w_DADATA
    * --- Creo la tabella temporanea TMPVEND1 dalla query GSPS_PDV che prende i dati dalla tabella documenti.
    *     Nella query vengono gi� eseguiti i filtri sui documenti che devono essere presi in considerazione a seconda 
    *     dei check spuntati sulla maschera di stampa.
    *     Inoltre per i Documenti di Trasporto viene presa in considerazione la quantit� evasa e spuntato il flag evaso 
    *     solo per le righe totalmente evase.  Per gli altri documenti questi campi sono vuoti.
    *     Sono stati inseriti alcune elaborazioni specifiche per determinate stampe la cui definizione non era possibile farla direttamente
    *     nella query finale .
    *     Per tale motivo si � reso necessario filtrare per w_Onume per identificare la stampa.
    *     Vengono anche predisposti due campi  GIODOC,TOTVEN che assumeranno i valori di totale venduto
    *     e giorno della settimana.
    *     Per ORACLE sono stati creati gli indici delle tabelle temporanee manualmente poich� non vengono generati in automatico.
    *     La mancanza di indici causa lentezza eccessiva con archivi pesanti.
    ah_Msg("Seleziono i documenti",.T.)
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\gpos\exe\query\gsps_sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Riscrivo in TMPVEND2 la tabella TMPVEND1 riportando anche il valore di riga con le spese accessorie ripartite
    * --- Inoltre in  MDQTAEV1 riporto la quantit� evasa, cio�, se il flag riga evasa � 'S' riporto MDQTAUM1 altrimenti MDQTAEV1
    ah_Msg("Ripartizione spese accessorie",.T.)
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\gpos\exe\query\gsps2sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TMPVEND2_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Elaboro dati di riga",.T.)
    * --- Passando attraverso la query Gsps1sav elimino righe non significative
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\gpos\exe\query\gsps1sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Scrivo il totale di riga su TMPVEND1 nel campo TOTDOC
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTDOC = _t2.TOTDOC";
          +i_ccchkf;
          +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND1.TOTDOC = _t2.TOTDOC";
          +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
          +"TOTDOC = _t2.TOTDOC";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTDOC = (select TOTDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_Msg("Azzeramento righe sconto",.T.)
    * --- SE ho scelto come tipo di aggiornamento LISTINO:
    this.w_TIPRIG = IIF(this.oParentObject.w_AGGIORN="LIST", " ", "F")
    this.w_TIPRIG1 = IIF(this.oParentObject.w_AGGIORN="LIST", " ", "M")
    if this.oParentObject.w_COSTMARG="S"
      if NOT EMPTY(this.oParentObject.w_LISTINO)
        ah_Msg("Aggiornamento da listino",.T.)
        * --- Considerando solo i listini validi, per ogni riga vado a considerare gli scaglioni prendendo i prezzi.
        *     Predispongo anche un campo di appoggio NUMSCA che inizializzer� a 9999999999999 se quantit� scaglione
        *     � OLTRE (0) e alla differenza tra quantit� scaglione e MVQTAUM1 negli altri casi.
        *     Questo poich� mi interesser� solo lo scaglione con il NUMSCA minore fra i positivi 
        * --- Create temporary table TMPVEND2
        i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\gpos\exe\query\gsps3sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Da TMPVEND2 prendo solo i listini con data di attivazione pi� vicina a quella che si � scelta per l'elaborazione
        ah_Msg("Selezione listino valido",.T.)
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\gpos\exe\query\gsps4sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- In TMPVEND2 per ogni riga dei documento con data attivazione uguale a quella di TMPVEND3
        *     e cio� quella pi� valida, vado a scrivere il prezzo presente sul listino
        *     su COSTO1(campo precedentemente inizializzato a 0)
        ah_Msg("Scrittura prezzo di listino",.T.)
        * --- Write into TMPVEND2
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND2_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPVEND2.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.DATATT = _t2.DATATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(987),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" from "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPVEND2.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.DATATT = _t2.DATATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND2.COSTO1 ="+cp_NullLink(cp_ToStrODBC(987),'TMPVEND2','COSTO1');
              +Iif(Empty(i_ccchkf),"",",TMPVEND2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="TMPVEND2.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.DATATT = _t2.DATATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2 set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(987),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".DATATT = "+i_cQueryTable+".DATATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(987),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimino da TMPVEND2 tutte le righe che hanno COSTO1 = 0 poich� sono quelle che non hanno un listino valido
        *     e tutte quelle che hanno NUMSCA minore di 0  
        ah_Msg("Eliminazione righe vuote",.T.)
        * --- Delete from TMPVEND2
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"COSTO1 <> "+cp_ToStrODBC(987);
                 )
        else
          delete from (i_cTable) where;
                COSTO1 <> 987;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Da TMPVEND2 prendo tra le righe uguali quelle che hanno il NUMSCA minore 
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\gpos\exe\query\gsps5sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Scrivo in TMPVEND2 il COSTO della riga dove NUMSCA � uguale a quello di TMPVEND3
        *     precedentemente filtrato
        ah_Msg("Scrittura costo",.T.)
        * --- Write into TMPVEND2
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND2_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPVEND2.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.MDCODART = _t2.MDCODART";
                  +" and "+"TMPVEND2.NUMSCA = _t2.NUMSCA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(789),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" from "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPVEND2.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.MDCODART = _t2.MDCODART";
                  +" and "+"TMPVEND2.NUMSCA = _t2.NUMSCA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND2.COSTO1 ="+cp_NullLink(cp_ToStrODBC(789),'TMPVEND2','COSTO1');
              +Iif(Empty(i_ccchkf),"",",TMPVEND2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="TMPVEND2.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.MDCODART = _t2.MDCODART";
                  +" and "+"TMPVEND2.NUMSCA = _t2.NUMSCA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2 set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(789),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
                  +" and "+i_cTable+".NUMSCA = "+i_cQueryTable+".NUMSCA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(789),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Cancello ancora da TMPVEND2 le righe con COSTO = 0 poich� sono quelle che non ci interessano
        * --- Delete from TMPVEND2
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"COSTO1 <> "+cp_ToStrODBC(789);
                 )
        else
          delete from (i_cTable) where;
                COSTO1 <> 789;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Prendo i dati utili dalla TMPVEND2 insieme a quelli della TMPVEND1 e li memorizzo in TMPVEND3
        *     Metto in join le due tabelle scrivendo il COSTO, prezzo di listino, dove MDSERIAL e CPROWNUM sono uguali 
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\gpos\exe\query\gsps6sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Riscrivo esattamente la TMPVEND3 in TMPVEND1 con i dati definitivi
        ah_Msg("Elaborazione temporaneo",.T.)
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\gpos\exe\query\gsps7sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Write into TMPVEND1
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COSTO = _t2.COSTO";
              +i_ccchkf;
              +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
              +"TMPVEND1.COSTO = _t2.COSTO";
              +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                  +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
              +"COSTO = _t2.COSTO";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COSTO = (select COSTO from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        do case
          case this.oParentObject.w_FLSCOIVA="S" AND this.oParentObject.w_TIPOLN="L"
            * --- Scorporo iva e Listino al lordo
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.COSTO1";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.COSTO1";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.COSTO1";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select COSTO1 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_FLSCOIVA<>"S" AND this.oParentObject.w_TIPOLN="N"
            * --- No Scorporo iva e Listino al netto
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.COSTO2";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.COSTO2";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.COSTO2";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select COSTO2 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
      endif
      if this.oParentObject.w_AGGIORN<>"LIST"
        if this.oParentObject.w_AGGIORN="UCST"
          * --- Caso in cui viene scelto come valorizzazione di costi L'ultimo costo standard presente nella 
          *     anagrafica degli articoli
          ah_Msg("Valorizzazione costi da ultimo costo standard",.T.)
          * --- Create temporary table TMPVEND3
          i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\gpos\exe\query\gsps8sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Caso di scelta di valorizzazione dei costi attraverso inventario
          ah_Msg("Valorizzazione costi da inventario",.T.)
          * --- Create temporary table TMPVEND3
          i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\gpos\exe\query\gsps9sav',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
        ah_Msg("Scrittura dei costi nel temporaneo",.T.)
        * --- Ogni valorizzazione del costo viene tradotta in EURO
        do case
          case this.oParentObject.w_AGGIORN="CMPA"
            * --- Costo Medio Ponderato Annuo
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSMPA";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSMPA";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSMPA";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSMPA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="CMPP"
            * --- Costo Medio Ponderato Periodico
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSMPP";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSMPP";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSMPP";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSMPP from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="COUL"
            * --- Costo Ultimo
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSULT";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSULT";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSULT";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSULT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="COST"
            * --- Costo Standard
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSSTA";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSSTA";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSSTA";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSSTA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="LICO"
            * --- Lifo Continuo
            if this.oParentObject.w_FLPUNT="S"
              * --- Elaborazione Lifo Continuo Puntuale
              * --- Create temporary table TMP_ACQU
              i_nIdx=cp_AddTableDef('TMP_ACQU') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('query\gsve_qlp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMP_ACQU_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Write into TMPVEND1
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPVEND1_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
              if i_nConn<>0
                local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                declare i_aIndex[1]
                i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
                i_cDB=cp_GetDatabaseType(i_nConn)
                do case
                case i_cDB="SQLServer"
                  i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COSTO = _t2.DICOSLCO";
                    +i_ccchkf;
                    +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
                case i_cDB="MySQL"
                  i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                    +"TMPVEND1.COSTO = _t2.DICOSLCO";
                    +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                    +" where "+i_cWhere)
                case i_cDB="PostgreSQL"
                  i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                    +"COSTO = _t2.DICOSLCO";
                    +i_ccchkf;
                    +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                otherwise
                  i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                        +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                        +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COSTO = (select DICOSLCO from "+i_cQueryTable+" where "+i_cWhere+")";
                    +i_ccchkf;
                    +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                endcase
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          case this.oParentObject.w_AGGIORN="FICO"
            * --- Fifo Continuo
            if this.oParentObject.w_FLPUNT="S"
              * --- Elaborazione Fifo Continuo Puntuale
              * --- Create temporary table TMP_ACQU
              i_nIdx=cp_AddTableDef('TMP_ACQU') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('query\gsve_qfp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMP_ACQU_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Write into TMPVEND1
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPVEND1_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
              if i_nConn<>0
                local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                declare i_aIndex[1]
                i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
                i_cDB=cp_GetDatabaseType(i_nConn)
                do case
                case i_cDB="SQLServer"
                  i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COSTO = _t2.DICOSFCO";
                    +i_ccchkf;
                    +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
                case i_cDB="MySQL"
                  i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                    +"TMPVEND1.COSTO = _t2.DICOSFCO";
                    +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                    +" where "+i_cWhere)
                case i_cDB="PostgreSQL"
                  i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                    +"COSTO = _t2.DICOSFCO";
                    +i_ccchkf;
                    +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                otherwise
                  i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                        +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                        +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COSTO = (select DICOSFCO from "+i_cQueryTable+" where "+i_cWhere+")";
                    +i_ccchkf;
                    +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                endcase
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          case this.oParentObject.w_AGGIORN="LISC"
            * --- Lifo a Scatti
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSLSC";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSLSC";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSLSC";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSLSC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="UCST"
            * --- Ultimo Costo Standard
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.PRCOSSTA";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.PRCOSSTA";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.PRCOSSTA";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select PRCOSSTA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
      endif
    endif
    * --- Nel caso di Note di credito metto il venduto e il costo in negativo! Inoltre converto i valori in EURO
    *     Moltiplico anche il costo per la quantit� che nel caso di Reso � negativa
    * --- Create temporary table TMPVEND3
    i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\gpos\exe\query\gsps10sv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if this.oParentObject.w_STVAL="C"
      * --- Eseguo la conversione dei valori nel caso in cui scelgo come valuta finale la moneta di conto
      ah_Msg("Conversione verso valuta di conto",.T.)
      * --- Write into TMPVEND1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPVEND1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COSTO = _t2.COSTO";
            +",TOTVEN = _t2.TOTVEN";
            +i_ccchkf;
            +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
            +"TMPVEND1.COSTO = _t2.COSTO";
            +",TMPVEND1.TOTVEN = _t2.TOTVEN";
            +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
            +"COSTO = _t2.COSTO";
            +",TOTVEN = _t2.TOTVEN";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COSTO = (select COSTO from "+i_cQueryTable+" where "+i_cWhere+")";
            +",TOTVEN = (select TOTVEN from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Eseguo la conversione dei valori nel caso in cui scelgo una valuta finale diversa da quella di conto 
      ah_Msg("Conversione verso valuta scelta",.T.)
      * --- Write into TMPVEND1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPVEND1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COSTO = _t2.COSTO";
            +",TOTVEN = _t2.TOTVEN";
            +i_ccchkf;
            +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
            +"TMPVEND1.COSTO = _t2.COSTO";
            +",TMPVEND1.TOTVEN = _t2.TOTVEN";
            +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
                +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPVEND1.MDCODART = _t2.MDCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
            +"COSTO = _t2.COSTO";
            +",TOTVEN = _t2.TOTVEN";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MDCODART = "+i_cQueryTable+".MDCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COSTO = (select COSTO from "+i_cQueryTable+" where "+i_cWhere+")";
            +",TOTVEN = (select TOTVEN from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TMPVEND1_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if this.oParentObject.w_FLSCOIVA="S" 
      * --- Eseguo scorporo dell'iva solo  per il venduto
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\gpos\exe\query\gsps14sv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Eseguo calcolo costo al lordo
      if this.oParentObject.w_AGGIORN<>"LIST"
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\gpos\exe\query\gsps15sv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TMPVEND1_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if  this.oParentObject.w_ONUME=11
      * --- Raggruppo per seriale documento senza sommare gli importi.
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\gpos\exe\query\gsps12sv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Distinguo Abbuoni dall'eventuale resto
      * --- Create temporary table TMPVEND3
      i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.TMPVEND1_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            +" where MDIMPABB<0";
            )
      this.TMPVEND3_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Aggiorno contante con il resto
      * --- Write into TMPVEND1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPVEND1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MDPAGCON = TMPVEND1.MDPAGCON+_t2.MDIMPABB";
        +",MDIMPABB ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','MDIMPABB');
            +i_ccchkf;
            +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
            +"TMPVEND1.MDPAGCON = TMPVEND1.MDPAGCON+_t2.MDIMPABB";
        +",TMPVEND1.MDIMPABB ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','MDIMPABB');
            +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
            +"MDPAGCON = TMPVEND1.MDPAGCON+_t2.MDIMPABB";
        +",MDIMPABB ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','MDIMPABB');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MDPAGCON = (select "+i_cTable+".MDPAGCON+MDIMPABB from "+i_cQueryTable+" where "+i_cWhere+")";
        +",MDIMPABB ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','MDIMPABB');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\gpos\exe\query\gsps11sv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Creo colonne relativi al giorno della settimana e delle ore
    if CP_DBTYPE="SQLServer" or CP_DBTYPE="PostgreSQL"
      * --- Write into TMPVEND1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPVEND1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MDSERIAL"
        do vq_exec with 'GSPSGSAV_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ORADOC = _t2.ORADOC";
            +",GIODOC = _t2.GIODOC";
            +i_ccchkf;
            +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
            +"TMPVEND1.ORADOC = _t2.ORADOC";
            +",TMPVEND1.GIODOC = _t2.GIODOC";
            +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPVEND1.MDSERIAL = t2.MDSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set (";
            +"ORADOC,";
            +"GIODOC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.ORADOC,";
            +"t2.GIODOC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
            +"ORADOC = _t2.ORADOC";
            +",GIODOC = _t2.GIODOC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ORADOC = (select ORADOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",GIODOC = (select GIODOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPVEND1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPVEND1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MDSERIAL"
        do vq_exec with 'GSPSGSAV',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ORADOC = _t2.ORADOC";
            +",GIODOC = _t2.GIODOC";
            +i_ccchkf;
            +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
            +"TMPVEND1.ORADOC = _t2.ORADOC";
            +",TMPVEND1.GIODOC = _t2.GIODOC";
            +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPVEND1.MDSERIAL = t2.MDSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set (";
            +"ORADOC,";
            +"GIODOC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.ORADOC,";
            +"t2.GIODOC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPVEND1.MDSERIAL = _t2.MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
            +"ORADOC = _t2.ORADOC";
            +",GIODOC = _t2.GIODOC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ORADOC = (select ORADOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",GIODOC = (select GIODOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Lettura della descrizione della valuta per la stampa e passaggio delle selezioni di stampa
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_VALU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_AGGIORN=this.oParentObject.w_AGGIORN
    L_CLIENTE=this.oParentObject.w_CLIENTE
    L_CODART=this.oParentObject.w_CODART
    L_GRUMER=this.oParentObject.w_GRUMER
    L_PAGAM=this.oParentObject.w_PAGAM
    L_LISTINO=this.oParentObject.w_LISTINO
    L_NUMERO=this.oParentObject.w_NUMERO
    L_COSTMARG=this.oParentObject.w_COSTMARG
    L_DECTOT2=this.oParentObject.w_DECTOT2
    L_SIMVAL=this.w_SIMVAL
    L_VALU=this.w_VALU
    L_CAMBIO=this.w_CAMBIO2L
    L_DADATA=this.w_DATAIN
    L_ADATA=this.w_DATAFIN
    L_FLPUNT=this.oParentObject.w_FLPUNT
    L_CODNEG=this.oParentObject.w_CODNEG
    L_CODREP=this.oParentObject.w_CODREP
    L_CODOPE=this.oParentObject.w_CODOPE
    L_INTERVAL=this.oParentObject.w_INTERVAL
    L_DOC=this.oParentObject.w_GIODOM
    L_SAB=this.oParentObject.w_GIOSAB
    L_VEN=this.oParentObject.w_GIOVEN
    L_GIO=this.oParentObject.w_GIOGIO
    L_MER=this.oParentObject.w_GIOMER
    L_MAR=this.oParentObject.w_GIOMAR
    L_LUN=this.oParentObject.w_GIOLUN
    L_SCOIVA=this.oParentObject.w_FLSCOIVA
    L_DATSTA=this.oParentObject.w_DATSTA
    * --- Eseguo la query per la Stampa
    ah_Msg("Stampa in corso...",.T.)
    vq_exec(alltrim(this.oParentObject.w_OQRY),this,"__tmp__")
    if this.oParentObject.w_FLORDI="S"
      * --- Ordina per Totale Venduto 
      SELECT * FROM __TMP__ ORDER BY 3 DESC INTO CURSOR __TMP__
    endif
    * --- Lancio la stampa
    CP_CHPRN(this.oParentObject.w_OREP, " ", this)
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
    * --- Drop temporary table TMPVEND3
    i_nIdx=cp_GetTableDefIdx('TMPVEND3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND3')
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_QTAMOV = 0
    ah_Msg("Elaborazione LIFO\FIFO puntuale",.T.)
    * --- Select from TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2],.t.,this.TMPVEND1_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPVEND1 ";
           ,"_Curs_TMPVEND1")
    else
      select * from (i_cTable);
        into cursor _Curs_TMPVEND1
    endif
    if used('_Curs_TMPVEND1')
      select _Curs_TMPVEND1
      locate for 1=1
      do while not(eof())
      this.w_COSTO = 0
      this.w_COUNT = 0
      this.w_QTAMOV = NVL(_Curs_TMPVEND1.MDQTAUM1,0)
      this.w_ARTICOLO = NVL(_Curs_TMPVEND1.MDCODART,SPACE(20))
      this.w_SERVEN = _Curs_TMPVEND1.MDSERIAL
      this.w_ROWVEN = _Curs_TMPVEND1.CPROWNUM
      if this.oParentObject.w_AGGIORN="LICO"
        * --- Select from TMP_ACQU
        i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2],.t.,this.TMP_ACQU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_ACQU ";
              +" where ARTICOLO= "+cp_ToStrODBC(this.w_ARTICOLO)+"";
              +" order by DATREG desc,SERIALE desc,RIFRIGA desc";
               ,"_Curs_TMP_ACQU")
        else
          select * from (i_cTable);
           where ARTICOLO= this.w_ARTICOLO;
           order by DATREG desc,SERIALE desc,RIFRIGA desc;
            into cursor _Curs_TMP_ACQU
        endif
        if used('_Curs_TMP_ACQU')
          select _Curs_TMP_ACQU
          locate for 1=1
          do while not(eof())
          this.w_SERIALE = _Curs_TMP_ACQU.SERIALE
          this.w_RIFRIGA = _Curs_TMP_ACQU.RIFRIGA
          if this.w_QTAMOV<>0
            do case
              case this.w_QTAMOV >= NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                this.w_QTAMOV = this.w_QTAMOV- NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.COSINACQ,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0))
                this.w_COUNT = this.w_COUNT+NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                * --- Delete from TMP_ACQU
                i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"SERIALE = "+cp_ToStrODBC(this.w_SERIALE);
                        +" and ARTICOLO = "+cp_ToStrODBC(this.w_ARTICOLO);
                        +" and RIFRIGA = "+cp_ToStrODBC(this.w_RIFRIGA);
                         )
                else
                  delete from (i_cTable) where;
                        SERIALE = this.w_SERIALE;
                        and ARTICOLO = this.w_ARTICOLO;
                        and RIFRIGA = this.w_RIFRIGA;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
              case this.w_QTAMOV < NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.COSINACQ,0)*this.w_QTAMOV)
                this.w_COUNT = this.w_COUNT+this.w_QTAMOV
                * --- Write into TMP_ACQU
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ACQU_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"QTAINACQ =QTAINACQ- "+cp_ToStrODBC(this.w_QTAMOV);
                      +i_ccchkf ;
                  +" where ";
                      +"SERIALE = "+cp_ToStrODBC(this.w_SERIALE);
                      +" and ARTICOLO = "+cp_ToStrODBC(this.w_ARTICOLO);
                      +" and RIFRIGA = "+cp_ToStrODBC(this.w_RIFRIGA);
                         )
                else
                  update (i_cTable) set;
                      QTAINACQ = QTAINACQ - this.w_QTAMOV;
                      &i_ccchkf. ;
                   where;
                      SERIALE = this.w_SERIALE;
                      and ARTICOLO = this.w_ARTICOLO;
                      and RIFRIGA = this.w_RIFRIGA;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                this.w_QTAMOV = 0
            endcase
          endif
            select _Curs_TMP_ACQU
            continue
          enddo
          use
        endif
      else
        * --- Select from TMP_ACQU
        i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2],.t.,this.TMP_ACQU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_ACQU ";
              +" where ARTICOLO= "+cp_ToStrODBC(this.w_ARTICOLO)+"";
              +" order by DATREG,SERIALE,RIFRIGA";
               ,"_Curs_TMP_ACQU")
        else
          select * from (i_cTable);
           where ARTICOLO= this.w_ARTICOLO;
           order by DATREG,SERIALE,RIFRIGA;
            into cursor _Curs_TMP_ACQU
        endif
        if used('_Curs_TMP_ACQU')
          select _Curs_TMP_ACQU
          locate for 1=1
          do while not(eof())
          this.w_SERIALE = _Curs_TMP_ACQU.SERIALE
          this.w_RIFRIGA = _Curs_TMP_ACQU.RIFRIGA
          if this.w_QTAMOV<>0
            do case
              case this.w_QTAMOV >= NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                this.w_QTAMOV = this.w_QTAMOV- NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.COSINACQ,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0))
                this.w_COUNT = this.w_COUNT+NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                * --- Delete from TMP_ACQU
                i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"SERIALE = "+cp_ToStrODBC(this.w_SERIALE);
                        +" and ARTICOLO = "+cp_ToStrODBC(this.w_ARTICOLO);
                        +" and RIFRIGA = "+cp_ToStrODBC(this.w_RIFRIGA);
                         )
                else
                  delete from (i_cTable) where;
                        SERIALE = this.w_SERIALE;
                        and ARTICOLO = this.w_ARTICOLO;
                        and RIFRIGA = this.w_RIFRIGA;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
              case this.w_QTAMOV < NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.COSINACQ,0)*this.w_QTAMOV)
                this.w_COUNT = this.w_COUNT+this.w_QTAMOV
                * --- Write into TMP_ACQU
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ACQU_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"QTAINACQ =QTAINACQ- "+cp_ToStrODBC(this.w_QTAMOV);
                      +i_ccchkf ;
                  +" where ";
                      +"SERIALE = "+cp_ToStrODBC(this.w_SERIALE);
                      +" and ARTICOLO = "+cp_ToStrODBC(this.w_ARTICOLO);
                      +" and RIFRIGA = "+cp_ToStrODBC(this.w_RIFRIGA);
                         )
                else
                  update (i_cTable) set;
                      QTAINACQ = QTAINACQ - this.w_QTAMOV;
                      &i_ccchkf. ;
                   where;
                      SERIALE = this.w_SERIALE;
                      and ARTICOLO = this.w_ARTICOLO;
                      and RIFRIGA = this.w_RIFRIGA;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                this.w_QTAMOV = 0
            endcase
          endif
            select _Curs_TMP_ACQU
            continue
          enddo
          use
        endif
      endif
      if this.w_COUNT<>0
        this.w_COSTO = this.w_COSTO/this.w_COUNT
        * --- Write into TMPVEND1
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO ="+cp_NullLink(cp_ToStrODBC(this.w_COSTO),'TMPVEND1','COSTO');
              +i_ccchkf ;
          +" where ";
              +"MDSERIAL = "+cp_ToStrODBC(this.w_SERVEN);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWVEN);
                 )
        else
          update (i_cTable) set;
              COSTO = this.w_COSTO;
              &i_ccchkf. ;
           where;
              MDSERIAL = this.w_SERVEN;
              and CPROWNUM = this.w_ROWVEN;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_TMPVEND1
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMP_ACQU
    i_nIdx=cp_GetTableDefIdx('TMP_ACQU')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ACQU')
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='*TMPVEND1'
    this.cWorkTables[2]='*TMPVEND2'
    this.cWorkTables[3]='*TMPVEND3'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='*TMP_ACQU'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_TMPVEND1')
      use in _Curs_TMPVEND1
    endif
    if used('_Curs_TMP_ACQU')
      use in _Curs_TMP_ACQU
    endif
    if used('_Curs_TMP_ACQU')
      use in _Curs_TMP_ACQU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
