* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_brf                                                        *
*              Ricostruzione Fidelity                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-20                                                      *
* Last revis.: 2005-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_brf",oParentObject)
return(i_retval)

define class tgsps_brf as StdBatch
  * --- Local variables
  * --- WorkFile variables
  FID_CARD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per Ricostruzione Punti e Importi Fidelity Card in base 
    *     alle vendite al Dettaglio e ai Movimenti Fidelity
    ah_Msg("Inizio elaborazione....",.T.)
    * --- Write into FID_CARD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.FID_CARD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FID_CARD_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="FCCODFID"
      do vq_exec with 'gsps2brf',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.FID_CARD_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="FID_CARD.FCCODFID = _t2.FCCODFID";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FCIMPRES = _t2.FCIMPRES";
          +",FCPUNFID = _t2.FCPUNFID";
          +",FCDATUAC = _t2.FCDATUAC";
          +i_ccchkf;
          +" from "+i_cTable+" FID_CARD, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="FID_CARD.FCCODFID = _t2.FCCODFID";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FID_CARD, "+i_cQueryTable+" _t2 set ";
          +"FID_CARD.FCIMPRES = _t2.FCIMPRES";
          +",FID_CARD.FCPUNFID = _t2.FCPUNFID";
          +",FID_CARD.FCDATUAC = _t2.FCDATUAC";
          +Iif(Empty(i_ccchkf),"",",FID_CARD.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="FID_CARD.FCCODFID = t2.FCCODFID";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FID_CARD set (";
          +"FCIMPRES,";
          +"FCPUNFID,";
          +"FCDATUAC";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.FCIMPRES,";
          +"t2.FCPUNFID,";
          +"t2.FCDATUAC";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="FID_CARD.FCCODFID = _t2.FCCODFID";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FID_CARD set ";
          +"FCIMPRES = _t2.FCIMPRES";
          +",FCPUNFID = _t2.FCPUNFID";
          +",FCDATUAC = _t2.FCDATUAC";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".FCCODFID = "+i_cQueryTable+".FCCODFID";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FCIMPRES = (select FCIMPRES from "+i_cQueryTable+" where "+i_cWhere+")";
          +",FCPUNFID = (select FCPUNFID from "+i_cQueryTable+" where "+i_cWhere+")";
          +",FCDATUAC = (select FCDATUAC from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_ErrorMsg("Elaborazione terminata",,"")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='FID_CARD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
