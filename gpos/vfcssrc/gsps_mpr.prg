* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_mpr                                                        *
*              Promozioni                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_239]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-19                                                      *
* Last revis.: 2015-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_mpr"))

* --- Class definition
define class tgsps_mpr as StdTrsForm
  Top    = 17
  Left   = 23

  * --- Standard Properties
  Width  = 587
  Height = 376+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-19"
  HelpContextID=147413097
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=73

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  PRM_MAST_IDX = 0
  PRM_DETT_IDX = 0
  CATECOMM_IDX = 0
  GRUMERC_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  cFile = "PRM_MAST"
  cFileDetail = "PRM_DETT"
  cKeySelect = "PRCODICE"
  cKeyWhere  = "PRCODICE=this.w_PRCODICE"
  cKeyDetail  = "PRCODICE=this.w_PRCODICE"
  cKeyWhereODBC = '"PRCODICE="+cp_ToStrODBC(this.w_PRCODICE)';

  cKeyDetailWhereODBC = '"PRCODICE="+cp_ToStrODBC(this.w_PRCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"PRM_DETT.PRCODICE="+cp_ToStrODBC(this.w_PRCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PRM_DETT.CPROWORD '
  cPrg = "gsps_mpr"
  cComment = "Promozioni"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRCODICE = space(15)
  w_PRDESCRI = space(40)
  w_PR__NOTE = space(0)
  w_PRARTDES = space(20)
  w_PRDESPRO = space(40)
  w_PRFLESCU = space(1)
  w_PRESENTE = .F.
  o_PRESENTE = .F.
  w_PRPRIORI = 0
  w_PRMIXMAT = space(1)
  o_PRMIXMAT = space(1)
  w_PRFLNOAR = space(1)
  w_PRFLOGPR = space(1)
  o_PRFLOGPR = space(1)
  w_PRCATCOM = space(3)
  w_PRDATINI = ctod('  /  /  ')
  w_PRDATFIN = ctod('  /  /  ')
  w_PRORAINI = space(2)
  o_PRORAINI = space(2)
  w_PRMININI = space(2)
  o_PRMININI = space(2)
  w_PRORAFIN = space(2)
  o_PRORAFIN = space(2)
  w_PRMINFIN = space(2)
  o_PRMINFIN = space(2)
  w_PRGIOLUN = space(1)
  w_PRGIOMAR = space(1)
  w_PRGIOMER = space(1)
  w_PRGIOGIO = space(1)
  w_PRGIOVEN = space(1)
  w_PRGIOSAB = space(1)
  w_PRGIODOM = space(1)
  w_PRTIPVAQ = space(1)
  o_PRTIPVAQ = space(1)
  w_TSTSCP = space(1)
  w_TSTSVT = space(1)
  w_TSTSVU = space(1)
  w_TSTPUF = space(1)
  w_TSTPTF = space(1)
  w_TSTMPZ = space(1)
  w_TSTMBL = space(1)
  w_TSTMVL = space(1)
  w_TSTQTA = space(1)
  w_CPROWORD = 0
  w_PRGRUMER = space(5)
  o_PRGRUMER = space(5)
  w_PRCODART = space(20)
  w_PRPEZMIN = 0
  w_PRMINBOL = 0
  w_PRMINIMP = 0
  w_PRTIPPRO = space(1)
  o_PRTIPPRO = space(1)
  w_PRFLOMAG = space(1)
  w_DESART = space(35)
  w_PRTIPSCO = space(3)
  o_PRTIPSCO = space(3)
  w_PRTIPFID = space(3)
  w_PRTIPOMA = space(3)
  w_DESCOM = space(35)
  w_PRPERSCO = 0
  w_PRPUNEXT = 0
  w_PRVALSCU = 0
  w_PRPRUFIS = 0
  w_PRPRTFIS = 0
  w_PRARTOMA = space(20)
  o_PRARTOMA = space(20)
  w_DTOBSO = ctod('  /  /  ')
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_PRUMIOMA = space(3)
  w_FLFRAZ = space(1)
  w_PRQTOMAG = 0
  w_PRQTENNE = 0
  w_PRQTEMME = 0
  w_FLSERG = space(1)
  w_TIPART = space(2)
  w_DTOBS1 = ctod('  /  /  ')
  w_DTOBOMA = ctod('  /  /  ')
  w_TARTOMA = space(2)
  w_ARTPOS = space(1)
  w_ARTPOS1 = space(1)
  w_ARTPOSM = space(1)
  w_DTOBS1M = ctod('  /  /  ')
  w_TIPARTM = space(2)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsps_mpr
  *lancia l'hasevent per verificare che non ci siano pi� gruppi merceologici nelle righe
    Func ah_HasCPEvents(cEvent)
    	     If (Upper(This.cFunction)$"EDIT|LOAD" And Upper(cEvent)='ECPF6')
            this.NotifyEvent('HasEvent')
    			  return(.T.)
           endif
    EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRM_MAST','gsps_mpr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_mprPag1","gsps_mpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).HelpContextID = 4001333
      .Pages(2).addobject("oPag","tgsps_mprPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Attivazione")
      .Pages(2).HelpContextID = 267508385
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPRCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CATECOMM'
    this.cWorkTables[2]='GRUMERC'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='PRM_MAST'
    this.cWorkTables[6]='PRM_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRM_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRM_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PRCODICE = NVL(PRCODICE,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_4_2_joined
    link_4_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_4_48_joined
    link_4_48_joined=.f.
    local link_4_67_joined
    link_4_67_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from PRM_MAST where PRCODICE=KeySet.PRCODICE
    *
    i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2],this.bLoadRecFilter,this.PRM_MAST_IDX,"gsps_mpr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRM_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRM_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"PRM_DETT.","PRM_MAST.")
      i_cTable = i_cTable+' PRM_MAST '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_2_joined=this.AddJoinedLink_4_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_48_joined=this.AddJoinedLink_4_48(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_67_joined=this.AddJoinedLink_4_67(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRCODICE',this.w_PRCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_PRESENTE = .f.
        .w_TSTSCP = 'N'
        .w_TSTSVT = 'N'
        .w_TSTSVU = 'N'
        .w_TSTPUF = 'N'
        .w_TSTPTF = 'N'
        .w_TSTMPZ = 'N'
        .w_TSTMBL = 'N'
        .w_TSTMVL = 'N'
        .w_TSTQTA = 'N'
        .w_DESCOM = space(35)
        .w_DTOBSO = i_DATSYS
        .w_UNMIS1 = space(3)
        .w_UNMIS2 = space(3)
        .w_UNMIS3 = SPACE(3)
        .w_FLFRAZ = space(1)
        .w_FLSERG = ' '
        .w_DTOBS1 = ctod("  /  /  ")
        .w_DTOBOMA = ctod("  /  /  ")
        .w_TARTOMA = space(2)
        .w_ARTPOS1 = space(1)
        .w_ARTPOSM = space(1)
        .w_DTOBS1M = ctod("  /  /  ")
        .w_TIPARTM = space(2)
        .w_PRCODICE = NVL(PRCODICE,space(15))
        .w_PRDESCRI = NVL(PRDESCRI,space(40))
        .w_PR__NOTE = NVL(PR__NOTE,space(0))
        .w_PRARTDES = NVL(PRARTDES,space(20))
          if link_1_5_joined
            this.w_PRARTDES = NVL(ARCODART105,NVL(this.w_PRARTDES,space(20)))
            this.w_ARTPOSM = NVL(ARARTPOS105,space(1))
            this.w_DTOBS1M = NVL(cp_ToDate(ARDTOBSO105),ctod("  /  /  "))
            this.w_TIPARTM = NVL(ARTIPART105,space(2))
          else
          .link_1_5('Load')
          endif
        .w_PRDESPRO = NVL(PRDESPRO,space(40))
        .w_PRFLESCU = NVL(PRFLESCU,space(1))
        .w_PRPRIORI = NVL(PRPRIORI,0)
        .w_PRMIXMAT = NVL(PRMIXMAT,space(1))
        .w_PRFLNOAR = NVL(PRFLNOAR,space(1))
        .w_PRFLOGPR = NVL(PRFLOGPR,space(1))
        .w_PRCATCOM = NVL(PRCATCOM,space(3))
          if link_4_2_joined
            this.w_PRCATCOM = NVL(CTCODICE402,NVL(this.w_PRCATCOM,space(3)))
            this.w_DESCOM = NVL(CTDESCRI402,space(35))
          else
          .link_4_2('Load')
          endif
        .w_PRDATINI = NVL(cp_ToDate(PRDATINI),ctod("  /  /  "))
        .w_PRDATFIN = NVL(cp_ToDate(PRDATFIN),ctod("  /  /  "))
        .w_PRORAINI = NVL(PRORAINI,space(2))
        .w_PRMININI = NVL(PRMININI,space(2))
        .w_PRORAFIN = NVL(PRORAFIN,space(2))
        .w_PRMINFIN = NVL(PRMINFIN,space(2))
        .w_PRGIOLUN = NVL(PRGIOLUN,space(1))
        .w_PRGIOMAR = NVL(PRGIOMAR,space(1))
        .w_PRGIOMER = NVL(PRGIOMER,space(1))
        .w_PRGIOGIO = NVL(PRGIOGIO,space(1))
        .w_PRGIOVEN = NVL(PRGIOVEN,space(1))
        .w_PRGIOSAB = NVL(PRGIOSAB,space(1))
        .w_PRGIODOM = NVL(PRGIODOM,space(1))
        .w_PRTIPVAQ = NVL(PRTIPVAQ,space(1))
        .w_PRPEZMIN = NVL(PRPEZMIN,0)
        .w_PRMINBOL = NVL(PRMINBOL,0)
        .w_PRMINIMP = NVL(PRMINIMP,0)
        .w_PRTIPPRO = NVL(PRTIPPRO,space(1))
        .w_PRFLOMAG = NVL(PRFLOMAG,space(1))
        .w_PRTIPSCO = NVL(PRTIPSCO,space(3))
        .w_PRTIPFID = NVL(PRTIPFID,space(3))
        .w_PRTIPOMA = NVL(PRTIPOMA,space(3))
        .oPgFrm.Page2.oPag.oObj_4_40.Calculate()
        .w_PRPERSCO = NVL(PRPERSCO,0)
        .w_PRPUNEXT = NVL(PRPUNEXT,0)
        .w_PRVALSCU = NVL(PRVALSCU,0)
        .w_PRPRUFIS = NVL(PRPRUFIS,0)
        .w_PRPRTFIS = NVL(PRPRTFIS,0)
        .w_PRARTOMA = NVL(PRARTOMA,space(20))
          if link_4_48_joined
            this.w_PRARTOMA = NVL(ARCODART448,NVL(this.w_PRARTOMA,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1448,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2448,space(3))
            this.w_TARTOMA = NVL(ARTIPART448,space(2))
            this.w_DTOBOMA = NVL(cp_ToDate(ARDTOBSO448),ctod("  /  /  "))
            this.w_ARTPOS1 = NVL(ARARTPOS448,space(1))
          else
          .link_4_48('Load')
          endif
        .w_PRUMIOMA = NVL(PRUMIOMA,space(3))
          if link_4_67_joined
            this.w_PRUMIOMA = NVL(UMCODICE467,NVL(this.w_PRUMIOMA,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ467,space(1))
          else
          .link_4_67('Load')
          endif
        .w_PRQTOMAG = NVL(PRQTOMAG,0)
        .w_PRQTENNE = NVL(PRQTENNE,0)
        .w_PRQTEMME = NVL(PRQTEMME,0)
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRM_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from PRM_DETT where PRCODICE=KeySet.PRCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.PRM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRM_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('PRM_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "PRM_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" PRM_DETT"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'PRCODICE',this.w_PRCODICE  )
        select * from (i_cTable) PRM_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESART = space(35)
          .w_TIPART = space(2)
          .w_ARTPOS = space(1)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PRGRUMER = NVL(PRGRUMER,space(5))
          * evitabile
          *.link_2_2('Load')
          .w_PRCODART = NVL(PRCODART,space(20))
          if link_2_3_joined
            this.w_PRCODART = NVL(ARCODART203,NVL(this.w_PRCODART,space(20)))
            this.w_DESART = NVL(ARDESART203,space(35))
            this.w_TIPART = NVL(ARTIPART203,space(2))
            this.w_DTOBS1 = NVL(cp_ToDate(ARDTOBSO203),ctod("  /  /  "))
            this.w_ARTPOS = NVL(ARARTPOS203,space(1))
          else
          .link_2_3('Load')
          endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page2.oPag.oObj_4_40.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PRCODICE=space(15)
      .w_PRDESCRI=space(40)
      .w_PR__NOTE=space(0)
      .w_PRARTDES=space(20)
      .w_PRDESPRO=space(40)
      .w_PRFLESCU=space(1)
      .w_PRESENTE=.f.
      .w_PRPRIORI=0
      .w_PRMIXMAT=space(1)
      .w_PRFLNOAR=space(1)
      .w_PRFLOGPR=space(1)
      .w_PRCATCOM=space(3)
      .w_PRDATINI=ctod("  /  /  ")
      .w_PRDATFIN=ctod("  /  /  ")
      .w_PRORAINI=space(2)
      .w_PRMININI=space(2)
      .w_PRORAFIN=space(2)
      .w_PRMINFIN=space(2)
      .w_PRGIOLUN=space(1)
      .w_PRGIOMAR=space(1)
      .w_PRGIOMER=space(1)
      .w_PRGIOGIO=space(1)
      .w_PRGIOVEN=space(1)
      .w_PRGIOSAB=space(1)
      .w_PRGIODOM=space(1)
      .w_PRTIPVAQ=space(1)
      .w_TSTSCP=space(1)
      .w_TSTSVT=space(1)
      .w_TSTSVU=space(1)
      .w_TSTPUF=space(1)
      .w_TSTPTF=space(1)
      .w_TSTMPZ=space(1)
      .w_TSTMBL=space(1)
      .w_TSTMVL=space(1)
      .w_TSTQTA=space(1)
      .w_CPROWORD=10
      .w_PRGRUMER=space(5)
      .w_PRCODART=space(20)
      .w_PRPEZMIN=0
      .w_PRMINBOL=0
      .w_PRMINIMP=0
      .w_PRTIPPRO=space(1)
      .w_PRFLOMAG=space(1)
      .w_DESART=space(35)
      .w_PRTIPSCO=space(3)
      .w_PRTIPFID=space(3)
      .w_PRTIPOMA=space(3)
      .w_DESCOM=space(35)
      .w_PRPERSCO=0
      .w_PRPUNEXT=0
      .w_PRVALSCU=0
      .w_PRPRUFIS=0
      .w_PRPRTFIS=0
      .w_PRARTOMA=space(20)
      .w_DTOBSO=ctod("  /  /  ")
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_PRUMIOMA=space(3)
      .w_FLFRAZ=space(1)
      .w_PRQTOMAG=0
      .w_PRQTENNE=0
      .w_PRQTEMME=0
      .w_FLSERG=space(1)
      .w_TIPART=space(2)
      .w_DTOBS1=ctod("  /  /  ")
      .w_DTOBOMA=ctod("  /  /  ")
      .w_TARTOMA=space(2)
      .w_ARTPOS=space(1)
      .w_ARTPOS1=space(1)
      .w_ARTPOSM=space(1)
      .w_DTOBS1M=ctod("  /  /  ")
      .w_TIPARTM=space(2)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_PRARTDES))
         .link_1_5('Full')
        endif
        .DoRTCalc(5,5,.f.)
        .w_PRFLESCU = 'E'
        .DoRTCalc(7,9,.f.)
        .w_PRFLNOAR = IIF(.w_PRMIXMAT<>'S',' ',.w_PRFLNOAR)
        .w_PRFLOGPR = 'G'
        .w_PRCATCOM = IIF(.w_PRFLOGPR='C',.w_PRCATCOM,SPACE(3))
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_PRCATCOM))
         .link_4_2('Full')
        endif
        .DoRTCalc(13,14,.f.)
        .w_PRORAINI = IIF(EMPTY(.w_PRORAINI),'00',RIGHT('00'+.w_PRORAINI,2))
        .w_PRMININI = IIF(EMPTY(.w_PRORAINI) OR EMPTY(.w_PRMININI),'00',RIGHT('00'+.w_PRMININI,2))
        .w_PRORAFIN = IIF(EMPTY(.w_PRORAFIN),'23',RIGHT('00'+.w_PRORAFIN,2))
        .w_PRMINFIN = IIF(EMPTY(.w_PRORAFIN) OR EMPTY(.w_PRMINFIN),'59',RIGHT('00'+.w_PRMINFIN,2))
        .w_PRGIOLUN = '2'
        .w_PRGIOMAR = '3'
        .w_PRGIOMER = '4'
        .w_PRGIOGIO = '5'
        .w_PRGIOVEN = '6'
        .w_PRGIOSAB = '7'
        .w_PRGIODOM = '1'
        .w_PRTIPVAQ = 'N'
        .w_TSTSCP = 'N'
        .w_TSTSVT = 'N'
        .w_TSTSVU = 'N'
        .w_TSTPUF = 'N'
        .w_TSTPTF = 'N'
        .w_TSTMPZ = 'N'
        .w_TSTMBL = 'N'
        .w_TSTMVL = 'N'
        .w_TSTQTA = 'N'
        .DoRTCalc(36,37,.f.)
        if not(empty(.w_PRGRUMER))
         .link_2_2('Full')
        endif
        .w_PRCODART = SPACE(20)
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_PRCODART))
         .link_2_3('Full')
        endif
        .w_PRPEZMIN = 0
        .w_PRMINBOL = 0
        .w_PRMINIMP = 0
        .w_PRTIPPRO = 'S'
        .w_PRFLOMAG = 'S'
        .DoRTCalc(44,44,.f.)
        .w_PRTIPSCO = 'SPT'
        .w_PRTIPFID = 'NPF'
        .w_PRTIPOMA = 'QAF'
        .oPgFrm.Page2.oPag.oObj_4_40.Calculate()
        .DoRTCalc(48,48,.f.)
        .w_PRPERSCO = 0
        .w_PRPUNEXT = 0
        .w_PRVALSCU = 0
        .w_PRPRUFIS = 0
        .w_PRPRTFIS = 0
        .w_PRARTOMA = SPACE(20)
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_PRARTOMA))
         .link_4_48('Full')
        endif
        .w_DTOBSO = i_DATSYS
        .DoRTCalc(56,57,.f.)
        .w_UNMIS3 = SPACE(3)
        .w_PRUMIOMA = IIF(.w_PRTIPPRO='O',.w_UNMIS1,SPACE(3))
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_PRUMIOMA))
         .link_4_67('Full')
        endif
        .DoRTCalc(60,60,.f.)
        .w_PRQTOMAG = 0
        .w_PRQTENNE = 0
        .w_PRQTEMME = 0
        .w_FLSERG = ' '
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRM_MAST')
    this.DoRTCalc(65,73,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPRCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oPRDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oPR__NOTE_1_4.enabled = i_bVal
      .Page1.oPag.oPRARTDES_1_5.enabled = i_bVal
      .Page1.oPag.oPRDESPRO_1_6.enabled = i_bVal
      .Page1.oPag.oPRFLESCU_1_7.enabled = i_bVal
      .Page1.oPag.oPRPRIORI_1_9.enabled = i_bVal
      .Page1.oPag.oPRMIXMAT_1_10.enabled = i_bVal
      .Page1.oPag.oPRFLNOAR_1_11.enabled = i_bVal
      .Page2.oPag.oPRFLOGPR_4_1.enabled = i_bVal
      .Page2.oPag.oPRCATCOM_4_2.enabled = i_bVal
      .Page2.oPag.oPRDATINI_4_3.enabled = i_bVal
      .Page2.oPag.oPRDATFIN_4_4.enabled = i_bVal
      .Page2.oPag.oPRORAINI_4_5.enabled = i_bVal
      .Page2.oPag.oPRMININI_4_6.enabled = i_bVal
      .Page2.oPag.oPRORAFIN_4_7.enabled = i_bVal
      .Page2.oPag.oPRMINFIN_4_8.enabled = i_bVal
      .Page2.oPag.oPRGIOLUN_4_9.enabled = i_bVal
      .Page2.oPag.oPRGIOMAR_4_10.enabled = i_bVal
      .Page2.oPag.oPRGIOMER_4_11.enabled = i_bVal
      .Page2.oPag.oPRGIOGIO_4_12.enabled = i_bVal
      .Page2.oPag.oPRGIOVEN_4_13.enabled = i_bVal
      .Page2.oPag.oPRGIOSAB_4_14.enabled = i_bVal
      .Page2.oPag.oPRGIODOM_4_15.enabled = i_bVal
      .Page2.oPag.oPRTIPVAQ_4_16.enabled = i_bVal
      .Page2.oPag.oPRPEZMIN_4_30.enabled = i_bVal
      .Page2.oPag.oPRMINBOL_4_31.enabled = i_bVal
      .Page2.oPag.oPRMINIMP_4_32.enabled = i_bVal
      .Page2.oPag.oPRTIPPRO_4_33.enabled = i_bVal
      .Page2.oPag.oPRFLOMAG_4_34.enabled = i_bVal
      .Page2.oPag.oPRTIPSCO_4_36.enabled = i_bVal
      .Page2.oPag.oPRTIPFID_4_37.enabled = i_bVal
      .Page2.oPag.oPRTIPOMA_4_38.enabled = i_bVal
      .Page2.oPag.oPRPERSCO_4_43.enabled = i_bVal
      .Page2.oPag.oPRPUNEXT_4_44.enabled = i_bVal
      .Page2.oPag.oPRVALSCU_4_45.enabled = i_bVal
      .Page2.oPag.oPRPRUFIS_4_46.enabled = i_bVal
      .Page2.oPag.oPRPRTFIS_4_47.enabled = i_bVal
      .Page2.oPag.oPRARTOMA_4_48.enabled = i_bVal
      .Page2.oPag.oPRUMIOMA_4_67.enabled = i_bVal
      .Page2.oPag.oPRQTOMAG_4_69.enabled = i_bVal
      .Page2.oPag.oPRQTENNE_4_77.enabled = i_bVal
      .Page2.oPag.oPRQTEMME_4_80.enabled = i_bVal
      .Page2.oPag.oObj_4_40.enabled = i_bVal
      .Page2.oPag.oObj_4_86.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.enabled = i_bVal
      .Page1.oPag.oObj_2_9.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPRCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPRCODICE_1_1.enabled = .t.
        .Page1.oPag.oPRDESCRI_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PRM_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODICE,"PRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDESCRI,"PRDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PR__NOTE,"PR__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRARTDES,"PRARTDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDESPRO,"PRDESPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRFLESCU,"PRFLESCU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPRIORI,"PRPRIORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRMIXMAT,"PRMIXMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRFLNOAR,"PRFLNOAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRFLOGPR,"PRFLOGPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCATCOM,"PRCATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATINI,"PRDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATFIN,"PRDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRORAINI,"PRORAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRMININI,"PRMININI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRORAFIN,"PRORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRMINFIN,"PRMINFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOLUN,"PRGIOLUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOMAR,"PRGIOMAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOMER,"PRGIOMER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOGIO,"PRGIOGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOVEN,"PRGIOVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOSAB,"PRGIOSAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIODOM,"PRGIODOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPVAQ,"PRTIPVAQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPEZMIN,"PRPEZMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRMINBOL,"PRMINBOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRMINIMP,"PRMINIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPPRO,"PRTIPPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRFLOMAG,"PRFLOMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPSCO,"PRTIPSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPFID,"PRTIPFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPOMA,"PRTIPOMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPERSCO,"PRPERSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPUNEXT,"PRPUNEXT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRVALSCU,"PRVALSCU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPRUFIS,"PRPRUFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPRTFIS,"PRPRTFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRARTOMA,"PRARTOMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRUMIOMA,"PRUMIOMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRQTOMAG,"PRQTOMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRQTENNE,"PRQTENNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRQTEMME,"PRQTEMME",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])
    i_lTable = "PRM_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRM_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSPS_SPR with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_PRGRUMER C(5);
      ,t_PRCODART C(20);
      ,t_DESART C(35);
      ,CPROWNUM N(10);
      ,t_TIPART C(2);
      ,t_ARTPOS C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsps_mprbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRGRUMER_2_2.controlsource=this.cTrsName+'.t_PRGRUMER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODART_2_3.controlsource=this.cTrsName+'.t_PRCODART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_4.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(74)
    this.AddVLine(139)
    this.AddVLine(295)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PRM_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRM_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'PRM_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(PRCODICE,PRDESCRI,PR__NOTE,PRARTDES,PRDESPRO"+;
                  ",PRFLESCU,PRPRIORI,PRMIXMAT,PRFLNOAR,PRFLOGPR"+;
                  ",PRCATCOM,PRDATINI,PRDATFIN,PRORAINI,PRMININI"+;
                  ",PRORAFIN,PRMINFIN,PRGIOLUN,PRGIOMAR,PRGIOMER"+;
                  ",PRGIOGIO,PRGIOVEN,PRGIOSAB,PRGIODOM,PRTIPVAQ"+;
                  ",PRPEZMIN,PRMINBOL,PRMINIMP,PRTIPPRO,PRFLOMAG"+;
                  ",PRTIPSCO,PRTIPFID,PRTIPOMA,PRPERSCO,PRPUNEXT"+;
                  ",PRVALSCU,PRPRUFIS,PRPRTFIS,PRARTOMA,PRUMIOMA"+;
                  ",PRQTOMAG,PRQTENNE,PRQTEMME"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_PRCODICE)+;
                    ","+cp_ToStrODBC(this.w_PRDESCRI)+;
                    ","+cp_ToStrODBC(this.w_PR__NOTE)+;
                    ","+cp_ToStrODBCNull(this.w_PRARTDES)+;
                    ","+cp_ToStrODBC(this.w_PRDESPRO)+;
                    ","+cp_ToStrODBC(this.w_PRFLESCU)+;
                    ","+cp_ToStrODBC(this.w_PRPRIORI)+;
                    ","+cp_ToStrODBC(this.w_PRMIXMAT)+;
                    ","+cp_ToStrODBC(this.w_PRFLNOAR)+;
                    ","+cp_ToStrODBC(this.w_PRFLOGPR)+;
                    ","+cp_ToStrODBCNull(this.w_PRCATCOM)+;
                    ","+cp_ToStrODBC(this.w_PRDATINI)+;
                    ","+cp_ToStrODBC(this.w_PRDATFIN)+;
                    ","+cp_ToStrODBC(this.w_PRORAINI)+;
                    ","+cp_ToStrODBC(this.w_PRMININI)+;
                    ","+cp_ToStrODBC(this.w_PRORAFIN)+;
                    ","+cp_ToStrODBC(this.w_PRMINFIN)+;
                    ","+cp_ToStrODBC(this.w_PRGIOLUN)+;
                    ","+cp_ToStrODBC(this.w_PRGIOMAR)+;
                    ","+cp_ToStrODBC(this.w_PRGIOMER)+;
                    ","+cp_ToStrODBC(this.w_PRGIOGIO)+;
                    ","+cp_ToStrODBC(this.w_PRGIOVEN)+;
                    ","+cp_ToStrODBC(this.w_PRGIOSAB)+;
                    ","+cp_ToStrODBC(this.w_PRGIODOM)+;
                    ","+cp_ToStrODBC(this.w_PRTIPVAQ)+;
                    ","+cp_ToStrODBC(this.w_PRPEZMIN)+;
                    ","+cp_ToStrODBC(this.w_PRMINBOL)+;
                    ","+cp_ToStrODBC(this.w_PRMINIMP)+;
                    ","+cp_ToStrODBC(this.w_PRTIPPRO)+;
                    ","+cp_ToStrODBC(this.w_PRFLOMAG)+;
                    ","+cp_ToStrODBC(this.w_PRTIPSCO)+;
                    ","+cp_ToStrODBC(this.w_PRTIPFID)+;
                    ","+cp_ToStrODBC(this.w_PRTIPOMA)+;
                    ","+cp_ToStrODBC(this.w_PRPERSCO)+;
                    ","+cp_ToStrODBC(this.w_PRPUNEXT)+;
                    ","+cp_ToStrODBC(this.w_PRVALSCU)+;
                    ","+cp_ToStrODBC(this.w_PRPRUFIS)+;
                    ","+cp_ToStrODBC(this.w_PRPRTFIS)+;
                    ","+cp_ToStrODBCNull(this.w_PRARTOMA)+;
                    ","+cp_ToStrODBCNull(this.w_PRUMIOMA)+;
                    ","+cp_ToStrODBC(this.w_PRQTOMAG)+;
                    ","+cp_ToStrODBC(this.w_PRQTENNE)+;
                    ","+cp_ToStrODBC(this.w_PRQTEMME)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRM_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'PRM_MAST')
        cp_CheckDeletedKey(i_cTable,0,'PRCODICE',this.w_PRCODICE)
        INSERT INTO (i_cTable);
              (PRCODICE,PRDESCRI,PR__NOTE,PRARTDES,PRDESPRO,PRFLESCU,PRPRIORI,PRMIXMAT,PRFLNOAR,PRFLOGPR,PRCATCOM,PRDATINI,PRDATFIN,PRORAINI,PRMININI,PRORAFIN,PRMINFIN,PRGIOLUN,PRGIOMAR,PRGIOMER,PRGIOGIO,PRGIOVEN,PRGIOSAB,PRGIODOM,PRTIPVAQ,PRPEZMIN,PRMINBOL,PRMINIMP,PRTIPPRO,PRFLOMAG,PRTIPSCO,PRTIPFID,PRTIPOMA,PRPERSCO,PRPUNEXT,PRVALSCU,PRPRUFIS,PRPRTFIS,PRARTOMA,PRUMIOMA,PRQTOMAG,PRQTENNE,PRQTEMME &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_PRCODICE;
                  ,this.w_PRDESCRI;
                  ,this.w_PR__NOTE;
                  ,this.w_PRARTDES;
                  ,this.w_PRDESPRO;
                  ,this.w_PRFLESCU;
                  ,this.w_PRPRIORI;
                  ,this.w_PRMIXMAT;
                  ,this.w_PRFLNOAR;
                  ,this.w_PRFLOGPR;
                  ,this.w_PRCATCOM;
                  ,this.w_PRDATINI;
                  ,this.w_PRDATFIN;
                  ,this.w_PRORAINI;
                  ,this.w_PRMININI;
                  ,this.w_PRORAFIN;
                  ,this.w_PRMINFIN;
                  ,this.w_PRGIOLUN;
                  ,this.w_PRGIOMAR;
                  ,this.w_PRGIOMER;
                  ,this.w_PRGIOGIO;
                  ,this.w_PRGIOVEN;
                  ,this.w_PRGIOSAB;
                  ,this.w_PRGIODOM;
                  ,this.w_PRTIPVAQ;
                  ,this.w_PRPEZMIN;
                  ,this.w_PRMINBOL;
                  ,this.w_PRMINIMP;
                  ,this.w_PRTIPPRO;
                  ,this.w_PRFLOMAG;
                  ,this.w_PRTIPSCO;
                  ,this.w_PRTIPFID;
                  ,this.w_PRTIPOMA;
                  ,this.w_PRPERSCO;
                  ,this.w_PRPUNEXT;
                  ,this.w_PRVALSCU;
                  ,this.w_PRPRUFIS;
                  ,this.w_PRPRTFIS;
                  ,this.w_PRARTOMA;
                  ,this.w_PRUMIOMA;
                  ,this.w_PRQTOMAG;
                  ,this.w_PRQTENNE;
                  ,this.w_PRQTEMME;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRM_DETT_IDX,2])
      *
      * insert into PRM_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(PRCODICE,CPROWORD,PRGRUMER,PRCODART,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PRCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_PRGRUMER)+","+cp_ToStrODBCNull(this.w_PRCODART)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PRCODICE',this.w_PRCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_PRCODICE,this.w_CPROWORD,this.w_PRGRUMER,this.w_PRCODART,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update PRM_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'PRM_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " PRDESCRI="+cp_ToStrODBC(this.w_PRDESCRI)+;
             ",PR__NOTE="+cp_ToStrODBC(this.w_PR__NOTE)+;
             ",PRARTDES="+cp_ToStrODBCNull(this.w_PRARTDES)+;
             ",PRDESPRO="+cp_ToStrODBC(this.w_PRDESPRO)+;
             ",PRFLESCU="+cp_ToStrODBC(this.w_PRFLESCU)+;
             ",PRPRIORI="+cp_ToStrODBC(this.w_PRPRIORI)+;
             ",PRMIXMAT="+cp_ToStrODBC(this.w_PRMIXMAT)+;
             ",PRFLNOAR="+cp_ToStrODBC(this.w_PRFLNOAR)+;
             ",PRFLOGPR="+cp_ToStrODBC(this.w_PRFLOGPR)+;
             ",PRCATCOM="+cp_ToStrODBCNull(this.w_PRCATCOM)+;
             ",PRDATINI="+cp_ToStrODBC(this.w_PRDATINI)+;
             ",PRDATFIN="+cp_ToStrODBC(this.w_PRDATFIN)+;
             ",PRORAINI="+cp_ToStrODBC(this.w_PRORAINI)+;
             ",PRMININI="+cp_ToStrODBC(this.w_PRMININI)+;
             ",PRORAFIN="+cp_ToStrODBC(this.w_PRORAFIN)+;
             ",PRMINFIN="+cp_ToStrODBC(this.w_PRMINFIN)+;
             ",PRGIOLUN="+cp_ToStrODBC(this.w_PRGIOLUN)+;
             ",PRGIOMAR="+cp_ToStrODBC(this.w_PRGIOMAR)+;
             ",PRGIOMER="+cp_ToStrODBC(this.w_PRGIOMER)+;
             ",PRGIOGIO="+cp_ToStrODBC(this.w_PRGIOGIO)+;
             ",PRGIOVEN="+cp_ToStrODBC(this.w_PRGIOVEN)+;
             ",PRGIOSAB="+cp_ToStrODBC(this.w_PRGIOSAB)+;
             ",PRGIODOM="+cp_ToStrODBC(this.w_PRGIODOM)+;
             ",PRTIPVAQ="+cp_ToStrODBC(this.w_PRTIPVAQ)+;
             ",PRPEZMIN="+cp_ToStrODBC(this.w_PRPEZMIN)+;
             ",PRMINBOL="+cp_ToStrODBC(this.w_PRMINBOL)+;
             ",PRMINIMP="+cp_ToStrODBC(this.w_PRMINIMP)+;
             ",PRTIPPRO="+cp_ToStrODBC(this.w_PRTIPPRO)+;
             ",PRFLOMAG="+cp_ToStrODBC(this.w_PRFLOMAG)+;
             ",PRTIPSCO="+cp_ToStrODBC(this.w_PRTIPSCO)+;
             ",PRTIPFID="+cp_ToStrODBC(this.w_PRTIPFID)+;
             ",PRTIPOMA="+cp_ToStrODBC(this.w_PRTIPOMA)+;
             ",PRPERSCO="+cp_ToStrODBC(this.w_PRPERSCO)+;
             ",PRPUNEXT="+cp_ToStrODBC(this.w_PRPUNEXT)+;
             ",PRVALSCU="+cp_ToStrODBC(this.w_PRVALSCU)+;
             ",PRPRUFIS="+cp_ToStrODBC(this.w_PRPRUFIS)+;
             ",PRPRTFIS="+cp_ToStrODBC(this.w_PRPRTFIS)+;
             ",PRARTOMA="+cp_ToStrODBCNull(this.w_PRARTOMA)+;
             ",PRUMIOMA="+cp_ToStrODBCNull(this.w_PRUMIOMA)+;
             ",PRQTOMAG="+cp_ToStrODBC(this.w_PRQTOMAG)+;
             ",PRQTENNE="+cp_ToStrODBC(this.w_PRQTENNE)+;
             ",PRQTEMME="+cp_ToStrODBC(this.w_PRQTEMME)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'PRM_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'PRCODICE',this.w_PRCODICE  )
          UPDATE (i_cTable) SET;
              PRDESCRI=this.w_PRDESCRI;
             ,PR__NOTE=this.w_PR__NOTE;
             ,PRARTDES=this.w_PRARTDES;
             ,PRDESPRO=this.w_PRDESPRO;
             ,PRFLESCU=this.w_PRFLESCU;
             ,PRPRIORI=this.w_PRPRIORI;
             ,PRMIXMAT=this.w_PRMIXMAT;
             ,PRFLNOAR=this.w_PRFLNOAR;
             ,PRFLOGPR=this.w_PRFLOGPR;
             ,PRCATCOM=this.w_PRCATCOM;
             ,PRDATINI=this.w_PRDATINI;
             ,PRDATFIN=this.w_PRDATFIN;
             ,PRORAINI=this.w_PRORAINI;
             ,PRMININI=this.w_PRMININI;
             ,PRORAFIN=this.w_PRORAFIN;
             ,PRMINFIN=this.w_PRMINFIN;
             ,PRGIOLUN=this.w_PRGIOLUN;
             ,PRGIOMAR=this.w_PRGIOMAR;
             ,PRGIOMER=this.w_PRGIOMER;
             ,PRGIOGIO=this.w_PRGIOGIO;
             ,PRGIOVEN=this.w_PRGIOVEN;
             ,PRGIOSAB=this.w_PRGIOSAB;
             ,PRGIODOM=this.w_PRGIODOM;
             ,PRTIPVAQ=this.w_PRTIPVAQ;
             ,PRPEZMIN=this.w_PRPEZMIN;
             ,PRMINBOL=this.w_PRMINBOL;
             ,PRMINIMP=this.w_PRMINIMP;
             ,PRTIPPRO=this.w_PRTIPPRO;
             ,PRFLOMAG=this.w_PRFLOMAG;
             ,PRTIPSCO=this.w_PRTIPSCO;
             ,PRTIPFID=this.w_PRTIPFID;
             ,PRTIPOMA=this.w_PRTIPOMA;
             ,PRPERSCO=this.w_PRPERSCO;
             ,PRPUNEXT=this.w_PRPUNEXT;
             ,PRVALSCU=this.w_PRVALSCU;
             ,PRPRUFIS=this.w_PRPRUFIS;
             ,PRPRTFIS=this.w_PRPRTFIS;
             ,PRARTOMA=this.w_PRARTOMA;
             ,PRUMIOMA=this.w_PRUMIOMA;
             ,PRQTOMAG=this.w_PRQTOMAG;
             ,PRQTENNE=this.w_PRQTENNE;
             ,PRQTEMME=this.w_PRQTEMME;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_PRGRUMER) OR NOT EMPTY(t_PRCODART)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.PRM_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.PRM_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from PRM_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRM_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PRGRUMER="+cp_ToStrODBCNull(this.w_PRGRUMER)+;
                     ",PRCODART="+cp_ToStrODBCNull(this.w_PRCODART)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,PRGRUMER=this.w_PRGRUMER;
                     ,PRCODART=this.w_PRCODART;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsps_mpr
    * - Controllo righe piene se non attivo Tutti gli articoli
    this.NotifyEvent('Controlli')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_PRGRUMER) OR NOT EMPTY(t_PRCODART)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.PRM_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PRM_DETT_IDX,2])
        *
        * delete PRM_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])
        *
        * delete PRM_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_PRGRUMER) OR NOT EMPTY(t_PRCODART)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRM_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_PRMIXMAT<>.w_PRMIXMAT
          .w_PRFLNOAR = IIF(.w_PRMIXMAT<>'S',' ',.w_PRFLNOAR)
        endif
        .DoRTCalc(11,11,.t.)
        if .o_PRFLOGPR<>.w_PRFLOGPR
          .w_PRCATCOM = IIF(.w_PRFLOGPR='C',.w_PRCATCOM,SPACE(3))
          .link_4_2('Full')
        endif
        .DoRTCalc(13,14,.t.)
        if .o_PRORAINI<>.w_PRORAINI
          .w_PRORAINI = IIF(EMPTY(.w_PRORAINI),'00',RIGHT('00'+.w_PRORAINI,2))
        endif
        if .o_PRORAINI<>.w_PRORAINI.or. .o_PRMININI<>.w_PRMININI
          .w_PRMININI = IIF(EMPTY(.w_PRORAINI) OR EMPTY(.w_PRMININI),'00',RIGHT('00'+.w_PRMININI,2))
        endif
        if .o_PRORAFIN<>.w_PRORAFIN
          .w_PRORAFIN = IIF(EMPTY(.w_PRORAFIN),'23',RIGHT('00'+.w_PRORAFIN,2))
        endif
        if .o_PRORAFIN<>.w_PRORAFIN.or. .o_PRMINFIN<>.w_PRMINFIN
          .w_PRMINFIN = IIF(EMPTY(.w_PRORAFIN) OR EMPTY(.w_PRMINFIN),'59',RIGHT('00'+.w_PRMINFIN,2))
        endif
        .DoRTCalc(19,37,.t.)
        if .o_PRGRUMER<>.w_PRGRUMER
          .w_PRCODART = SPACE(20)
          .link_2_3('Full')
        endif
        if .o_PRTIPVAQ<>.w_PRTIPVAQ
          .w_PRPEZMIN = 0
        endif
        if .o_PRTIPVAQ<>.w_PRTIPVAQ
          .w_PRMINBOL = 0
        endif
        if .o_PRTIPVAQ<>.w_PRTIPVAQ
          .w_PRMINIMP = 0
        endif
        .DoRTCalc(42,42,.t.)
        if .o_PRTIPPRO<>.w_PRTIPPRO
          .w_PRFLOMAG = 'S'
        endif
        .oPgFrm.Page2.oPag.oObj_4_40.Calculate()
        .DoRTCalc(44,48,.t.)
        if .o_PRTIPPRO<>.w_PRTIPPRO.or. .o_PRTIPSCO<>.w_PRTIPSCO
          .w_PRPERSCO = 0
        endif
        if .o_PRTIPPRO<>.w_PRTIPPRO
          .w_PRPUNEXT = 0
        endif
        if .o_PRTIPPRO<>.w_PRTIPPRO.or. .o_PRTIPSCO<>.w_PRTIPSCO
          .w_PRVALSCU = 0
        endif
        if .o_PRTIPPRO<>.w_PRTIPPRO.or. .o_PRTIPSCO<>.w_PRTIPSCO
          .w_PRPRUFIS = 0
        endif
        if .o_PRTIPPRO<>.w_PRTIPPRO.or. .o_PRTIPSCO<>.w_PRTIPSCO
          .w_PRPRTFIS = 0
        endif
        if .o_PRTIPPRO<>.w_PRTIPPRO
          .w_PRARTOMA = SPACE(20)
          .link_4_48('Full')
        endif
        .DoRTCalc(55,58,.t.)
        if .o_PRTIPPRO<>.w_PRTIPPRO.or. .o_PRARTOMA<>.w_PRARTOMA
          .w_PRUMIOMA = IIF(.w_PRTIPPRO='O',.w_UNMIS1,SPACE(3))
          .link_4_67('Full')
        endif
        .DoRTCalc(60,60,.t.)
        if .o_PRTIPPRO<>.w_PRTIPPRO.or. .o_PRARTOMA<>.w_PRARTOMA
          .w_PRQTOMAG = 0
        endif
        if .o_PRTIPPRO<>.w_PRTIPPRO.or. .o_PRTIPSCO<>.w_PRTIPSCO
          .w_PRQTENNE = 0
        endif
        if .o_PRTIPPRO<>.w_PRTIPPRO.or. .o_PRTIPSCO<>.w_PRTIPSCO
          .w_PRQTEMME = 0
        endif
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(64,73,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TIPART with this.w_TIPART
      replace t_ARTPOS with this.w_ARTPOS
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_4_40.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_86.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPRDESPRO_1_6.enabled = this.oPgFrm.Page1.oPag.oPRDESPRO_1_6.mCond()
    this.oPgFrm.Page1.oPag.oPRMIXMAT_1_10.enabled = this.oPgFrm.Page1.oPag.oPRMIXMAT_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPRFLNOAR_1_11.enabled = this.oPgFrm.Page1.oPag.oPRFLNOAR_1_11.mCond()
    this.oPgFrm.Page2.oPag.oPRCATCOM_4_2.enabled = this.oPgFrm.Page2.oPag.oPRCATCOM_4_2.mCond()
    this.oPgFrm.Page2.oPag.oPRMININI_4_6.enabled = this.oPgFrm.Page2.oPag.oPRMININI_4_6.mCond()
    this.oPgFrm.Page2.oPag.oPRMINFIN_4_8.enabled = this.oPgFrm.Page2.oPag.oPRMINFIN_4_8.mCond()
    this.oPgFrm.Page2.oPag.oPRPEZMIN_4_30.enabled = this.oPgFrm.Page2.oPag.oPRPEZMIN_4_30.mCond()
    this.oPgFrm.Page2.oPag.oPRMINBOL_4_31.enabled = this.oPgFrm.Page2.oPag.oPRMINBOL_4_31.mCond()
    this.oPgFrm.Page2.oPag.oPRMINIMP_4_32.enabled = this.oPgFrm.Page2.oPag.oPRMINIMP_4_32.mCond()
    this.oPgFrm.Page2.oPag.oPRFLOMAG_4_34.enabled = this.oPgFrm.Page2.oPag.oPRFLOMAG_4_34.mCond()
    this.oPgFrm.Page2.oPag.oPRTIPSCO_4_36.enabled = this.oPgFrm.Page2.oPag.oPRTIPSCO_4_36.mCond()
    this.oPgFrm.Page2.oPag.oPRTIPFID_4_37.enabled = this.oPgFrm.Page2.oPag.oPRTIPFID_4_37.mCond()
    this.oPgFrm.Page2.oPag.oPRTIPOMA_4_38.enabled = this.oPgFrm.Page2.oPag.oPRTIPOMA_4_38.mCond()
    this.oPgFrm.Page2.oPag.oPRPERSCO_4_43.enabled = this.oPgFrm.Page2.oPag.oPRPERSCO_4_43.mCond()
    this.oPgFrm.Page2.oPag.oPRPUNEXT_4_44.enabled = this.oPgFrm.Page2.oPag.oPRPUNEXT_4_44.mCond()
    this.oPgFrm.Page2.oPag.oPRVALSCU_4_45.enabled = this.oPgFrm.Page2.oPag.oPRVALSCU_4_45.mCond()
    this.oPgFrm.Page2.oPag.oPRPRUFIS_4_46.enabled = this.oPgFrm.Page2.oPag.oPRPRUFIS_4_46.mCond()
    this.oPgFrm.Page2.oPag.oPRPRTFIS_4_47.enabled = this.oPgFrm.Page2.oPag.oPRPRTFIS_4_47.mCond()
    this.oPgFrm.Page2.oPag.oPRARTOMA_4_48.enabled = this.oPgFrm.Page2.oPag.oPRARTOMA_4_48.mCond()
    this.oPgFrm.Page2.oPag.oPRUMIOMA_4_67.enabled = this.oPgFrm.Page2.oPag.oPRUMIOMA_4_67.mCond()
    this.oPgFrm.Page2.oPag.oPRQTOMAG_4_69.enabled = this.oPgFrm.Page2.oPag.oPRQTOMAG_4_69.mCond()
    this.oPgFrm.Page2.oPag.oPRQTENNE_4_77.enabled = this.oPgFrm.Page2.oPag.oPRQTENNE_4_77.mCond()
    this.oPgFrm.Page2.oPag.oPRQTEMME_4_80.enabled = this.oPgFrm.Page2.oPag.oPRQTEMME_4_80.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRGRUMER_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRGRUMER_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRCODART_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRCODART_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPRFLNOAR_1_11.visible=!this.oPgFrm.Page1.oPag.oPRFLNOAR_1_11.mHide()
    this.oPgFrm.Page2.oPag.oPRPEZMIN_4_30.visible=!this.oPgFrm.Page2.oPag.oPRPEZMIN_4_30.mHide()
    this.oPgFrm.Page2.oPag.oPRMINBOL_4_31.visible=!this.oPgFrm.Page2.oPag.oPRMINBOL_4_31.mHide()
    this.oPgFrm.Page2.oPag.oPRMINIMP_4_32.visible=!this.oPgFrm.Page2.oPag.oPRMINIMP_4_32.mHide()
    this.oPgFrm.Page2.oPag.oPRFLOMAG_4_34.visible=!this.oPgFrm.Page2.oPag.oPRFLOMAG_4_34.mHide()
    this.oPgFrm.Page2.oPag.oPRTIPSCO_4_36.visible=!this.oPgFrm.Page2.oPag.oPRTIPSCO_4_36.mHide()
    this.oPgFrm.Page2.oPag.oPRTIPFID_4_37.visible=!this.oPgFrm.Page2.oPag.oPRTIPFID_4_37.mHide()
    this.oPgFrm.Page2.oPag.oPRTIPOMA_4_38.visible=!this.oPgFrm.Page2.oPag.oPRTIPOMA_4_38.mHide()
    this.oPgFrm.Page2.oPag.oPRPERSCO_4_43.visible=!this.oPgFrm.Page2.oPag.oPRPERSCO_4_43.mHide()
    this.oPgFrm.Page2.oPag.oPRPUNEXT_4_44.visible=!this.oPgFrm.Page2.oPag.oPRPUNEXT_4_44.mHide()
    this.oPgFrm.Page2.oPag.oPRVALSCU_4_45.visible=!this.oPgFrm.Page2.oPag.oPRVALSCU_4_45.mHide()
    this.oPgFrm.Page2.oPag.oPRPRUFIS_4_46.visible=!this.oPgFrm.Page2.oPag.oPRPRUFIS_4_46.mHide()
    this.oPgFrm.Page2.oPag.oPRPRTFIS_4_47.visible=!this.oPgFrm.Page2.oPag.oPRPRTFIS_4_47.mHide()
    this.oPgFrm.Page2.oPag.oPRARTOMA_4_48.visible=!this.oPgFrm.Page2.oPag.oPRARTOMA_4_48.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_55.visible=!this.oPgFrm.Page2.oPag.oStr_4_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_56.visible=!this.oPgFrm.Page2.oPag.oStr_4_56.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_57.visible=!this.oPgFrm.Page2.oPag.oStr_4_57.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_58.visible=!this.oPgFrm.Page2.oPag.oStr_4_58.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_59.visible=!this.oPgFrm.Page2.oPag.oStr_4_59.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_60.visible=!this.oPgFrm.Page2.oPag.oStr_4_60.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_61.visible=!this.oPgFrm.Page2.oPag.oStr_4_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_62.visible=!this.oPgFrm.Page2.oPag.oStr_4_62.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_63.visible=!this.oPgFrm.Page2.oPag.oStr_4_63.mHide()
    this.oPgFrm.Page2.oPag.oPRUMIOMA_4_67.visible=!this.oPgFrm.Page2.oPag.oPRUMIOMA_4_67.mHide()
    this.oPgFrm.Page2.oPag.oPRQTOMAG_4_69.visible=!this.oPgFrm.Page2.oPag.oPRQTOMAG_4_69.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_70.visible=!this.oPgFrm.Page2.oPag.oStr_4_70.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_72.visible=!this.oPgFrm.Page2.oPag.oStr_4_72.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_73.visible=!this.oPgFrm.Page2.oPag.oStr_4_73.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_74.visible=!this.oPgFrm.Page2.oPag.oStr_4_74.mHide()
    this.oPgFrm.Page2.oPag.oPRQTENNE_4_77.visible=!this.oPgFrm.Page2.oPag.oPRQTENNE_4_77.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_78.visible=!this.oPgFrm.Page2.oPag.oStr_4_78.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_79.visible=!this.oPgFrm.Page2.oPag.oStr_4_79.mHide()
    this.oPgFrm.Page2.oPag.oPRQTEMME_4_80.visible=!this.oPgFrm.Page2.oPag.oPRQTEMME_4_80.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_87.visible=!this.oPgFrm.Page2.oPag.oStr_4_87.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_4_40.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_86.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRARTDES
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRARTDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_PRARTDES)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARARTPOS,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_PRARTDES))
          select ARCODART,ARARTPOS,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRARTDES)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRARTDES) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oPRARTDES_1_5'),i_cWhere,'',"Servizi descrittivi",'GSPS_ADP.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARARTPOS,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARARTPOS,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRARTDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARARTPOS,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PRARTDES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PRARTDES)
            select ARCODART,ARARTPOS,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRARTDES = NVL(_Link_.ARCODART,space(20))
      this.w_ARTPOSM = NVL(_Link_.ARARTPOS,space(1))
      this.w_DTOBS1M = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPARTM = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PRARTDES = space(20)
      endif
      this.w_ARTPOSM = space(1)
      this.w_DTOBS1M = ctod("  /  /  ")
      this.w_TIPARTM = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPARTM='DE' And  (.w_DTOBS1M>.w_DTOBSO OR EMPTY(.w_DTOBS1M)) AND .w_ARTPOSM='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Impossibile selezionare articoli non pos, obsoleti o non descrittivii")
        endif
        this.w_PRARTDES = space(20)
        this.w_ARTPOSM = space(1)
        this.w_DTOBS1M = ctod("  /  /  ")
        this.w_TIPARTM = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRARTDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ARCODART as ARCODART105"+ ",link_1_5.ARARTPOS as ARARTPOS105"+ ",link_1_5.ARDTOBSO as ARDTOBSO105"+ ",link_1_5.ARTIPART as ARTIPART105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on PRM_MAST.PRARTDES=link_1_5.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and PRM_MAST.PRARTDES=link_1_5.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCATCOM
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_PRCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_PRCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oPRCATCOM_4_2'),i_cWhere,'',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_PRCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_PRCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCOM = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PRCATCOM = space(3)
      endif
      this.w_DESCOM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATECOMM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_2.CTCODICE as CTCODICE402"+ ",link_4_2.CTDESCRI as CTDESCRI402"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_2 on PRM_MAST.PRCATCOM=link_4_2.CTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_2"
          i_cKey=i_cKey+'+" and PRM_MAST.PRCATCOM=link_4_2.CTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRGRUMER
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_PRGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_PRGRUMER))
          select GMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oPRGRUMER_2_2'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_PRGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_PRGRUMER)
            select GMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRGRUMER = NVL(_Link_.GMCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PRGRUMER = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PRFLNOAR<>'S' AND .w_PRMIXMAT='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Se � attivo il check tutti gli articoli non devono essere inserite righe")
        endif
        this.w_PRGRUMER = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRCODART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_PRCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARARTPOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_PRCODART))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARARTPOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oPRCODART_2_3'),i_cWhere,'',"Articoli",'GSPS_APR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARARTPOS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARARTPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARARTPOS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PRCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PRCODART)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARARTPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(35))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_ARTPOS = NVL(_Link_.ARARTPOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODART = space(20)
      endif
      this.w_DESART = space(35)
      this.w_TIPART = space(2)
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_ARTPOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS' AND (.w_DTOBS1>.w_DTOBSO OR EMPTY(.w_DTOBS1)) AND .w_ARTPOS='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Impossibile selezionare articoli non pos, obsoleti o servizi")
        endif
        this.w_PRCODART = space(20)
        this.w_DESART = space(35)
        this.w_TIPART = space(2)
        this.w_DTOBS1 = ctod("  /  /  ")
        this.w_ARTPOS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ARCODART as ARCODART203"+ ",link_2_3.ARDESART as ARDESART203"+ ",link_2_3.ARTIPART as ARTIPART203"+ ",link_2_3.ARDTOBSO as ARDTOBSO203"+ ",link_2_3.ARARTPOS as ARARTPOS203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on PRM_DETT.PRCODART=link_2_3.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and PRM_DETT.PRCODART=link_2_3.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRARTOMA
  func Link_4_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRARTOMA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_PRARTOMA)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,ARTIPART,ARDTOBSO,ARARTPOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_PRARTOMA))
          select ARCODART,ARUNMIS1,ARUNMIS2,ARTIPART,ARDTOBSO,ARARTPOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRARTOMA)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRARTOMA) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oPRARTOMA_4_48'),i_cWhere,'',"Articoli",'GSPS_APR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,ARTIPART,ARDTOBSO,ARARTPOS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARUNMIS1,ARUNMIS2,ARTIPART,ARDTOBSO,ARARTPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRARTOMA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,ARTIPART,ARDTOBSO,ARARTPOS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PRARTOMA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PRARTOMA)
            select ARCODART,ARUNMIS1,ARUNMIS2,ARTIPART,ARDTOBSO,ARARTPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRARTOMA = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_TARTOMA = NVL(_Link_.ARTIPART,space(2))
      this.w_DTOBOMA = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_ARTPOS1 = NVL(_Link_.ARARTPOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PRARTOMA = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_TARTOMA = space(2)
      this.w_DTOBOMA = ctod("  /  /  ")
      this.w_ARTPOS1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TARTOMA<>'DE' AND (.w_DTOBOMA>.w_DTOBSO OR EMPTY(.w_DTOBOMA)) AND .w_ARTPOS1='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Impossibile selezionare articoli non pos, obsoleti o descrittivi")
        endif
        this.w_PRARTOMA = space(20)
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_TARTOMA = space(2)
        this.w_DTOBOMA = ctod("  /  /  ")
        this.w_ARTPOS1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRARTOMA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_48(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_48.ARCODART as ARCODART448"+ ",link_4_48.ARUNMIS1 as ARUNMIS1448"+ ",link_4_48.ARUNMIS2 as ARUNMIS2448"+ ",link_4_48.ARTIPART as ARTIPART448"+ ",link_4_48.ARDTOBSO as ARDTOBSO448"+ ",link_4_48.ARARTPOS as ARARTPOS448"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_48 on PRM_MAST.PRARTOMA=link_4_48.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_48"
          i_cKey=i_cKey+'+" and PRM_MAST.PRARTOMA=link_4_48.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRUMIOMA
  func Link_4_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRUMIOMA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_PRUMIOMA)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_PRUMIOMA))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRUMIOMA)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRUMIOMA) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oPRUMIOMA_4_67'),i_cWhere,'',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRUMIOMA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PRUMIOMA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PRUMIOMA)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRUMIOMA = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PRUMIOMA = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(IIF(.w_PRUMIOMA<>'   ','***',.w_PRUMIOMA), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_PRUMIOMA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura incongruente per l'articolo selezionato")
        endif
        this.w_PRUMIOMA = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRUMIOMA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_67(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_67.UMCODICE as UMCODICE467"+ ",link_4_67.UMFLFRAZ as UMFLFRAZ467"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_67 on PRM_MAST.PRUMIOMA=link_4_67.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_67"
          i_cKey=i_cKey+'+" and PRM_MAST.PRUMIOMA=link_4_67.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPRCODICE_1_1.value==this.w_PRCODICE)
      this.oPgFrm.Page1.oPag.oPRCODICE_1_1.value=this.w_PRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESCRI_1_2.value==this.w_PRDESCRI)
      this.oPgFrm.Page1.oPag.oPRDESCRI_1_2.value=this.w_PRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPR__NOTE_1_4.value==this.w_PR__NOTE)
      this.oPgFrm.Page1.oPag.oPR__NOTE_1_4.value=this.w_PR__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPRARTDES_1_5.value==this.w_PRARTDES)
      this.oPgFrm.Page1.oPag.oPRARTDES_1_5.value=this.w_PRARTDES
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESPRO_1_6.value==this.w_PRDESPRO)
      this.oPgFrm.Page1.oPag.oPRDESPRO_1_6.value=this.w_PRDESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPRFLESCU_1_7.RadioValue()==this.w_PRFLESCU)
      this.oPgFrm.Page1.oPag.oPRFLESCU_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPRIORI_1_9.value==this.w_PRPRIORI)
      this.oPgFrm.Page1.oPag.oPRPRIORI_1_9.value=this.w_PRPRIORI
    endif
    if not(this.oPgFrm.Page1.oPag.oPRMIXMAT_1_10.RadioValue()==this.w_PRMIXMAT)
      this.oPgFrm.Page1.oPag.oPRMIXMAT_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRFLNOAR_1_11.RadioValue()==this.w_PRFLNOAR)
      this.oPgFrm.Page1.oPag.oPRFLNOAR_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRFLOGPR_4_1.RadioValue()==this.w_PRFLOGPR)
      this.oPgFrm.Page2.oPag.oPRFLOGPR_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRCATCOM_4_2.value==this.w_PRCATCOM)
      this.oPgFrm.Page2.oPag.oPRCATCOM_4_2.value=this.w_PRCATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oPRDATINI_4_3.value==this.w_PRDATINI)
      this.oPgFrm.Page2.oPag.oPRDATINI_4_3.value=this.w_PRDATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPRDATFIN_4_4.value==this.w_PRDATFIN)
      this.oPgFrm.Page2.oPag.oPRDATFIN_4_4.value=this.w_PRDATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPRORAINI_4_5.value==this.w_PRORAINI)
      this.oPgFrm.Page2.oPag.oPRORAINI_4_5.value=this.w_PRORAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPRMININI_4_6.value==this.w_PRMININI)
      this.oPgFrm.Page2.oPag.oPRMININI_4_6.value=this.w_PRMININI
    endif
    if not(this.oPgFrm.Page2.oPag.oPRORAFIN_4_7.value==this.w_PRORAFIN)
      this.oPgFrm.Page2.oPag.oPRORAFIN_4_7.value=this.w_PRORAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPRMINFIN_4_8.value==this.w_PRMINFIN)
      this.oPgFrm.Page2.oPag.oPRMINFIN_4_8.value=this.w_PRMINFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPRGIOLUN_4_9.RadioValue()==this.w_PRGIOLUN)
      this.oPgFrm.Page2.oPag.oPRGIOLUN_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRGIOMAR_4_10.RadioValue()==this.w_PRGIOMAR)
      this.oPgFrm.Page2.oPag.oPRGIOMAR_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRGIOMER_4_11.RadioValue()==this.w_PRGIOMER)
      this.oPgFrm.Page2.oPag.oPRGIOMER_4_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRGIOGIO_4_12.RadioValue()==this.w_PRGIOGIO)
      this.oPgFrm.Page2.oPag.oPRGIOGIO_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRGIOVEN_4_13.RadioValue()==this.w_PRGIOVEN)
      this.oPgFrm.Page2.oPag.oPRGIOVEN_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRGIOSAB_4_14.RadioValue()==this.w_PRGIOSAB)
      this.oPgFrm.Page2.oPag.oPRGIOSAB_4_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRGIODOM_4_15.RadioValue()==this.w_PRGIODOM)
      this.oPgFrm.Page2.oPag.oPRGIODOM_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRTIPVAQ_4_16.RadioValue()==this.w_PRTIPVAQ)
      this.oPgFrm.Page2.oPag.oPRTIPVAQ_4_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRPEZMIN_4_30.value==this.w_PRPEZMIN)
      this.oPgFrm.Page2.oPag.oPRPEZMIN_4_30.value=this.w_PRPEZMIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPRMINBOL_4_31.value==this.w_PRMINBOL)
      this.oPgFrm.Page2.oPag.oPRMINBOL_4_31.value=this.w_PRMINBOL
    endif
    if not(this.oPgFrm.Page2.oPag.oPRMINIMP_4_32.value==this.w_PRMINIMP)
      this.oPgFrm.Page2.oPag.oPRMINIMP_4_32.value=this.w_PRMINIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oPRTIPPRO_4_33.RadioValue()==this.w_PRTIPPRO)
      this.oPgFrm.Page2.oPag.oPRTIPPRO_4_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRFLOMAG_4_34.RadioValue()==this.w_PRFLOMAG)
      this.oPgFrm.Page2.oPag.oPRFLOMAG_4_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRTIPSCO_4_36.RadioValue()==this.w_PRTIPSCO)
      this.oPgFrm.Page2.oPag.oPRTIPSCO_4_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRTIPFID_4_37.RadioValue()==this.w_PRTIPFID)
      this.oPgFrm.Page2.oPag.oPRTIPFID_4_37.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRTIPOMA_4_38.RadioValue()==this.w_PRTIPOMA)
      this.oPgFrm.Page2.oPag.oPRTIPOMA_4_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_4_42.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_4_42.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oPRPERSCO_4_43.value==this.w_PRPERSCO)
      this.oPgFrm.Page2.oPag.oPRPERSCO_4_43.value=this.w_PRPERSCO
    endif
    if not(this.oPgFrm.Page2.oPag.oPRPUNEXT_4_44.value==this.w_PRPUNEXT)
      this.oPgFrm.Page2.oPag.oPRPUNEXT_4_44.value=this.w_PRPUNEXT
    endif
    if not(this.oPgFrm.Page2.oPag.oPRVALSCU_4_45.value==this.w_PRVALSCU)
      this.oPgFrm.Page2.oPag.oPRVALSCU_4_45.value=this.w_PRVALSCU
    endif
    if not(this.oPgFrm.Page2.oPag.oPRPRUFIS_4_46.value==this.w_PRPRUFIS)
      this.oPgFrm.Page2.oPag.oPRPRUFIS_4_46.value=this.w_PRPRUFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oPRPRTFIS_4_47.value==this.w_PRPRTFIS)
      this.oPgFrm.Page2.oPag.oPRPRTFIS_4_47.value=this.w_PRPRTFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oPRARTOMA_4_48.value==this.w_PRARTOMA)
      this.oPgFrm.Page2.oPag.oPRARTOMA_4_48.value=this.w_PRARTOMA
    endif
    if not(this.oPgFrm.Page2.oPag.oPRUMIOMA_4_67.value==this.w_PRUMIOMA)
      this.oPgFrm.Page2.oPag.oPRUMIOMA_4_67.value=this.w_PRUMIOMA
    endif
    if not(this.oPgFrm.Page2.oPag.oPRQTOMAG_4_69.value==this.w_PRQTOMAG)
      this.oPgFrm.Page2.oPag.oPRQTOMAG_4_69.value=this.w_PRQTOMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oPRQTENNE_4_77.value==this.w_PRQTENNE)
      this.oPgFrm.Page2.oPag.oPRQTENNE_4_77.value=this.w_PRQTENNE
    endif
    if not(this.oPgFrm.Page2.oPag.oPRQTEMME_4_80.value==this.w_PRQTEMME)
      this.oPgFrm.Page2.oPag.oPRQTEMME_4_80.value=this.w_PRQTEMME
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRGRUMER_2_2.value==this.w_PRGRUMER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRGRUMER_2_2.value=this.w_PRGRUMER
      replace t_PRGRUMER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRGRUMER_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODART_2_3.value==this.w_PRCODART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODART_2_3.value=this.w_PRCODART
      replace t_PRCODART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODART_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_4.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_4.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'PRM_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PRCODICE) or not(LEFT(.w_PRCODICE,9)<>'#########'))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPRCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PRCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso")
          case   not(.w_TIPARTM='DE' And  (.w_DTOBS1M>.w_DTOBSO OR EMPTY(.w_DTOBS1M)) AND .w_ARTPOSM='S')  and not(empty(.w_PRARTDES))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPRARTDES_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile selezionare articoli non pos, obsoleti o non descrittivii")
          case   not(VAL(.w_PRORAINI)<24 And Not Empty( .w_PRORAINI  ))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRORAINI_4_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora inizio validit� non corretta specificare da 00 a 23")
          case   not(VAL(.w_PRMININI)<60)  and (NOT EMPTY(.w_PRORAINI) AND .cFunction <>'Query')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRMININI_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti inizio validit� non corretti, specificare da 00 a 59")
          case   not(VAL(.w_PRORAFIN)<24 And Not Empty(.w_PRORAFIN))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRORAFIN_4_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora fine validit� non corretta, specificare da 00 a 23")
          case   not(VAL(.w_PRMINFIN)<60)  and (NOT EMPTY(.w_PRORAFIN) AND .cFunction <>'Query')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRMINFIN_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuti fine validit� non corretti, specificare da 00 a 59")
          case   (empty(.w_PRPEZMIN))  and not(.w_PRTIPVAQ<>'Q')  and (.cFunction<>'Query' AND .w_PRTIPVAQ='Q')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRPEZMIN_4_30.SetFocus()
            i_bnoObbl = !empty(.w_PRPEZMIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRMINBOL))  and not(.w_PRTIPVAQ<>'B')  and (.cFunction<>'Query' AND .w_PRTIPVAQ='B')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRMINBOL_4_31.SetFocus()
            i_bnoObbl = !empty(.w_PRMINBOL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRMINIMP) or not(.w_PRMINIMP>0))  and not(.w_PRTIPVAQ<>'V')  and (.cFunction<>'Query' AND .w_PRTIPVAQ='V')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRMINIMP_4_32.SetFocus()
            i_bnoObbl = !empty(.w_PRMINIMP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare valori positivi")
          case   (empty(.w_PRPERSCO))  and not(.w_PRTIPPRO<>'S' OR .w_TSTSCP<>'S')  and (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_TSTSCP='S')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRPERSCO_4_43.SetFocus()
            i_bnoObbl = !empty(.w_PRPERSCO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRPUNEXT))  and not(.w_PRTIPPRO<>'F')  and (.cFunction<>'Query' AND .w_PRTIPPRO='F')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRPUNEXT_4_44.SetFocus()
            i_bnoObbl = !empty(.w_PRPUNEXT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRVALSCU) or not(.w_PRVALSCU>0))  and not(.w_PRTIPPRO<>'S' OR .w_TSTSVU<>'S')  and (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_TSTSVU='S')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRVALSCU_4_45.SetFocus()
            i_bnoObbl = !empty(.w_PRVALSCU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare valori positivi")
          case   (empty(.w_PRPRUFIS) or not(.w_PRPRUFIS>0))  and not(.w_PRTIPPRO<>'S' OR .w_TSTPUF<>'S')  and (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_TSTPUF='S')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRPRUFIS_4_46.SetFocus()
            i_bnoObbl = !empty(.w_PRPRUFIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare valori positivi")
          case   (empty(.w_PRPRTFIS) or not(.w_PRPRTFIS>0))  and not(.w_PRTIPPRO<>'S' OR .w_TSTPTF<>'S')  and (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_TSTPTF='S')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRPRTFIS_4_47.SetFocus()
            i_bnoObbl = !empty(.w_PRPRTFIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare valori positivi")
          case   (empty(.w_PRARTOMA) or not(.w_TARTOMA<>'DE' AND (.w_DTOBOMA>.w_DTOBSO OR EMPTY(.w_DTOBOMA)) AND .w_ARTPOS1='S'))  and not(.w_PRTIPPRO<>'O')  and (.cFunction<>'Query' AND .w_PRTIPPRO='O')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRARTOMA_4_48.SetFocus()
            i_bnoObbl = !empty(.w_PRARTOMA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile selezionare articoli non pos, obsoleti o descrittivi")
          case   (empty(.w_PRUMIOMA) or not(CHKUNIMI(IIF(.w_PRUMIOMA<>'   ','***',.w_PRUMIOMA), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_PRUMIOMA)))  and not(.w_PRTIPPRO<>'O')  and (.cFunction<>'Query' AND .w_PRTIPPRO='O' AND NOT EMPTY(.w_PRARTOMA))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRUMIOMA_4_67.SetFocus()
            i_bnoObbl = !empty(.w_PRUMIOMA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� di misura incongruente per l'articolo selezionato")
          case   not((.w_PRQTOMAG>0 AND (.w_FLFRAZ<>'S' OR .w_PRQTOMAG=INT(.w_PRQTOMAG))))  and not(.w_PRTIPPRO<>'O')  and (.cFunction<>'Query' AND .w_PRTIPPRO='O' AND NOT EMPTY(.w_PRARTOMA))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRQTOMAG_4_69.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Quantit� non frazionabile")
          case   (empty(.w_PRQTENNE) or not(.w_PRQTENNE>.w_PRQTEMME))  and not(.w_PRTIPPRO<>'S' OR .w_PRTIPSCO<>'NXM')  and (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_PRTIPSCO='NXM')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRQTENNE_4_77.SetFocus()
            i_bnoObbl = !empty(.w_PRQTENNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRQTEMME) or not(.w_PRQTENNE>.w_PRQTEMME))  and not(.w_PRTIPPRO<>'S' OR .w_PRTIPSCO<>'NXM')  and (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_PRTIPSCO='NXM')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPRQTEMME_4_80.SetFocus()
            i_bnoObbl = !empty(.w_PRQTEMME)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_mpr
      * - Controllo Quantit� N * M
      IF .w_PRTIPPRO='S' AND .w_PRTIPSCO='NXM' AND (.w_PRQTENNE=0 OR .w_PRQTEMME=0)
         i_cErrorMsg = Ah_MsgFormat("Impostare quantit� N e M")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_PRFLNOAR<>'S' AND .w_PRMIXMAT='S') and (.w_PRMIXMAT='S') and not(empty(.w_PRGRUMER)) and (NOT EMPTY(.w_PRGRUMER) OR NOT EMPTY(.w_PRCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRGRUMER_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Se � attivo il check tutti gli articoli non devono essere inserite righe")
        case   not(.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS' AND (.w_DTOBS1>.w_DTOBSO OR EMPTY(.w_DTOBS1)) AND .w_ARTPOS='S') and (EMPTY(.w_PRGRUMER)) and not(empty(.w_PRCODART)) and (NOT EMPTY(.w_PRGRUMER) OR NOT EMPTY(.w_PRCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODART_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Impossibile selezionare articoli non pos, obsoleti o servizi")
      endcase
      if NOT EMPTY(.w_PRGRUMER) OR NOT EMPTY(.w_PRCODART)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PRESENTE = this.w_PRESENTE
    this.o_PRMIXMAT = this.w_PRMIXMAT
    this.o_PRFLOGPR = this.w_PRFLOGPR
    this.o_PRORAINI = this.w_PRORAINI
    this.o_PRMININI = this.w_PRMININI
    this.o_PRORAFIN = this.w_PRORAFIN
    this.o_PRMINFIN = this.w_PRMINFIN
    this.o_PRTIPVAQ = this.w_PRTIPVAQ
    this.o_PRGRUMER = this.w_PRGRUMER
    this.o_PRTIPPRO = this.w_PRTIPPRO
    this.o_PRTIPSCO = this.w_PRTIPSCO
    this.o_PRARTOMA = this.w_PRARTOMA
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_PRGRUMER) OR NOT EMPTY(t_PRCODART))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_PRGRUMER=space(5)
      .w_PRCODART=space(20)
      .w_DESART=space(35)
      .w_TIPART=space(2)
      .w_ARTPOS=space(1)
      .DoRTCalc(1,37,.f.)
      if not(empty(.w_PRGRUMER))
        .link_2_2('Full')
      endif
        .w_PRCODART = SPACE(20)
      .DoRTCalc(38,38,.f.)
      if not(empty(.w_PRCODART))
        .link_2_3('Full')
      endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
    endwith
    this.DoRTCalc(39,73,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_PRGRUMER = t_PRGRUMER
    this.w_PRCODART = t_PRCODART
    this.w_DESART = t_DESART
    this.w_TIPART = t_TIPART
    this.w_ARTPOS = t_ARTPOS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PRGRUMER with this.w_PRGRUMER
    replace t_PRCODART with this.w_PRCODART
    replace t_DESART with this.w_DESART
    replace t_TIPART with this.w_TIPART
    replace t_ARTPOS with this.w_ARTPOS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsps_mprPag1 as StdContainer
  Width  = 583
  height = 376
  stdWidth  = 583
  stdheight = 376
  resizeXpos=418
  resizeYpos=297
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRCODICE_1_1 as StdField with uid="JGMSWVNSZC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PRCODICE", cQueryName = "PRCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Codice promozione",;
    HelpContextID = 80359227,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=115, Top=14, InputMask=replicate('X',15)

  func oPRCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (LEFT(.w_PRCODICE,9)<>'#########')
    endwith
    return bRes
  endfunc

  add object oPRDESCRI_1_2 as StdField with uid="KSDVBTOLFB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PRDESCRI", cQueryName = "PRDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione promozione",;
    HelpContextID = 5226689,;
   bGlobalFont=.t.,;
    Height=21, Width=329, Left=237, Top=14, InputMask=replicate('X',40)

  add object oPR__NOTE_1_4 as StdMemo with uid="ORZOTDTHRX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PR__NOTE", cQueryName = "PR__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive",;
    HelpContextID = 192671547,;
   bGlobalFont=.t.,;
    Height=44, Width=451, Left=115, Top=42

  add object oPRARTDES_1_5 as StdField with uid="CZGKRMNOTA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PRARTDES", cQueryName = "PRARTDES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile selezionare articoli non pos, obsoleti o non descrittivii",;
    ToolTipText = "Servizio descrittivo per indicazione della promozione sulle vendite",;
    HelpContextID = 13438793,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=115, Top=93, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_PRARTDES"

  func oPRARTDES_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRARTDES_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRARTDES_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oPRARTDES_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Servizi descrittivi",'GSPS_ADP.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oPRDESPRO_1_6 as StdField with uid="XAZWJHUOMD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PRDESPRO", cQueryName = "PRDESPRO",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione che apparir� nelle vendite per indicare la promozione",;
    HelpContextID = 212877125,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=273, Top=93, InputMask=replicate('X',40)

  func oPRDESPRO_1_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PRARTDES) AND .cFunction<>'Query')
    endwith
  endfunc


  add object oPRFLESCU_1_7 as StdCombo with uid="QXYNFCUDRH",rtseq=6,rtrep=.f.,left=115,top=131,width=111,height=21;
    , ToolTipText = "Definisce la tipologia di attivazione della promozione";
    , HelpContextID = 248995659;
    , cFormVar="w_PRFLESCU",RowSource=""+"Esclusiva,"+"Cumulativa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRFLESCU_1_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRFLESCU,&i_cF..t_PRFLESCU),this.value)
    return(iif(xVal =1,'E',;
    iif(xVal =2,'C',;
    space(1))))
  endfunc
  func oPRFLESCU_1_7.GetRadio()
    this.Parent.oContained.w_PRFLESCU = this.RadioValue()
    return .t.
  endfunc

  func oPRFLESCU_1_7.ToRadio()
    this.Parent.oContained.w_PRFLESCU=trim(this.Parent.oContained.w_PRFLESCU)
    return(;
      iif(this.Parent.oContained.w_PRFLESCU=='E',1,;
      iif(this.Parent.oContained.w_PRFLESCU=='C',2,;
      0)))
  endfunc

  func oPRFLESCU_1_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRPRIORI_1_9 as StdField with uid="OWLKWGCUJZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PRPRIORI", cQueryName = "PRPRIORI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Livello priorit� promozione: indica anche l'ordine di applicazione",;
    HelpContextID = 186515263,;
   bGlobalFont=.t.,;
    Height=21, Width=42, Left=286, Top=131, cSayPict='"999"', cGetPict='"999"'

  add object oPRMIXMAT_1_10 as StdCheck with uid="YIRWUQVGKW",rtseq=9,rtrep=.f.,left=343, top=131, caption="Mix match",;
    ToolTipText = "Gli articoli presenti nella promozione vengono considerati come un paniere - il check � ineditabile se sono presenti gr.merceologici",;
    HelpContextID = 100348086,;
    cFormVar="w_PRMIXMAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRMIXMAT_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRMIXMAT,&i_cF..t_PRMIXMAT),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oPRMIXMAT_1_10.GetRadio()
    this.Parent.oContained.w_PRMIXMAT = this.RadioValue()
    return .t.
  endfunc

  func oPRMIXMAT_1_10.ToRadio()
    this.Parent.oContained.w_PRMIXMAT=trim(this.Parent.oContained.w_PRMIXMAT)
    return(;
      iif(this.Parent.oContained.w_PRMIXMAT=='S',1,;
      0))
  endfunc

  func oPRMIXMAT_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPRMIXMAT_1_10.mCond()
    with this.Parent.oContained
      return (.w_PRESENTE=.F.)
    endwith
  endfunc

  add object oPRFLNOAR_1_11 as StdCheck with uid="KRAFEJOGXB",rtseq=10,rtrep=.f.,left=449, top=131, caption="Tutti gli articoli",;
    ToolTipText = "Attivo: la promozione vale per tutti gli articoli della vendita",;
    HelpContextID = 77111480,;
    cFormVar="w_PRFLNOAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRFLNOAR_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRFLNOAR,&i_cF..t_PRFLNOAR),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oPRFLNOAR_1_11.GetRadio()
    this.Parent.oContained.w_PRFLNOAR = this.RadioValue()
    return .t.
  endfunc

  func oPRFLNOAR_1_11.ToRadio()
    this.Parent.oContained.w_PRFLNOAR=trim(this.Parent.oContained.w_PRFLNOAR)
    return(;
      iif(this.Parent.oContained.w_PRFLNOAR=='S',1,;
      0))
  endfunc

  func oPRFLNOAR_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPRFLNOAR_1_11.mCond()
    with this.Parent.oContained
      return (.w_PRMIXMAT='S' AND .cFunction<>'Query')
    endwith
  endfunc

  func oPRFLNOAR_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRMIXMAT<>'S')
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=24, top=163, width=548,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="PRGRUMER",Label2="Gru. Merc.",Field3="PRCODART",Label3="Articolo",Field4="DESART",Label4="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168677754

  add object oStr_1_3 as StdString with uid="ZBCYKXFODO",Visible=.t., Left=10, Top=15,;
    Alignment=1, Width=103, Height=18,;
    Caption="Promozione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JYSYPIVCCO",Visible=.t., Left=10, Top=42,;
    Alignment=1, Width=103, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="DJWULTDLWZ",Visible=.t., Left=10, Top=131,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="MAOGIPFIQW",Visible=.t., Left=230, Top=131,;
    Alignment=1, Width=55, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="CTFSQDMYHZ",Visible=.t., Left=10, Top=94,;
    Alignment=1, Width=103, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=14,top=182,;
    width=544+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=15,top=183,width=543+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GRUMERC|ART_ICOL|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GRUMERC'
        oDropInto=this.oBodyCol.oRow.oPRGRUMER_2_2
      case cFile='ART_ICOL'
        oDropInto=this.oBodyCol.oRow.oPRCODART_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oObj_2_9 as cp_runprogram with uid="RSRKFCZLCD",width=234,height=23,;
   left=355, top=389,;
    caption='GSPR_BPR(D)',;
   bGlobalFont=.t.,;
    prg="GSPS_BPR('D')",;
    cEvent = "HasEvent",;
    nPag=2;
    , HelpContextID = 8333768

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsps_mprPag2 as StdContainer
    Width  = 583
    height = 376
    stdWidth  = 583
    stdheight = 376
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPRFLOGPR_4_1 as StdCombo with uid="CMWVNFFETV",rtseq=11,rtrep=.f.,left=180,top=39,width=142,height=21;
    , ToolTipText = "Soggetto a cui � rivolta la promozione";
    , HelpContextID = 210280632;
    , cFormVar="w_PRFLOGPR",RowSource=""+"Tutti i clienti,"+"Categ. commerciale,"+"Fidelity Card", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPRFLOGPR_4_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRFLOGPR,&i_cF..t_PRFLOGPR),this.value)
    return(iif(xVal =1,'G',;
    iif(xVal =2,'C',;
    iif(xVal =3,'F',;
    space(1)))))
  endfunc
  func oPRFLOGPR_4_1.GetRadio()
    this.Parent.oContained.w_PRFLOGPR = this.RadioValue()
    return .t.
  endfunc

  func oPRFLOGPR_4_1.ToRadio()
    this.Parent.oContained.w_PRFLOGPR=trim(this.Parent.oContained.w_PRFLOGPR)
    return(;
      iif(this.Parent.oContained.w_PRFLOGPR=='G',1,;
      iif(this.Parent.oContained.w_PRFLOGPR=='C',2,;
      iif(this.Parent.oContained.w_PRFLOGPR=='F',3,;
      0))))
  endfunc

  func oPRFLOGPR_4_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRCATCOM_4_2 as StdField with uid="YXTAKHEYLI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PRCATCOM", cQueryName = "PRCATCOM",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale associata alla promozione",;
    HelpContextID = 4444349,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=180, Top=64, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", oKey_1_1="CTCODICE", oKey_1_2="this.w_PRCATCOM"

  func oPRCATCOM_4_2.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRFLOGPR='C')
    endwith
  endfunc

  func oPRCATCOM_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCATCOM_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCATCOM_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oPRCATCOM_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie commerciali",'',this.parent.oContained
  endproc

  add object oPRDATINI_4_3 as StdField with uid="QJIMXKUGOB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PRDATINI", cQueryName = "PRDATINI",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data attivazione promozione",;
    HelpContextID = 172212417,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=135, Top=125

  add object oPRDATFIN_4_4 as StdField with uid="KIEREINMQB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PRDATFIN", cQueryName = "PRDATFIN",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data disattivazione promozione",;
    HelpContextID = 45891396,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=135, Top=151

  add object oPRORAINI_4_5 as StdField with uid="SLGJHIRUQC",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PRORAINI", cQueryName = "PRORAINI",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora inizio validit� non corretta specificare da 00 a 23",;
    ToolTipText = "Ora inizio promozione",;
    HelpContextID = 190976193,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=314, Top=125, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPRORAINI_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PRORAINI)<24 And Not Empty( .w_PRORAINI  ))
    endwith
    return bRes
  endfunc

  add object oPRMININI_4_6 as StdField with uid="PJOQUTEONM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PRMININI", cQueryName = "PRMININI",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti inizio validit� non corretti, specificare da 00 a 59",;
    ToolTipText = "Minuti inizio promozione",;
    HelpContextID = 177942721,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=355, Top=125, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPRMININI_4_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PRORAINI) AND .cFunction <>'Query')
    endwith
  endfunc

  func oPRMININI_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PRMININI)<60)
    endwith
    return bRes
  endfunc

  add object oPRORAFIN_4_7 as StdField with uid="DWKUZFVXWN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PRORAFIN", cQueryName = "PRORAFIN",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora fine validit� non corretta, specificare da 00 a 23",;
    ToolTipText = "Ora fine promozione",;
    HelpContextID = 27127620,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=314, Top=151, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPRORAFIN_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PRORAFIN)<24 And Not Empty(.w_PRORAFIN))
    endwith
    return bRes
  endfunc

  add object oPRMINFIN_4_8 as StdField with uid="ADFRIDPPZJ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PRMINFIN", cQueryName = "PRMINFIN",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuti fine validit� non corretti, specificare da 00 a 59",;
    ToolTipText = "Minuti fine promozione",;
    HelpContextID = 40161092,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=355, Top=151, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPRMINFIN_4_8.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PRORAFIN) AND .cFunction <>'Query')
    endwith
  endfunc

  func oPRMINFIN_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PRMINFIN)<60)
    endwith
    return bRes
  endfunc

  add object oPRGIOLUN_4_9 as StdCheck with uid="NOBPEHCGSP",rtseq=19,rtrep=.f.,left=396, top=121, caption="Luned�",;
    ToolTipText = "Valida luned�",;
    HelpContextID = 141848388,;
    cFormVar="w_PRGIOLUN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPRGIOLUN_4_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRGIOLUN,&i_cF..t_PRGIOLUN),this.value)
    return(iif(xVal =1,'2',;
    ' '))
  endfunc
  func oPRGIOLUN_4_9.GetRadio()
    this.Parent.oContained.w_PRGIOLUN = this.RadioValue()
    return .t.
  endfunc

  func oPRGIOLUN_4_9.ToRadio()
    this.Parent.oContained.w_PRGIOLUN=trim(this.Parent.oContained.w_PRGIOLUN)
    return(;
      iif(this.Parent.oContained.w_PRGIOLUN=='2',1,;
      0))
  endfunc

  func oPRGIOLUN_4_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRGIOMAR_4_10 as StdCheck with uid="YYMCPWLRNL",rtseq=20,rtrep=.f.,left=489, top=121, caption="Marted�",;
    ToolTipText = "Valida marted�",;
    HelpContextID = 109809848,;
    cFormVar="w_PRGIOMAR", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPRGIOMAR_4_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRGIOMAR,&i_cF..t_PRGIOMAR),this.value)
    return(iif(xVal =1,'3',;
    ' '))
  endfunc
  func oPRGIOMAR_4_10.GetRadio()
    this.Parent.oContained.w_PRGIOMAR = this.RadioValue()
    return .t.
  endfunc

  func oPRGIOMAR_4_10.ToRadio()
    this.Parent.oContained.w_PRGIOMAR=trim(this.Parent.oContained.w_PRGIOMAR)
    return(;
      iif(this.Parent.oContained.w_PRGIOMAR=='3',1,;
      0))
  endfunc

  func oPRGIOMAR_4_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRGIOMER_4_11 as StdCheck with uid="NNALHCIBDW",rtseq=21,rtrep=.f.,left=396, top=141, caption="Mercoled�",;
    ToolTipText = "Valida mercoled�",;
    HelpContextID = 158625608,;
    cFormVar="w_PRGIOMER", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPRGIOMER_4_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRGIOMER,&i_cF..t_PRGIOMER),this.value)
    return(iif(xVal =1,'4',;
    ' '))
  endfunc
  func oPRGIOMER_4_11.GetRadio()
    this.Parent.oContained.w_PRGIOMER = this.RadioValue()
    return .t.
  endfunc

  func oPRGIOMER_4_11.ToRadio()
    this.Parent.oContained.w_PRGIOMER=trim(this.Parent.oContained.w_PRGIOMER)
    return(;
      iif(this.Parent.oContained.w_PRGIOMER=='4',1,;
      0))
  endfunc

  func oPRGIOMER_4_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRGIOGIO_4_12 as StdCheck with uid="OILVCSKSCB",rtseq=22,rtrep=.f.,left=489, top=141, caption="Gioved�",;
    ToolTipText = "Valida gioved�",;
    HelpContextID = 57962309,;
    cFormVar="w_PRGIOGIO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPRGIOGIO_4_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRGIOGIO,&i_cF..t_PRGIOGIO),this.value)
    return(iif(xVal =1,'5',;
    ' '))
  endfunc
  func oPRGIOGIO_4_12.GetRadio()
    this.Parent.oContained.w_PRGIOGIO = this.RadioValue()
    return .t.
  endfunc

  func oPRGIOGIO_4_12.ToRadio()
    this.Parent.oContained.w_PRGIOGIO=trim(this.Parent.oContained.w_PRGIOGIO)
    return(;
      iif(this.Parent.oContained.w_PRGIOGIO=='5',1,;
      0))
  endfunc

  func oPRGIOGIO_4_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRGIOVEN_4_13 as StdCheck with uid="FZNUHRVAEZ",rtseq=23,rtrep=.f.,left=396, top=161, caption="Venerd�",;
    ToolTipText = "Valida venerd�",;
    HelpContextID = 41185092,;
    cFormVar="w_PRGIOVEN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPRGIOVEN_4_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRGIOVEN,&i_cF..t_PRGIOVEN),this.value)
    return(iif(xVal =1,'6',;
    ' '))
  endfunc
  func oPRGIOVEN_4_13.GetRadio()
    this.Parent.oContained.w_PRGIOVEN = this.RadioValue()
    return .t.
  endfunc

  func oPRGIOVEN_4_13.ToRadio()
    this.Parent.oContained.w_PRGIOVEN=trim(this.Parent.oContained.w_PRGIOVEN)
    return(;
      iif(this.Parent.oContained.w_PRGIOVEN=='6',1,;
      0))
  endfunc

  func oPRGIOVEN_4_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRGIOSAB_4_14 as StdCheck with uid="DZIXTRDRGH",rtseq=24,rtrep=.f.,left=489, top=161, caption="Sabato",;
    ToolTipText = "Valida sabato",;
    HelpContextID = 9146568,;
    cFormVar="w_PRGIOSAB", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPRGIOSAB_4_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRGIOSAB,&i_cF..t_PRGIOSAB),this.value)
    return(iif(xVal =1,'7',;
    ' '))
  endfunc
  func oPRGIOSAB_4_14.GetRadio()
    this.Parent.oContained.w_PRGIOSAB = this.RadioValue()
    return .t.
  endfunc

  func oPRGIOSAB_4_14.ToRadio()
    this.Parent.oContained.w_PRGIOSAB=trim(this.Parent.oContained.w_PRGIOSAB)
    return(;
      iif(this.Parent.oContained.w_PRGIOSAB=='7',1,;
      0))
  endfunc

  func oPRGIOSAB_4_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRGIODOM_4_15 as StdCheck with uid="APVLNGRIJW",rtseq=25,rtrep=.f.,left=396, top=181, caption="Domenica",;
    ToolTipText = "Valida domenica",;
    HelpContextID = 260804797,;
    cFormVar="w_PRGIODOM", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPRGIODOM_4_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRGIODOM,&i_cF..t_PRGIODOM),this.value)
    return(iif(xVal =1,'1',;
    ' '))
  endfunc
  func oPRGIODOM_4_15.GetRadio()
    this.Parent.oContained.w_PRGIODOM = this.RadioValue()
    return .t.
  endfunc

  func oPRGIODOM_4_15.ToRadio()
    this.Parent.oContained.w_PRGIODOM=trim(this.Parent.oContained.w_PRGIODOM)
    return(;
      iif(this.Parent.oContained.w_PRGIODOM=='1',1,;
      0))
  endfunc

  func oPRGIODOM_4_15.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oPRTIPVAQ_4_16 as StdCombo with uid="BEPKHNYOAG",rtseq=26,rtrep=.f.,left=135,top=232,width=142,height=21;
    , ToolTipText = "Caratteristiche quantitative per validazione promozione";
    , HelpContextID = 42286919;
    , cFormVar="w_PRTIPVAQ",RowSource=""+"Non gestiti,"+"Minimo pezzi,"+"Minimo valore,"+"Minimo bollini", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPRTIPVAQ_4_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPVAQ,&i_cF..t_PRTIPVAQ),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'Q',;
    iif(xVal =3,'V',;
    iif(xVal =4,'B',;
    space(1))))))
  endfunc
  func oPRTIPVAQ_4_16.GetRadio()
    this.Parent.oContained.w_PRTIPVAQ = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPVAQ_4_16.ToRadio()
    this.Parent.oContained.w_PRTIPVAQ=trim(this.Parent.oContained.w_PRTIPVAQ)
    return(;
      iif(this.Parent.oContained.w_PRTIPVAQ=='N',1,;
      iif(this.Parent.oContained.w_PRTIPVAQ=='Q',2,;
      iif(this.Parent.oContained.w_PRTIPVAQ=='V',3,;
      iif(this.Parent.oContained.w_PRTIPVAQ=='B',4,;
      0)))))
  endfunc

  func oPRTIPVAQ_4_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRPEZMIN_4_30 as StdField with uid="YCJQRCASKZ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_PRPEZMIN", cQueryName = "PRPEZMIN",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo di pezzi x attivare la promozione",;
    HelpContextID = 169934660,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=428, Top=232, cSayPict="v_PQ(10)", cGetPict="v_GQ(10)"

  func oPRPEZMIN_4_30.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPVAQ='Q')
    endwith
  endfunc

  func oPRPEZMIN_4_30.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPVAQ<>'Q')
    endwith
    endif
  endfunc

  add object oPRMINBOL_4_31 as StdField with uid="APOIQGWOLJ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_PRMINBOL", cQueryName = "PRMINBOL",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo di bollini x attivare la promozione",;
    HelpContextID = 26947774,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=428, Top=232, cSayPict='"999999"', cGetPict='"999999"'

  func oPRMINBOL_4_31.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPVAQ='B')
    endwith
  endfunc

  func oPRMINBOL_4_31.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPVAQ<>'B')
    endwith
    endif
  endfunc

  add object oPRMINIMP_4_32 as StdField with uid="DTQRLBIUBU",rtseq=41,rtrep=.f.,;
    cFormVar = "w_PRMINIMP", cQueryName = "PRMINIMP",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare valori positivi",;
    ToolTipText = "Importo minimo vendita x attivare la promozione",;
    HelpContextID = 177942714,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=428, Top=232, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  func oPRMINIMP_4_32.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPVAQ='V')
    endwith
  endfunc

  func oPRMINIMP_4_32.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPVAQ<>'V')
    endwith
    endif
  endfunc

  func oPRMINIMP_4_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRMINIMP>0)
    endwith
    return bRes
  endfunc


  add object oPRTIPPRO_4_33 as StdCombo with uid="TSLIVZTHHB",rtseq=42,rtrep=.f.,left=180,top=294,width=110,height=21;
    , ToolTipText = "Definisce l'effetto della promozione";
    , HelpContextID = 210059077;
    , cFormVar="w_PRTIPPRO",RowSource=""+"Sconto,"+"Punti Fidelity,"+"Omaggio", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPRTIPPRO_4_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPPRO,&i_cF..t_PRTIPPRO),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'F',;
    iif(xVal =3,'O',;
    space(1)))))
  endfunc
  func oPRTIPPRO_4_33.GetRadio()
    this.Parent.oContained.w_PRTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPPRO_4_33.ToRadio()
    this.Parent.oContained.w_PRTIPPRO=trim(this.Parent.oContained.w_PRTIPPRO)
    return(;
      iif(this.Parent.oContained.w_PRTIPPRO=='S',1,;
      iif(this.Parent.oContained.w_PRTIPPRO=='F',2,;
      iif(this.Parent.oContained.w_PRTIPPRO=='O',3,;
      0))))
  endfunc

  func oPRTIPPRO_4_33.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oPRFLOMAG_4_34 as StdCombo with uid="HCUSLKMJOE",rtseq=43,rtrep=.f.,left=428,top=294,width=139,height=21;
    , ToolTipText = "Tipologia della riga da omaggiare";
    , HelpContextID = 109617347;
    , cFormVar="w_PRFLOMAG",RowSource=""+"Sconto merce,"+"Omaggio imp. + IVA", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPRFLOMAG_4_34.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRFLOMAG,&i_cF..t_PRFLOMAG),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'E',;
    space(1))))
  endfunc
  func oPRFLOMAG_4_34.GetRadio()
    this.Parent.oContained.w_PRFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oPRFLOMAG_4_34.ToRadio()
    this.Parent.oContained.w_PRFLOMAG=trim(this.Parent.oContained.w_PRFLOMAG)
    return(;
      iif(this.Parent.oContained.w_PRFLOMAG=='S',1,;
      iif(this.Parent.oContained.w_PRFLOMAG=='E',2,;
      0)))
  endfunc

  func oPRFLOMAG_4_34.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPRFLOMAG_4_34.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='O')
    endwith
  endfunc

  func oPRFLOMAG_4_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
    endif
  endfunc


  add object oPRTIPSCO_4_36 as StdCombo with uid="LCDBSAJJMO",rtseq=45,rtrep=.f.,left=180,top=319,width=213,height=21;
    , ToolTipText = "Tipologia promozione sconto";
    , HelpContextID = 260390725;
    , cFormVar="w_PRTIPSCO",RowSource=""+"Sc. % sul paniere,"+"Sc. % su eccedenza,"+"Sc. valore totale fisso,"+"Sc. valore totale multiplo,"+"Sc. valore unitario,"+"Sc. valore unitario su eccedenza,"+"Prezzo unitario fisso,"+"Prezzo tot. fisso per i primi N pezzi,"+"Prezzo tot. multiplo x N pezzi,"+"Promozione N x M", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPRTIPSCO_4_36.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPSCO,&i_cF..t_PRTIPSCO),this.value)
    return(iif(xVal =1,'SPT',;
    iif(xVal =2,'SPE',;
    iif(xVal =3,'SVF',;
    iif(xVal =4,'SVM',;
    iif(xVal =5,'SVP',;
    iif(xVal =6,'SVE',;
    iif(xVal =7,'PUP',;
    iif(xVal =8,'PTF',;
    iif(xVal =9,'PTM',;
    iif(xVal =10,'NXM',;
    space(3))))))))))))
  endfunc
  func oPRTIPSCO_4_36.GetRadio()
    this.Parent.oContained.w_PRTIPSCO = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPSCO_4_36.ToRadio()
    this.Parent.oContained.w_PRTIPSCO=trim(this.Parent.oContained.w_PRTIPSCO)
    return(;
      iif(this.Parent.oContained.w_PRTIPSCO=='SPT',1,;
      iif(this.Parent.oContained.w_PRTIPSCO=='SPE',2,;
      iif(this.Parent.oContained.w_PRTIPSCO=='SVF',3,;
      iif(this.Parent.oContained.w_PRTIPSCO=='SVM',4,;
      iif(this.Parent.oContained.w_PRTIPSCO=='SVP',5,;
      iif(this.Parent.oContained.w_PRTIPSCO=='SVE',6,;
      iif(this.Parent.oContained.w_PRTIPSCO=='PUP',7,;
      iif(this.Parent.oContained.w_PRTIPSCO=='PTF',8,;
      iif(this.Parent.oContained.w_PRTIPSCO=='PTM',9,;
      iif(this.Parent.oContained.w_PRTIPSCO=='NXM',10,;
      0)))))))))))
  endfunc

  func oPRTIPSCO_4_36.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPRTIPSCO_4_36.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='S')
    endwith
  endfunc

  func oPRTIPSCO_4_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S')
    endwith
    endif
  endfunc


  add object oPRTIPFID_4_37 as StdCombo with uid="KODNDJADTY",rtseq=46,rtrep=.f.,left=180,top=319,width=140,height=21;
    , ToolTipText = "Tipologia promozione Fidelity";
    , HelpContextID = 42286906;
    , cFormVar="w_PRTIPFID",RowSource=""+"Raggiunto il minimo,"+"Ad ogni multiplo", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPRTIPFID_4_37.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPFID,&i_cF..t_PRTIPFID),this.value)
    return(iif(xVal =1,'NPF',;
    iif(xVal =2,'NPM',;
    space(3))))
  endfunc
  func oPRTIPFID_4_37.GetRadio()
    this.Parent.oContained.w_PRTIPFID = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPFID_4_37.ToRadio()
    this.Parent.oContained.w_PRTIPFID=trim(this.Parent.oContained.w_PRTIPFID)
    return(;
      iif(this.Parent.oContained.w_PRTIPFID=='NPF',1,;
      iif(this.Parent.oContained.w_PRTIPFID=='NPM',2,;
      0)))
  endfunc

  func oPRTIPFID_4_37.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPRTIPFID_4_37.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='F')
    endwith
  endfunc

  func oPRTIPFID_4_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'F')
    endwith
    endif
  endfunc


  add object oPRTIPOMA_4_38 as StdCombo with uid="VUSKKJGOCZ",rtseq=47,rtrep=.f.,left=180,top=319,width=140,height=21;
    , ToolTipText = "Tipologia promozione omaggio";
    , HelpContextID = 75153609;
    , cFormVar="w_PRTIPOMA",RowSource=""+"Raggiunto il minimo,"+"Ad ogni multiplo", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPRTIPOMA_4_38.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPOMA,&i_cF..t_PRTIPOMA),this.value)
    return(iif(xVal =1,'QAF',;
    iif(xVal =2,'QAM',;
    space(3))))
  endfunc
  func oPRTIPOMA_4_38.GetRadio()
    this.Parent.oContained.w_PRTIPOMA = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPOMA_4_38.ToRadio()
    this.Parent.oContained.w_PRTIPOMA=trim(this.Parent.oContained.w_PRTIPOMA)
    return(;
      iif(this.Parent.oContained.w_PRTIPOMA=='QAF',1,;
      iif(this.Parent.oContained.w_PRTIPOMA=='QAM',2,;
      0)))
  endfunc

  func oPRTIPOMA_4_38.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPRTIPOMA_4_38.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='O')
    endwith
  endfunc

  func oPRTIPOMA_4_38.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
    endif
  endfunc


  add object oObj_4_40 as cp_runprogram with uid="SIYPDOVEBB",left=-6, top=388, width=548,height=20,;
    caption='GSPS_BTP(A)',;
   bGlobalFont=.t.,;
    prg="GSPS_BTP('A')",;
    cEvent = "w_PRTIPSCO Changed,w_PRTIPPRO Changed,w_PRTIPVAQ Changed,Load,New record",;
    nPag=4;
    , HelpContextID = 260166454

  add object oDESCOM_4_42 as StdField with uid="AFLYVTHBLU",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione categoria commerciale",;
    HelpContextID = 110157514,;
   bGlobalFont=.t.,;
    Height=21, Width=318, Left=231, Top=64, InputMask=replicate('X',35)

  add object oPRPERSCO_4_43 as StdField with uid="AMNGEUIAIZ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_PRPERSCO", cQueryName = "PRPERSCO",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 262209349,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=180, Top=343, cSayPict='"9999.99"', cGetPict='"9999.99"'

  func oPRPERSCO_4_43.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_TSTSCP='S')
    endwith
  endfunc

  func oPRPERSCO_4_43.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_TSTSCP<>'S')
    endwith
    endif
  endfunc

  add object oPRPUNEXT_4_44 as StdField with uid="YSFNJXPNGN",rtseq=50,rtrep=.f.,;
    cFormVar = "w_PRPUNEXT", cQueryName = "PRPUNEXT",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 24182602,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=180, Top=343, cSayPict='"999999"', cGetPict='"999999"'

  func oPRPUNEXT_4_44.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='F')
    endwith
  endfunc

  func oPRPUNEXT_4_44.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'F')
    endwith
    endif
  endfunc

  add object oPRVALSCU_4_45 as StdField with uid="PFMMGZOMUV",rtseq=51,rtrep=.f.,;
    cFormVar = "w_PRVALSCU", cQueryName = "PRVALSCU",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare valori positivi",;
    HelpContextID = 255680331,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=180, Top=343, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  func oPRVALSCU_4_45.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_TSTSVU='S')
    endwith
  endfunc

  func oPRVALSCU_4_45.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_TSTSVU<>'S')
    endwith
    endif
  endfunc

  func oPRVALSCU_4_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRVALSCU>0)
    endwith
    return bRes
  endfunc

  add object oPRPRUFIS_4_46 as StdField with uid="QKRBKLPJYK",rtseq=52,rtrep=.f.,;
    cFormVar = "w_PRPRUFIS", cQueryName = "PRPRUFIS",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare valori positivi",;
    HelpContextID = 48103241,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=180, Top=343, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  func oPRPRUFIS_4_46.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_TSTPUF='S')
    endwith
  endfunc

  func oPRPRUFIS_4_46.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_TSTPUF<>'S')
    endwith
    endif
  endfunc

  func oPRPRUFIS_4_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRPRUFIS>0)
    endwith
    return bRes
  endfunc

  add object oPRPRTFIS_4_47 as StdField with uid="DZXUGNAVKL",rtseq=53,rtrep=.f.,;
    cFormVar = "w_PRPRTFIS", cQueryName = "PRPRTFIS",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare valori positivi",;
    HelpContextID = 47054665,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=180, Top=343, cSayPict="v_PV(18)", cGetPict="v_PV(18)"

  func oPRPRTFIS_4_47.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_TSTPTF='S')
    endwith
  endfunc

  func oPRPRTFIS_4_47.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_TSTPTF<>'S')
    endwith
    endif
  endfunc

  func oPRPRTFIS_4_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRPRTFIS>0)
    endwith
    return bRes
  endfunc

  add object oPRARTOMA_4_48 as StdField with uid="HWNBREVAXN",rtseq=54,rtrep=.f.,;
    cFormVar = "w_PRARTOMA", cQueryName = "PRARTOMA",;
    bObbl = .t. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile selezionare articoli non pos, obsoleti o descrittivi",;
    HelpContextID = 70447305,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=180, Top=343, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_PRARTOMA"

  func oPRARTOMA_4_48.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='O')
    endwith
  endfunc

  func oPRARTOMA_4_48.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
    endif
  endfunc

  func oPRARTOMA_4_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRARTOMA_4_48.ecpDrop(oSource)
    this.Parent.oContained.link_4_48('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRARTOMA_4_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oPRARTOMA_4_48'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSPS_APR.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oPRUMIOMA_4_67 as StdField with uid="QMHDMGONLW",rtseq=59,rtrep=.f.,;
    cFormVar = "w_PRUMIOMA", cQueryName = "PRUMIOMA",;
    bObbl = .t. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura incongruente per l'articolo selezionato",;
    ToolTipText = "U.M. qta articolo omaggio",;
    HelpContextID = 82227401,;
   bGlobalFont=.t.,;
    Height=21, Width=36, Left=434, Top=343, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_PRUMIOMA"

  func oPRUMIOMA_4_67.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='O' AND NOT EMPTY(.w_PRARTOMA))
    endwith
  endfunc

  func oPRUMIOMA_4_67.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
    endif
  endfunc

  func oPRUMIOMA_4_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRUMIOMA_4_67.ecpDrop(oSource)
    this.Parent.oContained.link_4_67('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRUMIOMA_4_67.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oPRUMIOMA_4_67'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc

  add object oPRQTOMAG_4_69 as StdField with uid="OMOVEXMDVJ",rtseq=61,rtrep=.f.,;
    cFormVar = "w_PRQTOMAG", cQueryName = "PRQTOMAG",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� non frazionabile",;
    ToolTipText = "Quantit� articolo da omaggiare",;
    HelpContextID = 109048003,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=470, Top=343, cSayPict="v_PQ(10)", cGetPict="v_GQ(10)"

  func oPRQTOMAG_4_69.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='O' AND NOT EMPTY(.w_PRARTOMA))
    endwith
  endfunc

  func oPRQTOMAG_4_69.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
    endif
  endfunc

  func oPRQTOMAG_4_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PRQTOMAG>0 AND (.w_FLFRAZ<>'S' OR .w_PRQTOMAG=INT(.w_PRQTOMAG))))
    endwith
    return bRes
  endfunc

  add object oPRQTENNE_4_77 as StdField with uid="VGHKPCHTYY",rtseq=62,rtrep=.f.,;
    cFormVar = "w_PRQTENNE", cQueryName = "PRQTENNE",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� N di promozione NxM",;
    HelpContextID = 102756549,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=180, Top=343, cSayPict="v_PQ(10)", cGetPict="v_GQ(10)"

  func oPRQTENNE_4_77.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_PRTIPSCO='NXM')
    endwith
  endfunc

  func oPRQTENNE_4_77.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_PRTIPSCO<>'NXM')
    endwith
    endif
  endfunc

  func oPRQTENNE_4_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRQTENNE>.w_PRQTEMME)
    endwith
    return bRes
  endfunc

  add object oPRQTEMME_4_80 as StdField with uid="XQAQBAIWZN",rtseq=63,rtrep=.f.,;
    cFormVar = "w_PRQTEMME", cQueryName = "PRQTEMME",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� M di promozione NxM",;
    HelpContextID = 119533765,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=330, Top=343, cSayPict="v_PQ(10)", cGetPict="v_GQ(10)"

  func oPRQTEMME_4_80.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_PRTIPPRO='S' AND .w_PRTIPSCO='NXM')
    endwith
  endfunc

  func oPRQTEMME_4_80.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_PRTIPSCO<>'NXM')
    endwith
    endif
  endfunc

  func oPRQTEMME_4_80.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRQTENNE>.w_PRQTEMME)
    endwith
    return bRes
  endfunc


  add object oObj_4_86 as cp_runprogram with uid="JZXMYZGNPK",left=-5, top=410, width=181,height=20,;
    caption='GSPS_BTP(B)',;
   bGlobalFont=.t.,;
    prg="GSPS_BTP('B')",;
    cEvent = "Controlli",;
    nPag=4;
    , HelpContextID = 260166710

  add object oStr_4_26 as StdString with uid="XBDAWUQEPO",Visible=.t., Left=227, Top=127,;
    Alignment=1, Width=86, Height=18,;
    Caption="Ora inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="QDWYERCBGO",Visible=.t., Left=249, Top=152,;
    Alignment=1, Width=64, Height=18,;
    Caption="Ora fine:"  ;
  , bGlobalFont=.t.

  add object oStr_4_28 as StdString with uid="AGWYEVJITE",Visible=.t., Left=37, Top=38,;
    Alignment=1, Width=142, Height=18,;
    Caption="Soggetto promozione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_29 as StdString with uid="IMTMOUNKKE",Visible=.t., Left=37, Top=64,;
    Alignment=1, Width=142, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="PYLWABRDPO",Visible=.t., Left=17, Top=232,;
    Alignment=1, Width=117, Height=18,;
    Caption="Requisiti minimi:"  ;
  , bGlobalFont=.t.

  add object oStr_4_39 as StdString with uid="KMMMUQKHTT",Visible=.t., Left=11, Top=102,;
    Alignment=0, Width=172, Height=18,;
    Caption="Validit� temporale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_50 as StdString with uid="GEGOWYCMSJ",Visible=.t., Left=11, Top=15,;
    Alignment=0, Width=183, Height=18,;
    Caption="Validit� soggettiva"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_51 as StdString with uid="ETBWRWGFWD",Visible=.t., Left=11, Top=126,;
    Alignment=1, Width=123, Height=18,;
    Caption="Data inizio validita:"  ;
  , bGlobalFont=.t.

  add object oStr_4_52 as StdString with uid="PXZUVXUKEY",Visible=.t., Left=11, Top=152,;
    Alignment=1, Width=123, Height=18,;
    Caption="Data fine validita:"  ;
  , bGlobalFont=.t.

  add object oStr_4_53 as StdString with uid="YEUASZKRTJ",Visible=.t., Left=11, Top=208,;
    Alignment=0, Width=159, Height=18,;
    Caption="Validit� quantitativa"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_55 as StdString with uid="WUKBXTSDBF",Visible=.t., Left=280, Top=232,;
    Alignment=1, Width=146, Height=18,;
    Caption="Numero minimo pezzi:"  ;
  , bGlobalFont=.t.

  func oStr_4_55.mHide()
    with this.Parent.oContained
      return (.w_PRTIPVAQ<>'Q')
    endwith
  endfunc

  add object oStr_4_56 as StdString with uid="KMGLFVFUQC",Visible=.t., Left=280, Top=232,;
    Alignment=1, Width=146, Height=18,;
    Caption="Importo minimo vendita:"  ;
  , bGlobalFont=.t.

  func oStr_4_56.mHide()
    with this.Parent.oContained
      return (.w_PRTIPVAQ<>'V')
    endwith
  endfunc

  add object oStr_4_57 as StdString with uid="MNWWQGDJGO",Visible=.t., Left=280, Top=232,;
    Alignment=1, Width=146, Height=18,;
    Caption="Numero minimo bollini:"  ;
  , bGlobalFont=.t.

  func oStr_4_57.mHide()
    with this.Parent.oContained
      return (.w_PRTIPVAQ<>'B')
    endwith
  endfunc

  add object oStr_4_58 as StdString with uid="ZNKAPTOFQV",Visible=.t., Left=62, Top=344,;
    Alignment=1, Width=118, Height=18,;
    Caption="Sconto percentuale:"  ;
  , bGlobalFont=.t.

  func oStr_4_58.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_TSTSCP<>'S')
    endwith
  endfunc

  add object oStr_4_59 as StdString with uid="TEIGDMERNG",Visible=.t., Left=47, Top=344,;
    Alignment=1, Width=133, Height=18,;
    Caption="Sconto a valore:"  ;
  , bGlobalFont=.t.

  func oStr_4_59.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_TSTSVU<>'S')
    endwith
  endfunc

  add object oStr_4_60 as StdString with uid="XLDHEBQEDM",Visible=.t., Left=61, Top=344,;
    Alignment=1, Width=119, Height=18,;
    Caption="Prezzo unitario fisso:"  ;
  , bGlobalFont=.t.

  func oStr_4_60.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_TSTPUF<>'S')
    endwith
  endfunc

  add object oStr_4_61 as StdString with uid="JAVAVXZEZW",Visible=.t., Left=70, Top=344,;
    Alignment=1, Width=110, Height=18,;
    Caption="Prezzo totale fisso:"  ;
  , bGlobalFont=.t.

  func oStr_4_61.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_TSTPTF<>'S')
    endwith
  endfunc

  add object oStr_4_62 as StdString with uid="NDKWMMUOYJ",Visible=.t., Left=96, Top=344,;
    Alignment=1, Width=84, Height=18,;
    Caption="N.punti extra:"  ;
  , bGlobalFont=.t.

  func oStr_4_62.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'F')
    endwith
  endfunc

  add object oStr_4_63 as StdString with uid="MZCGUUFMZA",Visible=.t., Left=83, Top=344,;
    Alignment=1, Width=97, Height=18,;
    Caption="Articolo omaggio:"  ;
  , bGlobalFont=.t.

  func oStr_4_63.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
  endfunc

  add object oStr_4_70 as StdString with uid="BGZMEHCTYF",Visible=.t., Left=343, Top=345,;
    Alignment=1, Width=90, Height=18,;
    Caption="Qta articolo:"  ;
  , bGlobalFont=.t.

  func oStr_4_70.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
  endfunc

  add object oStr_4_71 as StdString with uid="IVYTDGYNIW",Visible=.t., Left=43, Top=294,;
    Alignment=1, Width=137, Height=18,;
    Caption="Effetto promozione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_72 as StdString with uid="QBKDNVHANS",Visible=.t., Left=42, Top=319,;
    Alignment=1, Width=138, Height=18,;
    Caption="Tipologia sconto:"  ;
  , bGlobalFont=.t.

  func oStr_4_72.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S')
    endwith
  endfunc

  add object oStr_4_73 as StdString with uid="ZYIRUMGSKN",Visible=.t., Left=34, Top=319,;
    Alignment=1, Width=146, Height=18,;
    Caption="Numero punti extra:"  ;
  , bGlobalFont=.t.

  func oStr_4_73.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'F')
    endwith
  endfunc

  add object oStr_4_74 as StdString with uid="LQHMVRZYMN",Visible=.t., Left=26, Top=319,;
    Alignment=1, Width=154, Height=18,;
    Caption="Omaggio:"  ;
  , bGlobalFont=.t.

  func oStr_4_74.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
  endfunc

  add object oStr_4_75 as StdString with uid="JMYFBDOVZQ",Visible=.t., Left=11, Top=268,;
    Alignment=0, Width=113, Height=18,;
    Caption="Effetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_78 as StdString with uid="NYIBYMTXWE",Visible=.t., Left=90, Top=344,;
    Alignment=1, Width=90, Height=18,;
    Caption="Qta N:"  ;
  , bGlobalFont=.t.

  func oStr_4_78.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_PRTIPSCO<>'NXM')
    endwith
  endfunc

  add object oStr_4_79 as StdString with uid="CKMMPFGSYP",Visible=.t., Left=275, Top=345,;
    Alignment=1, Width=52, Height=18,;
    Caption="x qta M:"  ;
  , bGlobalFont=.t.

  func oStr_4_79.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'S' OR .w_PRTIPSCO<>'NXM')
    endwith
  endfunc

  add object oStr_4_82 as StdString with uid="KJJGTWUQEV",Visible=.t., Left=341, Top=127,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_4_83 as StdString with uid="FNOENJZRYS",Visible=.t., Left=341, Top=153,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_4_87 as StdString with uid="IPSNAOGHPA",Visible=.t., Left=298, Top=294,;
    Alignment=1, Width=128, Height=18,;
    Caption="Tipo riga omaggio:"  ;
  , bGlobalFont=.t.

  func oStr_4_87.mHide()
    with this.Parent.oContained
      return (.w_PRTIPPRO<>'O')
    endwith
  endfunc

  add object oBox_4_41 as StdBox with uid="UPAFJLFVYS",left=2, top=118, width=578,height=83

  add object oBox_4_49 as StdBox with uid="HAMPGUVNIE",left=2, top=32, width=578,height=61

  add object oBox_4_54 as StdBox with uid="OBDROIKXOI",left=2, top=225, width=578,height=37

  add object oBox_4_76 as StdBox with uid="QGOXDGESXR",left=2, top=286, width=578,height=85
enddefine

* --- Defining Body row
define class tgsps_mprBodyRow as CPBodyRowCnt
  Width=534
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="LQVHYCRCGQ",rtseq=36,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 201006186,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oPRGRUMER_2_2 as StdTrsField with uid="AVDVOXBNKV",rtseq=37,rtrep=.t.,;
    cFormVar="w_PRGRUMER",value=space(5),;
    HelpContextID = 165506888,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Se � attivo il check tutti gli articoli non devono essere inserite righe",;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=50, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_PRGRUMER"

  func oPRGRUMER_2_2.mCond()
    with this.Parent.oContained
      return (.w_PRMIXMAT='S')
    endwith
  endfunc

  func oPRGRUMER_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRGRUMER_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRGRUMER_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oPRGRUMER_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oPRCODART_2_3 as StdTrsField with uid="QZPAISMLSW",rtseq=38,rtrep=.t.,;
    cFormVar="w_PRCODART",value=space(20),;
    HelpContextID = 53858486,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile selezionare articoli non pos, obsoleti o servizi",;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=115, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_PRCODART"

  func oPRCODART_2_3.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_PRGRUMER))
    endwith
  endfunc

  func oPRCODART_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODART_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRCODART_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oPRCODART_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSPS_APR.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDESART_2_4 as StdTrsField with uid="VANLAPIECV",rtseq=44,rtrep=.t.,;
    cFormVar="w_DESART",value=space(35),enabled=.f.,;
    HelpContextID = 258137802,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=258, Left=271, Top=0, InputMask=replicate('X',35)

  add object oObj_2_8 as cp_runprogram with uid="IIREOFIAIN",width=234,height=23,;
   left=92, top=207,;
    caption='GSPR_BPR(C)',;
   bGlobalFont=.t.,;
    prg="GSPS_BPR('C')",;
    cEvent = "w_PRGRUMER Changed",;
    nPag=2;
    , HelpContextID = 8334024
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_mpr','PRM_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRCODICE=PRM_MAST.PRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
