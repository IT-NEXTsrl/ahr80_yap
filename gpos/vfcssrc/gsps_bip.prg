* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bip                                                        *
*              Controlli al cambio prezzo vendita                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-06                                                      *
* Last revis.: 2003-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bip",oParentObject)
return(i_retval)

define class tgsps_bip as StdBatch
  * --- Local variables
  w_ORIF = space(1)
  w_PRZORI = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli al Cambio Prezzo sulle Righe (da GSPS_MVD)
    if NOT EMPTY(this.oParentObject.w_MDSERRIF) AND this.oParentObject.w_MDROWRIF<>0
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVPREZZO"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MDROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVPREZZO;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_MDSERRIF;
              and CPROWNUM = this.oParentObject.w_MDROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PRZORI = NVL(cp_ToDate(_read_.MVPREZZO),cp_NullValue(_read_.MVPREZZO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_MDTIPRIG="F" AND this.oParentObject.w_MDFLARIF<>" " AND (this.oParentObject.cFunction="Load" OR this.oParentObject.cFunction="Edit")
        this.w_ORIF = this.oParentObject.w_MDFLERIF
        if this.oParentObject.w_MDPREZZO<=this.w_PRZORI
          if ah_YesNo("Evado il valore rimanente sul documento di origine?")
            this.oParentObject.w_MDFLERIF = "S"
          else
            this.oParentObject.w_MDFLERIF = " "
          endif
        endif
        if this.w_ORIF=this.oParentObject.w_MDFLERIF
          * --- Non esegue la mCalc
          this.bUpdateParentObject = .F.
        endif
      endif
    else
      * --- Non esegue la mCalc
      this.bUpdateParentObject = .F.
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
