* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_acv                                                        *
*              Clienti negozio                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_110]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_acv"))

* --- Class definition
define class tgsps_acv as StdForm
  Top    = 7
  Left   = 13

  * --- Standard Properties
  Width  = 594
  Height = 317+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-14"
  HelpContextID=109664361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  CLI_VEND_IDX = 0
  NAZIONI_IDX = 0
  CATECOMM_IDX = 0
  CAT_SCMA_IDX = 0
  PAG_AMEN_IDX = 0
  LISTINI_IDX = 0
  BUSIUNIT_IDX = 0
  cFile = "CLI_VEND"
  cKeySelect = "CLCODCLI"
  cKeyWhere  = "CLCODCLI=this.w_CLCODCLI"
  cKeyWhereODBC = '"CLCODCLI="+cp_ToStrODBC(this.w_CLCODCLI)';

  cKeyWhereODBCqualified = '"CLI_VEND.CLCODCLI="+cp_ToStrODBC(this.w_CLCODCLI)';

  cPrg = "gsps_acv"
  cComment = "Clienti negozio"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CLCODCLI = space(15)
  w_CLDESCRI = space(60)
  w_CLCODCON = space(15)
  w_CLINDIRI = space(35)
  w_CL___CAP = space(8)
  w_CLLOCALI = space(35)
  w_CLPROVIN = space(2)
  w_CLNAZION = space(5)
  w_CLCODFIS = space(16)
  w_CLPARIVA = space(12)
  w_CLTELEFO = space(18)
  w_CLTELFAX = space(18)
  w_CLNUMCEL = space(18)
  w_CL_EMAIL = space(254)
  w_CL_EMPEC = space(254)
  w_DESNAZ = space(35)
  w_FLSCM = space(1)
  w_CLTIPCON = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CLCATCOM = space(3)
  w_CLCATSCM = space(5)
  w_DESCAT = space(35)
  w_DESSCM = space(35)
  w_CLCODPAG = space(5)
  w_DESPAG = space(30)
  w_CLCODLIS = space(5)
  w_CODAZI = space(5)
  w_CLSCONT1 = 0
  o_CLSCONT1 = 0
  w_CLSCONT2 = 0
  w_CLCODNEG = space(3)
  w_CLVALFID = 0
  w_CLFIDUTI = 0
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_FIDRES1 = 0
  w_FIDRES2 = 0
  w_DESLIS = space(30)
  w_CLDTINVA = ctod('  /  /  ')
  w_CLDTOBSO = ctod('  /  /  ')
  w_LISVAL = space(3)
  w_CLFLFIDO = space(1)
  w_TROV = .F.

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CLCODCLI = this.W_CLCODCLI
  * --- Area Manuale = Declare Variables
  * --- gsps_acv
  p_AUT=0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CLI_VEND','gsps_acv')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_acvPag1","gsps_acv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati anagrafici")
      .Pages(1).HelpContextID = 115594759
      .Pages(2).addobject("oPag","tgsps_acvPag2","gsps_acv",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati commerciali")
      .Pages(2).HelpContextID = 115464668
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLCODCLI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='NAZIONI'
    this.cWorkTables[2]='CATECOMM'
    this.cWorkTables[3]='CAT_SCMA'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='LISTINI'
    this.cWorkTables[6]='BUSIUNIT'
    this.cWorkTables[7]='CLI_VEND'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CLI_VEND_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CLI_VEND_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CLCODCLI = NVL(CLCODCLI,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CLI_VEND where CLCODCLI=KeySet.CLCODCLI
    *
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CLI_VEND')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CLI_VEND.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CLI_VEND '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CLCODCLI',this.w_CLCODCLI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESNAZ = space(35)
        .w_FLSCM = space(1)
        .w_DATOBSO = ctod("  /  /  ")
        .w_OBTEST = i_DATSYS
        .w_DESCAT = space(35)
        .w_DESSCM = space(35)
        .w_DESPAG = space(30)
        .w_CODAZI = i_CODAZI
        .w_DESLIS = space(30)
        .w_LISVAL = space(3)
        .w_TROV = .f.
        .w_CLCODCLI = NVL(CLCODCLI,space(15))
        .op_CLCODCLI = .w_CLCODCLI
        .w_CLDESCRI = NVL(CLDESCRI,space(60))
        .w_CLCODCON = NVL(CLCODCON,space(15))
        .w_CLINDIRI = NVL(CLINDIRI,space(35))
        .w_CL___CAP = NVL(CL___CAP,space(8))
        .w_CLLOCALI = NVL(CLLOCALI,space(35))
        .w_CLPROVIN = NVL(CLPROVIN,space(2))
        .w_CLNAZION = NVL(CLNAZION,space(5))
          if link_1_10_joined
            this.w_CLNAZION = NVL(NACODNAZ110,NVL(this.w_CLNAZION,space(5)))
            this.w_DESNAZ = NVL(NADESNAZ110,space(35))
          else
          .link_1_10('Load')
          endif
        .w_CLCODFIS = NVL(CLCODFIS,space(16))
        .w_CLPARIVA = NVL(CLPARIVA,space(12))
        .w_CLTELEFO = NVL(CLTELEFO,space(18))
        .w_CLTELFAX = NVL(CLTELFAX,space(18))
        .w_CLNUMCEL = NVL(CLNUMCEL,space(18))
        .w_CL_EMAIL = NVL(CL_EMAIL,space(254))
        .w_CL_EMPEC = NVL(CL_EMPEC,space(254))
        .w_CLTIPCON = NVL(CLTIPCON,space(1))
        .w_CLCATCOM = NVL(CLCATCOM,space(3))
          if link_2_3_joined
            this.w_CLCATCOM = NVL(CTCODICE203,NVL(this.w_CLCATCOM,space(3)))
            this.w_DESCAT = NVL(CTDESCRI203,space(35))
          else
          .link_2_3('Load')
          endif
        .w_CLCATSCM = NVL(CLCATSCM,space(5))
          if link_2_4_joined
            this.w_CLCATSCM = NVL(CSCODICE204,NVL(this.w_CLCATSCM,space(5)))
            this.w_DESSCM = NVL(CSDESCRI204,space(35))
            this.w_FLSCM = NVL(CSTIPCAT204,space(1))
          else
          .link_2_4('Load')
          endif
        .w_CLCODPAG = NVL(CLCODPAG,space(5))
          if link_2_7_joined
            this.w_CLCODPAG = NVL(PACODICE207,NVL(this.w_CLCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI207,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO207),ctod("  /  /  "))
          else
          .link_2_7('Load')
          endif
        .w_CLCODLIS = NVL(CLCODLIS,space(5))
          if link_2_9_joined
            this.w_CLCODLIS = NVL(LSCODLIS209,NVL(this.w_CLCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS209,space(30))
            this.w_LISVAL = NVL(LSVALLIS209,space(3))
          else
          .link_2_9('Load')
          endif
        .w_CLSCONT1 = NVL(CLSCONT1,0)
        .w_CLSCONT2 = NVL(CLSCONT2,0)
        .w_CLCODNEG = NVL(CLCODNEG,space(3))
          * evitabile
          *.link_2_16('Load')
        .w_CLVALFID = NVL(CLVALFID,0)
        .w_CLFIDUTI = NVL(CLFIDUTI,0)
        .w_CODI = .w_CLCODCLI
        .w_RAG1 = .w_CLDESCRI
        .w_FIDRES1 = .w_CLVALFID-.w_CLFIDUTI
        .w_FIDRES2 = .w_CLVALFID-.w_CLFIDUTI
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .w_CLDTINVA = NVL(cp_ToDate(CLDTINVA),ctod("  /  /  "))
        .w_CLDTOBSO = NVL(cp_ToDate(CLDTOBSO),ctod("  /  /  "))
        .w_CLFLFIDO = NVL(CLFLFIDO,space(1))
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CLI_VEND')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CLCODCLI = space(15)
      .w_CLDESCRI = space(60)
      .w_CLCODCON = space(15)
      .w_CLINDIRI = space(35)
      .w_CL___CAP = space(8)
      .w_CLLOCALI = space(35)
      .w_CLPROVIN = space(2)
      .w_CLNAZION = space(5)
      .w_CLCODFIS = space(16)
      .w_CLPARIVA = space(12)
      .w_CLTELEFO = space(18)
      .w_CLTELFAX = space(18)
      .w_CLNUMCEL = space(18)
      .w_CL_EMAIL = space(254)
      .w_CL_EMPEC = space(254)
      .w_DESNAZ = space(35)
      .w_FLSCM = space(1)
      .w_CLTIPCON = space(1)
      .w_DATOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_CLCATCOM = space(3)
      .w_CLCATSCM = space(5)
      .w_DESCAT = space(35)
      .w_DESSCM = space(35)
      .w_CLCODPAG = space(5)
      .w_DESPAG = space(30)
      .w_CLCODLIS = space(5)
      .w_CODAZI = space(5)
      .w_CLSCONT1 = 0
      .w_CLSCONT2 = 0
      .w_CLCODNEG = space(3)
      .w_CLVALFID = 0
      .w_CLFIDUTI = 0
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_FIDRES1 = 0
      .w_FIDRES2 = 0
      .w_DESLIS = space(30)
      .w_CLDTINVA = ctod("  /  /  ")
      .w_CLDTOBSO = ctod("  /  /  ")
      .w_LISVAL = space(3)
      .w_CLFLFIDO = space(1)
      .w_TROV = .f.
      if .cFunction<>"Filter"
        .DoRTCalc(1,8,.f.)
          if not(empty(.w_CLNAZION))
          .link_1_10('Full')
          endif
          .DoRTCalc(9,17,.f.)
        .w_CLTIPCON = 'C'
          .DoRTCalc(19,19,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_CLCATCOM))
          .link_2_3('Full')
          endif
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_CLCATSCM))
          .link_2_4('Full')
          endif
        .DoRTCalc(23,25,.f.)
          if not(empty(.w_CLCODPAG))
          .link_2_7('Full')
          endif
        .DoRTCalc(26,27,.f.)
          if not(empty(.w_CLCODLIS))
          .link_2_9('Full')
          endif
        .w_CODAZI = i_CODAZI
          .DoRTCalc(29,29,.f.)
        .w_CLSCONT2 = 0
        .w_CLCODNEG = g_CODNEG
        .DoRTCalc(31,31,.f.)
          if not(empty(.w_CLCODNEG))
          .link_2_16('Full')
          endif
          .DoRTCalc(32,33,.f.)
        .w_CODI = .w_CLCODCLI
        .w_RAG1 = .w_CLDESCRI
        .w_FIDRES1 = .w_CLVALFID-.w_CLFIDUTI
        .w_FIDRES2 = .w_CLVALFID-.w_CLFIDUTI
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CLI_VEND')
    this.DoRTCalc(38,43,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    with this	
      if g_FLCPOS='S' AND .p_AUT=0
        cp_AskTableProg(this,i_nConn,"PRCLIPOS","i_codazi,w_CLCODCLI")
      endif
      .op_codazi = .w_codazi
      .op_CLCODCLI = .w_CLCODCLI
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCLCODCLI_1_1.enabled = i_bVal
      .Page1.oPag.oCLDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCLINDIRI_1_5.enabled = i_bVal
      .Page1.oPag.oCL___CAP_1_7.enabled = i_bVal
      .Page1.oPag.oCLLOCALI_1_8.enabled = i_bVal
      .Page1.oPag.oCLPROVIN_1_9.enabled = i_bVal
      .Page1.oPag.oCLNAZION_1_10.enabled = i_bVal
      .Page1.oPag.oCLCODFIS_1_11.enabled = i_bVal
      .Page1.oPag.oCLPARIVA_1_12.enabled = i_bVal
      .Page1.oPag.oCLTELEFO_1_13.enabled = i_bVal
      .Page1.oPag.oCLTELFAX_1_14.enabled = i_bVal
      .Page1.oPag.oCLNUMCEL_1_15.enabled = i_bVal
      .Page1.oPag.oCL_EMAIL_1_16.enabled = i_bVal
      .Page1.oPag.oCL_EMPEC_1_17.enabled = i_bVal
      .Page2.oPag.oCLCATCOM_2_3.enabled = i_bVal
      .Page2.oPag.oCLCATSCM_2_4.enabled = i_bVal
      .Page2.oPag.oCLCODPAG_2_7.enabled = i_bVal
      .Page2.oPag.oCLCODLIS_2_9.enabled = i_bVal
      .Page2.oPag.oCLSCONT1_2_12.enabled = i_bVal
      .Page2.oPag.oCLSCONT2_2_13.enabled = i_bVal
      .Page2.oPag.oCLCODNEG_2_16.enabled = i_bVal
      .Page2.oPag.oCLVALFID_2_17.enabled = i_bVal
      .Page2.oPag.oCLFIDUTI_2_19.enabled = i_bVal
      .Page1.oPag.oCLDTINVA_1_34.enabled = i_bVal
      .Page1.oPag.oCLDTOBSO_1_35.enabled = i_bVal
      .Page2.oPag.oCLFLFIDO_2_30.enabled = i_bVal
      .Page1.oPag.oBtn_1_40.enabled = .Page1.oPag.oBtn_1_40.mCond()
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCLCODCLI_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCLCODCLI_1_1.enabled = .t.
        .Page1.oPag.oCLDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CLI_VEND',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODCLI,"CLCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLDESCRI,"CLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODCON,"CLCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLINDIRI,"CLINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CL___CAP,"CL___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLLOCALI,"CLLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLPROVIN,"CLPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLNAZION,"CLNAZION",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODFIS,"CLCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLPARIVA,"CLPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLTELEFO,"CLTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLTELFAX,"CLTELFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLNUMCEL,"CLNUMCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CL_EMAIL,"CL_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CL_EMPEC,"CL_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLTIPCON,"CLTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCATCOM,"CLCATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCATSCM,"CLCATSCM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODPAG,"CLCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODLIS,"CLCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLSCONT1,"CLSCONT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLSCONT2,"CLSCONT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODNEG,"CLCODNEG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLVALFID,"CLVALFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLFIDUTI,"CLFIDUTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLDTINVA,"CLDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLDTOBSO,"CLDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLFLFIDO,"CLFLFIDO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    i_lTable = "CLI_VEND"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CLI_VEND_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSPS_SCV with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gsps_acv
    ** Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
    local p_POS, p_COD
    p_COD = ALLTRIM(this.w_CLCODCLI)
    FOR p_POS=1 TO LEN(g_MASPOS)
        this.p_AUT = IIF(SUBSTR(p_COD, p_POS, 1)$'0123456789', this.p_AUT, 1)
    ENDFOR
    
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CLI_VEND_IDX,i_nConn)
      with this
        if g_FLCPOS='S' AND .p_AUT=0
          cp_NextTableProg(this,i_nConn,"PRCLIPOS","i_codazi,w_CLCODCLI")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CLI_VEND
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CLI_VEND')
        i_extval=cp_InsertValODBCExtFlds(this,'CLI_VEND')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CLCODCLI,CLDESCRI,CLCODCON,CLINDIRI,CL___CAP"+;
                  ",CLLOCALI,CLPROVIN,CLNAZION,CLCODFIS,CLPARIVA"+;
                  ",CLTELEFO,CLTELFAX,CLNUMCEL,CL_EMAIL,CL_EMPEC"+;
                  ",CLTIPCON,CLCATCOM,CLCATSCM,CLCODPAG,CLCODLIS"+;
                  ",CLSCONT1,CLSCONT2,CLCODNEG,CLVALFID,CLFIDUTI"+;
                  ",CLDTINVA,CLDTOBSO,CLFLFIDO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CLCODCLI)+;
                  ","+cp_ToStrODBC(this.w_CLDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CLCODCON)+;
                  ","+cp_ToStrODBC(this.w_CLINDIRI)+;
                  ","+cp_ToStrODBC(this.w_CL___CAP)+;
                  ","+cp_ToStrODBC(this.w_CLLOCALI)+;
                  ","+cp_ToStrODBC(this.w_CLPROVIN)+;
                  ","+cp_ToStrODBCNull(this.w_CLNAZION)+;
                  ","+cp_ToStrODBC(this.w_CLCODFIS)+;
                  ","+cp_ToStrODBC(this.w_CLPARIVA)+;
                  ","+cp_ToStrODBC(this.w_CLTELEFO)+;
                  ","+cp_ToStrODBC(this.w_CLTELFAX)+;
                  ","+cp_ToStrODBC(this.w_CLNUMCEL)+;
                  ","+cp_ToStrODBC(this.w_CL_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_CL_EMPEC)+;
                  ","+cp_ToStrODBC(this.w_CLTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_CLCATCOM)+;
                  ","+cp_ToStrODBCNull(this.w_CLCATSCM)+;
                  ","+cp_ToStrODBCNull(this.w_CLCODPAG)+;
                  ","+cp_ToStrODBCNull(this.w_CLCODLIS)+;
                  ","+cp_ToStrODBC(this.w_CLSCONT1)+;
                  ","+cp_ToStrODBC(this.w_CLSCONT2)+;
                  ","+cp_ToStrODBCNull(this.w_CLCODNEG)+;
                  ","+cp_ToStrODBC(this.w_CLVALFID)+;
                  ","+cp_ToStrODBC(this.w_CLFIDUTI)+;
                  ","+cp_ToStrODBC(this.w_CLDTINVA)+;
                  ","+cp_ToStrODBC(this.w_CLDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_CLFLFIDO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CLI_VEND')
        i_extval=cp_InsertValVFPExtFlds(this,'CLI_VEND')
        cp_CheckDeletedKey(i_cTable,0,'CLCODCLI',this.w_CLCODCLI)
        INSERT INTO (i_cTable);
              (CLCODCLI,CLDESCRI,CLCODCON,CLINDIRI,CL___CAP,CLLOCALI,CLPROVIN,CLNAZION,CLCODFIS,CLPARIVA,CLTELEFO,CLTELFAX,CLNUMCEL,CL_EMAIL,CL_EMPEC,CLTIPCON,CLCATCOM,CLCATSCM,CLCODPAG,CLCODLIS,CLSCONT1,CLSCONT2,CLCODNEG,CLVALFID,CLFIDUTI,CLDTINVA,CLDTOBSO,CLFLFIDO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CLCODCLI;
                  ,this.w_CLDESCRI;
                  ,this.w_CLCODCON;
                  ,this.w_CLINDIRI;
                  ,this.w_CL___CAP;
                  ,this.w_CLLOCALI;
                  ,this.w_CLPROVIN;
                  ,this.w_CLNAZION;
                  ,this.w_CLCODFIS;
                  ,this.w_CLPARIVA;
                  ,this.w_CLTELEFO;
                  ,this.w_CLTELFAX;
                  ,this.w_CLNUMCEL;
                  ,this.w_CL_EMAIL;
                  ,this.w_CL_EMPEC;
                  ,this.w_CLTIPCON;
                  ,this.w_CLCATCOM;
                  ,this.w_CLCATSCM;
                  ,this.w_CLCODPAG;
                  ,this.w_CLCODLIS;
                  ,this.w_CLSCONT1;
                  ,this.w_CLSCONT2;
                  ,this.w_CLCODNEG;
                  ,this.w_CLVALFID;
                  ,this.w_CLFIDUTI;
                  ,this.w_CLDTINVA;
                  ,this.w_CLDTOBSO;
                  ,this.w_CLFLFIDO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CLI_VEND_IDX,i_nConn)
      *
      * update CLI_VEND
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CLI_VEND')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CLDESCRI="+cp_ToStrODBC(this.w_CLDESCRI)+;
             ",CLCODCON="+cp_ToStrODBC(this.w_CLCODCON)+;
             ",CLINDIRI="+cp_ToStrODBC(this.w_CLINDIRI)+;
             ",CL___CAP="+cp_ToStrODBC(this.w_CL___CAP)+;
             ",CLLOCALI="+cp_ToStrODBC(this.w_CLLOCALI)+;
             ",CLPROVIN="+cp_ToStrODBC(this.w_CLPROVIN)+;
             ",CLNAZION="+cp_ToStrODBCNull(this.w_CLNAZION)+;
             ",CLCODFIS="+cp_ToStrODBC(this.w_CLCODFIS)+;
             ",CLPARIVA="+cp_ToStrODBC(this.w_CLPARIVA)+;
             ",CLTELEFO="+cp_ToStrODBC(this.w_CLTELEFO)+;
             ",CLTELFAX="+cp_ToStrODBC(this.w_CLTELFAX)+;
             ",CLNUMCEL="+cp_ToStrODBC(this.w_CLNUMCEL)+;
             ",CL_EMAIL="+cp_ToStrODBC(this.w_CL_EMAIL)+;
             ",CL_EMPEC="+cp_ToStrODBC(this.w_CL_EMPEC)+;
             ",CLTIPCON="+cp_ToStrODBC(this.w_CLTIPCON)+;
             ",CLCATCOM="+cp_ToStrODBCNull(this.w_CLCATCOM)+;
             ",CLCATSCM="+cp_ToStrODBCNull(this.w_CLCATSCM)+;
             ",CLCODPAG="+cp_ToStrODBCNull(this.w_CLCODPAG)+;
             ",CLCODLIS="+cp_ToStrODBCNull(this.w_CLCODLIS)+;
             ",CLSCONT1="+cp_ToStrODBC(this.w_CLSCONT1)+;
             ",CLSCONT2="+cp_ToStrODBC(this.w_CLSCONT2)+;
             ",CLCODNEG="+cp_ToStrODBCNull(this.w_CLCODNEG)+;
             ",CLVALFID="+cp_ToStrODBC(this.w_CLVALFID)+;
             ",CLFIDUTI="+cp_ToStrODBC(this.w_CLFIDUTI)+;
             ",CLDTINVA="+cp_ToStrODBC(this.w_CLDTINVA)+;
             ",CLDTOBSO="+cp_ToStrODBC(this.w_CLDTOBSO)+;
             ",CLFLFIDO="+cp_ToStrODBC(this.w_CLFLFIDO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CLI_VEND')
        i_cWhere = cp_PKFox(i_cTable  ,'CLCODCLI',this.w_CLCODCLI  )
        UPDATE (i_cTable) SET;
              CLDESCRI=this.w_CLDESCRI;
             ,CLCODCON=this.w_CLCODCON;
             ,CLINDIRI=this.w_CLINDIRI;
             ,CL___CAP=this.w_CL___CAP;
             ,CLLOCALI=this.w_CLLOCALI;
             ,CLPROVIN=this.w_CLPROVIN;
             ,CLNAZION=this.w_CLNAZION;
             ,CLCODFIS=this.w_CLCODFIS;
             ,CLPARIVA=this.w_CLPARIVA;
             ,CLTELEFO=this.w_CLTELEFO;
             ,CLTELFAX=this.w_CLTELFAX;
             ,CLNUMCEL=this.w_CLNUMCEL;
             ,CL_EMAIL=this.w_CL_EMAIL;
             ,CL_EMPEC=this.w_CL_EMPEC;
             ,CLTIPCON=this.w_CLTIPCON;
             ,CLCATCOM=this.w_CLCATCOM;
             ,CLCATSCM=this.w_CLCATSCM;
             ,CLCODPAG=this.w_CLCODPAG;
             ,CLCODLIS=this.w_CLCODLIS;
             ,CLSCONT1=this.w_CLSCONT1;
             ,CLSCONT2=this.w_CLSCONT2;
             ,CLCODNEG=this.w_CLCODNEG;
             ,CLVALFID=this.w_CLVALFID;
             ,CLFIDUTI=this.w_CLFIDUTI;
             ,CLDTINVA=this.w_CLDTINVA;
             ,CLDTOBSO=this.w_CLDTOBSO;
             ,CLFLFIDO=this.w_CLFLFIDO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CLI_VEND_IDX,i_nConn)
      *
      * delete CLI_VEND
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CLCODCLI',this.w_CLCODCLI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,17,.t.)
            .w_CLTIPCON = 'C'
        .DoRTCalc(19,29,.t.)
        if .o_CLSCONT1<>.w_CLSCONT1
            .w_CLSCONT2 = 0
        endif
        .DoRTCalc(31,33,.t.)
            .w_CODI = .w_CLCODCLI
            .w_RAG1 = .w_CLDESCRI
            .w_FIDRES1 = .w_CLVALFID-.w_CLFIDUTI
            .w_FIDRES2 = .w_CLVALFID-.w_CLFIDUTI
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
          if g_FLCPOS='S' AND .p_AUT=0
             cp_AskTableProg(this,i_nConn,"PRCLIPOS","i_codazi,w_CLCODCLI")
          endif
          .op_CLCODCLI = .w_CLCODCLI
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(38,43,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCLDESCRI_1_3.enabled = this.oPgFrm.Page1.oPag.oCLDESCRI_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCLINDIRI_1_5.enabled = this.oPgFrm.Page1.oPag.oCLINDIRI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCL___CAP_1_7.enabled = this.oPgFrm.Page1.oPag.oCL___CAP_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCLLOCALI_1_8.enabled = this.oPgFrm.Page1.oPag.oCLLOCALI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCLPROVIN_1_9.enabled = this.oPgFrm.Page1.oPag.oCLPROVIN_1_9.mCond()
    this.oPgFrm.Page1.oPag.oCLNAZION_1_10.enabled = this.oPgFrm.Page1.oPag.oCLNAZION_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCLCODFIS_1_11.enabled = this.oPgFrm.Page1.oPag.oCLCODFIS_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCLPARIVA_1_12.enabled = this.oPgFrm.Page1.oPag.oCLPARIVA_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCLTELEFO_1_13.enabled = this.oPgFrm.Page1.oPag.oCLTELEFO_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCLTELFAX_1_14.enabled = this.oPgFrm.Page1.oPag.oCLTELFAX_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCLNUMCEL_1_15.enabled = this.oPgFrm.Page1.oPag.oCLNUMCEL_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCL_EMAIL_1_16.enabled = this.oPgFrm.Page1.oPag.oCL_EMAIL_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCL_EMPEC_1_17.enabled = this.oPgFrm.Page1.oPag.oCL_EMPEC_1_17.mCond()
    this.oPgFrm.Page2.oPag.oCLCATCOM_2_3.enabled = this.oPgFrm.Page2.oPag.oCLCATCOM_2_3.mCond()
    this.oPgFrm.Page2.oPag.oCLCATSCM_2_4.enabled = this.oPgFrm.Page2.oPag.oCLCATSCM_2_4.mCond()
    this.oPgFrm.Page2.oPag.oCLCODPAG_2_7.enabled = this.oPgFrm.Page2.oPag.oCLCODPAG_2_7.mCond()
    this.oPgFrm.Page2.oPag.oCLCODLIS_2_9.enabled = this.oPgFrm.Page2.oPag.oCLCODLIS_2_9.mCond()
    this.oPgFrm.Page2.oPag.oCLSCONT1_2_12.enabled = this.oPgFrm.Page2.oPag.oCLSCONT1_2_12.mCond()
    this.oPgFrm.Page2.oPag.oCLSCONT2_2_13.enabled = this.oPgFrm.Page2.oPag.oCLSCONT2_2_13.mCond()
    this.oPgFrm.Page2.oPag.oCLVALFID_2_17.enabled = this.oPgFrm.Page2.oPag.oCLVALFID_2_17.mCond()
    this.oPgFrm.Page2.oPag.oCLFIDUTI_2_19.enabled = this.oPgFrm.Page2.oPag.oCLFIDUTI_2_19.mCond()
    this.oPgFrm.Page1.oPag.oCLDTINVA_1_34.enabled = this.oPgFrm.Page1.oPag.oCLDTINVA_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCLDTOBSO_1_35.enabled = this.oPgFrm.Page1.oPag.oCLDTOBSO_1_35.mCond()
    this.oPgFrm.Page2.oPag.oCLFLFIDO_2_30.enabled = this.oPgFrm.Page2.oPag.oCLFLFIDO_2_30.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oFIDRES1_2_25.visible=!this.oPgFrm.Page2.oPag.oFIDRES1_2_25.mHide()
    this.oPgFrm.Page2.oPag.oFIDRES2_2_26.visible=!this.oPgFrm.Page2.oPag.oFIDRES2_2_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLNAZION
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_CLNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_CLNAZION))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oCLNAZION_1_10'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_CLNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_CLNAZION)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLNAZION = NVL(_Link_.NACODNAZ,space(5))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CLNAZION = space(5)
      endif
      this.w_DESNAZ = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.NACODNAZ as NACODNAZ110"+ ",link_1_10.NADESNAZ as NADESNAZ110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on CLI_VEND.CLNAZION=link_1_10.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and CLI_VEND.CLNAZION=link_1_10.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLCATCOM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CLCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CLCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCLCATCOM_2_3'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CLCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CLCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAT = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CLCATCOM = space(3)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATECOMM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CTCODICE as CTCODICE203"+ ",link_2_3.CTDESCRI as CTDESCRI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on CLI_VEND.CLCATCOM=link_2_3.CTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and CLI_VEND.CLCATCOM=link_2_3.CTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLCATSCM
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCATSCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_CLCATSCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_CLCATSCM))
          select CSCODICE,CSDESCRI,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCATSCM)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLCATSCM) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oCLCATSCM_2_4'),i_cWhere,'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_ACL.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCATSCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_CLCATSCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_CLCATSCM)
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCATSCM = NVL(_Link_.CSCODICE,space(5))
      this.w_DESSCM = NVL(_Link_.CSDESCRI,space(35))
      this.w_FLSCM = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLCATSCM = space(5)
      endif
      this.w_DESSCM = space(35)
      this.w_FLSCM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSCM $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo cliente")
        endif
        this.w_CLCATSCM = space(5)
        this.w_DESSCM = space(35)
        this.w_FLSCM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCATSCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_SCMA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CSCODICE as CSCODICE204"+ ",link_2_4.CSDESCRI as CSDESCRI204"+ ",link_2_4.CSTIPCAT as CSTIPCAT204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on CLI_VEND.CLCATSCM=link_2_4.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and CLI_VEND.CLCATSCM=link_2_4.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLCODPAG
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_CLCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_CLCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_CLCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_CLCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_CLCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCLCODPAG_2_7'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_CLCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_CLCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_CLCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.PACODICE as PACODICE207"+ ","+cp_TransLinkFldName('link_2_7.PADESCRI')+" as PADESCRI207"+ ",link_2_7.PADTOBSO as PADTOBSO207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on CLI_VEND.CLCODPAG=link_2_7.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and CLI_VEND.CLCODPAG=link_2_7.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLCODLIS
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CLCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CLCODLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCLCODLIS_2_9'),i_cWhere,'GSAR_ALI',"Listini articoli",'GSPS_MVD.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CLCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CLCODLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(30))
      this.w_LISVAL = NVL(_Link_.LSVALLIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CLCODLIS = space(5)
      endif
      this.w_DESLIS = space(30)
      this.w_LISVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LISVAL=g_PERVAL
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CLCODLIS = space(5)
        this.w_DESLIS = space(30)
        this.w_LISVAL = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.LSCODLIS as LSCODLIS209"+ ",link_2_9.LSDESLIS as LSDESLIS209"+ ",link_2_9.LSVALLIS as LSVALLIS209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on CLI_VEND.CLCODLIS=link_2_9.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and CLI_VEND.CLCODLIS=link_2_9.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLCODNEG
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODNEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_KAN',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CLCODNEG)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_CODAZI;
                     ,'BUCODICE',trim(this.w_CLCODNEG))
          select BUCODAZI,BUCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCODNEG)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLCODNEG) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCLCODNEG_2_16'),i_cWhere,'GSPS_KAN',"Codici negozi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODNEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CLCODNEG);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_CODAZI;
                       ,'BUCODICE',this.w_CLCODNEG)
            select BUCODAZI,BUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODNEG = NVL(_Link_.BUCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CLCODNEG = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODNEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCLCODCLI_1_1.value==this.w_CLCODCLI)
      this.oPgFrm.Page1.oPag.oCLCODCLI_1_1.value=this.w_CLCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDESCRI_1_3.value==this.w_CLDESCRI)
      this.oPgFrm.Page1.oPag.oCLDESCRI_1_3.value=this.w_CLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCODCON_1_4.value==this.w_CLCODCON)
      this.oPgFrm.Page1.oPag.oCLCODCON_1_4.value=this.w_CLCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCLINDIRI_1_5.value==this.w_CLINDIRI)
      this.oPgFrm.Page1.oPag.oCLINDIRI_1_5.value=this.w_CLINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCL___CAP_1_7.value==this.w_CL___CAP)
      this.oPgFrm.Page1.oPag.oCL___CAP_1_7.value=this.w_CL___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oCLLOCALI_1_8.value==this.w_CLLOCALI)
      this.oPgFrm.Page1.oPag.oCLLOCALI_1_8.value=this.w_CLLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLPROVIN_1_9.value==this.w_CLPROVIN)
      this.oPgFrm.Page1.oPag.oCLPROVIN_1_9.value=this.w_CLPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLNAZION_1_10.value==this.w_CLNAZION)
      this.oPgFrm.Page1.oPag.oCLNAZION_1_10.value=this.w_CLNAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCODFIS_1_11.value==this.w_CLCODFIS)
      this.oPgFrm.Page1.oPag.oCLCODFIS_1_11.value=this.w_CLCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCLPARIVA_1_12.value==this.w_CLPARIVA)
      this.oPgFrm.Page1.oPag.oCLPARIVA_1_12.value=this.w_CLPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCLTELEFO_1_13.value==this.w_CLTELEFO)
      this.oPgFrm.Page1.oPag.oCLTELEFO_1_13.value=this.w_CLTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oCLTELFAX_1_14.value==this.w_CLTELFAX)
      this.oPgFrm.Page1.oPag.oCLTELFAX_1_14.value=this.w_CLTELFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oCLNUMCEL_1_15.value==this.w_CLNUMCEL)
      this.oPgFrm.Page1.oPag.oCLNUMCEL_1_15.value=this.w_CLNUMCEL
    endif
    if not(this.oPgFrm.Page1.oPag.oCL_EMAIL_1_16.value==this.w_CL_EMAIL)
      this.oPgFrm.Page1.oPag.oCL_EMAIL_1_16.value=this.w_CL_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCL_EMPEC_1_17.value==this.w_CL_EMPEC)
      this.oPgFrm.Page1.oPag.oCL_EMPEC_1_17.value=this.w_CL_EMPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ_1_19.value==this.w_DESNAZ)
      this.oPgFrm.Page1.oPag.oDESNAZ_1_19.value=this.w_DESNAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oCLCATCOM_2_3.value==this.w_CLCATCOM)
      this.oPgFrm.Page2.oPag.oCLCATCOM_2_3.value=this.w_CLCATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCLCATSCM_2_4.value==this.w_CLCATSCM)
      this.oPgFrm.Page2.oPag.oCLCATSCM_2_4.value=this.w_CLCATSCM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT_2_5.value==this.w_DESCAT)
      this.oPgFrm.Page2.oPag.oDESCAT_2_5.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSCM_2_6.value==this.w_DESSCM)
      this.oPgFrm.Page2.oPag.oDESSCM_2_6.value=this.w_DESSCM
    endif
    if not(this.oPgFrm.Page2.oPag.oCLCODPAG_2_7.value==this.w_CLCODPAG)
      this.oPgFrm.Page2.oPag.oCLCODPAG_2_7.value=this.w_CLCODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPAG_2_8.value==this.w_DESPAG)
      this.oPgFrm.Page2.oPag.oDESPAG_2_8.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oCLCODLIS_2_9.value==this.w_CLCODLIS)
      this.oPgFrm.Page2.oPag.oCLCODLIS_2_9.value=this.w_CLCODLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oCLSCONT1_2_12.value==this.w_CLSCONT1)
      this.oPgFrm.Page2.oPag.oCLSCONT1_2_12.value=this.w_CLSCONT1
    endif
    if not(this.oPgFrm.Page2.oPag.oCLSCONT2_2_13.value==this.w_CLSCONT2)
      this.oPgFrm.Page2.oPag.oCLSCONT2_2_13.value=this.w_CLSCONT2
    endif
    if not(this.oPgFrm.Page2.oPag.oCLCODNEG_2_16.value==this.w_CLCODNEG)
      this.oPgFrm.Page2.oPag.oCLCODNEG_2_16.value=this.w_CLCODNEG
    endif
    if not(this.oPgFrm.Page2.oPag.oCLVALFID_2_17.value==this.w_CLVALFID)
      this.oPgFrm.Page2.oPag.oCLVALFID_2_17.value=this.w_CLVALFID
    endif
    if not(this.oPgFrm.Page2.oPag.oCLFIDUTI_2_19.value==this.w_CLFIDUTI)
      this.oPgFrm.Page2.oPag.oCLFIDUTI_2_19.value=this.w_CLFIDUTI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_21.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_21.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oRAG1_2_22.value==this.w_RAG1)
      this.oPgFrm.Page2.oPag.oRAG1_2_22.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page2.oPag.oFIDRES1_2_25.value==this.w_FIDRES1)
      this.oPgFrm.Page2.oPag.oFIDRES1_2_25.value=this.w_FIDRES1
    endif
    if not(this.oPgFrm.Page2.oPag.oFIDRES2_2_26.value==this.w_FIDRES2)
      this.oPgFrm.Page2.oPag.oFIDRES2_2_26.value=this.w_FIDRES2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIS_2_28.value==this.w_DESLIS)
      this.oPgFrm.Page2.oPag.oDESLIS_2_28.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDTINVA_1_34.value==this.w_CLDTINVA)
      this.oPgFrm.Page1.oPag.oCLDTINVA_1_34.value=this.w_CLDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDTOBSO_1_35.value==this.w_CLDTOBSO)
      this.oPgFrm.Page1.oPag.oCLDTOBSO_1_35.value=this.w_CLDTOBSO
    endif
    if not(this.oPgFrm.Page2.oPag.oCLFLFIDO_2_30.RadioValue()==this.w_CLFLFIDO)
      this.oPgFrm.Page2.oPag.oCLFLFIDO_2_30.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CLI_VEND')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CLCODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLCODCLI_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CLCODCLI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(GSPS_BPI('gsps_acv', .w_CLCODFIS, "CF",.w_CLCODCLI, .w_CLNAZION))  and (EMPTY(.w_CLCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLCODFIS_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(GSPS_BPI('gsps_acv', .w_CLPARIVA, "PI",.w_CLCODCLI, .w_CLNAZION))  and (EMPTY(.w_CLCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLPARIVA_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FLSCM $ ' C')  and (EMPTY(.w_CLCODCON))  and not(empty(.w_CLCATSCM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLCATSCM_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo cliente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (EMPTY(.w_CLCODCON))  and not(empty(.w_CLCODPAG))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLCODPAG_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(.w_LISVAL=g_PERVAL)  and (EMPTY(.w_CLCODCON))  and not(empty(.w_CLCODLIS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLCODLIS_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_acv
      if i_bRes=.t. AND EMPTY(.w_CLDESCRI)
        i_bnoChk = .f.
        i_bRes = .f.
        i_cErrorMsg = Ah_MsgFormat("Inserire la descrizione cliente")
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CLSCONT1 = this.w_CLSCONT1
    return

enddefine

* --- Define pages as container
define class tgsps_acvPag1 as StdContainer
  Width  = 590
  height = 317
  stdWidth  = 590
  stdheight = 317
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLCODCLI_1_1 as StdField with uid="HIZEDZNEVB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CLCODCLI", cQueryName = "CLCODCLI",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente: i tre caratteri finali sono riservati alla procedura",;
    HelpContextID = 250992529,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=144, Top=12, cSayPict="REPL('X',15)", cGetPict="g_MASPOS", InputMask=replicate('X',15)

  proc oCLCODCLI_1_1.mAfter
    with this.Parent.oContained
      .w_CLCODCLI=PSCALZER(.w_CLCODCLI, .cFunction)
    endwith
  endproc

  add object oCLDESCRI_1_3 as StdField with uid="OPINKALZKM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CLDESCRI", cQueryName = "CLDESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cliente",;
    HelpContextID = 32520303,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=287, Top=12, InputMask=replicate('X',60)

  func oCLDESCRI_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oCLCODCON_1_4 as StdField with uid="XGBSBWBASH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CLCODCON", cQueryName = "CLCODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Rif. codice cliente in azienda",;
    HelpContextID = 17442932,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=441, Top=41, InputMask=replicate('X',15)

  add object oCLINDIRI_1_5 as StdField with uid="LJPHINISUH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CLINDIRI", cQueryName = "CLINDIRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo",;
    HelpContextID = 118065263,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=144, Top=67, InputMask=replicate('X',35)

  func oCLINDIRI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oCL___CAP_1_7 as StdField with uid="VMJUTBOREU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CL___CAP", cQueryName = "CL___CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "CAP",;
    HelpContextID = 46917750,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=144, Top=93, InputMask=replicate('X',8), bHasZoom = .t. 

  func oCL___CAP_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  proc oCL___CAP_1_7.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_CL___CAP",".w_CLLOCALI",".w_CLPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCLLOCALI_1_8 as StdField with uid="RZICOCAPBE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CLLOCALI", cQueryName = "CLLOCALI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Localita",;
    HelpContextID = 17123217,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=221, Top=93, InputMask=replicate('X',35), bHasZoom = .t. 

  func oCLLOCALI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  proc oCLLOCALI_1_8.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_CL___CAP",".w_CLLOCALI",".w_CLPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCLPROVIN_1_9 as StdField with uid="EBOUIDEVHV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CLPROVIN", cQueryName = "CLPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 188876684,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=539, Top=93, InputMask=replicate('X',2), bHasZoom = .t. 

  func oCLPROVIN_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  proc oCLPROVIN_1_9.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_CLPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCLNAZION_1_10 as StdField with uid="TDAJTUVRFF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CLNAZION", cQueryName = "CLNAZION",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Nazione",;
    HelpContextID = 140302452,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=144, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_CLNAZION"

  func oCLNAZION_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  func oCLNAZION_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLNAZION_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLNAZION_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oCLNAZION_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oCLNAZION_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_CLNAZION
     i_obj.ecpSave()
  endproc

  add object oCLCODFIS_1_11 as StdField with uid="EKZJYENJMX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CLCODFIS", cQueryName = "CLCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 200660871,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=144, Top=145, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16)

  func oCLCODFIS_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  func oCLCODFIS_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (GSPS_BPI('gsps_acv', .w_CLCODFIS, "CF",.w_CLCODCLI, .w_CLNAZION))
    endwith
    return bRes
  endfunc

  add object oCLPARIVA_1_12 as StdField with uid="CPTAQKIIAE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CLPARIVA", cQueryName = "CLPARIVA",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 131922023,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=144, Top=171, InputMask=replicate('X',12)

  func oCLPARIVA_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  func oCLPARIVA_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (GSPS_BPI('gsps_acv', .w_CLPARIVA, "PI",.w_CLCODCLI, .w_CLNAZION))
    endwith
    return bRes
  endfunc

  add object oCLTELEFO_1_13 as StdField with uid="ONQWHTWYOU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CLTELEFO", cQueryName = "CLTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico",;
    HelpContextID = 58800245,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=441, Top=145, InputMask=replicate('X',18)

  func oCLTELEFO_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oCLTELFAX_1_14 as StdField with uid="ROFNPMBAFH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CLTELFAX", cQueryName = "CLTELFAX",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero FAX",;
    HelpContextID = 75577470,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=441, Top=171, InputMask=replicate('X',18)

  func oCLTELFAX_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oCLNUMCEL_1_15 as StdField with uid="RKPSZJMFDL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CLNUMCEL", cQueryName = "CLNUMCEL",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero cellulare",;
    HelpContextID = 27318386,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=441, Top=197, InputMask=replicate('X',18)

  func oCLNUMCEL_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oCL_EMAIL_1_16 as StdField with uid="MDJCWLYSSD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CL_EMAIL", cQueryName = "CL_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail",;
    HelpContextID = 7214990,;
   bGlobalFont=.t.,;
    Height=21, Width=430, Left=144, Top=223, InputMask=replicate('X',254)

  func oCL_EMAIL_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oCL_EMPEC_1_17 as StdField with uid="CDNOGVBXKY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CL_EMPEC", cQueryName = "CL_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail PEC",;
    HelpContextID = 244443241,;
   bGlobalFont=.t.,;
    Height=21, Width=430, Left=144, Top=249, InputMask=replicate('X',254)

  func oCL_EMPEC_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oDESNAZ_1_19 as StdField with uid="BYTBUGDRRP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESNAZ", cQueryName = "DESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nazione",;
    HelpContextID = 131735862,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=221, Top=119, InputMask=replicate('X',35)


  add object oObj_1_33 as cp_runprogram with uid="XRJBLACLJR",left=605, top=82, width=188,height=19,;
    caption='GSPS_BAU(C)',;
   bGlobalFont=.t.,;
    prg="GSPS_BAU('C')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 29480251

  add object oCLDTINVA_1_34 as StdField with uid="WIFGPQDNZM",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CLDTINVA", cQueryName = "CLDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 207566951,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=288, Top=285

  func oCLDTINVA_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oCLDTOBSO_1_35 as StdField with uid="RAXPEIHFLO",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CLDTOBSO", cQueryName = "CLDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit�",;
    HelpContextID = 12531829,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=499, Top=285

  func oCLDTOBSO_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc


  add object oObj_1_38 as cp_runprogram with uid="BOFQCKLOZH",left=605, top=105, width=177,height=19,;
    caption='GSPS_BAU(D)',;
   bGlobalFont=.t.,;
    prg="GSPS_BAU('D')",;
    cEvent = "Delete end",;
    nPag=1;
    , HelpContextID = 29480507


  add object oObj_1_39 as cp_runprogram with uid="LOFXESPJVR",left=605, top=128, width=182,height=19,;
    caption='GSPS_BAU(E)',;
   bGlobalFont=.t.,;
    prg="GSPS_BAU('E')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 29480763


  add object oBtn_1_40 as StdButton with uid="BOJMNTCUPJ",left=4, top=269, width=48,height=45,;
    CpPicture="BMP\NonConf.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare le statistiche sintetiche relative al cliente";
    , HelpContextID = 209195073;
    , Caption='\<Statistiche';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSPS_BAU(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="ILXZOYXUTY",Visible=.t., Left=32, Top=12,;
    Alignment=1, Width=109, Height=18,;
    Caption="Codice cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="BSUIYEZFJH",Visible=.t., Left=32, Top=67,;
    Alignment=1, Width=109, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ETIUARUGRB",Visible=.t., Left=32, Top=223,;
    Alignment=1, Width=109, Height=18,;
    Caption="Indirizzo E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="ODQXIZHDKU",Visible=.t., Left=32, Top=93,;
    Alignment=1, Width=109, Height=18,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="AQIIZHEBZI",Visible=.t., Left=494, Top=94,;
    Alignment=1, Width=43, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FWIDXVFDQF",Visible=.t., Left=32, Top=145,;
    Alignment=1, Width=109, Height=18,;
    Caption="Cod. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="SBHJPCQHBJ",Visible=.t., Left=32, Top=171,;
    Alignment=1, Width=109, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="QLNHMTGLDP",Visible=.t., Left=32, Top=119,;
    Alignment=1, Width=109, Height=18,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="ALXKPMSZSJ",Visible=.t., Left=337, Top=145,;
    Alignment=1, Width=104, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VZCBHIXECX",Visible=.t., Left=337, Top=171,;
    Alignment=1, Width=104, Height=18,;
    Caption="Telefax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="VPPPROQFVY",Visible=.t., Left=337, Top=196,;
    Alignment=1, Width=104, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="HLAAZDKXDX",Visible=.t., Left=253, Top=41,;
    Alignment=1, Width=186, Height=18,;
    Caption="Riferimento cliente azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="WAORVCWHUN",Visible=.t., Left=375, Top=287,;
    Alignment=1, Width=123, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="VYDNHVORJA",Visible=.t., Left=173, Top=287,;
    Alignment=1, Width=111, Height=18,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="VOBNJZOJTF",Visible=.t., Left=20, Top=253,;
    Alignment=1, Width=124, Height=18,;
    Caption="Indirizzo e-mail PEC:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsps_acvPag2 as StdContainer
  Width  = 590
  height = 317
  stdWidth  = 590
  stdheight = 317
  resizeXpos=405
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLCATCOM_2_3 as StdField with uid="AWVXYNKFZM",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CLCATCOM", cQueryName = "CLCATCOM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale di appartenenza",;
    HelpContextID = 33302643,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=42, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CLCATCOM"

  func oCLCATCOM_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  func oCLCATCOM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCATCOM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLCATCOM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCLCATCOM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCLCATCOM_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CLCATCOM
     i_obj.ecpSave()
  endproc

  add object oCLCATSCM_2_4 as StdField with uid="HQHFEQNCKY",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CLCATSCM", cQueryName = "CLCATSCM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria sconti / maggiorazioni inesistente o non di tipo cliente",;
    ToolTipText = "Categoria sconti/maggiorazioni",;
    HelpContextID = 33302643,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=70, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_CLCATSCM"

  func oCLCATSCM_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  func oCLCATSCM_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCATSCM_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLCATSCM_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oCLCATSCM_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_ACL.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oCLCATSCM_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_CLCATSCM
     i_obj.ecpSave()
  endproc

  add object oDESCAT_2_5 as StdField with uid="GUCJDSOCOZ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 30351670,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=224, Top=42, InputMask=replicate('X',35)

  add object oDESSCM_2_6 as StdField with uid="QQVDGKJTTM",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESSCM", cQueryName = "DESSCM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 184492342,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=224, Top=70, InputMask=replicate('X',35)

  add object oCLCODPAG_2_7 as StdField with uid="AKEPZSFPPD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CLCODPAG", cQueryName = "CLCODPAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice del pagamento praticato normalmente al cliente",;
    HelpContextID = 235546733,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=98, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_CLCODPAG"

  func oCLCODPAG_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  func oCLCODPAG_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCODPAG_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLCODPAG_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCLCODPAG_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCLCODPAG_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_CLCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_2_8 as StdField with uid="LNHIRUXQRP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 81535286,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=224, Top=98, InputMask=replicate('X',30)

  add object oCLCODLIS_2_9 as StdField with uid="UJSJEVHAME",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CLCODLIS", cQueryName = "CLCODLIS",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino",;
    HelpContextID = 99997575,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=126, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CLCODLIS"

  func oCLCODLIS_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  func oCLCODLIS_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCODLIS_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLCODLIS_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCLCODLIS_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini articoli",'GSPS_MVD.LISTINI_VZM',this.parent.oContained
  endproc
  proc oCLCODLIS_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_CLCODLIS
     i_obj.ecpSave()
  endproc

  add object oCLSCONT1_2_12 as StdField with uid="BGWIPDOGYK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CLSCONT1", cQueryName = "CLSCONT1",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Prima maggiorazione (se positiva) o sconto (se negativa) praticata",;
    HelpContextID = 212805719,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=154, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCLSCONT1_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON))
    endwith
   endif
  endfunc

  add object oCLSCONT2_2_13 as StdField with uid="WJNUXHSSMG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CLSCONT2", cQueryName = "CLSCONT2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Seconda eventuale maggiorazione (se positiva) o sconto (se negativa) praticata",;
    HelpContextID = 212805720,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=234, Top=154, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCLSCONT2_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CLCODCON) AND .w_CLSCONT1<>0)
    endwith
   endif
  endfunc

  add object oCLCODNEG_2_16 as StdField with uid="RZWASVJSZM",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CLCODNEG", cQueryName = "CLCODNEG",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice negozio preferenziale",;
    HelpContextID = 201992301,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=434, Top=154, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSPS_KAN", oKey_1_1="BUCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_CLCODNEG"

  func oCLCODNEG_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCODNEG_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLCODNEG_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCLCODNEG_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_KAN',"Codici negozi",'',this.parent.oContained
  endproc
  proc oCLCODNEG_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSPS_KAN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_CODAZI
     i_obj.w_BUCODICE=this.parent.oContained.w_CLCODNEG
     i_obj.ecpSave()
  endproc

  add object oCLVALFID_2_17 as StdField with uid="BADVFHKDYN",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CLVALFID", cQueryName = "CLVALFID",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Fido accreditato al cliente",;
    HelpContextID = 193111958,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=157, Top=206, cSayPict="v_PV[20]", cGetPict="v_GV[20]"

  func oCLVALFID_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERFID='S' AND .w_CLFLFIDO='S')
    endwith
   endif
  endfunc

  add object oCLFIDUTI_2_19 as StdField with uid="HSBJXKTOVR",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CLFIDUTI", cQueryName = "CLFIDUTI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Fido utilizzato dal cliente",;
    HelpContextID = 50616431,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=157, Top=234, cSayPict="v_PV[20]", cGetPict="v_GV[20]"

  func oCLFIDUTI_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERFID='S' AND .w_CLFLFIDO='S')
    endwith
   endif
  endfunc

  add object oCODI_2_21 as StdField with uid="JNCHVLOARA",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 104580314,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=157, Top=13, InputMask=replicate('X',15)

  add object oRAG1_2_22 as StdField with uid="OKAVJIWEIO",rtseq=35,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 106144234,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=297, Top=13, InputMask=replicate('X',40)

  add object oFIDRES1_2_25 as StdField with uid="MXSEDUVIAZ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_FIDRES1", cQueryName = "FIDRES1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente ancora disponibile",;
    HelpContextID = 18691414,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=157, Top=262, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIDRES1_2_25.mHide()
    with this.Parent.oContained
      return (.w_FIDRES1<0)
    endwith
  endfunc

  add object oFIDRES2_2_26 as StdField with uid="ERNGWCMYDF",rtseq=37,rtrep=.f.,;
    cFormVar = "w_FIDRES2", cQueryName = "FIDRES2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente ancora disponibile",;
    HelpContextID = 18691414,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=157, Top=262, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIDRES2_2_26.mHide()
    with this.Parent.oContained
      return (.w_FIDRES2>=0)
    endwith
  endfunc

  add object oDESLIS_2_28 as StdField with uid="NYLJVMBOZC",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 22552886,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=224, Top=126, InputMask=replicate('X',30)

  add object oCLFLFIDO_2_30 as StdCheck with uid="VIKAZFETEJ",rtseq=42,rtrep=.f.,left=157, top=182, caption="Controllo fido",;
    ToolTipText = "Se attivo: abilita il controllo del fido all'emissione dei documenti",;
    HelpContextID = 120019061,;
    cFormVar="w_CLFLFIDO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCLFLFIDO_2_30.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCLFLFIDO_2_30.GetRadio()
    this.Parent.oContained.w_CLFLFIDO = this.RadioValue()
    return .t.
  endfunc

  func oCLFLFIDO_2_30.SetRadio()
    this.Parent.oContained.w_CLFLFIDO=trim(this.Parent.oContained.w_CLFLFIDO)
    this.value = ;
      iif(this.Parent.oContained.w_CLFLFIDO=='S',1,;
      0)
  endfunc

  func oCLFLFIDO_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERFID='S')
    endwith
   endif
  endfunc

  add object oStr_2_1 as StdString with uid="LNSBTXLSDS",Visible=.t., Left=14, Top=43,;
    Alignment=1, Width=140, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="RBQBAARGAD",Visible=.t., Left=4, Top=71,;
    Alignment=1, Width=150, Height=18,;
    Caption="Categoria sconti/magg.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="MGNYTKXQCV",Visible=.t., Left=28, Top=101,;
    Alignment=1, Width=126, Height=15,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="ICTHTHKNDN",Visible=.t., Left=28, Top=157,;
    Alignment=1, Width=126, Height=15,;
    Caption="Sconti/maggiorazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="OHDDDTCIWJ",Visible=.t., Left=224, Top=157,;
    Alignment=0, Width=7, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="MGFQGOFABB",Visible=.t., Left=57, Top=208,;
    Alignment=1, Width=97, Height=18,;
    Caption="Fido accreditato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="PZDHCGBDZX",Visible=.t., Left=64, Top=235,;
    Alignment=1, Width=90, Height=18,;
    Caption="Fido utilizzato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="KRFRXMKEFV",Visible=.t., Left=45, Top=13,;
    Alignment=1, Width=109, Height=18,;
    Caption="Codice cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_24 as StdString with uid="HOSSDRHSVW",Visible=.t., Left=31, Top=264,;
    Alignment=1, Width=123, Height=15,;
    Caption="Fido disponibile:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_27 as StdString with uid="POXBDKVKLK",Visible=.t., Left=28, Top=129,;
    Alignment=1, Width=126, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="EAOZVBPBTH",Visible=.t., Left=356, Top=157,;
    Alignment=1, Width=75, Height=15,;
    Caption="Negozio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_acv','CLI_VEND','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CLCODCLI=CLI_VEND.CLCODCLI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
