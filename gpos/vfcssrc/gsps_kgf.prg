* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_kgf                                                        *
*              Generazione fatture da corrispettivi                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_247]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-16                                                      *
* Last revis.: 2012-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_kgf",oParentObject))

* --- Class definition
define class tgsps_kgf as StdForm
  Top    = 6
  Left   = 11

  * --- Standard Properties
  Width  = 597
  Height = 373+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-19"
  HelpContextID=32069737
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=58

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CLI_VEND_IDX = 0
  PAR_VDET_IDX = 0
  CAM_AGAZ_IDX = 0
  CAU_CONT_IDX = 0
  PAG_AMEN_IDX = 0
  cPrg = "gsps_kgf"
  cComment = "Generazione fatture da corrispettivi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODCLI = space(15)
  o_CODCLI = space(15)
  w_DESCLI = space(40)
  w_FLNOCL = space(10)
  w_DATINI = ctod('  /  /  ')
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_FLVEAC = space(1)
  w_FILFA = space(2)
  w_NEGFIL = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_TIPDOC = space(5)
  o_TIPDOC = space(5)
  w_DESDOC = space(35)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_CODPAG = space(5)
  w_SELEZI = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_ORAFIN = space(2)
  w_MINFIN = space(2)
  w_NUMSC1 = 0
  w_MATSC1 = space(20)
  w_DATSC1 = ctod('  /  /  ')
  w_NUMSC2 = 0
  w_MATSC2 = space(20)
  w_DATSC2 = ctod('  /  /  ')
  w_NUMSC3 = 0
  w_MATSC3 = space(20)
  w_DATSC3 = ctod('  /  /  ')
  w_NUMSC4 = 0
  w_MATSC4 = space(20)
  w_DATSC4 = ctod('  /  /  ')
  w_NUMSC5 = 0
  w_MATSC5 = space(20)
  w_DATSC5 = ctod('  /  /  ')
  w_DOCFAF = space(5)
  w_CLADOC = space(2)
  w_CAUMAG = space(5)
  w_CODMAT = space(5)
  w_FLRICE = space(1)
  w_CAUCON = space(5)
  w_FLCASC = space(1)
  w_TIPDO1 = space(2)
  w_PRODOC = space(2)
  w_ANNDOC = space(4)
  w_ALLNSC = space(1)
  w_ALLDSC = space(1)
  w_ALLMSC = space(1)
  w_ORMIN1 = space(4)
  w_ORMFI1 = space(4)
  w_ORMINI = space(4)
  w_ORMFIN = space(4)
  w_DESPAG = space(30)
  w_RESCAU = space(5)
  w_STAMPA = 0
  w_ERRORE = .F.
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_kgfPag1","gsps_kgf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsps_kgfPag2","gsps_kgf",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Elenco corrispettivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCLI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(2).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CLI_VEND'
    this.cWorkTables[3]='PAR_VDET'
    this.cWorkTables[4]='CAM_AGAZ'
    this.cWorkTables[5]='CAU_CONT'
    this.cWorkTables[6]='PAG_AMEN'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSPS_BGF(this,"CO")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsps_kgf
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCLI=space(15)
      .w_DESCLI=space(40)
      .w_FLNOCL=space(10)
      .w_DATINI=ctod("  /  /  ")
      .w_CODAZI=space(5)
      .w_FLVEAC=space(1)
      .w_FILFA=space(2)
      .w_NEGFIL=space(3)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPDOC=space(5)
      .w_DESDOC=space(35)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_CODPAG=space(5)
      .w_SELEZI=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_ORAFIN=space(2)
      .w_MINFIN=space(2)
      .w_NUMSC1=0
      .w_MATSC1=space(20)
      .w_DATSC1=ctod("  /  /  ")
      .w_NUMSC2=0
      .w_MATSC2=space(20)
      .w_DATSC2=ctod("  /  /  ")
      .w_NUMSC3=0
      .w_MATSC3=space(20)
      .w_DATSC3=ctod("  /  /  ")
      .w_NUMSC4=0
      .w_MATSC4=space(20)
      .w_DATSC4=ctod("  /  /  ")
      .w_NUMSC5=0
      .w_MATSC5=space(20)
      .w_DATSC5=ctod("  /  /  ")
      .w_DOCFAF=space(5)
      .w_CLADOC=space(2)
      .w_CAUMAG=space(5)
      .w_CODMAT=space(5)
      .w_FLRICE=space(1)
      .w_CAUCON=space(5)
      .w_FLCASC=space(1)
      .w_TIPDO1=space(2)
      .w_PRODOC=space(2)
      .w_ANNDOC=space(4)
      .w_ALLNSC=space(1)
      .w_ALLDSC=space(1)
      .w_ALLMSC=space(1)
      .w_ORMIN1=space(4)
      .w_ORMFI1=space(4)
      .w_ORMINI=space(4)
      .w_ORMFIN=space(4)
      .w_DESPAG=space(30)
      .w_RESCAU=space(5)
      .w_STAMPA=0
      .w_ERRORE=.f.
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODCLI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_FLNOCL = 'S'
          .DoRTCalc(4,4,.f.)
        .w_CODAZI = i_CODAZI
        .w_FLVEAC = 'V'
        .w_FILFA = 'FA'
        .w_NEGFIL = g_CODNEG
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_NEGFIL))
          .link_1_8('Full')
        endif
        .w_OBTEST = i_DATSYS
        .w_TIPDOC = .w_DOCFAF
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_TIPDOC))
          .link_2_1('Full')
        endif
          .DoRTCalc(11,13,.f.)
        .w_DATDOC = i_DATSYS
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODPAG))
          .link_2_6('Full')
        endif
      .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .w_SELEZI = "D"
      .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .DoRTCalc(17,40,.f.)
        if not(empty(.w_CAUMAG))
          .link_2_17('Full')
        endif
        .DoRTCalc(41,43,.f.)
        if not(empty(.w_CAUCON))
          .link_2_20('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_23.Calculate()
          .DoRTCalc(44,46,.f.)
        .w_ANNDOC = STR(YEAR(.w_DATDOC), 4, 0)
        .w_ALLNSC = IIF(.w_NUMSC1+.w_NUMSC2+.w_NUMSC3+.w_NUMSC4+.w_NUMSC5=0, 'S', 'N')
        .w_ALLDSC = IIF(EMPTY(.w_DATSC1) AND EMPTY(.w_DATSC2) AND EMPTY(.w_DATSC3) AND EMPTY(.w_DATSC4) AND EMPTY(.w_DATSC5), 'S', 'N')
        .w_ALLMSC = IIF(EMPTY(.w_MATSC1) AND EMPTY(.w_MATSC2) AND EMPTY(.w_MATSC3) AND EMPTY(.w_MATSC4) AND EMPTY(.w_MATSC5), 'S', 'N')
        .w_ORMIN1 = IIF(EMPTY(.w_ORAINI), '00', RIGHT('00'+ALLTRIM(.w_ORAINI),2))+IIF(EMPTY(.w_MININI), '00', RIGHT('00'+ALLTRIM(.w_MININI),2))
        .w_ORMFI1 = IIF(EMPTY(.w_ORAFIN), '24', RIGHT('00'+ALLTRIM(.w_ORAFIN),2))+IIF(EMPTY(.w_MINFIN), '00', RIGHT('00'+ALLTRIM(.w_MINFIN),2))
        .w_ORMINI = IIF(.w_ORMIN1='0000', SPACE(4), .w_ORMIN1)
        .w_ORMFIN = IIF(.w_ORMFI1='2400', SPACE(4), .w_ORMFI1)
      .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_29.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_30.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_31.Calculate()
          .DoRTCalc(55,57,.f.)
        .w_ERRORE = .f.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_FLVEAC = 'V'
            .w_FILFA = 'FA'
        if .o_CODAZI<>.w_CODAZI
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
        if .o_CODAZI<>.w_CODAZI
            .w_TIPDOC = .w_DOCFAF
          .link_2_1('Full')
        endif
        .DoRTCalc(11,14,.t.)
        if .o_CODCLI<>.w_CODCLI
          .link_2_6('Full')
        endif
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .DoRTCalc(16,39,.t.)
        if .o_TIPDOC<>.w_TIPDOC
          .link_2_17('Full')
        endif
        .DoRTCalc(41,42,.t.)
        if .o_TIPDOC<>.w_TIPDOC
          .link_2_20('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_23.Calculate()
        .DoRTCalc(44,46,.t.)
            .w_ANNDOC = STR(YEAR(.w_DATDOC), 4, 0)
            .w_ALLNSC = IIF(.w_NUMSC1+.w_NUMSC2+.w_NUMSC3+.w_NUMSC4+.w_NUMSC5=0, 'S', 'N')
            .w_ALLDSC = IIF(EMPTY(.w_DATSC1) AND EMPTY(.w_DATSC2) AND EMPTY(.w_DATSC3) AND EMPTY(.w_DATSC4) AND EMPTY(.w_DATSC5), 'S', 'N')
            .w_ALLMSC = IIF(EMPTY(.w_MATSC1) AND EMPTY(.w_MATSC2) AND EMPTY(.w_MATSC3) AND EMPTY(.w_MATSC4) AND EMPTY(.w_MATSC5), 'S', 'N')
            .w_ORMIN1 = IIF(EMPTY(.w_ORAINI), '00', RIGHT('00'+ALLTRIM(.w_ORAINI),2))+IIF(EMPTY(.w_MININI), '00', RIGHT('00'+ALLTRIM(.w_MININI),2))
            .w_ORMFI1 = IIF(EMPTY(.w_ORAFIN), '24', RIGHT('00'+ALLTRIM(.w_ORAFIN),2))+IIF(EMPTY(.w_MINFIN), '00', RIGHT('00'+ALLTRIM(.w_MINFIN),2))
            .w_ORMINI = IIF(.w_ORMIN1='0000', SPACE(4), .w_ORMIN1)
            .w_ORMFIN = IIF(.w_ORMFI1='2400', SPACE(4), .w_ORMFI1)
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_29.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_30.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_31.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(55,58,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_29.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_30.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_31.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_9.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_29.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_30.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ACV',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_CODCLI)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLCODPAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_CODCLI))
          select CLCODCLI,CLDESCRI,CLCODPAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLI)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLI)+"%");

            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLCODPAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStr(trim(this.w_CODCLI)+"%");

            select CLCODCLI,CLDESCRI,CLCODPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLI) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oCODCLI_1_1'),i_cWhere,'GSPS_ACV',"Elenco nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLCODPAG";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI,CLCODPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI,CLCODPAG";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_CODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_CODCLI)
            select CLCODCLI,CLDESCRI,CLCODPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESCLI = NVL(_Link_.CLDESCRI,space(40))
      this.w_CODPAG = NVL(_Link_.CLCODPAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_CODPAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEGFIL
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_VDET_IDX,3]
    i_lTable = "PAR_VDET"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2], .t., this.PAR_VDET_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEGFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEGFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACODNEG,PADOCFAF,PAALFDOC,PACAURES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODNEG="+cp_ToStrODBC(this.w_NEGFIL);
                   +" and PACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_CODAZI;
                       ,'PACODNEG',this.w_NEGFIL)
            select PACODAZI,PACODNEG,PADOCFAF,PAALFDOC,PACAURES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEGFIL = NVL(_Link_.PACODNEG,space(3))
      this.w_DOCFAF = NVL(_Link_.PADOCFAF,space(5))
      this.w_ALFDOC = NVL(_Link_.PAALFDOC,space(10))
      this.w_RESCAU = NVL(_Link_.PACAURES,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NEGFIL = space(3)
      endif
      this.w_DOCFAF = space(5)
      this.w_ALFDOC = space(10)
      this.w_RESCAU = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_VDET_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)+'\'+cp_ToStr(_Link_.PACODNEG,1)
      cp_ShowWarn(i_cKey,this.PAR_VDET_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEGFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG,TDCODMAT,TDPRODOC,TDPRGSTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDCATDOC',this.w_FILFA;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG,TDCODMAT,TDPRODOC,TDPRGSTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_2_1'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);
           .or. this.w_FILFA<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG,TDCODMAT,TDPRODOC,TDPRGSTA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG,TDCODMAT,TDPRODOC,TDPRGSTA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG,TDCODMAT,TDPRODOC,TDPRGSTA";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDCATDOC',oSource.xKey(2);
                       ,'TDTIPDOC',oSource.xKey(3))
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG,TDCODMAT,TDPRODOC,TDPRGSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG,TDCODMAT,TDPRODOC,TDPRGSTA";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_FILFA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDCATDOC',this.w_FILFA;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDFLVEAC,TDCATDOC,TDTIPDOC,TDDESDOC,TDFLRICE,TDCAUCON,TDCAUMAG,TDCODMAT,TDPRODOC,TDPRGSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLRICE = NVL(_Link_.TDFLRICE,space(1))
      this.w_CAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_CAUMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_CODMAT = NVL(_Link_.TDCODMAT,space(5))
      this.w_PRODOC = NVL(_Link_.TDPRODOC,space(2))
      this.w_STAMPA = NVL(_Link_.TDPRGSTA,0)
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_FLVEAC = space(1)
      this.w_CLADOC = space(2)
      this.w_FLRICE = space(1)
      this.w_CAUCON = space(5)
      this.w_CAUMAG = space(5)
      this.w_CODMAT = space(5)
      this.w_PRODOC = space(2)
      this.w_STAMPA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLVEAC='V' AND .w_CLADOC = 'FA' AND EMPTY(.w_CODMAT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_TIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_FLVEAC = space(1)
        this.w_CLADOC = space(2)
        this.w_FLRICE = space(1)
        this.w_CAUCON = space(5)
        this.w_CAUMAG = space(5)
        this.w_CODMAT = space(5)
        this.w_PRODOC = space(2)
        this.w_STAMPA = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPAG
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_CODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_CODPAG))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCODPAG_2_6'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_CODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_CODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMAG
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLCASC";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUMAG)
            select CMCODICE,CMFLCASC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_FLCASC = NVL(_Link_.CMFLCASC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMAG = space(5)
      endif
      this.w_FLCASC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCON
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCON)
            select CCCODICE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_TIPDO1 = NVL(_Link_.CCTIPDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCON = space(5)
      endif
      this.w_TIPDO1 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCLI_1_1.value==this.w_CODCLI)
      this.oPgFrm.Page1.oPag.oCODCLI_1_1.value=this.w_CODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_2.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_2.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOCL_1_3.RadioValue()==this.w_FLNOCL)
      this.oPgFrm.Page1.oPag.oFLNOCL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_4.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDOC_2_1.value==this.w_TIPDOC)
      this.oPgFrm.Page2.oPag.oTIPDOC_2_1.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDOC_2_2.value==this.w_DESDOC)
      this.oPgFrm.Page2.oPag.oDESDOC_2_2.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOC_2_3.value==this.w_NUMDOC)
      this.oPgFrm.Page2.oPag.oNUMDOC_2_3.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOC_2_4.value==this.w_ALFDOC)
      this.oPgFrm.Page2.oPag.oALFDOC_2_4.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDATDOC_2_5.value==this.w_DATDOC)
      this.oPgFrm.Page2.oPag.oDATDOC_2_5.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPAG_2_6.value==this.w_CODPAG)
      this.oPgFrm.Page2.oPag.oCODPAG_2_6.value=this.w_CODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_8.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_12.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_12.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_13.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_13.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_14.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_14.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_15.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_15.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMSC1_1_27.value==this.w_NUMSC1)
      this.oPgFrm.Page1.oPag.oNUMSC1_1_27.value=this.w_NUMSC1
    endif
    if not(this.oPgFrm.Page1.oPag.oMATSC1_1_29.value==this.w_MATSC1)
      this.oPgFrm.Page1.oPag.oMATSC1_1_29.value=this.w_MATSC1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSC1_1_31.value==this.w_DATSC1)
      this.oPgFrm.Page1.oPag.oDATSC1_1_31.value=this.w_DATSC1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMSC2_1_33.value==this.w_NUMSC2)
      this.oPgFrm.Page1.oPag.oNUMSC2_1_33.value=this.w_NUMSC2
    endif
    if not(this.oPgFrm.Page1.oPag.oMATSC2_1_35.value==this.w_MATSC2)
      this.oPgFrm.Page1.oPag.oMATSC2_1_35.value=this.w_MATSC2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSC2_1_37.value==this.w_DATSC2)
      this.oPgFrm.Page1.oPag.oDATSC2_1_37.value=this.w_DATSC2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMSC3_1_39.value==this.w_NUMSC3)
      this.oPgFrm.Page1.oPag.oNUMSC3_1_39.value=this.w_NUMSC3
    endif
    if not(this.oPgFrm.Page1.oPag.oMATSC3_1_41.value==this.w_MATSC3)
      this.oPgFrm.Page1.oPag.oMATSC3_1_41.value=this.w_MATSC3
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSC3_1_43.value==this.w_DATSC3)
      this.oPgFrm.Page1.oPag.oDATSC3_1_43.value=this.w_DATSC3
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMSC4_1_45.value==this.w_NUMSC4)
      this.oPgFrm.Page1.oPag.oNUMSC4_1_45.value=this.w_NUMSC4
    endif
    if not(this.oPgFrm.Page1.oPag.oMATSC4_1_47.value==this.w_MATSC4)
      this.oPgFrm.Page1.oPag.oMATSC4_1_47.value=this.w_MATSC4
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSC4_1_49.value==this.w_DATSC4)
      this.oPgFrm.Page1.oPag.oDATSC4_1_49.value=this.w_DATSC4
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMSC5_1_51.value==this.w_NUMSC5)
      this.oPgFrm.Page1.oPag.oNUMSC5_1_51.value=this.w_NUMSC5
    endif
    if not(this.oPgFrm.Page1.oPag.oMATSC5_1_53.value==this.w_MATSC5)
      this.oPgFrm.Page1.oPag.oMATSC5_1_53.value=this.w_MATSC5
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSC5_1_55.value==this.w_DATSC5)
      this.oPgFrm.Page1.oPag.oDATSC5_1_55.value=this.w_DATSC5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPAG_2_27.value==this.w_DESPAG)
      this.oPgFrm.Page2.oPag.oDESPAG_2_27.value=this.w_DESPAG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLI_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODCLI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TIPDOC)) or not(.w_FLVEAC='V' AND .w_CLADOC = 'FA' AND EMPTY(.w_CODMAT)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oTIPDOC_2_1.SetFocus()
            i_bnoObbl = !empty(.w_TIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   (empty(.w_NUMDOC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNUMDOC_2_3.SetFocus()
            i_bnoObbl = !empty(.w_NUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATDOC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATDOC_2_5.SetFocus()
            i_bnoObbl = !empty(.w_DATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODPAG))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODPAG_2_6.SetFocus()
            i_bnoObbl = !empty(.w_CODPAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORAINI)<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_MININI)<=59)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORAFIN)<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_MINFIN)<=59)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsps_kgf
      if i_bRes=.t. AND .oPgFrm.ActivePage<>2
         *** Salta alla Pagina Dati Chiusura
         i_bnoChk = .t.
         i_bRes = .f.
         .oPgFrm.ActivePage=2
         .NotifyEvent('ActivatePage 2')
      endif
      if i_bRes=.t.
        * --- Verifica congruenza causale Documento
        if empty(.w_TIPDOC) OR (g_COGE<>'S' and .w_FLRICE<>'S') OR (g_COGE='S' and .w_TIPDO1<>'FC')
          i_bRes = .f.
          i_bnoChk = .f.		
         i_cErrorMsg = Ah_MsgFormat("Causale fattura fiscale incongruente (no compresa in corrispettivi)")
        else
          if not empty(.w_FLCASC)
            i_bRes = .f.
            i_bnoChk = .f.		
      	    i_cErrorMsg = Ah_MsgFormat("Causale fattura fiscale incongruente (aggiorna il magazzino)")
          endif
        endif
      endif
      if i_bRes=.t.
        * --- Controlli Progressivo Documento
        .w_ERRORE=.f.
        .NotifyEvent('ControlliFinali')
        if .w_ERRORE=.t.
           i_cErrorMsg = ""
           i_bRes = .f.
           i_bnoChk = .t.
         endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCLI = this.w_CODCLI
    this.o_CODAZI = this.w_CODAZI
    this.o_TIPDOC = this.w_TIPDOC
    return

enddefine

* --- Define pages as container
define class tgsps_kgfPag1 as StdContainer
  Width  = 593
  height = 373
  stdWidth  = 593
  stdheight = 373
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCLI_1_1 as StdField with uid="STBCGFGNLQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo di selezione",;
    HelpContextID = 203307814,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=115, Top=20, cSayPict="g_MASPOS", cGetPict="g_MASPOS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", cZoomOnZoom="GSPS_ACV", oKey_1_1="CLCODCLI", oKey_1_2="this.w_CODCLI"

  proc oCODCLI_1_1.mAfter
    with this.Parent.oContained
      .w_CODCLI=PSCALZER(.w_CODCLI, 'KKK')
    endwith
  endproc

  func oCODCLI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oCODCLI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ACV',"Elenco nominativi",'',this.parent.oContained
  endproc
  proc oCODCLI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLCODCLI=this.parent.oContained.w_CODCLI
     i_obj.ecpSave()
  endproc

  add object oDESCLI_1_2 as StdField with uid="SBGXMFMZZS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 203366710,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=242, Top=20, InputMask=replicate('X',40)

  add object oFLNOCL_1_3 as StdCheck with uid="ASEHXTUHXB",rtseq=3,rtrep=.f.,left=115, top=48, caption="Anche corrispettivi non intestati",;
    ToolTipText = "Se attivo: include nella ricerca anche i corrispettivi non intestati",;
    HelpContextID = 245028950,;
    cFormVar="w_FLNOCL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNOCL_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLNOCL_1_3.GetRadio()
    this.Parent.oContained.w_FLNOCL = this.RadioValue()
    return .t.
  endfunc

  func oFLNOCL_1_3.SetRadio()
    this.Parent.oContained.w_FLNOCL=trim(this.Parent.oContained.w_FLNOCL)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOCL=='S',1,;
      0)
  endfunc

  add object oDATINI_1_4 as StdField with uid="UCRKBPJIPT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione (vuota=no selezione)",;
    HelpContextID = 205860150,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=81

  add object oDATFIN_1_11 as StdField with uid="OXPGRYKRUK",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione (vuota=no selezione)",;
    HelpContextID = 15871286,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=110

  add object oORAINI_1_12 as StdField with uid="NAIRHRTBKF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Ora di inizio selezione (vuota=no selezione)",;
    HelpContextID = 205786854,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=270, Top=81, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI)<=24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_13 as StdField with uid="IMTJLJWMBK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Minuto di inizio selezione (vuota=no selezione)",;
    HelpContextID = 205837766,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=303, Top=81, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI)<=59)
    endwith
    return bRes
  endfunc

  add object oORAFIN_1_14 as StdField with uid="LZDCZEKDSN",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Ora di fine selezione (vuota=no selezione)",;
    HelpContextID = 15797990,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=270, Top=110, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN)<=24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_15 as StdField with uid="HZAVDWOKBW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Minuto di fine selezione (vuota=no selezione)",;
    HelpContextID = 15848902,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=303, Top=110, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN)<=59)
    endwith
    return bRes
  endfunc


  add object oBtn_1_17 as StdButton with uid="PPJFABIUST",left=487, top=322, width=48,height=45,;
    CpPicture="BMP\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza elenco corrispettivi che soddisfano le selezioni impostate";
    , HelpContextID = 128147709;
    , tabstop=.f., Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSPS_BGF(this.Parent.oContained,"VZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="MJNXVZGNOX",left=542, top=322, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24752314;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMSC1_1_27 as StdField with uid="QVFDCHOTIH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_NUMSC1", cQueryName = "NUMSC1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero scontrino da ricercare (0=nessuna ricerca)",;
    HelpContextID = 60740054,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=172, cSayPict='"@Z 999999"', cGetPict='"@Z 999999"'

  add object oMATSC1_1_29 as StdField with uid="IIQZKUORAS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MATSC1", cQueryName = "MATSC1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Matricola scontrino da ricercare (vuoto=nessuna ricerca)",;
    HelpContextID = 60763590,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=258, Top=172, InputMask=replicate('X',20)

  add object oDATSC1_1_31 as StdField with uid="CGUSBDOJRP",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATSC1", cQueryName = "DATSC1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scontrino da ricercare (vuota=nessuna ricerca)",;
    HelpContextID = 60763446,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=469, Top=172

  add object oNUMSC2_1_33 as StdField with uid="LRGZFDICTE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NUMSC2", cQueryName = "NUMSC2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero scontrino da ricercare (0=nessuna ricerca)",;
    HelpContextID = 77517270,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=202, cSayPict='"@Z 999999"', cGetPict='"@Z 999999"'

  add object oMATSC2_1_35 as StdField with uid="TGUIKWOEXS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MATSC2", cQueryName = "MATSC2",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Matricola scontrino da ricercare (vuoto=nessuna ricerca)",;
    HelpContextID = 77540806,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=258, Top=202, InputMask=replicate('X',20)

  add object oDATSC2_1_37 as StdField with uid="YKFWBLZSJJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DATSC2", cQueryName = "DATSC2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scontrino da ricercare (vuota=nessuna ricerca)",;
    HelpContextID = 77540662,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=469, Top=202

  add object oNUMSC3_1_39 as StdField with uid="AESHDKWANH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_NUMSC3", cQueryName = "NUMSC3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero scontrino da ricercare (0=nessuna ricerca)",;
    HelpContextID = 94294486,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=231, cSayPict='"@Z 999999"', cGetPict='"@Z 999999"'

  add object oMATSC3_1_41 as StdField with uid="GNBYHTJHSF",rtseq=30,rtrep=.f.,;
    cFormVar = "w_MATSC3", cQueryName = "MATSC3",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Matricola scontrino da ricercare (vuoto=nessuna ricerca)",;
    HelpContextID = 94318022,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=258, Top=231, InputMask=replicate('X',20)

  add object oDATSC3_1_43 as StdField with uid="CCFUBAMBFN",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DATSC3", cQueryName = "DATSC3",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scontrino da ricercare (vuota=nessuna ricerca)",;
    HelpContextID = 94317878,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=469, Top=231

  add object oNUMSC4_1_45 as StdField with uid="BAKWSMKVNB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_NUMSC4", cQueryName = "NUMSC4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero scontrino da ricercare (0=nessuna ricerca)",;
    HelpContextID = 111071702,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=261, cSayPict='"@Z 999999"', cGetPict='"@Z 999999"'

  add object oMATSC4_1_47 as StdField with uid="BWUKIGRCJU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MATSC4", cQueryName = "MATSC4",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Matricola scontrino da ricercare (vuoto=nessuna ricerca)",;
    HelpContextID = 111095238,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=258, Top=261, InputMask=replicate('X',20)

  add object oDATSC4_1_49 as StdField with uid="YBAICQFOXG",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DATSC4", cQueryName = "DATSC4",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scontrino da ricercare (vuota=nessuna ricerca)",;
    HelpContextID = 111095094,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=469, Top=261

  add object oNUMSC5_1_51 as StdField with uid="BADCGOMFAI",rtseq=35,rtrep=.f.,;
    cFormVar = "w_NUMSC5", cQueryName = "NUMSC5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero scontrino da ricercare (0=nessuna ricerca)",;
    HelpContextID = 127848918,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=292, cSayPict='"@Z 999999"', cGetPict='"@Z 999999"'

  add object oMATSC5_1_53 as StdField with uid="LOAIGNBCRD",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MATSC5", cQueryName = "MATSC5",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Matricola scontrino da ricercare (vuoto=nessuna ricerca)",;
    HelpContextID = 127872454,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=258, Top=292, InputMask=replicate('X',20)

  add object oDATSC5_1_55 as StdField with uid="WCYTOTOTVB",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DATSC5", cQueryName = "DATSC5",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scontrino da ricercare (vuota=nessuna ricerca)",;
    HelpContextID = 127872310,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=469, Top=292


  add object oObj_1_65 as cp_runprogram with uid="EBVGCFPGTX",left=378, top=391, width=203,height=19,;
    caption='GSPS_BGF(VZ)',;
   bGlobalFont=.t.,;
    prg="GSPS_BGF('VZ')",;
    cEvent = "ActivatePage 2",;
    nPag=1;
    , HelpContextID = 158468052

  add object oStr_1_19 as StdString with uid="BIMCAOOUDG",Visible=.t., Left=42, Top=81,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="IRQMLRZJJB",Visible=.t., Left=42, Top=110,;
    Alignment=1, Width=70, Height=18,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EYJGVSEFMG",Visible=.t., Left=205, Top=81,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FJCOTGUOMW",Visible=.t., Left=205, Top=110,;
    Alignment=1, Width=62, Height=18,;
    Caption="A ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="AWOEQDFKLC",Visible=.t., Left=296, Top=81,;
    Alignment=1, Width=8, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="FIFZBKZLAE",Visible=.t., Left=296, Top=110,;
    Alignment=1, Width=8, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="FYNVPPYQSB",Visible=.t., Left=4, Top=143,;
    Alignment=0, Width=164, Height=18,;
    Caption="Riferimenti scontrino"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ITGRWBFMBT",Visible=.t., Left=9, Top=172,;
    Alignment=1, Width=113, Height=18,;
    Caption="Scontrino numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="WTKOZZIOZH",Visible=.t., Left=190, Top=172,;
    Alignment=1, Width=66, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ERHBBHDDAI",Visible=.t., Left=434, Top=172,;
    Alignment=1, Width=32, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="HEXLPYRLZQ",Visible=.t., Left=9, Top=202,;
    Alignment=1, Width=113, Height=18,;
    Caption="Scontrino numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="WKAQLCKSZY",Visible=.t., Left=190, Top=202,;
    Alignment=1, Width=66, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="EOVNABEIWC",Visible=.t., Left=434, Top=202,;
    Alignment=1, Width=32, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="MMKGIIQRPJ",Visible=.t., Left=9, Top=231,;
    Alignment=1, Width=113, Height=18,;
    Caption="Scontrino numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="MNONERCXFF",Visible=.t., Left=190, Top=231,;
    Alignment=1, Width=66, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="FCTLOVKRVW",Visible=.t., Left=434, Top=231,;
    Alignment=1, Width=32, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="DJJIPTIVAU",Visible=.t., Left=9, Top=261,;
    Alignment=1, Width=113, Height=18,;
    Caption="Scontrino numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="NRSLYCJRXV",Visible=.t., Left=190, Top=261,;
    Alignment=1, Width=66, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="PEBKIDNSPM",Visible=.t., Left=434, Top=261,;
    Alignment=1, Width=32, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="OASLGPSRDD",Visible=.t., Left=9, Top=292,;
    Alignment=1, Width=113, Height=18,;
    Caption="Scontrino numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DVVGJVPHQT",Visible=.t., Left=190, Top=292,;
    Alignment=1, Width=66, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="LEXIIZTVKX",Visible=.t., Left=434, Top=292,;
    Alignment=1, Width=32, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="OUXKWQMCOH",Visible=.t., Left=5, Top=20,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice cliente:"  ;
  , bGlobalFont=.t.

  add object oBox_1_16 as StdBox with uid="AHKBTGXZEK",left=4, top=163, width=586,height=2
enddefine
define class tgsps_kgfPag2 as StdContainer
  Width  = 593
  height = 373
  stdWidth  = 593
  stdheight = 373
  resizeXpos=272
  resizeYpos=282
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPDOC_2_1 as StdField with uid="HJNMKCAPBA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Causale documento fattura fiscale",;
    HelpContextID = 105903670,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=167, Top=16, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDCATDOC", oKey_2_2="this.w_FILFA", oKey_3_1="TDTIPDOC", oKey_3_2="this.w_TIPDOC"

  func oTIPDOC_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_FILFA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_FILFA)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oTIPDOC_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
    i_obj.TDCATDOC=w_FILFA
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESDOC_2_2 as StdField with uid="NZLUHJPELA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 105914678,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=233, Top=16, InputMask=replicate('X',35)

  add object oNUMDOC_2_3 as StdField with uid="ZXXUTHULLA",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento da generare",;
    HelpContextID = 105894358,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=167, Top=44, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  add object oALFDOC_2_4 as StdField with uid="LGHDPDHADI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento da generare",;
    HelpContextID = 105863174,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=298, Top=44, InputMask=replicate('X',10)

  add object oDATDOC_2_5 as StdField with uid="SRXRGALYSN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 105917750,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=498, Top=44

  add object oCODPAG_2_6 as StdField with uid="HJJGFDUOVO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODPAG", cQueryName = "CODPAG",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice pagamento",;
    HelpContextID = 159071014,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=167, Top=72, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_CODPAG"

  func oCODPAG_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPAG_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPAG_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCODPAG_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCODPAG_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_CODPAG
     i_obj.ecpSave()
  endproc


  add object ZoomSel as cp_szoombox with uid="AEPLHZWOQV",left=-3, top=100, width=615,height=224,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="COR_RISP",cZoomFile="GSPS_ZGF",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 145927910

  add object oSELEZI_2_8 as StdRadio with uid="KCMBJLDROQ",rtseq=16,rtrep=.f.,left=6, top=327, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_8.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 218149414
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 218149414
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_8.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_8.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_8.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oObj_2_9 as cp_runprogram with uid="BHANWDYKJR",left=13, top=405, width=219,height=19,;
    caption='GSPS_BGF(SS)',;
   bGlobalFont=.t.,;
    prg="GSPS_BGF('SS')",;
    cEvent = "w_SELEZI Changed",;
    nPag=2;
    , HelpContextID = 158497492


  add object oBtn_2_10 as StdButton with uid="WQVAJMKWHR",left=486, top=328, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per generare la fattura";
    , HelpContextID = 32040986;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        GSPS_BGF(this.Parent.oContained,"AG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_11 as StdButton with uid="IKSWHZXFAE",left=541, top=328, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24752314;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_23 as cp_runprogram with uid="XPIOQBGYPF",left=13, top=387, width=434,height=19,;
    caption='GSPS_BGF(ND)',;
   bGlobalFont=.t.,;
    prg="GSPS_BGF('ND')",;
    cEvent = "w_ALFDOC Changed,w_TIPDOC Changed,w_DATDOC Changed",;
    nPag=2;
    , HelpContextID = 158560212

  add object oDESPAG_2_27 as StdField with uid="GBSAMCBZIP",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 159129910,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=233, Top=72, InputMask=replicate('X',30)


  add object oObj_2_29 as cp_runprogram with uid="MFHAOVGEHU",left=248, top=405, width=240,height=19,;
    caption='GSPS_BGF(NU)',;
   bGlobalFont=.t.,;
    prg="GSPS_BGF('NU')",;
    cEvent = "w_NUMDOC Changed",;
    nPag=2;
    , HelpContextID = 158490580


  add object oObj_2_30 as cp_runprogram with uid="EWNDOYJEQF",left=248, top=423, width=205,height=19,;
    caption='GSPS_BGF(NX)',;
   bGlobalFont=.t.,;
    prg="GSPS_BGF('NX')",;
    cEvent = "Init,NewProg",;
    nPag=2;
    , HelpContextID = 158478292


  add object oObj_2_31 as cp_runprogram with uid="SYXQUAUJTO",left=13, top=423, width=192,height=19,;
    caption='GSPS_BGF(CF)',;
   bGlobalFont=.t.,;
    prg="GSPS_BGF('CF')",;
    cEvent = "ControlliFinali",;
    nPag=2;
    , HelpContextID = 158554836

  add object oStr_2_12 as StdString with uid="RQIHYRQPQJ",Visible=.t., Left=24, Top=16,;
    Alignment=1, Width=141, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="HOVDNAQJWB",Visible=.t., Left=286, Top=44,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="JNOBILKAHU",Visible=.t., Left=24, Top=44,;
    Alignment=1, Width=141, Height=18,;
    Caption="Numero documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="FQFFGXFEAF",Visible=.t., Left=392, Top=44,;
    Alignment=1, Width=104, Height=18,;
    Caption="Data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="EHMTVASZBF",Visible=.t., Left=24, Top=72,;
    Alignment=1, Width=141, Height=18,;
    Caption="Codice pagamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_kgf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
