* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_aic                                                        *
*              Codici IVA/reparto cassa                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_24]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-05                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_aic"))

* --- Class definition
define class tgsps_aic as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 474
  Height = 103+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=9001065
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  IVA_CASS_IDX = 0
  REP_ARTI_IDX = 0
  VOCIIVA_IDX = 0
  cFile = "IVA_CASS"
  cKeySelect = "ICCODREP"
  cKeyWhere  = "ICCODREP=this.w_ICCODREP"
  cKeyWhereODBC = '"ICCODREP="+cp_ToStrODBC(this.w_ICCODREP)';

  cKeyWhereODBCqualified = '"IVA_CASS.ICCODREP="+cp_ToStrODBC(this.w_ICCODREP)';

  cPrg = "gsps_aic"
  cComment = "Codici IVA/reparto cassa"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ICCODREP = space(3)
  w_ICCODIVA = space(5)
  w_ICIVAREG = 0
  w_REDESREP = space(40)
  w_IVDESIVA = space(35)
  w_obtest = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'IVA_CASS','gsps_aic')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_aicPag1","gsps_aic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codice iva / reparto cassa")
      .Pages(1).HelpContextID = 79619346
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oICCODREP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='REP_ARTI'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='IVA_CASS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.IVA_CASS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.IVA_CASS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ICCODREP = NVL(ICCODREP,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from IVA_CASS where ICCODREP=KeySet.ICCODREP
    *
    i_nConn = i_TableProp[this.IVA_CASS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_CASS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('IVA_CASS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "IVA_CASS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' IVA_CASS '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ICCODREP',this.w_ICCODREP  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_REDESREP = space(40)
        .w_IVDESIVA = space(35)
        .w_obtest = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_ICCODREP = NVL(ICCODREP,space(3))
          if link_1_1_joined
            this.w_ICCODREP = NVL(RECODREP101,NVL(this.w_ICCODREP,space(3)))
            this.w_REDESREP = NVL(REDESREP101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_ICCODIVA = NVL(ICCODIVA,space(5))
          if link_1_3_joined
            this.w_ICCODIVA = NVL(IVCODIVA103,NVL(this.w_ICCODIVA,space(5)))
            this.w_IVDESIVA = NVL(IVDESIVA103,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO103),ctod("  /  /  "))
          else
          .link_1_3('Load')
          endif
        .w_ICIVAREG = NVL(ICIVAREG,0)
        cp_LoadRecExtFlds(this,'IVA_CASS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ICCODREP = space(3)
      .w_ICCODIVA = space(5)
      .w_ICIVAREG = 0
      .w_REDESREP = space(40)
      .w_IVDESIVA = space(35)
      .w_obtest = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_ICCODREP))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_ICCODIVA))
          .link_1_3('Full')
          endif
          .DoRTCalc(3,5,.f.)
        .w_obtest = i_datsys
      endif
    endwith
    cp_BlankRecExtFlds(this,'IVA_CASS')
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oICCODREP_1_1.enabled = i_bVal
      .Page1.oPag.oICCODIVA_1_3.enabled = i_bVal
      .Page1.oPag.oICIVAREG_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oICCODREP_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oICCODREP_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'IVA_CASS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.IVA_CASS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ICCODREP,"ICCODREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ICCODIVA,"ICCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ICIVAREG,"ICIVAREG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.IVA_CASS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_CASS_IDX,2])
    i_lTable = "IVA_CASS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.IVA_CASS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("..\GPOS\EXE\QUERY\GSPS_AIC.VQR,..\GPOS\EXE\QUERY\GSPS_AIC.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.IVA_CASS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_CASS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.IVA_CASS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into IVA_CASS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'IVA_CASS')
        i_extval=cp_InsertValODBCExtFlds(this,'IVA_CASS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ICCODREP,ICCODIVA,ICIVAREG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_ICCODREP)+;
                  ","+cp_ToStrODBCNull(this.w_ICCODIVA)+;
                  ","+cp_ToStrODBC(this.w_ICIVAREG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'IVA_CASS')
        i_extval=cp_InsertValVFPExtFlds(this,'IVA_CASS')
        cp_CheckDeletedKey(i_cTable,0,'ICCODREP',this.w_ICCODREP)
        INSERT INTO (i_cTable);
              (ICCODREP,ICCODIVA,ICIVAREG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ICCODREP;
                  ,this.w_ICCODIVA;
                  ,this.w_ICIVAREG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.IVA_CASS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_CASS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.IVA_CASS_IDX,i_nConn)
      *
      * update IVA_CASS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'IVA_CASS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ICCODIVA="+cp_ToStrODBCNull(this.w_ICCODIVA)+;
             ",ICIVAREG="+cp_ToStrODBC(this.w_ICIVAREG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'IVA_CASS')
        i_cWhere = cp_PKFox(i_cTable  ,'ICCODREP',this.w_ICCODREP  )
        UPDATE (i_cTable) SET;
              ICCODIVA=this.w_ICCODIVA;
             ,ICIVAREG=this.w_ICIVAREG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.IVA_CASS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_CASS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.IVA_CASS_IDX,i_nConn)
      *
      * delete IVA_CASS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ICCODREP',this.w_ICCODREP  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IVA_CASS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_CASS_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ICCODREP
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ICCODREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_ICCODREP)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_ICCODREP))
          select RECODREP,REDESREP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ICCODREP)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ICCODREP) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oICCODREP_1_1'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ICCODREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_ICCODREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_ICCODREP)
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ICCODREP = NVL(_Link_.RECODREP,space(3))
      this.w_REDESREP = NVL(_Link_.REDESREP,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ICCODREP = space(3)
      endif
      this.w_REDESREP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ICCODREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REP_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.RECODREP as RECODREP101"+ ",link_1_1.REDESREP as REDESREP101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on IVA_CASS.ICCODREP=link_1_1.RECODREP"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and IVA_CASS.ICCODREP=link_1_1.RECODREP(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ICCODIVA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ICCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_ICCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_ICCODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ICCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ICCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oICCODIVA_1_3'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ICCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_ICCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_ICCODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ICCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_IVDESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ICCODIVA = space(5)
      endif
      this.w_IVDESIVA = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ICCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.IVCODIVA as IVCODIVA103"+ ",link_1_3.IVDESIVA as IVDESIVA103"+ ",link_1_3.IVDTOBSO as IVDTOBSO103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on IVA_CASS.ICCODIVA=link_1_3.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and IVA_CASS.ICCODIVA=link_1_3.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oICCODREP_1_1.value==this.w_ICCODREP)
      this.oPgFrm.Page1.oPag.oICCODREP_1_1.value=this.w_ICCODREP
    endif
    if not(this.oPgFrm.Page1.oPag.oICCODIVA_1_3.value==this.w_ICCODIVA)
      this.oPgFrm.Page1.oPag.oICCODIVA_1_3.value=this.w_ICCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oICIVAREG_1_5.value==this.w_ICIVAREG)
      this.oPgFrm.Page1.oPag.oICIVAREG_1_5.value=this.w_ICIVAREG
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESREP_1_7.value==this.w_REDESREP)
      this.oPgFrm.Page1.oPag.oREDESREP_1_7.value=this.w_REDESREP
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDESIVA_1_8.value==this.w_IVDESIVA)
      this.oPgFrm.Page1.oPag.oIVDESIVA_1_8.value=this.w_IVDESIVA
    endif
    cp_SetControlsValueExtFlds(this,'IVA_CASS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ICCODREP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oICCODREP_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ICCODREP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ICCODIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oICCODIVA_1_3.SetFocus()
            i_bnoObbl = !empty(.w_ICCODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_aicPag1 as StdContainer
  Width  = 470
  height = 103
  stdWidth  = 470
  stdheight = 103
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oICCODREP_1_1 as StdField with uid="MUBPTHGQHJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ICCODREP", cQueryName = "ICCODREP",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice reparto",;
    HelpContextID = 101326806,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=16, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_ICCODREP"

  func oICCODREP_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oICCODREP_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oICCODREP_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oICCODREP_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oICCODREP_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_ICCODREP
     i_obj.ecpSave()
  endproc

  add object oICCODIVA_1_3 as StdField with uid="AJRKPCCZML",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ICCODIVA", cQueryName = "ICCODIVA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA che si vuole associare al codice IVA cassa",;
    HelpContextID = 218767303,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=44, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_ICCODIVA"

  func oICCODIVA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oICCODIVA_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oICCODIVA_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oICCODIVA_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oICCODIVA_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_ICCODIVA
     i_obj.ecpSave()
  endproc

  add object oICIVAREG_1_5 as StdField with uid="VKNXLUYIGJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ICIVAREG", cQueryName = "ICIVAREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA cassa",;
    HelpContextID = 98664397,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=109, Top=71, cSayPict='"9"', cGetPict='"9"'

  add object oREDESREP_1_7 as StdField with uid="XCHEAUNWWB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_REDESREP", cQueryName = "REDESREP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 116404838,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=173, Top=16, InputMask=replicate('X',40)

  add object oIVDESIVA_1_8 as StdField with uid="QRKWHBVPHD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IVDESIVA", cQueryName = "IVDESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 233849543,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=173, Top=44, InputMask=replicate('X',35)

  add object oStr_1_2 as StdString with uid="CBLGAYDSWP",Visible=.t., Left=4, Top=16,;
    Alignment=1, Width=102, Height=18,;
    Caption="Codice reparto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="QJLJOVXLFS",Visible=.t., Left=7, Top=44,;
    Alignment=1, Width=99, Height=18,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="PXSSPYEYLM",Visible=.t., Left=4, Top=71,;
    Alignment=1, Width=102, Height=18,;
    Caption="Codice IVA cassa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_aic','IVA_CASS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ICCODREP=IVA_CASS.ICCODREP";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
