* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bpr                                                        *
*              Verifica gruppi merceologici                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-08                                                      *
* Last revis.: 2004-09-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bpr",oParentObject,m.pOPER)
return(i_retval)

define class tgsps_bpr as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_PADRE = .NULL.
  w_CPROWNUM = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSPS_MPR
    * --- Controlla la presenza dei gruppi merceologici nelle righe e rende ineditabile il check mixmatch
    this.w_PADRE = THIS.OPARENTOBJECT
    SELECT (this.w_PADRE.cTrsName)
    this.w_PADRE.MarkPos()     
    this.w_CPROWNUM = CPROWNUM
    GO TOP
    LOCATE FOR NOT EMPTY(NVL(t_PRGRUMER,SPACE(5))) AND NOT DELETED() AND IIF(this.POPER="C",.T.,this.w_CPROWNUM<>CPROWNUM)
    if FOUND()
      this.oParentObject.w_PRESENTE = .T.
    else
      this.oParentObject.w_PRESENTE = .F.
    endif
    this.w_PADRE.RePos()     
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
