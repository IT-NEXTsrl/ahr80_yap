* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bmk                                                        *
*              Vendita negozio - controlli finali                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_156]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-20                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bmk",oParentObject)
return(i_retval)

define class tgsps_bmk as StdBatch
  * --- Local variables
  w_FTOTDOC = 0
  w_FIMPPRE = 0
  w_FPUNFID = 0
  w_OK = .f.
  w_MESS = space(10)
  w_CFUNC = space(10)
  w_CODCLI = space(15)
  w_DATDOC = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_TIPDOC = space(5)
  w_FLPROV = space(1)
  w_FLCONT = space(1)
  w_RECO = 0
  w_FLDEL = .f.
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_NUMRIF = 0
  w_TIPRIG = space(1)
  w_FLARIF = space(1)
  w_FLERIF = space(1)
  w_OFLERIF = space(1)
  w_CODART = space(20)
  w_OQTAIMP = 0
  w_OQTAIM1 = 0
  w_QTAIMP = 0
  w_QTAIM1 = 0
  w_PREZZO = 0
  w_OPREZZO = 0
  w_SRV = space(1)
  w_FLAPP = .f.
  w_EVARIF = space(1)
  w_SERKIT = space(10)
  w_TOTFAT = 0
  w_TOTDDT = 0
  w_OLDFAT = 0
  w_OLDDDT = 0
  w_TOTDOC = 0
  w_FIDRES = 0
  w_NEWCLI = space(15)
  w_AGGRIS = space(1)
  w_OAGGRIS = space(1)
  w_NRECO = 0
  w_PADRE = .NULL.
  w_ORDUBI = space(20)
  w_LOT = space(20)
  w_SALCOM = space(1)
  w_COMMDEFA = space(15)
  w_ARTCOD = space(20)
  w_CODCOM = space(15)
  w_SERDOC = space(10)
  w_MVFLVEAC = space(1)
  w_MVANNDOC = space(4)
  w_MVPRD = space(2)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVPRP = space(2)
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVANNPRO = space(4)
  w_MVCLADOC = space(2)
  w_RIFMAG = space(5)
  w_OFLEVAS = space(1)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_FLRISE = space(1)
  w_KEYSAL = space(40)
  w_OIMPDAE = 0
  w_OIMPEVA = 0
  w_OQTAEV1 = 0
  w_OQTAEVA = 0
  w_OQTAMOV = 0
  w_OQTAUM1 = 0
  w_OQTASAL = 0
  w_NQTAEVA = 0
  w_NQTAEV1 = 0
  w_NFLEVAS = space(1)
  w_NIMPEVA = 0
  w_APPSAL = 0
  w_NQTASAL = 0
  w_FLGIOM = space(20)
  w_MMKEYSAL = space(20)
  w_MMCODMAG = space(5)
  w_MMQTAUM1 = 0
  w_MMFLCASC = space(1)
  w_MMFLORDI = space(1)
  w_MMFLIMPE = space(1)
  w_MMFLRISE = space(1)
  w_MMCODART = space(20)
  w_MMCODCOM = space(15)
  w_DATDOC = ctod("  /  /  ")
  w_QTAUM1 = 0
  w_OLDQTA = 0
  w_FLAGLO = space(1)
  w_CODUBI = space(20)
  w_COART = space(20)
  w_CODLOT = space(20)
  w_COMAG = space(5)
  w_CODMAG = space(5)
  w_DISLOT = space(20)
  w_FLLOTT = space(1)
  w_FLSTAT = space(1)
  w_OLDCODART = space(20)
  w_OLDCODLOT = space(20)
  w_OLDFLSTAT = space(1)
  w_OLDCODART = space(20)
  w_QTAESI = 0
  w_TESDIS = .f.
  w_DMAGCAR = space(5)
  w_DMAGSCA = space(5)
  w_SERIAL = space(10)
  w_NUMRIF = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  DOC_RATE_idx=0
  SALDIART_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  FID_CARD_idx=0
  CLI_VEND_idx=0
  SIT_FIDI_idx=0
  LOTTIART_idx=0
  CON_PAGA_idx=0
  SALDILOT_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  SALOTCOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali sull'archivio Vendita Negozio (da GPSE_MVD)
    * --- Viene eseguito in A.M. Replace End e Delete End 
    this.w_OK = .T.
    * --- Flag Aggiorna Rischio
    this.w_PADRE = this.oParentObject
    this.w_AGGRIS = IIF(this.oParentObject.w_MDTIPCHI $ "NS-ES-BV-RF-FF", "S", this.oParentObject.w_FLRISC)
    this.w_OAGGRIS = IIF(this.oParentObject.w_OTIPCHI $ "NS-ES-BV-RF-FF", "S", this.oParentObject.w_OFLRISC)
    this.w_MESS = ah_Msgformat("Transazione abbandonata")
    this.w_CFUNC = this.w_PADRE.cFunction
    * --- Verifica Esisternza Causale Resi/KIT
    if EMPTY(this.oParentObject.w_CAURES) OR EMPTY(this.oParentObject.w_CARKIT) OR EMPTY(this.oParentObject.w_SCAKIT)
      this.w_MESS = ah_Msgformat("Causali magazzino per resi/kit non definite nei parametri P.O.S.%0Impossibile confermare la vendita")
      this.w_OK = .F.
    endif
    if this.w_OK AND this.w_CFUNC<>"Load"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_OK
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_OK
      * --- Imposta la Deleted OFF per riattivare i record Cancellati con F6 (serve nei Controlli su Doc.Importati)
       
 ah_Msg("Controlli finali...",.T.) 
 OLDSET = SET("DELETED") 
 SET DELETED OFF 
 SELECT (this.w_PADRE.cTrsName)
      this.w_RECO = RECNO()
      GO TOP
      COUNT FOR NOT DELETED() AND NOT EMPTY(t_MDCODART) AND t_MDTIPRIG<>"D" TO this.w_NRECO
      if this.w_NRECO=0
        this.w_MESS = ah_Msgformat("Impossibile salvare: documento senza dettaglio")
        this.w_OK = .F.
      endif
      GO TOP
      * --- Cicla sulle Righe del Documento
      SCAN FOR NOT EMPTY(t_MDCODART)
      * --- Test Se riga Eliminata
      this.w_FLDEL = this.w_CFUNC="Query" OR DELETED()
      this.w_SERRIF = NVL(t_MDSERRIF, SPACE(10))
      this.w_ROWRIF = NVL(t_MDROWRIF, 0)
      this.w_NUMRIF = -20
      this.w_TIPRIG = NVL(t_MDTIPRIG," ")
      this.w_FLARIF = NVL(t_MDFLARIF, " ")
      this.w_FLERIF = NVL(t_MDFLERIF, " ")
      this.w_OFLERIF = NVL(t_OFLERIF, " ")
      this.w_CODART = NVL(t_MDCODART, SPACE(20))
      this.w_OQTAIMP = NVL(t_OQTAIMP, 0)
      this.w_OQTAIM1 = NVL(t_OQTAIM1, 0)
      this.w_QTAIMP = NVL(t_MDQTAIMP, 0)
      this.w_QTAIM1 = NVL(t_MDQTAIM1, 0)
      this.w_PREZZO = NVL(t_MDPREZZO, 0)
      this.w_OPREZZO = NVL(t_OPREZZO, 0)
      this.w_SRV = i_SRV
      * --- Riga aggiunta e poi cancellata, non va considerata
      this.w_FLAPP = this.w_FLDEL AND this.w_SRV="A"
      * --- Test se Evade il Documento di Origine
      this.w_EVARIF = IIF(this.w_FLARIF $ "+-" AND (this.w_SRV $ "UA" OR this.w_FLDEL), "S", "N")
      if NOT EMPTY(this.w_SERRIF) AND this.w_ROWRIF<>0 AND this.w_EVARIF="S" AND this.w_FLAPP=.F.
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_OK And this.w_PADRE.FullRow() And ( this.w_PADRE.RowStatus() $ "A-U" Or this.w_FLDEL)
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT (this.w_PADRE.cTrsName)
      if Not this.w_OK
        * --- Errore!
        EXIT
      endif
      ENDSCAN
      SELECT (this.w_PADRE.cTrsName)
      if this.w_RECO>0 AND this.w_RECO<=RECCOUNT()
        GOTO this.w_RECO
      endif
      * --- set deleted ripristinata al valore originario al tern�mine del Batch
      SET DELETED &OLDSET
      WAIT CLEAR
    endif
    * --- Controllo del Rischio
    if this.w_OK
      * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
      this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
      if g_PERFID="S" AND (this.w_AGGRIS$"SD" OR (this.w_OAGGRIS$"SD" AND this.w_CFUNC<>"Load")) AND NOT EMPTY(this.oParentObject.w_MDCODCLI)
        this.w_TOTFAT = 0
        this.w_TOTDDT = 0
        this.w_OLDFAT = 0
        this.w_OLDDDT = 0
        if this.w_AGGRIS$"SD" AND this.w_CFUNC<>"Query"
          if this.oParentObject.w_MDTIPCHI $ "RS-DT"
            this.w_TOTDDT = this.oParentObject.w_MDPAGCLI
          else
            this.w_TOTFAT = this.oParentObject.w_MDPAGCLI
          endif
        endif
        if this.w_OAGGRIS$"SD" AND this.w_CFUNC<>"Load"
          * --- Eventuale storno Documento
          if this.oParentObject.w_OTIPCHI $ "RS-DT"
            this.w_OLDDDT = this.oParentObject.w_OPAGCLI
          else
            this.w_OLDFAT = this.oParentObject.w_OPAGCLI
          endif
        endif
        this.w_TOTDOC = (this.w_TOTFAT+this.w_TOTDDT) - (this.w_OLDFAT+this.w_OLDDDT)
        if (this.w_TOTFAT - this.w_OLDFAT)<>0 OR (this.w_TOTDDT - this.w_OLDDDT)<>0
          * --- Fido Residuo
          this.w_FIDRES = this.oParentObject.w_VALFID - (this.oParentObject.w_FIDUTI + this.w_TOTDOC)
          if this.w_FIDRES<0 AND this.w_CFUNC<>"Query" AND this.oParentObject.w_FLFIDO="S" 
            this.w_MESS = "Superato massimo importo fido cliente%0Residuo: %1"
            ah_ErrorMsg(this.w_MESS,"!","", ALLTRIM(STR(this.w_FIDRES,18,2)) )
          endif
          * --- Aggiorna Rischio Su Clienti Negozio
          * --- Write into CLI_VEND
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CLI_VEND_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLFIDUTI =CLFIDUTI+ "+cp_ToStrODBC(this.w_TOTDOC);
                +i_ccchkf ;
            +" where ";
                +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_MDCODCLI);
                   )
          else
            update (i_cTable) set;
                CLFIDUTI = CLFIDUTI + this.w_TOTDOC;
                &i_ccchkf. ;
             where;
                CLCODCLI = this.oParentObject.w_MDCODCLI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorna Rischio Su Rischio Clienti
          this.w_CODCLI = SPACE(15)
          * --- Read from CLI_VEND
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLI_VEND_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2],.t.,this.CLI_VEND_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLCODCON"+;
              " from "+i_cTable+" CLI_VEND where ";
                  +"CLCODCLI = "+cp_ToStrODBC(this.oParentObject.w_MDCODCLI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLCODCON;
              from (i_cTable) where;
                  CLCODCLI = this.oParentObject.w_MDCODCLI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCLI = NVL(cp_ToDate(_read_.CLCODCON),cp_NullValue(_read_.CLCODCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_CODCLI) AND NOT this.oParentObject.w_MDTIPCHI $ "NS-ES-BV-RF-FF"
            * --- Aggiorna il Fido Cliente (tranne per Corrispettivo/Fattura Fiscale)
            this.w_NEWCLI = SPACE(15)
            * --- Read from SIT_FIDI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2],.t.,this.SIT_FIDI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "FICODCLI"+;
                " from "+i_cTable+" SIT_FIDI where ";
                    +"FICODCLI = "+cp_ToStrODBC(this.w_CODCLI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                FICODCLI;
                from (i_cTable) where;
                    FICODCLI = this.w_CODCLI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NEWCLI = NVL(cp_ToDate(_read_.FICODCLI),cp_NullValue(_read_.FICODCLI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if EMPTY(this.w_NEWCLI)
              * --- Try
              local bErr_040038A8
              bErr_040038A8=bTrsErr
              this.Try_040038A8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_040038A8
              * --- End
            endif
            * --- Aggiorna Rischio relativo al Documento
            this.w_TOTFAT = this.w_TOTFAT - this.w_OLDFAT
            this.w_TOTDDT = this.w_TOTDDT - this.w_OLDDDT
            if this.oParentObject.w_FLRISC = "D"
              * --- Se l'aggiornamento del rischio sulla causale decrementa il rischio, 
              *     devo invertire il valore da aggiornare
              this.w_TOTFAT = this.w_TOTFAT * -1
              this.w_TOTDDT = this.w_TOTDDT * -1
            endif
            * --- Write into SIT_FIDI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SIT_FIDI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"FIIMPDDT =FIIMPDDT+ "+cp_ToStrODBC(this.w_TOTDDT);
              +",FIIMPFAT =FIIMPFAT+ "+cp_ToStrODBC(this.w_TOTFAT);
                  +i_ccchkf ;
              +" where ";
                  +"FICODCLI = "+cp_ToStrODBC(this.w_CODCLI);
                     )
            else
              update (i_cTable) set;
                  FIIMPDDT = FIIMPDDT + this.w_TOTDDT;
                  ,FIIMPFAT = FIIMPFAT + this.w_TOTFAT;
                  &i_ccchkf. ;
               where;
                  FICODCLI = this.w_CODCLI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    endif
    if this.w_OK And (g_PERLOT="S" OR g_PERUBI="S")
      * --- Controllo Disponibilita' Lotti/Ubicazioni
      this.w_OK = GSAR_BLK( This , "", this.w_PADRE )
    endif
    * --- Elimina Movimenti di Magazzino Associato al Carico KIT / Scarico Componenti
    if this.w_OK AND this.w_CFUNC<>"Load" AND NOT EMPTY(this.oParentObject.w_MDCARKIT) 
      this.w_SERKIT = this.oParentObject.w_MDCARKIT
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_OK AND this.w_CFUNC<>"Load" AND NOT EMPTY(this.oParentObject.w_MDSCAKIT) 
      this.w_SERKIT = this.oParentObject.w_MDSCAKIT
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_OK AND NOT EMPTY(this.oParentObject.w_MDCODFID) 
      if this.w_CFUNC="Load"
        if this.oParentObject.w_FLFAFI = "S"
          * --- Se attivo gestione No Fattura Fidelity aggiorno il totale monte acquisti con importo fidelity
          *     visto che il totale documento rimane a 0 tranne per l'importo non coperto dalla fidelity
          this.w_FTOTDOC = this.oParentObject.w_MDTOTDOC + this.oParentObject.w_MDIMPPRE
        else
          * --- Nell'altro caso aggiorno con totale documento
          this.w_FTOTDOC = this.oParentObject.w_MDTOTDOC
        endif
        * --- Write into FID_CARD
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.FID_CARD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FID_CARD_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FID_CARD_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FCIMPRES =FCIMPRES- "+cp_ToStrODBC(this.oParentObject.w_MDIMPPRE);
          +",FCTOTIMP =FCTOTIMP+ "+cp_ToStrODBC(this.w_FTOTDOC);
          +",FCPUNFID =FCPUNFID+ "+cp_ToStrODBC(this.oParentObject.w_MDPUNFID);
          +",FCNUMVIS =FCNUMVIS+ "+cp_ToStrODBC(1);
          +",FCDATUAC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDDATREG),'FID_CARD','FCDATUAC');
              +i_ccchkf ;
          +" where ";
              +"FCCODFID = "+cp_ToStrODBC(this.oParentObject.w_MDCODFID);
                 )
        else
          update (i_cTable) set;
              FCIMPRES = FCIMPRES - this.oParentObject.w_MDIMPPRE;
              ,FCTOTIMP = FCTOTIMP + this.w_FTOTDOC;
              ,FCPUNFID = FCPUNFID + this.oParentObject.w_MDPUNFID;
              ,FCNUMVIS = FCNUMVIS + 1;
              ,FCDATUAC = this.oParentObject.w_MDDATREG;
              &i_ccchkf. ;
           where;
              FCCODFID = this.oParentObject.w_MDCODFID;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if this.w_CFUNC="Edit"
          * --- In modifica il prepagato non � modificabile, 
          *     quindi aggiorno la fidelity per la differenza tra i due totali documento
          this.w_FTOTDOC = this.oParentObject.w_MDTOTDOC - this.oParentObject.w_OTOTDOC
          this.w_FIMPPRE = this.oParentObject.w_MDIMPPRE - this.oParentObject.w_OIMPPRE
          this.w_FPUNFID = this.oParentObject.w_MDPUNFID - this.oParentObject.w_OPUNFID
          * --- Write into FID_CARD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.FID_CARD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.FID_CARD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.FID_CARD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FCIMPRES =FCIMPRES- "+cp_ToStrODBC(this.w_FIMPPRE);
            +",FCTOTIMP =FCTOTIMP+ "+cp_ToStrODBC(this.w_FTOTDOC);
            +",FCPUNFID =FCPUNFID+ "+cp_ToStrODBC(this.w_FPUNFID);
            +",FCDATUAC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MDDATREG),'FID_CARD','FCDATUAC');
                +i_ccchkf ;
            +" where ";
                +"FCCODFID = "+cp_ToStrODBC(this.oParentObject.w_MDCODFID);
                   )
          else
            update (i_cTable) set;
                FCIMPRES = FCIMPRES - this.w_FIMPPRE;
                ,FCTOTIMP = FCTOTIMP + this.w_FTOTDOC;
                ,FCPUNFID = FCPUNFID + this.w_FPUNFID;
                ,FCDATUAC = this.oParentObject.w_MDDATREG;
                &i_ccchkf. ;
             where;
                FCCODFID = this.oParentObject.w_MDCODFID;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          if this.oParentObject.w_FLFAFI = "S"
            * --- Se no fattura fidelity, storno totale documento + prepagato
            this.w_FTOTDOC = this.oParentObject.w_MDTOTDOC + this.oParentObject.w_MDIMPPRE
          else
            this.w_FTOTDOC = this.oParentObject.w_MDTOTDOC
          endif
          * --- Write into FID_CARD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.FID_CARD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.FID_CARD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.FID_CARD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FCIMPRES =FCIMPRES+ "+cp_ToStrODBC(this.oParentObject.w_MDIMPPRE);
            +",FCTOTIMP =FCTOTIMP- "+cp_ToStrODBC(this.w_FTOTDOC);
            +",FCPUNFID =FCPUNFID- "+cp_ToStrODBC(this.oParentObject.w_MDPUNFID);
            +",FCNUMVIS =FCNUMVIS- "+cp_ToStrODBC(1);
            +",FCDATUAC ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("    -  -  ")),'FID_CARD','FCDATUAC');
                +i_ccchkf ;
            +" where ";
                +"FCCODFID = "+cp_ToStrODBC(this.oParentObject.w_MDCODFID);
                   )
          else
            update (i_cTable) set;
                FCIMPRES = FCIMPRES + this.oParentObject.w_MDIMPPRE;
                ,FCTOTIMP = FCTOTIMP - this.w_FTOTDOC;
                ,FCPUNFID = FCPUNFID - this.oParentObject.w_MDPUNFID;
                ,FCNUMVIS = FCNUMVIS - 1;
                ,FCDATUAC = cp_CharToDate("    -  -  ");
                &i_ccchkf. ;
             where;
                FCCODFID = this.oParentObject.w_MDCODFID;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    if this.w_OK = .F. 
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc
  proc Try_040038A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SIT_FIDI
    i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SIT_FIDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FICODCLI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCLI),'SIT_FIDI','FICODCLI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FICODCLI',this.w_CODCLI)
      insert into (i_cTable) (FICODCLI &i_ccchkf. );
         values (;
           this.w_CODCLI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se Variazione o Cancellazione e la Vendita ha generato un documento
    * --- Nel caso di emissione scontrino con corrispettivo generato, per eliminare lo scontrino devo prima eliminare il Corr generato.
    *     Nel caso di Ricevuta fiscale Immediata invece, la vendita Negozio comanda: 
    *     Non � possibile eliminare il documento. Cancellando la vendita negozio vengono cancellati entrambi
    * --- Se sono chiusure che generano documenti immediati eseguo i controlli e cancello anche il documento generato
    if Not (this.oParentObject.w_MDTIPCHI$"ES-BV-NS") And (Empty(this.oParentObject.w_MDRIFCOR) Or this.oParentObject.w_MDTIPCHI="RF" )
      if NOT EMPTY(this.oParentObject.w_MDRIFDOC) Or (this.oParentObject.w_MDTIPCHI="RF" And Not Empty(this.oParentObject.w_MDRIFCOR))
        this.w_DATDOC = cp_CharToDate("  -  -  ")
        this.w_NUMDOC = 0
        this.w_ALFDOC = Space(10)
        this.w_TIPDOC = SPACE(5)
        this.w_FLPROV = " "
        this.w_FLCONT = " "
        if this.oParentObject.w_MDTIPCHI="RF"
          * --- Se ricevuta fiscale....
          this.w_SERDOC = this.oParentObject.w_MDRIFCOR
        else
          * --- Se documento 
          this.w_SERDOC = this.oParentObject.w_MDRIFDOC
        endif
        * --- Dati per ripristinare il progressivo
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVGENPRO,MVFLCONT,MVFLVEAC,MVANNDOC,MVPRD,MVPRP,MVNUMEST,MVALFEST,MVANNPRO,MVCLADOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVGENPRO,MVFLCONT,MVFLVEAC,MVANNDOC,MVPRD,MVPRP,MVNUMEST,MVALFEST,MVANNPRO,MVCLADOC;
            from (i_cTable) where;
                MVSERIAL = this.w_SERDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          this.w_FLPROV = NVL(cp_ToDate(_read_.MVGENPRO),cp_NullValue(_read_.MVGENPRO))
          this.w_FLCONT = NVL(cp_ToDate(_read_.MVFLCONT),cp_NullValue(_read_.MVFLCONT))
          this.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
          this.w_MVANNDOC = NVL(cp_ToDate(_read_.MVANNDOC),cp_NullValue(_read_.MVANNDOC))
          this.w_MVPRD = NVL(cp_ToDate(_read_.MVPRD),cp_NullValue(_read_.MVPRD))
          this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.w_MVPRP = NVL(cp_ToDate(_read_.MVPRP),cp_NullValue(_read_.MVPRP))
          this.w_MVNUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
          this.w_MVALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
          this.w_MVANNPRO = NVL(cp_ToDate(_read_.MVANNPRO),cp_NullValue(_read_.MVANNPRO))
          this.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Controllo se i dettagli del documento sono stampati nel giornale magazzino
        if ! CHKGIOM(this.oParentObject.w_MDCODESE,this.oParentObject.w_MDCODMAG,this.w_DATDOC,"S")
          this.w_MESS = ah_Msgformat("Documento stampato su g.magazzino")
          this.w_OK = .F.
        endif
        * --- Vendita Associata  Ric.Fiscale/Fattura/DDT (documenti generati Immediatamente)
        do case
          case this.w_FLCONT="S"
            this.w_MESS = ah_Msgformat("Vendita associata ad un documento (%1 n.%2 del %3) contabilizzato",this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ), DTOC(this.w_DATDOC) )
            this.w_OK = .F.
          case this.w_FLPROV="S" AND g_PERAGE="S"
            this.w_MESS = ah_Msgformat("Vendita associata ad un documento (%1 n.%2 del %3) con provvigioni generate",this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ) , DTOC(this.w_DATDOC) )
            this.w_OK = .F.
        endcase
        if this.w_OK
          * --- Select from DOC_DETT
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
                +" where MVSERIAL="+cp_ToStrODBC(this.w_SERDOC)+"";
                 ,"_Curs_DOC_DETT")
          else
            select * from (i_cTable);
             where MVSERIAL=this.w_SERDOC;
              into cursor _Curs_DOC_DETT
          endif
          if used('_Curs_DOC_DETT')
            select _Curs_DOC_DETT
            locate for 1=1
            do while not(eof())
            if NVL(_Curs_DOC_DETT.MVQTAEV1, 0)<>0 OR NVL(_Curs_DOC_DETT.MVIMPEVA, 0)<>0
              if this.w_CFUNC="Query"
                this.w_MESS = ah_Msgformat("Vendita associata ad un documento (%1 n.%2 del %3) evaso da altri documenti%0Impossibile eliminare",this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ) , DTOC(this.w_DATDOC) )
              else
                this.w_MESS = ah_Msgformat("Vendita associata ad un documento (%1 n.%2 del %3) evaso da altri documenti%0Impossibile variare",this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ), DTOC(this.w_DATDOC) )
              endif
              this.w_OK = .F.
            endif
              select _Curs_DOC_DETT
              continue
            enddo
            use
          endif
        endif
        if this.w_OK
          this.w_NUMRIF = -20
          * --- Elimina Documento
          GSAR_BED(this,this.w_SERDOC, this.w_NUMRIF)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Ripristina il progressivo in cancellazione
          if ! this.oparentobject.cfunction="Edit"
            GSAR_BRP(this,this.w_MVFLVEAC, this.w_MVANNDOC, this.w_MVPRD, this.w_MVNUMDOC, this.w_MVALFDOC, this.w_MVPRP, this.w_MVNUMEST, this.w_MVALFEST, this.w_MVANNPRO, this.w_MVCLADOC)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
    else
      * --- Se la vendita � di tipo Emissione scontrino e il corrispettivo � stato generato dalla Generazione Massiva
      *     blocco la cancellazione perch� deve essere prima eliminato il corrispettivo generato.
      *     Se la vendita invece ha generato immediatamente un corrispettivo � la vendita negozio a comandare.
      *     La cancellazione della vendita negozio cancella anche il corrispettivo
      if Not empty(this.oParentObject.w_MDRIFCOR) And INLIST(this.oParentObject.w_MDTIPCHI, "ES", "NS", "BV")
        this.w_DATDOC = cp_CharToDate("  -  -  ")
        this.w_NUMDOC = 0
        this.w_ALFDOC = Space(10)
        this.w_TIPDOC = SPACE(5)
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MDRIFCOR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_MDRIFCOR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Vendita Corrispettivi (documenti generati in differita)
        if this.w_CFUNC="Query"
          this.w_MESS = ah_Msgformat("Vendita associata ad un documento di corrispettivi (%1 n.%2 del %3)%0Per eliminare occorre prima cancellare il documento generato", this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ) ,DTOC(this.w_DATDOC) ) 
        else
          this.w_MESS = ah_Msgformat("Vendita associata ad un documento di corrispettivi (%1 n.%2 del %3)%0Per variare occorre prima cancellare il documento generato", this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ), DTOC(this.w_DATDOC) ) 
        endif
        this.w_OK = .F.
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa Parte viene valutata solo se e' Stata Inserita, Variata o eliminata una Riga associata ad un Documento di Import
    * --- Riaggiorna la Quantita' da Evadere sul Documento Origine e Storna la Stessa dai Saldi
    this.w_RIFMAG = SPACE(5)
    this.w_OFLEVAS = " "
    this.w_FLORDI = " "
    this.w_FLIMPE = " "
    this.w_FLRISE = " "
    this.w_KEYSAL = SPACE(20)
    this.w_OIMPDAE = 0
    this.w_OIMPEVA = 0
    this.w_OQTAEV1 = 0
    this.w_OQTAEVA = 0
    this.w_OQTAMOV = 0
    this.w_OQTAUM1 = 0
    this.w_OQTASAL = 0
    * --- Read from DOC_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVCODMAG,MVFLEVAS,MVFLORDI,MVFLIMPE,MVFLRISE,MVKEYSAL,MVPREZZO,MVIMPEVA,MVQTAEV1,MVQTAEVA,MVQTAUM1,MVQTAMOV,MVQTASAL,MVCODUBI,MVCODLOT,MVCODART,MVCODCOM"+;
        " from "+i_cTable+" DOC_DETT where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVCODMAG,MVFLEVAS,MVFLORDI,MVFLIMPE,MVFLRISE,MVKEYSAL,MVPREZZO,MVIMPEVA,MVQTAEV1,MVQTAEVA,MVQTAUM1,MVQTAMOV,MVQTASAL,MVCODUBI,MVCODLOT,MVCODART,MVCODCOM;
        from (i_cTable) where;
            MVSERIAL = this.w_SERRIF;
            and CPROWNUM = this.w_ROWRIF;
            and MVNUMRIF = this.w_NUMRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_RIFMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
      this.w_OFLEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
      this.w_FLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
      this.w_FLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
      this.w_FLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
      this.w_KEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
      this.w_OIMPDAE = NVL(cp_ToDate(_read_.MVPREZZO),cp_NullValue(_read_.MVPREZZO))
      this.w_OIMPEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
      this.w_OQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
      this.w_OQTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
      this.w_OQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
      this.w_OQTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
      this.w_OQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
      this.w_ORDUBI = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
      this.w_LOT = NVL(cp_ToDate(_read_.MVCODLOT),cp_NullValue(_read_.MVCODLOT))
      this.w_ARTCOD = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
      this.w_CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows<>0
      * --- Se Trovata Riga Corrispondente Calcola la Nuova Qta da Evadere
      this.w_NQTAEVA = this.w_OQTAEVA + ((IIF(this.w_FLDEL, 0, this.w_QTAIMP) - this.w_OQTAIMP) * IIF(this.w_FLARIF="+", 1, -1))
      this.w_NQTAEV1 = this.w_OQTAEV1 + ((IIF(this.w_FLDEL, 0, this.w_QTAIMP) - this.w_OQTAIM1) * IIF(this.w_FLARIF="+", 1, -1))
      this.w_NIMPEVA = this.w_OIMPEVA + IIF(this.w_TIPRIG="F", (IIF(this.w_FLDEL, 0, this.w_QTAIMP) - this.w_OPREZZO) * IIF(this.w_FLARIF="+", 1, -1), 0)
      * --- Se la quantita' Importata sul documento di destinazione e' maggiore della qta presente nel doc. di origine
      * --- l'evasione massima e' fino alla qta totale di quest'ultimo 
      this.w_NQTAEVA = IIF(this.w_NQTAEVA>this.w_OQTAMOV, this.w_OQTAMOV, this.w_NQTAEVA)
      this.w_NQTAEVA = IIF(this.w_NQTAEVA<0, 0, this.w_NQTAEVA)
      * --- Nuova Qta Evasa nella 1^UM
      this.w_NQTAEV1 = IIF(this.w_NQTAEV1>this.w_OQTAUM1, this.w_OQTAUM1, this.w_NQTAEV1)
      this.w_NQTAEV1 = IIF(this.w_NQTAEV1<0, 0, this.w_NQTAEV1)
      * --- Flag Evaso
      this.w_NFLEVAS = IIF(this.w_FLDEL AND this.w_FLERIF="S" OR this.w_FLERIF<>"S" AND this.w_OFLERIF="S", " ", IIF(this.w_FLERIF="S", "S", this.w_OFLEVAS))
      if this.w_TIPRIG="F"
        * --- Riga Forfettaria
        this.w_NIMPEVA = this.w_OIMPEVA
        * --- Se non Evasa Totalmente Azzera le Quantita Evase
        if this.w_NFLEVAS<>"S"
          this.w_NQTAEV1 = 0
          this.w_NQTAEVA = 0
        endif
        this.w_NIMPEVA = IIF(this.w_NIMPEVA>this.w_OIMPDAE, this.w_OIMPDAE, this.w_NIMPEVA)
        this.w_NIMPEVA = IIF(this.w_NIMPEVA<0, 0, this.w_NIMPEVA)
      endif
      * --- Nuova Qta per Saldi
      this.w_NQTASAL = IIF(this.w_NFLEVAS="S" OR this.w_NQTAEV1>this.w_OQTAUM1, 0, this.w_OQTAUM1 - this.w_NQTAEV1)
      * --- Se Evasione da Import tranne Fattura Differita (anche se Rivalorizzo)
      * --- Try
      local bErr_04248D30
      bErr_04248D30=bTrsErr
      this.Try_04248D30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04248D30
      * --- End
      if this.w_OQTASAL<>this.w_NQTASAL AND NOT EMPTY(this.w_KEYSAL)
        * --- Se Variata Qta da Evadere Aggiorna anche i Saldi Ordinato/Impegnato
        this.w_APPSAL = this.w_NQTASAL-this.w_OQTASAL
        if NOT EMPTY(this.w_FLRISE+this.w_FLORDI+this.w_FLIMPE) AND NOT EMPTY(this.w_RIFMAG)
          * --- Try
          local bErr_0424B520
          bErr_0424B520=bTrsErr
          this.Try_0424B520()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0424B520
          * --- End
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_ARTCOD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_ARTCOD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_CODCOM,""))
              this.w_CODCOM = this.w_COMMDEFA
            endif
            * --- Try
            local bErr_0424CF00
            bErr_0424CF00=bTrsErr
            this.Try_0424CF00()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_0424CF00
            * --- End
          endif
        endif
      endif
    endif
  endproc
  proc Try_04248D30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(this.w_NIMPEVA),'DOC_DETT','MVIMPEVA');
      +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_NQTAEVA),'DOC_DETT','MVQTAEVA');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTAEV1),'DOC_DETT','MVQTAEV1');
      +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTASAL),'DOC_DETT','MVQTASAL');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_NFLEVAS),'DOC_DETT','MVFLEVAS');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
             )
    else
      update (i_cTable) set;
          MVIMPEVA = this.w_NIMPEVA;
          ,MVQTAEVA = this.w_NQTAEVA;
          ,MVQTAEV1 = this.w_NQTAEV1;
          ,MVQTASAL = this.w_NQTASAL;
          ,MVFLEVAS = this.w_NFLEVAS;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERRIF;
          and CPROWNUM = this.w_ROWRIF;
          and MVNUMRIF = this.w_NUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0424B520()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
      +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          ,SLQTRPER = &i_cOp3.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_KEYSAL;
          and SLCODMAG = this.w_RIFMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if g_MADV = "S" AND NOT EMPTY (this.w_LOT)
      * --- Write into SALDILOT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDILOT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SUQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SUQTRPER ="+cp_NullLink(i_cOp1,'SALDILOT','SUQTRPER');
            +i_ccchkf ;
        +" where ";
            +"SUCODART = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SUCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
            +" and SUCODUBI = "+cp_ToStrODBC(this.w_ORDUBI);
            +" and SUCODLOT = "+cp_ToStrODBC(this.w_LOT);
               )
      else
        update (i_cTable) set;
            SUQTRPER = &i_cOp1.;
            &i_ccchkf. ;
         where;
            SUCODART = this.w_KEYSAL;
            and SUCODMAG = this.w_RIFMAG;
            and SUCODUBI = this.w_ORDUBI;
            and SUCODLOT = this.w_LOT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_0424CF00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
             )
    else
      update (i_cTable) set;
          SCQTOPER = &i_cOp1.;
          ,SCQTIPER = &i_cOp2.;
          ,SCQTRPER = &i_cOp3.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_KEYSAL;
          and SCCODMAG = this.w_RIFMAG;
          and SCCODCAN = this.w_CODCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    if g_MADV = "S" AND NOT EMPTY (this.w_LOT) AND (g_PERUBI = "S" OR g_PERLOT = "S")
      * --- Write into SALOTCOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALOTCOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SMQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SMQTRPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTRPER');
            +i_ccchkf ;
        +" where ";
            +"SMCODART = "+cp_ToStrODBC(this.w_CODART);
            +" and SMCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
            +" and SMCODUBI = "+cp_ToStrODBC(this.w_ORDUBI);
            +" and SMCODLOT = "+cp_ToStrODBC(this.w_LOT);
               )
      else
        update (i_cTable) set;
            SMQTRPER = &i_cOp1.;
            &i_ccchkf. ;
         where;
            SMCODART = this.w_CODART;
            and SMCODMAG = this.w_RIFMAG;
            and SMCODUBI = this.w_ORDUBI;
            and SMCODLOT = this.w_LOT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione Documento di Evasione Componenti/Prodotti Finiti
    * --- Read from MVM_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2],.t.,this.MVM_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MMDATDOC"+;
        " from "+i_cTable+" MVM_MAST where ";
            +"MMSERIAL = "+cp_ToStrODBC(this.w_SERKIT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MMDATDOC;
        from (i_cTable) where;
            MMSERIAL = this.w_SERKIT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATDOC = NVL(cp_ToDate(_read_.MMDATDOC),cp_NullValue(_read_.MMDATDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo se il mov. di magazzino � stampato nel giornale magazzino
    if ! CHKGIOM(this.oParentObject.w_MDCODESE,this.oParentObject.w_MDCODMAG,this.w_DATDOC,"S")
      * --- Se Stampato G.M. e Cancellazione elimina solo il Riferimento
      if this.w_CFUNC="Query"
        * --- Try
        local bErr_04257DB0
        bErr_04257DB0=bTrsErr
        this.Try_04257DB0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Errore in scrittura movimento di carico KIT")
        endif
        bTrsErr=bTrsErr or bErr_04257DB0
        * --- End
      else
        * --- Altrimenti Inibisce la Modifica
        this.w_MESS = ah_Msgformat("Movimento di magazzino associato ai KIT stampato su G.M.; impossibile variare")
        this.w_OK = .F.
      endif
    else
      * --- Select from MVM_DETT
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MVM_DETT ";
            +" where MMSERIAL="+cp_ToStrODBC(this.w_SERKIT)+"";
             ,"_Curs_MVM_DETT")
      else
        select * from (i_cTable);
         where MMSERIAL=this.w_SERKIT;
          into cursor _Curs_MVM_DETT
      endif
      if used('_Curs_MVM_DETT')
        select _Curs_MVM_DETT
        locate for 1=1
        do while not(eof())
        if NOT EMPTY(NVL(_Curs_MVM_DETT.MMKEYSAL,"")) AND NOT EMPTY(NVL(_Curs_MVM_DETT.MMCODMAG,"")) AND NVL(_Curs_MVM_DETT.MMQTAUM1,0)<>0
          this.w_MMKEYSAL = _Curs_MVM_DETT.MMKEYSAL
          this.w_MMCODART = _Curs_MVM_DETT.MMCODART
          this.w_MMCODMAG = _Curs_MVM_DETT.MMCODMAG
          this.w_MMQTAUM1 = _Curs_MVM_DETT.MMQTAUM1
          this.w_MMFLCASC = NVL(_Curs_MVM_DETT.MMFLCASC," ")
          this.w_MMFLORDI = NVL(_Curs_MVM_DETT.MMFLORDI, " ")
          this.w_MMFLIMPE = NVL(_Curs_MVM_DETT.MMFLIMPE, " ")
          this.w_MMFLRISE = NVL(_Curs_MVM_DETT.MMFLRISE, " ")
          this.w_MMFLCASC = IIF(this.w_MMFLCASC="+", "-", IIF(this.w_MMFLCASC="-", "+", " "))
          this.w_MMFLORDI = IIF(this.w_MMFLORDI="+", "-", IIF(this.w_MMFLORDI="-", "+", " "))
          this.w_MMFLIMPE = IIF(this.w_MMFLIMPE="+", "-", IIF(this.w_MMFLIMPE="-", "+", " "))
          this.w_MMFLRISE = IIF(this.w_MMFLRISE="+", "-", IIF(this.w_MMFLRISE="-", "+", " "))
          this.w_MMCODCOM = _Curs_MVM_DETT.MMCODCOM
          if NOT EMPTY(this.w_MMFLCASC+this.w_MMFLRISE+this.w_MMFLORDI+this.w_MMFLIMPE)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
              +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
              +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTAPER = &i_cOp1.;
                  ,SLQTRPER = &i_cOp2.;
                  ,SLQTOPER = &i_cOp3.;
                  ,SLQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_MMKEYSAL;
                  and SLCODMAG = this.w_MMCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_MMCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_MMCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SALCOM="S"
              if empty(nvl(this.w_MMCODCOM,""))
                this.w_MMCODCOM = this.w_COMMDEFA
              endif
              * --- Try
              local bErr_04287CE8
              bErr_04287CE8=bTrsErr
              this.Try_04287CE8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04287CE8
              * --- End
            endif
          endif
        endif
          select _Curs_MVM_DETT
          continue
        enddo
        use
      endif
      * --- Try
      local bErr_0425B290
      bErr_0425B290=bTrsErr
      this.Try_0425B290()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_Errormsg("Errore in eliminazione movimento di magazzino KIT (detail)")
      endif
      bTrsErr=bTrsErr or bErr_0425B290
      * --- End
      * --- Try
      local bErr_0425BA70
      bErr_0425BA70=bTrsErr
      this.Try_0425BA70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_Errormsg("Errore in eliminazione movimento di magazzino KIT (testata)")
      endif
      bTrsErr=bTrsErr or bErr_0425BA70
      * --- End
    endif
  endproc
  proc Try_04257DB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MVM_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMSERPOS ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'MVM_MAST','MMSERPOS');
          +i_ccchkf ;
      +" where ";
          +"MMSERIAL = "+cp_ToStrODBC(this.w_SERKIT);
             )
    else
      update (i_cTable) set;
          MMSERPOS = SPACE(10);
          &i_ccchkf. ;
       where;
          MMSERIAL = this.w_SERKIT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04287CE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SCQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SCQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SCQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SCQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
      +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_MMCODCOM);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          ,SCQTRPER = &i_cOp2.;
          ,SCQTOPER = &i_cOp3.;
          ,SCQTIPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_MMKEYSAL;
          and SCCODMAG = this.w_MMCODMAG;
          and SCCODCAN = this.w_MMCODCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_0425B290()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from MVM_DETT
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MMSERIAL = "+cp_ToStrODBC(this.w_SERKIT);
             )
    else
      delete from (i_cTable) where;
            MMSERIAL = this.w_SERKIT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0425BA70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from MVM_MAST
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MMSERIAL = "+cp_ToStrODBC(this.w_SERKIT);
             )
    else
      delete from (i_cTable) where;
            MMSERIAL = this.w_SERKIT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica lotti ubicazioni
    this.w_CODMAG = NVL(t_MDMAGRIG, Space(5))
    this.w_FLAGLO = NVL(t_MDFLLOTT," ")
    this.w_CODLOT = NVL(t_MDCODLOT,Space(20))
    this.w_CODUBI = NVL(t_MDCODUBI,Space(20))
    this.w_COMAG = this.w_CODMAG
    this.w_OLDQTA = NVL( this.w_PADRE. Get( "MDQTAUM1" ) ,0)
    this.w_QTAUM1 = NVL(t_MDQTAUM1, 0)
    this.w_COART = NVL(t_MDCODART, Space(20))
    this.w_DISLOT = NVL(t_DISLOT, " ")
    this.w_FLLOTT = NVL(t_FLLOTT, " ")
    this.w_FLSTAT = NVL(t_FLSTAT, " ")
    * --- Recupera dal transitorio il vecchio valore del codice lotto e codice articolo eventualmente modificati per aggiornare lo status
    this.w_OLDCODLOT = NVL(this.w_PADRE.GET("MDCODLOT"), this.w_CODLOT)
    this.w_OLDCODART = NVL(this.w_PADRE.GET("MDCODART"), this.w_COART)
    if this.w_OK And (((g_PERLOT="S" AND this.w_FLLOTT $ "SC" ) OR g_PERUBI="S") AND this.w_TIPRIG $ "RM" )
      * --- Leggo la disponibilit� del lotto
      this.w_QTAESI = 0
      this.w_TESDIS = .F.
      * --- Select from SALDILOT
      i_nConn=i_TableProp[this.SALDILOT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select SUM(SUQTAPER) as SUQTAPER , SUM(SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
            +" where "+cp_ToStrODBC(this.w_COART)+"=SUCODART AND "+cp_ToStrODBC(this.w_CODLOT)+"=SUCODLOT";
             ,"_Curs_SALDILOT")
      else
        select SUM(SUQTAPER) as SUQTAPER , SUM(SUQTRPER) as SUQTRPER from (i_cTable);
         where this.w_COART=SUCODART AND this.w_CODLOT=SUCODLOT;
          into cursor _Curs_SALDILOT
      endif
      if used('_Curs_SALDILOT')
        select _Curs_SALDILOT
        locate for 1=1
        do while not(eof())
        this.w_TESDIS = .T.
        this.w_QTAESI = _Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
          select _Curs_SALDILOT
          continue
        enddo
        use
      endif
      do case
        case this.w_FLSTAT="E" 
          if (((this.w_QTAUM1<this.w_OLDQTA OR this.w_FLDEL ) AND this.w_FLAGLO="-" ) OR (this.w_QTAUM1>this.w_OLDQTA AND this.w_FLAGLO="+")) 
            * --- Aggiorno Status in caso di Modifica della Quantit� o Cancellazione di uno Scarico
            if this.w_TESDIS AND this.w_QTAESI>0
              * --- Write into LOTTIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.LOTTIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
                    +i_ccchkf ;
                +" where ";
                    +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                    +" and LOCODART = "+cp_ToStrODBC(this.w_COART);
                       )
              else
                update (i_cTable) set;
                    LOFLSTAT = "D";
                    &i_ccchkf. ;
                 where;
                    LOCODICE = this.w_CODLOT;
                    and LOCODART = this.w_COART;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='errore scrittura lotti'
                return
              endif
            endif
          else
            if EMPTY(this.w_SERRIF) and this.w_OLDQTA<>this.w_QTAUM1 AND this.w_DISLOT="S"
              this.w_MESS = ah_Msgformat("Disponibilit� lotto: %1 esaurita", ALLTRIM(this.w_CODLOT) )
              this.w_OK = .F.
            endif
          endif
        case this.w_FLSTAT="S"
          if this.w_QTAUM1>this.w_OLDQTA And Not this.w_FLDEL
            this.w_MESS = ah_Msgformat("Lotto: %1 sospeso", ALLTRIM(this.w_CODLOT) )
            this.w_OK = .F.
          endif
        otherwise
          * --- w_FLSTAT='D'
          if this.w_TESDIS AND this.w_QTAESI<=0 AND this.w_FLAGLO$"+-"
            * --- Lotto esaurito
            * --- Write into LOTTIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.LOTTIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("E"),'LOTTIART','LOFLSTAT');
                  +i_ccchkf ;
              +" where ";
                  +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                  +" and LOCODART = "+cp_ToStrODBC(this.w_COART);
                     )
            else
              update (i_cTable) set;
                  LOFLSTAT = "E";
                  &i_ccchkf. ;
               where;
                  LOCODICE = this.w_CODLOT;
                  and LOCODART = this.w_COART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
      endcase
      if (this.w_CODLOT<>this.w_OLDCODLOT OR this.w_COART<>this.w_OLDCODART) AND this.w_CFUNC="Edit"
        * --- Read from LOTTIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LOTTIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LOFLSTAT"+;
            " from "+i_cTable+" LOTTIART where ";
                +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                +" and LOCODART = "+cp_ToStrODBC(this.w_OLDCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LOFLSTAT;
            from (i_cTable) where;
                LOCODICE = this.w_OLDCODLOT;
                and LOCODART = this.w_OLDCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDFLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_QTAESI = 0
        this.w_TESDIS = .F.
        * --- Select from SALDILOT
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select SUM(SUQTAPER) as SUQTAPER , SUM(SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
              +" where "+cp_ToStrODBC(this.w_OLDCODART)+"=SUCODART AND "+cp_ToStrODBC(this.w_OLDCODLOT)+"=SUCODLOT";
               ,"_Curs_SALDILOT")
        else
          select SUM(SUQTAPER) as SUQTAPER , SUM(SUQTRPER) as SUQTRPER from (i_cTable);
           where this.w_OLDCODART=SUCODART AND this.w_OLDCODLOT=SUCODLOT;
            into cursor _Curs_SALDILOT
        endif
        if used('_Curs_SALDILOT')
          select _Curs_SALDILOT
          locate for 1=1
          do while not(eof())
          this.w_TESDIS = .T.
          this.w_QTAESI = _Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
            select _Curs_SALDILOT
            continue
          enddo
          use
        endif
        do case
          case this.w_OLDFLSTAT="E" 
            if (this.w_FLAGLO="-" OR (this.w_QTAUM1>this.w_OLDQTA AND this.w_FLAGLO="+" ) )
              * --- Aggiorno Status in caso di Modifica della Quantit� o Cancellazione di uno Scarico
              if this.w_TESDIS AND this.w_QTAESI>0 AND Not Empty(this.w_OLDCODLOT)
                * --- Write into LOTTIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.LOTTIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
                      +i_ccchkf ;
                  +" where ";
                      +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                      +" and LOCODART = "+cp_ToStrODBC(this.w_OLDCODART);
                         )
                else
                  update (i_cTable) set;
                      LOFLSTAT = "D";
                      &i_ccchkf. ;
                   where;
                      LOCODICE = this.w_OLDCODLOT;
                      and LOCODART = this.w_OLDCODART;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='errore scrittura lotti'
                  return
                endif
              endif
            endif
          case this.w_OLDFLSTAT="D" 
            * --- Aggiorno Status Recuperando esistenza generale del Lotto
            if this.w_TESDIS AND this.w_QTAESI<=0 AND this.w_FLAGLO $ "+-"
              * --- Write into LOTTIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.LOTTIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("E"),'LOTTIART','LOFLSTAT');
                    +i_ccchkf ;
                +" where ";
                    +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                    +" and LOCODART = "+cp_ToStrODBC(this.w_OLDCODART);
                       )
              else
                update (i_cTable) set;
                    LOFLSTAT = "E";
                    &i_ccchkf. ;
                 where;
                    LOCODICE = this.w_OLDCODLOT;
                    and LOCODART = this.w_OLDCODART;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
        endcase
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico la presenza delle matricole!
    if this.w_CFUNC<>"Query" And g_MATR="S" And this.oParentObject.w_MDDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    "))
      if this.w_OK
        * --- Verifico congruenza numero matricole con qt� di riga (SE NON IN FASE DI CORR. ERORRI LOTTI / UBICAZIONI)
        if NOT this.oParentObject.w_TESLOT
          * --- Select from GSPS_BMT
          do vq_exec with 'GSPS_BMT',this,'_Curs_GSPS_BMT','',.f.,.t.
          if used('_Curs_GSPS_BMT')
            select _Curs_GSPS_BMT
            locate for 1=1
            do while not(eof())
            this.w_OK = .F.
            if NOT EMPTY(NVL(_Curs_GSPS_BMT.DOMAGSCA," "))
              * --- Se si tratta di un trasferimento determino il magazzino di carico\scarico corretto del documento
              this.w_DMAGCAR = Nvl(_Curs_GSPS_BMT.DOMAGCAR,Space(5) )
              this.w_DMAGSCA = Nvl(_Curs_GSPS_BMT.DOMAGSCA,Space(5) )
            else
              this.w_DMAGCAR = IIF(_Curs_GSPS_BMT.MVFLCASC="+" , NVL(_Curs_GSPS_BMT.DOMAGCAR,Space(5)),NVL(_Curs_GSPS_BMT.DOMAGSCA,Space(5)))
              this.w_DMAGSCA = IIF(_Curs_GSPS_BMT.MVFLCASC="-" , NVL(_Curs_GSPS_BMT.DOMAGSCA,Space(5)),NVL(_Curs_GSPS_BMT.DOMAGCAR,Space(5)))
            endif
            do case
              case Nvl(qtaum1,0)<>Nvl(qtamat,0)
                if cp_Round(Nvl(qtaum1,0),0)<>Nvl(qtaum1,0)
                  this.w_MESS = ah_Msgformat("'Riga %1 Impossibile movimentare articoli gestiti a matricole per quantit� frazionabili", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ) )
                else
                  this.w_MESS = ah_Msgformat("Riga %1 con numero matricole incongruente. Qt� riga: %2 matricole movimentate: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim( Str( _Curs_GSPS_BMT.QTAUM1 ) ), Alltrim( Str( _Curs_GSPS_BMT.QTAMAT ) ) )
                endif
              case ALLTRIM(Nvl(Mvcodice,Space(20)))<>ALLTRIM(Nvl(Mtkeysal,Space(20)))
                this.w_MESS = ah_Msgformat("Riga %1 con articolo incongruente. Articolo documento: %2, articolo matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim( _Curs_GSPS_BMT.MVCODICE ), Alltrim( _Curs_GSPS_BMT.MTKEYSAL ) ) 
              case Nvl(Mvcodlot,Space(20)) <> Nvl(Mtcodlot,Space(20)) AND g_PERLOT="S"
                this.w_MESS = ah_Msgformat("Riga %1 con lotto incongruente. Lotto documento: %2, lotto matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSPS_BMT.MVCODLOT,Space(20)) ), Alltrim( NVL(_Curs_GSPS_BMT.MTCODLOT,Space(20)) ) )
              case ALLTRIM(Nvl(Mtmagpri,Space(5)))<>Alltrim(this.w_DMAGCAR)
                this.w_MESS = ah_Msgformat("Riga %1 con magazzino di carico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ) , Alltrim(this.w_DMAGCAR), Alltrim( NVL(_Curs_GSPS_BMT.MTMAGPRI,Space(5))) )
              case Nvl(Mvcodubi,Space(20))<>Nvl(Mtcodubi,Space(20)) AND g_PERUBI="S"
                this.w_MESS = ah_Msgformat("Riga %1 con ubicazione incongruente. Ubicazione documento: %2, ubicazione matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSPS_BMT.MVCODUBI,Space(20))), Alltrim( NVL(_Curs_GSPS_BMT.MTCODUBI,Space(20)) ) )
              case ALLTRIM(Nvl(Mtmagsca,Space(5)))<>Alltrim(this.w_DMAGSCA) and Not Empty(Nvl(Mtmagsca,Space(5)))
                this.w_MESS = ah_Msgformat("Riga %1 con magazzino di scarico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim(this.w_DMAGSCA), Alltrim( NVL(_Curs_GSPS_BMT.MTMAGSCA,Space(5))) )
            endcase
            Exit
              select _Curs_GSPS_BMT
              continue
            enddo
            use
          endif
        endif
      endif
      if this.w_OK 
        * --- Verifica se il documento contiene la stessa matricola due volte (cosa vietata)
        * --- Se ci� avviene la variabile caller w_OK viene posta a false
        this.w_SERIAL = this.oParentObject.w_MDSERIAL
        this.w_NUMRIF = this.oParentObject.w_MDNUMRIF
        * --- Select from GSVE_BML
        do vq_exec with 'GSVE_BML',this,'_Curs_GSVE_BML','',.f.,.t.
        if used('_Curs_GSVE_BML')
          select _Curs_GSVE_BML
          locate for 1=1
          do while not(eof())
          this.w_OK = ( _Curs_GSVE_BML.CODMAT<=1 )
          Exit
            select _Curs_GSVE_BML
            continue
          enddo
          use
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,15)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_RATE'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='MVM_DETT'
    this.cWorkTables[6]='MVM_MAST'
    this.cWorkTables[7]='FID_CARD'
    this.cWorkTables[8]='CLI_VEND'
    this.cWorkTables[9]='SIT_FIDI'
    this.cWorkTables[10]='LOTTIART'
    this.cWorkTables[11]='CON_PAGA'
    this.cWorkTables[12]='SALDILOT'
    this.cWorkTables[13]='ART_ICOL'
    this.cWorkTables[14]='SALDICOM'
    this.cWorkTables[15]='SALOTCOM'
    return(this.OpenAllTables(15))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_GSPS_BMT')
      use in _Curs_GSPS_BMT
    endif
    if used('_Curs_GSVE_BML')
      use in _Curs_GSVE_BML
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
