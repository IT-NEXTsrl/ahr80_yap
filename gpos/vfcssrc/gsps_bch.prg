* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_bch                                                        *
*              Chiusura di cassa                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-28                                                      *
* Last revis.: 2010-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsps_bch",oParentObject)
return(i_retval)

define class tgsps_bch as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_CHIUSURA = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura di cassa fiscale sul registratore
    if Ah_YesNo("Eseguo la chiusura di cassa fiscale sul registratore?")
      this.w_SERIAL = "CHIUSURA"
      this.w_CHIUSURA = .T.
      * --- La gestione controlla la variabile caller w_CHIUSURA e determina che si sta
      *     eseguendo una chiusura di cassa
      do GSPS_KES with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
