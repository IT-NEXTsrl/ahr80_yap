* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_sfc                                                        *
*              Stampa Fidelity Card                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-15                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_sfc",oParentObject))

* --- Class definition
define class tgsps_sfc as StdForm
  Top    = 10
  Left   = 102

  * --- Standard Properties
  Width  = 586
  Height = 422
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=40458345
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  CLI_VEND_IDX = 0
  FID_CARD_IDX = 0
  cPrg = "gsps_sfc"
  cComment = "Stampa Fidelity Card"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CARDIN = space(20)
  w_CARDFI = space(20)
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_DESCIN = space(40)
  w_DESCFI = space(40)
  w_ATTINI = ctod('  /  /  ')
  w_ATTFIN = ctod('  /  /  ')
  w_ACQINI = ctod('  /  /  ')
  w_ACQFIN = ctod('  /  /  ')
  w_PUNINI = 0
  w_PUNFIN = 0
  w_VISINI = 0
  w_VISFIN = 0
  w_PREINI = 0
  w_PREFIN = 0
  w_RESINI = 0
  w_RESFIN = 0
  w_TOTINI = 0
  w_TOTFIN = 0
  w_ONUME = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_sfcPag1","gsps_sfc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCARDIN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CLI_VEND'
    this.cWorkTables[2]='FID_CARD'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CARDIN=space(20)
      .w_CARDFI=space(20)
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_DESCIN=space(40)
      .w_DESCFI=space(40)
      .w_ATTINI=ctod("  /  /  ")
      .w_ATTFIN=ctod("  /  /  ")
      .w_ACQINI=ctod("  /  /  ")
      .w_ACQFIN=ctod("  /  /  ")
      .w_PUNINI=0
      .w_PUNFIN=0
      .w_VISINI=0
      .w_VISFIN=0
      .w_PREINI=0
      .w_PREFIN=0
      .w_RESINI=0
      .w_RESFIN=0
      .w_TOTINI=0
      .w_TOTFIN=0
      .w_ONUME=0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CARDIN))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CARDFI))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODFIN))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
    endwith
    this.DoRTCalc(5,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CARDIN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_lTable = "FID_CARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2], .t., this.FID_CARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CARDIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FID_CARD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FCCODFID like "+cp_ToStrODBC(trim(this.w_CARDIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FCCODFID","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FCCODFID',trim(this.w_CARDIN))
          select FCCODFID,FCDESFID;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FCCODFID into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CARDIN)==trim(_Link_.FCCODFID) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CARDIN) and !this.bDontReportError
            deferred_cp_zoom('FID_CARD','*','FCCODFID',cp_AbsName(oSource.parent,'oCARDIN_1_1'),i_cWhere,'',"Fidelity Card",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                     +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',oSource.xKey(1))
            select FCCODFID,FCDESFID;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CARDIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                   +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(this.w_CARDIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',this.w_CARDIN)
            select FCCODFID,FCDESFID;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CARDIN = NVL(_Link_.FCCODFID,space(20))
      this.w_DESCIN = NVL(_Link_.FCDESFID,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CARDIN = space(20)
      endif
      this.w_DESCIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CARDFI)) OR  (.w_CARDIN<=.w_CARDFI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CARDIN = space(20)
        this.w_DESCIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])+'\'+cp_ToStr(_Link_.FCCODFID,1)
      cp_ShowWarn(i_cKey,this.FID_CARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CARDIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CARDFI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FID_CARD_IDX,3]
    i_lTable = "FID_CARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2], .t., this.FID_CARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CARDFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FID_CARD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FCCODFID like "+cp_ToStrODBC(trim(this.w_CARDFI)+"%");

          i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FCCODFID","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FCCODFID',trim(this.w_CARDFI))
          select FCCODFID,FCDESFID;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FCCODFID into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CARDFI)==trim(_Link_.FCCODFID) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CARDFI) and !this.bDontReportError
            deferred_cp_zoom('FID_CARD','*','FCCODFID',cp_AbsName(oSource.parent,'oCARDFI_1_2'),i_cWhere,'',"Fidelity Card",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                     +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',oSource.xKey(1))
            select FCCODFID,FCDESFID;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CARDFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FCCODFID,FCDESFID";
                   +" from "+i_cTable+" "+i_lTable+" where FCCODFID="+cp_ToStrODBC(this.w_CARDFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FCCODFID',this.w_CARDFI)
            select FCCODFID,FCDESFID;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CARDFI = NVL(_Link_.FCCODFID,space(20))
      this.w_DESCFI = NVL(_Link_.FCDESFID,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CARDFI = space(20)
      endif
      this.w_DESCFI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CARDFI>=.w_CARDIN) or (empty(.w_CARDIN)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CARDFI = space(20)
        this.w_DESCFI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FID_CARD_IDX,2])+'\'+cp_ToStr(_Link_.FCCODFID,1)
      cp_ShowWarn(i_cKey,this.FID_CARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CARDFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_CODINI))
          select CLCODCLI,CLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oCODINI_1_3'),i_cWhere,'',"Clienti negozio",'GSPS_QZC.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_CODINI)
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESINI = NVL(_Link_.CLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_codfin)) OR  (.w_codini<=.w_codfin))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore a quello finale o inesistente o non legato a Fidelity Card")
        endif
        this.w_CODINI = space(15)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLI_VEND_IDX,3]
    i_lTable = "CLI_VEND"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2], .t., this.CLI_VEND_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLI_VEND')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLCODCLI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODCLI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODCLI',trim(this.w_CODFIN))
          select CLCODCLI,CLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODCLI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CLCODCLI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CLI_VEND','*','CLCODCLI',cp_AbsName(oSource.parent,'oCODFIN_1_4'),i_cWhere,'',"Clienti negozio",'GSPS_QZC.CLI_VEND_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',oSource.xKey(1))
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODCLI,CLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODCLI="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODCLI',this.w_CODFIN)
            select CLCODCLI,CLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CLCODCLI,space(15))
      this.w_DESFIN = NVL(_Link_.CLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_codfin>=.w_codini) or (empty(.w_codini)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice finale inferiore a quello iniziale o inesistente o non legato a Fidelity Card")
        endif
        this.w_CODFIN = space(15)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLI_VEND_IDX,2])+'\'+cp_ToStr(_Link_.CLCODCLI,1)
      cp_ShowWarn(i_cKey,this.CLI_VEND_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCARDIN_1_1.value==this.w_CARDIN)
      this.oPgFrm.Page1.oPag.oCARDIN_1_1.value=this.w_CARDIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCARDFI_1_2.value==this.w_CARDFI)
      this.oPgFrm.Page1.oPag.oCARDFI_1_2.value=this.w_CARDFI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_3.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_3.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_4.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_4.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_5.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_5.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_6.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_6.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCIN_1_9.value==this.w_DESCIN)
      this.oPgFrm.Page1.oPag.oDESCIN_1_9.value=this.w_DESCIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCFI_1_10.value==this.w_DESCFI)
      this.oPgFrm.Page1.oPag.oDESCFI_1_10.value=this.w_DESCFI
    endif
    if not(this.oPgFrm.Page1.oPag.oATTINI_1_13.value==this.w_ATTINI)
      this.oPgFrm.Page1.oPag.oATTINI_1_13.value=this.w_ATTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oATTFIN_1_14.value==this.w_ATTFIN)
      this.oPgFrm.Page1.oPag.oATTFIN_1_14.value=this.w_ATTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oACQINI_1_15.value==this.w_ACQINI)
      this.oPgFrm.Page1.oPag.oACQINI_1_15.value=this.w_ACQINI
    endif
    if not(this.oPgFrm.Page1.oPag.oACQFIN_1_16.value==this.w_ACQFIN)
      this.oPgFrm.Page1.oPag.oACQFIN_1_16.value=this.w_ACQFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPUNINI_1_17.value==this.w_PUNINI)
      this.oPgFrm.Page1.oPag.oPUNINI_1_17.value=this.w_PUNINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPUNFIN_1_18.value==this.w_PUNFIN)
      this.oPgFrm.Page1.oPag.oPUNFIN_1_18.value=this.w_PUNFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oVISINI_1_19.value==this.w_VISINI)
      this.oPgFrm.Page1.oPag.oVISINI_1_19.value=this.w_VISINI
    endif
    if not(this.oPgFrm.Page1.oPag.oVISFIN_1_20.value==this.w_VISFIN)
      this.oPgFrm.Page1.oPag.oVISFIN_1_20.value=this.w_VISFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPREINI_1_21.value==this.w_PREINI)
      this.oPgFrm.Page1.oPag.oPREINI_1_21.value=this.w_PREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIN_1_22.value==this.w_PREFIN)
      this.oPgFrm.Page1.oPag.oPREFIN_1_22.value=this.w_PREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRESINI_1_23.value==this.w_RESINI)
      this.oPgFrm.Page1.oPag.oRESINI_1_23.value=this.w_RESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRESFIN_1_24.value==this.w_RESFIN)
      this.oPgFrm.Page1.oPag.oRESFIN_1_24.value=this.w_RESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTINI_1_25.value==this.w_TOTINI)
      this.oPgFrm.Page1.oPag.oTOTINI_1_25.value=this.w_TOTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTFIN_1_26.value==this.w_TOTFIN)
      this.oPgFrm.Page1.oPag.oTOTFIN_1_26.value=this.w_TOTFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_CARDFI)) OR  (.w_CARDIN<=.w_CARDFI)))  and not(empty(.w_CARDIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCARDIN_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_CARDFI>=.w_CARDIN) or (empty(.w_CARDIN))))  and not(empty(.w_CARDFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCARDFI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_codfin)) OR  (.w_codini<=.w_codfin)))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore a quello finale o inesistente o non legato a Fidelity Card")
          case   not(((.w_codfin>=.w_codini) or (empty(.w_codini))))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice finale inferiore a quello iniziale o inesistente o non legato a Fidelity Card")
          case   not(((empty(.w_ATTFIN)) OR  (.w_ATTINI<=.w_ATTFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_ATTFIN>=.w_ATTINI) or (empty(.w_ATTINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_ACQFIN)) OR  (.w_ACQINI<=.w_ACQFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oACQINI_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_ACQFIN>=.w_ACQINI) or (empty(.w_ACQINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oACQFIN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_PUNFIN)) OR  (.w_PUNINI<=.w_PUNFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPUNINI_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_PUNFIN>=.w_PUNINI) or (empty(.w_PUNINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPUNFIN_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_VISFIN)) OR  (.w_VISINI<=.w_VISFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVISINI_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_VISFIN>=.w_VISINI) or (empty(.w_VISINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVISFIN_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_PREFIN)) OR  (.w_PREINI<=.w_PREFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPREINI_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_PREFIN>=.w_PREINI) or (empty(.w_PREINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPREFIN_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_RESFIN)) OR  (.w_RESINI<=.w_RESFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRESINI_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_RESFIN>=.w_RESINI) or (empty(.w_RESINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRESFIN_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_TOTFIN)) OR  (.w_TOTINI<=.w_TOTFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTOTINI_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_TOTFIN>=.w_TOTINI) or (empty(.w_TOTINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTOTFIN_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_sfcPag1 as StdContainer
  Width  = 582
  height = 422
  stdWidth  = 582
  stdheight = 422
  resizeXpos=411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCARDIN_1_1 as StdField with uid="XVIWSBZNPN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CARDIN", cQueryName = "CARDIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice Fidelity di inizio selezione",;
    HelpContextID = 261092058,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=114, Top=17, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FID_CARD", oKey_1_1="FCCODFID", oKey_1_2="this.w_CARDIN"

  func oCARDIN_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCARDIN_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCARDIN_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FID_CARD','*','FCCODFID',cp_AbsName(this.parent,'oCARDIN_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fidelity Card",'',this.parent.oContained
  endproc

  add object oCARDFI_1_2 as StdField with uid="YBGUAVEWZZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CARDFI", cQueryName = "CARDFI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice Fidelity di fine selezione",;
    HelpContextID = 79688410,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=114, Top=43, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FID_CARD", oKey_1_1="FCCODFID", oKey_1_2="this.w_CARDFI"

  func oCARDFI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCARDFI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCARDFI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FID_CARD','*','FCCODFID',cp_AbsName(this.parent,'oCARDFI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fidelity Card",'',this.parent.oContained
  endproc

  add object oCODINI_1_3 as StdField with uid="JRKJOGCJQI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore a quello finale o inesistente o non legato a Fidelity Card",;
    ToolTipText = "Cliente Fidelity di inizio selezione",;
    HelpContextID = 71025882,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=114, Top=76, cSayPict="REPL('X',15)", cGetPict="g_MASPOS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", oKey_1_1="CLCODCLI", oKey_1_2="this.w_CODINI"

  proc oCODINI_1_3.mAfter
    with this.Parent.oContained
      .w_CODINI=PSCALZER(.w_CODINI, 'LOAD')
    endwith
  endproc

  func oCODINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oCODINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti negozio",'GSPS_QZC.CLI_VEND_VZM',this.parent.oContained
  endproc

  add object oCODFIN_1_4 as StdField with uid="FXEIYJWHZM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice finale inferiore a quello iniziale o inesistente o non legato a Fidelity Card",;
    ToolTipText = "Cliente Fidelity di fine selezione",;
    HelpContextID = 261014746,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=114, Top=102, cSayPict="REPL('X',15)", cGetPict="g_MASPOS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLI_VEND", oKey_1_1="CLCODCLI", oKey_1_2="this.w_CODFIN"

  proc oCODFIN_1_4.mAfter
    with this.Parent.oContained
      .w_CODINI=PSCALZER(.w_CODINI, 'LOAD')
    endwith
  endproc

  func oCODFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLI_VEND','*','CLCODCLI',cp_AbsName(this.parent,'oCODFIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti negozio",'GSPS_QZC.CLI_VEND_VZM',this.parent.oContained
  endproc

  add object oDESINI_1_5 as StdField with uid="YJDNNQTGSR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 70966986,;
   bGlobalFont=.t.,;
    Height=21, Width=336, Left=233, Top=76, InputMask=replicate('X',40)

  add object oDESFIN_1_6 as StdField with uid="YTVOGAYYZO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 260955850,;
   bGlobalFont=.t.,;
    Height=21, Width=336, Left=233, Top=102, InputMask=replicate('X',40)

  add object oDESCIN_1_9 as StdField with uid="YTRZAZIXAJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCIN", cQueryName = "DESCIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 261152458,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=276, Top=17, InputMask=replicate('X',40)

  add object oDESCFI_1_10 as StdField with uid="TKQLMCUEND",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCFI", cQueryName = "DESCFI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 79748810,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=276, Top=43, InputMask=replicate('X',40)

  add object oATTINI_1_13 as StdField with uid="HBFRTMEOGD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ATTINI", cQueryName = "ATTINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data attivazione di inizio selezione",;
    HelpContextID = 70959098,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=132

  func oATTINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_ATTFIN)) OR  (.w_ATTINI<=.w_ATTFIN)))
    endwith
    return bRes
  endfunc

  add object oATTFIN_1_14 as StdField with uid="RKGFKNUODA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ATTFIN", cQueryName = "ATTFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data attivazione di fine selezione",;
    HelpContextID = 260947962,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=155

  func oATTFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_ATTFIN>=.w_ATTINI) or (empty(.w_ATTINI))))
    endwith
    return bRes
  endfunc

  add object oACQINI_1_15 as StdField with uid="DUDFUJWTOB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ACQINI", cQueryName = "ACQINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultimo acquisto di inizio selezione",;
    HelpContextID = 70975738,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=182

  func oACQINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_ACQFIN)) OR  (.w_ACQINI<=.w_ACQFIN)))
    endwith
    return bRes
  endfunc

  add object oACQFIN_1_16 as StdField with uid="YWLBFEDGKU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ACQFIN", cQueryName = "ACQFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultimo acquisto di fine selezione",;
    HelpContextID = 260964602,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=205

  func oACQFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_ACQFIN>=.w_ACQINI) or (empty(.w_ACQINI))))
    endwith
    return bRes
  endfunc

  add object oPUNINI_1_17 as StdField with uid="HHZURVVAUQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PUNINI", cQueryName = "PUNINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero punti Fidelity Card di inizio selezione",;
    HelpContextID = 70983178,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=233, cSayPict='"999999"', cGetPict='"999999"'

  func oPUNINI_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_PUNFIN)) OR  (.w_PUNINI<=.w_PUNFIN)))
    endwith
    return bRes
  endfunc

  add object oPUNFIN_1_18 as StdField with uid="OUYNARAQWL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PUNFIN", cQueryName = "PUNFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero punti Fidelity Card di fine selezione",;
    HelpContextID = 260972042,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=256, cSayPict='"999999"', cGetPict='"999999"'

  func oPUNFIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_PUNFIN>=.w_PUNINI) or (empty(.w_PUNINI))))
    endwith
    return bRes
  endfunc

  add object oVISINI_1_19 as StdField with uid="SKWZNNJGPK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_VISINI", cQueryName = "VISINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero visite Fidelity Card di inizio selezione",;
    HelpContextID = 70965674,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=284, cSayPict='"999999"', cGetPict='"999999"'

  func oVISINI_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_VISFIN)) OR  (.w_VISINI<=.w_VISFIN)))
    endwith
    return bRes
  endfunc

  add object oVISFIN_1_20 as StdField with uid="DSZNIUBNFT",rtseq=16,rtrep=.f.,;
    cFormVar = "w_VISFIN", cQueryName = "VISFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero visite Fidelity Card di fine selezione",;
    HelpContextID = 260954538,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=307, cSayPict='"999999"', cGetPict='"999999"'

  func oVISFIN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_VISFIN>=.w_VISINI) or (empty(.w_VISINI))))
    endwith
    return bRes
  endfunc

  add object oPREINI_1_21 as StdField with uid="PJUSADNIXG",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PREINI", cQueryName = "PREINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo prepagato di inizio selezione",;
    HelpContextID = 71020810,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=430, Top=132, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oPREINI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_PREFIN)) OR  (.w_PREINI<=.w_PREFIN)))
    endwith
    return bRes
  endfunc

  add object oPREFIN_1_22 as StdField with uid="PXXZHKXJQH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PREFIN", cQueryName = "PREFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo prepagato di fine selezione",;
    HelpContextID = 261009674,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=430, Top=155, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oPREFIN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_PREFIN>=.w_PREINI) or (empty(.w_PREINI))))
    endwith
    return bRes
  endfunc

  add object oRESINI_1_23 as StdField with uid="UIZYPLKZKV",rtseq=19,rtrep=.f.,;
    cFormVar = "w_RESINI", cQueryName = "RESINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo residuo di inizio selezione",;
    HelpContextID = 70966762,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=430, Top=183, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oRESINI_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_RESFIN)) OR  (.w_RESINI<=.w_RESFIN)))
    endwith
    return bRes
  endfunc

  add object oRESFIN_1_24 as StdField with uid="DDJHQLMFGP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_RESFIN", cQueryName = "RESFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo residuo di fine selezione",;
    HelpContextID = 260955626,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=430, Top=206, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oRESFIN_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_RESFIN>=.w_RESINI) or (empty(.w_RESINI))))
    endwith
    return bRes
  endfunc

  add object oTOTINI_1_25 as StdField with uid="BWFPWTRHJG",rtseq=21,rtrep=.f.,;
    cFormVar = "w_TOTINI", cQueryName = "TOTINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale acquisti di inizio selezione",;
    HelpContextID = 70960074,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=430, Top=233, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oTOTINI_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_TOTFIN)) OR  (.w_TOTINI<=.w_TOTFIN)))
    endwith
    return bRes
  endfunc

  add object oTOTFIN_1_26 as StdField with uid="OPKPIZSAHA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_TOTFIN", cQueryName = "TOTFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale acquisti di fine selezione",;
    HelpContextID = 260948938,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=430, Top=256, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  func oTOTFIN_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_TOTFIN>=.w_TOTINI) or (empty(.w_TOTINI))))
    endwith
    return bRes
  endfunc


  add object oObj_1_28 as cp_outputCombo with uid="ZLERXIVPWT",left=115, top=338, width=454,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 137539302


  add object oBtn_1_29 as StdButton with uid="VCZMCHUPGB",left=466, top=369, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare la stampa";
    , HelpContextID = 101331238;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="EFNLDLJGWV",left=522, top=369, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere paer annullare";
    , HelpContextID = 33140922;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="ZVGIMBXPIO",Visible=.t., Left=30, Top=77,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="HMPDNJIDNF",Visible=.t., Left=30, Top=103,;
    Alignment=1, Width=83, Height=18,;
    Caption="a cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="NVXBYKFJZC",Visible=.t., Left=30, Top=18,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="BUUUFYZPXQ",Visible=.t., Left=30, Top=44,;
    Alignment=1, Width=83, Height=18,;
    Caption="a codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="AUDGGQJBCS",Visible=.t., Left=267, Top=133,;
    Alignment=1, Width=162, Height=18,;
    Caption="Da importo prepagato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="UCLEQKKRIT",Visible=.t., Left=267, Top=184,;
    Alignment=1, Width=162, Height=18,;
    Caption="Da importo residuo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="YYATAPAKEV",Visible=.t., Left=267, Top=234,;
    Alignment=1, Width=162, Height=18,;
    Caption="Da totale acquistato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="QBJSPTAYDD",Visible=.t., Left=4, Top=133,;
    Alignment=1, Width=110, Height=18,;
    Caption="Da data attivazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="BOODLOCJTT",Visible=.t., Left=-18, Top=183,;
    Alignment=1, Width=132, Height=18,;
    Caption="Da data ultimo acq.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="BAYQZWDSCP",Visible=.t., Left=4, Top=234,;
    Alignment=1, Width=110, Height=18,;
    Caption="Da n. punti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="LIADPNXSRB",Visible=.t., Left=4, Top=286,;
    Alignment=1, Width=110, Height=18,;
    Caption="Da n. visite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="QUBVMUWGHM",Visible=.t., Left=267, Top=156,;
    Alignment=1, Width=162, Height=18,;
    Caption="a importo prepagato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="AAPVWGERZB",Visible=.t., Left=267, Top=207,;
    Alignment=1, Width=162, Height=18,;
    Caption="a importo residuo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="MPMRNWIYDD",Visible=.t., Left=267, Top=257,;
    Alignment=1, Width=162, Height=18,;
    Caption="a totale acquistato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="XKASHDIJJL",Visible=.t., Left=4, Top=156,;
    Alignment=1, Width=110, Height=18,;
    Caption="a data attivazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="BRTDQNOLKB",Visible=.t., Left=1, Top=206,;
    Alignment=1, Width=113, Height=18,;
    Caption="a data ultimo acq.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="BZNFIRADZC",Visible=.t., Left=4, Top=257,;
    Alignment=1, Width=110, Height=18,;
    Caption="a n. punti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="VSDKLABYWZ",Visible=.t., Left=4, Top=309,;
    Alignment=1, Width=110, Height=18,;
    Caption="a n. visite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="DYGLJWSYXZ",Visible=.t., Left=53, Top=338,;
    Alignment=1, Width=61, Height=18,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_sfc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
