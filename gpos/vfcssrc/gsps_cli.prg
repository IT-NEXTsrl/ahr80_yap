* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsps_cli                                                        *
*              Generazione clienti negozio                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_25]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-06                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsps_cli",oParentObject))

* --- Class definition
define class tgsps_cli as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 564
  Height = 259
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=43427735
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  MASTRI_IDX = 0
  ZONE_IDX = 0
  CACOCLFO_IDX = 0
  CATECOMM_IDX = 0
  BUSIUNIT_IDX = 0
  cPrg = "gsps_cli"
  cComment = "Generazione clienti negozio"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_C = space(1)
  w_AZIENDA = space(10)
  w_PBANCODI = space(15)
  w_PEANCODI = space(15)
  w_RAGSOCINI = space(40)
  w_RAGSOCFIN = space(40)
  w_PANCONSU = space(15)
  w_PANCODZON = space(3)
  w_CATCON = space(5)
  w_CATECOMM = space(3)
  w_CODNEG = space(3)
  w_DESCR1 = space(40)
  w_DESCR2 = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESAPP = space(40)
  w_CLIPOS = space(1)
  w_CLIPOS1 = space(1)
  w_CLIPOS2 = space(1)
  w_CLIPOS3 = space(1)
  w_DESNEG = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsps_cliPag1","gsps_cli",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPBANCODI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='ZONE'
    this.cWorkTables[4]='CACOCLFO'
    this.cWorkTables[5]='CATECOMM'
    this.cWorkTables[6]='BUSIUNIT'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_C=space(1)
      .w_AZIENDA=space(10)
      .w_PBANCODI=space(15)
      .w_PEANCODI=space(15)
      .w_RAGSOCINI=space(40)
      .w_RAGSOCFIN=space(40)
      .w_PANCONSU=space(15)
      .w_PANCODZON=space(3)
      .w_CATCON=space(5)
      .w_CATECOMM=space(3)
      .w_CODNEG=space(3)
      .w_DESCR1=space(40)
      .w_DESCR2=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESAPP=space(40)
      .w_CLIPOS=space(1)
      .w_CLIPOS1=space(1)
      .w_CLIPOS2=space(1)
      .w_CLIPOS3=space(1)
      .w_DESNEG=space(40)
        .w_C = 'C'
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PBANCODI))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PEANCODI))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_RAGSOCINI))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_RAGSOCFIN))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PANCONSU))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PANCODZON))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CATCON))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CATECOMM))
          .link_1_10('Full')
        endif
        .w_CODNEG = g_CODNEG
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODNEG))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,13,.f.)
        .w_OBTEST = i_datsys
    endwith
    this.DoRTCalc(15,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,13,.t.)
            .w_OBTEST = i_datsys
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PBANCODI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANCODICE',trim(this.w_PBANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_C);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPBANCODI_1_3'),i_cWhere,'GSAR_BZC',"Clienti",'GSPS_ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto oppure cliente P.O.S.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PBANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANCODICE',this.w_PBANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_CLIPOS = NVL(_Link_.ANCLIPOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PBANCODI = space(15)
      endif
      this.w_DESCR1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CLIPOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CLIPOS<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto oppure cliente P.O.S.")
        endif
        this.w_PBANCODI = space(15)
        this.w_DESCR1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CLIPOS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEANCODI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANCODICE',trim(this.w_PEANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_C);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPEANCODI_1_4'),i_cWhere,'GSAR_BZC',"Clienti",'GSPS_ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto oppure cliente P.O.S.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PEANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANCODICE',this.w_PEANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_CLIPOS1 = NVL(_Link_.ANCLIPOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PEANCODI = space(15)
      endif
      this.w_DESCR2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CLIPOS1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBancodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CLIPOS1<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto oppure cliente P.O.S.")
        endif
        this.w_PEANCODI = space(15)
        this.w_DESCR2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CLIPOS1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGSOCINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGSOCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RAGSOCINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI,ANCLIPOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANDESCRI',trim(this.w_RAGSOCINI))
          select ANTIPCON,ANDESCRI,ANCLIPOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGSOCINI)==trim(_Link_.ANDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RAGSOCINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(oSource.parent,'oRAGSOCINI_1_5'),i_cWhere,'GSAR_BZC',"Clienti",'GSPS_ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI,ANCLIPOS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANDESCRI,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta oppure cliente P.O.S.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI,ANCLIPOS";
                     +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANDESCRI',oSource.xKey(2))
            select ANTIPCON,ANDESCRI,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGSOCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI,ANCLIPOS";
                   +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(this.w_RAGSOCINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANDESCRI',this.w_RAGSOCINI)
            select ANTIPCON,ANDESCRI,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGSOCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_CLIPOS2 = NVL(_Link_.ANCLIPOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RAGSOCINI = space(40)
      endif
      this.w_CLIPOS2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_RAGSOCFIN)) OR  (.w_RAGSOCINI<=.w_RAGSOCFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CLIPOS2<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta oppure cliente P.O.S.")
        endif
        this.w_RAGSOCINI = space(40)
        this.w_CLIPOS2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANDESCRI,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGSOCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGSOCFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGSOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RAGSOCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI,ANCLIPOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANDESCRI',trim(this.w_RAGSOCFIN))
          select ANTIPCON,ANDESCRI,ANCLIPOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGSOCFIN)==trim(_Link_.ANDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RAGSOCFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(oSource.parent,'oRAGSOCFIN_1_6'),i_cWhere,'GSAR_BZC',"Clienti",'GSPS_ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI,ANCLIPOS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANDESCRI,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta oppure cliente P.O.S.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI,ANCLIPOS";
                     +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANDESCRI',oSource.xKey(2))
            select ANTIPCON,ANDESCRI,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGSOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI,ANCLIPOS";
                   +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(this.w_RAGSOCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANDESCRI',this.w_RAGSOCFIN)
            select ANTIPCON,ANDESCRI,ANCLIPOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGSOCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_CLIPOS3 = NVL(_Link_.ANCLIPOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RAGSOCFIN = space(40)
      endif
      this.w_CLIPOS3 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_RAGSOCFIN>=.w_RAGSOCINI) or (empty(.w_RAGSOCINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CLIPOS3<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta oppure cliente P.O.S.")
        endif
        this.w_RAGSOCFIN = space(40)
        this.w_CLIPOS3 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANDESCRI,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGSOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCONSU
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PANCONSU))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCONSU)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_PANCONSU)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PANCONSU) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPANCONSU_1_7'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_SCL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PANCONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PANCONSU)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCONSU = NVL(_Link_.MCCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PANCONSU = space(15)
      endif
      this.w_DESAPP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCODZON
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_PANCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_PANCODZON))
          select ZOCODZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PANCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oPANCODZON_1_8'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_PANCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_PANCODZON)
            select ZOCODZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PANCODZON = space(3)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_PANCODZON = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_9'),i_cWhere,'GSAR_AC2',"Categorie contabili",'GSAR_SCL.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATECOMM
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATECOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATECOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATECOMM))
          select CTCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATECOMM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATECOMM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATECOMM_1_10'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATECOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATECOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATECOMM)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATECOMM = NVL(_Link_.CTCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CATECOMM = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATECOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNEG
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_KAN',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODNEG)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_AZIENDA;
                     ,'BUCODICE',trim(this.w_CODNEG))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNEG)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODNEG) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODNEG_1_11'),i_cWhere,'GSPS_KAN',"Negozi azienda",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODNEG);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_AZIENDA;
                       ,'BUCODICE',this.w_CODNEG)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNEG = NVL(_Link_.BUCODICE,space(3))
      this.w_DESNEG = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODNEG = space(3)
      endif
      this.w_DESNEG = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPBANCODI_1_3.value==this.w_PBANCODI)
      this.oPgFrm.Page1.oPag.oPBANCODI_1_3.value=this.w_PBANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEANCODI_1_4.value==this.w_PEANCODI)
      this.oPgFrm.Page1.oPag.oPEANCODI_1_4.value=this.w_PEANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOCINI_1_5.value==this.w_RAGSOCINI)
      this.oPgFrm.Page1.oPag.oRAGSOCINI_1_5.value=this.w_RAGSOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_6.value==this.w_RAGSOCFIN)
      this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_6.value=this.w_RAGSOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCONSU_1_7.value==this.w_PANCONSU)
      this.oPgFrm.Page1.oPag.oPANCONSU_1_7.value=this.w_PANCONSU
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCODZON_1_8.value==this.w_PANCODZON)
      this.oPgFrm.Page1.oPag.oPANCODZON_1_8.value=this.w_PANCODZON
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_9.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_9.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCATECOMM_1_10.value==this.w_CATECOMM)
      this.oPgFrm.Page1.oPag.oCATECOMM_1_10.value=this.w_CATECOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNEG_1_11.value==this.w_CODNEG)
      this.oPgFrm.Page1.oPag.oCODNEG_1_11.value=this.w_CODNEG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_16.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_16.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_17.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_17.value=this.w_DESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNEG_1_31.value==this.w_DESNEG)
      this.oPgFrm.Page1.oPag.oDESNEG_1_31.value=this.w_DESNEG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CLIPOS<>'S')  and not(empty(.w_PBANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBANCODI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto oppure cliente P.O.S.")
          case   not(((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBancodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CLIPOS1<>'S')  and not(empty(.w_PEANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEANCODI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto oppure cliente P.O.S.")
          case   not(((empty(.w_RAGSOCFIN)) OR  (.w_RAGSOCINI<=.w_RAGSOCFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CLIPOS2<>'S')  and not(empty(.w_RAGSOCINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRAGSOCINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta oppure cliente P.O.S.")
          case   not(((.w_RAGSOCFIN>=.w_RAGSOCINI) or (empty(.w_RAGSOCINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CLIPOS3<>'S')  and not(empty(.w_RAGSOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRAGSOCFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta oppure cliente P.O.S.")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PANCODZON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPANCODZON_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsps_cliPag1 as StdContainer
  Width  = 560
  height = 259
  stdWidth  = 560
  stdheight = 259
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPBANCODI_1_3 as StdField with uid="DUNCZIVMRQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PBANCODI", cQueryName = "PBANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto oppure cliente P.O.S.",;
    ToolTipText = "Cliente di inizio selezione",;
    HelpContextID = 166133953,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=172, Top=9, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANCODICE", oKey_2_2="this.w_PBANCODI"

  func oPBANCODI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBANCODI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBANCODI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPBANCODI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSPS_ACL.CONTI_VZM',this.parent.oContained
  endproc
  proc oPBANCODI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANCODICE=this.parent.oContained.w_PBANCODI
     i_obj.ecpSave()
  endproc

  add object oPEANCODI_1_4 as StdField with uid="STHPWYOZXF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PEANCODI", cQueryName = "PEANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto oppure cliente P.O.S.",;
    ToolTipText = "Cliente di fine selezione",;
    HelpContextID = 166133185,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=172, Top=34, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANCODICE", oKey_2_2="this.w_PEANCODI"

  func oPEANCODI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEANCODI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEANCODI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPEANCODI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSPS_ACL.CONTI_VZM',this.parent.oContained
  endproc
  proc oPEANCODI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANCODICE=this.parent.oContained.w_PEANCODI
     i_obj.ecpSave()
  endproc

  add object oRAGSOCINI_1_5 as StdField with uid="VKPUFKLWYU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RAGSOCINI", cQueryName = "RAGSOCINI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta oppure cliente P.O.S.",;
    ToolTipText = "Ragione sociale di inizio selezione",;
    HelpContextID = 86088972,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=172, Top=59, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANDESCRI", oKey_2_2="this.w_RAGSOCINI"

  func oRAGSOCINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGSOCINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGSOCINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(this.parent,'oRAGSOCINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSPS_ACL.CONTI_VZM',this.parent.oContained
  endproc
  proc oRAGSOCINI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANDESCRI=this.parent.oContained.w_RAGSOCINI
     i_obj.ecpSave()
  endproc

  add object oRAGSOCFIN_1_6 as StdField with uid="OSOHGLOZBC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RAGSOCFIN", cQueryName = "RAGSOCFIN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta oppure cliente P.O.S.",;
    ToolTipText = "Ragione sociale di fine selezione",;
    HelpContextID = 86088897,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=172, Top=84, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANDESCRI", oKey_2_2="this.w_RAGSOCFIN"

  func oRAGSOCFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGSOCFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGSOCFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(this.parent,'oRAGSOCFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSPS_ACL.CONTI_VZM',this.parent.oContained
  endproc
  proc oRAGSOCFIN_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANDESCRI=this.parent.oContained.w_RAGSOCFIN
     i_obj.ecpSave()
  endproc

  add object oPANCONSU_1_7 as StdField with uid="UCPMPLCJTH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PANCONSU", cQueryName = "PANCONSU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento selezionato",;
    HelpContextID = 170996149,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=172, Top=109, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PANCONSU"

  func oPANCONSU_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCONSU_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCONSU_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPANCONSU_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_SCL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPANCONSU_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PANCONSU
     i_obj.ecpSave()
  endproc

  add object oPANCODZON_1_8 as StdField with uid="INSDLAWTRZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PANCODZON", cQueryName = "PANCODZON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Zona selezionata",;
    HelpContextID = 70331611,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=417, Top=109, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_PANCODZON"

  func oPANCODZON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCODZON_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCODZON_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oPANCODZON_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oPANCODZON_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_PANCODZON
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_9 as StdField with uid="YJNVKXSMLY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile selezionata",;
    HelpContextID = 97463590,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=172, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili",'GSAR_SCL.CACOCLFO_VZM',this.parent.oContained
  endproc
  proc oCATCON_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc

  add object oCATECOMM_1_10 as StdField with uid="FNNUKUNBXH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CATECOMM", cQueryName = "CATECOMM",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale selezionata",;
    HelpContextID = 101789043,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=417, Top=134, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATECOMM"

  func oCATECOMM_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATECOMM_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATECOMM_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATECOMM_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATECOMM_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATECOMM
     i_obj.ecpSave()
  endproc

  add object oCODNEG_1_11 as StdField with uid="ADRORXSOVO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODNEG", cQueryName = "CODNEG",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Negozio di riferimento del cliente",;
    HelpContextID = 238631718,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=172, Top=182, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSPS_KAN", oKey_1_1="BUCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODNEG"

  func oCODNEG_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNEG_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNEG_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODNEG_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_KAN',"Negozi azienda",'',this.parent.oContained
  endproc
  proc oCODNEG_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSPS_KAN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_AZIENDA
     i_obj.w_BUCODICE=this.parent.oContained.w_CODNEG
     i_obj.ecpSave()
  endproc


  add object oBtn_1_13 as StdButton with uid="VCZMCHUPGB",left=452, top=209, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 43456486;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSPS_BLI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="EFNLDLJGWV",left=505, top=209, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 50745158;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCR1_1_16 as StdField with uid="MELWPUDIJJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 150937910,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=310, Top=9, InputMask=replicate('X',40)

  add object oDESCR2_1_17 as StdField with uid="KYHNAZAQJF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 167715126,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=310, Top=34, InputMask=replicate('X',40)

  add object oDESNEG_1_31 as StdField with uid="JWGIIKSIAV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESNEG", cQueryName = "DESNEG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 238690614,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=224, Top=182, InputMask=replicate('X',40)

  add object oStr_1_12 as StdString with uid="AVLXLURTLG",Visible=.t., Left=67, Top=9,;
    Alignment=1, Width=104, Height=15,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BHVQXTHZIC",Visible=.t., Left=67, Top=34,;
    Alignment=1, Width=104, Height=15,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="HGBTOEMDXR",Visible=.t., Left=8, Top=134,;
    Alignment=1, Width=163, Height=15,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ZDUYAETGGJ",Visible=.t., Left=67, Top=109,;
    Alignment=1, Width=104, Height=15,;
    Caption="Mastro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="RDJAYBHXHJ",Visible=.t., Left=359, Top=109,;
    Alignment=1, Width=57, Height=15,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="IBNXRMOAOU",Visible=.t., Left=33, Top=62,;
    Alignment=1, Width=138, Height=18,;
    Caption="Da rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="QAKHWAGHPC",Visible=.t., Left=67, Top=89,;
    Alignment=1, Width=104, Height=18,;
    Caption="A rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="PKWGDEMLRM",Visible=.t., Left=253, Top=134,;
    Alignment=1, Width=163, Height=18,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="BSEDNZTUNR",Visible=.t., Left=100, Top=182,;
    Alignment=1, Width=71, Height=18,;
    Caption="Negozio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="BPAMHYGGLS",Visible=.t., Left=23, Top=159,;
    Alignment=0, Width=304, Height=19,;
    Caption="Valori predefiniti per i clienti generati"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsps_cli','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
