* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bge                                                        *
*              Carica gestioni jbsh                                            *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_45]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-04                                                      *
* Last revis.: 2009-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bge",oParentObject)
return(i_retval)

define class tgsjb_bge as StdBatch
  * --- Local variables
  * --- WorkFile variables
  GESTSJBD_idx=0
  GESTSJBM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.oParentObject.w_GSTIPPAR = IIF(Empty(this.oParentObject.w_GSTIPPAR), "V", this.oParentObject.w_GSTIPPAR)
    this.oParentObject.w_GSTIPESC = IIF(Empty(this.oParentObject.w_GSTIPESC), "S", this.oParentObject.w_GSTIPESC)
    * --- Try
    local bErr_035BD888
    bErr_035BD888=bTrsErr
    this.Try_035BD888()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_ERRORGRU = .t.
    endif
    bTrsErr=bTrsErr or bErr_035BD888
    * --- End
  endproc
  proc Try_035BD888()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_FLMD="M"
      * --- Inserisco Master
      * --- Try
      local bErr_035C7010
      bErr_035C7010=bTrsErr
      this.Try_035C7010()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        if this.oParentObject.w_AGG
          * --- Write into GESTSJBM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.GESTSJBM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.GESTSJBM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"GSDESC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSDESC),'GESTSJBM','GSDESC');
            +",GSTIPESC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSTIPESC),'GESTSJBM','GSTIPESC');
                +i_ccchkf ;
            +" where ";
                +"GSNOME = "+cp_ToStrODBC(this.oParentObject.w_GSNOME);
                   )
          else
            update (i_cTable) set;
                GSDESC = this.oParentObject.w_GSDESC;
                ,GSTIPESC = this.oParentObject.w_GSTIPESC;
                &i_ccchkf. ;
             where;
                GSNOME = this.oParentObject.w_GSNOME;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      bTrsErr=bTrsErr or bErr_035C7010
      * --- End
      * --- Inserisco Detail
      this.oParentObject.w_GSVALVAR = iif(this.oParentObject.w_GSFLGACT="S",this.oParentObject.w_GSVALVAR,"")
      if not(Empty(this.oParentObject.w_GSNOMVAL))
        * --- Try
        local bErr_035C8C60
        bErr_035C8C60=bTrsErr
        this.Try_035C8C60()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          if this.oParentObject.w_AGG
            * --- Write into GESTSJBD
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.GESTSJBD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBD_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.GESTSJBD_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"GSNOMVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOMVAL),'GESTSJBD','GSNOMVAL');
              +",GSFLGACT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSFLGACT),'GESTSJBD','GSFLGACT');
              +",GSVALVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSVALVAR),'GESTSJBD','GSVALVAR');
              +",GSNOMDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOMDES),'GESTSJBD','GSNOMDES');
              +",GSTIPPAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSTIPPAR),'GESTSJBD','GSTIPPAR');
                  +i_ccchkf ;
              +" where ";
                  +"GSNOME = "+cp_ToStrODBC(this.oParentObject.w_GSNOME);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  GSNOMVAL = this.oParentObject.w_GSNOMVAL;
                  ,GSFLGACT = this.oParentObject.w_GSFLGACT;
                  ,GSVALVAR = this.oParentObject.w_GSVALVAR;
                  ,GSNOMDES = this.oParentObject.w_GSNOMDES;
                  ,GSTIPPAR = this.oParentObject.w_GSTIPPAR;
                  &i_ccchkf. ;
               where;
                  GSNOME = this.oParentObject.w_GSNOME;
                  and CPROWNUM = this.oParentObject.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        bTrsErr=bTrsErr or bErr_035C8C60
        * --- End
      endif
      this.oParentObject.w_FLMD = "D"
    else
      * --- Inserisco Detail
      * --- Try
      local bErr_035C5900
      bErr_035C5900=bTrsErr
      this.Try_035C5900()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        if this.oParentObject.w_AGG
          * --- Write into GESTSJBD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.GESTSJBD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.GESTSJBD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"GSNOMVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOMVAL),'GESTSJBD','GSNOMVAL');
            +",GSFLGACT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSFLGACT),'GESTSJBD','GSFLGACT');
            +",GSVALVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSVALVAR),'GESTSJBD','GSVALVAR');
            +",GSNOMDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOMDES),'GESTSJBD','GSNOMDES');
            +",GSTIPPAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSTIPPAR),'GESTSJBD','GSTIPPAR');
                +i_ccchkf ;
            +" where ";
                +"GSNOME = "+cp_ToStrODBC(this.oParentObject.w_GSNOME);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                GSNOMVAL = this.oParentObject.w_GSNOMVAL;
                ,GSFLGACT = this.oParentObject.w_GSFLGACT;
                ,GSVALVAR = this.oParentObject.w_GSVALVAR;
                ,GSNOMDES = this.oParentObject.w_GSNOMDES;
                ,GSTIPPAR = this.oParentObject.w_GSTIPPAR;
                &i_ccchkf. ;
             where;
                GSNOME = this.oParentObject.w_GSNOME;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      bTrsErr=bTrsErr or bErr_035C5900
      * --- End
    endif
    return
  proc Try_035C7010()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GESTSJBM
    i_nConn=i_TableProp[this.GESTSJBM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GESTSJBM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GSNOME"+",GSDESC"+",GSTIPESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOME),'GESTSJBM','GSNOME');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSDESC),'GESTSJBM','GSDESC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSTIPESC),'GESTSJBM','GSTIPESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GSNOME',this.oParentObject.w_GSNOME,'GSDESC',this.oParentObject.w_GSDESC,'GSTIPESC',this.oParentObject.w_GSTIPESC)
      insert into (i_cTable) (GSNOME,GSDESC,GSTIPESC &i_ccchkf. );
         values (;
           this.oParentObject.w_GSNOME;
           ,this.oParentObject.w_GSDESC;
           ,this.oParentObject.w_GSTIPESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_035C8C60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GESTSJBD
    i_nConn=i_TableProp[this.GESTSJBD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GESTSJBD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GSNOME"+",CPROWNUM"+",GSNOMVAL"+",GSNOMDES"+",GSFLGACT"+",GSVALVAR"+",GSTIPPAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOME),'GESTSJBD','GSNOME');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'GESTSJBD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOMVAL),'GESTSJBD','GSNOMVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOMDES),'GESTSJBD','GSNOMDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSFLGACT),'GESTSJBD','GSFLGACT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSVALVAR),'GESTSJBD','GSVALVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSTIPPAR),'GESTSJBD','GSTIPPAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GSNOME',this.oParentObject.w_GSNOME,'CPROWNUM',this.oParentObject.w_CPROWNUM,'GSNOMVAL',this.oParentObject.w_GSNOMVAL,'GSNOMDES',this.oParentObject.w_GSNOMDES,'GSFLGACT',this.oParentObject.w_GSFLGACT,'GSVALVAR',this.oParentObject.w_GSVALVAR,'GSTIPPAR',this.oParentObject.w_GSTIPPAR)
      insert into (i_cTable) (GSNOME,CPROWNUM,GSNOMVAL,GSNOMDES,GSFLGACT,GSVALVAR,GSTIPPAR &i_ccchkf. );
         values (;
           this.oParentObject.w_GSNOME;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_GSNOMVAL;
           ,this.oParentObject.w_GSNOMDES;
           ,this.oParentObject.w_GSFLGACT;
           ,this.oParentObject.w_GSVALVAR;
           ,this.oParentObject.w_GSTIPPAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_035C5900()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GESTSJBD
    i_nConn=i_TableProp[this.GESTSJBD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GESTSJBD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GSNOME"+",CPROWNUM"+",GSNOMVAL"+",GSNOMDES"+",GSFLGACT"+",GSVALVAR"+",GSTIPPAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOME),'GESTSJBD','GSNOME');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'GESTSJBD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOMVAL),'GESTSJBD','GSNOMVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSNOMDES),'GESTSJBD','GSNOMDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSFLGACT),'GESTSJBD','GSFLGACT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSVALVAR),'GESTSJBD','GSVALVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GSTIPPAR),'GESTSJBD','GSTIPPAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GSNOME',this.oParentObject.w_GSNOME,'CPROWNUM',this.oParentObject.w_CPROWNUM,'GSNOMVAL',this.oParentObject.w_GSNOMVAL,'GSNOMDES',this.oParentObject.w_GSNOMDES,'GSFLGACT',this.oParentObject.w_GSFLGACT,'GSVALVAR',this.oParentObject.w_GSVALVAR,'GSTIPPAR',this.oParentObject.w_GSTIPPAR)
      insert into (i_cTable) (GSNOME,CPROWNUM,GSNOMVAL,GSNOMDES,GSFLGACT,GSVALVAR,GSTIPPAR &i_ccchkf. );
         values (;
           this.oParentObject.w_GSNOME;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_GSNOMVAL;
           ,this.oParentObject.w_GSNOMDES;
           ,this.oParentObject.w_GSFLGACT;
           ,this.oParentObject.w_GSVALVAR;
           ,this.oParentObject.w_GSTIPPAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='GESTSJBD'
    this.cWorkTables[2]='GESTSJBM'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
