* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_slg                                                        *
*              Stampa log                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-29                                                      *
* Last revis.: 2009-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsjb_slg",oParentObject))

* --- Class definition
define class tgsjb_slg as StdForm
  Top    = 8
  Left   = 7

  * --- Standard Properties
  Width  = 585
  Height = 148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-01-13"
  HelpContextID=208301673
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  SCHEDJOB_IDX = 0
  LOGELAJB_IDX = 0
  cPrg = "gsjb_slg"
  cComment = "Stampa log"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LSSERIAL1 = space(10)
  o_LSSERIAL1 = space(10)
  w_SHCODICE1 = 0
  w_SHDESCRI1 = space(254)
  w_LSSERIAL2 = space(10)
  w_SHCODICE2 = 0
  w_SHDESCRI2 = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsjb_slgPag1","gsjb_slg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLSSERIAL1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='SCHEDJOB'
    this.cWorkTables[2]='LOGELAJB'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LSSERIAL1=space(10)
      .w_SHCODICE1=0
      .w_SHDESCRI1=space(254)
      .w_LSSERIAL2=space(10)
      .w_SHCODICE2=0
      .w_SHDESCRI2=space(254)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_LSSERIAL1))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SHCODICE1))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_LSSERIAL2 = .w_LSSERIAL1
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_LSSERIAL2))
          .link_1_6('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_SHCODICE2))
          .link_1_7('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
    this.DoRTCalc(6,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,3,.t.)
        if .o_LSSERIAL1<>.w_LSSERIAL1
            .w_LSSERIAL2 = .w_LSSERIAL1
          .link_1_6('Full')
        endif
          .link_1_7('Full')
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LSSERIAL1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_lTable = "LOGELAJB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2], .t., this.LOGELAJB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LSSERIAL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSJB_MLG',True,'LOGELAJB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSSERIAL like "+cp_ToStrODBC(trim(this.w_LSSERIAL1)+"%");

          i_ret=cp_SQL(i_nConn,"select LSSERIAL,LSCODJOB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSSERIAL',trim(this.w_LSSERIAL1))
          select LSSERIAL,LSCODJOB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LSSERIAL1)==trim(_Link_.LSSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LSSERIAL1) and !this.bDontReportError
            deferred_cp_zoom('LOGELAJB','*','LSSERIAL',cp_AbsName(oSource.parent,'oLSSERIAL1_1_1'),i_cWhere,'GSJB_MLG',"Log schedulatore",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL,LSCODJOB";
                     +" from "+i_cTable+" "+i_lTable+" where LSSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',oSource.xKey(1))
            select LSSERIAL,LSCODJOB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LSSERIAL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL,LSCODJOB";
                   +" from "+i_cTable+" "+i_lTable+" where LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',this.w_LSSERIAL1)
            select LSSERIAL,LSCODJOB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LSSERIAL1 = NVL(_Link_.LSSERIAL,space(10))
      this.w_SHCODICE1 = NVL(_Link_.LSCODJOB,0)
    else
      if i_cCtrl<>'Load'
        this.w_LSSERIAL1 = space(10)
      endif
      this.w_SHCODICE1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LSSERIAL2) or (.w_LSSERIAL2>=.w_LSSERIAL1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LSSERIAL1 = space(10)
        this.w_SHCODICE1 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])+'\'+cp_ToStr(_Link_.LSSERIAL,1)
      cp_ShowWarn(i_cKey,this.LOGELAJB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LSSERIAL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SHCODICE1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_lTable = "SCHEDJOB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2], .t., this.SCHEDJOB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SHCODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SHCODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(this.w_SHCODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',this.w_SHCODICE1)
            select SHCODICE,SHDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SHCODICE1 = NVL(_Link_.SHCODICE,0)
      this.w_SHDESCRI1 = NVL(_Link_.SHDESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_SHCODICE1 = 0
      endif
      this.w_SHDESCRI1 = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])+'\'+cp_ToStr(_Link_.SHCODICE,1)
      cp_ShowWarn(i_cKey,this.SCHEDJOB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SHCODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LSSERIAL2
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_lTable = "LOGELAJB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2], .t., this.LOGELAJB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LSSERIAL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSJB_MLG',True,'LOGELAJB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSSERIAL like "+cp_ToStrODBC(trim(this.w_LSSERIAL2)+"%");

          i_ret=cp_SQL(i_nConn,"select LSSERIAL,LSCODJOB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSSERIAL',trim(this.w_LSSERIAL2))
          select LSSERIAL,LSCODJOB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LSSERIAL2)==trim(_Link_.LSSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LSSERIAL2) and !this.bDontReportError
            deferred_cp_zoom('LOGELAJB','*','LSSERIAL',cp_AbsName(oSource.parent,'oLSSERIAL2_1_6'),i_cWhere,'GSJB_MLG',"Log schedulatore",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL,LSCODJOB";
                     +" from "+i_cTable+" "+i_lTable+" where LSSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',oSource.xKey(1))
            select LSSERIAL,LSCODJOB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LSSERIAL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL,LSCODJOB";
                   +" from "+i_cTable+" "+i_lTable+" where LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',this.w_LSSERIAL2)
            select LSSERIAL,LSCODJOB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LSSERIAL2 = NVL(_Link_.LSSERIAL,space(10))
      this.w_SHCODICE2 = NVL(_Link_.LSCODJOB,0)
    else
      if i_cCtrl<>'Load'
        this.w_LSSERIAL2 = space(10)
      endif
      this.w_SHCODICE2 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_LSSERIAL2>=.w_LSSERIAL1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LSSERIAL2 = space(10)
        this.w_SHCODICE2 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])+'\'+cp_ToStr(_Link_.LSSERIAL,1)
      cp_ShowWarn(i_cKey,this.LOGELAJB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LSSERIAL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SHCODICE2
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_lTable = "SCHEDJOB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2], .t., this.SCHEDJOB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SHCODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SHCODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(this.w_SHCODICE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',this.w_SHCODICE2)
            select SHCODICE,SHDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SHCODICE2 = NVL(_Link_.SHCODICE,0)
      this.w_SHDESCRI2 = NVL(_Link_.SHDESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_SHCODICE2 = 0
      endif
      this.w_SHDESCRI2 = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])+'\'+cp_ToStr(_Link_.SHCODICE,1)
      cp_ShowWarn(i_cKey,this.SCHEDJOB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SHCODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLSSERIAL1_1_1.value==this.w_LSSERIAL1)
      this.oPgFrm.Page1.oPag.oLSSERIAL1_1_1.value=this.w_LSSERIAL1
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDESCRI1_1_4.value==this.w_SHDESCRI1)
      this.oPgFrm.Page1.oPag.oSHDESCRI1_1_4.value=this.w_SHDESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oLSSERIAL2_1_6.value==this.w_LSSERIAL2)
      this.oPgFrm.Page1.oPag.oLSSERIAL2_1_6.value=this.w_LSSERIAL2
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDESCRI2_1_8.value==this.w_SHDESCRI2)
      this.oPgFrm.Page1.oPag.oSHDESCRI2_1_8.value=this.w_SHDESCRI2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_LSSERIAL2) or (.w_LSSERIAL2>=.w_LSSERIAL1))  and not(empty(.w_LSSERIAL1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLSSERIAL1_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_LSSERIAL2>=.w_LSSERIAL1))  and not(empty(.w_LSSERIAL2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLSSERIAL2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LSSERIAL1 = this.w_LSSERIAL1
    return

enddefine

* --- Define pages as container
define class tgsjb_slgPag1 as StdContainer
  Width  = 581
  height = 148
  stdWidth  = 581
  stdheight = 148
  resizeXpos=302
  resizeYpos=63
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLSSERIAL1_1_1 as StdField with uid="KKGPMNFMAC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LSSERIAL1", cQueryName = "LSSERIAL1",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Log inizio selezione",;
    HelpContextID = 33561874,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=74, Top=11, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="LOGELAJB", cZoomOnZoom="GSJB_MLG", oKey_1_1="LSSERIAL", oKey_1_2="this.w_LSSERIAL1"

  func oLSSERIAL1_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oLSSERIAL1_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLSSERIAL1_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LOGELAJB','*','LSSERIAL',cp_AbsName(this.parent,'oLSSERIAL1_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSJB_MLG',"Log schedulatore",'',this.parent.oContained
  endproc
  proc oLSSERIAL1_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSJB_MLG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSSERIAL=this.parent.oContained.w_LSSERIAL1
     i_obj.ecpSave()
  endproc

  add object oSHDESCRI1_1_4 as StdField with uid="CTBFUPJVCF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SHDESCRI1", cQueryName = "SHDESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del job elaborato",;
    HelpContextID = 202318463,;
   bGlobalFont=.t.,;
    Height=21, Width=398, Left=174, Top=11, InputMask=replicate('X',254)

  add object oLSSERIAL2_1_6 as StdField with uid="NGOJIVFAYX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LSSERIAL2", cQueryName = "LSSERIAL2",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Log fine selezione",;
    HelpContextID = 33561890,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=74, Top=36, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="LOGELAJB", cZoomOnZoom="GSJB_MLG", oKey_1_1="LSSERIAL", oKey_1_2="this.w_LSSERIAL2"

  func oLSSERIAL2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oLSSERIAL2_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLSSERIAL2_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LOGELAJB','*','LSSERIAL',cp_AbsName(this.parent,'oLSSERIAL2_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSJB_MLG',"Log schedulatore",'',this.parent.oContained
  endproc
  proc oLSSERIAL2_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSJB_MLG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSSERIAL=this.parent.oContained.w_LSSERIAL2
     i_obj.ecpSave()
  endproc

  add object oSHDESCRI2_1_8 as StdField with uid="LKTDHQMQSN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SHDESCRI2", cQueryName = "SHDESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del job elaborato",;
    HelpContextID = 202318479,;
   bGlobalFont=.t.,;
    Height=21, Width=398, Left=174, Top=36, InputMask=replicate('X',254)


  add object oObj_1_9 as cp_outputCombo with uid="ZLERXIVPWT",left=113, top=69, width=384,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 238131430


  add object oBtn_1_10 as StdButton with uid="APIWEFNJEL",left=470, top=95, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 201923366;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="ZJIUAZYUNY",left=521, top=95, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 200984250;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="XJEUCEXOXA",Visible=.t., Left=6, Top=40,;
    Alignment=1, Width=61, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ETWPVPNWPP",Visible=.t., Left=3, Top=15,;
    Alignment=1, Width=64, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="HCVGMFFTWH",Visible=.t., Left=9, Top=71,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_slg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
