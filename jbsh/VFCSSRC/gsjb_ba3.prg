* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_ba3                                                        *
*              Aggiorna elenco processi della proattivit�                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-15                                                      *
* Last revis.: 2008-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_ba3",oParentObject)
return(i_retval)

define class tgsjb_ba3 as StdBatch
  * --- Local variables
  w_JBCODAZI = space(5)
  w_JBNOME = space(20)
  w_JBDESC = space(254)
  w_JBTIPESC = space(1)
  w_TROVATO = .f.
  w_CPROWNUM = 0
  w_CPROWORD = 0
  * --- WorkFile variables
  LISTAJOB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- Programma da lanciare
    this.w_JBNOME = "GSJB_BXP WITH THIS"
    * --- Descrizione del programma da lanciare
    this.w_JBDESC = "Esecuzione della proattivita"
    * --- Tipologia di uscita
    this.w_JBTIPESC = "N"
    SELECT( this.oParentObject.w_AZIPROCE.cCURSOR )
    GO TOP
    do while NOT EOF()
      this.w_JBCODAZI = AZCODAZI
      if XCHK = 1
        * --- Attiva la proattivit� sull'azienda selezionata
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Disattiva la proattivit� sull'azienda selezionata
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT( this.oParentObject.w_AZIPROCE.cCURSOR )
      SKIP
    enddo
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se il processo � gi� inserito per l'azienda corrente
    this.w_TROVATO = .F.
    * --- Select from LISTAJOB
    i_nConn=i_TableProp[this.LISTAJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2],.t.,this.LISTAJOB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" LISTAJOB ";
          +" where JBCODICEJOB = "+cp_ToStrODBC(this.oParentObject.w_SHCODICE)+"";
           ,"_Curs_LISTAJOB")
    else
      select * from (i_cTable);
       where JBCODICEJOB = this.oParentObject.w_SHCODICE;
        into cursor _Curs_LISTAJOB
    endif
    if used('_Curs_LISTAJOB')
      select _Curs_LISTAJOB
      locate for 1=1
      do while not(eof())
      this.w_CPROWNUM = _Curs_LISTAJOB.CPROWNUM
      if this.w_JBCODAZI = _Curs_LISTAJOB.JBCODAZI
        this.w_TROVATO = .T.
      endif
        select _Curs_LISTAJOB
        continue
      enddo
      use
    endif
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWNUM * 10
    if NOT this.w_TROVATO
      * --- Inserisce il record per l'azienda selezionata
      * --- Insert into LISTAJOB
      i_nConn=i_TableProp[this.LISTAJOB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LISTAJOB_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"JBCODICEJOB"+",CPROWNUM"+",CPROWORD"+",JBNOME"+",JBDESC"+",JBROUTINE"+",JBCODAZI"+",JBTIPESC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SHCODICE),'LISTAJOB','JBCODICEJOB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LISTAJOB','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'LISTAJOB','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LISTAJOB','JBNOME');
        +","+cp_NullLink(cp_ToStrODBC(this.w_JBDESC),'LISTAJOB','JBDESC');
        +","+cp_NullLink(cp_ToStrODBC("R"),'LISTAJOB','JBROUTINE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_JBCODAZI),'LISTAJOB','JBCODAZI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_JBTIPESC),'LISTAJOB','JBTIPESC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'JBCODICEJOB',this.oParentObject.w_SHCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'JBNOME',this.w_JBNOME,'JBDESC',this.w_JBDESC,'JBROUTINE',"R",'JBCODAZI',this.w_JBCODAZI,'JBTIPESC',this.w_JBTIPESC)
        insert into (i_cTable) (JBCODICEJOB,CPROWNUM,CPROWORD,JBNOME,JBDESC,JBROUTINE,JBCODAZI,JBTIPESC &i_ccchkf. );
           values (;
             this.oParentObject.w_SHCODICE;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_JBNOME;
             ,this.w_JBDESC;
             ,"R";
             ,this.w_JBCODAZI;
             ,this.w_JBTIPESC;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Delete from LISTAJOB
    i_nConn=i_TableProp[this.LISTAJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"JBCODICEJOB = "+cp_ToStrODBC(this.oParentObject.w_SHCODICE);
            +" and JBCODAZI = "+cp_ToStrODBC(this.w_JBCODAZI);
             )
    else
      delete from (i_cTable) where;
            JBCODICEJOB = this.oParentObject.w_SHCODICE;
            and JBCODAZI = this.w_JBCODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LISTAJOB'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_LISTAJOB')
      use in _Curs_LISTAJOB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
