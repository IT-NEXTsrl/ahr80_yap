* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bcp                                                        *
*              Copia processo fra aziende                                      *
*                                                                              *
*      Author: ZUCCHETTI SPA - CS                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-19                                                      *
* Last revis.: 2007-08-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bcp",oParentObject,m.pTIPO)
return(i_retval)

define class tgsjb_bcp as StdBatch
  * --- Local variables
  pTIPO = space(1)
  w_MASK = .NULL.
  w_ZOOM = .NULL.
  w_MAXROW = 0
  w_CODEAZI = space(5)
  * --- WorkFile variables
  LISTAJOB_idx=0
  PARAMJ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la duplicazione di un processo fra aziende
    *     pTIPO = 'A' Aggiorna dati
    *     pTIPO = 'S' Seleziona deseleziona
    this.w_MASK = this.oParentObject
    this.w_ZOOM = this.w_MASK.w_ZOOMAZI
    NC = this.w_ZOOM.cCursor
    do case
      case this.pTIPO = "A"
        SELECT * FROM &NC WHERE XCHK = 1 INTO CURSOR ELAB
        if USED("ELAB")
          * --- selezionio l'ultimo rownum dei processi
          * --- Select from LISTAJOB
          i_nConn=i_TableProp[this.LISTAJOB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2],.t.,this.LISTAJOB_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS MAXROW  from "+i_cTable+" LISTAJOB ";
                +" where JBCODICEJOB = "+cp_ToStrODBC(this.oParentObject.w_JOB_NAME)+"";
                 ,"_Curs_LISTAJOB")
          else
            select MAX(CPROWNUM) AS MAXROW from (i_cTable);
             where JBCODICEJOB = this.oParentObject.w_JOB_NAME;
              into cursor _Curs_LISTAJOB
          endif
          if used('_Curs_LISTAJOB')
            select _Curs_LISTAJOB
            locate for 1=1
            do while not(eof())
            this.w_MAXROW = NVL(_Curs_LISTAJOB.MAXROW, 0)
              select _Curs_LISTAJOB
              continue
            enddo
            use
          endif
          * --- Try
          local bErr_03C65018
          bErr_03C65018=bTrsErr
          this.Try_03C65018()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_errormsg( "Aggiornamento fallito!")
          endif
          bTrsErr=bTrsErr or bErr_03C65018
          * --- End
          SELECT ELAB
          USE
        endif
      case this.pTIPO = "S"
        NC = this.w_ZOOM.cCursor
        * --- Seleziona/Deseleziona Tutto 
        if this.oParentObject.w_SELEZI = "S"
          UPDATE &NC SET XCHK = 1
        else
          UPDATE &NC SET XCHK = 0
        endif
    endcase
  endproc
  proc Try_03C65018()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT ELAB
    GO TOP
    SCAN
    AH_MSG("Elaborazione azienda %1", .t., .f., .f., this.w_CODEAZI)
    this.w_CODEAZI = ELAB.AZCODAZI
    this.w_MAXROW = this.w_MAXROW + 1
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    ah_errormsg( "Aggiornamento effettuato con successo")
    this.w_MASK.NotifyEvent("ShowDati")     
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Insert into LISTAJOB
    i_nConn=i_TableProp[this.LISTAJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsjb4kcp",this.LISTAJOB_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into PARAMJ
    i_nConn=i_TableProp[this.PARAMJ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARAMJ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsjb5kcp",this.PARAMJ_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LISTAJOB'
    this.cWorkTables[2]='PARAMJ'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_LISTAJOB')
      use in _Curs_LISTAJOB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
