* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bde                                                        *
*              Calcolo prossima data schedulazione                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-14                                                      *
* Last revis.: 2018-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_data,w_DATASTART
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bde",oParentObject,m.w_data,m.w_DATASTART)
return(i_retval)

define class tgsjb_bde as StdBatch
  * --- Local variables
  w_data = ctod("  /  /  ")
  w_DATASTART = ctod("  /  /  ")
  w_countgg = 0
  w_dataAppo = ctod("  /  /  ")
  w_mese = 0
  w_meseAppo = 0
  w_countmm = 0
  w_anno = 0
  w_giorno = 0
  w_giornoAppo = space(3)
  w_week = 0
  w_oraatt = space(5)
  w_DATSTART = ctod("  /  /  ")
  w_ora_pros = space(2)
  w_mm_pros = space(2)
  w_orapp = space(2)
  w_mmapp = space(2)
  w_SECONDIINT = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Calcolo della data prossima esecuzione del lancio del processo, 
    *     lanciato da GSJB_ASH e GSJB_BSJ
    * --- Se il parametro � vuoto metto data vuota
    this.w_DATASTART = IIF(TYPE("w_DATASTART")<>"D", {}, this.w_DATASTART)
    * --- --tipo della frequenza temporale, unico, giornaliero, settimanale, mensile
    * --- --Cadenza ripetizione del lancio del processo
    * --- Giorni della settimana selezionati
    * --- --Mesi selezionati
    * --- Tipo data mese scelta
    * --- --Inizializzo le variabili
    this.w_oraatt = SUBSTR(TIME(),1,5)
    this.w_ora_pros = substr(this.w_oraatt,1,2)
    this.w_mm_pros = substr(this.w_oraatt,4,2)
    this.w_giorno = DAY(this.w_data)
    this.w_mese = MONTH(this.w_data)
    this.w_anno = YEAR(this.w_data)
    this.oParentObject.w_SHORAFIN = This.MakeTime(this.oParentObject.w_ora_lancifi, this.oParentObject.w_mm_lancifi)
    if UPPER(this.oParentObject.class)="TGSJB_ASH"
      * --- Se il batch � stato lanciato dall'anagrafica schedulatore, valorizzo l'ora di
      *     prossima esecuzione e l'ora iniziale con l'ora e minuti che ho specificato sulla
      *     gestione
      * --- MakeTime, definita nell'area manuale,
      *     ritorna una stringa di 5 caretteri nel formato pOra:pMin, aggiunge lo zero iniziale se non � presente  
      this.oParentObject.w_SHORAPROS = This.MakeTime(this.oParentObject.w_ora_lancio, this.oParentObject.w_mm_lancio)
      this.oParentObject.w_SHORAINI = this.oParentObject.w_SHORAPROS
    else
      * --- Se il batch � lanciato da GSJB_BSJ devo ricalcolare l'ora utilizzando, se � 
      *     presente l'intervallo di frequenza
      if this.oParentObject.w_SHMINUINT<>0 and (this.w_data=this.w_DATASTART and (VAL(this.oParentObject.w_ora_lancifi)*60+VAL(this.oParentObject.w_mm_lancifi)) >= (VAL(this.w_ora_pros)*60 + VAL(this.w_mm_pros) + this.oParentObject.w_SHMINUINT) OR this.oParentObject.w_SHFREQ="R")
        * --- Se ho un intervallo specificato e non supero l'ora finale se sommo all'ora attuale
        *     l'intervallo di frequenza
        * --- Calcolo ora e minuti di prossima esecuzione
        *     Come ora di partenza utilizzo l'ora attuale cos�, sfruttando il controllo sull'ora attuale 
        *     che viene effettuato per il calcolo della data di prossima esecuzione, riesco
        *     da non effettuare il ricalcolo della data
        this.w_orapp = ALLTRIM(STR(MOD(VAL(this.w_ora_pros) + INT((VAL(this.w_mm_pros) + this.oParentObject.w_SHMINUINT) / 60),24)))
        this.w_mmapp = ALLTRIM(STR(MOD(VAL(this.w_mm_pros) + this.oParentObject.w_SHMINUINT, 60)))
        this.oParentObject.w_SHORAPROS = This.MakeTime(this.w_orapp ,this.w_mmapp)
        if this.oParentObject.w_SHFREQ="R"
          this.w_SECONDIINT =  this.oParentObject.w_SHMINUINT*60
          this.oParentObject.w_SHDATAPRO = DATETIME(this.w_anno,this.w_mese,this.w_giorno, val(this.w_ora_pros),val(this.w_mm_pros),00)+this.w_SECONDIINT
          this.oParentObject.w_SHDATAPRO = TTOD(this.oParentObject.w_SHDATAPRO)
        endif
      else
        * --- Se non ho un intervallo di frequenza o ho superato l'ora finale, resetto l'ora
        *     di prossima esecuzione
        this.oParentObject.w_SHORAPROS = this.oParentObject.w_SHORAINI
      endif
    endif
    this.w_countgg = 0
    this.w_countmm = 0
    * --- --Calcolo la data di prossima esecuzione
    do case
      case this.oParentObject.w_SHFREQ="U"
        * --- --Il processo deve essere lanciato una sola volta alla data e ora stabilita
        this.oParentObject.w_SHDATAPRO = IIF(empty(this.oParentObject.w_SHDATAPRO),Date(this.w_anno,this.w_mese,this.w_giorno),this.oParentObject.w_SHDATAPRO)
      case this.oParentObject.w_SHFREQ="G"
        * --- --Il processo deve essere lanciato giornalmente all'ora stabilita
        if this.w_oraatt>=this.oParentObject.w_SHORAPROS
          this.oParentObject.w_SHDATAPRO = Date(this.w_anno,this.w_mese,this.w_giorno)+int(this.oParentObject.w_SHNUMFREQ)
        else
          this.oParentObject.w_SHDATAPRO = Date(this.w_anno,this.w_mese,this.w_giorno)
        endif
      case this.oParentObject.w_SHFREQ="S"
        * --- --Il processo deve essere lanciato settimanalmente all'ora stabilita
        if NOT EMPTY(this.oParentObject.w_SHSET)
          this.w_DATSTART = this.w_data
          if this.w_oraatt>=this.oParentObject.w_SHORAPROS
            this.w_countgg = 1
            this.w_giornoAppo = cdow(this.w_DATSTART+int(this.w_countgg))
          else
            this.w_giornoAppo = cdow(this.w_DATSTART)
          endif
          this.w_week = week(this.w_DATSTART)
          do while NOT (this.w_giornoAppo $ this.oParentObject.w_SHSET)
            this.w_countgg = this.w_countgg+1
            this.w_giornoAppo = cdow(this.w_DATSTART+int(this.w_countgg))
          enddo
          if this.w_week<>week(this.w_DATSTART+int(this.w_countgg))
            this.w_giorno = day(this.w_DATSTART+ (int(this.w_countgg+7*(this.oParentObject.w_SHNUMFREQ-1))))
            this.w_mese = MONTH(this.w_DATSTART+(int(this.w_countgg+7*(this.oParentObject.w_SHNUMFREQ-1))))
            this.w_anno = YEAR(this.w_DATSTART+(int(this.w_countgg+7*(this.oParentObject.w_SHNUMFREQ-1))))
            this.oParentObject.w_SHDATAPRO = DATE(this.w_anno,this.w_mese,this.w_giorno)
          else
            this.w_giorno = day(this.w_DATSTART+int(this.w_countgg))
            this.w_mese = MONTH(this.w_DATSTART+(int(this.w_countgg)))
            this.w_anno = YEAR(this.w_DATSTART+(int(this.w_countgg)))
            this.oParentObject.w_SHDATAPRO = DATE(this.w_anno,this.w_mese,this.w_giorno)
          endif
        else
          this.oParentObject.w_SHDATAPRO = cp_CharToDate("  -  -    ")
          this.oParentObject.w_SHORAPROS = " "
        endif
      case this.oParentObject.w_SHFREQ="M"
        * --- --Il processo deve essere lanciato mensilmente all'ora stabilita
        if NOT EMPTY(this.oParentObject.w_SHMESE)
          this.w_DATSTART = this.w_data
          this.w_mese = MONTH(this.w_DATSTART)
          this.w_anno = YEAR(this.w_DATSTART)
          * --- Calcolo il giorno di esecuzione, necessario per controllare se devo partire dal
          *     mese corrente o passare al mese successivo
          if this.oParentObject.w_SHIMPDAT="F"
            this.w_dataAppo = DATE(this.w_anno + IIF(this.w_mese + 1 = 12, 0, int((this.w_mese + 1) / 12)), IIF(this.w_mese + 1 = 12, 12, MOD( this.w_mese + 1,12 )), 1)
            this.w_giorno = day(this.w_dataAppo-1)
          else
            this.w_giorno = this.oParentObject.w_SHDATFIS
          endif
          * --- Controllo se devo passare al mese successivo
          if (this.w_data > DATE(this.w_anno,this.w_mese,this.w_giorno)) OR (this.w_data = DATE(this.w_anno,this.w_mese,this.w_giorno) AND this.w_oraatt>=this.oParentObject.w_SHORAPROS)
            this.w_mese = this.w_mese + 1
          endif
          * --- Calcolo il nome del mese di partenza
          this.w_meseAppo = CMONTH(DATE(this.w_anno + IIF(this.w_mese = 12, 0, int(this.w_mese / 12)), IIF(this.w_mese = 12, 12, MOD( this.w_mese,12 )), 1))
          * --- Calcolo il mese e l'anno di prossima esecuzione
          do while not(this.w_meseAppo $ this.oParentObject.w_SHMESE)
            this.w_mese = this.w_mese + 1
            this.w_meseAppo = CMONTH(DATE(this.w_anno + IIF(this.w_mese = 12, 0, int(this.w_mese / 12)) , IIF(this.w_mese = 12, 12, MOD( this.w_mese,12 )), 1))
          enddo
          this.w_anno = this.w_anno + IIF(this.w_mese = 12, 0, int(this.w_mese / 12))
          this.w_mese = IIF(this.w_mese = 12, 12, MOD( this.w_mese,12 ))
          * --- Ricalcolo il giorno di esecuzione
          if this.oParentObject.w_SHIMPDAT="F" 
 
            this.w_dataAppo = DATE(this.w_anno + IIF(this.w_mese + 1 = 12, 0, int((this.w_mese + 1) / 12)), IIF(this.w_mese + 1 = 12, 12, MOD( this.w_mese + 1,12 )), 1)
            this.w_giorno = day(this.w_dataAppo-1)
          else
            this.w_giorno = this.oParentObject.w_SHDATFIS
          endif
          * --- Controllo che la data ottenuta non sia vuota (es. 30/02/04)
          if EMPTY(DATE(this.w_anno,this.w_mese,this.w_giorno))
            this.oParentObject.w_SHDATAPRO = DATE(this.w_anno,this.w_mese+1,1)-1
          else
            this.oParentObject.w_SHDATAPRO = DATE(this.w_anno,this.w_mese,this.w_giorno)
          endif
        else
          * --- Non ho specificato nessun mese
          this.oParentObject.w_SHDATAPRO = cp_CharToDate("  -  -    ")
          this.oParentObject.w_SHORAPROS = " "
        endif
    endcase
  endproc


  proc Init(oParentObject,w_data,w_DATASTART)
    this.w_data=w_data
    this.w_DATASTART=w_DATASTART
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gsjb_bde
  * --- MakeTime, ritorna una stringa di 5 caretteri nel formato
  * --- pOra:pMin, aggiunge lo zero iniziale se non � presente  
  Func MakeTime(pOra, pMin)
  Return IIF(LEN(ALLTRIM(pOra)) < 2, '0' + ALLTRIM(pOra), pOra) + ':' + ;
         IIF(LEN(ALLTRIM(pMin)) < 2, '0' + ALLTRIM(pMin), pMin)
  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_data,w_DATASTART"
endproc
