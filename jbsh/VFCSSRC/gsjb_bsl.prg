* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bsl                                                        *
*              Lancia anag. sched./ log sched.                                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-13                                                      *
* Last revis.: 2008-06-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM,w_PARAM2
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bsl",oParentObject,m.w_PARAM,m.w_PARAM2)
return(i_retval)

define class tgsjb_bsl as StdBatch
  * --- Local variables
  w_PARAM = space(1)
  w_PARAM2 = space(10)
  w_SHTIPSCH = space(1)
  w_gsjb_ash = .NULL.
  w_gsjb_mlg = .NULL.
  * --- WorkFile variables
  SCHEDJOB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_PARAM="S"
        * --- Read from SCHEDJOB
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2],.t.,this.SCHEDJOB_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SHTIPSCH"+;
            " from "+i_cTable+" SCHEDJOB where ";
                +"SHCODICE = "+cp_ToStrODBC(this.w_PARAM2);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SHTIPSCH;
            from (i_cTable) where;
                SHCODICE = this.w_PARAM2;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SHTIPSCH = NVL(cp_ToDate(_read_.SHTIPSCH),cp_NullValue(_read_.SHTIPSCH))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_SHTIPSCH = NVL( this.w_SHTIPSCH , "N" )
        this.w_gsjb_ash = gsjb_ash( this.w_SHTIPSCH )
        if !(this.w_gsjb_ash.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Caricamento
        this.w_gsjb_ash.w_SHCODICE = this.w_PARAM2
        this.w_gsjb_ash.QueryKeySet("SHCODICE="+ str(this.w_PARAM2) )     
        this.w_gsjb_ash.LoadRecWarn()     
      otherwise
        this.w_gsjb_mlg = gsjb_mlg()
        if !(this.w_gsjb_mlg.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Caricamento
        this.w_gsjb_mlg.W_LSSERIAL = this.w_PARAM2
        this.w_gsjb_mlg.QueryKeySet("LSSERIAL='"+ this.w_PARAM2 +"'")     
        this.w_gsjb_mlg.LoadRecWarn()     
    endcase
  endproc


  proc Init(oParentObject,w_PARAM,w_PARAM2)
    this.w_PARAM=w_PARAM
    this.w_PARAM2=w_PARAM2
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SCHEDJOB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM,w_PARAM2"
endproc
