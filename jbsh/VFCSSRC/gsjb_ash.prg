* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_ash                                                        *
*              Schedulatore di job/Proattivit�                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-12                                                      *
* Last revis.: 2018-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsjb_ash
Parameter pTipSch
* --- Fine Area Manuale
return(createobject("tgsjb_ash"))

* --- Class definition
define class tgsjb_ash as StdForm
  Top    = 2
  Left   = 11

  * --- Standard Properties
  Width  = 739
  Height = 437+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-03"
  HelpContextID=109735529
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=76

  * --- Constant Properties
  SCHEDJOB_IDX = 0
  LOGELAJB_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  cFile = "SCHEDJOB"
  cKeySelect = "SHCODICE"
  cKeyWhere  = "SHCODICE=this.w_SHCODICE"
  cKeyWhereODBC = '"SHCODICE="+cp_ToStrODBC(this.w_SHCODICE)';

  cKeyWhereODBCqualified = '"SCHEDJOB.SHCODICE="+cp_ToStrODBC(this.w_SHCODICE)';

  cPrg = "gsjb_ash"
  cComment = "Schedulatore di job/Proattivit�"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SHCODICE = 0
  w_SHFLAT = space(1)
  w_GRPSCHED = 0
  w_SHUTEPRE = 0
  w_SHDESCRI = space(254)
  w_SHUTECREA = 0
  w_DESUTEC = space(20)
  w_SHUTEVAR = 0
  w_DESUTEV = space(20)
  w_SHDATACREA = ctod('  /  /  ')
  w_SHDATAVAR = ctod('  /  /  ')
  w_SHRELOAD = space(1)
  w_SHDATAPRO = ctod('  /  /  ')
  w_SHORAPROS = space(5)
  w_SHORAINI = space(5)
  o_SHORAINI = space(5)
  w_SHORAFIN = space(5)
  o_SHORAFIN = space(5)
  w_SHTMPMAX = space(5)
  o_SHTMPMAX = space(5)
  w_ora_attesa = space(2)
  o_ora_attesa = space(2)
  w_mm_attesa = space(2)
  o_mm_attesa = space(2)
  w_SHFREQ = space(1)
  o_SHFREQ = space(1)
  w_SHDATASTART = ctod('  /  /  ')
  w_SHORAINE = space(5)
  w_SHDATAFINE = ctod('  /  /  ')
  w_SHORAFNE = space(5)
  w_SHNUMFREQ = 0
  w_ora_lancio = space(2)
  o_ora_lancio = space(2)
  w_mm_lancio = space(2)
  o_mm_lancio = space(2)
  w_SHMINUINT = 0
  w_ora_lancifi = space(2)
  o_ora_lancifi = space(2)
  w_mm_lancifi = space(2)
  o_mm_lancifi = space(2)
  w_SHPATH = space(200)
  o_SHPATH = space(200)
  w_SHFLGSTC = space(1)
  w_SHFLINVS = space(1)
  o_SHFLINVS = space(1)
  w_SHSET = space(50)
  o_SHSET = space(50)
  w_SHMESE = space(75)
  o_SHMESE = space(75)
  w_lun = space(10)
  w_Mar = space(10)
  w_Mer = space(10)
  w_Gio = space(10)
  w_Ven = space(10)
  w_Sab = space(10)
  w_Dom = space(10)
  w_Gen = space(10)
  w_Feb = space(10)
  w_Marz = space(10)
  w_Apr = space(10)
  w_Mag = space(10)
  w_Giu = space(10)
  w_Lug = space(10)
  w_Ago = space(10)
  w_Set = space(10)
  w_Ott = space(10)
  w_Nov = space(10)
  w_Dic = space(10)
  w_SHIMPDAT = space(1)
  w_SHDATFIS = 0
  w_DataInizio = ctod('  /  /  ')
  w_SHTIPSCH = space(1)
  o_SHTIPSCH = space(1)
  w_SHTIPELA = space(1)
  o_SHTIPELA = space(1)
  w_SHELABOR = space(250)
  w_SHTIPRES = space(1)
  w_SHFRMRES = space(1)
  o_SHFRMRES = space(1)
  w_SHREPRES = space(250)
  w_SHTXTHEA = space(0)
  w_SHTXTBOD = space(0)
  w_SHTXTFOO = space(0)
  w_SHAGENDA = space(1)
  o_SHAGENDA = space(1)
  w_SHDESTIN = space(250)
  w_SHFINPRE = ctot('')
  w_MEMO = space(0)
  w_LOG = space(10)
  w_CPROWNUM = 0
  w_SELEZI = space(1)
  w_LPATH = .F.
  w_SHDATMAN = space(1)
  o_SHDATMAN = space(1)
  w_DESUTEP = space(20)

  * --- Autonumbered Variables
  op_SHCODICE = this.W_SHCODICE

  * --- Children pointers
  GSJB_MJB = .NULL.
  GSJB_MSA = .NULL.
  w_ZoomLog = .NULL.
  w_ZoomProc = .NULL.
  w_AZIPROCE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsjb_ash
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  
  w_TIPO=''
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=7, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SCHEDJOB','gsjb_ash')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsjb_ashPag1","gsjb_ash",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Schedulazione")
      .Pages(1).HelpContextID = 199678759
      .Pages(2).addobject("oPag","tgsjb_ashPag2","gsjb_ash",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Destinatari")
      .Pages(2).HelpContextID = 243807162
      .Pages(3).addobject("oPag","tgsjb_ashPag3","gsjb_ash",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Processi")
      .Pages(3).HelpContextID = 214959777
      .Pages(4).addobject("oPag","tgsjb_ashPag4","gsjb_ash",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Proattivit�")
      .Pages(4).HelpContextID = 86799356
      .Pages(5).addobject("oPag","tgsjb_ashPag5","gsjb_ash",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Aziende")
      .Pages(5).HelpContextID = 188637178
      .Pages(6).addobject("oPag","tgsjb_ashPag6","gsjb_ash",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Log")
      .Pages(6).HelpContextID = 109283914
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSHCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsjb_ash
    * Gestione tipo schedulazione (parametro passato da menu)
    * N normale / P proattivit�
    IF type("pTipSch")="C"
        IF pTipSch = "N" OR pTipSch = "P"
          this.Parent.w_TIPO = pTipSch
        ELSE
          this.Parent.w_TIPO = "N"
        ENDIF
    ELSE
        this.Parent.w_TIPO = "N"
    ENDIF
    
    * Titolo e zoom elenco
    IF this.Parent.w_TIPO = "P"
             this.Parent.cComment = ah_MsgFormat("Proattivit�")
             this.Parent.cAutoZoom = "PROATTIV"
    ELSE
             this.Parent.cComment = ah_MsgFormat("Schedulatore di job")
    ENDIF
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZoomLog = this.oPgFrm.Pages(6).oPag.ZoomLog
      this.w_ZoomProc = this.oPgFrm.Pages(6).oPag.ZoomProc
      this.w_AZIPROCE = this.oPgFrm.Pages(5).oPag.AZIPROCE
      DoDefault()
    proc Destroy()
      this.w_ZoomLog = .NULL.
      this.w_ZoomProc = .NULL.
      this.w_AZIPROCE = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='LOGELAJB'
    this.cWorkTables[2]='CPUSERS'
    this.cWorkTables[3]='CPGROUPS'
    this.cWorkTables[4]='SCHEDJOB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SCHEDJOB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SCHEDJOB_IDX,3]
  return

  function CreateChildren()
    this.GSJB_MJB = CREATEOBJECT('stdDynamicChild',this,'GSJB_MJB',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSJB_MJB.createrealchild()
    this.GSJB_MSA = CREATEOBJECT('stdDynamicChild',this,'GSJB_MSA',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSJB_MSA.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSJB_MJB)
      this.GSJB_MJB.DestroyChildrenChain()
      this.GSJB_MJB=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    if !ISNULL(this.GSJB_MSA)
      this.GSJB_MSA.DestroyChildrenChain()
      this.GSJB_MSA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSJB_MJB.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSJB_MSA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSJB_MJB.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSJB_MSA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSJB_MJB.NewDocument()
    this.GSJB_MSA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSJB_MJB.SetKey(;
            .w_SHCODICE,"JBCODICEJOB";
            )
      this.GSJB_MSA.SetKey(;
            .w_SHCODICE,"UGSHJOB";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSJB_MJB.ChangeRow(this.cRowID+'      1',1;
             ,.w_SHCODICE,"JBCODICEJOB";
             )
      .WriteTo_GSJB_MJB()
      .GSJB_MSA.ChangeRow(this.cRowID+'      1',1;
             ,.w_SHCODICE,"UGSHJOB";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSJB_MJB)
        i_f=.GSJB_MJB.BuildFilter()
        if !(i_f==.GSJB_MJB.cQueryFilter)
          i_fnidx=.GSJB_MJB.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSJB_MJB.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSJB_MJB.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSJB_MJB.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSJB_MJB.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSJB_MSA)
        i_f=.GSJB_MSA.BuildFilter()
        if !(i_f==.GSJB_MSA.cQueryFilter)
          i_fnidx=.GSJB_MSA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSJB_MSA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSJB_MSA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSJB_MSA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSJB_MSA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSJB_MJB()
  if at('gsjb_mjb',lower(this.GSJB_MJB.class))<>0
    if this.GSJB_MJB.w_SHTIPSCH<>this.w_SHTIPSCH
      this.GSJB_MJB.w_SHTIPSCH = this.w_SHTIPSCH
      this.GSJB_MJB.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SHCODICE = NVL(SHCODICE,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SCHEDJOB where SHCODICE=KeySet.SHCODICE
    *
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SCHEDJOB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SCHEDJOB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SCHEDJOB '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SHCODICE',this.w_SHCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_GRPSCHED = LOOKTAB('GROUPSJ', 'GJCODICE', '1', '1')
        .w_DESUTEC = space(20)
        .w_DESUTEV = space(20)
        .w_MEMO = space(0)
        .w_SELEZI = "S"
        .w_LPATH = .f.
        .w_SHDATMAN = 'N'
        .w_DESUTEP = space(20)
        .w_SHCODICE = NVL(SHCODICE,0)
        .op_SHCODICE = .w_SHCODICE
        .w_SHFLAT = NVL(SHFLAT,space(1))
        .w_SHUTEPRE = NVL(SHUTEPRE,0)
          .link_1_4('Load')
        .w_SHDESCRI = NVL(SHDESCRI,space(254))
        .w_SHUTECREA = NVL(SHUTECREA,0)
          .link_1_6('Load')
        .w_SHUTEVAR = NVL(SHUTEVAR,0)
          .link_1_8('Load')
        .w_SHDATACREA = NVL(cp_ToDate(SHDATACREA),ctod("  /  /  "))
        .w_SHDATAVAR = NVL(cp_ToDate(SHDATAVAR),ctod("  /  /  "))
        .w_SHRELOAD = NVL(SHRELOAD,space(1))
        .w_SHDATAPRO = NVL(cp_ToDate(SHDATAPRO),ctod("  /  /  "))
        .w_SHORAPROS = NVL(SHORAPROS,space(5))
        .w_SHORAINI = NVL(SHORAINI,space(5))
        .w_SHORAFIN = NVL(SHORAFIN,space(5))
        .w_SHTMPMAX = NVL(SHTMPMAX,space(5))
        .w_ora_attesa = iif(empty(LEFT(.w_SHTMPMAX,2)),'00',LEFT(.w_SHTMPMAX,2))
        .w_mm_attesa = iif(empty(RIGHT(.w_SHTMPMAX,2)),'00',RIGHT(.w_SHTMPMAX,2))
        .w_SHFREQ = NVL(SHFREQ,space(1))
        .w_SHDATASTART = NVL(cp_ToDate(SHDATASTART),ctod("  /  /  "))
        .w_SHORAINE = NVL(SHORAINE,space(5))
        .w_SHDATAFINE = NVL(cp_ToDate(SHDATAFINE),ctod("  /  /  "))
        .w_SHORAFNE = NVL(SHORAFNE,space(5))
        .w_SHNUMFREQ = NVL(SHNUMFREQ,0)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(IIF(!(.w_SHFREQ$'G-S'),'',iif(.w_shfreq='G', AH_MsgFormat("Giorno/i"), AH_MsgFormat("Settimana/e"))))
        .w_ora_lancio = iif(empty(.w_SHORAINI),'00',SUBSTR(.w_SHORAINI,1,2))
        .w_mm_lancio = iif(empty(.w_SHORAINI),'00',SUBSTR(.w_SHORAINI,4,2))
        .w_SHMINUINT = NVL(SHMINUINT,0)
        .w_ora_lancifi = iif(empty(.w_SHORAFIN),'23',SUBSTR(.w_SHORAFIN,1,2))
        .w_mm_lancifi = iif(empty(.w_SHORAFIN),'59',SUBSTR(.w_SHORAFIN,4,2))
        .w_SHPATH = NVL(SHPATH,space(200))
        .w_SHFLGSTC = NVL(SHFLGSTC,space(1))
        .w_SHFLINVS = NVL(SHFLINVS,space(1))
        .w_SHSET = NVL(SHSET,space(50))
        .w_SHMESE = NVL(SHMESE,space(75))
        .w_lun = IIF(AT('Monday',.w_SHSET)<>0,'Monday',' ')
        .w_Mar = IIF(AT('Tuesday',.w_SHSET)<>0,'Tuesday',' ')
        .w_Mer = IIF(AT('Wednesday',.w_SHSET)<>0,'Wednesday',' ')
        .w_Gio = IIF(AT('Thursday',.w_SHSET)<>0,'Thursday',' ')
        .w_Ven = IIF(AT('Friday',.w_SHSET)<>0,'Friday',' ')
        .w_Sab = IIF(AT('Saturday',.w_SHSET)<>0,'Saturday',' ')
        .w_Dom = IIF(AT('Sunday',.w_SHSET)<>0,'Sunday',' ')
        .w_Gen = IIF(AT('January',.w_SHMESE)<>0,'January',' ')
        .w_Feb = IIF(AT('February',.w_SHMESE)<>0,'February',' ')
        .w_Marz = IIF(AT('March',.w_SHMESE)<>0,'March',' ')
        .w_Apr = IIF(AT('April',.w_SHMESE)<>0,'April',' ')
        .w_Mag = IIF(AT('May',.w_SHMESE)<>0,'May',' ')
        .w_Giu = IIF(AT('June',.w_SHMESE)<>0,'June',' ')
        .w_Lug = IIF(AT('July',.w_SHMESE)<>0,'July',' ')
        .w_Ago = IIF(AT('August',.w_SHMESE)<>0,'August',' ')
        .w_Set = IIF(AT('September',.w_SHMESE)<>0,'September',' ')
        .w_Ott = IIF(AT('October',.w_SHMESE)<>0,'October',' ')
        .w_Nov = IIF(AT('November',.w_SHMESE)<>0,'November',' ')
        .w_Dic = IIF(AT('December',.w_SHMESE)<>0,'December',' ')
        .w_SHIMPDAT = NVL(SHIMPDAT,space(1))
        .w_SHDATFIS = NVL(SHDATFIS,0)
        .w_DataInizio = date()
        .w_SHTIPSCH = NVL(SHTIPSCH,space(1))
        .w_SHTIPELA = NVL(SHTIPELA,space(1))
        .w_SHELABOR = NVL(SHELABOR,space(250))
        .w_SHTIPRES = NVL(SHTIPRES,space(1))
        .w_SHFRMRES = NVL(SHFRMRES,space(1))
        .w_SHREPRES = NVL(SHREPRES,space(250))
        .w_SHTXTHEA = NVL(SHTXTHEA,space(0))
        .w_SHTXTBOD = NVL(SHTXTBOD,space(0))
        .w_SHTXTFOO = NVL(SHTXTFOO,space(0))
        .w_SHAGENDA = NVL(SHAGENDA,space(1))
        .w_SHDESTIN = NVL(SHDESTIN,space(250))
        .w_SHFINPRE = NVL(SHFINPRE,ctot(""))
        .oPgFrm.Page6.oPag.ZoomLog.Calculate()
        .w_LOG = Nvl(.w_ZoomLog.getvar('LSSERIAL'), Space(10))
          .link_6_8('Load')
        .oPgFrm.Page6.oPag.ZoomProc.Calculate(.w_LOG)
        .w_CPROWNUM = .w_ZoomProc.getvar('CPROWNUM')
          .link_6_11('Load')
        .oPgFrm.Page5.oPag.AZIPROCE.Calculate(.w_SHCODICE)
        cp_LoadRecExtFlds(this,'SCHEDJOB')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_1.enabled = this.oPgFrm.Page6.oPag.oBtn_6_1.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_6.enabled = this.oPgFrm.Page6.oPag.oBtn_6_6.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_7.enabled = this.oPgFrm.Page6.oPag.oBtn_6_7.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsjb_ash
    *--Aggiorno Zoom Log...
    this.NotifyEvent('Esegui')
    
    if this.w_SHTIPSCH <> this.w_TIPO
       this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SHCODICE = 0
      .w_SHFLAT = space(1)
      .w_GRPSCHED = 0
      .w_SHUTEPRE = 0
      .w_SHDESCRI = space(254)
      .w_SHUTECREA = 0
      .w_DESUTEC = space(20)
      .w_SHUTEVAR = 0
      .w_DESUTEV = space(20)
      .w_SHDATACREA = ctod("  /  /  ")
      .w_SHDATAVAR = ctod("  /  /  ")
      .w_SHRELOAD = space(1)
      .w_SHDATAPRO = ctod("  /  /  ")
      .w_SHORAPROS = space(5)
      .w_SHORAINI = space(5)
      .w_SHORAFIN = space(5)
      .w_SHTMPMAX = space(5)
      .w_ora_attesa = space(2)
      .w_mm_attesa = space(2)
      .w_SHFREQ = space(1)
      .w_SHDATASTART = ctod("  /  /  ")
      .w_SHORAINE = space(5)
      .w_SHDATAFINE = ctod("  /  /  ")
      .w_SHORAFNE = space(5)
      .w_SHNUMFREQ = 0
      .w_ora_lancio = space(2)
      .w_mm_lancio = space(2)
      .w_SHMINUINT = 0
      .w_ora_lancifi = space(2)
      .w_mm_lancifi = space(2)
      .w_SHPATH = space(200)
      .w_SHFLGSTC = space(1)
      .w_SHFLINVS = space(1)
      .w_SHSET = space(50)
      .w_SHMESE = space(75)
      .w_lun = space(10)
      .w_Mar = space(10)
      .w_Mer = space(10)
      .w_Gio = space(10)
      .w_Ven = space(10)
      .w_Sab = space(10)
      .w_Dom = space(10)
      .w_Gen = space(10)
      .w_Feb = space(10)
      .w_Marz = space(10)
      .w_Apr = space(10)
      .w_Mag = space(10)
      .w_Giu = space(10)
      .w_Lug = space(10)
      .w_Ago = space(10)
      .w_Set = space(10)
      .w_Ott = space(10)
      .w_Nov = space(10)
      .w_Dic = space(10)
      .w_SHIMPDAT = space(1)
      .w_SHDATFIS = 0
      .w_DataInizio = ctod("  /  /  ")
      .w_SHTIPSCH = space(1)
      .w_SHTIPELA = space(1)
      .w_SHELABOR = space(250)
      .w_SHTIPRES = space(1)
      .w_SHFRMRES = space(1)
      .w_SHREPRES = space(250)
      .w_SHTXTHEA = space(0)
      .w_SHTXTBOD = space(0)
      .w_SHTXTFOO = space(0)
      .w_SHAGENDA = space(1)
      .w_SHDESTIN = space(250)
      .w_SHFINPRE = ctot("")
      .w_MEMO = space(0)
      .w_LOG = space(10)
      .w_CPROWNUM = 0
      .w_SELEZI = space(1)
      .w_LPATH = .f.
      .w_SHDATMAN = space(1)
      .w_DESUTEP = space(20)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_SHFLAT = 'N'
        .w_GRPSCHED = LOOKTAB('GROUPSJ', 'GJCODICE', '1', '1')
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_SHUTEPRE))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,5,.f.)
        .w_SHUTECREA = I_CODUTE
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_SHUTECREA))
          .link_1_6('Full')
          endif
          .DoRTCalc(7,7,.f.)
        .w_SHUTEVAR = I_CODUTE
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_SHUTEVAR))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,9,.f.)
        .w_SHDATACREA = I_DATSYS
        .w_SHDATAVAR = i_DATSYS
        .w_SHRELOAD = 'N'
          .DoRTCalc(13,16,.f.)
        .w_SHTMPMAX = .w_ora_attesa+':'+.w_mm_attesa
        .w_ora_attesa = iif(empty(LEFT(.w_SHTMPMAX,2)),'00',LEFT(.w_SHTMPMAX,2))
        .w_mm_attesa = iif(empty(RIGHT(.w_SHTMPMAX,2)),'00',RIGHT(.w_SHTMPMAX,2))
        .w_SHFREQ = 'U'
          .DoRTCalc(21,24,.f.)
        .w_SHNUMFREQ = 1
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(IIF(!(.w_SHFREQ$'G-S'),'',iif(.w_shfreq='G', AH_MsgFormat("Giorno/i"), AH_MsgFormat("Settimana/e"))))
        .w_ora_lancio = iif(empty(.w_SHORAINI),'00',SUBSTR(.w_SHORAINI,1,2))
        .w_mm_lancio = iif(empty(.w_SHORAINI),'00',SUBSTR(.w_SHORAINI,4,2))
        .w_SHMINUINT = iif(.w_SHFREQ='U',0,.w_SHMINUINT)
        .w_ora_lancifi = iif(empty(.w_SHORAFIN),'23',SUBSTR(.w_SHORAFIN,1,2))
        .w_mm_lancifi = iif(empty(.w_SHORAFIN),'59',SUBSTR(.w_SHORAFIN,4,2))
          .DoRTCalc(31,31,.f.)
        .w_SHFLGSTC = 'N'
        .w_SHFLINVS = 'N'
        .w_SHSET = .w_lun+.w_mar+.w_mer+.w_gio+.w_ven+.w_sab+.w_dom
        .w_SHMESE = .w_Gen+.w_Feb+.w_Marz+.w_Apr+.w_Mag+.w_Giu+.w_Lug+.w_Ago+.w_Set+.w_Ott+.w_Nov+.w_Dic
        .w_lun = IIF(AT('Monday',.w_SHSET)<>0,'Monday',' ')
        .w_Mar = IIF(AT('Tuesday',.w_SHSET)<>0,'Tuesday',' ')
        .w_Mer = IIF(AT('Wednesday',.w_SHSET)<>0,'Wednesday',' ')
        .w_Gio = IIF(AT('Thursday',.w_SHSET)<>0,'Thursday',' ')
        .w_Ven = IIF(AT('Friday',.w_SHSET)<>0,'Friday',' ')
        .w_Sab = IIF(AT('Saturday',.w_SHSET)<>0,'Saturday',' ')
        .w_Dom = IIF(AT('Sunday',.w_SHSET)<>0,'Sunday',' ')
        .w_Gen = IIF(AT('January',.w_SHMESE)<>0,'January',' ')
        .w_Feb = IIF(AT('February',.w_SHMESE)<>0,'February',' ')
        .w_Marz = IIF(AT('March',.w_SHMESE)<>0,'March',' ')
        .w_Apr = IIF(AT('April',.w_SHMESE)<>0,'April',' ')
        .w_Mag = IIF(AT('May',.w_SHMESE)<>0,'May',' ')
        .w_Giu = IIF(AT('June',.w_SHMESE)<>0,'June',' ')
        .w_Lug = IIF(AT('July',.w_SHMESE)<>0,'July',' ')
        .w_Ago = IIF(AT('August',.w_SHMESE)<>0,'August',' ')
        .w_Set = IIF(AT('September',.w_SHMESE)<>0,'September',' ')
        .w_Ott = IIF(AT('October',.w_SHMESE)<>0,'October',' ')
        .w_Nov = IIF(AT('November',.w_SHMESE)<>0,'November',' ')
        .w_Dic = IIF(AT('December',.w_SHMESE)<>0,'December',' ')
        .w_SHIMPDAT = 'F'
          .DoRTCalc(56,56,.f.)
        .w_DataInizio = date()
        .w_SHTIPSCH = .w_TIPO
        .w_SHTIPELA = "Q"
        .w_SHELABOR = SPACE( 250 )
        .w_SHTIPRES = "X"
        .w_SHFRMRES = "T"
          .DoRTCalc(63,66,.f.)
        .w_SHAGENDA = "N"
        .w_SHDESTIN = IIF( .w_SHAGENDA <> "N" , .w_SHDESTIN , SPACE( 250 ) )
        .oPgFrm.Page6.oPag.ZoomLog.Calculate()
          .DoRTCalc(69,70,.f.)
        .w_LOG = Nvl(.w_ZoomLog.getvar('LSSERIAL'), Space(10))
        .DoRTCalc(71,71,.f.)
          if not(empty(.w_LOG))
          .link_6_8('Full')
          endif
        .oPgFrm.Page6.oPag.ZoomProc.Calculate(.w_LOG)
        .w_CPROWNUM = .w_ZoomProc.getvar('CPROWNUM')
        .DoRTCalc(72,72,.f.)
          if not(empty(.w_CPROWNUM))
          .link_6_11('Full')
          endif
        .oPgFrm.Page5.oPag.AZIPROCE.Calculate(.w_SHCODICE)
        .w_SELEZI = "S"
          .DoRTCalc(74,74,.f.)
        .w_SHDATMAN = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'SCHEDJOB')
    this.DoRTCalc(76,76,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_1.enabled = this.oPgFrm.Page6.oPag.oBtn_6_1.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_6.enabled = this.oPgFrm.Page6.oPag.oBtn_6_6.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_7.enabled = this.oPgFrm.Page6.oPag.oBtn_6_7.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SCHEDJOB","w_SHCODICE")
      .op_SHCODICE = .w_SHCODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSHCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oSHFLAT_1_2.enabled = i_bVal
      .Page1.oPag.oSHUTEPRE_1_4.enabled = i_bVal
      .Page1.oPag.oSHDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oSHRELOAD_1_12.enabled = i_bVal
      .Page1.oPag.oSHDATAPRO_1_13.enabled = i_bVal
      .Page1.oPag.oSHORAPROS_1_14.enabled = i_bVal
      .Page1.oPag.oora_attesa_1_18.enabled = i_bVal
      .Page1.oPag.omm_attesa_1_19.enabled = i_bVal
      .Page1.oPag.oSHFREQ_1_20.enabled_(i_bVal)
      .Page1.oPag.oSHNUMFREQ_1_25.enabled = i_bVal
      .Page1.oPag.oora_lancio_1_27.enabled = i_bVal
      .Page1.oPag.omm_lancio_1_28.enabled = i_bVal
      .Page1.oPag.oSHMINUINT_1_29.enabled = i_bVal
      .Page1.oPag.oora_lancifi_1_30.enabled = i_bVal
      .Page1.oPag.omm_lancifi_1_31.enabled = i_bVal
      .Page1.oPag.oSHPATH_1_42.enabled = i_bVal
      .Page1.oPag.oSHFLGSTC_1_44.enabled = i_bVal
      .Page1.oPag.oSHFLINVS_1_45.enabled = i_bVal
      .Page1.oPag.olun_1_53.enabled = i_bVal
      .Page1.oPag.oMar_1_54.enabled = i_bVal
      .Page1.oPag.oMer_1_55.enabled = i_bVal
      .Page1.oPag.oGio_1_56.enabled = i_bVal
      .Page1.oPag.oVen_1_57.enabled = i_bVal
      .Page1.oPag.oSab_1_58.enabled = i_bVal
      .Page1.oPag.oDom_1_59.enabled = i_bVal
      .Page1.oPag.oGen_1_60.enabled = i_bVal
      .Page1.oPag.oFeb_1_61.enabled = i_bVal
      .Page1.oPag.oMarz_1_62.enabled = i_bVal
      .Page1.oPag.oApr_1_63.enabled = i_bVal
      .Page1.oPag.oMag_1_64.enabled = i_bVal
      .Page1.oPag.oGiu_1_65.enabled = i_bVal
      .Page1.oPag.oLug_1_66.enabled = i_bVal
      .Page1.oPag.oAgo_1_67.enabled = i_bVal
      .Page1.oPag.oSet_1_68.enabled = i_bVal
      .Page1.oPag.oOtt_1_69.enabled = i_bVal
      .Page1.oPag.oNov_1_70.enabled = i_bVal
      .Page1.oPag.oDic_1_71.enabled = i_bVal
      .Page1.oPag.oSHIMPDAT_1_73.enabled_(i_bVal)
      .Page1.oPag.oSHDATFIS_1_75.enabled = i_bVal
      .Page4.oPag.oSHTIPELA_4_1.enabled = i_bVal
      .Page4.oPag.oSHELABOR_4_4.enabled = i_bVal
      .Page4.oPag.oSHTIPRES_4_6.enabled = i_bVal
      .Page4.oPag.oSHFRMRES_4_7.enabled = i_bVal
      .Page4.oPag.oSHREPRES_4_9.enabled = i_bVal
      .Page4.oPag.oSHTXTHEA_4_13.enabled = i_bVal
      .Page4.oPag.oSHTXTBOD_4_14.enabled = i_bVal
      .Page4.oPag.oSHTXTFOO_4_15.enabled = i_bVal
      .Page4.oPag.oSHAGENDA_4_19.enabled = i_bVal
      .Page4.oPag.oSHDESTIN_4_20.enabled = i_bVal
      .Page4.oPag.oSHFINPRE_4_24.enabled = i_bVal
      .Page6.oPag.oMEMO_6_3.enabled = i_bVal
      .Page5.oPag.oSELEZI_5_3.enabled_(i_bVal)
      .Page1.oPag.oSHDATMAN_1_115.enabled = i_bVal
      .Page1.oPag.oBtn_1_43.enabled = i_bVal
      .Page1.oPag.oBtn_1_87.enabled = .Page1.oPag.oBtn_1_87.mCond()
      .Page4.oPag.oBtn_4_5.enabled = i_bVal
      .Page4.oPag.oBtn_4_10.enabled = i_bVal
      .Page6.oPag.oBtn_6_1.enabled = .Page6.oPag.oBtn_6_1.mCond()
      .Page6.oPag.oBtn_6_6.enabled = .Page6.oPag.oBtn_6_6.mCond()
      .Page6.oPag.oBtn_6_7.enabled = .Page6.oPag.oBtn_6_7.mCond()
      .Page1.oPag.oObj_1_26.enabled = i_bVal
      .Page5.oPag.AZIPROCE.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSHCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSHCODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSJB_MJB.SetStatus(i_cOp)
    this.GSJB_MSA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'SCHEDJOB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsjb_ash
    *Abilito il controllo w_MEMO altrimenti non ho la barra di scorrimento
    local L_obj
    L_obj = this.GetCtrl("w_MEMO")
    L_obj.Enabled = .t.
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSJB_MJB.SetChildrenStatus(i_cOp)
  *  this.GSJB_MSA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHCODICE,"SHCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHFLAT,"SHFLAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHUTEPRE,"SHUTEPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHDESCRI,"SHDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHUTECREA,"SHUTECREA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHUTEVAR,"SHUTEVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHDATACREA,"SHDATACREA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHDATAVAR,"SHDATAVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHRELOAD,"SHRELOAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHDATAPRO,"SHDATAPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHORAPROS,"SHORAPROS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHORAINI,"SHORAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHORAFIN,"SHORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHTMPMAX,"SHTMPMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHFREQ,"SHFREQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHDATASTART,"SHDATASTART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHORAINE,"SHORAINE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHDATAFINE,"SHDATAFINE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHORAFNE,"SHORAFNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHNUMFREQ,"SHNUMFREQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHMINUINT,"SHMINUINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHPATH,"SHPATH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHFLGSTC,"SHFLGSTC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHFLINVS,"SHFLINVS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHSET,"SHSET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHMESE,"SHMESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHIMPDAT,"SHIMPDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHDATFIS,"SHDATFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHTIPSCH,"SHTIPSCH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHTIPELA,"SHTIPELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHELABOR,"SHELABOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHTIPRES,"SHTIPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHFRMRES,"SHFRMRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHREPRES,"SHREPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHTXTHEA,"SHTXTHEA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHTXTBOD,"SHTXTBOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHTXTFOO,"SHTXTFOO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHAGENDA,"SHAGENDA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHDESTIN,"SHDESTIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SHFINPRE,"SHFINPRE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    i_lTable = "SCHEDJOB"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SCHEDJOB_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do gsjb_bs1 with this
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SCHEDJOB_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SCHEDJOB","w_SHCODICE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SCHEDJOB
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SCHEDJOB')
        i_extval=cp_InsertValODBCExtFlds(this,'SCHEDJOB')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SHCODICE,SHFLAT,SHUTEPRE,SHDESCRI,SHUTECREA"+;
                  ",SHUTEVAR,SHDATACREA,SHDATAVAR,SHRELOAD,SHDATAPRO"+;
                  ",SHORAPROS,SHORAINI,SHORAFIN,SHTMPMAX,SHFREQ"+;
                  ",SHDATASTART,SHORAINE,SHDATAFINE,SHORAFNE,SHNUMFREQ"+;
                  ",SHMINUINT,SHPATH,SHFLGSTC,SHFLINVS,SHSET"+;
                  ",SHMESE,SHIMPDAT,SHDATFIS,SHTIPSCH,SHTIPELA"+;
                  ",SHELABOR,SHTIPRES,SHFRMRES,SHREPRES,SHTXTHEA"+;
                  ",SHTXTBOD,SHTXTFOO,SHAGENDA,SHDESTIN,SHFINPRE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SHCODICE)+;
                  ","+cp_ToStrODBC(this.w_SHFLAT)+;
                  ","+cp_ToStrODBCNull(this.w_SHUTEPRE)+;
                  ","+cp_ToStrODBC(this.w_SHDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_SHUTECREA)+;
                  ","+cp_ToStrODBCNull(this.w_SHUTEVAR)+;
                  ","+cp_ToStrODBC(this.w_SHDATACREA)+;
                  ","+cp_ToStrODBC(this.w_SHDATAVAR)+;
                  ","+cp_ToStrODBC(this.w_SHRELOAD)+;
                  ","+cp_ToStrODBC(this.w_SHDATAPRO)+;
                  ","+cp_ToStrODBC(this.w_SHORAPROS)+;
                  ","+cp_ToStrODBC(this.w_SHORAINI)+;
                  ","+cp_ToStrODBC(this.w_SHORAFIN)+;
                  ","+cp_ToStrODBC(this.w_SHTMPMAX)+;
                  ","+cp_ToStrODBC(this.w_SHFREQ)+;
                  ","+cp_ToStrODBC(this.w_SHDATASTART)+;
                  ","+cp_ToStrODBC(this.w_SHORAINE)+;
                  ","+cp_ToStrODBC(this.w_SHDATAFINE)+;
                  ","+cp_ToStrODBC(this.w_SHORAFNE)+;
                  ","+cp_ToStrODBC(this.w_SHNUMFREQ)+;
                  ","+cp_ToStrODBC(this.w_SHMINUINT)+;
                  ","+cp_ToStrODBC(this.w_SHPATH)+;
                  ","+cp_ToStrODBC(this.w_SHFLGSTC)+;
                  ","+cp_ToStrODBC(this.w_SHFLINVS)+;
                  ","+cp_ToStrODBC(this.w_SHSET)+;
                  ","+cp_ToStrODBC(this.w_SHMESE)+;
                  ","+cp_ToStrODBC(this.w_SHIMPDAT)+;
                  ","+cp_ToStrODBC(this.w_SHDATFIS)+;
                  ","+cp_ToStrODBC(this.w_SHTIPSCH)+;
                  ","+cp_ToStrODBC(this.w_SHTIPELA)+;
                  ","+cp_ToStrODBC(this.w_SHELABOR)+;
                  ","+cp_ToStrODBC(this.w_SHTIPRES)+;
                  ","+cp_ToStrODBC(this.w_SHFRMRES)+;
                  ","+cp_ToStrODBC(this.w_SHREPRES)+;
                  ","+cp_ToStrODBC(this.w_SHTXTHEA)+;
                  ","+cp_ToStrODBC(this.w_SHTXTBOD)+;
                  ","+cp_ToStrODBC(this.w_SHTXTFOO)+;
                  ","+cp_ToStrODBC(this.w_SHAGENDA)+;
                  ","+cp_ToStrODBC(this.w_SHDESTIN)+;
                  ","+cp_ToStrODBC(this.w_SHFINPRE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SCHEDJOB')
        i_extval=cp_InsertValVFPExtFlds(this,'SCHEDJOB')
        cp_CheckDeletedKey(i_cTable,0,'SHCODICE',this.w_SHCODICE)
        INSERT INTO (i_cTable);
              (SHCODICE,SHFLAT,SHUTEPRE,SHDESCRI,SHUTECREA,SHUTEVAR,SHDATACREA,SHDATAVAR,SHRELOAD,SHDATAPRO,SHORAPROS,SHORAINI,SHORAFIN,SHTMPMAX,SHFREQ,SHDATASTART,SHORAINE,SHDATAFINE,SHORAFNE,SHNUMFREQ,SHMINUINT,SHPATH,SHFLGSTC,SHFLINVS,SHSET,SHMESE,SHIMPDAT,SHDATFIS,SHTIPSCH,SHTIPELA,SHELABOR,SHTIPRES,SHFRMRES,SHREPRES,SHTXTHEA,SHTXTBOD,SHTXTFOO,SHAGENDA,SHDESTIN,SHFINPRE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SHCODICE;
                  ,this.w_SHFLAT;
                  ,this.w_SHUTEPRE;
                  ,this.w_SHDESCRI;
                  ,this.w_SHUTECREA;
                  ,this.w_SHUTEVAR;
                  ,this.w_SHDATACREA;
                  ,this.w_SHDATAVAR;
                  ,this.w_SHRELOAD;
                  ,this.w_SHDATAPRO;
                  ,this.w_SHORAPROS;
                  ,this.w_SHORAINI;
                  ,this.w_SHORAFIN;
                  ,this.w_SHTMPMAX;
                  ,this.w_SHFREQ;
                  ,this.w_SHDATASTART;
                  ,this.w_SHORAINE;
                  ,this.w_SHDATAFINE;
                  ,this.w_SHORAFNE;
                  ,this.w_SHNUMFREQ;
                  ,this.w_SHMINUINT;
                  ,this.w_SHPATH;
                  ,this.w_SHFLGSTC;
                  ,this.w_SHFLINVS;
                  ,this.w_SHSET;
                  ,this.w_SHMESE;
                  ,this.w_SHIMPDAT;
                  ,this.w_SHDATFIS;
                  ,this.w_SHTIPSCH;
                  ,this.w_SHTIPELA;
                  ,this.w_SHELABOR;
                  ,this.w_SHTIPRES;
                  ,this.w_SHFRMRES;
                  ,this.w_SHREPRES;
                  ,this.w_SHTXTHEA;
                  ,this.w_SHTXTBOD;
                  ,this.w_SHTXTFOO;
                  ,this.w_SHAGENDA;
                  ,this.w_SHDESTIN;
                  ,this.w_SHFINPRE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SCHEDJOB_IDX,i_nConn)
      *
      * update SCHEDJOB
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SCHEDJOB')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SHFLAT="+cp_ToStrODBC(this.w_SHFLAT)+;
             ",SHUTEPRE="+cp_ToStrODBCNull(this.w_SHUTEPRE)+;
             ",SHDESCRI="+cp_ToStrODBC(this.w_SHDESCRI)+;
             ",SHUTECREA="+cp_ToStrODBCNull(this.w_SHUTECREA)+;
             ",SHUTEVAR="+cp_ToStrODBCNull(this.w_SHUTEVAR)+;
             ",SHDATACREA="+cp_ToStrODBC(this.w_SHDATACREA)+;
             ",SHDATAVAR="+cp_ToStrODBC(this.w_SHDATAVAR)+;
             ",SHRELOAD="+cp_ToStrODBC(this.w_SHRELOAD)+;
             ",SHDATAPRO="+cp_ToStrODBC(this.w_SHDATAPRO)+;
             ",SHORAPROS="+cp_ToStrODBC(this.w_SHORAPROS)+;
             ",SHORAINI="+cp_ToStrODBC(this.w_SHORAINI)+;
             ",SHORAFIN="+cp_ToStrODBC(this.w_SHORAFIN)+;
             ",SHTMPMAX="+cp_ToStrODBC(this.w_SHTMPMAX)+;
             ",SHFREQ="+cp_ToStrODBC(this.w_SHFREQ)+;
             ",SHDATASTART="+cp_ToStrODBC(this.w_SHDATASTART)+;
             ",SHORAINE="+cp_ToStrODBC(this.w_SHORAINE)+;
             ",SHDATAFINE="+cp_ToStrODBC(this.w_SHDATAFINE)+;
             ",SHORAFNE="+cp_ToStrODBC(this.w_SHORAFNE)+;
             ",SHNUMFREQ="+cp_ToStrODBC(this.w_SHNUMFREQ)+;
             ",SHMINUINT="+cp_ToStrODBC(this.w_SHMINUINT)+;
             ",SHPATH="+cp_ToStrODBC(this.w_SHPATH)+;
             ",SHFLGSTC="+cp_ToStrODBC(this.w_SHFLGSTC)+;
             ",SHFLINVS="+cp_ToStrODBC(this.w_SHFLINVS)+;
             ",SHSET="+cp_ToStrODBC(this.w_SHSET)+;
             ",SHMESE="+cp_ToStrODBC(this.w_SHMESE)+;
             ",SHIMPDAT="+cp_ToStrODBC(this.w_SHIMPDAT)+;
             ",SHDATFIS="+cp_ToStrODBC(this.w_SHDATFIS)+;
             ",SHTIPSCH="+cp_ToStrODBC(this.w_SHTIPSCH)+;
             ",SHTIPELA="+cp_ToStrODBC(this.w_SHTIPELA)+;
             ",SHELABOR="+cp_ToStrODBC(this.w_SHELABOR)+;
             ",SHTIPRES="+cp_ToStrODBC(this.w_SHTIPRES)+;
             ",SHFRMRES="+cp_ToStrODBC(this.w_SHFRMRES)+;
             ",SHREPRES="+cp_ToStrODBC(this.w_SHREPRES)+;
             ",SHTXTHEA="+cp_ToStrODBC(this.w_SHTXTHEA)+;
             ",SHTXTBOD="+cp_ToStrODBC(this.w_SHTXTBOD)+;
             ",SHTXTFOO="+cp_ToStrODBC(this.w_SHTXTFOO)+;
             ",SHAGENDA="+cp_ToStrODBC(this.w_SHAGENDA)+;
             ",SHDESTIN="+cp_ToStrODBC(this.w_SHDESTIN)+;
             ",SHFINPRE="+cp_ToStrODBC(this.w_SHFINPRE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SCHEDJOB')
        i_cWhere = cp_PKFox(i_cTable  ,'SHCODICE',this.w_SHCODICE  )
        UPDATE (i_cTable) SET;
              SHFLAT=this.w_SHFLAT;
             ,SHUTEPRE=this.w_SHUTEPRE;
             ,SHDESCRI=this.w_SHDESCRI;
             ,SHUTECREA=this.w_SHUTECREA;
             ,SHUTEVAR=this.w_SHUTEVAR;
             ,SHDATACREA=this.w_SHDATACREA;
             ,SHDATAVAR=this.w_SHDATAVAR;
             ,SHRELOAD=this.w_SHRELOAD;
             ,SHDATAPRO=this.w_SHDATAPRO;
             ,SHORAPROS=this.w_SHORAPROS;
             ,SHORAINI=this.w_SHORAINI;
             ,SHORAFIN=this.w_SHORAFIN;
             ,SHTMPMAX=this.w_SHTMPMAX;
             ,SHFREQ=this.w_SHFREQ;
             ,SHDATASTART=this.w_SHDATASTART;
             ,SHORAINE=this.w_SHORAINE;
             ,SHDATAFINE=this.w_SHDATAFINE;
             ,SHORAFNE=this.w_SHORAFNE;
             ,SHNUMFREQ=this.w_SHNUMFREQ;
             ,SHMINUINT=this.w_SHMINUINT;
             ,SHPATH=this.w_SHPATH;
             ,SHFLGSTC=this.w_SHFLGSTC;
             ,SHFLINVS=this.w_SHFLINVS;
             ,SHSET=this.w_SHSET;
             ,SHMESE=this.w_SHMESE;
             ,SHIMPDAT=this.w_SHIMPDAT;
             ,SHDATFIS=this.w_SHDATFIS;
             ,SHTIPSCH=this.w_SHTIPSCH;
             ,SHTIPELA=this.w_SHTIPELA;
             ,SHELABOR=this.w_SHELABOR;
             ,SHTIPRES=this.w_SHTIPRES;
             ,SHFRMRES=this.w_SHFRMRES;
             ,SHREPRES=this.w_SHREPRES;
             ,SHTXTHEA=this.w_SHTXTHEA;
             ,SHTXTBOD=this.w_SHTXTBOD;
             ,SHTXTFOO=this.w_SHTXTFOO;
             ,SHAGENDA=this.w_SHAGENDA;
             ,SHDESTIN=this.w_SHDESTIN;
             ,SHFINPRE=this.w_SHFINPRE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSJB_MJB : Saving
      this.GSJB_MJB.ChangeRow(this.cRowID+'      1',0;
             ,this.w_SHCODICE,"JBCODICEJOB";
             )
      this.GSJB_MJB.mReplace()
      * --- GSJB_MSA : Saving
      this.GSJB_MSA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_SHCODICE,"UGSHJOB";
             )
      this.GSJB_MSA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSJB_MJB : Deleting
    this.GSJB_MJB.ChangeRow(this.cRowID+'      1',0;
           ,this.w_SHCODICE,"JBCODICEJOB";
           )
    this.GSJB_MJB.mDelete()
    * --- GSJB_MSA : Deleting
    this.GSJB_MSA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_SHCODICE,"UGSHJOB";
           )
    this.GSJB_MSA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SCHEDJOB_IDX,i_nConn)
      *
      * delete SCHEDJOB
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SHCODICE',this.w_SHCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
          .link_1_6('Full')
        .DoRTCalc(7,7,.t.)
            .w_SHUTEVAR = I_CODUTE
          .link_1_8('Full')
        .DoRTCalc(9,10,.t.)
            .w_SHDATAVAR = i_DATSYS
        .DoRTCalc(12,16,.t.)
        if .o_ora_attesa<>.w_ora_attesa.or. .o_mm_attesa<>.w_mm_attesa
            .w_SHTMPMAX = .w_ora_attesa+':'+.w_mm_attesa
        endif
        if .o_SHTMPMAX<>.w_SHTMPMAX
            .w_ora_attesa = iif(empty(LEFT(.w_SHTMPMAX,2)),'00',LEFT(.w_SHTMPMAX,2))
        endif
        if .o_SHTMPMAX<>.w_SHTMPMAX
            .w_mm_attesa = iif(empty(RIGHT(.w_SHTMPMAX,2)),'00',RIGHT(.w_SHTMPMAX,2))
        endif
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(IIF(!(.w_SHFREQ$'G-S'),'',iif(.w_shfreq='G', AH_MsgFormat("Giorno/i"), AH_MsgFormat("Settimana/e"))))
        .DoRTCalc(20,25,.t.)
        if .o_SHORAINI<>.w_SHORAINI
            .w_ora_lancio = iif(empty(.w_SHORAINI),'00',SUBSTR(.w_SHORAINI,1,2))
        endif
        if .o_SHORAINI<>.w_SHORAINI
            .w_mm_lancio = iif(empty(.w_SHORAINI),'00',SUBSTR(.w_SHORAINI,4,2))
        endif
        if .o_SHFREQ<>.w_SHFREQ
            .w_SHMINUINT = iif(.w_SHFREQ='U',0,.w_SHMINUINT)
        endif
        if .o_SHORAFIN<>.w_SHORAFIN
            .w_ora_lancifi = iif(empty(.w_SHORAFIN),'23',SUBSTR(.w_SHORAFIN,1,2))
        endif
        if .o_SHORAFIN<>.w_SHORAFIN
            .w_mm_lancifi = iif(empty(.w_SHORAFIN),'59',SUBSTR(.w_SHORAFIN,4,2))
        endif
        .DoRTCalc(31,33,.t.)
            .w_SHSET = .w_lun+.w_mar+.w_mer+.w_gio+.w_ven+.w_sab+.w_dom
            .w_SHMESE = .w_Gen+.w_Feb+.w_Marz+.w_Apr+.w_Mag+.w_Giu+.w_Lug+.w_Ago+.w_Set+.w_Ott+.w_Nov+.w_Dic
        if .o_SHSET<>.w_SHSET
            .w_lun = IIF(AT('Monday',.w_SHSET)<>0,'Monday',' ')
        endif
        if .o_SHSET<>.w_SHSET
            .w_Mar = IIF(AT('Tuesday',.w_SHSET)<>0,'Tuesday',' ')
        endif
        if .o_SHSET<>.w_SHSET
            .w_Mer = IIF(AT('Wednesday',.w_SHSET)<>0,'Wednesday',' ')
        endif
        if .o_SHSET<>.w_SHSET
            .w_Gio = IIF(AT('Thursday',.w_SHSET)<>0,'Thursday',' ')
        endif
        if .o_SHSET<>.w_SHSET
            .w_Ven = IIF(AT('Friday',.w_SHSET)<>0,'Friday',' ')
        endif
        if .o_SHSET<>.w_SHSET
            .w_Sab = IIF(AT('Saturday',.w_SHSET)<>0,'Saturday',' ')
        endif
        if .o_SHSET<>.w_SHSET
            .w_Dom = IIF(AT('Sunday',.w_SHSET)<>0,'Sunday',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Gen = IIF(AT('January',.w_SHMESE)<>0,'January',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Feb = IIF(AT('February',.w_SHMESE)<>0,'February',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Marz = IIF(AT('March',.w_SHMESE)<>0,'March',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Apr = IIF(AT('April',.w_SHMESE)<>0,'April',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Mag = IIF(AT('May',.w_SHMESE)<>0,'May',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Giu = IIF(AT('June',.w_SHMESE)<>0,'June',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Lug = IIF(AT('July',.w_SHMESE)<>0,'July',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Ago = IIF(AT('August',.w_SHMESE)<>0,'August',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Set = IIF(AT('September',.w_SHMESE)<>0,'September',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Ott = IIF(AT('October',.w_SHMESE)<>0,'October',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Nov = IIF(AT('November',.w_SHMESE)<>0,'November',' ')
        endif
        if .o_SHMESE<>.w_SHMESE
            .w_Dic = IIF(AT('December',.w_SHMESE)<>0,'December',' ')
        endif
        .DoRTCalc(55,56,.t.)
            .w_DataInizio = date()
        if  .o_SHTIPSCH<>.w_SHTIPSCH
          .WriteTo_GSJB_MJB()
        endif
        if .o_ora_lancio<>.w_ora_lancio
          .Calculate_SLCYSAMDOK()
        endif
        if .o_mm_lancio<>.w_mm_lancio
          .Calculate_XLTWHZKGZW()
        endif
        if .o_ora_lancifi<>.w_ora_lancifi
          .Calculate_TTQFMXGTWH()
        endif
        if .o_mm_lancifi<>.w_mm_lancifi
          .Calculate_EMHXGAZDWG()
        endif
        if .o_ora_attesa<>.w_ora_attesa
          .Calculate_VLOGACDMGA()
        endif
        if .o_mm_attesa<>.w_mm_attesa
          .Calculate_MUNXYLNFEC()
        endif
        .DoRTCalc(58,59,.t.)
        if .o_SHTIPELA<>.w_SHTIPELA
            .w_SHELABOR = SPACE( 250 )
        endif
        .DoRTCalc(61,67,.t.)
        if .o_SHAGENDA<>.w_SHAGENDA
            .w_SHDESTIN = IIF( .w_SHAGENDA <> "N" , .w_SHDESTIN , SPACE( 250 ) )
        endif
        .oPgFrm.Page6.oPag.ZoomLog.Calculate()
        .DoRTCalc(69,70,.t.)
            .w_LOG = Nvl(.w_ZoomLog.getvar('LSSERIAL'), Space(10))
          .link_6_8('Full')
        .oPgFrm.Page6.oPag.ZoomProc.Calculate(.w_LOG)
            .w_CPROWNUM = .w_ZoomProc.getvar('CPROWNUM')
          .link_6_11('Full')
        .oPgFrm.Page5.oPag.AZIPROCE.Calculate(.w_SHCODICE)
        if .o_SHFLINVS<>.w_SHFLINVS
          .Calculate_TENPJMAVSW()
        endif
        if .o_SHPATH<>.w_SHPATH
          .Calculate_LMZUISUYVI()
        endif
        if .o_SHDATMAN<>.w_SHDATMAN
          .Calculate_MEOXFHTNAI()
        endif
        if .o_SHFREQ<>.w_SHFREQ
          .Calculate_MNLRXGJPIH()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(73,76,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(IIF(!(.w_SHFREQ$'G-S'),'',iif(.w_shfreq='G', AH_MsgFormat("Giorno/i"), AH_MsgFormat("Settimana/e"))))
        .oPgFrm.Page6.oPag.ZoomLog.Calculate()
        .oPgFrm.Page6.oPag.ZoomProc.Calculate(.w_LOG)
        .oPgFrm.Page5.oPag.AZIPROCE.Calculate(.w_SHCODICE)
    endwith
  return

  proc Calculate_SLCYSAMDOK()
    with this
          * --- Resetto w_ora_lancio
          .w_ora_lancio = .ZeroFill(.w_ora_lancio)
    endwith
  endproc
  proc Calculate_XLTWHZKGZW()
    with this
          * --- Resetto w_mm_lancio
          .w_mm_lancio = .ZeroFill(.w_mm_lancio)
    endwith
  endproc
  proc Calculate_TTQFMXGTWH()
    with this
          * --- Resetto w_ora_lancifi
          .w_ora_lancifi = .ZeroFill(.w_ora_lancifi)
    endwith
  endproc
  proc Calculate_EMHXGAZDWG()
    with this
          * --- Resetto w_mm_lancio
          .w_mm_lancifi = .ZeroFill(.w_mm_lancifi)
    endwith
  endproc
  proc Calculate_VLOGACDMGA()
    with this
          * --- Resetto w_ora_attesa
          .w_ora_attesa = .ZeroFill(.w_ora_attesa)
    endwith
  endproc
  proc Calculate_MUNXYLNFEC()
    with this
          * --- Resetto w_mm_attesa
          .w_mm_attesa = .ZeroFill(.w_mm_attesa)
    endwith
  endproc
  proc Calculate_YOALXQOMFK()
    with this
          * --- Ricalcolo data/ora prossima esecuzione
      if .w_SHDATMAN='N'
          gsjb_bde(this;
              ,i_DATSYS;
             )
      endif
    endwith
  endproc
  proc Calculate_COOOAZPTJA()
    with this
          * --- Cancello log
          GSJB_BDL(this;
             )
    endwith
  endproc
  proc Calculate_UONUPKHBAT()
    with this
          * --- Inserisce proattivit� nei processi da eseguire
          GSJB_BIP(this;
             )
    endwith
  endproc
  proc Calculate_NMSRIPBNBG()
    with this
          * --- Attiva proattivit� nelle aziende selezionate
          GSJB_BA3(this;
             )
    endwith
  endproc
  proc Calculate_TENPJMAVSW()
    with this
          * --- GSJB_MSA.w_SHFLINVS
          .GSJB_MSA.w_SHFLINVS = .w_SHFLINVS
    endwith
  endproc
  proc Calculate_LMZUISUYVI()
    with this
          * --- Controllo path
          .w_SHPATH = IIF(right(alltrim(.w_SHPATH),1)='\' or empty(.w_SHPATH),.w_SHPATH,alltrim(.w_SHPATH)+iif(len(alltrim(.w_SHPATH))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_SHPATH,'F')
    endwith
  endproc
  proc Calculate_MEOXFHTNAI()
    with this
          * --- Sbianca prossima esecuzione
          .w_SHDATAPRO = IIF(.w_SHDATMAN='N',{},{})
          .w_SHORAPROS = IIF(.w_SHDATMAN='N',SPACE(5),SPACE(5))
          .w_ora_attesa = IIF(.w_SHDATMAN='N',SPACE(2),SPACE(2))
          .w_mm_attesa = IIF(.w_SHDATMAN='N',SPACE(2),SPACE(2))
    endwith
  endproc
  proc Calculate_MNLRXGJPIH()
    with this
          * --- Sbianca inserimento data manuale
          .w_SHDATMAN = IIF(.w_SHFREQ='U','N','N')
          .w_SHDATAPRO = IIF(.w_SHDATMAN='N',{},{})
          .w_SHORAPROS = IIF(.w_SHDATMAN='N',SPACE(5),SPACE(5))
          .w_ora_attesa = IIF(.w_SHDATMAN='N',SPACE(2),SPACE(2))
          .w_mm_attesa = IIF(.w_SHDATMAN='N',SPACE(2),SPACE(2))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSHDATAPRO_1_13.enabled = this.oPgFrm.Page1.oPag.oSHDATAPRO_1_13.mCond()
    this.oPgFrm.Page1.oPag.oSHORAPROS_1_14.enabled = this.oPgFrm.Page1.oPag.oSHORAPROS_1_14.mCond()
    this.oPgFrm.Page1.oPag.oora_lancifi_1_30.enabled = this.oPgFrm.Page1.oPag.oora_lancifi_1_30.mCond()
    this.oPgFrm.Page1.oPag.omm_lancifi_1_31.enabled = this.oPgFrm.Page1.oPag.omm_lancifi_1_31.mCond()
    this.oPgFrm.Page4.oPag.oSHREPRES_4_9.enabled = this.oPgFrm.Page4.oPag.oSHREPRES_4_9.mCond()
    this.oPgFrm.Page4.oPag.oSHDESTIN_4_20.enabled = this.oPgFrm.Page4.oPag.oSHDESTIN_4_20.mCond()
    this.oPgFrm.Page1.oPag.oSHDATMAN_1_115.enabled = this.oPgFrm.Page1.oPag.oSHDATMAN_1_115.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oSHDATAPRO_1_13
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page1.oPag.oSHORAPROS_1_14
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(3).enabled=not(this.w_SHTIPSCH = "P")
    this.oPgFrm.Pages(4).enabled=not(this.w_SHTIPSCH = "N")
    this.oPgFrm.Pages(5).enabled=not(this.w_SHTIPSCH = "N")
    local i_show2
    i_show2=not(this.w_SHTIPSCH = "P")
    this.oPgFrm.Pages(3).enabled=i_show2 and not(this.w_SHTIPSCH = "P")
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Processi"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    local i_show3
    i_show3=not(this.w_SHTIPSCH = "N")
    this.oPgFrm.Pages(4).enabled=i_show3 and not(this.w_SHTIPSCH = "N")
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Proattivit�"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    local i_show4
    i_show4=not(this.w_SHTIPSCH = "N")
    this.oPgFrm.Pages(5).enabled=i_show4 and not(this.w_SHTIPSCH = "N")
    this.oPgFrm.Pages(5).caption=iif(i_show4,cp_translate("Aziende"),"")
    this.oPgFrm.Pages(5).oPag.visible=this.oPgFrm.Pages(5).enabled
    this.oPgFrm.Page1.oPag.oSHNUMFREQ_1_25.visible=!this.oPgFrm.Page1.oPag.oSHNUMFREQ_1_25.mHide()
    this.oPgFrm.Page1.oPag.oSHMINUINT_1_29.visible=!this.oPgFrm.Page1.oPag.oSHMINUINT_1_29.mHide()
    this.oPgFrm.Page1.oPag.oora_lancifi_1_30.visible=!this.oPgFrm.Page1.oPag.oora_lancifi_1_30.mHide()
    this.oPgFrm.Page1.oPag.omm_lancifi_1_31.visible=!this.oPgFrm.Page1.oPag.omm_lancifi_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.olun_1_53.visible=!this.oPgFrm.Page1.oPag.olun_1_53.mHide()
    this.oPgFrm.Page1.oPag.oMar_1_54.visible=!this.oPgFrm.Page1.oPag.oMar_1_54.mHide()
    this.oPgFrm.Page1.oPag.oMer_1_55.visible=!this.oPgFrm.Page1.oPag.oMer_1_55.mHide()
    this.oPgFrm.Page1.oPag.oGio_1_56.visible=!this.oPgFrm.Page1.oPag.oGio_1_56.mHide()
    this.oPgFrm.Page1.oPag.oVen_1_57.visible=!this.oPgFrm.Page1.oPag.oVen_1_57.mHide()
    this.oPgFrm.Page1.oPag.oSab_1_58.visible=!this.oPgFrm.Page1.oPag.oSab_1_58.mHide()
    this.oPgFrm.Page1.oPag.oDom_1_59.visible=!this.oPgFrm.Page1.oPag.oDom_1_59.mHide()
    this.oPgFrm.Page1.oPag.oGen_1_60.visible=!this.oPgFrm.Page1.oPag.oGen_1_60.mHide()
    this.oPgFrm.Page1.oPag.oFeb_1_61.visible=!this.oPgFrm.Page1.oPag.oFeb_1_61.mHide()
    this.oPgFrm.Page1.oPag.oMarz_1_62.visible=!this.oPgFrm.Page1.oPag.oMarz_1_62.mHide()
    this.oPgFrm.Page1.oPag.oApr_1_63.visible=!this.oPgFrm.Page1.oPag.oApr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oMag_1_64.visible=!this.oPgFrm.Page1.oPag.oMag_1_64.mHide()
    this.oPgFrm.Page1.oPag.oGiu_1_65.visible=!this.oPgFrm.Page1.oPag.oGiu_1_65.mHide()
    this.oPgFrm.Page1.oPag.oLug_1_66.visible=!this.oPgFrm.Page1.oPag.oLug_1_66.mHide()
    this.oPgFrm.Page1.oPag.oAgo_1_67.visible=!this.oPgFrm.Page1.oPag.oAgo_1_67.mHide()
    this.oPgFrm.Page1.oPag.oSet_1_68.visible=!this.oPgFrm.Page1.oPag.oSet_1_68.mHide()
    this.oPgFrm.Page1.oPag.oOtt_1_69.visible=!this.oPgFrm.Page1.oPag.oOtt_1_69.mHide()
    this.oPgFrm.Page1.oPag.oNov_1_70.visible=!this.oPgFrm.Page1.oPag.oNov_1_70.mHide()
    this.oPgFrm.Page1.oPag.oDic_1_71.visible=!this.oPgFrm.Page1.oPag.oDic_1_71.mHide()
    this.oPgFrm.Page1.oPag.oSHIMPDAT_1_73.visible=!this.oPgFrm.Page1.oPag.oSHIMPDAT_1_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oSHDATFIS_1_75.visible=!this.oPgFrm.Page1.oPag.oSHDATFIS_1_75.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_2.visible=!this.oPgFrm.Page3.oPag.oStr_3_2.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_3.visible=!this.oPgFrm.Page3.oPag.oStr_3_3.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_4.visible=!this.oPgFrm.Page3.oPag.oStr_3_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_96.visible=!this.oPgFrm.Page1.oPag.oStr_1_96.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_5.visible=!this.oPgFrm.Page4.oPag.oBtn_4_5.mHide()
    this.oPgFrm.Page4.oPag.oSHREPRES_4_9.visible=!this.oPgFrm.Page4.oPag.oSHREPRES_4_9.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_10.visible=!this.oPgFrm.Page4.oPag.oBtn_4_10.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_11.visible=!this.oPgFrm.Page4.oPag.oStr_4_11.mHide()
    this.oPgFrm.Page4.oPag.oSHTXTHEA_4_13.visible=!this.oPgFrm.Page4.oPag.oSHTXTHEA_4_13.mHide()
    this.oPgFrm.Page4.oPag.oSHTXTBOD_4_14.visible=!this.oPgFrm.Page4.oPag.oSHTXTBOD_4_14.mHide()
    this.oPgFrm.Page4.oPag.oSHTXTFOO_4_15.visible=!this.oPgFrm.Page4.oPag.oSHTXTFOO_4_15.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_16.visible=!this.oPgFrm.Page4.oPag.oStr_4_16.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_17.visible=!this.oPgFrm.Page4.oPag.oStr_4_17.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_18.visible=!this.oPgFrm.Page4.oPag.oStr_4_18.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_23.visible=!this.oPgFrm.Page4.oPag.oStr_4_23.mHide()
    this.oPgFrm.Page4.oPag.oSHFINPRE_4_24.visible=!this.oPgFrm.Page4.oPag.oSHFINPRE_4_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_108.visible=!this.oPgFrm.Page1.oPag.oStr_1_108.mHide()
    this.oPgFrm.Page1.oPag.oSHDATMAN_1_115.visible=!this.oPgFrm.Page1.oPag.oSHDATMAN_1_115.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_YOALXQOMFK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Deleted")
          .Calculate_COOOAZPTJA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("Insert end")
          .Calculate_UONUPKHBAT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggiornaAziende") or lower(cEvent)==lower("Update end")
          .Calculate_NMSRIPBNBG()
          bRefresh=.t.
        endif
      .oPgFrm.Page6.oPag.ZoomLog.Event(cEvent)
      .oPgFrm.Page6.oPag.ZoomProc.Event(cEvent)
      .oPgFrm.Page5.oPag.AZIPROCE.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsjb_ash
    IF cEVENT = "Init"
       if Upper(this.GSJB_MSA.class)='STDLAZYCHILD'
          this.GSJB_MSA.createrealchild()
          This.oPgFrm.ActivePage=1
       Endif
    ENDIF
    
    
    IF cEVENT = "w_SHAGENDA Changed"
        IF VARTYPE( i_bDISABLEPOSTIN ) <> "U"
            IF this.w_SHAGENDA = "P" AND i_bDISABLEPOSTIN
              cp_errormsg( "Per attivare la funzionalit� occorre abilitare i post-in" , "!" )
            ENDIF
        ENDIF
    ENDIF
    
    IF cEVENT = "w_SHFLINVS Changed"
        this.GSJB_MSA.MarkPos()
        this.GSJB_MSA.FirstRow()
        do while not this.GSJB_MSA.Eof_Trs()
          IF this.w_SHFLINVS = "S"
            * Log e stampe
            this.GSJB_MSA.Set( "w_UGTIPMES" , 3 )
          ENDIF
          IF this.w_SHFLINVS = "N"
            * Solo log
            this.GSJB_MSA.Set( "w_UGTIPMES" , 1 )
          ENDIF
          this.GSJB_MSA.NextRow()
        enddo
        this.GSJB_MSA.RePos()
    ENDIF
    
    IF cEvent = "w_aziproce after query"
      Select (this.w_AZIPROCE.cCursor )
      go top
      scan
      REPLACE XCHK WITH ATTIVATO
      if AZCODAZI = i_CODAZI and THIS.CFUNCTION = 'Load'
        REPLACE XCHK WITH 1
      endif
      ENDSCAN
      Select (this.w_AZIPROCE.cCursor )
      go top
    ENDIF
    
    if cEvent = "w_SELEZI Changed"
      SELECT (THIS.w_AZIPROCE.cCURSOR)
      GO TOP
      SCAN
        IF THIS.w_SELEZI='S'
          REPLACE XCHK WITH 1
        ELSE
          REPLACE XCHK WITH 0
        ENDIF
      ENDSCAN
      SELECT (THIS.w_AZIPROCE.cCURSOR)
      GO TOP
    endif
    
    if cEvent = "w_aziproce row unchecked"
      SELECT (THIS.w_AZIPROCE.cCURSOR)
      L_RIGA = RECNO()
      sum xchk to L_CONTACHECK
      IF L_CONTACHECK = 0
        AH_ERRORMSG("Nessuna azienda selezionata" , "!" )
      ENDIF
      IF L_RIGA > 0 AND L_RIGA <= RECCOUNT(THIS.w_AZIPROCE.cCURSOR)
        GO L_RIGA
      ENDIF
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SHUTEPRE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SHUTEPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_SHUTEPRE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_SHUTEPRE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SHUTEPRE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oSHUTEPRE_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SHUTEPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_SHUTEPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_SHUTEPRE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SHUTEPRE = NVL(_Link_.CODE,0)
      this.w_DESUTEP = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_SHUTEPRE = 0
      endif
      this.w_DESUTEP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=looktab("cpusrgrp", "groupcode", "groupcode", .w_GRPSCHED, "usercode", .w_SHUTEPRE)=.w_GRPSCHED
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Utente non valido oppure non appartenente al gruppo schedulatore")
        endif
        this.w_SHUTEPRE = 0
        this.w_DESUTEP = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SHUTEPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SHUTECREA
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SHUTECREA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SHUTECREA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_SHUTECREA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_SHUTECREA)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SHUTECREA = NVL(_Link_.CODE,0)
      this.w_DESUTEC = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_SHUTECREA = 0
      endif
      this.w_DESUTEC = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SHUTECREA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SHUTEVAR
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SHUTEVAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SHUTEVAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_SHUTEVAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_SHUTEVAR)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SHUTEVAR = NVL(_Link_.CODE,0)
      this.w_DESUTEV = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_SHUTEVAR = 0
      endif
      this.w_DESUTEV = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SHUTEVAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOG
  func Link_6_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_lTable = "LOGELAJB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2], .t., this.LOGELAJB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL,LSNOTELA";
                   +" from "+i_cTable+" "+i_lTable+" where LSSERIAL="+cp_ToStrODBC(this.w_LOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',this.w_LOG)
            select LSSERIAL,LSNOTELA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOG = NVL(_Link_.LSSERIAL,space(10))
      this.w_MEMO = NVL(_Link_.LSNOTELA,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_LOG = space(10)
      endif
      this.w_MEMO = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])+'\'+cp_ToStr(_Link_.LSSERIAL,1)
      cp_ShowWarn(i_cKey,this.LOGELAJB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CPROWNUM
  func Link_6_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_lTable = "LOGELAJB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2], .t., this.LOGELAJB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPROWNUM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPROWNUM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL,CPROWNUM,LSNOTELA";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM);
                   +" and LSSERIAL="+cp_ToStrODBC(this.w_LOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',this.w_LOG;
                       ,'CPROWNUM',this.w_CPROWNUM)
            select LSSERIAL,CPROWNUM,LSNOTELA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPROWNUM = NVL(_Link_.CPROWNUM,0)
      this.w_MEMO = NVL(_Link_.LSNOTELA,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_CPROWNUM = 0
      endif
      this.w_MEMO = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])+'\'+cp_ToStr(_Link_.LSSERIAL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.LOGELAJB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPROWNUM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSHCODICE_1_1.value==this.w_SHCODICE)
      this.oPgFrm.Page1.oPag.oSHCODICE_1_1.value=this.w_SHCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSHFLAT_1_2.RadioValue()==this.w_SHFLAT)
      this.oPgFrm.Page1.oPag.oSHFLAT_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHUTEPRE_1_4.value==this.w_SHUTEPRE)
      this.oPgFrm.Page1.oPag.oSHUTEPRE_1_4.value=this.w_SHUTEPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDESCRI_1_5.value==this.w_SHDESCRI)
      this.oPgFrm.Page1.oPag.oSHDESCRI_1_5.value=this.w_SHDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSHUTECREA_1_6.value==this.w_SHUTECREA)
      this.oPgFrm.Page1.oPag.oSHUTECREA_1_6.value=this.w_SHUTECREA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTEC_1_7.value==this.w_DESUTEC)
      this.oPgFrm.Page1.oPag.oDESUTEC_1_7.value=this.w_DESUTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oSHUTEVAR_1_8.value==this.w_SHUTEVAR)
      this.oPgFrm.Page1.oPag.oSHUTEVAR_1_8.value=this.w_SHUTEVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTEV_1_9.value==this.w_DESUTEV)
      this.oPgFrm.Page1.oPag.oDESUTEV_1_9.value=this.w_DESUTEV
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDATACREA_1_10.value==this.w_SHDATACREA)
      this.oPgFrm.Page1.oPag.oSHDATACREA_1_10.value=this.w_SHDATACREA
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDATAVAR_1_11.value==this.w_SHDATAVAR)
      this.oPgFrm.Page1.oPag.oSHDATAVAR_1_11.value=this.w_SHDATAVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oSHRELOAD_1_12.RadioValue()==this.w_SHRELOAD)
      this.oPgFrm.Page1.oPag.oSHRELOAD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDATAPRO_1_13.value==this.w_SHDATAPRO)
      this.oPgFrm.Page1.oPag.oSHDATAPRO_1_13.value=this.w_SHDATAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSHORAPROS_1_14.value==this.w_SHORAPROS)
      this.oPgFrm.Page1.oPag.oSHORAPROS_1_14.value=this.w_SHORAPROS
    endif
    if not(this.oPgFrm.Page1.oPag.oora_attesa_1_18.value==this.w_ora_attesa)
      this.oPgFrm.Page1.oPag.oora_attesa_1_18.value=this.w_ora_attesa
    endif
    if not(this.oPgFrm.Page1.oPag.omm_attesa_1_19.value==this.w_mm_attesa)
      this.oPgFrm.Page1.oPag.omm_attesa_1_19.value=this.w_mm_attesa
    endif
    if not(this.oPgFrm.Page1.oPag.oSHFREQ_1_20.RadioValue()==this.w_SHFREQ)
      this.oPgFrm.Page1.oPag.oSHFREQ_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDATASTART_1_21.value==this.w_SHDATASTART)
      this.oPgFrm.Page1.oPag.oSHDATASTART_1_21.value=this.w_SHDATASTART
    endif
    if not(this.oPgFrm.Page1.oPag.oSHORAINE_1_22.value==this.w_SHORAINE)
      this.oPgFrm.Page1.oPag.oSHORAINE_1_22.value=this.w_SHORAINE
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDATAFINE_1_23.value==this.w_SHDATAFINE)
      this.oPgFrm.Page1.oPag.oSHDATAFINE_1_23.value=this.w_SHDATAFINE
    endif
    if not(this.oPgFrm.Page1.oPag.oSHORAFNE_1_24.value==this.w_SHORAFNE)
      this.oPgFrm.Page1.oPag.oSHORAFNE_1_24.value=this.w_SHORAFNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSHNUMFREQ_1_25.value==this.w_SHNUMFREQ)
      this.oPgFrm.Page1.oPag.oSHNUMFREQ_1_25.value=this.w_SHNUMFREQ
    endif
    if not(this.oPgFrm.Page1.oPag.oora_lancio_1_27.value==this.w_ora_lancio)
      this.oPgFrm.Page1.oPag.oora_lancio_1_27.value=this.w_ora_lancio
    endif
    if not(this.oPgFrm.Page1.oPag.omm_lancio_1_28.value==this.w_mm_lancio)
      this.oPgFrm.Page1.oPag.omm_lancio_1_28.value=this.w_mm_lancio
    endif
    if not(this.oPgFrm.Page1.oPag.oSHMINUINT_1_29.value==this.w_SHMINUINT)
      this.oPgFrm.Page1.oPag.oSHMINUINT_1_29.value=this.w_SHMINUINT
    endif
    if not(this.oPgFrm.Page1.oPag.oora_lancifi_1_30.value==this.w_ora_lancifi)
      this.oPgFrm.Page1.oPag.oora_lancifi_1_30.value=this.w_ora_lancifi
    endif
    if not(this.oPgFrm.Page1.oPag.omm_lancifi_1_31.value==this.w_mm_lancifi)
      this.oPgFrm.Page1.oPag.omm_lancifi_1_31.value=this.w_mm_lancifi
    endif
    if not(this.oPgFrm.Page1.oPag.oSHPATH_1_42.value==this.w_SHPATH)
      this.oPgFrm.Page1.oPag.oSHPATH_1_42.value=this.w_SHPATH
    endif
    if not(this.oPgFrm.Page1.oPag.oSHFLGSTC_1_44.RadioValue()==this.w_SHFLGSTC)
      this.oPgFrm.Page1.oPag.oSHFLGSTC_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHFLINVS_1_45.RadioValue()==this.w_SHFLINVS)
      this.oPgFrm.Page1.oPag.oSHFLINVS_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.olun_1_53.RadioValue()==this.w_lun)
      this.oPgFrm.Page1.oPag.olun_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMar_1_54.RadioValue()==this.w_Mar)
      this.oPgFrm.Page1.oPag.oMar_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMer_1_55.RadioValue()==this.w_Mer)
      this.oPgFrm.Page1.oPag.oMer_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGio_1_56.RadioValue()==this.w_Gio)
      this.oPgFrm.Page1.oPag.oGio_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVen_1_57.RadioValue()==this.w_Ven)
      this.oPgFrm.Page1.oPag.oVen_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSab_1_58.RadioValue()==this.w_Sab)
      this.oPgFrm.Page1.oPag.oSab_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDom_1_59.RadioValue()==this.w_Dom)
      this.oPgFrm.Page1.oPag.oDom_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGen_1_60.RadioValue()==this.w_Gen)
      this.oPgFrm.Page1.oPag.oGen_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFeb_1_61.RadioValue()==this.w_Feb)
      this.oPgFrm.Page1.oPag.oFeb_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMarz_1_62.RadioValue()==this.w_Marz)
      this.oPgFrm.Page1.oPag.oMarz_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oApr_1_63.RadioValue()==this.w_Apr)
      this.oPgFrm.Page1.oPag.oApr_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMag_1_64.RadioValue()==this.w_Mag)
      this.oPgFrm.Page1.oPag.oMag_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGiu_1_65.RadioValue()==this.w_Giu)
      this.oPgFrm.Page1.oPag.oGiu_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLug_1_66.RadioValue()==this.w_Lug)
      this.oPgFrm.Page1.oPag.oLug_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAgo_1_67.RadioValue()==this.w_Ago)
      this.oPgFrm.Page1.oPag.oAgo_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSet_1_68.RadioValue()==this.w_Set)
      this.oPgFrm.Page1.oPag.oSet_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOtt_1_69.RadioValue()==this.w_Ott)
      this.oPgFrm.Page1.oPag.oOtt_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNov_1_70.RadioValue()==this.w_Nov)
      this.oPgFrm.Page1.oPag.oNov_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDic_1_71.RadioValue()==this.w_Dic)
      this.oPgFrm.Page1.oPag.oDic_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHIMPDAT_1_73.RadioValue()==this.w_SHIMPDAT)
      this.oPgFrm.Page1.oPag.oSHIMPDAT_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDATFIS_1_75.value==this.w_SHDATFIS)
      this.oPgFrm.Page1.oPag.oSHDATFIS_1_75.value=this.w_SHDATFIS
    endif
    if not(this.oPgFrm.Page4.oPag.oSHTIPELA_4_1.RadioValue()==this.w_SHTIPELA)
      this.oPgFrm.Page4.oPag.oSHTIPELA_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oSHELABOR_4_4.value==this.w_SHELABOR)
      this.oPgFrm.Page4.oPag.oSHELABOR_4_4.value=this.w_SHELABOR
    endif
    if not(this.oPgFrm.Page4.oPag.oSHTIPRES_4_6.RadioValue()==this.w_SHTIPRES)
      this.oPgFrm.Page4.oPag.oSHTIPRES_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oSHFRMRES_4_7.RadioValue()==this.w_SHFRMRES)
      this.oPgFrm.Page4.oPag.oSHFRMRES_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oSHREPRES_4_9.value==this.w_SHREPRES)
      this.oPgFrm.Page4.oPag.oSHREPRES_4_9.value=this.w_SHREPRES
    endif
    if not(this.oPgFrm.Page4.oPag.oSHTXTHEA_4_13.value==this.w_SHTXTHEA)
      this.oPgFrm.Page4.oPag.oSHTXTHEA_4_13.value=this.w_SHTXTHEA
    endif
    if not(this.oPgFrm.Page4.oPag.oSHTXTBOD_4_14.value==this.w_SHTXTBOD)
      this.oPgFrm.Page4.oPag.oSHTXTBOD_4_14.value=this.w_SHTXTBOD
    endif
    if not(this.oPgFrm.Page4.oPag.oSHTXTFOO_4_15.value==this.w_SHTXTFOO)
      this.oPgFrm.Page4.oPag.oSHTXTFOO_4_15.value=this.w_SHTXTFOO
    endif
    if not(this.oPgFrm.Page4.oPag.oSHAGENDA_4_19.RadioValue()==this.w_SHAGENDA)
      this.oPgFrm.Page4.oPag.oSHAGENDA_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oSHDESTIN_4_20.value==this.w_SHDESTIN)
      this.oPgFrm.Page4.oPag.oSHDESTIN_4_20.value=this.w_SHDESTIN
    endif
    if not(this.oPgFrm.Page4.oPag.oSHFINPRE_4_24.value==this.w_SHFINPRE)
      this.oPgFrm.Page4.oPag.oSHFINPRE_4_24.value=this.w_SHFINPRE
    endif
    if not(this.oPgFrm.Page6.oPag.oMEMO_6_3.value==this.w_MEMO)
      this.oPgFrm.Page6.oPag.oMEMO_6_3.value=this.w_MEMO
    endif
    if not(this.oPgFrm.Page5.oPag.oSELEZI_5_3.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page5.oPag.oSELEZI_5_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDATMAN_1_115.RadioValue()==this.w_SHDATMAN)
      this.oPgFrm.Page1.oPag.oSHDATMAN_1_115.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTEP_1_117.value==this.w_DESUTEP)
      this.oPgFrm.Page1.oPag.oDESUTEP_1_117.value=this.w_DESUTEP
    endif
    cp_SetControlsValueExtFlds(this,'SCHEDJOB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(((.w_SHMINUINT<>0 And ((Val(.w_ora_lancifi) * 60 + Val(.w_mm_lancifi)) >= (Val(.w_ora_lancio) * 60 + Val(.w_mm_lancio) + .w_SHMINUINT))) Or .w_SHMINUINT = 0 OR .w_SHFREQ='R') and (.w_SHFREQ<>'S' OR !empty(ALLTRIM(.w_SHSET))) and (.w_SHFREQ<>'M' OR !empty(ALLTRIM(.w_SHMESE))))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di prossima esecuzione non definita od ora e/o minuti di fine esecuzione non validi")
          case   not(looktab("cpusrgrp", "groupcode", "groupcode", .w_GRPSCHED, "usercode", .w_SHUTEPRE)=.w_GRPSCHED)  and not(empty(.w_SHUTEPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSHUTEPRE_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Utente non valido oppure non appartenente al gruppo schedulatore")
          case   (empty(.w_SHDATAPRO) and (.w_SHDATMAN='S' OR .w_SHFREQ="R"))  and (.w_SHFREQ$"U-R" OR .w_SHDATMAN='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSHDATAPRO_1_13.SetFocus()
            i_bnoObbl = !empty(.w_SHDATAPRO) or !(.w_SHDATMAN='S' OR .w_SHFREQ="R")
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_SHORAPROS) and (.w_SHDATMAN='S' OR .w_SHFREQ="R")) or not(LEFT(.w_SHORAPROS,2)<'24' AND RIGHT(.w_SHORAPROS,2)<'60'  AND LEN(ALLTRIM(SUBSTR(.w_SHORAPROS,1,AT(':',.w_SHORAPROS)-1)))=2 AND LEN(ALLTRIM(SUBSTR(.w_SHORAPROS,AT(':',.w_SHORAPROS)+1)))=2))  and (.w_SHFREQ$"U-R" OR .w_SHDATMAN='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSHORAPROS_1_14.SetFocus()
            i_bnoObbl = !empty(.w_SHORAPROS) or !(.w_SHDATMAN='S' OR .w_SHFREQ="R")
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora inserita incogruente")
          case   not(.w_ora_attesa<'99')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oora_attesa_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore compreso tra 00 e 98")
          case   not(.w_mm_attesa<'60')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.omm_attesa_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore compreso tra 00 e 59")
          case   not(.w_SHNUMFREQ<>0)  and not(.w_SHFREQ$"M-U-R")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSHNUMFREQ_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ora_lancio<'24')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oora_lancio_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore compreso tra 00 e 23")
          case   not(.w_mm_lancio<'60')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.omm_lancio_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore compreso tra 00 e 59")
          case   not(.w_SHMINUINT<=720 OR .w_SHFREQ='R')  and not(.w_SHFREQ='U')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSHMINUINT_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di minuti per pi� volte al giorno, altrimenti selezionare 0")
          case   not(.w_ora_lancifi<'24')  and not(.w_SHFREQ$"U-R")  and (.w_SHMINUINT<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oora_lancifi_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore compreso tra 00 e 23")
          case   not(.w_mm_lancifi < '60')  and not(.w_SHFREQ$"U-R")  and (.w_SHMINUINT<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.omm_lancifi_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore compreso tra 00 e 59")
          case   not((.w_SHIMPDAT='F' or .w_SHFREQ<>'M' ) Or ( .w_SHDATFIS>0 and .w_SHDATFIS<32 ))  and not(.w_SHIMPDAT='F' or .w_SHFREQ<>'M')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSHDATFIS_1_75.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSJB_MJB.CheckForm()
      if i_bres
        i_bres=  .GSJB_MJB.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSJB_MSA.CheckForm()
      if i_bres
        i_bres=  .GSJB_MSA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsjb_ash
      * --- se attivo il check devo effettuare il controllo di esistenza del file INI
      * --- di configurazione del servizio. Se non esiste invito l'utente a configurarlo
      if this.w_SHRELOAD='S'
         if not file(SUBSTR(SYS(5)+SYS(2003),1,RAT('\',SYS(5)+SYS(2003),1))+'jbsh\ZSchSrv\Scheduler.ini')
         ah_ErrorMsg("Si � attivata l'esecuzione job con riavvio ma non � stato trovato il file di configurazione.%0Provvedere alla sua generazione tramite la voce 'parametri accesso silente schedulatore'")
         endif
      endif
      
      if this.w_SHTIPSCH='P' AND this.w_SHFRMRES='A' AND EMPTY( this.w_SHREPRES )
         ah_ErrorMsg("Se il formato dell'elaborazione selezionata � 'allegato', occorre specificare un report","!")
         i_bRes=.f.
      endif
      if i_bRes
         IF !chknfile(alltrim(.w_SHPATH),'F')
            i_bRes = .f.
          	i_bnoChk = .t.
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SHORAINI = this.w_SHORAINI
    this.o_SHORAFIN = this.w_SHORAFIN
    this.o_SHTMPMAX = this.w_SHTMPMAX
    this.o_ora_attesa = this.w_ora_attesa
    this.o_mm_attesa = this.w_mm_attesa
    this.o_SHFREQ = this.w_SHFREQ
    this.o_ora_lancio = this.w_ora_lancio
    this.o_mm_lancio = this.w_mm_lancio
    this.o_ora_lancifi = this.w_ora_lancifi
    this.o_mm_lancifi = this.w_mm_lancifi
    this.o_SHPATH = this.w_SHPATH
    this.o_SHFLINVS = this.w_SHFLINVS
    this.o_SHSET = this.w_SHSET
    this.o_SHMESE = this.w_SHMESE
    this.o_SHTIPSCH = this.w_SHTIPSCH
    this.o_SHTIPELA = this.w_SHTIPELA
    this.o_SHFRMRES = this.w_SHFRMRES
    this.o_SHAGENDA = this.w_SHAGENDA
    this.o_SHDATMAN = this.w_SHDATMAN
    * --- GSJB_MJB : Depends On
    this.GSJB_MJB.SaveDependsOn()
    * --- GSJB_MSA : Depends On
    this.GSJB_MSA.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsjb_ashPag1 as StdContainer
  Width  = 735
  height = 437
  stdWidth  = 735
  stdheight = 437
  resizeXpos=281
  resizeYpos=137
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSHCODICE_1_1 as StdField with uid="LBTTNUMQLE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SHCODICE", cQueryName = "SHCODICE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 118034283,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=89, Left=155, Top=7, cSayPict='"9999999999"', cGetPict='"9999999999"'

  add object oSHFLAT_1_2 as StdCheck with uid="EETXTTOYBC",rtseq=2,rtrep=.f.,left=302, top=7, caption="Attivata",;
    ToolTipText = "Attiva/disattiva",;
    HelpContextID = 30818086,;
    cFormVar="w_SHFLAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHFLAT_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSHFLAT_1_2.GetRadio()
    this.Parent.oContained.w_SHFLAT = this.RadioValue()
    return .t.
  endfunc

  func oSHFLAT_1_2.SetRadio()
    this.Parent.oContained.w_SHFLAT=trim(this.Parent.oContained.w_SHFLAT)
    this.value = ;
      iif(this.Parent.oContained.w_SHFLAT=='S',1,;
      0)
  endfunc

  add object oSHUTEPRE_1_4 as StdField with uid="TXJVZNDBIF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SHUTEPRE", cQueryName = "SHUTEPRE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Utente non valido oppure non appartenente al gruppo schedulatore",;
    ToolTipText = "Utente preferenziale per l'esecuzione del job",;
    HelpContextID = 236924779,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=516, Top=7, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_SHUTEPRE"

  func oSHUTEPRE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSHUTEPRE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSHUTEPRE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oSHUTEPRE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oSHDESCRI_1_5 as StdField with uid="GPGRYTAEDH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SHDESCRI", cQueryName = "SHDESCRI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 32448367,;
   bGlobalFont=.t.,;
    Height=21, Width=398, Left=155, Top=33, InputMask=replicate('X',254)

  add object oSHUTECREA_1_6 as StdField with uid="XPRKYWEIKS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SHUTECREA", cQueryName = "SHUTECREA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha creato il record",;
    HelpContextID = 18822011,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=98, Top=69, cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_SHUTECREA"

  func oSHUTECREA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESUTEC_1_7 as StdField with uid="TOTMUBYYSS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESUTEC", cQueryName = "DESUTEC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 68160310,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=158, Top=69, InputMask=replicate('X',20)

  add object oSHUTEVAR_1_8 as StdField with uid="TCFSWYUNSP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SHUTEVAR", cQueryName = "SHUTEVAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha variato il record",;
    HelpContextID = 69152632,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=98, Top=99, cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_SHUTEVAR"

  func oSHUTEVAR_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESUTEV_1_9 as StdField with uid="NDGABRTSGZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESUTEV", cQueryName = "DESUTEV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 200275146,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=158, Top=99, InputMask=replicate('X',20)

  add object oSHDATACREA_1_10 as StdField with uid="MGFZUNLCFN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SHDATACREA", cQueryName = "SHDATACREA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di creazione",;
    HelpContextID = 268133576,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=354, Top=69

  add object oSHDATAVAR_1_11 as StdField with uid="IFWGPKHVFX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SHDATAVAR", cQueryName = "SHDATAVAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data variazione",;
    HelpContextID = 318329,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=354, Top=99

  add object oSHRELOAD_1_12 as StdCheck with uid="MPCDTBCMJZ",rtseq=12,rtrep=.f.,left=463, top=68, caption="Esecuzione con riavvio",;
    ToolTipText = "Se attivo l'applicativo viene terminato riaprendo successivamente una nuova sessione prima dell'esecuzione [Valido utilizzando Zucchetti Scheduler Service]",;
    HelpContextID = 226492266,;
    cFormVar="w_SHRELOAD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHRELOAD_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSHRELOAD_1_12.GetRadio()
    this.Parent.oContained.w_SHRELOAD = this.RadioValue()
    return .t.
  endfunc

  func oSHRELOAD_1_12.SetRadio()
    this.Parent.oContained.w_SHRELOAD=trim(this.Parent.oContained.w_SHRELOAD)
    this.value = ;
      iif(this.Parent.oContained.w_SHRELOAD=='S',1,;
      0)
  endfunc

  add object oSHDATAPRO_1_13 as StdField with uid="GBHEJVXXWQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SHDATAPRO", cQueryName = "SHDATAPRO",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prossima esecuzione",;
    HelpContextID = 268117096,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=80, Top=165

  func oSHDATAPRO_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHFREQ$"U-R" OR .w_SHDATMAN='S')
    endwith
   endif
  endfunc

  func oSHDATAPRO_1_13.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_SHDATMAN='S' OR .w_SHFREQ="R"
    endwith
    return i_bres
  endfunc

  add object oSHORAPROS_1_14 as StdField with uid="MGBWRURJYV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SHORAPROS", cQueryName = "SHORAPROS",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Ora inserita incogruente",;
    ToolTipText = "Ora prossima esecuzione",;
    HelpContextID = 232576165,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=208, Top=165, cSayPict="'99:99'", cGetPict="'99:99'", InputMask=replicate('X',5)

  func oSHORAPROS_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHFREQ$"U-R" OR .w_SHDATMAN='S')
    endwith
   endif
  endfunc

  func oSHORAPROS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (LEFT(.w_SHORAPROS,2)<'24' AND RIGHT(.w_SHORAPROS,2)<'60'  AND LEN(ALLTRIM(SUBSTR(.w_SHORAPROS,1,AT(':',.w_SHORAPROS)-1)))=2 AND LEN(ALLTRIM(SUBSTR(.w_SHORAPROS,AT(':',.w_SHORAPROS)+1)))=2)
    endwith
    return bRes
  endfunc

  func oSHORAPROS_1_14.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_SHDATMAN='S' OR .w_SHFREQ="R"
    endwith
    return i_bres
  endfunc

  add object oora_attesa_1_18 as StdField with uid="ZXARWVIPWC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ora_attesa", cQueryName = "ora_attesa",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore compreso tra 00 e 98",;
    ToolTipText = "Ore tempo massimo di attesa in coda",;
    HelpContextID = 202669189,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=208, Top=199, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oora_attesa_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ora_attesa<'99')
    endwith
    return bRes
  endfunc

  add object omm_attesa_1_19 as StdField with uid="FURIQNDIKM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_mm_attesa", cQueryName = "mm_attesa",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti tempo massimo di attesa in coda",;
    HelpContextID = 182649783,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=243, Top=199, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func omm_attesa_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_mm_attesa<'60')
    endwith
    return bRes
  endfunc

  add object oSHFREQ_1_20 as StdRadio with uid="KDKCQBFGCR",rtseq=20,rtrep=.f.,left=300, top=162, width=402,height=23;
    , ToolTipText = "Frequenza lancio";
    , cFormVar="w_SHFREQ", ButtonCount=5, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSHFREQ_1_20.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Unica"
      this.Buttons(1).HelpContextID = 253509414
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Unica","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Giornaliera"
      this.Buttons(2).HelpContextID = 253509414
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Giornaliera","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Settimanale"
      this.Buttons(3).HelpContextID = 253509414
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Settimanale","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.Buttons(4).Caption="Mensile"
      this.Buttons(4).HelpContextID = 253509414
      this.Buttons(4).Left=i_coord
      this.Buttons(4).Width=(TxtWidth("Mensile","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(4).Width
      this.Buttons(5).Caption="Ripetuta"
      this.Buttons(5).HelpContextID = 253509414
      this.Buttons(5).Left=i_coord
      this.Buttons(5).Width=(TxtWidth("Ripetuta","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(5).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Frequenza lancio")
      StdRadio::init()
    endproc

  func oSHFREQ_1_20.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    iif(this.value =3,'S',;
    iif(this.value =4,'M',;
    iif(this.value =5,'R',;
    space(1)))))))
  endfunc
  func oSHFREQ_1_20.GetRadio()
    this.Parent.oContained.w_SHFREQ = this.RadioValue()
    return .t.
  endfunc

  func oSHFREQ_1_20.SetRadio()
    this.Parent.oContained.w_SHFREQ=trim(this.Parent.oContained.w_SHFREQ)
    this.value = ;
      iif(this.Parent.oContained.w_SHFREQ=='U',1,;
      iif(this.Parent.oContained.w_SHFREQ=='G',2,;
      iif(this.Parent.oContained.w_SHFREQ=='S',3,;
      iif(this.Parent.oContained.w_SHFREQ=='M',4,;
      iif(this.Parent.oContained.w_SHFREQ=='R',5,;
      0)))))
  endfunc

  add object oSHDATASTART_1_21 as StdField with uid="RDLYKSESUL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SHDATASTART", cQueryName = "SHDATASTART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data scelta per la schedulazione risulta antecedente a quella odierna! Inserirne una posteriore",;
    ToolTipText = "Data inizio elaborazione",;
    HelpContextID = 46474,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=87, Top=289

  add object oSHORAINE_1_22 as StdField with uid="FDXXTMKDRK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SHORAINE", cQueryName = "SHORAINE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Ora inizio elaborazione",;
    HelpContextID = 115134315,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=215, Top=289, InputMask=replicate('X',5)

  add object oSHDATAFINE_1_23 as StdField with uid="IISACFISQP",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SHDATAFINE", cQueryName = "SHDATAFINE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine elaborazione",;
    HelpContextID = 300721,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=87, Top=316

  add object oSHORAFNE_1_24 as StdField with uid="DCJIECITBR",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SHORAFNE", cQueryName = "SHORAFNE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Ora fine elaborazione",;
    HelpContextID = 64802667,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=215, Top=316, InputMask=replicate('X',5)

  add object oSHNUMFREQ_1_25 as StdField with uid="CINQIRHDFO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SHNUMFREQ", cQueryName = "SHNUMFREQ",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Frequenza nell'intervallo di tempo stabilito",;
    HelpContextID = 77579387,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=367, Top=208, cSayPict='"99"', cGetPict='"99"'

  func oSHNUMFREQ_1_25.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ$"M-U-R")
    endwith
  endfunc

  func oSHNUMFREQ_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SHNUMFREQ<>0)
    endwith
    return bRes
  endfunc


  add object oObj_1_26 as cp_calclbl with uid="ILXKCISRUM",left=397, top=210, width=134,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 68262118

  add object oora_lancio_1_27 as StdField with uid="YABYONNXIJ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ora_lancio", cQueryName = "ora_lancio",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore compreso tra 00 e 23",;
    ToolTipText = "Ore di inizio esecuzione",;
    HelpContextID = 26972377,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=593, Top=208, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oora_lancio_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ora_lancio<'24')
    endwith
    return bRes
  endfunc

  add object omm_lancio_1_28 as StdField with uid="NTMDFQRPVI",rtseq=27,rtrep=.f.,;
    cFormVar = "w_mm_lancio", cQueryName = "mm_lancio",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio esecuzione",;
    HelpContextID = 34079457,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=635, Top=208, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func omm_lancio_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_mm_lancio<'60')
    endwith
    return bRes
  endfunc

  add object oSHMINUINT_1_29 as StdField with uid="ZTAZVMCTHB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SHMINUINT", cQueryName = "SHMINUINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di minuti per pi� volte al giorno, altrimenti selezionare 0",;
    ToolTipText = "Intervallo di minuti in cui si esegue l'elaborazione",;
    HelpContextID = 207375180,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=367, Top=244

  func oSHMINUINT_1_29.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ='U')
    endwith
  endfunc

  func oSHMINUINT_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SHMINUINT<=720 OR .w_SHFREQ='R')
    endwith
    return bRes
  endfunc

  add object oora_lancifi_1_30 as StdField with uid="KLAEDKLYDB",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ora_lancifi", cQueryName = "ora_lancifi",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore compreso tra 00 e 23",;
    ToolTipText = "Ore di fine esecuzione",;
    HelpContextID = 27400153,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=593, Top=242, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oora_lancifi_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHMINUINT<>0)
    endwith
   endif
  endfunc

  func oora_lancifi_1_30.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ$"U-R")
    endwith
  endfunc

  func oora_lancifi_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ora_lancifi<'24')
    endwith
    return bRes
  endfunc

  add object omm_lancifi_1_31 as StdField with uid="ZZPIMOWPIO",rtseq=30,rtrep=.f.,;
    cFormVar = "w_mm_lancifi", cQueryName = "mm_lancifi",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine esecuzione",;
    HelpContextID = 34052721,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=635, Top=242, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func omm_lancifi_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHMINUINT<>0)
    endwith
   endif
  endfunc

  func omm_lancifi_1_31.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ$"U-R")
    endwith
  endfunc

  func omm_lancifi_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_mm_lancifi < '60')
    endwith
    return bRes
  endfunc

  add object oSHPATH_1_42 as StdField with uid="TMVWFEOEGD",rtseq=31,rtrep=.f.,;
    cFormVar = "w_SHPATH", cQueryName = "SHPATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Path di destinazione delle stampe PDF (se vuoto prende il path di default)",;
    HelpContextID = 117169958,;
   bGlobalFont=.t.,;
    Height=21, Width=505, Left=68, Top=377, InputMask=replicate('X',200)


  add object oBtn_1_43 as StdButton with uid="YWMDNIBBZZ",left=580, top=378, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare un path";
    , HelpContextID = 109534506;
  , bGlobalFont=.t.

    proc oBtn_1_43.Click()
      with this.Parent.oContained
        .w_SHPATH=left(cp_getdir(IIF(EMPTY(.w_SHPATH),sys(5)+sys(2003),.w_SHPATH),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSHFLGSTC_1_44 as StdCheck with uid="BYMBOVEDIO",rtseq=32,rtrep=.f.,left=68, top=404, caption="Abilita stampa cartacea",;
    ToolTipText = "Esegue anche la stampa su carta",;
    HelpContextID = 248103063,;
    cFormVar="w_SHFLGSTC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHFLGSTC_1_44.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSHFLGSTC_1_44.GetRadio()
    this.Parent.oContained.w_SHFLGSTC = this.RadioValue()
    return .t.
  endfunc

  func oSHFLGSTC_1_44.SetRadio()
    this.Parent.oContained.w_SHFLGSTC=trim(this.Parent.oContained.w_SHFLGSTC)
    this.value = ;
      iif(this.Parent.oContained.w_SHFLGSTC=='S',1,;
      0)
  endfunc


  add object oSHFLINVS_1_45 as StdCombo with uid="JQODONDFJW",rtseq=33,rtrep=.f.,left=446,top=406,width=153,height=21;
    , ToolTipText = "Seleziona che cosa inviare. Con 'selezione su destinatario' segue le impostazioni dei destinatari. Sui destinatari � possibile scegliere tra invio mail o post-in";
    , HelpContextID = 61456519;
    , cFormVar="w_SHFLINVS",RowSource=""+"Log e stampe,"+"Solo log,"+"Selezione su destinatario", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSHFLINVS_1_45.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oSHFLINVS_1_45.GetRadio()
    this.Parent.oContained.w_SHFLINVS = this.RadioValue()
    return .t.
  endfunc

  func oSHFLINVS_1_45.SetRadio()
    this.Parent.oContained.w_SHFLINVS=trim(this.Parent.oContained.w_SHFLINVS)
    this.value = ;
      iif(this.Parent.oContained.w_SHFLINVS=='S',1,;
      iif(this.Parent.oContained.w_SHFLINVS=='N',2,;
      iif(this.Parent.oContained.w_SHFLINVS=='D',3,;
      0)))
  endfunc

  add object olun_1_53 as StdCheck with uid="GOZBQWASTO",rtseq=36,rtrep=.f.,left=297, top=273, caption="Lun",;
    ToolTipText = "Luned�",;
    HelpContextID = 109253194,;
    cFormVar="w_lun", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func olun_1_53.RadioValue()
    return(iif(this.value =1,'Monday',;
    space(10)))
  endfunc
  func olun_1_53.GetRadio()
    this.Parent.oContained.w_lun = this.RadioValue()
    return .t.
  endfunc

  func olun_1_53.SetRadio()
    this.Parent.oContained.w_lun=trim(this.Parent.oContained.w_lun)
    this.value = ;
      iif(this.Parent.oContained.w_lun=='Monday',1,;
      0)
  endfunc

  func olun_1_53.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'S')
    endwith
  endfunc

  add object oMar_1_54 as StdCheck with uid="UHDFKKSAQX",rtseq=37,rtrep=.f.,left=349, top=273, caption="Mar",;
    ToolTipText = "Marted�",;
    HelpContextID = 109242426,;
    cFormVar="w_Mar", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMar_1_54.RadioValue()
    return(iif(this.value =1,'Tuesday',;
    space(10)))
  endfunc
  func oMar_1_54.GetRadio()
    this.Parent.oContained.w_Mar = this.RadioValue()
    return .t.
  endfunc

  func oMar_1_54.SetRadio()
    this.Parent.oContained.w_Mar=trim(this.Parent.oContained.w_Mar)
    this.value = ;
      iif(this.Parent.oContained.w_Mar=='Tuesday',1,;
      0)
  endfunc

  func oMar_1_54.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'S')
    endwith
  endfunc

  add object oMer_1_55 as StdCheck with uid="PTXXQJNHDX",rtseq=38,rtrep=.f.,left=407, top=273, caption="Mer",;
    ToolTipText = "Mercoled�",;
    HelpContextID = 109241402,;
    cFormVar="w_Mer", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMer_1_55.RadioValue()
    return(iif(this.value =1,'Wednesday',;
    space(10)))
  endfunc
  func oMer_1_55.GetRadio()
    this.Parent.oContained.w_Mer = this.RadioValue()
    return .t.
  endfunc

  func oMer_1_55.SetRadio()
    this.Parent.oContained.w_Mer=trim(this.Parent.oContained.w_Mer)
    this.value = ;
      iif(this.Parent.oContained.w_Mer=='Wednesday',1,;
      0)
  endfunc

  func oMer_1_55.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'S')
    endwith
  endfunc

  add object oGio_1_56 as StdCheck with uid="CRNEGPHSLO",rtseq=39,rtrep=.f.,left=468, top=273, caption="Gio",;
    ToolTipText = "Gioved�",;
    HelpContextID = 109252762,;
    cFormVar="w_Gio", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGio_1_56.RadioValue()
    return(iif(this.value =1,'Thursday',;
    space(10)))
  endfunc
  func oGio_1_56.GetRadio()
    this.Parent.oContained.w_Gio = this.RadioValue()
    return .t.
  endfunc

  func oGio_1_56.SetRadio()
    this.Parent.oContained.w_Gio=trim(this.Parent.oContained.w_Gio)
    this.value = ;
      iif(this.Parent.oContained.w_Gio=='Thursday',1,;
      0)
  endfunc

  func oGio_1_56.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'S')
    endwith
  endfunc

  add object oVen_1_57 as StdCheck with uid="HJNXEMHAGM",rtseq=40,rtrep=.f.,left=519, top=273, caption="Ven",;
    ToolTipText = "Venerd�",;
    HelpContextID = 109257642,;
    cFormVar="w_Ven", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVen_1_57.RadioValue()
    return(iif(this.value =1,'Friday',;
    space(10)))
  endfunc
  func oVen_1_57.GetRadio()
    this.Parent.oContained.w_Ven = this.RadioValue()
    return .t.
  endfunc

  func oVen_1_57.SetRadio()
    this.Parent.oContained.w_Ven=trim(this.Parent.oContained.w_Ven)
    this.value = ;
      iif(this.Parent.oContained.w_Ven=='Friday',1,;
      0)
  endfunc

  func oVen_1_57.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'S')
    endwith
  endfunc

  add object oSab_1_58 as StdCheck with uid="XHEYPUULCP",rtseq=41,rtrep=.f.,left=580, top=273, caption="Sab",;
    ToolTipText = "Sabato",;
    HelpContextID = 109307866,;
    cFormVar="w_Sab", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSab_1_58.RadioValue()
    return(iif(this.value =1,'Saturday',;
    space(10)))
  endfunc
  func oSab_1_58.GetRadio()
    this.Parent.oContained.w_Sab = this.RadioValue()
    return .t.
  endfunc

  func oSab_1_58.SetRadio()
    this.Parent.oContained.w_Sab=trim(this.Parent.oContained.w_Sab)
    this.value = ;
      iif(this.Parent.oContained.w_Sab=='Saturday',1,;
      0)
  endfunc

  func oSab_1_58.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'S')
    endwith
  endfunc

  add object oDom_1_59 as StdCheck with uid="UBZXQXOFLC",rtseq=42,rtrep=.f.,left=636, top=273, caption="Dom",;
    ToolTipText = "Domenica",;
    HelpContextID = 109259466,;
    cFormVar="w_Dom", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDom_1_59.RadioValue()
    return(iif(this.value =1,'Sunday',;
    space(10)))
  endfunc
  func oDom_1_59.GetRadio()
    this.Parent.oContained.w_Dom = this.RadioValue()
    return .t.
  endfunc

  func oDom_1_59.SetRadio()
    this.Parent.oContained.w_Dom=trim(this.Parent.oContained.w_Dom)
    this.value = ;
      iif(this.Parent.oContained.w_Dom=='Sunday',1,;
      0)
  endfunc

  func oDom_1_59.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'S')
    endwith
  endfunc

  add object oGen_1_60 as StdCheck with uid="HPCWCEDXTF",rtseq=43,rtrep=.f.,left=297, top=273, caption="Gen",;
    ToolTipText = "Gennaio",;
    HelpContextID = 109257882,;
    cFormVar="w_Gen", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGen_1_60.RadioValue()
    return(iif(this.value =1,'January',;
    space(10)))
  endfunc
  func oGen_1_60.GetRadio()
    this.Parent.oContained.w_Gen = this.RadioValue()
    return .t.
  endfunc

  func oGen_1_60.SetRadio()
    this.Parent.oContained.w_Gen=trim(this.Parent.oContained.w_Gen)
    this.value = ;
      iif(this.Parent.oContained.w_Gen=='January',1,;
      0)
  endfunc

  func oGen_1_60.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oFeb_1_61 as StdCheck with uid="XXKKJPGUES",rtseq=44,rtrep=.f.,left=349, top=273, caption="Feb",;
    ToolTipText = "Febbraio",;
    HelpContextID = 109307050,;
    cFormVar="w_Feb", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFeb_1_61.RadioValue()
    return(iif(this.value =1,'February',;
    space(10)))
  endfunc
  func oFeb_1_61.GetRadio()
    this.Parent.oContained.w_Feb = this.RadioValue()
    return .t.
  endfunc

  func oFeb_1_61.SetRadio()
    this.Parent.oContained.w_Feb=trim(this.Parent.oContained.w_Feb)
    this.value = ;
      iif(this.Parent.oContained.w_Feb=='February',1,;
      0)
  endfunc

  func oFeb_1_61.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oMarz_1_62 as StdCheck with uid="KJVAJHLNOF",rtseq=45,rtrep=.f.,left=407, top=273, caption="Mar",;
    ToolTipText = "Marzo",;
    HelpContextID = 101247034,;
    cFormVar="w_Marz", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMarz_1_62.RadioValue()
    return(iif(this.value =1,'March',;
    space(10)))
  endfunc
  func oMarz_1_62.GetRadio()
    this.Parent.oContained.w_Marz = this.RadioValue()
    return .t.
  endfunc

  func oMarz_1_62.SetRadio()
    this.Parent.oContained.w_Marz=trim(this.Parent.oContained.w_Marz)
    this.value = ;
      iif(this.Parent.oContained.w_Marz=='March',1,;
      0)
  endfunc

  func oMarz_1_62.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oApr_1_63 as StdCheck with uid="KZCVPRZDXR",rtseq=46,rtrep=.f.,left=468, top=273, caption="Apr",;
    ToolTipText = "Aprile",;
    HelpContextID = 109238778,;
    cFormVar="w_Apr", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oApr_1_63.RadioValue()
    return(iif(this.value =1,'April',;
    space(10)))
  endfunc
  func oApr_1_63.GetRadio()
    this.Parent.oContained.w_Apr = this.RadioValue()
    return .t.
  endfunc

  func oApr_1_63.SetRadio()
    this.Parent.oContained.w_Apr=trim(this.Parent.oContained.w_Apr)
    this.value = ;
      iif(this.Parent.oContained.w_Apr=='April',1,;
      0)
  endfunc

  func oApr_1_63.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oMag_1_64 as StdCheck with uid="CUVGCXYGYY",rtseq=47,rtrep=.f.,left=519, top=273, caption="Mag",;
    ToolTipText = "Maggio",;
    HelpContextID = 109287482,;
    cFormVar="w_Mag", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMag_1_64.RadioValue()
    return(iif(this.value =1,'May',;
    space(10)))
  endfunc
  func oMag_1_64.GetRadio()
    this.Parent.oContained.w_Mag = this.RadioValue()
    return .t.
  endfunc

  func oMag_1_64.SetRadio()
    this.Parent.oContained.w_Mag=trim(this.Parent.oContained.w_Mag)
    this.value = ;
      iif(this.Parent.oContained.w_Mag=='May',1,;
      0)
  endfunc

  func oMag_1_64.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oGiu_1_65 as StdCheck with uid="OLHCQFNWTP",rtseq=48,rtrep=.f.,left=580, top=273, caption="Giu",;
    ToolTipText = "Giugno",;
    HelpContextID = 109228186,;
    cFormVar="w_Giu", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGiu_1_65.RadioValue()
    return(iif(this.value =1,'June',;
    space(10)))
  endfunc
  func oGiu_1_65.GetRadio()
    this.Parent.oContained.w_Giu = this.RadioValue()
    return .t.
  endfunc

  func oGiu_1_65.SetRadio()
    this.Parent.oContained.w_Giu=trim(this.Parent.oContained.w_Giu)
    this.value = ;
      iif(this.Parent.oContained.w_Giu=='June',1,;
      0)
  endfunc

  func oGiu_1_65.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oLug_1_66 as StdCheck with uid="VAYQPSRXHY",rtseq=49,rtrep=.f.,left=297, top=300, caption="Lug",;
    ToolTipText = "Luglio",;
    HelpContextID = 109282378,;
    cFormVar="w_Lug", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLug_1_66.RadioValue()
    return(iif(this.value =1,'July',;
    space(10)))
  endfunc
  func oLug_1_66.GetRadio()
    this.Parent.oContained.w_Lug = this.RadioValue()
    return .t.
  endfunc

  func oLug_1_66.SetRadio()
    this.Parent.oContained.w_Lug=trim(this.Parent.oContained.w_Lug)
    this.value = ;
      iif(this.Parent.oContained.w_Lug=='July',1,;
      0)
  endfunc

  func oLug_1_66.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oAgo_1_67 as StdCheck with uid="AHGRZFGFKS",rtseq=50,rtrep=.f.,left=349, top=300, caption="Ago",;
    ToolTipText = "Agosto",;
    HelpContextID = 109253370,;
    cFormVar="w_Ago", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAgo_1_67.RadioValue()
    return(iif(this.value =1,'August',;
    space(10)))
  endfunc
  func oAgo_1_67.GetRadio()
    this.Parent.oContained.w_Ago = this.RadioValue()
    return .t.
  endfunc

  func oAgo_1_67.SetRadio()
    this.Parent.oContained.w_Ago=trim(this.Parent.oContained.w_Ago)
    this.value = ;
      iif(this.Parent.oContained.w_Ago=='August',1,;
      0)
  endfunc

  func oAgo_1_67.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oSet_1_68 as StdCheck with uid="LOPICJVCYI",rtseq=51,rtrep=.f.,left=407, top=300, caption="Set",;
    ToolTipText = "Settembre",;
    HelpContextID = 109233114,;
    cFormVar="w_Set", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSet_1_68.RadioValue()
    return(iif(this.value =1,'September',;
    space(10)))
  endfunc
  func oSet_1_68.GetRadio()
    this.Parent.oContained.w_Set = this.RadioValue()
    return .t.
  endfunc

  func oSet_1_68.SetRadio()
    this.Parent.oContained.w_Set=trim(this.Parent.oContained.w_Set)
    this.value = ;
      iif(this.Parent.oContained.w_Set=='September',1,;
      0)
  endfunc

  func oSet_1_68.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oOtt_1_69 as StdCheck with uid="DUNYJWBVSY",rtseq=52,rtrep=.f.,left=468, top=300, caption="Ott",;
    ToolTipText = "Ottobre",;
    HelpContextID = 109229338,;
    cFormVar="w_Ott", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOtt_1_69.RadioValue()
    return(iif(this.value =1,'October',;
    space(10)))
  endfunc
  func oOtt_1_69.GetRadio()
    this.Parent.oContained.w_Ott = this.RadioValue()
    return .t.
  endfunc

  func oOtt_1_69.SetRadio()
    this.Parent.oContained.w_Ott=trim(this.Parent.oContained.w_Ott)
    this.value = ;
      iif(this.Parent.oContained.w_Ott=='October',1,;
      0)
  endfunc

  func oOtt_1_69.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oNov_1_70 as StdCheck with uid="TEHVJRNEWH",rtseq=53,rtrep=.f.,left=519, top=300, caption="Nov",;
    ToolTipText = "Novembre",;
    HelpContextID = 109222442,;
    cFormVar="w_Nov", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNov_1_70.RadioValue()
    return(iif(this.value =1,'November',;
    space(10)))
  endfunc
  func oNov_1_70.GetRadio()
    this.Parent.oContained.w_Nov = this.RadioValue()
    return .t.
  endfunc

  func oNov_1_70.SetRadio()
    this.Parent.oContained.w_Nov=trim(this.Parent.oContained.w_Nov)
    this.value = ;
      iif(this.Parent.oContained.w_Nov=='November',1,;
      0)
  endfunc

  func oNov_1_70.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oDic_1_71 as StdCheck with uid="BTFGPFWMTA",rtseq=54,rtrep=.f.,left=580, top=300, caption="Dic",;
    ToolTipText = "Dicembre",;
    HelpContextID = 109301962,;
    cFormVar="w_Dic", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDic_1_71.RadioValue()
    return(iif(this.value =1,'December',;
    space(10)))
  endfunc
  func oDic_1_71.GetRadio()
    this.Parent.oContained.w_Dic = this.RadioValue()
    return .t.
  endfunc

  func oDic_1_71.SetRadio()
    this.Parent.oContained.w_Dic=trim(this.Parent.oContained.w_Dic)
    this.value = ;
      iif(this.Parent.oContained.w_Dic=='December',1,;
      0)
  endfunc

  func oDic_1_71.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oSHIMPDAT_1_73 as StdRadio with uid="YOMCNUYRNX",rtseq=55,rtrep=.f.,left=306, top=334, width=243,height=23;
    , ToolTipText = "Impostazione data (fine mese o data fissa)";
    , cFormVar="w_SHIMPDAT", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSHIMPDAT_1_73.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="A fine mese"
      this.Buttons(1).HelpContextID = 46624634
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("A fine mese","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="A data fissa"
      this.Buttons(2).HelpContextID = 46624634
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("A data fissa","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Impostazione data (fine mese o data fissa)")
      StdRadio::init()
    endproc

  func oSHIMPDAT_1_73.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSHIMPDAT_1_73.GetRadio()
    this.Parent.oContained.w_SHIMPDAT = this.RadioValue()
    return .t.
  endfunc

  func oSHIMPDAT_1_73.SetRadio()
    this.Parent.oContained.w_SHIMPDAT=trim(this.Parent.oContained.w_SHIMPDAT)
    this.value = ;
      iif(this.Parent.oContained.w_SHIMPDAT=='F',1,;
      iif(this.Parent.oContained.w_SHIMPDAT=='D',2,;
      0))
  endfunc

  func oSHIMPDAT_1_73.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oSHDATFIS_1_75 as StdField with uid="DHYVZICKOI",rtseq=56,rtrep=.f.,;
    cFormVar = "w_SHDATFIS", cQueryName = "SHDATFIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorno per data fissa",;
    HelpContextID = 184868999,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=622, Top=334, cSayPict="'99'", cGetPict="'99'"

  proc oSHDATFIS_1_75.mDefault
    with this.Parent.oContained
      if empty(.w_SHDATFIS)
        .w_SHDATFIS = 1
      endif
    endwith
  endproc

  func oSHDATFIS_1_75.mHide()
    with this.Parent.oContained
      return (.w_SHIMPDAT='F' or .w_SHFREQ<>'M')
    endwith
  endfunc

  func oSHDATFIS_1_75.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_SHIMPDAT='F' or .w_SHFREQ<>'M' ) Or ( .w_SHDATFIS>0 and .w_SHDATFIS<32 ))
    endwith
    return bRes
  endfunc


  add object oBtn_1_87 as StdButton with uid="LQPQGJIPXL",left=641, top=373, width=48,height=45,;
    CpPicture="bmp\date-time.BMP", caption="", nPag=1;
    , ToolTipText = "Calcolo data prossima esecuzione";
    , HelpContextID = 214939430;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_87.Click()
      with this.Parent.oContained
        gsjb_bde(this.Parent.oContained,i_datsys)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_87.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_SHDATMAN='N')
      endwith
    endif
  endfunc

  add object oSHDATMAN_1_115 as StdCheck with uid="VJCPYNGGTD",rtseq=75,rtrep=.f.,left=57, top=225, caption="Inserimento data manuale",;
    ToolTipText = "Inserimento data manuale",;
    HelpContextID = 201006964,;
    cFormVar="w_SHDATMAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHDATMAN_1_115.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSHDATMAN_1_115.GetRadio()
    this.Parent.oContained.w_SHDATMAN = this.RadioValue()
    return .t.
  endfunc

  func oSHDATMAN_1_115.SetRadio()
    this.Parent.oContained.w_SHDATMAN=trim(this.Parent.oContained.w_SHDATMAN)
    this.value = ;
      iif(this.Parent.oContained.w_SHDATMAN=='S',1,;
      0)
  endfunc

  func oSHDATMAN_1_115.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHFREQ<>'U')
    endwith
   endif
  endfunc

  func oSHDATMAN_1_115.mHide()
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
  endfunc

  add object oDESUTEP_1_117 as StdField with uid="IDZCMWGJBR",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESUTEP", cQueryName = "DESUTEP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 68160310,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=574, Top=7, InputMask=replicate('X',20)

  add object oStr_1_32 as StdString with uid="TJQFWHVZZN",Visible=.t., Left=90, Top=7,;
    Alignment=1, Width=60, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="KGLAQOQOVV",Visible=.t., Left=9, Top=33,;
    Alignment=1, Width=141, Height=18,;
    Caption="Descrizione job:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_SHTIPSCH <> "N")
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="IVDNNOIXIH",Visible=.t., Left=298, Top=69,;
    Alignment=1, Width=53, Height=18,;
    Caption="In data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="FECKGFNFQY",Visible=.t., Left=298, Top=99,;
    Alignment=1, Width=53, Height=18,;
    Caption="In data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="GLKYLWNDRF",Visible=.t., Left=292, Top=139,;
    Alignment=0, Width=195, Height=18,;
    Caption="Frequenza lancio"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="DWYXIAHOJP",Visible=.t., Left=7, Top=139,;
    Alignment=0, Width=195, Height=18,;
    Caption="Prossima esecuzione"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="HBQLCTROQI",Visible=.t., Left=314, Top=210,;
    Alignment=1, Width=49, Height=18,;
    Caption="Ogni:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ$"M-U-R")
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="SXUQDDWKOC",Visible=.t., Left=544, Top=211,;
    Alignment=1, Width=43, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="PQECCYDMGN",Visible=.t., Left=305, Top=188,;
    Alignment=0, Width=154, Height=18,;
    Caption="Giornaliero"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'G')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="TJBGDUHZXL",Visible=.t., Left=305, Top=188,;
    Alignment=0, Width=154, Height=18,;
    Caption="Settimanale"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'S')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="KAPISTXDDQ",Visible=.t., Left=305, Top=188,;
    Alignment=0, Width=154, Height=18,;
    Caption="Mensile"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ<>'M')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="MAYTRGUFMD",Visible=.t., Left=24, Top=289,;
    Alignment=1, Width=58, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="LDQSRJGLNU",Visible=.t., Left=553, Top=334,;
    Alignment=1, Width=63, Height=18,;
    Caption="Il giorno:"  ;
  , bGlobalFont=.t.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (.w_SHIMPDAT='F' or .w_SHFREQ<>'M')
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="XKOOIABPSN",Visible=.t., Left=621, Top=211,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="FBSHNNKBUF",Visible=.t., Left=15, Top=69,;
    Alignment=1, Width=78, Height=18,;
    Caption="Creato da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="YKSCRYLOBJ",Visible=.t., Left=15, Top=99,;
    Alignment=1, Width=78, Height=18,;
    Caption="Variato da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="ASMOYRELJR",Visible=.t., Left=13, Top=165,;
    Alignment=1, Width=62, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="MGQCGNCREU",Visible=.t., Left=161, Top=165,;
    Alignment=1, Width=43, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="SOXFQXPXVN",Visible=.t., Left=24, Top=316,;
    Alignment=1, Width=58, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="ADFWHUNHZI",Visible=.t., Left=7, Top=258,;
    Alignment=0, Width=195, Height=18,;
    Caption="Dati elaborazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="QTHDESGIRE",Visible=.t., Left=14, Top=377,;
    Alignment=1, Width=49, Height=18,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="NBUVZOFPKN",Visible=.t., Left=7, Top=353,;
    Alignment=0, Width=195, Height=18,;
    Caption="Stampa PDF"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="KJLXFXAFKT",Visible=.t., Left=170, Top=289,;
    Alignment=1, Width=43, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="FPTUXXXCFY",Visible=.t., Left=170, Top=316,;
    Alignment=1, Width=43, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="PAHEMEAEZW",Visible=.t., Left=14, Top=199,;
    Alignment=1, Width=191, Height=18,;
    Caption="Tempo massimo di attesa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="GKOVJQRSHV",Visible=.t., Left=232, Top=201,;
    Alignment=1, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="GLUEHPMDQG",Visible=.t., Left=314, Top=247,;
    Alignment=1, Width=49, Height=18,;
    Caption="Ogni:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ='U')
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="IGTCSUHVFR",Visible=.t., Left=406, Top=246,;
    Alignment=0, Width=47, Height=18,;
    Caption="minuti"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ='U')
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="WTNLKXKDRN",Visible=.t., Left=515, Top=245,;
    Alignment=1, Width=72, Height=18,;
    Caption="Fino alle:"  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ$"U-R")
    endwith
  endfunc

  add object oStr_1_96 as StdString with uid="ECWVOGZKSV",Visible=.t., Left=621, Top=245,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_96.mHide()
    with this.Parent.oContained
      return (.w_SHFREQ$"U-R")
    endwith
  endfunc

  add object oStr_1_107 as StdString with uid="CSXQERPYYV",Visible=.t., Left=279, Top=408,;
    Alignment=1, Width=164, Height=18,;
    Caption="Invio stampe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="MYRAWNQWZE",Visible=.t., Left=3, Top=33,;
    Alignment=1, Width=147, Height=18,;
    Caption="Descrizione proattivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_108.mHide()
    with this.Parent.oContained
      return (.w_SHTIPSCH = "N")
    endwith
  endfunc

  add object oStr_1_116 as StdString with uid="KGVWOPYFJM",Visible=.t., Left=402, Top=7,;
    Alignment=1, Width=113, Height=18,;
    Caption="Utente preferenziale:"  ;
  , bGlobalFont=.t.

  add object oBox_1_38 as StdBox with uid="DHXULWZWZT",left=292, top=156, width=418,height=203

  add object oBox_1_48 as StdBox with uid="RFASQMSYOG",left=7, top=275, width=269,height=71

  add object oBox_1_50 as StdBox with uid="MJKXXIUYAO",left=7, top=156, width=269,height=96

  add object oBox_1_72 as StdBox with uid="ISJGGKWPWI",left=293, top=327, width=414,height=1

  add object oBox_1_77 as StdBox with uid="WOQTJTFRBY",left=7, top=61, width=435,height=68

  add object oBox_1_86 as StdBox with uid="FARURLYNKO",left=7, top=373, width=596,height=61
enddefine
define class tgsjb_ashPag2 as StdContainer
  Width  = 735
  height = 437
  stdWidth  = 735
  stdheight = 437
  resizeXpos=401
  resizeYpos=226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="EJSEAKPGYD",left=6, top=11, width=727, height=395, bOnScreen=.t.;

enddefine
define class tgsjb_ashPag3 as StdContainer
  Width  = 735
  height = 437
  stdWidth  = 735
  stdheight = 437
  resizeXpos=496
  resizeYpos=249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="PADJQPJLYG",left=10, top=16, width=717, height=404, bOnScreen=.t.;


  add object oStr_3_2 as StdString with uid="KHIVBSZDPQ",Visible=.t., Left=5, Top=439,;
    Alignment=0, Width=637, Height=18,;
    Caption="ATTENZIONE SE L'OGGETTO CHILD VIENE SPOSTATO IN UN ALTRA PAGINA BISOGNA MODIFICARE"  ;
  , bGlobalFont=.t.

  func oStr_3_2.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_3_3 as StdString with uid="YOUYRGTPOA",Visible=.t., Left=5, Top=455,;
    Alignment=0, Width=564, Height=18,;
    Caption="Nel blocco notify event init la riga this.opgfrm.pages[3].opag.uienable(.T.)"  ;
  , bGlobalFont=.t.

  func oStr_3_3.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_3_4 as StdString with uid="IEFOPXSHML",Visible=.t., Left=5, Top=469,;
    Alignment=0, Width=240, Height=18,;
    Caption="MODIFICARE ANCHE IL BATCH GSJB_BMS"  ;
  , bGlobalFont=.t.

  func oStr_3_4.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc
enddefine
define class tgsjb_ashPag4 as StdContainer
  Width  = 735
  height = 437
  stdWidth  = 735
  stdheight = 437
  resizeXpos=568
  resizeYpos=301
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSHTIPELA_4_1 as StdCombo with uid="XZONLCMPPP",rtseq=59,rtrep=.f.,left=196,top=12,width=146,height=21;
    , ToolTipText = "Tipo elaborazione (batch o query) utilizzato per produrre il cursore utilizzato per notificare l'esito del controllo";
    , HelpContextID = 63184743;
    , cFormVar="w_SHTIPELA",RowSource=""+"Query,"+"Batch", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oSHTIPELA_4_1.RadioValue()
    return(iif(this.value =1,'Q',;
    iif(this.value =2,'B',;
    space(1))))
  endfunc
  func oSHTIPELA_4_1.GetRadio()
    this.Parent.oContained.w_SHTIPELA = this.RadioValue()
    return .t.
  endfunc

  func oSHTIPELA_4_1.SetRadio()
    this.Parent.oContained.w_SHTIPELA=trim(this.Parent.oContained.w_SHTIPELA)
    this.value = ;
      iif(this.Parent.oContained.w_SHTIPELA=='Q',1,;
      iif(this.Parent.oContained.w_SHTIPELA=='B',2,;
      0))
  endfunc

  add object oSHELABOR_4_4 as StdField with uid="WKOZGJJCAP",rtseq=60,rtrep=.f.,;
    cFormVar = "w_SHELABOR", cQueryName = "SHELABOR",;
    bObbl = .f. , nPag = 4, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Batch o query utilizzato per produrre il cursore utilizzato per notificare il messaggio",;
    HelpContextID = 265695096,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=196, Top=40, InputMask=replicate('X',250)


  add object oBtn_4_5 as StdButton with uid="OCNGIMWSVS",left=698, top=40, width=23,height=22,;
    caption="...", nPag=4;
    , ToolTipText = "Selezione query";
    , HelpContextID = 109534506;
  , bGlobalFont=.t.

    proc oBtn_4_5.Click()
      with this.Parent.oContained
        .w_SHELABOR = SYS( 2014 , GETFILE( "VQR" ) )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SHTIPELA <> "Q")
     endwith
    endif
  endfunc


  add object oSHTIPRES_4_6 as StdCombo with uid="CUTWJWSAYS",rtseq=61,rtrep=.f.,left=196,top=68,width=146,height=21;
    , ToolTipText = "Condizione di produzione del responso (ad ogni esecuzione, al verificarsi dell'evento)";
    , HelpContextID = 255582343;
    , cFormVar="w_SHTIPRES",RowSource=""+"Esecuzione,"+"Evento", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oSHTIPRES_4_6.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oSHTIPRES_4_6.GetRadio()
    this.Parent.oContained.w_SHTIPRES = this.RadioValue()
    return .t.
  endfunc

  func oSHTIPRES_4_6.SetRadio()
    this.Parent.oContained.w_SHTIPRES=trim(this.Parent.oContained.w_SHTIPRES)
    this.value = ;
      iif(this.Parent.oContained.w_SHTIPRES=='X',1,;
      iif(this.Parent.oContained.w_SHTIPRES=='E',2,;
      0))
  endfunc


  add object oSHFRMRES_4_7 as StdCombo with uid="XDLFQPBLTI",rtseq=62,rtrep=.f.,left=545,top=68,width=146,height=21;
    , ToolTipText = "Formato dell'elaborazione selezionata (allegato/testo)";
    , HelpContextID = 258195591;
    , cFormVar="w_SHFRMRES",RowSource=""+"Testo,"+"Allegato", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oSHFRMRES_4_7.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oSHFRMRES_4_7.GetRadio()
    this.Parent.oContained.w_SHFRMRES = this.RadioValue()
    return .t.
  endfunc

  func oSHFRMRES_4_7.SetRadio()
    this.Parent.oContained.w_SHFRMRES=trim(this.Parent.oContained.w_SHFRMRES)
    this.value = ;
      iif(this.Parent.oContained.w_SHFRMRES=='T',1,;
      iif(this.Parent.oContained.w_SHFRMRES=='A',2,;
      0))
  endfunc

  add object oSHREPRES_4_9 as StdField with uid="THUAIDFJGV",rtseq=63,rtrep=.f.,;
    cFormVar = "w_SHREPRES", cQueryName = "SHREPRES",;
    bObbl = .f. , nPag = 4, value=space(250), bMultilanguage =  .f.,;
    HelpContextID = 255852679,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=196, Top=94, InputMask=replicate('X',250)

  func oSHREPRES_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHFRMRES = "A")
    endwith
   endif
  endfunc

  func oSHREPRES_4_9.mHide()
    with this.Parent.oContained
      return (.w_SHFRMRES <> "A")
    endwith
  endfunc


  add object oBtn_4_10 as StdButton with uid="FRUQGEEGOE",left=698, top=94, width=23,height=22,;
    caption="...", nPag=4;
    , ToolTipText = "Selezione report";
    , HelpContextID = 109534506;
  , bGlobalFont=.t.

    proc oBtn_4_10.Click()
      with this.Parent.oContained
        .w_SHREPRES = SYS( 2014 , GETFILE( "FRX" ) )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SHFRMRES <> "A")
     endwith
    endif
  endfunc

  add object oSHTXTHEA_4_13 as StdMemo with uid="XHRHHUCGHT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_SHTXTHEA", cQueryName = "SHTXTHEA",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Intestazione del messaggio restituito dall'elaborazione",;
    HelpContextID = 149741721,;
   bGlobalFont=.t.,;
    Height=69, Width=497, Left=196, Top=95

  func oSHTXTHEA_4_13.mHide()
    with this.Parent.oContained
      return (.w_SHFRMRES <> "T")
    endwith
  endfunc

  add object oSHTXTBOD_4_14 as StdMemo with uid="MRMOHHFMTS",rtseq=65,rtrep=.f.,;
    cFormVar = "w_SHTXTBOD", cQueryName = "SHTXTBOD",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Dettaglio del messaggio restituito dall'elaborazione ripetuto per ogni riga",;
    HelpContextID = 18030442,;
   bGlobalFont=.t.,;
    Height=69, Width=497, Left=196, Top=171

  func oSHTXTBOD_4_14.mHide()
    with this.Parent.oContained
      return (.w_SHFRMRES <> "T")
    endwith
  endfunc

  add object oSHTXTFOO_4_15 as StdMemo with uid="GOMGVTSMPU",rtseq=66,rtrep=.f.,;
    cFormVar = "w_SHTXTFOO", cQueryName = "SHTXTFOO",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Piede del messaggio restituito dall'elaborazione",;
    HelpContextID = 85139317,;
   bGlobalFont=.t.,;
    Height=69, Width=497, Left=196, Top=247

  func oSHTXTFOO_4_15.mHide()
    with this.Parent.oContained
      return (.w_SHFRMRES <> "T")
    endwith
  endfunc


  add object oSHAGENDA_4_19 as StdCombo with uid="UPHISPBBNG",rtseq=67,rtrep=.f.,left=196,top=325,width=216,height=21;
    , ToolTipText = "Modalit� di invio messaggio ai destinatari nella query/batch il cui codice viene definito nel campo 'Identificazione destinatario'";
    , HelpContextID = 65999001;
    , cFormVar="w_SHAGENDA",RowSource=""+"Nessuna modalit�,"+"A mezzo post-in,"+"A mezzo e-mail,"+"Post-in per gestione attivit�", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oSHAGENDA_4_19.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'M',;
    iif(this.value =4,'Q',;
    space(1))))))
  endfunc
  func oSHAGENDA_4_19.GetRadio()
    this.Parent.oContained.w_SHAGENDA = this.RadioValue()
    return .t.
  endfunc

  func oSHAGENDA_4_19.SetRadio()
    this.Parent.oContained.w_SHAGENDA=trim(this.Parent.oContained.w_SHAGENDA)
    this.value = ;
      iif(this.Parent.oContained.w_SHAGENDA=='N',1,;
      iif(this.Parent.oContained.w_SHAGENDA=='P',2,;
      iif(this.Parent.oContained.w_SHAGENDA=='M',3,;
      iif(this.Parent.oContained.w_SHAGENDA=='Q',4,;
      0))))
  endfunc

  add object oSHDESTIN_4_20 as StdField with uid="LEDKOOZPUQ",rtseq=68,rtrep=.f.,;
    cFormVar = "w_SHDESTIN", cQueryName = "SHDESTIN",;
    bObbl = .f. , nPag = 4, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Nome del campo nel cursore elaborato, in cui si trova il codice dell'utente a cui inviare il post-in o l'indirizzo a cui inviare l'email",;
    HelpContextID = 219209868,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=196, Top=356, InputMask=replicate('X',250)

  func oSHDESTIN_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHAGENDA <> "N")
    endwith
   endif
  endfunc

  add object oSHFINPRE_4_24 as StdField with uid="BINAFGSOPB",rtseq=69,rtrep=.f.,;
    cFormVar = "w_SHFINPRE", cQueryName = "SHFINPRE",;
    bObbl = .f. , nPag = 4, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Filtro iniziale della prossima elaborazione",;
    HelpContextID = 245579627,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=196, Top=384

  func oSHFINPRE_4_24.mHide()
    with this.Parent.oContained
      return (! cp_isadministrator())
    endwith
  endfunc

  add object oStr_4_2 as StdString with uid="BBJGHDYMLN",Visible=.t., Left=9, Top=14,;
    Alignment=1, Width=179, Height=18,;
    Caption="Tipo di elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_3 as StdString with uid="NFQERTZPFU",Visible=.t., Left=9, Top=42,;
    Alignment=1, Width=179, Height=18,;
    Caption="Elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="JNLUQIWKHS",Visible=.t., Left=350, Top=69,;
    Alignment=1, Width=187, Height=18,;
    Caption="Formato elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="QRSDDOONQQ",Visible=.t., Left=9, Top=98,;
    Alignment=1, Width=179, Height=18,;
    Caption="Report:"  ;
  , bGlobalFont=.t.

  func oStr_4_11.mHide()
    with this.Parent.oContained
      return (.w_SHFRMRES <> "A")
    endwith
  endfunc

  add object oStr_4_12 as StdString with uid="HKGYMZCVMI",Visible=.t., Left=9, Top=69,;
    Alignment=1, Width=179, Height=18,;
    Caption="Occorrenza elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="VMTCISYOSR",Visible=.t., Left=9, Top=98,;
    Alignment=1, Width=179, Height=18,;
    Caption="Intestazione:"  ;
  , bGlobalFont=.t.

  func oStr_4_16.mHide()
    with this.Parent.oContained
      return (.w_SHFRMRES <> "T")
    endwith
  endfunc

  add object oStr_4_17 as StdString with uid="MEPDGMYKEF",Visible=.t., Left=9, Top=170,;
    Alignment=1, Width=179, Height=18,;
    Caption="Dettaglio:"  ;
  , bGlobalFont=.t.

  func oStr_4_17.mHide()
    with this.Parent.oContained
      return (.w_SHFRMRES <> "T")
    endwith
  endfunc

  add object oStr_4_18 as StdString with uid="DPXUKIEVGP",Visible=.t., Left=9, Top=244,;
    Alignment=1, Width=179, Height=18,;
    Caption="Piede:"  ;
  , bGlobalFont=.t.

  func oStr_4_18.mHide()
    with this.Parent.oContained
      return (.w_SHFRMRES <> "T")
    endwith
  endfunc

  add object oStr_4_21 as StdString with uid="ZGNJDGBMEX",Visible=.t., Left=9, Top=359,;
    Alignment=1, Width=179, Height=18,;
    Caption="Identificazione destinatario:"  ;
  , bGlobalFont=.t.

  add object oStr_4_22 as StdString with uid="FSGYKKLFNL",Visible=.t., Left=9, Top=329,;
    Alignment=1, Width=179, Height=18,;
    Caption="Smistamento dati:"  ;
  , bGlobalFont=.t.

  add object oStr_4_23 as StdString with uid="CYLZHEQBQS",Visible=.t., Left=9, Top=387,;
    Alignment=1, Width=179, Height=18,;
    Caption="Prossimo filtro iniziale:"  ;
  , bGlobalFont=.t.

  func oStr_4_23.mHide()
    with this.Parent.oContained
      return (! cp_isadministrator())
    endwith
  endfunc
enddefine
define class tgsjb_ashPag5 as StdContainer
  Width  = 735
  height = 437
  stdWidth  = 735
  stdheight = 437
  resizeXpos=458
  resizeYpos=293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object AZIPROCE as cp_szoombox with uid="PMULFPYZWA",left=43, top=40, width=522,height=322,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSJB_KA2",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,cTable="LISTAJOB",cMenuFile="",cZoomOnZoom="",bQueryOnLoad=.t.,bRetriveAllRows=.f.,;
    cEvent = "Init,Load,New record",;
    nPag=5;
    , ToolTipText = "Aziende su cui � attivata la proattivit�";
    , HelpContextID = 68262118

  add object oSELEZI_5_3 as StdRadio with uid="SAFCTZGRIT",rtseq=73,rtrep=.f.,left=53, top=372, width=202,height=40;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=5;
  , bGlobalFont=.t.

    proc oSELEZI_5_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 140483622
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 140483622
      this.Buttons(2).Top=19
      this.SetAll("Width",200)
      this.SetAll("Height",21)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_5_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_5_3.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_5_3.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  add object oStr_5_2 as StdString with uid="ZXKPCGKRAL",Visible=.t., Left=63, Top=16,;
    Alignment=0, Width=487, Height=18,;
    Caption="Elenco aziende in cui � attivata la proattivit�:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsjb_ashPag6 as StdContainer
  Width  = 735
  height = 437
  stdWidth  = 735
  stdheight = 437
  resizeXpos=413
  resizeYpos=319
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_6_1 as StdButton with uid="DGNPSKWQAW",left=678, top=5, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per aggiornare l'elenco code di lavoro";
    , HelpContextID = 47541607;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_1.Click()
      with this.Parent.oContained
        .notifyevent("Esegui")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomLog as cp_zoombox with uid="YOFKCCFACF",left=2, top=53, width=452,height=166,;
    caption='ZoomLog',;
   bGlobalFont=.t.,;
    cTable="LOGELAJB",cZoomFile="LogJob",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bRetriveAllRows=.f.,cZoomOnZoom="GSJB_MLG",cMenuFile="",;
    cEvent = "Esegui",;
    nPag=6;
    , ToolTipText = "Elenco log di elaborazione";
    , HelpContextID = 39192938

  add object oMEMO_6_3 as StdMemo with uid="RBAWVXYIRP",rtseq=70,rtrep=.f.,;
    cFormVar = "w_MEMO", cQueryName = "MEMO",;
    bObbl = .f. , nPag = 6, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Informazioni sul processo",;
    HelpContextID = 104223802,;
   bGlobalFont=.t.,;
    Height=134, Width=727, Left=2, Top=242, readonly=.t.


  add object oBtn_6_6 as StdButton with uid="PFGJOCUVTN",left=6, top=380, width=48,height=45,;
    CpPicture="bmp\log.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per accedere al log di elaborazione";
    , HelpContextID = 109283914;
    , Caption='\<Log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_6.Click()
      with this.Parent.oContained
        gsjb_bsl(this.Parent.oContained,"L",.w_LOG)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_7 as StdButton with uid="RLGNBYGPTA",left=57, top=380, width=48,height=45,;
    CpPicture="bmp\stampa.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per accedere alle stampe di elaborazione";
    , HelpContextID = 102605643;
    , Caption='\<Stampe';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_7.Click()
      do GSJB_KJS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomProc as cp_zoombox with uid="IQJZVLUNFD",left=456, top=53, width=276,height=166,;
    caption='ZoomProc',;
   bGlobalFont=.t.,;
    bOptions=.f.,cZoomFile="LogProcessi",cTable="LOGELAJB",bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Init",;
    nPag=6;
    , ToolTipText = "Elenco processi";
    , HelpContextID = 15333113

  add object oStr_6_4 as StdString with uid="PHPGHSMAES",Visible=.t., Left=6, Top=31,;
    Alignment=0, Width=298, Height=18,;
    Caption="Elenco log"  ;
  , bGlobalFont=.t.

  add object oStr_6_5 as StdString with uid="AQLYKFPKAH",Visible=.t., Left=6, Top=222,;
    Alignment=0, Width=298, Height=18,;
    Caption="Messaggio di log"  ;
  , bGlobalFont=.t.

  add object oStr_6_10 as StdString with uid="WHYUFNUTUE",Visible=.t., Left=458, Top=31,;
    Alignment=0, Width=215, Height=18,;
    Caption="Elenco processi"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_ash','SCHEDJOB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SHCODICE=SCHEDJOB.SHCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
