* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bpr                                                        *
*              Selezione parametro                                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-08                                                      *
* Last revis.: 2004-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bpr",oParentObject)
return(i_retval)

define class tgsjb_bpr as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_GSJB_MPR = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo il campo valore parametro con la variabile selezionata e 
    *     chiudo la maschera
    this.w_PADRE = This.oParentObject
    this.w_GSJB_MPR = this.w_PADRE.oParentObject
    * --- Valorizzo il dettaglio (GetBodyCtrl non funziona correttamente su figli integrati)
    this.w_GSJB_MPR.w_PRVALVAR = this.oParentObject.w_PARPAS
    * --- Aggiorno il controllo..
    this.w_GSJB_MPR.SetControlsValue()     
    this.w_PADRE.ecpSave()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
