* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bdl                                                        *
*              Cancellazione log                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-28                                                      *
* Last revis.: 2006-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bdl",oParentObject)
return(i_retval)

define class tgsjb_bdl as StdBatch
  * --- Local variables
  w_LSCODJOB = space(10)
  * --- WorkFile variables
  LOGELAJB_idx=0
  JOBSTAMP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcellazione log e stampe job
    this.w_LSCODJOB = this.oParentObject.w_SHCODICE
    * --- Delete from JOBSTAMP
    i_nConn=i_TableProp[this.JOBSTAMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.JOBSTAMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".STLSSER = "+i_cQueryTable+".STLSSER";
    
      do vq_exec with '..\jbsh\exe\query\GSJB_BDL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from LOGELAJB
    i_nConn=i_TableProp[this.LOGELAJB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='LSSERIAL'
      cp_CreateTempTable(i_nConn,i_cTempTable,"LSSERIAL "," from "+i_cQueryTable+" where LSCODJOB="+cp_ToStrODBC(this.w_LSCODJOB)+"",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".LSSERIAL = "+i_cQueryTable+".LSSERIAL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".LSSERIAL = "+i_cQueryTable+".LSSERIAL";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LOGELAJB'
    this.cWorkTables[2]='JOBSTAMP'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
