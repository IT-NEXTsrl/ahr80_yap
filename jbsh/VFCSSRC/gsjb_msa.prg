* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_msa                                                        *
*              Destinatari                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-01                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsjb_msa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsjb_msa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsjb_msa")
  return

* --- Class definition
define class tgsjb_msa as StdPCForm
  Width  = 730
  Height = 397
  Top    = 5
  Left   = 4
  cComment = "Destinatari"
  cPrg = "gsjb_msa"
  HelpContextID=97152617
  add object cnt as tcgsjb_msa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsjb_msa as PCContext
  w_UGSHJOB = 0
  w_SHFLINVS = space(1)
  w_CPROWORD = 0
  w_UGTIPFIL = space(1)
  w_UGCODUTE = 0
  w_UGCODGRU = 0
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_DESUTEGRU = space(20)
  w_UGTIPINV = space(1)
  w_UGTIPMES = space(1)
  proc Save(i_oFrom)
    this.w_UGSHJOB = i_oFrom.w_UGSHJOB
    this.w_SHFLINVS = i_oFrom.w_SHFLINVS
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_UGTIPFIL = i_oFrom.w_UGTIPFIL
    this.w_UGCODUTE = i_oFrom.w_UGCODUTE
    this.w_UGCODGRU = i_oFrom.w_UGCODGRU
    this.w_DESUTE = i_oFrom.w_DESUTE
    this.w_DESGRU = i_oFrom.w_DESGRU
    this.w_DESUTEGRU = i_oFrom.w_DESUTEGRU
    this.w_UGTIPINV = i_oFrom.w_UGTIPINV
    this.w_UGTIPMES = i_oFrom.w_UGTIPMES
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_UGSHJOB = this.w_UGSHJOB
    i_oTo.w_SHFLINVS = this.w_SHFLINVS
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_UGTIPFIL = this.w_UGTIPFIL
    i_oTo.w_UGCODUTE = this.w_UGCODUTE
    i_oTo.w_UGCODGRU = this.w_UGCODGRU
    i_oTo.w_DESUTE = this.w_DESUTE
    i_oTo.w_DESGRU = this.w_DESGRU
    i_oTo.w_DESUTEGRU = this.w_DESUTEGRU
    i_oTo.w_UGTIPINV = this.w_UGTIPINV
    i_oTo.w_UGTIPMES = this.w_UGTIPMES
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsjb_msa as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 730
  Height = 397
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=97152617
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  UGSHJOB_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  cFile = "UGSHJOB"
  cKeySelect = "UGSHJOB"
  cKeyWhere  = "UGSHJOB=this.w_UGSHJOB"
  cKeyDetail  = "UGSHJOB=this.w_UGSHJOB"
  cKeyWhereODBC = '"UGSHJOB="+cp_ToStrODBC(this.w_UGSHJOB)';

  cKeyDetailWhereODBC = '"UGSHJOB="+cp_ToStrODBC(this.w_UGSHJOB)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"UGSHJOB.UGSHJOB="+cp_ToStrODBC(this.w_UGSHJOB)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'UGSHJOB.CPROWORD '
  cPrg = "gsjb_msa"
  cComment = "Destinatari"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 19
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_UGSHJOB = 0
  w_SHFLINVS = space(1)
  o_SHFLINVS = space(1)
  w_CPROWORD = 0
  w_UGTIPFIL = space(1)
  o_UGTIPFIL = space(1)
  w_UGCODUTE = 0
  w_UGCODGRU = 0
  w_DESUTE = space(20)
  w_DESGRU = space(20)
  w_DESUTEGRU = space(20)
  w_UGTIPINV = space(1)
  w_UGTIPMES = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsjb_msaPag1","gsjb_msa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='UGSHJOB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UGSHJOB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UGSHJOB_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsjb_msa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from UGSHJOB where UGSHJOB=KeySet.UGSHJOB
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.UGSHJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UGSHJOB_IDX,2],this.bLoadRecFilter,this.UGSHJOB_IDX,"gsjb_msa")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UGSHJOB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UGSHJOB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UGSHJOB '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UGSHJOB',this.w_UGSHJOB  )
      select * from (i_cTable) UGSHJOB where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_UGSHJOB = NVL(UGSHJOB,0)
        .w_SHFLINVS = .oParentObject .w_SHFLINVS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'UGSHJOB')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESUTE = space(20)
          .w_DESGRU = space(20)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_UGTIPFIL = NVL(UGTIPFIL,space(1))
          .w_UGCODUTE = NVL(UGCODUTE,0)
          .link_2_3('Load')
          .w_UGCODGRU = NVL(UGCODGRU,0)
          .link_2_4('Load')
        .w_DESUTEGRU = IIF(.w_UGTIPFIL='U',.w_DESUTE,.w_DESGRU)
          .w_UGTIPINV = NVL(UGTIPINV,space(1))
          .w_UGTIPMES = NVL(UGTIPMES,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_SHFLINVS = .oParentObject .w_SHFLINVS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_UGSHJOB=0
      .w_SHFLINVS=space(1)
      .w_CPROWORD=10
      .w_UGTIPFIL=space(1)
      .w_UGCODUTE=0
      .w_UGCODGRU=0
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_DESUTEGRU=space(20)
      .w_UGTIPINV=space(1)
      .w_UGTIPMES=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_SHFLINVS = .oParentObject .w_SHFLINVS
        .DoRTCalc(3,4,.f.)
        .w_UGCODUTE = IIF(.w_UGTIPFIL='U',.w_UGCODUTE, 0)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_UGCODUTE))
         .link_2_3('Full')
        endif
        .w_UGCODGRU = IIF(.w_UGTIPFIL='G',.w_UGCODGRU, 0)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_UGCODGRU))
         .link_2_4('Full')
        endif
        .DoRTCalc(7,8,.f.)
        .w_DESUTEGRU = IIF(.w_UGTIPFIL='U',.w_DESUTE,.w_DESGRU)
        .w_UGTIPINV = "M"
        .w_UGTIPMES = IIF( .w_SHFLINVS = "N" , "L" , "E" )
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'UGSHJOB')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'UGSHJOB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UGSHJOB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UGSHJOB,"UGSHJOB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_UGTIPFIL N(3);
      ,t_UGCODUTE N(4);
      ,t_UGCODGRU N(4);
      ,t_DESUTEGRU C(20);
      ,t_UGTIPINV N(3);
      ,t_UGTIPMES N(3);
      ,CPROWNUM N(10);
      ,t_DESUTE C(20);
      ,t_DESGRU C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsjb_msabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPFIL_2_2.controlsource=this.cTrsName+'.t_UGTIPFIL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUGCODUTE_2_3.controlsource=this.cTrsName+'.t_UGCODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUGCODGRU_2_4.controlsource=this.cTrsName+'.t_UGCODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_7.controlsource=this.cTrsName+'.t_DESUTEGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPINV_2_8.controlsource=this.cTrsName+'.t_UGTIPINV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPMES_2_9.controlsource=this.cTrsName+'.t_UGTIPMES'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(54)
    this.AddVLine(128)
    this.AddVLine(186)
    this.AddVLine(246)
    this.AddVLine(517)
    this.AddVLine(582)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UGSHJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UGSHJOB_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UGSHJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UGSHJOB_IDX,2])
      *
      * insert into UGSHJOB
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UGSHJOB')
        i_extval=cp_InsertValODBCExtFlds(this,'UGSHJOB')
        i_cFldBody=" "+;
                  "(UGSHJOB,CPROWORD,UGTIPFIL,UGCODUTE,UGCODGRU"+;
                  ",UGTIPINV,UGTIPMES,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_UGSHJOB)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_UGTIPFIL)+","+cp_ToStrODBCNull(this.w_UGCODUTE)+","+cp_ToStrODBCNull(this.w_UGCODGRU)+;
             ","+cp_ToStrODBC(this.w_UGTIPINV)+","+cp_ToStrODBC(this.w_UGTIPMES)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UGSHJOB')
        i_extval=cp_InsertValVFPExtFlds(this,'UGSHJOB')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'UGSHJOB',this.w_UGSHJOB)
        INSERT INTO (i_cTable) (;
                   UGSHJOB;
                  ,CPROWORD;
                  ,UGTIPFIL;
                  ,UGCODUTE;
                  ,UGCODGRU;
                  ,UGTIPINV;
                  ,UGTIPMES;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_UGSHJOB;
                  ,this.w_CPROWORD;
                  ,this.w_UGTIPFIL;
                  ,this.w_UGCODUTE;
                  ,this.w_UGCODGRU;
                  ,this.w_UGTIPINV;
                  ,this.w_UGTIPMES;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.UGSHJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UGSHJOB_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_UGCODUTE)) or not(Empty(t_UGCODGRU))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'UGSHJOB')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'UGSHJOB')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_UGCODUTE)) or not(Empty(t_UGCODGRU))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update UGSHJOB
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'UGSHJOB')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",UGTIPFIL="+cp_ToStrODBC(this.w_UGTIPFIL)+;
                     ",UGCODUTE="+cp_ToStrODBCNull(this.w_UGCODUTE)+;
                     ",UGCODGRU="+cp_ToStrODBCNull(this.w_UGCODGRU)+;
                     ",UGTIPINV="+cp_ToStrODBC(this.w_UGTIPINV)+;
                     ",UGTIPMES="+cp_ToStrODBC(this.w_UGTIPMES)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'UGSHJOB')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,UGTIPFIL=this.w_UGTIPFIL;
                     ,UGCODUTE=this.w_UGCODUTE;
                     ,UGCODGRU=this.w_UGCODGRU;
                     ,UGTIPINV=this.w_UGTIPINV;
                     ,UGTIPMES=this.w_UGTIPMES;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UGSHJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UGSHJOB_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_UGCODUTE)) or not(Empty(t_UGCODGRU))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete UGSHJOB
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_UGCODUTE)) or not(Empty(t_UGCODGRU))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UGSHJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UGSHJOB_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_SHFLINVS = .oParentObject .w_SHFLINVS
        .DoRTCalc(3,4,.t.)
        if .o_UGTIPFIL<>.w_UGTIPFIL
          .w_UGCODUTE = IIF(.w_UGTIPFIL='U',.w_UGCODUTE, 0)
          .link_2_3('Full')
        endif
        if .o_UGTIPFIL<>.w_UGTIPFIL
          .w_UGCODGRU = IIF(.w_UGTIPFIL='G',.w_UGCODGRU, 0)
          .link_2_4('Full')
        endif
        .DoRTCalc(7,8,.t.)
          .w_DESUTEGRU = IIF(.w_UGTIPFIL='U',.w_DESUTE,.w_DESGRU)
        .DoRTCalc(10,10,.t.)
        if .o_SHFLINVS<>.w_SHFLINVS
          .w_UGTIPMES = IIF( .w_SHFLINVS = "N" , "L" , "E" )
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESUTE with this.w_DESUTE
      replace t_DESGRU with this.w_DESGRU
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oUGCODUTE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oUGCODUTE_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oUGCODGRU_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oUGCODGRU_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oUGTIPMES_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oUGTIPMES_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsjb_msa
    IF cEVENT = "w_UGTIPINV Changed"
        IF VARTYPE( i_bDISABLEPOSTIN ) <> "U"
            IF i_bDISABLEPOSTIN AND this.w_UGTIPINV = "P"
              cp_errormsg( "Per attivare la funzionalit� occorre abilitare i post-in" , "!" )
            ENDIF
        ENDIF
    ENDIF
    
    IF cEVENT = "Blank"
        this.W_UGSHJOB = this.oParentObject.W_SHCODICE
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UGCODUTE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UGCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UGCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UGCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UGCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oUGCODUTE_2_3'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UGCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UGCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UGCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UGCODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UGCODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UGCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UGCODGRU
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UGCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UGCODGRU);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UGCODGRU)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UGCODGRU) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oUGCODGRU_2_4'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UGCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UGCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UGCODGRU)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UGCODGRU = NVL(_Link_.CODE,0)
      this.w_DESGRU = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UGCODGRU = 0
      endif
      this.w_DESGRU = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UGCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPFIL_2_2.RadioValue()==this.w_UGTIPFIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPFIL_2_2.SetRadio()
      replace t_UGTIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPFIL_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGCODUTE_2_3.value==this.w_UGCODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGCODUTE_2_3.value=this.w_UGCODUTE
      replace t_UGCODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGCODUTE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGCODGRU_2_4.value==this.w_UGCODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGCODGRU_2_4.value=this.w_UGCODGRU
      replace t_UGCODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGCODGRU_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_7.value==this.w_DESUTEGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_7.value=this.w_DESUTEGRU
      replace t_DESUTEGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESUTEGRU_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPINV_2_8.RadioValue()==this.w_UGTIPINV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPINV_2_8.SetRadio()
      replace t_UGTIPINV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPINV_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPMES_2_9.RadioValue()==this.w_UGTIPMES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPMES_2_9.SetRadio()
      replace t_UGTIPMES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPMES_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'UGSHJOB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_UGCODUTE)) or not(Empty(.w_UGCODGRU))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SHFLINVS = this.w_SHFLINVS
    this.o_UGTIPFIL = this.w_UGTIPFIL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_UGCODUTE)) or not(Empty(t_UGCODGRU)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_UGTIPFIL=space(1)
      .w_UGCODUTE=0
      .w_UGCODGRU=0
      .w_DESUTE=space(20)
      .w_DESGRU=space(20)
      .w_DESUTEGRU=space(20)
      .w_UGTIPINV=space(1)
      .w_UGTIPMES=space(1)
      .DoRTCalc(1,4,.f.)
        .w_UGCODUTE = IIF(.w_UGTIPFIL='U',.w_UGCODUTE, 0)
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_UGCODUTE))
        .link_2_3('Full')
      endif
        .w_UGCODGRU = IIF(.w_UGTIPFIL='G',.w_UGCODGRU, 0)
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_UGCODGRU))
        .link_2_4('Full')
      endif
      .DoRTCalc(7,8,.f.)
        .w_DESUTEGRU = IIF(.w_UGTIPFIL='U',.w_DESUTE,.w_DESGRU)
        .w_UGTIPINV = "M"
        .w_UGTIPMES = IIF( .w_SHFLINVS = "N" , "L" , "E" )
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_UGTIPFIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPFIL_2_2.RadioValue(.t.)
    this.w_UGCODUTE = t_UGCODUTE
    this.w_UGCODGRU = t_UGCODGRU
    this.w_DESUTE = t_DESUTE
    this.w_DESGRU = t_DESGRU
    this.w_DESUTEGRU = t_DESUTEGRU
    this.w_UGTIPINV = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPINV_2_8.RadioValue(.t.)
    this.w_UGTIPMES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPMES_2_9.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_UGTIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPFIL_2_2.ToRadio()
    replace t_UGCODUTE with this.w_UGCODUTE
    replace t_UGCODGRU with this.w_UGCODGRU
    replace t_DESUTE with this.w_DESUTE
    replace t_DESGRU with this.w_DESGRU
    replace t_DESUTEGRU with this.w_DESUTEGRU
    replace t_UGTIPINV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPINV_2_8.ToRadio()
    replace t_UGTIPMES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUGTIPMES_2_9.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsjb_msaPag1 as StdContainer
  Width  = 726
  height = 397
  stdWidth  = 726
  stdheight = 397
  resizeXpos=354
  resizeYpos=262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=16, top=7, width=694,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Seq.",Field2="UGTIPFIL",Label2="Soggetto",Field3="UGCODUTE",Label3="Utente",Field4="UGCODGRU",Label4="Gruppo",Field5="DESUTEGRU",Label5="Descrizione",Field6="UGTIPINV",Label6="Invio",Field7="UGTIPMES",Label7="Messaggi",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218938234

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=6,top=27,;
    width=690+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*19*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=7,top=28,width=689+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*19*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CPUSERS|CPGROUPS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CPUSERS'
        oDropInto=this.oBodyCol.oRow.oUGCODUTE_2_3
      case cFile='CPGROUPS'
        oDropInto=this.oBodyCol.oRow.oUGCODGRU_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsjb_msaBodyRow as CPBodyRowCnt
  Width=680
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="RXBNXQFDGR",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17168790,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=19, Width=35, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oUGTIPFIL_2_2 as StdTrsCombo with uid="TAADSGJAOQ",rtrep=.t.,;
    cFormVar="w_UGTIPFIL", RowSource=""+"Utente,"+"Gruppo" , ;
    ToolTipText = "Tipo filtro (utente/gruppo)",;
    HelpContextID = 92544658,;
    Height=21, Width=68, Left=40, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oUGTIPFIL_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..UGTIPFIL,&i_cF..t_UGTIPFIL),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'G',;
    space(1))))
  endfunc
  func oUGTIPFIL_2_2.GetRadio()
    this.Parent.oContained.w_UGTIPFIL = this.RadioValue()
    return .t.
  endfunc

  func oUGTIPFIL_2_2.ToRadio()
    this.Parent.oContained.w_UGTIPFIL=trim(this.Parent.oContained.w_UGTIPFIL)
    return(;
      iif(this.Parent.oContained.w_UGTIPFIL=='U',1,;
      iif(this.Parent.oContained.w_UGTIPFIL=='G',2,;
      0)))
  endfunc

  func oUGTIPFIL_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oUGCODUTE_2_3 as StdTrsField with uid="CVRSOWVNVI",rtseq=5,rtrep=.t.,;
    cFormVar="w_UGCODUTE",value=0,;
    ToolTipText = "Codice utente",;
    HelpContextID = 63508107,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=112, Top=0, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_UGCODUTE"

  func oUGCODUTE_2_3.mCond()
    with this.Parent.oContained
      return (.w_UGTIPFIL='U')
    endwith
  endfunc

  func oUGCODUTE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oUGCODUTE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oUGCODUTE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oUGCODUTE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oUGCODGRU_2_4 as StdTrsField with uid="ABOZQOYNOI",rtseq=6,rtrep=.t.,;
    cFormVar="w_UGCODGRU",value=0,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 171372901,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=170, Top=0, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_UGCODGRU"

  func oUGCODGRU_2_4.mCond()
    with this.Parent.oContained
      return (.w_UGTIPFIL='G')
    endwith
  endfunc

  func oUGCODGRU_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oUGCODGRU_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oUGCODGRU_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oUGCODGRU_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESUTEGRU_2_7 as StdTrsField with uid="MGATVCEFKH",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESUTEGRU",value=space(20),enabled=.f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 80744664,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=266, Left=231, Top=0, InputMask=replicate('X',20)

  add object oUGTIPINV_2_8 as StdTrsCombo with uid="KPCHACNYVR",rtrep=.t.,;
    cFormVar="w_UGTIPINV", RowSource=""+"E-mail,"+"Post-in" , ;
    ToolTipText = "Utilizzato solo in caso nella pagina principale sia richiesto l'invio secondo destinatari",;
    HelpContextID = 125559140,;
    Height=21, Width=62, Left=501, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oUGTIPINV_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..UGTIPINV,&i_cF..t_UGTIPINV),this.value)
    return(iif(xVal =1,'M',;
    iif(xVal =2,'P',;
    space(1))))
  endfunc
  func oUGTIPINV_2_8.GetRadio()
    this.Parent.oContained.w_UGTIPINV = this.RadioValue()
    return .t.
  endfunc

  func oUGTIPINV_2_8.ToRadio()
    this.Parent.oContained.w_UGTIPINV=trim(this.Parent.oContained.w_UGTIPINV)
    return(;
      iif(this.Parent.oContained.w_UGTIPINV=='M',1,;
      iif(this.Parent.oContained.w_UGTIPINV=='P',2,;
      0)))
  endfunc

  func oUGTIPINV_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oUGTIPMES_2_9 as StdTrsCombo with uid="TXGPEKVSBA",rtrep=.t.,;
    cFormVar="w_UGTIPMES", RowSource=""+"Solo log,"+"Solo stampe,"+"Log e stampe" , ;
    ToolTipText = "Messaggi da inviare nella mail. Utilizzato solo se 'invio stampe per email' � selezionato a 'selezione tipo messaggio'",;
    HelpContextID = 209985177,;
    Height=21, Width=110, Left=565, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oUGTIPMES_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..UGTIPMES,&i_cF..t_UGTIPMES),this.value)
    return(iif(xVal =1,'L',;
    iif(xVal =2,'R',;
    iif(xVal =3,'E',;
    space(1)))))
  endfunc
  func oUGTIPMES_2_9.GetRadio()
    this.Parent.oContained.w_UGTIPMES = this.RadioValue()
    return .t.
  endfunc

  func oUGTIPMES_2_9.ToRadio()
    this.Parent.oContained.w_UGTIPMES=trim(this.Parent.oContained.w_UGTIPMES)
    return(;
      iif(this.Parent.oContained.w_UGTIPMES=='L',1,;
      iif(this.Parent.oContained.w_UGTIPMES=='R',2,;
      iif(this.Parent.oContained.w_UGTIPMES=='E',3,;
      0))))
  endfunc

  func oUGTIPMES_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oUGTIPMES_2_9.mCond()
    with this.Parent.oContained
      return (.w_SHFLINVS = "D")
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=18
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_msa','UGSHJOB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UGSHJOB=UGSHJOB.UGSHJOB";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
