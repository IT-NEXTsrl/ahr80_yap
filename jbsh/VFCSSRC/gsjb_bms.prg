* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bms                                                        *
*              Modifica processo schedulato                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-23                                                      *
* Last revis.: 2007-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bms",oParentObject,m.w_pOper)
return(i_retval)

define class tgsjb_bms as StdBatch
  * --- Local variables
  w_pOper = space(1)
  w_MASK = .NULL.
  w_GSJB_ASH = .NULL.
  w_VARIABLE = space(20)
  w_DESC = space(254)
  w_VALUE = space(254)
  w_GSVALVAR = space(254)
  w_GSFLGACT = space(1)
  w_NOHIDE = .f.
  w_GSTIPESC = space(1)
  * --- WorkFile variables
  GESTSJBM_idx=0
  GESTSJBD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attenzione le modifiche effettuate vanno (se necessario) riportate sul batch
    *     gsjb_bgs
    * --- --Modifica  processo schedulato
    * --- --Visualizzazione maschera schedulatore
    * --- Istanzio l' anagrafica schedulatore
    this.w_GSJB_ASH = GSJB_ASH()
    if !(this.w_GSJB_ASH.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Caricamento
    if this.w_pOper="M" or this.w_pOper="V"
      this.w_GSJB_ASH.W_SHCODICE = this.oParentObject.w_JOB
      this.w_GSJB_ASH.QueryKeySet("SHCODICE="+ STR(this.oParentObject.w_JOB) )     
      this.w_GSJB_ASH.LoadRecWarn()     
    endif
    if this.w_pOper="M" or this.w_pOper="C"
      if this.w_pOper="M" 
        this.w_GSJB_ASH.ecpEdit()     
      else
        this.w_GSJB_ASH.ecpLoad()     
        this.w_GSJB_ASH.W_SHDESCRI = this.oParentObject.w_DESCR
      endif
      this.w_GSJB_ASH.SaveDependsOn()     
      this.w_GSJB_ASH.SetControlsValue()     
      this.w_GSJB_ASH.mHideControls()     
      this.w_GSJB_ASH.ChildrenChangeRow()     
      this.w_GSJB_ASH.oPgFrm.ActivePage = 3
      if NOT EMPTY(this.w_GSJB_ASH.GSJB_MJB.w_JBNOME)
        this.w_GSJB_ASH.GSJB_MJB.InitRow()     
      endif
      this.w_GSJB_ASH.GSJB_MJB.WorkFromTrs()     
      this.w_GSJB_ASH.GSJB_MJB.w_JBNOME = this.oParentObject.w_NOMEAP
      this.w_GSJB_ASH.GSJB_MJB.w_JBDESC = this.oParentObject.w_DESCR
      this.w_GSJB_ASH.GSJB_MJB.w_JBCODAZI = i_CODAZI
      this.w_GSJB_ASH.GSJB_MJB.w_JBROUTINE = "M"
      if empty(this.oParentObject.oParentObject.cfile)
        this.w_GSJB_ASH.GSJB_MJB.w_JBROUTINE = "A"
      endif
      * --- Di default metto alla chiusura F10
      * --- Read from GESTSJBM
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.GESTSJBM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBM_idx,2],.t.,this.GESTSJBM_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "GSTIPESC"+;
          " from "+i_cTable+" GESTSJBM where ";
              +"GSNOME = "+cp_ToStrODBC(this.oParentObject.w_NOMEAP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          GSTIPESC;
          from (i_cTable) where;
              GSNOME = this.oParentObject.w_NOMEAP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GSTIPESC = NVL(cp_ToDate(_read_.GSTIPESC),cp_NullValue(_read_.GSTIPESC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_GSJB_ASH.GSJB_MJB.w_JBTIPESC = IIF(EMPTY(this.w_GSTIPESC), "S", this.w_GSTIPESC)
      this.w_GSJB_ASH.GSJB_MJB.TrsFromWork()     
      this.w_GSJB_ASH.GSJB_MJB.mCalc(.t.)     
      this.w_GSJB_ASH.GSJB_MJB.setfocus()     
      this.w_GSJB_ASH.GSJB_MJB.ChildrenChangeRow()     
      this.w_GSJB_ASH.GSJB_MJB.GSJB_MPR.LinkPCClick()     
      this.w_GSJB_ASH.GSJB_MJB.oPgFrm.Page1.oPag.oBody.Refresh()     
      this.w_MASK = this.oParentObject.oParentObject
      GSUT_BCP(this,this.w_MASK, this.w_GSJB_ASH.GSJB_MJB.GSJB_MPR.cnt, "J", this.oParentObject.w_NOMEAP)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_GSJB_ASH.GSJB_MJB.GSJB_MPR.EcpSave()     
      this.w_GSJB_ASH.GSJB_MJB.setfocus()     
      this.w_GSJB_ASH.GSJB_MJB.Refresh()     
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case type("w_valore")="D"
        w_valore="cp_CharToDate('"+DTOC(w_valore)+"')"
      case type("w_valore")="N"
        do case
          case this.w_NOHIDE AND UPPER(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).class) $ "STDCOMBO-STDCHECK-STDRADIO"
            w_valore=this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).RadioValue()
            do case
              case type("w_valore")="C"
                w_valore='"'+this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).RadioValue()+'"'
              case type("w_valore")="N"
                w_valore="VAL('"+STR(w_valore , 25, 8)+"')"
              case type("w_valore")="L"
                w_valore=iif(w_valore,"true","false")
            endcase
          otherwise
            w_valore="VAL('"+STR(w_valore , 25, 8)+"')"
        endcase
      case type("w_valore")="L"
        w_valore=iif(w_valore,"true","false")
      case type("w_valore")="C"
        w_valore='"'+LEFT(w_valore, 252)+'"'
    endcase
  endproc


  proc Init(oParentObject,w_pOper)
    this.w_pOper=w_pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='GESTSJBM'
    this.cWorkTables[2]='GESTSJBD'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_pOper"
endproc
