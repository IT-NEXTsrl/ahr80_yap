* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bxp                                                        *
*              Esecuzione proattivit�                                          *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-30                                                      *
* Last revis.: 2014-02-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bxp",oParentObject)
return(i_retval)

define class tgsjb_bxp as StdBatch
  * --- Local variables
  w_GSJB_BSJ = .NULL.
  w_SHCODICE = 0
  w_SHTIPELA = space(1)
  w_SHELABOR = space(250)
  w_SHFRMRES = space(1)
  w_SHREPRES = space(250)
  w_SHTIPRES = space(1)
  w_SHTXTHEA = space(0)
  w_SHTXTBOD = space(0)
  w_SHTXTFOO = space(0)
  w_SHFREQ = space(1)
  w_SHNUMFREQ = 0
  w_SHSET = space(50)
  w_SHMESE = space(75)
  w_SHIMPDAT = ctod("  /  /  ")
  w_SHDATFIS = ctod("  /  /  ")
  w_SHDATAPRO = ctod("  /  /  ")
  w_SHDATASTART = ctod("  /  /  ")
  w_SHORAPROS = space(5)
  w_ora_lancio = space(2)
  w_mm_lancio = space(2)
  w_SHMINUINT = 0
  w_ora_lancifi = space(2)
  w_mm_lancifi = space(2)
  w_SHORAFIN = space(5)
  w_SHORAINI = space(5)
  w_SHDATA = ctod("  /  /  ")
  w_SHORA = space(5)
  w_SHDESCRI = space(254)
  w_SHFINPRE = ctot("")
  w_INIZIOATTIVITA = ctot("")
  w_FINEATTIVITA = ctot("")
  w_CURSORE = space(10)
  w_SHAGENDA = space(1)
  w_SHDESTIN = space(250)
  w_ELENCODESTINATARI = space(10)
  w_NUMEROINVIO = 0
  w_DESTINATARIOCORRENTE = 0
  w_CURSOREDESTINATARIO = space(10)
  w_ALLEGATO = space(250)
  w_BOTTONE = space(250)
  w_RIGHERESPONSO = 0
  w_TESTODELRESPONSO = space(0)
  w_ACAPO = space(2)
  w_Ret = .f.
  * --- WorkFile variables
  SCHEDJOB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esecutore del job
    this.w_GSJB_BSJ = this.oParentObject
    * --- Numero del job
    this.w_SHCODICE = this.w_GSJB_BSJ.w_SHCOD
    * --- Lettura delle informazioni della proattivit�
    * --- Tipo di elaborazione (Q query/B batch)
    * --- Elaborazione (nome della query o del batch)
    * --- Formato del responso (A allegato/T testo)
    * --- Report per il responso
    * --- Occorrenza del responso (X esecuzione/E evento)
    * --- Testata del responso
    * --- Dettaglio del responso
    * --- Piede del responso
    * --- --tipo della frequenza temporale, unico, giornaliero, settimanale, mensile
    * --- --Cadenza ripetizione del lancio del processo
    * --- Giorni della settimana selezionati
    * --- --Mesi selezionati
    * --- Tipo data mese scelta
    * --- Read from SCHEDJOB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2],.t.,this.SCHEDJOB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SHTIPELA,SHELABOR,SHFRMRES,SHREPRES,SHTIPRES,SHTXTHEA,SHTXTBOD,SHTXTFOO,SHAGENDA,SHDESTIN,SHFREQ,SHNUMFREQ,SHSET,SHMESE,SHIMPDAT,SHDATFIS,SHDATAPRO,SHDATASTART,SHORAPROS,SHMINUINT,SHORAFIN,SHORAINI,SHDESCRI,SHFINPRE"+;
        " from "+i_cTable+" SCHEDJOB where ";
            +"SHCODICE = "+cp_ToStrODBC(this.w_SHCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SHTIPELA,SHELABOR,SHFRMRES,SHREPRES,SHTIPRES,SHTXTHEA,SHTXTBOD,SHTXTFOO,SHAGENDA,SHDESTIN,SHFREQ,SHNUMFREQ,SHSET,SHMESE,SHIMPDAT,SHDATFIS,SHDATAPRO,SHDATASTART,SHORAPROS,SHMINUINT,SHORAFIN,SHORAINI,SHDESCRI,SHFINPRE;
        from (i_cTable) where;
            SHCODICE = this.w_SHCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SHTIPELA = NVL(cp_ToDate(_read_.SHTIPELA),cp_NullValue(_read_.SHTIPELA))
      this.w_SHELABOR = NVL(cp_ToDate(_read_.SHELABOR),cp_NullValue(_read_.SHELABOR))
      this.w_SHFRMRES = NVL(cp_ToDate(_read_.SHFRMRES),cp_NullValue(_read_.SHFRMRES))
      this.w_SHREPRES = NVL(cp_ToDate(_read_.SHREPRES),cp_NullValue(_read_.SHREPRES))
      this.w_SHTIPRES = NVL(cp_ToDate(_read_.SHTIPRES),cp_NullValue(_read_.SHTIPRES))
      this.w_SHTXTHEA = NVL(cp_ToDate(_read_.SHTXTHEA),cp_NullValue(_read_.SHTXTHEA))
      this.w_SHTXTBOD = NVL(cp_ToDate(_read_.SHTXTBOD),cp_NullValue(_read_.SHTXTBOD))
      this.w_SHTXTFOO = NVL(cp_ToDate(_read_.SHTXTFOO),cp_NullValue(_read_.SHTXTFOO))
      this.w_SHAGENDA = NVL(cp_ToDate(_read_.SHAGENDA),cp_NullValue(_read_.SHAGENDA))
      this.w_SHDESTIN = NVL(cp_ToDate(_read_.SHDESTIN),cp_NullValue(_read_.SHDESTIN))
      this.w_SHFREQ = NVL(cp_ToDate(_read_.SHFREQ),cp_NullValue(_read_.SHFREQ))
      this.w_SHNUMFREQ = NVL(cp_ToDate(_read_.SHNUMFREQ),cp_NullValue(_read_.SHNUMFREQ))
      this.w_SHSET = NVL(cp_ToDate(_read_.SHSET),cp_NullValue(_read_.SHSET))
      this.w_SHMESE = NVL(cp_ToDate(_read_.SHMESE),cp_NullValue(_read_.SHMESE))
      this.w_SHIMPDAT = NVL(cp_ToDate(_read_.SHIMPDAT),cp_NullValue(_read_.SHIMPDAT))
      this.w_SHDATFIS = NVL(cp_ToDate(_read_.SHDATFIS),cp_NullValue(_read_.SHDATFIS))
      this.w_SHDATAPRO = NVL(cp_ToDate(_read_.SHDATAPRO),cp_NullValue(_read_.SHDATAPRO))
      this.w_SHDATASTART = NVL(cp_ToDate(_read_.SHDATASTART),cp_NullValue(_read_.SHDATASTART))
      this.w_SHORAPROS = NVL(cp_ToDate(_read_.SHORAPROS),cp_NullValue(_read_.SHORAPROS))
      this.w_SHMINUINT = NVL(cp_ToDate(_read_.SHMINUINT),cp_NullValue(_read_.SHMINUINT))
      this.w_SHORAFIN = NVL(cp_ToDate(_read_.SHORAFIN),cp_NullValue(_read_.SHORAFIN))
      this.w_SHORAINI = NVL(cp_ToDate(_read_.SHORAINI),cp_NullValue(_read_.SHORAINI))
      this.w_SHDESCRI = NVL(cp_ToDate(_read_.SHDESCRI),cp_NullValue(_read_.SHDESCRI))
      this.w_SHFINPRE = NVL((_read_.SHFINPRE),cp_NullValue(_read_.SHFINPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Utilizzabile come selezione nella query o nel batch che costruisce il cursore ed
    *     indica il momento in cui � stato schedulato l'inzio del processo corrente
    * --- Valori passati come parametro all'esecutore del job
    this.w_SHDATA = this.w_GSJB_BSJ.w_SHDATA
    this.w_SHORA = this.w_GSJB_BSJ.w_SHORA
    this.w_INIZIOATTIVITA = DATETIME( YEAR(this.w_SHDATA) , MONTH(this.w_SHDATA), DAY(this.w_SHDATA), VAL(LEFT(this.w_SHORA,2)) , VAL(RIGHT(this.w_SHORA,2)) )
    * --- Utilizzabile come selezione nella query o nel batch che costruisce il cursore ed
    *     indica il momento in cui � stato schedulato l'inzio del prossimo processo
    * --- Calcola la data di fine selezione
    if this.w_SHFREQ<>"U"
      this.w_ora_lancio = substr(this.w_SHORA,1,2)
      this.w_mm_lancio = substr(this.w_SHORA,4,2)
      this.w_ora_lancifi = substr(this.w_SHORAFIN,1,2)
      this.w_mm_lancifi = substr(this.w_SHORAFIN,4,2)
      * --- Calcolo data prossima esecuzione
      gsjb_bde(this,DATE(),this.w_SHDATASTART)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_FINEATTIVITA = DATETIME( YEAR(this.w_SHDATAPRO) , MONTH(this.w_SHDATAPRO), DAY(this.w_SHDATAPRO), VAL(LEFT(this.w_SHORAPROS,2)) , VAL(RIGHT(this.w_SHORAPROS,2)) )
    endif
    * --- Se la stessa proattivit� � stata lanciata in precedenza, l'esecutore di proattivit� avr� scritto la data
    *     e l'ora di elaborazione successiva calcolata durante la sua elaborazione. Se questa risulta precedente alla
    *     data e ora di inizio della schedulazione corrente, viene utilizzata come inizio selezione nel filtro della query
    *     utilizzata dalla proattivit�.
    if NOT EMPTY( CP_TODATE( this.w_SHFINPRE ) )
      if this.w_SHFINPRE < this.w_INIZIOATTIVITA
        this.w_INIZIOATTIVITA = this.w_SHFINPRE
      endif
    endif
    * --- Write into SCHEDJOB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SCHEDJOB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SHFINPRE ="+cp_NullLink(cp_ToStrODBC(this.w_FINEATTIVITA),'SCHEDJOB','SHFINPRE');
          +i_ccchkf ;
      +" where ";
          +"SHCODICE = "+cp_ToStrODBC(this.w_SHCODICE);
             )
    else
      update (i_cTable) set;
          SHFINPRE = this.w_FINEATTIVITA;
          &i_ccchkf. ;
       where;
          SHCODICE = this.w_SHCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Cursore con i dati utilizzati per l'alert
    this.w_CURSORE = SYS( 2015 )
    if this.w_SHTIPELA= "Q"
      * --- Tipo di elaborazione 'query'
      VQ_EXEC( this.w_SHELABOR , THIS , this.w_CURSORE )
    else
      * --- Tipo di elaborazione batch
      w_ESECUZIONEBATCH = "DO " + ALLTRIM( this.w_SHELABOR ) + " WITH THIS , '" + this.w_CURSORE + "'"
      &w_ESECUZIONEBATCH
    endif
    * --- Se � settato il flag 'appuntamenti in agenda', viene indiato un campo che contiene il codice
    *     dell'utente che deve ricevere la notifica dell'appuntamento
    if this.w_SHAGENDA $ "P-M-Q"
      * --- Se il flag 'agenda' � 'P', la query continene un campo, il cui nome � indicato in w_SHDESTIN,
      *     che contiene il codice dell'utente ad hoc a cui inviare il messaggio
      *     Se il flag 'agenda' � 'M', la query continene un campo, il cui nome � indicato in w_SHDESTIN,
      *     che contiene l'indirizzo e-mail a cui inviare il messaggio
      * --- Costruisce l'elenco dei destinatari
      this.w_ELENCODESTINATARI = SYS( 2015 )
      w_COSTRUZIONECURSOREDESTINATARI = "SELECT DISTINCT " + ALLTRIM( this.w_SHDESTIN ) + " AS DESTINATARIO FROM " + this.w_CURSORE + " INTO CURSOR " + this.w_ELENCODESTINATARI
      &w_COSTRUZIONECURSOREDESTINATARI
      this.w_NUMEROINVIO = 0
      * --- Preparazione ed invio messaggi
      SELECT ( this.w_ELENCODESTINATARI )
      GO TOP
      do while NOT EOF()
        this.w_DESTINATARIOCORRENTE = DESTINATARIO
        this.w_NUMEROINVIO = this.w_NUMEROINVIO + 1
        * --- Esegue filtro per selezionare i soli record del destinatario corrente
        this.w_CURSOREDESTINATARIO = "__TMP__"
        w_CURSOREDESTINATARIOCORRENTE = "SELECT * FROM " + this.w_CURSORE + " WHERE " + ALLTRIM( this.w_SHDESTIN ) + " = " + CP_TOSTR(this.w_DESTINATARIOCORRENTE) + " INTO CURSOR " + this.w_CURSOREDESTINATARIO
        &w_CURSOREDESTINATARIOCORRENTE
        this.w_TESTODELRESPONSO = ""
        this.w_ALLEGATO = ""
        this.w_BOTTONE = ""
        if RECCOUNT( this.w_CURSOREDESTINATARIO ) > 0
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if USED( this.w_CURSOREDESTINATARIO )
          SELECT ( this.w_CURSOREDESTINATARIO )
          USE
        endif
        * --- Passa al destinatario successivo
        SELECT ( this.w_ELENCODESTINATARI )
        SKIP
      enddo
    endif
    * --- Sia nel caso di query che di batch viene costruito un cursore utilizzato per il reponso
    this.w_RIGHERESPONSO = RECCOUNT( this.w_CURSORE )
    this.w_TESTODELRESPONSO = ""
    * --- Se abbiamo un risultato nel cursore, oppure se il responso � ad esecuzione
    if this.w_RIGHERESPONSO > 0 OR this.w_SHTIPRES = "X"
      * --- Si costruisce il messaggio da inserire nell'avviso, oppure il resoconto su report
      if this.w_SHFRMRES = "A"
        * --- Riapro
        if lower(this.w_CURSORE)<>"__tmp__"
          SELECT ( this.w_CURSORE ) 
 USE DBF() AGAIN IN 0 ALIAS "__TMP__"
        endif
        * --- Creazione del responso da allegare ai messaggi
        CP_CHPRN( this.w_SHREPRES )
      else
        this.w_TESTODELRESPONSO = CUR_TO_MSG( this.w_CURSORE , this.w_SHTXTHEA , this.w_SHTXTBOD , this.w_SHTXTFOO )
        * --- Utilizzato dalla routine di schedulazione
        g_MESSAGGIORESPONSO = this.w_TESTODELRESPONSO
      endif
    else
      * --- Se il cursore non ha righe e l'elaborazione � ad evento metto un messaggio standard di avviso no dati
      this.w_TESTODELRESPONSO = "L'elaborazione dell'evento non contiene dati"
      g_MESSAGGIORESPONSO = this.w_TESTODELRESPONSO
      this.w_GSJB_BSJ.w_SOLOCONLOG = .T.
    endif
    if USED( this.w_CURSORE )
      SELECT ( this.w_CURSORE )
      USE
    endif
    i_retcode = 'stop'
    i_retval = this.w_TESTODELRESPONSO
    return
    * --- --
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_SHFRMRES = "A"
      * --- Il responso viene fornito attraverso un report visual fox pro
      if EMPTY( g_PATHSJ )
        if VARTYPE( this.w_DESTINATARIOCORRENTE ) = "N"
          this.w_ALLEGATO = TEMPADHOC()+"\"+g_JBCODAZI+"_"+TTOC(this.w_INIZIOATTIVITA,1)+"_"+g_JBNOME+"_"+RIGHT("000"+ALLTRIM(STR(this.w_DESTINATARIOCORRENTE)),3)+".PDF"
        else
          this.w_ALLEGATO = TEMPADHOC()+"\"+g_JBCODAZI+"_"+TTOC(this.w_INIZIOATTIVITA,1)+"_"+g_JBNOME+"_"+ALLTRIM(this.w_DESTINATARIOCORRENTE)+".PDF"
        endif
      else
        if VARTYPE( this.w_DESTINATARIOCORRENTE ) = "N"
          this.w_ALLEGATO = g_PATHSJ+g_JBCODAZI+"_"+TTOC(this.w_INIZIOATTIVITA,1)+"_"+g_JBNOME+"_"+RIGHT("000"+ALLTRIM(STR(this.w_DESTINATARIOCORRENTE)),3)+".PDF"
        else
          this.w_ALLEGATO = g_PATHSJ+g_JBCODAZI+"_"+TTOC(this.w_INIZIOATTIVITA,1)+"_"+g_JBNOME+"_"+ALLTRIM(this.w_DESTINATARIOCORRENTE)+".PDF"
        endif
      endif
      PRINT_TO_FILE( this.w_ALLEGATO , this.w_SHREPRES , "PDF" )
    else
      * --- Il responso viene fornito attraverso il testo costruito tramite le impostazioni
      *     nella pagina delle proattivit�
      this.w_TESTODELRESPONSO = CUR_TO_MSG( this.w_CURSOREDESTINATARIO , this.w_SHTXTHEA , this.w_SHTXTBOD , this.w_SHTXTFOO )
    endif
    do case
      case this.w_SHAGENDA $ "P-Q"
        if VARTYPE( this.w_DESTINATARIOCORRENTE ) = "N"
          * --- In questo caso w_DESTINATARIOCORRENTE deve essere numerico, perch� � il codice di un utente di ad hoc
          if NOT i_bDisablePostin
            if this.w_SHAGENDA $ "Q"
              this.w_BOTTONE = ""
              SELECT( this.w_CURSOREDESTINATARIO )
              GO TOP
              do while NOT EOF()
                L_CODICEATTIVITA = this.w_CURSOREDESTINATARIO + ".ATSERIAL"
                if TYPE("L_CODICEATTIVITA") = "C"
                  this.w_ACAPO = CHR( 13 ) + CHR( 10 )
                  * --- Inserisce il bottone per il rinvio dell'attivit�
                  L_CODICEATTIVITA1 = CP_TOSTRODBC( &L_CODICEATTIVITA )
                  this.w_BOTTONE = IIF( EMPTY( this.w_BOTTONE ) , "" , this.w_BOTTONE + this.w_ACAPO ) + GSUT_BMB("GSAG_BRI(.NULL.," + L_CODICEATTIVITA1 + ")","Posponi",i_codazi)
                  * --- Inserisce il bottone per l'apertura dell'attivit�
                  this.w_BOTTONE = this.w_BOTTONE + this.w_ACAPO + GSUT_BMB("GSAG_AAT","Apri   att.",i_codazi,&L_CODICEATTIVITA)
                endif
                SELECT( this.w_CURSOREDESTINATARIO )
                SKIP
              enddo
            endif
            GSUT_BIP(this, this.w_DESTINATARIOCORRENTE , this.w_TESTODELRESPONSO , this.w_ALLEGATO , this.w_BOTTONE )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Inserisce messaggio nel log
            AH_ERRORMSG("%0Eseguito smistamento dati tramite post-in a utente %1%0", , , ALLTRIM(STR( this.w_DESTINATARIOCORRENTE )))
          else
            * --- Inserisce messaggio nel log
            AH_ERRORMSG("%0Impossibile eseguire smistamento dati tramite post-in all'utente %1: la funzionalit� � disabilitata%0", , , ALLTRIM(STR( this.w_DESTINATARIOCORRENTE )))
          endif
        endif
      case this.w_SHAGENDA $ "M"
        if VARTYPE( this.w_DESTINATARIOCORRENTE ) = "C"
          * --- In questo caso w_DESTINATARIOCORRENTE deve essere carattere, perch� � � un indirizzo e-mail
          PRIVATE ListInd
          Dimension ListInd(1,2)
          ListInd(1,1)=this.w_DESTINATARIOCORRENTE
          ListInd(1,2)="A"
          * --- Invia la mail
          this.w_Ret = EMAIL( this.w_ALLEGATO ,"N",,@ListInd, ALLTRIM(this.w_SHDESCRI) ,this.w_TESTODELRESPONSO)
          if this.w_Ret
            AH_ERRORMSG("%0eseguito smistamento dati tramite e-mail inviato all'indirizzo %1%0", , , ALLTRIM( this.w_DESTINATARIOCORRENTE ))
          else
            AH_ERRORMSG("%0impossibile eseguire smistamento dati tramite e-mail errore nell'invio dati all'indirizzo %1%0", , , ALLTRIM( this.w_DESTINATARIOCORRENTE ))
          endif
        endif
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SCHEDJOB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
