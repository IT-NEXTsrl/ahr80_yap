* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bjs                                                        *
*              PDF viewer                                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_18]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-27                                                      *
* Last revis.: 2006-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bjs",oParentObject)
return(i_retval)

define class tgsjb_bjs as StdBatch
  * --- Local variables
  l_OK = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Visualizza file PDF, lanciato da GSJB_KJS
    * --- Elimino eventuali spazi
    this.oParentObject.w_FILENAME = ALLTRIM(this.oParentObject.w_FILENAME)
    * --- Controllo l'eventuale presenza di un path, se non ho un path uso la cartella 
    *     corrente
    if at("\",this.oParentObject.w_FILENAME) = 0
      this.l_OK = this.ShellEx(sys(5)+sys(2003)+"\"+this.oParentObject.w_FILENAME,"","")
    else
      this.l_OK = this.ShellEx(this.oParentObject.w_FILENAME,"","")
    endif
    if this.l_OK <= 32
      * --- --ERRORE DURANTE L'APERTURA DEL PDF
      do case
        case this.l_OK = 2
          * --- --FILE NON TROVATO
          ah_ErrorMsg("File non trovato","!","")
        case this.l_OK =31
          * --- --NESSUN APPLICATIVO ASSOCIATO
          ah_ErrorMsg("Nessun applicativo associato al file","!","")
        case this.l_OK = 8
          * --- --MEMORIA INSUFFICIENTE
          ah_ErrorMsg("Memoria insufficiente","!","")
        otherwise
          * --- --ERRORE SCONOSCIUTO
          ah_ErrorMsg("Errore durante l'apertura del file","!","")
      endcase
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gsjb_bjs
  func ShellEx(cLink,cAction,cParms)
    declare integer ShellExecute in shell32.dll;
      integer nHwnd,;
      string cOperation,;
      string cFileName,;
      string cParms,;
      string cDir,;
      integer nShowCmd
  
    declare integer FindWindow in win32api;
      string cNull,;
      string cWinName
  
    cAction=iif(empty(cAction),"Open",cAction)
    cParms=iif(empty(cParms),"",cParms)
    return(ShellExecute(FindWindow(0,_screen.caption),cAction,cLink,cParms,tempadhoc(),1))
  endfunc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
