* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_mpr                                                        *
*              Parametri                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_76]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-20                                                      *
* Last revis.: 2015-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsjb_mpr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsjb_mpr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsjb_mpr")
  return

* --- Class definition
define class tgsjb_mpr as StdPCForm
  Width  = 811
  Height = 320
  Top    = 1
  Left   = 1
  cComment = "Parametri"
  cPrg = "gsjb_mpr"
  HelpContextID=147484265
  add object cnt as tcgsjb_mpr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsjb_mpr as PCContext
  w_PRJOBCOD = 0
  w_CPROWORD = 0
  w_JBNOME = space(20)
  w_PRNOMVAL = space(20)
  w_PRCODPAR = 0
  w_PRNOMDES = space(254)
  w_PRFLGPP = space(1)
  w_PRVALVAR = space(254)
  w_PRTIPPAR = space(1)
  proc Save(i_oFrom)
    this.w_PRJOBCOD = i_oFrom.w_PRJOBCOD
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_JBNOME = i_oFrom.w_JBNOME
    this.w_PRNOMVAL = i_oFrom.w_PRNOMVAL
    this.w_PRCODPAR = i_oFrom.w_PRCODPAR
    this.w_PRNOMDES = i_oFrom.w_PRNOMDES
    this.w_PRFLGPP = i_oFrom.w_PRFLGPP
    this.w_PRVALVAR = i_oFrom.w_PRVALVAR
    this.w_PRTIPPAR = i_oFrom.w_PRTIPPAR
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PRJOBCOD = this.w_PRJOBCOD
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_JBNOME = this.w_JBNOME
    i_oTo.w_PRNOMVAL = this.w_PRNOMVAL
    i_oTo.w_PRCODPAR = this.w_PRCODPAR
    i_oTo.w_PRNOMDES = this.w_PRNOMDES
    i_oTo.w_PRFLGPP = this.w_PRFLGPP
    i_oTo.w_PRVALVAR = this.w_PRVALVAR
    i_oTo.w_PRTIPPAR = this.w_PRTIPPAR
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsjb_mpr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 811
  Height = 320
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-16"
  HelpContextID=147484265
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PARAMJ_IDX = 0
  GESTSJBD_IDX = 0
  LISTAJOB_IDX = 0
  cFile = "PARAMJ"
  cKeySelect = "PRJOBCOD,PRCODPAR"
  cKeyWhere  = "PRJOBCOD=this.w_PRJOBCOD and PRCODPAR=this.w_PRCODPAR"
  cKeyDetail  = "PRJOBCOD=this.w_PRJOBCOD and PRCODPAR=this.w_PRCODPAR"
  cKeyWhereODBC = '"PRJOBCOD="+cp_ToStrODBC(this.w_PRJOBCOD)';
      +'+" and PRCODPAR="+cp_ToStrODBC(this.w_PRCODPAR)';

  cKeyDetailWhereODBC = '"PRJOBCOD="+cp_ToStrODBC(this.w_PRJOBCOD)';
      +'+" and PRCODPAR="+cp_ToStrODBC(this.w_PRCODPAR)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PARAMJ.PRJOBCOD="+cp_ToStrODBC(this.w_PRJOBCOD)';
      +'+" and PARAMJ.PRCODPAR="+cp_ToStrODBC(this.w_PRCODPAR)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PARAMJ.CPROWORD '
  cPrg = "gsjb_mpr"
  cComment = "Parametri"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRJOBCOD = 0
  w_CPROWORD = 0
  w_JBNOME = space(20)
  w_PRNOMVAL = space(20)
  w_PRCODPAR = 0
  w_PRNOMDES = space(254)
  w_PRFLGPP = space(1)
  o_PRFLGPP = space(1)
  w_PRVALVAR = space(254)
  w_PRTIPPAR = space(1)
  o_PRTIPPAR = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsjb_mprPag1","gsjb_mpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='GESTSJBD'
    this.cWorkTables[2]='LISTAJOB'
    this.cWorkTables[3]='PARAMJ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PARAMJ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PARAMJ_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsjb_mpr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PARAMJ where PRJOBCOD=KeySet.PRJOBCOD
    *                            and PRCODPAR=KeySet.PRCODPAR
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PARAMJ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARAMJ_IDX,2],this.bLoadRecFilter,this.PARAMJ_IDX,"gsjb_mpr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PARAMJ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PARAMJ.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PARAMJ '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRJOBCOD',this.w_PRJOBCOD  ,'PRCODPAR',this.w_PRCODPAR  )
      select * from (i_cTable) PARAMJ where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_JBNOME = this.oParentObject .w_JBNOME
        .w_PRJOBCOD = NVL(PRJOBCOD,0)
        .w_PRCODPAR = NVL(PRCODPAR,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PARAMJ')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PRNOMVAL = NVL(PRNOMVAL,space(20))
          * evitabile
          *.link_2_3('Load')
          .w_PRNOMDES = NVL(PRNOMDES,space(254))
          .w_PRFLGPP = NVL(PRFLGPP,space(1))
          .w_PRVALVAR = NVL(PRVALVAR,space(254))
          .w_PRTIPPAR = NVL(PRTIPPAR,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PRJOBCOD=0
      .w_CPROWORD=10
      .w_JBNOME=space(20)
      .w_PRNOMVAL=space(20)
      .w_PRCODPAR=0
      .w_PRNOMDES=space(254)
      .w_PRFLGPP=space(1)
      .w_PRVALVAR=space(254)
      .w_PRTIPPAR=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_JBNOME = this.oParentObject .w_JBNOME
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PRNOMVAL))
         .link_2_3('Full')
        endif
        .DoRTCalc(5,7,.f.)
        .w_PRVALVAR = IIF(.w_PRFLGPP='P','',.w_PRVALVAR)
        .w_PRTIPPAR = 'V'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PARAMJ')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PARAMJ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PARAMJ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRJOBCOD,"PRJOBCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODPAR,"PRCODPAR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(7);
      ,t_PRNOMVAL C(20);
      ,t_PRNOMDES C(254);
      ,t_PRFLGPP N(3);
      ,t_PRVALVAR C(254);
      ,t_PRTIPPAR N(3);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsjb_mprbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMVAL_2_3.controlsource=this.cTrsName+'.t_PRNOMVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMDES_2_4.controlsource=this.cTrsName+'.t_PRNOMDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRFLGPP_2_5.controlsource=this.cTrsName+'.t_PRFLGPP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRVALVAR_2_6.controlsource=this.cTrsName+'.t_PRVALVAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAR_2_7.controlsource=this.cTrsName+'.t_PRTIPPAR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(71)
    this.AddVLine(180)
    this.AddVLine(380)
    this.AddVLine(449)
    this.AddVLine(687)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PARAMJ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARAMJ_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PARAMJ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARAMJ_IDX,2])
      *
      * insert into PARAMJ
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PARAMJ')
        i_extval=cp_InsertValODBCExtFlds(this,'PARAMJ')
        i_cFldBody=" "+;
                  "(PRJOBCOD,CPROWORD,PRNOMVAL,PRCODPAR,PRNOMDES"+;
                  ",PRFLGPP,PRVALVAR,PRTIPPAR,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PRJOBCOD)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_PRNOMVAL)+","+cp_ToStrODBC(this.w_PRCODPAR)+","+cp_ToStrODBC(this.w_PRNOMDES)+;
             ","+cp_ToStrODBC(this.w_PRFLGPP)+","+cp_ToStrODBC(this.w_PRVALVAR)+","+cp_ToStrODBC(this.w_PRTIPPAR)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PARAMJ')
        i_extval=cp_InsertValVFPExtFlds(this,'PARAMJ')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PRJOBCOD',this.w_PRJOBCOD,'PRCODPAR',this.w_PRCODPAR)
        INSERT INTO (i_cTable) (;
                   PRJOBCOD;
                  ,CPROWORD;
                  ,PRNOMVAL;
                  ,PRCODPAR;
                  ,PRNOMDES;
                  ,PRFLGPP;
                  ,PRVALVAR;
                  ,PRTIPPAR;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PRJOBCOD;
                  ,this.w_CPROWORD;
                  ,this.w_PRNOMVAL;
                  ,this.w_PRCODPAR;
                  ,this.w_PRNOMDES;
                  ,this.w_PRFLGPP;
                  ,this.w_PRVALVAR;
                  ,this.w_PRTIPPAR;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PARAMJ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARAMJ_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PRNOMVAL))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PARAMJ')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PARAMJ')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PRNOMVAL))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PARAMJ
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PARAMJ')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PRNOMVAL="+cp_ToStrODBCNull(this.w_PRNOMVAL)+;
                     ",PRNOMDES="+cp_ToStrODBC(this.w_PRNOMDES)+;
                     ",PRFLGPP="+cp_ToStrODBC(this.w_PRFLGPP)+;
                     ",PRVALVAR="+cp_ToStrODBC(this.w_PRVALVAR)+;
                     ",PRTIPPAR="+cp_ToStrODBC(this.w_PRTIPPAR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PARAMJ')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,PRNOMVAL=this.w_PRNOMVAL;
                     ,PRNOMDES=this.w_PRNOMDES;
                     ,PRFLGPP=this.w_PRFLGPP;
                     ,PRVALVAR=this.w_PRVALVAR;
                     ,PRTIPPAR=this.w_PRTIPPAR;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PARAMJ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARAMJ_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PRNOMVAL))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PARAMJ
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PRNOMVAL))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PARAMJ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARAMJ_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_PRFLGPP<>.w_PRFLGPP
          .w_PRVALVAR = IIF(.w_PRFLGPP='P','',.w_PRVALVAR)
        endif
        if .o_PRTIPPAR<>.w_PRTIPPAR
          .Calculate_GAIAIESLUH()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_GAIAIESLUH()
    with this
          * --- Setta passaggio se tipologia bottone
          .w_PRFLGPP = IIF(.w_PRTIPPAR='B',' ',.w_PRFLGPP)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRFLGPP_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRFLGPP_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRVALVAR_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRVALVAR_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRTIPPAR_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRTIPPAR_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBtn_2_8.visible=!this.oPgFrm.Page1.oPag.oBtn_2_8.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRNOMVAL
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GESTSJBD_IDX,3]
    i_lTable = "GESTSJBD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GESTSJBD_IDX,2], .t., this.GESTSJBD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GESTSJBD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRNOMVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSJB_MGS',True,'GESTSJBD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GSNOMVAL like "+cp_ToStrODBC(trim(this.w_PRNOMVAL)+"%");
                   +" and GSNOME="+cp_ToStrODBC(this.w_JBNOME);

          i_ret=cp_SQL(i_nConn,"select GSNOME,GSNOMVAL,GSNOMDES,GSTIPPAR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GSNOME,GSNOMVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GSNOME',this.w_JBNOME;
                     ,'GSNOMVAL',trim(this.w_PRNOMVAL))
          select GSNOME,GSNOMVAL,GSNOMDES,GSTIPPAR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GSNOME,GSNOMVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRNOMVAL)==trim(_Link_.GSNOMVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRNOMVAL) and !this.bDontReportError
            deferred_cp_zoom('GESTSJBD','*','GSNOME,GSNOMVAL',cp_AbsName(oSource.parent,'oPRNOMVAL_2_3'),i_cWhere,'GSJB_MGS',"Selezione parametro",'gsjb_mpr.GESTSJBD_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_JBNOME<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GSNOME,GSNOMVAL,GSNOMDES,GSTIPPAR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select GSNOME,GSNOMVAL,GSNOMDES,GSTIPPAR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GSNOME,GSNOMVAL,GSNOMDES,GSTIPPAR";
                     +" from "+i_cTable+" "+i_lTable+" where GSNOMVAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and GSNOME="+cp_ToStrODBC(this.w_JBNOME);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GSNOME',oSource.xKey(1);
                       ,'GSNOMVAL',oSource.xKey(2))
            select GSNOME,GSNOMVAL,GSNOMDES,GSTIPPAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRNOMVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GSNOME,GSNOMVAL,GSNOMDES,GSTIPPAR";
                   +" from "+i_cTable+" "+i_lTable+" where GSNOMVAL="+cp_ToStrODBC(this.w_PRNOMVAL);
                   +" and GSNOME="+cp_ToStrODBC(this.w_JBNOME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GSNOME',this.w_JBNOME;
                       ,'GSNOMVAL',this.w_PRNOMVAL)
            select GSNOME,GSNOMVAL,GSNOMDES,GSTIPPAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRNOMVAL = NVL(_Link_.GSNOMVAL,space(20))
      this.w_PRNOMDES = NVL(_Link_.GSNOMDES,space(254))
      this.w_PRTIPPAR = NVL(_Link_.GSTIPPAR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PRNOMVAL = space(20)
      endif
      this.w_PRNOMDES = space(254)
      this.w_PRTIPPAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GESTSJBD_IDX,2])+'\'+cp_ToStr(_Link_.GSNOME,1)+'\'+cp_ToStr(_Link_.GSNOMVAL,1)
      cp_ShowWarn(i_cKey,this.GESTSJBD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRNOMVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMVAL_2_3.value==this.w_PRNOMVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMVAL_2_3.value=this.w_PRNOMVAL
      replace t_PRNOMVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMVAL_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMDES_2_4.value==this.w_PRNOMDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMDES_2_4.value=this.w_PRNOMDES
      replace t_PRNOMDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMDES_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRFLGPP_2_5.RadioValue()==this.w_PRFLGPP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRFLGPP_2_5.SetRadio()
      replace t_PRFLGPP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRFLGPP_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRVALVAR_2_6.value==this.w_PRVALVAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRVALVAR_2_6.value=this.w_PRVALVAR
      replace t_PRVALVAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRVALVAR_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAR_2_7.RadioValue()==this.w_PRTIPPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAR_2_7.SetRadio()
      replace t_PRTIPPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAR_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'PARAMJ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_PRNOMVAL))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PRFLGPP = this.w_PRFLGPP
    this.o_PRTIPPAR = this.w_PRTIPPAR
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PRNOMVAL)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999999,cp_maxroword()+10)
      .w_PRNOMVAL=space(20)
      .w_PRNOMDES=space(254)
      .w_PRFLGPP=space(1)
      .w_PRVALVAR=space(254)
      .w_PRTIPPAR=space(1)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_PRNOMVAL))
        .link_2_3('Full')
      endif
      .DoRTCalc(5,7,.f.)
        .w_PRVALVAR = IIF(.w_PRFLGPP='P','',.w_PRVALVAR)
        .w_PRTIPPAR = 'V'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_PRNOMVAL = t_PRNOMVAL
    this.w_PRNOMDES = t_PRNOMDES
    this.w_PRFLGPP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRFLGPP_2_5.RadioValue(.t.)
    this.w_PRVALVAR = t_PRVALVAR
    this.w_PRTIPPAR = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAR_2_7.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PRNOMVAL with this.w_PRNOMVAL
    replace t_PRNOMDES with this.w_PRNOMDES
    replace t_PRFLGPP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRFLGPP_2_5.ToRadio()
    replace t_PRVALVAR with this.w_PRVALVAR
    replace t_PRTIPPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAR_2_7.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsjb_mprPag1 as StdContainer
  Width  = 807
  height = 320
  stdWidth  = 807
  stdheight = 320
  resizeXpos=319
  resizeYpos=296
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=9, width=777,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="PRNOMVAL",Label2="Nome parametro",Field3="PRNOMDES",Label3="Descrizione",Field4="PRFLGPP",Label4="Passaggio",Field5="PRVALVAR",Label5="Valore parametro",Field6="PRTIPPAR",Label6="Tipo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168606586

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=28,;
    width=773+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=29,width=772+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GESTSJBD|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GESTSJBD'
        oDropInto=this.oBodyCol.oRow.oPRNOMVAL_2_3
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_8 as StdButton with uid="UUKTLYUEHQ",width=18,height=19,;
   left=785, top=30,;
    caption="...", nPag=2;
    , ToolTipText = "Selezione parametro";
    , HelpContextID = 147283242;
  , bGlobalFont=.t.

    proc oBtn_2_8.Click()
      do GSJB_KPR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRFLGPP<>'C')
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsjb_mprBodyRow as CPBodyRowCnt
  Width=763
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="ZNAOJPLYXH",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 200935018,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, cSayPict=["9999999"], cGetPict=["9999999"]

  add object oPRNOMVAL_2_3 as StdTrsField with uid="FQYFGICPIM",rtseq=4,rtrep=.t.,;
    cFormVar="w_PRNOMVAL",value=space(20),;
    ToolTipText = "Nome parametro",;
    HelpContextID = 39438658,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=105, Left=64, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="GESTSJBD", cZoomOnZoom="GSJB_MGS", oKey_1_1="GSNOME", oKey_1_2="this.w_JBNOME", oKey_2_1="GSNOMVAL", oKey_2_2="this.w_PRNOMVAL"

  func oPRNOMVAL_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRNOMVAL_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRNOMVAL_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.GESTSJBD_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"GSNOME="+cp_ToStrODBC(this.Parent.oContained.w_JBNOME)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"GSNOME="+cp_ToStr(this.Parent.oContained.w_JBNOME)
    endif
    do cp_zoom with 'GESTSJBD','*','GSNOME,GSNOMVAL',cp_AbsName(this.parent,'oPRNOMVAL_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSJB_MGS',"Selezione parametro",'gsjb_mpr.GESTSJBD_VZM',this.parent.oContained
  endproc
  proc oPRNOMVAL_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSJB_MGS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.GSNOME=w_JBNOME
     i_obj.w_GSNOMVAL=this.parent.oContained.w_PRNOMVAL
    i_obj.ecpSave()
  endproc

  add object oPRNOMDES_2_4 as StdTrsField with uid="FFZWZSZMCY",rtseq=6,rtrep=.t.,;
    cFormVar="w_PRNOMDES",value=space(254),;
    ToolTipText = "Descrizione parametro",;
    HelpContextID = 5884233,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=195, Left=173, Top=0, InputMask=replicate('X',254)

  add object oPRFLGPP_2_5 as StdTrsCombo with uid="SVATYAMNXG",rtrep=.t.,;
    cFormVar="w_PRFLGPP", RowSource=""+"Passa,"+"Riceve,"+"''" , ;
    ToolTipText = "Flag per passaggio parametri",;
    HelpContextID = 67745546,;
    Height=21, Width=63, Left=374, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPRFLGPP_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRFLGPP,&i_cF..t_PRFLGPP),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'C',;
    iif(xVal =3,' ',;
    space(1)))))
  endfunc
  func oPRFLGPP_2_5.GetRadio()
    this.Parent.oContained.w_PRFLGPP = this.RadioValue()
    return .t.
  endfunc

  func oPRFLGPP_2_5.ToRadio()
    this.Parent.oContained.w_PRFLGPP=trim(this.Parent.oContained.w_PRFLGPP)
    return(;
      iif(this.Parent.oContained.w_PRFLGPP=='P',1,;
      iif(this.Parent.oContained.w_PRFLGPP=='C',2,;
      iif(this.Parent.oContained.w_PRFLGPP=='',3,;
      0))))
  endfunc

  func oPRFLGPP_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPRFLGPP_2_5.mCond()
    with this.Parent.oContained
      return (.w_PRTIPPAR='V')
    endwith
  endfunc

  add object oPRVALVAR_2_6 as StdTrsField with uid="GARJQMMKCV",rtseq=8,rtrep=.t.,;
    cFormVar="w_PRVALVAR",value=space(254),;
    ToolTipText = "Valore parametro",;
    HelpContextID = 37505352,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=232, Left=444, Top=0, InputMask=replicate('X',254), bHasZoom = .t. 

  func oPRVALVAR_2_6.mCond()
    with this.Parent.oContained
      return (.w_PRFLGPP<>'P' AND .w_PRTIPPAR='V')
    endwith
  endfunc

  proc oPRVALVAR_2_6.mZoom
      with this.Parent.oContained
        GSAR_BMZ(this.Parent.oContained,this,"GSUT_KMV")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPRTIPPAR_2_7 as StdTrsCombo with uid="PFYKXTYYDD",rtrep=.t.,;
    cFormVar="w_PRTIPPAR", RowSource=""+"Variabile,"+"Bottone" , ;
    HelpContextID = 58447544,;
    Height=21, Width=78, Left=680, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPRTIPPAR_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPPAR,&i_cF..t_PRTIPPAR),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'B',;
    space(1))))
  endfunc
  func oPRTIPPAR_2_7.GetRadio()
    this.Parent.oContained.w_PRTIPPAR = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPPAR_2_7.ToRadio()
    this.Parent.oContained.w_PRTIPPAR=trim(this.Parent.oContained.w_PRTIPPAR)
    return(;
      iif(this.Parent.oContained.w_PRTIPPAR=='V',1,;
      iif(this.Parent.oContained.w_PRTIPPAR=='B',2,;
      0)))
  endfunc

  func oPRTIPPAR_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPRTIPPAR_2_7.mCond()
    with this.Parent.oContained
      return (.oParentObject .w_JBROUTINE<>'R')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_mpr','PARAMJ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRJOBCOD=PARAMJ.PRJOBCOD";
  +" and "+i_cAliasName2+".PRCODPAR=PARAMJ.PRCODPAR";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
