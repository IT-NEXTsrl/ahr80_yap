* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bes                                                        *
*              Esempio per proattivit�                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-12                                                      *
* Last revis.: 2009-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParametro
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bes",oParentObject,m.pParametro)
return(i_retval)

define class tgsjb_bes as StdBatch
  * --- Local variables
  pParametro = space(10)
  w_QUERY = space(50)
  w_CURSORE = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di esempio che crea un cursore utilizzato dalla proattivita
    * --- Il primo parametro, obbligatoriamente, � il nome assegnato al cursore
    *     dall'esecutore della proattivit�
    this.w_QUERY = "c:\prova\prova.vqr"
    this.w_CURSORE = this.pParametro
    VQ_EXEC( this.w_QUERY, THIS.OPARENTOBJECT, this.w_CURSORE )
  endproc


  proc Init(oParentObject,pParametro)
    this.pParametro=pParametro
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParametro"
endproc
