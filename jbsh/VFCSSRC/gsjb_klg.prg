* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_klg                                                        *
*              Visualizzazione log di elaborazione                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-27                                                      *
* Last revis.: 2010-06-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsjb_klg",oParentObject))

* --- Class definition
define class tgsjb_klg as StdForm
  Top    = 3
  Left   = 3

  * --- Standard Properties
  Width  = 673
  Height = 487
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-03"
  HelpContextID=216690281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  SCHEDJOB_IDX = 0
  LOGELAJB_IDX = 0
  cPrg = "gsjb_klg"
  cComment = "Visualizzazione log di elaborazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ULTELA = space(1)
  o_ULTELA = space(1)
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_LOG = space(10)
  w_CPROWNUM = 0
  w_MEMO = space(0)
  w_LSCODJOB = 0
  w_SHTIPSCH = space(1)
  w_ZoomLog = .NULL.
  w_ZoomProc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsjb_klgPag1","gsjb_klg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oULTELA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomLog = this.oPgFrm.Pages(1).oPag.ZoomLog
    this.w_ZoomProc = this.oPgFrm.Pages(1).oPag.ZoomProc
    DoDefault()
    proc Destroy()
      this.w_ZoomLog = .NULL.
      this.w_ZoomProc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='SCHEDJOB'
    this.cWorkTables[2]='LOGELAJB'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ULTELA=space(1)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_LOG=space(10)
      .w_CPROWNUM=0
      .w_MEMO=space(0)
      .w_LSCODJOB=0
      .w_SHTIPSCH=space(1)
        .w_ULTELA = 'S'
        .w_DATAINI = iif(.w_ULTELA='S',cp_CharToDate('  -  -    '),i_DATSYS)
        .w_DATAFIN = iif(.w_ULTELA='S',cp_CharToDate('  -  -    '),i_DATSYS)
      .oPgFrm.Page1.oPag.ZoomLog.Calculate(.w_ULTELA)
        .w_LOG = .w_ZoomLog.getvar('LSSERIAL')
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_LOG))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomProc.Calculate(.w_LOG)
        .w_CPROWNUM = .w_ZoomProc.getvar('CPROWNUM')
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CPROWNUM))
          .link_1_8('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_LSCODJOB = .w_ZoomLog.getvar('LSCODJOB')
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_ULTELA<>.w_ULTELA
            .w_DATAINI = iif(.w_ULTELA='S',cp_CharToDate('  -  -    '),i_DATSYS)
        endif
        if .o_ULTELA<>.w_ULTELA
            .w_DATAFIN = iif(.w_ULTELA='S',cp_CharToDate('  -  -    '),i_DATSYS)
        endif
        .oPgFrm.Page1.oPag.ZoomLog.Calculate(.w_ULTELA)
            .w_LOG = .w_ZoomLog.getvar('LSSERIAL')
          .link_1_6('Full')
        .oPgFrm.Page1.oPag.ZoomProc.Calculate(.w_LOG)
            .w_CPROWNUM = .w_ZoomProc.getvar('CPROWNUM')
          .link_1_8('Full')
        .DoRTCalc(6,6,.t.)
            .w_LSCODJOB = .w_ZoomLog.getvar('LSCODJOB')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomLog.Calculate(.w_ULTELA)
        .oPgFrm.Page1.oPag.ZoomProc.Calculate(.w_LOG)
    endwith
  return

  proc Calculate_RULJOZNICA()
    with this
          * --- Doppio click su zoom
          gsjb_bsl(this;
              ,'L';
              ,.w_LOG;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATAINI_1_2.enabled = this.oPgFrm.Page1.oPag.oDATAINI_1_2.mCond()
    this.oPgFrm.Page1.oPag.oDATAFIN_1_3.enabled = this.oPgFrm.Page1.oPag.oDATAFIN_1_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomLog.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomProc.Event(cEvent)
        if lower(cEvent)==lower("w_zoomlog selected")
          .Calculate_RULJOZNICA()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LOG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_lTable = "LOGELAJB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2], .t., this.LOGELAJB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL,LSNOTELA";
                   +" from "+i_cTable+" "+i_lTable+" where LSSERIAL="+cp_ToStrODBC(this.w_LOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',this.w_LOG)
            select LSSERIAL,LSNOTELA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOG = NVL(_Link_.LSSERIAL,space(10))
      this.w_MEMO = NVL(_Link_.LSNOTELA,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_LOG = space(10)
      endif
      this.w_MEMO = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])+'\'+cp_ToStr(_Link_.LSSERIAL,1)
      cp_ShowWarn(i_cKey,this.LOGELAJB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CPROWNUM
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_lTable = "LOGELAJB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2], .t., this.LOGELAJB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPROWNUM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPROWNUM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL,CPROWNUM,LSNOTELA";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM);
                   +" and LSSERIAL="+cp_ToStrODBC(this.w_LOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',this.w_LOG;
                       ,'CPROWNUM',this.w_CPROWNUM)
            select LSSERIAL,CPROWNUM,LSNOTELA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPROWNUM = NVL(_Link_.CPROWNUM,0)
      this.w_MEMO = NVL(_Link_.LSNOTELA,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_CPROWNUM = 0
      endif
      this.w_MEMO = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])+'\'+cp_ToStr(_Link_.LSSERIAL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.LOGELAJB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPROWNUM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oULTELA_1_1.RadioValue()==this.w_ULTELA)
      this.oPgFrm.Page1.oPag.oULTELA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_2.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_2.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_3.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_3.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMEMO_1_9.value==this.w_MEMO)
      this.oPgFrm.Page1.oPag.oMEMO_1_9.value=this.w_MEMO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_DATAFIN) or (.w_DATAFIN>=.w_DATAINI))  and (.w_ULTELA<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data inesistente o maggiore della data finale")
          case   not(empty(.w_DATAINI) or (.w_DATAFIN>=.w_DATAINI))  and (.w_ULTELA<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data inesistente o minore della data iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ULTELA = this.w_ULTELA
    return

enddefine

* --- Define pages as container
define class tgsjb_klgPag1 as StdContainer
  Width  = 669
  height = 487
  stdWidth  = 669
  stdheight = 487
  resizeXpos=367
  resizeYpos=353
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oULTELA_1_1 as StdCheck with uid="WWSPCTYVDP",rtseq=1,rtrep=.f.,left=21, top=6, caption="Ultime elaborazioni",;
    ToolTipText = "Visualizza solo le ultime elaborazioni per ogni job eseguito",;
    HelpContextID = 153101126,;
    cFormVar="w_ULTELA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oULTELA_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oULTELA_1_1.GetRadio()
    this.Parent.oContained.w_ULTELA = this.RadioValue()
    return .t.
  endfunc

  func oULTELA_1_1.SetRadio()
    this.Parent.oContained.w_ULTELA=trim(this.Parent.oContained.w_ULTELA)
    this.value = ;
      iif(this.Parent.oContained.w_ULTELA=='S',1,;
      iif(this.Parent.oContained.w_ULTELA=='N',2,;
      0))
  endfunc

  add object oDATAINI_1_2 as StdField with uid="TEJGPSZTRQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inesistente o maggiore della data finale",;
    ToolTipText = "Data di inizio",;
    HelpContextID = 169076938,;
   bGlobalFont=.t.,;
    Height=22, Width=76, Left=276, Top=10

  func oDATAINI_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ULTELA<>'S')
    endwith
   endif
  endfunc

  func oDATAINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATAFIN) or (.w_DATAFIN>=.w_DATAINI))
    endwith
    return bRes
  endfunc

  add object oDATAFIN_1_3 as StdField with uid="GHGHGHCKQF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inesistente o minore della data iniziale",;
    ToolTipText = "Data di fine",;
    HelpContextID = 12326710,;
   bGlobalFont=.t.,;
    Height=22, Width=76, Left=433, Top=10

  func oDATAFIN_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ULTELA<>'S')
    endwith
   endif
  endfunc

  func oDATAFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATAINI) or (.w_DATAFIN>=.w_DATAINI))
    endwith
    return bRes
  endfunc


  add object oBtn_1_4 as StdButton with uid="RCIDTHNZYU",left=604, top=7, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare la ricerca";
    , HelpContextID = 228667414;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .notifyEvent('Aggiorna')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomLog as cp_zoombox with uid="FZCJQMGNSZ",left=5, top=75, width=403,height=211,;
    caption='ZoomLog',;
   bGlobalFont=.t.,;
    cTable="LOGELAJB",cZoomFile="LogGroupJob",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSJB_MLG",bRetriveAllRows=.f.,;
    cEvent = "Init, Aggiorna",;
    nPag=1;
    , HelpContextID = 146147690


  add object ZoomProc as cp_zoombox with uid="LAXZSTRWNY",left=410, top=75, width=250,height=211,;
    caption='ZoomProc',;
   bGlobalFont=.t.,;
    cTable="LOGELAJB",cZoomFile="LogProcessi",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 176813817

  add object oMEMO_1_9 as StdMemo with uid="IBTFGCFGJI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MEMO", cQueryName = "MEMO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Informazioni sul processo",;
    HelpContextID = 211178554,;
   bGlobalFont=.t.,;
    Height=125, Width=654, Left=7, Top=308, readonly=.t.


  add object oBtn_1_10 as StdButton with uid="PFGJOCUVTN",left=8, top=437, width=48,height=45,;
    CpPicture="bmp\log.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al log di elaborazione";
    , HelpContextID = 216238154;
    , Caption='\<Log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        gsjb_bsl(this.Parent.oContained,"L",.w_LOG)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="RLGNBYGPTA",left=59, top=437, width=48,height=45,;
    CpPicture="bmp\stampa.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alle stampe di elaborazione";
    , HelpContextID = 264086347;
    , Caption='S\<tampe';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      do GSJB_KJS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="MWCSXCBXIE",left=110, top=437, width=55,height=45,;
    CpPicture="bmp\schedulatore.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla gestione";
    , HelpContextID = 153168843;
    , Caption='\<Gestione';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSJB_BSL(this.Parent.oContained,"S",.w_LSCODJOB)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_13 as StdString with uid="PHPGHSMAES",Visible=.t., Left=7, Top=58,;
    Alignment=0, Width=208, Height=18,;
    Caption="Elenco log"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="AQLYKFPKAH",Visible=.t., Left=10, Top=290,;
    Alignment=0, Width=208, Height=18,;
    Caption="Messaggio di log"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WHYUFNUTUE",Visible=.t., Left=411, Top=59,;
    Alignment=0, Width=208, Height=18,;
    Caption="Elenco processi"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PCXNIIHJLB",Visible=.t., Left=196, Top=10,;
    Alignment=1, Width=77, Height=23,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="BGVUJZSSRB",Visible=.t., Left=362, Top=10,;
    Alignment=1, Width=68, Height=23,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oBox_1_18 as StdBox with uid="SFUZRULWUK",left=7, top=3, width=653,height=53
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_klg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
