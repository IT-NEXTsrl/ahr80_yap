* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_kcp                                                        *
*              Copia processo fra aziende                                      *
*                                                                              *
*      Author: ZUCCHETTI SPA - CS                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-19                                                      *
* Last revis.: 2009-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsjb_kcp",oParentObject))

* --- Class definition
define class tgsjb_kcp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 720
  Height = 514
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-01-08"
  HelpContextID=169185687
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  SCHEDJOB_IDX = 0
  LISTAJOB_IDX = 0
  cPrg = "gsjb_kcp"
  cComment = "Copia processo fra aziende"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_JOB_NAME = 0
  o_JOB_NAME = 0
  w_SHDESCRI = space(254)
  w_JOB_PROC = space(20)
  w_JOB_ROW = 0
  w_JBDESC = space(254)
  w_JBAZI = space(5)
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_SHTIPSCH = space(1)
  w_ZOOMPARA = .NULL.
  w_ZOOMAZI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsjb_kcpPag1","gsjb_kcp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oJOB_NAME_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMPARA = this.oPgFrm.Pages(1).oPag.ZOOMPARA
    this.w_ZOOMAZI = this.oPgFrm.Pages(1).oPag.ZOOMAZI
    DoDefault()
    proc Destroy()
      this.w_ZOOMPARA = .NULL.
      this.w_ZOOMAZI = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='SCHEDJOB'
    this.cWorkTables[2]='LISTAJOB'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_JOB_NAME=0
      .w_SHDESCRI=space(254)
      .w_JOB_PROC=space(20)
      .w_JOB_ROW=0
      .w_JBDESC=space(254)
      .w_JBAZI=space(5)
      .w_SELEZI=space(1)
      .w_SHTIPSCH=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_JOB_NAME))
          .link_1_3('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_JOB_PROC = ""
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_JOB_ROW))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOMPARA.Calculate()
      .oPgFrm.Page1.oPag.ZOOMAZI.Calculate()
          .DoRTCalc(5,6,.f.)
        .w_SELEZI = 'S'
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_JOB_NAME<>.w_JOB_NAME
            .w_JOB_PROC = ""
        endif
        .oPgFrm.Page1.oPag.ZOOMPARA.Calculate()
        .oPgFrm.Page1.oPag.ZOOMAZI.Calculate()
        if .o_SELEZI<>.w_SELEZI
          .Calculate_OFBNUHFCRQ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMPARA.Calculate()
        .oPgFrm.Page1.oPag.ZOOMAZI.Calculate()
    endwith
  return

  proc Calculate_JATNPLLUPB()
    with this
          * --- Esegue aggiornamento
          gsjb_bcp(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_OFBNUHFCRQ()
    with this
          * --- Seleziona deseleziona
          gsjb_bcp(this;
              ,'S';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oJOB_PROC_1_5.enabled = this.oPgFrm.Page1.oPag.oJOB_PROC_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oJOB_ROW_1_6.visible=!this.oPgFrm.Page1.oPag.oJOB_ROW_1_6.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMPARA.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMAZI.Event(cEvent)
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_JATNPLLUPB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=JOB_NAME
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_lTable = "SCHEDJOB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2], .t., this.SCHEDJOB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_JOB_NAME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SCHEDJOB')
        if i_nConn<>0
          i_cWhere = " SHCODICE="+cp_ToStrODBC(this.w_JOB_NAME);

          i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SHCODICE',this.w_JOB_NAME)
          select SHCODICE,SHDESCRI,SHTIPSCH;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = " 1=2";

            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = " 1=2";

            select SHCODICE,SHDESCRI,SHTIPSCH;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_JOB_NAME) and !this.bDontReportError
            deferred_cp_zoom('SCHEDJOB','*','SHCODICE',cp_AbsName(oSource.parent,'oJOB_NAME_1_3'),i_cWhere,'',"Job schedulati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                     +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',oSource.xKey(1))
            select SHCODICE,SHDESCRI,SHTIPSCH;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_JOB_NAME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                   +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(this.w_JOB_NAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',this.w_JOB_NAME)
            select SHCODICE,SHDESCRI,SHTIPSCH;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_JOB_NAME = NVL(_Link_.SHCODICE,0)
      this.w_SHDESCRI = NVL(_Link_.SHDESCRI,space(254))
      this.w_SHTIPSCH = NVL(_Link_.SHTIPSCH,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_JOB_NAME = 0
      endif
      this.w_SHDESCRI = space(254)
      this.w_SHTIPSCH = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NVL( .w_SHTIPSCH , "N" ) = "N"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_JOB_NAME = 0
        this.w_SHDESCRI = space(254)
        this.w_SHTIPSCH = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])+'\'+cp_ToStr(_Link_.SHCODICE,1)
      cp_ShowWarn(i_cKey,this.SCHEDJOB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_JOB_NAME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=JOB_ROW
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTAJOB_IDX,3]
    i_lTable = "LISTAJOB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2], .t., this.LISTAJOB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_JOB_ROW) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTAJOB')
        if i_nConn<>0
          i_cWhere = " CPROWNUM="+cp_ToStrODBC(this.w_JOB_ROW);
                   +" and JBCODICEJOB="+cp_ToStrODBC(this.w_JOB_NAME);

          i_ret=cp_SQL(i_nConn,"select JBCODICEJOB,CPROWNUM,JBNOME,JBDESC,JBCODAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'JBCODICEJOB',this.w_JOB_NAME;
                     ,'CPROWNUM',this.w_JOB_ROW)
          select JBCODICEJOB,CPROWNUM,JBNOME,JBDESC,JBCODAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_JOB_ROW) and !this.bDontReportError
            deferred_cp_zoom('LISTAJOB','*','JBCODICEJOB,CPROWNUM',cp_AbsName(oSource.parent,'oJOB_ROW_1_6'),i_cWhere,'',"",'GSJB_KCP.LISTAJOB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_JOB_NAME<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select JBCODICEJOB,CPROWNUM,JBNOME,JBDESC,JBCODAZI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select JBCODICEJOB,CPROWNUM,JBNOME,JBDESC,JBCODAZI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select JBCODICEJOB,CPROWNUM,JBNOME,JBDESC,JBCODAZI";
                     +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(oSource.xKey(2));
                     +" and JBCODICEJOB="+cp_ToStrODBC(this.w_JOB_NAME);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'JBCODICEJOB',oSource.xKey(1);
                       ,'CPROWNUM',oSource.xKey(2))
            select JBCODICEJOB,CPROWNUM,JBNOME,JBDESC,JBCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_JOB_ROW)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select JBCODICEJOB,CPROWNUM,JBNOME,JBDESC,JBCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_JOB_ROW);
                   +" and JBCODICEJOB="+cp_ToStrODBC(this.w_JOB_NAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'JBCODICEJOB',this.w_JOB_NAME;
                       ,'CPROWNUM',this.w_JOB_ROW)
            select JBCODICEJOB,CPROWNUM,JBNOME,JBDESC,JBCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_JOB_ROW = NVL(_Link_.CPROWNUM,0)
      this.w_JOB_PROC = NVL(_Link_.JBNOME,space(20))
      this.w_JBDESC = NVL(_Link_.JBDESC,space(254))
      this.w_JBAZI = NVL(_Link_.JBCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_JOB_ROW = 0
      endif
      this.w_JOB_PROC = space(20)
      this.w_JBDESC = space(254)
      this.w_JBAZI = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2])+'\'+cp_ToStr(_Link_.JBCODICEJOB,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.LISTAJOB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_JOB_ROW Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oJOB_NAME_1_3.value==this.w_JOB_NAME)
      this.oPgFrm.Page1.oPag.oJOB_NAME_1_3.value=this.w_JOB_NAME
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDESCRI_1_4.value==this.w_SHDESCRI)
      this.oPgFrm.Page1.oPag.oSHDESCRI_1_4.value=this.w_SHDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oJOB_PROC_1_5.value==this.w_JOB_PROC)
      this.oPgFrm.Page1.oPag.oJOB_PROC_1_5.value=this.w_JOB_PROC
    endif
    if not(this.oPgFrm.Page1.oPag.oJOB_ROW_1_6.value==this.w_JOB_ROW)
      this.oPgFrm.Page1.oPag.oJOB_ROW_1_6.value=this.w_JOB_ROW
    endif
    if not(this.oPgFrm.Page1.oPag.oJBDESC_1_7.value==this.w_JBDESC)
      this.oPgFrm.Page1.oPag.oJBDESC_1_7.value=this.w_JBDESC
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_13.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_13.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(NVL( .w_SHTIPSCH , "N" ) = "N")  and not(empty(.w_JOB_NAME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oJOB_NAME_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_JOB_NAME = this.w_JOB_NAME
    this.o_SELEZI = this.w_SELEZI
    return

enddefine

* --- Define pages as container
define class tgsjb_kcpPag1 as StdContainer
  Width  = 716
  height = 514
  stdWidth  = 716
  stdheight = 514
  resizeXpos=373
  resizeYpos=405
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oJOB_NAME_1_3 as StdField with uid="DIHTAMQIRI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_JOB_NAME", cQueryName = "JOB_NAME",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nome del job",;
    HelpContextID = 262601253,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=83, Top=16, cSayPict='"9999999999"', cGetPict='"9999999999"', bHasZoom = .t. , cLinkFile="SCHEDJOB", oKey_1_1="SHCODICE", oKey_1_2="this.w_JOB_NAME"

  func oJOB_NAME_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_JOB_ROW)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oJOB_NAME_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oJOB_NAME_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SCHEDJOB','*','SHCODICE',cp_AbsName(this.parent,'oJOB_NAME_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Job schedulati",'',this.parent.oContained
  endproc

  add object oSHDESCRI_1_4 as StdField with uid="CAXTYSFALK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SHDESCRI", cQueryName = "SHDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 225501329,;
   bGlobalFont=.t.,;
    Height=21, Width=462, Left=239, Top=17, InputMask=replicate('X',254)

  add object oJOB_PROC_1_5 as StdField with uid="PNAKMCTOSW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_JOB_PROC", cQueryName = "JOB_PROC",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Procedura da eseguire",;
    HelpContextID = 243726887,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=83, Top=45, InputMask=replicate('X',20), bHasZoom = .t. 

  func oJOB_PROC_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_JOB_NAME))
    endwith
   endif
  endfunc

  func oJOB_PROC_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      .w_JOB_ROW = .w_JOB_PROC
      * bres=this.parent.oJOB_ROW_1_6.Check()
      bres=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oJOB_PROC_1_5.mZoom
    this.parent.oJOB_ROW_1_6.mZoom()
  endproc

  add object oJOB_ROW_1_6 as StdField with uid="TXQRYAJABX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_JOB_ROW", cQueryName = "JOB_ROW",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 244909462,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=83, Top=65, bHasZoom = .t. , cLinkFile="LISTAJOB", oKey_1_1="JBCODICEJOB", oKey_1_2="this.w_JOB_NAME", oKey_2_1="CPROWNUM", oKey_2_2="this.w_JOB_ROW"

  func oJOB_ROW_1_6.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  func oJOB_ROW_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oJOB_ROW_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oJOB_ROW_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTAJOB_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"JBCODICEJOB="+cp_ToStrODBC(this.Parent.oContained.w_JOB_NAME)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"JBCODICEJOB="+cp_ToStr(this.Parent.oContained.w_JOB_NAME)
    endif
    do cp_zoom with 'LISTAJOB','*','JBCODICEJOB,CPROWNUM',cp_AbsName(this.parent,'oJOB_ROW_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSJB_KCP.LISTAJOB_VZM',this.parent.oContained
  endproc

  add object oJBDESC_1_7 as StdField with uid="ZOBKJUVLZE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_JBDESC", cQueryName = "JBDESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 225503082,;
   bGlobalFont=.t.,;
    Height=22, Width=463, Left=239, Top=45, InputMask=replicate('X',254)


  add object ZOOMPARA as cp_zoombox with uid="HRXYGPVUYK",left=16, top=121, width=375,height=369,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PARAMJ",cZoomFile="GSJB_KCP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "ShowDati",;
    nPag=1;
    , HelpContextID = 189687578


  add object ZOOMAZI as cp_szoombox with uid="NMCIAVXUBT",left=398, top=121, width=309,height=334,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="AZIENDA",cZoomFile="GSJB_KCP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "ShowDati",;
    nPag=1;
    , HelpContextID = 189687578

  add object oSELEZI_1_13 as StdRadio with uid="LJPNRGPOIY",rtseq=7,rtrep=.f.,left=398, top=461, width=162,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_13.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 117466074
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 117466074
      this.Buttons(2).Top=15
      this.SetAll("Width",160)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_13.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_13.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_14 as StdButton with uid="PXBGFOADEM",left=659, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8303850;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="TSXESUULAA",left=601, top=463, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Conferma la copia dei processi nelle aziende";
    , HelpContextID = 85465721;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        .NotifyEvent('Aggiorna')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_JOB_NAME) AND !EMPTY(.w_JOB_ROW))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="WWOWKJBQZB",left=659, top=73, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Ricerca dati in base a selezione";
    , HelpContextID = 190762986;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .NotifyEvent('ShowDati')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_JOB_NAME) AND !EMPTY(.w_JOB_ROW))
      endwith
    endif
  endfunc

  add object oStr_1_1 as StdString with uid="VONCCKBOFF",Visible=.t., Left=8, Top=45,;
    Alignment=1, Width=71, Height=18,;
    Caption="Processo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="MDUYYQCZCH",Visible=.t., Left=23, Top=21,;
    Alignment=1, Width=56, Height=18,;
    Caption="Job:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="UQWKNGOLJR",Visible=.t., Left=18, Top=103,;
    Alignment=0, Width=135, Height=18,;
    Caption="Parametri"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="PWYLVWHKET",Visible=.t., Left=399, Top=99,;
    Alignment=0, Width=149, Height=18,;
    Caption="Aziende"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_kcp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
