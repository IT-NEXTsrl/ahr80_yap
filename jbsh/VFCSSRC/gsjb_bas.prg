* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bas                                                        *
*              Controlli account di rete                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-03                                                      *
* Last revis.: 2007-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bas",oParentObject)
return(i_retval)

define class tgsjb_bas as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if alltrim(this.oParentObject.w_ZSPSWD)=="**********"
      ah_errormsg("Inserire una password valida")
      i_retcode = 'stop'
      return
    endif
    if alltrim(this.oParentObject.w_ZSPSWD) == alltrim(this.oParentObject.w_ZSCOPSWD)
      this.oParentObject.w_ZSPSWD = alltrim(this.oParentObject.w_ZSPSWD)
      this.oParentObject.w_ZSCOPSWD = alltrim(this.oParentObject.w_ZSCOPSWD)
      this.oParentObject.w_ZSBTNOK = "S"
      this.oParentObject.ecpsave()
    else
      ah_errormsg("Le password digitate non corrispondono")
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
