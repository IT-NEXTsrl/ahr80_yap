* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_mjb                                                        *
*              Processi                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-12                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsjb_mjb")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsjb_mjb")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsjb_mjb")
  return

* --- Class definition
define class tgsjb_mjb as StdPCForm
  Width  = 656
  Height = 400
  Top    = 3
  Left   = 4
  cComment = "Processi"
  cPrg = "gsjb_mjb"
  HelpContextID=248147561
  add object cnt as tcgsjb_mjb
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsjb_mjb as PCContext
  w_JBCODICEJOB = 0
  w_CPROWORD = 0
  w_JBNOME = space(20)
  w_JBROUTINE = space(1)
  w_JBCODAZI = space(5)
  w_JBDESC = space(254)
  w_JBTIPESC = space(1)
  w_CODUTE = 0
  w_JBCODICEJOB = 0
  w_SHTIPSCH = space(1)
  proc Save(i_oFrom)
    this.w_JBCODICEJOB = i_oFrom.w_JBCODICEJOB
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_JBNOME = i_oFrom.w_JBNOME
    this.w_JBROUTINE = i_oFrom.w_JBROUTINE
    this.w_JBCODAZI = i_oFrom.w_JBCODAZI
    this.w_JBDESC = i_oFrom.w_JBDESC
    this.w_JBTIPESC = i_oFrom.w_JBTIPESC
    this.w_CODUTE = i_oFrom.w_CODUTE
    this.w_JBCODICEJOB = i_oFrom.w_JBCODICEJOB
    this.w_SHTIPSCH = i_oFrom.w_SHTIPSCH
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_JBCODICEJOB = this.w_JBCODICEJOB
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_JBNOME = this.w_JBNOME
    i_oTo.w_JBROUTINE = this.w_JBROUTINE
    i_oTo.w_JBCODAZI = this.w_JBCODAZI
    i_oTo.w_JBDESC = this.w_JBDESC
    i_oTo.w_JBTIPESC = this.w_JBTIPESC
    i_oTo.w_CODUTE = this.w_CODUTE
    i_oTo.w_JBCODICEJOB = this.w_JBCODICEJOB
    i_oTo.w_SHTIPSCH = this.w_SHTIPSCH
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsjb_mjb as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 656
  Height = 400
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=248147561
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  LISTAJOB_IDX = 0
  AZIENDA_IDX = 0
  UTE_AZI_IDX = 0
  GESTSJBM_IDX = 0
  cFile = "LISTAJOB"
  cKeySelect = "JBCODICEJOB,JBCODICEJOB"
  cKeyWhere  = "JBCODICEJOB=this.w_JBCODICEJOB and JBCODICEJOB=this.w_JBCODICEJOB"
  cKeyDetail  = "JBCODICEJOB=this.w_JBCODICEJOB and JBCODICEJOB=this.w_JBCODICEJOB"
  cKeyWhereODBC = '"JBCODICEJOB="+cp_ToStrODBC(this.w_JBCODICEJOB)';
      +'+" and JBCODICEJOB="+cp_ToStrODBC(this.w_JBCODICEJOB)';

  cKeyDetailWhereODBC = '"JBCODICEJOB="+cp_ToStrODBC(this.w_JBCODICEJOB)';
      +'+" and JBCODICEJOB="+cp_ToStrODBC(this.w_JBCODICEJOB)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"LISTAJOB.JBCODICEJOB="+cp_ToStrODBC(this.w_JBCODICEJOB)';
      +'+" and LISTAJOB.JBCODICEJOB="+cp_ToStrODBC(this.w_JBCODICEJOB)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'LISTAJOB.CPROWORD'
  cPrg = "gsjb_mjb"
  cComment = "Processi"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 17
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_JBCODICEJOB = 0
  w_CPROWORD = 0
  w_JBNOME = space(20)
  w_JBROUTINE = space(1)
  w_JBCODAZI = space(5)
  w_JBDESC = space(254)
  w_JBTIPESC = space(1)
  w_CODUTE = 0
  w_JBCODICEJOB = 0
  w_SHTIPSCH = space(1)

  * --- Children pointers
  GSJB_MPR = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSJB_MPR additive
    with this
      .Pages(1).addobject("oPag","tgsjb_mjbPag1","gsjb_mjb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSJB_MPR
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='UTE_AZI'
    this.cWorkTables[3]='GESTSJBM'
    this.cWorkTables[4]='LISTAJOB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LISTAJOB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LISTAJOB_IDX,3]
  return

  function CreateChildren()
    this.GSJB_MPR = CREATEOBJECT('stdLazyChild',this,'GSJB_MPR')
    return

  procedure NewContext()
    return(createobject('tsgsjb_mjb'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSJB_MPR)
      this.GSJB_MPR.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSJB_MPR.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSJB_MPR)
      this.GSJB_MPR.DestroyChildrenChain()
      this.GSJB_MPR=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSJB_MPR.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSJB_MPR.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSJB_MPR.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSJB_MPR.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_JBCODICEJOB,"PRJOBCOD";
             ,.w_CPROWNUM,"PRCODPAR";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from LISTAJOB where JBCODICEJOB=KeySet.JBCODICEJOB
    *                            and JBCODICEJOB=KeySet.JBCODICEJOB
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.LISTAJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2],this.bLoadRecFilter,this.LISTAJOB_IDX,"gsjb_mjb")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LISTAJOB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LISTAJOB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LISTAJOB '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'JBCODICEJOB',this.w_JBCODICEJOB  ,'JBCODICEJOB',this.w_JBCODICEJOB  )
      select * from (i_cTable) LISTAJOB where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SHTIPSCH = space(1)
        .w_JBCODICEJOB = NVL(JBCODICEJOB,0)
        .w_JBCODICEJOB = NVL(JBCODICEJOB,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'LISTAJOB')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_JBNOME = NVL(JBNOME,space(20))
          * evitabile
          *.link_2_2('Load')
          .w_JBROUTINE = NVL(JBROUTINE,space(1))
          .w_JBCODAZI = NVL(JBCODAZI,space(5))
          * evitabile
          *.link_2_4('Load')
          .w_JBDESC = NVL(JBDESC,space(254))
          .w_JBTIPESC = NVL(JBTIPESC,space(1))
        .w_CODUTE = i_CODUTE
          .link_2_7('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_JBCODICEJOB=0
      .w_CPROWORD=10
      .w_JBNOME=space(20)
      .w_JBROUTINE=space(1)
      .w_JBCODAZI=space(5)
      .w_JBDESC=space(254)
      .w_JBTIPESC=space(1)
      .w_CODUTE=0
      .w_JBCODICEJOB=0
      .w_SHTIPSCH=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_JBNOME = IIF( .w_SHTIPSCH = "P" , "GSJB_BXP WITH THIS" , SPACE( 20 ) )
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_JBNOME))
         .link_2_2('Full')
        endif
        .w_JBROUTINE = 'R'
        .w_JBCODAZI = i_CODAZI
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_JBCODAZI))
         .link_2_4('Full')
        endif
        .DoRTCalc(6,6,.f.)
        .w_JBTIPESC = 'N'
        .w_CODUTE = i_CODUTE
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODUTE))
         .link_2_7('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'LISTAJOB')
    this.DoRTCalc(9,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSJB_MPR.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'LISTAJOB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSJB_MPR.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LISTAJOB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_JBCODICEJOB,"JBCODICEJOB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_JBCODICEJOB,"JBCODICEJOB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_JBNOME C(20);
      ,t_JBROUTINE N(3);
      ,t_JBCODAZI C(5);
      ,t_JBDESC C(254);
      ,t_JBTIPESC N(3);
      ,CPROWNUM N(10);
      ,t_CODUTE N(6);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsjb_mjbbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oJBNOME_2_2.controlsource=this.cTrsName+'.t_JBNOME'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oJBROUTINE_2_3.controlsource=this.cTrsName+'.t_JBROUTINE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oJBCODAZI_2_4.controlsource=this.cTrsName+'.t_JBCODAZI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oJBDESC_2_5.controlsource=this.cTrsName+'.t_JBDESC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oJBTIPESC_2_6.controlsource=this.cTrsName+'.t_JBTIPESC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(72)
    this.AddVLine(174)
    this.AddVLine(244)
    this.AddVLine(303)
    this.AddVLine(532)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LISTAJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LISTAJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2])
      *
      * insert into LISTAJOB
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LISTAJOB')
        i_extval=cp_InsertValODBCExtFlds(this,'LISTAJOB')
        i_cFldBody=" "+;
                  "(JBCODICEJOB,CPROWORD,JBNOME,JBROUTINE,JBCODAZI"+;
                  ",JBDESC,JBTIPESC,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_JBCODICEJOB)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_JBNOME)+","+cp_ToStrODBC(this.w_JBROUTINE)+","+cp_ToStrODBCNull(this.w_JBCODAZI)+;
             ","+cp_ToStrODBC(this.w_JBDESC)+","+cp_ToStrODBC(this.w_JBTIPESC)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LISTAJOB')
        i_extval=cp_InsertValVFPExtFlds(this,'LISTAJOB')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'JBCODICEJOB',this.w_JBCODICEJOB,'JBCODICEJOB',this.w_JBCODICEJOB)
        INSERT INTO (i_cTable) (;
                   JBCODICEJOB;
                  ,CPROWORD;
                  ,JBNOME;
                  ,JBROUTINE;
                  ,JBCODAZI;
                  ,JBDESC;
                  ,JBTIPESC;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_JBCODICEJOB;
                  ,this.w_CPROWORD;
                  ,this.w_JBNOME;
                  ,this.w_JBROUTINE;
                  ,this.w_JBCODAZI;
                  ,this.w_JBDESC;
                  ,this.w_JBTIPESC;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.LISTAJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_JBNOME))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'LISTAJOB')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'LISTAJOB')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_JBNOME))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSJB_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_JBCODICEJOB,"PRJOBCOD";
                     ,this.w_CPROWNUM,"PRCODPAR";
                     )
              this.GSJB_MPR.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update LISTAJOB
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'LISTAJOB')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",JBNOME="+cp_ToStrODBCNull(this.w_JBNOME)+;
                     ",JBROUTINE="+cp_ToStrODBC(this.w_JBROUTINE)+;
                     ",JBCODAZI="+cp_ToStrODBCNull(this.w_JBCODAZI)+;
                     ",JBDESC="+cp_ToStrODBC(this.w_JBDESC)+;
                     ",JBTIPESC="+cp_ToStrODBC(this.w_JBTIPESC)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'LISTAJOB')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,JBNOME=this.w_JBNOME;
                     ,JBROUTINE=this.w_JBROUTINE;
                     ,JBCODAZI=this.w_JBCODAZI;
                     ,JBDESC=this.w_JBDESC;
                     ,JBTIPESC=this.w_JBTIPESC;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_JBNOME)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSJB_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_JBCODICEJOB,"PRJOBCOD";
               ,this.w_CPROWNUM,"PRCODPAR";
               )
          this.GSJB_MPR.mReplace()
          this.GSJB_MPR.bSaveContext=.f.
        endif
      endscan
     this.GSJB_MPR.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LISTAJOB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_JBNOME))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSJB_MPR : Deleting
        this.GSJB_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_JBCODICEJOB,"PRJOBCOD";
               ,this.w_CPROWNUM,"PRCODPAR";
               )
        this.GSJB_MPR.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete LISTAJOB
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_JBNOME))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LISTAJOB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LISTAJOB_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_2_4('Full')
        .DoRTCalc(6,7,.t.)
          .w_CODUTE = i_CODUTE
          .link_2_7('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CODUTE with this.w_CODUTE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oJBNOME_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oJBNOME_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oJBDESC_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oJBDESC_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oJBTIPESC_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oJBTIPESC_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_8.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_8.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSJB_MPR.visible")=='L' And this.GSJB_MPR.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_8.visible
      this.GSJB_MPR.HideChildrenChain()
    endif 
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsjb_mjb
    * instanzio il figlio della seconda pagina immediatamente
    * viene richiamato per i calcoli
    IF Upper(CEVENT)='INIT'
      if Upper(this.GSJB_MPR.class)='STDDYNAMICCHILD'
        This.oPgFrm.Pages[3].opag.uienable(.T.)
      Endif
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=JBNOME
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GESTSJBM_IDX,3]
    i_lTable = "GESTSJBM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GESTSJBM_IDX,2], .t., this.GESTSJBM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GESTSJBM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_JBNOME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSJB_MGS',True,'GESTSJBM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GSNOME like "+cp_ToStrODBC(trim(this.w_JBNOME)+"%");

          i_ret=cp_SQL(i_nConn,"select GSNOME,GSDESC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GSNOME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GSNOME',trim(this.w_JBNOME))
          select GSNOME,GSDESC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GSNOME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_JBNOME)==trim(_Link_.GSNOME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_JBNOME) and !this.bDontReportError
            deferred_cp_zoom('GESTSJBM','*','GSNOME',cp_AbsName(oSource.parent,'oJBNOME_2_2'),i_cWhere,'GSJB_MGS',"Elenco gestioni schedulatore",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GSNOME,GSDESC";
                     +" from "+i_cTable+" "+i_lTable+" where GSNOME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GSNOME',oSource.xKey(1))
            select GSNOME,GSDESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_JBNOME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GSNOME,GSDESC";
                   +" from "+i_cTable+" "+i_lTable+" where GSNOME="+cp_ToStrODBC(this.w_JBNOME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GSNOME',this.w_JBNOME)
            select GSNOME,GSDESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_JBNOME = NVL(_Link_.GSNOME,space(20))
      this.w_JBDESC = NVL(_Link_.GSDESC,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_JBNOME = space(20)
      endif
      this.w_JBDESC = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GESTSJBM_IDX,2])+'\'+cp_ToStr(_Link_.GSNOME,1)
      cp_ShowWarn(i_cKey,this.GESTSJBM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_JBNOME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=JBCODAZI
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_JBCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_JBCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_JBCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_JBCODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_JBCODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_JBCODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_JBCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UTE_AZI_IDX,3]
    i_lTable = "UTE_AZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2], .t., this.UTE_AZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UACODAZI,UACODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where UACODUTE="+cp_ToStrODBC(this.w_CODUTE);
                   +" and UACODAZI="+cp_ToStrODBC(this.w_JBCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UACODAZI',this.w_JBCODAZI;
                       ,'UACODUTE',this.w_CODUTE)
            select UACODAZI,UACODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.UACODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UTE_AZI_IDX,2])+'\'+cp_ToStr(_Link_.UACODAZI,1)+'\'+cp_ToStr(_Link_.UACODUTE,1)
      cp_ShowWarn(i_cKey,this.UTE_AZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBNOME_2_2.value==this.w_JBNOME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBNOME_2_2.value=this.w_JBNOME
      replace t_JBNOME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBNOME_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBROUTINE_2_3.RadioValue()==this.w_JBROUTINE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBROUTINE_2_3.SetRadio()
      replace t_JBROUTINE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBROUTINE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBCODAZI_2_4.value==this.w_JBCODAZI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBCODAZI_2_4.value=this.w_JBCODAZI
      replace t_JBCODAZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBCODAZI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBDESC_2_5.value==this.w_JBDESC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBDESC_2_5.value=this.w_JBDESC
      replace t_JBDESC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBDESC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBTIPESC_2_6.RadioValue()==this.w_JBTIPESC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBTIPESC_2_6.SetRadio()
      replace t_JBTIPESC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBTIPESC_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'LISTAJOB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSJB_MPR.CheckForm()
      if not(Empty(.w_JBNOME))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSJB_MPR : Depends On
    this.GSJB_MPR.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_JBNOME)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_JBNOME=space(20)
      .w_JBROUTINE=space(1)
      .w_JBCODAZI=space(5)
      .w_JBDESC=space(254)
      .w_JBTIPESC=space(1)
      .w_CODUTE=0
      .DoRTCalc(1,2,.f.)
        .w_JBNOME = IIF( .w_SHTIPSCH = "P" , "GSJB_BXP WITH THIS" , SPACE( 20 ) )
      .DoRTCalc(3,3,.f.)
      if not(empty(.w_JBNOME))
        .link_2_2('Full')
      endif
        .w_JBROUTINE = 'R'
        .w_JBCODAZI = i_CODAZI
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_JBCODAZI))
        .link_2_4('Full')
      endif
      .DoRTCalc(6,6,.f.)
        .w_JBTIPESC = 'N'
        .w_CODUTE = i_CODUTE
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_CODUTE))
        .link_2_7('Full')
      endif
    endwith
    this.DoRTCalc(9,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_JBNOME = t_JBNOME
    this.w_JBROUTINE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBROUTINE_2_3.RadioValue(.t.)
    this.w_JBCODAZI = t_JBCODAZI
    this.w_JBDESC = t_JBDESC
    this.w_JBTIPESC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBTIPESC_2_6.RadioValue(.t.)
    this.w_CODUTE = t_CODUTE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_JBNOME with this.w_JBNOME
    replace t_JBROUTINE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBROUTINE_2_3.ToRadio()
    replace t_JBCODAZI with this.w_JBCODAZI
    replace t_JBDESC with this.w_JBDESC
    replace t_JBTIPESC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oJBTIPESC_2_6.ToRadio()
    replace t_CODUTE with this.w_CODUTE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsjb_mjbPag1 as StdContainer
  Width  = 652
  height = 400
  stdWidth  = 652
  stdheight = 400
  resizeXpos=451
  resizeYpos=204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=2, width=639,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Posizione",Field2="JBNOME",Label2="Nome",Field3="JBROUTINE",Label3="Tipo",Field4="JBCODAZI",Label4="Cod. azi.",Field5="JBDESC",Label5="Descrizione programma",Field6="JBTIPESC",Label6="Chiusura",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 67943290

  add object oStr_1_3 as StdString with uid="KHIVBSZDPQ",Visible=.t., Left=10, Top=418,;
    Alignment=0, Width=637, Height=18,;
    Caption="ATTENZIONE SE L'OGGETTO CHILD VIENE SPOSTATO IN UN ALTRA PAGINA BISOGNA MODIFICARE"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="YOUYRGTPOA",Visible=.t., Left=10, Top=434,;
    Alignment=0, Width=440, Height=18,;
    Caption="Nel blocco notify event init la riga this.opgfrm.pages[3].opag.uienable(.T.)"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=21,;
    width=635+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*17*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=22,width=634+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*17*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GESTSJBM|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GESTSJBM'
        oDropInto=this.oBodyCol.oRow.oJBNOME_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_8 as StdButton with uid="HWJWJJKNQG",width=48,height=45,;
   left=596, top=351,;
    CpPicture="bmp\parametri.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare i parametri relativi al processo";
    , HelpContextID = 225320696;
    , TabStop=.f.,Caption='\<Param.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_8.Click()
      this.Parent.oContained.GSJB_MPR.LinkPCClick()
    endproc

  func oLinkPC_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_JBROUTINE='S' OR .w_CODUTE<>i_CODUTE OR .w_SHTIPSCH = 'P')
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsjb_mjbBodyRow as CPBodyRowCnt
  Width=625
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="ONKJSCDIVF",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 100271722,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=63, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oJBNOME_2_2 as StdTrsField with uid="NCUPUPYRPE",rtseq=3,rtrep=.t.,;
    cFormVar="w_JBNOME",value=space(20),;
    ToolTipText = "Nome del programma da eseguire",;
    HelpContextID = 78006122,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=98, Left=64, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="GESTSJBM", cZoomOnZoom="GSJB_MGS", oKey_1_1="GSNOME", oKey_1_2="this.w_JBNOME"

  func oJBNOME_2_2.mCond()
    with this.Parent.oContained
      return (.w_SHTIPSCH <> "P")
    endwith
  endfunc

  func oJBNOME_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oJBNOME_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oJBNOME_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GESTSJBM','*','GSNOME',cp_AbsName(this.parent,'oJBNOME_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSJB_MGS',"Elenco gestioni schedulatore",'',this.parent.oContained
  endproc
  proc oJBNOME_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSJB_MGS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GSNOME=this.parent.oContained.w_JBNOME
    i_obj.ecpSave()
  endproc

  add object oJBROUTINE_2_3 as StdTrsCombo with uid="WOROLUEZHZ",rtrep=.t.,;
    cFormVar="w_JBROUTINE", RowSource=""+"Routine,"+"Master,"+"Dialog" , enabled=.f.,;
    ToolTipText = "Flag tipo gestione da lanciare",;
    HelpContextID = 182058292,;
    Height=21, Width=62, Left=166, Top=1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oJBROUTINE_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..JBROUTINE,&i_cF..t_JBROUTINE),this.value)
    return(iif(xVal =1,'R',;
    iif(xVal =2,'M',;
    iif(xVal =3,'A',;
    space(1)))))
  endfunc
  func oJBROUTINE_2_3.GetRadio()
    this.Parent.oContained.w_JBROUTINE = this.RadioValue()
    return .t.
  endfunc

  func oJBROUTINE_2_3.ToRadio()
    this.Parent.oContained.w_JBROUTINE=trim(this.Parent.oContained.w_JBROUTINE)
    return(;
      iif(this.Parent.oContained.w_JBROUTINE=='R',1,;
      iif(this.Parent.oContained.w_JBROUTINE=='M',2,;
      iif(this.Parent.oContained.w_JBROUTINE=='A',3,;
      0))))
  endfunc

  func oJBROUTINE_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oJBCODAZI_2_4 as StdTrsField with uid="SFFZLMJZKV",rtseq=5,rtrep=.t.,;
    cFormVar="w_JBCODAZI",value=space(5),enabled=.f.,;
    ToolTipText = "Codice azienda",;
    HelpContextID = 113838303,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=240, Top=0, InputMask=replicate('X',5), cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_JBCODAZI"

  func oJBCODAZI_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODUTE)
        bRes2=.link_2_7('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oJBDESC_2_5 as StdTrsField with uid="ZMMEEGCMPA",rtseq=6,rtrep=.t.,;
    cFormVar="w_JBDESC",value=space(254),;
    ToolTipText = "Descrizione programma",;
    HelpContextID = 105965418,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=298, Top=0, InputMask=replicate('X',254)

  func oJBDESC_2_5.mCond()
    with this.Parent.oContained
      return (.w_SHTIPSCH <> "P")
    endwith
  endfunc

  add object oJBTIPESC_2_6 as StdTrsCombo with uid="ZDOKGZNCYA",rtrep=.t.,;
    cFormVar="w_JBTIPESC", RowSource=""+"Nessuna,"+"Save (F10),"+"Quit (ESC)" , ;
    ToolTipText = "Operazione da eseguire al termine della gestione",;
    HelpContextID = 193206489,;
    Height=21, Width=96, Left=524, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oJBTIPESC_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..JBTIPESC,&i_cF..t_JBTIPESC),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    iif(xVal =3,'Q',;
    space(1)))))
  endfunc
  func oJBTIPESC_2_6.GetRadio()
    this.Parent.oContained.w_JBTIPESC = this.RadioValue()
    return .t.
  endfunc

  func oJBTIPESC_2_6.ToRadio()
    this.Parent.oContained.w_JBTIPESC=trim(this.Parent.oContained.w_JBTIPESC)
    return(;
      iif(this.Parent.oContained.w_JBTIPESC=='N',1,;
      iif(this.Parent.oContained.w_JBTIPESC=='S',2,;
      iif(this.Parent.oContained.w_JBTIPESC=='Q',3,;
      0))))
  endfunc

  func oJBTIPESC_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oJBTIPESC_2_6.mCond()
    with this.Parent.oContained
      return (.w_JBROUTINE<>'R')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=16
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_mjb','LISTAJOB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".JBCODICEJOB=LISTAJOB.JBCODICEJOB";
  +" and "+i_cAliasName2+".JBCODICEJOB=LISTAJOB.JBCODICEJOB";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
