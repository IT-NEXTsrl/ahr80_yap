* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bca                                                        *
*              Cambio azienda                                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-17                                                      *
* Last revis.: 2015-06-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bca",oParentObject)
return(i_retval)

define class tgsjb_bca as StdBatch
  * --- Local variables
  * --- WorkFile variables
  CPUSERS_idx=0
  AZIENDA_idx=0
  BUSIUNIT_idx=0
  AHEUSRCO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cambio azienda schedulatore
    GSUT_BAZ(this,this.oParentObject.w_JBCODAZI)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='BUSIUNIT'
    this.cWorkTables[4]='AHEUSRCO'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
