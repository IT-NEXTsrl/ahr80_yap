* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bgs                                                        *
*              Gestione schedulatore                                           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_84]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-22                                                      *
* Last revis.: 2005-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bgs",oParentObject)
return(i_retval)

define class tgsjb_bgs as StdBatch
  * --- Local variables
  w_GSJB_KJB = .NULL.
  w_PADRE = .NULL.
  w_GSNOME = space(20)
  w_GSDESC = space(50)
  w_GSNOMVAL = space(20)
  w_GSVALVAR = space(254)
  w_GSNOMDES = space(254)
  w_GSFLGACT = space(254)
  w_MASK = .NULL.
  w_VARIABLE = space(20)
  w_DESC = space(254)
  w_VALUE = space(254)
  w_PARAM = space(254)
  w_NOHIDE = .f.
  w_INIT = .f.
  w_gsjb_mgs = .NULL.
  * --- WorkFile variables
  GESTSJBM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attenzione le modifiche effettuate vanno (se necessario) riportate sul batch
    *     gsjb_bms
    * --- --File Gestione Schedulatore
    ah_Msg("Avvio gestione parametri schedulatore",.T.)
    * --- ---------------
     
 L_errsav=on("ERROR") 
 Messaggio="" 
 ON ERROR Messaggio="Errore"
    this.w_PADRE = this.oParentObject
    this.w_GSNOME = substr(this.w_PADRE.class,2,len(this.w_PADRE.class)-1)
    this.w_GSDESC = Left(this.w_PADRE.Caption, 50)
    this.w_PARAM = this.oParentObject.oParentObject
    on error &L_errsav
    if Messaggio="Errore"
      this.w_PARAM = " "
    else
      w_par=this.w_PARAM
      if type("w_par")<>"O"
        this.w_PARAM=alltrim(this.w_PARAM)
      else
        ah_ErrorMsg("Attenzione: impossibile schedulare dialog o master lanciate da routine","!","")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Costruisco il nome della gestione comprensivo di parametro
    if nvl(this.w_PARAM," ") <> " "
      this.w_GSNOME = this.w_GSNOME+'("'+this.w_PARAM+'")'
    else
      this.w_GSNOME = this.w_GSNOME+"()"
    endif
    this.w_GSNOME = this.w_GSNOME+space(20-len(this.w_GSNOME))
    * --- Read from GESTSJBM
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.GESTSJBM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBM_idx,2],.t.,this.GESTSJBM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" GESTSJBM where ";
            +"GSNOME = "+cp_ToStrODBC(this.w_GSNOME);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            GSNOME = this.w_GSNOME;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS = 0
      if ah_YesNo("Attenzione: la gestione non � gestita dallo schedulatore%0si desidera inserire la gestione?")
        * --- Inserisco la nuova gestione
        this.w_gsjb_mgs = gsjb_mgs()
        if !(this.w_gsjb_mgs.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_gsjb_mgs.ecpLoad()     
        this.w_gsjb_mgs.WorkFromTrs()     
        this.w_gsjb_mgs.w_GSNOME = this.w_GSNOME
        this.w_gsjb_mgs.w_GSDESC = this.w_GSDESC
        this.w_gsjb_mgs.w_GSTIPESC = "S"
        this.w_gsjb_mgs.TrsFromWork()     
        this.w_gsjb_mgs.mCalc(.t.)     
        this.w_MASK = this.oParentObject
        * --- Carico parametri
        GSUT_BCP(this,this.w_MASK, this.w_gsjb_mgs, "G")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      else
        * --- Non inserisco la gestione
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_GSJB_KJB = GSJB_KJB(this.w_PADRE)
    this.w_GSJB_KJB.w_NOMEAP = this.w_GSNOME
    this.w_GSJB_KJB.w_DESCR = this.w_GSDESC
    this.w_GSJB_KJB.w_ZoomCode.setfocus()     
    this.w_GSJB_KJB.Refresh()     
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case type("w_valore")="D"
        w_valore="cp_CharToDate('"+DTOC(w_valore)+"')"
      case type("w_valore")="T"
        w_valore="cp_CharToDatetime('"+TTOC(w_valore)+"')"
      case type("w_valore")="N"
        do case
          case this.w_NOHIDE AND UPPER(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).class) $ "STDCOMBO-STDCHECK-STDRADIO"
            w_valore=this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).RadioValue()
            do case
              case type("w_valore")="C"
                w_valore='"'+this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).RadioValue()+'"'
              case type("w_valore")="N"
                w_valore="VAL('"+STR(w_valore , 25, 8)+"')"
              case type("w_valore")="L"
                w_valore=iif(w_valore,"true","false")
            endcase
          otherwise
            w_valore="VAL('"+STR(w_valore , 25, 8)+"')"
        endcase
      case type("w_valore")="L"
        w_valore=iif(w_valore,"true","false")
      case type("w_valore")="C"
        w_valore='"'+LEFT(w_valore,252)+'"'
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='GESTSJBM'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
