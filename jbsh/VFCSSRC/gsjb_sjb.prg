* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_sjb                                                        *
*              Stampa job                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-20                                                      *
* Last revis.: 2009-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsjb_sjb",oParentObject))

* --- Class definition
define class tgsjb_sjb as StdForm
  Top    = 2
  Left   = 4

  * --- Standard Properties
  Width  = 583
  Height = 146
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-01-13"
  HelpContextID=241856105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  SCHEDJOB_IDX = 0
  cPrg = "gsjb_sjb"
  cComment = "Stampa job"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SHCODICEJOB = 0
  w_SHCODICE1 = 0
  o_SHCODICE1 = 0
  w_SHDESCRI1 = space(254)
  w_SHCODICE2 = 0
  w_SHDESCRI2 = space(254)
  w_SHTIPSCH1 = space(1)
  w_SHTIPSCH2 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsjb_sjbPag1","gsjb_sjb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSHCODICE1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='SCHEDJOB'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SHCODICEJOB=0
      .w_SHCODICE1=0
      .w_SHDESCRI1=space(254)
      .w_SHCODICE2=0
      .w_SHDESCRI2=space(254)
      .w_SHTIPSCH1=space(1)
      .w_SHTIPSCH2=space(1)
        .w_SHCODICEJOB = IIF( TYPE( "THIS.OPARENTOBJECT .w_CODICE" ) <> "N" , 0 , THIS.OPARENTOBJECT .w_CODICE )
        .w_SHCODICE1 = .w_SHCODICEJOB
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SHCODICE1))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_SHCODICE2 = .w_SHCODICE1
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_SHCODICE2))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
    this.DoRTCalc(5,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_SHCODICE1<>.w_SHCODICE1
            .w_SHCODICE2 = .w_SHCODICE1
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SHCODICE1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_lTable = "SCHEDJOB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2], .t., this.SCHEDJOB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SHCODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSJB_ASH',True,'SCHEDJOB')
        if i_nConn<>0
          i_cWhere = " SHCODICE="+cp_ToStrODBC(this.w_SHCODICE1);

          i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SHCODICE',this.w_SHCODICE1)
          select SHCODICE,SHDESCRI,SHTIPSCH;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SHCODICE1) and !this.bDontReportError
            deferred_cp_zoom('SCHEDJOB','*','SHCODICE',cp_AbsName(oSource.parent,'oSHCODICE1_1_2'),i_cWhere,'GSJB_ASH',"Job schedulatore",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                     +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',oSource.xKey(1))
            select SHCODICE,SHDESCRI,SHTIPSCH;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SHCODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                   +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(this.w_SHCODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',this.w_SHCODICE1)
            select SHCODICE,SHDESCRI,SHTIPSCH;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SHCODICE1 = NVL(_Link_.SHCODICE,0)
      this.w_SHDESCRI1 = NVL(_Link_.SHDESCRI,space(254))
      this.w_SHTIPSCH1 = NVL(_Link_.SHTIPSCH,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SHCODICE1 = 0
      endif
      this.w_SHDESCRI1 = space(254)
      this.w_SHTIPSCH1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=( empty(.w_SHCODICE2) or (.w_SHCODICE2>=.w_SHCODICE1) ) AND NVL( .w_SHTIPSCH1 , "N" ) <> "P"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SHCODICE1 = 0
        this.w_SHDESCRI1 = space(254)
        this.w_SHTIPSCH1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])+'\'+cp_ToStr(_Link_.SHCODICE,1)
      cp_ShowWarn(i_cKey,this.SCHEDJOB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SHCODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SHCODICE2
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_lTable = "SCHEDJOB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2], .t., this.SCHEDJOB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SHCODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSJB_ASH',True,'SCHEDJOB')
        if i_nConn<>0
          i_cWhere = " SHCODICE="+cp_ToStrODBC(this.w_SHCODICE2);

          i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SHCODICE',this.w_SHCODICE2)
          select SHCODICE,SHDESCRI,SHTIPSCH;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SHCODICE2) and !this.bDontReportError
            deferred_cp_zoom('SCHEDJOB','*','SHCODICE',cp_AbsName(oSource.parent,'oSHCODICE2_1_6'),i_cWhere,'GSJB_ASH',"Job schedulatore",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                     +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',oSource.xKey(1))
            select SHCODICE,SHDESCRI,SHTIPSCH;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SHCODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                   +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(this.w_SHCODICE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',this.w_SHCODICE2)
            select SHCODICE,SHDESCRI,SHTIPSCH;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SHCODICE2 = NVL(_Link_.SHCODICE,0)
      this.w_SHDESCRI2 = NVL(_Link_.SHDESCRI,space(254))
      this.w_SHTIPSCH2 = NVL(_Link_.SHTIPSCH,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SHCODICE2 = 0
      endif
      this.w_SHDESCRI2 = space(254)
      this.w_SHTIPSCH2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_SHCODICE2>=.w_SHCODICE1) AND NVL( .w_SHTIPSCH2 , "N" ) <> "P"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SHCODICE2 = 0
        this.w_SHDESCRI2 = space(254)
        this.w_SHTIPSCH2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])+'\'+cp_ToStr(_Link_.SHCODICE,1)
      cp_ShowWarn(i_cKey,this.SCHEDJOB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SHCODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSHCODICE1_1_2.value==this.w_SHCODICE1)
      this.oPgFrm.Page1.oPag.oSHCODICE1_1_2.value=this.w_SHCODICE1
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDESCRI1_1_3.value==this.w_SHDESCRI1)
      this.oPgFrm.Page1.oPag.oSHDESCRI1_1_3.value=this.w_SHDESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oSHCODICE2_1_6.value==this.w_SHCODICE2)
      this.oPgFrm.Page1.oPag.oSHCODICE2_1_6.value=this.w_SHCODICE2
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDESCRI2_1_7.value==this.w_SHDESCRI2)
      this.oPgFrm.Page1.oPag.oSHDESCRI2_1_7.value=this.w_SHDESCRI2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(( empty(.w_SHCODICE2) or (.w_SHCODICE2>=.w_SHCODICE1) ) AND NVL( .w_SHTIPSCH1 , "N" ) <> "P")  and not(empty(.w_SHCODICE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSHCODICE1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_SHCODICE2>=.w_SHCODICE1) AND NVL( .w_SHTIPSCH2 , "N" ) <> "P")  and not(empty(.w_SHCODICE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSHCODICE2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SHCODICE1 = this.w_SHCODICE1
    return

enddefine

* --- Define pages as container
define class tgsjb_sjbPag1 as StdContainer
  Width  = 579
  height = 146
  stdWidth  = 579
  stdheight = 146
  resizeXpos=405
  resizeYpos=62
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSHCODICE1_1_2 as StdField with uid="KKGPMNFMAC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SHCODICE1", cQueryName = "SHCODICE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice inizio selezione",;
    HelpContextID = 254349947,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=10, cSayPict='"9999999999"', cGetPict='"9999999999"', bHasZoom = .t. , cLinkFile="SCHEDJOB", cZoomOnZoom="GSJB_ASH", oKey_1_1="SHCODICE", oKey_1_2="this.w_SHCODICE1"

  func oSHCODICE1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSHCODICE1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSHCODICE1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SCHEDJOB','*','SHCODICE',cp_AbsName(this.parent,'oSHCODICE1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSJB_ASH',"Job schedulatore",'',this.parent.oContained
  endproc
  proc oSHCODICE1_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSJB_ASH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SHCODICE=this.parent.oContained.w_SHCODICE1
     i_obj.ecpSave()
  endproc

  add object oSHDESCRI1_1_3 as StdField with uid="CTBFUPJVCF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SHDESCRI1", cQueryName = "SHDESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione del codice",;
    HelpContextID = 99671425,;
   bGlobalFont=.t.,;
    Height=21, Width=398, Left=172, Top=10, InputMask=replicate('X',254)

  add object oSHCODICE2_1_6 as StdField with uid="NGOJIVFAYX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SHCODICE2", cQueryName = "SHCODICE2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice fine selezione",;
    HelpContextID = 254349963,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=35, cSayPict='"9999999999"', cGetPict='"9999999999"', bHasZoom = .t. , cLinkFile="SCHEDJOB", cZoomOnZoom="GSJB_ASH", oKey_1_1="SHCODICE", oKey_1_2="this.w_SHCODICE2"

  func oSHCODICE2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSHCODICE2_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSHCODICE2_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SCHEDJOB','*','SHCODICE',cp_AbsName(this.parent,'oSHCODICE2_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSJB_ASH',"Job schedulatore",'',this.parent.oContained
  endproc
  proc oSHCODICE2_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSJB_ASH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SHCODICE=this.parent.oContained.w_SHCODICE2
     i_obj.ecpSave()
  endproc

  add object oSHDESCRI2_1_7 as StdField with uid="LKTDHQMQSN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SHDESCRI2", cQueryName = "SHDESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione del codice",;
    HelpContextID = 99671409,;
   bGlobalFont=.t.,;
    Height=21, Width=398, Left=172, Top=35, InputMask=replicate('X',254)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=110, top=70, width=384,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 204576998


  add object oBtn_1_9 as StdButton with uid="APIWEFNJEL",left=468, top=96, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 100066522;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="ZJIUAZYUNY",left=519, top=96, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 234538682;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="XJEUCEXOXA",Visible=.t., Left=4, Top=39,;
    Alignment=1, Width=61, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ETWPVPNWPP",Visible=.t., Left=1, Top=14,;
    Alignment=1, Width=64, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="HCVGMFFTWH",Visible=.t., Left=10, Top=72,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_sjb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
