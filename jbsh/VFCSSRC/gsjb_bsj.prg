* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bsj                                                        *
*              Esecutore di job                                                *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][21][VRS_263]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-10                                                      *
* Last revis.: 2018-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SHCOD,w_SHDATA,w_SHORA,w_SHTMPMAX
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bsj",oParentObject,m.w_SHCOD,m.w_SHDATA,m.w_SHORA,m.w_SHTMPMAX)
return(i_retval)

define class tgsjb_bsj as StdBatch
  * --- Local variables
  w_SHCOD = 0
  w_SHDATA = ctod("  /  /  ")
  w_SHORA = space(5)
  w_SHTMPMAX = space(5)
  w_DIR = space(254)
  w_UTE = space(0)
  w_SHMINUINT = 0
  w_SHORAFIN = space(5)
  w_SHORAINI = space(5)
  w_ora_lancifi = space(2)
  w_mm_lancifi = space(2)
  w_ora = space(5)
  w_data = ctod("  /  /  ")
  w_JBCODICEJOB = 0
  w_JBNOME = space(20)
  w_JBCODAZI = space(5)
  w_JBROUTINE = space(1)
  w_JBTIPESC = space(1)
  w_PRCODPAR = 0
  w_PRNOMVAL = space(20)
  w_PRVALVAR = space(254)
  w_GSVALVAR = space(254)
  w_GSFLGACT = space(1)
  w_OBJPARENT = .NULL.
  w_PRROWORD = 0
  w_FLCODAZI = .f.
  w_SHFREQ = space(1)
  w_SHNUMFREQ = 0
  w_SHSET = space(50)
  w_SHMESE = space(75)
  w_SHIMPDAT = space(1)
  w_SHDATFIS = 0
  w_SHDATAPRO = ctod("  /  /  ")
  w_SHDATASTART = ctod("  /  /  ")
  w_SHORAPROS = space(5)
  w_ora_lancio = space(2)
  w_mm_lancio = space(2)
  w_LSSERIAL = space(10)
  w_CPROWNUM = 0
  w_LSFLELCR = space(1)
  w_ESCENABLED = .f.
  w_LEN = 0
  w_DESCR = space(50)
  w_COUNT = 0
  w_PRFLGPP = space(1)
  w_MSGT = space(0)
  w_lunghezza = 0
  w_inizio = 0
  w_obj = .NULL.
  w_ESEGUITO = space(1)
  w_TMPMAX = 0
  w_SMTPPRIO = 0
  w_FLINVS = space(1)
  w_PDFATTACH = space(0)
  w_EMPTYDATE = ctod("  /  /  ")
  w_EMPTYTIME = space(5)
  w_LOGERMSG = space(256)
  w_SOLOCONLOG = .f.
  w_MTXFILE = space(254)
  w_HANDLE = 0
  w_SHTIPSCH = space(1)
  w_MESSAGGIORESPONSO = space(0)
  w_SMTPFROM = space(250)
  w_SMTP__TO = space(0)
  w_SMTP__CC = space(0)
  w_SMTP_CCN = space(0)
  w_SMTPSUBJ = space(250)
  w_SMTPBODY = space(0)
  w_SMTP_ATT = space(0)
  w_SMTPSERV = space(250)
  w_SMTPPORT = 0
  w_SMTPPRIO = 0
  w_MESSAGGIOJOB = space(0)
  w_LSSERIAL1 = space(10)
  w_LSSERIAL2 = space(10)
  w_NomeFile = space(254)
  w_DESTINATARISOLOLOG = space(0)
  w_DESTINATARISOLOELAB = space(0)
  w_DESTINATARILOGEELAB = space(0)
  w_INOLTROGIAESEGUITO = .f.
  w_UGTIPINV = space(1)
  w_UGTIPMES = space(1)
  w_UTCODICE = 0
  w_ErrEmail = .f.
  w_SMTPBODYpostin = space(0)
  w_PARAMCHILD = space(254)
  w_BUTTON = .NULL.
  * --- WorkFile variables
  SCHEDJOB_idx=0
  LISTAJOB_idx=0
  PARAMJ_idx=0
  CONTROPA_idx=0
  LOGELAJB_idx=0
  CPUSERS_idx=0
  AZIENDA_idx=0
  BUSIUNIT_idx=0
  UTE_AZI_idx=0
  JOBSTAMP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da schedTimer (timer dello schedulatore) contenuto in ztam_class,
    * --- Variabili per l'aggiornamento della data/ora
    * --- --tipo della frequenza temporale, unico, giornaliero, settimanale, mensile
    * --- --Cadenza ripetizione del lancio del processo
    * --- Giorni della settimana selezionati
    * --- --Mesi selezionati
    * --- Tipo data mese scelta
    * --- Variabili per controllo lunghezza log
    * --- Variabile per il controllo della propriet� w_xxx
    * --- --Variabili di servizio
    * --- Variabile che conterra i nomi dei file delle stampe da allegare
    * --- --Serve per forzare l invio delle mail all elenco dei destinatari ai soli utenti per cui si � impostato solo Log o Log + stampa
    this.w_SOLOCONLOG = .F.
    * --- Lo schedulatore � attivo
    cp_SetGlobalVar("g_SCHEDULER","S")
    * --- Ripulisco la variabile contenente il log
    cp_SetGlobalVar("g_MSG","")
    * --- Metto a null data e ora in modo che il job non venga pi� selezionato per tutta la durata della routine
    this.w_ora = SUBSTR(TIME(),1,5)
    this.w_data = DATE()
    i_DATSYS=DATE()
    * --- --Modifica per  gestione servizio windows
    * --- In base al tipo di DB utilizzato inserisco date e ore pari a null o ad empty
    * --- Inoltre ribadisco a 'N' il campo SHESENOW
     
 i_Conn=i_TableProp[this.LOGELAJB_IDX, 3]
    this.w_EMPTYDATE = iif(i_Conn=0,CTOD("  -  -    "),null)
    this.w_EMPTYTIME = iif(i_Conn=0,"",null)
    * --- Write into SCHEDJOB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SCHEDJOB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SHDATAPRO ="+cp_NullLink(cp_ToStrODBC(this.w_EMPTYDATE),'SCHEDJOB','SHDATAPRO');
      +",SHORAPROS ="+cp_NullLink(cp_ToStrODBC(this.w_EMPTYTIME),'SCHEDJOB','SHORAPROS');
      +",SHDATASTART ="+cp_NullLink(cp_ToStrODBC(this.w_data),'SCHEDJOB','SHDATASTART');
      +",SHORAINE ="+cp_NullLink(cp_ToStrODBC(this.w_ora),'SCHEDJOB','SHORAINE');
      +",SHESENOW ="+cp_NullLink(cp_ToStrODBC("N"),'SCHEDJOB','SHESENOW');
          +i_ccchkf ;
      +" where ";
          +"SHCODICE = "+cp_ToStrODBC(this.w_SHCOD);
          +" and SHDATAPRO = "+cp_ToStrODBC(this.w_SHDATA);
          +" and SHORAPROS = "+cp_ToStrODBC(this.w_SHORA);
             )
    else
      update (i_cTable) set;
          SHDATAPRO = this.w_EMPTYDATE;
          ,SHORAPROS = this.w_EMPTYTIME;
          ,SHDATASTART = this.w_data;
          ,SHORAINE = this.w_ora;
          ,SHESENOW = "N";
          &i_ccchkf. ;
       where;
          SHCODICE = this.w_SHCOD;
          and SHDATAPRO = this.w_SHDATA;
          and SHORAPROS = this.w_SHORA;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Ho appena sbiancato le date di prossima esecuzione in quanto sto 
    *     per eseguire il job. In questo caso creo un file di log che funziona anche 
    *     da semaforo per lo Zucchetti Scheduler Service. Tale file viene qui scritto 
    *     e a fine elaborazione rimosso. E' utilizzato per la scrittura di alcuni dati di log
    *     e per indicare (fintanto che esso � presente) che lo schedulatore sta attualmente
    *     eseguendo un job e pertanto la sua chiusura (arresto del servizio) pu� causare la 
    *     perdita di dati. Il file deve essere all'interno della cartella Zschsrv dove risiedono
    *     gli eseguibili per il servizio
    this.w_MTXFILE = ForceExt(SUBSTR(SYS(5)+SYS(2003),1,RAT("\",SYS(5)+SYS(2003),1))+"jbsh\ZSchSrv\Scheduler_mutex_"+Tran(i_codute), "log")
    this.w_HANDLE = FCREATE(this.w_MTXFILE)
    FPUTS(this.w_HANDLE,"Avvio job schedulato n. "+alltrim(STR(this.w_SHCOD)) + " alle ore: "+TTOC(DATETIME())) 
 FFLUSH(this.w_HANDLE) 
 FCLOSE(this.w_HANDLE)
    * --- Preparo il log del job inserendo la data di inizio elaborazione
    this.w_LSSERIAL = SPACE(10)
     
 cp_NextTableProg(this, i_Conn, "LOGJB", "w_LSSERIAL")
    * --- Insert into LOGELAJB
    i_nConn=i_TableProp[this.LOGELAJB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGELAJB_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSSERIAL"+",LSCODJOB"+",LSDATINI"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOGELAJB','LSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SHCOD),'LOGELAJB','LSCODJOB');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOGELAJB','LSDATINI');
      +","+cp_NullLink(cp_ToStrODBC(1),'LOGELAJB','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODJOB',this.w_SHCOD,'LSDATINI',i_DATSYS,'CPROWNUM',1)
      insert into (i_cTable) (LSSERIAL,LSCODJOB,LSDATINI,CPROWNUM &i_ccchkf. );
         values (;
           this.w_LSSERIAL;
           ,this.w_SHCOD;
           ,i_DATSYS;
           ,1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Controllo il tempo di massima esecuzione
    this.w_TMPMAX = (int( val( left( this.w_SHTMPMAX, 2 ) ) ) * 3600 + int( val( right( this.w_SHTMPMAX, 2 ) ) ) * 60)
    if this.w_TMPMAX = 0 OR this.w_TMPMAX > (datetime() - cp_CharToDatetime( dtoc( this.w_SHDATA ) + " " + this.w_SHORA)) 
      * --- Test positivo, eseguo il job
      this.w_ESEGUITO = "S"
      * --- Eseguo i processi del job
      * --- Read from SCHEDJOB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2],.t.,this.SCHEDJOB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SHPATH,SHFLGSTC,SHDESCRI,SHFLINVS,SHTIPSCH"+;
          " from "+i_cTable+" SCHEDJOB where ";
              +"SHCODICE = "+cp_ToStrODBC(this.w_SHCOD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SHPATH,SHFLGSTC,SHDESCRI,SHFLINVS,SHTIPSCH;
          from (i_cTable) where;
              SHCODICE = this.w_SHCOD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        cp_SetGlobalVar("g_PATHSJ", NVL(cp_ToDate(_read_.SHPATH),cp_NullValue(_read_.SHPATH)))
        cp_SetGlobalVar("g_FLGSTC", NVL(cp_ToDate(_read_.SHFLGSTC),cp_NullValue(_read_.SHFLGSTC)))
        this.w_DESCR = NVL(cp_ToDate(_read_.SHDESCRI),cp_NullValue(_read_.SHDESCRI))
        this.w_FLINVS = NVL(cp_ToDate(_read_.SHFLINVS),cp_NullValue(_read_.SHFLINVS))
        this.w_SHTIPSCH = NVL(cp_ToDate(_read_.SHTIPSCH),cp_NullValue(_read_.SHTIPSCH))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DIR = ADDBS(JUSTPATH(ALLTRIM(cp_GetGlobalVar("g_PATHSJ"))))
      cp_SetGlobalVar("g_PATHSJ",iif(empty(nvl(cp_GetGlobalVar("g_PATHSJ")," ")),alltrim(cp_GetGlobalVar("g_PATHSJ")),alltrim(FULLPATH(cp_GetGlobalVar("g_PATHSJ")))))
      * --- Select from LISTAJOB
      i_nConn=i_TableProp[this.LISTAJOB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2],.t.,this.LISTAJOB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select JBNOME,CPROWNUM,JBROUTINE,CPROWORD,JBCODAZI,JBDESC,JBTIPESC  from "+i_cTable+" LISTAJOB ";
            +" where JBCODICEJOB="+cp_ToStrODBC(this.w_SHCOD)+"";
            +" order by CPROWORD";
             ,"_Curs_LISTAJOB")
      else
        select JBNOME,CPROWNUM,JBROUTINE,CPROWORD,JBCODAZI,JBDESC,JBTIPESC from (i_cTable);
         where JBCODICEJOB=this.w_SHCOD;
         order by CPROWORD;
          into cursor _Curs_LISTAJOB
      endif
      if used('_Curs_LISTAJOB')
        select _Curs_LISTAJOB
        locate for 1=1
        do while not(eof())
        cp_SetGlobalVar("g_FLAGSTMP",.F.)
        this.w_COUNT = 0
        PUBLIC ARRAY g_STAMPJOB(1)
        PUBLIC g_MESSAGGIORESPONSO
        g_MESSAGGIORESPONSO = ""
        this.w_ora = SUBSTR(TIME(),1,8)
        this.w_data = DATE()
        this.w_FLCODAZI = .F.
        cp_SetGlobalVar("g_MSG",ah_MsgFormat("Data inizio elaborazione: %1 %2%0", DTOC(this.w_DATA), this.w_ORA))
        this.w_JBNOME = _Curs_LISTAJOB.JBNOME
        cp_SetGlobalVar("g_JBCODAZI",trim(_Curs_LISTAJOB.JBCODAZI))
        this.w_JBCODAZI = _Curs_LISTAJOB.JBCODAZI
        this.w_JBTIPESC = _Curs_LISTAJOB.JBTIPESC
        * --- Verifico per un eventuale cambio di azienda
        if this.w_JBCODAZI<>i_CODAZI
          VQ_EXEC("..\JBSH\EXE\GSJB_BSJ.VQR",this,"CURS")
          VQ_EXEC("..\JBSH\EXE\GSJB1BSJ.VQR",this,"CURS1")
          if reccount("CURS1")=0
            cp_SetGlobalVar("g_MSG",cp_GetGlobalVar("g_MSG")+ah_MsgFormat("Attenzione: l'azienda %1 non esiste", Upper(this.w_JBCODAZI)))
            this.w_LSFLELCR = "N"
            this.w_FLCODAZI = .T.
          else
            if reccount("CURS")=0
              cp_SetGlobalVar("g_MSG",cp_GetGlobalVar("g_MSG")+ah_MsgFormat("Attenzione: l'utente %1 non � associato all'azienda %2", ALLTRIM(STR(i_CODUTE)), Upper(this.w_JBCODAZI)))
              this.w_LSFLELCR = "N"
              this.w_FLCODAZI = .T.
              this.w_LSFLELCR = "N"
              this.w_FLCODAZI = .T.
            else
              do GSJB_BCA with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          if USED("CURS")
            SELECT CURS
            USE
          endif
          if USED("CURS1")
            SELECT CURS1
            USE
          endif
        endif
        * --- Controllo la presenza di un path personalizzato
        if empty(cp_GetGlobalVar("g_PATHSJ"))
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COPATHSJ"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COPATHSJ;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            cp_SetGlobalVar("g_PATHSJ", NVL(cp_ToDate(_read_.COPATHSJ),cp_NullValue(_read_.COPATHSJ)))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          cp_SetGlobalVar("g_PATHSJ",alltrim(fullpath(cp_GetGlobalVar("g_PATHSJ"))))
        endif
        this.w_DIR = ADDBS(JUSTPATH(ALLTRIM(cp_GetGlobalVar("g_PATHSJ"))))
        * --- --Gestione messaggio errore per MD
         
 l_errsav=on("ERROR") 
 Messaggio="" 
 ON ERROR Messaggio=Message()+" "+Message(1)
        if !EMPTY(this.w_DIR) AND NOT DIRECTORY(this.w_DIR)
          MD (this.w_DIR)
        endif
        on error &l_errsav
        if not empty(Messaggio)
          cp_SetGlobalVar("g_MSG",cp_GetGlobalVar("g_MSG")+Messaggio)
          * --- --verificato errore si esce dall'esecuzione del Job si forza l'errore sulla variabile booleana w_FLCODAZI
          cp_SetGlobalVar("g_MSG",cp_GetGlobalVar("g_MSG")+ah_MsgFormat("Attenzione: impossibile creare la cartella: %1 definita nel path", ALLTRIM(this.w_DIR)))
          this.w_FLCODAZI = .T.
        endif
        this.w_PRCODPAR = _Curs_LISTAJOB.CPROWNUM
        this.w_PRROWORD = _Curs_LISTAJOB.CPROWORD
        this.w_JBROUTINE = _Curs_LISTAJOB.JBROUTINE
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        if not this.w_FLCODAZI
           
 L_errsav=on("ERROR") 
 Messaggio="" 
 ON ERROR Messaggio=Message()+" "+Message(1)
          do case
            case this.w_JBROUTINE="R"
              * --- Eseguo la routine
              w_JBNOME_GO=this.w_JBNOME
              cp_SetGlobalVar("g_JBNOME",Alltrim(_Curs_LISTAJOB.JBDESC))
              if Empty( cp_GetGlobalVar("g_JBNOME") )
                cp_SetGlobalVar("g_JBNOME",Alltrim(_Curs_LISTAJOB.JBNOME))
              endif
              * --- Elimino gli eventuali caratteri che non devono formare il nome del file pdf
              cp_SetGlobalVar("g_JBNOME",CHKSTRCH(cp_GetGlobalVar("g_JBNOME")))
              do &w_JBNOME_GO
            case this.w_JBROUTINE="M"
              * --- Lancio una maschera Master, Master/Detail
              w_JBNOME_GO=this.w_JBNOME
              this.w_OBJPARENT = &w_JBNOME_GO
              * --- Utilizzo la Caption della gestione per costruire il nome del pdf della stampa
              cp_SetGlobalVar("g_JBNOME",this.w_OBJPARENT.Caption)
              * --- Elimino gli eventuali caratteri che non devono formare il nome del file pdf
              *                             Es. se � presente / Interroga lo devo eliminare
              cp_SetGlobalVar("g_JBNOME",STRTRAN(trim(cp_GetGlobalVar("g_JBNOME")), " "+AH_MSGFORMAT(MSG_BS)+" "+AH_MSGFORMAT(MSG_QUERY),""))
              cp_SetGlobalVar("g_JBNOME",CHKSTRCH(cp_GetGlobalVar("g_JBNOME")))
              this.w_OBJPARENT.ecpload()     
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_OBJPARENT.ecpquit()     
              this.w_OBJPARENT.ecpquit()     
              this.w_OBJPARENT = null
            otherwise
              * --- Lancio una DialogWindow
              w_JBNOME_GO=this.w_JBNOME
              this.w_OBJPARENT = &w_JBNOME_GO
              * --- Utilizzo la Caption della gestione per costruire il nome del pdf della stampa
              cp_SetGlobalVar("g_JBNOME",this.w_OBJPARENT.Caption)
              * --- Elimino gli eventuali caratteri che non devono formare il nome del file pdf
              cp_SetGlobalVar("g_JBNOME",CHKSTRCH(cp_GetGlobalVar("g_JBNOME")))
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_OBJPARENT = null
          endcase
          * --- Controllo la presenza di eventuali errori a seguito dell'esecuzione della gestione
          on error &L_errsav
          if not empty(Messaggio)
            cp_SetGlobalVar("g_MSG",cp_GetGlobalVar("g_MSG")+Messaggio)
            this.w_LSFLELCR = "N"
          else
            this.w_LSFLELCR = "S"
          endif
          if not empty(g_MESSAGGIORESPONSO)
            cp_SetGlobalVar("g_MSG",cp_GetGlobalVar("g_MSG")+ chr(13) + g_MESSAGGIORESPONSO + chr(13))
          endif
        endif
        this.w_ora = SUBSTR(TIME(),1,8)
        this.w_data = DATE()
        cp_SetGlobalVar("g_MSG",cp_GetGlobalVar("g_MSG")+ah_MsgFormat("%0Data fine elaborazione: %1 %2", DTOC(this.w_DATA), this.w_ORA))
        * --- Esecuzione terminata, scrivo il messaggio di log
        this.w_lunghezza = LEN(cp_GetGlobalVar("g_MSG"))
        * --- Controllo se sto usando oracle, in quest'ultimo caso devo controllare la lunghezza del log
        if "ORACLE"=upper(CP_DBTYPE) AND this.w_lunghezza > 4000
          * --- Gestisco problema di oracle quando la lunghezza del log � maggiore di 4000
          this.w_inizio = 1
          do while this.w_lunghezza > 4000
            * --- Scompongo in blocchi di 4000
            this.w_MSGT = SUBSTR(cp_GetGlobalVar("g_MSG"),this.w_inizio,4000)
            this.w_inizio = this.w_inizio+4000
            * --- Scrivo il log
            if this.w_CPROWNUM>1
              * --- Insert into LOGELAJB
              i_nConn=i_TableProp[this.LOGELAJB_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGELAJB_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"LSSERIAL"+",LSCODJOB"+",LSDATINI"+",LSPROCES"+",LSROWNUM"+",LSROWORD"+",CPROWNUM"+",LSFLELCR"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOGELAJB','LSSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SHCOD),'LOGELAJB','LSCODJOB');
                +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOGELAJB','LSDATINI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LOGELAJB','LSPROCES');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODPAR),'LOGELAJB','LSROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWORD),'LOGELAJB','LSROWORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LOGELAJB','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_LSFLELCR),'LOGELAJB','LSFLELCR');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODJOB',this.w_SHCOD,'LSDATINI',i_DATSYS,'LSPROCES',this.w_JBNOME,'LSROWNUM',this.w_PRCODPAR,'LSROWORD',this.w_PRROWORD,'CPROWNUM',this.w_CPROWNUM,'LSFLELCR',this.w_LSFLELCR)
                insert into (i_cTable) (LSSERIAL,LSCODJOB,LSDATINI,LSPROCES,LSROWNUM,LSROWORD,CPROWNUM,LSFLELCR &i_ccchkf. );
                   values (;
                     this.w_LSSERIAL;
                     ,this.w_SHCOD;
                     ,i_DATSYS;
                     ,this.w_JBNOME;
                     ,this.w_PRCODPAR;
                     ,this.w_PRROWORD;
                     ,this.w_CPROWNUM;
                     ,this.w_LSFLELCR;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            else
              * --- Write into LOGELAJB
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.LOGELAJB_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"LSPROCES ="+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LOGELAJB','LSPROCES');
                +",LSNOTELA ="+cp_NullLink(cp_ToStrODBC(this.w_MSGT),'LOGELAJB','LSNOTELA');
                +",LSROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODPAR),'LOGELAJB','LSROWNUM');
                +",LSROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_PRROWORD),'LOGELAJB','LSROWORD');
                +",LSFLELCR ="+cp_NullLink(cp_ToStrODBC(this.w_LSFLELCR),'LOGELAJB','LSFLELCR');
                    +i_ccchkf ;
                +" where ";
                    +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                       )
              else
                update (i_cTable) set;
                    LSPROCES = this.w_JBNOME;
                    ,LSNOTELA = this.w_MSGT;
                    ,LSROWNUM = this.w_PRCODPAR;
                    ,LSROWORD = this.w_PRROWORD;
                    ,LSFLELCR = this.w_LSFLELCR;
                    &i_ccchkf. ;
                 where;
                    LSSERIAL = this.w_LSSERIAL;
                    and CPROWNUM = this.w_CPROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- Provo a inserire il log
            * --- Try
            local bErr_04CBC2F0
            bErr_04CBC2F0=bTrsErr
            this.Try_04CBC2F0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              this.w_LOGERMSG = AH_MsgFormat("Errore nella scrittura del log schedulatore: %1", Message())
              * --- Write into LOGELAJB
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.LOGELAJB_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"LSNOTELA ="+cp_NullLink(cp_ToStrODBC(this.w_LOGERMSG),'LOGELAJB','LSNOTELA');
                    +i_ccchkf ;
                +" where ";
                    +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                       )
              else
                update (i_cTable) set;
                    LSNOTELA = this.w_LOGERMSG;
                    &i_ccchkf. ;
                 where;
                    LSSERIAL = this.w_LSSERIAL;
                    and CPROWNUM = this.w_CPROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            bTrsErr=bTrsErr or bErr_04CBC2F0
            * --- End
            this.w_lunghezza = this.w_lunghezza - 4000
            this.w_CPROWNUM = this.w_CPROWNUM + 1
          enddo
          * --- Inserisco la parte rimanente minore di 4000
          this.w_MSGT = SUBSTR(cp_GetGlobalVar("g_MSG"),this.w_inizio,4000)
          * --- Insert into LOGELAJB
          i_nConn=i_TableProp[this.LOGELAJB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGELAJB_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LSSERIAL"+",LSCODJOB"+",LSDATINI"+",LSPROCES"+",LSROWNUM"+",LSROWORD"+",CPROWNUM"+",LSFLELCR"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOGELAJB','LSSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SHCOD),'LOGELAJB','LSCODJOB');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOGELAJB','LSDATINI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LOGELAJB','LSPROCES');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODPAR),'LOGELAJB','LSROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWORD),'LOGELAJB','LSROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LOGELAJB','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LSFLELCR),'LOGELAJB','LSFLELCR');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODJOB',this.w_SHCOD,'LSDATINI',i_DATSYS,'LSPROCES',this.w_JBNOME,'LSROWNUM',this.w_PRCODPAR,'LSROWORD',this.w_PRROWORD,'CPROWNUM',this.w_CPROWNUM,'LSFLELCR',this.w_LSFLELCR)
            insert into (i_cTable) (LSSERIAL,LSCODJOB,LSDATINI,LSPROCES,LSROWNUM,LSROWORD,CPROWNUM,LSFLELCR &i_ccchkf. );
               values (;
                 this.w_LSSERIAL;
                 ,this.w_SHCOD;
                 ,i_DATSYS;
                 ,this.w_JBNOME;
                 ,this.w_PRCODPAR;
                 ,this.w_PRROWORD;
                 ,this.w_CPROWNUM;
                 ,this.w_LSFLELCR;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Provo a inserire il log
          * --- Try
          local bErr_04CB71F0
          bErr_04CB71F0=bTrsErr
          this.Try_04CB71F0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            this.w_LOGERMSG = AH_MsgFormat("Errore nella scrittura del log schedulatore: %1", Message())
            * --- Write into LOGELAJB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.LOGELAJB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LSNOTELA ="+cp_NullLink(cp_ToStrODBC(this.w_LOGERMSG),'LOGELAJB','LSNOTELA');
                  +i_ccchkf ;
              +" where ";
                  +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  LSNOTELA = this.w_LOGERMSG;
                  &i_ccchkf. ;
               where;
                  LSSERIAL = this.w_LSSERIAL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04CB71F0
          * --- End
        else
          * --- Non sono in oracle o il msg di log � minore di 4000
          * --- Scrivo il log
          if this.w_CPROWNUM>1
            * --- Insert into LOGELAJB
            i_nConn=i_TableProp[this.LOGELAJB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGELAJB_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LSSERIAL"+",LSCODJOB"+",LSDATINI"+",LSPROCES"+",LSROWNUM"+",LSROWORD"+",CPROWNUM"+",LSFLELCR"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOGELAJB','LSSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SHCOD),'LOGELAJB','LSCODJOB');
              +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOGELAJB','LSDATINI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LOGELAJB','LSPROCES');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODPAR),'LOGELAJB','LSROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWORD),'LOGELAJB','LSROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LOGELAJB','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LSFLELCR),'LOGELAJB','LSFLELCR');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODJOB',this.w_SHCOD,'LSDATINI',i_DATSYS,'LSPROCES',this.w_JBNOME,'LSROWNUM',this.w_PRCODPAR,'LSROWORD',this.w_PRROWORD,'CPROWNUM',this.w_CPROWNUM,'LSFLELCR',this.w_LSFLELCR)
              insert into (i_cTable) (LSSERIAL,LSCODJOB,LSDATINI,LSPROCES,LSROWNUM,LSROWORD,CPROWNUM,LSFLELCR &i_ccchkf. );
                 values (;
                   this.w_LSSERIAL;
                   ,this.w_SHCOD;
                   ,i_DATSYS;
                   ,this.w_JBNOME;
                   ,this.w_PRCODPAR;
                   ,this.w_PRROWORD;
                   ,this.w_CPROWNUM;
                   ,this.w_LSFLELCR;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Write into LOGELAJB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.LOGELAJB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LSPROCES ="+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LOGELAJB','LSPROCES');
              +",LSROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODPAR),'LOGELAJB','LSROWNUM');
              +",LSROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_PRROWORD),'LOGELAJB','LSROWORD');
              +",LSFLELCR ="+cp_NullLink(cp_ToStrODBC(this.w_LSFLELCR),'LOGELAJB','LSFLELCR');
                  +i_ccchkf ;
              +" where ";
                  +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  LSPROCES = this.w_JBNOME;
                  ,LSROWNUM = this.w_PRCODPAR;
                  ,LSROWORD = this.w_PRROWORD;
                  ,LSFLELCR = this.w_LSFLELCR;
                  &i_ccchkf. ;
               where;
                  LSSERIAL = this.w_LSSERIAL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Provo a inserire il log
          * --- Try
          local bErr_04CCB9F8
          bErr_04CCB9F8=bTrsErr
          this.Try_04CCB9F8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            this.w_LOGERMSG = AH_MsgFormat("Errore nella scrittura del log schedulatore: %1", Message())
            * --- Write into LOGELAJB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.LOGELAJB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LSNOTELA ="+cp_NullLink(cp_ToStrODBC(this.w_LOGERMSG),'LOGELAJB','LSNOTELA');
                  +i_ccchkf ;
              +" where ";
                  +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  LSNOTELA = this.w_LOGERMSG;
                  &i_ccchkf. ;
               where;
                  LSSERIAL = this.w_LSSERIAL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04CCB9F8
          * --- End
        endif
        if cp_GetGlobalVar("g_FLAGSTMP")=.T.
          * --- Movimento Stampe dei job
          this.w_LEN = ALEN(g_STAMPJOB)-1
          FOR w_N=1 TO this.w_LEN
          * --- Insert into JOBSTAMP
          i_nConn=i_TableProp[this.JOBSTAMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.JOBSTAMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.JOBSTAMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"STLSSER"+",STLSRN"+",CPROWNUM"+",STFILENA"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'JOBSTAMP','STLSSER');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'JOBSTAMP','STLSRN');
            +","+cp_NullLink(cp_ToStrODBC(w_N),'JOBSTAMP','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(g_STAMPJOB(w_N)),'JOBSTAMP','STFILENA');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'STLSSER',this.w_LSSERIAL,'STLSRN',this.w_CPROWNUM,'CPROWNUM',w_N,'STFILENA',g_STAMPJOB(w_N))
            insert into (i_cTable) (STLSSER,STLSRN,CPROWNUM,STFILENA &i_ccchkf. );
               values (;
                 this.w_LSSERIAL;
                 ,this.w_CPROWNUM;
                 ,w_N;
                 ,g_STAMPJOB(w_N);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- w_FLINVS = 'S': a tutti i destinatari
          *     w_FLINVS = 'N': nessun invio
          *     w_FLINVS = 'D': secondo quanto specificato nell'elenco dei destinatari
          if this.w_FLINVS = "S" OR this.w_FLINVS = "D"
            * --- Concateno i file PDF generati dalle stampe
            this.w_PDFATTACH = this.w_PDFATTACH + ALLTRIM( FULLPATH( g_STAMPJOB( w_N ) ) ) + ";"
          endif
          ENDFOR
          RELEASE g_STAMPJOB
        endif
        this.w_MESSAGGIORESPONSO = IIF( EMPTY( g_MESSAGGIORESPONSO ) , this.w_MESSAGGIORESPONSO , this.w_MESSAGGIORESPONSO + CHR( 13 ) + g_MESSAGGIORESPONSO )
        RELEASE g_MESSAGGIORESPONSO
        cp_SetGlobalVar("g_MSG","")
          select _Curs_LISTAJOB
          continue
        enddo
        use
      endif
    else
      * --- Test negativo, non eseguo il job a causa del superamento del 
      *                 tempo massimo di ritardo
      * --- Read from SCHEDJOB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2],.t.,this.SCHEDJOB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SHDESCRI,SHFLINVS,SHTIPSCH"+;
          " from "+i_cTable+" SCHEDJOB where ";
              +"SHCODICE = "+cp_ToStrODBC(this.w_SHCOD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SHDESCRI,SHFLINVS,SHTIPSCH;
          from (i_cTable) where;
              SHCODICE = this.w_SHCOD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESCR = NVL(cp_ToDate(_read_.SHDESCRI),cp_NullValue(_read_.SHDESCRI))
        this.w_FLINVS = NVL(cp_ToDate(_read_.SHFLINVS),cp_NullValue(_read_.SHFLINVS))
        this.w_SHTIPSCH = NVL(cp_ToDate(_read_.SHTIPSCH),cp_NullValue(_read_.SHTIPSCH))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ESEGUITO = "N"
      * --- Select from LISTAJOB
      i_nConn=i_TableProp[this.LISTAJOB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2],.t.,this.LISTAJOB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select JBNOME,CPROWNUM,JBROUTINE,CPROWORD,JBCODAZI  from "+i_cTable+" LISTAJOB ";
            +" where JBCODICEJOB="+cp_ToStrODBC(this.w_SHCOD)+"";
            +" order by CPROWORD";
             ,"_Curs_LISTAJOB")
      else
        select JBNOME,CPROWNUM,JBROUTINE,CPROWORD,JBCODAZI from (i_cTable);
         where JBCODICEJOB=this.w_SHCOD;
         order by CPROWORD;
          into cursor _Curs_LISTAJOB
      endif
      if used('_Curs_LISTAJOB')
        select _Curs_LISTAJOB
        locate for 1=1
        do while not(eof())
        this.w_ora = SUBSTR(TIME(),1,8)
        this.w_data = DATE()
        cp_SetGlobalVar("g_MSG",ah_MsgFormat("Attenzione:%0il processo non � stato eseguito a causa del superamento del tempo massimo di attesa in coda del job%0"))
        this.w_JBNOME = _Curs_LISTAJOB.JBNOME
        this.w_PRCODPAR = _Curs_LISTAJOB.CPROWNUM
        this.w_PRROWORD = _Curs_LISTAJOB.CPROWORD
        this.w_JBROUTINE = _Curs_LISTAJOB.JBROUTINE
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        * --- Scrivo il log
        if this.w_CPROWNUM>1
          * --- Insert into LOGELAJB
          i_nConn=i_TableProp[this.LOGELAJB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGELAJB_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LSSERIAL"+",LSCODJOB"+",LSDATINI"+",LSPROCES"+",LSNOTELA"+",LSROWNUM"+",LSROWORD"+",CPROWNUM"+",LSFLELCR"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOGELAJB','LSSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SHCOD),'LOGELAJB','LSCODJOB');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOGELAJB','LSDATINI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LOGELAJB','LSPROCES');
            +","+cp_NullLink(cp_ToStrODBC(cp_GetGlobalVar("g_MSG")),'LOGELAJB','LSNOTELA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODPAR),'LOGELAJB','LSROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWORD),'LOGELAJB','LSROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LOGELAJB','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC("N"),'LOGELAJB','LSFLELCR');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODJOB',this.w_SHCOD,'LSDATINI',i_DATSYS,'LSPROCES',this.w_JBNOME,'LSNOTELA',cp_GetGlobalVar("g_MSG"),'LSROWNUM',this.w_PRCODPAR,'LSROWORD',this.w_PRROWORD,'CPROWNUM',this.w_CPROWNUM,'LSFLELCR',"N")
            insert into (i_cTable) (LSSERIAL,LSCODJOB,LSDATINI,LSPROCES,LSNOTELA,LSROWNUM,LSROWORD,CPROWNUM,LSFLELCR &i_ccchkf. );
               values (;
                 this.w_LSSERIAL;
                 ,this.w_SHCOD;
                 ,i_DATSYS;
                 ,this.w_JBNOME;
                 ,cp_GetGlobalVar("g_MSG");
                 ,this.w_PRCODPAR;
                 ,this.w_PRROWORD;
                 ,this.w_CPROWNUM;
                 ,"N";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Write into LOGELAJB
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LOGELAJB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LSPROCES ="+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LOGELAJB','LSPROCES');
            +",LSNOTELA ="+cp_NullLink(cp_ToStrODBC(cp_GetGlobalVar("g_MSG")),'LOGELAJB','LSNOTELA');
            +",LSROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODPAR),'LOGELAJB','LSROWNUM');
            +",LSROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_PRROWORD),'LOGELAJB','LSROWORD');
            +",LSFLELCR ="+cp_NullLink(cp_ToStrODBC("N"),'LOGELAJB','LSFLELCR');
                +i_ccchkf ;
            +" where ";
                +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                LSPROCES = this.w_JBNOME;
                ,LSNOTELA = cp_GetGlobalVar("g_MSG");
                ,LSROWNUM = this.w_PRCODPAR;
                ,LSROWORD = this.w_PRROWORD;
                ,LSFLELCR = "N";
                &i_ccchkf. ;
             where;
                LSSERIAL = this.w_LSSERIAL;
                and CPROWNUM = this.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        cp_SetGlobalVar("g_MSG","")
          select _Curs_LISTAJOB
          continue
        enddo
        use
      endif
    endif
    * --- Aggiorno data e ora per il successivo 
    * --- Read from SCHEDJOB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2],.t.,this.SCHEDJOB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SHMESE,SHNUMFREQ,SHSET,SHFREQ,SHDATASTART,SHDATAPRO,SHORAPROS,SHDATFIS,SHIMPDAT,SHMINUINT,SHORAFIN,SHORAINI"+;
        " from "+i_cTable+" SCHEDJOB where ";
            +"SHCODICE = "+cp_ToStrODBC(this.w_SHCOD);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SHMESE,SHNUMFREQ,SHSET,SHFREQ,SHDATASTART,SHDATAPRO,SHORAPROS,SHDATFIS,SHIMPDAT,SHMINUINT,SHORAFIN,SHORAINI;
        from (i_cTable) where;
            SHCODICE = this.w_SHCOD;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SHMESE = NVL(cp_ToDate(_read_.SHMESE),cp_NullValue(_read_.SHMESE))
      this.w_SHNUMFREQ = NVL(cp_ToDate(_read_.SHNUMFREQ),cp_NullValue(_read_.SHNUMFREQ))
      this.w_SHSET = NVL(cp_ToDate(_read_.SHSET),cp_NullValue(_read_.SHSET))
      this.w_SHFREQ = NVL(cp_ToDate(_read_.SHFREQ),cp_NullValue(_read_.SHFREQ))
      this.w_SHDATASTART = NVL(cp_ToDate(_read_.SHDATASTART),cp_NullValue(_read_.SHDATASTART))
      this.w_SHDATAPRO = NVL(cp_ToDate(_read_.SHDATAPRO),cp_NullValue(_read_.SHDATAPRO))
      this.w_SHORAPROS = NVL(cp_ToDate(_read_.SHORAPROS),cp_NullValue(_read_.SHORAPROS))
      this.w_SHDATFIS = NVL(cp_ToDate(_read_.SHDATFIS),cp_NullValue(_read_.SHDATFIS))
      this.w_SHIMPDAT = NVL(cp_ToDate(_read_.SHIMPDAT),cp_NullValue(_read_.SHIMPDAT))
      this.w_SHMINUINT = NVL(cp_ToDate(_read_.SHMINUINT),cp_NullValue(_read_.SHMINUINT))
      this.w_SHORAFIN = NVL(cp_ToDate(_read_.SHORAFIN),cp_NullValue(_read_.SHORAFIN))
      this.w_SHORAINI = NVL(cp_ToDate(_read_.SHORAINI),cp_NullValue(_read_.SHORAINI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_SHFREQ<>"U"
      this.w_ora_lancio = substr(this.w_SHORA,1,2)
      this.w_mm_lancio = substr(this.w_SHORA,4,2)
      this.w_ora_lancifi = substr(this.w_SHORAFIN,1,2)
      this.w_mm_lancifi = substr(this.w_SHORAFIN,4,2)
      * --- Calcolo data prossima esecuzione
      gsjb_bde(this,DATE(), this.w_SHDATASTART)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Rivalorizzo data prossima esecuzione
      this.w_ora = SUBSTR(TIME(),1,5)
      this.w_data = DATE()
      * --- Write into SCHEDJOB
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SCHEDJOB_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SHDATAPRO ="+cp_NullLink(cp_ToStrODBC(this.w_SHDATAPRO),'SCHEDJOB','SHDATAPRO');
        +",SHORAPROS ="+cp_NullLink(cp_ToStrODBC(this.w_SHORAPROS),'SCHEDJOB','SHORAPROS');
        +",SHDATAFINE ="+cp_NullLink(cp_ToStrODBC(this.w_data),'SCHEDJOB','SHDATAFINE');
        +",SHORAFNE ="+cp_NullLink(cp_ToStrODBC(this.w_ora),'SCHEDJOB','SHORAFNE');
            +i_ccchkf ;
        +" where ";
            +"SHCODICE = "+cp_ToStrODBC(this.w_SHCOD);
               )
      else
        update (i_cTable) set;
            SHDATAPRO = this.w_SHDATAPRO;
            ,SHORAPROS = this.w_SHORAPROS;
            ,SHDATAFINE = this.w_data;
            ,SHORAFNE = this.w_ora;
            &i_ccchkf. ;
         where;
            SHCODICE = this.w_SHCOD;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_SHFREQ="R"
        * --- Write into SCHEDJOB
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SCHEDJOB_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SHORAINI ="+cp_NullLink(cp_ToStrODBC(this.w_SHORAPROS),'SCHEDJOB','SHORAINI');
              +i_ccchkf ;
          +" where ";
              +"SHCODICE = "+cp_ToStrODBC(this.w_SHCOD);
                 )
        else
          update (i_cTable) set;
              SHORAINI = this.w_SHORAPROS;
              &i_ccchkf. ;
           where;
              SHCODICE = this.w_SHCOD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      this.w_ora = SUBSTR(TIME(),1,5)
      this.w_data = DATE()
      * --- Write into SCHEDJOB
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SCHEDJOB_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SHDATAFINE ="+cp_NullLink(cp_ToStrODBC(this.w_data),'SCHEDJOB','SHDATAFINE');
        +",SHORAFNE ="+cp_NullLink(cp_ToStrODBC(this.w_ora),'SCHEDJOB','SHORAFNE');
            +i_ccchkf ;
        +" where ";
            +"SHCODICE = "+cp_ToStrODBC(this.w_SHCOD);
               )
      else
        update (i_cTable) set;
            SHDATAFINE = this.w_data;
            ,SHORAFNE = this.w_ora;
            &i_ccchkf. ;
         where;
            SHCODICE = this.w_SHCOD;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Write into LOGELAJB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LOGELAJB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LSDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_data),'LOGELAJB','LSDATFIN');
          +i_ccchkf ;
      +" where ";
          +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
             )
    else
      update (i_cTable) set;
          LSDATFIN = this.w_data;
          &i_ccchkf. ;
       where;
          LSSERIAL = this.w_LSSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_SMTPFROM = g_MITTEN
    this.w_SMTPPRIO = 3
    if NVL( this.w_SHTIPSCH , "N" ) = "P"
      * --- Proattivit�
      this.w_SMTPSUBJ = ah_MsgFormat("Proattivit�: %1", ALLTRIM(this.w_DESCR))
    else
      this.w_SMTPSUBJ = ah_MsgFormat("Schedulatore di job - codice job: %1", alltrim(str(this.w_SHCOD)))
    endif
    if this.w_ESEGUITO="S"
      this.w_MESSAGGIOJOB = ah_MsgFormat("Eseguito job: %1%0Descrizione: %2%0Verificare log: %3", alltrim(str(this.w_SHCOD)), alltrim(this.w_DESCR), this.w_LSSERIAL)
    else
      this.w_MESSAGGIOJOB = ah_MsgFormat("Attenzione!%0Non eseguito job: %1%0Descrizione: %2%0Superato tempo massimo di attesa in coda del job%0Verificare log: %3", alltrim(str(this.w_SHCOD)), alltrim(this.w_DESCR), this.w_LSSERIAL)
    endif
    * --- Lancio la query per popolare il cursore __TMP__ per poter generare il PDF 
    *                 che contiene il log da allegare all' e-mail
    this.w_LSSERIAL1 = this.w_LSSERIAL
    this.w_LSSERIAL2 = this.w_LSSERIAL
    vq_exec("..\jbsh\exe\gsjb6sjb.vqr",this,"__TMP__")
    SELECT "__TMP__"
    this.w_NomeFile = tempadhoc() + "\LOG_" + ALLTRIM( STR( this.w_SHCOD ) ) + "_" + SYS(2015)+".PDF"
    * --- Creo il PDF del log nella cartella temporanea di windows
    RET = PDF_Format(this.w_NomeFile, "..\jbsh\exe\gsjb4sjb.frx")
    if not RET
      * --- Si � verificato un errore nella costruzione del PDF
      this.w_MESSAGGIOJOB = this.w_MESSAGGIOJOB+ah_MsgFormat("Impossibile allegare il file di log in formato PDF: %1", Message())
    endif
    this.w_SMTPSERV = g_SRVMAIL
    this.w_SMTPPORT = g_SRVPORTA
    cp_SetGlobalVar("g_MSG","")
    * --- Memorizza gli indirizzi a cui � gi� stato eseguito l'inoltro
    this.w_DESTINATARISOLOLOG = ""
    this.w_DESTINATARISOLOELAB = ""
    this.w_DESTINATARILOGEELAB = ""
    * --- Per sapere se occorre eseguire l'inoltro
    * --- Select from EMAIL2
    do vq_exec with 'EMAIL2',this,'_Curs_EMAIL2','',.f.,.t.
    if used('_Curs_EMAIL2')
      select _Curs_EMAIL2
      locate for 1=1
      do while not(eof())
      if NVL(UGTIPMES," ") <>"R" Or not this.w_SOLOCONLOG
        this.w_SMTP__TO = NVL(UTEMAIL,"")
        this.w_INOLTROGIAESEGUITO = .F.
        * --- Codice utente
        this.w_UTCODICE = UTCODICE
        * --- Tipo invio: M mail - P post in
        this.w_UGTIPINV = NVL(UGTIPINV,"M")
        * --- w_UGTIPMES Tipo messaggio: L Solo log - R Solo stampe - E Log e stampe
        this.w_UGTIPMES = NVL(UGTIPMES,"E")
        * --- Allego tutte le stampe o solo il log
        * --- w_FLINVS = 'S': a tutti i destinatari
        *     w_FLINVS = 'N': nessun invio
        *     w_FLINVS = 'D': secondo quanto specificato nell'elenco dei destinatari
        * --- w_UGTIPMES Tipo messaggio: L Solo log - R Solo stampe - E Log e stampe
        * --- w_MESSAGGIOJOB � il messaggio di log fornito dall'usuale schedulatore di job
        * --- g_MESSAGGIORESPONSO � il messaggio della proattivit� da inserire nel corpo della mail
        do case
          case this.w_FLINVS = "S" OR this.w_FLINVS = "D" AND this.w_UGTIPMES = "E"
            * --- Log + elaborazione
            this.w_SMTPBODY = this.w_MESSAGGIOJOB+IIF(EMPTY(this.w_MESSAGGIORESPONSO),"",CHR(13)+CHR(10)+this.w_MESSAGGIORESPONSO)
            this.w_SMTP_ATT = this.w_PDFATTACH + this.w_NomeFile
            * --- Controlla se log+elaborazione sono gi� stati inoltrati al destinatario corrente
            if ALLTRIM( this.w_SMTP__TO ) $ this.w_DESTINATARILOGEELAB
              this.w_INOLTROGIAESEGUITO = .T.
            else
              this.w_INOLTROGIAESEGUITO = .F.
              this.w_DESTINATARILOGEELAB = this.w_DESTINATARILOGEELAB + ";" + ALLTRIM( this.w_SMTP__TO )
            endif
          case this.w_FLINVS = "N" OR this.w_FLINVS = "D" AND this.w_UGTIPMES = "L"
            * --- Solo log
            this.w_SMTPBODY = this.w_MESSAGGIOJOB
            this.w_SMTP_ATT = this.w_NomeFile
            * --- Controlla se il solo log � gi� stato inoltrato al destinatario corrente
            if ALLTRIM( this.w_SMTP__TO ) $ this.w_DESTINATARISOLOLOG
              this.w_INOLTROGIAESEGUITO = .T.
            else
              this.w_INOLTROGIAESEGUITO = .F.
              this.w_DESTINATARISOLOLOG = this.w_DESTINATARISOLOLOG + ";" + ALLTRIM( this.w_SMTP__TO )
            endif
          case this.w_FLINVS = "D" AND this.w_UGTIPMES = "R"
            * --- Solo elaborazione
            this.w_SMTPBODY = this.w_MESSAGGIORESPONSO
            * --- In questo caso occorre rimuovere il ';' in fondo a w_PDFATTACH
            this.w_SMTP_ATT = LEFT(ALLTRIM(this.w_PDFATTACH),RAT(";",this.w_PDFATTACH)-1)
            * --- Controlla se la sola elaborazione � gi� stata inoltrata al destinatario corrente
            if ALLTRIM( this.w_SMTP__TO ) $ this.w_DESTINATARISOLOELAB
              this.w_INOLTROGIAESEGUITO = .T.
            else
              this.w_INOLTROGIAESEGUITO = .F.
              this.w_DESTINATARISOLOELAB = this.w_DESTINATARISOLOELAB + ";" + ALLTRIM( this.w_SMTP__TO )
            endif
        endcase
        * --- --
        if NOT EMPTY(this.w_SMTP__TO) AND this.w_UGTIPINV="M" AND NOT this.w_INOLTROGIAESEGUITO
          * --- Selezionato l'invio tramite e-mail
          this.w_ErrEmail = EMAIL(this.w_SMTP_ATT, "N", .F. , this.w_SMTP__TO,this.w_SMTPSUBJ,this.w_SMTPBODY, .F.)
          if TYPE("i_EMAIL")="N"
            this.w_LSFLELCR = IIF(i_EMAIL=0,"S","N")
          else
            this.w_LSFLELCR = "N"
          endif
          * --- Select from EMAIL4
          do vq_exec with 'EMAIL4',this,'_Curs_EMAIL4','',.f.,.t.
          if used('_Curs_EMAIL4')
            select _Curs_EMAIL4
            locate for 1=1
            do while not(eof())
            if this.w_ErrEmail
              this.w_MSGT = this.w_MSGT+ah_MsgFormat("%0E-mail inviata a utente: %1 %2, indirizzo: %3%0", ALLTRIM(STR(UTE)), alltrim(DESCR), ALLTRIM(this.w_SMTP__TO))
            else
              this.w_MSGT = this.w_MSGT+ah_MsgFormat("%0Impossibile inviare e-mail a utente: %1 %2, indirizzo: %3%0%4%0", ALLTRIM(STR(UTE)), alltrim(DESCR), ALLTRIM(this.w_SMTP__TO), cp_GetGlobalVar("g_MSG"))
            endif
              select _Curs_EMAIL4
              continue
            enddo
            use
          endif
          cp_SetGlobalVar("g_MSG","")
        endif
        if this.w_UGTIPINV = "P"
          * --- Selezionata la notifica tramite postit
          * --- --
          if NOT i_bDisablePostin
            if "ORACLE"=upper(CP_DBTYPE) AND LEN( this.w_SMTPBODY ) > 1000
              this.w_SMTPBODYpostin = LEFT( AH_MSGFORMAT( "Attenzione: messaggio incompleto perch� di lunghezza eccedente i 2000 caratteri. Si consiglia l'uso degli allegati PDF%0" ) + this.w_SMTPBODY , 1000 )
            else
              this.w_SMTPBODYpostin = this.w_SMTPBODY
            endif
            GSUT_BIP(this, this.w_UTCODICE , this.w_SMTPBODYpostin , this.w_SMTP_ATT )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_MSGT = this.w_MSGT+ah_MsgFormat("%0Post-in inviato a utente %1%0", ALLTRIM(STR(this.w_UTCODICE)))
          else
            this.w_MSGT = this.w_MSGT+ah_MsgFormat("%0Impossibile inviare post-in all'utente %1: la funzionalit� � disabilitata%0", ALLTRIM(STR(this.w_UTCODICE)), )
          endif
        endif
      endif
        select _Curs_EMAIL2
        continue
      enddo
      use
    endif
    this.w_UTE = ""
    * --- Select from EMAIL3
    do vq_exec with 'EMAIL3',this,'_Curs_EMAIL3','',.f.,.t.
    if used('_Curs_EMAIL3')
      select _Curs_EMAIL3
      locate for 1=1
      do while not(eof())
      this.w_UTE = IIF( UGTIPINV="P", this.w_UTE , this.w_UTE+chr(13)+ALLTRIM(STR(UGCODUTE))+ " "+NAME+", " )
        select _Curs_EMAIL3
        continue
      enddo
      use
    endif
    if !empty(this.w_UTE)
      this.w_MSGT = this.w_MSGT+ah_MsgFormat("%0Attenzione: per l'utente: %1%0non sono stati definiti indirizzi e-mail", SUBSTR(this.w_UTE,1,LEN(this.w_UTE)-2))
    endif
    * --- Select from LOGELAJB
    i_nConn=i_TableProp[this.LOGELAJB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2],.t.,this.LOGELAJB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select LSNOTELA,CPROWNUM  from "+i_cTable+" LOGELAJB ";
          +" where LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL)+"";
           ,"_Curs_LOGELAJB")
    else
      select LSNOTELA,CPROWNUM from (i_cTable);
       where LSSERIAL=this.w_LSSERIAL;
        into cursor _Curs_LOGELAJB
    endif
    if used('_Curs_LOGELAJB')
      select _Curs_LOGELAJB
      locate for 1=1
      do while not(eof())
      cp_SetGlobalVar("g_MSG",LSNOTELA)
      this.w_CPROWNUM = CPROWNUM
      * --- Aggiungo al messaggio di log l'invio delle email
      cp_SetGlobalVar("g_MSG",cp_GetGlobalVar("g_MSG")+CHR(13)+CHR(13)+this.w_MSGT)
      this.w_lunghezza = LEN(cp_GetGlobalVar("g_MSG"))
      * --- Controllo se sto usando oracle, in quest'ultimo caso devo controllare la lunghezza del log
      if !("ORACLE"=upper(CP_DBTYPE) AND this.w_lunghezza > 4000)
        * --- Non sono in oracle o il msg di log � minore di 4000
        * --- Scrivo il log
        * --- Write into LOGELAJB
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LOGELAJB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LSNOTELA ="+cp_NullLink(cp_ToStrODBC(cp_GetGlobalVar("g_MSG")),'LOGELAJB','LSNOTELA');
              +i_ccchkf ;
          +" where ";
              +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              LSNOTELA = cp_GetGlobalVar("g_MSG");
              &i_ccchkf. ;
           where;
              LSSERIAL = this.w_LSSERIAL;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_LOGELAJB
        continue
      enddo
      use
    endif
    * --- Rimuovo il PDF di appoggio e il cursore temporaneo
    if used("__TMP__")
      Select __TMP__ 
 Use
    endif
    * --- Il PDf generato ed inviato come allegato non � cancellabile in quanto
    *                 lockato dalla DLL\PROCEDURA.
    *                 Un comando come
    *                 DELETE FILE ( w_NomeFile )
    *                 Da quindi come responso File Access is Denied
    * --- Prima di disattivare lo schedulatore, elimino il file mutex
    DELETE FILE(this.w_MTXFILE)
    * --- Lo schedulatore � disattivo
    cp_SetGlobalVar("g_SCHEDULER","N")
  endproc
  proc Try_04CBC2F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into LOGELAJB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LOGELAJB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LSNOTELA ="+cp_NullLink(cp_ToStrODBC(this.w_MSGT),'LOGELAJB','LSNOTELA');
          +i_ccchkf ;
      +" where ";
          +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          LSNOTELA = this.w_MSGT;
          &i_ccchkf. ;
       where;
          LSSERIAL = this.w_LSSERIAL;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04CB71F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into LOGELAJB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LOGELAJB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LSNOTELA ="+cp_NullLink(cp_ToStrODBC(this.w_MSGT),'LOGELAJB','LSNOTELA');
          +i_ccchkf ;
      +" where ";
          +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          LSNOTELA = this.w_MSGT;
          &i_ccchkf. ;
       where;
          LSSERIAL = this.w_LSSERIAL;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04CCB9F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into LOGELAJB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LOGELAJB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELAJB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGELAJB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LSNOTELA ="+cp_NullLink(cp_ToStrODBC(cp_GetGlobalVar("g_MSG")),'LOGELAJB','LSNOTELA');
          +i_ccchkf ;
      +" where ";
          +"LSSERIAL = "+cp_ToStrODBC(this.w_LSSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          LSNOTELA = cp_GetGlobalVar("g_MSG");
          &i_ccchkf. ;
       where;
          LSSERIAL = this.w_LSSERIAL;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza le propriet� ed i controlli dell'oggetto form painter, maschera o anagrafica
    * --- Select from PARAMJ
    i_nConn=i_TableProp[this.PARAMJ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARAMJ_idx,2],.t.,this.PARAMJ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PRNOMVAL,PRVALVAR,PRFLGPP,PRTIPPAR  from "+i_cTable+" PARAMJ ";
          +" where PRJOBCOD="+cp_ToStrODBC(this.w_SHCOD)+" AND PRCODPAR="+cp_ToStrODBC(this.w_PRCODPAR)+"";
          +" order by CPROWORD";
           ,"_Curs_PARAMJ")
    else
      select PRNOMVAL,PRVALVAR,PRFLGPP,PRTIPPAR from (i_cTable);
       where PRJOBCOD=this.w_SHCOD AND PRCODPAR=this.w_PRCODPAR;
       order by CPROWORD;
        into cursor _Curs_PARAMJ
    endif
    if used('_Curs_PARAMJ')
      select _Curs_PARAMJ
      locate for 1=1
      do while not(eof())
      do case
        case _Curs_PARAMJ.PRTIPPAR="V"
          * --- Assegno i parametri della maschera
          this.w_PRNOMVAL = _Curs_PARAMJ.PRNOMVAL
          this.w_PRVALVAR = _Curs_PARAMJ.PRVALVAR
          this.w_PRFLGPP = NVL(_Curs_PARAMJ.PRFLGPP," ")
          w_PRNOMVAL_GO=this.w_PRNOMVAL
          w_PRVALVAR_GO=this.w_PRVALVAR
          if this.w_PRFLGPP="P"
            this.w_COUNT = this.w_COUNT+1
            DIMENSION w_GLOBVAR(this.w_COUNT) 
 w_GLOBVAR(this.w_COUNT)=this.w_PRNOMVAL
            w_PRNOMVAL_GO=this.w_PRNOMVAL
            PUBLIC &w_PRNOMVAL_GO
          else
            * --- Valorizzo parametro maschera
            if AT(".", this.w_PRNOMVAL) <> 0
              * --- Se � un figlio ho un ".", richiamo GSUT_BVP cambiando il padre e il parametro
              w_CHILD = JUSTSTEM(this.w_PRNOMVAL)
              this.w_PARAMCHILD = JUSTEXT(this.w_PRNOMVAL)
              if UPPER(this.w_OBJPARENT.&w_CHILD..Class) == "CP_OUTPUTCOMBO"
                GSUT_BVP(this,this.w_OBJPARENT, this.w_PRNOMVAL, this.w_PRVALVAR)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                GSUT_BVP(this,this.w_OBJPARENT.&w_CHILD, this.w_PARAMCHILD, this.w_PRVALVAR)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              GSUT_BVP(this,this.w_OBJPARENT, this.w_PRNOMVAL, this.w_PRVALVAR)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        case _Curs_PARAMJ.PRTIPPAR="B"
          this.w_PRNOMVAL = _Curs_PARAMJ.PRNOMVAL
          * --- Tramite UID recupero il controllo StdButton e lancio il metodo click
          this.w_BUTTON = this.w_OBJPARENT.GetCtrl(ALLTRIM(this.w_PRNOMVAL))
          this.w_BUTTON.Click()     
          this.w_BUTTON = .null.
      endcase
        select _Curs_PARAMJ
        continue
      enddo
      use
    endif
    FOR w_N=1 TO this.w_COUNT
    w_PRNOMVAL_GO=w_GLOBVAR(w_N)
    &w_PRNOMVAL_GO = this.w_OBJPARENT.&w_PRNOMVAL_GO
    ENDFOR
    do case
      case this.w_JBTIPESC="S"
        * --- Eseguo F10
        this.w_OBJPARENT.ecpsave()     
      case this.w_JBTIPESC="Q"
        * --- Eseguo ESC
        this.w_OBJPARENT.ecpquit()     
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_SHCOD,w_SHDATA,w_SHORA,w_SHTMPMAX)
    this.w_SHCOD=w_SHCOD
    this.w_SHDATA=w_SHDATA
    this.w_SHORA=w_SHORA
    this.w_SHTMPMAX=w_SHTMPMAX
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='SCHEDJOB'
    this.cWorkTables[2]='LISTAJOB'
    this.cWorkTables[3]='PARAMJ'
    this.cWorkTables[4]='CONTROPA'
    this.cWorkTables[5]='LOGELAJB'
    this.cWorkTables[6]='CPUSERS'
    this.cWorkTables[7]='AZIENDA'
    this.cWorkTables[8]='BUSIUNIT'
    this.cWorkTables[9]='UTE_AZI'
    this.cWorkTables[10]='JOBSTAMP'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_LISTAJOB')
      use in _Curs_LISTAJOB
    endif
    if used('_Curs_LISTAJOB')
      use in _Curs_LISTAJOB
    endif
    if used('_Curs_EMAIL2')
      use in _Curs_EMAIL2
    endif
    if used('_Curs_EMAIL4')
      use in _Curs_EMAIL4
    endif
    if used('_Curs_EMAIL3')
      use in _Curs_EMAIL3
    endif
    if used('_Curs_LOGELAJB')
      use in _Curs_LOGELAJB
    endif
    if used('_Curs_PARAMJ')
      use in _Curs_PARAMJ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SHCOD,w_SHDATA,w_SHORA,w_SHTMPMAX"
endproc
