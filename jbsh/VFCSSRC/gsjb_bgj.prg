* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bgj                                                        *
*              Selezione gruppo schedulatore                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-16                                                      *
* Last revis.: 2004-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bgj",oParentObject,m.w_OP)
return(i_retval)

define class tgsjb_bgj as StdBatch
  * --- Local variables
  w_OP = space(1)
  w_CODICE = 0
  w_PUNPAD = .NULL.
  * --- WorkFile variables
  GROUPSJ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PUNPAD = THIS.OPARENTOBJECT
    do case
      case this.w_OP="I"
        * --- Select from GROUPSJ
        i_nConn=i_TableProp[this.GROUPSJ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GROUPSJ_idx,2],.t.,this.GROUPSJ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select GJCODICE  from "+i_cTable+" GROUPSJ ";
               ,"_Curs_GROUPSJ")
        else
          select GJCODICE from (i_cTable);
            into cursor _Curs_GROUPSJ
        endif
        if used('_Curs_GROUPSJ')
          select _Curs_GROUPSJ
          locate for 1=1
          do while not(eof())
          this.w_CODICE = _Curs_GROUPSJ.GJCODICE
            select _Curs_GROUPSJ
            continue
          enddo
          use
        endif
        if USED(this.w_PUNPAD.w_ZoomGrp.cCursor)
          MM=this.w_PUNPAD.w_ZoomGrp.cCursor
          Select (MM)
          GO TOP
          Scan
          REPLACE XCHK WITH 1 FOR CODE=this.w_CODICE
          Endscan
          GO TOP
          this.w_PUNPAD.w_OLDG=this.w_CODICE
          this.w_PUNPAD.w_NEWG=this.w_CODICE
        endif
      case this.w_OP="S"
        if USED(this.w_PUNPAD.w_ZoomGrp.cCursor)
          this.w_PUNPAD.w_NEWG=this.w_PUNPAD.w_TMPG
          MM=this.w_PUNPAD.w_ZoomGrp.cCursor
          Select (MM)
          w_NR=RECNO()
          GO TOP
          Scan
          REPLACE XCHK WITH 0 FOR CODE<>this.w_PUNPAD.w_NEWG
          Endscan
          GO w_NR
        endif
      case this.w_OP="U"
        this.w_PUNPAD.w_NEWG=0
      case this.w_OP="A"
        * --- Delete from GROUPSJ
        i_nConn=i_TableProp[this.GROUPSJ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GROUPSJ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"GJCODICE = "+cp_ToStrODBC(this.w_PUNPAD.w_OLDG);
                 )
        else
          delete from (i_cTable) where;
                GJCODICE = this.w_PUNPAD.w_OLDG;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        if this.w_PUNPAD.w_NEWG<>0
          * --- Insert into GROUPSJ
          i_nConn=i_TableProp[this.GROUPSJ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GROUPSJ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GROUPSJ_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"GJCODICE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PUNPAD.w_NEWG),'GROUPSJ','GJCODICE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'GJCODICE',this.w_PUNPAD.w_NEWG)
            insert into (i_cTable) (GJCODICE &i_ccchkf. );
               values (;
                 this.w_PUNPAD.w_NEWG;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_PUNPAD.w_OLDG=this.w_PUNPAD.w_NEWG
        endif
        this.w_PUNPAD.ecpquit()
    endcase
  endproc


  proc Init(oParentObject,w_OP)
    this.w_OP=w_OP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='GROUPSJ'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GROUPSJ')
      use in _Curs_GROUPSJ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OP"
endproc
