* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_ka3                                                        *
*              Destinatari                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-15                                                      *
* Last revis.: 2010-08-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsjb_ka3",oParentObject))

* --- Class definition
define class tgsjb_ka3 as StdForm
  Top    = 10
  Left   = 2

  * --- Standard Properties
  Width  = 721
  Height = 484
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-08-31"
  HelpContextID=132804201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  SCHEDJOB_IDX = 0
  cPrg = "gsjb_ka3"
  cComment = "Destinatari"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SHCODICE = 0
  o_SHCODICE = 0
  w_SHDESCRI = space(254)
  w_SHFLINVS = space(1)
  o_SHFLINVS = space(1)

  * --- Children pointers
  GSJB_MSA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSJB_MSA additive
    with this
      .Pages(1).addobject("oPag","tgsjb_ka3Pag1","gsjb_ka3",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSJB_MSA
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='SCHEDJOB'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSJB_MSA = CREATEOBJECT('stdDynamicChild',this,'GSJB_MSA',this.oPgFrm.Page1.oPag.oLinkPC_1_6)
    this.GSJB_MSA.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSJB_MSA)
      this.GSJB_MSA.DestroyChildrenChain()
      this.GSJB_MSA=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_6')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSJB_MSA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSJB_MSA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSJB_MSA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSJB_MSA.SetKey(;
            .w_SHCODICE,"UGSHJOB";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSJB_MSA.ChangeRow(this.cRowID+'      1',1;
             ,.w_SHCODICE,"UGSHJOB";
             )
      .WriteTo_GSJB_MSA()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSJB_MSA)
        i_f=.GSJB_MSA.BuildFilter()
        if !(i_f==.GSJB_MSA.cQueryFilter)
          i_fnidx=.GSJB_MSA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSJB_MSA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSJB_MSA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSJB_MSA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSJB_MSA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSJB_MSA()
  if at('gsjb_msa',lower(this.GSJB_MSA.class))<>0
    if this.GSJB_MSA.w_SHFLINVS<>this.w_SHFLINVS
      this.GSJB_MSA.w_SHFLINVS = this.w_SHFLINVS
      this.GSJB_MSA.mCalc(.t.)
    endif
  endif
  return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSJB_MSA(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSJB_MSA.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSJB_MSA(i_ask)
    if this.GSJB_MSA.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (Destinatari)'))
      cp_BeginTrs()
      this.GSJB_MSA.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SHCODICE=0
      .w_SHDESCRI=space(254)
      .w_SHFLINVS=space(1)
      .w_SHCODICE=oParentObject.w_SHCODICE
      .w_SHFLINVS=oParentObject.w_SHFLINVS
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SHCODICE))
          .link_1_1('Full')
        endif
      .GSJB_MSA.NewDocument()
      .GSJB_MSA.ChangeRow('1',1,.w_SHCODICE,"UGSHJOB")
      if not(.GSJB_MSA.bLoaded)
        .GSJB_MSA.SetKey(.w_SHCODICE,"UGSHJOB")
      endif
    endwith
    this.DoRTCalc(2,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSJB_MSA.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSJB_MSA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SHCODICE=.w_SHCODICE
      .oParentObject.w_SHFLINVS=.w_SHFLINVS
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        if  .o_SHFLINVS<>.w_SHFLINVS
          .WriteTo_GSJB_MSA()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_SHCODICE<>.o_SHCODICE
          .Save_GSJB_MSA(.t.)
          .GSJB_MSA.NewDocument()
          .GSJB_MSA.ChangeRow('1',1,.w_SHCODICE,"UGSHJOB")
          if not(.GSJB_MSA.bLoaded)
            .GSJB_MSA.SetKey(.w_SHCODICE,"UGSHJOB")
          endif
        endif
      endwith
      this.DoRTCalc(2,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsjb_ka3
    IF cEvent = "w_aziproce after query"
      Select (this.w_AZIPROCE.cCursor )
      go top
      scan
      REPLACE XCHK WITH ATTIVATO
      ENDSCAN
    ENDIF
    
    if cEvent = "w_SELEZI Changed"
      SELECT (THIS.w_AZIPROCE.cCURSOR)
      GO TOP
      SCAN
        IF THIS.w_SELEZI='S'
          REPLACE XCHK WITH 1
        ELSE
          REPLACE XCHK WITH 0
        ENDIF
      ENDSCAN
      SELECT (THIS.w_AZIPROCE.cCURSOR)
      GO TOP
    endif
    
    if cEvent = "w_aziproce row unchecked"
      SELECT (THIS.w_AZIPROCE.cCURSOR)
      L_RIGA = RECNO()
      sum xchk to L_CONTACHECK
      IF L_CONTACHECK = 0
        AH_ERRORMSG("Nessuna azienda selezionata" , "!" )
      ENDIF
      IF L_RIGA > 0 AND L_RIGA <= RECCOUNT(THIS.w_AZIPROCE.cCURSOR)
        GO L_RIGA
      ENDIF
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SHCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_lTable = "SCHEDJOB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2], .t., this.SCHEDJOB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SHCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SHCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(this.w_SHCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',this.w_SHCODICE)
            select SHCODICE,SHDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SHCODICE = NVL(_Link_.SHCODICE,0)
      this.w_SHDESCRI = NVL(_Link_.SHDESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_SHCODICE = 0
      endif
      this.w_SHDESCRI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])+'\'+cp_ToStr(_Link_.SHCODICE,1)
      cp_ShowWarn(i_cKey,this.SCHEDJOB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SHCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSHCODICE_1_1.value==this.w_SHCODICE)
      this.oPgFrm.Page1.oPag.oSHCODICE_1_1.value=this.w_SHCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSHDESCRI_1_5.value==this.w_SHDESCRI)
      this.oPgFrm.Page1.oPag.oSHDESCRI_1_5.value=this.w_SHDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSJB_MSA.CheckForm()
      if i_bres
        i_bres=  .GSJB_MSA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SHCODICE = this.w_SHCODICE
    this.o_SHFLINVS = this.w_SHFLINVS
    * --- GSJB_MSA : Depends On
    this.GSJB_MSA.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsjb_ka3Pag1 as StdContainer
  Width  = 717
  height = 484
  stdWidth  = 717
  stdheight = 484
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSHCODICE_1_1 as StdField with uid="DJEIJOTCZP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SHCODICE", cQueryName = "SHCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 94965611,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=83, Top=8, cLinkFile="SCHEDJOB", oKey_1_1="SHCODICE", oKey_1_2="this.w_SHCODICE"

  func oSHCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oBtn_1_2 as StdButton with uid="TSXESUULAA",left=600, top=432, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Conferma l'attivazione delle proattivitą selezionate";
    , HelpContextID = 243962521;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="PXBGFOADEM",left=658, top=432, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226577174;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSHDESCRI_1_5 as StdField with uid="XWFCYTMGEL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SHDESCRI", cQueryName = "SHDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 9379695,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=166, Top=8, InputMask=replicate('X',254)


  add object oLinkPC_1_6 as stdDynamicChildContainer with uid="XALLSZZTZR",left=2, top=31, width=706, height=391, bOnScreen=.t.;


  add object oStr_1_4 as StdString with uid="VMJQJEAUCT",Visible=.t., Left=11, Top=10,;
    Alignment=1, Width=69, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_ka3','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
