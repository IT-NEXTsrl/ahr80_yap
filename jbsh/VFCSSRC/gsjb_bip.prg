* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bip                                                        *
*              Inserimento proattivita nei processi                            *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-30                                                      *
* Last revis.: 2008-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bip",oParentObject)
return(i_retval)

define class tgsjb_bip as StdBatch
  * --- Local variables
  w_JBNOME = space(20)
  w_JBDESC = space(254)
  w_JBTIPESC = space(1)
  w_JBCODAZI = space(5)
  w_PROCESSOINSERITO = .f.
  * --- WorkFile variables
  LISTAJOB_idx=0
  GESTSJBM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codice del job
    * --- Tipo job (N normale/P proattivit�)
    if this.oParentObject.w_SHTIPSCH = "P"
      * --- Programma da lanciare
      this.w_JBNOME = "GSJB_BXP WITH THIS"
      * --- Descrizione del programma da lanciare
      this.w_JBDESC = "Esecuzione della proattivita"
      * --- Tipologia di uscita
      this.w_JBTIPESC = "N"
      * --- Try
      local bErr_0369A210
      bErr_0369A210=bTrsErr
      this.Try_0369A210()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Il record � gi� inserito
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0369A210
      * --- End
      * --- Codice azienda
      this.w_JBCODAZI = i_CODAZI
      if this.oParentObject.CurrentEvent = "Insert end"
        * --- Al salvataggio inserisce il record nella tabella dei processi.
        *     Siccome � stata rimessa a video la pagina dei processi, occorre controllare se la pagina dei processi � stata
        *     compilata a mano
        *     Se non contiene record, viene automaticamente inserito un processo per
        *     l'azienda corrente
        this.w_PROCESSOINSERITO = .F.
        * --- Select from LISTAJOB
        i_nConn=i_TableProp[this.LISTAJOB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2],.t.,this.LISTAJOB_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" LISTAJOB ";
              +" where JBCODICEJOB = "+cp_ToStrODBC(this.oParentObject.w_SHCODICE)+"";
               ,"_Curs_LISTAJOB")
        else
          select * from (i_cTable);
           where JBCODICEJOB = this.oParentObject.w_SHCODICE;
            into cursor _Curs_LISTAJOB
        endif
        if used('_Curs_LISTAJOB')
          select _Curs_LISTAJOB
          locate for 1=1
          do while not(eof())
          this.w_PROCESSOINSERITO = .T.
            select _Curs_LISTAJOB
            continue
          enddo
          use
        endif
        if NOT this.w_PROCESSOINSERITO
          * --- Insert into LISTAJOB
          i_nConn=i_TableProp[this.LISTAJOB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTAJOB_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LISTAJOB_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"JBCODICEJOB"+",CPROWNUM"+",CPROWORD"+",JBNOME"+",JBDESC"+",JBROUTINE"+",JBCODAZI"+",JBTIPESC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SHCODICE),'LISTAJOB','JBCODICEJOB');
            +","+cp_NullLink(cp_ToStrODBC(1),'LISTAJOB','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(10),'LISTAJOB','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'LISTAJOB','JBNOME');
            +","+cp_NullLink(cp_ToStrODBC(this.w_JBDESC),'LISTAJOB','JBDESC');
            +","+cp_NullLink(cp_ToStrODBC("R"),'LISTAJOB','JBROUTINE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_JBCODAZI),'LISTAJOB','JBCODAZI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_JBTIPESC),'LISTAJOB','JBTIPESC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'JBCODICEJOB',this.oParentObject.w_SHCODICE,'CPROWNUM',1,'CPROWORD',10,'JBNOME',this.w_JBNOME,'JBDESC',this.w_JBDESC,'JBROUTINE',"R",'JBCODAZI',this.w_JBCODAZI,'JBTIPESC',this.w_JBTIPESC)
            insert into (i_cTable) (JBCODICEJOB,CPROWNUM,CPROWORD,JBNOME,JBDESC,JBROUTINE,JBCODAZI,JBTIPESC &i_ccchkf. );
               values (;
                 this.oParentObject.w_SHCODICE;
                 ,1;
                 ,10;
                 ,this.w_JBNOME;
                 ,this.w_JBDESC;
                 ,"R";
                 ,this.w_JBCODAZI;
                 ,this.w_JBTIPESC;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Inserisce le attivit� secondo lo zoom
        this.oParentObject.NotifyEvent( "AggiornaAziende" )
      endif
    endif
  endproc
  proc Try_0369A210()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GESTSJBM
    i_nConn=i_TableProp[this.GESTSJBM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GESTSJBM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GSNOME"+",GSDESC"+",GSTIPESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_JBNOME),'GESTSJBM','GSNOME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_JBDESC),'GESTSJBM','GSDESC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_JBTIPESC),'GESTSJBM','GSTIPESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GSNOME',this.w_JBNOME,'GSDESC',this.w_JBDESC,'GSTIPESC',this.w_JBTIPESC)
      insert into (i_cTable) (GSNOME,GSDESC,GSTIPESC &i_ccchkf. );
         values (;
           this.w_JBNOME;
           ,this.w_JBDESC;
           ,this.w_JBTIPESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LISTAJOB'
    this.cWorkTables[2]='GESTSJBM'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_LISTAJOB')
      use in _Curs_LISTAJOB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
