* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_bap                                                        *
*              Attivazione proattivitą                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-14                                                      *
* Last revis.: 2009-08-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SHCODICE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_bap",oParentObject,m.w_SHCODICE)
return(i_retval)

define class tgsjb_bap as StdBatch
  * --- Local variables
  w_SHCODICE = 0
  w_SHSET = space(50)
  w_SHNUMFREQ = 0
  w_SHMESE = space(75)
  w_SHIMPDAT = ctod("  /  /  ")
  w_SHORAFIN = space(5)
  w_SHORAPROS = space(5)
  w_SHORAINI = space(5)
  w_SHMINUINT = 0
  w_SHDATAPRO = ctod("  /  /  ")
  w_ora_lancifi = space(2)
  w_mm_lancifi = space(2)
  w_ora_lancio = space(2)
  w_mm_lancio = space(2)
  w_SHFREQ = space(1)
  w_SHDATFIS = ctod("  /  /  ")
  w_SHDATASTART = ctod("  /  /  ")
  * --- WorkFile variables
  SCHEDJOB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codice del job
    * --- Giorni della settimana selezionati
    * --- --Cadenza ripetizione del lancio del processo
    * --- --Mesi selezionati
    * --- Tipo data mese scelta
    * --- --tipo della frequenza temporale, unico, giornaliero, settimanale, mensile
    * --- Read from SCHEDJOB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2],.t.,this.SCHEDJOB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SHFREQ,SHNUMFREQ,SHSET,SHMESE,SHIMPDAT,SHDATFIS,SHDATAPRO,SHDATASTART,SHORAPROS,SHMINUINT,SHORAFIN,SHORAINI,SHDESCRI,SHFINPRE"+;
        " from "+i_cTable+" SCHEDJOB where ";
            +"SHCODICE = "+cp_ToStrODBC(this.w_SHCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SHFREQ,SHNUMFREQ,SHSET,SHMESE,SHIMPDAT,SHDATFIS,SHDATAPRO,SHDATASTART,SHORAPROS,SHMINUINT,SHORAFIN,SHORAINI,SHDESCRI,SHFINPRE;
        from (i_cTable) where;
            SHCODICE = this.w_SHCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SHFREQ = NVL(cp_ToDate(_read_.SHFREQ),cp_NullValue(_read_.SHFREQ))
      this.w_SHNUMFREQ = NVL(cp_ToDate(_read_.SHNUMFREQ),cp_NullValue(_read_.SHNUMFREQ))
      this.w_SHSET = NVL(cp_ToDate(_read_.SHSET),cp_NullValue(_read_.SHSET))
      this.w_SHMESE = NVL(cp_ToDate(_read_.SHMESE),cp_NullValue(_read_.SHMESE))
      this.w_SHIMPDAT = NVL(cp_ToDate(_read_.SHIMPDAT),cp_NullValue(_read_.SHIMPDAT))
      this.w_SHDATFIS = NVL(cp_ToDate(_read_.SHDATFIS),cp_NullValue(_read_.SHDATFIS))
      this.w_SHDATAPRO = NVL(cp_ToDate(_read_.SHDATAPRO),cp_NullValue(_read_.SHDATAPRO))
      this.w_SHDATASTART = NVL(cp_ToDate(_read_.SHDATASTART),cp_NullValue(_read_.SHDATASTART))
      this.w_SHORAPROS = NVL(cp_ToDate(_read_.SHORAPROS),cp_NullValue(_read_.SHORAPROS))
      this.w_SHMINUINT = NVL(cp_ToDate(_read_.SHMINUINT),cp_NullValue(_read_.SHMINUINT))
      this.w_SHORAFIN = NVL(cp_ToDate(_read_.SHORAFIN),cp_NullValue(_read_.SHORAFIN))
      this.w_SHORAINI = NVL(cp_ToDate(_read_.SHORAINI),cp_NullValue(_read_.SHORAINI))
      w_SHDESCRI = NVL(cp_ToDate(_read_.SHDESCRI),cp_NullValue(_read_.SHDESCRI))
      w_SHFINPRE = NVL(cp_ToDate(_read_.SHFINPRE),cp_NullValue(_read_.SHFINPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ora_lancio = iif(empty(this.w_SHORAINI),"00",SUBSTR(this.w_SHORAINI,1,2))
    this.w_mm_lancio = iif(empty(this.w_SHORAINI),"00",SUBSTR(this.w_SHORAINI,4,2))
    this.w_ora_lancifi = iif(empty(this.w_SHORAFIN),"23",SUBSTR(this.w_SHORAFIN,1,2))
    this.w_mm_lancifi = iif(empty(this.w_SHORAFIN),"59",SUBSTR(this.w_SHORAFIN,4,2))
    gsjb_bde(this,i_datsys)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Write into SCHEDJOB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SCHEDJOB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SHFLAT ="+cp_NullLink(cp_ToStrODBC("S"),'SCHEDJOB','SHFLAT');
      +",SHORAFIN ="+cp_NullLink(cp_ToStrODBC(this.w_SHORAFIN),'SCHEDJOB','SHORAFIN');
      +",SHORAPROS ="+cp_NullLink(cp_ToStrODBC(this.w_SHORAPROS),'SCHEDJOB','SHORAPROS');
      +",SHORAINI ="+cp_NullLink(cp_ToStrODBC(this.w_SHORAINI),'SCHEDJOB','SHORAINI');
      +",SHDATAPRO ="+cp_NullLink(cp_ToStrODBC(this.w_SHDATAPRO),'SCHEDJOB','SHDATAPRO');
          +i_ccchkf ;
      +" where ";
          +"SHCODICE = "+cp_ToStrODBC(this.w_SHCODICE);
             )
    else
      update (i_cTable) set;
          SHFLAT = "S";
          ,SHORAFIN = this.w_SHORAFIN;
          ,SHORAPROS = this.w_SHORAPROS;
          ,SHORAINI = this.w_SHORAINI;
          ,SHDATAPRO = this.w_SHDATAPRO;
          &i_ccchkf. ;
       where;
          SHCODICE = this.w_SHCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,w_SHCODICE)
    this.w_SHCODICE=w_SHCODICE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SCHEDJOB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SHCODICE"
endproc
