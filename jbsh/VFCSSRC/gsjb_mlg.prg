* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_mlg                                                        *
*              Log elaborazione                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-13                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsjb_mlg"))

* --- Class definition
define class tgsjb_mlg as StdTrsForm
  Top    = 1
  Left   = 9

  * --- Standard Properties
  Width  = 672
  Height = 396+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=214593129
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  LOGELAJB_IDX = 0
  SCHEDJOB_IDX = 0
  cFile = "LOGELAJB"
  cKeySelect = "LSSERIAL"
  cKeyWhere  = "LSSERIAL=this.w_LSSERIAL"
  cKeyDetail  = "LSSERIAL=this.w_LSSERIAL"
  cKeyWhereODBC = '"LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL)';

  cKeyDetailWhereODBC = '"LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"LOGELAJB.LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'LOGELAJB.CPROWNUM '
  cPrg = "gsjb_mlg"
  cComment = "Log elaborazione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LSSERIAL = space(10)
  w_LSCODJOB = 0
  w_LSDATINI = ctod('  /  /  ')
  w_LSDATFIN = ctod('  /  /  ')
  w_LSROWORD = 0
  w_LSROWNUM = 0
  w_LSPROCES = space(20)
  w_LSFLELCR = space(1)
  w_LSNOTELA = space(0)
  w_DESCJOB = space(254)
  w_LOG = space(10)
  w_SHTIPSCH = space(1)

  * --- Autonumbered Variables
  op_LSSERIAL = this.W_LSSERIAL

  * --- Children pointers
  GSJB_MJS = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'LOGELAJB','gsjb_mlg')
    stdPageFrame::Init()
    *set procedure to GSJB_MJS additive
    with this
      .Pages(1).addobject("oPag","tgsjb_mlgPag1","gsjb_mlg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Log")
      .Pages(1).HelpContextID = 214141514
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLSSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSJB_MJS
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='SCHEDJOB'
    this.cWorkTables[2]='LOGELAJB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LOGELAJB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LOGELAJB_IDX,3]
  return

  function CreateChildren()
    this.GSJB_MJS = CREATEOBJECT('stdLazyChild',this,'GSJB_MJS')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSJB_MJS)
      this.GSJB_MJS.DestroyChildrenChain()
      this.GSJB_MJS=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSJB_MJS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSJB_MJS.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSJB_MJS.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSJB_MJS.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_LSSERIAL,"STLSSER";
             ,.w_CPROWNUM,"STLSRN";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_LSSERIAL = NVL(LSSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from LOGELAJB where LSSERIAL=KeySet.LSSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2],this.bLoadRecFilter,this.LOGELAJB_IDX,"gsjb_mlg")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LOGELAJB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LOGELAJB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LOGELAJB '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LSSERIAL',this.w_LSSERIAL  )
      select * from (i_cTable) LOGELAJB where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCJOB = space(254)
        .w_SHTIPSCH = space(1)
        .w_LSSERIAL = NVL(LSSERIAL,space(10))
        .op_LSSERIAL = .w_LSSERIAL
        .w_LSCODJOB = NVL(LSCODJOB,0)
          if link_1_2_joined
            this.w_LSCODJOB = NVL(SHCODICE102,NVL(this.w_LSCODJOB,0))
            this.w_DESCJOB = NVL(SHDESCRI102,space(254))
            this.w_SHTIPSCH = NVL(SHTIPSCH102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_LSDATINI = NVL(cp_ToDate(LSDATINI),ctod("  /  /  "))
        .w_LSDATFIN = NVL(cp_ToDate(LSDATFIN),ctod("  /  /  "))
        .w_LOG = .w_LSSERIAL
          .link_1_14('Load')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'LOGELAJB')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_LSROWORD = NVL(LSROWORD,0)
          .w_LSROWNUM = NVL(LSROWNUM,0)
          .w_LSPROCES = NVL(LSPROCES,space(20))
          .w_LSFLELCR = NVL(LSFLELCR,space(1))
          .w_LSNOTELA = NVL(LSNOTELA,space(0))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_LOG = .w_LSSERIAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_3.enabled = .oPgFrm.Page1.oPag.oBtn_1_3.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_4.enabled = .oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_LSSERIAL=space(10)
      .w_LSCODJOB=0
      .w_LSDATINI=ctod("  /  /  ")
      .w_LSDATFIN=ctod("  /  /  ")
      .w_LSROWORD=0
      .w_LSROWNUM=0
      .w_LSPROCES=space(20)
      .w_LSFLELCR=space(1)
      .w_LSNOTELA=space(0)
      .w_DESCJOB=space(254)
      .w_LOG=space(10)
      .w_SHTIPSCH=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_LSCODJOB))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,10,.f.)
        .w_LOG = .w_LSSERIAL
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_LOG))
         .link_1_14('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'LOGELAJB')
    this.DoRTCalc(12,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLSSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oLSNOTELA_2_5.enabled = i_bVal
      .Page1.oPag.oBtn_1_3.enabled = .Page1.oPag.oBtn_1_3.mCond()
      .Page1.oPag.oBtn_1_4.enabled = .Page1.oPag.oBtn_1_4.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLSSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLSSERIAL_1_1.enabled = .t.
      endif
    endwith
    this.GSJB_MJS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'LOGELAJB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsjb_mlg
    * Abilito il campo note sul dettaglio per scorrere eventuali log lunghi
    Local L_Ctrl
    L_Ctrl=This.GetCtrl('w_LSNOTELA')
    L_Ctrl.Enabled=.t.
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"LOGJB","w_LSSERIAL")
      .op_LSSERIAL = .w_LSSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSJB_MJS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LSSERIAL,"LSSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LSCODJOB,"LSCODJOB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LSDATINI,"LSDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LSDATFIN,"LSDATFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    i_lTable = "LOGELAJB"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.LOGELAJB_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsjb_slg with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_LSROWORD N(5);
      ,t_LSPROCES C(20);
      ,t_LSFLELCR N(3);
      ,t_LSNOTELA M(10);
      ,CPROWNUM N(10);
      ,t_LSROWNUM N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsjb_mlgbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLSROWORD_2_1.controlsource=this.cTrsName+'.t_LSROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLSPROCES_2_3.controlsource=this.cTrsName+'.t_LSPROCES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLSFLELCR_2_4.controlsource=this.cTrsName+'.t_LSFLELCR'
    this.oPgFRm.Page1.oPag.oLSNOTELA_2_5.controlsource=this.cTrsName+'.t_LSNOTELA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(86)
    this.AddVLine(243)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSPROCES_2_3
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"LOGJB","w_LSSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
      *
      * insert into LOGELAJB
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LOGELAJB')
        i_extval=cp_InsertValODBCExtFlds(this,'LOGELAJB')
        i_cFldBody=" "+;
                  "(LSSERIAL,LSCODJOB,LSDATINI,LSDATFIN,LSROWORD"+;
                  ",LSROWNUM,LSPROCES,LSFLELCR,LSNOTELA,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LSSERIAL)+","+cp_ToStrODBCNull(this.w_LSCODJOB)+","+cp_ToStrODBC(this.w_LSDATINI)+","+cp_ToStrODBC(this.w_LSDATFIN)+","+cp_ToStrODBC(this.w_LSROWORD)+;
             ","+cp_ToStrODBC(this.w_LSROWNUM)+","+cp_ToStrODBC(this.w_LSPROCES)+","+cp_ToStrODBC(this.w_LSFLELCR)+","+cp_ToStrODBC(this.w_LSNOTELA)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LOGELAJB')
        i_extval=cp_InsertValVFPExtFlds(this,'LOGELAJB')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'LSSERIAL',this.w_LSSERIAL)
        INSERT INTO (i_cTable) (;
                   LSSERIAL;
                  ,LSCODJOB;
                  ,LSDATINI;
                  ,LSDATFIN;
                  ,LSROWORD;
                  ,LSROWNUM;
                  ,LSPROCES;
                  ,LSFLELCR;
                  ,LSNOTELA;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_LSSERIAL;
                  ,this.w_LSCODJOB;
                  ,this.w_LSDATINI;
                  ,this.w_LSDATFIN;
                  ,this.w_LSROWORD;
                  ,this.w_LSROWNUM;
                  ,this.w_LSPROCES;
                  ,this.w_LSFLELCR;
                  ,this.w_LSNOTELA;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_LSROWNUM))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'LOGELAJB')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " LSCODJOB="+cp_ToStrODBCNull(this.w_LSCODJOB)+;
                 ",LSDATINI="+cp_ToStrODBC(this.w_LSDATINI)+;
                 ",LSDATFIN="+cp_ToStrODBC(this.w_LSDATFIN)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'LOGELAJB')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  LSCODJOB=this.w_LSCODJOB;
                 ,LSDATINI=this.w_LSDATINI;
                 ,LSDATFIN=this.w_LSDATFIN;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_LSROWNUM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSJB_MJS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_LSSERIAL,"STLSSER";
                     ,this.w_CPROWNUM,"STLSRN";
                     )
              this.GSJB_MJS.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update LOGELAJB
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'LOGELAJB')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " LSCODJOB="+cp_ToStrODBCNull(this.w_LSCODJOB)+;
                     ",LSDATINI="+cp_ToStrODBC(this.w_LSDATINI)+;
                     ",LSDATFIN="+cp_ToStrODBC(this.w_LSDATFIN)+;
                     ",LSROWORD="+cp_ToStrODBC(this.w_LSROWORD)+;
                     ",LSROWNUM="+cp_ToStrODBC(this.w_LSROWNUM)+;
                     ",LSPROCES="+cp_ToStrODBC(this.w_LSPROCES)+;
                     ",LSFLELCR="+cp_ToStrODBC(this.w_LSFLELCR)+;
                     ",LSNOTELA="+cp_ToStrODBC(this.w_LSNOTELA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'LOGELAJB')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      LSCODJOB=this.w_LSCODJOB;
                     ,LSDATINI=this.w_LSDATINI;
                     ,LSDATFIN=this.w_LSDATFIN;
                     ,LSROWORD=this.w_LSROWORD;
                     ,LSROWNUM=this.w_LSROWNUM;
                     ,LSPROCES=this.w_LSPROCES;
                     ,LSFLELCR=this.w_LSFLELCR;
                     ,LSNOTELA=this.w_LSNOTELA;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_LSROWNUM)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSJB_MJS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_LSSERIAL,"STLSSER";
               ,this.w_CPROWNUM,"STLSRN";
               )
          this.GSJB_MJS.mReplace()
          this.GSJB_MJS.bSaveContext=.f.
        endif
      endscan
     this.GSJB_MJS.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_LSROWNUM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSJB_MJS : Deleting
        this.GSJB_MJS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_LSSERIAL,"STLSSER";
               ,this.w_CPROWNUM,"STLSRN";
               )
        this.GSJB_MJS.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete LOGELAJB
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_LSROWNUM))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,10,.t.)
          .w_LOG = .w_LSSERIAL
          .link_1_14('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_LSROWNUM with this.w_LSROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_6.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_6.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSJB_MJS.visible")=='L' And this.GSJB_MJS.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_6.visible
      this.GSJB_MJS.HideChildrenChain()
    endif 
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LSCODJOB
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCHEDJOB_IDX,3]
    i_lTable = "SCHEDJOB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2], .t., this.SCHEDJOB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LSCODJOB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LSCODJOB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SHCODICE,SHDESCRI,SHTIPSCH";
                   +" from "+i_cTable+" "+i_lTable+" where SHCODICE="+cp_ToStrODBC(this.w_LSCODJOB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SHCODICE',this.w_LSCODJOB)
            select SHCODICE,SHDESCRI,SHTIPSCH;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LSCODJOB = NVL(_Link_.SHCODICE,0)
      this.w_DESCJOB = NVL(_Link_.SHDESCRI,space(254))
      this.w_SHTIPSCH = NVL(_Link_.SHTIPSCH,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LSCODJOB = 0
      endif
      this.w_DESCJOB = space(254)
      this.w_SHTIPSCH = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])+'\'+cp_ToStr(_Link_.SHCODICE,1)
      cp_ShowWarn(i_cKey,this.SCHEDJOB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LSCODJOB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.SCHEDJOB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.SCHEDJOB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.SHCODICE as SHCODICE102"+ ",link_1_2.SHDESCRI as SHDESCRI102"+ ",link_1_2.SHTIPSCH as SHTIPSCH102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on LOGELAJB.LSCODJOB=link_1_2.SHCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and LOGELAJB.LSCODJOB=link_1_2.SHCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LOG
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOGELAJB_IDX,3]
    i_lTable = "LOGELAJB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2], .t., this.LOGELAJB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where LSSERIAL="+cp_ToStrODBC(this.w_LOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSSERIAL',this.w_LOG)
            select LSSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOG = NVL(_Link_.LSSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_LOG = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOGELAJB_IDX,2])+'\'+cp_ToStr(_Link_.LSSERIAL,1)
      cp_ShowWarn(i_cKey,this.LOGELAJB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oLSSERIAL_1_1.value==this.w_LSSERIAL)
      this.oPgFrm.Page1.oPag.oLSSERIAL_1_1.value=this.w_LSSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oLSCODJOB_1_2.value==this.w_LSCODJOB)
      this.oPgFrm.Page1.oPag.oLSCODJOB_1_2.value=this.w_LSCODJOB
    endif
    if not(this.oPgFrm.Page1.oPag.oLSDATINI_1_6.value==this.w_LSDATINI)
      this.oPgFrm.Page1.oPag.oLSDATINI_1_6.value=this.w_LSDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oLSDATFIN_1_8.value==this.w_LSDATFIN)
      this.oPgFrm.Page1.oPag.oLSDATFIN_1_8.value=this.w_LSDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLSNOTELA_2_5.value==this.w_LSNOTELA)
      this.oPgFrm.Page1.oPag.oLSNOTELA_2_5.value=this.w_LSNOTELA
      replace t_LSNOTELA with this.oPgFrm.Page1.oPag.oLSNOTELA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCJOB_1_13.value==this.w_DESCJOB)
      this.oPgFrm.Page1.oPag.oDESCJOB_1_13.value=this.w_DESCJOB
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSROWORD_2_1.value==this.w_LSROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSROWORD_2_1.value=this.w_LSROWORD
      replace t_LSROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSPROCES_2_3.value==this.w_LSPROCES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSPROCES_2_3.value=this.w_LSPROCES
      replace t_LSPROCES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSPROCES_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSFLELCR_2_4.RadioValue()==this.w_LSFLELCR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSFLELCR_2_4.SetRadio()
      replace t_LSFLELCR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSFLELCR_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'LOGELAJB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_LSROWNUM)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSJB_MJS.CheckForm()
      if not(Empty(.w_LSROWNUM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSJB_MJS : Depends On
    this.GSJB_MJS.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_LSROWNUM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_LSROWORD=0
      .w_LSROWNUM=0
      .w_LSPROCES=space(20)
      .w_LSFLELCR=space(1)
      .w_LSNOTELA=space(0)
    endwith
    this.DoRTCalc(1,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_LSROWORD = t_LSROWORD
    this.w_LSROWNUM = t_LSROWNUM
    this.w_LSPROCES = t_LSPROCES
    this.w_LSFLELCR = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSFLELCR_2_4.RadioValue(.t.)
    this.w_LSNOTELA = t_LSNOTELA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_LSROWORD with this.w_LSROWORD
    replace t_LSROWNUM with this.w_LSROWNUM
    replace t_LSPROCES with this.w_LSPROCES
    replace t_LSFLELCR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLSFLELCR_2_4.ToRadio()
    replace t_LSNOTELA with this.w_LSNOTELA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanEdit()
    local i_res
    i_res=1=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossible variare dati di log"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=1=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossible inserire dati di log"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsjb_mlgPag1 as StdContainer
  Width  = 668
  height = 396
  stdWidth  = 668
  stdheight = 396
  resizeXpos=344
  resizeYpos=248
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLSSERIAL_1_1 as StdField with uid="AZVPZBFHTF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LSSERIAL", cQueryName = "LSSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale log",;
    HelpContextID = 27269634,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=92, Left=58, Top=10, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oLSCODJOB_1_2 as StdField with uid="OPVYTXJLEW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LSCODJOB", cQueryName = "LSCODJOB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice job di riferimento elaborazione",;
    HelpContextID = 29956600,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=256, Top=10, cSayPict='"9999999999"', cGetPict='"9999999999"', cLinkFile="SCHEDJOB", cZoomOnZoom="GSJB_ASH", oKey_1_1="SHCODICE", oKey_1_2="this.w_LSCODJOB"

  func oLSCODJOB_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oBtn_1_3 as StdButton with uid="MWCSXCBXIE",left=23, top=36, width=50,height=45,;
    CpPicture="bmp\schedulatore.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla gestione";
    , HelpContextID = 155265995;
    , Caption='\<Gestione';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        GSJB_BSL(this.Parent.oContained,"S",.w_LSCODJOB)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_4 as StdButton with uid="RLGNBYGPTA",left=100, top=36, width=48,height=45,;
    CpPicture="bmp\stampa.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alle stampe di elaborazione";
    , HelpContextID = 266183499;
    , Caption='\<Stampe';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      do GSJB_KJS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLSDATINI_1_6 as StdField with uid="WIAPLXDWDK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LSDATINI", cQueryName = "LSDATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio elaborazione",;
    HelpContextID = 29043199,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=450, Top=39

  add object oLSDATFIN_1_8 as StdField with uid="ICFJXIDDSC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LSDATFIN", cQueryName = "LSDATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine elaborazione",;
    HelpContextID = 21288444,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=581, Top=39

  add object oDESCJOB_1_13 as StdField with uid="NYJHNYRNQY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCJOB", cQueryName = "DESCJOB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del job",;
    HelpContextID = 119409462,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=334, Top=10, InputMask=replicate('X',254)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=87, width=306,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="LSROWORD",Label1="Posizione",Field2="LSPROCES",Label2="Processo",Field3="LSFLELCR",Label3="Con errore",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101497722

  add object oStr_1_5 as StdString with uid="MSMLGBSZTT",Visible=.t., Left=161, Top=10,;
    Alignment=1, Width=92, Height=18,;
    Caption="Job:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (.w_SHTIPSCH <> "N")
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="JZPNBWKLPU",Visible=.t., Left=238, Top=39,;
    Alignment=1, Width=155, Height=18,;
    Caption="Date elaborazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ABNCJTDBVU",Visible=.t., Left=397, Top=39,;
    Alignment=1, Width=48, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="EXAYCKZFVE",Visible=.t., Left=313, Top=87,;
    Alignment=2, Width=350, Height=18,;
    Caption="Informazioni elaborazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="WUBMDYAMPA",Visible=.t., Left=3, Top=10,;
    Alignment=1, Width=52, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="XATHUIFCXP",Visible=.t., Left=538, Top=39,;
    Alignment=1, Width=42, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="VDAMJWXQQB",Visible=.t., Left=161, Top=10,;
    Alignment=1, Width=92, Height=18,;
    Caption="Proattivitą:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_SHTIPSCH = "N")
    endwith
  endfunc

  add object oBox_1_12 as StdBox with uid="NUWNXPLCQR",left=151, top=6, width=517,height=64

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=106,;
    width=302+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=107,width=301+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oLSNOTELA_2_5.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLSNOTELA_2_5 as StdTrsMemo with uid="WJQYMQTNWW",rtseq=9,rtrep=.t.,;
    cFormVar="w_LSNOTELA",value=space(0),;
    ToolTipText = "Log informazioni elaborazione",;
    HelpContextID = 37107209,;
    cTotal="", bFixedPos=.t., cQueryName = "LSNOTELA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=287, Width=347, Left=319, Top=107, readonly=.t.

  add object oLinkPC_2_6 as StdButton with uid="JAMAIHQCXM",width=76,height=22,visible=.f.,;
   left=678, top=0,;
    caption="Stampe job", nPag=2;
    , ToolTipText = "Inserito bottone per eliminare le stampe del log alla sua cancellazione, reso nascosto per impedire modifiche";
    , HelpContextID = 262767744;
  , bGlobalFont=.t.

    proc oLinkPC_2_6.Click()
      this.Parent.oContained.GSJB_MJS.LinkPCClick()
    endproc

  func oLinkPC_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.T.)
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsjb_mlgBodyRow as CPBodyRowCnt
  Width=292
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oLSROWORD_2_1 as StdTrsField with uid="WQWBWRYMVW",rtseq=5,rtrep=.t.,;
    cFormVar="w_LSROWORD",value=0,enabled=.f.,;
    ToolTipText = "Cproword del processo",;
    HelpContextID = 133827066,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=71, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oLSPROCES_2_3 as StdTrsField with uid="XTVIRZCYAO",rtseq=7,rtrep=.t.,;
    cFormVar="w_LSPROCES",value=space(20),;
    ToolTipText = "Codice processo",;
    HelpContextID = 192735753,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=82, Top=0, InputMask=replicate('X',20), BackStyle=IIF(type('i_udisabledbackcolor')$'UL' And type('i_udisabledforecolor')$'UL',0,1)

  add object oLSFLELCR_2_4 as StdTrsCheck with uid="IQPIXLVZPY",rtrep=.t.,;
    cFormVar="w_LSFLELCR", enabled=.f., caption="",;
    ToolTipText = "Elaborazione terminata non correttamente/correttamente",;
    HelpContextID = 64375304,;
    Left=236, Top=0, Width=51,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oLSFLELCR_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LSFLELCR,&i_cF..t_LSFLELCR),this.value)
    return(iif(xVal =1,'N',;
    space(1)))
  endfunc
  func oLSFLELCR_2_4.GetRadio()
    this.Parent.oContained.w_LSFLELCR = this.RadioValue()
    return .t.
  endfunc

  func oLSFLELCR_2_4.ToRadio()
    this.Parent.oContained.w_LSFLELCR=trim(this.Parent.oContained.w_LSFLELCR)
    return(;
      iif(this.Parent.oContained.w_LSFLELCR=='N',1,;
      0))
  endfunc

  func oLSFLELCR_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oLSPROCES_2_3.When()
    return(.t.)
  proc oLSPROCES_2_3.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oLSPROCES_2_3.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsjb_mlg','LOGELAJB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LSSERIAL=LOGELAJB.LSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
