* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsjb_ba2                                                        *
*              Attiva le proattivit� selezionate                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-14                                                      *
* Last revis.: 2013-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsjb_ba2",oParentObject,m.pPARAM)
return(i_retval)

define class tgsjb_ba2 as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_SHCODICE = 0
  * --- WorkFile variables
  SCHEDJOB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attiva i job selezionati
    * --- pPARAM ="A" - Attiva
    *     pPARAM ="D" - Disattiva
    SELECT( this.oParentObject.w_ELENCO.cCURSOR )
    SUM XCHK TO L_NUMPROATT
    if L_NUMPROATT = 0
      AH_ERRORMSG( "Non � stata selezionata alcuna proattivit�" , "!" )
      i_retcode = 'stop'
      return
    endif
    if this.pPARAM ="U" and this.oParentObject.w_SHUTEPRE=0
      if not ah_Yesno("Attenzione, verr� cancellato l'utente preferenziale. Si desidera proseguire?")
        i_retcode = 'stop'
        return
      endif
    endif
    GO TOP
    do while NOT EOF()
      * --- Esegue l'attivazione
      this.w_SHCODICE = SHCODICE
      if XCHK = 1
        do case
          case this.pPARAM ="A"
            * --- pPARAM ="A" - Attiva
            GSJB_BAP(this, this.w_SHCODICE )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.pPARAM ="D"
            * --- pPARAM ="D" - Disattiva
            * --- Write into SCHEDJOB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SCHEDJOB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SHFLAT ="+cp_NullLink(cp_ToStrODBC("N"),'SCHEDJOB','SHFLAT');
                  +i_ccchkf ;
              +" where ";
                  +"SHCODICE = "+cp_ToStrODBC(this.w_SHCODICE);
                     )
            else
              update (i_cTable) set;
                  SHFLAT = "N";
                  &i_ccchkf. ;
               where;
                  SHCODICE = this.w_SHCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.pPARAM ="U"
            * --- pPARAM ="U" - Utente preferenziale
            * --- Write into SCHEDJOB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SCHEDJOB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SCHEDJOB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SCHEDJOB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SHUTEPRE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SHUTEPRE),'SCHEDJOB','SHUTEPRE');
                  +i_ccchkf ;
              +" where ";
                  +"SHCODICE = "+cp_ToStrODBC(this.w_SHCODICE);
                     )
            else
              update (i_cTable) set;
                  SHUTEPRE = this.oParentObject.w_SHUTEPRE;
                  &i_ccchkf. ;
               where;
                  SHCODICE = this.w_SHCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
      endif
      SELECT( this.oParentObject.w_ELENCO.cCURSOR )
      SKIP
    enddo
    THIS.oParentobject.NOTIFYEVENT( "Interroga" )
    AH_ERRORMSG( "L'aggiornamento delle proattivit� selezionate � stato eseguito correttamente" , "i" )
    * --- Setta per non eseguire ricalcoli
    this.bUpdateParentObject=.f.
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SCHEDJOB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
