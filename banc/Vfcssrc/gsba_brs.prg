* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_brs                                                        *
*              Ricostruzione saldi conti correnti                              *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_30]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-01-30                                                      *
* Last revis.: 2001-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsba_brs",oParentObject)
return(i_retval)

define class tgsba_brs as StdBatch
  * --- Local variables
  w_MESS = space(10)
  w_CODBAN = space(15)
  w_IMPDEB = 0
  w_IMPCRE = 0
  w_IMPCOM = 0
  * --- WorkFile variables
  COC_MAST_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce i Saldi Conti Corrente
    * --- Lanciato da Gsba_Krs
    * --- Try
    local bErr_03608390
    bErr_03608390=bTrsErr
    this.Try_03608390()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Impossibile aggiornare i saldi conti correnti",,"")
    endif
    bTrsErr=bTrsErr or bErr_03608390
    * --- End
  endproc
  proc Try_03608390()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Azzerro i Saldi
    * --- Write into COC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"BASALDEB ="+cp_NullLink(cp_ToStrODBC(0),'COC_MAST','BASALDEB');
      +",BASALCRE ="+cp_NullLink(cp_ToStrODBC(0),'COC_MAST','BASALCRE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          BASALDEB = 0;
          ,BASALCRE = 0;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from GSBA_QSA
    do vq_exec with 'GSBA_QSA',this,'_Curs_GSBA_QSA','',.f.,.t.
    if used('_Curs_GSBA_QSA')
      select _Curs_GSBA_QSA
      locate for 1=1
      do while not(eof())
      this.w_CODBAN = NVL(_Curs_GSBA_QSA.CCNUMCOR," ")
      if NOT EMPTY(this.w_CODBAN)
        this.w_IMPCRE = _Curs_GSBA_QSA.CCIMPCRE
        this.w_IMPCOM = _Curs_GSBA_QSA.CCCOSCOM
        this.w_IMPDEB = _Curs_GSBA_QSA.CCIMPDEB + this.w_IMPCOM
        ah_Msg("Aggiorna: %1",.T.,.F.,.F., this.w_CODBAN)
        * --- Try
        local bErr_035C8D68
        bErr_035C8D68=bTrsErr
        this.Try_035C8D68()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_035C8D68
        * --- End
        * --- Try
        local bErr_035C3240
        bErr_035C3240=bTrsErr
        this.Try_035C3240()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error="ABBANDONA"
          return
        endif
        bTrsErr=bTrsErr or bErr_035C3240
        * --- End
      endif
        select _Curs_GSBA_QSA
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Aggiornamento saldi conti correnti completato",,"")
    return
  proc Try_035C8D68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- scrivo i saldi -se non c'� lo creo (l'utente cancella il saldo !)
    * --- Insert into COC_MAST
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"BACODBAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODBAN),'COC_MAST','BACODBAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'BACODBAN',this.w_CODBAN)
      insert into (i_cTable) (BACODBAN &i_ccchkf. );
         values (;
           this.w_CODBAN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_035C3240()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into COC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"BASALCRE ="+cp_NullLink(cp_ToStrODBC(this.w_IMPCRE),'COC_MAST','BASALCRE');
      +",BASALDEB ="+cp_NullLink(cp_ToStrODBC(this.w_IMPDEB),'COC_MAST','BASALDEB');
          +i_ccchkf ;
      +" where ";
          +"BACODBAN = "+cp_ToStrODBC(this.w_CODBAN);
             )
    else
      update (i_cTable) set;
          BASALCRE = this.w_IMPCRE;
          ,BASALDEB = this.w_IMPDEB;
          &i_ccchkf. ;
       where;
          BACODBAN = this.w_CODBAN;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSBA_QSA')
      use in _Curs_GSBA_QSA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
