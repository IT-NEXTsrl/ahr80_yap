* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_scc                                                        *
*              Conteggio competenze                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_68]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-12-19                                                      *
* Last revis.: 2008-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsba_scc",oParentObject))

* --- Class definition
define class tgsba_scc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 522
  Height = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-05"
  HelpContextID=90867305
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  VALUTE_IDX = 0
  CCC_MAST_IDX = 0
  cPrg = "gsba_scc"
  cComment = "Conteggio competenze"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_OBTEST = space(10)
  w_CODICE = space(15)
  o_CODICE = space(15)
  w_CCOBSO = ctod('  /  /  ')
  w_CODABI = space(5)
  o_CODABI = space(5)
  w_DESABI = space(80)
  w_CODCAB = space(5)
  w_DESCAB = space(40)
  w_CODVAL = space(3)
  w_TIPCON = space(1)
  w_DESVAL = space(35)
  w_DESCRI = space(35)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_TIPSTA = space(1)
  o_TIPSTA = space(1)
  w_CAUSPE = space(5)
  w_CAUINP = space(5)
  w_CAUINA = space(5)
  w_DESSUP = space(50)
  w_FLCOMP = space(1)
  w_FLCRDP = space(1)
  w_FLCOMA = space(1)
  w_FLCRDA = space(1)
  w_FLCOMS = space(1)
  w_DESCAS = space(35)
  w_DESCAP = space(35)
  w_DESCAA = space(35)
  w_SIMVAL = space(5)
  w_DATREG = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsba_sccPag1","gsba_scc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='COD_ABI'
    this.cWorkTables[3]='COD_CAB'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CCC_MAST'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=space(10)
      .w_CODICE=space(15)
      .w_CCOBSO=ctod("  /  /  ")
      .w_CODABI=space(5)
      .w_DESABI=space(80)
      .w_CODCAB=space(5)
      .w_DESCAB=space(40)
      .w_CODVAL=space(3)
      .w_TIPCON=space(1)
      .w_DESVAL=space(35)
      .w_DESCRI=space(35)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPSTA=space(1)
      .w_CAUSPE=space(5)
      .w_CAUINP=space(5)
      .w_CAUINA=space(5)
      .w_DESSUP=space(50)
      .w_FLCOMP=space(1)
      .w_FLCRDP=space(1)
      .w_FLCOMA=space(1)
      .w_FLCRDA=space(1)
      .w_FLCOMS=space(1)
      .w_DESCAS=space(35)
      .w_DESCAP=space(35)
      .w_DESCAA=space(35)
      .w_SIMVAL=space(5)
      .w_DATREG=ctod("  /  /  ")
        .w_OBTEST = i_DATSYS
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODICE))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_CODABI))
          .link_1_5('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_CODCAB))
          .link_1_7('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_CODVAL))
          .link_1_9('Full')
        endif
          .DoRTCalc(9,11,.f.)
        .w_DATINI = cp_CharToDate('01-01-'+ALLTRIM(STR(YEAR(iif(Not Empty(.w_DATFIN),.w_DATFIN,i_DATSYS)))))
        .w_DATFIN = i_DATSYS
        .w_TIPSTA = 'P'
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CAUSPE))
          .link_1_21('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CAUINP))
          .link_1_22('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CAUINA))
          .link_1_23('Full')
        endif
    endwith
    this.DoRTCalc(18,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_CODICE<>.w_CODICE
          .link_1_5('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_CODICE<>.w_CODICE.or. .o_CODABI<>.w_CODABI
          .link_1_7('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_CODICE<>.w_CODICE
          .link_1_9('Full')
        endif
        .DoRTCalc(9,11,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_DATINI = cp_CharToDate('01-01-'+ALLTRIM(STR(YEAR(iif(Not Empty(.w_DATFIN),.w_DATFIN,i_DATSYS)))))
        endif
        .DoRTCalc(13,14,.t.)
        if .o_TIPSTA<>.w_TIPSTA
          .link_1_21('Full')
        endif
        if .o_TIPSTA<>.w_TIPSTA
          .link_1_22('Full')
        endif
        if .o_TIPSTA<>.w_TIPSTA
          .link_1_23('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODABI_1_5.enabled = this.oPgFrm.Page1.oPag.oCODABI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODCAB_1_7.enabled = this.oPgFrm.Page1.oPag.oCODCAB_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCODVAL_1_9.enabled = this.oPgFrm.Page1.oPag.oCODVAL_1_9.mCond()
    this.oPgFrm.Page1.oPag.oTIPCON_1_10.enabled = this.oPgFrm.Page1.oPag.oTIPCON_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCAUSPE_1_21.enabled = this.oPgFrm.Page1.oPag.oCAUSPE_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCAUINP_1_22.enabled = this.oPgFrm.Page1.oPag.oCAUINP_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCAUINA_1_23.enabled = this.oPgFrm.Page1.oPag.oCAUINA_1_23.mCond()
    this.oPgFrm.Page1.oPag.oDESSUP_1_24.enabled = this.oPgFrm.Page1.oPag.oDESSUP_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BADTOBSO,BATIPCON,BACODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODICE))
          select BACODBAN,BADESCRI,BACODABI,BACODCAB,BADTOBSO,BATIPCON,BACODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BADTOBSO,BATIPCON,BACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODICE)+"%");

            select BACODBAN,BADESCRI,BACODABI,BACODCAB,BADTOBSO,BATIPCON,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODICE_1_2'),i_cWhere,'GSTE_ACB',"Conti correnti",'gste5mcc.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BADTOBSO,BATIPCON,BACODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACODABI,BACODCAB,BADTOBSO,BATIPCON,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BADTOBSO,BATIPCON,BACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODICE)
            select BACODBAN,BADESCRI,BACODABI,BACODCAB,BADTOBSO,BATIPCON,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_CODABI = NVL(_Link_.BACODABI,space(5))
      this.w_CODCAB = NVL(_Link_.BACODCAB,space(5))
      this.w_CCOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_TIPCON = NVL(_Link_.BATIPCON,space(1))
      this.w_CODVAL = NVL(_Link_.BACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_CODABI = space(5)
      this.w_CODCAB = space(5)
      this.w_CCOBSO = ctod("  /  /  ")
      this.w_TIPCON = space(1)
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CCOBSO>.w_OBTEST OR EMPTY(.w_CCOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto corrente inesistente o obsoleto")
        endif
        this.w_CODICE = space(15)
        this.w_DESCRI = space(35)
        this.w_CODABI = space(5)
        this.w_CODCAB = space(5)
        this.w_CCOBSO = ctod("  /  /  ")
        this.w_TIPCON = space(1)
        this.w_CODVAL = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODABI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODABI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABI',True,'COD_ABI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ABCODABI like "+cp_ToStrODBC(trim(this.w_CODABI)+"%");

          i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ABCODABI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ABCODABI',trim(this.w_CODABI))
          select ABCODABI,ABDESABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ABCODABI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODABI)==trim(_Link_.ABCODABI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODABI) and !this.bDontReportError
            deferred_cp_zoom('COD_ABI','*','ABCODABI',cp_AbsName(oSource.parent,'oCODABI_1_5'),i_cWhere,'GSAR_ABI',"Codici ABI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                     +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',oSource.xKey(1))
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODABI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_CODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_CODABI)
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODABI = NVL(_Link_.ABCODABI,space(5))
      this.w_DESABI = NVL(_Link_.ABDESABI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_CODABI = space(5)
      endif
      this.w_DESABI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODABI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAB
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFI',True,'COD_CAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FICODCAB like "+cp_ToStrODBC(trim(this.w_CODCAB)+"%");
                   +" and FICODABI="+cp_ToStrODBC(this.w_CODABI);

          i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FIDESFIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FICODABI,FICODCAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FICODABI',this.w_CODABI;
                     ,'FICODCAB',trim(this.w_CODCAB))
          select FICODABI,FICODCAB,FIDESFIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FICODABI,FICODCAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAB)==trim(_Link_.FICODCAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAB) and !this.bDontReportError
            deferred_cp_zoom('COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(oSource.parent,'oCODCAB_1_7'),i_cWhere,'GSAR_AFI',"Codici CAB",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODABI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FIDESFIL";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FICODABI,FICODCAB,FIDESFIL;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FIDESFIL";
                     +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FICODABI="+cp_ToStrODBC(this.w_CODABI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',oSource.xKey(1);
                       ,'FICODCAB',oSource.xKey(2))
            select FICODABI,FICODCAB,FIDESFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FIDESFIL";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_CODCAB);
                   +" and FICODABI="+cp_ToStrODBC(this.w_CODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',this.w_CODABI;
                       ,'FICODCAB',this.w_CODCAB)
            select FICODABI,FICODCAB,FIDESFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAB = NVL(_Link_.FICODCAB,space(5))
      this.w_DESCAB = NVL(_Link_.FIDESFIL,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAB = space(5)
      endif
      this.w_DESCAB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODABI,1)+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_9'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSPE
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MCT',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CAUSPE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CAUSPE))
          select CACODICE,CADESCRI,CAFLCOMP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSPE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUSPE) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCAUSPE_1_21'),i_cWhere,'GSBA_MCT',"Causali conti correnti",'GSBA_SCC.CCC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CAFLCOMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CAUSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CAUSPE)
            select CACODICE,CADESCRI,CAFLCOMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSPE = NVL(_Link_.CACODICE,space(5))
      this.w_DESCAS = NVL(_Link_.CADESCRI,space(35))
      this.w_FLCOMS = NVL(_Link_.CAFLCOMP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSPE = space(5)
      endif
      this.w_DESCAS = space(35)
      this.w_FLCOMS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLCOMS='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale addebito spese incongruente o non definita")
        endif
        this.w_CAUSPE = space(5)
        this.w_DESCAS = space(35)
        this.w_FLCOMS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUINP
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUINP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MCT',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CAUINP)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CAUINP))
          select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUINP)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUINP) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCAUINP_1_22'),i_cWhere,'GSBA_MCT',"Causali conti correnti",'GSBA_SCC.CCC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUINP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CAUINP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CAUINP)
            select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUINP = NVL(_Link_.CACODICE,space(5))
      this.w_DESCAP = NVL(_Link_.CADESCRI,space(35))
      this.w_FLCOMP = NVL(_Link_.CAFLCOMP,space(1))
      this.w_FLCRDP = NVL(_Link_.CAFLCRDE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUINP = space(5)
      endif
      this.w_DESCAP = space(35)
      this.w_FLCOMP = space(1)
      this.w_FLCRDP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLCOMP='I' AND .w_FLCRDP='D'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale addebito interessi passivi incongruente o non definita")
        endif
        this.w_CAUINP = space(5)
        this.w_DESCAP = space(35)
        this.w_FLCOMP = space(1)
        this.w_FLCRDP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUINP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUINA
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MCT',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CAUINA)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CAUINA))
          select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUINA)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUINA) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCAUINA_1_23'),i_cWhere,'GSBA_MCT',"Causali conti correnti",'GSBA_SCC.CCC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CAUINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CAUINA)
            select CACODICE,CADESCRI,CAFLCOMP,CAFLCRDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUINA = NVL(_Link_.CACODICE,space(5))
      this.w_DESCAA = NVL(_Link_.CADESCRI,space(35))
      this.w_FLCOMA = NVL(_Link_.CAFLCOMP,space(1))
      this.w_FLCRDA = NVL(_Link_.CAFLCRDE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUINA = space(5)
      endif
      this.w_DESCAA = space(35)
      this.w_FLCOMA = space(1)
      this.w_FLCRDA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLCOMA='I' AND .w_FLCRDA='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale accredito interessi attivi incongruente o non definita")
        endif
        this.w_CAUINA = space(5)
        this.w_DESCAA = space(35)
        this.w_FLCOMA = space(1)
        this.w_FLCRDA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_2.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_2.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODABI_1_5.value==this.w_CODABI)
      this.oPgFrm.Page1.oPag.oCODABI_1_5.value=this.w_CODABI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESABI_1_6.value==this.w_DESABI)
      this.oPgFrm.Page1.oPag.oDESABI_1_6.value=this.w_DESABI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAB_1_7.value==this.w_CODCAB)
      this.oPgFrm.Page1.oPag.oCODCAB_1_7.value=this.w_CODCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAB_1_8.value==this.w_DESCAB)
      this.oPgFrm.Page1.oPag.oDESCAB_1_8.value=this.w_DESCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_9.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_9.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_10.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_11.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_11.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_12.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_12.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_16.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_16.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_17.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_17.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSTA_1_20.RadioValue()==this.w_TIPSTA)
      this.oPgFrm.Page1.oPag.oTIPSTA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSPE_1_21.value==this.w_CAUSPE)
      this.oPgFrm.Page1.oPag.oCAUSPE_1_21.value=this.w_CAUSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUINP_1_22.value==this.w_CAUINP)
      this.oPgFrm.Page1.oPag.oCAUINP_1_22.value=this.w_CAUINP
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUINA_1_23.value==this.w_CAUINA)
      this.oPgFrm.Page1.oPag.oCAUINA_1_23.value=this.w_CAUINA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSUP_1_24.value==this.w_DESSUP)
      this.oPgFrm.Page1.oPag.oDESSUP_1_24.value=this.w_DESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAS_1_35.value==this.w_DESCAS)
      this.oPgFrm.Page1.oPag.oDESCAS_1_35.value=this.w_DESCAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAP_1_36.value==this.w_DESCAP)
      this.oPgFrm.Page1.oPag.oDESCAP_1_36.value=this.w_DESCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAA_1_37.value==this.w_DESCAA)
      this.oPgFrm.Page1.oPag.oDESCAA_1_37.value=this.w_DESCAA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CCOBSO>.w_OBTEST OR EMPTY(.w_CCOBSO)))  and not(empty(.w_CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto corrente inesistente o obsoleto")
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_16.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_17.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CAUSPE)) or not(.w_FLCOMS='S'))  and (.w_TIPSTA='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUSPE_1_21.SetFocus()
            i_bnoObbl = !empty(.w_CAUSPE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale addebito spese incongruente o non definita")
          case   ((empty(.w_CAUINP)) or not(.w_FLCOMP='I' AND .w_FLCRDP='D'))  and (.w_TIPSTA='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUINP_1_22.SetFocus()
            i_bnoObbl = !empty(.w_CAUINP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale addebito interessi passivi incongruente o non definita")
          case   ((empty(.w_CAUINA)) or not(.w_FLCOMA='I' AND .w_FLCRDA='C'))  and (.w_TIPSTA='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUINA_1_23.SetFocus()
            i_bnoObbl = !empty(.w_CAUINA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale accredito interessi attivi incongruente o non definita")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODICE = this.w_CODICE
    this.o_CODABI = this.w_CODABI
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_TIPSTA = this.w_TIPSTA
    return

enddefine

* --- Define pages as container
define class tgsba_sccPag1 as StdContainer
  Width  = 518
  height = 390
  stdWidth  = 518
  stdheight = 390
  resizeXpos=503
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_2 as StdField with uid="SEDCEQQJXL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto corrente inesistente o obsoleto",;
    ToolTipText = "Codice conto corrente da stampare (spazio=no selezione)",;
    HelpContextID = 200078042,;
   bGlobalFont=.t.,;
    Height=21, Width=130, Left=119, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODICE"

  func oCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti correnti",'gste5mcc.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oCODICE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oCODABI_1_5 as StdField with uid="VQNYVVATYR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODABI", cQueryName = "CODABI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI",;
    HelpContextID = 134542042,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=119, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_CODABI"

  func oCODABI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODICE))
    endwith
   endif
  endfunc

  func oCODABI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
      if .not. empty(.w_CODCAB)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODABI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODABI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_ABI','*','ABCODABI',cp_AbsName(this.parent,'oCODABI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABI',"Codici ABI",'',this.parent.oContained
  endproc
  proc oCODABI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ABCODABI=this.parent.oContained.w_CODABI
     i_obj.ecpSave()
  endproc

  add object oDESABI_1_6 as StdField with uid="OPKVDUXLDY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESABI", cQueryName = "DESABI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 134483146,;
   bGlobalFont=.t.,;
    Height=21, Width=342, Left=170, Top=36, InputMask=replicate('X',80)

  add object oCODCAB_1_7 as StdField with uid="FGTZIXSXWA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODCAB", cQueryName = "CODCAB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB",;
    HelpContextID = 252900058,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=119, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_CAB", cZoomOnZoom="GSAR_AFI", oKey_1_1="FICODABI", oKey_1_2="this.w_CODABI", oKey_2_1="FICODCAB", oKey_2_2="this.w_CODCAB"

  func oCODCAB_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODICE) AND NOT EMPTY(.w_CODABI))
    endwith
   endif
  endfunc

  func oCODCAB_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAB_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAB_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.COD_CAB_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FICODABI="+cp_ToStrODBC(this.Parent.oContained.w_CODABI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FICODABI="+cp_ToStr(this.Parent.oContained.w_CODABI)
    endif
    do cp_zoom with 'COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(this.parent,'oCODCAB_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFI',"Codici CAB",'',this.parent.oContained
  endproc
  proc oCODCAB_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FICODABI=w_CODABI
     i_obj.w_FICODCAB=this.parent.oContained.w_CODCAB
     i_obj.ecpSave()
  endproc

  add object oDESCAB_1_8 as StdField with uid="OOMBLDAUNZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCAB", cQueryName = "DESCAB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 252841162,;
   bGlobalFont=.t.,;
    Height=21, Width=342, Left=170, Top=63, InputMask=replicate('X',40)

  add object oCODVAL_1_9 as StdField with uid="NKVUWVHTTZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 83882714,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=119, Top=89, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODICE))
    endwith
   endif
  endfunc

  func oCODVAL_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCODVAL_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
     i_obj.ecpSave()
  endproc

  add object oTIPCON_1_10 as StdCheck with uid="CHOBFJWUZT",rtseq=9,rtrep=.f.,left=119, top=115, caption="Conti compensazione",;
    ToolTipText = "Se attivo: seleziona la stampa sui soli conti di tipo compensazione, altrimenti solo sui conti normali",;
    HelpContextID = 36845514,;
    cFormVar="w_TIPCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPCON_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPCON_1_10.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_10.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='S',1,;
      0)
  endfunc

  func oTIPCON_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODICE))
    endwith
   endif
  endfunc

  add object oDESVAL_1_11 as StdField with uid="UCCKZLEWBC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 83823818,;
   bGlobalFont=.t.,;
    Height=21, Width=342, Left=170, Top=89, InputMask=replicate('X',35)

  add object oDESCRI_1_12 as StdField with uid="RHYGQCEIUF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117574858,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=251, Top=9, InputMask=replicate('X',35)

  add object oDATINI_1_16 as StdField with uid="MNZUSCRMWP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 121372874,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=119, Top=142

  add object oDATFIN_1_17 as StdField with uid="UPOMIHCACE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 42926282,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=290, Top=142


  add object oTIPSTA_1_20 as StdCombo with uid="YOXCPLCYLP",rtseq=14,rtrep=.f.,left=119,top=172,width=145,height=21;
    , ToolTipText = "Se stampa in definitiva: al termine saranno registrati i movimenti di conto corrente di conteggio competenze";
    , HelpContextID = 248657866;
    , cFormVar="w_TIPSTA",RowSource=""+"Prova,"+"Definitiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSTA_1_20.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oTIPSTA_1_20.GetRadio()
    this.Parent.oContained.w_TIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oTIPSTA_1_20.SetRadio()
    this.Parent.oContained.w_TIPSTA=trim(this.Parent.oContained.w_TIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSTA=='P',1,;
      iif(this.Parent.oContained.w_TIPSTA=='D',2,;
      0))
  endfunc

  add object oCAUSPE_1_21 as StdField with uid="FBSUYGVBOW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CAUSPE", cQueryName = "CAUSPE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale addebito spese incongruente o non definita",;
    ToolTipText = "Causale di addebito spese",;
    HelpContextID = 185725146,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=119, Top=231, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="GSBA_MCT", oKey_1_1="CACODICE", oKey_1_2="this.w_CAUSPE"

  func oCAUSPE_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA='D')
    endwith
   endif
  endfunc

  func oCAUSPE_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSPE_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSPE_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCAUSPE_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MCT',"Causali conti correnti",'GSBA_SCC.CCC_MAST_VZM',this.parent.oContained
  endproc
  proc oCAUSPE_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CAUSPE
     i_obj.ecpSave()
  endproc

  add object oCAUINP_1_22 as StdField with uid="KMVKANEFNX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CAUINP", cQueryName = "CAUINP",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale addebito interessi passivi incongruente o non definita",;
    ToolTipText = "Causale di addebito interessi passivi",;
    HelpContextID = 3928282,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=119, Top=258, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="GSBA_MCT", oKey_1_1="CACODICE", oKey_1_2="this.w_CAUINP"

  func oCAUINP_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA='D')
    endwith
   endif
  endfunc

  func oCAUINP_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUINP_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUINP_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCAUINP_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MCT',"Causali conti correnti",'GSBA_SCC.CCC_MAST_VZM',this.parent.oContained
  endproc
  proc oCAUINP_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CAUINP
     i_obj.ecpSave()
  endproc

  add object oCAUINA_1_23 as StdField with uid="JDEFEWCFGQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CAUINA", cQueryName = "CAUINA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale accredito interessi attivi incongruente o non definita",;
    ToolTipText = "Causale di accredito interessi attivi",;
    HelpContextID = 255586522,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=119, Top=285, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="GSBA_MCT", oKey_1_1="CACODICE", oKey_1_2="this.w_CAUINA"

  func oCAUINA_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA='D')
    endwith
   endif
  endfunc

  func oCAUINA_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUINA_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUINA_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCAUINA_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MCT',"Causali conti correnti",'GSBA_SCC.CCC_MAST_VZM',this.parent.oContained
  endproc
  proc oCAUINA_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CAUINA
     i_obj.ecpSave()
  endproc

  add object oDESSUP_1_24 as StdField with uid="MCCEEKEOLZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESSUP", cQueryName = "DESSUP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione dei movimenti da generare",;
    HelpContextID = 264375498,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=119, Top=312, InputMask=replicate('X',50)

  func oDESSUP_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA='D')
    endwith
   endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="KSIIZZSMDN",left=408, top=340, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inizio elaborazione stampa";
    , HelpContextID = 90838554;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSBA_BSS(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="DTPOWDEWGK",left=463, top=340, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83549882;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCAS_1_35 as StdField with uid="REZJILGMNK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCAS", cQueryName = "DESCAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 236063946,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=170, Top=231, InputMask=replicate('X',35)

  add object oDESCAP_1_36 as StdField with uid="OQRWVXHWPN",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCAP", cQueryName = "DESCAP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 17960138,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=170, Top=258, InputMask=replicate('X',35)

  add object oDESCAA_1_37 as StdField with uid="RMPFTCWTNH",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCAA", cQueryName = "DESCAA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1182922,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=170, Top=285, InputMask=replicate('X',35)

  add object oStr_1_4 as StdString with uid="ONKACWOFIP",Visible=.t., Left=21, Top=12,;
    Alignment=1, Width=94, Height=18,;
    Caption="Codice C/C:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TZHRPJTVAD",Visible=.t., Left=21, Top=39,;
    Alignment=1, Width=94, Height=18,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="MPRSDKGYEW",Visible=.t., Left=21, Top=66,;
    Alignment=1, Width=94, Height=18,;
    Caption="Codice CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="FBWTCYDEXX",Visible=.t., Left=21, Top=92,;
    Alignment=1, Width=94, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IIFVPVRZRJ",Visible=.t., Left=27, Top=142,;
    Alignment=1, Width=88, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="KYYLUVQBTV",Visible=.t., Left=200, Top=142,;
    Alignment=1, Width=88, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WRFLADPMOC",Visible=.t., Left=12, Top=172,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="RUFIEPPBKF",Visible=.t., Left=7, Top=200,;
    Alignment=0, Width=316, Height=18,;
    Caption="Causali per conteggio competenze"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="TNHTRJPGEH",Visible=.t., Left=5, Top=231,;
    Alignment=1, Width=110, Height=18,;
    Caption="Addebito spese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="HUHLMHNHAS",Visible=.t., Left=5, Top=258,;
    Alignment=1, Width=110, Height=18,;
    Caption="Interessi passivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="MLYBQNNDIO",Visible=.t., Left=5, Top=285,;
    Alignment=1, Width=110, Height=18,;
    Caption="Interessi attivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="ONBEGQJQUJ",Visible=.t., Left=5, Top=312,;
    Alignment=1, Width=110, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_28 as StdBox with uid="BHHFYYMJWV",left=4, top=221, width=505,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsba_scc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
