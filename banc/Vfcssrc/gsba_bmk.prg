* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_bmk                                                        *
*              Controlli finali sui movimenti di C\C                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_19]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-14                                                      *
* Last revis.: 2002-01-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsba_bmk",oParentObject)
return(i_retval)

define class tgsba_bmk as StdBatch
  * --- Local variables
  w_Test = .f.
  w_MESS = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lanciato da GSBA_MMC esegue controlli sui Movimenti di C\C
    this.w_Test = .F.
    SELECT (this.oParentObject.cTrsName)
    scan for NOT EMPTY(nvl(t_CCCODCAU," ")) AND NOT EMPTY(NVL(t_CCNUMCOR," ")) AND (NVL(t_CCIMPCRE,0)<>0 OR NVL(t_CCIMPDEB,0)<>0)
    this.w_Test = .T.
    endscan
    if this.w_Test=.F.
      this.w_MESS = ah_Msgformat("Registrazione senza righe di dettaglio")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
