* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_ssc                                                        *
*              Stampa schede conto corrente                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_28]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-16                                                      *
* Last revis.: 2008-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsba_ssc",oParentObject))

* --- Class definition
define class tgsba_ssc as StdForm
  Top    = 45
  Left   = 21

  * --- Standard Properties
  Width  = 559
  Height = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-05"
  HelpContextID=90867305
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  cPrg = "gsba_ssc"
  cComment = "Stampa schede conto corrente"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_CODICE = space(15)
  o_CODICE = space(15)
  w_CODICE1 = space(15)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_DESCRI = space(35)
  w_DESCRI1 = space(35)
  w_DATOBSO = ctod('  /  /  ')
  w_FLGSALD = space(1)
  w_DATAINI = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsba_sscPag1","gsba_ssc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='COC_MAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsba_ssc
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_CODICE=space(15)
      .w_CODICE1=space(15)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_DESCRI=space(35)
      .w_DESCRI1=space(35)
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLGSALD=space(1)
      .w_DATAINI=ctod("  /  /  ")
        .w_OBTEST = i_DATSYS
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODICE))
          .link_1_2('Full')
        endif
        .w_CODICE1 = .w_CODICE
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODICE1))
          .link_1_3('Full')
        endif
        .w_DATA1 = g_iniese
        .w_DATA2 = g_finese
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
          .DoRTCalc(6,9,.f.)
        .w_DATAINI = .w_DATA1-1
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CODICE<>.w_CODICE
            .w_CODICE1 = .w_CODICE
          .link_1_3('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(4,9,.t.)
        if .o_DATA1<>.w_DATA1
            .w_DATAINI = .w_DATA1-1
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODICE))
          select BACODBAN,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODICE)+"%");

            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODICE_1_2'),i_cWhere,'GSTE_ACB',"Conto banche",'gste5mcc.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODICE)
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.BACODBAN,space(15))
      this.w_Descri = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_Descri = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODICE<=.w_CODICE1 or (empty(.w_CODICE1)))  and (empty(nvl(.w_DATOBSO,ctod('  -  -    '))) or .w_DATOBSO > i_DATSYS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o obsoleto")
        endif
        this.w_CODICE = space(15)
        this.w_Descri = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODICE1)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODICE1))
          select BACODBAN,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE1)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODICE1)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODICE1)+"%");

            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE1) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODICE1_1_3'),i_cWhere,'GSTE_ACB',"Conto banche",'gste5mcc.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODICE1)
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE1 = NVL(_Link_.BACODBAN,space(15))
      this.w_Descri1 = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE1 = space(15)
      endif
      this.w_Descri1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODICE<=.w_CODICE1 or (empty(.w_CODICE))) and (empty(nvl(.w_DATOBSO,ctod('  -  -    '))) or .w_DATOBSO > i_DATSYS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o obsoleto")
        endif
        this.w_CODICE1 = space(15)
        this.w_Descri1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_2.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_2.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE1_1_3.value==this.w_CODICE1)
      this.oPgFrm.Page1.oPag.oCODICE1_1_3.value=this.w_CODICE1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_4.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_4.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_5.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_5.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_6.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_6.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_10.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_10.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGSALD_1_17.RadioValue()==this.w_FLGSALD)
      this.oPgFrm.Page1.oPag.oFLGSALD_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CODICE<=.w_CODICE1 or (empty(.w_CODICE1)))  and (empty(nvl(.w_DATOBSO,ctod('  -  -    '))) or .w_DATOBSO > i_DATSYS))  and not(empty(.w_CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o obsoleto")
          case   not((.w_CODICE<=.w_CODICE1 or (empty(.w_CODICE))) and (empty(nvl(.w_DATOBSO,ctod('  -  -    '))) or .w_DATOBSO > i_DATSYS))  and not(empty(.w_CODICE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE1_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o obsoleto")
          case   ((empty(.w_DATA1)) or not(.w_DATA1<.w_DATA2 or empty(.w_DATA2)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATA1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date errato")
          case   ((empty(.w_DATA2)) or not(.w_DATA1<.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATA2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date errato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODICE = this.w_CODICE
    this.o_DATA1 = this.w_DATA1
    return

enddefine

* --- Define pages as container
define class tgsba_sscPag1 as StdContainer
  Width  = 555
  height = 217
  stdWidth  = 555
  stdheight = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_2 as StdField with uid="SEDCEQQJXL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale o obsoleto",;
    ToolTipText = "Conto di inizio selezione",;
    HelpContextID = 200078042,;
   bGlobalFont=.t.,;
    Height=21, Width=130, Left=158, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODICE"

  func oCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conto banche",'gste5mcc.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oCODICE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oCODICE1_1_3 as StdField with uid="KMNYDEZXBZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODICE1", cQueryName = "CODICE1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale o obsoleto",;
    ToolTipText = "Conto di fine selezione",;
    HelpContextID = 68357414,;
   bGlobalFont=.t.,;
    Height=21, Width=130, Left=158, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODICE1"

  func oCODICE1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE1_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE1_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODICE1_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conto banche",'gste5mcc.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oCODICE1_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CODICE1
     i_obj.ecpSave()
  endproc

  add object oDATA1_1_4 as StdField with uid="VXRCDGLWFS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date errato",;
    ToolTipText = "Data di selezione inizio stampa",;
    HelpContextID = 34865354,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=158, Top=61

  func oDATA1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATA1<.w_DATA2 or empty(.w_DATA2))
    endwith
    return bRes
  endfunc

  add object oDATA2_1_5 as StdField with uid="CGAOHUAYQU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date errato",;
    ToolTipText = "Data di selezione fine stampa",;
    HelpContextID = 33816778,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=158, Top=85

  func oDATA2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATA1<.w_DATA2)
    endwith
    return bRes
  endfunc

  add object oDESCRI_1_6 as StdField with uid="AMBEZJKTKT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117574858,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=292, Top=9, InputMask=replicate('X',35)

  add object oDESCRI1_1_10 as StdField with uid="BCGZPFUYHY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 150860598,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=292, Top=32, InputMask=replicate('X',35)


  add object oObj_1_13 as cp_outputCombo with uid="ZLERXIVPWT",left=157, top=137, width=391,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87130342


  add object oBtn_1_14 as StdButton with uid="CCWKPZTUOA",left=444, top=167, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare stampa";
    , HelpContextID = 50922278;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OREP))
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="JOWDNEMOQA",left=500, top=167, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire dalla maschera";
    , HelpContextID = 83549882;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLGSALD_1_17 as StdCheck with uid="MAFNMUXAPU",rtseq=9,rtrep=.f.,left=158, top=112, caption="Saldi periodo",;
    ToolTipText = "Se attivo: la stampa calcola saldi periodo",;
    HelpContextID = 184367702,;
    cFormVar="w_FLGSALD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGSALD_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGSALD_1_17.GetRadio()
    this.Parent.oContained.w_FLGSALD = this.RadioValue()
    return .t.
  endfunc

  func oFLGSALD_1_17.SetRadio()
    this.Parent.oContained.w_FLGSALD=trim(this.Parent.oContained.w_FLGSALD)
    this.value = ;
      iif(this.Parent.oContained.w_FLGSALD=='S',1,;
      0)
  endfunc

  add object oStr_1_7 as StdString with uid="ONKACWOFIP",Visible=.t., Left=45, Top=9,;
    Alignment=1, Width=109, Height=15,;
    Caption="Da conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ZTOBCNANQT",Visible=.t., Left=6, Top=61,;
    Alignment=1, Width=148, Height=18,;
    Caption="Da data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="RZEKKZQAOD",Visible=.t., Left=66, Top=32,;
    Alignment=1, Width=88, Height=15,;
    Caption="A conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="NDLSKAYPTF",Visible=.t., Left=6, Top=85,;
    Alignment=1, Width=148, Height=18,;
    Caption="A data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="UWYQFANCYF",Visible=.t., Left=13, Top=137,;
    Alignment=1, Width=140, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsba_ssc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
