* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_mmc                                                        *
*              Movimenti di conto corrente                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_162]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-12                                                      *
* Last revis.: 2015-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsba_mmc"))

* --- Class definition
define class tgsba_mmc as StdTrsForm
  Top    = 10
  Left   = 9

  * --- Standard Properties
  Width  = 759
  Height = 360+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-13"
  HelpContextID=197822057
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CCM_MAST_IDX = 0
  CCM_DETT_IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  CCC_MAST_IDX = 0
  CCC_DETT_IDX = 0
  cFile = "CCM_MAST"
  cFileDetail = "CCM_DETT"
  cKeySelect = "CCSERIAL,CCROWRIF"
  cKeyWhere  = "CCSERIAL=this.w_CCSERIAL and CCROWRIF=this.w_CCROWRIF"
  cKeyDetail  = "CCSERIAL=this.w_CCSERIAL and CCROWRIF=this.w_CCROWRIF"
  cKeyWhereODBC = '"CCSERIAL="+cp_ToStrODBC(this.w_CCSERIAL)';
      +'+" and CCROWRIF="+cp_ToStrODBC(this.w_CCROWRIF)';

  cKeyDetailWhereODBC = '"CCSERIAL="+cp_ToStrODBC(this.w_CCSERIAL)';
      +'+" and CCROWRIF="+cp_ToStrODBC(this.w_CCROWRIF)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CCM_DETT.CCSERIAL="+cp_ToStrODBC(this.w_CCSERIAL)';
      +'+" and CCM_DETT.CCROWRIF="+cp_ToStrODBC(this.w_CCROWRIF)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CCM_DETT.CPROWORD '
  cPrg = "gsba_mmc"
  cComment = "Movimenti di conto corrente"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCSERIAL = space(10)
  o_CCSERIAL = space(10)
  w_CCROWRIF = 0
  o_CCROWRIF = 0
  w_CCNUMREG = 0
  w_CCDATREG = ctod('  /  /  ')
  o_CCDATREG = ctod('  /  /  ')
  w_CC__ANNO = space(4)
  w_CCCODCAU = space(5)
  o_CCCODCAU = space(5)
  w_CPROWORD = 0
  w_FLCOMP = space(1)
  w_FLCRDE = space(1)
  w_TIPCAU = space(1)
  w_CCNUMCOR = space(15)
  o_CCNUMCOR = space(15)
  w_TIPCON = space(1)
  w_CALFIR = space(3)
  w_NUMCOR = space(15)
  o_NUMCOR = space(15)
  w_CCDATVAL = ctod('  /  /  ')
  w_CCIMPCRE = 0
  o_CCIMPCRE = 0
  w_CCIMPDEB = 0
  o_CCIMPDEB = 0
  w_CCCOSCOM = 0
  o_CCCOSCOM = 0
  w_CCCOMVAL = 0
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_CCNUMDOC = 0
  w_CCALFDOC = space(10)
  w_CCDATDOC = ctod('  /  /  ')
  w_CCDESCRI = space(50)
  w_CCCODVAL = space(3)
  o_CCCODVAL = space(3)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_CAOVAL = 0
  w_SIMVAL = space(5)
  w_CCCAOVAL = 0
  w_DECTOT = 0
  w_CALCPICT = 0
  w_CCFLCOMM = space(1)
  w_DESCOR = space(35)
  w_DESCAU = space(35)
  w_TOTCRE = 0
  w_TOTDEB = 0
  w_TOTCOM = 0
  w_TOTALE = 0
  w_CCNUMCOR = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_DECEUR = 0
  w_CALCPIP = 0
  w_CCOBSO = ctod('  /  /  ')
  w_BAVAL = space(3)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CCSERIAL = this.W_CCSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CC__ANNO = this.W_CC__ANNO
  op_CCNUMREG = this.W_CCNUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CCM_MAST','gsba_mmc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsba_mmcPag1","gsba_mmc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimento di conto corrente")
      .Pages(1).HelpContextID = 12454115
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CCC_MAST'
    this.cWorkTables[4]='CCC_DETT'
    this.cWorkTables[5]='CCM_MAST'
    this.cWorkTables[6]='CCM_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CCM_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CCM_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CCSERIAL = NVL(CCSERIAL,space(10))
      .w_CCROWRIF = NVL(CCROWRIF,0)
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CCM_MAST where CCSERIAL=KeySet.CCSERIAL
    *                            and CCROWRIF=KeySet.CCROWRIF
    *
    i_nConn = i_TableProp[this.CCM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCM_MAST_IDX,2],this.bLoadRecFilter,this.CCM_MAST_IDX,"gsba_mmc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CCM_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CCM_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CCM_DETT.","CCM_MAST.")
      i_cTable = i_cTable+' CCM_MAST '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCSERIAL',this.w_CCSERIAL  ,'CCROWRIF',this.w_CCROWRIF  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CAOVAL = 0
        .w_SIMVAL = space(5)
        .w_DECTOT = 0
        .w_TOTCRE = 0
        .w_TOTDEB = 0
        .w_TOTCOM = 0
        .w_CODVAL = g_PERVAL
        .w_DECEUR = 0
        .w_CCSERIAL = NVL(CCSERIAL,space(10))
        .op_CCSERIAL = .w_CCSERIAL
        .w_CCROWRIF = NVL(CCROWRIF,0)
        .w_CCNUMREG = NVL(CCNUMREG,0)
        .op_CCNUMREG = .w_CCNUMREG
        .w_CCDATREG = NVL(cp_ToDate(CCDATREG),ctod("  /  /  "))
        .w_CC__ANNO = NVL(CC__ANNO,space(4))
        .op_CC__ANNO = .w_CC__ANNO
        .w_CCNUMDOC = NVL(CCNUMDOC,0)
        .w_CCALFDOC = NVL(CCALFDOC,space(10))
        .w_CCDATDOC = NVL(cp_ToDate(CCDATDOC),ctod("  /  /  "))
        .w_CCDESCRI = NVL(CCDESCRI,space(50))
        .w_CCCODVAL = NVL(CCCODVAL,space(3))
          if link_1_10_joined
            this.w_CCCODVAL = NVL(VACODVAL110,NVL(this.w_CCCODVAL,space(3)))
            this.w_SIMVAL = NVL(VASIMVAL110,space(5))
            this.w_DECTOT = NVL(VADECTOT110,0)
            this.w_CAOVAL = NVL(VACAOVAL110,0)
          else
          .link_1_10('Load')
          endif
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CCCAOVAL = NVL(CCCAOVAL,0)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_TOTALE = .w_TOTCRE - (.w_TOTDEB + .w_TOTCOM)
        .w_OBTEST = .w_CCDATREG
          .link_1_30('Load')
        .w_CALCPIP = DEFPIP(.w_DECEUR)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CCM_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CCM_DETT where CCSERIAL=KeySet.CCSERIAL
      *                            and CCROWRIF=KeySet.CCROWRIF
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CCM_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CCM_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CCM_DETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CCSERIAL',this.w_CCSERIAL  ,'CCROWRIF',this.w_CCROWRIF  )
        select * from (i_cTable) CCM_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTCRE = 0
      this.w_TOTDEB = 0
      this.w_TOTCOM = 0
      scan
        with this
          .w_FLCOMP = space(1)
          .w_FLCRDE = space(1)
          .w_TIPCAU = space(1)
          .w_TIPCON = space(1)
          .w_CALFIR = space(3)
          .w_DESCOR = space(35)
          .w_DESCAU = space(35)
          .w_CCOBSO = ctod("  /  /  ")
          .w_BAVAL = space(3)
          .w_CPROWNUM = CPROWNUM
          .w_CCCODCAU = NVL(CCCODCAU,space(5))
          if link_2_1_joined
            this.w_CCCODCAU = NVL(CACODICE201,NVL(this.w_CCCODCAU,space(5)))
            this.w_FLCRDE = NVL(CAFLCRDE201,space(1))
            this.w_DESCAU = NVL(CADESCRI201,space(35))
            this.w_TIPCAU = NVL(CATIPCON201,space(1))
            this.w_FLCOMP = NVL(CAFLCOMP201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CCNUMCOR = NVL(CCNUMCOR,space(15))
          if link_2_6_joined
            this.w_CCNUMCOR = NVL(BACODBAN206,NVL(this.w_CCNUMCOR,space(15)))
            this.w_DESCOR = NVL(BADESCRI206,space(35))
            this.w_TIPCON = NVL(BATIPCON206,space(1))
            this.w_CALFIR = NVL(BACALFES206,space(3))
            this.w_CCOBSO = NVL(cp_ToDate(BADTOBSO206),ctod("  /  /  "))
            this.w_BAVAL = NVL(BACODVAL206,space(3))
          else
          .link_2_6('Load')
          endif
        .w_NUMCOR = .w_CCNUMCOR
          .link_2_9('Load')
          .w_CCDATVAL = NVL(cp_ToDate(CCDATVAL),ctod("  /  /  "))
          .w_CCIMPCRE = NVL(CCIMPCRE,0)
          .w_CCIMPDEB = NVL(CCIMPDEB,0)
          .w_CCCOSCOM = NVL(CCCOSCOM,0)
          .w_CCCOMVAL = NVL(CCCOMVAL,0)
          .w_CCFLCRED = NVL(CCFLCRED,space(1))
          .w_CCFLDEBI = NVL(CCFLDEBI,space(1))
          .w_CCFLCOMM = NVL(CCFLCOMM,space(1))
          .w_CCNUMCOR = NVL(CCNUMCOR,space(15))
          * evitabile
          *.link_2_22('Load')
          select (this.cTrsName)
          append blank
          replace CCNUMCOR with .w_CCNUMCOR
          replace CCIMPCRE with .w_CCIMPCRE
          replace CCIMPDEB with .w_CCIMPDEB
          replace CCCOMVAL with .w_CCCOMVAL
          replace CCFLCRED with .w_CCFLCRED
          replace CCFLDEBI with .w_CCFLDEBI
          replace CCFLCOMM with .w_CCFLCOMM
          replace CCNUMCOR with .w_CCNUMCOR
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTCRE = .w_TOTCRE+.w_CCIMPCRE
          .w_TOTDEB = .w_TOTDEB+.w_CCIMPDEB
          .w_TOTCOM = .w_TOTCOM+.w_CCCOMVAL
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_TOTALE = .w_TOTCRE - (.w_TOTDEB + .w_TOTCOM)
        .w_OBTEST = .w_CCDATREG
        .w_CALCPIP = DEFPIP(.w_DECEUR)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CCSERIAL=space(10)
      .w_CCROWRIF=0
      .w_CCNUMREG=0
      .w_CCDATREG=ctod("  /  /  ")
      .w_CC__ANNO=space(4)
      .w_CCCODCAU=space(5)
      .w_CPROWORD=10
      .w_FLCOMP=space(1)
      .w_FLCRDE=space(1)
      .w_TIPCAU=space(1)
      .w_CCNUMCOR=space(15)
      .w_TIPCON=space(1)
      .w_CALFIR=space(3)
      .w_NUMCOR=space(15)
      .w_CCDATVAL=ctod("  /  /  ")
      .w_CCIMPCRE=0
      .w_CCIMPDEB=0
      .w_CCCOSCOM=0
      .w_CCCOMVAL=0
      .w_CCFLCRED=space(1)
      .w_CCFLDEBI=space(1)
      .w_CCNUMDOC=0
      .w_CCALFDOC=space(10)
      .w_CCDATDOC=ctod("  /  /  ")
      .w_CCDESCRI=space(50)
      .w_CCCODVAL=space(3)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTCV=0
      .w_UTDV=ctot("")
      .w_CAOVAL=0
      .w_SIMVAL=space(5)
      .w_CCCAOVAL=0
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_CCFLCOMM=space(1)
      .w_DESCOR=space(35)
      .w_DESCAU=space(35)
      .w_TOTCRE=0
      .w_TOTDEB=0
      .w_TOTCOM=0
      .w_TOTALE=0
      .w_CCNUMCOR=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_DECEUR=0
      .w_CALCPIP=0
      .w_CCOBSO=ctod("  /  /  ")
      .w_BAVAL=space(3)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CCROWRIF = -1
        .DoRTCalc(3,3,.f.)
        .w_CCDATREG = i_datsys
        .w_CC__ANNO = STR(YEAR(.w_CCDATREG),4,0)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CCCODCAU))
         .link_2_1('Full')
        endif
        .DoRTCalc(7,10,.f.)
        .w_CCNUMCOR = SPACE(15)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CCNUMCOR))
         .link_2_6('Full')
        endif
        .DoRTCalc(12,13,.f.)
        .w_NUMCOR = .w_CCNUMCOR
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_NUMCOR))
         .link_2_9('Full')
        endif
        .w_CCDATVAL = CALCFEST(.w_CCDATREG, .w_CALFIR, .w_CCNUMCOR, .w_CCCODCAU, .w_TIPCAU,1)
        .w_CCIMPCRE = 0
        .w_CCIMPDEB = 0
        .DoRTCalc(18,18,.f.)
        .w_CCCOMVAL = IIF(.w_CCIMPCRE<>0 or .w_CCIMPDEB<>0,VAL2VAL(.w_CCCOSCOM,.w_CCCAOVAL,.w_CCDATREG,I_DATSYS,GETCAM(g_PERVAL, I_DATSYS), .w_DECTOT),0)
        .w_CCFLCRED = IIF(.w_FLCRDE='C', '+', ' ')
        .w_CCFLDEBI = IIF(.w_FLCRDE='D', '+', ' ')
        .DoRTCalc(22,25,.f.)
        .w_CCCODVAL = g_PERVAL
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CCCODVAL))
         .link_1_10('Full')
        endif
        .DoRTCalc(27,32,.f.)
        .w_CCCAOVAL = GETCAM(.w_CCCODVAL, .w_CCDATREG, 7)
        .DoRTCalc(34,34,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CCFLCOMM = '+'
        .DoRTCalc(37,41,.f.)
        .w_TOTALE = .w_TOTCRE - (.w_TOTDEB + .w_TOTCOM)
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_CCNUMCOR))
         .link_2_22('Full')
        endif
        .w_OBTEST = .w_CCDATREG
        .w_CODVAL = g_PERVAL
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_CODVAL))
         .link_1_30('Full')
        endif
        .DoRTCalc(46,46,.f.)
        .w_CALCPIP = DEFPIP(.w_DECEUR)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CCM_MAST')
    this.DoRTCalc(48,49,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCNUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oCCDATREG_1_4.enabled = i_bVal
      .Page1.oPag.oCC__ANNO_1_5.enabled = i_bVal
      .Page1.oPag.oCCNUMDOC_1_6.enabled = i_bVal
      .Page1.oPag.oCCALFDOC_1_7.enabled = i_bVal
      .Page1.oPag.oCCDATDOC_1_8.enabled = i_bVal
      .Page1.oPag.oCCDESCRI_1_9.enabled = i_bVal
      .Page1.oPag.oCCCODVAL_1_10.enabled = i_bVal
      .Page1.oPag.oCCCAOVAL_1_20.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCCNUMREG_1_3.enabled = .t.
        .Page1.oPag.oCCDATREG_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CCM_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCM_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERMOVCO","i_codazi,w_CCSERIAL")
      cp_AskTableProg(this,i_nConn,"PROMOVCO","i_codazi,w_CC__ANNO,w_CCNUMREG")
      .op_codazi = .w_codazi
      .op_CCSERIAL = .w_CCSERIAL
      .op_codazi = .w_codazi
      .op_CC__ANNO = .w_CC__ANNO
      .op_CCNUMREG = .w_CCNUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CCM_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCSERIAL,"CCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCROWRIF,"CCROWRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCNUMREG,"CCNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDATREG,"CCDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CC__ANNO,"CC__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCNUMDOC,"CCNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCALFDOC,"CCALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDATDOC,"CCDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESCRI,"CCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODVAL,"CCCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCAOVAL,"CCCAOVAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CCM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCM_MAST_IDX,2])
    i_lTable = "CCM_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CCM_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do Gsba_sbm with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CCCODCAU C(5);
      ,t_CPROWORD N(4);
      ,t_CCNUMCOR C(15);
      ,t_CCDATVAL D(8);
      ,t_CCIMPCRE N(18,4);
      ,t_CCIMPDEB N(18,4);
      ,t_CCCOSCOM N(12,4);
      ,t_DESCOR C(35);
      ,t_DESCAU C(35);
      ,CCNUMCOR C(15);
      ,CCIMPCRE N(18,4);
      ,CCIMPDEB N(18,4);
      ,CCCOMVAL N(12,4);
      ,CCFLCRED C(1);
      ,CCFLDEBI C(1);
      ,CCFLCOMM C(1);
      ,CPROWNUM N(10);
      ,t_FLCOMP C(1);
      ,t_FLCRDE C(1);
      ,t_TIPCAU C(1);
      ,t_TIPCON C(1);
      ,t_CALFIR C(3);
      ,t_NUMCOR C(15);
      ,t_CCCOMVAL N(12,4);
      ,t_CCFLCRED C(1);
      ,t_CCFLDEBI C(1);
      ,t_CCFLCOMM C(1);
      ,t_CCOBSO D(8);
      ,t_BAVAL C(3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsba_mmcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1.controlsource=this.cTrsName+'.t_CCCODCAU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_6.controlsource=this.cTrsName+'.t_CCNUMCOR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_10.controlsource=this.cTrsName+'.t_CCDATVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPCRE_2_11.controlsource=this.cTrsName+'.t_CCIMPCRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPDEB_2_12.controlsource=this.cTrsName+'.t_CCIMPDEB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCCOSCOM_2_13.controlsource=this.cTrsName+'.t_CCCOSCOM'
    this.oPgFRm.Page1.oPag.oDESCOR_2_18.controlsource=this.cTrsName+'.t_DESCOR'
    this.oPgFRm.Page1.oPag.oDESCAU_2_19.controlsource=this.cTrsName+'.t_DESCAU'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(54)
    this.AddVLine(119)
    this.AddVLine(255)
    this.AddVLine(344)
    this.AddVLine(484)
    this.AddVLine(619)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCM_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCM_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SERMOVCO","i_codazi,w_CCSERIAL")
          cp_NextTableProg(this,i_nConn,"PROMOVCO","i_codazi,w_CC__ANNO,w_CCNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CCM_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CCM_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'CCM_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(CCSERIAL,CCROWRIF,CCNUMREG,CCDATREG,CC__ANNO"+;
                  ",CCNUMDOC,CCALFDOC,CCDATDOC,CCDESCRI,CCCODVAL"+;
                  ",UTCC,UTDC,UTCV,UTDV,CCCAOVAL"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CCSERIAL)+;
                    ","+cp_ToStrODBC(this.w_CCROWRIF)+;
                    ","+cp_ToStrODBC(this.w_CCNUMREG)+;
                    ","+cp_ToStrODBC(this.w_CCDATREG)+;
                    ","+cp_ToStrODBC(this.w_CC__ANNO)+;
                    ","+cp_ToStrODBC(this.w_CCNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_CCALFDOC)+;
                    ","+cp_ToStrODBC(this.w_CCDATDOC)+;
                    ","+cp_ToStrODBC(this.w_CCDESCRI)+;
                    ","+cp_ToStrODBCNull(this.w_CCCODVAL)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_CCCAOVAL)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CCM_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'CCM_MAST')
        cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_CCSERIAL,'CCROWRIF',this.w_CCROWRIF)
        INSERT INTO (i_cTable);
              (CCSERIAL,CCROWRIF,CCNUMREG,CCDATREG,CC__ANNO,CCNUMDOC,CCALFDOC,CCDATDOC,CCDESCRI,CCCODVAL,UTCC,UTDC,UTCV,UTDV,CCCAOVAL &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CCSERIAL;
                  ,this.w_CCROWRIF;
                  ,this.w_CCNUMREG;
                  ,this.w_CCDATREG;
                  ,this.w_CC__ANNO;
                  ,this.w_CCNUMDOC;
                  ,this.w_CCALFDOC;
                  ,this.w_CCDATDOC;
                  ,this.w_CCDESCRI;
                  ,this.w_CCCODVAL;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_CCCAOVAL;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
      *
      * insert into CCM_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CCSERIAL,CCROWRIF,CCCODCAU,CPROWORD,CCNUMCOR"+;
                  ",CCDATVAL,CCIMPCRE,CCIMPDEB,CCCOSCOM,CCCOMVAL"+;
                  ",CCFLCRED,CCFLDEBI,CCFLCOMM,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CCSERIAL)+","+cp_ToStrODBC(this.w_CCROWRIF)+","+cp_ToStrODBCNull(this.w_CCCODCAU)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_CCNUMCOR)+;
             ","+cp_ToStrODBC(this.w_CCDATVAL)+","+cp_ToStrODBC(this.w_CCIMPCRE)+","+cp_ToStrODBC(this.w_CCIMPDEB)+","+cp_ToStrODBC(this.w_CCCOSCOM)+","+cp_ToStrODBC(this.w_CCCOMVAL)+;
             ","+cp_ToStrODBC(this.w_CCFLCRED)+","+cp_ToStrODBC(this.w_CCFLDEBI)+","+cp_ToStrODBC(this.w_CCFLCOMM)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CCSERIAL',this.w_CCSERIAL,'CCROWRIF',this.w_CCROWRIF)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CCSERIAL,this.w_CCROWRIF,this.w_CCCODCAU,this.w_CPROWORD,this.w_CCNUMCOR"+;
                ",this.w_CCDATVAL,this.w_CCIMPCRE,this.w_CCIMPDEB,this.w_CCCOSCOM,this.w_CCCOMVAL"+;
                ",this.w_CCFLCRED,this.w_CCFLDEBI,this.w_CCFLCOMM,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CCM_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCM_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CCM_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CCM_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CCNUMREG="+cp_ToStrODBC(this.w_CCNUMREG)+;
             ",CCDATREG="+cp_ToStrODBC(this.w_CCDATREG)+;
             ",CC__ANNO="+cp_ToStrODBC(this.w_CC__ANNO)+;
             ",CCNUMDOC="+cp_ToStrODBC(this.w_CCNUMDOC)+;
             ",CCALFDOC="+cp_ToStrODBC(this.w_CCALFDOC)+;
             ",CCDATDOC="+cp_ToStrODBC(this.w_CCDATDOC)+;
             ",CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
             ",CCCODVAL="+cp_ToStrODBCNull(this.w_CCCODVAL)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CCCAOVAL="+cp_ToStrODBC(this.w_CCCAOVAL)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CCM_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'CCSERIAL',this.w_CCSERIAL  ,'CCROWRIF',this.w_CCROWRIF  )
          UPDATE (i_cTable) SET;
              CCNUMREG=this.w_CCNUMREG;
             ,CCDATREG=this.w_CCDATREG;
             ,CC__ANNO=this.w_CC__ANNO;
             ,CCNUMDOC=this.w_CCNUMDOC;
             ,CCALFDOC=this.w_CCALFDOC;
             ,CCDATDOC=this.w_CCDATDOC;
             ,CCDESCRI=this.w_CCDESCRI;
             ,CCCODVAL=this.w_CCCODVAL;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,CCCAOVAL=this.w_CCCAOVAL;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_CCCODCAU) AND NOT EMPTY(t_CCNUMCOR) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CCM_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CCM_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CCCODCAU="+cp_ToStrODBCNull(this.w_CCCODCAU)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CCNUMCOR="+cp_ToStrODBCNull(this.w_CCNUMCOR)+;
                     ",CCDATVAL="+cp_ToStrODBC(this.w_CCDATVAL)+;
                     ",CCIMPCRE="+cp_ToStrODBC(this.w_CCIMPCRE)+;
                     ",CCIMPDEB="+cp_ToStrODBC(this.w_CCIMPDEB)+;
                     ",CCCOSCOM="+cp_ToStrODBC(this.w_CCCOSCOM)+;
                     ",CCCOMVAL="+cp_ToStrODBC(this.w_CCCOMVAL)+;
                     ",CCFLCRED="+cp_ToStrODBC(this.w_CCFLCRED)+;
                     ",CCFLDEBI="+cp_ToStrODBC(this.w_CCFLDEBI)+;
                     ",CCFLCOMM="+cp_ToStrODBC(this.w_CCFLCOMM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CCCODCAU=this.w_CCCODCAU;
                     ,CPROWORD=this.w_CPROWORD;
                     ,CCNUMCOR=this.w_CCNUMCOR;
                     ,CCDATVAL=this.w_CCDATVAL;
                     ,CCIMPCRE=this.w_CCIMPCRE;
                     ,CCIMPDEB=this.w_CCIMPDEB;
                     ,CCCOSCOM=this.w_CCCOSCOM;
                     ,CCCOMVAL=this.w_CCCOMVAL;
                     ,CCFLCRED=this.w_CCFLCRED;
                     ,CCFLDEBI=this.w_CCFLDEBI;
                     ,CCFLCOMM=this.w_CCFLCOMM;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsba_mmc
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * --- Riposiziona sul Primo record del Temporaneo dei Documenti
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        this.SaveDependsOn()
    
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..CCFLCRED,space(1))==this.w_CCFLCRED;
              and NVL(&i_cF..CCIMPCRE,0)==this.w_CCIMPCRE;
              and NVL(&i_cF..CCFLDEBI,space(1))==this.w_CCFLDEBI;
              and NVL(&i_cF..CCIMPDEB,0)==this.w_CCIMPDEB;
              and NVL(&i_cF..CCNUMCOR,space(15))==this.w_CCNUMCOR;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..CCFLCRED,space(1)),'BASALCRE','',NVL(&i_cF..CCIMPCRE,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..CCFLDEBI,space(1)),'BASALDEB','',NVL(&i_cF..CCIMPDEB,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..CCNUMCOR,space(15))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" BASALCRE="+i_cOp1+","           +" BASALDEB="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE BACODBAN="+cp_ToStrODBC(NVL(&i_cF..CCNUMCOR,space(15)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..CCFLCRED,'BASALCRE',i_cF+'.CCIMPCRE',&i_cF..CCIMPCRE,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..CCFLDEBI,'BASALDEB',i_cF+'.CCIMPDEB',&i_cF..CCIMPDEB,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'BACODBAN',&i_cF..CCNUMCOR)
      UPDATE (i_cTable) SET ;
           BASALCRE=&i_cOp1.  ,;
           BASALDEB=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..CCFLCOMM,space(1))==this.w_CCFLCOMM;
              and NVL(&i_cF..CCCOMVAL,0)==this.w_CCCOMVAL;
              and NVL(&i_cF..CCNUMCOR,space(15))==this.w_CCNUMCOR;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..CCFLCOMM,space(1)),'BASALDEB','',NVL(&i_cF..CCCOMVAL,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..CCNUMCOR,space(15))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" BASALDEB="+i_cOp1+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE BACODBAN="+cp_ToStrODBC(NVL(&i_cF..CCNUMCOR,space(15)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..CCFLCOMM,'BASALDEB',i_cF+'.CCCOMVAL',&i_cF..CCCOMVAL,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'BACODBAN',&i_cF..CCNUMCOR)
      UPDATE (i_cTable) SET ;
           BASALDEB=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..CCFLCRED,space(1))==this.w_CCFLCRED;
              and NVL(&i_cF..CCIMPCRE,0)==this.w_CCIMPCRE;
              and NVL(&i_cF..CCFLDEBI,space(1))==this.w_CCFLDEBI;
              and NVL(&i_cF..CCIMPDEB,0)==this.w_CCIMPDEB;
              and NVL(&i_cF..CCNUMCOR,space(15))==this.w_CCNUMCOR;

      i_cOp1=cp_SetTrsOp(this.w_CCFLCRED,'BASALCRE','this.w_CCIMPCRE',this.w_CCIMPCRE,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_CCFLDEBI,'BASALDEB','this.w_CCIMPDEB',this.w_CCIMPDEB,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_CCNUMCOR)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" BASALCRE="+i_cOp1  +",";
         +" BASALDEB="+i_cOp2  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE BACODBAN="+cp_ToStrODBC(this.w_CCNUMCOR);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_CCFLCRED,'BASALCRE','this.w_CCIMPCRE',this.w_CCIMPCRE,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_CCFLDEBI,'BASALDEB','this.w_CCIMPDEB',this.w_CCIMPDEB,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'BACODBAN',this.w_CCNUMCOR)
      UPDATE (i_cTable) SET;
           BASALCRE=&i_cOp1.  ,;
           BASALDEB=&i_cOp2.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..CCFLCOMM,space(1))==this.w_CCFLCOMM;
              and NVL(&i_cF..CCCOMVAL,0)==this.w_CCCOMVAL;
              and NVL(&i_cF..CCNUMCOR,space(15))==this.w_CCNUMCOR;

      i_cOp1=cp_SetTrsOp(this.w_CCFLCOMM,'BASALDEB','this.w_CCCOMVAL',this.w_CCCOMVAL,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_CCNUMCOR)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" BASALDEB="+i_cOp1  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE BACODBAN="+cp_ToStrODBC(this.w_CCNUMCOR);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_CCFLCOMM,'BASALDEB','this.w_CCCOMVAL',this.w_CCCOMVAL,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'BACODBAN',this.w_CCNUMCOR)
      UPDATE (i_cTable) SET;
           BASALDEB=&i_cOp1.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_CCCODCAU) AND NOT EMPTY(t_CCNUMCOR) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
        *
        * delete CCM_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CCM_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CCM_MAST_IDX,2])
        *
        * delete CCM_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_CCCODCAU) AND NOT EMPTY(t_CCNUMCOR) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCM_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
        if .o_CCCODCAU<>.w_CCCODCAU
          .link_2_6('Full')
        endif
        .DoRTCalc(12,13,.t.)
        if .o_CCNUMCOR<>.w_CCNUMCOR
          .w_NUMCOR = .w_CCNUMCOR
          .link_2_9('Full')
        endif
        if .o_CCDATREG<>.w_CCDATREG.or. .o_CCCODCAU<>.w_CCCODCAU.or. .o_CCNUMCOR<>.w_CCNUMCOR
          .w_CCDATVAL = CALCFEST(.w_CCDATREG, .w_CALFIR, .w_CCNUMCOR, .w_CCCODCAU, .w_TIPCAU,1)
        endif
        if .o_CCCODCAU<>.w_CCCODCAU
          .w_TOTCRE = .w_TOTCRE-.w_ccimpcre
          .w_CCIMPCRE = 0
          .w_TOTCRE = .w_TOTCRE+.w_ccimpcre
        endif
        if .o_CCCODCAU<>.w_CCCODCAU
          .w_TOTDEB = .w_TOTDEB-.w_ccimpdeb
          .w_CCIMPDEB = 0
          .w_TOTDEB = .w_TOTDEB+.w_ccimpdeb
        endif
        .DoRTCalc(18,18,.t.)
        if .o_CCCOSCOM<>.w_CCCOSCOM.or. .o_CCIMPCRE<>.w_CCIMPCRE.or. .o_CCIMPDEB<>.w_CCIMPDEB
          .w_TOTCOM = .w_TOTCOM-.w_cccomval
          .w_CCCOMVAL = IIF(.w_CCIMPCRE<>0 or .w_CCIMPDEB<>0,VAL2VAL(.w_CCCOSCOM,.w_CCCAOVAL,.w_CCDATREG,I_DATSYS,GETCAM(g_PERVAL, I_DATSYS), .w_DECTOT),0)
          .w_TOTCOM = .w_TOTCOM+.w_cccomval
        endif
        if .o_CCCODCAU<>.w_CCCODCAU
          .w_CCFLCRED = IIF(.w_FLCRDE='C', '+', ' ')
        endif
        if .o_CCCODCAU<>.w_CCCODCAU
          .w_CCFLDEBI = IIF(.w_FLCRDE='D', '+', ' ')
        endif
        .DoRTCalc(22,32,.t.)
        if .o_CCCODVAL<>.w_CCCODVAL
          .w_CCCAOVAL = GETCAM(.w_CCCODVAL, .w_CCDATREG, 7)
        endif
        .DoRTCalc(34,34,.t.)
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        if .o_CCCODCAU<>.w_CCCODCAU
          .w_CCFLCOMM = '+'
        endif
        .DoRTCalc(37,41,.t.)
          .w_TOTALE = .w_TOTCRE - (.w_TOTDEB + .w_TOTCOM)
        if .o_CCSERIAL<>.w_CCSERIAL.or. .o_CCROWRIF<>.w_CCROWRIF
          .link_2_22('Full')
        endif
        if .o_CCDATREG<>.w_CCDATREG
          .w_OBTEST = .w_CCDATREG
        endif
          .link_1_30('Full')
        .DoRTCalc(46,46,.t.)
          .w_CALCPIP = DEFPIP(.w_DECEUR)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SERMOVCO","i_codazi,w_CCSERIAL")
          .op_CCSERIAL = .w_CCSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_CC__ANNO<>.w_CC__ANNO
           cp_AskTableProg(this,i_nConn,"PROMOVCO","i_codazi,w_CC__ANNO,w_CCNUMREG")
          .op_CCNUMREG = .w_CCNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_CC__ANNO = .w_CC__ANNO
      endwith
      this.DoRTCalc(48,49,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_FLCOMP with this.w_FLCOMP
      replace t_FLCRDE with this.w_FLCRDE
      replace t_TIPCAU with this.w_TIPCAU
      replace t_TIPCON with this.w_TIPCON
      replace t_CALFIR with this.w_CALFIR
      replace t_NUMCOR with this.w_NUMCOR
      replace t_CCCOMVAL with this.w_CCCOMVAL
      replace t_CCFLCRED with this.w_CCFLCRED
      replace t_CCFLDEBI with this.w_CCFLDEBI
      replace t_CCFLCOMM with this.w_CCFLCOMM
      replace t_CCNUMCOR with this.w_CCNUMCOR
      replace t_CCOBSO with this.w_CCOBSO
      replace t_BAVAL with this.w_BAVAL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCCCODVAL_1_10.enabled = this.oPgFrm.Page1.oPag.oCCCODVAL_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCCCAOVAL_1_20.enabled = this.oPgFrm.Page1.oPag.oCCCAOVAL_1_20.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCNUMCOR_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCNUMCOR_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCDATVAL_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCDATVAL_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCIMPCRE_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCIMPCRE_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCIMPDEB_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCIMPDEB_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCOSCOM_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCOSCOM_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCCCAOVAL_1_20.visible=!this.oPgFrm.Page1.oPag.oCCCAOVAL_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCCODCAU
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MCT',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CCCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CAFLCRDE,CADESCRI,CATIPCON,CAFLCOMP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CCCODCAU))
          select CACODICE,CAFLCRDE,CADESCRI,CATIPCON,CAFLCOMP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODCAU)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCCCODCAU_2_1'),i_cWhere,'GSBA_MCT',"Causali conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAFLCRDE,CADESCRI,CATIPCON,CAFLCOMP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CAFLCRDE,CADESCRI,CATIPCON,CAFLCOMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAFLCRDE,CADESCRI,CATIPCON,CAFLCOMP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CCCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CCCODCAU)
            select CACODICE,CAFLCRDE,CADESCRI,CATIPCON,CAFLCOMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODCAU = NVL(_Link_.CACODICE,space(5))
      this.w_FLCRDE = NVL(_Link_.CAFLCRDE,space(1))
      this.w_DESCAU = NVL(_Link_.CADESCRI,space(35))
      this.w_TIPCAU = NVL(_Link_.CATIPCON,space(1))
      this.w_FLCOMP = NVL(_Link_.CAFLCOMP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODCAU = space(5)
      endif
      this.w_FLCRDE = space(1)
      this.w_DESCAU = space(35)
      this.w_TIPCAU = space(1)
      this.w_FLCOMP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CCC_MAST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CACODICE as CACODICE201"+ ",link_2_1.CAFLCRDE as CAFLCRDE201"+ ",link_2_1.CADESCRI as CADESCRI201"+ ",link_2_1.CATIPCON as CATIPCON201"+ ",link_2_1.CAFLCOMP as CAFLCOMP201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on CCM_DETT.CCCODCAU=link_2_1.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and CCM_DETT.CCCODCAU=link_2_1.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCNUMCOR
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CCNUMCOR)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BATIPCON,BACALFES,BADTOBSO,BACODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CCNUMCOR))
          select BACODBAN,BADESCRI,BATIPCON,BACALFES,BADTOBSO,BACODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCNUMCOR)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCNUMCOR) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCCNUMCOR_2_6'),i_cWhere,'GSTE_ACB',"Conti correnti",'GSBA_QCM.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BATIPCON,BACALFES,BADTOBSO,BACODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BATIPCON,BACALFES,BADTOBSO,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BATIPCON,BACALFES,BADTOBSO,BACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CCNUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CCNUMCOR)
            select BACODBAN,BADESCRI,BATIPCON,BACALFES,BADTOBSO,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCNUMCOR = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCOR = NVL(_Link_.BADESCRI,space(35))
      this.w_TIPCON = NVL(_Link_.BATIPCON,space(1))
      this.w_CALFIR = NVL(_Link_.BACALFES,space(3))
      this.w_CCOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_BAVAL = NVL(_Link_.BACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CCNUMCOR = space(15)
      endif
      this.w_DESCOR = space(35)
      this.w_TIPCON = space(1)
      this.w_CALFIR = space(3)
      this.w_CCOBSO = ctod("  /  /  ")
      this.w_BAVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CCNUMCOR) OR .w_FLCOMP<>'N' OR CHKNUMCR(.w_CCCODCAU, .w_TIPCAU, .w_CCNUMCOR, .w_TIPCON) AND (.w_CCOBSO>.w_CCDATREG OR EMPTY(.w_CCOBSO)))AND(.w_BAVAL=.w_CCCODVAL)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto corrente inesistente o incongruente o obsoleto")
        endif
        this.w_CCNUMCOR = space(15)
        this.w_DESCOR = space(35)
        this.w_TIPCON = space(1)
        this.w_CALFIR = space(3)
        this.w_CCOBSO = ctod("  /  /  ")
        this.w_BAVAL = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.BACODBAN as BACODBAN206"+ ",link_2_6.BADESCRI as BADESCRI206"+ ",link_2_6.BATIPCON as BATIPCON206"+ ",link_2_6.BACALFES as BACALFES206"+ ",link_2_6.BADTOBSO as BADTOBSO206"+ ",link_2_6.BACODVAL as BACODVAL206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on CCM_DETT.CCNUMCOR=link_2_6.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and CCM_DETT.CCNUMCOR=link_2_6.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NUMCOR
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_DETT_IDX,3]
    i_lTable = "CCC_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2], .t., this.CCC_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CANUMCOR,CAIMPCOM";
                   +" from "+i_cTable+" "+i_lTable+" where CANUMCOR="+cp_ToStrODBC(this.w_NUMCOR);
                   +" and CACODICE="+cp_ToStrODBC(this.w_CCCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CCCODCAU;
                       ,'CANUMCOR',this.w_NUMCOR)
            select CACODICE,CANUMCOR,CAIMPCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMCOR = NVL(_Link_.CANUMCOR,space(15))
      this.w_CCCOSCOM = NVL(_Link_.CAIMPCOM,0)
    else
      if i_cCtrl<>'Load'
        this.w_NUMCOR = space(15)
      endif
      this.w_CCCOSCOM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)+'\'+cp_ToStr(_Link_.CANUMCOR,1)
      cp_ShowWarn(i_cKey,this.CCC_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODVAL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CCCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CCCODVAL))
          select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCCCODVAL_1_10'),i_cWhere,'',"",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CCCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CCCODVAL)
            select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_CCCODVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.VACODVAL as VACODVAL110"+ ",link_1_10.VASIMVAL as VASIMVAL110"+ ",link_1_10.VADECTOT as VADECTOT110"+ ",link_1_10.VACAOVAL as VACAOVAL110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on CCM_MAST.CCCODVAL=link_1_10.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and CCM_MAST.CCCODVAL=link_1_10.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCNUMCOR
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CCNUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CCNUMCOR)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCNUMCOR = NVL(_Link_.BACODBAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CCNUMCOR = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECEUR = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DECEUR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCCNUMREG_1_3.value==this.w_CCNUMREG)
      this.oPgFrm.Page1.oPag.oCCNUMREG_1_3.value=this.w_CCNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDATREG_1_4.value==this.w_CCDATREG)
      this.oPgFrm.Page1.oPag.oCCDATREG_1_4.value=this.w_CCDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCC__ANNO_1_5.value==this.w_CC__ANNO)
      this.oPgFrm.Page1.oPag.oCC__ANNO_1_5.value=this.w_CC__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNUMDOC_1_6.value==this.w_CCNUMDOC)
      this.oPgFrm.Page1.oPag.oCCNUMDOC_1_6.value=this.w_CCNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCCALFDOC_1_7.value==this.w_CCALFDOC)
      this.oPgFrm.Page1.oPag.oCCALFDOC_1_7.value=this.w_CCALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDATDOC_1_8.value==this.w_CCDATDOC)
      this.oPgFrm.Page1.oPag.oCCDATDOC_1_8.value=this.w_CCDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_9.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_9.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODVAL_1_10.value==this.w_CCCODVAL)
      this.oPgFrm.Page1.oPag.oCCCODVAL_1_10.value=this.w_CCCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_19.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_19.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCAOVAL_1_20.value==this.w_CCCAOVAL)
      this.oPgFrm.Page1.oPag.oCCCAOVAL_1_20.value=this.w_CCCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOR_2_18.value==this.w_DESCOR)
      this.oPgFrm.Page1.oPag.oDESCOR_2_18.value=this.w_DESCOR
      replace t_DESCOR with this.oPgFrm.Page1.oPag.oDESCOR_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_2_19.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_2_19.value=this.w_DESCAU
      replace t_DESCAU with this.oPgFrm.Page1.oPag.oDESCAU_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_4.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_4.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1.value==this.w_CCCODCAU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1.value=this.w_CCCODCAU
      replace t_CCCODCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_6.value==this.w_CCNUMCOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_6.value=this.w_CCNUMCOR
      replace t_CCNUMCOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_10.value==this.w_CCDATVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_10.value=this.w_CCDATVAL
      replace t_CCDATVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPCRE_2_11.value==this.w_CCIMPCRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPCRE_2_11.value=this.w_CCIMPCRE
      replace t_CCIMPCRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPCRE_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPDEB_2_12.value==this.w_CCIMPDEB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPDEB_2_12.value=this.w_CCIMPDEB
      replace t_CCIMPDEB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPDEB_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCOSCOM_2_13.value==this.w_CCCOSCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCOSCOM_2_13.value=this.w_CCCOSCOM
      replace t_CCCOSCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCOSCOM_2_13.value
    endif
    cp_SetControlsValueExtFlds(this,'CCM_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CCDATREG>=.w_CCDATDOC)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCCDATDOC_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data documento superiore alla data registrazione")
          case   (empty(.w_CCCODVAL))  and (.cfunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCCCODVAL_1_10.SetFocus()
            i_bnoObbl = !empty(.w_CCCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((EMPTY(.w_CCNUMCOR) OR .w_FLCOMP<>'N' OR CHKNUMCR(.w_CCCODCAU, .w_TIPCAU, .w_CCNUMCOR, .w_TIPCON) AND (.w_CCOBSO>.w_CCDATREG OR EMPTY(.w_CCOBSO)))AND(.w_BAVAL=.w_CCCODVAL)) and (NOT EMPTY(.w_CCCODCAU)) and not(empty(.w_CCNUMCOR)) and (NOT EMPTY(.w_CCCODCAU) AND NOT EMPTY(.w_CCNUMCOR) AND (.w_CCIMPCRE<>0 OR .w_CCIMPDEB<>0))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Conto corrente inesistente o incongruente o obsoleto")
        case   empty(.w_CCDATVAL) and (NOT EMPTY(.w_CCNUMCOR)) and (NOT EMPTY(.w_CCCODCAU) AND NOT EMPTY(.w_CCNUMCOR) AND (.w_CCIMPCRE<>0 OR .w_CCIMPDEB<>0))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_10
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if NOT EMPTY(.w_CCCODCAU) AND NOT EMPTY(.w_CCNUMCOR) AND (.w_CCIMPCRE<>0 OR .w_CCIMPDEB<>0)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CCSERIAL = this.w_CCSERIAL
    this.o_CCROWRIF = this.w_CCROWRIF
    this.o_CCDATREG = this.w_CCDATREG
    this.o_CCCODCAU = this.w_CCCODCAU
    this.o_CCNUMCOR = this.w_CCNUMCOR
    this.o_NUMCOR = this.w_NUMCOR
    this.o_CCIMPCRE = this.w_CCIMPCRE
    this.o_CCIMPDEB = this.w_CCIMPDEB
    this.o_CCCOSCOM = this.w_CCCOSCOM
    this.o_CCCODVAL = this.w_CCCODVAL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_CCCODCAU) AND NOT EMPTY(t_CCNUMCOR) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CCCODCAU=space(5)
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_FLCOMP=space(1)
      .w_FLCRDE=space(1)
      .w_TIPCAU=space(1)
      .w_CCNUMCOR=space(15)
      .w_TIPCON=space(1)
      .w_CALFIR=space(3)
      .w_NUMCOR=space(15)
      .w_CCDATVAL=ctod("  /  /  ")
      .w_CCIMPCRE=0
      .w_CCIMPDEB=0
      .w_CCCOSCOM=0
      .w_CCCOMVAL=0
      .w_CCFLCRED=space(1)
      .w_CCFLDEBI=space(1)
      .w_CCFLCOMM=space(1)
      .w_DESCOR=space(35)
      .w_DESCAU=space(35)
      .w_CCNUMCOR=space(15)
      .w_CCOBSO=ctod("  /  /  ")
      .w_BAVAL=space(3)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_CCCODCAU))
        .link_2_1('Full')
      endif
      .DoRTCalc(7,10,.f.)
        .w_CCNUMCOR = SPACE(15)
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_CCNUMCOR))
        .link_2_6('Full')
      endif
      .DoRTCalc(12,13,.f.)
        .w_NUMCOR = .w_CCNUMCOR
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_NUMCOR))
        .link_2_9('Full')
      endif
        .w_CCDATVAL = CALCFEST(.w_CCDATREG, .w_CALFIR, .w_CCNUMCOR, .w_CCCODCAU, .w_TIPCAU,1)
        .w_CCIMPCRE = 0
        .w_CCIMPDEB = 0
      .DoRTCalc(18,18,.f.)
        .w_CCCOMVAL = IIF(.w_CCIMPCRE<>0 or .w_CCIMPDEB<>0,VAL2VAL(.w_CCCOSCOM,.w_CCCAOVAL,.w_CCDATREG,I_DATSYS,GETCAM(g_PERVAL, I_DATSYS), .w_DECTOT),0)
        .w_CCFLCRED = IIF(.w_FLCRDE='C', '+', ' ')
        .w_CCFLDEBI = IIF(.w_FLCRDE='D', '+', ' ')
      .DoRTCalc(22,35,.f.)
        .w_CCFLCOMM = '+'
      .DoRTCalc(37,43,.f.)
      if not(empty(.w_CCNUMCOR))
        .link_2_22('Full')
      endif
    endwith
    this.DoRTCalc(44,49,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CCCODCAU = t_CCCODCAU
    this.w_CPROWORD = t_CPROWORD
    this.w_FLCOMP = t_FLCOMP
    this.w_FLCRDE = t_FLCRDE
    this.w_TIPCAU = t_TIPCAU
    this.w_CCNUMCOR = t_CCNUMCOR
    this.w_TIPCON = t_TIPCON
    this.w_CALFIR = t_CALFIR
    this.w_NUMCOR = t_NUMCOR
    this.w_CCDATVAL = t_CCDATVAL
    this.w_CCIMPCRE = t_CCIMPCRE
    this.w_CCIMPDEB = t_CCIMPDEB
    this.w_CCCOSCOM = t_CCCOSCOM
    this.w_CCCOMVAL = t_CCCOMVAL
    this.w_CCFLCRED = t_CCFLCRED
    this.w_CCFLDEBI = t_CCFLDEBI
    this.w_CCFLCOMM = t_CCFLCOMM
    this.w_DESCOR = t_DESCOR
    this.w_DESCAU = t_DESCAU
    this.w_CCNUMCOR = t_CCNUMCOR
    this.w_CCOBSO = t_CCOBSO
    this.w_BAVAL = t_BAVAL
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CCCODCAU with this.w_CCCODCAU
    replace t_CPROWORD with this.w_CPROWORD
    replace t_FLCOMP with this.w_FLCOMP
    replace t_FLCRDE with this.w_FLCRDE
    replace t_TIPCAU with this.w_TIPCAU
    replace t_CCNUMCOR with this.w_CCNUMCOR
    replace t_TIPCON with this.w_TIPCON
    replace t_CALFIR with this.w_CALFIR
    replace t_NUMCOR with this.w_NUMCOR
    replace t_CCDATVAL with this.w_CCDATVAL
    replace t_CCIMPCRE with this.w_CCIMPCRE
    replace t_CCIMPDEB with this.w_CCIMPDEB
    replace t_CCCOSCOM with this.w_CCCOSCOM
    replace t_CCCOMVAL with this.w_CCCOMVAL
    replace t_CCFLCRED with this.w_CCFLCRED
    replace t_CCFLDEBI with this.w_CCFLDEBI
    replace t_CCFLCOMM with this.w_CCFLCOMM
    replace t_DESCOR with this.w_DESCOR
    replace t_DESCAU with this.w_DESCAU
    replace t_CCNUMCOR with this.w_CCNUMCOR
    replace t_CCOBSO with this.w_CCOBSO
    replace t_BAVAL with this.w_BAVAL
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTCRE = .w_TOTCRE-.w_ccimpcre
        .w_TOTDEB = .w_TOTDEB-.w_ccimpdeb
        .w_TOTCOM = .w_TOTCOM-.w_cccomval
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsba_mmcPag1 as StdContainer
  Width  = 755
  height = 360
  stdWidth  = 755
  stdheight = 360
  resizeXpos=201
  resizeYpos=280
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCNUMREG_1_3 as StdField with uid="HRVVBYSKPX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCNUMREG", cQueryName = "CCNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione movimento",;
    HelpContextID = 190816621,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=101, Top=14, cSayPict='"999999"', cGetPict='"999999"'

  add object oCCDATREG_1_4 as StdField with uid="MVRUDMRIVY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CCDATREG", cQueryName = "CCNUMREG,CCDATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione movimento",;
    HelpContextID = 196804973,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=389, Top=14

  add object oCC__ANNO_1_5 as StdField with uid="JFMMNFKRUZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CC__ANNO", cQueryName = "CC__ANNO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di competenza",;
    HelpContextID = 156585611,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=178, Top=14, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4), tabstop=.f.

  add object oCCNUMDOC_1_6 as StdField with uid="ZZOKOSBWVE",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CCNUMDOC", cQueryName = "CCNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 44064407,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=101, Top=41, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oCCALFDOC_1_7 as StdField with uid="MVOWBYKFGV",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CCALFDOC", cQueryName = "CCALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 52047511,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=231, Top=41, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  add object oCCDATDOC_1_8 as StdField with uid="JTIWDWXUJY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CCDATDOC", cQueryName = "CCDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data documento superiore alla data registrazione",;
    ToolTipText = "Data documento",;
    HelpContextID = 38076055,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=389, Top=40

  func oCCDATDOC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CCDATREG>=.w_CCDATDOC)
    endwith
    return bRes
  endfunc

  add object oCCDESCRI_1_9 as StdField with uid="PHLXPQDNHH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione movimento",;
    HelpContextID = 212795759,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=101, Top=68, InputMask=replicate('X',50)

  add object oCCCODVAL_1_10 as StdField with uid="VOYENFCMWA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CCCODVAL", cQueryName = "CCCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta movimento",;
    HelpContextID = 248050034,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=101, Top=95, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_CCCODVAL"

  func oCCCODVAL_1_10.mCond()
    with this.Parent.oContained
      return (.cfunction='Load')
    endwith
  endfunc

  func oCCCODVAL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODVAL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCODVAL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCCCODVAL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc

  add object oSIMVAL_1_19 as StdField with uid="WFSESOTBLV",rtseq=32,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo valuta movimento",;
    HelpContextID = 190801882,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=153, Top=95, InputMask=replicate('X',5)

  add object oCCCAOVAL_1_20 as StdField with uid="DYYBHHRUKY",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CCCAOVAL", cQueryName = "CCCAOVAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio valuta movimento",;
    HelpContextID = 258666866,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=368, Top=95, cSayPict='"99999.999999"', cGetPict='"99999.999999"', tabstop=.f.

  func oCCCAOVAL_1_20.mCond()
    with this.Parent.oContained
      return (FASETRAN(.w_CCDATREG)=0 OR .w_CAOVAL=0)
    endwith
  endfunc

  func oCCCAOVAL_1_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL<>0)
    endwith
    endif
  endfunc


  add object oObj_1_33 as cp_runprogram with uid="TUYJRUMVHW",left=13, top=377, width=98,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBA_BDA",;
    cEvent = "w_CCDATREG Changed",;
    nPag=1;
    , HelpContextID = 248611046


  add object oObj_1_34 as cp_runprogram with uid="FOUXNNMZLC",left=121, top=377, width=98,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBA_BMK",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 248611046


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=125, width=747,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Riga",Field2="CCCODCAU",Label2="Causale",Field3="CCNUMCOR",Label3="Conto corrente",Field4="CCDATVAL",Label4="Data valuta",Field5="CCIMPCRE",Label5="Importo creditore",Field6="CCIMPDEB",Label6="Importo debitore",Field7="CCCOSCOM",Label7="Commissioni (Euro)",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118268794

  add object oStr_1_11 as StdString with uid="SITJOOGFSG",Visible=.t., Left=3, Top=14,;
    Alignment=1, Width=96, Height=18,;
    Caption="Numero reg.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="ZUBGBKBGUX",Visible=.t., Left=165, Top=14,;
    Alignment=2, Width=11, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="UOXDQGWSXP",Visible=.t., Left=327, Top=14,;
    Alignment=1, Width=58, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="EBWOONHISQ",Visible=.t., Left=291, Top=95,;
    Alignment=1, Width=75, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0)
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="JPTKIGDATZ",Visible=.t., Left=3, Top=41,;
    Alignment=1, Width=96, Height=18,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="WDWRLAXCJZ",Visible=.t., Left=327, Top=40,;
    Alignment=1, Width=58, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="KPQLSYSSRB",Visible=.t., Left=219, Top=41,;
    Alignment=2, Width=11, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="DAAUGGDSFZ",Visible=.t., Left=3, Top=68,;
    Alignment=1, Width=96, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="DJHNASOAAG",Visible=.t., Left=12, Top=95,;
    Alignment=1, Width=87, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=148,;
    width=743+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=149,width=742+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CCC_MAST|COC_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCOR_2_18.Refresh()
      this.Parent.oDESCAU_2_19.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CCC_MAST'
        oDropInto=this.oBodyCol.oRow.oCCCODCAU_2_1
      case cFile='COC_MAST'
        oDropInto=this.oBodyCol.oRow.oCCNUMCOR_2_6
    endcase
    return(oDropInto)
  EndFunc


  add object oDESCOR_2_18 as StdTrsField with uid="SJKHEEAHDJ",rtseq=37,rtrep=.t.,;
    cFormVar="w_DESCOR",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione conto corrente movimento",;
    HelpContextID = 76680394,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=112, Top=336, InputMask=replicate('X',35)

  add object oDESCAU_2_19 as StdTrsField with uid="KDJPBMGCXV",rtseq=38,rtrep=.t.,;
    cFormVar="w_DESCAU",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione causale movimento",;
    HelpContextID = 41028810,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAU",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=111, Top=310, InputMask=replicate('X',35)

  add object oStr_2_20 as StdString with uid="ZBNIBSALYQ",Visible=.t., Left=14, Top=310,;
    Alignment=1, Width=95, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="SNFPUMQYXK",Visible=.t., Left=3, Top=336,;
    Alignment=1, Width=106, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_4 as StdField with uid="DRYOIUYPUV",rtseq=42,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    ToolTipText = "Totale movimento",;
    HelpContextID = 29618634,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=536, Top=336, cSayPict=[v_PV(38+VVL)], cGetPict=[v_PV(38+VVL)]

  add object oStr_3_5 as StdString with uid="TBTAAUFHQN",Visible=.t., Left=389, Top=336,;
    Alignment=1, Width=144, Height=18,;
    Caption="Totale movimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

* --- Defining Body row
define class tgsba_mmcBodyRow as CPBodyRowCnt
  Width=733
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCCCODCAU_2_1 as StdTrsField with uid="NHZSVKXJYN",rtseq=6,rtrep=.t.,;
    cFormVar="w_CCCODCAU",value=space(5),;
    HelpContextID = 70717061,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=52, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="GSBA_MCT", oKey_1_1="CACODICE", oKey_1_2="this.w_CCCODCAU"

  func oCCCODCAU_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
      if .not. empty(.w_NUMCOR)
        bRes2=.link_2_9('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCCCODCAU_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCCCODCAU_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCCCODCAU_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MCT',"Causali conti correnti",'',this.parent.oContained
  endproc
  proc oCCCODCAU_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CCCODCAU
    i_obj.ecpSave()
  endproc

  add object oCPROWORD_2_2 as StdTrsField with uid="GIXFLAORRV",rtseq=7,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 150597226,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"], tabstop=.f.

  add object oCCNUMCOR_2_6 as StdTrsField with uid="AKOZJMOYVA",rtseq=11,rtrep=.t.,;
    cFormVar="w_CCNUMCOR",value=space(15),;
    ToolTipText = "Codice conto corrente da tabella conti banche",;
    HelpContextID = 60841608,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto corrente inesistente o incongruente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=119, Left=121, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CCNUMCOR"

  func oCCNUMCOR_2_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CCCODCAU))
    endwith
  endfunc

  func oCCNUMCOR_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCNUMCOR_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCCNUMCOR_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCCNUMCOR_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti correnti",'GSBA_QCM.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oCCNUMCOR_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CCNUMCOR
    i_obj.ecpSave()
  endproc

  add object oCCDATVAL_2_10 as StdTrsField with uid="BLOZTUTFHP",rtseq=15,rtrep=.t.,;
    cFormVar="w_CCDATVAL",value=ctod("  /  /  "),;
    HelpContextID = 263913842,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=253, Top=0

  func oCCDATVAL_2_10.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CCNUMCOR))
    endwith
  endfunc

  add object oCCIMPCRE_2_11 as StdTrsField with uid="RUGQKOPRSN",rtseq=16,rtrep=.t.,;
    cFormVar="w_CCIMPCRE",value=0,;
    HelpContextID = 210194795,;
    cTotal = "this.Parent.oContained.w_totcre", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=342, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  func oCCIMPCRE_2_11.mCond()
    with this.Parent.oContained
      return (.w_FLCRDE='C' AND NOT EMPTY(.w_CCNUMCOR))
    endwith
  endfunc

  add object oCCIMPDEB_2_12 as StdTrsField with uid="XEXHHRBGXX",rtseq=17,rtrep=.t.,;
    cFormVar="w_CCIMPDEB",value=0,;
    HelpContextID = 226972008,;
    cTotal = "this.Parent.oContained.w_totdeb", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=476, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  func oCCIMPDEB_2_12.mCond()
    with this.Parent.oContained
      return (.w_FLCRDE='D' AND NOT EMPTY(.w_CCNUMCOR))
    endwith
  endfunc

  add object oCCCOSCOM_2_13 as StdTrsField with uid="GULQNQBCGE",rtseq=18,rtrep=.t.,;
    cFormVar="w_CCCOSCOM",value=0,;
    HelpContextID = 54988429,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=116, Left=612, Top=-1, cSayPict=[v_PV(32+VVP)], cGetPict=[v_GV(32+VVP)]

  func oCCCOSCOM_2_13.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CCNUMCOR) AND .w_TIPCON=' ')
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCCCODCAU_2_1.When()
    return(.t.)
  proc oCCCODCAU_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCCCODCAU_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsba_mmc','CCM_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCSERIAL=CCM_MAST.CCSERIAL";
  +" and "+i_cAliasName2+".CCROWRIF=CCM_MAST.CCROWRIF";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
