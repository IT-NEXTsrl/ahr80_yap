* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_mct                                                        *
*              Causali conti correnti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_64]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-13                                                      *
* Last revis.: 2015-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsba_mct"))

* --- Class definition
define class tgsba_mct as StdTrsForm
  Top    = 8
  Left   = 8

  * --- Standard Properties
  Width  = 728
  Height = 308+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-07"
  HelpContextID=97158761
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CCC_MAST_IDX = 0
  CCC_DETT_IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  cFile = "CCC_MAST"
  cFileDetail = "CCC_DETT"
  cKeySelect = "CACODICE"
  cKeyWhere  = "CACODICE=this.w_CACODICE"
  cKeyDetail  = "CACODICE=this.w_CACODICE"
  cKeyWhereODBC = '"CACODICE="+cp_ToStrODBC(this.w_CACODICE)';

  cKeyDetailWhereODBC = '"CACODICE="+cp_ToStrODBC(this.w_CACODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CCC_DETT.CACODICE="+cp_ToStrODBC(this.w_CACODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CCC_DETT.CPROWNUM '
  cPrg = "gsba_mct"
  cComment = "Causali conti correnti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CACODICE = space(5)
  w_CADESCRI = space(35)
  w_CAFLCOMP = space(1)
  o_CAFLCOMP = space(1)
  w_CATIPCON = space(1)
  w_CAFLCRDE = space(1)
  w_CANUMCOR = space(15)
  w_DESCOR = space(35)
  w_CAGIOVAL = 0
  w_CAVALFIS = space(1)
  w_CAFLOPER = space(1)
  w_CAIMPCOM = 0
  w_CACOMRIS = space(1)
  w_CAADDCOS = space(1)
  w_CAFLDATC = space(1)
  w_CAGIOVA2 = 0
  w_CAVALFI2 = space(1)
  w_CAFLOPE2 = space(1)
  w_CODVAL = space(3)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_TIPCON = space(1)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CCC_MAST','gsba_mct')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsba_mctPag1","gsba_mct",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Causale conto corrente")
      .Pages(1).HelpContextID = 14700037
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CCC_MAST'
    this.cWorkTables[4]='CCC_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CCC_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CCC_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CACODICE = NVL(CACODICE,space(5))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CCC_MAST where CACODICE=KeySet.CACODICE
    *
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2],this.bLoadRecFilter,this.CCC_MAST_IDX,"gsba_mct")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CCC_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CCC_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CCC_DETT.","CCC_MAST.")
      i_cTable = i_cTable+' CCC_MAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DECTOT = 0
        .w_CACODICE = NVL(CACODICE,space(5))
        .w_CADESCRI = NVL(CADESCRI,space(35))
        .w_CAFLCOMP = NVL(CAFLCOMP,space(1))
        .w_CATIPCON = NVL(CATIPCON,space(1))
        .w_CAFLCRDE = NVL(CAFLCRDE,space(1))
        .w_CAGIOVA2 = NVL(CAGIOVA2,0)
        .w_CAVALFI2 = NVL(CAVALFI2,space(1))
        .w_CAFLOPE2 = NVL(CAFLOPE2,space(1))
        .w_CODVAL = g_CODEUR
          .link_1_12('Load')
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        cp_LoadRecExtFlds(this,'CCC_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CCC_DETT where CACODICE=KeySet.CACODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CCC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CCC_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CCC_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CCC_DETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
        select * from (i_cTable) CCC_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCOR = space(35)
          .w_TIPCON = space(1)
          .w_CPROWNUM = CPROWNUM
          .w_CANUMCOR = NVL(CANUMCOR,space(15))
          if link_2_1_joined
            this.w_CANUMCOR = NVL(BACODBAN201,NVL(this.w_CANUMCOR,space(15)))
            this.w_DESCOR = NVL(BADESCRI201,space(35))
            this.w_TIPCON = NVL(BATIPCON201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_CAGIOVAL = NVL(CAGIOVAL,0)
          .w_CAVALFIS = NVL(CAVALFIS,space(1))
          .w_CAFLOPER = NVL(CAFLOPER,space(1))
          .w_CAIMPCOM = NVL(CAIMPCOM,0)
          .w_CACOMRIS = NVL(CACOMRIS,space(1))
          .w_CAADDCOS = NVL(CAADDCOS,space(1))
          .w_CAFLDATC = NVL(CAFLDATC,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CODVAL = g_CODEUR
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CACODICE=space(5)
      .w_CADESCRI=space(35)
      .w_CAFLCOMP=space(1)
      .w_CATIPCON=space(1)
      .w_CAFLCRDE=space(1)
      .w_CANUMCOR=space(15)
      .w_DESCOR=space(35)
      .w_CAGIOVAL=0
      .w_CAVALFIS=space(1)
      .w_CAFLOPER=space(1)
      .w_CAIMPCOM=0
      .w_CACOMRIS=space(1)
      .w_CAADDCOS=space(1)
      .w_CAFLDATC=space(1)
      .w_CAGIOVA2=0
      .w_CAVALFI2=space(1)
      .w_CAFLOPE2=space(1)
      .w_CODVAL=space(3)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_TIPCON=space(1)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_CAFLCOMP = 'N'
        .DoRTCalc(4,4,.f.)
        .w_CAFLCRDE = IIF(.w_CAFLCOMP='S', 'D', 'C')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CANUMCOR))
         .link_2_1('Full')
        endif
        .DoRTCalc(7,13,.f.)
        .w_CAFLDATC = 'V'
        .DoRTCalc(15,17,.f.)
        .w_CODVAL = g_CODEUR
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODVAL))
         .link_1_12('Full')
        endif
        .DoRTCalc(19,19,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CCC_MAST')
    this.DoRTCalc(21,25,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCADESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCAFLCOMP_1_4.enabled = i_bVal
      .Page1.oPag.oCATIPCON_1_5.enabled = i_bVal
      .Page1.oPag.oCAFLCRDE_1_6.enabled = i_bVal
      .Page1.oPag.oCAIMPCOM_2_6.enabled = i_bVal
      .Page1.oPag.oCACOMRIS_2_7.enabled = i_bVal
      .Page1.oPag.oCAADDCOS_2_8.enabled = i_bVal
      .Page1.oPag.oCAFLDATC_2_9.enabled = i_bVal
      .Page1.oPag.oCAGIOVA2_1_7.enabled = i_bVal
      .Page1.oPag.oCAVALFI2_1_8.enabled = i_bVal
      .Page1.oPag.oCAFLOPE2_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_16.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCACODICE_1_1.enabled = .t.
        .Page1.oPag.oCADESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CCC_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODICE,"CACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESCRI,"CADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLCOMP,"CAFLCOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPCON,"CATIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLCRDE,"CAFLCRDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAGIOVA2,"CAGIOVA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAVALFI2,"CAVALFI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLOPE2,"CAFLOPE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    i_lTable = "CCC_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CCC_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSBA_SCM with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CANUMCOR C(15);
      ,t_DESCOR C(35);
      ,t_CAGIOVAL N(3);
      ,t_CAVALFIS N(3);
      ,t_CAFLOPER N(3);
      ,t_CAIMPCOM N(12,4);
      ,t_CACOMRIS N(3);
      ,t_CAADDCOS N(3);
      ,t_CAFLDATC N(3);
      ,CPROWNUM N(10);
      ,t_TIPCON C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsba_mctbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCANUMCOR_2_1.controlsource=this.cTrsName+'.t_CANUMCOR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOR_2_2.controlsource=this.cTrsName+'.t_DESCOR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAGIOVAL_2_3.controlsource=this.cTrsName+'.t_CAGIOVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALFIS_2_4.controlsource=this.cTrsName+'.t_CAVALFIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLOPER_2_5.controlsource=this.cTrsName+'.t_CAFLOPER'
    this.oPgFRm.Page1.oPag.oCAIMPCOM_2_6.controlsource=this.cTrsName+'.t_CAIMPCOM'
    this.oPgFRm.Page1.oPag.oCACOMRIS_2_7.controlsource=this.cTrsName+'.t_CACOMRIS'
    this.oPgFRm.Page1.oPag.oCAADDCOS_2_8.controlsource=this.cTrsName+'.t_CAADDCOS'
    this.oPgFRm.Page1.oPag.oCAFLDATC_2_9.controlsource=this.cTrsName+'.t_CAFLDATC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(127)
    this.AddVLine(368)
    this.AddVLine(412)
    this.AddVLine(471)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCANUMCOR_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CCC_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CCC_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'CCC_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(CACODICE,CADESCRI,CAFLCOMP,CATIPCON,CAFLCRDE"+;
                  ",CAGIOVA2,CAVALFI2,CAFLOPE2,UTCC,UTDC"+;
                  ",UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CACODICE)+;
                    ","+cp_ToStrODBC(this.w_CADESCRI)+;
                    ","+cp_ToStrODBC(this.w_CAFLCOMP)+;
                    ","+cp_ToStrODBC(this.w_CATIPCON)+;
                    ","+cp_ToStrODBC(this.w_CAFLCRDE)+;
                    ","+cp_ToStrODBC(this.w_CAGIOVA2)+;
                    ","+cp_ToStrODBC(this.w_CAVALFI2)+;
                    ","+cp_ToStrODBC(this.w_CAFLOPE2)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CCC_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'CCC_MAST')
        cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CACODICE)
        INSERT INTO (i_cTable);
              (CACODICE,CADESCRI,CAFLCOMP,CATIPCON,CAFLCRDE,CAGIOVA2,CAVALFI2,CAFLOPE2,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CACODICE;
                  ,this.w_CADESCRI;
                  ,this.w_CAFLCOMP;
                  ,this.w_CATIPCON;
                  ,this.w_CAFLCRDE;
                  ,this.w_CAGIOVA2;
                  ,this.w_CAVALFI2;
                  ,this.w_CAFLOPE2;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2])
      *
      * insert into CCC_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CACODICE,CANUMCOR,CAGIOVAL,CAVALFIS,CAFLOPER"+;
                  ",CAIMPCOM,CACOMRIS,CAADDCOS,CAFLDATC,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CACODICE)+","+cp_ToStrODBCNull(this.w_CANUMCOR)+","+cp_ToStrODBC(this.w_CAGIOVAL)+","+cp_ToStrODBC(this.w_CAVALFIS)+","+cp_ToStrODBC(this.w_CAFLOPER)+;
             ","+cp_ToStrODBC(this.w_CAIMPCOM)+","+cp_ToStrODBC(this.w_CACOMRIS)+","+cp_ToStrODBC(this.w_CAADDCOS)+","+cp_ToStrODBC(this.w_CAFLDATC)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CACODICE',this.w_CACODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CACODICE,this.w_CANUMCOR,this.w_CAGIOVAL,this.w_CAVALFIS,this.w_CAFLOPER"+;
                ",this.w_CAIMPCOM,this.w_CACOMRIS,this.w_CAADDCOS,this.w_CAFLDATC,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CCC_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CCC_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CADESCRI="+cp_ToStrODBC(this.w_CADESCRI)+;
             ",CAFLCOMP="+cp_ToStrODBC(this.w_CAFLCOMP)+;
             ",CATIPCON="+cp_ToStrODBC(this.w_CATIPCON)+;
             ",CAFLCRDE="+cp_ToStrODBC(this.w_CAFLCRDE)+;
             ",CAGIOVA2="+cp_ToStrODBC(this.w_CAGIOVA2)+;
             ",CAVALFI2="+cp_ToStrODBC(this.w_CAVALFI2)+;
             ",CAFLOPE2="+cp_ToStrODBC(this.w_CAFLOPE2)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CCC_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
          UPDATE (i_cTable) SET;
              CADESCRI=this.w_CADESCRI;
             ,CAFLCOMP=this.w_CAFLCOMP;
             ,CATIPCON=this.w_CATIPCON;
             ,CAFLCRDE=this.w_CAFLCRDE;
             ,CAGIOVA2=this.w_CAGIOVA2;
             ,CAVALFI2=this.w_CAVALFI2;
             ,CAFLOPE2=this.w_CAFLOPE2;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not Empty(t_CANUMCOR)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CCC_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CCC_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CCC_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CANUMCOR="+cp_ToStrODBCNull(this.w_CANUMCOR)+;
                     ",CAGIOVAL="+cp_ToStrODBC(this.w_CAGIOVAL)+;
                     ",CAVALFIS="+cp_ToStrODBC(this.w_CAVALFIS)+;
                     ",CAFLOPER="+cp_ToStrODBC(this.w_CAFLOPER)+;
                     ",CAIMPCOM="+cp_ToStrODBC(this.w_CAIMPCOM)+;
                     ",CACOMRIS="+cp_ToStrODBC(this.w_CACOMRIS)+;
                     ",CAADDCOS="+cp_ToStrODBC(this.w_CAADDCOS)+;
                     ",CAFLDATC="+cp_ToStrODBC(this.w_CAFLDATC)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CANUMCOR=this.w_CANUMCOR;
                     ,CAGIOVAL=this.w_CAGIOVAL;
                     ,CAVALFIS=this.w_CAVALFIS;
                     ,CAFLOPER=this.w_CAFLOPER;
                     ,CAIMPCOM=this.w_CAIMPCOM;
                     ,CACOMRIS=this.w_CACOMRIS;
                     ,CAADDCOS=this.w_CAADDCOS;
                     ,CAFLDATC=this.w_CAFLDATC;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsba_mct
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * --- Riposiziona sul Primo record del Temporaneo dei Documenti
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        this.SaveDependsOn()
    
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not Empty(t_CANUMCOR)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CCC_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2])
        *
        * delete CCC_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
        *
        * delete CCC_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not Empty(t_CANUMCOR)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CAFLCOMP<>.w_CAFLCOMP
          .w_CAFLCRDE = IIF(.w_CAFLCOMP='S', 'D', 'C')
        endif
        .DoRTCalc(6,17,.t.)
          .w_CODVAL = g_CODEUR
          .link_1_12('Full')
        .DoRTCalc(19,19,.t.)
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TIPCON with this.w_TIPCON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCATIPCON_1_5.enabled = this.oPgFrm.Page1.oPag.oCATIPCON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCAFLCRDE_1_6.enabled = this.oPgFrm.Page1.oPag.oCAFLCRDE_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCAGIOVA2_1_7.enabled = this.oPgFrm.Page1.oPag.oCAGIOVA2_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCAVALFI2_1_8.enabled = this.oPgFrm.Page1.oPag.oCAVALFI2_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCAFLOPE2_1_9.enabled = this.oPgFrm.Page1.oPag.oCAFLOPE2_1_9.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAGIOVAL_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAGIOVAL_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAVALFIS_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAVALFIS_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAFLOPER_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAFLOPER_2_5.mCond()
    this.oPgFrm.Page1.oPag.oCAIMPCOM_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCAIMPCOM_2_6.mCond()
    this.oPgFrm.Page1.oPag.oCACOMRIS_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCACOMRIS_2_7.mCond()
    this.oPgFrm.Page1.oPag.oCAADDCOS_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCAADDCOS_2_8.mCond()
    this.oPgFrm.Page1.oPag.oCAFLDATC_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCAFLDATC_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAGIOVA2_1_7.visible=!this.oPgFrm.Page1.oPag.oCAGIOVA2_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCAVALFI2_1_8.visible=!this.oPgFrm.Page1.oPag.oCAVALFI2_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCAFLOPE2_1_9.visible=!this.oPgFrm.Page1.oPag.oCAFLOPE2_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CANUMCOR
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CANUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CANUMCOR)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BATIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CANUMCOR))
          select BACODBAN,BADESCRI,BATIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CANUMCOR)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CANUMCOR) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCANUMCOR_2_1'),i_cWhere,'GSTE_ACB',"Conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BATIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CANUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BATIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CANUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CANUMCOR)
            select BACODBAN,BADESCRI,BATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CANUMCOR = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCOR = NVL(_Link_.BADESCRI,space(35))
      this.w_TIPCON = NVL(_Link_.BATIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CANUMCOR = space(15)
      endif
      this.w_DESCOR = space(35)
      this.w_TIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATIPCON<>'S' AND .w_TIPCON<>'S'  AND .w_CAFLCOMP='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo conto incongruente")
        endif
        this.w_CANUMCOR = space(15)
        this.w_DESCOR = space(35)
        this.w_TIPCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CANUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.BACODBAN as BACODBAN201"+ ",link_2_1.BADESCRI as BADESCRI201"+ ",link_2_1.BATIPCON as BATIPCON201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on CCC_DETT.CANUMCOR=link_2_1.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and CCC_DETT.CANUMCOR=link_2_1.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCACODICE_1_1.value==this.w_CACODICE)
      this.oPgFrm.Page1.oPag.oCACODICE_1_1.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_1_3.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_1_3.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLCOMP_1_4.RadioValue()==this.w_CAFLCOMP)
      this.oPgFrm.Page1.oPag.oCAFLCOMP_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPCON_1_5.RadioValue()==this.w_CATIPCON)
      this.oPgFrm.Page1.oPag.oCATIPCON_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLCRDE_1_6.RadioValue()==this.w_CAFLCRDE)
      this.oPgFrm.Page1.oPag.oCAFLCRDE_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAIMPCOM_2_6.value==this.w_CAIMPCOM)
      this.oPgFrm.Page1.oPag.oCAIMPCOM_2_6.value=this.w_CAIMPCOM
      replace t_CAIMPCOM with this.oPgFrm.Page1.oPag.oCAIMPCOM_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCACOMRIS_2_7.RadioValue()==this.w_CACOMRIS)
      this.oPgFrm.Page1.oPag.oCACOMRIS_2_7.SetRadio()
      replace t_CACOMRIS with this.oPgFrm.Page1.oPag.oCACOMRIS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCAADDCOS_2_8.RadioValue()==this.w_CAADDCOS)
      this.oPgFrm.Page1.oPag.oCAADDCOS_2_8.SetRadio()
      replace t_CAADDCOS with this.oPgFrm.Page1.oPag.oCAADDCOS_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLDATC_2_9.RadioValue()==this.w_CAFLDATC)
      this.oPgFrm.Page1.oPag.oCAFLDATC_2_9.SetRadio()
      replace t_CAFLDATC with this.oPgFrm.Page1.oPag.oCAFLDATC_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCAGIOVA2_1_7.value==this.w_CAGIOVA2)
      this.oPgFrm.Page1.oPag.oCAGIOVA2_1_7.value=this.w_CAGIOVA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCAVALFI2_1_8.RadioValue()==this.w_CAVALFI2)
      this.oPgFrm.Page1.oPag.oCAVALFI2_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLOPE2_1_9.RadioValue()==this.w_CAFLOPE2)
      this.oPgFrm.Page1.oPag.oCAFLOPE2_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCANUMCOR_2_1.value==this.w_CANUMCOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCANUMCOR_2_1.value=this.w_CANUMCOR
      replace t_CANUMCOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCANUMCOR_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOR_2_2.value==this.w_DESCOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOR_2_2.value=this.w_DESCOR
      replace t_DESCOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOR_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAGIOVAL_2_3.value==this.w_CAGIOVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAGIOVAL_2_3.value=this.w_CAGIOVAL
      replace t_CAGIOVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAGIOVAL_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALFIS_2_4.RadioValue()==this.w_CAVALFIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALFIS_2_4.SetRadio()
      replace t_CAVALFIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALFIS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLOPER_2_5.RadioValue()==this.w_CAFLOPER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLOPER_2_5.SetRadio()
      replace t_CAFLOPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLOPER_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'CCC_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CAFLCOMP))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCAFLCOMP_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CAFLCOMP)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_CATIPCON<>'S' AND .w_TIPCON<>'S'  AND .w_CAFLCOMP='N') and not(empty(.w_CANUMCOR)) and (not Empty(.w_CANUMCOR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCANUMCOR_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Tipo conto incongruente")
      endcase
      if not Empty(.w_CANUMCOR)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CAFLCOMP = this.w_CAFLCOMP
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not Empty(t_CANUMCOR))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CANUMCOR=space(15)
      .w_DESCOR=space(35)
      .w_CAGIOVAL=0
      .w_CAVALFIS=space(1)
      .w_CAFLOPER=space(1)
      .w_CAIMPCOM=0
      .w_CACOMRIS=space(1)
      .w_CAADDCOS=space(1)
      .w_CAFLDATC=space(1)
      .w_TIPCON=space(1)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_CANUMCOR))
        .link_2_1('Full')
      endif
      .DoRTCalc(7,13,.f.)
        .w_CAFLDATC = 'V'
    endwith
    this.DoRTCalc(15,25,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CANUMCOR = t_CANUMCOR
    this.w_DESCOR = t_DESCOR
    this.w_CAGIOVAL = t_CAGIOVAL
    this.w_CAVALFIS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALFIS_2_4.RadioValue(.t.)
    this.w_CAFLOPER = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLOPER_2_5.RadioValue(.t.)
    this.w_CAIMPCOM = t_CAIMPCOM
    this.w_CACOMRIS = this.oPgFrm.Page1.oPag.oCACOMRIS_2_7.RadioValue(.t.)
    this.w_CAADDCOS = this.oPgFrm.Page1.oPag.oCAADDCOS_2_8.RadioValue(.t.)
    this.w_CAFLDATC = this.oPgFrm.Page1.oPag.oCAFLDATC_2_9.RadioValue(.t.)
    this.w_TIPCON = t_TIPCON
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CANUMCOR with this.w_CANUMCOR
    replace t_DESCOR with this.w_DESCOR
    replace t_CAGIOVAL with this.w_CAGIOVAL
    replace t_CAVALFIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALFIS_2_4.ToRadio()
    replace t_CAFLOPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLOPER_2_5.ToRadio()
    replace t_CAIMPCOM with this.w_CAIMPCOM
    replace t_CACOMRIS with this.oPgFrm.Page1.oPag.oCACOMRIS_2_7.ToRadio()
    replace t_CAADDCOS with this.oPgFrm.Page1.oPag.oCAADDCOS_2_8.ToRadio()
    replace t_CAFLDATC with this.oPgFrm.Page1.oPag.oCAFLDATC_2_9.ToRadio()
    replace t_TIPCON with this.w_TIPCON
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsba_mctPag1 as StdContainer
  Width  = 724
  height = 308
  stdWidth  = 724
  stdheight = 308
  resizeXpos=243
  resizeYpos=251
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODICE_1_1 as StdField with uid="QZIHLAJEJN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale",;
    HelpContextID = 130609003,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=116, Top=10, InputMask=replicate('X',5)

  add object oCADESCRI_1_3 as StdField with uid="SWAQSCEIFO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale movimento",;
    HelpContextID = 45023087,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=181, Top=10, InputMask=replicate('X',35)


  add object oCAFLCOMP_1_4 as StdCombo with uid="MNGFKHDHDR",rtseq=3,rtrep=.f.,left=557,top=10,width=127,height=21;
    , ToolTipText = "Se attivo: la causale viene utilizzata per il calcolo delle competenze";
    , HelpContextID = 38396042;
    , cFormVar="w_CAFLCOMP",RowSource=""+"Normale,"+"Aggiorna interessi,"+"Aggiorna spese", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCAFLCOMP_1_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLCOMP,&i_cF..t_CAFLCOMP),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'I',;
    iif(xVal =3,'S',;
    ' '))))
  endfunc
  func oCAFLCOMP_1_4.GetRadio()
    this.Parent.oContained.w_CAFLCOMP = this.RadioValue()
    return .t.
  endfunc

  func oCAFLCOMP_1_4.ToRadio()
    this.Parent.oContained.w_CAFLCOMP=trim(this.Parent.oContained.w_CAFLCOMP)
    return(;
      iif(this.Parent.oContained.w_CAFLCOMP=='N',1,;
      iif(this.Parent.oContained.w_CAFLCOMP=='I',2,;
      iif(this.Parent.oContained.w_CAFLCOMP=='S',3,;
      0))))
  endfunc

  func oCAFLCOMP_1_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCATIPCON_1_5 as StdCheck with uid="GLINNZXSQZ",rtseq=4,rtrep=.f.,left=116, top=36, caption="Causale conti compensazione",;
    ToolTipText = "Se attivo: la causale � relativa a conti di compensazione",;
    HelpContextID = 226230412,;
    cFormVar="w_CATIPCON", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCATIPCON_1_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CATIPCON,&i_cF..t_CATIPCON),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCATIPCON_1_5.GetRadio()
    this.Parent.oContained.w_CATIPCON = this.RadioValue()
    return .t.
  endfunc

  func oCATIPCON_1_5.ToRadio()
    this.Parent.oContained.w_CATIPCON=trim(this.Parent.oContained.w_CATIPCON)
    return(;
      iif(this.Parent.oContained.w_CATIPCON=='S',1,;
      0))
  endfunc

  func oCATIPCON_1_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCATIPCON_1_5.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc


  add object oCAFLCRDE_1_6 as StdCombo with uid="ZKLZARABMS",rtseq=5,rtrep=.f.,left=116,top=67,width=127,height=21;
    , ToolTipText = "Tipo operazione C=credito, D=debito";
    , HelpContextID = 11935595;
    , cFormVar="w_CAFLCRDE",RowSource=""+"Credito,"+"Debito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAFLCRDE_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLCRDE,&i_cF..t_CAFLCRDE),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'D',;
    space(1))))
  endfunc
  func oCAFLCRDE_1_6.GetRadio()
    this.Parent.oContained.w_CAFLCRDE = this.RadioValue()
    return .t.
  endfunc

  func oCAFLCRDE_1_6.ToRadio()
    this.Parent.oContained.w_CAFLCRDE=trim(this.Parent.oContained.w_CAFLCRDE)
    return(;
      iif(this.Parent.oContained.w_CAFLCRDE=='C',1,;
      iif(this.Parent.oContained.w_CAFLCRDE=='D',2,;
      0)))
  endfunc

  func oCAFLCRDE_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLCRDE_1_6.mCond()
    with this.Parent.oContained
      return (.w_CAFLCOMP<>'S')
    endwith
  endfunc

  add object oCAGIOVA2_1_7 as StdField with uid="VIKQAHGGHE",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CAGIOVA2", cQueryName = "CAGIOVA2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero giorni da aggiungere/togliere alla data dell'operazione per il calcolo della data valuta",;
    HelpContextID = 91434840,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=318, Top=66, cSayPict='"999"', cGetPict='"999"'

  func oCAGIOVA2_1_7.mCond()
    with this.Parent.oContained
      return (.w_CATIPCON='S' AND .w_CAFLCOMP='N')
    endwith
  endfunc

  func oCAGIOVA2_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATIPCON<>'S' OR .w_CAFLCOMP<>'N')
    endwith
    endif
  endfunc

  add object oCAVALFI2_1_8 as StdCheck with uid="IMEWTEGQVL",rtseq=16,rtrep=.f.,left=363, top=65, caption="Valuta fissa",;
    ToolTipText = "Se attivo: la data valuta pu� cadere in un giorno festivo",;
    HelpContextID = 180609192,;
    cFormVar="w_CAVALFI2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAVALFI2_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAVALFI2,&i_cF..t_CAVALFI2),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCAVALFI2_1_8.GetRadio()
    this.Parent.oContained.w_CAVALFI2 = this.RadioValue()
    return .t.
  endfunc

  func oCAVALFI2_1_8.ToRadio()
    this.Parent.oContained.w_CAVALFI2=trim(this.Parent.oContained.w_CAVALFI2)
    return(;
      iif(this.Parent.oContained.w_CAVALFI2=='S',1,;
      0))
  endfunc

  func oCAVALFI2_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAVALFI2_1_8.mCond()
    with this.Parent.oContained
      return (.w_CATIPCON='S' AND .w_CAFLCOMP='N')
    endwith
  endfunc

  func oCAVALFI2_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATIPCON<>'S' OR .w_CAFLCOMP<>'N')
    endwith
    endif
  endfunc

  add object oCAFLOPE2_1_9 as StdCheck with uid="CHMHCBDHFA",rtseq=17,rtrep=.f.,left=497, top=65, caption="Costo operazione addebitato",;
    ToolTipText = "Se attivo: verr� generato un costo di addebito per l'operazione",;
    HelpContextID = 259399512,;
    cFormVar="w_CAFLOPE2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLOPE2_1_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLOPE2,&i_cF..t_CAFLOPE2),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCAFLOPE2_1_9.GetRadio()
    this.Parent.oContained.w_CAFLOPE2 = this.RadioValue()
    return .t.
  endfunc

  func oCAFLOPE2_1_9.ToRadio()
    this.Parent.oContained.w_CAFLOPE2=trim(this.Parent.oContained.w_CAFLOPE2)
    return(;
      iif(this.Parent.oContained.w_CAFLOPE2=='S',1,;
      0))
  endfunc

  func oCAFLOPE2_1_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLOPE2_1_9.mCond()
    with this.Parent.oContained
      return (.w_CATIPCON='S' AND .w_CAFLCOMP='N')
    endwith
  endfunc

  func oCAFLOPE2_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATIPCON<>'S' OR .w_CAFLCOMP<>'N')
    endwith
    endif
  endfunc


  add object oObj_1_16 as cp_runprogram with uid="KBRRZIFHKW",left=491, top=331, width=146,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBA_BCK",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 80838886


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=94, width=529,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CANUMCOR",Label1="Conto corrente",Field2="DESCOR",Label2="Descrizione",Field3="CAGIOVAL",Label3="Gg.valuta",Field4="CAVALFIS",Label4="Val.fissa",Field5="CAFLOPER",Label5="Costo oper.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218932090

  add object oStr_1_2 as StdString with uid="DFXFLRIGKM",Visible=.t., Left=26, Top=10,;
    Alignment=1, Width=86, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="COKTOWQPIH",Visible=.t., Left=3, Top=70,;
    Alignment=1, Width=109, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="CJWJMWCWPJ",Visible=.t., Left=247, Top=70,;
    Alignment=1, Width=67, Height=18,;
    Caption="Gg.valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_CATIPCON<>'S' OR .w_CAFLCOMP<>'N')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="CYZDAVTBFD",Visible=.t., Left=460, Top=10,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tipo causale:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=115,;
    width=527+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=116,width=526+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COC_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCAIMPCOM_2_6.Refresh()
      this.Parent.oCACOMRIS_2_7.Refresh()
      this.Parent.oCAADDCOS_2_8.Refresh()
      this.Parent.oCAFLDATC_2_9.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COC_MAST'
        oDropInto=this.oBodyCol.oRow.oCANUMCOR_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCAIMPCOM_2_6 as StdTrsField with uid="UABBXBWPIX",rtseq=11,rtrep=.t.,;
    cFormVar="w_CAIMPCOM",value=0,;
    ToolTipText = "Importo spese commissioni (in Euro)",;
    HelpContextID = 226013325,;
    cTotal="", bFixedPos=.t., cQueryName = "CAIMPCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=548, Top=140, cSayPict=[v_PV(32+VVL)], cGetPict=[v_GV(32+VVL)]

  func oCAIMPCOM_2_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CANUMCOR))
    endwith
  endfunc

  add object oCACOMRIS_2_7 as StdTrsCheck with uid="JYVPKLJWKC",rtrep=.t.,;
    cFormVar="w_CACOMRIS",  caption="Su riga separata",;
    ToolTipText = "Se attivo: le commissioni verranno evidenziate su riga separate nell'estratto conto",;
    HelpContextID = 245829767,;
    Left=548, Top=170,;
    cTotal="", cQueryName = "CACOMRIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCACOMRIS_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CACOMRIS,&i_cF..t_CACOMRIS),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCACOMRIS_2_7.GetRadio()
    this.Parent.oContained.w_CACOMRIS = this.RadioValue()
    return .t.
  endfunc

  func oCACOMRIS_2_7.ToRadio()
    this.Parent.oContained.w_CACOMRIS=trim(this.Parent.oContained.w_CACOMRIS)
    return(;
      iif(this.Parent.oContained.w_CACOMRIS=='S',1,;
      0))
  endfunc

  func oCACOMRIS_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCACOMRIS_2_7.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CANUMCOR))
    endwith
  endfunc

  add object oCAADDCOS_2_8 as StdTrsCheck with uid="PGNOHGQSHF",rtrep=.t.,;
    cFormVar="w_CAADDCOS",  caption="Addeb.costo operazione",;
    ToolTipText = "Se attivo: le commissioni su riga separata genereranno un addebito per l'operazione",;
    HelpContextID = 239218823,;
    Left=548, Top=200,;
    cTotal="", cQueryName = "CAADDCOS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCAADDCOS_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAADDCOS,&i_cF..t_CAADDCOS),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCAADDCOS_2_8.GetRadio()
    this.Parent.oContained.w_CAADDCOS = this.RadioValue()
    return .t.
  endfunc

  func oCAADDCOS_2_8.ToRadio()
    this.Parent.oContained.w_CAADDCOS=trim(this.Parent.oContained.w_CAADDCOS)
    return(;
      iif(this.Parent.oContained.w_CAADDCOS=='S',1,;
      0))
  endfunc

  func oCAADDCOS_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAADDCOS_2_8.mCond()
    with this.Parent.oContained
      return (.w_CACOMRIS='S')
    endwith
  endfunc

  add object oCAFLDATC_2_9 as StdTrsCombo with uid="RHKTYZECDR",rtrep=.t.,;
    cFormVar="w_CAFLDATC", RowSource=""+"Valuta operazione,"+"Data operazione" , ;
    ToolTipText = "Metodo di calcolo data valuta delle commissioni su riga separata",;
    HelpContextID = 264642409,;
    Height=25, Width=146, Left=548, Top=250,;
    cTotal="", cQueryName = "CAFLDATC",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCAFLDATC_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLDATC,&i_cF..t_CAFLDATC),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'D',;
    space(1))))
  endfunc
  func oCAFLDATC_2_9.GetRadio()
    this.Parent.oContained.w_CAFLDATC = this.RadioValue()
    return .t.
  endfunc

  func oCAFLDATC_2_9.ToRadio()
    this.Parent.oContained.w_CAFLDATC=trim(this.Parent.oContained.w_CAFLDATC)
    return(;
      iif(this.Parent.oContained.w_CAFLDATC=='V',1,;
      iif(this.Parent.oContained.w_CAFLDATC=='D',2,;
      0)))
  endfunc

  func oCAFLDATC_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLDATC_2_9.mCond()
    with this.Parent.oContained
      return (.w_CACOMRIS='S')
    endwith
  endfunc

  add object oStr_2_10 as StdString with uid="WZLURDGYKK",Visible=.t., Left=543, Top=96,;
    Alignment=2, Width=153, Height=18,;
    Caption="Commissioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_11 as StdString with uid="WYCGHZQDIQ",Visible=.t., Left=548, Top=230,;
    Alignment=0, Width=97, Height=18,;
    Caption="Data valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="ROOMHZBMTS",Visible=.t., Left=548, Top=121,;
    Alignment=0, Width=63, Height=18,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oBox_2_14 as StdBox with uid="SKOLZRFBPU",left=539, top=94, width=174,height=211

  add object oBox_2_15 as StdBox with uid="ZSRVOPMCAL",left=540, top=112, width=172,height=1

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsba_mctBodyRow as CPBodyRowCnt
  Width=517
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCANUMCOR_2_1 as StdTrsField with uid="KKINKFHCCE",rtseq=6,rtrep=.t.,;
    cFormVar="w_CANUMCOR",value=space(15),;
    ToolTipText = "Codice conto corrente",;
    HelpContextID = 228614280,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Tipo conto incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CANUMCOR"

  func oCANUMCOR_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCANUMCOR_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCANUMCOR_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCANUMCOR_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti correnti",'',this.parent.oContained
  endproc
  proc oCANUMCOR_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CANUMCOR
    i_obj.ecpSave()
  endproc

  add object oDESCOR_2_2 as StdTrsField with uid="DSSIOVGWLQ",rtseq=7,rtrep=.t.,;
    cFormVar="w_DESCOR",value=space(35),enabled=.f.,;
    ToolTipText = "escrizione conto corrente",;
    HelpContextID = 23982902,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=237, Left=120, Top=0, InputMask=replicate('X',35)

  add object oCAGIOVAL_2_3 as StdTrsField with uid="HSQJDKLWDQ",rtseq=8,rtrep=.t.,;
    cFormVar="w_CAGIOVAL",value=0,;
    ToolTipText = "Numero giorni da aggiungere/togliere alla data dell'operazione per il calcolo della data valuta",;
    HelpContextID = 91434866,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=362, Top=0, cSayPict=["999"], cGetPict=["999"]

  func oCAGIOVAL_2_3.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CANUMCOR))
    endwith
  endfunc

  add object oCAVALFIS_2_4 as StdTrsCheck with uid="DTQMVOVZUT",rtrep=.t.,;
    cFormVar="w_CAVALFIS",  caption="",;
    ToolTipText = "Calcolo valuta fissa",;
    HelpContextID = 180609159,;
    Left=410, Top=0, Width=49,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.f.;
   , bGlobalFont=.t.


  func oCAVALFIS_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAVALFIS,&i_cF..t_CAVALFIS),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCAVALFIS_2_4.GetRadio()
    this.Parent.oContained.w_CAVALFIS = this.RadioValue()
    return .t.
  endfunc

  func oCAVALFIS_2_4.ToRadio()
    this.Parent.oContained.w_CAVALFIS=trim(this.Parent.oContained.w_CAVALFIS)
    return(;
      iif(this.Parent.oContained.w_CAVALFIS=='S',1,;
      0))
  endfunc

  func oCAVALFIS_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAVALFIS_2_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CANUMCOR))
    endwith
  endfunc

  add object oCAFLOPER_2_5 as StdTrsCheck with uid="FGGFFLCLZO",rtrep=.t.,;
    cFormVar="w_CAFLOPER",  caption="",;
    ToolTipText = "Costo operazione addebitato",;
    HelpContextID = 259399544,;
    Left=466, Top=0, Width=46,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.f.;
   , bGlobalFont=.t.


  func oCAFLOPER_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLOPER,&i_cF..t_CAFLOPER),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCAFLOPER_2_5.GetRadio()
    this.Parent.oContained.w_CAFLOPER = this.RadioValue()
    return .t.
  endfunc

  func oCAFLOPER_2_5.ToRadio()
    this.Parent.oContained.w_CAFLOPER=trim(this.Parent.oContained.w_CAFLOPER)
    return(;
      iif(this.Parent.oContained.w_CAFLOPER=='S',1,;
      0))
  endfunc

  func oCAFLOPER_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLOPER_2_5.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CANUMCOR))
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCANUMCOR_2_1.When()
    return(.t.)
  proc oCANUMCOR_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCANUMCOR_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsba_mct','CCC_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CACODICE=CCC_MAST.CACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
