* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_bss                                                        *
*              Elabora scalare                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_905]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-12-19                                                      *
* Last revis.: 2014-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsba_bss",oParentObject,m.pOper)
return(i_retval)

define class tgsba_bss as StdBatch
  * --- Local variables
  pOper = space(1)
  w_CODCON = space(15)
  w_DATVAL = ctod("  /  /  ")
  w_TIPREC = space(1)
  w_IMPCRE = 0
  w_IMPDEB = 0
  w_FLADDC = space(1)
  w_FLCOMP = space(1)
  w_FLTIPO = space(1)
  w_GIOANN = 0
  w_CODVAL = space(3)
  w_DECTOT = 0
  w_BASINC = space(1)
  w_BASIND = space(1)
  w_BASLIQ = space(1)
  w_PERRIT = 0
  w_TASDE1 = 0
  w_LIMDE1 = 0
  w_COMSC1 = 0
  w_TASDE2 = 0
  w_LIMDE2 = 0
  w_COMSC2 = 0
  w_TASDE3 = 0
  w_COMSC3 = 0
  w_MINGIO = 0
  w_TASCRE = 0
  w_COSTOP = 0
  w_COSINV = 0
  w_SPELIQ = 0
  w_SPEFOR = 0
  w_NUMOPE = 0
  w_FLCAMB = space(1)
  w_SALINI = 0
  w_TOTIMP = 0
  w_OREP = space(50)
  w_INIANN = ctod("  /  /  ")
  w_OLDCON = space(15)
  w_OLDDAT = ctod("  /  /  ")
  w_OKWRI = .f.
  w_OKWRI2 = 0
  w_OK = .f.
  w_APPO = 0
  w_APPO1 = 0
  w_APPO2 = 0
  w_APPO3 = 0
  w_CONTA = 0
  w_NEXDAT = ctod("  /  /  ")
  w_INIVAL = ctod("  /  /  ")
  w_IMPSPE = 0
  w_NUMPER = 0
  w_IMPSAL = 0
  w_INTCRE = 0
  w_INTDEB = 0
  w_TIPCOM = space(1)
  w_CONT = 0
  w_ANNO = 0
  w_MESEFIN = 0
  w_TCODCON = space(15)
  w_TCONCOR = space(12)
  w_TDESCON = space(35)
  w_TDATVAL = ctod("  /  /  ")
  w_TIMPSAL = 0
  w_TNUMGIO = 0
  w_TNUMDE1 = 0
  w_TNUMDE2 = 0
  w_TNUMDE3 = 0
  w_TINTDEB = 0
  w_NINTDEB = 0
  w_TNUMCRE = 0
  w_TINTCRE = 0
  w_NINTCRE = 0
  w_TNUMOPE = 0
  w_TCODVAL = space(3)
  w_OLDVAL = space(3)
  w_TDECTOT = 0
  w_TFLSPES = space(1)
  w_TFLINTA = space(1)
  w_TFLINTP = space(1)
  w_TFLCAMB = space(1)
  w_TTIPRIG = space(1)
  w_INAFIT = space(1)
  w_INPFIT = space(1)
  w_SPEFIT = space(1)
  w_ADDINT = 0
  w_IMPCOM1 = 0
  w_IMPCOM2 = 0
  w_IMPCOM3 = 0
  w_INTCREM = 0
  w_INTDEBM = 0
  w_TOTOPE = 0
  w_MAXSCO = 0
  w_TOTCAMB = 0
  w_OPECON = 0
  w_ANTERGATE = space(1)
  w_DATSAL = ctod("  /  /  ")
  w_INTSOMMATI = 0
  w_TOTPASS = 0
  w_MESEINI = 0
  w_CCSERIAL = space(10)
  w_CCROWRIF = 0
  w_CC__ANNO = space(4)
  w_CCNUMREG = 0
  w_CCDATREG = ctod("  /  /  ")
  w_CCCODVAL = space(3)
  w_CCCAOVAL = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_CCCODCAU = space(5)
  w_CCNUMCOR = space(15)
  w_CCIMPCRE = 0
  w_CCIMPDEB = 0
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_CCDESCRI = space(50)
  * --- WorkFile variables
  CCM_DETT_idx=0
  CCM_MAST_idx=0
  COC_MAST_idx=0
  TMP_SCA2_idx=0
  TMP_SCAL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Stampa Estratto Conto Scalare/Conteggio Competenze (da GSBA_SES, GSBA_SCC)
    * --- Parametro pOper : S=Scalare ; C=Conteggio Competenze
    do case
      case this.oParentObject.w_DATINI>this.oParentObject.w_DATFIN OR EMPTY(this.oParentObject.w_DATINI) OR EMPTY(this.oParentObject.w_DATFIN)
        ah_ErrorMsg("Date di selezione incongruenti o non definite",,"")
        i_retcode = 'stop'
        return
      case YEAR(this.oParentObject.w_DATINI)>YEAR(this.oParentObject.w_DATFIN)
        ah_ErrorMsg("L'intervallo di date deve essere compreso nello stesso anno",,"")
        i_retcode = 'stop'
        return
    endcase
    if this.pOPER="C" AND this.oParentObject.w_TIPSTA="D" AND (EMPTY(this.oParentObject.w_CAUSPE) OR EMPTY(this.oParentObject.w_CAUINP) OR EMPTY(this.oParentObject.w_CAUINA))
      ah_ErrorMsg("Causale per il conteggio competenze non specificata",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Variabili da TMP_SCAL
    * --- Variabili Elaborazione
    * --- Variabili del __TMP__
    * --- variabili sul Conto
    * --- Data di Inizio Selezione delle Queriers
    this.w_INIANN = cp_CharToDate("01-01-"+ALLTRIM(STR(YEAR(this.oParentObject.w_DATINI))))
    ah_Msg("Ricerca movimenti di conto corrente...",.T.)
    vq_exec("..\BANC\EXE\QUERY\GSBASBSS.VQR",this,"SALDICON")
    * --- Create temporary table TMP_SCAL
    i_nIdx=cp_AddTableDef('TMP_SCAL') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\BANC\EXE\QUERY\GSBA0BSS.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_SCAL_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMP_SCA2
    i_nIdx=cp_AddTableDef('TMP_SCA2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\BANC\EXE\QUERY\GSBATBSS.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_SCA2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Crea Cursore Elaborazione/Stampa
    CREATE CURSOR __TMP__ ;
    (CODCON C(15), DESCON C(35), CONCOR C(12), DATVAL D(8), IMPSAL N(18,4), NUMGIO N(4,0), NUMDE1 N(18,4), NUMDE2 N(18,4), NUMDE3 N(18,4), ;
    INTDEB N(18,4), NUMCRE N(18,4), INTCRE N(18,4), NUMOPE N(4,0), FLCAMB C(1), CODVAL C(3), DECTOT N(1), TIPREC C(1), ;
    TASDE1 N(7,4), LIMDE1 N(18,4), TASDE2 N(7,4), LIMDE2 N(18,4), TASDE3 N(7,4), TASCRE N(7,4), ;
    FLSPES C(1), FLINTA C(1), FLINTP C(1), COSTOP N(18,4), COSINV N(18,4), SPELIQ N(18,4), PERRIT N(6,2), ;
    OPEESE N(10,0), SPEFIT C(1), INAFIT C(1), INPFIT C(1), SPEFOR N(18,4), COMSC1 N(7,3), COMSC2 N(7,3), COMSC3 N(7,3),MINGIO N(2),;
    IMPCOM1 N(18,4),IMPCOM2 N(18,4),IMPCOM3 N(18,4),INTCREM N(18,4),INTDEBM N(18,4))
    if this.pOper="S"
      this.w_OREP = "..\BANC\EXE\QUERY\" + IIF(this.oParentObject.w_TIPSTA="S", "GSBA1SES.FRX", "GSBA_SES.FRX")
    else
      * --- Crea Cursore Stampa Competenze
      CREATE CURSOR CALCOM ;
      (CODCON C(15), DESCON C(35), CONCOR C(12), DATVAL D(8), NUMDE1 N(18,4), NUMDE2 N(18,4), NUMDE3 N(18,4), ;
      INTDEB N(18,4), NUMCRE N(18,4), INTCRE N(18,4), NUMOPE N(4,0), FLCAMB C(1), CODVAL C(3), DECTOT N(1,0), TIPREC C(1), ;
      TASDE1 N(7,4), TASDE2 N(7,4), TASDE3 N(7,4), TASCRE N(7,4), COSTOP N(18,4), COSINV N(18,4), SPELIQ N(18,4), PERRIT N(6,2), ;
      OPEESE N(10,0), SPEFIT C(1), INAFIT C(1), INPFIT C(1), COMSC1 N(7,3), COMSC2 N(7,3), COMSC3 N(7,3),MINGIO N(2);
      ,IMPCOM1 N(18,4), IMPCOM2 N(18,4), IMPCOM3 N(18,4))
      * --- Crea Cursore Aggiorna Competenze
      CREATE CURSOR AGGCOM (CODCON C(15), DATVAL D(8), TIPCOM C(1), TOTIMP N(18,4), CODVAL C(3), DECTOT N(1,0))
      this.w_OREP = "..\BANC\EXE\QUERY\GSBA_SCC.FRX"
    endif
     
 CREATE CURSOR COMPET ; 
 (CODCON C(15), INIVAL D(8), NUMPER N(2,0), IMPCRE N(18,4), IMPDEB N(18,4), FLCOMP C(1), FLTIPO C(1), CODVAL C(3), DECTOT N(1))
    * --- Aggiunge a TMP_SCAL eventuali Competenze per Spese e Interessi non ancora registrati
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Cicla sul TMP Dei Movimenti di C/C - ORDER BY CODCON,DATVAL,TIPREC,CPROWORD,IMPCOM
    this.w_OLDCON = "xxx@@@zzz###"
    this.w_OLDDAT = i_INIDAT
    this.w_OKWRI = .F.
    this.w_OKWRI2 = 0
    this.w_TIMPSAL = 0
    this.w_TFLCAMB = " "
    this.w_TFLSPES = " "
    this.w_TTIPRIG = " "
    this.w_TFLINTA = " "
    this.w_TFLINTP = " "
    this.w_INAFIT = " "
    this.w_INPFIT = " "
    this.w_SPEFIT = " "
    this.w_SPEFOR = 0
    this.w_CONTA = 0
    this.w_SALINI = 0
    this.w_NUMOPE = 0
    this.w_TOTOPE = 0
    ah_Msg("Elabora cursore per data valuta...",.T.)
    * --- Select from TMP_SCAL
    i_nConn=i_TableProp[this.TMP_SCAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCAL_idx,2],.t.,this.TMP_SCAL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_SCAL ";
          +" order by CODCON,DATVAL,TIPREC,CPROWORD,IMPCOM";
           ,"_Curs_TMP_SCAL")
    else
      select * from (i_cTable);
       order by CODCON,DATVAL,TIPREC,CPROWORD,IMPCOM;
        into cursor _Curs_TMP_SCAL
    endif
    if used('_Curs_TMP_SCAL')
      select _Curs_TMP_SCAL
      locate for 1=1
      do while not(eof())
      this.w_CODCON = _Curs_TMP_SCAL.CODCON
      this.w_DATVAL = CP_TODATE(_Curs_TMP_SCAL.DATVAL)
      this.w_ANTERGATE = ANTERGATE
      if this.w_OKWRI=.T. AND this.w_CONTA<>0 AND (this.w_DATVAL<>this.w_OLDDAT OR this.w_CODCON<>this.w_OLDCON)
        * --- Cambio data: Aggiorna TMP_SCA2
        if this.w_OKWRI2=1 
          * --- Inserisce il Saldo al giorno precedente (solo all'inizio di un nuovo Conto)
          if USED("SALDICON") 
            SELECT SALDICON
            GO TOP
            LOCATE FOR this.w_OLDCON= Nvl(CODCON," ") AND (NVL(IMPCRE,0)<>0 OR NVL(IMPDEB,0)<>0)
            if Found()
              this.w_IMPCRE = NVL(IMPCRE,0)
              this.w_IMPDEB = NVL(IMPDEB,0)
              this.w_SALINI = (this.w_IMPCRE-this.w_IMPDEB)
            endif
          endif
          this.w_DATSAL = this.oParentObject.w_DATINI - 1
          * --- Try
          local bErr_00F5D2D0
          bErr_00F5D2D0=bTrsErr
          this.Try_00F5D2D0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg("Errore in inserimento TMP_SCA2 (4)")
          endif
          bTrsErr=bTrsErr or bErr_00F5D2D0
          * --- End
        endif
        this.w_OKWRI2 = 2
        * --- Insert into TMP_SCA2
        i_nConn=i_TableProp[this.TMP_SCA2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCA2_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_SCA2_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"NUMGIO"+",BASINC"+",BASIND"+",BASLIQ"+",CODVAL"+",COMSC1"+",COMSC2"+",COMSC3"+",COSINV"+",COSTOP"+",DECTOT"+",GIOANN"+",LIMDE1"+",LIMDE2"+",MINGIO"+",CODCON"+",DATVAL"+",PERRIT"+",SPELIQ"+",TASCRE"+",TASDE1"+",TASDE2"+",TASDE3"+",FLCAMB"+",FLINTA"+",FLINTP"+",FLSPES"+",IMPSAL"+",NUMOPE"+",TIPRIG"+",OPEESE"+",SPEFIT"+",INAFIT"+",INPFIT"+",SPEFOR"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(0),'TMP_SCA2','NUMGIO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_BASINC),'TMP_SCA2','BASINC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_BASIND),'TMP_SCA2','BASIND');
          +","+cp_NullLink(cp_ToStrODBC(this.w_BASLIQ),'TMP_SCA2','BASLIQ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'TMP_SCA2','CODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC1),'TMP_SCA2','COMSC1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC2),'TMP_SCA2','COMSC2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC3),'TMP_SCA2','COMSC3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COSINV),'TMP_SCA2','COSINV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COSTOP),'TMP_SCA2','COSTOP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DECTOT),'TMP_SCA2','DECTOT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_GIOANN),'TMP_SCA2','GIOANN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LIMDE1),'TMP_SCA2','LIMDE1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LIMDE2),'TMP_SCA2','LIMDE2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MINGIO),'TMP_SCA2','MINGIO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLDCON),'TMP_SCA2','CODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OLDDAT),'TMP_SCA2','DATVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PERRIT),'TMP_SCA2','PERRIT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SPELIQ),'TMP_SCA2','SPELIQ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TASCRE),'TMP_SCA2','TASCRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE1),'TMP_SCA2','TASDE1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE2),'TMP_SCA2','TASDE2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE3),'TMP_SCA2','TASDE3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TFLCAMB),'TMP_SCA2','FLCAMB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TFLINTA),'TMP_SCA2','FLINTA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TFLINTP),'TMP_SCA2','FLINTP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TFLSPES),'TMP_SCA2','FLSPES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIMPSAL),'TMP_SCA2','IMPSAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TNUMOPE),'TMP_SCA2','NUMOPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TTIPRIG),'TMP_SCA2','TIPRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMOPE),'TMP_SCA2','OPEESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SPEFIT),'TMP_SCA2','SPEFIT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_INAFIT),'TMP_SCA2','INAFIT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_INPFIT),'TMP_SCA2','INPFIT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SPEFOR),'TMP_SCA2','SPEFOR');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'NUMGIO',0,'BASINC',this.w_BASINC,'BASIND',this.w_BASIND,'BASLIQ',this.w_BASLIQ,'CODVAL',this.w_CODVAL,'COMSC1',this.w_COMSC1,'COMSC2',this.w_COMSC2,'COMSC3',this.w_COMSC3,'COSINV',this.w_COSINV,'COSTOP',this.w_COSTOP,'DECTOT',this.w_DECTOT,'GIOANN',this.w_GIOANN)
          insert into (i_cTable) (NUMGIO,BASINC,BASIND,BASLIQ,CODVAL,COMSC1,COMSC2,COMSC3,COSINV,COSTOP,DECTOT,GIOANN,LIMDE1,LIMDE2,MINGIO,CODCON,DATVAL,PERRIT,SPELIQ,TASCRE,TASDE1,TASDE2,TASDE3,FLCAMB,FLINTA,FLINTP,FLSPES,IMPSAL,NUMOPE,TIPRIG,OPEESE,SPEFIT,INAFIT,INPFIT,SPEFOR &i_ccchkf. );
             values (;
               0;
               ,this.w_BASINC;
               ,this.w_BASIND;
               ,this.w_BASLIQ;
               ,this.w_CODVAL;
               ,this.w_COMSC1;
               ,this.w_COMSC2;
               ,this.w_COMSC3;
               ,this.w_COSINV;
               ,this.w_COSTOP;
               ,this.w_DECTOT;
               ,this.w_GIOANN;
               ,this.w_LIMDE1;
               ,this.w_LIMDE2;
               ,this.w_MINGIO;
               ,this.w_OLDCON;
               ,this.w_OLDDAT;
               ,this.w_PERRIT;
               ,this.w_SPELIQ;
               ,this.w_TASCRE;
               ,this.w_TASDE1;
               ,this.w_TASDE2;
               ,this.w_TASDE3;
               ,this.w_TFLCAMB;
               ,this.w_TFLINTA;
               ,this.w_TFLINTP;
               ,this.w_TFLSPES;
               ,this.w_TIMPSAL;
               ,this.w_TNUMOPE;
               ,this.w_TTIPRIG;
               ,this.w_NUMOPE;
               ,this.w_SPEFIT;
               ,this.w_INAFIT;
               ,this.w_INPFIT;
               ,this.w_SPEFOR;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore in Inserimento TMP_SCA2 (2)'
          return
        endif
        this.w_TIMPSAL = 0
        this.w_TNUMOPE = 0
        this.w_TFLCAMB = " "
        this.w_TFLSPES = " "
        this.w_TFLINTA = " "
        this.w_TFLINTP = " "
        this.w_TTIPRIG = " "
        this.w_INAFIT = " "
        this.w_INPFIT = " "
        this.w_SPEFIT = " "
        this.w_SPEFOR = 0
        this.w_CONTA = 0
        this.w_OLDDAT = this.w_DATVAL
      endif
      this.w_DECTOT = NVL(_Curs_TMP_SCAL.DECTOT, 0)
      this.w_CODVAL = NVL(_Curs_TMP_SCAL.CODVAL, g_PERVAL)
      * --- Cambio Conto Corrente e Condizioni di Conto Corrente , Inizializza Dati del Conto e relative Condizioni C/C
      if this.w_OLDCON<>this.w_CODCON
        this.w_OKWRI = .T.
        this.w_OKWRI2 = 0
        * --- Dati del Conto
        this.w_BASINC = NVL(_Curs_TMP_SCAL.BASINC," ")
        this.w_BASIND = NVL(_Curs_TMP_SCAL.BASIND," ")
        this.w_BASLIQ = NVL(_Curs_TMP_SCAL.BASLIQ," ")
        this.w_PERRIT = NVL(_Curs_TMP_SCAL.PERRIT,0)
        this.w_GIOANN = NVL(_Curs_TMP_SCAL.GIOANN,0)
        * --- Variabili del __TMP__
        this.w_TIMPSAL = 0
        * --- IIF(NVL(TIPREC='C', ' '), NVL(IMPCRE, 0) - NVL(IMPDEB, 0), 0)
        this.w_TNUMOPE = 0
        this.w_TOTOPE = 0
        this.w_TFLCAMB = " "
        this.w_TFLSPES = " "
        this.w_TFLINTA = " "
        this.w_TFLINTP = " "
        this.w_TTIPRIG = " "
        this.w_INAFIT = " "
        this.w_INPFIT = " "
        this.w_SPEFIT = " "
        this.w_SPEFOR = 0
        this.w_CONTA = 0
        this.w_SALINI = 0
        this.w_OLDCON = this.w_CODCON
      endif
      * --- Tipo record C=Conto Corrente+Condizione di C/C ; M=Movimento di C/C ; S=Spesa Competenze Fittizi
      this.w_TIPREC = NVL(_Curs_TMP_SCAL.TIPREC, " ")
      this.w_IMPCRE = NVL(_Curs_TMP_SCAL.IMPCRE,0)
      this.w_IMPDEB = NVL(_Curs_TMP_SCAL.IMPDEB,0)+NVL(_Curs_TMP_SCAL.IMPCOM,0)
      this.w_FLADDC = NVL(_Curs_TMP_SCAL.FLADDC," ")
      this.w_FLCOMP = NVL(_Curs_TMP_SCAL.FLCOMP," ")
      this.w_FLTIPO = NVL(_Curs_TMP_SCAL.FLTIPO, " ")
      if this.w_TIPREC="C"
        * --- Nuove Condizioni Condizioni di Conto Corrente 
        this.w_TASDE1 = NVL(_Curs_TMP_SCAL.TASDE1, 0)
        this.w_LIMDE1 = ABS(NVL(_Curs_TMP_SCAL.LIMDE1, 0))
        this.w_COMSC1 = NVL(_Curs_TMP_SCAL.COMSC1, 0)
        this.w_TASDE2 = NVL(_Curs_TMP_SCAL.TASDE2, 0)
        this.w_LIMDE2 = ABS(NVL(_Curs_TMP_SCAL.LIMDE2, 0))
        this.w_COMSC2 = NVL(_Curs_TMP_SCAL.COMSC2, 0)
        this.w_TASDE3 = NVL(_Curs_TMP_SCAL.TASDE3, 0)
        this.w_COMSC3 = NVL(_Curs_TMP_SCAL.COMSC3, 0)
        this.w_MINGIO = NVL(_Curs_TMP_SCAL.MINGIO,0)
        this.w_TASCRE = NVL(_Curs_TMP_SCAL.TASCRE,0)
        this.w_COSTOP = NVL(_Curs_TMP_SCAL.COSTOP,0)
        this.w_COSINV = NVL(_Curs_TMP_SCAL.COSINV,0)
        this.w_SPELIQ = NVL(_Curs_TMP_SCAL.SPELIQ,0)
        this.w_NUMOPE = NVL(_Curs_TMP_SCAL.NUMOPE,0)
        * --- Notifica avvenuto Cambio Condizioni (solo se il cambio Condizione e' successivo alla data di inizio selezione)
        this.w_TFLCAMB = IIF(this.w_DATVAL>=this.oParentObject.w_DATINI, "S", "I")
        * --- Saldo Iniziale
        this.w_SALINI = this.w_TIMPSAL
      endif
      * --- Segna se la Riga di Calcolo Competenze
      do case
        case this.w_FLCOMP="S"
          * --- Spese
          this.w_TFLSPES = "S"
          if this.w_TIPREC="S"
            * --- Spesa Fittizia
            this.w_SPEFIT = "S"
            if this.w_NUMOPE<=this.w_TNUMOPE AND this.w_NUMOPE>0 
              * --- Azzero Spese se Num. Op Esenti <= Totale Operazioni (solo Fittizie)
              this.w_IMPCRE = 0
              this.w_IMPDEB = 0
              * --- Azzero anche il Flag per evitare il conteggio in caso di Calcolo competenze
              this.w_SPEFIT = "N"
            endif
          else
            * --- Se riga Spese non Fittizia Aggiorna le spese con l'effettivo valore inserito (Forfettario)
            this.w_SPEFOR = this.w_IMPDEB
          endif
        case this.w_FLCOMP="I" AND this.w_FLTIPO="C"
          * --- Interessi Attivi
          this.w_TFLINTA = "S"
          if this.w_TIPREC="S"
            * --- Fittizia
            this.w_INAFIT = "S"
          endif
        case this.w_FLCOMP="I" AND this.w_FLTIPO="D"
          * --- Interessi Passivi
          this.w_TFLINTP = "S"
          if this.w_TIPREC="S"
            * --- Fittizia
            this.w_INPFIT = "S"
          endif
      endcase
      * --- Contatore Righe da "scrivere"
      if this.w_DATVAL>=this.oParentObject.w_DATINI OR this.w_ANTERGATE="S" OR this.w_TFLCAMB="I"
        if this.w_OKWRI2=0 
          * --- Se e' la prima volta
          this.w_OKWRI2 = 1
          this.w_OLDDAT = this.w_DATVAL
        endif
        this.w_CONTA = this.w_CONTA + 1
      endif
      * --- Righe Movimenti
      * --- N.Operazioni giornaliere con Addebito Costi del Conto (a partire dal 1/1)
      this.w_TNUMOPE = this.w_TNUMOPE + IIF(this.w_FLADDC="S", 1, 0)
      * --- Aggiorna Progressivo Saldo
      this.w_TIMPSAL = this.w_TIMPSAL +IIF(this.w_FLCOMP="I",0, (this.w_IMPCRE-this.w_IMPDEB))
        select _Curs_TMP_SCAL
        continue
      enddo
      use
    endif
    * --- Ultimo Record
    if this.w_OKWRI=.T. AND this.w_CONTA<>0 
      * --- Cambio data: Aggiorna TMP_SCA2
      if this.w_OKWRI2=1
        * --- Inserisce il Saldo al giorno precedente (solo all'inizio di un nuovo Conto)
        if USED("SALDICON") 
          SELECT SALDICON
          GO TOP
          LOCATE FOR this.w_CODCON= Nvl(CODCON," ") AND (NVL(IMPCRE,0)<>0 OR NVL(IMPDEB,0)<>0)
          if Found()
            this.w_IMPCRE = NVL(IMPCRE,0)
            this.w_IMPDEB = NVL(IMPDEB,0)
            this.w_SALINI = (this.w_IMPCRE-this.w_IMPDEB)
          endif
        endif
        this.w_DATSAL = this.oParentObject.w_DATINI - 1
        * --- Try
        local bErr_00F01450
        bErr_00F01450=bTrsErr
        this.Try_00F01450()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_Errormsg("Errore in inserimento TMP_SCA2 (4)")
        endif
        bTrsErr=bTrsErr or bErr_00F01450
        * --- End
      endif
      * --- Try
      local bErr_00E6C330
      bErr_00E6C330=bTrsErr
      this.Try_00E6C330()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_Errormsg("Errore in inserimento TMP_SCA2 (4)")
      endif
      bTrsErr=bTrsErr or bErr_00E6C330
      * --- End
    endif
    * --- Alla Fine Aggiorna sul record 'C' Saldi precedenti alla data di Inizio Selezione
    * --- Drop temporary table TMP_SCAL
    i_nIdx=cp_GetTableDefIdx('TMP_SCAL')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_SCAL')
    endif
    * --- Inserisco i Giorni
    this.w_OLDCON = "xxx@@@zzz###"
    this.w_OLDDAT = this.oParentObject.w_DATFIN
    ah_Msg("Inserisce numero giorni valuta...",.T.)
    * --- Select from TMP_SCA2
    i_nConn=i_TableProp[this.TMP_SCA2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCA2_idx,2],.t.,this.TMP_SCA2_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_SCA2 ";
          +" order by CODCON,DATVAL DESC";
           ,"_Curs_TMP_SCA2")
    else
      select * from (i_cTable);
       order by CODCON,DATVAL DESC;
        into cursor _Curs_TMP_SCA2
    endif
    if used('_Curs_TMP_SCA2')
      select _Curs_TMP_SCA2
      locate for 1=1
      do while not(eof())
      this.w_CODCON = CODCON
      this.w_DATVAL = CP_TODATE(DATVAL)
      if this.w_OLDCON<>this.w_CODCON
        this.w_OLDCON = this.w_CODCON
        this.w_OLDDAT = this.oParentObject.w_DATFIN
      endif
      this.w_TNUMGIO = IIF(NVL(FLCAMB," ")="I",0,this.w_OLDDAT- this.w_DATVAL)
      * --- Write into TMP_SCA2
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMP_SCA2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCA2_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_SCA2_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"NUMGIO ="+cp_NullLink(cp_ToStrODBC(this.w_TNUMGIO),'TMP_SCA2','NUMGIO');
            +i_ccchkf ;
        +" where ";
            +"CODCON = "+cp_ToStrODBC(this.w_CODCON);
            +" and DATVAL = "+cp_ToStrODBC(this.w_DATVAL);
               )
      else
        update (i_cTable) set;
            NUMGIO = this.w_TNUMGIO;
            &i_ccchkf. ;
         where;
            CODCON = this.w_CODCON;
            and DATVAL = this.w_DATVAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_OLDDAT = this.w_DATVAL
        select _Curs_TMP_SCA2
        continue
      enddo
      use
    endif
    * --- Alla Fine genera TMP Stampa
    ah_Msg("Aggiorna temporaneo stampa...",.T.)
    this.w_OLDCON = "xxx@@@zzz###"
    this.w_TIMPSAL = 0
    this.w_NINTDEB = 0
    this.w_NINTCRE = 0
    this.w_TFLINTA = " "
    this.w_TFLINTP = " "
    this.w_TDESCON = " "
    this.w_TCONCOR = " "
    this.w_INTCRE = 0
    this.w_INTDEB = 0
    this.w_MAXSCO = 0
    this.w_INTCREM = 0
    this.w_INTDEBM = 0
    this.w_INIVAL = i_INIDAT
    * --- Il periodo idi maturazione interessi considera nel relativo saldo per valuta gli intereissi
    *     passivi \attivi maturati nel periodo di competenza
    *     Gli interessi presenti nel record del periodo di maturazione fanno invece riferimento 
    *     al periodo successivo.
    this.w_INTSOMMATI = 0
    * --- Select from TMP_SCA2
    i_nConn=i_TableProp[this.TMP_SCA2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCA2_idx,2],.t.,this.TMP_SCA2_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_SCA2 ";
          +" order by CODCON,DATVAL";
           ,"_Curs_TMP_SCA2")
    else
      select * from (i_cTable);
       order by CODCON,DATVAL;
        into cursor _Curs_TMP_SCA2
    endif
    if used('_Curs_TMP_SCA2')
      select _Curs_TMP_SCA2
      locate for 1=1
      do while not(eof())
      this.w_TCODCON = CODCON
      if this.w_TCODCON<>this.w_OLDCON
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BADESCRI,BACONCOR"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_TCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BADESCRI,BACONCOR;
            from (i_cTable) where;
                BACODBAN = this.w_TCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TDESCON = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
          this.w_TCONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_OLDCON = this.w_TCODCON
        this.w_INIVAL = CP_TODATE(DATVAL)
        this.w_TIMPSAL = 0
        this.w_TFLINTA = " "
        this.w_TFLINTP = " "
        this.w_NINTDEB = 0
        this.w_NINTCRE = 0
        this.w_TINTCRE = 0
        this.w_TINTDEB = 0
      endif
      this.w_TIMPSAL = this.w_TIMPSAL + NVL(IMPSAL, 0)
      * --- Verifica se la Riga di Calcolo Competenze Interessi
      this.w_TFLINTA = NVL(FLINTA," ")
      this.w_TFLINTP = NVL(FLINTP," ")
      this.w_TFLSPES = NVL(FLSPES," ")
      this.w_INAFIT = NVL(INAFIT, " ")
      this.w_INPFIT = NVL(INPFIT, " ")
      this.w_SPEFIT = NVL(SPEFIT, " ")
      this.w_SPEFOR = NVL(SPEFOR, 0)
      this.w_TDATVAL = CP_TODATE(DATVAL)
      this.w_TNUMGIO = NUMGIO
      this.w_TNUMOPE = NVL(NUMOPE,0)
      this.w_BASINC = NVL(BASINC," ")
      this.w_BASIND = NVL(BASIND," ")
      this.w_BASLIQ = NVL(BASLIQ," ")
      this.w_PERRIT = NVL(PERRIT,0)
      this.w_GIOANN = NVL(GIOANN,0)
      this.w_TASDE1 = NVL(TASDE1, 0)
      this.w_LIMDE1 = ABS(NVL(LIMDE1, 0))
      this.w_COMSC1 = NVL(COMSC1, 0)
      this.w_TASDE2 = NVL(TASDE2, 0)
      this.w_LIMDE2 = ABS(NVL(LIMDE2, 0))
      this.w_COMSC2 = NVL(COMSC2, 0)
      this.w_TASDE3 = NVL(TASDE3, 0)
      this.w_COMSC3 = NVL(COMSC3, 0)
      this.w_MINGIO = NVL(MINGIO,0)
      this.w_TASCRE = NVL(TASCRE,0)
      this.w_COSTOP = NVL(COSTOP,0)
      this.w_COSINV = NVL(COSINV,0)
      this.w_SPELIQ = NVL(SPELIQ,0)
      this.w_TCODVAL = NVL(CODVAL, g_PERVAL)
      this.w_TDECTOT = NVL(DECTOT, 0)
      this.w_TFLCAMB = NVL(FLCAMB, " ")
      this.w_NUMOPE = NVL(OPEESE,0)
      if this.w_TFLINTA="S"
        * --- detraggo anche eventuale importo della ritenuta fiscale
        this.w_INTCRE = this.w_NINTCRE - this.w_INTSOMMATI
        this.w_TIMPSAL = this.w_TIMPSAL + this.w_INTCRE-cp_ROUND((((this.w_INTCRE)*NVL(PERRIT,0))/100),this.w_DECTOT)
        this.w_INTSOMMATI = this.w_INTSOMMATI + this.w_INTCRE
      endif
      if this.w_TFLINTP="S"
        this.w_INTDEB = this.w_NINTDEB + this.w_INTDEBM
        * --- Aggiorno l'importo su cui calcolare gli interessi con l'ultimo interesse calcolcato
        this.w_TIMPSAL = this.w_TIMPSAL - this.w_INTDEBM
      endif
      if this.w_TFLCAMB<>"I"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Setta la data a partire dalla quale il saldo e' negativo, per il calcolo delle eventuali commissioni scoperto
      if this.w_TIMPSAL>=0
        this.w_INIVAL = this.w_TDATVAL
      endif
      if this.w_TDATVAL<=this.oParentObject.w_DATFIN
        * --- Riporta sulle variabili di totalizzazione (NINTCRE e NINTDEB) e sulla variabile degli interessi singoli l'importo dell'ultimo interesse calcolato
        this.w_NINTCRE = this.w_NINTCRE + this.w_TINTCRE
        this.w_NINTDEB = this.w_NINTDEB + this.w_TINTDEB
        this.w_INTDEBM = this.w_TINTDEB
        * --- Se periodo di Maturazione interrssi devo aggiornare
        *     INTECREM
        *     INTEDEBM
        *     Interessi specifici dell'ultimo giorno del periodo (che verranno visualizzati nello scalare)
        *     INTCRE
        *     INTDEB
        *     Totale interessi di competenza del periodo utilizzate nel calcolo delle competenze
        if this.w_TFLINTA="S"
          this.w_INTCREM = this.w_TINTCRE
          this.w_TINTCRE = this.w_NINTCRE 
        endif
        if this.w_TFLINTP="S"
          this.w_INTDEBM = this.w_TINTDEB
          this.w_TINTDEB = this.w_NINTDEB 
        endif
      else
        this.w_TINTCRE = 0
        this.w_TINTDEB = 0
      endif
      if this.w_TFLCAMB<>"I"
        * --- Scrive il Temporaneo Se non � riga di condizioni iniziale
        INSERT INTO __TMP__ ;
        (CODCON, DESCON, CONCOR, DATVAL, IMPSAL, NUMGIO, NUMDE1, NUMDE2, NUMDE3, INTDEB, ;
        NUMCRE, INTCRE, NUMOPE, FLCAMB, CODVAL, DECTOT, TIPREC, ;
        TASDE1, LIMDE1, TASDE2, LIMDE2, TASDE3, TASCRE, ;
        FLSPES, FLINTA, FLINTP, COSTOP, COSINV, SPELIQ, PERRIT, OPEESE, INAFIT, INPFIT, SPEFIT, SPEFOR, COMSC1, COMSC2, COMSC3,MINGIO,;
        IMPCOM1,IMPCOM2,IMPCOM3,INTCREM,INTDEBM) VALUES (this.w_TCODCON, this.w_TDESCON, this.w_TCONCOR, this.w_TDATVAL, this.w_TIMPSAL, this.w_TNUMGIO, this.w_TNUMDE1, ;
        this.w_TNUMDE2, this.w_TNUMDE3, this.w_TINTDEB, this.w_TNUMCRE, this.w_TINTCRE, this.w_TNUMOPE, this.w_TFLCAMB, this.w_TCODVAL, this.w_TDECTOT, "M", ;
        this.w_TASDE1, this.w_LIMDE1, this.w_TASDE2, this.w_LIMDE2, this.w_TASDE3, this.w_TASCRE, ;
        this.w_TFLSPES, this.w_TFLINTA, this.w_TFLINTP, this.w_COSTOP, this.w_COSINV, this.w_SPELIQ, this.w_PERRIT, this.w_NUMOPE, this.w_INAFIT, this.w_INPFIT, this.w_SPEFIT, this.w_SPEFOR, ;
        this.w_COMSC1, this.w_COMSC2, this.w_COMSC3,this.w_MINGIO,this.w_IMPCOM1,this.w_IMPCOM2,this.w_IMPCOM3,this.w_INTCREM,this.w_INTDEBM)
      endif
      if this.pOper="S"
        if (this.w_TFLCAMB="S" OR this.w_TFLCAMB="I") AND this.oParentObject.w_TIPSTA="S" AND RECCOUNT()>0
          * --- Stampa Scalare Analitico Aggiunge Sezione Condizioni e Interessi
          INSERT INTO __TMP__ ;
          (CODCON, DESCON, CONCOR, DATVAL, IMPSAL, NUMGIO, NUMDE1, NUMDE2, NUMDE3, INTDEB, ;
          NUMCRE, INTCRE, NUMOPE, FLCAMB, CODVAL, DECTOT, TIPREC, ;
          TASDE1, LIMDE1, TASDE2, LIMDE2, TASDE3, TASCRE, ;
          FLSPES, FLINTA, FLINTP, COSTOP, COSINV, SPELIQ, PERRIT, OPEESE, COMSC1, COMSC2, COMSC3,MINGIO,INTCREM,INTDEBM) VALUES ;
          (this.w_TCODCON, this.w_TDESCON, this.w_TCONCOR, this.w_TDATVAL, this.w_TIMPSAL, this.w_TNUMGIO, this.w_TNUMDE1, this.w_TNUMDE2, this.w_TNUMDE3, this.w_TINTDEB, ;
          this.w_TNUMCRE, this.w_TINTCRE, this.w_TNUMOPE, this.w_TFLCAMB, this.w_TCODVAL, this.w_TDECTOT, "V", ;
          this.w_TASDE1, this.w_LIMDE1, this.w_TASDE2, this.w_LIMDE2, this.w_TASDE3, this.w_TASCRE, ;
          this.w_TFLSPES, this.w_TFLINTA, this.w_TFLINTP, this.w_COSTOP, this.w_COSINV, this.w_SPELIQ, this.w_PERRIT, ;
          this.w_NUMOPE, this.w_COMSC1, this.w_COMSC2, this.w_COMSC3,this.w_MINGIO,this.w_INTCREM,this.w_INTDEBM)
        endif
      endif
      * --- Azzero variabili sula commissione Max scoperto
      if this.w_TFLINTP="S"
        this.w_IMPCOM1 = 0
        this.w_IMPCOM2 = 0
        this.w_IMPCOM3 = 0
      endif
        select _Curs_TMP_SCA2
        continue
      enddo
      use
    endif
    if USED("__TMP__")
      if this.pOper="S"
        SELECT __TMP__
        if this.oParentObject.w_TIPSTA="S" AND RECCOUNT()>0
          * --- Stampa Scalare Analitico Ordina in funzione delle Sezioni
          SELECT * FROM __TMP__ INTO CURSOR __TMP__ ORDER BY CODCON, TIPREC, DATVAL
        endif
      else
        this.w_OLDCON = "xxx@@@zzz###"
        this.w_TNUMCRE = 0
        this.w_TINTCRE = 0
        this.w_TASCRE = 0
        this.w_PERRIT = 0
        this.w_INAFIT = " "
        this.w_TOTIMP = 0
        SELECT __TMP__
        GO TOP
        SCAN FOR NOT EMPTY(CODCON)
        this.w_TCODCON = CODCON
        * --- Cambio Conto riazzera tutto
        if this.w_TCODCON<>this.w_OLDCON
          this.w_OLDCON = this.w_TCODCON
          this.w_TNUMCRE = 0
          this.w_TINTCRE = 0
          this.w_TASCRE = 0
          this.w_PERRIT = 0
          this.w_INAFIT = " "
          this.w_TOTIMP = 0
          this.w_PERRIT = Nvl(PERRIT,0)
        endif
        this.w_TDESCON = DESCON
        this.w_TCONCOR = CONCOR
        this.w_DATVAL = CP_TODATE(DATVAL)
        this.w_TCODVAL = CODVAL
        this.w_TDECTOT = DECTOT
        * --- Aggiorna dati per Generazione Movimento di C/C
        if FLINTA="S" 
          * --- Se Calcolo Competenze
          this.w_TOTIMP = (NVL(this.w_TINTCRE,0))-cp_ROUND((((NVL(this.w_TINTCRE ,0))*Nvl(PERRIT,0))/100),this.w_DECTOT) 
          if INAFIT="S"
            * --- Aggiorna solo se "fittizia"
            INSERT INTO AGGCOM ;
            (CODCON, DATVAL, TIPCOM, TOTIMP, CODVAL, DECTOT) VALUES ;
            (this.w_TCODCON, this.w_DATVAL, "A", this.w_TOTIMP, this.w_CODVAL, this.w_DECTOT)
          endif
          this.w_TOTIMP = 0
        endif
        this.w_PERRIT = IIF(FLCAMB="S", Nvl(PERRIT,0), this.w_PERRIT)
        this.w_TFLCAMB = IIF(FLCAMB="S", "S", "T")
        this.w_TASCRE = IIF(FLCAMB="S", TASCRE, this.w_TASCRE)
        this.w_INAFIT = IIF(FLINTA="S", INAFIT, this.w_INAFIT)
        this.w_TNUMCRE = NVL(NUMCRE,0)
        if FLINTA="S" AND (this.w_TNUMCRE<>0 OR this.w_TINTCRE<>0 OR this.w_DATVAL=this.oParentObject.w_DATFIN)
          * --- Se cambio Condizioni o Maturazione Interessi Attivi
          INSERT INTO CALCOM ;
          (CODCON, DESCON, CONCOR, DATVAL, TIPREC, FLCAMB, NUMCRE, INTCRE, CODVAL, DECTOT, TASCRE, PERRIT, INAFIT) VALUES ;
          (this.w_TCODCON, this.w_TDESCON, this.w_TCONCOR, this.w_DATVAL, "A", this.w_TFLCAMB, this.w_TNUMCRE, this.w_TINTCRE, this.w_TCODVAL, this.w_TDECTOT, ;
          this.w_TASCRE, this.w_PERRIT, this.w_INAFIT)
          this.w_TNUMCRE = 0
          this.w_TINTCRE = 0
          this.w_TINTCRE = NVL(INTCREM,0)
          SELECT __TMP__
        else
          this.w_TINTCRE = this.w_TINTCRE + NVL(INTCRE,0)
        endif
        ENDSCAN
        this.w_OLDCON = "xxx@@@zzz###"
        this.w_TNUMDE1 = 0
        this.w_TNUMDE2 = 0
        this.w_TNUMDE3 = 0
        this.w_TINTDEB = 0
        this.w_TASDE1 = 0
        this.w_TASDE2 = 0
        this.w_TASDE3 = 0
        this.w_INPFIT = " "
        this.w_TOTIMP = 0
        SELECT __TMP__
        GO TOP
        SCAN FOR NOT EMPTY(CODCON)
        this.w_TCODCON = CODCON
        * --- Cambio Conto riazzera tutto
        if this.w_TCODCON<>this.w_OLDCON
          this.w_OLDCON = this.w_TCODCON
          this.w_TNUMDE1 = 0
          this.w_TNUMDE2 = 0
          this.w_TNUMDE3 = 0
          this.w_TINTDEB = 0
          this.w_TASDE1 = 0
          this.w_TASDE2 = 0
          this.w_TASDE3 = 0
          this.w_INPFIT = " "
          this.w_TOTIMP = 0
        endif
        this.w_TDESCON = DESCON
        this.w_TCONCOR = CONCOR
        this.w_DATVAL = CP_TODATE(DATVAL)
        this.w_TCODVAL = CODVAL
        this.w_TDECTOT = DECTOT
        * --- Aggiorna dati per Generazione Movimento di C/C
        if FLINTP="S" 
          * --- Se Calcolo Competenze
          this.w_TOTIMP = this.w_TOTPASS
          if INPFIT="S"
            * --- Aggiorna solo se "fittizia"
            INSERT INTO AGGCOM ;
            (CODCON, DATVAL, TIPCOM, TOTIMP, CODVAL, DECTOT) VALUES ;
            (this.w_TCODCON, this.w_DATVAL, "P", this.w_TOTIMP, this.w_CODVAL, this.w_DECTOT)
          endif
          this.w_TOTPASS = ABS(NVL(INTDEBM,0))+(NVL(IMPCOM1,0)+NVL(IMPCOM2,0)+NVL(IMPCOM3,0))
          this.w_TOTIMP = 0
        else
          this.w_TOTPASS = this.w_TOTPASS + ABS(NVL(INTDEB,0))+(NVL(IMPCOM1,0)+NVL(IMPCOM2,0)+NVL(IMPCOM3,0))
        endif
        this.w_TFLCAMB = IIF(FLCAMB="S", "S", "T")
        this.w_TASDE1 = IIF(FLCAMB="S", TASDE1, this.w_TASDE1)
        this.w_TASDE2 = IIF(FLCAMB="S", TASDE2, this.w_TASDE2)
        this.w_TASDE3 = IIF(FLCAMB="S", TASDE3, this.w_TASDE3)
        this.w_INPFIT = IIF(FLINTP="S", INPFIT, this.w_INPFIT)
        this.w_COMSC1 = COMSC1
        this.w_COMSC2 = COMSC2
        this.w_COMSC3 = COMSC3
        this.w_IMPCOM1 = IMPCOM1
        this.w_IMPCOM2 = IMPCOM2
        this.w_IMPCOM3 = IMPCOM3
        this.w_MINGIO = MINGIO
        this.w_TNUMDE1 = NUMDE1
        this.w_TNUMDE2 = NUMDE2
        this.w_TNUMDE3 = NUMDE3
        if FLINTP="S" AND (this.w_TNUMDE1<>0 OR this.w_TNUMDE2<>0 OR this.w_TNUMDE3<>0 OR this.w_TINTDEB<>0 OR this.w_DATVAL=this.oParentObject.w_DATFIN)
          * --- Maturazione Interessi Passivi
          INSERT INTO CALCOM ;
          (CODCON, DESCON, CONCOR, DATVAL, TIPREC, FLCAMB, NUMDE1, NUMDE2, NUMDE3, INTDEB, ;
          CODVAL, DECTOT, TASDE1, TASDE2, TASDE3, INPFIT, COMSC1, COMSC2, COMSC3,MINGIO,IMPCOM1,IMPCOM2,IMPCOM3) VALUES ;
          (this.w_TCODCON, this.w_TDESCON, this.w_TCONCOR, this.w_DATVAL, "P", this.w_TFLCAMB, this.w_TNUMDE1, this.w_TNUMDE2, this.w_TNUMDE3, this.w_TINTDEB, ;
          this.w_TCODVAL, this.w_TDECTOT, this.w_TASDE1, this.w_TASDE2, this.w_TASDE3, this.w_INPFIT, this.w_COMSC1, this.w_COMSC2, this.w_COMSC3,this.w_MINGIO,this.w_IMPCOM1,;
          this.w_IMPCOM2,this.w_IMPCOM3)
          this.w_TNUMDE1 = 0
          this.w_TNUMDE2 = 0
          this.w_TNUMDE3 = 0
          this.w_TINTDEB = 0
          this.w_TINTDEB = Nvl(INTDEBM,0)
          SELECT __TMP__
        else
          this.w_TINTDEB = this.w_TINTDEB + Nvl(INTDEB,0)
        endif
        ENDSCAN
        this.w_OLDCON = "xxx@@@zzz###"
        this.w_TNUMOPE = 0
        this.w_NUMOPE = 0
        this.w_COSTOP = 0
        this.w_COSINV = 0
        this.w_SPELIQ = 0
        this.w_SPEFIT = " "
        this.w_SPEFOR = 0
        this.w_TOTIMP = 0
        SELECT __TMP__
        GO TOP
        SCAN FOR NOT EMPTY(CODCON)
        this.w_TCODCON = CODCON
        * --- Cambio Conto riazzera tutto
        if this.w_TCODCON<>this.w_OLDCON
          this.w_OLDCON = this.w_TCODCON
          this.w_TNUMOPE = 0
          this.w_NUMOPE = 0
          this.w_COSTOP = 0
          this.w_COSINV = 0
          this.w_SPELIQ = 0
          this.w_SPEFIT = " "
          this.w_SPEFOR = 0
          this.w_TOTIMP = 0
        endif
        this.w_TDESCON = DESCON
        this.w_TCONCOR = CONCOR
        this.w_DATVAL = CP_TODATE(DATVAL)
        this.w_TCODVAL = CODVAL
        this.w_TDECTOT = DECTOT
        * --- Aggiorna dati per Generazione Movimento di C/C
        this.w_TOTIMP = COSINV+SPELIQ
        if FLSPES="S" AND this.w_TOTIMP<>0
          this.w_TOTIMP = this.w_TOTIMP+(COSTOP*IIF(this.w_TNUMOPE-this.w_NUMOPE<0,0,this.w_TNUMOPE-this.w_NUMOPE))
          * --- Se Calcolo Competenze
          if SPEFIT="S"
            * --- Aggiorna solo se "fittizia"
            INSERT INTO AGGCOM ;
            (CODCON, DATVAL, TIPCOM, TOTIMP, CODVAL, DECTOT) VALUES ;
            (this.w_TCODCON, this.w_DATVAL, "S", this.w_TOTIMP, this.w_CODVAL, this.w_DECTOT)
          endif
          this.w_TOTIMP = 0
        endif
        this.w_TNUMOPE = this.w_TNUMOPE + NUMOPE
        this.w_NUMOPE = NVL(OPEESE,0)
        this.w_COSTOP = COSTOP
        this.w_COSINV = COSINV
        this.w_SPELIQ = SPELIQ
        this.w_SPEFIT = NVL(SPEFIT," ")
        this.w_SPEFOR = SPEFOR
        if FLSPES="S" AND SPEFIT<>"N"
          if this.w_SPEFIT<>"S"
            * --- Se spesa non Fittizia prende come importo quanto effettivamente inserito sul Movimento di C/C
            this.w_COSTOP = this.w_SPEFOR
            this.w_COSINV = 0
            this.w_SPELIQ = 0
          endif
          * --- Se Maturazione Spese
          INSERT INTO CALCOM ;
          (CODCON, DESCON, CONCOR, DATVAL, TIPREC, NUMOPE, CODVAL, DECTOT, COSTOP, COSINV, SPELIQ, OPEESE, SPEFIT) VALUES;
           (this.w_TCODCON, this.w_TDESCON, this.w_TCONCOR, this.w_DATVAL, "S", this.w_TNUMOPE, this.w_TCODVAL, this.w_TDECTOT, ;
          this.w_COSTOP, this.w_COSINV, this.w_SPELIQ, this.w_NUMOPE, this.w_SPEFIT)
          this.w_TNUMOPE = 0
          SELECT __TMP__
        endif
        ENDSCAN
        SELECT __TMP__
        USE
        SELECT * FROM CALCOM INTO CURSOR __TMP__ ORDER BY CODCON, TIPREC, DATVAL, FLCAMB
        SELECT CALCOM
        USE
      endif
      SELECT __TMP__
      GO TOP
      * --- Variabili Locali per il Report
      L_DATINI = this.oParentObject.w_DATINI
      L_DATFIN = this.oParentObject.w_DATFIN
      L_CODABI = this.oParentObject.w_CODABI
      L_CODCAB = this.oParentObject.w_CODCAB
      L_CODICE = this.oParentObject.w_CODICE
      L_CODVAL = this.w_CODVAL
      L_SIMVAL = this.oParentObject.w_SIMVAL
      L_DESVAL = this.oParentObject.w_DESVAL
      if RECCOUNT()>0
        * --- Esegue la Stampa
        CP_CHPRN(ALLTRIM(this.w_OREP), " ", this)
        if this.pOper<>"S" AND this.oParentObject.w_TIPSTA="D"
          * --- Stampa Definitiva, Aggiorna Competenze
          SELECT AGGCOM
          if RECCOUNT()>0
            if ah_YesNo("Aggiorno i movimenti di C/Corrente con le competenze calcolate?")
              * --- Stampa Scalare Analitico Ordina in funzione delle Sezioni
              SELECT * FROM AGGCOM INTO CURSOR AGGCOM ORDER BY DATVAL, CODVAL, CODCON, TIPCOM
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            ah_ErrorMsg("Non esistono competenze da aggiornare",,"")
          endif
        endif
      else
        ah_ErrorMsg("Per le selezioni impostate non esistono dati da elaborare",,"")
      endif
    endif
    * --- Elimina Cursori
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("COMPET")
      SELECT COMPET
      USE
    endif
    if USED("AGGCOM")
      SELECT AGGCOM
      USE
    endif
    if USED("CALCOM")
      SELECT CALCOM
      USE
    endif
    if USED("SALDICON")
      SELECT SALDICON
      USE
    endif
    * --- Drop temporary table TMP_SCA2
    i_nIdx=cp_GetTableDefIdx('TMP_SCA2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_SCA2')
    endif
  endproc
  proc Try_00F5D2D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMP_SCA2
    i_nConn=i_TableProp[this.TMP_SCA2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCA2_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_SCA2_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CODCON"+",DATVAL"+",IMPSAL"+",NUMGIO"+",NUMOPE"+",FLSPES"+",FLINTA"+",FLINTP"+",FLCAMB"+",GIOANN"+",CODVAL"+",DECTOT"+",BASINC"+",BASIND"+",BASLIQ"+",TASDE1"+",PERRIT"+",LIMDE1"+",COMSC1"+",TASDE2"+",LIMDE2"+",COMSC2"+",COMSC3"+",TASDE3"+",MINGIO"+",TASCRE"+",COSTOP"+",COSINV"+",SPELIQ"+",TIPRIG"+",OPEESE"+",SPEFIT"+",INAFIT"+",INPFIT"+",SPEFOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLDCON),'TMP_SCA2','CODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATSAL),'TMP_SCA2','DATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SALINI),'TMP_SCA2','IMPSAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'TMP_SCA2','NUMGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TNUMOPE),'TMP_SCA2','NUMOPE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCA2','FLSPES');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCA2','FLINTA');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCA2','FLINTP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCA2','FLCAMB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GIOANN),'TMP_SCA2','GIOANN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'TMP_SCA2','CODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DECTOT),'TMP_SCA2','DECTOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASINC),'TMP_SCA2','BASINC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASIND),'TMP_SCA2','BASIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASLIQ),'TMP_SCA2','BASLIQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE1),'TMP_SCA2','TASDE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERRIT),'TMP_SCA2','PERRIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIMDE1),'TMP_SCA2','LIMDE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC1),'TMP_SCA2','COMSC1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE2),'TMP_SCA2','TASDE2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIMDE2),'TMP_SCA2','LIMDE2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC2),'TMP_SCA2','COMSC2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC3),'TMP_SCA2','COMSC3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE3),'TMP_SCA2','TASDE3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINGIO),'TMP_SCA2','MINGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASCRE),'TMP_SCA2','TASCRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COSTOP),'TMP_SCA2','COSTOP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COSINV),'TMP_SCA2','COSINV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPELIQ),'TMP_SCA2','SPELIQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TTIPRIG),'TMP_SCA2','TIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMOPE),'TMP_SCA2','OPEESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPEFIT),'TMP_SCA2','SPEFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INAFIT),'TMP_SCA2','INAFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INPFIT),'TMP_SCA2','INPFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPEFOR),'TMP_SCA2','SPEFOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CODCON',this.w_OLDCON,'DATVAL',this.w_DATSAL,'IMPSAL',this.w_SALINI,'NUMGIO',0,'NUMOPE',this.w_TNUMOPE,'FLSPES'," ",'FLINTA'," ",'FLINTP'," ",'FLCAMB'," ",'GIOANN',this.w_GIOANN,'CODVAL',this.w_CODVAL,'DECTOT',this.w_DECTOT)
      insert into (i_cTable) (CODCON,DATVAL,IMPSAL,NUMGIO,NUMOPE,FLSPES,FLINTA,FLINTP,FLCAMB,GIOANN,CODVAL,DECTOT,BASINC,BASIND,BASLIQ,TASDE1,PERRIT,LIMDE1,COMSC1,TASDE2,LIMDE2,COMSC2,COMSC3,TASDE3,MINGIO,TASCRE,COSTOP,COSINV,SPELIQ,TIPRIG,OPEESE,SPEFIT,INAFIT,INPFIT,SPEFOR &i_ccchkf. );
         values (;
           this.w_OLDCON;
           ,this.w_DATSAL;
           ,this.w_SALINI;
           ,0;
           ,this.w_TNUMOPE;
           ," ";
           ," ";
           ," ";
           ," ";
           ,this.w_GIOANN;
           ,this.w_CODVAL;
           ,this.w_DECTOT;
           ,this.w_BASINC;
           ,this.w_BASIND;
           ,this.w_BASLIQ;
           ,this.w_TASDE1;
           ,this.w_PERRIT;
           ,this.w_LIMDE1;
           ,this.w_COMSC1;
           ,this.w_TASDE2;
           ,this.w_LIMDE2;
           ,this.w_COMSC2;
           ,this.w_COMSC3;
           ,this.w_TASDE3;
           ,this.w_MINGIO;
           ,this.w_TASCRE;
           ,this.w_COSTOP;
           ,this.w_COSINV;
           ,this.w_SPELIQ;
           ,this.w_TTIPRIG;
           ,this.w_NUMOPE;
           ,this.w_SPEFIT;
           ,this.w_INAFIT;
           ,this.w_INPFIT;
           ,this.w_SPEFOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_00F01450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMP_SCA2
    i_nConn=i_TableProp[this.TMP_SCA2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCA2_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_SCA2_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CODCON"+",DATVAL"+",IMPSAL"+",NUMGIO"+",NUMOPE"+",FLSPES"+",FLINTA"+",FLINTP"+",FLCAMB"+",GIOANN"+",CODVAL"+",DECTOT"+",BASINC"+",BASIND"+",BASLIQ"+",TASDE1"+",PERRIT"+",LIMDE1"+",COMSC1"+",TASDE2"+",LIMDE2"+",COMSC2"+",COMSC3"+",TASDE3"+",MINGIO"+",TASCRE"+",COSTOP"+",COSINV"+",SPELIQ"+",TIPRIG"+",OPEESE"+",SPEFIT"+",INAFIT"+",INPFIT"+",SPEFOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLDCON),'TMP_SCA2','CODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATSAL),'TMP_SCA2','DATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SALINI),'TMP_SCA2','IMPSAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'TMP_SCA2','NUMGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TNUMOPE),'TMP_SCA2','NUMOPE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCA2','FLSPES');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCA2','FLINTA');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCA2','FLINTP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCA2','FLCAMB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GIOANN),'TMP_SCA2','GIOANN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'TMP_SCA2','CODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DECTOT),'TMP_SCA2','DECTOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASINC),'TMP_SCA2','BASINC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASIND),'TMP_SCA2','BASIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASLIQ),'TMP_SCA2','BASLIQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE1),'TMP_SCA2','TASDE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERRIT),'TMP_SCA2','PERRIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIMDE1),'TMP_SCA2','LIMDE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC1),'TMP_SCA2','COMSC1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE2),'TMP_SCA2','TASDE2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIMDE2),'TMP_SCA2','LIMDE2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC2),'TMP_SCA2','COMSC2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC3),'TMP_SCA2','COMSC3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE3),'TMP_SCA2','TASDE3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINGIO),'TMP_SCA2','MINGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASCRE),'TMP_SCA2','TASCRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COSTOP),'TMP_SCA2','COSTOP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COSINV),'TMP_SCA2','COSINV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPELIQ),'TMP_SCA2','SPELIQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TTIPRIG),'TMP_SCA2','TIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMOPE),'TMP_SCA2','OPEESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPEFIT),'TMP_SCA2','SPEFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INAFIT),'TMP_SCA2','INAFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INPFIT),'TMP_SCA2','INPFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPEFOR),'TMP_SCA2','SPEFOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CODCON',this.w_OLDCON,'DATVAL',this.w_DATSAL,'IMPSAL',this.w_SALINI,'NUMGIO',0,'NUMOPE',this.w_TNUMOPE,'FLSPES'," ",'FLINTA'," ",'FLINTP'," ",'FLCAMB'," ",'GIOANN',this.w_GIOANN,'CODVAL',this.w_CODVAL,'DECTOT',this.w_DECTOT)
      insert into (i_cTable) (CODCON,DATVAL,IMPSAL,NUMGIO,NUMOPE,FLSPES,FLINTA,FLINTP,FLCAMB,GIOANN,CODVAL,DECTOT,BASINC,BASIND,BASLIQ,TASDE1,PERRIT,LIMDE1,COMSC1,TASDE2,LIMDE2,COMSC2,COMSC3,TASDE3,MINGIO,TASCRE,COSTOP,COSINV,SPELIQ,TIPRIG,OPEESE,SPEFIT,INAFIT,INPFIT,SPEFOR &i_ccchkf. );
         values (;
           this.w_OLDCON;
           ,this.w_DATSAL;
           ,this.w_SALINI;
           ,0;
           ,this.w_TNUMOPE;
           ," ";
           ," ";
           ," ";
           ," ";
           ,this.w_GIOANN;
           ,this.w_CODVAL;
           ,this.w_DECTOT;
           ,this.w_BASINC;
           ,this.w_BASIND;
           ,this.w_BASLIQ;
           ,this.w_TASDE1;
           ,this.w_PERRIT;
           ,this.w_LIMDE1;
           ,this.w_COMSC1;
           ,this.w_TASDE2;
           ,this.w_LIMDE2;
           ,this.w_COMSC2;
           ,this.w_COMSC3;
           ,this.w_TASDE3;
           ,this.w_MINGIO;
           ,this.w_TASCRE;
           ,this.w_COSTOP;
           ,this.w_COSINV;
           ,this.w_SPELIQ;
           ,this.w_TTIPRIG;
           ,this.w_NUMOPE;
           ,this.w_SPEFIT;
           ,this.w_INAFIT;
           ,this.w_INPFIT;
           ,this.w_SPEFOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_00E6C330()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMP_SCA2
    i_nConn=i_TableProp[this.TMP_SCA2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCA2_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_SCA2_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CODCON"+",DATVAL"+",IMPSAL"+",NUMGIO"+",NUMOPE"+",FLSPES"+",FLINTA"+",FLINTP"+",FLCAMB"+",GIOANN"+",CODVAL"+",DECTOT"+",BASINC"+",BASIND"+",BASLIQ"+",TASDE1"+",PERRIT"+",LIMDE1"+",COMSC1"+",TASDE2"+",LIMDE2"+",COMSC2"+",COMSC3"+",TASDE3"+",MINGIO"+",TASCRE"+",COSTOP"+",COSINV"+",SPELIQ"+",TIPRIG"+",OPEESE"+",SPEFIT"+",INAFIT"+",INPFIT"+",SPEFOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLDCON),'TMP_SCA2','CODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLDDAT),'TMP_SCA2','DATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIMPSAL),'TMP_SCA2','IMPSAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'TMP_SCA2','NUMGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TNUMOPE),'TMP_SCA2','NUMOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TFLSPES),'TMP_SCA2','FLSPES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TFLINTA),'TMP_SCA2','FLINTA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TFLINTP),'TMP_SCA2','FLINTP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TFLCAMB),'TMP_SCA2','FLCAMB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GIOANN),'TMP_SCA2','GIOANN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'TMP_SCA2','CODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DECTOT),'TMP_SCA2','DECTOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASINC),'TMP_SCA2','BASINC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASIND),'TMP_SCA2','BASIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BASLIQ),'TMP_SCA2','BASLIQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE1),'TMP_SCA2','TASDE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERRIT),'TMP_SCA2','PERRIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIMDE1),'TMP_SCA2','LIMDE1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC1),'TMP_SCA2','COMSC1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE2),'TMP_SCA2','TASDE2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LIMDE2),'TMP_SCA2','LIMDE2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC2),'TMP_SCA2','COMSC2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMSC3),'TMP_SCA2','COMSC3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASDE3),'TMP_SCA2','TASDE3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINGIO),'TMP_SCA2','MINGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TASCRE),'TMP_SCA2','TASCRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COSTOP),'TMP_SCA2','COSTOP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COSINV),'TMP_SCA2','COSINV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPELIQ),'TMP_SCA2','SPELIQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TTIPRIG),'TMP_SCA2','TIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMOPE),'TMP_SCA2','OPEESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPEFIT),'TMP_SCA2','SPEFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INAFIT),'TMP_SCA2','INAFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INPFIT),'TMP_SCA2','INPFIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPEFOR),'TMP_SCA2','SPEFOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CODCON',this.w_OLDCON,'DATVAL',this.w_OLDDAT,'IMPSAL',this.w_TIMPSAL,'NUMGIO',0,'NUMOPE',this.w_TNUMOPE,'FLSPES',this.w_TFLSPES,'FLINTA',this.w_TFLINTA,'FLINTP',this.w_TFLINTP,'FLCAMB',this.w_TFLCAMB,'GIOANN',this.w_GIOANN,'CODVAL',this.w_CODVAL,'DECTOT',this.w_DECTOT)
      insert into (i_cTable) (CODCON,DATVAL,IMPSAL,NUMGIO,NUMOPE,FLSPES,FLINTA,FLINTP,FLCAMB,GIOANN,CODVAL,DECTOT,BASINC,BASIND,BASLIQ,TASDE1,PERRIT,LIMDE1,COMSC1,TASDE2,LIMDE2,COMSC2,COMSC3,TASDE3,MINGIO,TASCRE,COSTOP,COSINV,SPELIQ,TIPRIG,OPEESE,SPEFIT,INAFIT,INPFIT,SPEFOR &i_ccchkf. );
         values (;
           this.w_OLDCON;
           ,this.w_OLDDAT;
           ,this.w_TIMPSAL;
           ,0;
           ,this.w_TNUMOPE;
           ,this.w_TFLSPES;
           ,this.w_TFLINTA;
           ,this.w_TFLINTP;
           ,this.w_TFLCAMB;
           ,this.w_GIOANN;
           ,this.w_CODVAL;
           ,this.w_DECTOT;
           ,this.w_BASINC;
           ,this.w_BASIND;
           ,this.w_BASLIQ;
           ,this.w_TASDE1;
           ,this.w_PERRIT;
           ,this.w_LIMDE1;
           ,this.w_COMSC1;
           ,this.w_TASDE2;
           ,this.w_LIMDE2;
           ,this.w_COMSC2;
           ,this.w_COMSC3;
           ,this.w_TASDE3;
           ,this.w_MINGIO;
           ,this.w_TASCRE;
           ,this.w_COSTOP;
           ,this.w_COSINV;
           ,this.w_SPELIQ;
           ,this.w_TTIPRIG;
           ,this.w_NUMOPE;
           ,this.w_SPEFIT;
           ,this.w_INAFIT;
           ,this.w_INPFIT;
           ,this.w_SPEFOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiunge Nuovo record di Stampa
    * --- Calcolo Interessi e Spese
    * --- N.Giorni Differenza tra la  Nuova data Attuale (che ha fatto scattare l'aggiornamento) e la data che sto inserendo ora
    this.w_TNUMDE1 = 0
    this.w_TNUMDE2 = 0
    this.w_TNUMDE3 = 0
    this.w_TINTDEB = 0
    this.w_TNUMCRE = 0
    this.w_TINTCRE = 0
    this.w_APPO3 = 0
    if this.w_TNUMGIO>=0 AND this.w_TIMPSAL<>0 AND this.w_GIOANN<>0
      if this.w_TFLSPES="S"
        this.w_TIMPSAL = this.w_TIMPSAL - (this.w_COSTOP * (IIF(this.w_TOTOPE-this.w_NUMOPE<0,0,this.w_TOTOPE-this.w_NUMOPE))) 
        this.w_TOTOPE = 0
      endif
      this.w_APPO = ABS(this.w_TIMPSAL)
      if this.w_TIMPSAL<0
        * --- Saldo Negativo; Aggiorna Numeri/Interessi Debitori
        if this.w_LIMDE1>0
          this.w_APPO1 = IIF(this.w_APPO<=this.w_LIMDE1, this.w_APPO, this.w_LIMDE1)
          this.w_TNUMDE1 = -cp_ROUND((this.w_APPO1 * this.w_TNUMGIO) / IIF(this.w_TCODVAL=g_CODLIR, 1000, 1), this.w_TDECTOT)
          this.w_APPO3 = cp_ROUND((this.w_TNUMDE1 * this.w_TASDE1) / (this.w_GIOANN * 100), this.w_TDECTOT)
          this.w_TINTDEB = ABS(this.w_APPO3)
          * --- Aggiunge eventuali Commissioni Max.Scoperto Calcolate sul minimo saldo del periodo
          if this.w_COMSC1<>0 AND (this.w_TDATVAL-this.w_INIVAL)>=this.w_MINGIO AND this.w_TFLINTP<>"S" AND this.w_TIMPSAL<=this.w_MAXSCO AND this.w_APPO<=this.w_LIMDE1
            this.w_IMPCOM1 = ABS(cp_ROUND((this.w_APPO * this.w_COMSC1) / 100, this.w_TDECTOT))
            this.w_MAXSCO = this.w_TIMPSAL
          else
            this.w_COMSC1 = 0
          endif
          if this.w_TFLINTP="S" 
            this.w_TNUMDE1 = -cp_ROUND(((this.w_APPO1+(this.w_IMPCOM1))* this.w_TNUMGIO) / IIF(this.w_TCODVAL=g_CODLIR, 1000, 1), this.w_TDECTOT)
          endif
          * --- Aggiunge costo operazione ad ogni fine periodo
          if this.w_APPO>this.w_LIMDE1 AND this.w_LIMDE2>this.w_LIMDE1
            this.w_APPO1 = IIF(this.w_APPO<=this.w_LIMDE2, (this.w_APPO-this.w_LIMDE1), (this.w_LIMDE2-this.w_LIMDE1))
            this.w_TNUMDE2 = -cp_ROUND((this.w_APPO1 * this.w_TNUMGIO) / IIF(this.w_TCODVAL=g_CODLIR, 1000, 1), this.w_TDECTOT)
            this.w_APPO3 = cp_ROUND((this.w_TNUMDE2 * this.w_TASDE2) / (this.w_GIOANN * 100), this.w_TDECTOT)
            this.w_TINTDEB = this.w_TINTDEB+ABS(this.w_APPO3)
            * --- Aggiunge eventuali Commissioni Max.Scoperto
            if this.w_COMSC2<>0 AND (this.w_TDATVAL-this.w_INIVAL)>=this.w_MINGIO AND this.w_TFLINTP<>"S" AND this.w_TIMPSAL<=this.w_MAXSCO AND this.w_APPO<=this.w_LIMDE2
              this.w_IMPCOM2 = ABS(cp_ROUND((this.w_APPO * this.w_COMSC2) / 100, this.w_TDECTOT))
              this.w_MAXSCO = this.w_TIMPSAL
            else
              this.w_COMSC2 = 0
            endif
            if this.w_TFLINTP="S" OR this.w_TFLINTA="S"
              this.w_TNUMDE2 = -cp_ROUND(((this.w_APPO1+this.w_IMPCOM2)* this.w_TNUMGIO) / IIF(this.w_TCODVAL=g_CODLIR, 1000, 1), this.w_TDECTOT)
            endif
          endif
        endif
        if this.w_APPO>MAX(this.w_LIMDE1, this.w_LIMDE2)
          this.w_APPO1 = (this.w_APPO-MAX(this.w_LIMDE1, this.w_LIMDE2))
          this.w_TNUMDE3 = -cp_ROUND((this.w_APPO1 * this.w_TNUMGIO) / IIF(this.w_TCODVAL=g_CODLIR, 1000, 1), this.w_TDECTOT)
          this.w_APPO3 = cp_ROUND((this.w_TNUMDE3 * this.w_TASDE3) / (this.w_GIOANN * 100), this.w_TDECTOT)
          this.w_TINTDEB = this.w_TINTDEB+ABS(this.w_APPO3)
          * --- Aggiunge eventuali Commissioni Max.Scoperto
          if this.w_COMSC3<>0 AND (this.w_TDATVAL-this.w_INIVAL)>=this.w_MINGIO AND this.w_TFLINTP<>"S" AND this.w_TIMPSAL<=this.w_MAXSCO 
            this.w_IMPCOM3 = abs(cp_ROUND((this.w_APPO * this.w_COMSC3) / 100, this.w_TDECTOT))
            this.w_MAXSCO = this.w_TIMPSAL
          else
            this.w_COMSC3 = 0
          endif
          if this.w_TFLINTP="S" OR this.w_TFLINTA="S"
            this.w_TNUMDE3 = -cp_ROUND(((this.w_APPO1+this.w_IMPCOM3)* this.w_TNUMGIO) / IIF(this.w_TCODVAL=g_CODLIR, 1000, 1), this.w_TDECTOT)
          endif
        endif
        if this.w_TFLINTP="S" 
          this.w_TIMPSAL = this.w_TIMPSAL - ( this.w_IMPCOM1+this.w_IMPCOM2+this.w_IMPCOM3)
          this.w_MAXSCO = 0
        else
          this.w_TOTOPE = this.w_TOTOPE+this.w_TNUMOPE
        endif
      else
        * --- Saldo Positivo; Aggiorna Numeri Creditori
        this.w_TNUMCRE = cp_ROUND((this.w_APPO * this.w_TNUMGIO) / IIF(this.w_TCODVAL=g_CODLIR, 1000, 1), this.w_TDECTOT)
        this.w_TINTCRE = cp_ROUND((this.w_TNUMCRE * this.w_TASCRE) / (this.w_GIOANN * 100), this.w_TDECTOT)
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica per eventuale Conteggio Interessi/Spese
    * --- Estrae tutte le Registrazioni Di Calcolo Competenze
    this.w_OLDCON = "xxx@@@zzz###"
    this.w_INIVAL = i_INIDAT
    this.w_IMPSPE = 0
    * --- Select from TMP_SCAL
    i_nConn=i_TableProp[this.TMP_SCAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCAL_idx,2],.t.,this.TMP_SCAL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_SCAL ";
          +" where TIPREC='C' OR (TIPREC='M' AND (FLCOMP='I' OR FLCOMP='S'))";
          +" order by CODCON, DATVAL,TIPREC";
           ,"_Curs_TMP_SCAL")
    else
      select * from (i_cTable);
       where TIPREC="C" OR (TIPREC="M" AND (FLCOMP="I" OR FLCOMP="S"));
       order by CODCON, DATVAL,TIPREC;
        into cursor _Curs_TMP_SCAL
    endif
    if used('_Curs_TMP_SCAL')
      select _Curs_TMP_SCAL
      locate for 1=1
      do while not(eof())
      this.w_CODCON = CODCON
      this.w_DECTOT = NVL(DECTOT, 0)
      this.w_CODVAL = NVL(CODVAL, g_PERVAL)
      this.w_TIPREC = NVL(TIPREC, " ")
      this.w_FLCOMP = NVL(FLCOMP," ")
      this.w_FLTIPO = NVL(FLTIPO, " ")
      this.w_DATVAL = CP_TODATE(DATVAL)
      this.w_IMPCRE = NVL(IMPCRE,0)
      this.w_IMPDEB = NVL(IMPDEB,0)
      * --- Cambio Conto Corrente (Si presume sia sempre TIPREC='C')
      if (this.w_OLDCON<>this.w_CODCON AND this.w_DATVAL>=this.oParentObject.w_DATINI) OR this.w_TIPREC="C" 
        * --- Dati del Conto
        this.w_IMPSPE = NVL(COSINV, 0) + NVL(SPELIQ, 0)
        this.w_BASINC = NVL(BASINC," ")
        this.w_BASIND = NVL(BASIND," ")
        this.w_BASLIQ = NVL(BASLIQ," ")
        this.w_INIVAL = this.w_DATVAL
        if this.w_OLDCON<>this.w_CODCON
          this.w_ANNO = YEAR(this.oParentObject.w_DATFIN)- YEAR(this.oParentObject.w_DATINI)
          this.w_MESEFIN = IIF(this.w_ANNO>0,12,MONTH(this.oParentObject.w_DATFIN))
          this.w_ANNO = IIF(this.w_ANNO=0,1,this.w_ANNO+1)
          this.w_CONT = 0
          this.w_INIVAL = cp_CharToDate(LEFT(DTOC(this.w_DATVAL),6)+ALLTRIM(STR(YEAR(this.oParentObject.w_DATINI)+this.w_CONT)))
          this.w_MESEINI = MONTH(this.oParentObject.w_DATINI)
          * --- Inserisce per ciascuno il Mese in cui deve essere effettuato il Conteggio Interessi / Spese
          do while this.w_ANNO>0
            FOR L_m = 1 TO 12
            if L_m>=this.w_MESEINI AND L_m<=this.w_MESEFIN 
              FOR L_i = 1 TO 3
              this.w_APPO = IIF(L_i=1, this.w_BASINC, IIF(L_i=2, this.w_BASIND, this.w_BASLIQ))
              this.w_OK = .F.
              do case
                case this.w_APPO="M" OR (this.w_APPO="B" AND MOD(L_m, 2)=0) OR (this.w_APPO="S" AND L_m=6) OR L_m=12
                  * --- Mensile o Bimestrale o Semestrale Ultimo Mese
                  this.w_OK = .T.
                case (this.w_APPO="T" AND (L_m=3 OR L_m=6 OR L_m=9)) OR (this.w_APPO="Q" AND (L_m=4 OR L_m=8))
                  * --- Altri Casi
                  this.w_OK = .T.
              endcase
              if this.w_OK=.T.
                this.w_APPO1 = L_m
                this.w_APPO2 = IIF(L_i=3, "S", "I")
                this.w_APPO3 = IIF(L_i=1, "C", "D")
                INSERT INTO COMPET ;
                (CODCON, INIVAL, NUMPER, IMPCRE, IMPDEB, FLCOMP, FLTIPO, CODVAL, DECTOT) VALUES ;
                (this.w_CODCON, this.w_INIVAL, this.w_APPO1, 0, IIF(this.w_APPO2="S", this.w_IMPSPE, 0), this.w_APPO2, this.w_APPO3, this.w_CODVAL, this.w_DECTOT) 
              endif
              ENDFOR
            endif
            ENDFOR
            this.w_ANNO = this.w_ANNO-1
            this.w_CONT = this.w_CONT +1
            this.w_MESEINI = 1
            this.w_MESEFIN = IIF(this.w_ANNO>1,12,MONTH(this.oParentObject.w_DATFIN))
            this.w_INIVAL = cp_CharToDate(LEFT(DTOC(this.w_DATVAL),6)+ALLTRIM(STR(YEAR(this.oParentObject.w_DATINI)+this.w_CONT)))
          enddo
        else
          * --- Cambio Condizioni Aggiorno Cursore
          SELECT COMPET
          GO TOP
          SCAN FOR FLCOMP="S"
          if COMPET.NUMPER>=MONTH(this.w_INIVAL) AND COMPET.FLCOMP="S"
            REPLACE COMPET.IMPDEB WITH this.w_IMPSPE
          endif
          ENDSCAN
        endif
        * --- Inizializza Contatore Operazioni sul Conto dall'Inizio Anno
        this.w_OLDCON = this.w_CODCON
      endif
      * --- Elimina tutti i Record gia' Inseriti come registrazioni di C/C
      if this.w_TIPREC="M" AND this.w_FLCOMP $ "IS" AND (this.w_IMPCRE<>0 OR this.w_IMPDEB<>0)
        this.w_APPO1 = MONTH(this.w_DATVAL)
        if this.w_APPO1>=MONTH(this.oParentObject.w_DATINI) AND this.w_APPO1<=MONTH(this.oParentObject.w_DATFIN) 
          DELETE FROM COMPET WHERE CODCON=this.w_CODCON AND NUMPER=this.w_APPO1 AND FLCOMP=this.w_FLCOMP
        endif
      endif
        select _Curs_TMP_SCAL
        continue
      enddo
      use
    endif
    * --- Inserisce nel TMP principale tutte le Registrazioni Associate a Spese / Interessi ancora da inserire
    if USED("COMPET")
      SELECT COMPET
      GO TOP
      SCAN FOR NOT EMPTY(CODCON) 
      this.w_CODCON = CODCON
      this.w_INIVAL = INIVAL
      * --- Calcola Data di Fine Periodo
      this.w_APPO = NUMPER+1
      this.w_APPO1 = YEAR(this.w_INIVAL)
      if this.w_APPO>12
        this.w_APPO = 1
        this.w_APPO1 = this.w_APPO1 + 1
      endif
      this.w_APPO = RIGHT("00"+ALLTRIM(STR(this.w_APPO)), 2)
      this.w_APPO1 = ALLTRIM(STR(this.w_APPO1))
      this.w_DATVAL = cp_CharToDate("01-"+this.w_APPO+"-"+this.w_APPO1) - 1
      * --- Se la Data di riferimento e' maggiore della data di Apertura del Conto ed e' compresa nell'intervallo
      if this.w_DATVAL>=this.oParentObject.w_DATINI AND this.w_DATVAL<=this.oParentObject.w_DATFIN
        this.w_DECTOT = NVL(DECTOT, 0)
        this.w_CODVAL = NVL(CODVAL, g_PERVAL)
        this.w_TIPREC = "S"
        this.w_FLCOMP = FLCOMP
        this.w_FLTIPO = FLTIPO
        this.w_IMPCRE = NVL(IMPCRE,0)
        this.w_IMPDEB = NVL(IMPDEB,0)
        * --- Try
        local bErr_00FFF8A0
        bErr_00FFF8A0=bTrsErr
        this.Try_00FFF8A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Errore in inserimento TMP_SCAL")
        endif
        bTrsErr=bTrsErr or bErr_00FFF8A0
        * --- End
      endif
      SELECT COMPET
      ENDSCAN
    endif
  endproc
  proc Try_00FFF8A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMP_SCAL
    i_nConn=i_TableProp[this.TMP_SCAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SCAL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_SCAL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CODCON"+",DATVAL"+",TIPREC"+",CPROWORD"+",IMPCRE"+",IMPDEB"+",IMPCOM"+",FLADDC"+",FLCOMP"+",FLTIPO"+",CODVAL"+",DECTOT"+",ANTERGATE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCON),'TMP_SCAL','CODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'TMP_SCAL','DATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREC),'TMP_SCAL','TIPREC');
      +","+cp_NullLink(cp_ToStrODBC(1),'TMP_SCAL','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCRE),'TMP_SCAL','IMPCRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDEB),'TMP_SCAL','IMPDEB');
      +","+cp_NullLink(cp_ToStrODBC(0),'TMP_SCAL','IMPCOM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCAL','FLADDC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLCOMP),'TMP_SCAL','FLCOMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLTIPO),'TMP_SCAL','FLTIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'TMP_SCAL','CODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DECTOT),'TMP_SCAL','DECTOT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'TMP_SCAL','ANTERGATE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CODCON',this.w_CODCON,'DATVAL',this.w_DATVAL,'TIPREC',this.w_TIPREC,'CPROWORD',1,'IMPCRE',this.w_IMPCRE,'IMPDEB',this.w_IMPDEB,'IMPCOM',0,'FLADDC'," ",'FLCOMP',this.w_FLCOMP,'FLTIPO',this.w_FLTIPO,'CODVAL',this.w_CODVAL,'DECTOT',this.w_DECTOT)
      insert into (i_cTable) (CODCON,DATVAL,TIPREC,CPROWORD,IMPCRE,IMPDEB,IMPCOM,FLADDC,FLCOMP,FLTIPO,CODVAL,DECTOT,ANTERGATE &i_ccchkf. );
         values (;
           this.w_CODCON;
           ,this.w_DATVAL;
           ,this.w_TIPREC;
           ,1;
           ,this.w_IMPCRE;
           ,this.w_IMPDEB;
           ,0;
           ," ";
           ,this.w_FLCOMP;
           ,this.w_FLTIPO;
           ,this.w_CODVAL;
           ,this.w_DECTOT;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Movimenti di Conto Corrente
    this.w_INIVAL = i_INIDAT
    this.w_OLDVAL = "@@@"
    * --- Try
    local bErr_01035FD8
    bErr_01035FD8=bTrsErr
    this.Try_01035FD8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Impossibile aggiornare i movimenti di conto corrente",,"")
    endif
    bTrsErr=bTrsErr or bErr_01035FD8
    * --- End
  endproc
  proc Try_01035FD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT AGGCOM
    GO TOP
    SCAN FOR NOT EMPTY(CODCON) AND TOTIMP<>0 AND TIPCOM $ "APS"
    this.w_CODCON = CODCON
    this.w_DATVAL = CP_TODATE(DATVAL)
    this.w_TIPCOM = TIPCOM
    this.w_TOTIMP = TOTIMP
    this.w_CODVAL = CODVAL
    this.w_DECTOT = DECTOT
    if this.w_INIVAL<>this.w_DATVAL OR this.w_OLDVAL<>this.w_CODVAL
      * --- Nuova registrazione
      this.w_INIVAL = this.w_DATVAL
      this.w_OLDVAL = this.w_CODVAL
      this.w_CCROWRIF = -1
      this.w_CCDATREG = this.w_DATVAL
      this.w_CC__ANNO = STR(YEAR(this.w_CCDATREG),4,0)
      this.w_CCCODVAL = this.w_CODVAL
      this.w_CCCAOVAL = GETCAM(this.w_CCCODVAL, this.w_CCDATREG, 0)
      this.w_CCSERIAL = SPACE(10)
      this.w_CCNUMREG = 0
      this.w_CCDESCRI = this.oParentObject.w_DESSUP
      i_Conn=i_TableProp[this.CCM_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SERMOVCO", "i_codazi,w_CCSERIAL")
      cp_NextTableProg(this, i_Conn, "PROMOVCO", "i_codazi,w_CC__ANNO,w_CCNUMREG")
      * --- Insert into CCM_MAST
      i_nConn=i_TableProp[this.CCM_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CCM_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCM_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCSERIAL"+",CCROWRIF"+",CC__ANNO"+",CCNUMREG"+",CCDATREG"+",CCNUMDOC"+",CCALFDOC"+",CCDATDOC"+",CCDESCRI"+",CCCODVAL"+",CCCAOVAL"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CCSERIAL),'CCM_MAST','CCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCROWRIF),'CCM_MAST','CCROWRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CC__ANNO),'CCM_MAST','CC__ANNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCNUMREG),'CCM_MAST','CCNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDATREG),'CCM_MAST','CCDATREG');
        +","+cp_NullLink(cp_ToStrODBC(0),'CCM_MAST','CCNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(Space(10)),'CCM_MAST','CCALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDATREG),'CCM_MAST','CCDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CCM_MAST','CCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODVAL),'CCM_MAST','CCCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCAOVAL),'CCM_MAST','CCCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CCM_MAST','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'CCM_MAST','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(0),'CCM_MAST','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'CCM_MAST','UTDV');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_CCSERIAL,'CCROWRIF',this.w_CCROWRIF,'CC__ANNO',this.w_CC__ANNO,'CCNUMREG',this.w_CCNUMREG,'CCDATREG',this.w_CCDATREG,'CCNUMDOC',0,'CCALFDOC',Space(10),'CCDATDOC',this.w_CCDATREG,'CCDESCRI',this.w_CCDESCRI,'CCCODVAL',this.w_CCCODVAL,'CCCAOVAL',this.w_CCCAOVAL,'UTCC',i_CODUTE)
        insert into (i_cTable) (CCSERIAL,CCROWRIF,CC__ANNO,CCNUMREG,CCDATREG,CCNUMDOC,CCALFDOC,CCDATDOC,CCDESCRI,CCCODVAL,CCCAOVAL,UTCC,UTDC,UTCV,UTDV &i_ccchkf. );
           values (;
             this.w_CCSERIAL;
             ,this.w_CCROWRIF;
             ,this.w_CC__ANNO;
             ,this.w_CCNUMREG;
             ,this.w_CCDATREG;
             ,0;
             ,Space(10);
             ,this.w_CCDATREG;
             ,this.w_CCDESCRI;
             ,this.w_CCCODVAL;
             ,this.w_CCCAOVAL;
             ,i_CODUTE;
             ,SetInfoDate( g_CALUTD );
             ,0;
             ,cp_CharToDate("  -  -    ");
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
    endif
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_CCNUMCOR = this.w_CODCON
    do case
      case this.w_TIPCOM="A"
        * --- Interessi Attivi
        this.w_CCCODCAU = this.oParentObject.w_CAUINA
        this.w_CCIMPCRE = this.w_TOTIMP
        this.w_CCIMPDEB = 0
        this.w_CCFLCRED = "+"
        this.w_CCFLDEBI = " "
      case this.w_TIPCOM="P"
        * --- Interessi Passivi
        this.w_CCCODCAU = this.oParentObject.w_CAUINP
        this.w_CCIMPCRE = 0
        this.w_CCIMPDEB = this.w_TOTIMP
        this.w_CCFLCRED = " "
        this.w_CCFLDEBI = "+"
      otherwise
        * --- Spese
        this.w_CCCODCAU = this.oParentObject.w_CAUSPE
        this.w_CCIMPCRE = 0
        this.w_CCIMPDEB = this.w_TOTIMP
        this.w_CCFLCRED = " "
        this.w_CCFLDEBI = "+"
    endcase
    * --- Insert into CCM_DETT
    i_nConn=i_TableProp[this.CCM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCM_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCSERIAL"+",CCROWRIF"+",CPROWNUM"+",CPROWORD"+",CCCODCAU"+",CCNUMCOR"+",CCDATVAL"+",CCIMPCRE"+",CCIMPDEB"+",CCCOSCOM"+",CCCOMVAL"+",CCFLCRED"+",CCFLDEBI"+",CCFLCOMM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCSERIAL),'CCM_DETT','CCSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCROWRIF),'CCM_DETT','CCROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CCM_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'CCM_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODCAU),'CCM_DETT','CCCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCNUMCOR),'CCM_DETT','CCNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDATREG),'CCM_DETT','CCDATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCIMPCRE),'CCM_DETT','CCIMPCRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCIMPDEB),'CCM_DETT','CCIMPDEB');
      +","+cp_NullLink(cp_ToStrODBC(0),'CCM_DETT','CCCOSCOM');
      +","+cp_NullLink(cp_ToStrODBC(0),'CCM_DETT','CCCOMVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCRED),'CCM_DETT','CCFLCRED');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDEBI),'CCM_DETT','CCFLDEBI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'CCM_DETT','CCFLCOMM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_CCSERIAL,'CCROWRIF',this.w_CCROWRIF,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'CCCODCAU',this.w_CCCODCAU,'CCNUMCOR',this.w_CCNUMCOR,'CCDATVAL',this.w_CCDATREG,'CCIMPCRE',this.w_CCIMPCRE,'CCIMPDEB',this.w_CCIMPDEB,'CCCOSCOM',0,'CCCOMVAL',0,'CCFLCRED',this.w_CCFLCRED)
      insert into (i_cTable) (CCSERIAL,CCROWRIF,CPROWNUM,CPROWORD,CCCODCAU,CCNUMCOR,CCDATVAL,CCIMPCRE,CCIMPDEB,CCCOSCOM,CCCOMVAL,CCFLCRED,CCFLDEBI,CCFLCOMM &i_ccchkf. );
         values (;
           this.w_CCSERIAL;
           ,this.w_CCROWRIF;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_CCCODCAU;
           ,this.w_CCNUMCOR;
           ,this.w_CCDATREG;
           ,this.w_CCIMPCRE;
           ,this.w_CCIMPDEB;
           ,0;
           ,0;
           ,this.w_CCFLCRED;
           ,this.w_CCFLDEBI;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into COC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_CCFLCRED,'BASALCRE','this.w_CCIMPCRE',this.w_CCIMPCRE,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_CCFLDEBI,'BASALDEB','this.w_CCIMPDEB',this.w_CCIMPDEB,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"BASALCRE ="+cp_NullLink(i_cOp1,'COC_MAST','BASALCRE');
      +",BASALDEB ="+cp_NullLink(i_cOp2,'COC_MAST','BASALDEB');
          +i_ccchkf ;
      +" where ";
          +"BACODBAN = "+cp_ToStrODBC(this.w_CCNUMCOR);
             )
    else
      update (i_cTable) set;
          BASALCRE = &i_cOp1.;
          ,BASALDEB = &i_cOp2.;
          &i_ccchkf. ;
       where;
          BACODBAN = this.w_CCNUMCOR;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    SELECT AGGCOM
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Aggiornamento completato",,"")
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CCM_DETT'
    this.cWorkTables[2]='CCM_MAST'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='*TMP_SCA2'
    this.cWorkTables[5]='*TMP_SCAL'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_TMP_SCAL')
      use in _Curs_TMP_SCAL
    endif
    if used('_Curs_TMP_SCA2')
      use in _Curs_TMP_SCA2
    endif
    if used('_Curs_TMP_SCA2')
      use in _Curs_TMP_SCA2
    endif
    if used('_Curs_TMP_SCAL')
      use in _Curs_TMP_SCAL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
