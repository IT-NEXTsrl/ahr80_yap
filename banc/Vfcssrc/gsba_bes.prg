* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_bes                                                        *
*              Elabora schede conto corrente                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_91]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-16                                                      *
* Last revis.: 2001-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pSeriale,pRowrif
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsba_bes",oParentObject,m.pTipo,m.pSeriale,m.pRowrif)
return(i_retval)

define class tgsba_bes as StdBatch
  * --- Local variables
  pTipo = space(1)
  pSeriale = space(10)
  pRowrif = 0
  w_ANNOPRE = space(4)
  w_ANNO = space(4)
  w_MOVIMENTO = .NULL.
  w_DATAINI = ctod("  /  /  ")
  w_IMPCREP = 0
  w_IMPCRED = 0
  w_IMPCREDI = 0
  w_SALDO = 0
  w_IMPDEBP = 0
  w_IMPDEBD = 0
  w_IMPDEBDI = 0
  w_ZOOM = space(10)
  w_MOVINIZ = space(1)
  w_DECIMI = 0
  * --- WorkFile variables
  VALUTE_idx=0
  CCC_MAST_idx=0
  COC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Visualizza (da GSBA_KVC)
    * --- Parametri: V=Visualizza 
    * --- Anno attuale e anno precedente
    this.w_ANNO = STR(YEAR(this.oParentObject.w_DATA1),4,0)
    this.w_ANNOPRE = STR(YEAR(this.oParentObject.w_DATA1)-1,4,0)
    * --- Data di Riferimento per Saldo Iniziale
    this.w_DATAINI = this.oParentObject.w_DATA1-1
    do case
      case this.pTipo="V"
        * --- Lanciato dalla Visualizzazione
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo="O" AND NOT EMPTY(this.pSeriale)
        if this.pRowrif=-1
          * --- Movimento di Conto Corrente manuale
          this.w_MOVIMENTO = GSBA_MMC()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_MOVIMENTO.bSec1)
            i_retcode = 'stop'
            return
          endif
          this.w_MOVIMENTO.w_CCSERIAL = this.oParentObject.w_SERIALE
          this.w_MOVIMENTO.QueryKeySet("CCSERIAL="+ cp_ToStrODBC(this.oParentObject.w_SERIALE),"")     
          this.w_MOVIMENTO.LoadRecWarn()     
        else
          * --- Apro Registrazione di Primanota
          gsar_bzp(this,this.oParentObject.w_SERIALE)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla Visualizzazione
    if EMPTY(this.oParentObject.w_CODICE)
      ah_ErrorMsg("Inserire un conto corrente",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Legge Saldo Iniziale
    vq_exec("..\BANC\exe\QUERY\GSBAISEC.vqr",this,"TOTVISU")
    SELECT NUMCOR,SUM(IMPCRE) AS IMPCRE,SUM(IMPDEB) AS IMPDEB,SUM(IMPCOM) AS IMPCOM FROM TOTVISU;
    GROUP BY NUMCOR INTO CURSOR TOTVISU
    this.oParentObject.w_SALDOIN = (nvl(TOTVISU.IMPCRE,0) -(nvl(TOTVISU.IMPDEB,0)+TOTVISU.IMPCOM))
    * --- LANCIA AL NOTIFYEVENT (da GSTE_SEC)
    This.oParentObject.NotifyEvent("Esegui")
    this.w_DECIMI = this.oParentObject.w_DECTOT
    * --- Cambio gli importi non in moneta di Conto nel cursore a VIDEO
    this.w_ZOOM = this.oParentObject.w_ZoomScad
    * --- Mi posizione sull'oggettino Zoom per fare i totali
    SELECT ( this.w_ZOOM.cCursor )
    SUM CCIMPCRE TO this.oParentObject.w_TOTDAR
    SUM CCIMPDEB TO this.oParentObject.w_TOTAVE
    this.oParentObject.w_SALDOFIN = this.oParentObject.w_SALDOIN+this.oParentObject.w_TOTDAR-this.oParentObject.w_TOTAVE
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura dei Cursori
    if used("totvisu")
      select totvisu
      use
    endif
  endproc


  proc Init(oParentObject,pTipo,pSeriale,pRowrif)
    this.pTipo=pTipo
    this.pSeriale=pSeriale
    this.pRowrif=pRowrif
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CCC_MAST'
    this.cWorkTables[3]='COC_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pSeriale,pRowrif"
endproc
