* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_bck                                                        *
*              Controlli finali sulle causali movimenti                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_16]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-14                                                      *
* Last revis.: 2002-01-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsba_bck",oParentObject)
return(i_retval)

define class tgsba_bck as StdBatch
  * --- Local variables
  w_Test = .f.
  w_MESS = space(100)
  w_OK = .f.
  w_DOPPIO = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lanciato da GSBA_MCT esegue controlli sulle Causali Movimenti
    this.w_OK = .T.
    this.w_Test = .F.
    if this.oParentObject.w_CATIPCON<>"S" AND this.oParentObject.w_CAFLCOMP="N"
      SELECT (this.oParentObject.cTrsName)
      scan for not empty(NVL(t_CANUMCOR," "))
      this.w_Test = .T.
      endscan
      if this.w_Test=.F.
        this.w_MESS = ah_MsgFormat("Deve essere specificato almeno un conto corrente")
        this.w_OK = .F.
      endif
    endif
    if this.w_OK
      * --- Utilizzo il temporaneo  per capire se � stato inserito un codice ripetuto. 
      *     Questo per evitare il messaggio di chiave gi� utilizzata e che vengano sbiancati i codici 
      *     percedentemente inseriti.
      select Count(*) As CONTA, t_CANUMCOR from (this.oParentObject.cTrsName) ; 
 Where t_CANUMCOR Is Not Null And not deleted(); 
 group by t_CANUMCOR having CONTA >= 2 into cursor ELENCO
      this.w_DOPPIO = Nvl ( Elenco.t_CANUMCOR , "" )
       
 Select ELENCO 
 Use
      if Not Empty ( this.w_DOPPIO )
        this.w_MESS = ah_MsgFormat("Conto corrente %1 ripetuto",ALLTRIM(this.w_DOPPIO))
        this.w_OK = .F.
      endif
    endif
    if NOT this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
