* --- Container for functions
* --- START CALCFEST
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: calcfest                                                        *
*              Calcola data valuta                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_16]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-15                                                      *
* Last revis.: 2001-11-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func calcfest
param pData,pCale,pNUMCOR,pCODCAU,pTIPCAU,pREWFOR

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_DATA
  m.w_DATA=ctod("  /  /  ")
  private w_OKDATA
  m.w_OKDATA=ctod("  /  /  ")
  private w_GIORVA
  m.w_GIORVA=0
  private w_CALE
  m.w_CALE=space(8)
  private w_VALFIR
  m.w_VALFIR=space(1)
* --- WorkFile variables
  private FES_DETT_idx
  FES_DETT_idx=0
  private CCC_DETT_idx
  CCC_DETT_idx=0
  private CCC_MAST_idx
  CCC_MAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "calcfest"
if vartype(__calcfest_hook__)='O'
  __calcfest_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'calcfest('+Transform(pData)+','+Transform(pCale)+','+Transform(pNUMCOR)+','+Transform(pCODCAU)+','+Transform(pTIPCAU)+','+Transform(pREWFOR)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calcfest')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if calcfest_OpenTables()
  calcfest_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'calcfest('+Transform(pData)+','+Transform(pCale)+','+Transform(pNUMCOR)+','+Transform(pCODCAU)+','+Transform(pTIPCAU)+','+Transform(pREWFOR)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calcfest')
Endif
*--- Activity log
if vartype(__calcfest_hook__)='O'
  __calcfest_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure calcfest_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Calcola Data Feriale (Utilizzato in Tesoreria per calcolare la Data Valuta)
  * --- Parametri:  
  * --- pData - Data da Valutare
  *     pCale - Eventuale Calendario Festivita'
  *     pNUMCOR Numero Conto Corrente
  *     pCODCAU Causale di tesoreria
  *     pTIPCAU   Tipo Causale Conti (Banche Normali/Compensazione)
  *     pREWFOR avanti (1) o indietro (-1) nella ricerca primo giorno feriale
  * --- Ritorna: Prima Data Utile
  * --- Se non lo passo vale UNO (valore di default)
  if Type("pREWFOR")="L"
    m.pREWFOR = 1
  endif
  m.w_DATA = CP_TODATE(m.pData)
  m.w_CALE = NVL(m.pCale,SPACE(8))
  m.w_OKDATA = cp_CharToDate("  -  -  ")
  m.w_VALFIR = "N"
  m.w_GIORVA = 0
  if NOT EMPTY(m.w_DATA)
    if NOT EMPTY(m.pNUMCOR) AND NOT EMPTY(m.pCODCAU)
      * --- Cerca flag valuta fissa se non passato dal programma chiamante (Praticamente dalla Manutenzione Mov.Tesoreria)
      if m.pTIPCAU="S"
        * --- Read from CCC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[CCC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[CCC_MAST_idx,2],.t.,CCC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAVALFI2,CAGIOVA2"+;
            " from "+i_cTable+" CCC_MAST where ";
                +"CACODICE = "+cp_ToStrODBC(m.pCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAVALFI2,CAGIOVA2;
            from (i_cTable) where;
                CACODICE = m.pCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_VALFIR = NVL(cp_ToDate(_read_.CAVALFI2),cp_NullValue(_read_.CAVALFI2))
          m.w_GIORVA = NVL(cp_ToDate(_read_.CAGIOVA2),cp_NullValue(_read_.CAGIOVA2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from CCC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[CCC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[CCC_DETT_idx,2],.t.,CCC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAVALFIS,CAGIOVAL"+;
            " from "+i_cTable+" CCC_DETT where ";
                +"CACODICE = "+cp_ToStrODBC(m.pCODCAU);
                +" and CANUMCOR = "+cp_ToStrODBC(m.pNUMCOR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAVALFIS,CAGIOVAL;
            from (i_cTable) where;
                CACODICE = m.pCODCAU;
                and CANUMCOR = m.pNUMCOR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_VALFIR = NVL(cp_ToDate(_read_.CAVALFIS),cp_NullValue(_read_.CAVALFIS))
          m.w_GIORVA = NVL(cp_ToDate(_read_.CAGIOVAL),cp_NullValue(_read_.CAGIOVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Setto parametro in funzione del segno dei giorni indicati
      if m.w_GIORVA>=0
        m.pREWFOR = 1
      else
        m.pREWFOR = -1
      endif
      * --- Aggiunge eventuali Gioni Fissi
      m.w_DATA = m.w_DATA + m.w_GIORVA
    endif
    do while m.w_VALFIR<>"S"
      * --- Cicla finche trova la prima data utile se calcolo valuta Fissa
      if DOW(m.w_DATA)=7 OR DOW(m.w_DATA)=1
        * --- Cade di Sabato o Domenica - I giorni valuta possono essere negativi quindi risalgo alla data feriale antecedente
        m.w_DATA = m.w_DATA + m.pREWFOR
      else
        * --- Verifica se Festivo infrasettimanale
        m.w_OKDATA = cp_CharToDate("  -  -  ")
        if NOT EMPTY(m.w_CALE)
          * --- Read from FES_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[FES_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[FES_DETT_idx,2],.t.,FES_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "FEDATFES"+;
              " from "+i_cTable+" FES_DETT where ";
                  +"FECODICE = "+cp_ToStrODBC(m.w_CALE);
                  +" and FEDATFES = "+cp_ToStrODBC(m.w_DATA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              FEDATFES;
              from (i_cTable) where;
                  FECODICE = m.w_CALE;
                  and FEDATFES = m.w_DATA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_OKDATA = NVL(cp_ToDate(_read_.FEDATFES),cp_NullValue(_read_.FEDATFES))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if EMPTY(m.w_OKDATA) OR m.w_OKDATA<>m.w_DATA
          * --- Data OK
          EXIT
        else
          m.w_DATA = m.w_DATA + m.pREWFOR
        endif
      endif
    enddo
  endif
  i_retcode = 'stop'
  i_retval = m.w_DATA
  return
endproc


  function calcfest_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='FES_DETT'
    i_cWorkTables[2]='CCC_DETT'
    i_cWorkTables[3]='CCC_MAST'
    return(cp_OpenFuncTables(3))
* --- END CALCFEST