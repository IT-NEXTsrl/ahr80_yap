* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_kvc                                                        *
*              Visualizza schede conto corrente                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_66]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-16                                                      *
* Last revis.: 2009-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsba_kvc",oParentObject))

* --- Class definition
define class tgsba_kvc as StdForm
  Top    = 5
  Left   = 8

  * --- Standard Properties
  Width  = 792
  Height = 464
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-03-10"
  HelpContextID=48924265
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  CCM_DETT_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsba_kvc"
  cComment = "Visualizza schede conto corrente"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODICE = space(15)
  o_CODICE = space(15)
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_DESCRI = space(35)
  w_SALDOIN = 0
  o_SALDOIN = 0
  w_INICRE = 0
  w_TOTDAR = 0
  w_TOTAVE = 0
  w_SALDOFIN = 0
  o_SALDOFIN = 0
  w_SERIALE = space(10)
  w_ROWRIF = 0
  w_OBTEST = ctod('  /  /  ')
  w_CODICE1 = space(15)
  w_SIMVAL = space(5)
  w_INIDEB = 0
  w_DESCRI1 = space(45)
  w_FINCRE = 0
  w_FINDEB = 0
  w_ZoomScad = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsba_kvcPag1","gsba_kvc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomScad = this.oPgFrm.Pages(1).oPag.ZoomScad
    DoDefault()
    proc Destroy()
      this.w_ZoomScad = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='CCM_DETT'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='VALUTE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE=space(15)
      .w_VALUTA=space(3)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_DESCRI=space(35)
      .w_SALDOIN=0
      .w_INICRE=0
      .w_TOTDAR=0
      .w_TOTAVE=0
      .w_SALDOFIN=0
      .w_SERIALE=space(10)
      .w_ROWRIF=0
      .w_OBTEST=ctod("  /  /  ")
      .w_CODICE1=space(15)
      .w_SIMVAL=space(5)
      .w_INIDEB=0
      .w_DESCRI1=space(45)
      .w_FINCRE=0
      .w_FINDEB=0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODICE))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_VALUTA))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_DATA1 = g_INIESE
        .w_DATA2 = g_FINESE
      .oPgFrm.Page1.oPag.ZoomScad.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_Msgformat(iif(.w_saldoin>=0,'Saldo al %1 a credito:','Saldo al %1 a debito:'),dtoc( .w_DATA1-1 )))
          .DoRTCalc(7,8,.f.)
        .w_INICRE = ABS(.w_SALDOIN)
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(AH_Msgformat(iif(.w_saldofin>=0,'Saldo al %1 a credito:','Saldo al %1 a debito:'),dtoc( .w_DATA2 )))
          .DoRTCalc(10,12,.f.)
        .w_SERIALE = Nvl( .w_ZoomScad.getVar('CCSERIAL') , Space(10))
        .w_ROWRIF = Nvl( .w_ZoomScad.getVar('CCROWRIF') , 0 )
        .w_OBTEST = i_INIDAT
        .w_CODICE1 = .w_CODICE
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODICE1))
          .link_1_26('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_INIDEB = ABS(.w_SALDOIN)
        .w_DESCRI1 = .w_ZoomScad.getVar('CCDESCRI')
        .w_FINCRE = ABS(.w_SALDOFIN)
        .w_FINDEB = ABS(.w_SALDOFIN)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_3('Full')
        .DoRTCalc(3,3,.t.)
        if .o_VALUTA<>.w_VALUTA
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_Msgformat(iif(.w_saldoin>=0,'Saldo al %1 a credito:','Saldo al %1 a debito:'),dtoc( .w_DATA1-1 )))
        .DoRTCalc(5,8,.t.)
        if .o_SALDOIN<>.w_SALDOIN.or. .o_CODICE<>.w_CODICE
            .w_INICRE = ABS(.w_SALDOIN)
        endif
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(AH_Msgformat(iif(.w_saldofin>=0,'Saldo al %1 a credito:','Saldo al %1 a debito:'),dtoc( .w_DATA2 )))
        .DoRTCalc(10,12,.t.)
            .w_SERIALE = Nvl( .w_ZoomScad.getVar('CCSERIAL') , Space(10))
            .w_ROWRIF = Nvl( .w_ZoomScad.getVar('CCROWRIF') , 0 )
        .DoRTCalc(15,15,.t.)
        if .o_CODICE<>.w_CODICE
            .w_CODICE1 = .w_CODICE
          .link_1_26('Full')
        endif
        .DoRTCalc(17,17,.t.)
        if .o_SALDOIN<>.w_SALDOIN.or. .o_CODICE<>.w_CODICE
            .w_INIDEB = ABS(.w_SALDOIN)
        endif
            .w_DESCRI1 = .w_ZoomScad.getVar('CCDESCRI')
        if .o_SALDOFIN<>.w_SALDOFIN.or. .o_CODICE<>.w_CODICE
            .w_FINCRE = ABS(.w_SALDOFIN)
        endif
        if .o_SALDOFIN<>.w_SALDOFIN.or. .o_CODICE<>.w_CODICE
            .w_FINDEB = ABS(.w_SALDOFIN)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_Msgformat(iif(.w_saldoin>=0,'Saldo al %1 a credito:','Saldo al %1 a debito:'),dtoc( .w_DATA1-1 )))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(AH_Msgformat(iif(.w_saldofin>=0,'Saldo al %1 a credito:','Saldo al %1 a debito:'),dtoc( .w_DATA2 )))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oINICRE_1_16.visible=!this.oPgFrm.Page1.oPag.oINICRE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oINIDEB_1_28.visible=!this.oPgFrm.Page1.oPag.oINIDEB_1_28.mHide()
    this.oPgFrm.Page1.oPag.oFINCRE_1_31.visible=!this.oPgFrm.Page1.oPag.oFINCRE_1_31.mHide()
    this.oPgFrm.Page1.oPag.oFINDEB_1_32.visible=!this.oPgFrm.Page1.oPag.oFINDEB_1_32.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomScad.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODICE))
          select BACODBAN,BADESCRI,BACODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODICE)+"%");

            select BACODBAN,BADESCRI,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODICE_1_2'),i_cWhere,'GSTE_ACB',"Conti banche",'gste5mcc.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODICE)
            select BACODBAN,BADESCRI,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_VALUTA = NVL(_Link_.BACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_VALUTA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE1
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODICE1)
            select BACODBAN,BADESCRI,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE1 = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_VALUTA = NVL(_Link_.BACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE1 = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_VALUTA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_2.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_2.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_6.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_6.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_7.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_7.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_11.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_11.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oINICRE_1_16.value==this.w_INICRE)
      this.oPgFrm.Page1.oPag.oINICRE_1_16.value=this.w_INICRE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAR_1_18.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oTOTDAR_1_18.value=this.w_TOTDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAVE_1_19.value==this.w_TOTAVE)
      this.oPgFrm.Page1.oPag.oTOTAVE_1_19.value=this.w_TOTAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_27.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_27.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oINIDEB_1_28.value==this.w_INIDEB)
      this.oPgFrm.Page1.oPag.oINIDEB_1_28.value=this.w_INIDEB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_29.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_29.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oFINCRE_1_31.value==this.w_FINCRE)
      this.oPgFrm.Page1.oPag.oFINCRE_1_31.value=this.w_FINCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINDEB_1_32.value==this.w_FINDEB)
      this.oPgFrm.Page1.oPag.oFINDEB_1_32.value=this.w_FINDEB
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_data1<=.w_data2 or (empty(.w_DATA2))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((.w_data1<=.w_data2 or (empty(.w_DATA1))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODICE = this.w_CODICE
    this.o_VALUTA = this.w_VALUTA
    this.o_SALDOIN = this.w_SALDOIN
    this.o_SALDOFIN = this.w_SALDOFIN
    return

enddefine

* --- Define pages as container
define class tgsba_kvcPag1 as StdContainer
  Width  = 788
  height = 464
  stdWidth  = 788
  stdheight = 464
  resizeXpos=225
  resizeYpos=267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_2 as StdField with uid="LKXRKGBPCG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice di conto corrente selezionato",;
    HelpContextID = 158135002,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=68, Top=13, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODICE"

  func oCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'gste5mcc.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oCODICE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oDATA1_1_6 as StdField with uid="GDXZDGGRWB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di registrazione di inizio visualizzazione",;
    HelpContextID = 261357770,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=68, Top=40

  func oDATA1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_data1<=.w_data2 or (empty(.w_DATA2))))
    endwith
    return bRes
  endfunc

  add object oDATA2_1_7 as StdField with uid="EXOZTEVISW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di registrazione di fine visualizzazione",;
    HelpContextID = 260309194,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=183, Top=40

  func oDATA2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_data1<=.w_data2 or (empty(.w_DATA1))))
    endwith
    return bRes
  endfunc


  add object ZoomScad as cp_zoombox with uid="EWWPYRKURU",left=14, top=65, width=781,height=323,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CCM_MAST",cZoomFile="GSBA_SEC",bOptions=.f.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 129073382


  add object oBtn_1_9 as StdButton with uid="JYXXTSJWRZ",left=6, top=414, width=48,height=45,;
    CpPicture="BMP\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la registrazione di conto corrente selezionata";
    , HelpContextID = 34832614;
    , caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSBA_BES(this.Parent.oContained,"O", .w_SERIALE, .w_ROWRIF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE) AND .w_ROWRIF<>0)
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="FQQJLWDKLN",left=729, top=413, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41606842;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI_1_11 as StdField with uid="UJKSZQWPOL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 75631818,;
   bGlobalFont=.t.,;
    Height=21, Width=263, Left=202, Top=13, InputMask=replicate('X',35)


  add object oObj_1_14 as cp_calclbl with uid="EMLNAPOBHQ",left=266, top=41, width=179,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Saldo iniziale:",alignment=1,;
    cEvent = "w_DATA1 Changed",;
    nPag=1;
    , HelpContextID = 129073382

  add object oINICRE_1_16 as StdField with uid="PQUOBXUCHQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_INICRE", cQueryName = "INICRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo a credito all'inizio del periodo selezionato",;
    HelpContextID = 142779258,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=446, Top=40, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oINICRE_1_16.mHide()
    with this.Parent.oContained
      return (.w_SALDOIN<0)
    endwith
  endfunc

  add object oTOTDAR_1_18 as StdField with uid="AXLLEGNMCM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_TOTDAR", cQueryName = "TOTDAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale dare",;
    HelpContextID = 57609782,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=425, Top=390, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oTOTAVE_1_19 as StdField with uid="QOVQNAKKFW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TOTAVE", cQueryName = "TOTAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale avere",;
    HelpContextID = 138670538,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=563, Top=390, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"


  add object oObj_1_20 as cp_calclbl with uid="IMVHJUQIGO",left=201, top=415, width=222,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Saldo finale:",alignment=1,;
    cEvent = "w_DATA2 Changed",;
    nPag=1;
    , HelpContextID = 129073382


  add object oBtn_1_24 as StdButton with uid="MRXEHFOEVE",left=729, top=13, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 127997974;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSBA_BES(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((NOT empty(.w_DATA1)) AND (NOT empty(.w_DATA2)))
      endwith
    endif
  endfunc

  add object oSIMVAL_1_27 as StdField with uid="HPAZUXKLKQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 41904090,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=472, Top=13, InputMask=replicate('X',5)

  add object oINIDEB_1_28 as StdField with uid="BRRURDGZJK",rtseq=18,rtrep=.f.,;
    cFormVar = "w_INIDEB", cQueryName = "INIDEB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo a debito all'inizio del periodo selezionato",;
    HelpContextID = 206676858,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=584, Top=40, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oINIDEB_1_28.mHide()
    with this.Parent.oContained
      return (.w_SALDOIN>=0)
    endwith
  endfunc

  add object oDESCRI1_1_29 as StdField with uid="XGUPNRKBHI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva",;
    HelpContextID = 192803638,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=88, Top=390, InputMask=replicate('X',45)

  add object oFINCRE_1_31 as StdField with uid="AXNVFAAAFT",rtseq=20,rtrep=.f.,;
    cFormVar = "w_FINCRE", cQueryName = "FINCRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo a credito alla fine del periodo selezionato",;
    HelpContextID = 142760106,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=425, Top=413, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oFINCRE_1_31.mHide()
    with this.Parent.oContained
      return (.w_SALDOFIN<0)
    endwith
  endfunc

  add object oFINDEB_1_32 as StdField with uid="WLIBLYDBWJ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FINDEB", cQueryName = "FINDEB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo a debito alla fine del periodo selezionato",;
    HelpContextID = 206657706,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=563, Top=413, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oFINDEB_1_32.mHide()
    with this.Parent.oContained
      return (.w_SALDOFIN>=0)
    endwith
  endfunc

  add object oStr_1_1 as StdString with uid="PAJZALDKXT",Visible=.t., Left=9, Top=15,;
    Alignment=1, Width=56, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="OWMBFNFGRJ",Visible=.t., Left=13, Top=44,;
    Alignment=1, Width=51, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VHRFWTJJIC",Visible=.t., Left=146, Top=44,;
    Alignment=1, Width=33, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PQTPNNDBJB",Visible=.t., Left=354, Top=392,;
    Alignment=1, Width=68, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="LYERAMAQLP",Visible=.t., Left=5, Top=392,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsba_kvc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
