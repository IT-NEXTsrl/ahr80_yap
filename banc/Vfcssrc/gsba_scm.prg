* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsba_scm                                                        *
*              Stampa causali conti correnti                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_49]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-16                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsba_scm",oParentObject))

* --- Class definition
define class tgsba_scm as StdForm
  Top    = 47
  Left   = 100

  * --- Standard Properties
  Width  = 501
  Height = 196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-10"
  HelpContextID=177568151
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  CCC_MAST_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsba_scm"
  cComment = "Stampa causali conti correnti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOSTAM = space(1)
  w_CODICE1 = space(5)
  o_CODICE1 = space(5)
  w_CODICE2 = space(5)
  w_DES1 = space(35)
  w_DES2 = space(35)
  w_TIPOPER = space(1)
  o_TIPOPER = space(1)
  w_TIPOPER1 = space(1)
  w_CODVAL = space(10)
  w_DECTOT = 0
  w_CALCPICT = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsba_scmPag1","gsba_scm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOSTAM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CCC_MAST'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsba_scm
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOSTAM=space(1)
      .w_CODICE1=space(5)
      .w_CODICE2=space(5)
      .w_DES1=space(35)
      .w_DES2=space(35)
      .w_TIPOPER=space(1)
      .w_TIPOPER1=space(1)
      .w_CODVAL=space(10)
      .w_DECTOT=0
      .w_CALCPICT=space(1)
        .w_TIPOSTAM = ' '
        .w_CODICE1 = SPACE(5)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODICE1))
          .link_1_4('Full')
        endif
        .w_CODICE2 = .w_CODICE1
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODICE2))
          .link_1_5('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
          .DoRTCalc(4,5,.f.)
        .w_TIPOPER = 'T'
        .w_TIPOPER1 = IIF(.w_TIPOPER='T',' ',.w_TIPOPER)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODVAL))
          .link_1_15('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPOPER<>.w_TIPOPER
            .w_CODICE1 = SPACE(5)
          .link_1_4('Full')
        endif
        if .o_TIPOPER<>.w_TIPOPER.or. .o_CODICE1<>.w_CODICE1
            .w_CODICE2 = .w_CODICE1
          .link_1_5('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(4,6,.t.)
        if .o_TIPOPER<>.w_TIPOPER
            .w_TIPOPER1 = IIF(.w_TIPOPER='T',' ',.w_TIPOPER)
        endif
          .link_1_15('Full')
        .DoRTCalc(9,9,.t.)
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE1
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsba_mct',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODICE1)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODICE1))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE1)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE1) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCODICE1_1_4'),i_cWhere,'gsba_mct',"Causali conti correnti",'GSBA0SCM.CCC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODICE1)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE1 = NVL(_Link_.CACODICE,space(5))
      this.w_DES1 = NVL(_Link_.CADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE1 = space(5)
      endif
      this.w_DES1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE2
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsba_mct',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODICE2)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODICE2))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE2)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE2) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCODICE2_1_5'),i_cWhere,'gsba_mct',"Causali conti correnti",'GSBA0SCM.CCC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODICE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODICE2)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE2 = NVL(_Link_.CACODICE,space(5))
      this.w_DES2 = NVL(_Link_.CADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE2 = space(5)
      endif
      this.w_DES2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODICE2>=.w_CODICE1) or (empty(.w_CODICE1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inferiore alla selezione precedente")
        endif
        this.w_CODICE2 = space(5)
        this.w_DES2 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(10))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(10)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOSTAM_1_1.RadioValue()==this.w_TIPOSTAM)
      this.oPgFrm.Page1.oPag.oTIPOSTAM_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE1_1_4.value==this.w_CODICE1)
      this.oPgFrm.Page1.oPag.oCODICE1_1_4.value=this.w_CODICE1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE2_1_5.value==this.w_CODICE2)
      this.oPgFrm.Page1.oPag.oCODICE2_1_5.value=this.w_CODICE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDES1_1_6.value==this.w_DES1)
      this.oPgFrm.Page1.oPag.oDES1_1_6.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page1.oPag.oDES2_1_7.value==this.w_DES2)
      this.oPgFrm.Page1.oPag.oDES2_1_7.value=this.w_DES2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPER_1_12.RadioValue()==this.w_TIPOPER)
      this.oPgFrm.Page1.oPag.oTIPOPER_1_12.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CODICE2>=.w_CODICE1) or (empty(.w_CODICE1)))  and not(empty(.w_CODICE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE2_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inferiore alla selezione precedente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODICE1 = this.w_CODICE1
    this.o_TIPOPER = this.w_TIPOPER
    return

enddefine

* --- Define pages as container
define class tgsba_scmPag1 as StdContainer
  Width  = 497
  height = 196
  stdWidth  = 497
  stdheight = 196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPOSTAM_1_1 as StdCheck with uid="WVUZCYVNVY",rtseq=1,rtrep=.f.,left=110, top=91, caption="Compensazione",;
    ToolTipText = "Flag conto di compensazione",;
    HelpContextID = 199636861,;
    cFormVar="w_TIPOSTAM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPOSTAM_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPOSTAM_1_1.GetRadio()
    this.Parent.oContained.w_TIPOSTAM = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSTAM_1_1.SetRadio()
    this.Parent.oContained.w_TIPOSTAM=trim(this.Parent.oContained.w_TIPOSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSTAM=='S',1,;
      0)
  endfunc

  add object oCODICE1_1_4 as StdField with uid="EYONCSQNTV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODICE1", cQueryName = "CODICE1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale conti correnti di inizio selezione",;
    HelpContextID = 200078042,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=110, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="gsba_mct", oKey_1_1="CACODICE", oKey_1_2="this.w_CODICE1"

  func oCODICE1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE1_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE1_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCODICE1_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'gsba_mct',"Causali conti correnti",'GSBA0SCM.CCC_MAST_VZM',this.parent.oContained
  endproc
  proc oCODICE1_1_4.mZoomOnZoom
    local i_obj
    i_obj=gsba_mct()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODICE1
     i_obj.ecpSave()
  endproc

  add object oCODICE2_1_5 as StdField with uid="LATCLGXZCQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODICE2", cQueryName = "CODICE2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inferiore alla selezione precedente",;
    ToolTipText = "Causale conti correnti di fine selezione",;
    HelpContextID = 200078042,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=110, Top=64, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="gsba_mct", oKey_1_1="CACODICE", oKey_1_2="this.w_CODICE2"

  func oCODICE2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE2_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE2_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCODICE2_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsba_mct',"Causali conti correnti",'GSBA0SCM.CCC_MAST_VZM',this.parent.oContained
  endproc
  proc oCODICE2_1_5.mZoomOnZoom
    local i_obj
    i_obj=gsba_mct()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODICE2
     i_obj.ecpSave()
  endproc

  add object oDES1_1_6 as StdField with uid="IQNWYZDWCC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 181138230,;
   bGlobalFont=.t.,;
    Height=21, Width=308, Left=182, Top=38, InputMask=replicate('X',35)

  add object oDES2_1_7 as StdField with uid="SVXMUJQCPW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DES2", cQueryName = "DES2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 181203766,;
   bGlobalFont=.t.,;
    Height=21, Width=308, Left=182, Top=64, InputMask=replicate('X',35)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=110, top=118, width=380,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181305114


  add object oBtn_1_9 as StdButton with uid="LFPFPHISOF",left=388, top=145, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217513178;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="UUWLSFWELF",left=442, top=145, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184885574;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPOPER_1_12 as StdCombo with uid="JNTVBKBNWB",rtseq=6,rtrep=.f.,left=110,top=9,width=134,height=21;
    , ToolTipText = "Tipologia dell'operazione della causale (credito, debito, tutte)";
    , HelpContextID = 186005450;
    , cFormVar="w_TIPOPER",RowSource=""+"Credito,"+"Debito,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPER_1_12.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPOPER_1_12.GetRadio()
    this.Parent.oContained.w_TIPOPER = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPER_1_12.SetRadio()
    this.Parent.oContained.w_TIPOPER=trim(this.Parent.oContained.w_TIPOPER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPER=='C',1,;
      iif(this.Parent.oContained.w_TIPOPER=='D',2,;
      iif(this.Parent.oContained.w_TIPOPER=='T',3,;
      0)))
  endfunc

  add object oStr_1_2 as StdString with uid="RGYTHMSDYP",Visible=.t., Left=5, Top=38,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="UEMECDXEEA",Visible=.t., Left=5, Top=64,;
    Alignment=1, Width=99, Height=15,;
    Caption="A causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="LONOLHLLUJ",Visible=.t., Left=5, Top=118,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="GGRJRESEIG",Visible=.t., Left=5, Top=11,;
    Alignment=1, Width=99, Height=18,;
    Caption="Operazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsba_scm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
