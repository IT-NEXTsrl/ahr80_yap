* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bfg                                                        *
*              Stampa dati file generato                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-12-15                                                      *
* Last revis.: 2012-02-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bfg",oParentObject)
return(i_retval)

define class tgsai_bfg as StdBatch
  * --- Local variables
  w_MESS = space(50)
  w_CONT_FILE = space(254)
  w_NUM_REC = 0
  w_AI__ANNO = space(4)
  w_RECORD = space(1)
  w_AICOGNOM = space(24)
  w_AI__NOME = space(20)
  w_AIRAGSOC = space(60)
  w_AICODFIS = space(16)
  w_AIPARIVA = space(11)
  w_AIDOMFIS = space(40)
  w_AIPRODOM = space(2)
  w_AILOCNAS = space(40)
  w_AIDATNAS = space(8)
  w_AI_SESSO = space(1)
  w_AITIPINV = space(1)
  w_AISOCINC = space(1)
  w_AICODINT = space(16)
  w_AICODCAF = space(5)
  w_AIIMPTRA = space(1)
  w_AIDATIMP = space(8)
  w_DEDATOPE = space(8)
  w_DEMODPAG = space(1)
  w_DECODFIS = space(16)
  w_DEIMPDOC = space(9)
  w_DENUMFAT = space(15)
  w_DEOPERAZ = space(1)
  w_DEPARIVA = space(11)
  w_DEIMPOST = space(9)
  w_DEDESCRI = space(60)
  w_DECOGNOM = space(24)
  w_DE__NOME = space(20)
  w_DECOMEST = space(40)
  w_DEPRONAS = space(2)
  w_DEDATNAS = space(8)
  w_DESTADOM = space(3)
  w_DECITEST = space(40)
  w_DESTAEST = space(3)
  w_DEINDEST = space(40)
  w_DEDATFAT = space(8)
  w_DENUMRET = space(15)
  w_DEVARIMP = space(1)
  w_DEVARIMS = space(1)
  w_AIPROTEC = space(17)
  w_AIPRODOC = space(6)
  w_AIPROGIN = space(4)
  w_AINUMMOD = space(4)
  w_NUM_RECORD = 0
  w_NUM_RECORD_1 = 0
  w_NUM_RECORD_2 = 0
  w_NUM_RECORD_3 = 0
  w_NUM_RECORD_4 = 0
  w_NUM_RECORD_5 = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa dati file generato ( Gsai_Sfg )
    * --- Controllo se il file esiste
    if NOT file( this.oParentObject.w_NOMEFILE )
      * --- Se il file non esiste
      this.w_MESS = "Il file selezionato non esiste"
      ah_errormsg(this.w_MESS,48)
      i_retcode = 'stop'
      return
    endif
    CREATE CURSOR CursStampa (AI__ANNO C(4),AICOGNOM C(24),AI__NOME C(20),AIRAGSOC C(60),AICODFIS C(16), ;
    AIPARIVA C(11),AIDOMFIS C(40),AIPRODOM C(2),AILOCNAS C(40),AIDATNAS C(8),AI_SESSO C(1),;
    AITIPINV C(1),AISOCINC C(1),AICODINT C(16),AICODCAF C(5),AIIMPTRA C(1),AIDATIMP C(8),;
    RECORD C(1),DEDATOPE C(8),DEMODPAG C(1),DECODFIS C(16),DEIMPDOC C(9),;
    DENUMFAT C(15),DEOPERAZ C(1),DEPARIVA C(11),DEIMPOST C(9),DEDESCRI C(60),;
    DECOGNOM C(24),DE__NOME C(20),DECOMEST C(40),DEPRONAS C(2),DEDATNAS C(8),;
    DESTADOM C(3),DECITEST C(40),DESTAEST C(3),DEINDEST C(40),DEDATFAT C(8),DENUMRET C(15),;
    DEVARIMP C(1),DEVARIMS C(1),AIPROTEC C(17),AIPRODOC C(6),;
    AIPROGIN C(4),AINUMMOD C(4))
    this.w_CONT_FILE = FILETOSTR(this.oParentObject.w_NOMEFILE)
    ALINES(GEN_FILE,this.w_CONT_FILE)
    this.w_CONT_FILE = " "
    this.w_NUM_REC = 1
    Select CursStampa
    WRCURSOR ("CursStampa")
    do while this.w_NUM_REC< ALEN(GEN_FILE)
      Select CursStampa
      APPEND BLANK
      do case
        case SUBSTR(GEN_FILE[this.w_NUM_REC],1,8)="0ART2147"
          * --- Record di Testa
          this.w_AI__ANNO = SUBSTR(GEN_FILE[this.w_NUM_REC],257,4)
          this.w_AICOGNOM = SUBSTR(GEN_FILE[this.w_NUM_REC],162,24)
          this.w_AI__NOME = SUBSTR(GEN_FILE[this.w_NUM_REC],186,20)
          this.w_AIRAGSOC = SUBSTR(GEN_FILE[this.w_NUM_REC],60,60)
          this.w_AICODFIS = SUBSTR(GEN_FILE[this.w_NUM_REC],33,16)
          this.w_AIPARIVA = SUBSTR(GEN_FILE[this.w_NUM_REC],49,11)
          this.w_AIDOMFIS = SUBSTR(GEN_FILE[this.w_NUM_REC],120,40)
          this.w_AIPRODOM = SUBSTR(GEN_FILE[this.w_NUM_REC],160,2)
          this.w_AILOCNAS = SUBSTR(GEN_FILE[this.w_NUM_REC],215,40)
          this.w_AIDATNAS = SUBSTR(GEN_FILE[this.w_NUM_REC],207,8)
          this.w_AI_SESSO = SUBSTR(GEN_FILE[this.w_NUM_REC],206,1)
          this.w_AITIPINV = SUBSTR(GEN_FILE[this.w_NUM_REC],9,1)
          this.w_AISOCINC = SUBSTR(GEN_FILE[this.w_NUM_REC],261,1)
          this.w_AICODINT = SUBSTR(GEN_FILE[this.w_NUM_REC],270,16)
          this.w_AICODCAF = SUBSTR(GEN_FILE[this.w_NUM_REC],286,5)
          this.w_AIIMPTRA = SUBSTR(GEN_FILE[this.w_NUM_REC],291,1)
          this.w_AIDATIMP = SUBSTR(GEN_FILE[this.w_NUM_REC],292,8)
          this.w_AIPROTEC = SUBSTR(GEN_FILE[this.w_NUM_REC],10,17)
          this.w_AIPRODOC = SUBSTR(GEN_FILE[this.w_NUM_REC],27,6)
          this.w_AIPROGIN = SUBSTR(GEN_FILE[this.w_NUM_REC],262,4)
          this.w_AINUMMOD = SUBSTR(GEN_FILE[this.w_NUM_REC],266,4)
          this.w_DEDATOPE = SPACE(8)
          this.w_DEMODPAG = SPACE(1)
          this.w_DECODFIS = SPACE(16)
          this.w_DEIMPDOC = SPACE(9)
          this.w_DENUMFAT = SPACE(15)
          this.w_DEOPERAZ = SPACE(1)
          this.w_DEPARIVA = SPACE(11)
          this.w_DEIMPOST = SPACE(9)
          this.w_DEDESCRI = SPACE(60)
          this.w_DECOGNOM = SPACE(24)
          this.w_DE__NOME = SPACE(20)
          this.w_DECOMEST = SPACE(40)
          this.w_DEPRONAS = SPACE(2)
          this.w_DEDATNAS = SPACE(8)
          this.w_DESTADOM = SPACE(3)
          this.w_DECITEST = SPACE(40)
          this.w_DESTAEST = SPACE(3)
          this.w_DEINDEST = SPACE(40)
          this.w_DEDATFAT = SPACE(8)
          this.w_DENUMRET = SPACE(15)
          this.w_DEVARIMP = SPACE(1)
          this.w_DEVARIMS = SPACE(1)
          this.w_RECORD = "0"
        case SUBSTR(GEN_FILE[this.w_NUM_REC],1,1)="1"
          this.w_AICOGNOM = SPACE(24)
          this.w_AI__NOME = SPACE(20)
          this.w_AIRAGSOC = SPACE(60)
          this.w_AICODFIS = SPACE(16)
          this.w_AIPARIVA = SPACE(11)
          this.w_AIDOMFIS = SPACE(40)
          this.w_AIPRODOM = SPACE(2)
          this.w_AILOCNAS = SPACE(40)
          this.w_AIDATNAS = SPACE(8)
          this.w_AI_SESSO = SPACE(1)
          this.w_AITIPINV = SPACE(1)
          this.w_AISOCINC = SPACE(1)
          this.w_AICODINT = SPACE(16)
          this.w_AICODCAF = SPACE(5)
          this.w_AIIMPTRA = SPACE(1)
          this.w_AIDATIMP = SPACE(8)
          this.w_DEDATOPE = SUBSTR(GEN_FILE[this.w_NUM_REC],18,8)
          this.w_DEMODPAG = SUBSTR(GEN_FILE[this.w_NUM_REC],26,1)
          this.w_DECODFIS = SUBSTR(GEN_FILE[this.w_NUM_REC],2,16)
          this.w_DEIMPDOC = SUBSTR(GEN_FILE[this.w_NUM_REC],27,9)
          this.w_DENUMFAT = SPACE(15)
          this.w_DEOPERAZ = SPACE(1)
          this.w_DEPARIVA = SPACE(11)
          this.w_DEIMPOST = SPACE(9)
          this.w_DEDESCRI = SPACE(60)
          this.w_DECOGNOM = SPACE(24)
          this.w_DE__NOME = SPACE(20)
          this.w_DECOMEST = SPACE(40)
          this.w_DEPRONAS = SPACE(2)
          this.w_DEDATNAS = SPACE(8)
          this.w_DESTADOM = SPACE(3)
          this.w_DECITEST = SPACE(40)
          this.w_DESTAEST = SPACE(3)
          this.w_DEINDEST = SPACE(40)
          this.w_DEDATFAT = SPACE(8)
          this.w_DENUMRET = SPACE(15)
          this.w_DEVARIMP = SPACE(1)
          this.w_DEVARIMS = SPACE(1)
          this.w_AIPROTEC = SPACE(17)
          this.w_AIPRODOC = SPACE(6)
          this.w_AIPROGIN = SPACE(4)
          this.w_AINUMMOD = SPACE(4)
          this.w_RECORD = "1"
        case SUBSTR(GEN_FILE[this.w_NUM_REC],1,1)="2"
          this.w_AICOGNOM = SPACE(24)
          this.w_AI__NOME = SPACE(20)
          this.w_AIRAGSOC = SPACE(60)
          this.w_AICODFIS = SPACE(16)
          this.w_AIPARIVA = SPACE(11)
          this.w_AIDOMFIS = SPACE(40)
          this.w_AIPRODOM = SPACE(2)
          this.w_AILOCNAS = SPACE(40)
          this.w_AIDATNAS = SPACE(8)
          this.w_AI_SESSO = SPACE(1)
          this.w_AITIPINV = SPACE(1)
          this.w_AISOCINC = SPACE(1)
          this.w_AICODINT = SPACE(16)
          this.w_AICODCAF = SPACE(5)
          this.w_AIIMPTRA = SPACE(1)
          this.w_AIDATIMP = SPACE(8)
          this.w_DEDATOPE = SUBSTR(GEN_FILE[this.w_NUM_REC],13,8)
          this.w_DEMODPAG = SUBSTR(GEN_FILE[this.w_NUM_REC],36,1)
          this.w_DECODFIS = SPACE(16)
          this.w_DEIMPDOC = SUBSTR(GEN_FILE[this.w_NUM_REC],37,9)
          this.w_DENUMFAT = SUBSTR(GEN_FILE[this.w_NUM_REC],21,15)
          this.w_DEOPERAZ = SUBSTR(GEN_FILE[this.w_NUM_REC],55,1)
          this.w_DEPARIVA = SUBSTR(GEN_FILE[this.w_NUM_REC],2,11)
          this.w_DEIMPOST = SUBSTR(GEN_FILE[this.w_NUM_REC],46,9)
          this.w_DEDESCRI = SPACE(60)
          this.w_DECOGNOM = SPACE(24)
          this.w_DE__NOME = SPACE(20)
          this.w_DECOMEST = SPACE(40)
          this.w_DEPRONAS = SPACE(2)
          this.w_DEDATNAS = SPACE(8)
          this.w_DESTADOM = SPACE(3)
          this.w_DECITEST = SPACE(40)
          this.w_DESTAEST = SPACE(3)
          this.w_DEINDEST = SPACE(40)
          this.w_DEDATFAT = SPACE(8)
          this.w_DENUMRET = SPACE(15)
          this.w_DEVARIMP = SPACE(1)
          this.w_DEVARIMS = SPACE(1)
          this.w_AIPROTEC = SPACE(17)
          this.w_AIPRODOC = SPACE(6)
          this.w_AIPROGIN = SPACE(4)
          this.w_AINUMMOD = SPACE(4)
          this.w_RECORD = "2"
        case SUBSTR(GEN_FILE[this.w_NUM_REC],1,1)="3"
          this.w_AICOGNOM = SPACE(24)
          this.w_AI__NOME = SPACE(20)
          this.w_AIRAGSOC = SPACE(60)
          this.w_AICODFIS = SPACE(16)
          this.w_AIPARIVA = SPACE(11)
          this.w_AIDOMFIS = SPACE(40)
          this.w_AIPRODOM = SPACE(2)
          this.w_AILOCNAS = SPACE(40)
          this.w_AIDATNAS = SPACE(8)
          this.w_AI_SESSO = SPACE(1)
          this.w_AITIPINV = SPACE(1)
          this.w_AISOCINC = SPACE(1)
          this.w_AICODINT = SPACE(16)
          this.w_AICODCAF = SPACE(5)
          this.w_AIIMPTRA = SPACE(1)
          this.w_AIDATIMP = SPACE(8)
          this.w_DEDATOPE = SUBSTR(GEN_FILE[this.w_NUM_REC],242,8)
          this.w_DEMODPAG = SUBSTR(GEN_FILE[this.w_NUM_REC],265,1)
          this.w_DECODFIS = SPACE(16)
          this.w_DEIMPDOC = SUBSTR(GEN_FILE[this.w_NUM_REC],266,9)
          this.w_DENUMFAT = SUBSTR(GEN_FILE[this.w_NUM_REC],250,15)
          this.w_DEOPERAZ = SUBSTR(GEN_FILE[this.w_NUM_REC],284,1)
          this.w_DEPARIVA = SPACE(11)
          this.w_DEIMPOST = SUBSTR(GEN_FILE[this.w_NUM_REC],275,9)
          this.w_DEDESCRI = SUBSTR(GEN_FILE[this.w_NUM_REC],99,60)
          this.w_DECOGNOM = SUBSTR(GEN_FILE[this.w_NUM_REC],2,24)
          this.w_DE__NOME = SUBSTR(GEN_FILE[this.w_NUM_REC],26,20)
          this.w_DECOMEST = SUBSTR(GEN_FILE[this.w_NUM_REC],54,40)
          this.w_DEPRONAS = SUBSTR(GEN_FILE[this.w_NUM_REC],94,2)
          this.w_DEDATNAS = SUBSTR(GEN_FILE[this.w_NUM_REC],46,8)
          this.w_DESTADOM = SUBSTR(GEN_FILE[this.w_NUM_REC],96,3)
          this.w_DECITEST = SUBSTR(GEN_FILE[this.w_NUM_REC],159,40)
          this.w_DESTAEST = SUBSTR(GEN_FILE[this.w_NUM_REC],199,3)
          this.w_DEINDEST = SUBSTR(GEN_FILE[this.w_NUM_REC],202,40)
          this.w_DEDATFAT = SPACE(8)
          this.w_DENUMRET = SPACE(15)
          this.w_DEVARIMP = SPACE(1)
          this.w_DEVARIMS = SPACE(1)
          this.w_AIPROTEC = SPACE(17)
          this.w_AIPRODOC = SPACE(6)
          this.w_AIPROGIN = SPACE(4)
          this.w_AINUMMOD = SPACE(4)
          this.w_RECORD = "3"
        case SUBSTR(GEN_FILE[this.w_NUM_REC],1,1)="4"
          this.w_AICOGNOM = SPACE(24)
          this.w_AI__NOME = SPACE(20)
          this.w_AIRAGSOC = SPACE(60)
          this.w_AICODFIS = SPACE(16)
          this.w_AIPARIVA = SPACE(11)
          this.w_AIDOMFIS = SPACE(40)
          this.w_AIPRODOM = SPACE(2)
          this.w_AILOCNAS = SPACE(40)
          this.w_AIDATNAS = SPACE(8)
          this.w_AI_SESSO = SPACE(1)
          this.w_AITIPINV = SPACE(1)
          this.w_AISOCINC = SPACE(1)
          this.w_AICODINT = SPACE(16)
          this.w_AICODCAF = SPACE(5)
          this.w_AIIMPTRA = SPACE(1)
          this.w_AIDATIMP = SPACE(8)
          this.w_DEDATOPE = SUBSTR(GEN_FILE[this.w_NUM_REC],29,8)
          this.w_DEMODPAG = SPACE(1)
          this.w_DECODFIS = SUBSTR(GEN_FILE[this.w_NUM_REC],13,16)
          this.w_DEIMPDOC = SUBSTR(GEN_FILE[this.w_NUM_REC],52,9)
          this.w_DENUMFAT = SUBSTR(GEN_FILE[this.w_NUM_REC],37,15)
          this.w_DEOPERAZ = SPACE(1)
          this.w_DEPARIVA = SUBSTR(GEN_FILE[this.w_NUM_REC],2,11)
          this.w_DEIMPOST = SUBSTR(GEN_FILE[this.w_NUM_REC],61,9)
          this.w_DEDESCRI = SPACE(60)
          this.w_DECOGNOM = SPACE(24)
          this.w_DE__NOME = SPACE(20)
          this.w_DECOMEST = SPACE(40)
          this.w_DEPRONAS = SPACE(2)
          this.w_DEDATNAS = SPACE(8)
          this.w_DESTADOM = SPACE(3)
          this.w_DECITEST = SPACE(40)
          this.w_DESTAEST = SPACE(3)
          this.w_DEINDEST = SPACE(40)
          this.w_DEDATFAT = SUBSTR(GEN_FILE[this.w_NUM_REC],70,8)
          this.w_DENUMRET = SUBSTR(GEN_FILE[this.w_NUM_REC],78,15)
          this.w_DEVARIMP = SUBSTR(GEN_FILE[this.w_NUM_REC],93,1)
          this.w_DEVARIMS = SUBSTR(GEN_FILE[this.w_NUM_REC],94,1)
          this.w_AIPROTEC = SPACE(17)
          this.w_AIPRODOC = SPACE(6)
          this.w_AIPROGIN = SPACE(4)
          this.w_AINUMMOD = SPACE(4)
          this.w_RECORD = "4"
        case SUBSTR(GEN_FILE[this.w_NUM_REC],1,1)="5"
          this.w_AICOGNOM = SPACE(24)
          this.w_AI__NOME = SPACE(20)
          this.w_AIRAGSOC = SPACE(60)
          this.w_AICODFIS = SPACE(16)
          this.w_AIPARIVA = SPACE(11)
          this.w_AIDOMFIS = SPACE(40)
          this.w_AIPRODOM = SPACE(2)
          this.w_AILOCNAS = SPACE(40)
          this.w_AIDATNAS = SPACE(8)
          this.w_AI_SESSO = SPACE(1)
          this.w_AITIPINV = SPACE(1)
          this.w_AISOCINC = SPACE(1)
          this.w_AICODINT = SPACE(16)
          this.w_AICODCAF = SPACE(5)
          this.w_AIIMPTRA = SPACE(1)
          this.w_AIDATIMP = SPACE(8)
          this.w_DEDATOPE = SUBSTR(GEN_FILE[this.w_NUM_REC],242,8)
          this.w_DEMODPAG = SPACE(1)
          this.w_DECODFIS = SPACE(16)
          this.w_DEIMPDOC = SUBSTR(GEN_FILE[this.w_NUM_REC],265,9)
          this.w_DENUMFAT = SUBSTR(GEN_FILE[this.w_NUM_REC],250,15)
          this.w_DEOPERAZ = SPACE(1)
          this.w_DEPARIVA = SPACE(11)
          this.w_DEIMPOST = SUBSTR(GEN_FILE[this.w_NUM_REC],274,9)
          this.w_DEDESCRI = SUBSTR(GEN_FILE[this.w_NUM_REC],99,60)
          this.w_DECOGNOM = SUBSTR(GEN_FILE[this.w_NUM_REC],2,24)
          this.w_DE__NOME = SUBSTR(GEN_FILE[this.w_NUM_REC],26,20)
          this.w_DECOMEST = SUBSTR(GEN_FILE[this.w_NUM_REC],54,40)
          this.w_DEPRONAS = SUBSTR(GEN_FILE[this.w_NUM_REC],94,2)
          this.w_DEDATNAS = SUBSTR(GEN_FILE[this.w_NUM_REC],46,8)
          this.w_DESTADOM = SUBSTR(GEN_FILE[this.w_NUM_REC],96,3)
          this.w_DECITEST = SUBSTR(GEN_FILE[this.w_NUM_REC],159,40)
          this.w_DESTAEST = SUBSTR(GEN_FILE[this.w_NUM_REC],199,3)
          this.w_DEINDEST = SUBSTR(GEN_FILE[this.w_NUM_REC],202,40)
          this.w_DEDATFAT = SUBSTR(GEN_FILE[this.w_NUM_REC],283,8)
          this.w_DENUMRET = SUBSTR(GEN_FILE[this.w_NUM_REC],291,15)
          this.w_DEVARIMP = SUBSTR(GEN_FILE[this.w_NUM_REC],306,1)
          this.w_DEVARIMS = SUBSTR(GEN_FILE[this.w_NUM_REC],307,1)
          this.w_AIPROTEC = SPACE(17)
          this.w_AIPRODOC = SPACE(6)
          this.w_AIPROGIN = SPACE(4)
          this.w_AINUMMOD = SPACE(4)
          this.w_RECORD = "5"
      endcase
      Select CursStampa
       REPLACE AI__ANNO with this.w_AI__ANNO, RECORD with this.w_RECORD, AICOGNOM with this.w_AICOGNOM, AI__NOME with this.w_AI__NOME,; 
 AIRAGSOC with this.w_AIRAGSOC, AICODFIS with this.w_AICODFIS, AIPARIVA with this.w_AIPARIVA,; 
 AIDOMFIS with this.w_AIDOMFIS, AIPRODOM with this.w_AIPRODOM, AILOCNAS with this.w_AILOCNAS,; 
 AIDATNAS with this.w_AIDATNAS, AI_SESSO with this.w_AI_SESSO, AITIPINV with this.w_AITIPINV,; 
 AISOCINC with this.w_AISOCINC, AICODINT with this.w_AICODINT, AICODCAF with this.w_AICODCAF,; 
 AIIMPTRA with this.w_AIIMPTRA, AIDATIMP with this.w_AIDATIMP, DEDATOPE with this.w_DEDATOPE,; 
 DEMODPAG with this.w_DEMODPAG, DECODFIS with this.w_DECODFIS, DEIMPDOC with this.w_DEIMPDOC,; 
 DENUMFAT with this.w_DENUMFAT, DEOPERAZ with this.w_DEOPERAZ, DEPARIVA with this.w_DEPARIVA,; 
 DEIMPOST with this.w_DEIMPOST, DEDESCRI with this.w_DEDESCRI, DECOGNOM with this.w_DECOGNOM,; 
 DE__NOME with this.w_DE__NOME, DECOMEST with this.w_DECOMEST, DEPRONAS with this.w_DEPRONAS,; 
 DEDATNAS with this.w_DEDATNAS, DESTADOM with this.w_DESTADOM, DECITEST with this.w_DECITEST,; 
 DESTAEST with this.w_DESTAEST, DEINDEST with this.w_DEINDEST, DEDATFAT with this.w_DEDATFAT,; 
 DENUMRET with this.w_DENUMRET, DEVARIMP with this.w_DEVARIMP, DEVARIMS with this.w_DEVARIMS,; 
 AIPROTEC with this.w_AIPROTEC,AIPRODOC with this.w_AIPRODOC,AIPROGIN with this.w_AIPROGIN,AINUMMOD with this.w_AINUMMOD
      this.w_NUM_REC = this.w_NUM_REC+1
    enddo
    Select CursStampa
    Go Top
    Count for Record $ "124" to this.w_Num_Record
    if this.w_Num_Record>0
      Vq_Exec("..\Aift\Exe\Query\Gsai_bfg.vqr",this,"Codconto")
      L_Macro="Select "
      this.w_NUM_REC = Afields(Nome_Campi,"CursStampa")
      do while this.w_Num_Rec>0
        L_Macro=L_Macro+iif(Upper(Nome_Campi(this.w_Num_Rec,1)) $ ("DECOGNOM-DE__NOME-DEDESCRI"),'iif(Record $ "124",Codconto.'+alltrim(Nome_Campi(this.w_Num_Rec,1))+",CursStampa."+alltrim(Nome_Campi(this.w_Num_Rec,1))+") as "+alltrim(Nome_Campi(this.w_Num_Rec,1)),"CursStampa."+alltrim(Nome_Campi(this.w_Num_Rec,1)))+", "
        this.w_NUM_REC = this.w_Num_Rec-1
      enddo
      L_Macro=Left(L_Macro,Len(L_Macro)-2)
      L_Macro=L_Macro+' from CursStampa left outer join Codconto on (CursStampa.Record="1" and CursStampa.Record=CodConto.De__Area and Alltrim(CursStampa.Decodfis)=CodConto.Decodfis)'
      L_Macro=L_Macro+' or (CursStampa.Record="2" and CursStampa.Record=CodConto.De__Area and CursStampa.Depariva=CodConto.Depariva)'
      L_Macro=L_Macro+' or (CursStampa.Record="4" and CursStampa.Record=CodConto.De__Area and CursStampa.Depariva=CodConto.Depariva And Alltrim(CursStampa.Decodfis)=Alltrim(CodConto.Decodfis)) into cursor CursStampa1'
      &L_Macro
    else
      Select * from CursStampa into cursor CursStampa1
    endif
    * --- Prima di lanciare i vari report controllo se esistono almeno un record
    *     per le varie tipologie
    Select CursStampa1
    Go Top
    Count for Record = "1" to this.w_Num_Record_1
    Count for Record = "2" to this.w_Num_Record_2
    Count for Record = "3" to this.w_Num_Record_3
    Count for Record = "4" to this.w_Num_Record_4
    Count for Record = "5" to this.w_Num_Record_5
    Select * from CursStampa1 into cursor __TMP__ where Record="0"
    CP_CHPRN("..\aift\exe\query\Gsaifsfg", , this.oParentObject)
    if this.w_Num_Record_1>0
      Select * from CursStampa1 into cursor __TMP__ where Record="1" 
      CP_CHPRN("..\aift\exe\query\Gsai_sfg", , this.oParentObject)
    endif
    if this.w_Num_Record_2>0
      Select * from CursStampa1 into cursor __TMP__ where Record="2" 
      CP_CHPRN("..\aift\exe\query\Gsai_sfg", , this.oParentObject)
    endif
    if this.w_Num_Record_3>0
      Select * from CursStampa1 into cursor __TMP__ where Record="3" 
      CP_CHPRN("..\aift\exe\query\Gsai3sfg", , this.oParentObject)
    endif
    if this.w_Num_Record_4>0
      Select * from CursStampa1 into cursor __TMP__ where Record="4" 
      CP_CHPRN("..\aift\exe\query\Gsai4sfg", , this.oParentObject)
    endif
    if this.w_Num_Record_5>0
      Select * from CursStampa1 into cursor __TMP__ where Record="5" 
      CP_CHPRN("..\aift\exe\query\Gsai5sfg", , this.oParentObject)
    endif
    Use in select ("CursStampa")
    Use in select ("CursStampa1")
    Use in select ("Codconto")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
