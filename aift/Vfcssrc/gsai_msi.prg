* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_msi                                                        *
*              Dettaglio parametri dati IVA spesometro                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-28                                                      *
* Last revis.: 2013-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsai_msi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsai_msi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsai_msi")
  return

* --- Class definition
define class tgsai_msi as StdPCForm
  Width  = 799
  Height = 453
  Top    = 10
  Left   = 10
  cComment = "Dettaglio parametri dati IVA spesometro"
  cPrg = "gsai_msi"
  HelpContextID=171309207
  add object cnt as tcgsai_msi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsai_msi as PCContext
  w_PE__ANNO = space(4)
  w_PECAUCON = space(5)
  w_CCDESCRI = space(35)
  w_PETIPREG = space(1)
  w_PENUMREG = 0
  w_PECODIVA = space(5)
  w_PECONDIZ = space(1)
  w_OBTEST = space(8)
  w_PENONRES = space(1)
  w_PEQUADRO = space(2)
  w_CCTIPREG = space(1)
  w_CCFLRIFE = space(1)
  w_PEVALORI = space(1)
  w_PENOLEGG = space(1)
  proc Save(i_oFrom)
    this.w_PE__ANNO = i_oFrom.w_PE__ANNO
    this.w_PECAUCON = i_oFrom.w_PECAUCON
    this.w_CCDESCRI = i_oFrom.w_CCDESCRI
    this.w_PETIPREG = i_oFrom.w_PETIPREG
    this.w_PENUMREG = i_oFrom.w_PENUMREG
    this.w_PECODIVA = i_oFrom.w_PECODIVA
    this.w_PECONDIZ = i_oFrom.w_PECONDIZ
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_PENONRES = i_oFrom.w_PENONRES
    this.w_PEQUADRO = i_oFrom.w_PEQUADRO
    this.w_CCTIPREG = i_oFrom.w_CCTIPREG
    this.w_CCFLRIFE = i_oFrom.w_CCFLRIFE
    this.w_PEVALORI = i_oFrom.w_PEVALORI
    this.w_PENOLEGG = i_oFrom.w_PENOLEGG
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PE__ANNO = this.w_PE__ANNO
    i_oTo.w_PECAUCON = this.w_PECAUCON
    i_oTo.w_CCDESCRI = this.w_CCDESCRI
    i_oTo.w_PETIPREG = this.w_PETIPREG
    i_oTo.w_PENUMREG = this.w_PENUMREG
    i_oTo.w_PECODIVA = this.w_PECODIVA
    i_oTo.w_PECONDIZ = this.w_PECONDIZ
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_PENONRES = this.w_PENONRES
    i_oTo.w_PEQUADRO = this.w_PEQUADRO
    i_oTo.w_CCTIPREG = this.w_CCTIPREG
    i_oTo.w_CCFLRIFE = this.w_CCFLRIFE
    i_oTo.w_PEVALORI = this.w_PEVALORI
    i_oTo.w_PENOLEGG = this.w_PENOLEGG
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsai_msi as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 799
  Height = 453
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-13"
  HelpContextID=171309207
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DATPIVSP_IDX = 0
  CAU_CONT_IDX = 0
  VOCIIVA_IDX = 0
  cFile = "DATPIVSP"
  cKeySelect = "PE__ANNO"
  cKeyWhere  = "PE__ANNO=this.w_PE__ANNO"
  cKeyDetail  = "PE__ANNO=this.w_PE__ANNO"
  cKeyWhereODBC = '"PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';

  cKeyDetailWhereODBC = '"PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DATPIVSP.PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DATPIVSP.CPROWNUM '
  cPrg = "gsai_msi"
  cComment = "Dettaglio parametri dati IVA spesometro"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PE__ANNO = space(4)
  w_PECAUCON = space(5)
  w_CCDESCRI = space(35)
  w_PETIPREG = space(1)
  o_PETIPREG = space(1)
  w_PENUMREG = 0
  w_PECODIVA = space(5)
  w_PECONDIZ = space(1)
  o_PECONDIZ = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_PENONRES = space(1)
  w_PEQUADRO = space(2)
  w_CCTIPREG = space(1)
  w_CCFLRIFE = space(1)
  w_PEVALORI = space(1)
  w_PENOLEGG = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_msiPag1","gsai_msi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='DATPIVSP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATPIVSP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATPIVSP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsai_msi'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DATPIVSP where PE__ANNO=KeySet.PE__ANNO
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DATPIVSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSP_IDX,2],this.bLoadRecFilter,this.DATPIVSP_IDX,"gsai_msi")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATPIVSP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATPIVSP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATPIVSP '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  )
      select * from (i_cTable) DATPIVSP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_PE__ANNO = NVL(PE__ANNO,space(4))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DATPIVSP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CCDESCRI = space(35)
          .w_CCTIPREG = space(1)
          .w_CCFLRIFE = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PECAUCON = NVL(PECAUCON,space(5))
          if link_2_1_joined
            this.w_PECAUCON = NVL(CCCODICE201,NVL(this.w_PECAUCON,space(5)))
            this.w_CCDESCRI = NVL(CCDESCRI201,space(35))
            this.w_CCTIPREG = NVL(CCTIPREG201,space(1))
            this.w_CCFLRIFE = NVL(CCFLRIFE201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_PETIPREG = NVL(PETIPREG,space(1))
          .w_PENUMREG = NVL(PENUMREG,0)
          .w_PECODIVA = NVL(PECODIVA,space(5))
          * evitabile
          *.link_2_5('Load')
          .w_PECONDIZ = NVL(PECONDIZ,space(1))
          .w_PENONRES = NVL(PENONRES,space(1))
          .w_PEQUADRO = NVL(PEQUADRO,space(2))
          .w_PEVALORI = NVL(PEVALORI,space(1))
          .w_PENOLEGG = NVL(PENOLEGG,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PE__ANNO=space(4)
      .w_PECAUCON=space(5)
      .w_CCDESCRI=space(35)
      .w_PETIPREG=space(1)
      .w_PENUMREG=0
      .w_PECODIVA=space(5)
      .w_PECONDIZ=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_PENONRES=space(1)
      .w_PEQUADRO=space(2)
      .w_CCTIPREG=space(1)
      .w_CCFLRIFE=space(1)
      .w_PEVALORI=space(1)
      .w_PENOLEGG=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_PECAUCON))
         .link_2_1('Full')
        endif
        .DoRTCalc(3,3,.f.)
        .w_PETIPREG = 'N'
        .w_PENUMREG = 0
        .w_PECODIVA = iif(.w_Pecondiz='B',Space(5),.w_Pecodiva)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_PECODIVA))
         .link_2_5('Full')
        endif
        .w_PECONDIZ = 'I'
        .w_OBTEST = CTOD('01-01-1900')
        .w_PENONRES = 'N'
        .w_PEQUADRO = 'FE'
        .DoRTCalc(11,12,.f.)
        .w_PEVALORI = 'N'
        .w_PENOLEGG = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATPIVSP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DATPIVSP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATPIVSP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PE__ANNO,"PE__ANNO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PECAUCON C(5);
      ,t_CCDESCRI C(35);
      ,t_PETIPREG N(3);
      ,t_PENUMREG N(2);
      ,t_PECODIVA C(5);
      ,t_PECONDIZ N(3);
      ,t_PENONRES N(3);
      ,t_PEQUADRO N(3);
      ,t_PEVALORI N(3);
      ,t_PENOLEGG N(3);
      ,CPROWNUM N(10);
      ,t_CCTIPREG C(1);
      ,t_CCFLRIFE C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsai_msibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECAUCON_2_1.controlsource=this.cTrsName+'.t_PECAUCON'
    this.oPgFRm.Page1.oPag.oCCDESCRI_2_2.controlsource=this.cTrsName+'.t_CCDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPREG_2_3.controlsource=this.cTrsName+'.t_PETIPREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPENUMREG_2_4.controlsource=this.cTrsName+'.t_PENUMREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECODIVA_2_5.controlsource=this.cTrsName+'.t_PECODIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECONDIZ_2_6.controlsource=this.cTrsName+'.t_PECONDIZ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPENONRES_2_7.controlsource=this.cTrsName+'.t_PENONRES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPEQUADRO_2_8.controlsource=this.cTrsName+'.t_PEQUADRO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPEVALORI_2_11.controlsource=this.cTrsName+'.t_PEVALORI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPENOLEGG_2_12.controlsource=this.cTrsName+'.t_PENOLEGG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(73)
    this.AddVLine(180)
    this.AddVLine(221)
    this.AddVLine(284)
    this.AddVLine(363)
    this.AddVLine(462)
    this.AddVLine(512)
    this.AddVLine(636)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECAUCON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATPIVSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATPIVSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSP_IDX,2])
      *
      * insert into DATPIVSP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATPIVSP')
        i_extval=cp_InsertValODBCExtFlds(this,'DATPIVSP')
        i_cFldBody=" "+;
                  "(PE__ANNO,PECAUCON,PETIPREG,PENUMREG,PECODIVA"+;
                  ",PECONDIZ,PENONRES,PEQUADRO,PEVALORI,PENOLEGG,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PE__ANNO)+","+cp_ToStrODBCNull(this.w_PECAUCON)+","+cp_ToStrODBC(this.w_PETIPREG)+","+cp_ToStrODBC(this.w_PENUMREG)+","+cp_ToStrODBCNull(this.w_PECODIVA)+;
             ","+cp_ToStrODBC(this.w_PECONDIZ)+","+cp_ToStrODBC(this.w_PENONRES)+","+cp_ToStrODBC(this.w_PEQUADRO)+","+cp_ToStrODBC(this.w_PEVALORI)+","+cp_ToStrODBC(this.w_PENOLEGG)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATPIVSP')
        i_extval=cp_InsertValVFPExtFlds(this,'DATPIVSP')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PE__ANNO',this.w_PE__ANNO)
        INSERT INTO (i_cTable) (;
                   PE__ANNO;
                  ,PECAUCON;
                  ,PETIPREG;
                  ,PENUMREG;
                  ,PECODIVA;
                  ,PECONDIZ;
                  ,PENONRES;
                  ,PEQUADRO;
                  ,PEVALORI;
                  ,PENOLEGG;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PE__ANNO;
                  ,this.w_PECAUCON;
                  ,this.w_PETIPREG;
                  ,this.w_PENUMREG;
                  ,this.w_PECODIVA;
                  ,this.w_PECONDIZ;
                  ,this.w_PENONRES;
                  ,this.w_PEQUADRO;
                  ,this.w_PEVALORI;
                  ,this.w_PENOLEGG;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DATPIVSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not Empty(t_PECAUCON) Or iif(vartype(t_PETIPREG)='N',iif(t_PETIPREG=1,'N','X'),t_PETIPREG)<>'N' Or not Empty(t_PECODIVA)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DATPIVSP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DATPIVSP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not Empty(t_PECAUCON) Or iif(vartype(t_PETIPREG)='N',iif(t_PETIPREG=1,'N','X'),t_PETIPREG)<>'N' Or not Empty(t_PECODIVA)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DATPIVSP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DATPIVSP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PECAUCON="+cp_ToStrODBCNull(this.w_PECAUCON)+;
                     ",PETIPREG="+cp_ToStrODBC(this.w_PETIPREG)+;
                     ",PENUMREG="+cp_ToStrODBC(this.w_PENUMREG)+;
                     ",PECODIVA="+cp_ToStrODBCNull(this.w_PECODIVA)+;
                     ",PECONDIZ="+cp_ToStrODBC(this.w_PECONDIZ)+;
                     ",PENONRES="+cp_ToStrODBC(this.w_PENONRES)+;
                     ",PEQUADRO="+cp_ToStrODBC(this.w_PEQUADRO)+;
                     ",PEVALORI="+cp_ToStrODBC(this.w_PEVALORI)+;
                     ",PENOLEGG="+cp_ToStrODBC(this.w_PENOLEGG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DATPIVSP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PECAUCON=this.w_PECAUCON;
                     ,PETIPREG=this.w_PETIPREG;
                     ,PENUMREG=this.w_PENUMREG;
                     ,PECODIVA=this.w_PECODIVA;
                     ,PECONDIZ=this.w_PECONDIZ;
                     ,PENONRES=this.w_PENONRES;
                     ,PEQUADRO=this.w_PEQUADRO;
                     ,PEVALORI=this.w_PEVALORI;
                     ,PENOLEGG=this.w_PENOLEGG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATPIVSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not Empty(t_PECAUCON) Or iif(vartype(t_PETIPREG)='N',iif(t_PETIPREG=1,'N','X'),t_PETIPREG)<>'N' Or not Empty(t_PECODIVA)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DATPIVSP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not Empty(t_PECAUCON) Or iif(vartype(t_PETIPREG)='N',iif(t_PETIPREG=1,'N','X'),t_PETIPREG)<>'N' Or not Empty(t_PECODIVA)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATPIVSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_PETIPREG<>.w_PETIPREG
          .w_PENUMREG = 0
        endif
        if .o_PECONDIZ<>.w_PECONDIZ
          .w_PECODIVA = iif(.w_Pecondiz='B',Space(5),.w_Pecodiva)
          .link_2_5('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CCTIPREG with this.w_CCTIPREG
      replace t_CCFLRIFE with this.w_CCFLRIFE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPENUMREG_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPENUMREG_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODIVA_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECODIVA_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PECAUCON
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_PECAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_PECAUCON))
          select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PECAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_PECAUCON)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_PECAUCON)+"%");

            select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PECAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oPECAUCON_2_1'),i_cWhere,'GSCG_ACC',"CAUSALI CONTABILI",'GSCG_MPE.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_PECAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_PECAUCON)
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(35))
      this.w_CCTIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_CCFLRIFE = NVL(_Link_.CCFLRIFE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PECAUCON = space(5)
      endif
      this.w_CCDESCRI = space(35)
      this.w_CCTIPREG = space(1)
      this.w_CCFLRIFE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CCFLRIFE$'CF' AND NVL(.w_CCTIPREG,'N')<>'N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PECAUCON = space(5)
        this.w_CCDESCRI = space(35)
        this.w_CCTIPREG = space(1)
        this.w_CCFLRIFE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CCCODICE as CCCODICE201"+ ",link_2_1.CCDESCRI as CCDESCRI201"+ ",link_2_1.CCTIPREG as CCTIPREG201"+ ",link_2_1.CCFLRIFE as CCFLRIFE201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on DATPIVSP.PECAUCON=link_2_1.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and DATPIVSP.PECAUCON=link_2_1.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PECODIVA
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_PECODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_PECODIVA))
          select IVCODIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PECODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PECODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oPECODIVA_2_5'),i_cWhere,'GSAR_AIV',"CODICI IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_PECODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_PECODIVA)
            select IVCODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECODIVA = NVL(_Link_.IVCODIVA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PECODIVA = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_2_2.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_2_2.value=this.w_CCDESCRI
      replace t_CCDESCRI with this.oPgFrm.Page1.oPag.oCCDESCRI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECAUCON_2_1.value==this.w_PECAUCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECAUCON_2_1.value=this.w_PECAUCON
      replace t_PECAUCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECAUCON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPREG_2_3.RadioValue()==this.w_PETIPREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPREG_2_3.SetRadio()
      replace t_PETIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPREG_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENUMREG_2_4.value==this.w_PENUMREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENUMREG_2_4.value=this.w_PENUMREG
      replace t_PENUMREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENUMREG_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODIVA_2_5.value==this.w_PECODIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODIVA_2_5.value=this.w_PECODIVA
      replace t_PECODIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECODIVA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONDIZ_2_6.RadioValue()==this.w_PECONDIZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONDIZ_2_6.SetRadio()
      replace t_PECONDIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONDIZ_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENONRES_2_7.RadioValue()==this.w_PENONRES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENONRES_2_7.SetRadio()
      replace t_PENONRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENONRES_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEQUADRO_2_8.RadioValue()==this.w_PEQUADRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEQUADRO_2_8.SetRadio()
      replace t_PEQUADRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEQUADRO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEVALORI_2_11.RadioValue()==this.w_PEVALORI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEVALORI_2_11.SetRadio()
      replace t_PEVALORI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEVALORI_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENOLEGG_2_12.RadioValue()==this.w_PENOLEGG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENOLEGG_2_12.SetRadio()
      replace t_PENOLEGG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENOLEGG_2_12.value
    endif
    cp_SetControlsValueExtFlds(this,'DATPIVSP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_CCFLRIFE$'CF' AND NVL(.w_CCTIPREG,'N')<>'N') and not(empty(.w_PECAUCON)) and (not Empty(.w_PECAUCON) Or iif(vartype(.w_PETIPREG)='N',iif(.w_PETIPREG=1,'N','X'),.w_PETIPREG)<>'N' Or not Empty(.w_PECODIVA))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECAUCON_2_1
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(iif(.w_PENUMREG<>0, iif(isahe(),controllonr(.w_PETIPREG,.w_PENUMREG),CHKREGIV(.w_PETIPREG,.w_PENUMREG)=1), .t.)) and (.w_PETIPREG $ 'VAC') and (not Empty(.w_PECAUCON) Or iif(vartype(.w_PETIPREG)='N',iif(.w_PETIPREG=1,'N','X'),.w_PETIPREG)<>'N' Or not Empty(.w_PECODIVA))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENUMREG_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il numero di registro inserito non esiste")
      endcase
      if not Empty(.w_PECAUCON) Or iif(vartype(.w_PETIPREG)='N',iif(.w_PETIPREG=1,'N','X'),.w_PETIPREG)<>'N' Or not Empty(.w_PECODIVA)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PETIPREG = this.w_PETIPREG
    this.o_PECONDIZ = this.w_PECONDIZ
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not Empty(t_PECAUCON) Or iif(vartype(t_PETIPREG)='N',iif(t_PETIPREG=1,'N','X'),t_PETIPREG)<>'N' Or not Empty(t_PECODIVA))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PECAUCON=space(5)
      .w_CCDESCRI=space(35)
      .w_PETIPREG=space(1)
      .w_PENUMREG=0
      .w_PECODIVA=space(5)
      .w_PECONDIZ=space(1)
      .w_PENONRES=space(1)
      .w_PEQUADRO=space(2)
      .w_CCTIPREG=space(1)
      .w_CCFLRIFE=space(1)
      .w_PEVALORI=space(1)
      .w_PENOLEGG=space(1)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_PECAUCON))
        .link_2_1('Full')
      endif
      .DoRTCalc(3,3,.f.)
        .w_PETIPREG = 'N'
        .w_PENUMREG = 0
        .w_PECODIVA = iif(.w_Pecondiz='B',Space(5),.w_Pecodiva)
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_PECODIVA))
        .link_2_5('Full')
      endif
        .w_PECONDIZ = 'I'
      .DoRTCalc(8,8,.f.)
        .w_PENONRES = 'N'
        .w_PEQUADRO = 'FE'
      .DoRTCalc(11,12,.f.)
        .w_PEVALORI = 'N'
        .w_PENOLEGG = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PECAUCON = t_PECAUCON
    this.w_CCDESCRI = t_CCDESCRI
    this.w_PETIPREG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPREG_2_3.RadioValue(.t.)
    this.w_PENUMREG = t_PENUMREG
    this.w_PECODIVA = t_PECODIVA
    this.w_PECONDIZ = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONDIZ_2_6.RadioValue(.t.)
    this.w_PENONRES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENONRES_2_7.RadioValue(.t.)
    this.w_PEQUADRO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEQUADRO_2_8.RadioValue(.t.)
    this.w_CCTIPREG = t_CCTIPREG
    this.w_CCFLRIFE = t_CCFLRIFE
    this.w_PEVALORI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEVALORI_2_11.RadioValue(.t.)
    this.w_PENOLEGG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENOLEGG_2_12.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PECAUCON with this.w_PECAUCON
    replace t_CCDESCRI with this.w_CCDESCRI
    replace t_PETIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPREG_2_3.ToRadio()
    replace t_PENUMREG with this.w_PENUMREG
    replace t_PECODIVA with this.w_PECODIVA
    replace t_PECONDIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONDIZ_2_6.ToRadio()
    replace t_PENONRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENONRES_2_7.ToRadio()
    replace t_PEQUADRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEQUADRO_2_8.ToRadio()
    replace t_CCTIPREG with this.w_CCTIPREG
    replace t_CCFLRIFE with this.w_CCFLRIFE
    replace t_PEVALORI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEVALORI_2_11.ToRadio()
    replace t_PENOLEGG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPENOLEGG_2_12.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsai_msiPag1 as StdContainer
  Width  = 795
  height = 453
  stdWidth  = 795
  stdheight = 453
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=1, width=782,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="PECAUCON",Label1="Causale",Field2="PETIPREG",Label2="Tipo registro",Field3="PENUMREG",Label3="N.reg.",Field4="PECODIVA",Label4="Cod. IVA",Field5="PECONDIZ",Label5="Condizione",Field6="PENONRES",Label6="Soggetto",Field7="PEQUADRO",Label7="Quadro",Field8="PEVALORI",Label8="Valorizzazione",Field9="PENOLEGG",Label9="Tipo noleggio",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49470854

  add object oStr_1_2 as StdString with uid="EDNERFMEMJ",Visible=.t., Left=10, Top=420,;
    Alignment=1, Width=85, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=22,;
    width=778+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=23,width=777+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CAU_CONT|VOCIIVA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCCDESCRI_2_2.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CAU_CONT'
        oDropInto=this.oBodyCol.oRow.oPECAUCON_2_1
      case cFile='VOCIIVA'
        oDropInto=this.oBodyCol.oRow.oPECODIVA_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oCCDESCRI_2_2 as StdTrsField with uid="CDIJUAKKGX",rtseq=3,rtrep=.t.,;
    cFormVar="w_CCDESCRI",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione causale",;
    HelpContextID = 45056111,;
    cTotal="", bFixedPos=.t., cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=98, Top=416, InputMask=replicate('X',35)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsai_msiBodyRow as CPBodyRowCnt
  Width=768
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPECAUCON_2_1 as StdTrsField with uid="XSUIXHGZBK",rtseq=2,rtrep=.t.,;
    cFormVar="w_PECAUCON",value=space(5),;
    ToolTipText = "Causale contabile",;
    HelpContextID = 46887748,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_PECAUCON"

  func oPECAUCON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECAUCON_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECAUCON_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oPECAUCON_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"CAUSALI CONTABILI",'GSCG_MPE.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oPECAUCON_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_PECAUCON
    i_obj.ecpSave()
  endproc

  add object oPETIPREG_2_3 as StdTrsCombo with uid="BXXQXWIDLA",rtrep=.t.,;
    cFormVar="w_PETIPREG", RowSource=""+"Nessun filtro,"+"Vendite,"+"Acquisti,"+"Corr. scorporo" , ;
    ToolTipText = "Tipo registro",;
    HelpContextID = 242973891,;
    Height=22, Width=102, Left=66, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPETIPREG_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PETIPREG,&i_cF..t_PETIPREG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'V',;
    iif(xVal =3,'A',;
    iif(xVal =4,'C',;
    space(1))))))
  endfunc
  func oPETIPREG_2_3.GetRadio()
    this.Parent.oContained.w_PETIPREG = this.RadioValue()
    return .t.
  endfunc

  func oPETIPREG_2_3.ToRadio()
    this.Parent.oContained.w_PETIPREG=trim(this.Parent.oContained.w_PETIPREG)
    return(;
      iif(this.Parent.oContained.w_PETIPREG=='N',1,;
      iif(this.Parent.oContained.w_PETIPREG=='V',2,;
      iif(this.Parent.oContained.w_PETIPREG=='A',3,;
      iif(this.Parent.oContained.w_PETIPREG=='C',4,;
      0)))))
  endfunc

  func oPETIPREG_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPENUMREG_2_4 as StdTrsField with uid="UEMHAMRFNW",rtseq=5,rtrep=.t.,;
    cFormVar="w_PENUMREG",value=0,;
    ToolTipText = "Numero registro",;
    HelpContextID = 245357763,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il numero di registro inserito non esiste",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=173, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oPENUMREG_2_4.mCond()
    with this.Parent.oContained
      return (.w_PETIPREG $ 'VAC')
    endwith
  endfunc

  func oPENUMREG_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_PENUMREG<>0, iif(isahe(),controllonr(.w_PETIPREG,.w_PENUMREG),CHKREGIV(.w_PETIPREG,.w_PENUMREG)=1), .t.))
    endwith
    return bRes
  endfunc

  add object oPECODIVA_2_5 as StdTrsField with uid="IUFAPDQDPD",rtseq=6,rtrep=.t.,;
    cFormVar="w_PECODIVA",value=space(5),;
    ToolTipText = "Codice IVA",;
    HelpContextID = 137792713,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=215, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_PECODIVA"

  func oPECODIVA_2_5.mCond()
    with this.Parent.oContained
      return (.w_Pecondiz $ 'IE')
    endwith
  endfunc

  func oPECODIVA_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECODIVA_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECODIVA_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oPECODIVA_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"CODICI IVA",'',this.parent.oContained
  endproc
  proc oPECODIVA_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_PECODIVA
    i_obj.ecpSave()
  endproc

  add object oPECONDIZ_2_6 as StdTrsCombo with uid="OYKLTNRZIB",rtrep=.t.,;
    cFormVar="w_PECONDIZ", RowSource=""+"Includi,"+"Escludi" , ;
    ToolTipText = "Condizione di estrazione",;
    HelpContextID = 211193008,;
    Height=22, Width=72, Left=278, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPECONDIZ_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECONDIZ,&i_cF..t_PECONDIZ),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'E',;
    space(1))))
  endfunc
  func oPECONDIZ_2_6.GetRadio()
    this.Parent.oContained.w_PECONDIZ = this.RadioValue()
    return .t.
  endfunc

  func oPECONDIZ_2_6.ToRadio()
    this.Parent.oContained.w_PECONDIZ=trim(this.Parent.oContained.w_PECONDIZ)
    return(;
      iif(this.Parent.oContained.w_PECONDIZ=='I',1,;
      iif(this.Parent.oContained.w_PECONDIZ=='E',2,;
      0)))
  endfunc

  func oPECONDIZ_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPENONRES_2_7 as StdTrsCombo with uid="EYOHBHLZDM",rtrep=.t.,;
    cFormVar="w_PENONRES", RowSource=""+"Non residente,"+"Residente" , ;
    HelpContextID = 244702391,;
    Height=22, Width=92, Left=357, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPENONRES_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PENONRES,&i_cF..t_PENONRES),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oPENONRES_2_7.GetRadio()
    this.Parent.oContained.w_PENONRES = this.RadioValue()
    return .t.
  endfunc

  func oPENONRES_2_7.ToRadio()
    this.Parent.oContained.w_PENONRES=trim(this.Parent.oContained.w_PENONRES)
    return(;
      iif(this.Parent.oContained.w_PENONRES=='S',1,;
      iif(this.Parent.oContained.w_PENONRES=='N',2,;
      0)))
  endfunc

  func oPENONRES_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPEQUADRO_2_8 as StdTrsCombo with uid="CXHJNKUUWI",rtrep=.t.,;
    cFormVar="w_PEQUADRO", RowSource=""+"FE,"+"FR,"+"DF,"+"NE,"+"NR,"+"FN,"+"SE" , ;
    HelpContextID = 44061509,;
    Height=22, Width=42, Left=456, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPEQUADRO_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PEQUADRO,&i_cF..t_PEQUADRO),this.value)
    return(iif(xVal =1,'FE',;
    iif(xVal =2,'FR',;
    iif(xVal =3,'DF',;
    iif(xVal =4,'NE',;
    iif(xVal =5,'NR',;
    iif(xVal =6,'FN',;
    iif(xVal =7,'SE',;
    space(2)))))))))
  endfunc
  func oPEQUADRO_2_8.GetRadio()
    this.Parent.oContained.w_PEQUADRO = this.RadioValue()
    return .t.
  endfunc

  func oPEQUADRO_2_8.ToRadio()
    this.Parent.oContained.w_PEQUADRO=trim(this.Parent.oContained.w_PEQUADRO)
    return(;
      iif(this.Parent.oContained.w_PEQUADRO=='FE',1,;
      iif(this.Parent.oContained.w_PEQUADRO=='FR',2,;
      iif(this.Parent.oContained.w_PEQUADRO=='DF',3,;
      iif(this.Parent.oContained.w_PEQUADRO=='NE',4,;
      iif(this.Parent.oContained.w_PEQUADRO=='NR',5,;
      iif(this.Parent.oContained.w_PEQUADRO=='FN',6,;
      iif(this.Parent.oContained.w_PEQUADRO=='SE',7,;
      0))))))))
  endfunc

  func oPEQUADRO_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPEVALORI_2_11 as StdTrsCombo with uid="FVPVEQAZRG",rtrep=.t.,;
    cFormVar="w_PEVALORI", RowSource=""+"Autofattura,"+"IVA non esposta,"+"Reverse charge,"+"Nessuna" , ;
    HelpContextID = 238854975,;
    Height=22, Width=116, Left=506, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPEVALORI_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PEVALORI,&i_cF..t_PEVALORI),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'I',;
    iif(xVal =3,'R',;
    iif(xVal =4,'N',;
    space(1))))))
  endfunc
  func oPEVALORI_2_11.GetRadio()
    this.Parent.oContained.w_PEVALORI = this.RadioValue()
    return .t.
  endfunc

  func oPEVALORI_2_11.ToRadio()
    this.Parent.oContained.w_PEVALORI=trim(this.Parent.oContained.w_PEVALORI)
    return(;
      iif(this.Parent.oContained.w_PEVALORI=='A',1,;
      iif(this.Parent.oContained.w_PEVALORI=='I',2,;
      iif(this.Parent.oContained.w_PEVALORI=='R',3,;
      iif(this.Parent.oContained.w_PEVALORI=='N',4,;
      0)))))
  endfunc

  func oPEVALORI_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPENOLEGG_2_12 as StdTrsCombo with uid="YOPLNNQFPT",rtrep=.t.,;
    cFormVar="w_PENOLEGG", RowSource=""+"Autovettura,"+"Caravan,"+"Altri veicoli,"+"Unit� da diporto,"+"Aeromobili,"+"Nessuno" , ;
    ToolTipText = "Tipo noleggio",;
    HelpContextID = 196467907,;
    Height=22, Width=132, Left=631, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPENOLEGG_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PENOLEGG,&i_cF..t_PENOLEGG),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'B',;
    iif(xVal =3,'C',;
    iif(xVal =4,'D',;
    iif(xVal =5,'E',;
    iif(xVal =6,'N',;
    space(1))))))))
  endfunc
  func oPENOLEGG_2_12.GetRadio()
    this.Parent.oContained.w_PENOLEGG = this.RadioValue()
    return .t.
  endfunc

  func oPENOLEGG_2_12.ToRadio()
    this.Parent.oContained.w_PENOLEGG=trim(this.Parent.oContained.w_PENOLEGG)
    return(;
      iif(this.Parent.oContained.w_PENOLEGG=='A',1,;
      iif(this.Parent.oContained.w_PENOLEGG=='B',2,;
      iif(this.Parent.oContained.w_PENOLEGG=='C',3,;
      iif(this.Parent.oContained.w_PENOLEGG=='D',4,;
      iif(this.Parent.oContained.w_PENOLEGG=='E',5,;
      iif(this.Parent.oContained.w_PENOLEGG=='N',6,;
      0)))))))
  endfunc

  func oPENOLEGG_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oPECAUCON_2_1.When()
    return(.t.)
  proc oPECAUCON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPECAUCON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_msi','DATPIVSP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PE__ANNO=DATPIVSP.PE__ANNO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
