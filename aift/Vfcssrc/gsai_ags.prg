* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_ags                                                        *
*              Generazione file telematico                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-25                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_ags"))

* --- Class definition
define class tgsai_ags as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 907
  Height = 430+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-07"
  HelpContextID=42600297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=55

  * --- Constant Properties
  ANTIVAGS_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  SEDIAZIE_IDX = 0
  cFile = "ANTIVAGS"
  cKeySelect = "AISERIAL"
  cKeyWhere  = "AISERIAL=this.w_AISERIAL"
  cKeyWhereODBC = '"AISERIAL="+cp_ToStrODBC(this.w_AISERIAL)';

  cKeyWhereODBCqualified = '"ANTIVAGS.AISERIAL="+cp_ToStrODBC(this.w_AISERIAL)';

  cPrg = "gsai_ags"
  cComment = "Generazione file telematico"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AI__ANNO = 0
  o_AI__ANNO = 0
  w_ANNO = 0
  w_SERIALE = space(10)
  o_SERIALE = space(10)
  w_CODAZI = space(5)
  w_AITIPINV = 0
  o_AITIPINV = 0
  w_AIPROTEC = space(17)
  w_AIPROTEL = 0
  w_AIPRODOC = 0
  w_AISOCINC = space(1)
  w_AZPIVAZI = space(12)
  w_AISERIAL = space(10)
  w_AZCODFIS = space(16)
  w_AZPERAZI = space(1)
  w_AZCODNAZ = space(3)
  w_AZLOCAZI = space(30)
  w_AZPROAZI = space(2)
  w_AZNAGAZI = space(5)
  w_AZTELEFO = space(18)
  w_AZ_EMAIL = space(254)
  w_AZIVACOF = space(16)
  w_SETIPRIF = space(2)
  w_CODAZI1 = space(5)
  w_AZTELFAX = space(18)
  w_CODAZI2 = space(5)
  w_COGTIT1 = space(25)
  w_NOMTIT1 = space(25)
  w_DATNAS1 = ctod('  /  /  ')
  w_LOCTIT1 = space(30)
  w_PROTIT1 = space(2)
  w_SESSO1 = space(1)
  w_TELEFO1 = space(18)
  w_AICODFIS = space(16)
  w_AIPARIVA = space(11)
  w_AICOGNOM = space(24)
  w_AI__NOME = space(20)
  w_AI_SESSO = space(1)
  w_AIDATNAS = ctod('  /  /  ')
  w_AILOCNAS = space(40)
  w_AIPRONAS = space(2)
  w_AIRAGSOC = space(60)
  w_AIDOMFIS = space(40)
  w_AIPRODOM = space(2)
  w_DirName = space(200)
  w_AINUMMOD = 0
  w_MAXNORIG = 0
  w_AITIPFOR = space(2)
  o_AITIPFOR = space(2)
  w_AICODINT = space(16)
  o_AICODINT = space(16)
  w_AICODCAF = space(5)
  w_AIIMPTRA = space(1)
  w_AIDATIMP = ctod('  /  /  ')
  w_AINOMFIL = space(254)
  o_AINOMFIL = space(254)
  w_ESTDATVER = space(1)
  w_ESCNUMFAT = space(1)
  w_SELOCALI = space(30)
  w_SEPROVIN = space(30)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_AISERIAL = this.W_AISERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANTIVAGS','gsai_ags')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_agsPag1","gsai_ags",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Comunicazione/contribuente")
      .Pages(1).HelpContextID = 254321956
      .Pages(2).addobject("oPag","tgsai_agsPag2","gsai_ags",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati trasmissione")
      .Pages(2).HelpContextID = 191846011
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsai_ags
    Getblackbversion()
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TITOLARI'
    this.cWorkTables[3]='SEDIAZIE'
    this.cWorkTables[4]='ANTIVAGS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVAGS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVAGS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AISERIAL = NVL(AISERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANTIVAGS where AISERIAL=KeySet.AISERIAL
    *
    i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVAGS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVAGS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVAGS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AISERIAL',this.w_AISERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SERIALE = space(10)
        .w_CODAZI = i_CODAZI
        .w_AZPIVAZI = space(12)
        .w_AZCODFIS = space(16)
        .w_AZPERAZI = space(1)
        .w_AZCODNAZ = space(3)
        .w_AZLOCAZI = space(30)
        .w_AZPROAZI = space(2)
        .w_AZNAGAZI = space(5)
        .w_AZTELEFO = space(18)
        .w_AZ_EMAIL = space(254)
        .w_AZIVACOF = space(16)
        .w_SETIPRIF = 'SL'
        .w_CODAZI1 = i_CODAZI
        .w_AZTELFAX = space(18)
        .w_CODAZI2 = i_CODAZI
        .w_COGTIT1 = space(25)
        .w_NOMTIT1 = space(25)
        .w_DATNAS1 = ctod("  /  /  ")
        .w_LOCTIT1 = space(30)
        .w_PROTIT1 = space(2)
        .w_SESSO1 = space(1)
        .w_TELEFO1 = space(18)
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_MAXNORIG = 15000
        .w_ESTDATVER = 'N'
        .w_ESCNUMFAT = 'N'
        .w_SELOCALI = space(30)
        .w_SEPROVIN = space(30)
        .w_AI__ANNO = NVL(AI__ANNO,0)
        .w_ANNO = .w_AI__ANNO
          .link_1_2('Load')
          .link_1_4('Load')
        .w_AITIPINV = NVL(AITIPINV,0)
        .w_AIPROTEC = NVL(AIPROTEC,space(17))
        .w_AIPROTEL = NVL(AIPROTEL,0)
        .w_AIPRODOC = NVL(AIPRODOC,0)
        .w_AISOCINC = NVL(AISOCINC,space(1))
        .w_AISERIAL = NVL(AISERIAL,space(10))
        .op_AISERIAL = .w_AISERIAL
          .link_1_22('Load')
          .link_1_24('Load')
        .w_AICODFIS = NVL(AICODFIS,space(16))
        .w_AIPARIVA = NVL(AIPARIVA,space(11))
        .w_AICOGNOM = NVL(AICOGNOM,space(24))
        .w_AI__NOME = NVL(AI__NOME,space(20))
        .w_AI_SESSO = NVL(AI_SESSO,space(1))
        .w_AIDATNAS = NVL(cp_ToDate(AIDATNAS),ctod("  /  /  "))
        .w_AILOCNAS = NVL(AILOCNAS,space(40))
        .w_AIPRONAS = NVL(AIPRONAS,space(2))
        .w_AIRAGSOC = NVL(AIRAGSOC,space(60))
        .w_AIDOMFIS = NVL(AIDOMFIS,space(40))
        .w_AIPRODOM = NVL(AIPRODOM,space(2))
        .w_AINUMMOD = NVL(AINUMMOD,0)
        .w_AITIPFOR = NVL(AITIPFOR,space(2))
        .w_AICODINT = NVL(AICODINT,space(16))
        .w_AICODCAF = NVL(AICODCAF,space(5))
        .w_AIIMPTRA = NVL(AIIMPTRA,space(1))
        .w_AIDATIMP = NVL(cp_ToDate(AIDATIMP),ctod("  /  /  "))
        .w_AINOMFIL = NVL(AINOMFIL,space(254))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ANTIVAGS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AI__ANNO = 0
      .w_ANNO = 0
      .w_SERIALE = space(10)
      .w_CODAZI = space(5)
      .w_AITIPINV = 0
      .w_AIPROTEC = space(17)
      .w_AIPROTEL = 0
      .w_AIPRODOC = 0
      .w_AISOCINC = space(1)
      .w_AZPIVAZI = space(12)
      .w_AISERIAL = space(10)
      .w_AZCODFIS = space(16)
      .w_AZPERAZI = space(1)
      .w_AZCODNAZ = space(3)
      .w_AZLOCAZI = space(30)
      .w_AZPROAZI = space(2)
      .w_AZNAGAZI = space(5)
      .w_AZTELEFO = space(18)
      .w_AZ_EMAIL = space(254)
      .w_AZIVACOF = space(16)
      .w_SETIPRIF = space(2)
      .w_CODAZI1 = space(5)
      .w_AZTELFAX = space(18)
      .w_CODAZI2 = space(5)
      .w_COGTIT1 = space(25)
      .w_NOMTIT1 = space(25)
      .w_DATNAS1 = ctod("  /  /  ")
      .w_LOCTIT1 = space(30)
      .w_PROTIT1 = space(2)
      .w_SESSO1 = space(1)
      .w_TELEFO1 = space(18)
      .w_AICODFIS = space(16)
      .w_AIPARIVA = space(11)
      .w_AICOGNOM = space(24)
      .w_AI__NOME = space(20)
      .w_AI_SESSO = space(1)
      .w_AIDATNAS = ctod("  /  /  ")
      .w_AILOCNAS = space(40)
      .w_AIPRONAS = space(2)
      .w_AIRAGSOC = space(60)
      .w_AIDOMFIS = space(40)
      .w_AIPRODOM = space(2)
      .w_DirName = space(200)
      .w_AINUMMOD = 0
      .w_MAXNORIG = 0
      .w_AITIPFOR = space(2)
      .w_AICODINT = space(16)
      .w_AICODCAF = space(5)
      .w_AIIMPTRA = space(1)
      .w_AIDATIMP = ctod("  /  /  ")
      .w_AINOMFIL = space(254)
      .w_ESTDATVER = space(1)
      .w_ESCNUMFAT = space(1)
      .w_SELOCALI = space(30)
      .w_SEPROVIN = space(30)
      if .cFunction<>"Filter"
        .w_AI__ANNO = year(i_DATSYS)-1
        .w_ANNO = .w_AI__ANNO
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_ANNO))
          .link_1_2('Full')
          endif
          .DoRTCalc(3,3,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CODAZI))
          .link_1_4('Full')
          endif
        .w_AITIPINV = iif(Empty(.w_Seriale),0,1)
        .w_AIPROTEC = SPACE(17)
          .DoRTCalc(7,7,.f.)
        .w_AIPRODOC = 0
        .w_AISOCINC = '0'
          .DoRTCalc(10,20,.f.)
        .w_SETIPRIF = 'SL'
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_CODAZI1))
          .link_1_22('Full')
          endif
          .DoRTCalc(23,23,.f.)
        .w_CODAZI2 = i_CODAZI
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_CODAZI2))
          .link_1_24('Full')
          endif
          .DoRTCalc(25,31,.f.)
        .w_AICODFIS = IIF(LEN(ALLTRIM(.w_AZCODFIS))<=16, LEFT(ALLTRIM(.w_AZCODFIS),16), RIGHT(ALLTRIM(.w_AZCODFIS),16))
        .w_AIPARIVA = left(.w_AZPIVAZI, 11)
        .w_AICOGNOM = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_COGTIT1), space(24)), 24)
        .w_AI__NOME = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_NOMTIT1), space(20)), 20)
        .w_AI_SESSO = iif(.w_AZPERAZI='S' and not empty(.w_SESSO1), .w_SESSO1, 'M')
        .w_AIDATNAS = iif(.w_AZPERAZI='S', .w_DATNAS1, CTOD("  -  -    "))
        .w_AILOCNAS = left(iif(.w_AZPERAZI='S', UPPER(.w_LOCTIT1), space(40)), 40)
        .w_AIPRONAS = iif(.w_AZPERAZI='S', UPPER(.w_PROTIT1), space(2))
        .w_AIRAGSOC = LEFT(iif(.w_AZPERAZI<>'S', UPPER(g_RAGAZI), space(60)), 60)
        .w_AIDOMFIS = left(iif(.w_AZPERAZI<>'S', iif(not empty(nvl(.w_SELOCALI,'')), UPPER(.w_SELOCALI), UPPER(.w_AZLOCAZI)), space(40)), 40)
        .w_AIPRODOM = iif(.w_AZPERAZI<>'S', iif(not empty(nvl(.w_SEPROVIN,'')), UPPER(.w_SEPROVIN), UPPER(.w_AZPROAZI)), space(2))
        .w_DirName = sys(5)+sys(2003)+'\'
          .DoRTCalc(44,44,.f.)
        .w_MAXNORIG = 15000
        .w_AITIPFOR = '01'
        .w_AICODINT = iif(.w_AITIPFOR='01', space(16), .w_AICODINT)
        .w_AICODCAF = iif(EMPTY(.w_AICODINT), space(5), .w_AICODCAF)
        .w_AIIMPTRA = '1'
        .w_AIDATIMP = iif(EMPTY(.w_AICODINT), CTOD('  -  -    '), .w_AIDATIMP)
          .DoRTCalc(51,51,.f.)
        .w_ESTDATVER = 'N'
        .w_ESCNUMFAT = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVAGS')
    this.DoRTCalc(54,55,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"AICGF","i_codazi,w_AISERIAL")
      .op_codazi = .w_codazi
      .op_AISERIAL = .w_AISERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAI__ANNO_1_1.enabled = i_bVal
      .Page1.oPag.oAITIPINV_1_5.enabled = i_bVal
      .Page1.oPag.oAIPROTEC_1_6.enabled = i_bVal
      .Page1.oPag.oAIPRODOC_1_8.enabled = i_bVal
      .Page1.oPag.oAISOCINC_1_9.enabled = i_bVal
      .Page1.oPag.oAICODFIS_1_33.enabled = i_bVal
      .Page1.oPag.oAIPARIVA_1_34.enabled = i_bVal
      .Page1.oPag.oAICOGNOM_1_37.enabled = i_bVal
      .Page1.oPag.oAI__NOME_1_39.enabled = i_bVal
      .Page1.oPag.oAI_SESSO_1_41.enabled_(i_bVal)
      .Page1.oPag.oAIDATNAS_1_43.enabled = i_bVal
      .Page1.oPag.oAILOCNAS_1_45.enabled = i_bVal
      .Page1.oPag.oAIPRONAS_1_46.enabled = i_bVal
      .Page1.oPag.oAIRAGSOC_1_48.enabled = i_bVal
      .Page1.oPag.oAIDOMFIS_1_64.enabled = i_bVal
      .Page1.oPag.oAIPRODOM_1_65.enabled = i_bVal
      .Page2.oPag.oMAXNORIG_2_3.enabled = i_bVal
      .Page2.oPag.oAITIPFOR_2_5.enabled = i_bVal
      .Page2.oPag.oAICODINT_2_6.enabled = i_bVal
      .Page2.oPag.oAICODCAF_2_8.enabled = i_bVal
      .Page2.oPag.oAIIMPTRA_2_10.enabled = i_bVal
      .Page2.oPag.oAIDATIMP_2_11.enabled = i_bVal
      .Page2.oPag.oAINOMFIL_2_13.enabled = i_bVal
      .Page2.oPag.oESTDATVER_2_16.enabled = i_bVal
      .Page2.oPag.oESCNUMFAT_2_17.enabled = i_bVal
      .Page2.oPag.oBtn_2_15.enabled = i_bVal
      .Page2.oPag.oBtn_2_18.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ANTIVAGS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI__ANNO,"AI__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITIPINV,"AITIPINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPROTEC,"AIPROTEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPROTEL,"AIPROTEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPRODOC,"AIPRODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISOCINC,"AISOCINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERIAL,"AISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODFIS,"AICODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPARIVA,"AIPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOGNOM,"AICOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI__NOME,"AI__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI_SESSO,"AI_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATNAS,"AIDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AILOCNAS,"AILOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPRONAS,"AIPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIRAGSOC,"AIRAGSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDOMFIS,"AIDOMFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPRODOM,"AIPRODOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINUMMOD,"AINUMMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITIPFOR,"AITIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODINT,"AICODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODCAF,"AICODCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIMPTRA,"AIIMPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATIMP,"AIDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINOMFIL,"AINOMFIL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])
    i_lTable = "ANTIVAGS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANTIVAGS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANTIVAGS_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"AICGF","i_codazi,w_AISERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANTIVAGS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVAGS')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVAGS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AI__ANNO,AITIPINV,AIPROTEC,AIPROTEL,AIPRODOC"+;
                  ",AISOCINC,AISERIAL,AICODFIS,AIPARIVA,AICOGNOM"+;
                  ",AI__NOME,AI_SESSO,AIDATNAS,AILOCNAS,AIPRONAS"+;
                  ",AIRAGSOC,AIDOMFIS,AIPRODOM,AINUMMOD,AITIPFOR"+;
                  ",AICODINT,AICODCAF,AIIMPTRA,AIDATIMP,AINOMFIL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AI__ANNO)+;
                  ","+cp_ToStrODBC(this.w_AITIPINV)+;
                  ","+cp_ToStrODBC(this.w_AIPROTEC)+;
                  ","+cp_ToStrODBC(this.w_AIPROTEL)+;
                  ","+cp_ToStrODBC(this.w_AIPRODOC)+;
                  ","+cp_ToStrODBC(this.w_AISOCINC)+;
                  ","+cp_ToStrODBC(this.w_AISERIAL)+;
                  ","+cp_ToStrODBC(this.w_AICODFIS)+;
                  ","+cp_ToStrODBC(this.w_AIPARIVA)+;
                  ","+cp_ToStrODBC(this.w_AICOGNOM)+;
                  ","+cp_ToStrODBC(this.w_AI__NOME)+;
                  ","+cp_ToStrODBC(this.w_AI_SESSO)+;
                  ","+cp_ToStrODBC(this.w_AIDATNAS)+;
                  ","+cp_ToStrODBC(this.w_AILOCNAS)+;
                  ","+cp_ToStrODBC(this.w_AIPRONAS)+;
                  ","+cp_ToStrODBC(this.w_AIRAGSOC)+;
                  ","+cp_ToStrODBC(this.w_AIDOMFIS)+;
                  ","+cp_ToStrODBC(this.w_AIPRODOM)+;
                  ","+cp_ToStrODBC(this.w_AINUMMOD)+;
                  ","+cp_ToStrODBC(this.w_AITIPFOR)+;
                  ","+cp_ToStrODBC(this.w_AICODINT)+;
                  ","+cp_ToStrODBC(this.w_AICODCAF)+;
                  ","+cp_ToStrODBC(this.w_AIIMPTRA)+;
                  ","+cp_ToStrODBC(this.w_AIDATIMP)+;
                  ","+cp_ToStrODBC(this.w_AINOMFIL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVAGS')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVAGS')
        cp_CheckDeletedKey(i_cTable,0,'AISERIAL',this.w_AISERIAL)
        INSERT INTO (i_cTable);
              (AI__ANNO,AITIPINV,AIPROTEC,AIPROTEL,AIPRODOC,AISOCINC,AISERIAL,AICODFIS,AIPARIVA,AICOGNOM,AI__NOME,AI_SESSO,AIDATNAS,AILOCNAS,AIPRONAS,AIRAGSOC,AIDOMFIS,AIPRODOM,AINUMMOD,AITIPFOR,AICODINT,AICODCAF,AIIMPTRA,AIDATIMP,AINOMFIL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AI__ANNO;
                  ,this.w_AITIPINV;
                  ,this.w_AIPROTEC;
                  ,this.w_AIPROTEL;
                  ,this.w_AIPRODOC;
                  ,this.w_AISOCINC;
                  ,this.w_AISERIAL;
                  ,this.w_AICODFIS;
                  ,this.w_AIPARIVA;
                  ,this.w_AICOGNOM;
                  ,this.w_AI__NOME;
                  ,this.w_AI_SESSO;
                  ,this.w_AIDATNAS;
                  ,this.w_AILOCNAS;
                  ,this.w_AIPRONAS;
                  ,this.w_AIRAGSOC;
                  ,this.w_AIDOMFIS;
                  ,this.w_AIPRODOM;
                  ,this.w_AINUMMOD;
                  ,this.w_AITIPFOR;
                  ,this.w_AICODINT;
                  ,this.w_AICODCAF;
                  ,this.w_AIIMPTRA;
                  ,this.w_AIDATIMP;
                  ,this.w_AINOMFIL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANTIVAGS_IDX,i_nConn)
      *
      * update ANTIVAGS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVAGS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AI__ANNO="+cp_ToStrODBC(this.w_AI__ANNO)+;
             ",AITIPINV="+cp_ToStrODBC(this.w_AITIPINV)+;
             ",AIPROTEC="+cp_ToStrODBC(this.w_AIPROTEC)+;
             ",AIPROTEL="+cp_ToStrODBC(this.w_AIPROTEL)+;
             ",AIPRODOC="+cp_ToStrODBC(this.w_AIPRODOC)+;
             ",AISOCINC="+cp_ToStrODBC(this.w_AISOCINC)+;
             ",AICODFIS="+cp_ToStrODBC(this.w_AICODFIS)+;
             ",AIPARIVA="+cp_ToStrODBC(this.w_AIPARIVA)+;
             ",AICOGNOM="+cp_ToStrODBC(this.w_AICOGNOM)+;
             ",AI__NOME="+cp_ToStrODBC(this.w_AI__NOME)+;
             ",AI_SESSO="+cp_ToStrODBC(this.w_AI_SESSO)+;
             ",AIDATNAS="+cp_ToStrODBC(this.w_AIDATNAS)+;
             ",AILOCNAS="+cp_ToStrODBC(this.w_AILOCNAS)+;
             ",AIPRONAS="+cp_ToStrODBC(this.w_AIPRONAS)+;
             ",AIRAGSOC="+cp_ToStrODBC(this.w_AIRAGSOC)+;
             ",AIDOMFIS="+cp_ToStrODBC(this.w_AIDOMFIS)+;
             ",AIPRODOM="+cp_ToStrODBC(this.w_AIPRODOM)+;
             ",AINUMMOD="+cp_ToStrODBC(this.w_AINUMMOD)+;
             ",AITIPFOR="+cp_ToStrODBC(this.w_AITIPFOR)+;
             ",AICODINT="+cp_ToStrODBC(this.w_AICODINT)+;
             ",AICODCAF="+cp_ToStrODBC(this.w_AICODCAF)+;
             ",AIIMPTRA="+cp_ToStrODBC(this.w_AIIMPTRA)+;
             ",AIDATIMP="+cp_ToStrODBC(this.w_AIDATIMP)+;
             ",AINOMFIL="+cp_ToStrODBC(this.w_AINOMFIL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVAGS')
        i_cWhere = cp_PKFox(i_cTable  ,'AISERIAL',this.w_AISERIAL  )
        UPDATE (i_cTable) SET;
              AI__ANNO=this.w_AI__ANNO;
             ,AITIPINV=this.w_AITIPINV;
             ,AIPROTEC=this.w_AIPROTEC;
             ,AIPROTEL=this.w_AIPROTEL;
             ,AIPRODOC=this.w_AIPRODOC;
             ,AISOCINC=this.w_AISOCINC;
             ,AICODFIS=this.w_AICODFIS;
             ,AIPARIVA=this.w_AIPARIVA;
             ,AICOGNOM=this.w_AICOGNOM;
             ,AI__NOME=this.w_AI__NOME;
             ,AI_SESSO=this.w_AI_SESSO;
             ,AIDATNAS=this.w_AIDATNAS;
             ,AILOCNAS=this.w_AILOCNAS;
             ,AIPRONAS=this.w_AIPRONAS;
             ,AIRAGSOC=this.w_AIRAGSOC;
             ,AIDOMFIS=this.w_AIDOMFIS;
             ,AIPRODOM=this.w_AIPRODOM;
             ,AINUMMOD=this.w_AINUMMOD;
             ,AITIPFOR=this.w_AITIPFOR;
             ,AICODINT=this.w_AICODINT;
             ,AICODCAF=this.w_AICODCAF;
             ,AIIMPTRA=this.w_AIIMPTRA;
             ,AIDATIMP=this.w_AIDATIMP;
             ,AINOMFIL=this.w_AINOMFIL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANTIVAGS_IDX,i_nConn)
      *
      * delete ANTIVAGS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AISERIAL',this.w_AISERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_ANNO = .w_AI__ANNO
          .link_1_2('Full')
        .DoRTCalc(3,3,.t.)
          .link_1_4('Full')
        if .o_SERIALE<>.w_SERIALE
            .w_AITIPINV = iif(Empty(.w_Seriale),0,1)
        endif
        if .o_AITIPINV<>.w_AITIPINV
            .w_AIPROTEC = SPACE(17)
        endif
        .DoRTCalc(7,7,.t.)
        if .o_AITIPINV<>.w_AITIPINV
            .w_AIPRODOC = 0
        endif
        .DoRTCalc(9,21,.t.)
          .link_1_22('Full')
        .DoRTCalc(23,23,.t.)
          .link_1_24('Full')
        .DoRTCalc(25,46,.t.)
        if .o_AITIPFOR<>.w_AITIPFOR
            .w_AICODINT = iif(.w_AITIPFOR='01', space(16), .w_AICODINT)
        endif
        if .o_AICODINT<>.w_AICODINT
            .w_AICODCAF = iif(EMPTY(.w_AICODINT), space(5), .w_AICODCAF)
        endif
        if .o_AICODINT<>.w_AICODINT
            .w_AIIMPTRA = '1'
        endif
        if .o_AICODINT<>.w_AICODINT
            .w_AIDATIMP = iif(EMPTY(.w_AICODINT), CTOD('  -  -    '), .w_AIDATIMP)
        endif
        if .o_AI__ANNO<>.w_AI__ANNO
          .Calculate_ZJFMMVWDUN()
        endif
        if .o_AINOMFIL<>.w_AINOMFIL
          .Calculate_ZFEOJBUZPN()
        endif
        if .o_AITIPINV<>.w_AITIPINV
          .Calculate_LYRTWZRLMI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"AICGF","i_codazi,w_AISERIAL")
          .op_AISERIAL = .w_AISERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(51,55,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_KHSUCLYDCK()
    with this
          * --- Ripulisci identificativo comunicazione
          GSAI_BSD(this;
             )
    endwith
  endproc
  proc Calculate_ZJFMMVWDUN()
    with this
          * --- Inizializzo nome file
          .w_AINOMFIL = iif(not empty(.w_AINOMFIL), .w_AINOMFIL, left(.w_DirName+'SP'+alltrim(i_CODAZI)+STR(.w_AI__ANNO,4)+.w_AISERIAL+'.ART21'+space(254),254))
    endwith
  endproc
  proc Calculate_ZFEOJBUZPN()
    with this
          * --- Inizializzo nome file
          .w_AINOMFIL = iif(not empty(.w_AINOMFIL), .w_AINOMFIL, left(.w_DirName+'SP'+alltrim(i_CODAZI)+STR(.w_AI__ANNO,4)+.w_AISERIAL+'.ART21'+space(254),254))
    endwith
  endproc
  proc Calculate_LYRTWZRLMI()
    with this
          * --- Ricalcola valore tipologia  invio
          .w_AITIPINV = iif(Not Empty(.w_Seriale) and .w_Aitipinv<>0,.w_Aitipinv,iif(Empty(.w_Seriale),0,1))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAITIPINV_1_5.enabled = this.oPgFrm.Page1.oPag.oAITIPINV_1_5.mCond()
    this.oPgFrm.Page1.oPag.oAIPROTEC_1_6.enabled = this.oPgFrm.Page1.oPag.oAIPROTEC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oAIPRODOC_1_8.enabled = this.oPgFrm.Page1.oPag.oAIPRODOC_1_8.mCond()
    this.oPgFrm.Page1.oPag.oAICOGNOM_1_37.enabled = this.oPgFrm.Page1.oPag.oAICOGNOM_1_37.mCond()
    this.oPgFrm.Page1.oPag.oAI__NOME_1_39.enabled = this.oPgFrm.Page1.oPag.oAI__NOME_1_39.mCond()
    this.oPgFrm.Page1.oPag.oAI_SESSO_1_41.enabled_(this.oPgFrm.Page1.oPag.oAI_SESSO_1_41.mCond())
    this.oPgFrm.Page1.oPag.oAIDATNAS_1_43.enabled = this.oPgFrm.Page1.oPag.oAIDATNAS_1_43.mCond()
    this.oPgFrm.Page1.oPag.oAILOCNAS_1_45.enabled = this.oPgFrm.Page1.oPag.oAILOCNAS_1_45.mCond()
    this.oPgFrm.Page1.oPag.oAIPRONAS_1_46.enabled = this.oPgFrm.Page1.oPag.oAIPRONAS_1_46.mCond()
    this.oPgFrm.Page1.oPag.oAIRAGSOC_1_48.enabled = this.oPgFrm.Page1.oPag.oAIRAGSOC_1_48.mCond()
    this.oPgFrm.Page1.oPag.oAIDOMFIS_1_64.enabled = this.oPgFrm.Page1.oPag.oAIDOMFIS_1_64.mCond()
    this.oPgFrm.Page1.oPag.oAIPRODOM_1_65.enabled = this.oPgFrm.Page1.oPag.oAIPRODOM_1_65.mCond()
    this.oPgFrm.Page2.oPag.oAICODCAF_2_8.enabled = this.oPgFrm.Page2.oPag.oAICODCAF_2_8.mCond()
    this.oPgFrm.Page2.oPag.oAIIMPTRA_2_10.enabled = this.oPgFrm.Page2.oPag.oAIIMPTRA_2_10.mCond()
    this.oPgFrm.Page2.oPag.oAIDATIMP_2_11.enabled = this.oPgFrm.Page2.oPag.oAIDATIMP_2_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_18.enabled = this.oPgFrm.Page2.oPag.oBtn_2_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Delete end")
          .Calculate_KHSUCLYDCK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_ZJFMMVWDUN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ANNO
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANTIVAGS_IDX,3]
    i_lTable = "ANTIVAGS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2], .t., this.ANTIVAGS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AITIPINV,AI__ANNO,AISERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where AI__ANNO="+cp_ToStrODBC(this.w_ANNO);
                   +" and AITIPINV="+cp_ToStrODBC(0);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AITIPINV',0;
                       ,'AI__ANNO',this.w_ANNO)
            select AITIPINV,AI__ANNO,AISERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNO = NVL(_Link_.AI__ANNO,0)
      this.w_SERIALE = NVL(_Link_.AISERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ANNO = 0
      endif
      this.w_SERIALE = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANTIVAGS_IDX,2])+'\'+cp_ToStr(_Link_.AITIPINV,1)+'\'+cp_ToStr(_Link_.AI__ANNO,1)
      cp_ShowWarn(i_cKey,this.ANTIVAGS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPIVAZI,AZCOFAZI,AZPERAZI,AZCODNAZ,AZLOCAZI,AZPROAZI,AZNAGAZI,AZTELEFO,AZ_EMAIL,AZTELFAX,AZIVACOF";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZPIVAZI,AZCOFAZI,AZPERAZI,AZCODNAZ,AZLOCAZI,AZPROAZI,AZNAGAZI,AZTELEFO,AZ_EMAIL,AZTELFAX,AZIVACOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZCODFIS = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZCODNAZ = NVL(_Link_.AZCODNAZ,space(3))
      this.w_AZLOCAZI = NVL(_Link_.AZLOCAZI,space(30))
      this.w_AZPROAZI = NVL(_Link_.AZPROAZI,space(2))
      this.w_AZNAGAZI = NVL(_Link_.AZNAGAZI,space(5))
      this.w_AZTELEFO = NVL(_Link_.AZTELEFO,space(18))
      this.w_AZ_EMAIL = NVL(_Link_.AZ_EMAIL,space(254))
      this.w_AZTELFAX = NVL(_Link_.AZTELFAX,space(18))
      this.w_AZIVACOF = NVL(_Link_.AZIVACOF,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZPIVAZI = space(12)
      this.w_AZCODFIS = space(16)
      this.w_AZPERAZI = space(1)
      this.w_AZCODNAZ = space(3)
      this.w_AZLOCAZI = space(30)
      this.w_AZPROAZI = space(2)
      this.w_AZNAGAZI = space(5)
      this.w_AZTELEFO = space(18)
      this.w_AZ_EMAIL = space(254)
      this.w_AZTELFAX = space(18)
      this.w_AZIVACOF = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_SETIPRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_SETIPRIF;
                       ,'SECODAZI',this.w_CODAZI1)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.SECODAZI,space(5))
      this.w_SELOCALI = NVL(_Link_.SELOCALI,space(30))
      this.w_SEPROVIN = NVL(_Link_.SEPROVIN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_SELOCALI = space(30)
      this.w_SEPROVIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI2)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT1 = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT1 = NVL(_Link_.TTNOMTIT,space(25))
      this.w_DATNAS1 = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_LOCTIT1 = NVL(_Link_.TTLUONAS,space(30))
      this.w_PROTIT1 = NVL(_Link_.TTPRONAS,space(2))
      this.w_SESSO1 = NVL(_Link_.TT_SESSO,space(1))
      this.w_TELEFO1 = NVL(_Link_.TTTELEFO,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI2 = space(5)
      endif
      this.w_COGTIT1 = space(25)
      this.w_NOMTIT1 = space(25)
      this.w_DATNAS1 = ctod("  /  /  ")
      this.w_LOCTIT1 = space(30)
      this.w_PROTIT1 = space(2)
      this.w_SESSO1 = space(1)
      this.w_TELEFO1 = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAI__ANNO_1_1.value==this.w_AI__ANNO)
      this.oPgFrm.Page1.oPag.oAI__ANNO_1_1.value=this.w_AI__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oAITIPINV_1_5.RadioValue()==this.w_AITIPINV)
      this.oPgFrm.Page1.oPag.oAITIPINV_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPROTEC_1_6.value==this.w_AIPROTEC)
      this.oPgFrm.Page1.oPag.oAIPROTEC_1_6.value=this.w_AIPROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPRODOC_1_8.value==this.w_AIPRODOC)
      this.oPgFrm.Page1.oPag.oAIPRODOC_1_8.value=this.w_AIPRODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oAISOCINC_1_9.RadioValue()==this.w_AISOCINC)
      this.oPgFrm.Page1.oPag.oAISOCINC_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODFIS_1_33.value==this.w_AICODFIS)
      this.oPgFrm.Page1.oPag.oAICODFIS_1_33.value=this.w_AICODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPARIVA_1_34.value==this.w_AIPARIVA)
      this.oPgFrm.Page1.oPag.oAIPARIVA_1_34.value=this.w_AIPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oAICOGNOM_1_37.value==this.w_AICOGNOM)
      this.oPgFrm.Page1.oPag.oAICOGNOM_1_37.value=this.w_AICOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oAI__NOME_1_39.value==this.w_AI__NOME)
      this.oPgFrm.Page1.oPag.oAI__NOME_1_39.value=this.w_AI__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oAI_SESSO_1_41.RadioValue()==this.w_AI_SESSO)
      this.oPgFrm.Page1.oPag.oAI_SESSO_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDATNAS_1_43.value==this.w_AIDATNAS)
      this.oPgFrm.Page1.oPag.oAIDATNAS_1_43.value=this.w_AIDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oAILOCNAS_1_45.value==this.w_AILOCNAS)
      this.oPgFrm.Page1.oPag.oAILOCNAS_1_45.value=this.w_AILOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPRONAS_1_46.value==this.w_AIPRONAS)
      this.oPgFrm.Page1.oPag.oAIPRONAS_1_46.value=this.w_AIPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIRAGSOC_1_48.value==this.w_AIRAGSOC)
      this.oPgFrm.Page1.oPag.oAIRAGSOC_1_48.value=this.w_AIRAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDOMFIS_1_64.value==this.w_AIDOMFIS)
      this.oPgFrm.Page1.oPag.oAIDOMFIS_1_64.value=this.w_AIDOMFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPRODOM_1_65.value==this.w_AIPRODOM)
      this.oPgFrm.Page1.oPag.oAIPRODOM_1_65.value=this.w_AIPRODOM
    endif
    if not(this.oPgFrm.Page2.oPag.oAINUMMOD_2_2.value==this.w_AINUMMOD)
      this.oPgFrm.Page2.oPag.oAINUMMOD_2_2.value=this.w_AINUMMOD
    endif
    if not(this.oPgFrm.Page2.oPag.oMAXNORIG_2_3.value==this.w_MAXNORIG)
      this.oPgFrm.Page2.oPag.oMAXNORIG_2_3.value=this.w_MAXNORIG
    endif
    if not(this.oPgFrm.Page2.oPag.oAITIPFOR_2_5.RadioValue()==this.w_AITIPFOR)
      this.oPgFrm.Page2.oPag.oAITIPFOR_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAICODINT_2_6.value==this.w_AICODINT)
      this.oPgFrm.Page2.oPag.oAICODINT_2_6.value=this.w_AICODINT
    endif
    if not(this.oPgFrm.Page2.oPag.oAICODCAF_2_8.value==this.w_AICODCAF)
      this.oPgFrm.Page2.oPag.oAICODCAF_2_8.value=this.w_AICODCAF
    endif
    if not(this.oPgFrm.Page2.oPag.oAIIMPTRA_2_10.RadioValue()==this.w_AIIMPTRA)
      this.oPgFrm.Page2.oPag.oAIIMPTRA_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAIDATIMP_2_11.value==this.w_AIDATIMP)
      this.oPgFrm.Page2.oPag.oAIDATIMP_2_11.value=this.w_AIDATIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oAINOMFIL_2_13.value==this.w_AINOMFIL)
      this.oPgFrm.Page2.oPag.oAINOMFIL_2_13.value=this.w_AINOMFIL
    endif
    if not(this.oPgFrm.Page2.oPag.oESTDATVER_2_16.RadioValue()==this.w_ESTDATVER)
      this.oPgFrm.Page2.oPag.oESTDATVER_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oESCNUMFAT_2_17.RadioValue()==this.w_ESCNUMFAT)
      this.oPgFrm.Page2.oPag.oESCNUMFAT_2_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVAGS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AI__ANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAI__ANNO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_AI__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Not Empty(.w_Seriale) and .w_Aitipinv<>0 )  and (.cFunction='Load' And not Empty(.w_Seriale))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAITIPINV_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Nell'anno � gi� presente un invio ordinario")
          case   ((empty(.w_AIPROTEC)) or not(VAL(.w_AIPROTEC)<>0))  and (.w_AITIPINV<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIPROTEC_1_6.SetFocus()
            i_bnoObbl = !empty(.w_AIPROTEC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIPRODOC))  and (.w_AITIPINV<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIPRODOC_1_8.SetFocus()
            i_bnoObbl = !empty(.w_AIPRODOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AICODFIS)) or not(chkcfp(alltrim(.w_AICODFIS),'CF')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICODFIS_1_33.SetFocus()
            i_bnoObbl = !empty(.w_AICODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_AIPARIVA,"PI", "", "", ""))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIPARIVA_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AICOGNOM))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICOGNOM_1_37.SetFocus()
            i_bnoObbl = !empty(.w_AICOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AI__NOME))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAI__NOME_1_39.SetFocus()
            i_bnoObbl = !empty(.w_AI__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIDATNAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIDATNAS_1_43.SetFocus()
            i_bnoObbl = !empty(.w_AIDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AILOCNAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAILOCNAS_1_45.SetFocus()
            i_bnoObbl = !empty(.w_AILOCNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIPRONAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIPRONAS_1_46.SetFocus()
            i_bnoObbl = !empty(.w_AIPRONAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIRAGSOC))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIRAGSOC_1_48.SetFocus()
            i_bnoObbl = !empty(.w_AIRAGSOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIDOMFIS))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIDOMFIS_1_64.SetFocus()
            i_bnoObbl = !empty(.w_AIDOMFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIPRODOM))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIPRODOM_1_65.SetFocus()
            i_bnoObbl = !empty(.w_AIPRODOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MAXNORIG)) or not(.w_MAXNORIG<=15000))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMAXNORIG_2_3.SetFocus()
            i_bnoObbl = !empty(.w_MAXNORIG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AITIPFOR='01' OR .w_AITIPFOR='10')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAITIPFOR_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione non consentita con tipo fornitore persona fisica")
          case   not((empty(.w_AICODINT) or chkcfp(alltrim(.w_AICODINT),'CF')) AND (NOT EMPTY(.w_AICODINT) OR .w_AITIPFOR='01'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAICODINT_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fiscale dell'intermediario obbligatorio")
          case   not(EMPTY(.w_AICODINT) OR .w_AIIMPTRA<>'0')  and (NOT EMPTY(.w_AICODINT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAIIMPTRA_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIDATIMP))  and (NOT EMPTY(.w_AICODINT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAIDATIMP_2_11.SetFocus()
            i_bnoObbl = !empty(.w_AIDATIMP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AI__ANNO = this.w_AI__ANNO
    this.o_SERIALE = this.w_SERIALE
    this.o_AITIPINV = this.w_AITIPINV
    this.o_AITIPFOR = this.w_AITIPFOR
    this.o_AICODINT = this.w_AICODINT
    this.o_AINOMFIL = this.w_AINOMFIL
    return

enddefine

* --- Define pages as container
define class tgsai_agsPag1 as StdContainer
  Width  = 903
  height = 430
  stdWidth  = 903
  stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAI__ANNO_1_1 as StdField with uid="BFKAZISJJU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AI__ANNO", cQueryName = "AI__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 1362347,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=128, Top=48, cSayPict='"9999"', cGetPict='"9999"'


  add object oAITIPINV_1_5 as StdCombo with uid="XTZIJJTBQC",value=1,rtseq=5,rtrep=.f.,left=341,top=50,width=139,height=21;
    , ToolTipText = "Tipologia di invio";
    , HelpContextID = 71006628;
    , cFormVar="w_AITIPINV",RowSource=""+"Invio ordinario,"+"Invio sostitutivo,"+"Annullamento", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Nell'anno � gi� presente un invio ordinario";
  , bGlobalFont=.t.


  func oAITIPINV_1_5.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oAITIPINV_1_5.GetRadio()
    this.Parent.oContained.w_AITIPINV = this.RadioValue()
    return .t.
  endfunc

  func oAITIPINV_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AITIPINV==0,1,;
      iif(this.Parent.oContained.w_AITIPINV==1,2,;
      iif(this.Parent.oContained.w_AITIPINV==2,3,;
      0)))
  endfunc

  func oAITIPINV_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' And not Empty(.w_Seriale))
    endwith
   endif
  endfunc

  func oAITIPINV_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Not Empty(.w_Seriale) and .w_Aitipinv<>0 )
    endwith
    return bRes
  endfunc

  add object oAIPROTEC_1_6 as StdField with uid="OCCWFOKGWE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AIPROTEC", cQueryName = "AIPROTEC",;
    bObbl = .t. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico",;
    HelpContextID = 113067593,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=341, Top=81, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oAIPROTEC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AITIPINV<>0)
    endwith
   endif
  endfunc

  func oAIPROTEC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_AIPROTEC)<>0)
    endwith
    return bRes
  endfunc

  add object oAIPRODOC_1_8 as StdField with uid="TSSGDCKFDL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AIPRODOC", cQueryName = "AIPRODOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo documento",;
    HelpContextID = 155367863,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=680, Top=81, cSayPict='"999999"', cGetPict='"999999"'

  func oAIPRODOC_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AITIPINV<>0)
    endwith
   endif
  endfunc

  add object oAISOCINC_1_9 as StdCheck with uid="DYYEBCVTPL",rtseq=9,rtrep=.f.,left=341, top=109, caption="Comunicazione dati di societ� incorporata",;
    ToolTipText = "Comunicazione dati di societ� incorporata",;
    HelpContextID = 84249015,;
    cFormVar="w_AISOCINC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAISOCINC_1_9.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAISOCINC_1_9.GetRadio()
    this.Parent.oContained.w_AISOCINC = this.RadioValue()
    return .t.
  endfunc

  func oAISOCINC_1_9.SetRadio()
    this.Parent.oContained.w_AISOCINC=trim(this.Parent.oContained.w_AISOCINC)
    this.value = ;
      iif(this.Parent.oContained.w_AISOCINC=='1',1,;
      0)
  endfunc

  add object oAICODFIS_1_33 as StdField with uid="DFQWHMDFGZ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_AICODFIS", cQueryName = "AICODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del contribuente",;
    HelpContextID = 134837849,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=128, Top=174, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oAICODFIS_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chkcfp(alltrim(.w_AICODFIS),'CF'))
    endwith
    return bRes
  endfunc

  add object oAIPARIVA_1_34 as StdField with uid="JPNFHTXPJM",rtseq=33,rtrep=.f.,;
    cFormVar = "w_AIPARIVA", cQueryName = "AIPARIVA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA del contribuente",;
    HelpContextID = 198985287,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=397, Top=174, cSayPict='REPLICATE("!",11)', cGetPict='REPLICATE("!",11)', InputMask=replicate('X',11)

  func oAIPARIVA_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AIPARIVA,"PI", "", "", ""))
    endwith
    return bRes
  endfunc

  add object oAICOGNOM_1_37 as StdField with uid="USTYABUOUZ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_AICOGNOM", cQueryName = "AICOGNOM",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 264669613,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=128, Top=240, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oAICOGNOM_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAI__NOME_1_39 as StdField with uid="LKFBHLXRZV",rtseq=35,rtrep=.f.,;
    cFormVar = "w_AI__NOME", cQueryName = "AI__NOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 239389109,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=435, Top=240, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oAI__NOME_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAI_SESSO_1_41 as StdRadio with uid="FQZPXYVUYR",rtseq=36,rtrep=.f.,left=744, top=240, width=153,height=23;
    , ToolTipText = "Sesso";
    , cFormVar="w_AI_SESSO", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oAI_SESSO_1_41.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Maschio"
      this.Buttons(1).HelpContextID = 85931605
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Maschio","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Femmina"
      this.Buttons(2).HelpContextID = 85931605
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Femmina","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Sesso")
      StdRadio::init()
    endproc

  func oAI_SESSO_1_41.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAI_SESSO_1_41.GetRadio()
    this.Parent.oContained.w_AI_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oAI_SESSO_1_41.SetRadio()
    this.Parent.oContained.w_AI_SESSO=trim(this.Parent.oContained.w_AI_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_AI_SESSO=='M',1,;
      iif(this.Parent.oContained.w_AI_SESSO=='F',2,;
      0))
  endfunc

  func oAI_SESSO_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAIDATNAS_1_43 as StdField with uid="ECDVVXPNAO",rtseq=37,rtrep=.f.,;
    cFormVar = "w_AIDATNAS", cQueryName = "AIDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 16483929,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=128, Top=281

  func oAIDATNAS_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAILOCNAS_1_45 as StdField with uid="UCAJUOVOUI",rtseq=38,rtrep=.f.,;
    cFormVar = "w_AILOCNAS", cQueryName = "AILOCNAS",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 391591,;
   bGlobalFont=.t.,;
    Height=21, Width=562, Left=233, Top=281, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oAILOCNAS_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oAILOCNAS_1_45.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_AILOCNAS",".w_AIPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIPRONAS_1_46 as StdField with uid="HHSQKKMXTC",rtseq=39,rtrep=.f.,;
    cFormVar = "w_AIPRONAS", cQueryName = "AIPRONAS",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 12404313,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=823, Top=281, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oAIPRONAS_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oAIPRONAS_1_46.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_AIPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIRAGSOC_1_48 as StdField with uid="QITJWPXNAA",rtseq=40,rtrep=.f.,;
    cFormVar = "w_AIRAGSOC", cQueryName = "AIRAGSOC",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione o ragione sociale",;
    HelpContextID = 181639607,;
   bGlobalFont=.t.,;
    Height=21, Width=683, Left=128, Top=355, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oAIRAGSOC_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  add object oAIDOMFIS_1_64 as StdField with uid="HFQHCBDUHY",rtseq=41,rtrep=.f.,;
    cFormVar = "w_AIDOMFIS", cQueryName = "AIDOMFIS",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune del domicilio fiscale",;
    HelpContextID = 144279129,;
   bGlobalFont=.t.,;
    Height=21, Width=562, Left=128, Top=399, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oAIDOMFIS_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  proc oAIDOMFIS_1_64.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_AIDOMFIS",".w_AIPRODOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIPRODOM_1_65 as StdField with uid="GBMTNJARAJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_AIPRODOM", cQueryName = "AIPRODOM",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia del domicilio fiscale",;
    HelpContextID = 155367853,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=786, Top=399, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oAIPRODOM_1_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  proc oAIPRODOM_1_65.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_AIPRODOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oStr_1_32 as StdString with uid="TSXMGJAOXV",Visible=.t., Left=128, Top=34,;
    Alignment=2, Width=41, Height=18,;
    Caption="Anno"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="IXLIJYIGMO",Visible=.t., Left=309, Top=176,;
    Alignment=1, Width=84, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="HEUTCFKDNL",Visible=.t., Left=233, Top=262,;
    Alignment=0, Width=193, Height=18,;
    Caption="Comune (o stato estero) di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="OSGCYPTITY",Visible=.t., Left=128, Top=224,;
    Alignment=1, Width=58, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="LGSUOYWZDY",Visible=.t., Left=435, Top=224,;
    Alignment=0, Width=37, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="JOPERGGNNX",Visible=.t., Left=796, Top=221,;
    Alignment=0, Width=39, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="CNJREOUFHV",Visible=.t., Left=128, Top=262,;
    Alignment=2, Width=85, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="GTAPKTSGGM",Visible=.t., Left=785, Top=262,;
    Alignment=2, Width=109, Height=18,;
    Caption="Provincia di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="BOGQGLFCGC",Visible=.t., Left=128, Top=339,;
    Alignment=0, Width=188, Height=18,;
    Caption="Denominazione o ragione sociale"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=7, Top=145,;
    Alignment=0, Width=133, Height=19,;
    Caption="Dati del contribuente"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_53 as StdString with uid="FMQSXZQXJQ",Visible=.t., Left=7, Top=13,;
    Alignment=0, Width=141, Height=19,;
    Caption="Estremi della fornitura"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_54 as StdString with uid="BQYEOWHSUJ",Visible=.t., Left=7, Top=215,;
    Alignment=0, Width=88, Height=18,;
    Caption="Persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="KWZECAJTFT",Visible=.t., Left=7, Top=318,;
    Alignment=0, Width=202, Height=18,;
    Caption="Soggetti diversi dalle persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="MSUZFRVMFD",Visible=.t., Left=44, Top=176,;
    Alignment=0, Width=81, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="ILYQQCORZL",Visible=.t., Left=214, Top=50,;
    Alignment=1, Width=123, Height=18,;
    Caption="Tipologia di invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="ZBWKKGSIJD",Visible=.t., Left=189, Top=84,;
    Alignment=1, Width=148, Height=18,;
    Caption="Protocollo telematico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="ZBUGPNAZTE",Visible=.t., Left=505, Top=84,;
    Alignment=1, Width=165, Height=18,;
    Caption="Protocollo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="VAEZIQCAGB",Visible=.t., Left=128, Top=382,;
    Alignment=0, Width=193, Height=18,;
    Caption="Comune del domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="XMEDCUBLTQ",Visible=.t., Left=707, Top=382,;
    Alignment=2, Width=184, Height=18,;
    Caption="Provincia del domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oBox_1_50 as StdBox with uid="RDMWYOEEFE",left=1, top=139, width=900,height=289

  add object oBox_1_52 as StdBox with uid="GEMAGDKVQQ",left=1, top=7, width=900,height=134

  add object oBox_1_56 as StdBox with uid="WMHLBIJLHD",left=118, top=210, width=782,height=2

  add object oBox_1_57 as StdBox with uid="RQGVVXVVOU",left=118, top=313, width=782,height=2
enddefine
define class tgsai_agsPag2 as StdContainer
  Width  = 903
  height = 430
  stdWidth  = 903
  stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAINUMMOD_2_2 as StdField with uid="GWWKVWAVVN",rtseq=44,rtrep=.f.,;
    cFormVar = "w_AINUMMOD", cQueryName = "AINUMMOD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale degli invii",;
    HelpContextID = 6281654,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=354, Top=35, cSayPict='"9999"', cGetPict='"9999"'

  add object oMAXNORIG_2_3 as StdField with uid="YWPJFYVBEH",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MAXNORIG", cQueryName = "MAXNORIG",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo documenti per file",;
    HelpContextID = 189153523,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=354, Top=62, cSayPict='"99999"', cGetPict='"99999"'

  func oMAXNORIG_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MAXNORIG<=15000)
    endwith
    return bRes
  endfunc


  add object oAITIPFOR_2_5 as StdCombo with uid="TMIIFPZQPN",rtseq=46,rtrep=.f.,left=354,top=150,width=279,height=21;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 121338280;
    , cFormVar="w_AITIPFOR",RowSource=""+"01: Soggetti che inviano le dichiarazioni,"+"10: C.A.F. dip. e pens C.A.F. imp art3 C 2 altri int.", bObbl = .f. , nPag = 2;
    , sErrorMsg = "Selezione non consentita con tipo fornitore persona fisica";
  , bGlobalFont=.t.


  func oAITIPFOR_2_5.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'10',;
    space(2))))
  endfunc
  func oAITIPFOR_2_5.GetRadio()
    this.Parent.oContained.w_AITIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oAITIPFOR_2_5.SetRadio()
    this.Parent.oContained.w_AITIPFOR=trim(this.Parent.oContained.w_AITIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_AITIPFOR=='01',1,;
      iif(this.Parent.oContained.w_AITIPFOR=='10',2,;
      0))
  endfunc

  func oAITIPFOR_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AITIPFOR='01' OR .w_AITIPFOR='10')
    endwith
    return bRes
  endfunc

  add object oAICODINT_2_6 as StdField with uid="VHBKITNWRO",rtseq=47,rtrep=.f.,;
    cFormVar = "w_AICODINT", cQueryName = "AICODINT",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fiscale dell'intermediario obbligatorio",;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 83265958,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=354, Top=186, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oAICODINT_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_AICODINT) or chkcfp(alltrim(.w_AICODINT),'CF')) AND (NOT EMPTY(.w_AICODINT) OR .w_AITIPFOR='01'))
    endwith
    return bRes
  endfunc

  add object oAICODCAF_2_8 as StdField with uid="FCYYNVPVIH",rtseq=48,rtrep=.f.,;
    cFormVar = "w_AICODCAF", cQueryName = "AICODCAF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Numero di iscrizione all'albo del C.A.F.",;
    HelpContextID = 183929268,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=774, Top=186, cSayPict='"99999"', cGetPict='"99999"', InputMask=replicate('X',5)

  func oAICODCAF_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODINT))
    endwith
   endif
  endfunc


  add object oAIIMPTRA_2_10 as StdCombo with uid="JKQBQAAXZG",rtseq=49,rtrep=.f.,left=354,top=222,width=279,height=21;
    , ToolTipText = "Impegno a trasmettere in via telematica la comunicazione";
    , HelpContextID = 113759815;
    , cFormVar="w_AIIMPTRA",RowSource=""+"1 - Dichiarazione predisposta dal contribuente,"+"2 - Dichiarazione predisposta da chi effettua l'invio", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAIIMPTRA_2_10.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(1))))
  endfunc
  func oAIIMPTRA_2_10.GetRadio()
    this.Parent.oContained.w_AIIMPTRA = this.RadioValue()
    return .t.
  endfunc

  func oAIIMPTRA_2_10.SetRadio()
    this.Parent.oContained.w_AIIMPTRA=trim(this.Parent.oContained.w_AIIMPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_AIIMPTRA=='1',1,;
      iif(this.Parent.oContained.w_AIIMPTRA=='2',2,;
      0))
  endfunc

  func oAIIMPTRA_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODINT))
    endwith
   endif
  endfunc

  func oAIIMPTRA_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AICODINT) OR .w_AIIMPTRA<>'0')
    endwith
    return bRes
  endfunc

  add object oAIDATIMP_2_11 as StdField with uid="SYRFGCWSSA",rtseq=50,rtrep=.f.,;
    cFormVar = "w_AIDATIMP", cQueryName = "AIDATIMP",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'impegno",;
    HelpContextID = 67402154,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=354, Top=263

  func oAIDATIMP_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODINT))
    endwith
   endif
  endfunc

  add object oAINOMFIL_2_13 as StdField with uid="VPKTICWKHO",rtseq=51,rtrep=.f.,;
    cFormVar = "w_AINOMFIL", cQueryName = "AINOMFIL",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file e percorso per inoltro telematico",;
    HelpContextID = 144320082,;
   bGlobalFont=.t.,;
    Height=21, Width=706, Left=128, Top=340, InputMask=replicate('X',254)


  add object oBtn_2_15 as StdButton with uid="TQEMIMWBDV",left=843, top=340, width=21,height=20,;
    caption="...", nPag=2;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file";
    , HelpContextID = 42399274;
  , bGlobalFont=.t.

    proc oBtn_2_15.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oESTDATVER_2_16 as StdCheck with uid="NHMVZZZTAC",rtseq=52,rtrep=.f.,left=128, top=365, caption="Estrarre anche i soggetti da verificare",;
    ToolTipText = "Estrarre anche i soggetti da verificare",;
    HelpContextID = 97490347,;
    cFormVar="w_ESTDATVER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oESTDATVER_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESTDATVER_2_16.GetRadio()
    this.Parent.oContained.w_ESTDATVER = this.RadioValue()
    return .t.
  endfunc

  func oESTDATVER_2_16.SetRadio()
    this.Parent.oContained.w_ESTDATVER=trim(this.Parent.oContained.w_ESTDATVER)
    this.value = ;
      iif(this.Parent.oContained.w_ESTDATVER=='S',1,;
      0)
  endfunc

  add object oESCNUMFAT_2_17 as StdCheck with uid="NHVNFGPJZM",rtseq=53,rtrep=.f.,left=128, top=391, caption="Non estrarre il numero sulle fatture ricevute",;
    ToolTipText = "Non estrarre il numero sulle fatture ricevute",;
    HelpContextID = 1607111,;
    cFormVar="w_ESCNUMFAT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oESCNUMFAT_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESCNUMFAT_2_17.GetRadio()
    this.Parent.oContained.w_ESCNUMFAT = this.RadioValue()
    return .t.
  endfunc

  func oESCNUMFAT_2_17.SetRadio()
    this.Parent.oContained.w_ESCNUMFAT=trim(this.Parent.oContained.w_ESCNUMFAT)
    this.value = ;
      iif(this.Parent.oContained.w_ESCNUMFAT=='S',1,;
      0)
  endfunc


  add object oBtn_2_18 as StdButton with uid="AYLATODQCL",left=816, top=378, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=2;
    , ToolTipText = "Genera file";
    , HelpContextID = 48345622;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_18.Click()
      with this.Parent.oContained
        do GSAI_BGS in blackbox with this.Parent.oContained,"GENERA"
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AI__ANNO) and not empty(.w_AINOMFIL) and not g_DEMO)
      endwith
    endif
  endfunc

  add object oStr_2_4 as StdString with uid="FUOMFIFISI",Visible=.t., Left=193, Top=36,;
    Alignment=1, Width=160, Height=18,;
    Caption="Totale degli invii presentati:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="NECDVAUAQY",Visible=.t., Left=133, Top=190,;
    Alignment=1, Width=220, Height=18,;
    Caption="Codice fiscale dell'intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="ABCTFAMFTH",Visible=.t., Left=559, Top=190,;
    Alignment=1, Width=214, Height=18,;
    Caption="N. di iscrizione all'albo del C.A.F.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="AOYTOWESTU",Visible=.t., Left=250, Top=267,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data dell'impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="SSBWQANJWI",Visible=.t., Left=46, Top=344,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="CMEGIEQADK",Visible=.t., Left=8, Top=12,;
    Alignment=0, Width=174, Height=19,;
    Caption="Comunicazione su pi� invii"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_24 as StdString with uid="ERDKNGNDJP",Visible=.t., Left=8, Top=117,;
    Alignment=0, Width=249, Height=19,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_25 as StdString with uid="RCGCOENWMJ",Visible=.t., Left=134, Top=223,;
    Alignment=1, Width=219, Height=18,;
    Caption="Impegno a trasmettere in via telematica:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=266, Top=150,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="ATUGIPJKAJ",Visible=.t., Left=117, Top=63,;
    Alignment=1, Width=236, Height=18,;
    Caption="Numero massimo documenti per file:"  ;
  , bGlobalFont=.t.

  add object oBox_2_22 as StdBox with uid="XTEZQYXSTV",left=3, top=8, width=897,height=105

  add object oBox_2_23 as StdBox with uid="DQNRVKFLMJ",left=3, top=111, width=897,height=210
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_ags','ANTIVAGS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AISERIAL=ANTIVAGS.AISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsai_ags
proc SfogliaDir (parent)
local PathName, oField
  PathName = cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
    parent.w_AINOMFIL=left(parent.w_DirName+'SP'+alltrim(i_CODAZI)+STR(parent.w_AI__ANNO,4)+parent.w_AISERIAL+'.ART21'+space(254),254)
  endif
endproc
* --- Fine Area Manuale
