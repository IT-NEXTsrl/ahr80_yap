* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bfc                                                        *
*              Stampa identificativi fiscali                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-11-14                                                      *
* Last revis.: 2013-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bfc",oParentObject)
return(i_retval)

define class tgsai_bfc as StdBatch
  * --- Local variables
  w_ERRP = .f.
  w_ERRC = .f.
  w_LEN = 0
  w_lencf = 0
  w_ESITOP = space(3)
  w_ESITOC = space(3)
  w_SPSERIAL = space(10)
  * --- WorkFile variables
  OUTPUTMP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa elenco identificativi fiscali da controllare
    this.w_LEN = 0
    this.w_lencf = 0
    * --- Create temporary table OUTPUTMP
    i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\AIFT\EXE\QUERY\GSAI5KMS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.OUTPUTMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from OUTPUTMP
    i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" OUTPUTMP ";
           ,"_Curs_OUTPUTMP")
    else
      select * from (i_cTable);
        into cursor _Curs_OUTPUTMP
    endif
    if used('_Curs_OUTPUTMP')
      select _Curs_OUTPUTMP
      locate for 1=1
      do while not(eof())
      this.w_ERRP = .F.
      this.w_ERRC = .F.
      this.w_ESITOP = "  "
      this.w_ESITOC = "  "
      this.w_SPSERIAL = _Curs_OUTPUTMP.SPSERIAL
      do case
        case SPTIPSOG="I"
          * --- SOGGETTO TIPO IVA
          * --- CONTROLLI PARTITA IVA
          this.w_LEN = LEN(ALLTRIM(_Curs_OUTPUTMP.SPPARIVA))
          this.w_lencf = LEN(ALLTRIM(_Curs_OUTPUTMP.SPCODFIS))
          if this.w_len=0
            this.w_ERRP = .T.
            this.w_ESITOP = "IP4"
          endif
          if not empty(nvl(_Curs_OUTPUTMP.SPPARIVA,""))
            if this.w_LEN<>11 AND NOT this.w_ERRP
              this.w_ERRP = .T.
              this.w_ESITOP = "IP1"
            endif
            if !this.w_ERRP AND NOT(RIGHT(REPL("0",11)+alltrim(STR(VAL(_Curs_OUTPUTMP.SPPARIVA),11)),11)=LEFT(_Curs_OUTPUTMP.SPPARIVA,11))
              this.w_ERRP = .T.
              this.w_ESITOP = "IP2"
            endif
            if !this.w_ERRP AND REPL("0",11)=LEFT(_Curs_OUTPUTMP.SPPARIVA,11)
              this.w_ERRP = .T.
              this.w_ESITOP = "IP3"
            endif
          endif
          * --- CONTROLLI CODICE FISCALE
          if not empty(nvl(_Curs_OUTPUTMP.SPCODFIS,"")) 
 
 
 
            if RIGHT(REPL("0",this.w_lencf)+alltrim(STR(VAL(_Curs_OUTPUTMP.SPCODFIS),this.w_lencf)),this.w_lencf)=LEFT(_Curs_OUTPUTMP.SPCODFIS,this.w_lencf) 
              if !this.w_ERRC and this.w_lencf <>12 and this.w_lencf <>11
                this.w_ERRC = .T.
                this.w_ESITOC = "IC1"
              endif
            else
              if this.w_lencf <> 16 AND NOT this.w_ERRC
                this.w_ERRC = .T.
                this.w_ESITOC = "IC2"
              endif
            endif
          endif
        case SPTIPSOG="P"
          * --- CONTROLLI PARTITA IVA
          this.w_LEN = LEN(ALLTRIM(_Curs_OUTPUTMP.SPPARIVA))
          this.w_lencf = LEN(ALLTRIM(_Curs_OUTPUTMP.SPCODFIS))
          if not empty(nvl(_Curs_OUTPUTMP.SPPARIVA,""))
            if this.w_LEN<>11
              this.w_ERRP = .T.
              this.w_ESITOP = "PP1"
            endif
            if !this.w_ERRP AND NOT(RIGHT(REPL("0",11)+alltrim(STR(VAL(_Curs_OUTPUTMP.SPPARIVA),11)),11)=LEFT(_Curs_OUTPUTMP.SPPARIVA,11))
              this.w_ERRP = .T.
              this.w_ESITOP = "PP2"
            endif
          endif
          * --- CONTROLLI CODICE FISCALE
          if this.w_lencf=0 
            this.w_ERRC = .T.
            this.w_ESITOC = "PC1"
          endif
          if NOT this.w_ERRC
            if RIGHT(REPL("0",this.w_lencf)+alltrim(STR(VAL(_Curs_OUTPUTMP.SPCODFIS),this.w_lencf)),this.w_lencf)=LEFT(_Curs_OUTPUTMP.SPCODFIS,this.w_lencf) 
              if !this.w_ERRC and this.w_lencf <>12 and this.w_lencf <>11
                this.w_ERRC = .T.
                this.w_ESITOC = "PC2"
              endif
            else
              if this.w_lencf <> 16 
                this.w_ERRC = .T.
                this.w_ESITOC = "PC3"
              endif
            endif
          endif
      endcase
      if this.w_ERRC OR this.w_ERRP
        * --- Write into OUTPUTMP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OUTPUTMP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ESITOP ="+cp_NullLink(cp_ToStrODBC(this.w_ESITOP),'OUTPUTMP','ESITOP');
          +",ESITOC ="+cp_NullLink(cp_ToStrODBC(this.w_ESITOC),'OUTPUTMP','ESITOC');
              +i_ccchkf ;
          +" where ";
              +"SPSERIAL = "+cp_ToStrODBC(this.w_SPSERIAL);
                 )
        else
          update (i_cTable) set;
              ESITOP = this.w_ESITOP;
              ,ESITOC = this.w_ESITOC;
              &i_ccchkf. ;
           where;
              SPSERIAL = this.w_SPSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_OUTPUTMP
        continue
      enddo
      use
    endif
    L_SP_ANNO=this.oParentObject.w_SP__ANNO
    L_SPTIPSOG=this.oParentObject.w_SPTIPSOG
    L_DATA1=this.oParentObject.w_DATA1
    L_DATA2=this.oParentObject.w_DATA2
    vq_exec(this.oParentObject.w_OQRY, this, "__TMP__")
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
    CP_CHPRN(this.oParentObject.w_OREP, , this)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*OUTPUTMP'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_OUTPUTMP')
      use in _Curs_OUTPUTMP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
