* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_adb                                                        *
*              Dati estratti black list                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2015-04-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_adb"))

* --- Class definition
define class tgsai_adb as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 810
  Height = 573+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-04-20"
  HelpContextID=92931945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=78

  * --- Constant Properties
  DATESTBL_IDX = 0
  CONTI_IDX = 0
  NAZIONI_IDX = 0
  DATPARBL_IDX = 0
  SOGCOLNR_IDX = 0
  cFile = "DATESTBL"
  cKeySelect = "DBSERIAL"
  cKeyWhere  = "DBSERIAL=this.w_DBSERIAL"
  cKeyWhereODBC = '"DBSERIAL="+cp_ToStrODBC(this.w_DBSERIAL)';

  cKeyWhereODBCqualified = '"DATESTBL.DBSERIAL="+cp_ToStrODBC(this.w_DBSERIAL)';

  cPrg = "gsai_adb"
  cComment = "Dati estratti black list"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DBSERIAL = space(10)
  w_DB__ANNO = 0
  o_DB__ANNO = 0
  w_ANNO = space(4)
  w_BLPERIOD = space(1)
  w_PERIOD = space(1)
  o_PERIOD = space(1)
  w_DB__MESE = 0
  w_DBTRIMES = 0
  w_DBTIPCON = space(1)
  o_DBTIPCON = space(1)
  w_DBCODCON = space(15)
  o_DBCODCON = space(15)
  w_SCCODCLF = space(15)
  o_SCCODCLF = space(15)
  w_CHCODCLF = space(15)
  w_SCCOGSCN = space(24)
  w_SCNOMSCN = space(20)
  w_SCDTNSCN = ctod('  /  /  ')
  w_SCPRNSCN = space(2)
  w_SCLCRSCN = space(40)
  w_SCSESDOM = space(3)
  w_ANDESCRI = space(40)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANCOGNOM = space(20)
  w_AN__NOME = space(20)
  w_ANINDIRI = space(35)
  w_ANCOFISC = space(20)
  w_ANCODFIS = space(16)
  w_ANPARIVA = space(12)
  w_ANLOCALI = space(30)
  w_ANNAZION = space(3)
  w_ANPERFIS = space(1)
  w_NADESNAZ = space(35)
  w_NACODEST = space(5)
  w_NACODISO = space(3)
  w_DBPERFIS = space(1)
  o_DBPERFIS = space(1)
  w_DBDESCRI = space(60)
  w_DBCOGNOM = space(40)
  w_DB__NOME = space(40)
  w_DBDATNAS = ctod('  /  /  ')
  w_DBCOMEST = space(40)
  w_DBPRONAS = space(2)
  w_DBSTADOM = space(3)
  w_DBSTAEST = space(3)
  w_DBCITEST = space(40)
  w_DBINDEST = space(40)
  w_DBPARIVA = space(25)
  w_DBCODFIS = space(25)
  w_DBCODGEN = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_TIPREGV = space(1)
  w_TIPREGA = space(1)
  w_TAIMC003 = 0
  w_TAIMS003 = 0
  w_TAIMC004 = 0
  w_TAIMS004 = 0
  w_TAIMC005 = 0
  w_TAIMS005 = 0
  w_TPIMC003 = 0
  w_TPIMS003 = 0
  w_TPIMC004 = 0
  w_TPIMC005 = 0
  w_TPIMS005 = 0
  w_DB003001 = space(1)
  w_DB003002 = space(1)
  w_DB004001 = space(1)
  w_DB004002 = space(1)
  w_DB005001 = space(1)
  w_DB005002 = space(1)
  w_DB006001 = space(1)
  w_DB006002 = space(1)
  w_DB007001 = space(1)
  w_DB008001 = space(1)
  w_DB008002 = space(1)
  w_DBSCLGEN = space(1)
  w_DBDACONT = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DBSERIAL = this.W_DBSERIAL

  * --- Children pointers
  GSAI_MDB = .NULL.
  GSAIAMDB = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DATESTBL','gsai_adb')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_adbPag1","gsai_adb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati anagrafici")
      .Pages(1).HelpContextID = 136108281
      .Pages(2).addobject("oPag","tgsai_adbPag2","gsai_adb",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Quadro BL")
      .Pages(2).HelpContextID = 16812040
      .Pages(3).addobject("oPag","tgsai_adbPag3","gsai_adb",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Operazioni del periodo")
      .Pages(3).HelpContextID = 211077387
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='NAZIONI'
    this.cWorkTables[3]='DATPARBL'
    this.cWorkTables[4]='SOGCOLNR'
    this.cWorkTables[5]='DATESTBL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATESTBL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATESTBL_IDX,3]
  return

  function CreateChildren()
    this.GSAI_MDB = CREATEOBJECT('stdDynamicChild',this,'GSAI_MDB',this.oPgFrm.Page3.oPag.oLinkPC_3_3)
    this.GSAI_MDB.createrealchild()
    this.GSAIAMDB = CREATEOBJECT('stdDynamicChild',this,'GSAIAMDB',this.oPgFrm.Page3.oPag.oLinkPC_3_8)
    this.GSAIAMDB.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAI_MDB)
      this.GSAI_MDB.DestroyChildrenChain()
      this.GSAI_MDB=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_3')
    if !ISNULL(this.GSAIAMDB)
      this.GSAIAMDB.DestroyChildrenChain()
      this.GSAIAMDB=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_8')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAI_MDB.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAIAMDB.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAI_MDB.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAIAMDB.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAI_MDB.NewDocument()
    this.GSAIAMDB.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAI_MDB.SetKey(;
            .w_DBSERIAL,"DBSERIAL";
            ,.w_TIPREGV,"DBTIPREG";
            )
      this.GSAIAMDB.SetKey(;
            .w_DBSERIAL,"DBSERIAL";
            ,.w_TIPREGA,"DBTIPREG";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAI_MDB.ChangeRow(this.cRowID+'      1',1;
             ,.w_DBSERIAL,"DBSERIAL";
             ,.w_TIPREGV,"DBTIPREG";
             )
      .GSAIAMDB.ChangeRow(this.cRowID+'      1',1;
             ,.w_DBSERIAL,"DBSERIAL";
             ,.w_TIPREGA,"DBTIPREG";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAI_MDB)
        i_f=.GSAI_MDB.BuildFilter()
        if !(i_f==.GSAI_MDB.cQueryFilter)
          i_fnidx=.GSAI_MDB.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MDB.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MDB.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MDB.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MDB.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAIAMDB)
        i_f=.GSAIAMDB.BuildFilter()
        if !(i_f==.GSAIAMDB.cQueryFilter)
          i_fnidx=.GSAIAMDB.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAIAMDB.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAIAMDB.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAIAMDB.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAIAMDB.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DBSERIAL = NVL(DBSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DATESTBL where DBSERIAL=KeySet.DBSERIAL
    *
    i_nConn = i_TableProp[this.DATESTBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTBL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATESTBL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATESTBL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATESTBL '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DBSERIAL',this.w_DBSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_BLPERIOD = space(1)
        .w_CHCODCLF = space(15)
        .w_SCCOGSCN = space(24)
        .w_SCNOMSCN = space(20)
        .w_SCDTNSCN = ctod("  /  /  ")
        .w_SCPRNSCN = space(2)
        .w_SCLCRSCN = space(40)
        .w_SCSESDOM = space(3)
        .w_ANDESCRI = space(40)
        .w_ANDATNAS = ctod("  /  /  ")
        .w_ANLOCNAS = space(30)
        .w_ANPRONAS = space(2)
        .w_ANCOGNOM = space(20)
        .w_AN__NOME = space(20)
        .w_ANINDIRI = space(35)
        .w_ANCOFISC = space(20)
        .w_ANCODFIS = space(16)
        .w_ANPARIVA = space(12)
        .w_ANLOCALI = space(30)
        .w_ANNAZION = space(3)
        .w_ANPERFIS = space(1)
        .w_NADESNAZ = space(35)
        .w_NACODEST = space(5)
        .w_NACODISO = space(3)
        .w_OBTEST = CTOD('01-01-1900')
        .w_TIPREGV = 'V'
        .w_TIPREGA = 'A'
        .w_TAIMC003 = 0
        .w_TAIMS003 = 0
        .w_TAIMC004 = 0
        .w_TAIMS004 = 0
        .w_TAIMC005 = 0
        .w_TAIMS005 = 0
        .w_TPIMC003 = 0
        .w_TPIMS003 = 0
        .w_TPIMC004 = 0
        .w_TPIMC005 = 0
        .w_TPIMS005 = 0
        .w_DBSERIAL = NVL(DBSERIAL,space(10))
        .op_DBSERIAL = .w_DBSERIAL
        .w_DB__ANNO = NVL(DB__ANNO,0)
        .w_ANNO = Str(.w_DB__ANNO,4,0)
          .link_1_3('Load')
        .w_PERIOD = iif(EMPTY(NVL(.w_BLPERIOD, ' ')), 'M', .w_BLPERIOD)
        .w_DB__MESE = NVL(DB__MESE,0)
        .w_DBTRIMES = NVL(DBTRIMES,0)
        .w_DBTIPCON = NVL(DBTIPCON,space(1))
        .w_DBCODCON = NVL(DBCODCON,space(15))
          if link_1_11_joined
            this.w_DBCODCON = NVL(ANCODICE111,NVL(this.w_DBCODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI111,space(40))
            this.w_ANINDIRI = NVL(ANINDIRI111,space(35))
            this.w_ANDATNAS = NVL(cp_ToDate(ANDATNAS111),ctod("  /  /  "))
            this.w_ANLOCNAS = NVL(ANLOCNAS111,space(30))
            this.w_ANPRONAS = NVL(ANPRONAS111,space(2))
            this.w_ANCOGNOM = NVL(ANCOGNOM111,space(20))
            this.w_AN__NOME = NVL(AN__NOME111,space(20))
            this.w_ANPARIVA = NVL(ANPARIVA111,space(12))
            this.w_ANCOFISC = NVL(ANCOFISC111,space(20))
            this.w_ANLOCALI = NVL(ANLOCALI111,space(30))
            this.w_ANNAZION = NVL(ANNAZION111,space(3))
            this.w_ANCODFIS = NVL(ANCODFIS111,space(16))
            this.w_ANPERFIS = NVL(ANPERFIS111,space(1))
          else
          .link_1_11('Load')
          endif
        .w_SCCODCLF = .w_DBCODCON
          .link_1_12('Load')
          .link_1_13('Load')
          .link_1_31('Load')
        .w_DBPERFIS = NVL(DBPERFIS,space(1))
        .w_DBDESCRI = NVL(DBDESCRI,space(60))
        .w_DBCOGNOM = NVL(DBCOGNOM,space(40))
        .w_DB__NOME = NVL(DB__NOME,space(40))
        .w_DBDATNAS = NVL(cp_ToDate(DBDATNAS),ctod("  /  /  "))
        .w_DBCOMEST = NVL(DBCOMEST,space(40))
        .w_DBPRONAS = NVL(DBPRONAS,space(2))
        .w_DBSTADOM = NVL(DBSTADOM,space(3))
        .w_DBSTAEST = NVL(DBSTAEST,space(3))
        .w_DBCITEST = NVL(DBCITEST,space(40))
        .w_DBINDEST = NVL(DBINDEST,space(40))
        .w_DBPARIVA = NVL(DBPARIVA,space(25))
        .w_DBCODFIS = NVL(DBCODFIS,space(25))
        .w_DBCODGEN = NVL(DBCODGEN,space(10))
        .w_DB003001 = NVL(DB003001,space(1))
        .w_DB003002 = NVL(DB003002,space(1))
        .w_DB004001 = NVL(DB004001,space(1))
        .w_DB004002 = NVL(DB004002,space(1))
        .w_DB005001 = NVL(DB005001,space(1))
        .w_DB005002 = NVL(DB005002,space(1))
        .w_DB006001 = NVL(DB006001,space(1))
        .w_DB006002 = NVL(DB006002,space(1))
        .w_DB007001 = NVL(DB007001,space(1))
        .w_DB008001 = NVL(DB008001,space(1))
        .w_DB008002 = NVL(DB008002,space(1))
        .w_DBSCLGEN = NVL(DBSCLGEN,space(1))
        .w_DBDACONT = NVL(DBDACONT,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DATESTBL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DBSERIAL = space(10)
      .w_DB__ANNO = 0
      .w_ANNO = space(4)
      .w_BLPERIOD = space(1)
      .w_PERIOD = space(1)
      .w_DB__MESE = 0
      .w_DBTRIMES = 0
      .w_DBTIPCON = space(1)
      .w_DBCODCON = space(15)
      .w_SCCODCLF = space(15)
      .w_CHCODCLF = space(15)
      .w_SCCOGSCN = space(24)
      .w_SCNOMSCN = space(20)
      .w_SCDTNSCN = ctod("  /  /  ")
      .w_SCPRNSCN = space(2)
      .w_SCLCRSCN = space(40)
      .w_SCSESDOM = space(3)
      .w_ANDESCRI = space(40)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANCOGNOM = space(20)
      .w_AN__NOME = space(20)
      .w_ANINDIRI = space(35)
      .w_ANCOFISC = space(20)
      .w_ANCODFIS = space(16)
      .w_ANPARIVA = space(12)
      .w_ANLOCALI = space(30)
      .w_ANNAZION = space(3)
      .w_ANPERFIS = space(1)
      .w_NADESNAZ = space(35)
      .w_NACODEST = space(5)
      .w_NACODISO = space(3)
      .w_DBPERFIS = space(1)
      .w_DBDESCRI = space(60)
      .w_DBCOGNOM = space(40)
      .w_DB__NOME = space(40)
      .w_DBDATNAS = ctod("  /  /  ")
      .w_DBCOMEST = space(40)
      .w_DBPRONAS = space(2)
      .w_DBSTADOM = space(3)
      .w_DBSTAEST = space(3)
      .w_DBCITEST = space(40)
      .w_DBINDEST = space(40)
      .w_DBPARIVA = space(25)
      .w_DBCODFIS = space(25)
      .w_DBCODGEN = space(10)
      .w_OBTEST = ctod("  /  /  ")
      .w_TIPREGV = space(1)
      .w_TIPREGA = space(1)
      .w_TAIMC003 = 0
      .w_TAIMS003 = 0
      .w_TAIMC004 = 0
      .w_TAIMS004 = 0
      .w_TAIMC005 = 0
      .w_TAIMS005 = 0
      .w_TPIMC003 = 0
      .w_TPIMS003 = 0
      .w_TPIMC004 = 0
      .w_TPIMC005 = 0
      .w_TPIMS005 = 0
      .w_DB003001 = space(1)
      .w_DB003002 = space(1)
      .w_DB004001 = space(1)
      .w_DB004002 = space(1)
      .w_DB005001 = space(1)
      .w_DB005002 = space(1)
      .w_DB006001 = space(1)
      .w_DB006002 = space(1)
      .w_DB007001 = space(1)
      .w_DB008001 = space(1)
      .w_DB008002 = space(1)
      .w_DBSCLGEN = space(1)
      .w_DBDACONT = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DB__ANNO = year(i_DATSYS)
        .w_ANNO = Str(.w_DB__ANNO,4,0)
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_ANNO))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_PERIOD = iif(EMPTY(NVL(.w_BLPERIOD, ' ')), 'M', .w_BLPERIOD)
        .w_DB__MESE = iif(NVL(.w_PERIOD,'M')='M', Month(i_Datsys), 0)
        .w_DBTRIMES = iif(NVL(.w_PERIOD,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        .w_DBTIPCON = 'C'
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_DBCODCON))
          .link_1_11('Full')
          endif
        .w_SCCODCLF = .w_DBCODCON
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_SCCODCLF))
          .link_1_12('Full')
          endif
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_CHCODCLF))
          .link_1_13('Full')
          endif
        .DoRTCalc(12,29,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_31('Full')
          endif
          .DoRTCalc(30,33,.f.)
        .w_DBPERFIS = IIF(.w_ANPERFIS='S','P',IIF(Not Empty(NVL(.w_SCCODCLF,'')),'S','N'))
        .w_DBDESCRI = .w_ANDESCRI
        .w_DBCOGNOM = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCCOGSCN,LEFT(.w_ANCOGNOM,40)),SPACE(40))
        .w_DB__NOME = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCNOMSCN,LEFT(.w_AN__NOME,40)),SPACE(40))
        .w_DBDATNAS = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCDTNSCN,.w_ANDATNAS),CTOD('  -  -   '))
        .w_DBCOMEST = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCLCRSCN,.w_ANLOCNAS),SPACE(40))
        .w_DBPRONAS = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCPRNSCN,.w_ANPRONAS),SPACE(2))
        .w_DBSTADOM = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCSESDOM,LEFT(.w_NACODEST,3)),SPACE(3))
        .w_DBSTAEST = IIF(.w_DBPERFIS$'NS',LEFT(.w_NACODEST,3),SPACE(3))
        .w_DBCITEST = IIF(.w_DBPERFIS$'NS',.w_ANLOCALI,SPACE(40))
        .w_DBINDEST = IIF(.w_DBPERFIS$'NS',.w_ANINDIRI,SPACE(40))
        .w_DBPARIVA = iif(Not Empty(Nvl(.w_Anpariva,'')),Alltrim(.w_Nacodiso)+Alltrim(.w_Anpariva),Space(25))
        .w_DBCODFIS = iif(Empty(.w_Ancofisc), .w_Ancodfis, .w_Ancofisc)
        .w_DBCODGEN = space(10)
        .w_OBTEST = CTOD('01-01-1900')
        .w_TIPREGV = 'V'
        .w_TIPREGA = 'A'
          .DoRTCalc(51,61,.f.)
        .w_DB003001 = 'N'
        .w_DB003002 = 'N'
        .w_DB004001 = 'N'
        .w_DB004002 = 'N'
        .w_DB005001 = 'N'
        .w_DB005002 = 'N'
        .w_DB006001 = 'N'
        .w_DB006002 = 'N'
        .w_DB007001 = 'N'
        .w_DB008001 = 'N'
        .w_DB008002 = 'N'
        .w_DBSCLGEN = 'N'
        .w_DBDACONT = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATESTBL')
    this.DoRTCalc(75,78,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTBL_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"DEBLK","i_codazi,w_DBSERIAL")
      .op_codazi = .w_codazi
      .op_DBSERIAL = .w_DBSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDB__ANNO_1_2.enabled = i_bVal
      .Page1.oPag.oPERIOD_1_6.enabled_(i_bVal)
      .Page1.oPag.oDB__MESE_1_7.enabled = i_bVal
      .Page1.oPag.oDBTRIMES_1_8.enabled = i_bVal
      .Page1.oPag.oDBTIPCON_1_10.enabled = i_bVal
      .Page1.oPag.oDBCODCON_1_11.enabled = i_bVal
      .Page1.oPag.oDBPERFIS_1_36.enabled = i_bVal
      .Page1.oPag.oDBDESCRI_1_37.enabled = i_bVal
      .Page1.oPag.oDBCOGNOM_1_38.enabled = i_bVal
      .Page1.oPag.oDB__NOME_1_39.enabled = i_bVal
      .Page1.oPag.oDBDATNAS_1_40.enabled = i_bVal
      .Page1.oPag.oDBCOMEST_1_41.enabled = i_bVal
      .Page1.oPag.oDBPRONAS_1_42.enabled = i_bVal
      .Page1.oPag.oDBSTADOM_1_43.enabled = i_bVal
      .Page1.oPag.oDBSTAEST_1_44.enabled = i_bVal
      .Page1.oPag.oDBCITEST_1_45.enabled = i_bVal
      .Page1.oPag.oDBINDEST_1_46.enabled = i_bVal
      .Page1.oPag.oDBPARIVA_1_47.enabled = i_bVal
      .Page1.oPag.oDBCODFIS_1_48.enabled = i_bVal
      .Page2.oPag.oDB003001_2_47.enabled = i_bVal
      .Page2.oPag.oDB003002_2_48.enabled = i_bVal
      .Page2.oPag.oDB004001_2_49.enabled = i_bVal
      .Page2.oPag.oDB004002_2_50.enabled = i_bVal
      .Page2.oPag.oDB005001_2_51.enabled = i_bVal
      .Page2.oPag.oDB005002_2_52.enabled = i_bVal
      .Page2.oPag.oDB006001_2_53.enabled = i_bVal
      .Page2.oPag.oDB006002_2_54.enabled = i_bVal
      .Page2.oPag.oDB007001_2_55.enabled = i_bVal
      .Page2.oPag.oDB008001_2_56.enabled = i_bVal
      .Page2.oPag.oDB008002_2_57.enabled = i_bVal
      .Page1.oPag.oDBSCLGEN_1_74.enabled = i_bVal
      .Page1.oPag.oDBDACONT_1_75.enabled = i_bVal
    endwith
    this.GSAI_MDB.SetStatus(i_cOp)
    this.GSAIAMDB.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DATESTBL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAI_MDB.SetChildrenStatus(i_cOp)
  *  this.GSAIAMDB.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATESTBL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBSERIAL,"DBSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB__ANNO,"DB__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB__MESE,"DB__MESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBTRIMES,"DBTRIMES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBTIPCON,"DBTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCODCON,"DBCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBPERFIS,"DBPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDESCRI,"DBDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCOGNOM,"DBCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB__NOME,"DB__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDATNAS,"DBDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCOMEST,"DBCOMEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBPRONAS,"DBPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBSTADOM,"DBSTADOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBSTAEST,"DBSTAEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCITEST,"DBCITEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBINDEST,"DBINDEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBPARIVA,"DBPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCODFIS,"DBCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCODGEN,"DBCODGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB003001,"DB003001",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB003002,"DB003002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB004001,"DB004001",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB004002,"DB004002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB005001,"DB005001",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB005002,"DB005002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB006001,"DB006001",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB006002,"DB006002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB007001,"DB007001",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB008001,"DB008001",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DB008002,"DB008002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBSCLGEN,"DBSCLGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDACONT,"DBDACONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESTBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTBL_IDX,2])
    i_lTable = "DATESTBL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DATESTBL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATESTBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTBL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DATESTBL_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"DEBLK","i_codazi,w_DBSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DATESTBL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATESTBL')
        i_extval=cp_InsertValODBCExtFlds(this,'DATESTBL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DBSERIAL,DB__ANNO,DB__MESE,DBTRIMES,DBTIPCON"+;
                  ",DBCODCON,DBPERFIS,DBDESCRI,DBCOGNOM,DB__NOME"+;
                  ",DBDATNAS,DBCOMEST,DBPRONAS,DBSTADOM,DBSTAEST"+;
                  ",DBCITEST,DBINDEST,DBPARIVA,DBCODFIS,DBCODGEN"+;
                  ",DB003001,DB003002,DB004001,DB004002,DB005001"+;
                  ",DB005002,DB006001,DB006002,DB007001,DB008001"+;
                  ",DB008002,DBSCLGEN,DBDACONT,UTCC,UTCV"+;
                  ",UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DBSERIAL)+;
                  ","+cp_ToStrODBC(this.w_DB__ANNO)+;
                  ","+cp_ToStrODBC(this.w_DB__MESE)+;
                  ","+cp_ToStrODBC(this.w_DBTRIMES)+;
                  ","+cp_ToStrODBC(this.w_DBTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_DBCODCON)+;
                  ","+cp_ToStrODBC(this.w_DBPERFIS)+;
                  ","+cp_ToStrODBC(this.w_DBDESCRI)+;
                  ","+cp_ToStrODBC(this.w_DBCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_DB__NOME)+;
                  ","+cp_ToStrODBC(this.w_DBDATNAS)+;
                  ","+cp_ToStrODBC(this.w_DBCOMEST)+;
                  ","+cp_ToStrODBC(this.w_DBPRONAS)+;
                  ","+cp_ToStrODBC(this.w_DBSTADOM)+;
                  ","+cp_ToStrODBC(this.w_DBSTAEST)+;
                  ","+cp_ToStrODBC(this.w_DBCITEST)+;
                  ","+cp_ToStrODBC(this.w_DBINDEST)+;
                  ","+cp_ToStrODBC(this.w_DBPARIVA)+;
                  ","+cp_ToStrODBC(this.w_DBCODFIS)+;
                  ","+cp_ToStrODBC(this.w_DBCODGEN)+;
                  ","+cp_ToStrODBC(this.w_DB003001)+;
                  ","+cp_ToStrODBC(this.w_DB003002)+;
                  ","+cp_ToStrODBC(this.w_DB004001)+;
                  ","+cp_ToStrODBC(this.w_DB004002)+;
                  ","+cp_ToStrODBC(this.w_DB005001)+;
                  ","+cp_ToStrODBC(this.w_DB005002)+;
                  ","+cp_ToStrODBC(this.w_DB006001)+;
                  ","+cp_ToStrODBC(this.w_DB006002)+;
                  ","+cp_ToStrODBC(this.w_DB007001)+;
                  ","+cp_ToStrODBC(this.w_DB008001)+;
                  ","+cp_ToStrODBC(this.w_DB008002)+;
                  ","+cp_ToStrODBC(this.w_DBSCLGEN)+;
                  ","+cp_ToStrODBC(this.w_DBDACONT)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATESTBL')
        i_extval=cp_InsertValVFPExtFlds(this,'DATESTBL')
        cp_CheckDeletedKey(i_cTable,0,'DBSERIAL',this.w_DBSERIAL)
        INSERT INTO (i_cTable);
              (DBSERIAL,DB__ANNO,DB__MESE,DBTRIMES,DBTIPCON,DBCODCON,DBPERFIS,DBDESCRI,DBCOGNOM,DB__NOME,DBDATNAS,DBCOMEST,DBPRONAS,DBSTADOM,DBSTAEST,DBCITEST,DBINDEST,DBPARIVA,DBCODFIS,DBCODGEN,DB003001,DB003002,DB004001,DB004002,DB005001,DB005002,DB006001,DB006002,DB007001,DB008001,DB008002,DBSCLGEN,DBDACONT,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DBSERIAL;
                  ,this.w_DB__ANNO;
                  ,this.w_DB__MESE;
                  ,this.w_DBTRIMES;
                  ,this.w_DBTIPCON;
                  ,this.w_DBCODCON;
                  ,this.w_DBPERFIS;
                  ,this.w_DBDESCRI;
                  ,this.w_DBCOGNOM;
                  ,this.w_DB__NOME;
                  ,this.w_DBDATNAS;
                  ,this.w_DBCOMEST;
                  ,this.w_DBPRONAS;
                  ,this.w_DBSTADOM;
                  ,this.w_DBSTAEST;
                  ,this.w_DBCITEST;
                  ,this.w_DBINDEST;
                  ,this.w_DBPARIVA;
                  ,this.w_DBCODFIS;
                  ,this.w_DBCODGEN;
                  ,this.w_DB003001;
                  ,this.w_DB003002;
                  ,this.w_DB004001;
                  ,this.w_DB004002;
                  ,this.w_DB005001;
                  ,this.w_DB005002;
                  ,this.w_DB006001;
                  ,this.w_DB006002;
                  ,this.w_DB007001;
                  ,this.w_DB008001;
                  ,this.w_DB008002;
                  ,this.w_DBSCLGEN;
                  ,this.w_DBDACONT;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DATESTBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTBL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DATESTBL_IDX,i_nConn)
      *
      * update DATESTBL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DATESTBL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DB__ANNO="+cp_ToStrODBC(this.w_DB__ANNO)+;
             ",DB__MESE="+cp_ToStrODBC(this.w_DB__MESE)+;
             ",DBTRIMES="+cp_ToStrODBC(this.w_DBTRIMES)+;
             ",DBTIPCON="+cp_ToStrODBC(this.w_DBTIPCON)+;
             ",DBCODCON="+cp_ToStrODBCNull(this.w_DBCODCON)+;
             ",DBPERFIS="+cp_ToStrODBC(this.w_DBPERFIS)+;
             ",DBDESCRI="+cp_ToStrODBC(this.w_DBDESCRI)+;
             ",DBCOGNOM="+cp_ToStrODBC(this.w_DBCOGNOM)+;
             ",DB__NOME="+cp_ToStrODBC(this.w_DB__NOME)+;
             ",DBDATNAS="+cp_ToStrODBC(this.w_DBDATNAS)+;
             ",DBCOMEST="+cp_ToStrODBC(this.w_DBCOMEST)+;
             ",DBPRONAS="+cp_ToStrODBC(this.w_DBPRONAS)+;
             ",DBSTADOM="+cp_ToStrODBC(this.w_DBSTADOM)+;
             ",DBSTAEST="+cp_ToStrODBC(this.w_DBSTAEST)+;
             ",DBCITEST="+cp_ToStrODBC(this.w_DBCITEST)+;
             ",DBINDEST="+cp_ToStrODBC(this.w_DBINDEST)+;
             ",DBPARIVA="+cp_ToStrODBC(this.w_DBPARIVA)+;
             ",DBCODFIS="+cp_ToStrODBC(this.w_DBCODFIS)+;
             ",DBCODGEN="+cp_ToStrODBC(this.w_DBCODGEN)+;
             ",DB003001="+cp_ToStrODBC(this.w_DB003001)+;
             ",DB003002="+cp_ToStrODBC(this.w_DB003002)+;
             ",DB004001="+cp_ToStrODBC(this.w_DB004001)+;
             ",DB004002="+cp_ToStrODBC(this.w_DB004002)+;
             ",DB005001="+cp_ToStrODBC(this.w_DB005001)+;
             ",DB005002="+cp_ToStrODBC(this.w_DB005002)+;
             ",DB006001="+cp_ToStrODBC(this.w_DB006001)+;
             ",DB006002="+cp_ToStrODBC(this.w_DB006002)+;
             ",DB007001="+cp_ToStrODBC(this.w_DB007001)+;
             ",DB008001="+cp_ToStrODBC(this.w_DB008001)+;
             ",DB008002="+cp_ToStrODBC(this.w_DB008002)+;
             ",DBSCLGEN="+cp_ToStrODBC(this.w_DBSCLGEN)+;
             ",DBDACONT="+cp_ToStrODBC(this.w_DBDACONT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DATESTBL')
        i_cWhere = cp_PKFox(i_cTable  ,'DBSERIAL',this.w_DBSERIAL  )
        UPDATE (i_cTable) SET;
              DB__ANNO=this.w_DB__ANNO;
             ,DB__MESE=this.w_DB__MESE;
             ,DBTRIMES=this.w_DBTRIMES;
             ,DBTIPCON=this.w_DBTIPCON;
             ,DBCODCON=this.w_DBCODCON;
             ,DBPERFIS=this.w_DBPERFIS;
             ,DBDESCRI=this.w_DBDESCRI;
             ,DBCOGNOM=this.w_DBCOGNOM;
             ,DB__NOME=this.w_DB__NOME;
             ,DBDATNAS=this.w_DBDATNAS;
             ,DBCOMEST=this.w_DBCOMEST;
             ,DBPRONAS=this.w_DBPRONAS;
             ,DBSTADOM=this.w_DBSTADOM;
             ,DBSTAEST=this.w_DBSTAEST;
             ,DBCITEST=this.w_DBCITEST;
             ,DBINDEST=this.w_DBINDEST;
             ,DBPARIVA=this.w_DBPARIVA;
             ,DBCODFIS=this.w_DBCODFIS;
             ,DBCODGEN=this.w_DBCODGEN;
             ,DB003001=this.w_DB003001;
             ,DB003002=this.w_DB003002;
             ,DB004001=this.w_DB004001;
             ,DB004002=this.w_DB004002;
             ,DB005001=this.w_DB005001;
             ,DB005002=this.w_DB005002;
             ,DB006001=this.w_DB006001;
             ,DB006002=this.w_DB006002;
             ,DB007001=this.w_DB007001;
             ,DB008001=this.w_DB008001;
             ,DB008002=this.w_DB008002;
             ,DBSCLGEN=this.w_DBSCLGEN;
             ,DBDACONT=this.w_DBDACONT;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAI_MDB : Saving
      this.GSAI_MDB.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DBSERIAL,"DBSERIAL";
             ,this.w_TIPREGV,"DBTIPREG";
             )
      this.GSAI_MDB.mReplace()
      * --- GSAIAMDB : Saving
      this.GSAIAMDB.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DBSERIAL,"DBSERIAL";
             ,this.w_TIPREGA,"DBTIPREG";
             )
      this.GSAIAMDB.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAI_MDB : Deleting
    this.GSAI_MDB.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DBSERIAL,"DBSERIAL";
           ,this.w_TIPREGV,"DBTIPREG";
           )
    this.GSAI_MDB.mDelete()
    * --- GSAIAMDB : Deleting
    this.GSAIAMDB.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DBSERIAL,"DBSERIAL";
           ,this.w_TIPREGA,"DBTIPREG";
           )
    this.GSAIAMDB.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATESTBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTBL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DATESTBL_IDX,i_nConn)
      *
      * delete DATESTBL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DBSERIAL',this.w_DBSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTBL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_DB__ANNO<>.w_DB__ANNO
            .w_ANNO = Str(.w_DB__ANNO,4,0)
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.t.)
        if .o_DB__ANNO<>.w_DB__ANNO
            .w_PERIOD = iif(EMPTY(NVL(.w_BLPERIOD, ' ')), 'M', .w_BLPERIOD)
        endif
        if .o_PERIOD<>.w_PERIOD
            .w_DB__MESE = iif(NVL(.w_PERIOD,'M')='M', Month(i_Datsys), 0)
        endif
        if .o_PERIOD<>.w_PERIOD
            .w_DBTRIMES = iif(NVL(.w_PERIOD,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        endif
        .DoRTCalc(8,9,.t.)
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBTIPCON<>.w_DBTIPCON
            .w_SCCODCLF = .w_DBCODCON
          .link_1_12('Full')
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBTIPCON<>.w_DBTIPCON
          .link_1_13('Full')
        endif
        .DoRTCalc(12,28,.t.)
          .link_1_31('Full')
        .DoRTCalc(30,33,.t.)
        if .o_DBCODCON<>.w_DBCODCON.or. .o_SCCODCLF<>.w_SCCODCLF
            .w_DBPERFIS = IIF(.w_ANPERFIS='S','P',IIF(Not Empty(NVL(.w_SCCODCLF,'')),'S','N'))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DBDESCRI = .w_ANDESCRI
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DBCOGNOM = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCCOGSCN,LEFT(.w_ANCOGNOM,40)),SPACE(40))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DB__NOME = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCNOMSCN,LEFT(.w_AN__NOME,40)),SPACE(40))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS <>.w_DBPERFIS 
            .w_DBDATNAS = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCDTNSCN,.w_ANDATNAS),CTOD('  -  -   '))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DBCOMEST = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCLCRSCN,.w_ANLOCNAS),SPACE(40))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DBPRONAS = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCPRNSCN,.w_ANPRONAS),SPACE(2))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DBSTADOM = IIF(.w_DBPERFIS$'PS',IIF(Not Empty(NVL(.w_SCCODCLF,'')),.w_SCSESDOM,LEFT(.w_NACODEST,3)),SPACE(3))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DBSTAEST = IIF(.w_DBPERFIS$'NS',LEFT(.w_NACODEST,3),SPACE(3))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DBCITEST = IIF(.w_DBPERFIS$'NS',.w_ANLOCALI,SPACE(40))
        endif
        if .o_DBCODCON<>.w_DBCODCON.or. .o_DBPERFIS<>.w_DBPERFIS
            .w_DBINDEST = IIF(.w_DBPERFIS$'NS',.w_ANINDIRI,SPACE(40))
        endif
        if .o_DBCODCON<>.w_DBCODCON
            .w_DBPARIVA = iif(Not Empty(Nvl(.w_Anpariva,'')),Alltrim(.w_Nacodiso)+Alltrim(.w_Anpariva),Space(25))
        endif
        if .o_DBCODCON<>.w_DBCODCON
            .w_DBCODFIS = iif(Empty(.w_Ancofisc), .w_Ancodfis, .w_Ancofisc)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"DEBLK","i_codazi,w_DBSERIAL")
          .op_DBSERIAL = .w_DBSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(47,78,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_UCADHAFXIZ()
    with this
          * --- Calcolo totali
          GSAI_BCB(this;
              ,'Totali';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDBTIPCON_1_10.enabled = this.oPgFrm.Page1.oPag.oDBTIPCON_1_10.mCond()
    this.oPgFrm.Page1.oPag.oDBCODCON_1_11.enabled = this.oPgFrm.Page1.oPag.oDBCODCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oDBDESCRI_1_37.enabled = this.oPgFrm.Page1.oPag.oDBDESCRI_1_37.mCond()
    this.oPgFrm.Page1.oPag.oDBCOGNOM_1_38.enabled = this.oPgFrm.Page1.oPag.oDBCOGNOM_1_38.mCond()
    this.oPgFrm.Page1.oPag.oDB__NOME_1_39.enabled = this.oPgFrm.Page1.oPag.oDB__NOME_1_39.mCond()
    this.oPgFrm.Page1.oPag.oDBDATNAS_1_40.enabled = this.oPgFrm.Page1.oPag.oDBDATNAS_1_40.mCond()
    this.oPgFrm.Page1.oPag.oDBCOMEST_1_41.enabled = this.oPgFrm.Page1.oPag.oDBCOMEST_1_41.mCond()
    this.oPgFrm.Page1.oPag.oDBPRONAS_1_42.enabled = this.oPgFrm.Page1.oPag.oDBPRONAS_1_42.mCond()
    this.oPgFrm.Page1.oPag.oDBSTADOM_1_43.enabled = this.oPgFrm.Page1.oPag.oDBSTADOM_1_43.mCond()
    this.oPgFrm.Page1.oPag.oDBSTAEST_1_44.enabled = this.oPgFrm.Page1.oPag.oDBSTAEST_1_44.mCond()
    this.oPgFrm.Page1.oPag.oDBCITEST_1_45.enabled = this.oPgFrm.Page1.oPag.oDBCITEST_1_45.mCond()
    this.oPgFrm.Page1.oPag.oDBINDEST_1_46.enabled = this.oPgFrm.Page1.oPag.oDBINDEST_1_46.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPERIOD_1_6.visible=!this.oPgFrm.Page1.oPag.oPERIOD_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDB__MESE_1_7.visible=!this.oPgFrm.Page1.oPag.oDB__MESE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDBTRIMES_1_8.visible=!this.oPgFrm.Page1.oPag.oDBTRIMES_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_UCADHAFXIZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ANNO
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DATPARBL_IDX,3]
    i_lTable = "DATPARBL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DATPARBL_IDX,2], .t., this.DATPARBL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DATPARBL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BL__ANNO,BLPERIOD";
                   +" from "+i_cTable+" "+i_lTable+" where BL__ANNO="+cp_ToStrODBC(this.w_ANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BL__ANNO',this.w_ANNO)
            select BL__ANNO,BLPERIOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNO = NVL(_Link_.BL__ANNO,space(4))
      this.w_BLPERIOD = NVL(_Link_.BLPERIOD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANNO = space(4)
      endif
      this.w_BLPERIOD = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DATPARBL_IDX,2])+'\'+cp_ToStr(_Link_.BL__ANNO,1)
      cp_ShowWarn(i_cKey,this.DATPARBL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DBCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DBTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS,ANPERFIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DBTIPCON;
                     ,'ANCODICE',trim(this.w_DBCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS,ANPERFIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDBCODCON_1_11'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DBTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS,ANPERFIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS,ANPERFIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS,ANPERFIS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DBTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS,ANPERFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS,ANPERFIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DBCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DBTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DBTIPCON;
                       ,'ANCODICE',this.w_DBCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS,ANPERFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANINDIRI = NVL(_Link_.ANINDIRI,space(35))
      this.w_ANDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_ANLOCNAS = NVL(_Link_.ANLOCNAS,space(30))
      this.w_ANPRONAS = NVL(_Link_.ANPRONAS,space(2))
      this.w_ANCOGNOM = NVL(_Link_.ANCOGNOM,space(20))
      this.w_AN__NOME = NVL(_Link_.AN__NOME,space(20))
      this.w_ANPARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_ANCOFISC = NVL(_Link_.ANCOFISC,space(20))
      this.w_ANLOCALI = NVL(_Link_.ANLOCALI,space(30))
      this.w_ANNAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
      this.w_ANPERFIS = NVL(_Link_.ANPERFIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_ANINDIRI = space(35)
      this.w_ANDATNAS = ctod("  /  /  ")
      this.w_ANLOCNAS = space(30)
      this.w_ANPRONAS = space(2)
      this.w_ANCOGNOM = space(20)
      this.w_AN__NOME = space(20)
      this.w_ANPARIVA = space(12)
      this.w_ANCOFISC = space(20)
      this.w_ANLOCALI = space(30)
      this.w_ANNAZION = space(3)
      this.w_ANCODFIS = space(16)
      this.w_ANPERFIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.ANCODICE as ANCODICE111"+ ",link_1_11.ANDESCRI as ANDESCRI111"+ ",link_1_11.ANINDIRI as ANINDIRI111"+ ",link_1_11.ANDATNAS as ANDATNAS111"+ ",link_1_11.ANLOCNAS as ANLOCNAS111"+ ",link_1_11.ANPRONAS as ANPRONAS111"+ ",link_1_11.ANCOGNOM as ANCOGNOM111"+ ",link_1_11.AN__NOME as AN__NOME111"+ ",link_1_11.ANPARIVA as ANPARIVA111"+ ",link_1_11.ANCOFISC as ANCOFISC111"+ ",link_1_11.ANLOCALI as ANLOCALI111"+ ",link_1_11.ANNAZION as ANNAZION111"+ ",link_1_11.ANCODFIS as ANCODFIS111"+ ",link_1_11.ANPERFIS as ANPERFIS111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on DATESTBL.DBCODCON=link_1_11.ANCODICE"+" and DATESTBL.DBTIPCON=link_1_11.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and DATESTBL.DBCODCON=link_1_11.ANCODICE(+)"'+'+" and DATESTBL.DBTIPCON=link_1_11.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODCLF
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_lTable = "SOGCOLNR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2], .t., this.SOGCOLNR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODCLF="+cp_ToStrODBC(this.w_SCCODCLF);
                   +" and SCTIPCLF="+cp_ToStrODBC(this.w_DBTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCTIPCLF',this.w_DBTIPCON;
                       ,'SCCODCLF',this.w_SCCODCLF)
            select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_SCCOGSCN = NVL(_Link_.SCCOGSCN,space(24))
      this.w_SCNOMSCN = NVL(_Link_.SCNOMSCN,space(20))
      this.w_SCDTNSCN = NVL(cp_ToDate(_Link_.SCDTNSCN),ctod("  /  /  "))
      this.w_SCLCRSCN = NVL(_Link_.SCLCRSCN,space(40))
      this.w_SCPRNSCN = NVL(_Link_.SCPRNSCN,space(2))
      this.w_SCSESDOM = NVL(_Link_.SCSESDOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCLF = space(15)
      endif
      this.w_CHCODCLF = space(15)
      this.w_SCCOGSCN = space(24)
      this.w_SCNOMSCN = space(20)
      this.w_SCDTNSCN = ctod("  /  /  ")
      this.w_SCLCRSCN = space(40)
      this.w_SCPRNSCN = space(2)
      this.w_SCSESDOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])+'\'+cp_ToStr(_Link_.SCTIPCLF,1)+'\'+cp_ToStr(_Link_.SCCODCLF,1)
      cp_ShowWarn(i_cKey,this.SOGCOLNR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CHCODCLF
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_lTable = "SOGCOLNR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2], .t., this.SOGCOLNR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODCLF="+cp_ToStrODBC(this.w_CHCODCLF);
                   +" and SCTIPCLF="+cp_ToStrODBC(this.w_DBTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCTIPCLF',this.w_DBTIPCON;
                       ,'SCCODCLF',this.w_CHCODCLF)
            select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_SCCOGSCN = NVL(_Link_.SCCOGSCN,space(24))
      this.w_SCNOMSCN = NVL(_Link_.SCNOMSCN,space(20))
      this.w_SCDTNSCN = NVL(cp_ToDate(_Link_.SCDTNSCN),ctod("  /  /  "))
      this.w_SCLCRSCN = NVL(_Link_.SCLCRSCN,space(40))
      this.w_SCPRNSCN = NVL(_Link_.SCPRNSCN,space(2))
      this.w_SCSESDOM = NVL(_Link_.SCSESDOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CHCODCLF = space(15)
      endif
      this.w_CHCODCLF = space(15)
      this.w_SCCOGSCN = space(24)
      this.w_SCNOMSCN = space(20)
      this.w_SCDTNSCN = ctod("  /  /  ")
      this.w_SCLCRSCN = space(40)
      this.w_SCPRNSCN = space(2)
      this.w_SCSESDOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])+'\'+cp_ToStr(_Link_.SCTIPCLF,1)+'\'+cp_ToStr(_Link_.SCCODCLF,1)
      cp_ShowWarn(i_cKey,this.SOGCOLNR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODEST,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NADESNAZ,NACODEST,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_NADESNAZ = NVL(_Link_.NADESNAZ,space(35))
      this.w_NACODEST = NVL(_Link_.NACODEST,space(5))
      this.w_NACODISO = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_NADESNAZ = space(35)
      this.w_NACODEST = space(5)
      this.w_NACODISO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDB__ANNO_1_2.value==this.w_DB__ANNO)
      this.oPgFrm.Page1.oPag.oDB__ANNO_1_2.value=this.w_DB__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIOD_1_6.RadioValue()==this.w_PERIOD)
      this.oPgFrm.Page1.oPag.oPERIOD_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDB__MESE_1_7.value==this.w_DB__MESE)
      this.oPgFrm.Page1.oPag.oDB__MESE_1_7.value=this.w_DB__MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDBTRIMES_1_8.value==this.w_DBTRIMES)
      this.oPgFrm.Page1.oPag.oDBTRIMES_1_8.value=this.w_DBTRIMES
    endif
    if not(this.oPgFrm.Page1.oPag.oDBTIPCON_1_10.RadioValue()==this.w_DBTIPCON)
      this.oPgFrm.Page1.oPag.oDBTIPCON_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODCON_1_11.value==this.w_DBCODCON)
      this.oPgFrm.Page1.oPag.oDBCODCON_1_11.value=this.w_DBCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDBPERFIS_1_36.RadioValue()==this.w_DBPERFIS)
      this.oPgFrm.Page1.oPag.oDBPERFIS_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDESCRI_1_37.value==this.w_DBDESCRI)
      this.oPgFrm.Page1.oPag.oDBDESCRI_1_37.value=this.w_DBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCOGNOM_1_38.value==this.w_DBCOGNOM)
      this.oPgFrm.Page1.oPag.oDBCOGNOM_1_38.value=this.w_DBCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDB__NOME_1_39.value==this.w_DB__NOME)
      this.oPgFrm.Page1.oPag.oDB__NOME_1_39.value=this.w_DB__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDATNAS_1_40.value==this.w_DBDATNAS)
      this.oPgFrm.Page1.oPag.oDBDATNAS_1_40.value=this.w_DBDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCOMEST_1_41.value==this.w_DBCOMEST)
      this.oPgFrm.Page1.oPag.oDBCOMEST_1_41.value=this.w_DBCOMEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDBPRONAS_1_42.value==this.w_DBPRONAS)
      this.oPgFrm.Page1.oPag.oDBPRONAS_1_42.value=this.w_DBPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDBSTADOM_1_43.value==this.w_DBSTADOM)
      this.oPgFrm.Page1.oPag.oDBSTADOM_1_43.value=this.w_DBSTADOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDBSTAEST_1_44.value==this.w_DBSTAEST)
      this.oPgFrm.Page1.oPag.oDBSTAEST_1_44.value=this.w_DBSTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCITEST_1_45.value==this.w_DBCITEST)
      this.oPgFrm.Page1.oPag.oDBCITEST_1_45.value=this.w_DBCITEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDBINDEST_1_46.value==this.w_DBINDEST)
      this.oPgFrm.Page1.oPag.oDBINDEST_1_46.value=this.w_DBINDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDBPARIVA_1_47.value==this.w_DBPARIVA)
      this.oPgFrm.Page1.oPag.oDBPARIVA_1_47.value=this.w_DBPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODFIS_1_48.value==this.w_DBCODFIS)
      this.oPgFrm.Page1.oPag.oDBCODFIS_1_48.value=this.w_DBCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oTAIMC003_2_12.value==this.w_TAIMC003)
      this.oPgFrm.Page2.oPag.oTAIMC003_2_12.value=this.w_TAIMC003
    endif
    if not(this.oPgFrm.Page2.oPag.oTAIMS003_2_30.value==this.w_TAIMS003)
      this.oPgFrm.Page2.oPag.oTAIMS003_2_30.value=this.w_TAIMS003
    endif
    if not(this.oPgFrm.Page2.oPag.oTAIMC004_2_31.value==this.w_TAIMC004)
      this.oPgFrm.Page2.oPag.oTAIMC004_2_31.value=this.w_TAIMC004
    endif
    if not(this.oPgFrm.Page2.oPag.oTAIMS004_2_32.value==this.w_TAIMS004)
      this.oPgFrm.Page2.oPag.oTAIMS004_2_32.value=this.w_TAIMS004
    endif
    if not(this.oPgFrm.Page2.oPag.oTAIMC005_2_33.value==this.w_TAIMC005)
      this.oPgFrm.Page2.oPag.oTAIMC005_2_33.value=this.w_TAIMC005
    endif
    if not(this.oPgFrm.Page2.oPag.oTAIMS005_2_34.value==this.w_TAIMS005)
      this.oPgFrm.Page2.oPag.oTAIMS005_2_34.value=this.w_TAIMS005
    endif
    if not(this.oPgFrm.Page2.oPag.oTPIMC003_2_35.value==this.w_TPIMC003)
      this.oPgFrm.Page2.oPag.oTPIMC003_2_35.value=this.w_TPIMC003
    endif
    if not(this.oPgFrm.Page2.oPag.oTPIMS003_2_36.value==this.w_TPIMS003)
      this.oPgFrm.Page2.oPag.oTPIMS003_2_36.value=this.w_TPIMS003
    endif
    if not(this.oPgFrm.Page2.oPag.oTPIMC004_2_37.value==this.w_TPIMC004)
      this.oPgFrm.Page2.oPag.oTPIMC004_2_37.value=this.w_TPIMC004
    endif
    if not(this.oPgFrm.Page2.oPag.oTPIMC005_2_38.value==this.w_TPIMC005)
      this.oPgFrm.Page2.oPag.oTPIMC005_2_38.value=this.w_TPIMC005
    endif
    if not(this.oPgFrm.Page2.oPag.oTPIMS005_2_39.value==this.w_TPIMS005)
      this.oPgFrm.Page2.oPag.oTPIMS005_2_39.value=this.w_TPIMS005
    endif
    if not(this.oPgFrm.Page2.oPag.oDB003001_2_47.RadioValue()==this.w_DB003001)
      this.oPgFrm.Page2.oPag.oDB003001_2_47.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB003002_2_48.RadioValue()==this.w_DB003002)
      this.oPgFrm.Page2.oPag.oDB003002_2_48.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB004001_2_49.RadioValue()==this.w_DB004001)
      this.oPgFrm.Page2.oPag.oDB004001_2_49.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB004002_2_50.RadioValue()==this.w_DB004002)
      this.oPgFrm.Page2.oPag.oDB004002_2_50.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB005001_2_51.RadioValue()==this.w_DB005001)
      this.oPgFrm.Page2.oPag.oDB005001_2_51.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB005002_2_52.RadioValue()==this.w_DB005002)
      this.oPgFrm.Page2.oPag.oDB005002_2_52.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB006001_2_53.RadioValue()==this.w_DB006001)
      this.oPgFrm.Page2.oPag.oDB006001_2_53.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB006002_2_54.RadioValue()==this.w_DB006002)
      this.oPgFrm.Page2.oPag.oDB006002_2_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB007001_2_55.RadioValue()==this.w_DB007001)
      this.oPgFrm.Page2.oPag.oDB007001_2_55.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB008001_2_56.RadioValue()==this.w_DB008001)
      this.oPgFrm.Page2.oPag.oDB008001_2_56.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDB008002_2_57.RadioValue()==this.w_DB008002)
      this.oPgFrm.Page2.oPag.oDB008002_2_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBSCLGEN_1_74.RadioValue()==this.w_DBSCLGEN)
      this.oPgFrm.Page1.oPag.oDBSCLGEN_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDACONT_1_75.RadioValue()==this.w_DBDACONT)
      this.oPgFrm.Page1.oPag.oDBDACONT_1_75.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DATESTBL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DB__MESE>=1 AND .w_DB__MESE<=12)  and not(NOT(.w_DB__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDB__MESE_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBTRIMES>=1 AND .w_DBTRIMES<=4)  and not(NOT(.w_DBTRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBTRIMES_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBDESCRI))  and (.cFunction<>'Query' And  .w_DBPERFIS$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBDESCRI_1_37.SetFocus()
            i_bnoObbl = !empty(.w_DBDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBCOGNOM))  and (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCOGNOM_1_38.SetFocus()
            i_bnoObbl = !empty(.w_DBCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DB__NOME))  and (.cFunction<>'Query' And .w_DBPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDB__NOME_1_39.SetFocus()
            i_bnoObbl = !empty(.w_DB__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBDATNAS))  and (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBDATNAS_1_40.SetFocus()
            i_bnoObbl = !empty(.w_DBDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBCOMEST))  and (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCOMEST_1_41.SetFocus()
            i_bnoObbl = !empty(.w_DBCOMEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBSTADOM))  and (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBSTADOM_1_43.SetFocus()
            i_bnoObbl = !empty(.w_DBSTADOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBSTAEST))  and (.cFunction<>'Query' And .w_DBPERFIS$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBSTAEST_1_44.SetFocus()
            i_bnoObbl = !empty(.w_DBSTAEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBCITEST))  and (.cFunction<>'Query' AND .w_DBPERFIS$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCITEST_1_45.SetFocus()
            i_bnoObbl = !empty(.w_DBCITEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBINDEST))  and (.cFunction<>'Query' And .w_DBPERFIS$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBINDEST_1_46.SetFocus()
            i_bnoObbl = !empty(.w_DBINDEST)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAI_MDB.CheckForm()
      if i_bres
        i_bres=  .GSAI_MDB.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSAIAMDB.CheckForm()
      if i_bres
        i_bres=  .GSAIAMDB.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DB__ANNO = this.w_DB__ANNO
    this.o_PERIOD = this.w_PERIOD
    this.o_DBTIPCON = this.w_DBTIPCON
    this.o_DBCODCON = this.w_DBCODCON
    this.o_SCCODCLF = this.w_SCCODCLF
    this.o_DBPERFIS = this.w_DBPERFIS
    * --- GSAI_MDB : Depends On
    this.GSAI_MDB.SaveDependsOn()
    * --- GSAIAMDB : Depends On
    this.GSAIAMDB.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=EMPTY(this.w_DBCODGEN)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Movimento incluso in generazione per file telematico"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=EMPTY(this.w_DBCODGEN)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Movimento incluso in generazione per file telematico"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsai_adbPag1 as StdContainer
  Width  = 806
  height = 578
  stdWidth  = 806
  stdheight = 578
  resizeXpos=589
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDB__ANNO_1_2 as StdField with uid="HYNPMMDCOL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DB__ANNO", cQueryName = "DB__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 51695739,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=236, Top=18, cSayPict='"9999"', cGetPict='"9999"'

  add object oPERIOD_1_6 as StdRadio with uid="LYVLNUYUSV",rtseq=5,rtrep=.f.,left=438, top=16, width=124,height=54;
    , ToolTipText = "Periodicit� adempimento";
    , cFormVar="w_PERIOD", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPERIOD_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Mensile"
      this.Buttons(1).HelpContextID = 206281994
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Trimestrale"
      this.Buttons(2).HelpContextID = 206281994
      this.Buttons(2).Top=17
      this.Buttons(3).Caption="Annuale"
      this.Buttons(3).HelpContextID = 206281994
      this.Buttons(3).Top=34
      this.SetAll("Width",122)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Periodicit� adempimento")
      StdRadio::init()
    endproc

  func oPERIOD_1_6.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPERIOD_1_6.GetRadio()
    this.Parent.oContained.w_PERIOD = this.RadioValue()
    return .t.
  endfunc

  func oPERIOD_1_6.SetRadio()
    this.Parent.oContained.w_PERIOD=trim(this.Parent.oContained.w_PERIOD)
    this.value = ;
      iif(this.Parent.oContained.w_PERIOD=='M',1,;
      iif(this.Parent.oContained.w_PERIOD=='T',2,;
      iif(this.Parent.oContained.w_PERIOD=='A',3,;
      0)))
  endfunc

  func oPERIOD_1_6.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oDB__MESE_1_7 as StdField with uid="NCANBKUKPL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DB__MESE", cQueryName = "DB__MESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese/trimestre di riferimento",;
    HelpContextID = 78327675,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=382, Top=18

  func oDB__MESE_1_7.mHide()
    with this.Parent.oContained
      return (NOT(.w_DB__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))
    endwith
  endfunc

  func oDB__MESE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DB__MESE>=1 AND .w_DB__MESE<=12)
    endwith
    return bRes
  endfunc

  add object oDBTRIMES_1_8 as StdField with uid="KIDSWEQFCY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DBTRIMES", cQueryName = "DBTRIMES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese/trimestre di riferimento",;
    HelpContextID = 207454089,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=382, Top=18

  func oDBTRIMES_1_8.mHide()
    with this.Parent.oContained
      return (NOT(.w_DBTRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))
    endwith
  endfunc

  func oDBTRIMES_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DBTRIMES>=1 AND .w_DBTRIMES<=4)
    endwith
    return bRes
  endfunc


  add object oDBTIPCON_1_10 as StdCombo with uid="FAVUWBBZYH",rtseq=8,rtrep=.f.,left=160,top=85,width=118,height=22;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 222003324;
    , cFormVar="w_DBTIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDBTIPCON_1_10.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDBTIPCON_1_10.GetRadio()
    this.Parent.oContained.w_DBTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDBTIPCON_1_10.SetRadio()
    this.Parent.oContained.w_DBTIPCON=trim(this.Parent.oContained.w_DBTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DBTIPCON=='C',1,;
      iif(this.Parent.oContained.w_DBTIPCON=='F',2,;
      0))
  endfunc

  func oDBTIPCON_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oDBTIPCON_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DBCODCON)
        bRes2=.link_1_11('Full')
      endif
      if .not. empty(.w_SCCODCLF)
        bRes2=.link_1_12('Full')
      endif
      if .not. empty(.w_CHCODCLF)
        bRes2=.link_1_13('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDBCODCON_1_11 as StdField with uid="EAHXPIIFWI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DBCODCON", cQueryName = "DBCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 234262652,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=289, Top=85, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DBTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DBCODCON"

  func oDBCODCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oDBCODCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DBTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DBTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDBCODCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oDBCODCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DBTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DBCODCON
     i_obj.ecpSave()
  endproc


  add object oDBPERFIS_1_36 as StdCombo with uid="IZUNFFGLMN",rtseq=34,rtrep=.f.,left=542,top=84,width=178,height=22;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 98582409;
    , cFormVar="w_DBPERFIS",RowSource=""+"Persona giuridica,"+"Persona fisica,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDBPERFIS_1_36.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oDBPERFIS_1_36.GetRadio()
    this.Parent.oContained.w_DBPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oDBPERFIS_1_36.SetRadio()
    this.Parent.oContained.w_DBPERFIS=trim(this.Parent.oContained.w_DBPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_DBPERFIS=='N',1,;
      iif(this.Parent.oContained.w_DBPERFIS=='P',2,;
      iif(this.Parent.oContained.w_DBPERFIS=='S',3,;
      0)))
  endfunc

  add object oDBDESCRI_1_37 as StdField with uid="EPFAFIXABF",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DBDESCRI", cQueryName = "DBDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 219185281,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=160, Top=136, InputMask=replicate('X',60)

  func oDBDESCRI_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And  .w_DBPERFIS$'NS')
    endwith
   endif
  endfunc

  add object oDBCOGNOM_1_38 as StdField with uid="VBWHBYPETD",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DBCOGNOM", cQueryName = "DBCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 46567549,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=160, Top=188, InputMask=replicate('X',40)

  func oDBCOGNOM_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
    endwith
   endif
  endfunc

  add object oDB__NOME_1_39 as StdField with uid="TZTOWOZVVJ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DB__NOME", cQueryName = "DB__NOME",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 21287045,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=458, Top=188, InputMask=replicate('X',40)

  func oDB__NOME_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DBPERFIS$'PS')
    endwith
   endif
  endfunc

  add object oDBDATNAS_1_40 as StdField with uid="VTKSSEXLQD",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DBDATNAS", cQueryName = "DBDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 33849463,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=247

  func oDBDATNAS_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
    endwith
   endif
  endfunc

  add object oDBCOMEST_1_41 as StdField with uid="OEGUAGVHVP",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DBCOMEST", cQueryName = "DBCOMEST",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 77164426,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=277, Top=247, InputMask=replicate('X',40), bHasZoom = .t. 

  func oDBCOMEST_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
    endwith
   endif
  endfunc

  proc oDBCOMEST_1_41.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_DBCOMEST",".w_DBPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDBPRONAS_1_42 as StdField with uid="HCPUHURXUB",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DBPRONAS", cQueryName = "DBPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 37929079,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=677, Top=247, InputMask=replicate('X',2), bHasZoom = .t. 

  func oDBPRONAS_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
    endwith
   endif
  endfunc

  proc oDBPRONAS_1_42.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_DBPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDBSTADOM_1_43 as StdField with uid="JPPHEZDBPD",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DBSTADOM", cQueryName = "DBSTADOM",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero del domicilio fiscale",;
    HelpContextID = 220237949,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=309, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oDBSTADOM_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DBPERFIS$'PS')
    endwith
   endif
  endfunc

  add object oDBSTAEST_1_44 as StdField with uid="JZJZNJGHQI",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DBSTAEST", cQueryName = "DBSTAEST",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 64974730,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=377, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oDBSTAEST_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DBPERFIS$'NS')
    endwith
   endif
  endfunc

  add object oDBCITEST_1_45 as StdField with uid="VZATVQHIMD",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DBCITEST", cQueryName = "DBCITEST",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Citt� estera della sede legale",;
    HelpContextID = 84111242,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=278, Top=377, InputMask=replicate('X',40)

  func oDBCITEST_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DBPERFIS$'NS')
    endwith
   endif
  endfunc

  add object oDBINDEST_1_46 as StdField with uid="PPBQNCJMIR",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DBINDEST", cQueryName = "DBINDEST",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero della sede legale",;
    HelpContextID = 67686282,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=160, Top=439, InputMask=replicate('X',40)

  func oDBINDEST_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DBPERFIS$'NS')
    endwith
   endif
  endfunc

  add object oDBPARIVA_1_47 as StdField with uid="AOJRIDAGNZ",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DBPARIVA", cQueryName = "DBPARIVA",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 148651895,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=160, Top=506, InputMask=replicate('X',25)

  add object oDBCODFIS_1_48 as StdField with uid="NEICLLJLQF",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DBCODFIS", cQueryName = "DBCODFIS",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 84504457,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=475, Top=506, InputMask=replicate('X',25)

  add object oDBSCLGEN_1_74 as StdCheck with uid="FZBDCCDEUF",rtseq=73,rtrep=.f.,left=592, top=18, caption="Escludi da generazione",;
    ToolTipText = "Se attivo, esclude i dati estratti dalla generazione",;
    HelpContextID = 108949380,;
    cFormVar="w_DBSCLGEN", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDBSCLGEN_1_74.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDBSCLGEN_1_74.GetRadio()
    this.Parent.oContained.w_DBSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oDBSCLGEN_1_74.SetRadio()
    this.Parent.oContained.w_DBSCLGEN=trim(this.Parent.oContained.w_DBSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_DBSCLGEN=='S',1,;
      0)
  endfunc

  add object oDBDACONT_1_75 as StdCheck with uid="NMLXKBMXSE",rtseq=74,rtrep=.f.,left=592, top=49, caption="Da verificare",;
    ToolTipText = "Se attivo, i dati estratti sono da verificare",;
    HelpContextID = 34898038,;
    cFormVar="w_DBDACONT", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDBDACONT_1_75.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDBDACONT_1_75.GetRadio()
    this.Parent.oContained.w_DBDACONT = this.RadioValue()
    return .t.
  endfunc

  func oDBDACONT_1_75.SetRadio()
    this.Parent.oContained.w_DBDACONT=trim(this.Parent.oContained.w_DBDACONT)
    this.value = ;
      iif(this.Parent.oContained.w_DBDACONT=='S',1,;
      0)
  endfunc

  add object oStr_1_5 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=159, Top=18,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="JGEELRUKLD",Visible=.t., Left=304, Top=18,;
    Alignment=1, Width=74, Height=18,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (NOT(.w_DB__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=160, Top=233,;
    Alignment=0, Width=76, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_51 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=277, Top=233,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune (o stato estero) di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="NOWYYKZWAZ",Visible=.t., Left=160, Top=361,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_53 as StdString with uid="DMKPRROUYT",Visible=.t., Left=668, Top=233,;
    Alignment=0, Width=52, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="XCNUOVRHIB",Visible=.t., Left=6, Top=503,;
    Alignment=0, Width=111, Height=18,;
    Caption="Identificativi fiscali"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="LHYQUAFBCO",Visible=.t., Left=160, Top=493,;
    Alignment=0, Width=158, Height=13,;
    Caption="Identificativo IVA"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="BSYUVARGFL",Visible=.t., Left=475, Top=489,;
    Alignment=0, Width=204, Height=17,;
    Caption="Codice fiscale/identificativo fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=7, Top=110,;
    Alignment=0, Width=111, Height=19,;
    Caption="Dati anagrafici"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_62 as StdString with uid="FZWRBKVHFS",Visible=.t., Left=7, Top=31,;
    Alignment=0, Width=118, Height=18,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="SWZCULOHUB",Visible=.t., Left=304, Top=18,;
    Alignment=1, Width=74, Height=18,;
    Caption="Trimestre:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (NOT(.w_DBTRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="OBGOXQVJWR",Visible=.t., Left=160, Top=118,;
    Alignment=0, Width=130, Height=17,;
    Caption="Ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_66 as StdString with uid="KOKISJQUOL",Visible=.t., Left=160, Top=172,;
    Alignment=0, Width=130, Height=17,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_67 as StdString with uid="GZLERJIXDA",Visible=.t., Left=459, Top=172,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_68 as StdString with uid="ANHHASVODD",Visible=.t., Left=160, Top=296,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="TPWEYQUKDS",Visible=.t., Left=7, Top=297,;
    Alignment=0, Width=111, Height=18,;
    Caption="Domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="WQAUIMBAAE",Visible=.t., Left=278, Top=361,;
    Alignment=0, Width=83, Height=13,;
    Caption="Citt� estera"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="KBZOHAZPBS",Visible=.t., Left=7, Top=380,;
    Alignment=0, Width=111, Height=18,;
    Caption="Sede legale"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="HGABSVUXLY",Visible=.t., Left=160, Top=425,;
    Alignment=0, Width=83, Height=13,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_73 as StdString with uid="FALBAPPRTT",Visible=.t., Left=289, Top=114,;
    Alignment=0, Width=209, Height=18,;
    Caption="Soggetto collettivo non residente"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (Empty(NVL(.w_CHCODCLF,'')))
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="DSOQGUGCHN",Visible=.t., Left=443, Top=87,;
    Alignment=1, Width=94, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oBox_1_54 as StdBox with uid="RDMWYOEEFE",left=1, top=77, width=787,height=464

  add object oBox_1_58 as StdBox with uid="CMPEHMCRJP",left=1, top=483, width=763,height=2

  add object oBox_1_59 as StdBox with uid="ZGZUPYKXSJ",left=1, top=345, width=763,height=2

  add object oBox_1_61 as StdBox with uid="PIUTDLGCGZ",left=149, top=77, width=1,height=462
enddefine
define class tgsai_adbPag2 as StdContainer
  Width  = 806
  height = 578
  stdWidth  = 806
  stdheight = 578
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTAIMC003_2_12 as StdField with uid="NYPJPEAUMW",rtseq=51,rtrep=.f.,;
    cFormVar = "w_TAIMC003", cQueryName = "TAIMC003",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo operazioni imponibili non imponibili ed esenti",;
    HelpContextID = 17313943,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=68, Top=84, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTAIMS003_2_30 as StdField with uid="FZKZVGUDDF",rtseq=52,rtrep=.f.,;
    cFormVar = "w_TAIMS003", cQueryName = "TAIMS003",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta operazioni imponibili non imponibili ed esenti",;
    HelpContextID = 536727,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=435, Top=84, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTAIMC004_2_31 as StdField with uid="DIBRUGQCXL",rtseq=53,rtrep=.f.,;
    cFormVar = "w_TAIMC004", cQueryName = "TAIMC004",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo operazioni non soggette ad IVA di beni",;
    HelpContextID = 17313942,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=68, Top=177, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTAIMS004_2_32 as StdField with uid="MKKIKOKORK",rtseq=54,rtrep=.f.,;
    cFormVar = "w_TAIMS004", cQueryName = "TAIMS004",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo operazioni non soggette ad IVA di servizi",;
    HelpContextID = 536726,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=435, Top=177, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTAIMC005_2_33 as StdField with uid="LBZPGRYULL",rtseq=55,rtrep=.f.,;
    cFormVar = "w_TAIMC005", cQueryName = "TAIMC005",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo note di variazione",;
    HelpContextID = 17313941,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=68, Top=253, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTAIMS005_2_34 as StdField with uid="JLKGFIVZKI",rtseq=56,rtrep=.f.,;
    cFormVar = "w_TAIMS005", cQueryName = "TAIMS005",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta note di variazione",;
    HelpContextID = 536725,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=435, Top=253, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTPIMC003_2_35 as StdField with uid="EEIHEVGPYB",rtseq=57,rtrep=.f.,;
    cFormVar = "w_TPIMC003", cQueryName = "TPIMC003",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo operazioni imponibili non imponibili ed esenti",;
    HelpContextID = 17310103,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=68, Top=350, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTPIMS003_2_36 as StdField with uid="ZILFRLIOST",rtseq=58,rtrep=.f.,;
    cFormVar = "w_TPIMS003", cQueryName = "TPIMS003",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta operazioni imponibili non imponibili ed esenti",;
    HelpContextID = 532887,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=435, Top=350, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTPIMC004_2_37 as StdField with uid="PXYOUJHPZK",rtseq=59,rtrep=.f.,;
    cFormVar = "w_TPIMC004", cQueryName = "TPIMC004",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo operazioni non soggette ad IVA",;
    HelpContextID = 17310102,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=68, Top=431, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTPIMC005_2_38 as StdField with uid="CNDUSJIPEG",rtseq=60,rtrep=.f.,;
    cFormVar = "w_TPIMC005", cQueryName = "TPIMC005",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo note di variazione",;
    HelpContextID = 17310101,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=68, Top=511, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oTPIMS005_2_39 as StdField with uid="GRGEUODMAH",rtseq=61,rtrep=.f.,;
    cFormVar = "w_TPIMS005", cQueryName = "TPIMS005",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta note di variazione",;
    HelpContextID = 532885,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=435, Top=511, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDB003001_2_47 as StdCheck with uid="TSFHKQNHVH",rtseq=62,rtrep=.f.,left=215, top=88, caption="Includi BL003001 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL003001 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 36094105,;
    cFormVar="w_DB003001", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB003001_2_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB003001_2_47.GetRadio()
    this.Parent.oContained.w_DB003001 = this.RadioValue()
    return .t.
  endfunc

  func oDB003001_2_47.SetRadio()
    this.Parent.oContained.w_DB003001=trim(this.Parent.oContained.w_DB003001)
    this.value = ;
      iif(this.Parent.oContained.w_DB003001=='S',1,;
      0)
  endfunc

  add object oDB003002_2_48 as StdCheck with uid="XRPYCNQNEC",rtseq=63,rtrep=.f.,left=578, top=88, caption="Includi BL003002 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL003002 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 36094104,;
    cFormVar="w_DB003002", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB003002_2_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB003002_2_48.GetRadio()
    this.Parent.oContained.w_DB003002 = this.RadioValue()
    return .t.
  endfunc

  func oDB003002_2_48.SetRadio()
    this.Parent.oContained.w_DB003002=trim(this.Parent.oContained.w_DB003002)
    this.value = ;
      iif(this.Parent.oContained.w_DB003002=='S',1,;
      0)
  endfunc

  add object oDB004001_2_49 as StdCheck with uid="SCHSBBRCFR",rtseq=64,rtrep=.f.,left=215, top=181, caption="Includi BL004001 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL004001 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 35045529,;
    cFormVar="w_DB004001", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB004001_2_49.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB004001_2_49.GetRadio()
    this.Parent.oContained.w_DB004001 = this.RadioValue()
    return .t.
  endfunc

  func oDB004001_2_49.SetRadio()
    this.Parent.oContained.w_DB004001=trim(this.Parent.oContained.w_DB004001)
    this.value = ;
      iif(this.Parent.oContained.w_DB004001=='S',1,;
      0)
  endfunc

  add object oDB004002_2_50 as StdCheck with uid="BCJXAXDGXS",rtseq=65,rtrep=.f.,left=578, top=181, caption="Includi BL004002 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL004002 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 35045528,;
    cFormVar="w_DB004002", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB004002_2_50.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB004002_2_50.GetRadio()
    this.Parent.oContained.w_DB004002 = this.RadioValue()
    return .t.
  endfunc

  func oDB004002_2_50.SetRadio()
    this.Parent.oContained.w_DB004002=trim(this.Parent.oContained.w_DB004002)
    this.value = ;
      iif(this.Parent.oContained.w_DB004002=='S',1,;
      0)
  endfunc

  add object oDB005001_2_51 as StdCheck with uid="YXKCKMZVCJ",rtseq=66,rtrep=.f.,left=215, top=257, caption="Includi BL005001 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL005001 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 33996953,;
    cFormVar="w_DB005001", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB005001_2_51.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB005001_2_51.GetRadio()
    this.Parent.oContained.w_DB005001 = this.RadioValue()
    return .t.
  endfunc

  func oDB005001_2_51.SetRadio()
    this.Parent.oContained.w_DB005001=trim(this.Parent.oContained.w_DB005001)
    this.value = ;
      iif(this.Parent.oContained.w_DB005001=='S',1,;
      0)
  endfunc

  add object oDB005002_2_52 as StdCheck with uid="GZBNJSVRNZ",rtseq=67,rtrep=.f.,left=578, top=257, caption="Includi BL005002 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL005002 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 33996952,;
    cFormVar="w_DB005002", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB005002_2_52.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB005002_2_52.GetRadio()
    this.Parent.oContained.w_DB005002 = this.RadioValue()
    return .t.
  endfunc

  func oDB005002_2_52.SetRadio()
    this.Parent.oContained.w_DB005002=trim(this.Parent.oContained.w_DB005002)
    this.value = ;
      iif(this.Parent.oContained.w_DB005002=='S',1,;
      0)
  endfunc

  add object oDB006001_2_53 as StdCheck with uid="TGRKKNTNKI",rtseq=68,rtrep=.f.,left=215, top=354, caption="Includi BL006001 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL006001 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 32948377,;
    cFormVar="w_DB006001", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB006001_2_53.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB006001_2_53.GetRadio()
    this.Parent.oContained.w_DB006001 = this.RadioValue()
    return .t.
  endfunc

  func oDB006001_2_53.SetRadio()
    this.Parent.oContained.w_DB006001=trim(this.Parent.oContained.w_DB006001)
    this.value = ;
      iif(this.Parent.oContained.w_DB006001=='S',1,;
      0)
  endfunc

  add object oDB006002_2_54 as StdCheck with uid="DFFGFJTUIO",rtseq=69,rtrep=.f.,left=578, top=354, caption="Includi BL006002 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL006002 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 32948376,;
    cFormVar="w_DB006002", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB006002_2_54.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB006002_2_54.GetRadio()
    this.Parent.oContained.w_DB006002 = this.RadioValue()
    return .t.
  endfunc

  func oDB006002_2_54.SetRadio()
    this.Parent.oContained.w_DB006002=trim(this.Parent.oContained.w_DB006002)
    this.value = ;
      iif(this.Parent.oContained.w_DB006002=='S',1,;
      0)
  endfunc

  add object oDB007001_2_55 as StdCheck with uid="POXKPOXTRX",rtseq=70,rtrep=.f.,left=215, top=435, caption="Includi BL007001 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL007001 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 31899801,;
    cFormVar="w_DB007001", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB007001_2_55.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB007001_2_55.GetRadio()
    this.Parent.oContained.w_DB007001 = this.RadioValue()
    return .t.
  endfunc

  func oDB007001_2_55.SetRadio()
    this.Parent.oContained.w_DB007001=trim(this.Parent.oContained.w_DB007001)
    this.value = ;
      iif(this.Parent.oContained.w_DB007001=='S',1,;
      0)
  endfunc

  add object oDB008001_2_56 as StdCheck with uid="GMPBLMMMKU",rtseq=71,rtrep=.f.,left=215, top=512, caption="Includi BL008001 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL008001 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 30851225,;
    cFormVar="w_DB008001", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB008001_2_56.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB008001_2_56.GetRadio()
    this.Parent.oContained.w_DB008001 = this.RadioValue()
    return .t.
  endfunc

  func oDB008001_2_56.SetRadio()
    this.Parent.oContained.w_DB008001=trim(this.Parent.oContained.w_DB008001)
    this.value = ;
      iif(this.Parent.oContained.w_DB008001=='S',1,;
      0)
  endfunc

  add object oDB008002_2_57 as StdCheck with uid="ZSKULTFRAI",rtseq=72,rtrep=.f.,left=578, top=512, caption="Includi BL008002 con valore a 1",;
    ToolTipText = "Se attivo, nel caso in cui il valore del campo BL008002 � compreso tra 0,01 e 0,99 in generazione sar� valorizzato con 1",;
    HelpContextID = 30851224,;
    cFormVar="w_DB008002", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDB008002_2_57.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDB008002_2_57.GetRadio()
    this.Parent.oContained.w_DB008002 = this.RadioValue()
    return .t.
  endfunc

  func oDB008002_2_57.SetRadio()
    this.Parent.oContained.w_DB008002=trim(this.Parent.oContained.w_DB008002)
    this.value = ;
      iif(this.Parent.oContained.w_DB008002=='S',1,;
      0)
  endfunc

  add object oStr_2_2 as StdString with uid="UTLLKNICHC",Visible=.t., Left=262, Top=15,;
    Alignment=2, Width=211, Height=19,;
    Caption="Operazioni attive"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_3 as StdString with uid="QWEGVRFTWU",Visible=.t., Left=262, Top=280,;
    Alignment=2, Width=211, Height=19,;
    Caption="Operazioni passive"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_5 as StdString with uid="EVUWLBWGBH",Visible=.t., Left=226, Top=42,;
    Alignment=2, Width=300, Height=18,;
    Caption="Operazioni imponibili, non imponibili ed esenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="EROCCLVVSJ",Visible=.t., Left=222, Top=114,;
    Alignment=2, Width=300, Height=18,;
    Caption="Operazioni non soggette a IVA"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="XSGRCLTKNK",Visible=.t., Left=229, Top=207,;
    Alignment=2, Width=300, Height=18,;
    Caption="Note di variazione"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="UXJSQXOYXG",Visible=.t., Left=68, Top=67,;
    Alignment=0, Width=157, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="TXGEFDQWRN",Visible=.t., Left=435, Top=67,;
    Alignment=0, Width=157, Height=18,;
    Caption="Imposta"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="DNGJIXRMGC",Visible=.t., Left=106, Top=133,;
    Alignment=0, Width=157, Height=18,;
    Caption="Cessione di beni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_14 as StdString with uid="CLRULRQEOM",Visible=.t., Left=430, Top=133,;
    Alignment=0, Width=157, Height=18,;
    Caption="Prestazione di servizi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_15 as StdString with uid="PHDIJPTOTA",Visible=.t., Left=68, Top=157,;
    Alignment=0, Width=157, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="MJOWOGHKSW",Visible=.t., Left=435, Top=157,;
    Alignment=0, Width=157, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="UIRURRBBMC",Visible=.t., Left=68, Top=231,;
    Alignment=0, Width=157, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="IXTSNEUVEY",Visible=.t., Left=435, Top=231,;
    Alignment=0, Width=157, Height=18,;
    Caption="Imposta"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="YQDDLGRQZG",Visible=.t., Left=226, Top=305,;
    Alignment=2, Width=300, Height=18,;
    Caption="Operazioni imponibili, non imponibili ed esenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="YQZZXZNUIF",Visible=.t., Left=222, Top=383,;
    Alignment=2, Width=300, Height=18,;
    Caption="Operazioni non soggette a IVA"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="QUPFINGMQG",Visible=.t., Left=229, Top=464,;
    Alignment=2, Width=300, Height=18,;
    Caption="Note di variazione"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="TWTKFBLEGX",Visible=.t., Left=68, Top=328,;
    Alignment=0, Width=157, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="YKSMDPYXIK",Visible=.t., Left=435, Top=328,;
    Alignment=0, Width=157, Height=18,;
    Caption="Imposta"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="DHDQMISZML",Visible=.t., Left=68, Top=409,;
    Alignment=0, Width=157, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="EKJEPDNHDE",Visible=.t., Left=68, Top=492,;
    Alignment=0, Width=157, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="AZLDHMRBNV",Visible=.t., Left=435, Top=492,;
    Alignment=0, Width=157, Height=18,;
    Caption="Imposta"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="QVNKQKIGCR",Visible=.t., Left=14, Top=84,;
    Alignment=0, Width=50, Height=19,;
    Caption="BL003"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="HXWZNYGRYO",Visible=.t., Left=14, Top=177,;
    Alignment=0, Width=50, Height=19,;
    Caption="BL004"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="EUBNTQUNLP",Visible=.t., Left=14, Top=254,;
    Alignment=0, Width=50, Height=19,;
    Caption="BL005"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_44 as StdString with uid="KMMTYDYVBE",Visible=.t., Left=14, Top=353,;
    Alignment=0, Width=50, Height=19,;
    Caption="BL006"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_45 as StdString with uid="UPIQNDYDEV",Visible=.t., Left=14, Top=430,;
    Alignment=0, Width=50, Height=19,;
    Caption="BL007"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_46 as StdString with uid="ODFDWDKJYV",Visible=.t., Left=14, Top=513,;
    Alignment=0, Width=50, Height=19,;
    Caption="BL008"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_1 as StdBox with uid="HHETSLKHRL",left=8, top=6, width=777,height=535

  add object oBox_2_4 as StdBox with uid="BPQSISMOJF",left=8, top=36, width=772,height=27

  add object oBox_2_6 as StdBox with uid="PQIKTDUZBJ",left=8, top=108, width=773,height=46

  add object oBox_2_8 as StdBox with uid="YJBHLUZYSD",left=8, top=202, width=773,height=27

  add object oBox_2_19 as StdBox with uid="WLFTEFPYII",left=8, top=299, width=773,height=27

  add object oBox_2_21 as StdBox with uid="OIWYXXINDL",left=8, top=377, width=773,height=29

  add object oBox_2_23 as StdBox with uid="QIMOQYJZXE",left=8, top=458, width=773,height=27
enddefine
define class tgsai_adbPag3 as StdContainer
  Width  = 806
  height = 578
  stdWidth  = 806
  stdheight = 578
  resizeXpos=470
  resizeYpos=128
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_3 as stdDynamicChildContainer with uid="VDHGXXJESU",left=11, top=26, width=793, height=260, bOnScreen=.t.;



  add object oLinkPC_3_8 as stdDynamicChildContainer with uid="JILYNLSWOC",left=11, top=315, width=790, height=258, bOnScreen=.t.;


  add object oStr_3_4 as StdString with uid="BGFPLFWJFG",Visible=.t., Left=14, Top=7,;
    Alignment=0, Width=211, Height=19,;
    Caption="Operazioni attive"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_7 as StdString with uid="JGWYRKVSMK",Visible=.t., Left=14, Top=293,;
    Alignment=0, Width=211, Height=19,;
    Caption="Operazioni passive"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_1 as StdBox with uid="GETSHRJOPE",left=7, top=5, width=797,height=286

  add object oBox_3_6 as StdBox with uid="WRJLXRBDWU",left=7, top=289, width=798,height=289
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_adb','DATESTBL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DBSERIAL=DATESTBL.DBSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
