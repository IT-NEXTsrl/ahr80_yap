* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bgp                                                        *
*              Calcola periodo di generazione file antievasione IVA            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-25                                                      *
* Last revis.: 2010-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bgp",oParentObject,m.pPARAM)
return(i_retval)

define class tgsai_bgp as StdBatch
  * --- Local variables
  pPARAM = space(3)
  w_MESE = 0
  w_TRIMES = 0
  * --- WorkFile variables
  ANTIVAGC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- calcolo periodo successivo all'ultima estrazione per l'anno selezionato
    if this.pPARAM="CHK"
      if this.oParentObject.w_PERIOD="M"
        this.oParentObject.w_AITRIMES = 0
        * --- Select from ANTIVAGC
        i_nConn=i_TableProp[this.ANTIVAGC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAGC_idx,2],.t.,this.ANTIVAGC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(AI__MESE) as DT__MESE  from "+i_cTable+" ANTIVAGC ";
              +" where AI__ANNO="+cp_ToStrODBC(this.oParentObject.w_AI__ANNO)+" AND AI__MESE="+cp_ToStrODBC(this.oParentObject.w_AI__MESE)+"";
               ,"_Curs_ANTIVAGC")
        else
          select MAX(AI__MESE) as DT__MESE from (i_cTable);
           where AI__ANNO=this.oParentObject.w_AI__ANNO AND AI__MESE=this.oParentObject.w_AI__MESE;
            into cursor _Curs_ANTIVAGC
        endif
        if used('_Curs_ANTIVAGC')
          select _Curs_ANTIVAGC
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_AICORTER = IIF(DT__MESE=this.oParentObject.w_AI__MESE, "S", this.oParentObject.w_AICORTER)
          this.oParentObject.w_AICOMINT = iif(this.oParentObject.w_AICORTER="S"," ",this.oParentObject.w_AICOMINT)
            select _Curs_ANTIVAGC
            continue
          enddo
          use
        endif
      else
        this.oParentObject.w_AI__MESE = 0
        * --- Select from ANTIVAGC
        i_nConn=i_TableProp[this.ANTIVAGC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAGC_idx,2],.t.,this.ANTIVAGC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(AITRIMES) as DTTRIMES  from "+i_cTable+" ANTIVAGC ";
              +" where AI__ANNO="+cp_ToStrODBC(this.oParentObject.w_AI__ANNO)+" AND AITRIMES="+cp_ToStrODBC(this.oParentObject.w_AITRIMES)+"";
               ,"_Curs_ANTIVAGC")
        else
          select MAX(AITRIMES) as DTTRIMES from (i_cTable);
           where AI__ANNO=this.oParentObject.w_AI__ANNO AND AITRIMES=this.oParentObject.w_AITRIMES;
            into cursor _Curs_ANTIVAGC
        endif
        if used('_Curs_ANTIVAGC')
          select _Curs_ANTIVAGC
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_AICORTER = IIF(DTTRIMES=this.oParentObject.w_AITRIMES, "S", this.oParentObject.w_AICORTER)
          this.oParentObject.w_AICOMINT = iif(this.oParentObject.w_AICORTER="S"," ",this.oParentObject.w_AICOMINT)
            select _Curs_ANTIVAGC
            continue
          enddo
          use
        endif
      endif
    else
      if this.oParentObject.w_PERIOD="M"
        this.oParentObject.w_AITRIMES = 0
        * --- Select from ANTIVAGC
        i_nConn=i_TableProp[this.ANTIVAGC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAGC_idx,2],.t.,this.ANTIVAGC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(AI__MESE) as DT__MESE  from "+i_cTable+" ANTIVAGC ";
              +" where AI__ANNO="+cp_ToStrODBC(this.oParentObject.w_AI__ANNO)+"";
               ,"_Curs_ANTIVAGC")
        else
          select MAX(AI__MESE) as DT__MESE from (i_cTable);
           where AI__ANNO=this.oParentObject.w_AI__ANNO;
            into cursor _Curs_ANTIVAGC
        endif
        if used('_Curs_ANTIVAGC')
          select _Curs_ANTIVAGC
          locate for 1=1
          do while not(eof())
          this.w_MESE = NVL(_Curs_ANTIVAGC.DT__MESE,0)
            select _Curs_ANTIVAGC
            continue
          enddo
          use
        endif
        this.oParentObject.w_AI__MESE = iif(NVL(this.w_MESE,0)<>0, this.w_MESE + 1, this.oParentObject.w_AI__MESE)
        this.oParentObject.w_AI__MESE = iif(this.oParentObject.w_AI__MESE>12, 12, this.oParentObject.w_AI__MESE)
      else
        this.oParentObject.w_AI__MESE = 0
        * --- Select from ANTIVAGC
        i_nConn=i_TableProp[this.ANTIVAGC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAGC_idx,2],.t.,this.ANTIVAGC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(AITRIMES) as DTTRIMES  from "+i_cTable+" ANTIVAGC ";
              +" where AI__ANNO="+cp_ToStrODBC(this.oParentObject.w_AI__ANNO)+"";
               ,"_Curs_ANTIVAGC")
        else
          select MAX(AITRIMES) as DTTRIMES from (i_cTable);
           where AI__ANNO=this.oParentObject.w_AI__ANNO;
            into cursor _Curs_ANTIVAGC
        endif
        if used('_Curs_ANTIVAGC')
          select _Curs_ANTIVAGC
          locate for 1=1
          do while not(eof())
          this.w_TRIMES = NVL(_Curs_ANTIVAGC.DTTRIMES,0)
            select _Curs_ANTIVAGC
            continue
          enddo
          use
        endif
        this.oParentObject.w_AITRIMES = iif(NVL(this.w_TRIMES,0)<>0, this.w_TRIMES + 1, this.oParentObject.w_AITRIMES)
        this.oParentObject.w_AITRIMES = iif(this.oParentObject.w_AITRIMES>4, 4, this.oParentObject.w_AITRIMES)
      endif
    endif
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ANTIVAGC'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ANTIVAGC')
      use in _Curs_ANTIVAGC
    endif
    if used('_Curs_ANTIVAGC')
      use in _Curs_ANTIVAGC
    endif
    if used('_Curs_ANTIVAGC')
      use in _Curs_ANTIVAGC
    endif
    if used('_Curs_ANTIVAGC')
      use in _Curs_ANTIVAGC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
