* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_mco                                                        *
*              Dettagli parametri controparte                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-28                                                      *
* Last revis.: 2012-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsai_mco")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsai_mco")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsai_mco")
  return

* --- Class definition
define class tgsai_mco as StdPCForm
  Width  = 793
  Height = 517
  Top    = 10
  Left   = 10
  cComment = "Dettagli parametri controparte"
  cPrg = "gsai_mco"
  HelpContextID=171309207
  add object cnt as tcgsai_mco
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsai_mco as PCContext
  w_PE__ANNO = space(4)
  w_PETIPCON = space(1)
  w_TIPCON = space(1)
  w_PEOPETRE = space(1)
  w_PETIPPRE = space(1)
  w_PECATCON = space(5)
  w_PEFLPRIV = space(1)
  w_PECONSUP = space(15)
  w_PEREGATT = space(1)
  w_PEOPERAT = space(1)
  w_PEIMPFAT = 0
  w_PECONDIZ = space(1)
  w_PEOPERAR = space(1)
  w_PENUMFAT = 0
  w_PECONEST = space(1)
  w_PETIPEST = space(1)
  w_PEIMPEST = 0
  w_NULIV = 0
  w_TIPMAS = space(1)
  w_FILTRO1 = space(254)
  w_FILTRO2 = space(254)
  w_FILTRO3 = space(254)
  w_LOG_FORMULA = space(10)
  w_C2DESCRI = space(35)
  w_MCDESCRI = space(40)
  proc Save(i_oFrom)
    this.w_PE__ANNO = i_oFrom.w_PE__ANNO
    this.w_PETIPCON = i_oFrom.w_PETIPCON
    this.w_TIPCON = i_oFrom.w_TIPCON
    this.w_PEOPETRE = i_oFrom.w_PEOPETRE
    this.w_PETIPPRE = i_oFrom.w_PETIPPRE
    this.w_PECATCON = i_oFrom.w_PECATCON
    this.w_PEFLPRIV = i_oFrom.w_PEFLPRIV
    this.w_PECONSUP = i_oFrom.w_PECONSUP
    this.w_PEREGATT = i_oFrom.w_PEREGATT
    this.w_PEOPERAT = i_oFrom.w_PEOPERAT
    this.w_PEIMPFAT = i_oFrom.w_PEIMPFAT
    this.w_PECONDIZ = i_oFrom.w_PECONDIZ
    this.w_PEOPERAR = i_oFrom.w_PEOPERAR
    this.w_PENUMFAT = i_oFrom.w_PENUMFAT
    this.w_PECONEST = i_oFrom.w_PECONEST
    this.w_PETIPEST = i_oFrom.w_PETIPEST
    this.w_PEIMPEST = i_oFrom.w_PEIMPEST
    this.w_NULIV = i_oFrom.w_NULIV
    this.w_TIPMAS = i_oFrom.w_TIPMAS
    this.w_FILTRO1 = i_oFrom.w_FILTRO1
    this.w_FILTRO2 = i_oFrom.w_FILTRO2
    this.w_FILTRO3 = i_oFrom.w_FILTRO3
    this.w_LOG_FORMULA = i_oFrom.w_LOG_FORMULA
    this.w_C2DESCRI = i_oFrom.w_C2DESCRI
    this.w_MCDESCRI = i_oFrom.w_MCDESCRI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PE__ANNO = this.w_PE__ANNO
    i_oTo.w_PETIPCON = this.w_PETIPCON
    i_oTo.w_TIPCON = this.w_TIPCON
    i_oTo.w_PEOPETRE = this.w_PEOPETRE
    i_oTo.w_PETIPPRE = this.w_PETIPPRE
    i_oTo.w_PECATCON = this.w_PECATCON
    i_oTo.w_PEFLPRIV = this.w_PEFLPRIV
    i_oTo.w_PECONSUP = this.w_PECONSUP
    i_oTo.w_PEREGATT = this.w_PEREGATT
    i_oTo.w_PEOPERAT = this.w_PEOPERAT
    i_oTo.w_PEIMPFAT = this.w_PEIMPFAT
    i_oTo.w_PECONDIZ = this.w_PECONDIZ
    i_oTo.w_PEOPERAR = this.w_PEOPERAR
    i_oTo.w_PENUMFAT = this.w_PENUMFAT
    i_oTo.w_PECONEST = this.w_PECONEST
    i_oTo.w_PETIPEST = this.w_PETIPEST
    i_oTo.w_PEIMPEST = this.w_PEIMPEST
    i_oTo.w_NULIV = this.w_NULIV
    i_oTo.w_TIPMAS = this.w_TIPMAS
    i_oTo.w_FILTRO1 = this.w_FILTRO1
    i_oTo.w_FILTRO2 = this.w_FILTRO2
    i_oTo.w_FILTRO3 = this.w_FILTRO3
    i_oTo.w_LOG_FORMULA = this.w_LOG_FORMULA
    i_oTo.w_C2DESCRI = this.w_C2DESCRI
    i_oTo.w_MCDESCRI = this.w_MCDESCRI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsai_mco as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 793
  Height = 517
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-28"
  HelpContextID=171309207
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ANTIVDPC_IDX = 0
  CACOCLFO_IDX = 0
  MASTRI_IDX = 0
  cFile = "ANTIVDPC"
  cKeySelect = "PE__ANNO"
  cKeyWhere  = "PE__ANNO=this.w_PE__ANNO"
  cKeyDetail  = "PE__ANNO=this.w_PE__ANNO"
  cKeyWhereODBC = '"PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';

  cKeyDetailWhereODBC = '"PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ANTIVDPC.PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ANTIVDPC.CPROWNUM '
  cPrg = "gsai_mco"
  cComment = "Dettagli parametri controparte"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 19
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PE__ANNO = space(4)
  w_PETIPCON = space(1)
  o_PETIPCON = space(1)
  w_TIPCON = space(1)
  w_PEOPETRE = space(1)
  o_PEOPETRE = space(1)
  w_PETIPPRE = space(1)
  o_PETIPPRE = space(1)
  w_PECATCON = space(5)
  o_PECATCON = space(5)
  w_PEFLPRIV = space(1)
  o_PEFLPRIV = space(1)
  w_PECONSUP = space(15)
  o_PECONSUP = space(15)
  w_PEREGATT = space(1)
  w_PEOPERAT = space(1)
  o_PEOPERAT = space(1)
  w_PEIMPFAT = 0
  o_PEIMPFAT = 0
  w_PECONDIZ = space(1)
  w_PEOPERAR = space(1)
  o_PEOPERAR = space(1)
  w_PENUMFAT = 0
  o_PENUMFAT = 0
  w_PECONEST = space(1)
  w_PETIPEST = space(1)
  o_PETIPEST = space(1)
  w_PEIMPEST = 0
  w_NULIV = 0
  w_TIPMAS = space(1)
  w_FILTRO1 = space(254)
  w_FILTRO2 = space(254)
  w_FILTRO3 = space(254)
  w_LOG_FORMULA = space(0)
  w_C2DESCRI = space(35)
  w_MCDESCRI = space(40)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_mcoPag1","gsai_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CACOCLFO'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='ANTIVDPC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVDPC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVDPC_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsai_mco'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ANTIVDPC where PE__ANNO=KeySet.PE__ANNO
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ANTIVDPC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDPC_IDX,2],this.bLoadRecFilter,this.ANTIVDPC_IDX,"gsai_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVDPC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVDPC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVDPC '
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  )
      select * from (i_cTable) ANTIVDPC where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PE__ANNO = NVL(PE__ANNO,space(4))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ANTIVDPC')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_NULIV = 0
          .w_TIPMAS = space(1)
          .w_C2DESCRI = space(35)
          .w_MCDESCRI = space(40)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PETIPCON = NVL(PETIPCON,space(1))
        .w_TIPCON = .w_PETIPCON
          .w_PEOPETRE = NVL(PEOPETRE,space(1))
          .w_PETIPPRE = NVL(PETIPPRE,space(1))
          .w_PECATCON = NVL(PECATCON,space(5))
          if link_2_5_joined
            this.w_PECATCON = NVL(C2CODICE205,NVL(this.w_PECATCON,space(5)))
            this.w_C2DESCRI = NVL(C2DESCRI205,space(35))
          else
          .link_2_5('Load')
          endif
          .w_PEFLPRIV = NVL(PEFLPRIV,space(1))
          .w_PECONSUP = NVL(PECONSUP,space(15))
          if link_2_7_joined
            this.w_PECONSUP = NVL(MCCODICE207,NVL(this.w_PECONSUP,space(15)))
            this.w_NULIV = NVL(MCNUMLIV207,0)
            this.w_TIPMAS = NVL(MCTIPMAS207,space(1))
            this.w_MCDESCRI = NVL(MCDESCRI207,space(40))
          else
          .link_2_7('Load')
          endif
          .w_PEREGATT = NVL(PEREGATT,space(1))
          .w_PEOPERAT = NVL(PEOPERAT,space(1))
          .w_PEIMPFAT = NVL(PEIMPFAT,0)
          .w_PECONDIZ = NVL(PECONDIZ,space(1))
          .w_PEOPERAR = NVL(PEOPERAR,space(1))
          .w_PENUMFAT = NVL(PENUMFAT,0)
          .w_PECONEST = NVL(PECONEST,space(1))
          .w_PETIPEST = NVL(PETIPEST,space(1))
          .w_PEIMPEST = NVL(PEIMPEST,0)
        .w_FILTRO1 = IIF(.w_PEOPERAT='N','',IIF(.w_PEOPERAT='M','maggiore uguale di ',IIF(.w_PEOPERAT='I','minore di ','uguale a '))+alltrim(STR(.w_PEIMPFAT,18,2)))
        .w_FILTRO2 = IIF(.w_PEOPERAR='N','',IIF(.w_PEOPERAR='M','maggiore uguale di ',IIF(.w_PEOPERAR='I','minore di ','uguale a '))+alltrim(STR(.w_PENUMFAT,4)))
        .w_FILTRO3 = 'allora verranno'+IIF(.w_PECONEST='I',' incluse ',' escluse ')+IIF(.w_PETIPEST='T','tutte le registrazioni','le registrazioni '+IIF(.w_PETIPEST='M','maggiori uguali di ',IIF(.w_PETIPEST='I','minori di ','uguale a '))+alltrim(STR(.w_PEIMPEST,18,2)))
        .w_LOG_FORMULA = IIF(.w_PETIPCON<>'N' Or .w_PEOPETRE<>'F' Or .w_PETIPPRE<>'F' Or Not Empty(.w_PECATCON) Or .w_PEFLPRIV='S' Or Not Empty(.w_PECONSUP),'La regola � valida per i '+IIF(.w_PETIPCON='C','clienti',IIF(.w_PETIPCON='F','fornitori','soggetti'))+IIF(.w_PEOPETRE='F','',CHR(13)+'con operazioni rilevanti uguali a '+IIF(.w_PEOPETRE='E','escludi',IIF(.w_PEOPETRE='P','corrispettivi periodici',IIF(.w_PEOPETRE='C','contratti collegati','non definibile'))))+IIF(.w_PETIPPRE='F','',Chr(13)+'tipologia prevalente uguale a '+IIF(.w_PETIPPRE='B','beni',IIF(.w_PETIPPRE='S','servizi','non definibile')))+IIF(Empty(.w_PECATCON),'',Chr(13)+'categoria contabile uguale a '+alltrim(.w_C2DESCRI))+IIF(.w_PEFLPRIV<>'S','',Chr(13)+'di tipo privato')+IIF(Empty(.w_PECONSUP),'',+Chr(13)+'con mastro uguale a '+alltrim(.w_MCDESCRI))+iif(Empty(.w_FILTRO1) And Empty(.w_FILTRO2),'',iif(Not Empty(.w_FILTRO1) And Not Empty(.w_FILTRO2),+Chr(13)+'a condizione che il totale importo dovuto sia '+alltrim(.w_FILTRO1)+' '+lower(.w_PECONDIZ)+Chr(13)+'il numero delle registrazioni sia '+alltrim(.w_FILTRO2)+Chr(13)+.w_FILTRO3,iif(Not Empty(.w_FILTRO1),+Chr(13)+'a condizione che il totale importo dovuto sia '+alltrim(.w_FILTRO1)+Chr(13)+.w_FILTRO3,+Chr(13)+'a condizione che il numero delle registrazioni sia '+alltrim(.w_FILTRO2)+Chr(13)+.w_FILTRO3))),'')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PE__ANNO=space(4)
      .w_PETIPCON=space(1)
      .w_TIPCON=space(1)
      .w_PEOPETRE=space(1)
      .w_PETIPPRE=space(1)
      .w_PECATCON=space(5)
      .w_PEFLPRIV=space(1)
      .w_PECONSUP=space(15)
      .w_PEREGATT=space(1)
      .w_PEOPERAT=space(1)
      .w_PEIMPFAT=0
      .w_PECONDIZ=space(1)
      .w_PEOPERAR=space(1)
      .w_PENUMFAT=0
      .w_PECONEST=space(1)
      .w_PETIPEST=space(1)
      .w_PEIMPEST=0
      .w_NULIV=0
      .w_TIPMAS=space(1)
      .w_FILTRO1=space(254)
      .w_FILTRO2=space(254)
      .w_FILTRO3=space(254)
      .w_LOG_FORMULA=space(0)
      .w_C2DESCRI=space(35)
      .w_MCDESCRI=space(40)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_PETIPCON = 'N'
        .w_TIPCON = .w_PETIPCON
        .w_PEOPETRE = "F"
        .w_PETIPPRE = 'F'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_PECATCON))
         .link_2_5('Full')
        endif
        .w_PEFLPRIV = ' '
        .w_PECONSUP = .w_PETIPCON
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PECONSUP))
         .link_2_7('Full')
        endif
        .w_PEREGATT = iif(.w_PETIPCON<>'N' Or .w_PEOPETRE<>'F' Or .w_PETIPPRE<>'F' Or Not Empty(.w_PECATCON) Or .w_PEFLPRIV='S' Or Not Empty(.w_PECONSUP),'S','N')
        .w_PEOPERAT = 'M'
        .w_PEIMPFAT = 0
        .w_PECONDIZ = 'E'
        .w_PEOPERAR = 'M'
        .w_PENUMFAT = 0
        .w_PECONEST = 'I'
        .w_PETIPEST = 'T'
        .w_PEIMPEST = IIF(.w_PETIPEST='T',0,.w_PEIMPEST)
        .DoRTCalc(18,19,.f.)
        .w_FILTRO1 = IIF(.w_PEOPERAT='N','',IIF(.w_PEOPERAT='M','maggiore uguale di ',IIF(.w_PEOPERAT='I','minore di ','uguale a '))+alltrim(STR(.w_PEIMPFAT,18,2)))
        .w_FILTRO2 = IIF(.w_PEOPERAR='N','',IIF(.w_PEOPERAR='M','maggiore uguale di ',IIF(.w_PEOPERAR='I','minore di ','uguale a '))+alltrim(STR(.w_PENUMFAT,4)))
        .w_FILTRO3 = 'allora verranno'+IIF(.w_PECONEST='I',' incluse ',' escluse ')+IIF(.w_PETIPEST='T','tutte le registrazioni','le registrazioni '+IIF(.w_PETIPEST='M','maggiori uguali di ',IIF(.w_PETIPEST='I','minori di ','uguale a '))+alltrim(STR(.w_PEIMPEST,18,2)))
        .w_LOG_FORMULA = IIF(.w_PETIPCON<>'N' Or .w_PEOPETRE<>'F' Or .w_PETIPPRE<>'F' Or Not Empty(.w_PECATCON) Or .w_PEFLPRIV='S' Or Not Empty(.w_PECONSUP),'La regola � valida per i '+IIF(.w_PETIPCON='C','clienti',IIF(.w_PETIPCON='F','fornitori','soggetti'))+IIF(.w_PEOPETRE='F','',CHR(13)+'con operazioni rilevanti uguali a '+IIF(.w_PEOPETRE='E','escludi',IIF(.w_PEOPETRE='P','corrispettivi periodici',IIF(.w_PEOPETRE='C','contratti collegati','non definibile'))))+IIF(.w_PETIPPRE='F','',Chr(13)+'tipologia prevalente uguale a '+IIF(.w_PETIPPRE='B','beni',IIF(.w_PETIPPRE='S','servizi','non definibile')))+IIF(Empty(.w_PECATCON),'',Chr(13)+'categoria contabile uguale a '+alltrim(.w_C2DESCRI))+IIF(.w_PEFLPRIV<>'S','',Chr(13)+'di tipo privato')+IIF(Empty(.w_PECONSUP),'',+Chr(13)+'con mastro uguale a '+alltrim(.w_MCDESCRI))+iif(Empty(.w_FILTRO1) And Empty(.w_FILTRO2),'',iif(Not Empty(.w_FILTRO1) And Not Empty(.w_FILTRO2),+Chr(13)+'a condizione che il totale importo dovuto sia '+alltrim(.w_FILTRO1)+' '+lower(.w_PECONDIZ)+Chr(13)+'il numero delle registrazioni sia '+alltrim(.w_FILTRO2)+Chr(13)+.w_FILTRO3,iif(Not Empty(.w_FILTRO1),+Chr(13)+'a condizione che il totale importo dovuto sia '+alltrim(.w_FILTRO1)+Chr(13)+.w_FILTRO3,+Chr(13)+'a condizione che il numero delle registrazioni sia '+alltrim(.w_FILTRO2)+Chr(13)+.w_FILTRO3))),'')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVDPC')
    this.DoRTCalc(24,25,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPEOPERAT_2_9.enabled = i_bVal
      .Page1.oPag.oPEIMPFAT_2_10.enabled = i_bVal
      .Page1.oPag.oPECONDIZ_2_11.enabled = i_bVal
      .Page1.oPag.oPEOPERAR_2_12.enabled = i_bVal
      .Page1.oPag.oPENUMFAT_2_13.enabled = i_bVal
      .Page1.oPag.oPETIPEST_2_15.enabled = i_bVal
      .Page1.oPag.oPEIMPEST_2_16.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ANTIVDPC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVDPC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PE__ANNO,"PE__ANNO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PETIPCON N(3);
      ,t_PEOPETRE N(3);
      ,t_PETIPPRE N(3);
      ,t_PECATCON C(5);
      ,t_PEFLPRIV N(3);
      ,t_PECONSUP C(15);
      ,t_PEREGATT N(3);
      ,t_PEOPERAT N(3);
      ,t_PEIMPFAT N(18,4);
      ,t_PECONDIZ N(3);
      ,t_PEOPERAR N(3);
      ,t_PENUMFAT N(4);
      ,t_PECONEST N(3);
      ,t_PETIPEST N(3);
      ,t_PEIMPEST N(18,4);
      ,t_LOG_FORMULA M(10);
      ,CPROWNUM N(10);
      ,t_TIPCON C(1);
      ,t_NULIV N(1);
      ,t_TIPMAS C(1);
      ,t_FILTRO1 C(254);
      ,t_FILTRO2 C(254);
      ,t_FILTRO3 C(254);
      ,t_C2DESCRI C(35);
      ,t_MCDESCRI C(40);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsai_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPCON_2_1.controlsource=this.cTrsName+'.t_PETIPCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPEOPETRE_2_3.controlsource=this.cTrsName+'.t_PEOPETRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPPRE_2_4.controlsource=this.cTrsName+'.t_PETIPPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCON_2_5.controlsource=this.cTrsName+'.t_PECATCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPEFLPRIV_2_6.controlsource=this.cTrsName+'.t_PEFLPRIV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECONSUP_2_7.controlsource=this.cTrsName+'.t_PECONSUP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPEREGATT_2_8.controlsource=this.cTrsName+'.t_PEREGATT'
    this.oPgFRm.Page1.oPag.oPEOPERAT_2_9.controlsource=this.cTrsName+'.t_PEOPERAT'
    this.oPgFRm.Page1.oPag.oPEIMPFAT_2_10.controlsource=this.cTrsName+'.t_PEIMPFAT'
    this.oPgFRm.Page1.oPag.oPECONDIZ_2_11.controlsource=this.cTrsName+'.t_PECONDIZ'
    this.oPgFRm.Page1.oPag.oPEOPERAR_2_12.controlsource=this.cTrsName+'.t_PEOPERAR'
    this.oPgFRm.Page1.oPag.oPENUMFAT_2_13.controlsource=this.cTrsName+'.t_PENUMFAT'
    this.oPgFRm.Page1.oPag.oPECONEST_2_14.controlsource=this.cTrsName+'.t_PECONEST'
    this.oPgFRm.Page1.oPag.oPETIPEST_2_15.controlsource=this.cTrsName+'.t_PETIPEST'
    this.oPgFRm.Page1.oPag.oPEIMPEST_2_16.controlsource=this.cTrsName+'.t_PEIMPEST'
    this.oPgFRm.Page1.oPag.oLOG_FORMULA_2_22.controlsource=this.cTrsName+'.t_LOG_FORMULA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(91)
    this.AddVLine(186)
    this.AddVLine(282)
    this.AddVLine(345)
    this.AddVLine(376)
    this.AddVLine(506)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPCON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVDPC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDPC_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVDPC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDPC_IDX,2])
      *
      * insert into ANTIVDPC
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVDPC')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVDPC')
        i_cFldBody=" "+;
                  "(PE__ANNO,PETIPCON,PEOPETRE,PETIPPRE,PECATCON"+;
                  ",PEFLPRIV,PECONSUP,PEREGATT,PEOPERAT,PEIMPFAT"+;
                  ",PECONDIZ,PEOPERAR,PENUMFAT,PECONEST,PETIPEST"+;
                  ",PEIMPEST,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PE__ANNO)+","+cp_ToStrODBC(this.w_PETIPCON)+","+cp_ToStrODBC(this.w_PEOPETRE)+","+cp_ToStrODBC(this.w_PETIPPRE)+","+cp_ToStrODBCNull(this.w_PECATCON)+;
             ","+cp_ToStrODBC(this.w_PEFLPRIV)+","+cp_ToStrODBCNull(this.w_PECONSUP)+","+cp_ToStrODBC(this.w_PEREGATT)+","+cp_ToStrODBC(this.w_PEOPERAT)+","+cp_ToStrODBC(this.w_PEIMPFAT)+;
             ","+cp_ToStrODBC(this.w_PECONDIZ)+","+cp_ToStrODBC(this.w_PEOPERAR)+","+cp_ToStrODBC(this.w_PENUMFAT)+","+cp_ToStrODBC(this.w_PECONEST)+","+cp_ToStrODBC(this.w_PETIPEST)+;
             ","+cp_ToStrODBC(this.w_PEIMPEST)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVDPC')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVDPC')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PE__ANNO',this.w_PE__ANNO)
        INSERT INTO (i_cTable) (;
                   PE__ANNO;
                  ,PETIPCON;
                  ,PEOPETRE;
                  ,PETIPPRE;
                  ,PECATCON;
                  ,PEFLPRIV;
                  ,PECONSUP;
                  ,PEREGATT;
                  ,PEOPERAT;
                  ,PEIMPFAT;
                  ,PECONDIZ;
                  ,PEOPERAR;
                  ,PENUMFAT;
                  ,PECONEST;
                  ,PETIPEST;
                  ,PEIMPEST;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PE__ANNO;
                  ,this.w_PETIPCON;
                  ,this.w_PEOPETRE;
                  ,this.w_PETIPPRE;
                  ,this.w_PECATCON;
                  ,this.w_PEFLPRIV;
                  ,this.w_PECONSUP;
                  ,this.w_PEREGATT;
                  ,this.w_PEOPERAT;
                  ,this.w_PEIMPFAT;
                  ,this.w_PECONDIZ;
                  ,this.w_PEOPERAR;
                  ,this.w_PENUMFAT;
                  ,this.w_PECONEST;
                  ,this.w_PETIPEST;
                  ,this.w_PEIMPEST;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ANTIVDPC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDPC_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (iif(vartype(t_PETIPCON)='N',iif(t_PETIPCON=1,'N','X'),t_PETIPCON)<>'N' Or iif(vartype(t_PEOPETRE)='N',iif(t_PEOPETRE=5,'F','X'),t_PEOPETRE)<>'F' Or iif(vartype(t_PETIPPRE)='N',iif(t_PETIPPRE=4,'F','X'),t_PETIPPRE)<>'F' Or Not Empty(t_PECATCON) Or iif(vartype(t_PEFLPRIV)='N',iif(t_PEFLPRIV=1,'S',' '),t_PEFLPRIV)='S' Or Not Empty(t_PECONSUP)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVDPC')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVDPC')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (iif(vartype(t_PETIPCON)='N',iif(t_PETIPCON=1,'N','X'),t_PETIPCON)<>'N' Or iif(vartype(t_PEOPETRE)='N',iif(t_PEOPETRE=5,'F','X'),t_PEOPETRE)<>'F' Or iif(vartype(t_PETIPPRE)='N',iif(t_PETIPPRE=4,'F','X'),t_PETIPPRE)<>'F' Or Not Empty(t_PECATCON) Or iif(vartype(t_PEFLPRIV)='N',iif(t_PEFLPRIV=1,'S',' '),t_PEFLPRIV)='S' Or Not Empty(t_PECONSUP)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ANTIVDPC
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVDPC')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PETIPCON="+cp_ToStrODBC(this.w_PETIPCON)+;
                     ",PEOPETRE="+cp_ToStrODBC(this.w_PEOPETRE)+;
                     ",PETIPPRE="+cp_ToStrODBC(this.w_PETIPPRE)+;
                     ",PECATCON="+cp_ToStrODBCNull(this.w_PECATCON)+;
                     ",PEFLPRIV="+cp_ToStrODBC(this.w_PEFLPRIV)+;
                     ",PECONSUP="+cp_ToStrODBCNull(this.w_PECONSUP)+;
                     ",PEREGATT="+cp_ToStrODBC(this.w_PEREGATT)+;
                     ",PEOPERAT="+cp_ToStrODBC(this.w_PEOPERAT)+;
                     ",PEIMPFAT="+cp_ToStrODBC(this.w_PEIMPFAT)+;
                     ",PECONDIZ="+cp_ToStrODBC(this.w_PECONDIZ)+;
                     ",PEOPERAR="+cp_ToStrODBC(this.w_PEOPERAR)+;
                     ",PENUMFAT="+cp_ToStrODBC(this.w_PENUMFAT)+;
                     ",PECONEST="+cp_ToStrODBC(this.w_PECONEST)+;
                     ",PETIPEST="+cp_ToStrODBC(this.w_PETIPEST)+;
                     ",PEIMPEST="+cp_ToStrODBC(this.w_PEIMPEST)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVDPC')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PETIPCON=this.w_PETIPCON;
                     ,PEOPETRE=this.w_PEOPETRE;
                     ,PETIPPRE=this.w_PETIPPRE;
                     ,PECATCON=this.w_PECATCON;
                     ,PEFLPRIV=this.w_PEFLPRIV;
                     ,PECONSUP=this.w_PECONSUP;
                     ,PEREGATT=this.w_PEREGATT;
                     ,PEOPERAT=this.w_PEOPERAT;
                     ,PEIMPFAT=this.w_PEIMPFAT;
                     ,PECONDIZ=this.w_PECONDIZ;
                     ,PEOPERAR=this.w_PEOPERAR;
                     ,PENUMFAT=this.w_PENUMFAT;
                     ,PECONEST=this.w_PECONEST;
                     ,PETIPEST=this.w_PETIPEST;
                     ,PEIMPEST=this.w_PEIMPEST;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVDPC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDPC_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (iif(vartype(t_PETIPCON)='N',iif(t_PETIPCON=1,'N','X'),t_PETIPCON)<>'N' Or iif(vartype(t_PEOPETRE)='N',iif(t_PEOPETRE=5,'F','X'),t_PEOPETRE)<>'F' Or iif(vartype(t_PETIPPRE)='N',iif(t_PETIPPRE=4,'F','X'),t_PETIPPRE)<>'F' Or Not Empty(t_PECATCON) Or iif(vartype(t_PEFLPRIV)='N',iif(t_PEFLPRIV=1,'S',' '),t_PEFLPRIV)='S' Or Not Empty(t_PECONSUP)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ANTIVDPC
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (iif(vartype(t_PETIPCON)='N',iif(t_PETIPCON=1,'N','X'),t_PETIPCON)<>'N' Or iif(vartype(t_PEOPETRE)='N',iif(t_PEOPETRE=5,'F','X'),t_PEOPETRE)<>'F' Or iif(vartype(t_PETIPPRE)='N',iif(t_PETIPPRE=4,'F','X'),t_PETIPPRE)<>'F' Or Not Empty(t_PECATCON) Or iif(vartype(t_PEFLPRIV)='N',iif(t_PEFLPRIV=1,'S',' '),t_PEFLPRIV)='S' Or Not Empty(t_PECONSUP)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVDPC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDPC_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_PETIPCON<>.w_PETIPCON
          .w_TIPCON = .w_PETIPCON
        endif
        .DoRTCalc(4,7,.t.)
        if .o_PETIPCON<>.w_PETIPCON
          .w_PECONSUP = .w_PETIPCON
          .link_2_7('Full')
        endif
        if .o_PETIPCON<>.w_PETIPCON.or. .o_PEOPETRE<>.w_PEOPETRE.or. .o_PETIPPRE<>.w_PETIPPRE.or. .o_PECATCON<>.w_PECATCON.or. .o_PEFLPRIV<>.w_PEFLPRIV.or. .o_PECONSUP<>.w_PECONSUP
          .w_PEREGATT = iif(.w_PETIPCON<>'N' Or .w_PEOPETRE<>'F' Or .w_PETIPPRE<>'F' Or Not Empty(.w_PECATCON) Or .w_PEFLPRIV='S' Or Not Empty(.w_PECONSUP),'S','N')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_PEOPERAT<>.w_PEOPERAT
          .w_PEIMPFAT = 0
        endif
        .DoRTCalc(12,13,.t.)
        if .o_PEOPERAR<>.w_PEOPERAR
          .w_PENUMFAT = 0
        endif
        .DoRTCalc(15,16,.t.)
        if .o_PETIPEST<>.w_PETIPEST
          .w_PEIMPEST = IIF(.w_PETIPEST='T',0,.w_PEIMPEST)
        endif
        .DoRTCalc(18,19,.t.)
        if .o_PEOPERAT<>.w_PEOPERAT.or. .o_PEIMPFAT<>.w_PEIMPFAT
          .w_FILTRO1 = IIF(.w_PEOPERAT='N','',IIF(.w_PEOPERAT='M','maggiore uguale di ',IIF(.w_PEOPERAT='I','minore di ','uguale a '))+alltrim(STR(.w_PEIMPFAT,18,2)))
        endif
        if .o_PEOPERAR<>.w_PEOPERAR.or. .o_PENUMFAT<>.w_PENUMFAT
          .w_FILTRO2 = IIF(.w_PEOPERAR='N','',IIF(.w_PEOPERAR='M','maggiore uguale di ',IIF(.w_PEOPERAR='I','minore di ','uguale a '))+alltrim(STR(.w_PENUMFAT,4)))
        endif
          .w_FILTRO3 = 'allora verranno'+IIF(.w_PECONEST='I',' incluse ',' escluse ')+IIF(.w_PETIPEST='T','tutte le registrazioni','le registrazioni '+IIF(.w_PETIPEST='M','maggiori uguali di ',IIF(.w_PETIPEST='I','minori di ','uguale a '))+alltrim(STR(.w_PEIMPEST,18,2)))
          .w_LOG_FORMULA = IIF(.w_PETIPCON<>'N' Or .w_PEOPETRE<>'F' Or .w_PETIPPRE<>'F' Or Not Empty(.w_PECATCON) Or .w_PEFLPRIV='S' Or Not Empty(.w_PECONSUP),'La regola � valida per i '+IIF(.w_PETIPCON='C','clienti',IIF(.w_PETIPCON='F','fornitori','soggetti'))+IIF(.w_PEOPETRE='F','',CHR(13)+'con operazioni rilevanti uguali a '+IIF(.w_PEOPETRE='E','escludi',IIF(.w_PEOPETRE='P','corrispettivi periodici',IIF(.w_PEOPETRE='C','contratti collegati','non definibile'))))+IIF(.w_PETIPPRE='F','',Chr(13)+'tipologia prevalente uguale a '+IIF(.w_PETIPPRE='B','beni',IIF(.w_PETIPPRE='S','servizi','non definibile')))+IIF(Empty(.w_PECATCON),'',Chr(13)+'categoria contabile uguale a '+alltrim(.w_C2DESCRI))+IIF(.w_PEFLPRIV<>'S','',Chr(13)+'di tipo privato')+IIF(Empty(.w_PECONSUP),'',+Chr(13)+'con mastro uguale a '+alltrim(.w_MCDESCRI))+iif(Empty(.w_FILTRO1) And Empty(.w_FILTRO2),'',iif(Not Empty(.w_FILTRO1) And Not Empty(.w_FILTRO2),+Chr(13)+'a condizione che il totale importo dovuto sia '+alltrim(.w_FILTRO1)+' '+lower(.w_PECONDIZ)+Chr(13)+'il numero delle registrazioni sia '+alltrim(.w_FILTRO2)+Chr(13)+.w_FILTRO3,iif(Not Empty(.w_FILTRO1),+Chr(13)+'a condizione che il totale importo dovuto sia '+alltrim(.w_FILTRO1)+Chr(13)+.w_FILTRO3,+Chr(13)+'a condizione che il numero delle registrazioni sia '+alltrim(.w_FILTRO2)+Chr(13)+.w_FILTRO3))),'')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(24,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TIPCON with this.w_TIPCON
      replace t_NULIV with this.w_NULIV
      replace t_TIPMAS with this.w_TIPMAS
      replace t_FILTRO1 with this.w_FILTRO1
      replace t_FILTRO2 with this.w_FILTRO2
      replace t_FILTRO3 with this.w_FILTRO3
      replace t_C2DESCRI with this.w_C2DESCRI
      replace t_MCDESCRI with this.w_MCDESCRI
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPEREGATT_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPEREGATT_2_8.mCond()
    this.oPgFrm.Page1.oPag.oPEIMPFAT_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPEIMPFAT_2_10.mCond()
    this.oPgFrm.Page1.oPag.oPENUMFAT_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPENUMFAT_2_13.mCond()
    this.oPgFrm.Page1.oPag.oPEIMPEST_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPEIMPEST_2_16.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oPECONEST_2_14.visible=!this.oPgFrm.Page1.oPag.oPECONEST_2_14.mHide()
    this.oPgFrm.Page1.oPag.oPETIPEST_2_15.visible=!this.oPgFrm.Page1.oPag.oPETIPEST_2_15.mHide()
    this.oPgFrm.Page1.oPag.oPEIMPEST_2_16.visible=!this.oPgFrm.Page1.oPag.oPEIMPEST_2_16.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PECATCON
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_PECATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_PECATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PECATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PECATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oPECATCON_2_5'),i_cWhere,'GSAR_AC2',"CATEGORIE CONTABILI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_PECATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_PECATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_C2DESCRI = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PECATCON = space(5)
      endif
      this.w_C2DESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.C2CODICE as C2CODICE205"+ ",link_2_5.C2DESCRI as C2DESCRI205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on ANTIVDPC.PECATCON=link_2_5.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and ANTIVDPC.PECATCON=link_2_5.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PECONSUP
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PECONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PECONSUP))
          select MCCODICE,MCNUMLIV,MCTIPMAS,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PECONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PECONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPECONSUP_2_7'),i_cWhere,'GSAR_AMC',"MASTRI CONTABILI ",'GSAI_MCO.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCNUMLIV,MCTIPMAS,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PECONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PECONSUP)
            select MCCODICE,MCNUMLIV,MCTIPMAS,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
      this.w_MCDESCRI = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PECONSUP = space(15)
      endif
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
      this.w_MCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND ((.w_TIPCON='N' AND (.w_TIPMAS='C' or .w_TIPMAS='F')) OR (.w_TIPCON=.w_TIPMAS))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PECONSUP = space(15)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
        this.w_MCDESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.MCCODICE as MCCODICE207"+ ",link_2_7.MCNUMLIV as MCNUMLIV207"+ ",link_2_7.MCTIPMAS as MCTIPMAS207"+ ",link_2_7.MCDESCRI as MCDESCRI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on ANTIVDPC.PECONSUP=link_2_7.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and ANTIVDPC.PECONSUP=link_2_7.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPEOPERAT_2_9.RadioValue()==this.w_PEOPERAT)
      this.oPgFrm.Page1.oPag.oPEOPERAT_2_9.SetRadio()
      replace t_PEOPERAT with this.oPgFrm.Page1.oPag.oPEOPERAT_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPEIMPFAT_2_10.value==this.w_PEIMPFAT)
      this.oPgFrm.Page1.oPag.oPEIMPFAT_2_10.value=this.w_PEIMPFAT
      replace t_PEIMPFAT with this.oPgFrm.Page1.oPag.oPEIMPFAT_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPECONDIZ_2_11.RadioValue()==this.w_PECONDIZ)
      this.oPgFrm.Page1.oPag.oPECONDIZ_2_11.SetRadio()
      replace t_PECONDIZ with this.oPgFrm.Page1.oPag.oPECONDIZ_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPEOPERAR_2_12.RadioValue()==this.w_PEOPERAR)
      this.oPgFrm.Page1.oPag.oPEOPERAR_2_12.SetRadio()
      replace t_PEOPERAR with this.oPgFrm.Page1.oPag.oPEOPERAR_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPENUMFAT_2_13.value==this.w_PENUMFAT)
      this.oPgFrm.Page1.oPag.oPENUMFAT_2_13.value=this.w_PENUMFAT
      replace t_PENUMFAT with this.oPgFrm.Page1.oPag.oPENUMFAT_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPECONEST_2_14.RadioValue()==this.w_PECONEST)
      this.oPgFrm.Page1.oPag.oPECONEST_2_14.SetRadio()
      replace t_PECONEST with this.oPgFrm.Page1.oPag.oPECONEST_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPETIPEST_2_15.RadioValue()==this.w_PETIPEST)
      this.oPgFrm.Page1.oPag.oPETIPEST_2_15.SetRadio()
      replace t_PETIPEST with this.oPgFrm.Page1.oPag.oPETIPEST_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPEIMPEST_2_16.value==this.w_PEIMPEST)
      this.oPgFrm.Page1.oPag.oPEIMPEST_2_16.value=this.w_PEIMPEST
      replace t_PEIMPEST with this.oPgFrm.Page1.oPag.oPEIMPEST_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLOG_FORMULA_2_22.value==this.w_LOG_FORMULA)
      this.oPgFrm.Page1.oPag.oLOG_FORMULA_2_22.value=this.w_LOG_FORMULA
      replace t_LOG_FORMULA with this.oPgFrm.Page1.oPag.oLOG_FORMULA_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPCON_2_1.RadioValue()==this.w_PETIPCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPCON_2_1.SetRadio()
      replace t_PETIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPCON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEOPETRE_2_3.RadioValue()==this.w_PEOPETRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEOPETRE_2_3.SetRadio()
      replace t_PEOPETRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEOPETRE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPPRE_2_4.RadioValue()==this.w_PETIPPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPPRE_2_4.SetRadio()
      replace t_PETIPPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPPRE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCON_2_5.value==this.w_PECATCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCON_2_5.value=this.w_PECATCON
      replace t_PECATCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCON_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEFLPRIV_2_6.RadioValue()==this.w_PEFLPRIV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEFLPRIV_2_6.SetRadio()
      replace t_PEFLPRIV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEFLPRIV_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONSUP_2_7.value==this.w_PECONSUP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONSUP_2_7.value=this.w_PECONSUP
      replace t_PECONSUP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONSUP_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEREGATT_2_8.RadioValue()==this.w_PEREGATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEREGATT_2_8.SetRadio()
      replace t_PEREGATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEREGATT_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVDPC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_NULIV=1 AND ((.w_TIPCON='N' AND (.w_TIPMAS='C' or .w_TIPMAS='F')) OR (.w_TIPCON=.w_TIPMAS))) and not(empty(.w_PECONSUP)) and (iif(vartype(.w_PETIPCON)='N',iif(.w_PETIPCON=1,'N','X'),.w_PETIPCON)<>'N' Or iif(vartype(.w_PEOPETRE)='N',iif(.w_PEOPETRE=5,'F','X'),.w_PEOPETRE)<>'F' Or iif(vartype(.w_PETIPPRE)='N',iif(.w_PETIPPRE=4,'F','X'),.w_PETIPPRE)<>'F' Or Not Empty(.w_PECATCON) Or iif(vartype(.w_PEFLPRIV)='N',iif(.w_PEFLPRIV=1,'S',' '),.w_PEFLPRIV)='S' Or Not Empty(.w_PECONSUP))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECONSUP_2_7
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if iif(vartype(.w_PETIPCON)='N',iif(.w_PETIPCON=1,'N','X'),.w_PETIPCON)<>'N' Or iif(vartype(.w_PEOPETRE)='N',iif(.w_PEOPETRE=5,'F','X'),.w_PEOPETRE)<>'F' Or iif(vartype(.w_PETIPPRE)='N',iif(.w_PETIPPRE=4,'F','X'),.w_PETIPPRE)<>'F' Or Not Empty(.w_PECATCON) Or iif(vartype(.w_PEFLPRIV)='N',iif(.w_PEFLPRIV=1,'S',' '),.w_PEFLPRIV)='S' Or Not Empty(.w_PECONSUP)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PETIPCON = this.w_PETIPCON
    this.o_PEOPETRE = this.w_PEOPETRE
    this.o_PETIPPRE = this.w_PETIPPRE
    this.o_PECATCON = this.w_PECATCON
    this.o_PEFLPRIV = this.w_PEFLPRIV
    this.o_PECONSUP = this.w_PECONSUP
    this.o_PEOPERAT = this.w_PEOPERAT
    this.o_PEIMPFAT = this.w_PEIMPFAT
    this.o_PEOPERAR = this.w_PEOPERAR
    this.o_PENUMFAT = this.w_PENUMFAT
    this.o_PETIPEST = this.w_PETIPEST
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(iif(vartype(t_PETIPCON)='N',iif(t_PETIPCON=1,'N','X'),t_PETIPCON)<>'N' Or iif(vartype(t_PEOPETRE)='N',iif(t_PEOPETRE=5,'F','X'),t_PEOPETRE)<>'F' Or iif(vartype(t_PETIPPRE)='N',iif(t_PETIPPRE=4,'F','X'),t_PETIPPRE)<>'F' Or Not Empty(t_PECATCON) Or iif(vartype(t_PEFLPRIV)='N',iif(t_PEFLPRIV=1,'S',' '),t_PEFLPRIV)='S' Or Not Empty(t_PECONSUP))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PETIPCON=space(1)
      .w_TIPCON=space(1)
      .w_PEOPETRE=space(1)
      .w_PETIPPRE=space(1)
      .w_PECATCON=space(5)
      .w_PEFLPRIV=space(1)
      .w_PECONSUP=space(15)
      .w_PEREGATT=space(1)
      .w_PEOPERAT=space(1)
      .w_PEIMPFAT=0
      .w_PECONDIZ=space(1)
      .w_PEOPERAR=space(1)
      .w_PENUMFAT=0
      .w_PECONEST=space(1)
      .w_PETIPEST=space(1)
      .w_PEIMPEST=0
      .w_NULIV=0
      .w_TIPMAS=space(1)
      .w_FILTRO1=space(254)
      .w_FILTRO2=space(254)
      .w_FILTRO3=space(254)
      .w_LOG_FORMULA=space(0)
      .w_C2DESCRI=space(35)
      .w_MCDESCRI=space(40)
      .DoRTCalc(1,1,.f.)
        .w_PETIPCON = 'N'
        .w_TIPCON = .w_PETIPCON
        .w_PEOPETRE = "F"
        .w_PETIPPRE = 'F'
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_PECATCON))
        .link_2_5('Full')
      endif
        .w_PEFLPRIV = ' '
        .w_PECONSUP = .w_PETIPCON
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_PECONSUP))
        .link_2_7('Full')
      endif
        .w_PEREGATT = iif(.w_PETIPCON<>'N' Or .w_PEOPETRE<>'F' Or .w_PETIPPRE<>'F' Or Not Empty(.w_PECATCON) Or .w_PEFLPRIV='S' Or Not Empty(.w_PECONSUP),'S','N')
        .w_PEOPERAT = 'M'
        .w_PEIMPFAT = 0
        .w_PECONDIZ = 'E'
        .w_PEOPERAR = 'M'
        .w_PENUMFAT = 0
        .w_PECONEST = 'I'
        .w_PETIPEST = 'T'
        .w_PEIMPEST = IIF(.w_PETIPEST='T',0,.w_PEIMPEST)
      .DoRTCalc(18,19,.f.)
        .w_FILTRO1 = IIF(.w_PEOPERAT='N','',IIF(.w_PEOPERAT='M','maggiore uguale di ',IIF(.w_PEOPERAT='I','minore di ','uguale a '))+alltrim(STR(.w_PEIMPFAT,18,2)))
        .w_FILTRO2 = IIF(.w_PEOPERAR='N','',IIF(.w_PEOPERAR='M','maggiore uguale di ',IIF(.w_PEOPERAR='I','minore di ','uguale a '))+alltrim(STR(.w_PENUMFAT,4)))
        .w_FILTRO3 = 'allora verranno'+IIF(.w_PECONEST='I',' incluse ',' escluse ')+IIF(.w_PETIPEST='T','tutte le registrazioni','le registrazioni '+IIF(.w_PETIPEST='M','maggiori uguali di ',IIF(.w_PETIPEST='I','minori di ','uguale a '))+alltrim(STR(.w_PEIMPEST,18,2)))
        .w_LOG_FORMULA = IIF(.w_PETIPCON<>'N' Or .w_PEOPETRE<>'F' Or .w_PETIPPRE<>'F' Or Not Empty(.w_PECATCON) Or .w_PEFLPRIV='S' Or Not Empty(.w_PECONSUP),'La regola � valida per i '+IIF(.w_PETIPCON='C','clienti',IIF(.w_PETIPCON='F','fornitori','soggetti'))+IIF(.w_PEOPETRE='F','',CHR(13)+'con operazioni rilevanti uguali a '+IIF(.w_PEOPETRE='E','escludi',IIF(.w_PEOPETRE='P','corrispettivi periodici',IIF(.w_PEOPETRE='C','contratti collegati','non definibile'))))+IIF(.w_PETIPPRE='F','',Chr(13)+'tipologia prevalente uguale a '+IIF(.w_PETIPPRE='B','beni',IIF(.w_PETIPPRE='S','servizi','non definibile')))+IIF(Empty(.w_PECATCON),'',Chr(13)+'categoria contabile uguale a '+alltrim(.w_C2DESCRI))+IIF(.w_PEFLPRIV<>'S','',Chr(13)+'di tipo privato')+IIF(Empty(.w_PECONSUP),'',+Chr(13)+'con mastro uguale a '+alltrim(.w_MCDESCRI))+iif(Empty(.w_FILTRO1) And Empty(.w_FILTRO2),'',iif(Not Empty(.w_FILTRO1) And Not Empty(.w_FILTRO2),+Chr(13)+'a condizione che il totale importo dovuto sia '+alltrim(.w_FILTRO1)+' '+lower(.w_PECONDIZ)+Chr(13)+'il numero delle registrazioni sia '+alltrim(.w_FILTRO2)+Chr(13)+.w_FILTRO3,iif(Not Empty(.w_FILTRO1),+Chr(13)+'a condizione che il totale importo dovuto sia '+alltrim(.w_FILTRO1)+Chr(13)+.w_FILTRO3,+Chr(13)+'a condizione che il numero delle registrazioni sia '+alltrim(.w_FILTRO2)+Chr(13)+.w_FILTRO3))),'')
    endwith
    this.DoRTCalc(24,25,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PETIPCON = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPCON_2_1.RadioValue(.t.)
    this.w_TIPCON = t_TIPCON
    this.w_PEOPETRE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEOPETRE_2_3.RadioValue(.t.)
    this.w_PETIPPRE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPPRE_2_4.RadioValue(.t.)
    this.w_PECATCON = t_PECATCON
    this.w_PEFLPRIV = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEFLPRIV_2_6.RadioValue(.t.)
    this.w_PECONSUP = t_PECONSUP
    this.w_PEREGATT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEREGATT_2_8.RadioValue(.t.)
    this.w_PEOPERAT = this.oPgFrm.Page1.oPag.oPEOPERAT_2_9.RadioValue(.t.)
    this.w_PEIMPFAT = t_PEIMPFAT
    this.w_PECONDIZ = this.oPgFrm.Page1.oPag.oPECONDIZ_2_11.RadioValue(.t.)
    this.w_PEOPERAR = this.oPgFrm.Page1.oPag.oPEOPERAR_2_12.RadioValue(.t.)
    this.w_PENUMFAT = t_PENUMFAT
    this.w_PECONEST = this.oPgFrm.Page1.oPag.oPECONEST_2_14.RadioValue(.t.)
    this.w_PETIPEST = this.oPgFrm.Page1.oPag.oPETIPEST_2_15.RadioValue(.t.)
    this.w_PEIMPEST = t_PEIMPEST
    this.w_NULIV = t_NULIV
    this.w_TIPMAS = t_TIPMAS
    this.w_FILTRO1 = t_FILTRO1
    this.w_FILTRO2 = t_FILTRO2
    this.w_FILTRO3 = t_FILTRO3
    this.w_LOG_FORMULA = t_LOG_FORMULA
    this.w_C2DESCRI = t_C2DESCRI
    this.w_MCDESCRI = t_MCDESCRI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PETIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPCON_2_1.ToRadio()
    replace t_TIPCON with this.w_TIPCON
    replace t_PEOPETRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEOPETRE_2_3.ToRadio()
    replace t_PETIPPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPETIPPRE_2_4.ToRadio()
    replace t_PECATCON with this.w_PECATCON
    replace t_PEFLPRIV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEFLPRIV_2_6.ToRadio()
    replace t_PECONSUP with this.w_PECONSUP
    replace t_PEREGATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEREGATT_2_8.ToRadio()
    replace t_PEOPERAT with this.oPgFrm.Page1.oPag.oPEOPERAT_2_9.ToRadio()
    replace t_PEIMPFAT with this.w_PEIMPFAT
    replace t_PECONDIZ with this.oPgFrm.Page1.oPag.oPECONDIZ_2_11.ToRadio()
    replace t_PEOPERAR with this.oPgFrm.Page1.oPag.oPEOPERAR_2_12.ToRadio()
    replace t_PENUMFAT with this.w_PENUMFAT
    replace t_PECONEST with this.oPgFrm.Page1.oPag.oPECONEST_2_14.ToRadio()
    replace t_PETIPEST with this.oPgFrm.Page1.oPag.oPETIPEST_2_15.ToRadio()
    replace t_PEIMPEST with this.w_PEIMPEST
    replace t_NULIV with this.w_NULIV
    replace t_TIPMAS with this.w_TIPMAS
    replace t_FILTRO1 with this.w_FILTRO1
    replace t_FILTRO2 with this.w_FILTRO2
    replace t_FILTRO3 with this.w_FILTRO3
    replace t_LOG_FORMULA with this.w_LOG_FORMULA
    replace t_C2DESCRI with this.w_C2DESCRI
    replace t_MCDESCRI with this.w_MCDESCRI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsai_mcoPag1 as StdContainer
  Width  = 789
  height = 517
  stdWidth  = 789
  stdheight = 517
  resizeXpos=387
  resizeYpos=469
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=2, width=538,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="PETIPCON",Label1="Tipo",Field2="PEOPETRE",Label2="Oper. rilev.",Field3="PETIPPRE",Label3="Tip. prev.",Field4="PECATCON",Label4="Cat. con.",Field5="PEFLPRIV",Label5="Priv.",Field6="PECONSUP",Label6="Mastro",Field7="PEREGATT",Label7="Attiva",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49470854

  add object oStr_1_2 as StdString with uid="PTEYATRJFJ",Visible=.t., Left=593, Top=166,;
    Alignment=0, Width=186, Height=18,;
    Caption="Se numero registrazioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="GITWWZLCUM",Visible=.t., Left=568, Top=29,;
    Alignment=0, Width=194, Height=19,;
    Caption="Condizione di filtro"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="BJXHNZHWNY",Visible=.t., Left=568, Top=255,;
    Alignment=0, Width=118, Height=19,;
    Caption="Allora"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_PEOPERAT='N' And .w_PEOPERAR='N')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="LOPNWHJZVM",Visible=.t., Left=593, Top=57,;
    Alignment=0, Width=186, Height=18,;
    Caption="Se totale importo dovuto"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="CMTOLQTLTY",Visible=.t., Left=593, Top=310,;
    Alignment=0, Width=186, Height=18,;
    Caption="le registrazioni"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_PEOPERAT='N' And .w_PEOPERAR='N')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=24,;
    width=534+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*19*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=25,width=533+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*19*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CACOCLFO|MASTRI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPEOPERAT_2_9.Refresh()
      this.Parent.oPEIMPFAT_2_10.Refresh()
      this.Parent.oPECONDIZ_2_11.Refresh()
      this.Parent.oPEOPERAR_2_12.Refresh()
      this.Parent.oPENUMFAT_2_13.Refresh()
      this.Parent.oPECONEST_2_14.Refresh()
      this.Parent.oPETIPEST_2_15.Refresh()
      this.Parent.oPEIMPEST_2_16.Refresh()
      this.Parent.oLOG_FORMULA_2_22.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CACOCLFO'
        oDropInto=this.oBodyCol.oRow.oPECATCON_2_5
      case cFile='MASTRI'
        oDropInto=this.oBodyCol.oRow.oPECONSUP_2_7
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPEOPERAT_2_9 as StdTrsCombo with uid="MJBAHVOOVI",rtrep=.t.,;
    cFormVar="w_PEOPERAT", RowSource=""+"Maggiore uguale di,"+"Minore di,"+"Uguale a,"+"Nessun filtro" , ;
    HelpContextID = 254069942,;
    Height=25, Width=136, Left=613, Top=79,;
    cTotal="", cQueryName = "PEOPERAT",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPEOPERAT_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PEOPERAT,&i_cF..t_PEOPERAT),this.value)
    return(iif(xVal =1,'M',;
    iif(xVal =2,'I',;
    iif(xVal =3,'U',;
    iif(xVal =4,'N',;
    space(1))))))
  endfunc
  func oPEOPERAT_2_9.GetRadio()
    this.Parent.oContained.w_PEOPERAT = this.RadioValue()
    return .t.
  endfunc

  func oPEOPERAT_2_9.ToRadio()
    this.Parent.oContained.w_PEOPERAT=trim(this.Parent.oContained.w_PEOPERAT)
    return(;
      iif(this.Parent.oContained.w_PEOPERAT=='M',1,;
      iif(this.Parent.oContained.w_PEOPERAT=='I',2,;
      iif(this.Parent.oContained.w_PEOPERAT=='U',3,;
      iif(this.Parent.oContained.w_PEOPERAT=='N',4,;
      0)))))
  endfunc

  func oPEOPERAT_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPEIMPFAT_2_10 as StdTrsField with uid="BCRVLZUACM",rtseq=11,rtrep=.t.,;
    cFormVar="w_PEIMPFAT",value=0,;
    ToolTipText = "Importo totale fatturato ( Imponibile + IVA )",;
    HelpContextID = 175647926,;
    cTotal="", bFixedPos=.t., cQueryName = "PEIMPFAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=613, Top=106, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oPEIMPFAT_2_10.mCond()
    with this.Parent.oContained
      return (.w_PEOPERAT $ 'IMU')
    endwith
  endfunc

  add object oPECONDIZ_2_11 as StdTrsCombo with uid="PFXFMTVXDH",rtrep=.t.,;
    cFormVar="w_PECONDIZ", RowSource=""+"e,"+"o" , ;
    HelpContextID = 57242448,;
    Height=25, Width=60, Left=613, Top=135,;
    cTotal="", cQueryName = "PECONDIZ",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPECONDIZ_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECONDIZ,&i_cF..t_PECONDIZ),this.value)
    return(iif(xVal =1,'E',;
    iif(xVal =2,'O',;
    space(1))))
  endfunc
  func oPECONDIZ_2_11.GetRadio()
    this.Parent.oContained.w_PECONDIZ = this.RadioValue()
    return .t.
  endfunc

  func oPECONDIZ_2_11.ToRadio()
    this.Parent.oContained.w_PECONDIZ=trim(this.Parent.oContained.w_PECONDIZ)
    return(;
      iif(this.Parent.oContained.w_PECONDIZ=='E',1,;
      iif(this.Parent.oContained.w_PECONDIZ=='O',2,;
      0)))
  endfunc

  func oPECONDIZ_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPEOPERAR_2_12 as StdTrsCombo with uid="ACIJZAIRFP",rtrep=.t.,;
    cFormVar="w_PEOPERAR", RowSource=""+"Maggiore uguale di,"+"Minore di,"+"Uguale a,"+"Nessun filtro" , ;
    HelpContextID = 254069944,;
    Height=25, Width=136, Left=613, Top=193,;
    cTotal="", cQueryName = "PEOPERAR",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPEOPERAR_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PEOPERAR,&i_cF..t_PEOPERAR),this.value)
    return(iif(xVal =1,'M',;
    iif(xVal =2,'I',;
    iif(xVal =3,'U',;
    iif(xVal =4,'N',;
    space(1))))))
  endfunc
  func oPEOPERAR_2_12.GetRadio()
    this.Parent.oContained.w_PEOPERAR = this.RadioValue()
    return .t.
  endfunc

  func oPEOPERAR_2_12.ToRadio()
    this.Parent.oContained.w_PEOPERAR=trim(this.Parent.oContained.w_PEOPERAR)
    return(;
      iif(this.Parent.oContained.w_PEOPERAR=='M',1,;
      iif(this.Parent.oContained.w_PEOPERAR=='I',2,;
      iif(this.Parent.oContained.w_PEOPERAR=='U',3,;
      iif(this.Parent.oContained.w_PEOPERAR=='N',4,;
      0)))))
  endfunc

  func oPEOPERAR_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPENUMFAT_2_13 as StdTrsField with uid="XSKNUNXFVA",rtseq=14,rtrep=.t.,;
    cFormVar="w_PENUMFAT",value=0,;
    HelpContextID = 178248886,;
    cTotal="", bFixedPos=.t., cQueryName = "PENUMFAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=613, Top=222, cSayPict=["9999"], cGetPict=["9999"]

  func oPENUMFAT_2_13.mCond()
    with this.Parent.oContained
      return (.w_PEOPERAR $ 'IMU')
    endwith
  endfunc

  add object oPECONEST_2_14 as StdTrsCombo with uid="PCVHBEMKGB",rtrep=.t.,;
    cFormVar="w_PECONEST", RowSource=""+"Includi,"+"Escludi" , enabled=.f.,;
    HelpContextID = 194415798,;
    Height=25, Width=129, Left=613, Top=280,;
    cTotal="", cQueryName = "PECONEST",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPECONEST_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PECONEST,&i_cF..t_PECONEST),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'E',;
    space(1))))
  endfunc
  func oPECONEST_2_14.GetRadio()
    this.Parent.oContained.w_PECONEST = this.RadioValue()
    return .t.
  endfunc

  func oPECONEST_2_14.ToRadio()
    this.Parent.oContained.w_PECONEST=trim(this.Parent.oContained.w_PECONEST)
    return(;
      iif(this.Parent.oContained.w_PECONEST=='I',1,;
      iif(this.Parent.oContained.w_PECONEST=='E',2,;
      0)))
  endfunc

  func oPECONEST_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPECONEST_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PEOPERAT='N' And .w_PEOPERAR='N')
    endwith
    endif
  endfunc

  add object oPETIPEST_2_15 as StdTrsCombo with uid="WZDWLMODXG",rtrep=.t.,;
    cFormVar="w_PETIPEST", RowSource=""+"Tutte,"+"Maggiore uguale di,"+"Minore di,"+"Uguale a" , ;
    HelpContextID = 192642230,;
    Height=25, Width=136, Left=613, Top=335,;
    cTotal="", cQueryName = "PETIPEST",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPETIPEST_2_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PETIPEST,&i_cF..t_PETIPEST),this.value)
    return(iif(xVal =1,'T',;
    iif(xVal =2,'M',;
    iif(xVal =3,'I',;
    iif(xVal =4,'U',;
    space(1))))))
  endfunc
  func oPETIPEST_2_15.GetRadio()
    this.Parent.oContained.w_PETIPEST = this.RadioValue()
    return .t.
  endfunc

  func oPETIPEST_2_15.ToRadio()
    this.Parent.oContained.w_PETIPEST=trim(this.Parent.oContained.w_PETIPEST)
    return(;
      iif(this.Parent.oContained.w_PETIPEST=='T',1,;
      iif(this.Parent.oContained.w_PETIPEST=='M',2,;
      iif(this.Parent.oContained.w_PETIPEST=='I',3,;
      iif(this.Parent.oContained.w_PETIPEST=='U',4,;
      0)))))
  endfunc

  func oPETIPEST_2_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPETIPEST_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PEOPERAT='N' And .w_PEOPERAR='N')
    endwith
    endif
  endfunc

  add object oPEIMPEST_2_16 as StdTrsField with uid="CDZLEHAKOW",rtseq=17,rtrep=.t.,;
    cFormVar="w_PEIMPEST",value=0,;
    HelpContextID = 192425142,;
    cTotal="", bFixedPos=.t., cQueryName = "PEIMPEST",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=613, Top=369, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oPEIMPEST_2_16.mCond()
    with this.Parent.oContained
      return (.w_PETIPEST $ 'MIU')
    endwith
  endfunc

  func oPEIMPEST_2_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PEOPERAT='N' And .w_PEOPERAR='N')
    endwith
    endif
  endfunc

  add object oLOG_FORMULA_2_22 as StdTrsMemo with uid="OXTJFOLZEZ",rtseq=23,rtrep=.t.,;
    cFormVar="w_LOG_FORMULA",value=space(0),enabled=.f.,;
    HelpContextID = 33677741,;
    cTotal="", bFixedPos=.t., cQueryName = "LOG_FORMULA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=120, Width=544, Left=2, Top=392

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsai_mcoBodyRow as CPBodyRowCnt
  Width=524
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPETIPCON_2_1 as StdTrsCombo with uid="ZLNYHULNFT",rtrep=.t.,;
    cFormVar="w_PETIPCON", RowSource=""+"Ness. filtro,"+"Clienti,"+"Fornitori" , ;
    HelpContextID = 226196668,;
    Height=21, Width=81, Left=-2, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPETIPCON_2_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PETIPCON,&i_cF..t_PETIPCON),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'C',;
    iif(xVal =3,'F',;
    space(1)))))
  endfunc
  func oPETIPCON_2_1.GetRadio()
    this.Parent.oContained.w_PETIPCON = this.RadioValue()
    return .t.
  endfunc

  func oPETIPCON_2_1.ToRadio()
    this.Parent.oContained.w_PETIPCON=trim(this.Parent.oContained.w_PETIPCON)
    return(;
      iif(this.Parent.oContained.w_PETIPCON=='N',1,;
      iif(this.Parent.oContained.w_PETIPCON=='C',2,;
      iif(this.Parent.oContained.w_PETIPCON=='F',3,;
      0))))
  endfunc

  func oPETIPCON_2_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPEOPETRE_2_3 as StdTrsCombo with uid="AFCZBNBKKO",rtrep=.t.,;
    cFormVar="w_PEOPETRE", RowSource=""+"Escludi,"+"Corr. period.,"+"Contr. coll.,"+"Non def.,"+"Ness. filtro" , ;
    ToolTipText = "Operazioni rilevanti IVA",;
    HelpContextID = 220515525,;
    Height=21, Width=89, Left=84, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPEOPETRE_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PEOPETRE,&i_cF..t_PEOPETRE),this.value)
    return(iif(xVal =1,'E',;
    iif(xVal =2,'P',;
    iif(xVal =3,'C',;
    iif(xVal =4,'N',;
    iif(xVal =5,'F',;
    space(1)))))))
  endfunc
  func oPEOPETRE_2_3.GetRadio()
    this.Parent.oContained.w_PEOPETRE = this.RadioValue()
    return .t.
  endfunc

  func oPEOPETRE_2_3.ToRadio()
    this.Parent.oContained.w_PEOPETRE=trim(this.Parent.oContained.w_PEOPETRE)
    return(;
      iif(this.Parent.oContained.w_PEOPETRE=='E',1,;
      iif(this.Parent.oContained.w_PEOPETRE=='P',2,;
      iif(this.Parent.oContained.w_PEOPETRE=='C',3,;
      iif(this.Parent.oContained.w_PEOPETRE=='N',4,;
      iif(this.Parent.oContained.w_PEOPETRE=='F',5,;
      0))))))
  endfunc

  func oPEOPETRE_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPETIPPRE_2_4 as StdTrsCombo with uid="LDTMERYTVS",rtrep=.t.,;
    cFormVar="w_PETIPPRE", RowSource=""+"Non def.,"+"Beni,"+"Servizi,"+"Ness. filtro" , ;
    ToolTipText = "Tipologia prevalente",;
    HelpContextID = 8092869,;
    Height=21, Width=87, Left=180, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPETIPPRE_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PETIPPRE,&i_cF..t_PETIPPRE),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'B',;
    iif(xVal =3,'S',;
    iif(xVal =4,'F',;
    space(1))))))
  endfunc
  func oPETIPPRE_2_4.GetRadio()
    this.Parent.oContained.w_PETIPPRE = this.RadioValue()
    return .t.
  endfunc

  func oPETIPPRE_2_4.ToRadio()
    this.Parent.oContained.w_PETIPPRE=trim(this.Parent.oContained.w_PETIPPRE)
    return(;
      iif(this.Parent.oContained.w_PETIPPRE=='N',1,;
      iif(this.Parent.oContained.w_PETIPPRE=='B',2,;
      iif(this.Parent.oContained.w_PETIPPRE=='S',3,;
      iif(this.Parent.oContained.w_PETIPPRE=='F',4,;
      0)))))
  endfunc

  func oPETIPPRE_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPECATCON_2_5 as StdTrsField with uid="XNWCFBKYQZ",rtseq=6,rtrep=.t.,;
    cFormVar="w_PECATCON",value=space(5),;
    HelpContextID = 222596284,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=277, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_PECATCON"

  func oPECATCON_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECATCON_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECATCON_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oPECATCON_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"CATEGORIE CONTABILI",'',this.parent.oContained
  endproc
  proc oPECATCON_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_PECATCON
    i_obj.ecpSave()
  endproc

  add object oPEFLPRIV_2_6 as StdTrsCheck with uid="GNEFQEYRUF",rtrep=.t.,;
    cFormVar="w_PEFLPRIV",  caption="",;
    ToolTipText = "Flag cliente privato",;
    HelpContextID = 25600844,;
    Left=339, Top=0, Width=24,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oPEFLPRIV_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PEFLPRIV,&i_cF..t_PEFLPRIV),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oPEFLPRIV_2_6.GetRadio()
    this.Parent.oContained.w_PEFLPRIV = this.RadioValue()
    return .t.
  endfunc

  func oPEFLPRIV_2_6.ToRadio()
    this.Parent.oContained.w_PEFLPRIV=trim(this.Parent.oContained.w_PEFLPRIV)
    return(;
      iif(this.Parent.oContained.w_PEFLPRIV=='S',1,;
      0))
  endfunc

  func oPEFLPRIV_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPECONSUP_2_7 as StdTrsField with uid="VCUFEYECNG",rtseq=8,rtrep=.t.,;
    cFormVar="w_PECONSUP",value=space(15),;
    HelpContextID = 40465222,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=369, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PECONSUP"

  func oPECONSUP_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECONSUP_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECONSUP_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPECONSUP_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"MASTRI CONTABILI ",'GSAI_MCO.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPECONSUP_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PECONSUP
    i_obj.ecpSave()
  endproc

  add object oPEREGATT_2_8 as StdTrsCheck with uid="RDQJDPPDIP",rtrep=.t.,;
    cFormVar="w_PEREGATT",  caption="",;
    ToolTipText = "Regola attiva",;
    HelpContextID = 1023158,;
    Left=499, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oPEREGATT_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PEREGATT,&i_cF..t_PEREGATT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPEREGATT_2_8.GetRadio()
    this.Parent.oContained.w_PEREGATT = this.RadioValue()
    return .t.
  endfunc

  func oPEREGATT_2_8.ToRadio()
    this.Parent.oContained.w_PEREGATT=trim(this.Parent.oContained.w_PEREGATT)
    return(;
      iif(this.Parent.oContained.w_PEREGATT=='S',1,;
      0))
  endfunc

  func oPEREGATT_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPEREGATT_2_8.mCond()
    with this.Parent.oContained
      return (.w_PETIPCON<>'N' Or .w_PEOPETRE<>'F' Or .w_PETIPPRE<>'F' Or Not Empty(.w_PECATCON) Or .w_PEFLPRIV='S' Or Not Empty(.w_PECONSUP))
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPETIPCON_2_1.When()
    return(.t.)
  proc oPETIPCON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPETIPCON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=18
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_mco','ANTIVDPC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PE__ANNO=ANTIVDPC.PE__ANNO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
