* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_ked                                                        *
*              Estrazione dati                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-23                                                      *
* Last revis.: 2010-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_ked",oParentObject))

* --- Class definition
define class tgsai_ked as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 456
  Height = 348
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-10-28"
  HelpContextID=65668969
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  ANTIVAPA_IDX = 0
  cPrg = "gsai_ked"
  cComment = "Estrazione dati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(10)
  w_PAPERIOD = space(1)
  w_PERIOD = space(1)
  o_PERIOD = space(1)
  w_AIVARPER = space(1)
  w_ANNO = 0
  o_ANNO = 0
  w_MESE = 0
  o_MESE = 0
  w_TRIMES = 0
  o_TRIMES = 0
  w_DADATA = ctod('  /  /  ')
  w_ADATA = ctod('  /  /  ')
  w_PAESCNVA = space(1)
  w_ELIDATI = space(1)
  w_PADATINI = ctod('  /  /  ')
  w_Msg = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_kedPag1","gsai_ked",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPERIOD_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ANTIVAPA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAI_BED(this,"GENERA")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(10)
      .w_PAPERIOD=space(1)
      .w_PERIOD=space(1)
      .w_AIVARPER=space(1)
      .w_ANNO=0
      .w_MESE=0
      .w_TRIMES=0
      .w_DADATA=ctod("  /  /  ")
      .w_ADATA=ctod("  /  /  ")
      .w_PAESCNVA=space(1)
      .w_ELIDATI=space(1)
      .w_PADATINI=ctod("  /  /  ")
      .w_Msg=space(0)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_PERIOD = iif(EMPTY(NVL(.w_PAPERIOD,' ')), 'M', .w_PAPERIOD)
        .w_AIVARPER = ' '
        .w_ANNO = year(i_DATSYS)
        .w_MESE = iif(NVL(.w_PERIOD,'M')='M', month(i_DATSYS), 0)
        .w_TRIMES = iif(NVL(.w_PERIOD,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        .w_DADATA = iif((.w_MESE<>0 OR .w_TRIMES<>0) AND .w_ANNO<>0,iif(.w_PERIOD='M', DATE(.w_ANNO, .w_MESE, 1), DATE(.w_ANNO, (.w_TRIMES-1)*3+1, 1)),CTOD('  -  -    '))
        .w_ADATA = iif((.w_MESE<>0 OR .w_TRIMES<>0) AND .w_ANNO<>0,iif(.w_PERIOD='M', DATE(YEAR(.w_DADATA + 31), MONTH(.w_DADATA + 31), 1), DATE(YEAR(.w_DADATA + 93), MONTH(.w_DADATA + 93), 1))-1,CTOD('  -  -    '))
    endwith
    this.DoRTCalc(10,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
        if .o_PERIOD<>.w_PERIOD
            .w_AIVARPER = ' '
        endif
        .DoRTCalc(5,5,.t.)
        if .o_PERIOD<>.w_PERIOD
            .w_MESE = iif(NVL(.w_PERIOD,'M')='M', month(i_DATSYS), 0)
        endif
        if .o_PERIOD<>.w_PERIOD
            .w_TRIMES = iif(NVL(.w_PERIOD,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        endif
        if .o_ANNO<>.w_ANNO.or. .o_MESE<>.w_MESE.or. .o_PERIOD<>.w_PERIOD.or. .o_TRIMES<>.w_TRIMES
            .w_DADATA = iif((.w_MESE<>0 OR .w_TRIMES<>0) AND .w_ANNO<>0,iif(.w_PERIOD='M', DATE(.w_ANNO, .w_MESE, 1), DATE(.w_ANNO, (.w_TRIMES-1)*3+1, 1)),CTOD('  -  -    '))
        endif
        if .o_ANNO<>.w_ANNO.or. .o_MESE<>.w_MESE.or. .o_PERIOD<>.w_PERIOD.or. .o_TRIMES<>.w_TRIMES
            .w_ADATA = iif((.w_MESE<>0 OR .w_TRIMES<>0) AND .w_ANNO<>0,iif(.w_PERIOD='M', DATE(YEAR(.w_DADATA + 31), MONTH(.w_DADATA + 31), 1), DATE(YEAR(.w_DADATA + 93), MONTH(.w_DADATA + 93), 1))-1,CTOD('  -  -    '))
        endif
        if .o_ANNO<>.w_ANNO.or. .o_PERIOD<>.w_PERIOD
          .Calculate_SMYYCIVSJA()
        endif
        if .o_MESE<>.w_MESE.or. .o_TRIMES<>.w_TRIMES
          .Calculate_ZGVVYFTIUV()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_SMYYCIVSJA()
    with this
          * --- Calcolo periodo di estrazione
          GSAI_BCP(this;
              ,'PER';
             )
    endwith
  endproc
  proc Calculate_ZGVVYFTIUV()
    with this
          * --- Controllo se periodo gi� estratto
          GSAI_BCP(this;
              ,'CHK';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oADATA_1_13.enabled = this.oPgFrm.Page1.oPag.oADATA_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAIVARPER_1_4.visible=!this.oPgFrm.Page1.oPag.oAIVARPER_1_4.mHide()
    this.oPgFrm.Page1.oPag.oMESE_1_6.visible=!this.oPgFrm.Page1.oPag.oMESE_1_6.mHide()
    this.oPgFrm.Page1.oPag.oTRIMES_1_7.visible=!this.oPgFrm.Page1.oPag.oTRIMES_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_SMYYCIVSJA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_ZGVVYFTIUV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANTIVAPA_IDX,3]
    i_lTable = "ANTIVAPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2], .t., this.ANTIVAPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAPERIOD,PAESCNVA,PADATINI";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_CODAZI)
            select PACODAZI,PAPERIOD,PAESCNVA,PADATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PACODAZI,space(10))
      this.w_PAPERIOD = NVL(_Link_.PAPERIOD,space(1))
      this.w_PAESCNVA = NVL(_Link_.PAESCNVA,space(1))
      this.w_PADATINI = NVL(cp_ToDate(_Link_.PADATINI),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_PAPERIOD = space(1)
      this.w_PAESCNVA = space(1)
      this.w_PADATINI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.ANTIVAPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPERIOD_1_3.RadioValue()==this.w_PERIOD)
      this.oPgFrm.Page1.oPag.oPERIOD_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIVARPER_1_4.RadioValue()==this.w_AIVARPER)
      this.oPgFrm.Page1.oPag.oAIVARPER_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_5.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_5.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE_1_6.value==this.w_MESE)
      this.oPgFrm.Page1.oPag.oMESE_1_6.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIMES_1_7.value==this.w_TRIMES)
      this.oPgFrm.Page1.oPag.oTRIMES_1_7.value=this.w_TRIMES
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_11.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_11.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oADATA_1_13.value==this.w_ADATA)
      this.oPgFrm.Page1.oPag.oADATA_1_13.value=this.w_ADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oELIDATI_1_15.RadioValue()==this.w_ELIDATI)
      this.oPgFrm.Page1.oPag.oELIDATI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_23.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_23.value=this.w_Msg
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_MESE>=1 AND .w_MESE<=12)  and not(.w_PERIOD='T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESE_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TRIMES>=1 AND .w_TRIMES<=4)  and not(.w_PERIOD='M')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRIMES_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PERIOD = this.w_PERIOD
    this.o_ANNO = this.w_ANNO
    this.o_MESE = this.w_MESE
    this.o_TRIMES = this.w_TRIMES
    return

enddefine

* --- Define pages as container
define class tgsai_kedPag1 as StdContainer
  Width  = 452
  height = 348
  stdWidth  = 452
  stdheight = 348
  resizeXpos=117
  resizeYpos=280
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPERIOD_1_3 as StdRadio with uid="BZNWQVRBTP",rtseq=3,rtrep=.f.,left=164, top=8, width=98,height=36;
    , ToolTipText = "Periodicit� adempimento";
    , cFormVar="w_PERIOD", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPERIOD_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Mensile"
      this.Buttons(1).HelpContextID = 179019018
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Trimestrale"
      this.Buttons(2).HelpContextID = 179019018
      this.Buttons(2).Top=17
      this.SetAll("Width",96)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Periodicit� adempimento")
      StdRadio::init()
    endproc

  func oPERIOD_1_3.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oPERIOD_1_3.GetRadio()
    this.Parent.oContained.w_PERIOD = this.RadioValue()
    return .t.
  endfunc

  func oPERIOD_1_3.SetRadio()
    this.Parent.oContained.w_PERIOD=trim(this.Parent.oContained.w_PERIOD)
    this.value = ;
      iif(this.Parent.oContained.w_PERIOD=='M',1,;
      iif(this.Parent.oContained.w_PERIOD=='T',2,;
      0))
  endfunc

  add object oAIVARPER_1_4 as StdCheck with uid="SHCXJVPCYR",rtseq=4,rtrep=.f.,left=269, top=8, caption="Variazione di periodicit�",;
    ToolTipText = "Variazione di periodicit�",;
    HelpContextID = 24946264,;
    cFormVar="w_AIVARPER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAIVARPER_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAIVARPER_1_4.GetRadio()
    this.Parent.oContained.w_AIVARPER = this.RadioValue()
    return .t.
  endfunc

  func oAIVARPER_1_4.SetRadio()
    this.Parent.oContained.w_AIVARPER=trim(this.Parent.oContained.w_AIVARPER)
    this.value = ;
      iif(this.Parent.oContained.w_AIVARPER=='S',1,;
      0)
  endfunc

  func oAIVARPER_1_4.mHide()
    with this.Parent.oContained
      return (.w_PERIOD<>'T')
    endwith
  endfunc

  add object oANNO_1_5 as StdField with uid="SZFWRROVJC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di estrazione",;
    HelpContextID = 60151034,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=159, Top=49, cSayPict='"9999"', cGetPict='"9999"'

  add object oMESE_1_6 as StdField with uid="JVLRLQLKWY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese/trimestre di estrazione",;
    HelpContextID = 60788026,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=223, Top=49

  func oMESE_1_6.mHide()
    with this.Parent.oContained
      return (.w_PERIOD='T')
    endwith
  endfunc

  func oMESE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MESE>=1 AND .w_MESE<=12)
    endwith
    return bRes
  endfunc

  add object oTRIMES_1_7 as StdField with uid="MIWUJVBIXP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TRIMES", cQueryName = "TRIMES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Trimestre di estrazione",;
    HelpContextID = 62382134,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=223, Top=49

  func oTRIMES_1_7.mHide()
    with this.Parent.oContained
      return (.w_PERIOD='M')
    endwith
  endfunc

  func oTRIMES_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TRIMES>=1 AND .w_TRIMES<=4)
    endwith
    return bRes
  endfunc

  add object oDADATA_1_11 as StdField with uid="RBDPFVLVNM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio estrazione",;
    HelpContextID = 224690634,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=159, Top=78

  add object oADATA_1_13 as StdField with uid="HKRYLDSXIM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ADATA", cQueryName = "ADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine estrazione",;
    HelpContextID = 260157178,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=305, Top=78

  func oADATA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIVARPER='S')
    endwith
   endif
  endfunc

  add object oELIDATI_1_15 as StdCheck with uid="NXPSMGHBLD",rtseq=11,rtrep=.f.,left=159, top=110, caption="Elimina dati gi� estratti", enabled=.f.,;
    ToolTipText = "Elimina dati gi� estratti per il periodo",;
    HelpContextID = 194062010,;
    cFormVar="w_ELIDATI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELIDATI_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oELIDATI_1_15.GetRadio()
    this.Parent.oContained.w_ELIDATI = this.RadioValue()
    return .t.
  endfunc

  func oELIDATI_1_15.SetRadio()
    this.Parent.oContained.w_ELIDATI=trim(this.Parent.oContained.w_ELIDATI)
    this.value = ;
      iif(this.Parent.oContained.w_ELIDATI=='S',1,;
      0)
  endfunc


  add object oBtn_1_16 as StdButton with uid="MKILKUTQNH",left=283, top=151, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i dati anagrafici";
    , HelpContextID = 91608167;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSAI_BED(this.Parent.oContained,"UPDATE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="ZMIZZNTUDJ",left=339, top=151, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per estrarre i dati";
    , HelpContextID = 10513898;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSAI_BED(this.Parent.oContained,"GENERA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="JWIBHYOYEX",left=395, top=151, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58351546;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMsg_1_23 as StdMemo with uid="ATBBCHAJSX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 65216314,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=147, Width=443, Left=3, Top=196, tabstop = .f., readonly = .t.

  add object oStr_1_8 as StdString with uid="TXOFEDZUGY",Visible=.t., Left=34, Top=51,;
    Alignment=1, Width=119, Height=18,;
    Caption="Periodo di estrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ONOZZAJCNN",Visible=.t., Left=213, Top=49,;
    Alignment=0, Width=8, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="KTXAMEUHPD",Visible=.t., Left=107, Top=80,;
    Alignment=1, Width=46, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GGDPMRZOJW",Visible=.t., Left=250, Top=80,;
    Alignment=1, Width=46, Height=18,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TOHZLQGVNJ",Visible=.t., Left=34, Top=8,;
    Alignment=1, Width=119, Height=18,;
    Caption="Periodicit�:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_ked','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
