* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_mpi                                                        *
*              Dettagli parametri eccezioni codici IVA                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-25                                                      *
* Last revis.: 2012-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsai_mpi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsai_mpi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsai_mpi")
  return

* --- Class definition
define class tgsai_mpi as StdPCForm
  Width  = 403
  Height = 309
  Top    = 10
  Left   = 10
  cComment = "Dettagli parametri eccezioni codici IVA"
  cPrg = "gsai_mpi"
  HelpContextID=120977559
  add object cnt as tcgsai_mpi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsai_mpi as PCContext
  w_PACODAZI = space(5)
  w_PACODIVA = space(5)
  w_PACODICE = space(5)
  w_XXCODICE = space(5)
  w_PATIPOPE = 0
  w_IVDESIVA = space(35)
  w_CCDESCRI = space(35)
  w_OBTEST = space(8)
  w_IVFLGSER = space(1)
  w_CCFLRIFE = space(1)
  proc Save(i_oFrom)
    this.w_PACODAZI = i_oFrom.w_PACODAZI
    this.w_PACODIVA = i_oFrom.w_PACODIVA
    this.w_PACODICE = i_oFrom.w_PACODICE
    this.w_XXCODICE = i_oFrom.w_XXCODICE
    this.w_PATIPOPE = i_oFrom.w_PATIPOPE
    this.w_IVDESIVA = i_oFrom.w_IVDESIVA
    this.w_CCDESCRI = i_oFrom.w_CCDESCRI
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_IVFLGSER = i_oFrom.w_IVFLGSER
    this.w_CCFLRIFE = i_oFrom.w_CCFLRIFE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PACODAZI = this.w_PACODAZI
    i_oTo.w_PACODIVA = this.w_PACODIVA
    i_oTo.w_PACODICE = this.w_PACODICE
    i_oTo.w_XXCODICE = this.w_XXCODICE
    i_oTo.w_PATIPOPE = this.w_PATIPOPE
    i_oTo.w_IVDESIVA = this.w_IVDESIVA
    i_oTo.w_CCDESCRI = this.w_CCDESCRI
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_IVFLGSER = this.w_IVFLGSER
    i_oTo.w_CCFLRIFE = this.w_CCFLRIFE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsai_mpi as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 403
  Height = 309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-27"
  HelpContextID=120977559
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ANTIVAPI_IDX = 0
  VOCIIVA_IDX = 0
  CAU_CONT_IDX = 0
  cFile = "ANTIVAPI"
  cKeySelect = "PACODAZI"
  cKeyWhere  = "PACODAZI=this.w_PACODAZI"
  cKeyDetail  = "PACODAZI=this.w_PACODAZI and PACODIVA=this.w_PACODIVA and PACODICE=this.w_PACODICE"
  cKeyWhereODBC = '"PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cKeyDetailWhereODBC = '"PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';
      +'+" and PACODIVA="+cp_ToStrODBC(this.w_PACODIVA)';
      +'+" and PACODICE="+cp_ToStrODBC(this.w_PACODICE)';

  cKeyWhereODBCqualified = '"ANTIVAPI.PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ANTIVAPI.PACODIVA,ANTIVAPI.PACODICE'
  cPrg = "gsai_mpi"
  cComment = "Dettagli parametri eccezioni codici IVA"
  i_nRowNum = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PACODAZI = space(5)
  w_PACODIVA = space(5)
  o_PACODIVA = space(5)
  w_PACODICE = space(5)
  o_PACODICE = space(5)
  w_XXCODICE = space(5)
  o_XXCODICE = space(5)
  w_PATIPOPE = 0
  w_IVDESIVA = space(35)
  w_CCDESCRI = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_IVFLGSER = space(1)
  w_CCFLRIFE = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_mpiPag1","gsai_mpi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='ANTIVAPI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVAPI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVAPI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsai_mpi'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ANTIVAPI where PACODAZI=KeySet.PACODAZI
    *                            and PACODIVA=KeySet.PACODIVA
    *                            and PACODICE=KeySet.PACODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ANTIVAPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPI_IDX,2],this.bLoadRecFilter,this.ANTIVAPI_IDX,"gsai_mpi")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVAPI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVAPI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVAPI '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
      select * from (i_cTable) ANTIVAPI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_PACODAZI = NVL(PACODAZI,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ANTIVAPI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_IVDESIVA = space(35)
          .w_CCDESCRI = space(35)
          .w_IVFLGSER = space(1)
          .w_CCFLRIFE = space(1)
          .w_PACODIVA = NVL(PACODIVA,space(5))
          if link_2_1_joined
            this.w_PACODIVA = NVL(IVCODIVA201,NVL(this.w_PACODIVA,space(5)))
            this.w_IVDESIVA = NVL(IVDESIVA201,space(35))
            this.w_IVFLGSER = NVL(IVFLGSER201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_PACODICE = NVL(PACODICE,space(5))
        .w_XXCODICE = .w_PACODICE
          .link_2_3('Load')
          .w_PATIPOPE = NVL(PATIPOPE,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace PACODIVA with .w_PACODIVA
          replace PACODICE with .w_PACODICE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PACODAZI=space(5)
      .w_PACODIVA=space(5)
      .w_PACODICE=space(5)
      .w_XXCODICE=space(5)
      .w_PATIPOPE=0
      .w_IVDESIVA=space(35)
      .w_CCDESCRI=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_IVFLGSER=space(1)
      .w_CCFLRIFE=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_PACODIVA))
         .link_2_1('Full')
        endif
        .w_PACODICE = NVL(.w_XXCODICE, space(5))
        .w_XXCODICE = .w_PACODICE
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_XXCODICE))
         .link_2_3('Full')
        endif
        .w_PATIPOPE = 0
        .DoRTCalc(6,7,.f.)
        .w_OBTEST = CTOD('01-01-1900')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVAPI')
    this.DoRTCalc(9,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ANTIVAPI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVAPI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODAZI,"PACODAZI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PACODIVA C(5);
      ,t_XXCODICE C(5);
      ,t_PATIPOPE N(3);
      ,t_IVDESIVA C(35);
      ,t_CCDESCRI C(35);
      ,PACODIVA C(5);
      ,PACODICE C(5);
      ,t_PACODICE C(5);
      ,t_IVFLGSER C(1);
      ,t_CCFLRIFE C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsai_mpibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPACODIVA_2_1.controlsource=this.cTrsName+'.t_PACODIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oXXCODICE_2_3.controlsource=this.cTrsName+'.t_XXCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPOPE_2_4.controlsource=this.cTrsName+'.t_PATIPOPE'
    this.oPgFRm.Page1.oPag.oIVDESIVA_2_5.controlsource=this.cTrsName+'.t_IVDESIVA'
    this.oPgFRm.Page1.oPag.oCCDESCRI_2_6.controlsource=this.cTrsName+'.t_CCDESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(75)
    this.AddVLine(150)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODIVA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVAPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVAPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPI_IDX,2])
      *
      * insert into ANTIVAPI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVAPI')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVAPI')
        i_cFldBody=" "+;
                  "(PACODAZI,PACODIVA,PACODICE,PATIPOPE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PACODAZI)+","+cp_ToStrODBCNull(this.w_PACODIVA)+","+cp_ToStrODBC(this.w_PACODICE)+","+cp_ToStrODBC(this.w_PATIPOPE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVAPI')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVAPI')
        cp_CheckDeletedKey(i_cTable,0,'PACODAZI',this.w_PACODAZI,'PACODIVA',this.w_PACODIVA,'PACODICE',this.w_PACODICE)
        INSERT INTO (i_cTable) (;
                   PACODAZI;
                  ,PACODIVA;
                  ,PACODICE;
                  ,PATIPOPE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PACODAZI;
                  ,this.w_PACODIVA;
                  ,this.w_PACODICE;
                  ,this.w_PATIPOPE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ANTIVAPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PACODIVA))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVAPI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and PACODIVA="+cp_ToStrODBC(&i_TN.->PACODIVA)+;
                 " and PACODICE="+cp_ToStrODBC(&i_TN.->PACODICE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVAPI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and PACODIVA=&i_TN.->PACODIVA;
                      and PACODICE=&i_TN.->PACODICE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PACODIVA))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PACODIVA="+cp_ToStrODBC(&i_TN.->PACODIVA)+;
                            " and PACODICE="+cp_ToStrODBC(&i_TN.->PACODICE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PACODIVA=&i_TN.->PACODIVA;
                            and PACODICE=&i_TN.->PACODICE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace PACODIVA with this.w_PACODIVA
              replace PACODICE with this.w_PACODICE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ANTIVAPI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVAPI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PATIPOPE="+cp_ToStrODBC(this.w_PATIPOPE)+;
                     ",PACODIVA="+cp_ToStrODBC(this.w_PACODIVA)+;
                     ",PACODICE="+cp_ToStrODBC(this.w_PACODICE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and PACODIVA="+cp_ToStrODBC(PACODIVA)+;
                             " and PACODICE="+cp_ToStrODBC(PACODICE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVAPI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PATIPOPE=this.w_PATIPOPE;
                     ,PACODIVA=this.w_PACODIVA;
                     ,PACODICE=this.w_PACODICE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and PACODIVA=&i_TN.->PACODIVA;
                                      and PACODICE=&i_TN.->PACODICE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVAPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PACODIVA))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ANTIVAPI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PACODIVA="+cp_ToStrODBC(&i_TN.->PACODIVA)+;
                            " and PACODICE="+cp_ToStrODBC(&i_TN.->PACODICE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PACODIVA=&i_TN.->PACODIVA;
                              and PACODICE=&i_TN.->PACODICE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PACODIVA))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVAPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_XXCODICE<>.w_XXCODICE
          .w_PACODICE = NVL(.w_XXCODICE, space(5))
        endif
        if .o_PACODICE<>.w_PACODICE
          .w_XXCODICE = .w_PACODICE
          .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        if .o_PACODIVA<>.w_PACODIVA
          .Calculate_NPSGGLYNBF()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PACODICE with this.w_PACODICE
      replace t_IVFLGSER with this.w_IVFLGSER
      replace t_CCFLRIFE with this.w_CCFLRIFE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_NPSGGLYNBF()
    with this
          * --- Sbianca campo
          .w_XXCODICE = IIF(empty(.w_PACODIVA),'    ',.w_XXCODICE)
          .link_2_3('Full')
          .w_PACODICE = IIF(empty(.w_PACODIVA),'    ',.w_XXCODICE)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oXXCODICE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oXXCODICE_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PACODIVA
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_PACODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVFLGSER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_PACODIVA))
          select IVCODIVA,IVDESIVA,IVFLGSER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_PACODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVFLGSER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_PACODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVFLGSER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oPACODIVA_2_1'),i_cWhere,'GSAR_AIV',"Codici iva",'GSAI_MPI.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVFLGSER";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVFLGSER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVFLGSER";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_PACODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_PACODIVA)
            select IVCODIVA,IVDESIVA,IVFLGSER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_IVDESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_IVFLGSER = NVL(_Link_.IVFLGSER,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PACODIVA = space(5)
      endif
      this.w_IVDESIVA = space(35)
      this.w_IVFLGSER = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IVFLGSER $ 'SB'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("FiscalitÓ di tipo escluso")
        endif
        this.w_PACODIVA = space(5)
        this.w_IVDESIVA = space(35)
        this.w_IVFLGSER = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.IVCODIVA as IVCODIVA201"+ ",link_2_1.IVDESIVA as IVDESIVA201"+ ",link_2_1.IVFLGSER as IVFLGSER201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on ANTIVAPI.PACODIVA=link_2_1.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and ANTIVAPI.PACODIVA=link_2_1.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=XXCODICE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_XXCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_XXCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_XXCODICE))
          select CCCODICE,CCDESCRI,CCFLRIFE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_XXCODICE)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_XXCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_XXCODICE)+"%");

            select CCCODICE,CCDESCRI,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_XXCODICE) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oXXCODICE_2_3'),i_cWhere,'',"Causali contabili",'GSAI1MPI.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_XXCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_XXCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_XXCODICE)
            select CCCODICE,CCDESCRI,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_XXCODICE = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(35))
      this.w_CCFLRIFE = NVL(_Link_.CCFLRIFE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_XXCODICE = space(5)
      endif
      this.w_CCDESCRI = space(35)
      this.w_CCFLRIFE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CCFLRIFE<>'N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_XXCODICE = space(5)
        this.w_CCDESCRI = space(35)
        this.w_CCFLRIFE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_XXCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oIVDESIVA_2_5.value==this.w_IVDESIVA)
      this.oPgFrm.Page1.oPag.oIVDESIVA_2_5.value=this.w_IVDESIVA
      replace t_IVDESIVA with this.oPgFrm.Page1.oPag.oIVDESIVA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_2_6.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_2_6.value=this.w_CCDESCRI
      replace t_CCDESCRI with this.oPgFrm.Page1.oPag.oCCDESCRI_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODIVA_2_1.value==this.w_PACODIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODIVA_2_1.value=this.w_PACODIVA
      replace t_PACODIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODIVA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXXCODICE_2_3.value==this.w_XXCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXXCODICE_2_3.value=this.w_XXCODICE
      replace t_XXCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXXCODICE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPOPE_2_4.RadioValue()==this.w_PATIPOPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPOPE_2_4.SetRadio()
      replace t_PATIPOPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPOPE_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVAPI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_IVFLGSER $ 'SB') and not(empty(.w_PACODIVA)) and (not(Empty(.w_PACODIVA)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPACODIVA_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("FiscalitÓ di tipo escluso")
        case   not(.w_CCFLRIFE<>'N') and (NOT EMPTY(.w_PACODIVA)) and not(empty(.w_XXCODICE)) and (not(Empty(.w_PACODIVA)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXXCODICE_2_3
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_PACODIVA))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PACODIVA = this.w_PACODIVA
    this.o_PACODICE = this.w_PACODICE
    this.o_XXCODICE = this.w_XXCODICE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PACODIVA)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PACODIVA=space(5)
      .w_PACODICE=space(5)
      .w_XXCODICE=space(5)
      .w_PATIPOPE=0
      .w_IVDESIVA=space(35)
      .w_CCDESCRI=space(35)
      .w_IVFLGSER=space(1)
      .w_CCFLRIFE=space(1)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_PACODIVA))
        .link_2_1('Full')
      endif
        .w_PACODICE = NVL(.w_XXCODICE, space(5))
        .w_XXCODICE = .w_PACODICE
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_XXCODICE))
        .link_2_3('Full')
      endif
        .w_PATIPOPE = 0
    endwith
    this.DoRTCalc(6,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PACODIVA = t_PACODIVA
    this.w_PACODICE = t_PACODICE
    this.w_XXCODICE = t_XXCODICE
    this.w_PATIPOPE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPOPE_2_4.RadioValue(.t.)
    this.w_IVDESIVA = t_IVDESIVA
    this.w_CCDESCRI = t_CCDESCRI
    this.w_IVFLGSER = t_IVFLGSER
    this.w_CCFLRIFE = t_CCFLRIFE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PACODIVA with this.w_PACODIVA
    replace t_PACODICE with this.w_PACODICE
    replace t_XXCODICE with this.w_XXCODICE
    replace t_PATIPOPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPATIPOPE_2_4.ToRadio()
    replace t_IVDESIVA with this.w_IVDESIVA
    replace t_CCDESCRI with this.w_CCDESCRI
    replace t_IVFLGSER with this.w_IVFLGSER
    replace t_CCFLRIFE with this.w_CCFLRIFE
    if i_srv='A'
      replace PACODIVA with this.w_PACODIVA
      replace PACODICE with this.w_PACODICE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsai_mpiPag1 as StdContainer
  Width  = 399
  height = 309
  stdWidth  = 399
  stdheight = 309
  resizeXpos=294
  resizeYpos=174
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=1, width=386,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="PACODIVA",Label1="Codice IVA",Field2="XXCODICE",Label2="Causale",Field3="PATIPOPE",Label3="Tipologia operazione IVA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 99802502

  add object oStr_1_3 as StdString with uid="LWBKMSQYBF",Visible=.t., Left=13, Top=260,;
    Alignment=1, Width=88, Height=18,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="IOLKOWHBUI",Visible=.t., Left=26, Top=284,;
    Alignment=1, Width=75, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=24,;
    width=382+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=25,width=381+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOCIIVA|CAU_CONT|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oIVDESIVA_2_5.Refresh()
      this.Parent.oCCDESCRI_2_6.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOCIIVA'
        oDropInto=this.oBodyCol.oRow.oPACODIVA_2_1
      case cFile='CAU_CONT'
        oDropInto=this.oBodyCol.oRow.oXXCODICE_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oIVDESIVA_2_5 as StdTrsField with uid="BGCRWVXCNF",rtseq=6,rtrep=.t.,;
    cFormVar="w_IVDESIVA",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione codice IVA",;
    HelpContextID = 173042745,;
    cTotal="", bFixedPos=.t., cQueryName = "IVDESIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=105, Top=257, InputMask=replicate('X',35)

  add object oCCDESCRI_2_6 as StdTrsField with uid="VRURKSQAJM",rtseq=7,rtrep=.t.,;
    cFormVar="w_CCDESCRI",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione causale contabile",;
    HelpContextID = 263159919,;
    cTotal="", bFixedPos=.t., cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=105, Top=282, InputMask=replicate('X',35)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsai_mpiBodyRow as CPBodyRowCnt
  Width=372
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPACODIVA_2_1 as StdTrsField with uid="LWXWSLNUJP",rtseq=2,rtrep=.t.,;
    cFormVar="w_PACODIVA",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Codice IVA",;
    HelpContextID = 188125385,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "FiscalitÓ di tipo escluso",;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_PACODIVA"

  func oPACODIVA_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODIVA_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPACODIVA_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPACODIVA_2_1.readonly and this.parent.oPACODIVA_2_1.isprimarykey)
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oPACODIVA_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici iva",'GSAI_MPI.VOCIIVA_VZM',this.parent.oContained
   endif
  endproc
  proc oPACODIVA_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_PACODIVA
    i_obj.ecpSave()
  endproc

  add object oXXCODICE_2_3 as StdTrsField with uid="FHYXXPGFIY",rtseq=4,rtrep=.t.,;
    cFormVar="w_XXCODICE",value=space(5),;
    ToolTipText = "Codice causale contabile",;
    HelpContextID = 188119365,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=72, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_XXCODICE"

  func oXXCODICE_2_3.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PACODIVA))
    endwith
  endfunc

  func oXXCODICE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oXXCODICE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oXXCODICE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oXXCODICE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali contabili",'GSAI1MPI.CAU_CONT_VZM',this.parent.oContained
  endproc

  add object oPATIPOPE_2_4 as StdTrsCombo with uid="LFMYDJFNYG",rtrep=.t.,;
    cFormVar="w_PATIPOPE", RowSource=""+"Imponibile,"+"Non imponibile,"+"Esente,"+"Non soggetta" , ;
    ToolTipText = "Tipologia operazione IVA",;
    HelpContextID = 193232699,;
    Height=22, Width=219, Left=148, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPATIPOPE_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PATIPOPE,&i_cF..t_PATIPOPE),this.value)
    return(iif(xVal =1,0,;
    iif(xVal =2,1,;
    iif(xVal =3,2,;
    iif(xVal =4,3,;
    0)))))
  endfunc
  func oPATIPOPE_2_4.GetRadio()
    this.Parent.oContained.w_PATIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oPATIPOPE_2_4.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_PATIPOPE==0,1,;
      iif(this.Parent.oContained.w_PATIPOPE==1,2,;
      iif(this.Parent.oContained.w_PATIPOPE==2,3,;
      iif(this.Parent.oContained.w_PATIPOPE==3,4,;
      0)))))
  endfunc

  func oPATIPOPE_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oPACODIVA_2_1.When()
    return(.t.)
  proc oPACODIVA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPACODIVA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_mpi','ANTIVAPI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODAZI=ANTIVAPI.PACODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
