* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bcf                                                        *
*              Controllo finali                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-02-07                                                      *
* Last revis.: 2013-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bcf",oParentObject)
return(i_retval)

define class tgsai_bcf as StdBatch
  * --- Local variables
  w_TRIMINI = 0
  w_TRIMFIN = 0
  w_TRIMESTRE = 0
  w_MESE = 0
  w_DATAINIZIALE = .f.
  w_TRIMVAL = .f.
  w_CONTRIM = 0
  w_TRIMVAL = .f.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli finali lanciato da "Parametri dati estratti"
    *     Devo verificare che tra le date di filtro degli autotrasportatori
    *     non siano presenti pi� di 4 trimestri
    this.w_TRIMVAL = .F.
    this.w_CONTRIM = 1
    this.w_DATAINIZIALE = .T.
    do while this.w_CONTRIM<=2
      if this.w_DATAINIZIALE
        this.w_MESE = MONTH(this.oParentObject.w_PEDATINA)
      else
        this.w_MESE = MONTH(this.oParentObject.w_PEDATFIA)
      endif
      do case
        case (this.w_MESE = 12) OR (this.w_MESE = 11) OR (this.w_MESE = 10)
          this.w_TRIMESTRE = 4
        case (this.w_MESE = 9) OR (this.w_MESE = 8) OR (this.w_MESE = 7)
          this.w_TRIMESTRE = 3
        case (this.w_MESE = 6) OR (this.w_MESE = 5) OR (this.w_MESE = 4)
          this.w_TRIMESTRE = 2
        case (this.w_MESE = 3) OR (this.w_MESE = 2) OR (this.w_MESE = 1)
          this.w_TRIMESTRE = 1
      endcase
      if this.w_DATAINIZIALE
        this.w_TRIMINI = this.w_TRIMESTRE
      else
        this.w_TRIMFIN = this.w_TRIMESTRE
      endif
      this.w_CONTRIM = this.w_CONTRIM+1
      this.w_DATAINIZIALE = .F.
    enddo
    do case
      case (this.w_Trimini=4 And ( this.w_Trimfin>=1 And this.w_Trimfin<=3) And Year(this.oParentObject.w_Pedatina)+1=Year(this.oParentObject.w_Pedatfia) ) Or (this.w_Trimini=4 And this.w_Trimfin=4 And Year(this.oParentObject.w_Pedatina)=Year(this.oParentObject.w_Pedatfia))
        this.w_Trimval = .T.
      case (this.w_Trimini=1 And (this.w_Trimfin>=1 And this.w_Trimfin<=4) And Year(this.oParentObject.w_Pedatina)=Year(this.oParentObject.w_Pedatfia)) 
        this.w_Trimval = .T.
      case (this.w_Trimini=2 And (this.w_Trimfin>=2 And this.w_Trimfin<=4) And Year(this.oParentObject.w_Pedatina)=Year(this.oParentObject.w_Pedatfia)) Or (this.w_Trimini=2 And this.w_Trimfin=1 And Year(this.oParentObject.w_Pedatina)+1=Year(this.oParentObject.w_Pedatfia))
        this.w_Trimval = .T.
      case (this.w_Trimini=3 And (this.w_Trimfin=3 or this.w_Trimfin=4) And Year(this.oParentObject.w_Pedatina)=Year(this.oParentObject.w_Pedatfia)) Or (this.w_Trimini=3 And (this.w_Trimfin=1 or this.w_Trimfin=2) And Year(this.oParentObject.w_Pedatina)+1=Year(this.oParentObject.w_Pedatfia))
        this.w_Trimval = .T.
    endcase
    if ! this.w_Trimval
      ah_errorMsg("Date adempimento autofatture incongruenti")
      this.oParentObject.w_RESCHK = -1
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
