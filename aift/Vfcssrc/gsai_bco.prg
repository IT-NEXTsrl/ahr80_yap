* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bco                                                        *
*              Aggiornamento manuale fatture con movimenti di rettifica        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-11                                                      *
* Last revis.: 2011-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bco",oParentObject)
return(i_retval)

define class tgsai_bco as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_GSAI_MDR = .NULL.
  w_NUM_RIGA = 0
  w_NUMREC = 0
  w_DELROWNUM = 0
  w_IMPORTO_DOVUTO_L = 0
  w_IMPOSTA_DOVUTA_L = 0
  w_NUM_REC_RETT = 0
  w_RIFPNT = space(10)
  w_PNNUMDOC = 0
  w_PNALFDOC = space(2)
  w_PNDATDOC = ctod("  /  /  ")
  w_ORNUMDOC = 0
  w_ORALFDOC = space(2)
  w_ORDATDOC = ctod("  /  /  ")
  * --- WorkFile variables
  ANTIVDDR_idx=0
  TMPPNT_IVA_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per poter aggiornare le fatture con movimenti di rettifica
    * --- Read from PNT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PNDATDOC,PNALFDOC,PNNUMDOC"+;
        " from "+i_cTable+" PNT_MAST where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DERIFPNT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PNDATDOC,PNALFDOC,PNNUMDOC;
        from (i_cTable) where;
            PNSERIAL = this.oParentObject.w_DERIFPNT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ORDATDOC = NVL(cp_ToDate(_read_.PNDATDOC),cp_NullValue(_read_.PNDATDOC))
      this.w_ORALFDOC = NVL(cp_ToDate(_read_.PNALFDOC),cp_NullValue(_read_.PNALFDOC))
      this.w_ORNUMDOC = NVL(cp_ToDate(_read_.PNNUMDOC),cp_NullValue(_read_.PNNUMDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Nel caso di record 1 visualizzo tutte le fatture con importo dovuto 
    *     maggiore della nota di credito selezionata.
    *     Nel caso di record 2/3 visualizzo tutte le fatture 
    *     sia con importo dovuto che imposta dovuta maggiori della nota di 
    *     credito selezionata
    this.w_PADRE = This.oParentObject
    this.w_GSAI_MDR = this.w_Padre.Gsai_Mdr
    if .F.
      * --- Create temporary table TMPPNT_IVA
      i_nIdx=cp_AddTableDef('TMPPNT_IVA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPPNT_IVA_proto';
            )
      this.TMPPNT_IVA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    this.w_PADRE.MarkPos()     
    CURTOTAB(this.w_PADRE.cTrsName,"TMPPNT_IVA", this)
    this.w_PADRE.RePos()     
    vx_exec("STD\GSAI1MCO.VZM",this)
    * --- Drop temporary table TMPPNT_IVA
    i_nIdx=cp_GetTableDefIdx('TMPPNT_IVA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPPNT_IVA')
    endif
    if this.w_NUM_RIGA<>0 AND ah_yesno("Si vuole confermare l'associazione della nota di variazione n. %1 del %2 con la fattura n. %3 del %4?",,ALLTRIM(STR(this.w_ORNUMDOC)+iif(not empty(this.w_ORALFDOC),"/"+this.w_ORALFDOC,"")),DTOC(this.w_ORDATDOC),ALLTRIM(STR(this.w_PNNUMDOC)+iif(not empty(this.w_PNALFDOC),"/"+this.w_PNALFDOC,"")),DTOC(this.w_PNDATDOC))
      this.w_DELROWNUM = this.oParentObject.w_CPROWNUM
      this.w_IMPORTO_DOVUTO_L = this.oParentObject.w_DEIMPDOC
      this.w_IMPOSTA_DOVUTA_L = this.oParentObject.w_DEIMPOST
      this.w_RIFPNT = this.oParentObject.w_DERIFPNT
      this.w_PADRE.FirstRow()     
      this.w_NUMREC = this.w_Padre.Search("Cprownum="+Alltrim(Str(this.w_Num_Riga,6,0)))
      this.w_PADRE.SetRow(this.w_Numrec)     
      this.w_PADRE.MarkPos()     
      this.w_PADRE.ChildrenChangeRow()     
      this.w_GSAI_MDR = this.w_Padre.Gsai_Mdr
      this.w_GSAI_MDR.AddRow()     
      this.w_GSAI_MDR.w_Reriffat = this.w_Rifpnt
      this.w_GSAI_MDR.w_Reimpdov = this.w_Importo_Dovuto_L*-1
      this.w_GSAI_MDR.w_Reimpost = this.w_Imposta_Dovuta_L*-1
      this.w_GSAI_MDR.SaveRow()     
      this.oParentObject.w_DEIMPDOC = this.oParentObject.w_DEIMPDOC+this.w_IMPORTO_DOVUTO_L
      this.oParentObject.w_DEIMPOST = this.oParentObject.w_DEIMPOST+this.w_IMPOSTA_DOVUTA_L
      this.oParentObject.w_IMPORTO_DOVUTO = this.oParentObject.w_DEIMPDOC
      this.oParentObject.w_IMPOSTA_DOVUTA = this.oParentObject.w_DEIMPOST
      this.oParentObject.w_TOTIMPDOV = this.oParentObject.w_TOTIMPDOV+this.w_IMPORTO_DOVUTO_L
      this.oParentObject.w_TOTIMPSDOV = this.oParentObject.w_TOTIMPSDOV+this.w_IMPOSTA_DOVUTA_L
      if this.oParentObject.w_DEIMPDOC=0 AND this.oParentObject.w_DEIMPOST=0
        this.oParentObject.w_DEESCLUD = "S"
      endif
      this.w_PADRE.SaveRow()     
      this.w_PADRE.FirstRow()     
      this.w_NUMREC = this.w_Padre.Search("Cprownum="+Alltrim(Str(this.w_Delrownum,6,0)))
      this.w_PADRE.SetRow(this.w_Numrec)     
      this.w_PADRE.DeleteRow()     
      this.w_PADRE.SaveRow()     
      this.w_PADRE.FirstRow()     
      this.w_PADRE.RePos()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ANTIVDDR'
    this.cWorkTables[2]='*TMPPNT_IVA'
    this.cWorkTables[3]='PNT_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
