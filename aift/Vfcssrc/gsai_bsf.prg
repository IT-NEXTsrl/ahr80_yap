* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bsf                                                        *
*              Stampa Spesometro                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-10-30                                                      *
* Last revis.: 2014-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bsf",oParentObject,m.pParame)
return(i_retval)

define class tgsai_bsf as StdBatch
  * --- Local variables
  pParame = space(10)
  w_RESFDF = 0
  w_QUADRO = 0
  w_AZPERAZI = space(1)
  w_MODELLO = space(20)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pParame="StampaFront"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Effettuiamo questo assegnamento perch� altrimenti i tasti funzione
        *     sarebbe disabilitati
        i_Curform=This.oParentObject
      case this.pParame="StampaMod"
        do case
          case this.oParentObject.w_Atgenspe="S"
            if this.oParentObject.w_Attipagr="S"
              Vq_Exec("..\Aift\Exe\Query\Gsai1bgt.vqr",this,"__tmp__")
              Cp_Chprn("..\aift\exe\query\Gsai1bgt", , this.oParentObject)
            else
              Vq_Exec("..\Aift\Exe\Query\Gsai_bgt.vqr",this,"__tmp__")
              Cp_Chprn("..\aift\exe\query\Gsai_bgt", , this.oParentObject)
            endif
          case this.oParentObject.w_Atgenblk="S"
            Vq_Exec("..\Aift\Exe\Query\Gsai4bgt.vqr",this,"__tmp__")
            Cp_Chprn("..\aift\exe\query\Gsai4bgt", , this.oParentObject)
          case this.oParentObject.w_Atgensma="S"
            Vq_Exec("..\Aift\Exe\Query\Gsai5bgt.vqr",this,"__tmp__")
            Cp_Chprn("..\aift\exe\query\Gsai5bgt", , this.oParentObject)
        endcase
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ritardo l'operazione successiva
    if type("g_PDFDELAY")="N"
      wait "" timeout g_PDFDELAY
    else
      wait "" timeout 5
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Select __TMP__
    this.w_RESFDF = cp_ffdf(" ",alltrim(this.w_MODELLO)+".pdf",this," ",@ARRFDF)
    * --- Se non mi ritorna un numero significa che ho incontrato un errore
    do case
      case Not Left( this.w_RESFDF , 1) $ "0123456789"
        * --- Errore durante creazione di un FDF..
        this.w_MESS = this.w_RESFDF
        ah_ErrorMsg(this.w_MESS,"stop","")
      case Left( this.w_RESFDF , 1) = "0"
        * --- Nessun FDF creato...
        this.w_MESS = "Nessun file PDF da generare."
        ah_ErrorMsg(this.w_MESS,"stop","")
      otherwise
        this.w_QUADRO = 1
        do while this.w_QUADRO<=Val( this.w_RESFDF )
          CP_CHPRN(tempadhoc()+"\"+alltrim(this.w_MODELLO)+ALLTRIM(STR(this.w_QUADRO))+".FDF",.NULL.,2)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_QUADRO = this.w_QUADRO+1
        enddo
    endcase
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Spesometro
    DECLARE ARRFDF (4,2)
    ARRFDF (1,1) = "ATCODFIS"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "ATCFISOT"
    ARRFDF (2,2) = 16
    ARRFDF (3,1) = "ATCODINT"
    ARRFDF (3,2) = 16
    ARRFDF (4,1) = "ATPARIVA"
    ARRFDF (4,2) = 11
    this.w_AZPERAZI = this.oparentobject.w_AZPERAZI
    * --- Lancio diversi PDF a seconda del tipo di generazione
    if this.oParentObject.w_Attipinv=2
      VQ_EXEC("..\AIFT\EXE\QUERY\GSAI2BST", this, "__TMP__")
      if USED("__TMP__") AND RECCOUNT("__TMP__")>0 
        this.w_MODELLO = "MODPOL2013A"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        ah_ErrorMsg("Non ci sono dati da stampare")
        i_retcode = 'stop'
        return
      endif
      if used("__TMP__")
        select __TMP__
        use
      endif
    else
      VQ_EXEC("..\AIFT\EXE\QUERY\GSAI2BST", this, "__TMP__")
      if USED("__TMP__") AND RECCOUNT("__TMP__")>0 
        this.w_MODELLO = "MODPOLDESCR"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        ah_ErrorMsg("Non ci sono dati da stampare")
        i_retcode = 'stop'
        return
      endif
      if used("TEMP")
        select TEMP
        use
      endif
      * --- Stampa Fontespizio e quadro TA
      VQ_EXEC("..\AIFT\EXE\QUERY\GSAI_BST", this, "__TMP__")
      if USED("__TMP__") AND RECCOUNT("__TMP__")>0 
        this.w_MODELLO = "MODPOL2013"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        ah_ErrorMsg("Non ci sono dati da stampare")
        i_retcode = 'stop'
        return
      endif
      if used("TEMP")
        select TEMP
        use
      endif
    endif
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
