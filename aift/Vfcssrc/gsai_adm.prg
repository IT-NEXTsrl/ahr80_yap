* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_adm                                                        *
*              Dati estratti acquisti da San Marino                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_adm"))

* --- Class definition
define class tgsai_adm as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 796
  Height = 573+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=175503511
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  DATESTSM_IDX = 0
  CONTI_IDX = 0
  NAZIONI_IDX = 0
  SOGCOLNR_IDX = 0
  cFile = "DATESTSM"
  cKeySelect = "DMSERIAL"
  cKeyWhere  = "DMSERIAL=this.w_DMSERIAL"
  cKeyWhereODBC = '"DMSERIAL="+cp_ToStrODBC(this.w_DMSERIAL)';

  cKeyWhereODBCqualified = '"DATESTSM.DMSERIAL="+cp_ToStrODBC(this.w_DMSERIAL)';

  cPrg = "gsai_adm"
  cComment = "Dati estratti acquisti da San Marino"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DMSERIAL = space(10)
  w_DM__ANNO = 0
  w_DM__MESE = 0
  w_TIPCON = space(10)
  w_DMTIPCON = space(1)
  w_DMCODCON = space(15)
  o_DMCODCON = space(15)
  w_ANDESCRI = space(40)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANCOGNOM = space(20)
  w_AN__NOME = space(20)
  w_ANINDIRI = space(35)
  w_ANCODFIS = space(16)
  w_ANPARIVA = space(12)
  w_ANLOCALI = space(30)
  w_ANNAZION = space(3)
  w_ANCOFISC = space(25)
  w_ANPERFIS = space(1)
  w_NACODEST = space(5)
  w_NACODISO = space(3)
  w_DMPERFIS = space(1)
  o_DMPERFIS = space(1)
  w_DMDESCRI = space(60)
  w_DMCOGNOM = space(40)
  w_DM__NOME = space(40)
  w_DMDATNAS = ctod('  /  /  ')
  w_DMCOMEST = space(40)
  w_DMPRONAS = space(2)
  w_DMSTADOM = space(3)
  w_DMSTAEST = space(3)
  w_DMCITEST = space(40)
  w_DMINDEST = space(40)
  w_DMPARIVA = space(25)
  w_DMCODFIS = space(25)
  w_OBTEST = ctod('  /  /  ')
  w_DMSCLGEN = space(1)
  w_DMDACONT = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DMSERIAL = this.W_DMSERIAL

  * --- Children pointers
  GSAI_MSA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DATESTSM','gsai_adm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_admPag1","gsai_adm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati anagrafici")
      .Pages(1).HelpContextID = 132327175
      .Pages(2).addobject("oPag","tgsai_admPag2","gsai_adm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio dati estratti")
      .Pages(2).HelpContextID = 195975900
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='NAZIONI'
    this.cWorkTables[3]='SOGCOLNR'
    this.cWorkTables[4]='DATESTSM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATESTSM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATESTSM_IDX,3]
  return

  function CreateChildren()
    this.GSAI_MSA = CREATEOBJECT('stdDynamicChild',this,'GSAI_MSA',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAI_MSA)
      this.GSAI_MSA.DestroyChildrenChain()
      this.GSAI_MSA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAI_MSA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAI_MSA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAI_MSA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAI_MSA.SetKey(;
            .w_DMSERIAL,"DSSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAI_MSA.ChangeRow(this.cRowID+'      1',1;
             ,.w_DMSERIAL,"DSSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAI_MSA)
        i_f=.GSAI_MSA.BuildFilter()
        if !(i_f==.GSAI_MSA.cQueryFilter)
          i_fnidx=.GSAI_MSA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MSA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MSA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MSA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MSA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DMSERIAL = NVL(DMSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DATESTSM where DMSERIAL=KeySet.DMSERIAL
    *
    i_nConn = i_TableProp[this.DATESTSM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSM_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATESTSM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATESTSM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATESTSM '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DMSERIAL',this.w_DMSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPCON = 'F'
        .w_ANDESCRI = space(40)
        .w_ANDATNAS = ctod("  /  /  ")
        .w_ANLOCNAS = space(30)
        .w_ANPRONAS = space(2)
        .w_ANCOGNOM = space(20)
        .w_AN__NOME = space(20)
        .w_ANINDIRI = space(35)
        .w_ANCODFIS = space(16)
        .w_ANPARIVA = space(12)
        .w_ANLOCALI = space(30)
        .w_ANNAZION = space(3)
        .w_ANCOFISC = space(25)
        .w_ANPERFIS = space(1)
        .w_NACODEST = space(5)
        .w_NACODISO = space(3)
        .w_OBTEST = CTOD('01-01-1900')
        .w_DMSERIAL = NVL(DMSERIAL,space(10))
        .op_DMSERIAL = .w_DMSERIAL
        .w_DM__ANNO = NVL(DM__ANNO,0)
        .w_DM__MESE = NVL(DM__MESE,0)
        .w_DMTIPCON = NVL(DMTIPCON,space(1))
        .w_DMCODCON = NVL(DMCODCON,space(15))
          if link_1_8_joined
            this.w_DMCODCON = NVL(ANCODICE108,NVL(this.w_DMCODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI108,space(40))
            this.w_ANINDIRI = NVL(ANINDIRI108,space(35))
            this.w_ANDATNAS = NVL(cp_ToDate(ANDATNAS108),ctod("  /  /  "))
            this.w_ANLOCNAS = NVL(ANLOCNAS108,space(30))
            this.w_ANPRONAS = NVL(ANPRONAS108,space(2))
            this.w_ANCOGNOM = NVL(ANCOGNOM108,space(20))
            this.w_AN__NOME = NVL(AN__NOME108,space(20))
            this.w_ANPARIVA = NVL(ANPARIVA108,space(12))
            this.w_ANLOCALI = NVL(ANLOCALI108,space(30))
            this.w_ANNAZION = NVL(ANNAZION108,space(3))
            this.w_ANCODFIS = NVL(ANCODFIS108,space(16))
            this.w_ANCOFISC = NVL(ANCOFISC108,space(25))
            this.w_ANPERFIS = NVL(ANPERFIS108,space(1))
          else
          .link_1_8('Load')
          endif
          .link_1_19('Load')
        .w_DMPERFIS = NVL(DMPERFIS,space(1))
        .w_DMDESCRI = NVL(DMDESCRI,space(60))
        .w_DMCOGNOM = NVL(DMCOGNOM,space(40))
        .w_DM__NOME = NVL(DM__NOME,space(40))
        .w_DMDATNAS = NVL(cp_ToDate(DMDATNAS),ctod("  /  /  "))
        .w_DMCOMEST = NVL(DMCOMEST,space(40))
        .w_DMPRONAS = NVL(DMPRONAS,space(2))
        .w_DMSTADOM = NVL(DMSTADOM,space(3))
        .w_DMSTAEST = NVL(DMSTAEST,space(3))
        .w_DMCITEST = NVL(DMCITEST,space(40))
        .w_DMINDEST = NVL(DMINDEST,space(40))
        .w_DMPARIVA = NVL(DMPARIVA,space(25))
        .w_DMCODFIS = NVL(DMCODFIS,space(25))
        .w_DMSCLGEN = NVL(DMSCLGEN,space(1))
        .w_DMDACONT = NVL(DMDACONT,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DATESTSM')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DMSERIAL = space(10)
      .w_DM__ANNO = 0
      .w_DM__MESE = 0
      .w_TIPCON = space(10)
      .w_DMTIPCON = space(1)
      .w_DMCODCON = space(15)
      .w_ANDESCRI = space(40)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANCOGNOM = space(20)
      .w_AN__NOME = space(20)
      .w_ANINDIRI = space(35)
      .w_ANCODFIS = space(16)
      .w_ANPARIVA = space(12)
      .w_ANLOCALI = space(30)
      .w_ANNAZION = space(3)
      .w_ANCOFISC = space(25)
      .w_ANPERFIS = space(1)
      .w_NACODEST = space(5)
      .w_NACODISO = space(3)
      .w_DMPERFIS = space(1)
      .w_DMDESCRI = space(60)
      .w_DMCOGNOM = space(40)
      .w_DM__NOME = space(40)
      .w_DMDATNAS = ctod("  /  /  ")
      .w_DMCOMEST = space(40)
      .w_DMPRONAS = space(2)
      .w_DMSTADOM = space(3)
      .w_DMSTAEST = space(3)
      .w_DMCITEST = space(40)
      .w_DMINDEST = space(40)
      .w_DMPARIVA = space(25)
      .w_DMCODFIS = space(25)
      .w_OBTEST = ctod("  /  /  ")
      .w_DMSCLGEN = space(1)
      .w_DMDACONT = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DM__ANNO = year(i_DATSYS)
        .w_DM__MESE = Month(i_Datsys)
        .w_TIPCON = 'F'
        .w_DMTIPCON = 'F'
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_DMCODCON))
          .link_1_8('Full')
          endif
        .DoRTCalc(7,17,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_19('Full')
          endif
          .DoRTCalc(18,21,.f.)
        .w_DMPERFIS = IIF(.w_ANPERFIS='S','P','N')
        .w_DMDESCRI = .w_ANDESCRI
        .w_DMCOGNOM = IIF(.w_DMPERFIS$'PS',LEFT(.w_ANCOGNOM,40),SPACE(40))
        .w_DM__NOME = IIF(.w_DMPERFIS$'PS',LEFT(.w_AN__NOME,40),SPACE(40))
        .w_DMDATNAS = IIF(.w_DMPERFIS$'PS',.w_ANDATNAS,CTOD('  -  -   '))
        .w_DMCOMEST = IIF(.w_DMPERFIS$'PS',.w_ANLOCNAS,SPACE(40))
        .w_DMPRONAS = IIF(.w_DMPERFIS$'PS',.w_ANPRONAS,SPACE(2))
        .w_DMSTADOM = IIF(.w_DMPERFIS$'PS',LEFT(.w_NACODEST,3),SPACE(3))
        .w_DMSTAEST = IIF(.w_DMPERFIS$'NS',LEFT(.w_NACODEST,3),SPACE(3))
        .w_DMCITEST = IIF(.w_DMPERFIS$'NS',.w_ANLOCALI,SPACE(40))
        .w_DMINDEST = IIF(.w_DMPERFIS$'NS',.w_ANINDIRI,SPACE(40))
        .w_DMPARIVA = iif(Not Empty(.w_Ancofisc),.w_Ancofisc,iif(Not Empty(Nvl(.w_Anpariva,'')),Alltrim(.w_Nacodiso)+Alltrim(.w_Anpariva),Space(25)))
        .w_DMCODFIS = .w_Ancodfis
        .w_OBTEST = CTOD('01-01-1900')
        .w_DMSCLGEN = 'N'
        .w_DMDACONT = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATESTSM')
    this.DoRTCalc(38,41,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTSM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSM_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"DEBSM","i_codazi,w_DMSERIAL")
      .op_codazi = .w_codazi
      .op_DMSERIAL = .w_DMSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDM__ANNO_1_2.enabled = i_bVal
      .Page1.oPag.oDM__MESE_1_4.enabled = i_bVal
      .Page1.oPag.oDMTIPCON_1_7.enabled = i_bVal
      .Page1.oPag.oDMCODCON_1_8.enabled = i_bVal
      .Page1.oPag.oDMPERFIS_1_24.enabled = i_bVal
      .Page1.oPag.oDMDESCRI_1_25.enabled = i_bVal
      .Page1.oPag.oDMCOGNOM_1_26.enabled = i_bVal
      .Page1.oPag.oDM__NOME_1_27.enabled = i_bVal
      .Page1.oPag.oDMDATNAS_1_28.enabled = i_bVal
      .Page1.oPag.oDMCOMEST_1_29.enabled = i_bVal
      .Page1.oPag.oDMPRONAS_1_30.enabled = i_bVal
      .Page1.oPag.oDMSTADOM_1_31.enabled = i_bVal
      .Page1.oPag.oDMSTAEST_1_32.enabled = i_bVal
      .Page1.oPag.oDMCITEST_1_33.enabled = i_bVal
      .Page1.oPag.oDMINDEST_1_34.enabled = i_bVal
      .Page1.oPag.oDMPARIVA_1_35.enabled = i_bVal
      .Page1.oPag.oDMCODFIS_1_36.enabled = i_bVal
      .Page1.oPag.oDMSCLGEN_1_59.enabled = i_bVal
      .Page1.oPag.oDMDACONT_1_60.enabled = i_bVal
    endwith
    this.GSAI_MSA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DATESTSM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAI_MSA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATESTSM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMSERIAL,"DMSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DM__ANNO,"DM__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DM__MESE,"DM__MESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMTIPCON,"DMTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMCODCON,"DMCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMPERFIS,"DMPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMDESCRI,"DMDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMCOGNOM,"DMCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DM__NOME,"DM__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMDATNAS,"DMDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMCOMEST,"DMCOMEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMPRONAS,"DMPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMSTADOM,"DMSTADOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMSTAEST,"DMSTAEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMCITEST,"DMCITEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMINDEST,"DMINDEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMPARIVA,"DMPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMCODFIS,"DMCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMSCLGEN,"DMSCLGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DMDACONT,"DMDACONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESTSM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSM_IDX,2])
    i_lTable = "DATESTSM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DATESTSM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATESTSM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSM_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DATESTSM_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"DEBSM","i_codazi,w_DMSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DATESTSM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATESTSM')
        i_extval=cp_InsertValODBCExtFlds(this,'DATESTSM')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DMSERIAL,DM__ANNO,DM__MESE,DMTIPCON,DMCODCON"+;
                  ",DMPERFIS,DMDESCRI,DMCOGNOM,DM__NOME,DMDATNAS"+;
                  ",DMCOMEST,DMPRONAS,DMSTADOM,DMSTAEST,DMCITEST"+;
                  ",DMINDEST,DMPARIVA,DMCODFIS,DMSCLGEN,DMDACONT"+;
                  ",UTCC,UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DMSERIAL)+;
                  ","+cp_ToStrODBC(this.w_DM__ANNO)+;
                  ","+cp_ToStrODBC(this.w_DM__MESE)+;
                  ","+cp_ToStrODBC(this.w_DMTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_DMCODCON)+;
                  ","+cp_ToStrODBC(this.w_DMPERFIS)+;
                  ","+cp_ToStrODBC(this.w_DMDESCRI)+;
                  ","+cp_ToStrODBC(this.w_DMCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_DM__NOME)+;
                  ","+cp_ToStrODBC(this.w_DMDATNAS)+;
                  ","+cp_ToStrODBC(this.w_DMCOMEST)+;
                  ","+cp_ToStrODBC(this.w_DMPRONAS)+;
                  ","+cp_ToStrODBC(this.w_DMSTADOM)+;
                  ","+cp_ToStrODBC(this.w_DMSTAEST)+;
                  ","+cp_ToStrODBC(this.w_DMCITEST)+;
                  ","+cp_ToStrODBC(this.w_DMINDEST)+;
                  ","+cp_ToStrODBC(this.w_DMPARIVA)+;
                  ","+cp_ToStrODBC(this.w_DMCODFIS)+;
                  ","+cp_ToStrODBC(this.w_DMSCLGEN)+;
                  ","+cp_ToStrODBC(this.w_DMDACONT)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATESTSM')
        i_extval=cp_InsertValVFPExtFlds(this,'DATESTSM')
        cp_CheckDeletedKey(i_cTable,0,'DMSERIAL',this.w_DMSERIAL)
        INSERT INTO (i_cTable);
              (DMSERIAL,DM__ANNO,DM__MESE,DMTIPCON,DMCODCON,DMPERFIS,DMDESCRI,DMCOGNOM,DM__NOME,DMDATNAS,DMCOMEST,DMPRONAS,DMSTADOM,DMSTAEST,DMCITEST,DMINDEST,DMPARIVA,DMCODFIS,DMSCLGEN,DMDACONT,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DMSERIAL;
                  ,this.w_DM__ANNO;
                  ,this.w_DM__MESE;
                  ,this.w_DMTIPCON;
                  ,this.w_DMCODCON;
                  ,this.w_DMPERFIS;
                  ,this.w_DMDESCRI;
                  ,this.w_DMCOGNOM;
                  ,this.w_DM__NOME;
                  ,this.w_DMDATNAS;
                  ,this.w_DMCOMEST;
                  ,this.w_DMPRONAS;
                  ,this.w_DMSTADOM;
                  ,this.w_DMSTAEST;
                  ,this.w_DMCITEST;
                  ,this.w_DMINDEST;
                  ,this.w_DMPARIVA;
                  ,this.w_DMCODFIS;
                  ,this.w_DMSCLGEN;
                  ,this.w_DMDACONT;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DATESTSM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSM_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DATESTSM_IDX,i_nConn)
      *
      * update DATESTSM
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DATESTSM')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DM__ANNO="+cp_ToStrODBC(this.w_DM__ANNO)+;
             ",DM__MESE="+cp_ToStrODBC(this.w_DM__MESE)+;
             ",DMTIPCON="+cp_ToStrODBC(this.w_DMTIPCON)+;
             ",DMCODCON="+cp_ToStrODBCNull(this.w_DMCODCON)+;
             ",DMPERFIS="+cp_ToStrODBC(this.w_DMPERFIS)+;
             ",DMDESCRI="+cp_ToStrODBC(this.w_DMDESCRI)+;
             ",DMCOGNOM="+cp_ToStrODBC(this.w_DMCOGNOM)+;
             ",DM__NOME="+cp_ToStrODBC(this.w_DM__NOME)+;
             ",DMDATNAS="+cp_ToStrODBC(this.w_DMDATNAS)+;
             ",DMCOMEST="+cp_ToStrODBC(this.w_DMCOMEST)+;
             ",DMPRONAS="+cp_ToStrODBC(this.w_DMPRONAS)+;
             ",DMSTADOM="+cp_ToStrODBC(this.w_DMSTADOM)+;
             ",DMSTAEST="+cp_ToStrODBC(this.w_DMSTAEST)+;
             ",DMCITEST="+cp_ToStrODBC(this.w_DMCITEST)+;
             ",DMINDEST="+cp_ToStrODBC(this.w_DMINDEST)+;
             ",DMPARIVA="+cp_ToStrODBC(this.w_DMPARIVA)+;
             ",DMCODFIS="+cp_ToStrODBC(this.w_DMCODFIS)+;
             ",DMSCLGEN="+cp_ToStrODBC(this.w_DMSCLGEN)+;
             ",DMDACONT="+cp_ToStrODBC(this.w_DMDACONT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DATESTSM')
        i_cWhere = cp_PKFox(i_cTable  ,'DMSERIAL',this.w_DMSERIAL  )
        UPDATE (i_cTable) SET;
              DM__ANNO=this.w_DM__ANNO;
             ,DM__MESE=this.w_DM__MESE;
             ,DMTIPCON=this.w_DMTIPCON;
             ,DMCODCON=this.w_DMCODCON;
             ,DMPERFIS=this.w_DMPERFIS;
             ,DMDESCRI=this.w_DMDESCRI;
             ,DMCOGNOM=this.w_DMCOGNOM;
             ,DM__NOME=this.w_DM__NOME;
             ,DMDATNAS=this.w_DMDATNAS;
             ,DMCOMEST=this.w_DMCOMEST;
             ,DMPRONAS=this.w_DMPRONAS;
             ,DMSTADOM=this.w_DMSTADOM;
             ,DMSTAEST=this.w_DMSTAEST;
             ,DMCITEST=this.w_DMCITEST;
             ,DMINDEST=this.w_DMINDEST;
             ,DMPARIVA=this.w_DMPARIVA;
             ,DMCODFIS=this.w_DMCODFIS;
             ,DMSCLGEN=this.w_DMSCLGEN;
             ,DMDACONT=this.w_DMDACONT;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAI_MSA : Saving
      this.GSAI_MSA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DMSERIAL,"DSSERIAL";
             )
      this.GSAI_MSA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAI_MSA : Deleting
    this.GSAI_MSA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DMSERIAL,"DSSERIAL";
           )
    this.GSAI_MSA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATESTSM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSM_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DATESTSM_IDX,i_nConn)
      *
      * delete DATESTSM
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DMSERIAL',this.w_DMSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTSM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,16,.t.)
          .link_1_19('Full')
        .DoRTCalc(18,21,.t.)
        if .o_DMCODCON<>.w_DMCODCON
            .w_DMPERFIS = IIF(.w_ANPERFIS='S','P','N')
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DMDESCRI = .w_ANDESCRI
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DMCOGNOM = IIF(.w_DMPERFIS$'PS',LEFT(.w_ANCOGNOM,40),SPACE(40))
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DM__NOME = IIF(.w_DMPERFIS$'PS',LEFT(.w_AN__NOME,40),SPACE(40))
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS <>.w_DMPERFIS 
            .w_DMDATNAS = IIF(.w_DMPERFIS$'PS',.w_ANDATNAS,CTOD('  -  -   '))
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DMCOMEST = IIF(.w_DMPERFIS$'PS',.w_ANLOCNAS,SPACE(40))
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DMPRONAS = IIF(.w_DMPERFIS$'PS',.w_ANPRONAS,SPACE(2))
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DMSTADOM = IIF(.w_DMPERFIS$'PS',LEFT(.w_NACODEST,3),SPACE(3))
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DMSTAEST = IIF(.w_DMPERFIS$'NS',LEFT(.w_NACODEST,3),SPACE(3))
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DMCITEST = IIF(.w_DMPERFIS$'NS',.w_ANLOCALI,SPACE(40))
        endif
        if .o_DMCODCON<>.w_DMCODCON.or. .o_DMPERFIS<>.w_DMPERFIS
            .w_DMINDEST = IIF(.w_DMPERFIS$'NS',.w_ANINDIRI,SPACE(40))
        endif
        if .o_DMCODCON<>.w_DMCODCON
            .w_DMPARIVA = iif(Not Empty(.w_Ancofisc),.w_Ancofisc,iif(Not Empty(Nvl(.w_Anpariva,'')),Alltrim(.w_Nacodiso)+Alltrim(.w_Anpariva),Space(25)))
        endif
        if .o_DMCODCON<>.w_DMCODCON
            .w_DMCODFIS = .w_Ancodfis
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"DEBSM","i_codazi,w_DMSERIAL")
          .op_DMSERIAL = .w_DMSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(35,41,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDMTIPCON_1_7.enabled = this.oPgFrm.Page1.oPag.oDMTIPCON_1_7.mCond()
    this.oPgFrm.Page1.oPag.oDMCODCON_1_8.enabled = this.oPgFrm.Page1.oPag.oDMCODCON_1_8.mCond()
    this.oPgFrm.Page1.oPag.oDMDESCRI_1_25.enabled = this.oPgFrm.Page1.oPag.oDMDESCRI_1_25.mCond()
    this.oPgFrm.Page1.oPag.oDMCOGNOM_1_26.enabled = this.oPgFrm.Page1.oPag.oDMCOGNOM_1_26.mCond()
    this.oPgFrm.Page1.oPag.oDM__NOME_1_27.enabled = this.oPgFrm.Page1.oPag.oDM__NOME_1_27.mCond()
    this.oPgFrm.Page1.oPag.oDMDATNAS_1_28.enabled = this.oPgFrm.Page1.oPag.oDMDATNAS_1_28.mCond()
    this.oPgFrm.Page1.oPag.oDMCOMEST_1_29.enabled = this.oPgFrm.Page1.oPag.oDMCOMEST_1_29.mCond()
    this.oPgFrm.Page1.oPag.oDMPRONAS_1_30.enabled = this.oPgFrm.Page1.oPag.oDMPRONAS_1_30.mCond()
    this.oPgFrm.Page1.oPag.oDMSTADOM_1_31.enabled = this.oPgFrm.Page1.oPag.oDMSTADOM_1_31.mCond()
    this.oPgFrm.Page1.oPag.oDMSTAEST_1_32.enabled = this.oPgFrm.Page1.oPag.oDMSTAEST_1_32.mCond()
    this.oPgFrm.Page1.oPag.oDMCITEST_1_33.enabled = this.oPgFrm.Page1.oPag.oDMCITEST_1_33.mCond()
    this.oPgFrm.Page1.oPag.oDMINDEST_1_34.enabled = this.oPgFrm.Page1.oPag.oDMINDEST_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DMCODCON
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DMCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANLOCALI,ANNAZION,ANCODFIS,ANCOFISC,ANPERFIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DMTIPCON;
                     ,'ANCODICE',trim(this.w_DMCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANLOCALI,ANNAZION,ANCODFIS,ANCOFISC,ANPERFIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DMCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DMCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDMCODCON_1_8'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'Gsai_Adm.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DMTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANLOCALI,ANNAZION,ANCODFIS,ANCOFISC,ANPERFIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANLOCALI,ANNAZION,ANCODFIS,ANCOFISC,ANPERFIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANLOCALI,ANNAZION,ANCODFIS,ANCOFISC,ANPERFIS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANLOCALI,ANNAZION,ANCODFIS,ANCOFISC,ANPERFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DMCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANLOCALI,ANNAZION,ANCODFIS,ANCOFISC,ANPERFIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DMCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DMTIPCON;
                       ,'ANCODICE',this.w_DMCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPARIVA,ANLOCALI,ANNAZION,ANCODFIS,ANCOFISC,ANPERFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DMCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANINDIRI = NVL(_Link_.ANINDIRI,space(35))
      this.w_ANDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_ANLOCNAS = NVL(_Link_.ANLOCNAS,space(30))
      this.w_ANPRONAS = NVL(_Link_.ANPRONAS,space(2))
      this.w_ANCOGNOM = NVL(_Link_.ANCOGNOM,space(20))
      this.w_AN__NOME = NVL(_Link_.AN__NOME,space(20))
      this.w_ANPARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_ANLOCALI = NVL(_Link_.ANLOCALI,space(30))
      this.w_ANNAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
      this.w_ANCOFISC = NVL(_Link_.ANCOFISC,space(25))
      this.w_ANPERFIS = NVL(_Link_.ANPERFIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DMCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_ANINDIRI = space(35)
      this.w_ANDATNAS = ctod("  /  /  ")
      this.w_ANLOCNAS = space(30)
      this.w_ANPRONAS = space(2)
      this.w_ANCOGNOM = space(20)
      this.w_AN__NOME = space(20)
      this.w_ANPARIVA = space(12)
      this.w_ANLOCALI = space(30)
      this.w_ANNAZION = space(3)
      this.w_ANCODFIS = space(16)
      this.w_ANCOFISC = space(25)
      this.w_ANPERFIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DMCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.ANCODICE as ANCODICE108"+ ",link_1_8.ANDESCRI as ANDESCRI108"+ ",link_1_8.ANINDIRI as ANINDIRI108"+ ",link_1_8.ANDATNAS as ANDATNAS108"+ ",link_1_8.ANLOCNAS as ANLOCNAS108"+ ",link_1_8.ANPRONAS as ANPRONAS108"+ ",link_1_8.ANCOGNOM as ANCOGNOM108"+ ",link_1_8.AN__NOME as AN__NOME108"+ ",link_1_8.ANPARIVA as ANPARIVA108"+ ",link_1_8.ANLOCALI as ANLOCALI108"+ ",link_1_8.ANNAZION as ANNAZION108"+ ",link_1_8.ANCODFIS as ANCODFIS108"+ ",link_1_8.ANCOFISC as ANCOFISC108"+ ",link_1_8.ANPERFIS as ANPERFIS108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on DATESTSM.DMCODCON=link_1_8.ANCODICE"+" and DATESTSM.DMTIPCON=link_1_8.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and DATESTSM.DMCODCON=link_1_8.ANCODICE(+)"'+'+" and DATESTSM.DMTIPCON=link_1_8.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODEST,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NACODEST,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_NACODEST = NVL(_Link_.NACODEST,space(5))
      this.w_NACODISO = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_NACODEST = space(5)
      this.w_NACODISO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDM__ANNO_1_2.value==this.w_DM__ANNO)
      this.oPgFrm.Page1.oPag.oDM__ANNO_1_2.value=this.w_DM__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDM__MESE_1_4.value==this.w_DM__MESE)
      this.oPgFrm.Page1.oPag.oDM__MESE_1_4.value=this.w_DM__MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDMTIPCON_1_7.RadioValue()==this.w_DMTIPCON)
      this.oPgFrm.Page1.oPag.oDMTIPCON_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDMCODCON_1_8.value==this.w_DMCODCON)
      this.oPgFrm.Page1.oPag.oDMCODCON_1_8.value=this.w_DMCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDMPERFIS_1_24.RadioValue()==this.w_DMPERFIS)
      this.oPgFrm.Page1.oPag.oDMPERFIS_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDMDESCRI_1_25.value==this.w_DMDESCRI)
      this.oPgFrm.Page1.oPag.oDMDESCRI_1_25.value=this.w_DMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDMCOGNOM_1_26.value==this.w_DMCOGNOM)
      this.oPgFrm.Page1.oPag.oDMCOGNOM_1_26.value=this.w_DMCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDM__NOME_1_27.value==this.w_DM__NOME)
      this.oPgFrm.Page1.oPag.oDM__NOME_1_27.value=this.w_DM__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oDMDATNAS_1_28.value==this.w_DMDATNAS)
      this.oPgFrm.Page1.oPag.oDMDATNAS_1_28.value=this.w_DMDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDMCOMEST_1_29.value==this.w_DMCOMEST)
      this.oPgFrm.Page1.oPag.oDMCOMEST_1_29.value=this.w_DMCOMEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDMPRONAS_1_30.value==this.w_DMPRONAS)
      this.oPgFrm.Page1.oPag.oDMPRONAS_1_30.value=this.w_DMPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDMSTADOM_1_31.value==this.w_DMSTADOM)
      this.oPgFrm.Page1.oPag.oDMSTADOM_1_31.value=this.w_DMSTADOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDMSTAEST_1_32.value==this.w_DMSTAEST)
      this.oPgFrm.Page1.oPag.oDMSTAEST_1_32.value=this.w_DMSTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDMCITEST_1_33.value==this.w_DMCITEST)
      this.oPgFrm.Page1.oPag.oDMCITEST_1_33.value=this.w_DMCITEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDMINDEST_1_34.value==this.w_DMINDEST)
      this.oPgFrm.Page1.oPag.oDMINDEST_1_34.value=this.w_DMINDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDMPARIVA_1_35.value==this.w_DMPARIVA)
      this.oPgFrm.Page1.oPag.oDMPARIVA_1_35.value=this.w_DMPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDMCODFIS_1_36.value==this.w_DMCODFIS)
      this.oPgFrm.Page1.oPag.oDMCODFIS_1_36.value=this.w_DMCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDMSCLGEN_1_59.RadioValue()==this.w_DMSCLGEN)
      this.oPgFrm.Page1.oPag.oDMSCLGEN_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDMDACONT_1_60.RadioValue()==this.w_DMDACONT)
      this.oPgFrm.Page1.oPag.oDMDACONT_1_60.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DATESTSM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not((.w_Nacodiso='SM' And .w_Nacodest='037') Or Empty(.w_Dmcodcon))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto non di tipologia San Marino")
          case   not(.w_DM__MESE>=1 AND .w_DM__MESE<=12)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDM__MESE_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMDESCRI))  and (.cFunction<>'Query' And  .w_DMPERFIS$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMDESCRI_1_25.SetFocus()
            i_bnoObbl = !empty(.w_DMDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMCOGNOM))  and (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMCOGNOM_1_26.SetFocus()
            i_bnoObbl = !empty(.w_DMCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DM__NOME))  and (.cFunction<>'Query' And .w_DMPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDM__NOME_1_27.SetFocus()
            i_bnoObbl = !empty(.w_DM__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMDATNAS))  and (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMDATNAS_1_28.SetFocus()
            i_bnoObbl = !empty(.w_DMDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMCOMEST))  and (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMCOMEST_1_29.SetFocus()
            i_bnoObbl = !empty(.w_DMCOMEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMSTADOM))  and (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMSTADOM_1_31.SetFocus()
            i_bnoObbl = !empty(.w_DMSTADOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMSTAEST))  and (.cFunction<>'Query' And .w_DMPERFIS$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMSTAEST_1_32.SetFocus()
            i_bnoObbl = !empty(.w_DMSTAEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMCITEST))  and (.cFunction<>'Query' AND .w_DMPERFIS$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMCITEST_1_33.SetFocus()
            i_bnoObbl = !empty(.w_DMCITEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMINDEST))  and (.cFunction<>'Query' And .w_DMPERFIS$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMINDEST_1_34.SetFocus()
            i_bnoObbl = !empty(.w_DMINDEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DMPARIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDMPARIVA_1_35.SetFocus()
            i_bnoObbl = !empty(.w_DMPARIVA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAI_MSA.CheckForm()
      if i_bres
        i_bres=  .GSAI_MSA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DMCODCON = this.w_DMCODCON
    this.o_DMPERFIS = this.w_DMPERFIS
    * --- GSAI_MSA : Depends On
    this.GSAI_MSA.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsai_admPag1 as StdContainer
  Width  = 792
  height = 573
  stdWidth  = 792
  stdheight = 573
  resizeXpos=589
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDM__ANNO_1_2 as StdField with uid="HYNPMMDCOL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DM__ANNO", cQueryName = "DM__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 216742533,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=242, Top=18, cSayPict='"9999"', cGetPict='"9999"'

  add object oDM__MESE_1_4 as StdField with uid="NCANBKUKPL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DM__MESE", cQueryName = "DM__MESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese/trimestre di riferimento",;
    HelpContextID = 190104965,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=385, Top=18

  func oDM__MESE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DM__MESE>=1 AND .w_DM__MESE<=12)
    endwith
    return bRes
  endfunc


  add object oDMTIPCON_1_7 as StdCombo with uid="FAVUWBBZYH",rtseq=5,rtrep=.f.,left=162,top=84,width=118,height=22;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 46434948;
    , cFormVar="w_DMTIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDMTIPCON_1_7.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDMTIPCON_1_7.GetRadio()
    this.Parent.oContained.w_DMTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDMTIPCON_1_7.SetRadio()
    this.Parent.oContained.w_DMTIPCON=trim(this.Parent.oContained.w_DMTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DMTIPCON=='C',1,;
      iif(this.Parent.oContained.w_DMTIPCON=='F',2,;
      0))
  endfunc

  func oDMTIPCON_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oDMTIPCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DMCODCON)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDMCODCON_1_8 as StdField with uid="EAHXPIIFWI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DMCODCON", cQueryName = "DMCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 34175620,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=285, Top=85, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DMTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DMCODCON"

  func oDMCODCON_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oDMCODCON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oDMCODCON_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDMCODCON_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DMTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DMTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDMCODCON_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'Gsai_Adm.CONTI_VZM',this.parent.oContained
  endproc
  proc oDMCODCON_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DMTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DMCODCON
     i_obj.ecpSave()
  endproc


  add object oDMPERFIS_1_24 as StdCombo with uid="IZUNFFGLMN",rtseq=22,rtrep=.f.,left=519,top=84,width=178,height=22;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 98585225;
    , cFormVar="w_DMPERFIS",RowSource=""+"Persona giuridica,"+"Persona fisica,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDMPERFIS_1_24.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oDMPERFIS_1_24.GetRadio()
    this.Parent.oContained.w_DMPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oDMPERFIS_1_24.SetRadio()
    this.Parent.oContained.w_DMPERFIS=trim(this.Parent.oContained.w_DMPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_DMPERFIS=='N',1,;
      iif(this.Parent.oContained.w_DMPERFIS=='P',2,;
      iif(this.Parent.oContained.w_DMPERFIS=='S',3,;
      0)))
  endfunc

  add object oDMDESCRI_1_25 as StdField with uid="EPFAFIXABF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DMDESCRI", cQueryName = "DMDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 219182465,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=160, Top=136, InputMask=replicate('X',60)

  func oDMDESCRI_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And  .w_DMPERFIS$'NS')
    endwith
   endif
  endfunc

  add object oDMCOGNOM_1_26 as StdField with uid="VBWHBYPETD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DMCOGNOM", cQueryName = "DMCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 221870723,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=160, Top=188, InputMask=replicate('X',40)

  func oDMCOGNOM_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
    endwith
   endif
  endfunc

  add object oDM__NOME_1_27 as StdField with uid="TZTOWOZVVJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DM__NOME", cQueryName = "DM__NOME",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 247151227,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=458, Top=188, InputMask=replicate('X',40)

  func oDM__NOME_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DMPERFIS$'PS')
    endwith
   endif
  endfunc

  add object oDMDATNAS_1_28 as StdField with uid="VTKSSEXLQD",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DMDATNAS", cQueryName = "DMDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 33846647,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=247

  func oDMDATNAS_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
    endwith
   endif
  endfunc

  add object oDMCOMEST_1_29 as StdField with uid="OEGUAGVHVP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DMCOMEST", cQueryName = "DMCOMEST",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 191268214,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=277, Top=247, InputMask=replicate('X',40), bHasZoom = .t. 

  func oDMCOMEST_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
    endwith
   endif
  endfunc

  proc oDMCOMEST_1_29.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_DMCOMEST",".w_DMPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDMPRONAS_1_30 as StdField with uid="HCPUHURXUB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DMPRONAS", cQueryName = "DMPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 37926263,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=677, Top=247, InputMask=replicate('X',2), bHasZoom = .t. 

  func oDMPRONAS_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
    endwith
   endif
  endfunc

  proc oDMPRONAS_1_30.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_DMPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDMSTADOM_1_31 as StdField with uid="JPPHEZDBPD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DMSTADOM", cQueryName = "DMSTADOM",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero del domicilio fiscale",;
    HelpContextID = 48200323,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=309, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oDMSTADOM_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DMPERFIS$'PS')
    endwith
   endif
  endfunc

  add object oDMSTAEST_1_32 as StdField with uid="JZJZNJGHQI",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DMSTAEST", cQueryName = "DMSTAEST",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 203457910,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=377, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oDMSTAEST_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DMPERFIS$'NS')
    endwith
   endif
  endfunc

  add object oDMCITEST_1_33 as StdField with uid="VZATVQHIMD",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DMCITEST", cQueryName = "DMCITEST",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Citt� estera della sede legale",;
    HelpContextID = 184321398,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=278, Top=377, InputMask=replicate('X',40)

  func oDMCITEST_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_DMPERFIS$'NS')
    endwith
   endif
  endfunc

  add object oDMINDEST_1_34 as StdField with uid="PPBQNCJMIR",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DMINDEST", cQueryName = "DMINDEST",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero della sede legale",;
    HelpContextID = 200746358,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=160, Top=439, InputMask=replicate('X',40)

  func oDMINDEST_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DMPERFIS$'NS')
    endwith
   endif
  endfunc

  add object oDMPARIVA_1_35 as StdField with uid="AOJRIDAGNZ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DMPARIVA", cQueryName = "DMPARIVA",;
    bObbl = .t. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 119780745,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=160, Top=506, InputMask=replicate('X',25)

  add object oDMCODFIS_1_36 as StdField with uid="NEICLLJLQF",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DMCODFIS", cQueryName = "DMCODFIS",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 84507273,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=475, Top=506, InputMask=replicate('X',25)

  add object oDMSCLGEN_1_59 as StdCheck with uid="FZBDCCDEUF",rtseq=36,rtrep=.f.,left=592, top=18, caption="Escludi da generazione",;
    ToolTipText = "Se attivo, esclude i dati estratti dalla generazione",;
    HelpContextID = 159483260,;
    cFormVar="w_DMSCLGEN", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDMSCLGEN_1_59.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDMSCLGEN_1_59.GetRadio()
    this.Parent.oContained.w_DMSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oDMSCLGEN_1_59.SetRadio()
    this.Parent.oContained.w_DMSCLGEN=trim(this.Parent.oContained.w_DMSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_DMSCLGEN=='S',1,;
      0)
  endfunc

  add object oDMDACONT_1_60 as StdCheck with uid="NMLXKBMXSE",rtseq=37,rtrep=.f.,left=592, top=49, caption="Da verificare",;
    ToolTipText = "Se attivo, i dati estratti sono da verificare",;
    HelpContextID = 233540234,;
    cFormVar="w_DMDACONT", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDMDACONT_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDMDACONT_1_60.GetRadio()
    this.Parent.oContained.w_DMDACONT = this.RadioValue()
    return .t.
  endfunc

  func oDMDACONT_1_60.SetRadio()
    this.Parent.oContained.w_DMDACONT=trim(this.Parent.oContained.w_DMDACONT)
    this.value = ;
      iif(this.Parent.oContained.w_DMDACONT=='S',1,;
      0)
  endfunc

  add object oStr_1_3 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=169, Top=18,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="JGEELRUKLD",Visible=.t., Left=303, Top=18,;
    Alignment=1, Width=74, Height=18,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=160, Top=233,;
    Alignment=0, Width=76, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=277, Top=233,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune (o stato estero) di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="NOWYYKZWAZ",Visible=.t., Left=160, Top=361,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="DMKPRROUYT",Visible=.t., Left=668, Top=233,;
    Alignment=0, Width=52, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="XCNUOVRHIB",Visible=.t., Left=6, Top=503,;
    Alignment=0, Width=111, Height=18,;
    Caption="Identificativi fiscali"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="LHYQUAFBCO",Visible=.t., Left=160, Top=489,;
    Alignment=0, Width=195, Height=13,;
    Caption="Identificativo fiscale/Identificativo IVA"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_44 as StdString with uid="BSYUVARGFL",Visible=.t., Left=475, Top=489,;
    Alignment=0, Width=142, Height=17,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=7, Top=110,;
    Alignment=0, Width=111, Height=19,;
    Caption="Dati anagrafici"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="FZWRBKVHFS",Visible=.t., Left=7, Top=31,;
    Alignment=0, Width=118, Height=18,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="OBGOXQVJWR",Visible=.t., Left=160, Top=118,;
    Alignment=0, Width=130, Height=17,;
    Caption="Ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="KOKISJQUOL",Visible=.t., Left=160, Top=172,;
    Alignment=0, Width=130, Height=17,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_53 as StdString with uid="GZLERJIXDA",Visible=.t., Left=459, Top=172,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_54 as StdString with uid="ANHHASVODD",Visible=.t., Left=160, Top=296,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="TPWEYQUKDS",Visible=.t., Left=7, Top=297,;
    Alignment=0, Width=111, Height=18,;
    Caption="Domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="WQAUIMBAAE",Visible=.t., Left=278, Top=361,;
    Alignment=0, Width=83, Height=13,;
    Caption="Citt� estera"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="KBZOHAZPBS",Visible=.t., Left=7, Top=380,;
    Alignment=0, Width=111, Height=18,;
    Caption="Sede legale"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="HGABSVUXLY",Visible=.t., Left=160, Top=425,;
    Alignment=0, Width=83, Height=13,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="DSOQGUGCHN",Visible=.t., Left=419, Top=87,;
    Alignment=1, Width=94, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oBox_1_41 as StdBox with uid="RDMWYOEEFE",left=1, top=77, width=787,height=464

  add object oBox_1_45 as StdBox with uid="CMPEHMCRJP",left=1, top=483, width=763,height=2

  add object oBox_1_46 as StdBox with uid="ZGZUPYKXSJ",left=1, top=345, width=763,height=2

  add object oBox_1_48 as StdBox with uid="PIUTDLGCGZ",left=149, top=77, width=1,height=462
enddefine
define class tgsai_admPag2 as StdContainer
  Width  = 792
  height = 573
  stdWidth  = 792
  stdheight = 573
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="VNMLGJNPFU",left=1, top=10, width=722, height=562, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsai_msa",lower(this.oContained.GSAI_MSA.class))=0
        this.oContained.GSAI_MSA.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_adm','DATESTSM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DMSERIAL=DATESTSM.DMSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
