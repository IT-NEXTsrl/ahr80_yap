* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_ade                                                        *
*              Anagrafica dati estratti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-23                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_ade"))

* --- Class definition
define class tgsai_ade as StdForm
  Top    = 4
  Left   = 9

  * --- Standard Properties
  Width  = 804
  Height = 540+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=92931945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=55

  * --- Constant Properties
  ANTIVADE_IDX = 0
  CONTI_IDX = 0
  SOGCOLNR_IDX = 0
  NAZIONI_IDX = 0
  cFile = "ANTIVADE"
  cKeySelect = "DESERIAL"
  cKeyWhere  = "DESERIAL=this.w_DESERIAL"
  cKeyWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';

  cKeyWhereODBCqualified = '"ANTIVADE.DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';

  cPrg = "gsai_ade"
  cComment = "Anagrafica dati estratti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DESERIAL = space(10)
  w_DE__ANNO = 0
  w_DETIPCON = space(1)
  o_DETIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DECODCON = space(15)
  o_DECODCON = space(15)
  w_SCCODCLF = space(15)
  w_CHCODCLF = space(15)
  w_SCCOGSCN = space(24)
  w_SCNOMSCN = space(20)
  w_SCDTNSCN = ctod('  /  /  ')
  w_SCLCRSCN = space(40)
  w_SCPRNSCN = space(2)
  w_SCSESDOM = space(3)
  w_ANPARIVA = space(12)
  w_ANFLPRIV = space(1)
  w_ANFLSGRE = space(1)
  w_ANCODFIS = space(16)
  w_ANPERFIS = space(1)
  w_ANDESCRI = space(40)
  w_ANCOGNOM = space(40)
  w_AN__NOME = space(20)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANNAZION = space(3)
  w_NACODEST = space(5)
  w_ANLOCALI = space(30)
  w_ANINDIRI = space(35)
  w_DETIPSOG = space(1)
  o_DETIPSOG = space(1)
  w_SOGESTPER = space(1)
  o_SOGESTPER = space(1)
  w_DESCLGEN = space(1)
  w_DEDESCRI = space(60)
  w_DECOGNOM = space(40)
  w_DE__NOME = space(40)
  w_DEDATNAS = ctod('  /  /  ')
  w_DECOMEST = space(40)
  w_DEPRONAS = space(2)
  w_DESTADOM = space(3)
  w_DESTAEST = space(3)
  w_DECITEST = space(40)
  w_DEINDEST = space(40)
  w_DEPARIVA = space(25)
  w_DECODFIS = space(25)
  w_AREA_1 = space(1)
  w_AREA_2 = space(1)
  w_EDIT_DETIPSOG = .F.
  w_DECODGEN = space(10)
  w_AREA_4 = space(1)
  w_EDIT_DETIPSOG2 = .F.
  w_DEDACONT = space(1)
  w_CONTROLS = .F.
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DESERIAL = this.W_DESERIAL

  * --- Children pointers
  GSAI_MDE = .NULL.
  GSAI2MDE = .NULL.
  GSAI4MDE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANTIVADE','gsai_ade')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_adePag1","gsai_ade",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati anagrafici")
      .Pages(1).HelpContextID = 136108281
      .Pages(2).addobject("oPag","tgsai_adePag2","gsai_ade",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Record tipo 1")
      .Pages(2).HelpContextID = 155384346
      .Pages(3).addobject("oPag","tgsai_adePag3","gsai_ade",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Record tipo 2")
      .Pages(3).HelpContextID = 156432922
      .Pages(4).addobject("oPag","tgsai_adePag4","gsai_ade",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Record tipo 4")
      .Pages(4).HelpContextID = 158530074
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='SOGCOLNR'
    this.cWorkTables[3]='NAZIONI'
    this.cWorkTables[4]='ANTIVADE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVADE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVADE_IDX,3]
  return

  function CreateChildren()
    this.GSAI_MDE = CREATEOBJECT('stdDynamicChild',this,'GSAI_MDE',this.oPgFrm.Page2.oPag.oLinkPC_2_2)
    this.GSAI_MDE.createrealchild()
    this.GSAI2MDE = CREATEOBJECT('stdDynamicChild',this,'GSAI2MDE',this.oPgFrm.Page3.oPag.oLinkPC_3_2)
    this.GSAI2MDE.createrealchild()
    this.GSAI4MDE = CREATEOBJECT('stdDynamicChild',this,'GSAI4MDE',this.oPgFrm.Page4.oPag.oLinkPC_4_1)
    this.GSAI4MDE.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAI_MDE)
      this.GSAI_MDE.DestroyChildrenChain()
      this.GSAI_MDE=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_2')
    if !ISNULL(this.GSAI2MDE)
      this.GSAI2MDE.DestroyChildrenChain()
      this.GSAI2MDE=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_2')
    if !ISNULL(this.GSAI4MDE)
      this.GSAI4MDE.DestroyChildrenChain()
      this.GSAI4MDE=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAI_MDE.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAI2MDE.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAI4MDE.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAI_MDE.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAI2MDE.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAI4MDE.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAI_MDE.NewDocument()
    this.GSAI2MDE.NewDocument()
    this.GSAI4MDE.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAI_MDE.SetKey(;
            .w_DESERIAL,"DESERIAL";
            ,.w_AREA_1,"DE__AREA";
            )
      this.GSAI2MDE.SetKey(;
            .w_DESERIAL,"DESERIAL";
            ,.w_AREA_2,"DE__AREA";
            )
      this.GSAI4MDE.SetKey(;
            .w_DESERIAL,"DESERIAL";
            ,.w_AREA_4,"DE__AREA";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAI_MDE.ChangeRow(this.cRowID+'      1',1;
             ,.w_DESERIAL,"DESERIAL";
             ,.w_AREA_1,"DE__AREA";
             )
      .GSAI2MDE.ChangeRow(this.cRowID+'      1',1;
             ,.w_DESERIAL,"DESERIAL";
             ,.w_AREA_2,"DE__AREA";
             )
      .GSAI4MDE.ChangeRow(this.cRowID+'      1',1;
             ,.w_DESERIAL,"DESERIAL";
             ,.w_AREA_4,"DE__AREA";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAI_MDE)
        i_f=.GSAI_MDE.BuildFilter()
        if !(i_f==.GSAI_MDE.cQueryFilter)
          i_fnidx=.GSAI_MDE.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MDE.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MDE.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MDE.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MDE.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAI2MDE)
        i_f=.GSAI2MDE.BuildFilter()
        if !(i_f==.GSAI2MDE.cQueryFilter)
          i_fnidx=.GSAI2MDE.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI2MDE.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI2MDE.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI2MDE.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI2MDE.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAI4MDE)
        i_f=.GSAI4MDE.BuildFilter()
        if !(i_f==.GSAI4MDE.cQueryFilter)
          i_fnidx=.GSAI4MDE.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI4MDE.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI4MDE.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI4MDE.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI4MDE.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DESERIAL = NVL(DESERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANTIVADE where DESERIAL=KeySet.DESERIAL
    *
    i_nConn = i_TableProp[this.ANTIVADE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVADE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVADE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVADE '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_CHCODCLF = space(15)
        .w_SCCOGSCN = space(24)
        .w_SCNOMSCN = space(20)
        .w_SCDTNSCN = ctod("  /  /  ")
        .w_SCLCRSCN = space(40)
        .w_SCPRNSCN = space(2)
        .w_SCSESDOM = space(3)
        .w_ANPARIVA = space(12)
        .w_ANFLPRIV = space(1)
        .w_ANFLSGRE = space(1)
        .w_ANCODFIS = space(16)
        .w_ANPERFIS = space(1)
        .w_ANDESCRI = space(40)
        .w_ANCOGNOM = space(40)
        .w_AN__NOME = space(20)
        .w_ANDATNAS = ctod("  /  /  ")
        .w_ANLOCNAS = space(30)
        .w_ANPRONAS = space(2)
        .w_ANNAZION = space(3)
        .w_NACODEST = space(5)
        .w_ANLOCALI = space(30)
        .w_ANINDIRI = space(35)
        .w_SOGESTPER = 'N'
        .w_AREA_1 = "1"
        .w_AREA_2 = "2"
        .w_EDIT_DETIPSOG = .T.
        .w_AREA_4 = "4"
        .w_EDIT_DETIPSOG2 = .T.
        .w_CONTROLS = .f.
        .w_DESERIAL = NVL(DESERIAL,space(10))
        .op_DESERIAL = .w_DESERIAL
        .w_DE__ANNO = NVL(DE__ANNO,0)
        .w_DETIPCON = NVL(DETIPCON,space(1))
        .w_DECODCON = NVL(DECODCON,space(15))
          if link_1_5_joined
            this.w_DECODCON = NVL(ANCODICE105,NVL(this.w_DECODCON,space(15)))
            this.w_ANPARIVA = NVL(ANPARIVA105,space(12))
            this.w_ANFLPRIV = NVL(ANFLPRIV105,space(1))
            this.w_ANFLSGRE = NVL(ANFLSGRE105,space(1))
            this.w_ANCODFIS = NVL(ANCODFIS105,space(16))
            this.w_ANPERFIS = NVL(ANPERFIS105,space(1))
            this.w_ANDESCRI = NVL(ANDESCRI105,space(40))
            this.w_ANCOGNOM = NVL(ANCOGNOM105,space(40))
            this.w_AN__NOME = NVL(AN__NOME105,space(20))
            this.w_ANDATNAS = NVL(cp_ToDate(ANDATNAS105),ctod("  /  /  "))
            this.w_ANLOCNAS = NVL(ANLOCNAS105,space(30))
            this.w_ANPRONAS = NVL(ANPRONAS105,space(2))
            this.w_ANNAZION = NVL(ANNAZION105,space(3))
            this.w_ANLOCALI = NVL(ANLOCALI105,space(30))
            this.w_ANINDIRI = NVL(ANINDIRI105,space(35))
          else
          .link_1_5('Load')
          endif
        .w_SCCODCLF = .w_DECODCON
          .link_1_6('Load')
          .link_1_7('Load')
          .link_1_25('Load')
        .w_DETIPSOG = NVL(DETIPSOG,space(1))
        .w_DESCLGEN = NVL(DESCLGEN,space(1))
        .w_DEDESCRI = NVL(DEDESCRI,space(60))
        .w_DECOGNOM = NVL(DECOGNOM,space(40))
        .w_DE__NOME = NVL(DE__NOME,space(40))
        .w_DEDATNAS = NVL(cp_ToDate(DEDATNAS),ctod("  /  /  "))
        .w_DECOMEST = NVL(DECOMEST,space(40))
        .w_DEPRONAS = NVL(DEPRONAS,space(2))
        .w_DESTADOM = NVL(DESTADOM,space(3))
        .w_DESTAEST = NVL(DESTAEST,space(3))
        .w_DECITEST = NVL(DECITEST,space(40))
        .w_DEINDEST = NVL(DEINDEST,space(40))
        .w_DEPARIVA = NVL(DEPARIVA,space(25))
        .w_DECODFIS = NVL(DECODFIS,space(25))
        .w_DECODGEN = NVL(DECODGEN,space(10))
        .w_DEDACONT = NVL(DEDACONT,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ANTIVADE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DESERIAL = space(10)
      .w_DE__ANNO = 0
      .w_DETIPCON = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DECODCON = space(15)
      .w_SCCODCLF = space(15)
      .w_CHCODCLF = space(15)
      .w_SCCOGSCN = space(24)
      .w_SCNOMSCN = space(20)
      .w_SCDTNSCN = ctod("  /  /  ")
      .w_SCLCRSCN = space(40)
      .w_SCPRNSCN = space(2)
      .w_SCSESDOM = space(3)
      .w_ANPARIVA = space(12)
      .w_ANFLPRIV = space(1)
      .w_ANFLSGRE = space(1)
      .w_ANCODFIS = space(16)
      .w_ANPERFIS = space(1)
      .w_ANDESCRI = space(40)
      .w_ANCOGNOM = space(40)
      .w_AN__NOME = space(20)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANNAZION = space(3)
      .w_NACODEST = space(5)
      .w_ANLOCALI = space(30)
      .w_ANINDIRI = space(35)
      .w_DETIPSOG = space(1)
      .w_SOGESTPER = space(1)
      .w_DESCLGEN = space(1)
      .w_DEDESCRI = space(60)
      .w_DECOGNOM = space(40)
      .w_DE__NOME = space(40)
      .w_DEDATNAS = ctod("  /  /  ")
      .w_DECOMEST = space(40)
      .w_DEPRONAS = space(2)
      .w_DESTADOM = space(3)
      .w_DESTAEST = space(3)
      .w_DECITEST = space(40)
      .w_DEINDEST = space(40)
      .w_DEPARIVA = space(25)
      .w_DECODFIS = space(25)
      .w_AREA_1 = space(1)
      .w_AREA_2 = space(1)
      .w_EDIT_DETIPSOG = .f.
      .w_DECODGEN = space(10)
      .w_AREA_4 = space(1)
      .w_EDIT_DETIPSOG2 = .f.
      .w_DEDACONT = space(1)
      .w_CONTROLS = .f.
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DE__ANNO = year(i_DATSYS)
        .w_DETIPCON = 'C'
        .w_OBTEST = CTOD('01-01-1900')
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_DECODCON))
          .link_1_5('Full')
          endif
        .w_SCCODCLF = .w_DECODCON
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_SCCODCLF))
          .link_1_6('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_CHCODCLF))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,25,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_25('Full')
          endif
          .DoRTCalc(26,28,.f.)
        .w_DETIPSOG = IIF(.w_ANFLPRIV='S' And .w_ANFLSGRE<>'S','P',IIF(.w_ANFLSGRE='S','N',IIF(.w_ANFLPRIV<>'S' And .w_ANFLSGRE<>'S','I',' ')))
        .w_SOGESTPER = 'N'
        .w_DESCLGEN = 'N'
        .w_DEDESCRI = .w_ANDESCRI
        .w_DECOGNOM = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCCOGSCN,LEFT(.w_ANCOGNOM,40)),SPACE(40))
        .w_DE__NOME = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCNOMSCN,LEFT(.w_AN__NOME,40)),SPACE(40))
        .w_DEDATNAS = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCDTNSCN,.w_ANDATNAS),CTOD('  -  -   '))
        .w_DECOMEST = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCLCRSCN,.w_ANLOCNAS),SPACE(40))
        .w_DEPRONAS = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCPRNSCN,.w_ANPRONAS),SPACE(2))
        .w_DESTADOM = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCSESDOM,LEFT(.w_NACODEST,3)),SPACE(3))
        .w_DESTAEST = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'NS',LEFT(.w_NACODEST,3),SPACE(3))
        .w_DECITEST = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'NS',.w_ANLOCALI,SPACE(40))
        .w_DEINDEST = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'NS',.w_ANINDIRI,SPACE(40))
        .w_DEPARIVA = ALLTRIM(.w_ANPARIVA)
        .w_DECODFIS = .w_ANCODFIS
        .w_AREA_1 = "1"
        .w_AREA_2 = "2"
        .w_EDIT_DETIPSOG = .T.
        .w_DECODGEN = space(10)
        .w_AREA_4 = "4"
        .w_EDIT_DETIPSOG2 = .T.
        .w_DEDACONT = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVADE')
    this.DoRTCalc(51,55,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVADE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"ANTIE","i_codazi,w_DESERIAL")
      .op_codazi = .w_codazi
      .op_DESERIAL = .w_DESERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDE__ANNO_1_2.enabled = i_bVal
      .Page1.oPag.oDETIPCON_1_3.enabled = i_bVal
      .Page1.oPag.oDECODCON_1_5.enabled = i_bVal
      .Page1.oPag.oDETIPSOG_1_29.enabled = i_bVal
      .Page1.oPag.oSOGESTPER_1_30.enabled = i_bVal
      .Page1.oPag.oDESCLGEN_1_32.enabled = i_bVal
      .Page1.oPag.oDEDESCRI_1_33.enabled = i_bVal
      .Page1.oPag.oDECOGNOM_1_34.enabled = i_bVal
      .Page1.oPag.oDE__NOME_1_35.enabled = i_bVal
      .Page1.oPag.oDEDATNAS_1_49.enabled = i_bVal
      .Page1.oPag.oDECOMEST_1_50.enabled = i_bVal
      .Page1.oPag.oDEPRONAS_1_51.enabled = i_bVal
      .Page1.oPag.oDESTADOM_1_52.enabled = i_bVal
      .Page1.oPag.oDESTAEST_1_53.enabled = i_bVal
      .Page1.oPag.oDECITEST_1_55.enabled = i_bVal
      .Page1.oPag.oDEINDEST_1_56.enabled = i_bVal
      .Page1.oPag.oDEPARIVA_1_58.enabled = i_bVal
      .Page1.oPag.oDECODFIS_1_60.enabled = i_bVal
      .Page1.oPag.oDEDACONT_1_73.enabled = i_bVal
    endwith
    this.GSAI_MDE.SetStatus(i_cOp)
    this.GSAI2MDE.SetStatus(i_cOp)
    this.GSAI4MDE.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ANTIVADE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAI_MDE.SetChildrenStatus(i_cOp)
  *  this.GSAI2MDE.SetChildrenStatus(i_cOp)
  *  this.GSAI4MDE.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVADE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESERIAL,"DESERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__ANNO,"DE__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DETIPCON,"DETIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODCON,"DECODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DETIPSOG,"DETIPSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESCLGEN,"DESCLGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDESCRI,"DEDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECOGNOM,"DECOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__NOME,"DE__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDATNAS,"DEDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECOMEST,"DECOMEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEPRONAS,"DEPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESTADOM,"DESTADOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESTAEST,"DESTAEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECITEST,"DECITEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEINDEST,"DEINDEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEPARIVA,"DEPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODFIS,"DECODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODGEN,"DECODGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDACONT,"DEDACONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVADE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADE_IDX,2])
    i_lTable = "ANTIVADE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANTIVADE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVADE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANTIVADE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"ANTIE","i_codazi,w_DESERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANTIVADE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVADE')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVADE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DESERIAL,DE__ANNO,DETIPCON,DECODCON,DETIPSOG"+;
                  ",DESCLGEN,DEDESCRI,DECOGNOM,DE__NOME,DEDATNAS"+;
                  ",DECOMEST,DEPRONAS,DESTADOM,DESTAEST,DECITEST"+;
                  ",DEINDEST,DEPARIVA,DECODFIS,DECODGEN,DEDACONT"+;
                  ",UTCC,UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DESERIAL)+;
                  ","+cp_ToStrODBC(this.w_DE__ANNO)+;
                  ","+cp_ToStrODBC(this.w_DETIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_DECODCON)+;
                  ","+cp_ToStrODBC(this.w_DETIPSOG)+;
                  ","+cp_ToStrODBC(this.w_DESCLGEN)+;
                  ","+cp_ToStrODBC(this.w_DEDESCRI)+;
                  ","+cp_ToStrODBC(this.w_DECOGNOM)+;
                  ","+cp_ToStrODBC(this.w_DE__NOME)+;
                  ","+cp_ToStrODBC(this.w_DEDATNAS)+;
                  ","+cp_ToStrODBC(this.w_DECOMEST)+;
                  ","+cp_ToStrODBC(this.w_DEPRONAS)+;
                  ","+cp_ToStrODBC(this.w_DESTADOM)+;
                  ","+cp_ToStrODBC(this.w_DESTAEST)+;
                  ","+cp_ToStrODBC(this.w_DECITEST)+;
                  ","+cp_ToStrODBC(this.w_DEINDEST)+;
                  ","+cp_ToStrODBC(this.w_DEPARIVA)+;
                  ","+cp_ToStrODBC(this.w_DECODFIS)+;
                  ","+cp_ToStrODBC(this.w_DECODGEN)+;
                  ","+cp_ToStrODBC(this.w_DEDACONT)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVADE')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVADE')
        cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL)
        INSERT INTO (i_cTable);
              (DESERIAL,DE__ANNO,DETIPCON,DECODCON,DETIPSOG,DESCLGEN,DEDESCRI,DECOGNOM,DE__NOME,DEDATNAS,DECOMEST,DEPRONAS,DESTADOM,DESTAEST,DECITEST,DEINDEST,DEPARIVA,DECODFIS,DECODGEN,DEDACONT,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DESERIAL;
                  ,this.w_DE__ANNO;
                  ,this.w_DETIPCON;
                  ,this.w_DECODCON;
                  ,this.w_DETIPSOG;
                  ,this.w_DESCLGEN;
                  ,this.w_DEDESCRI;
                  ,this.w_DECOGNOM;
                  ,this.w_DE__NOME;
                  ,this.w_DEDATNAS;
                  ,this.w_DECOMEST;
                  ,this.w_DEPRONAS;
                  ,this.w_DESTADOM;
                  ,this.w_DESTAEST;
                  ,this.w_DECITEST;
                  ,this.w_DEINDEST;
                  ,this.w_DEPARIVA;
                  ,this.w_DECODFIS;
                  ,this.w_DECODGEN;
                  ,this.w_DEDACONT;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANTIVADE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANTIVADE_IDX,i_nConn)
      *
      * update ANTIVADE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVADE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DE__ANNO="+cp_ToStrODBC(this.w_DE__ANNO)+;
             ",DETIPCON="+cp_ToStrODBC(this.w_DETIPCON)+;
             ",DECODCON="+cp_ToStrODBCNull(this.w_DECODCON)+;
             ",DETIPSOG="+cp_ToStrODBC(this.w_DETIPSOG)+;
             ",DESCLGEN="+cp_ToStrODBC(this.w_DESCLGEN)+;
             ",DEDESCRI="+cp_ToStrODBC(this.w_DEDESCRI)+;
             ",DECOGNOM="+cp_ToStrODBC(this.w_DECOGNOM)+;
             ",DE__NOME="+cp_ToStrODBC(this.w_DE__NOME)+;
             ",DEDATNAS="+cp_ToStrODBC(this.w_DEDATNAS)+;
             ",DECOMEST="+cp_ToStrODBC(this.w_DECOMEST)+;
             ",DEPRONAS="+cp_ToStrODBC(this.w_DEPRONAS)+;
             ",DESTADOM="+cp_ToStrODBC(this.w_DESTADOM)+;
             ",DESTAEST="+cp_ToStrODBC(this.w_DESTAEST)+;
             ",DECITEST="+cp_ToStrODBC(this.w_DECITEST)+;
             ",DEINDEST="+cp_ToStrODBC(this.w_DEINDEST)+;
             ",DEPARIVA="+cp_ToStrODBC(this.w_DEPARIVA)+;
             ",DECODFIS="+cp_ToStrODBC(this.w_DECODFIS)+;
             ",DECODGEN="+cp_ToStrODBC(this.w_DECODGEN)+;
             ",DEDACONT="+cp_ToStrODBC(this.w_DEDACONT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVADE')
        i_cWhere = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
        UPDATE (i_cTable) SET;
              DE__ANNO=this.w_DE__ANNO;
             ,DETIPCON=this.w_DETIPCON;
             ,DECODCON=this.w_DECODCON;
             ,DETIPSOG=this.w_DETIPSOG;
             ,DESCLGEN=this.w_DESCLGEN;
             ,DEDESCRI=this.w_DEDESCRI;
             ,DECOGNOM=this.w_DECOGNOM;
             ,DE__NOME=this.w_DE__NOME;
             ,DEDATNAS=this.w_DEDATNAS;
             ,DECOMEST=this.w_DECOMEST;
             ,DEPRONAS=this.w_DEPRONAS;
             ,DESTADOM=this.w_DESTADOM;
             ,DESTAEST=this.w_DESTAEST;
             ,DECITEST=this.w_DECITEST;
             ,DEINDEST=this.w_DEINDEST;
             ,DEPARIVA=this.w_DEPARIVA;
             ,DECODFIS=this.w_DECODFIS;
             ,DECODGEN=this.w_DECODGEN;
             ,DEDACONT=this.w_DEDACONT;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAI_MDE : Saving
      this.GSAI_MDE.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DESERIAL,"DESERIAL";
             ,this.w_AREA_1,"DE__AREA";
             )
      this.GSAI_MDE.mReplace()
      * --- GSAI2MDE : Saving
      this.GSAI2MDE.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DESERIAL,"DESERIAL";
             ,this.w_AREA_2,"DE__AREA";
             )
      this.GSAI2MDE.mReplace()
      * --- GSAI4MDE : Saving
      this.GSAI4MDE.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DESERIAL,"DESERIAL";
             ,this.w_AREA_4,"DE__AREA";
             )
      this.GSAI4MDE.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAI_MDE : Deleting
    this.GSAI_MDE.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DESERIAL,"DESERIAL";
           ,this.w_AREA_1,"DE__AREA";
           )
    this.GSAI_MDE.mDelete()
    * --- GSAI2MDE : Deleting
    this.GSAI2MDE.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DESERIAL,"DESERIAL";
           ,this.w_AREA_2,"DE__AREA";
           )
    this.GSAI2MDE.mDelete()
    * --- GSAI4MDE : Deleting
    this.GSAI4MDE.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DESERIAL,"DESERIAL";
           ,this.w_AREA_4,"DE__AREA";
           )
    this.GSAI4MDE.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVADE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANTIVADE_IDX,i_nConn)
      *
      * delete ANTIVADE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVADE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPCON<>.w_DETIPCON
            .w_SCCODCLF = .w_DECODCON
          .link_1_6('Full')
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPCON<>.w_DETIPCON
          .link_1_7('Full')
        endif
        .DoRTCalc(8,24,.t.)
          .link_1_25('Full')
        .DoRTCalc(26,28,.t.)
        if .o_DETIPCON<>.w_DETIPCON.or. .o_DECODCON<>.w_DECODCON
            .w_DETIPSOG = IIF(.w_ANFLPRIV='S' And .w_ANFLSGRE<>'S','P',IIF(.w_ANFLSGRE='S','N',IIF(.w_ANFLPRIV<>'S' And .w_ANFLSGRE<>'S','I',' ')))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG
          .Calculate_AJQKCDGKDI()
        endif
        .DoRTCalc(30,31,.t.)
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DEDESCRI = .w_ANDESCRI
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DECOGNOM = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCCOGSCN,LEFT(.w_ANCOGNOM,40)),SPACE(40))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DE__NOME = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCNOMSCN,LEFT(.w_AN__NOME,40)),SPACE(40))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DEDATNAS = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCDTNSCN,.w_ANDATNAS),CTOD('  -  -   '))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DECOMEST = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCLCRSCN,.w_ANLOCNAS),SPACE(40))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DEPRONAS = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCPRNSCN,.w_ANPRONAS),SPACE(2))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DESTADOM = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCSESDOM,LEFT(.w_NACODEST,3)),SPACE(3))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DESTAEST = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'NS',LEFT(.w_NACODEST,3),SPACE(3))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DECITEST = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'NS',.w_ANLOCALI,SPACE(40))
        endif
        if .o_DECODCON<>.w_DECODCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_SOGESTPER<>.w_SOGESTPER
            .w_DEINDEST = IIF(.w_DETIPSOG='N' AND .w_SOGESTPER$'NS',.w_ANINDIRI,SPACE(40))
        endif
        if .o_DECODCON<>.w_DECODCON
            .w_DEPARIVA = ALLTRIM(.w_ANPARIVA)
        endif
        if .o_DECODCON<>.w_DECODCON
            .w_DECODFIS = .w_ANCODFIS
        endif
        if .o_DETIPSOG<>.w_DETIPSOG
          .Calculate_CTITYWXDQV()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"ANTIE","i_codazi,w_DESERIAL")
          .op_DESERIAL = .w_DESERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(44,55,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_AJQKCDGKDI()
    with this
          * --- Controllo soggetto estero persona fisica/giuridica/altro
          .w_SOGESTPER = iif(.w_ANPERFIS='S' OR EMPTY(NVL(.w_DECODCON,'')) AND !EMPTY(.w_DECOGNOM) AND EMPTY(.w_DEDESCRI), 'P', iif(!EMPTY(NVL(.w_DECODCON,'')) AND .w_ANPERFIS<>'S' AND Empty(NVL(.w_CHCODCLF,'')) OR EMPTY(NVL(.w_DECODCON,'')) AND EMPTY(.w_DECOGNOM),'N','S'))
    endwith
  endproc
  proc Calculate_CTITYWXDQV()
    with this
          * --- Abilitazione pagine 
          .oPgFrm.Pages(3).Caption = IIF(.w_DETIPSOG='I',"Record tipo 2","Record tipo 3")
          .oPgFrm.Pages(4).Caption = IIF(.w_DETIPSOG $ 'IP',"Record tipo 4","Record tipo 5")
    endwith
  endproc
  proc Calculate_QZHCUKRTAL()
    with this
          * --- Editabilit� campo Detipsog
          .w_EDIT_DETIPSOG = Upper(this.GSAI_MDE.class)='STDDYNAMICCHILD' OR This.GSAI_MDE.w_Totnumdoc=0
          .w_EDIT_DETIPSOG2 = Upper(this.GSAI2MDE.class)='STDDYNAMICCHILD' OR This.GSAI2MDE.w_Totnumdoc=0
          .w_CONTROLS = This.mEnablecontrols()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDETIPCON_1_3.enabled = this.oPgFrm.Page1.oPag.oDETIPCON_1_3.mCond()
    this.oPgFrm.Page1.oPag.oDECODCON_1_5.enabled = this.oPgFrm.Page1.oPag.oDECODCON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oDETIPSOG_1_29.enabled = this.oPgFrm.Page1.oPag.oDETIPSOG_1_29.mCond()
    this.oPgFrm.Page1.oPag.oDEDESCRI_1_33.enabled = this.oPgFrm.Page1.oPag.oDEDESCRI_1_33.mCond()
    this.oPgFrm.Page1.oPag.oDECOGNOM_1_34.enabled = this.oPgFrm.Page1.oPag.oDECOGNOM_1_34.mCond()
    this.oPgFrm.Page1.oPag.oDE__NOME_1_35.enabled = this.oPgFrm.Page1.oPag.oDE__NOME_1_35.mCond()
    this.oPgFrm.Page1.oPag.oDEDATNAS_1_49.enabled = this.oPgFrm.Page1.oPag.oDEDATNAS_1_49.mCond()
    this.oPgFrm.Page1.oPag.oDECOMEST_1_50.enabled = this.oPgFrm.Page1.oPag.oDECOMEST_1_50.mCond()
    this.oPgFrm.Page1.oPag.oDEPRONAS_1_51.enabled = this.oPgFrm.Page1.oPag.oDEPRONAS_1_51.mCond()
    this.oPgFrm.Page1.oPag.oDESTADOM_1_52.enabled = this.oPgFrm.Page1.oPag.oDESTADOM_1_52.mCond()
    this.oPgFrm.Page1.oPag.oDESTAEST_1_53.enabled = this.oPgFrm.Page1.oPag.oDESTAEST_1_53.mCond()
    this.oPgFrm.Page1.oPag.oDECITEST_1_55.enabled = this.oPgFrm.Page1.oPag.oDECITEST_1_55.mCond()
    this.oPgFrm.Page1.oPag.oDEINDEST_1_56.enabled = this.oPgFrm.Page1.oPag.oDEINDEST_1_56.mCond()
    this.oPgFrm.Page1.oPag.oDEPARIVA_1_58.enabled = this.oPgFrm.Page1.oPag.oDEPARIVA_1_58.mCond()
    this.oPgFrm.Page1.oPag.oDECODFIS_1_60.enabled = this.oPgFrm.Page1.oPag.oDECODFIS_1_60.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(this.w_DETIPSOG $ 'IN')
    this.oPgFrm.Pages(3).enabled=not(this.w_DETIPSOG = 'P')
    this.oPgFrm.Page1.oPag.oSOGESTPER_1_30.visible=!this.oPgFrm.Page1.oPag.oSOGESTPER_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Load")
          .Calculate_AJQKCDGKDI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("Load") or lower(cEvent)==lower("New record")
          .Calculate_CTITYWXDQV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 1") or lower(cEvent)==lower("Edit Started")
          .Calculate_QZHCUKRTAL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DECODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DECODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DETIPCON;
                     ,'ANCODICE',trim(this.w_DECODCON))
          select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DECODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DECODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDECODCON_1_5'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'Record tipo 2/3 incongruenti con il tipo soggetto.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DETIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DECODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DETIPCON;
                       ,'ANCODICE',this.w_DECODCON)
            select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANPARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_ANFLPRIV = NVL(_Link_.ANFLPRIV,space(1))
      this.w_ANFLSGRE = NVL(_Link_.ANFLSGRE,space(1))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
      this.w_ANPERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANCOGNOM = NVL(_Link_.ANCOGNOM,space(40))
      this.w_AN__NOME = NVL(_Link_.AN__NOME,space(20))
      this.w_ANDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_ANLOCNAS = NVL(_Link_.ANLOCNAS,space(30))
      this.w_ANPRONAS = NVL(_Link_.ANPRONAS,space(2))
      this.w_ANNAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANLOCALI = NVL(_Link_.ANLOCALI,space(30))
      this.w_ANINDIRI = NVL(_Link_.ANINDIRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_DECODCON = space(15)
      endif
      this.w_ANPARIVA = space(12)
      this.w_ANFLPRIV = space(1)
      this.w_ANFLSGRE = space(1)
      this.w_ANCODFIS = space(16)
      this.w_ANPERFIS = space(1)
      this.w_ANDESCRI = space(40)
      this.w_ANCOGNOM = space(40)
      this.w_AN__NOME = space(20)
      this.w_ANDATNAS = ctod("  /  /  ")
      this.w_ANLOCNAS = space(30)
      this.w_ANPRONAS = space(2)
      this.w_ANNAZION = space(3)
      this.w_ANLOCALI = space(30)
      this.w_ANINDIRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_EDIT_DETIPSOG2 OR !(.w_ANFLPRIV='S' And .w_ANFLSGRE<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DECODCON = space(15)
        this.w_ANPARIVA = space(12)
        this.w_ANFLPRIV = space(1)
        this.w_ANFLSGRE = space(1)
        this.w_ANCODFIS = space(16)
        this.w_ANPERFIS = space(1)
        this.w_ANDESCRI = space(40)
        this.w_ANCOGNOM = space(40)
        this.w_AN__NOME = space(20)
        this.w_ANDATNAS = ctod("  /  /  ")
        this.w_ANLOCNAS = space(30)
        this.w_ANPRONAS = space(2)
        this.w_ANNAZION = space(3)
        this.w_ANLOCALI = space(30)
        this.w_ANINDIRI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 15 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+15<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ANCODICE as ANCODICE105"+ ",link_1_5.ANPARIVA as ANPARIVA105"+ ",link_1_5.ANFLPRIV as ANFLPRIV105"+ ",link_1_5.ANFLSGRE as ANFLSGRE105"+ ",link_1_5.ANCODFIS as ANCODFIS105"+ ",link_1_5.ANPERFIS as ANPERFIS105"+ ",link_1_5.ANDESCRI as ANDESCRI105"+ ",link_1_5.ANCOGNOM as ANCOGNOM105"+ ",link_1_5.AN__NOME as AN__NOME105"+ ",link_1_5.ANDATNAS as ANDATNAS105"+ ",link_1_5.ANLOCNAS as ANLOCNAS105"+ ",link_1_5.ANPRONAS as ANPRONAS105"+ ",link_1_5.ANNAZION as ANNAZION105"+ ",link_1_5.ANLOCALI as ANLOCALI105"+ ",link_1_5.ANINDIRI as ANINDIRI105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on ANTIVADE.DECODCON=link_1_5.ANCODICE"+" and ANTIVADE.DETIPCON=link_1_5.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and ANTIVADE.DECODCON=link_1_5.ANCODICE(+)"'+'+" and ANTIVADE.DETIPCON=link_1_5.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODCLF
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_lTable = "SOGCOLNR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2], .t., this.SOGCOLNR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODCLF="+cp_ToStrODBC(this.w_SCCODCLF);
                   +" and SCTIPCLF="+cp_ToStrODBC(this.w_DETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCTIPCLF',this.w_DETIPCON;
                       ,'SCCODCLF',this.w_SCCODCLF)
            select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_SCCOGSCN = NVL(_Link_.SCCOGSCN,space(24))
      this.w_SCNOMSCN = NVL(_Link_.SCNOMSCN,space(20))
      this.w_SCDTNSCN = NVL(cp_ToDate(_Link_.SCDTNSCN),ctod("  /  /  "))
      this.w_SCLCRSCN = NVL(_Link_.SCLCRSCN,space(40))
      this.w_SCPRNSCN = NVL(_Link_.SCPRNSCN,space(2))
      this.w_SCSESDOM = NVL(_Link_.SCSESDOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCLF = space(15)
      endif
      this.w_CHCODCLF = space(15)
      this.w_SCCOGSCN = space(24)
      this.w_SCNOMSCN = space(20)
      this.w_SCDTNSCN = ctod("  /  /  ")
      this.w_SCLCRSCN = space(40)
      this.w_SCPRNSCN = space(2)
      this.w_SCSESDOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])+'\'+cp_ToStr(_Link_.SCTIPCLF,1)+'\'+cp_ToStr(_Link_.SCCODCLF,1)
      cp_ShowWarn(i_cKey,this.SOGCOLNR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CHCODCLF
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_lTable = "SOGCOLNR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2], .t., this.SOGCOLNR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODCLF="+cp_ToStrODBC(this.w_CHCODCLF);
                   +" and SCTIPCLF="+cp_ToStrODBC(this.w_DETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCTIPCLF',this.w_DETIPCON;
                       ,'SCCODCLF',this.w_CHCODCLF)
            select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_SCCOGSCN = NVL(_Link_.SCCOGSCN,space(24))
      this.w_SCNOMSCN = NVL(_Link_.SCNOMSCN,space(20))
      this.w_SCDTNSCN = NVL(cp_ToDate(_Link_.SCDTNSCN),ctod("  /  /  "))
      this.w_SCLCRSCN = NVL(_Link_.SCLCRSCN,space(40))
      this.w_SCPRNSCN = NVL(_Link_.SCPRNSCN,space(2))
      this.w_SCSESDOM = NVL(_Link_.SCSESDOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CHCODCLF = space(15)
      endif
      this.w_CHCODCLF = space(15)
      this.w_SCCOGSCN = space(24)
      this.w_SCNOMSCN = space(20)
      this.w_SCDTNSCN = ctod("  /  /  ")
      this.w_SCLCRSCN = space(40)
      this.w_SCPRNSCN = space(2)
      this.w_SCSESDOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])+'\'+cp_ToStr(_Link_.SCTIPCLF,1)+'\'+cp_ToStr(_Link_.SCCODCLF,1)
      cp_ShowWarn(i_cKey,this.SOGCOLNR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODEST";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NACODEST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_NACODEST = NVL(_Link_.NACODEST,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_NACODEST = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDE__ANNO_1_2.value==this.w_DE__ANNO)
      this.oPgFrm.Page1.oPag.oDE__ANNO_1_2.value=this.w_DE__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPCON_1_3.RadioValue()==this.w_DETIPCON)
      this.oPgFrm.Page1.oPag.oDETIPCON_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDECODCON_1_5.value==this.w_DECODCON)
      this.oPgFrm.Page1.oPag.oDECODCON_1_5.value=this.w_DECODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPSOG_1_29.RadioValue()==this.w_DETIPSOG)
      this.oPgFrm.Page1.oPag.oDETIPSOG_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOGESTPER_1_30.RadioValue()==this.w_SOGESTPER)
      this.oPgFrm.Page1.oPag.oSOGESTPER_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLGEN_1_32.RadioValue()==this.w_DESCLGEN)
      this.oPgFrm.Page1.oPag.oDESCLGEN_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDESCRI_1_33.value==this.w_DEDESCRI)
      this.oPgFrm.Page1.oPag.oDEDESCRI_1_33.value=this.w_DEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDECOGNOM_1_34.value==this.w_DECOGNOM)
      this.oPgFrm.Page1.oPag.oDECOGNOM_1_34.value=this.w_DECOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDE__NOME_1_35.value==this.w_DE__NOME)
      this.oPgFrm.Page1.oPag.oDE__NOME_1_35.value=this.w_DE__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDATNAS_1_49.value==this.w_DEDATNAS)
      this.oPgFrm.Page1.oPag.oDEDATNAS_1_49.value=this.w_DEDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDECOMEST_1_50.value==this.w_DECOMEST)
      this.oPgFrm.Page1.oPag.oDECOMEST_1_50.value=this.w_DECOMEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDEPRONAS_1_51.value==this.w_DEPRONAS)
      this.oPgFrm.Page1.oPag.oDEPRONAS_1_51.value=this.w_DEPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTADOM_1_52.value==this.w_DESTADOM)
      this.oPgFrm.Page1.oPag.oDESTADOM_1_52.value=this.w_DESTADOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTAEST_1_53.value==this.w_DESTAEST)
      this.oPgFrm.Page1.oPag.oDESTAEST_1_53.value=this.w_DESTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDECITEST_1_55.value==this.w_DECITEST)
      this.oPgFrm.Page1.oPag.oDECITEST_1_55.value=this.w_DECITEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDEINDEST_1_56.value==this.w_DEINDEST)
      this.oPgFrm.Page1.oPag.oDEINDEST_1_56.value=this.w_DEINDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDEPARIVA_1_58.value==this.w_DEPARIVA)
      this.oPgFrm.Page1.oPag.oDEPARIVA_1_58.value=this.w_DEPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDECODFIS_1_60.value==this.w_DECODFIS)
      this.oPgFrm.Page1.oPag.oDECODFIS_1_60.value=this.w_DECODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDACONT_1_73.RadioValue()==this.w_DEDACONT)
      this.oPgFrm.Page1.oPag.oDEDACONT_1_73.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVADE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DE__ANNO)) or not(.w_DE__ANNO>2005 and .w_DE__ANNO<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDE__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DE__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_EDIT_DETIPSOG2 OR !(.w_ANFLPRIV='S' And .w_ANFLSGRE<>'S'))  and (.w_EDIT_DETIPSOG and .cFunction<>'Query')  and not(empty(.w_DECODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDECODCON_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DETIPSOG))  and (.w_EDIT_DETIPSOG and .cFunction<>'Query')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDETIPSOG_1_29.SetFocus()
            i_bnoObbl = !empty(.w_DETIPSOG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DEDESCRI))  and (.cFunction<>'Query' And .w_DETIPSOG='N' And .w_SOGESTPER$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDEDESCRI_1_33.SetFocus()
            i_bnoObbl = !empty(.w_DEDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DECOGNOM))  and (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDECOGNOM_1_34.SetFocus()
            i_bnoObbl = !empty(.w_DECOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DE__NOME))  and (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDE__NOME_1_35.SetFocus()
            i_bnoObbl = !empty(.w_DE__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DEDATNAS))  and (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDEDATNAS_1_49.SetFocus()
            i_bnoObbl = !empty(.w_DEDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DECOMEST))  and (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDECOMEST_1_50.SetFocus()
            i_bnoObbl = !empty(.w_DECOMEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DEPRONAS))  and (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDEPRONAS_1_51.SetFocus()
            i_bnoObbl = !empty(.w_DEPRONAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DESTADOM))  and (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDESTADOM_1_52.SetFocus()
            i_bnoObbl = !empty(.w_DESTADOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DESTAEST))  and (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDESTAEST_1_53.SetFocus()
            i_bnoObbl = !empty(.w_DESTAEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DEPARIVA))  and (.cFunction<>'Query' And .w_DETIPSOG='I')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDEPARIVA_1_58.SetFocus()
            i_bnoObbl = !empty(.w_DEPARIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DECODFIS))  and (.cFunction<>'Query' And .w_DETIPSOG='P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDECODFIS_1_60.SetFocus()
            i_bnoObbl = !empty(.w_DECODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAI_MDE.CheckForm()
      if i_bres
        i_bres=  .GSAI_MDE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSAI2MDE.CheckForm()
      if i_bres
        i_bres=  .GSAI2MDE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSAI4MDE.CheckForm()
      if i_bres
        i_bres=  .GSAI4MDE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DETIPCON = this.w_DETIPCON
    this.o_DECODCON = this.w_DECODCON
    this.o_DETIPSOG = this.w_DETIPSOG
    this.o_SOGESTPER = this.w_SOGESTPER
    * --- GSAI_MDE : Depends On
    this.GSAI_MDE.SaveDependsOn()
    * --- GSAI2MDE : Depends On
    this.GSAI2MDE.SaveDependsOn()
    * --- GSAI4MDE : Depends On
    this.GSAI4MDE.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsai_adePag1 as StdContainer
  Width  = 803
  height = 545
  stdWidth  = 803
  stdheight = 545
  resizeXpos=408
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDE__ANNO_1_2 as StdField with uid="HYNPMMDCOL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DE__ANNO", cQueryName = "DE__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 51694971,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=283, Top=21, cSayPict='"9999"', cGetPict='"9999"'

  func oDE__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DE__ANNO>2005 and .w_DE__ANNO<2100)
    endwith
    return bRes
  endfunc


  add object oDETIPCON_1_3 as StdCombo with uid="FAVUWBBZYH",rtseq=3,rtrep=.f.,left=160,top=73,width=118,height=21;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 222002556;
    , cFormVar="w_DETIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPCON_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDETIPCON_1_3.GetRadio()
    this.Parent.oContained.w_DETIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDETIPCON_1_3.SetRadio()
    this.Parent.oContained.w_DETIPCON=trim(this.Parent.oContained.w_DETIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DETIPCON=='C',1,;
      iif(this.Parent.oContained.w_DETIPCON=='F',2,;
      0))
  endfunc

  func oDETIPCON_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDIT_DETIPSOG and .cFunction<>'Query')
    endwith
   endif
  endfunc

  func oDETIPCON_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DECODCON)
        bRes2=.link_1_5('Full')
      endif
      if .not. empty(.w_SCCODCLF)
        bRes2=.link_1_6('Full')
      endif
      if .not. empty(.w_CHCODCLF)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDECODCON_1_5 as StdField with uid="EAHXPIIFWI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DECODCON", cQueryName = "DECODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 234261884,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=296, Top=73, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DETIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DECODCON"

  func oDECODCON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDIT_DETIPSOG and .cFunction<>'Query')
    endwith
   endif
  endfunc

  func oDECODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDECODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDECODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DETIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DETIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDECODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'Record tipo 2/3 incongruenti con il tipo soggetto.CONTI_VZM',this.parent.oContained
  endproc
  proc oDECODCON_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DETIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DECODCON
     i_obj.ecpSave()
  endproc


  add object oDETIPSOG_1_29 as StdCombo with uid="ZEGKZBAVTV",rtseq=29,rtrep=.f.,left=545,top=73,width=106,height=21;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 46432893;
    , cFormVar="w_DETIPSOG",RowSource=""+"Iva,"+"Privato,"+"Non residente", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPSOG_1_29.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oDETIPSOG_1_29.GetRadio()
    this.Parent.oContained.w_DETIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oDETIPSOG_1_29.SetRadio()
    this.Parent.oContained.w_DETIPSOG=trim(this.Parent.oContained.w_DETIPSOG)
    this.value = ;
      iif(this.Parent.oContained.w_DETIPSOG=='I',1,;
      iif(this.Parent.oContained.w_DETIPSOG=='P',2,;
      iif(this.Parent.oContained.w_DETIPSOG=='N',3,;
      0)))
  endfunc

  func oDETIPSOG_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDIT_DETIPSOG and .cFunction<>'Query')
    endwith
   endif
  endfunc

  proc oDETIPSOG_1_29.mAfter
    with this.Parent.oContained
      .w_DETIPSOG=IIF(.w_EDIT_DETIPSOG2 OR .w_DETIPSOG $ 'IN' OR .cFunction='Filter',.w_DETIPSOG, IIF(Ah_errormsg("Record tipo 2/3 incongruenti con il tipo soggetto"),this.Parent.oContained.o_DETIPSOG,this.Parent.oContained.o_DETIPSOG ) )
    endwith
  endproc


  add object oSOGESTPER_1_30 as StdCombo with uid="QACSGUVOGO",rtseq=30,rtrep=.f.,left=545,top=101,width=178,height=21;
    , ToolTipText = "Abilita campi persona fisica o persona giuridica nel caso di soggetto estero";
    , HelpContextID = 66044555;
    , cFormVar="w_SOGESTPER",RowSource=""+"Persona giuridica,"+"Persona fisica,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSOGESTPER_1_30.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oSOGESTPER_1_30.GetRadio()
    this.Parent.oContained.w_SOGESTPER = this.RadioValue()
    return .t.
  endfunc

  func oSOGESTPER_1_30.SetRadio()
    this.Parent.oContained.w_SOGESTPER=trim(this.Parent.oContained.w_SOGESTPER)
    this.value = ;
      iif(this.Parent.oContained.w_SOGESTPER=='N',1,;
      iif(this.Parent.oContained.w_SOGESTPER=='P',2,;
      iif(this.Parent.oContained.w_SOGESTPER=='S',3,;
      0)))
  endfunc

  func oSOGESTPER_1_30.mHide()
    with this.Parent.oContained
      return (NOT(.w_DETIPSOG='N'))
    endwith
  endfunc

  add object oDESCLGEN_1_32 as StdCheck with uid="FZBDCCDEUF",rtseq=31,rtrep=.f.,left=545, top=6, caption="Escludi da generazione",;
    ToolTipText = "Se attivo, esclude i dati estratti dalla generazione",;
    HelpContextID = 108950148,;
    cFormVar="w_DESCLGEN", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDESCLGEN_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDESCLGEN_1_32.GetRadio()
    this.Parent.oContained.w_DESCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oDESCLGEN_1_32.SetRadio()
    this.Parent.oContained.w_DESCLGEN=trim(this.Parent.oContained.w_DESCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_DESCLGEN=='S',1,;
      0)
  endfunc

  add object oDEDESCRI_1_33 as StdField with uid="UNDUYNBBES",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DEDESCRI", cQueryName = "DEDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale",;
    HelpContextID = 49250943,;
   bGlobalFont=.t.,;
    Height=21, Width=572, Left=160, Top=142, InputMask=replicate('X',60)

  func oDEDESCRI_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' And .w_SOGESTPER$'NS')
    endwith
   endif
  endfunc

  add object oDECOGNOM_1_34 as StdField with uid="VBWHBYPETD",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DECOGNOM", cQueryName = "DECOGNOM",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 46566781,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=160, Top=182, InputMask=replicate('X',40)

  func oDECOGNOM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
    endwith
   endif
  endfunc

  add object oDE__NOME_1_35 as StdField with uid="TZTOWOZVVJ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DE__NOME", cQueryName = "DE__NOME",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 21286277,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=460, Top=182, InputMask=replicate('X',40)

  func oDE__NOME_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
    endwith
   endif
  endfunc

  add object oDEDATNAS_1_49 as StdField with uid="VTKSSEXLQD",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DEDATNAS", cQueryName = "DEDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 234586761,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=222

  func oDEDATNAS_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
    endwith
   endif
  endfunc

  add object oDECOMEST_1_50 as StdField with uid="OEGUAGVHVP",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DECOMEST", cQueryName = "DECOMEST",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 77165194,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=273, Top=222, InputMask=replicate('X',40), bHasZoom = .t. 

  func oDECOMEST_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
    endwith
   endif
  endfunc

  proc oDECOMEST_1_50.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_DECOMEST",".w_DEPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDEPRONAS_1_51 as StdField with uid="HCPUHURXUB",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DEPRONAS", cQueryName = "DEPRONAS",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 230507145,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=640, Top=222, InputMask=replicate('X',2), bHasZoom = .t. 

  func oDEPRONAS_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
    endwith
   endif
  endfunc

  proc oDEPRONAS_1_51.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_DEPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDESTADOM_1_52 as StdField with uid="JZJZNJGHQI",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESTADOM", cQueryName = "DESTADOM",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero del domicilio fiscale",;
    HelpContextID = 220237181,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=263, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oDESTADOM_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'PS')
    endwith
   endif
  endfunc

  add object oDESTAEST_1_53 as StdField with uid="VNIBPXSZVB",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESTAEST", cQueryName = "DESTAEST",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero della sede legale",;
    HelpContextID = 64975498,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=315, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oDESTAEST_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'NS')
    endwith
   endif
  endfunc

  add object oDECITEST_1_55 as StdField with uid="VGAZWLEGEC",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DECITEST", cQueryName = "DECITEST",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Citt� estera della sede legale",;
    HelpContextID = 84112010,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=270, Top=315, InputMask=replicate('X',40)

  func oDECITEST_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'NS')
    endwith
   endif
  endfunc

  add object oDEINDEST_1_56 as StdField with uid="PPBQNCJMIR",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DEINDEST", cQueryName = "DEINDEST",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero della sede legale",;
    HelpContextID = 67687050,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=160, Top=357, InputMask=replicate('X',40)

  func oDEINDEST_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='N' AND .w_SOGESTPER$'NS')
    endwith
   endif
  endfunc

  add object oDEPARIVA_1_58 as StdField with uid="AOJRIDAGNZ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DEPARIVA", cQueryName = "DEPARIVA",;
    bObbl = .t. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 148652663,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=160, Top=414, InputMask=replicate('X',25)

  func oDEPARIVA_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='I')
    endwith
   endif
  endfunc

  add object oDECODFIS_1_60 as StdField with uid="NEICLLJLQF",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DECODFIS", cQueryName = "DECODFIS",;
    bObbl = .t. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 183930231,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=479, Top=414, InputMask=replicate('X',25)

  func oDECODFIS_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_DETIPSOG='P')
    endwith
   endif
  endfunc

  add object oDEDACONT_1_73 as StdCheck with uid="NMLXKBMXSE",rtseq=50,rtrep=.f.,left=545, top=32, caption="Da verificare",;
    ToolTipText = "Se attivo, i dati estratti sono da verificare",;
    HelpContextID = 34897270,;
    cFormVar="w_DEDACONT", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDEDACONT_1_73.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDEDACONT_1_73.GetRadio()
    this.Parent.oContained.w_DEDACONT = this.RadioValue()
    return .t.
  endfunc

  func oDEDACONT_1_73.SetRadio()
    this.Parent.oContained.w_DEDACONT=trim(this.Parent.oContained.w_DEDACONT)
    this.value = ;
      iif(this.Parent.oContained.w_DEDACONT=='S',1,;
      0)
  endfunc

  add object oStr_1_36 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=206, Top=21,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="FZWRBKVHFS",Visible=.t., Left=1, Top=23,;
    Alignment=0, Width=118, Height=18,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=9, Top=107,;
    Alignment=0, Width=111, Height=19,;
    Caption="Dati anagrafici"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="FMGZLDBDTV",Visible=.t., Left=440, Top=77,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="XCNUOVRHIB",Visible=.t., Left=9, Top=414,;
    Alignment=0, Width=111, Height=18,;
    Caption="Identificativi fiscali"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="LHYQUAFBCO",Visible=.t., Left=162, Top=401,;
    Alignment=0, Width=158, Height=13,;
    Caption="Partita IVA"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_44 as StdString with uid="BSYUVARGFL",Visible=.t., Left=479, Top=401,;
    Alignment=0, Width=158, Height=13,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="KIZOFHIWHV",Visible=.t., Left=160, Top=126,;
    Alignment=0, Width=130, Height=17,;
    Caption="Ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_DETIPSOG<>'N')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="KOKISJQUOL",Visible=.t., Left=160, Top=166,;
    Alignment=0, Width=130, Height=17,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_48 as StdString with uid="LIFDJEITLU",Visible=.t., Left=461, Top=166,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_54 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=160, Top=208,;
    Alignment=0, Width=76, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=273, Top=208,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune (o stato estero) di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_59 as StdString with uid="DMKPRROUYT",Visible=.t., Left=632, Top=208,;
    Alignment=0, Width=52, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="NOWYYKZWAZ",Visible=.t., Left=160, Top=250,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_62 as StdString with uid="TPWEYQUKDS",Visible=.t., Left=9, Top=253,;
    Alignment=0, Width=111, Height=18,;
    Caption="Domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="KBZOHAZPBS",Visible=.t., Left=9, Top=323,;
    Alignment=0, Width=111, Height=18,;
    Caption="Sede legale"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="HYIMBFNHGG",Visible=.t., Left=160, Top=300,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_66 as StdString with uid="WQAUIMBAAE",Visible=.t., Left=270, Top=300,;
    Alignment=0, Width=83, Height=13,;
    Caption="Citt� estera"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_67 as StdString with uid="HGABSVUXLY",Visible=.t., Left=162, Top=343,;
    Alignment=0, Width=83, Height=13,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="FALBAPPRTT",Visible=.t., Left=296, Top=101,;
    Alignment=0, Width=178, Height=18,;
    Caption="Soggetto collettivo non residente"  ;
  , bGlobalFont=.t.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (Empty(NVL(.w_CHCODCLF,'')) OR .w_DETIPSOG<>'N')
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="IKBATICWMT",Visible=.t., Left=160, Top=126,;
    Alignment=0, Width=130, Height=17,;
    Caption="Descrizione soggetto"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (.w_DETIPSOG='N')
    endwith
  endfunc

  add object oBox_1_38 as StdBox with uid="RDMWYOEEFE",left=3, top=64, width=763,height=391

  add object oBox_1_40 as StdBox with uid="PIUTDLGCGZ",left=151, top=64, width=1,height=391

  add object oBox_1_45 as StdBox with uid="OFIETXWFMW",left=4, top=390, width=760,height=1

  add object oBox_1_63 as StdBox with uid="URMYZLQHZM",left=4, top=289, width=760,height=1
enddefine
define class tgsai_adePag2 as StdContainer
  Width  = 803
  height = 545
  stdWidth  = 803
  stdheight = 545
  resizeXpos=614
  resizeYpos=323
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_2 as stdDynamicChildContainer with uid="BGMQZERBRO",left=4, top=13, width=787, height=469, bOnScreen=.t.;

enddefine
define class tgsai_adePag3 as StdContainer
  Width  = 803
  height = 545
  stdWidth  = 803
  stdheight = 545
  resizeXpos=667
  resizeYpos=365
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_2 as stdDynamicChildContainer with uid="KVAHMVNNWC",left=2, top=3, width=787, height=542, bOnScreen=.t.;

enddefine
define class tgsai_adePag4 as StdContainer
  Width  = 803
  height = 545
  stdWidth  = 803
  stdheight = 545
  resizeXpos=703
  resizeYpos=432
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="FWDXTHWKDJ",left=0, top=2, width=803, height=542, bOnScreen=.t.;

enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_ade','ANTIVADE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DESERIAL=ANTIVADE.DESERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
