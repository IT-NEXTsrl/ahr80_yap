* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsaiamdb                                                        *
*              Dettaglio dati estratti black list                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2014-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsaiamdb")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsaiamdb")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsaiamdb")
  return

* --- Class definition
define class tgsaiamdb as StdPCForm
  Width  = 797
  Height = 265
  Top    = 9
  Left   = 7
  cComment = "Dettaglio dati estratti black list"
  cPrg = "gsaiamdb"
  HelpContextID=80217961
  add object cnt as tcgsaiamdb
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsaiamdb as PCContext
  w_DBSERIAL = space(10)
  w_DBNUMDOC = 0
  w_DBDATDOC = space(8)
  w_DBALFDOC = space(10)
  w_DBESCLGE = space(1)
  w_DBRIFPNT = space(10)
  w_DBIMC003 = 0
  w_DBIMS003 = 0
  w_DBTIPREG = space(1)
  w_DBIMC004 = 0
  w_DBIMC005 = 0
  w_DBIMS005 = 0
  proc Save(i_oFrom)
    this.w_DBSERIAL = i_oFrom.w_DBSERIAL
    this.w_DBNUMDOC = i_oFrom.w_DBNUMDOC
    this.w_DBDATDOC = i_oFrom.w_DBDATDOC
    this.w_DBALFDOC = i_oFrom.w_DBALFDOC
    this.w_DBESCLGE = i_oFrom.w_DBESCLGE
    this.w_DBRIFPNT = i_oFrom.w_DBRIFPNT
    this.w_DBIMC003 = i_oFrom.w_DBIMC003
    this.w_DBIMS003 = i_oFrom.w_DBIMS003
    this.w_DBTIPREG = i_oFrom.w_DBTIPREG
    this.w_DBIMC004 = i_oFrom.w_DBIMC004
    this.w_DBIMC005 = i_oFrom.w_DBIMC005
    this.w_DBIMS005 = i_oFrom.w_DBIMS005
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DBSERIAL = this.w_DBSERIAL
    i_oTo.w_DBNUMDOC = this.w_DBNUMDOC
    i_oTo.w_DBDATDOC = this.w_DBDATDOC
    i_oTo.w_DBALFDOC = this.w_DBALFDOC
    i_oTo.w_DBESCLGE = this.w_DBESCLGE
    i_oTo.w_DBRIFPNT = this.w_DBRIFPNT
    i_oTo.w_DBIMC003 = this.w_DBIMC003
    i_oTo.w_DBIMS003 = this.w_DBIMS003
    i_oTo.w_DBTIPREG = this.w_DBTIPREG
    i_oTo.w_DBIMC004 = this.w_DBIMC004
    i_oTo.w_DBIMC005 = this.w_DBIMC005
    i_oTo.w_DBIMS005 = this.w_DBIMS005
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsaiamdb as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 797
  Height = 265
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-01-31"
  HelpContextID=80217961
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DATESDBL_IDX = 0
  cFile = "DATESDBL"
  cKeySelect = "DBSERIAL,DBTIPREG"
  cQueryFilter="DTTIPOPE='A' AND DTTIPREG='V'"
  cKeyWhere  = "DBSERIAL=this.w_DBSERIAL and DBTIPREG=this.w_DBTIPREG"
  cKeyDetail  = "DBSERIAL=this.w_DBSERIAL and DBTIPREG=this.w_DBTIPREG"
  cKeyWhereODBC = '"DBSERIAL="+cp_ToStrODBC(this.w_DBSERIAL)';
      +'+" and DBTIPREG="+cp_ToStrODBC(this.w_DBTIPREG)';

  cKeyDetailWhereODBC = '"DBSERIAL="+cp_ToStrODBC(this.w_DBSERIAL)';
      +'+" and DBTIPREG="+cp_ToStrODBC(this.w_DBTIPREG)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DATESDBL.DBSERIAL="+cp_ToStrODBC(this.w_DBSERIAL)';
      +'+" and DATESDBL.DBTIPREG="+cp_ToStrODBC(this.w_DBTIPREG)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DATESDBL.DBDATDOC,DATESDBL.DBNUMDOC,DATESDBL.DBALFDOC'
  cPrg = "gsaiamdb"
  cComment = "Dettaglio dati estratti black list"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- gsaiamdb
  *cKeyWhereODBC=cKeyWhereODBC+' AND '+cQueryFilter
  * --- Fine Area Manuale

  * --- Local Variables
  w_DBSERIAL = space(10)
  w_DBNUMDOC = 0
  w_DBDATDOC = ctod('  /  /  ')
  w_DBALFDOC = space(10)
  w_DBESCLGE = space(1)
  w_DBRIFPNT = space(10)
  w_DBIMC003 = 0
  w_DBIMS003 = 0
  w_DBTIPREG = space(1)
  w_DBIMC004 = 0
  w_DBIMC005 = 0
  w_DBIMS005 = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsaiamdbPag1","gsaiamdb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DATESDBL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATESDBL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATESDBL_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsaiamdb'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DATESDBL where DBSERIAL=KeySet.DBSERIAL
    *                            and DBTIPREG=KeySet.DBTIPREG
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DATESDBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDBL_IDX,2],this.bLoadRecFilter,this.DATESDBL_IDX,"gsaiamdb")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATESDBL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATESDBL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATESDBL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DBSERIAL',this.w_DBSERIAL  ,'DBTIPREG',this.w_DBTIPREG  )
      select * from (i_cTable) DATESDBL where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DBSERIAL = NVL(DBSERIAL,space(10))
        .w_DBTIPREG = NVL(DBTIPREG,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DATESDBL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DBNUMDOC = NVL(DBNUMDOC,0)
          .w_DBDATDOC = NVL(cp_ToDate(DBDATDOC),ctod("  /  /  "))
          .w_DBALFDOC = NVL(DBALFDOC,space(10))
          .w_DBESCLGE = NVL(DBESCLGE,space(1))
          .w_DBRIFPNT = NVL(DBRIFPNT,space(10))
          .w_DBIMC003 = NVL(DBIMC003,0)
          .w_DBIMS003 = NVL(DBIMS003,0)
          .w_DBIMC004 = NVL(DBIMC004,0)
          .w_DBIMC005 = NVL(DBIMC005,0)
          .w_DBIMS005 = NVL(DBIMS005,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_6.enabled = .oPgFrm.Page1.oPag.oBtn_2_6.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DBSERIAL=space(10)
      .w_DBNUMDOC=0
      .w_DBDATDOC=ctod("  /  /  ")
      .w_DBALFDOC=space(10)
      .w_DBESCLGE=space(1)
      .w_DBRIFPNT=space(10)
      .w_DBIMC003=0
      .w_DBIMS003=0
      .w_DBTIPREG=space(1)
      .w_DBIMC004=0
      .w_DBIMC005=0
      .w_DBIMS005=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_DBESCLGE = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATESDBL')
    this.DoRTCalc(6,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_6.enabled = this.oPgFrm.Page1.oPag.oBtn_2_6.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDBIMC003_2_7.enabled = i_bVal
      .Page1.oPag.oDBIMS003_2_8.enabled = i_bVal
      .Page1.oPag.oDBIMC004_2_9.enabled = i_bVal
      .Page1.oPag.oDBIMC005_2_10.enabled = i_bVal
      .Page1.oPag.oDBIMS005_2_11.enabled = i_bVal
      .Page1.oPag.oBtn_2_6.enabled = .Page1.oPag.oBtn_2_6.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DATESDBL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATESDBL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBSERIAL,"DBSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBTIPREG,"DBTIPREG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DBNUMDOC N(15);
      ,t_DBDATDOC D(8);
      ,t_DBALFDOC C(10);
      ,t_DBESCLGE N(3);
      ,t_DBIMC003 N(18,4);
      ,t_DBIMS003 N(18,4);
      ,t_DBIMC004 N(18,4);
      ,t_DBIMC005 N(18,4);
      ,t_DBIMS005 N(18,4);
      ,CPROWNUM N(10);
      ,t_DBRIFPNT C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsaiamdbbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBNUMDOC_2_1.controlsource=this.cTrsName+'.t_DBNUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBDATDOC_2_2.controlsource=this.cTrsName+'.t_DBDATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBALFDOC_2_3.controlsource=this.cTrsName+'.t_DBALFDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBESCLGE_2_4.controlsource=this.cTrsName+'.t_DBESCLGE'
    this.oPgFRm.Page1.oPag.oDBIMC003_2_7.controlsource=this.cTrsName+'.t_DBIMC003'
    this.oPgFRm.Page1.oPag.oDBIMS003_2_8.controlsource=this.cTrsName+'.t_DBIMS003'
    this.oPgFRm.Page1.oPag.oDBIMC004_2_9.controlsource=this.cTrsName+'.t_DBIMC004'
    this.oPgFRm.Page1.oPag.oDBIMC005_2_10.controlsource=this.cTrsName+'.t_DBIMC005'
    this.oPgFRm.Page1.oPag.oDBIMS005_2_11.controlsource=this.cTrsName+'.t_DBIMS005'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(91)
    this.AddVLine(206)
    this.AddVLine(272)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBNUMDOC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESDBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDBL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATESDBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDBL_IDX,2])
      *
      * insert into DATESDBL
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATESDBL')
        i_extval=cp_InsertValODBCExtFlds(this,'DATESDBL')
        i_cFldBody=" "+;
                  "(DBSERIAL,DBNUMDOC,DBDATDOC,DBALFDOC,DBESCLGE"+;
                  ",DBRIFPNT,DBIMC003,DBIMS003,DBTIPREG,DBIMC004"+;
                  ",DBIMC005,DBIMS005,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DBSERIAL)+","+cp_ToStrODBC(this.w_DBNUMDOC)+","+cp_ToStrODBC(this.w_DBDATDOC)+","+cp_ToStrODBC(this.w_DBALFDOC)+","+cp_ToStrODBC(this.w_DBESCLGE)+;
             ","+cp_ToStrODBC(this.w_DBRIFPNT)+","+cp_ToStrODBC(this.w_DBIMC003)+","+cp_ToStrODBC(this.w_DBIMS003)+","+cp_ToStrODBC(this.w_DBTIPREG)+","+cp_ToStrODBC(this.w_DBIMC004)+;
             ","+cp_ToStrODBC(this.w_DBIMC005)+","+cp_ToStrODBC(this.w_DBIMS005)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATESDBL')
        i_extval=cp_InsertValVFPExtFlds(this,'DATESDBL')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DBSERIAL',this.w_DBSERIAL,'DBTIPREG',this.w_DBTIPREG)
        INSERT INTO (i_cTable) (;
                   DBSERIAL;
                  ,DBNUMDOC;
                  ,DBDATDOC;
                  ,DBALFDOC;
                  ,DBESCLGE;
                  ,DBRIFPNT;
                  ,DBIMC003;
                  ,DBIMS003;
                  ,DBTIPREG;
                  ,DBIMC004;
                  ,DBIMC005;
                  ,DBIMS005;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DBSERIAL;
                  ,this.w_DBNUMDOC;
                  ,this.w_DBDATDOC;
                  ,this.w_DBALFDOC;
                  ,this.w_DBESCLGE;
                  ,this.w_DBRIFPNT;
                  ,this.w_DBIMC003;
                  ,this.w_DBIMS003;
                  ,this.w_DBTIPREG;
                  ,this.w_DBIMC004;
                  ,this.w_DBIMC005;
                  ,this.w_DBIMS005;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DATESDBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDBL_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_DBDATDOC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DATESDBL')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DATESDBL')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_DBDATDOC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DATESDBL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DATESDBL')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DBNUMDOC="+cp_ToStrODBC(this.w_DBNUMDOC)+;
                     ",DBDATDOC="+cp_ToStrODBC(this.w_DBDATDOC)+;
                     ",DBALFDOC="+cp_ToStrODBC(this.w_DBALFDOC)+;
                     ",DBESCLGE="+cp_ToStrODBC(this.w_DBESCLGE)+;
                     ",DBRIFPNT="+cp_ToStrODBC(this.w_DBRIFPNT)+;
                     ",DBIMC003="+cp_ToStrODBC(this.w_DBIMC003)+;
                     ",DBIMS003="+cp_ToStrODBC(this.w_DBIMS003)+;
                     ",DBIMC004="+cp_ToStrODBC(this.w_DBIMC004)+;
                     ",DBIMC005="+cp_ToStrODBC(this.w_DBIMC005)+;
                     ",DBIMS005="+cp_ToStrODBC(this.w_DBIMS005)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DATESDBL')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DBNUMDOC=this.w_DBNUMDOC;
                     ,DBDATDOC=this.w_DBDATDOC;
                     ,DBALFDOC=this.w_DBALFDOC;
                     ,DBESCLGE=this.w_DBESCLGE;
                     ,DBRIFPNT=this.w_DBRIFPNT;
                     ,DBIMC003=this.w_DBIMC003;
                     ,DBIMS003=this.w_DBIMS003;
                     ,DBIMC004=this.w_DBIMC004;
                     ,DBIMC005=this.w_DBIMC005;
                     ,DBIMS005=this.w_DBIMS005;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATESDBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDBL_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_DBDATDOC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DATESDBL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_DBDATDOC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESDBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDBL_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DBRIFPNT with this.w_DBRIFPNT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_6.enabled =this.oPgFrm.Page1.oPag.oBtn_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDBIMC003_2_7.value==this.w_DBIMC003)
      this.oPgFrm.Page1.oPag.oDBIMC003_2_7.value=this.w_DBIMC003
      replace t_DBIMC003 with this.oPgFrm.Page1.oPag.oDBIMC003_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBIMS003_2_8.value==this.w_DBIMS003)
      this.oPgFrm.Page1.oPag.oDBIMS003_2_8.value=this.w_DBIMS003
      replace t_DBIMS003 with this.oPgFrm.Page1.oPag.oDBIMS003_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBIMC004_2_9.value==this.w_DBIMC004)
      this.oPgFrm.Page1.oPag.oDBIMC004_2_9.value=this.w_DBIMC004
      replace t_DBIMC004 with this.oPgFrm.Page1.oPag.oDBIMC004_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBIMC005_2_10.value==this.w_DBIMC005)
      this.oPgFrm.Page1.oPag.oDBIMC005_2_10.value=this.w_DBIMC005
      replace t_DBIMC005 with this.oPgFrm.Page1.oPag.oDBIMC005_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBIMS005_2_11.value==this.w_DBIMS005)
      this.oPgFrm.Page1.oPag.oDBIMS005_2_11.value=this.w_DBIMS005
      replace t_DBIMS005 with this.oPgFrm.Page1.oPag.oDBIMS005_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBNUMDOC_2_1.value==this.w_DBNUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBNUMDOC_2_1.value=this.w_DBNUMDOC
      replace t_DBNUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBNUMDOC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDATDOC_2_2.value==this.w_DBDATDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDATDOC_2_2.value=this.w_DBDATDOC
      replace t_DBDATDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDATDOC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBALFDOC_2_3.value==this.w_DBALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBALFDOC_2_3.value=this.w_DBALFDOC
      replace t_DBALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBALFDOC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBESCLGE_2_4.RadioValue()==this.w_DBESCLGE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBESCLGE_2_4.SetRadio()
      replace t_DBESCLGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBESCLGE_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'DATESDBL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_DBDATDOC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_DBDATDOC))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DBNUMDOC=0
      .w_DBDATDOC=ctod("  /  /  ")
      .w_DBALFDOC=space(10)
      .w_DBESCLGE=space(1)
      .w_DBRIFPNT=space(10)
      .w_DBIMC003=0
      .w_DBIMS003=0
      .w_DBIMC004=0
      .w_DBIMC005=0
      .w_DBIMS005=0
      .DoRTCalc(1,4,.f.)
        .w_DBESCLGE = 'N'
    endwith
    this.DoRTCalc(6,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DBNUMDOC = t_DBNUMDOC
    this.w_DBDATDOC = t_DBDATDOC
    this.w_DBALFDOC = t_DBALFDOC
    this.w_DBESCLGE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBESCLGE_2_4.RadioValue(.t.)
    this.w_DBRIFPNT = t_DBRIFPNT
    this.w_DBIMC003 = t_DBIMC003
    this.w_DBIMS003 = t_DBIMS003
    this.w_DBIMC004 = t_DBIMC004
    this.w_DBIMC005 = t_DBIMC005
    this.w_DBIMS005 = t_DBIMS005
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DBNUMDOC with this.w_DBNUMDOC
    replace t_DBDATDOC with this.w_DBDATDOC
    replace t_DBALFDOC with this.w_DBALFDOC
    replace t_DBESCLGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBESCLGE_2_4.ToRadio()
    replace t_DBRIFPNT with this.w_DBRIFPNT
    replace t_DBIMC003 with this.w_DBIMC003
    replace t_DBIMS003 with this.w_DBIMS003
    replace t_DBIMC004 with this.w_DBIMC004
    replace t_DBIMC005 with this.w_DBIMC005
    replace t_DBIMS005 with this.w_DBIMS005
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsaiamdbPag1 as StdContainer
  Width  = 793
  height = 265
  stdWidth  = 793
  stdheight = 265
  resizeXpos=617
  resizeYpos=211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=3, width=354,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="DBDATDOC",Label1="Data doc.",Field2="DBNUMDOC",Label2="Numero",Field3="DBALFDOC",Label3="Alfa",Field4="DBESCLGE",Label4="Escl. da gen.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235872890

  add object oStr_1_3 as StdString with uid="FLRZNMWFLM",Visible=.t., Left=441, Top=25,;
    Alignment=2, Width=302, Height=18,;
    Caption="Operazioni imponibili, non imponibili ed esenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="CSDCKEXDVW",Visible=.t., Left=427, Top=48,;
    Alignment=0, Width=154, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="XOVUEBKZFL",Visible=.t., Left=634, Top=48,;
    Alignment=0, Width=154, Height=18,;
    Caption="Imposta"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="UTIUFEURQG",Visible=.t., Left=465, Top=96,;
    Alignment=2, Width=273, Height=18,;
    Caption="Operazioni non soggette a IVA"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="BEIKNUPYGO",Visible=.t., Left=427, Top=127,;
    Alignment=0, Width=154, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="QBYUBWQTID",Visible=.t., Left=492, Top=181,;
    Alignment=2, Width=209, Height=18,;
    Caption="Note di variazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GQUGCVFYSU",Visible=.t., Left=427, Top=208,;
    Alignment=0, Width=154, Height=18,;
    Caption="Importo complessivo"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="ALJCQAOYJT",Visible=.t., Left=634, Top=208,;
    Alignment=0, Width=154, Height=18,;
    Caption="Imposta"  ;
  , bGlobalFont=.t.

  add object oBox_1_6 as StdBox with uid="LBEQNVIAEK",left=414, top=19, width=372,height=28

  add object oBox_1_8 as StdBox with uid="EABZWQUFPX",left=414, top=93, width=372,height=26

  add object oBox_1_11 as StdBox with uid="AKTKEGXHZK",left=414, top=176, width=372,height=28

  add object oBox_1_14 as StdBox with uid="HSNGDGBHBG",left=595, top=45, width=1,height=49

  add object oBox_1_15 as StdBox with uid="XMAYNRATJZ",left=595, top=119, width=1,height=58

  add object oBox_1_16 as StdBox with uid="KJAUMHTGYK",left=595, top=204, width=1,height=59

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=24,;
    width=350+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=25,width=349+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDBIMC003_2_7.Refresh()
      this.Parent.oDBIMS003_2_8.Refresh()
      this.Parent.oDBIMC004_2_9.Refresh()
      this.Parent.oDBIMC005_2_10.Refresh()
      this.Parent.oDBIMS005_2_11.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_6 as StdButton with uid="XEDXHWOFBI",width=48,height=45,;
   left=361, top=26,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza la registrazione di Prima Nota";
    , HelpContextID = 108538870;
    , Caption='Re\<g.Cont';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_DBRIFPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_6.mCond()
    with this.Parent.oContained
      return (g_COGE='S' AND NOT EMPTY(.w_DBRIFPNT))
    endwith
  endfunc

  add object oDBIMC003_2_7 as StdTrsField with uid="NYPJPEAUMW",rtseq=7,rtrep=.t.,;
    cFormVar="w_DBIMC003",value=0,;
    ToolTipText = "Importo complessivo",;
    HelpContextID = 4599959,;
    cTotal="", bFixedPos=.t., cQueryName = "DBIMC003",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=426, Top=69, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDBIMS003_2_8 as StdTrsField with uid="KQBADHPBIU",rtseq=8,rtrep=.t.,;
    cFormVar="w_DBIMS003",value=0,;
    ToolTipText = "Imposta",;
    HelpContextID = 256258199,;
    cTotal="", bFixedPos=.t., cQueryName = "DBIMS003",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=634, Top=69, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDBIMC004_2_9 as StdTrsField with uid="LLGDUVXDWI",rtseq=10,rtrep=.t.,;
    cFormVar="w_DBIMC004",value=0,;
    ToolTipText = "Importo complessivo operazioni non soggette ad IVA",;
    HelpContextID = 4599958,;
    cTotal="", bFixedPos=.t., cQueryName = "DBIMC004",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=426, Top=149, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDBIMC005_2_10 as StdTrsField with uid="TIOUUGCVAY",rtseq=11,rtrep=.t.,;
    cFormVar="w_DBIMC005",value=0,;
    ToolTipText = "Importo complessivo",;
    HelpContextID = 4599957,;
    cTotal="", bFixedPos=.t., cQueryName = "DBIMC005",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=426, Top=235, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDBIMS005_2_11 as StdTrsField with uid="KHRZVENTHQ",rtseq=12,rtrep=.t.,;
    cFormVar="w_DBIMS005",value=0,;
    ToolTipText = "Imposta",;
    HelpContextID = 256258197,;
    cTotal="", bFixedPos=.t., cQueryName = "DBIMS005",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=634, Top=235, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsaiamdbBodyRow as CPBodyRowCnt
  Width=340
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDBNUMDOC_2_1 as StdTrsField with uid="QLJYUFEEHA",rtseq=2,rtrep=.t.,;
    cFormVar="w_DBNUMDOC",value=0,;
    ToolTipText = "Numero documento",;
    HelpContextID = 194896007,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=85, Top=0, cSayPict=["999999999999999"], cGetPict=["999999999999999"]

  add object oDBDATDOC_2_2 as StdTrsField with uid="OHRWOSWGLF",rtseq=3,rtrep=.t.,;
    cFormVar="w_DBDATDOC",value=ctod("  /  /  "),;
    ToolTipText = "Data documento",;
    HelpContextID = 188907655,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=-2, Top=0

  add object oDBALFDOC_2_3 as StdTrsField with uid="REHQXKICCX",rtseq=4,rtrep=.t.,;
    cFormVar="w_DBALFDOC",value=space(10),;
    ToolTipText = "Serie documento",;
    HelpContextID = 202879111,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=200, Top=0, InputMask=replicate('X',10)

  add object oDBESCLGE_2_4 as StdTrsCombo with uid="QMOZXTGKZO",rtrep=.t.,;
    cFormVar="w_DBESCLGE", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 197103483,;
    Height=22, Width=69, Left=266, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDBESCLGE_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DBESCLGE,&i_cF..t_DBESCLGE),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oDBESCLGE_2_4.GetRadio()
    this.Parent.oContained.w_DBESCLGE = this.RadioValue()
    return .t.
  endfunc

  func oDBESCLGE_2_4.ToRadio()
    this.Parent.oContained.w_DBESCLGE=trim(this.Parent.oContained.w_DBESCLGE)
    return(;
      iif(this.Parent.oContained.w_DBESCLGE=='S',1,;
      iif(this.Parent.oContained.w_DBESCLGE=='N',2,;
      0)))
  endfunc

  func oDBESCLGE_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDBNUMDOC_2_1.When()
    return(.t.)
  proc oDBNUMDOC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDBNUMDOC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="DTTIPOPE='A' AND DTTIPREG='V'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".DBSERIAL=DATESDBL.DBSERIAL";
  +" and "+i_cAliasName+".DBTIPREG=DATESDBL.DBTIPREG";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsaiamdb','DATESDBL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DBSERIAL=DATESDBL.DBSERIAL";
  +" and "+i_cAliasName2+".DBTIPREG=DATESDBL.DBTIPREG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
