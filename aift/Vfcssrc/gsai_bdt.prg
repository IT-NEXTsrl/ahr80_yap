* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bdt                                                        *
*              Lettura tabella Dati Estratti                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-11-08                                                      *
* Last revis.: 2011-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bdt",oParentObject)
return(i_retval)

define class tgsai_bdt as StdBatch
  * --- Local variables
  * --- WorkFile variables
  ANTIVDDE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura "Dati estratti"
    * --- Read from ANTIVDDE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ANTIVDDE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDE_idx,2],.t.,this.ANTIVDDE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DESERIAL,DE__AREA,CPROWNUM"+;
        " from "+i_cTable+" ANTIVDDE where ";
            +"DERIFPNT = "+cp_ToStrODBC(this.oParentObject.w_DERIFPNT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DESERIAL,DE__AREA,CPROWNUM;
        from (i_cTable) where;
            DERIFPNT = this.oParentObject.w_DERIFPNT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DESERIAL = NVL(cp_ToDate(_read_.DESERIAL),cp_NullValue(_read_.DESERIAL))
      this.oParentObject.w_DE__AREA = NVL(cp_ToDate(_read_.DE__AREA),cp_NullValue(_read_.DE__AREA))
      this.oParentObject.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ANTIVDDE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
