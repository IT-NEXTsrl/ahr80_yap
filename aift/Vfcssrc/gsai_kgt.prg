* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_kgt                                                        *
*              Stampa dati file generato                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-11-06                                                      *
* Last revis.: 2014-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_kgt",oParentObject))

* --- Class definition
define class tgsai_kgt as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 809
  Height = 594
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-01-31"
  HelpContextID=32114537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsai_kgt"
  cComment = "Stampa dati file generato"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DGSERIAL = 0
  o_DGSERIAL = 0
  w_DGPROINV = 0
  o_DGPROINV = 0
  w_NOMEFILE = space(254)
  w_ATTIPAGR = space(1)
  w_QUADFA = space(1)
  w_QUADSA = space(1)
  w_QUADBL = space(1)
  w_QUADTUA = space(1)
  w_ATTIPDAT = space(1)
  w_QUADFE = space(1)
  w_QUADFR = space(1)
  w_QUADNE = space(1)
  w_QUADNR = space(1)
  w_QUADDF = space(1)
  w_QUADFN = space(1)
  w_QUADTU = space(1)
  w_QUADSE = space(1)
  w_Atgenspe = space(1)
  w_Atgenblk = space(1)
  w_Atgensma = space(1)
  w_ATTIPINV = 0
  w_ZoomSpe = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_kgtPag1","gsai_kgt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATTIPAGR_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSpe = this.oPgFrm.Pages(1).oPag.ZoomSpe
    DoDefault()
    proc Destroy()
      this.w_ZoomSpe = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DGSERIAL=0
      .w_DGPROINV=0
      .w_NOMEFILE=space(254)
      .w_ATTIPAGR=space(1)
      .w_QUADFA=space(1)
      .w_QUADSA=space(1)
      .w_QUADBL=space(1)
      .w_QUADTUA=space(1)
      .w_ATTIPDAT=space(1)
      .w_QUADFE=space(1)
      .w_QUADFR=space(1)
      .w_QUADNE=space(1)
      .w_QUADNR=space(1)
      .w_QUADDF=space(1)
      .w_QUADFN=space(1)
      .w_QUADTU=space(1)
      .w_QUADSE=space(1)
      .w_Atgenspe=space(1)
      .w_Atgenblk=space(1)
      .w_Atgensma=space(1)
      .w_ATTIPINV=0
      .oPgFrm.Page1.oPag.ZoomSpe.Calculate()
        .w_DGSERIAL = NVL(.w_ZoomSpe.getVar('Dgserial'), ' ')
        .w_DGPROINV = NVL(.w_ZoomSpe.getVar('Dgproinv'),0)
        .w_NOMEFILE = NVL(.w_ZoomSpe.getVar('Dgnomfil'), ' ')
        .w_ATTIPAGR = NVL(.w_ZoomSpe.getVar('Attipagr'), ' ')
        .w_QUADFA = NVL(.w_ZoomSpe.getVar('QuadFA'), ' ')
        .w_QUADSA = NVL(.w_ZoomSpe.getVar('QuadSA'), ' ')
        .w_QUADBL = NVL(.w_ZoomSpe.getVar('QuadBL'), ' ')
        .w_QUADTUA = NVL(.w_ZoomSpe.getVar('QuadTUA'), ' ')
        .w_ATTIPDAT = NVL(.w_ZoomSpe.getVar('Attipdat'), ' ')
        .w_QUADFE = NVL(.w_ZoomSpe.getVar('QuadFE'), ' ')
        .w_QUADFR = NVL(.w_ZoomSpe.getVar('QuadFR'), ' ')
        .w_QUADNE = NVL(.w_ZoomSpe.getVar('QuadNE'), ' ')
        .w_QUADNR = NVL(.w_ZoomSpe.getVar('QuadNR'), ' ')
        .w_QUADDF = NVL(.w_ZoomSpe.getVar('QuadDF'), ' ')
        .w_QUADFN = NVL(.w_ZoomSpe.getVar('QuadFN'), ' ')
        .w_QUADTU = NVL(.w_ZoomSpe.getVar('QuadTU'), ' ')
        .w_QUADSE = NVL(.w_ZoomSpe.getVar('QuadSE'), ' ')
        .w_Atgenspe = NVL(.w_ZoomSpe.getVar('Atgenspe'), ' ')
        .w_Atgenblk = NVL(.w_ZoomSpe.getVar('Atgenblk'), ' ')
        .w_Atgensma = NVL(.w_ZoomSpe.getVar('Atgensma'), ' ')
        .w_ATTIPINV = 0
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomSpe.Calculate()
            .w_DGSERIAL = NVL(.w_ZoomSpe.getVar('Dgserial'), ' ')
            .w_DGPROINV = NVL(.w_ZoomSpe.getVar('Dgproinv'),0)
        if .o_DGPROINV<>.w_DGPROINV.or. .o_DGSERIAL<>.w_DGSERIAL
            .w_NOMEFILE = NVL(.w_ZoomSpe.getVar('Dgnomfil'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_ATTIPAGR = NVL(.w_ZoomSpe.getVar('Attipagr'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADFA = NVL(.w_ZoomSpe.getVar('QuadFA'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADSA = NVL(.w_ZoomSpe.getVar('QuadSA'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADBL = NVL(.w_ZoomSpe.getVar('QuadBL'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADTUA = NVL(.w_ZoomSpe.getVar('QuadTUA'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_ATTIPDAT = NVL(.w_ZoomSpe.getVar('Attipdat'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADFE = NVL(.w_ZoomSpe.getVar('QuadFE'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADFR = NVL(.w_ZoomSpe.getVar('QuadFR'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADNE = NVL(.w_ZoomSpe.getVar('QuadNE'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADNR = NVL(.w_ZoomSpe.getVar('QuadNR'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADDF = NVL(.w_ZoomSpe.getVar('QuadDF'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADFN = NVL(.w_ZoomSpe.getVar('QuadFN'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADTU = NVL(.w_ZoomSpe.getVar('QuadTU'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_QUADSE = NVL(.w_ZoomSpe.getVar('QuadSE'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_Atgenspe = NVL(.w_ZoomSpe.getVar('Atgenspe'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_Atgenblk = NVL(.w_ZoomSpe.getVar('Atgenblk'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_Atgensma = NVL(.w_ZoomSpe.getVar('Atgensma'), ' ')
        endif
        if .o_Dgserial<>.w_Dgserial.or. .o_Dgproinv<>.w_Dgproinv
            .w_ATTIPINV = 0
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomSpe.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATTIPAGR_1_7.enabled = this.oPgFrm.Page1.oPag.oATTIPAGR_1_7.mCond()
    this.oPgFrm.Page1.oPag.oQUADFA_1_9.enabled = this.oPgFrm.Page1.oPag.oQUADFA_1_9.mCond()
    this.oPgFrm.Page1.oPag.oQUADSA_1_10.enabled = this.oPgFrm.Page1.oPag.oQUADSA_1_10.mCond()
    this.oPgFrm.Page1.oPag.oQUADBL_1_11.enabled = this.oPgFrm.Page1.oPag.oQUADBL_1_11.mCond()
    this.oPgFrm.Page1.oPag.oQUADTUA_1_12.enabled = this.oPgFrm.Page1.oPag.oQUADTUA_1_12.mCond()
    this.oPgFrm.Page1.oPag.oATTIPDAT_1_13.enabled = this.oPgFrm.Page1.oPag.oATTIPDAT_1_13.mCond()
    this.oPgFrm.Page1.oPag.oQUADFE_1_14.enabled = this.oPgFrm.Page1.oPag.oQUADFE_1_14.mCond()
    this.oPgFrm.Page1.oPag.oQUADFR_1_15.enabled = this.oPgFrm.Page1.oPag.oQUADFR_1_15.mCond()
    this.oPgFrm.Page1.oPag.oQUADNE_1_16.enabled = this.oPgFrm.Page1.oPag.oQUADNE_1_16.mCond()
    this.oPgFrm.Page1.oPag.oQUADNR_1_17.enabled = this.oPgFrm.Page1.oPag.oQUADNR_1_17.mCond()
    this.oPgFrm.Page1.oPag.oQUADDF_1_18.enabled = this.oPgFrm.Page1.oPag.oQUADDF_1_18.mCond()
    this.oPgFrm.Page1.oPag.oQUADFN_1_19.enabled = this.oPgFrm.Page1.oPag.oQUADFN_1_19.mCond()
    this.oPgFrm.Page1.oPag.oQUADTU_1_20.enabled = this.oPgFrm.Page1.oPag.oQUADTU_1_20.mCond()
    this.oPgFrm.Page1.oPag.oQUADSE_1_21.enabled = this.oPgFrm.Page1.oPag.oQUADSE_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAtgenspe_1_24.visible=!this.oPgFrm.Page1.oPag.oAtgenspe_1_24.mHide()
    this.oPgFrm.Page1.oPag.oAtgenblk_1_25.visible=!this.oPgFrm.Page1.oPag.oAtgenblk_1_25.mHide()
    this.oPgFrm.Page1.oPag.oAtgensma_1_26.visible=!this.oPgFrm.Page1.oPag.oAtgensma_1_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomSpe.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_5.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_5.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPAGR_1_7.RadioValue()==this.w_ATTIPAGR)
      this.oPgFrm.Page1.oPag.oATTIPAGR_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADFA_1_9.RadioValue()==this.w_QUADFA)
      this.oPgFrm.Page1.oPag.oQUADFA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADSA_1_10.RadioValue()==this.w_QUADSA)
      this.oPgFrm.Page1.oPag.oQUADSA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADBL_1_11.RadioValue()==this.w_QUADBL)
      this.oPgFrm.Page1.oPag.oQUADBL_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADTUA_1_12.RadioValue()==this.w_QUADTUA)
      this.oPgFrm.Page1.oPag.oQUADTUA_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPDAT_1_13.RadioValue()==this.w_ATTIPDAT)
      this.oPgFrm.Page1.oPag.oATTIPDAT_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADFE_1_14.RadioValue()==this.w_QUADFE)
      this.oPgFrm.Page1.oPag.oQUADFE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADFR_1_15.RadioValue()==this.w_QUADFR)
      this.oPgFrm.Page1.oPag.oQUADFR_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADNE_1_16.RadioValue()==this.w_QUADNE)
      this.oPgFrm.Page1.oPag.oQUADNE_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADNR_1_17.RadioValue()==this.w_QUADNR)
      this.oPgFrm.Page1.oPag.oQUADNR_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADDF_1_18.RadioValue()==this.w_QUADDF)
      this.oPgFrm.Page1.oPag.oQUADDF_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADFN_1_19.RadioValue()==this.w_QUADFN)
      this.oPgFrm.Page1.oPag.oQUADFN_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADTU_1_20.RadioValue()==this.w_QUADTU)
      this.oPgFrm.Page1.oPag.oQUADTU_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADSE_1_21.RadioValue()==this.w_QUADSE)
      this.oPgFrm.Page1.oPag.oQUADSE_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAtgenspe_1_24.RadioValue()==this.w_Atgenspe)
      this.oPgFrm.Page1.oPag.oAtgenspe_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAtgenblk_1_25.RadioValue()==this.w_Atgenblk)
      this.oPgFrm.Page1.oPag.oAtgenblk_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAtgensma_1_26.RadioValue()==this.w_Atgensma)
      this.oPgFrm.Page1.oPag.oAtgensma_1_26.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DGSERIAL = this.w_DGSERIAL
    this.o_DGPROINV = this.w_DGPROINV
    return

enddefine

* --- Define pages as container
define class tgsai_kgtPag1 as StdContainer
  Width  = 805
  height = 594
  stdWidth  = 805
  stdheight = 594
  resizeXpos=369
  resizeYpos=176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomSpe as cp_zoombox with uid="JMTRXDWYZF",left=5, top=11, width=774,height=341,;
    caption='ZoomSpe',;
   bGlobalFont=.t.,;
    bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,bRetriveAllRows=.f.,cZoomOnZoom="",cTable="GENDFILT",cMenuFile="",cZoomFile="GSAI_KGT",bOptions=.t.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 62545302

  add object oNOMEFILE_1_5 as StdField with uid="CQJVEPDBZT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file da stampare",;
    HelpContextID = 71295717,;
   bGlobalFont=.t.,;
    Height=21, Width=636, Left=91, Top=363, InputMask=replicate('X',254)


  add object oBtn_1_6 as StdButton with uid="BYWCYEOHKL",left=735, top=364, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 31913514;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        Gsai_Bst(this.Parent.oContained,"SceltaFile")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oATTIPAGR_1_7 as StdCheck with uid="VZIUFDUEEQ",rtseq=4,rtrep=.f.,left=245, top=433, caption="Dati Aggregati",;
    ToolTipText = "Dati Aggregati",;
    HelpContextID = 73699672,;
    cFormVar="w_ATTIPAGR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTIPAGR_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATTIPAGR_1_7.GetRadio()
    this.Parent.oContained.w_ATTIPAGR = this.RadioValue()
    return .t.
  endfunc

  func oATTIPAGR_1_7.SetRadio()
    this.Parent.oContained.w_ATTIPAGR=trim(this.Parent.oContained.w_ATTIPAGR)
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPAGR=='S',1,;
      0)
  endfunc

  func oATTIPAGR_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADFA_1_9 as StdCheck with uid="WWAWIJIOKR",rtseq=5,rtrep=.f.,left=245, top=466, caption="Quadro FA",;
    ToolTipText = "Quadro FA",;
    HelpContextID = 205626618,;
    cFormVar="w_QUADFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADFA_1_9.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADFA_1_9.GetRadio()
    this.Parent.oContained.w_QUADFA = this.RadioValue()
    return .t.
  endfunc

  func oQUADFA_1_9.SetRadio()
    this.Parent.oContained.w_QUADFA=trim(this.Parent.oContained.w_QUADFA)
    this.value = ;
      iif(this.Parent.oContained.w_QUADFA=='1',1,;
      0)
  endfunc

  func oQUADFA_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipagr='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADSA_1_10 as StdCheck with uid="XREXLFHIMS",rtseq=6,rtrep=.f.,left=356, top=466, caption="Quadro SA",;
    ToolTipText = "Quadro SA",;
    HelpContextID = 191995130,;
    cFormVar="w_QUADSA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADSA_1_10.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADSA_1_10.GetRadio()
    this.Parent.oContained.w_QUADSA = this.RadioValue()
    return .t.
  endfunc

  func oQUADSA_1_10.SetRadio()
    this.Parent.oContained.w_QUADSA=trim(this.Parent.oContained.w_QUADSA)
    this.value = ;
      iif(this.Parent.oContained.w_QUADSA=='1',1,;
      0)
  endfunc

  func oQUADSA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipagr='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADBL_1_11 as StdCheck with uid="MTJHAFRDTB",rtseq=7,rtrep=.f.,left=476, top=466, caption="Quadro BL",;
    ToolTipText = "Quadro BL",;
    HelpContextID = 25271546,;
    cFormVar="w_QUADBL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADBL_1_11.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADBL_1_11.GetRadio()
    this.Parent.oContained.w_QUADBL = this.RadioValue()
    return .t.
  endfunc

  func oQUADBL_1_11.SetRadio()
    this.Parent.oContained.w_QUADBL=trim(this.Parent.oContained.w_QUADBL)
    this.value = ;
      iif(this.Parent.oContained.w_QUADBL=='1',1,;
      0)
  endfunc

  func oQUADBL_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipagr='S' And (.w_Atgenspe='S' Or .w_Atgenblk='S'))
    endwith
   endif
  endfunc

  add object oQUADTUA_1_12 as StdCheck with uid="CBQSTGILXN",rtseq=8,rtrep=.f.,left=588, top=466, caption="Quadro TU",;
    ToolTipText = "Quadro TU",;
    HelpContextID = 144597766,;
    cFormVar="w_QUADTUA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADTUA_1_12.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADTUA_1_12.GetRadio()
    this.Parent.oContained.w_QUADTUA = this.RadioValue()
    return .t.
  endfunc

  func oQUADTUA_1_12.SetRadio()
    this.Parent.oContained.w_QUADTUA=trim(this.Parent.oContained.w_QUADTUA)
    this.value = ;
      iif(this.Parent.oContained.w_QUADTUA=='1',1,;
      0)
  endfunc

  func oQUADTUA_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipagr='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATTIPDAT_1_13 as StdCheck with uid="KYNWECUSKG",rtseq=9,rtrep=.f.,left=245, top=501, caption="Dati Analitici",;
    ToolTipText = "Dati analitici",;
    HelpContextID = 124031322,;
    cFormVar="w_ATTIPDAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTIPDAT_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATTIPDAT_1_13.GetRadio()
    this.Parent.oContained.w_ATTIPDAT = this.RadioValue()
    return .t.
  endfunc

  func oATTIPDAT_1_13.SetRadio()
    this.Parent.oContained.w_ATTIPDAT=trim(this.Parent.oContained.w_ATTIPDAT)
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPDAT=='S',1,;
      0)
  endfunc

  func oATTIPDAT_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADFE_1_14 as StdCheck with uid="NVENBJLRMK",rtseq=10,rtrep=.f.,left=245, top=532, caption="Quadro FE",;
    ToolTipText = "Quadro FE",;
    HelpContextID = 138517754,;
    cFormVar="w_QUADFE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADFE_1_14.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADFE_1_14.GetRadio()
    this.Parent.oContained.w_QUADFE = this.RadioValue()
    return .t.
  endfunc

  func oQUADFE_1_14.SetRadio()
    this.Parent.oContained.w_QUADFE=trim(this.Parent.oContained.w_QUADFE)
    this.value = ;
      iif(this.Parent.oContained.w_QUADFE=='1',1,;
      0)
  endfunc

  func oQUADFE_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipdat='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADFR_1_15 as StdCheck with uid="WOROVMYLGM",rtseq=11,rtrep=.f.,left=356, top=532, caption="Quadro FR",;
    ToolTipText = "Quadro FR",;
    HelpContextID = 79586054,;
    cFormVar="w_QUADFR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADFR_1_15.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADFR_1_15.GetRadio()
    this.Parent.oContained.w_QUADFR = this.RadioValue()
    return .t.
  endfunc

  func oQUADFR_1_15.SetRadio()
    this.Parent.oContained.w_QUADFR=trim(this.Parent.oContained.w_QUADFR)
    this.value = ;
      iif(this.Parent.oContained.w_QUADFR=='1',1,;
      0)
  endfunc

  func oQUADFR_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipdat='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADNE_1_16 as StdCheck with uid="NDUSLZVAJH",rtseq=12,rtrep=.f.,left=476, top=532, caption="Quadro NE",;
    ToolTipText = "Quadro NE",;
    HelpContextID = 130129146,;
    cFormVar="w_QUADNE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADNE_1_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADNE_1_16.GetRadio()
    this.Parent.oContained.w_QUADNE = this.RadioValue()
    return .t.
  endfunc

  func oQUADNE_1_16.SetRadio()
    this.Parent.oContained.w_QUADNE=trim(this.Parent.oContained.w_QUADNE)
    this.value = ;
      iif(this.Parent.oContained.w_QUADNE=='1',1,;
      0)
  endfunc

  func oQUADNE_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipdat='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADNR_1_17 as StdCheck with uid="ZFBWRUNULW",rtseq=13,rtrep=.f.,left=588, top=532, caption="Quadro NR",;
    ToolTipText = "Quadro NR",;
    HelpContextID = 87974662,;
    cFormVar="w_QUADNR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADNR_1_17.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADNR_1_17.GetRadio()
    this.Parent.oContained.w_QUADNR = this.RadioValue()
    return .t.
  endfunc

  func oQUADNR_1_17.SetRadio()
    this.Parent.oContained.w_QUADNR=trim(this.Parent.oContained.w_QUADNR)
    this.value = ;
      iif(this.Parent.oContained.w_QUADNR=='1',1,;
      0)
  endfunc

  func oQUADNR_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipdat='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADDF_1_18 as StdCheck with uid="UMWUGCFIBU",rtseq=14,rtrep=.f.,left=245, top=565, caption="Quadro DF",;
    ToolTipText = "Quadro DF",;
    HelpContextID = 123837690,;
    cFormVar="w_QUADDF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADDF_1_18.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADDF_1_18.GetRadio()
    this.Parent.oContained.w_QUADDF = this.RadioValue()
    return .t.
  endfunc

  func oQUADDF_1_18.SetRadio()
    this.Parent.oContained.w_QUADDF=trim(this.Parent.oContained.w_QUADDF)
    this.value = ;
      iif(this.Parent.oContained.w_QUADDF=='1',1,;
      0)
  endfunc

  func oQUADDF_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipdat='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADFN_1_19 as StdCheck with uid="VRWABUMXLK",rtseq=15,rtrep=.f.,left=356, top=565, caption="Quadro FN",;
    ToolTipText = "Quadro FN",;
    HelpContextID = 12477190,;
    cFormVar="w_QUADFN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADFN_1_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADFN_1_19.GetRadio()
    this.Parent.oContained.w_QUADFN = this.RadioValue()
    return .t.
  endfunc

  func oQUADFN_1_19.SetRadio()
    this.Parent.oContained.w_QUADFN=trim(this.Parent.oContained.w_QUADFN)
    this.value = ;
      iif(this.Parent.oContained.w_QUADFN=='1',1,;
      0)
  endfunc

  func oQUADFN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipdat='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADTU_1_20 as StdCheck with uid="LZYVTQURMB",rtseq=16,rtrep=.f.,left=476, top=565, caption="Quadro TU",;
    ToolTipText = "Quadro TU",;
    HelpContextID = 144597766,;
    cFormVar="w_QUADTU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADTU_1_20.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADTU_1_20.GetRadio()
    this.Parent.oContained.w_QUADTU = this.RadioValue()
    return .t.
  endfunc

  func oQUADTU_1_20.SetRadio()
    this.Parent.oContained.w_QUADTU=trim(this.Parent.oContained.w_QUADTU)
    this.value = ;
      iif(this.Parent.oContained.w_QUADTU=='1',1,;
      0)
  endfunc

  func oQUADTU_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipdat='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oQUADSE_1_21 as StdCheck with uid="JPJZKUIOYK",rtseq=17,rtrep=.f.,left=588, top=565, caption="Quadro SE",;
    ToolTipText = "Quadro SE",;
    HelpContextID = 124886266,;
    cFormVar="w_QUADSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oQUADSE_1_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oQUADSE_1_21.GetRadio()
    this.Parent.oContained.w_QUADSE = this.RadioValue()
    return .t.
  endfunc

  func oQUADSE_1_21.SetRadio()
    this.Parent.oContained.w_QUADSE=trim(this.Parent.oContained.w_QUADSE)
    this.value = ;
      iif(this.Parent.oContained.w_QUADSE=='1',1,;
      0)
  endfunc

  func oQUADSE_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attipdat='S')
    endwith
   endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="KWFWCRWNHO",left=699, top=543, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 58831382;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        Gsai_Bst(this.Parent.oContained,"StampaDett")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NOMEFILE))
      endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="KUWCMEGMLG",left=750, top=543, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 58831382;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAtgenspe_1_24 as StdCheck with uid="QOWAAHGDHP",rtseq=18,rtrep=.f.,left=91, top=396, caption="Operazioni rilevanti ai fini IVA (Spesometro)", enabled=.f.,;
    ToolTipText = "Identifica il tipo di comunicazione che si intende stampare",;
    HelpContextID = 140632427,;
    cFormVar="w_Atgenspe", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAtgenspe_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAtgenspe_1_24.GetRadio()
    this.Parent.oContained.w_Atgenspe = this.RadioValue()
    return .t.
  endfunc

  func oAtgenspe_1_24.SetRadio()
    this.Parent.oContained.w_Atgenspe=trim(this.Parent.oContained.w_Atgenspe)
    this.value = ;
      iif(this.Parent.oContained.w_Atgenspe=='S',1,;
      0)
  endfunc

  func oAtgenspe_1_24.mHide()
    with this.Parent.oContained
      return (.w_Attipinv=2)
    endwith
  endfunc

  add object oAtgenblk_1_25 as StdCheck with uid="TFRKMUVBXH",rtseq=19,rtrep=.f.,left=354, top=396, caption="Operazioni con soggetti black list", enabled=.f.,;
    ToolTipText = "Identifica il tipo di comunicazione che si intende stampare",;
    HelpContextID = 144580239,;
    cFormVar="w_Atgenblk", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAtgenblk_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAtgenblk_1_25.GetRadio()
    this.Parent.oContained.w_Atgenblk = this.RadioValue()
    return .t.
  endfunc

  func oAtgenblk_1_25.SetRadio()
    this.Parent.oContained.w_Atgenblk=trim(this.Parent.oContained.w_Atgenblk)
    this.value = ;
      iif(this.Parent.oContained.w_Atgenblk=='S',1,;
      0)
  endfunc

  func oAtgenblk_1_25.mHide()
    with this.Parent.oContained
      return (.w_Attipinv=2)
    endwith
  endfunc

  add object oAtgensma_1_26 as StdCheck with uid="GXNBYKASYN",rtseq=20,rtrep=.f.,left=593, top=396, caption="Acquisti da San Marino", enabled=.f.,;
    ToolTipText = "Identifica il tipo di comunicazione che si intende stampare",;
    HelpContextID = 127803033,;
    cFormVar="w_Atgensma", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAtgensma_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAtgensma_1_26.GetRadio()
    this.Parent.oContained.w_Atgensma = this.RadioValue()
    return .t.
  endfunc

  func oAtgensma_1_26.SetRadio()
    this.Parent.oContained.w_Atgensma=trim(this.Parent.oContained.w_Atgensma)
    this.value = ;
      iif(this.Parent.oContained.w_Atgensma=='S',1,;
      0)
  endfunc

  func oAtgensma_1_26.mHide()
    with this.Parent.oContained
      return (.w_Attipinv=2)
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="VDUAONTUST",Visible=.t., Left=9, Top=365,;
    Alignment=1, Width=79, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="KUMWJJFZDI",Visible=.t., Left=8, Top=435,;
    Alignment=1, Width=225, Height=19,;
    Caption="Formato comunicazione e quadri"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_kgt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
