* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_kio                                                        *
*              Inclusione fatture da analisi ordini                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-12-01                                                      *
* Last revis.: 2012-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_kio",oParentObject))

* --- Class definition
define class tgsai_kio as StdForm
  Top    = 5
  Left   = 6

  * --- Standard Properties
  Width  = 563
  Height = 293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-26"
  HelpContextID=1439895
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  cPrg = "gsai_kio"
  cComment = "Inclusione fatture da analisi ordini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CICLO = space(1)
  o_CICLO = space(1)
  w_TIPODOC = space(5)
  w_DESCRI = space(35)
  w_CATEGO = space(2)
  w_APPCIC = space(1)
  w_APPCAT = space(2)
  w_ANNO_FILTRO = space(4)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DATINIFAT = ctod('  /  /  ')
  w_DATFINFAT = ctod('  /  /  ')
  w_IMP_FILTRO = 0
  o_IMP_FILTRO = 0
  w_TIPO_OPE = space(1)
  w_TDORDAPE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_kioPag1","gsai_kio",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCICLO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TIP_DOCU'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CICLO=space(1)
      .w_TIPODOC=space(5)
      .w_DESCRI=space(35)
      .w_CATEGO=space(2)
      .w_APPCIC=space(1)
      .w_APPCAT=space(2)
      .w_ANNO_FILTRO=space(4)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DATINIFAT=ctod("  /  /  ")
      .w_DATFINFAT=ctod("  /  /  ")
      .w_IMP_FILTRO=0
      .w_TIPO_OPE=space(1)
      .w_TDORDAPE=space(1)
        .w_CICLO = 'V'
        .w_TIPODOC = SPACE(5)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_TIPODOC))
          .link_1_4('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_CATEGO = 'OR'
          .DoRTCalc(5,6,.f.)
        .w_ANNO_FILTRO = STR(Year(i_datsys)-1,4)
        .w_DATINI = Cp_chartodate('01-01-'+.w_ANNO_FILTRO)
        .w_DATFIN = Cp_chartodate('31-12-'+.w_ANNO_FILTRO)
        .w_DATINIFAT = Cp_chartodate('01-01-'+.w_ANNO_FILTRO)
        .w_DATFINFAT = Cp_chartodate('31-12-'+.w_ANNO_FILTRO)
        .w_IMP_FILTRO = ABS(.w_IMP_FILTRO)
        .w_TIPO_OPE = 'A'
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CICLO<>.w_CICLO
            .w_TIPODOC = SPACE(5)
          .link_1_4('Full')
        endif
        .DoRTCalc(3,11,.t.)
        if .o_IMP_FILTRO<>.w_IMP_FILTRO
            .w_IMP_FILTRO = ABS(.w_IMP_FILTRO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
  return

  proc Calculate_OVBBJINCRT()
    with this
          * --- Calcolo importo di filtro
          Gsai_Bio(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_YWASZSLRPN()
    with this
          * --- Stampa
          .w_TIPO_OPE = 'S'
          Gsai_Bio(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_SMHESUBYQO()
    with this
          * --- Aggiorna
          .w_TIPO_OPE = 'A'
          Gsai_Bio(this;
              ,'S';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_OVBBJINCRT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Stampa")
          .Calculate_YWASZSLRPN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_SMHESUBYQO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPODOC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPODOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDORDAPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPODOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDORDAPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPODOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPODOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPODOC_1_4'),i_cWhere,'GSOR_ATD',"CAUSALI DOCUMENTI",'Gsai_Kio.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDORDAPE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDORDAPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPODOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDORDAPE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPODOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPODOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDORDAPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPODOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCRI = NVL(_Link_.TDDESDOC,space(35))
      this.w_APPCAT = NVL(_Link_.TDCATDOC,space(2))
      this.w_APPCIC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDORDAPE = NVL(_Link_.TDORDAPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPODOC = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_APPCAT = space(2)
      this.w_APPCIC = space(1)
      this.w_TDORDAPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_APPCIC=.w_CICLO and .w_APPCAT=.w_CATEGO and .w_TDORDAPE<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente oppure incongruente")
        endif
        this.w_TIPODOC = space(5)
        this.w_DESCRI = space(35)
        this.w_APPCAT = space(2)
        this.w_APPCIC = space(1)
        this.w_TDORDAPE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPODOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCICLO_1_1.RadioValue()==this.w_CICLO)
      this.oPgFrm.Page1.oPag.oCICLO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODOC_1_4.value==this.w_TIPODOC)
      this.oPgFrm.Page1.oPag.oTIPODOC_1_4.value=this.w_TIPODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_5.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_5.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINIFAT_1_12.value==this.w_DATINIFAT)
      this.oPgFrm.Page1.oPag.oDATINIFAT_1_12.value=this.w_DATINIFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINFAT_1_13.value==this.w_DATFINFAT)
      this.oPgFrm.Page1.oPag.oDATFINFAT_1_13.value=this.w_DATFINFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oIMP_FILTRO_1_14.value==this.w_IMP_FILTRO)
      this.oPgFrm.Page1.oPag.oIMP_FILTRO_1_14.value=this.w_IMP_FILTRO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_APPCIC=.w_CICLO and .w_APPCAT=.w_CATEGO and .w_TDORDAPE<>'S')  and not(empty(.w_TIPODOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPODOC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente oppure incongruente")
          case   ((empty(.w_DATINI)) or not(.w_DATFIN>=.w_DATINI OR empty(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not(.w_DATFIN>=.w_DATINI OR empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   ((empty(.w_DATINIFAT)) or not(empty(.w_DATFINFAT) OR (.w_DATFINFAT>=.w_DATINIFAT)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINIFAT_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DATINIFAT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not(empty(.w_DATFINFAT) OR (.w_DATFINFAT>=.w_DATINIFAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFINFAT_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CICLO = this.w_CICLO
    this.o_IMP_FILTRO = this.w_IMP_FILTRO
    return

enddefine

* --- Define pages as container
define class tgsai_kioPag1 as StdContainer
  Width  = 559
  height = 293
  stdWidth  = 559
  stdheight = 293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCICLO_1_1 as StdCombo with uid="ZQSVNGVRJV",rtseq=1,rtrep=.f.,left=159,top=20,width=175,height=21;
    , ToolTipText = "Tipo gestione interessata";
    , HelpContextID = 89552422;
    , cFormVar="w_CICLO",RowSource=""+"Impegni da cliente,"+"Ordini a fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCICLO_1_1.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oCICLO_1_1.GetRadio()
    this.Parent.oContained.w_CICLO = this.RadioValue()
    return .t.
  endfunc

  func oCICLO_1_1.SetRadio()
    this.Parent.oContained.w_CICLO=trim(this.Parent.oContained.w_CICLO)
    this.value = ;
      iif(this.Parent.oContained.w_CICLO=='V',1,;
      iif(this.Parent.oContained.w_CICLO=='A',2,;
      0))
  endfunc

  add object oTIPODOC_1_4 as StdField with uid="TWRRFKKQSM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TIPODOC", cQueryName = "TIPODOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente oppure incongruente",;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 206944458,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=159, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPODOC"

  func oTIPODOC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPODOC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPODOC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPODOC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"CAUSALI DOCUMENTI",'Gsai_Kio.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPODOC_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPODOC
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_5 as StdField with uid="GQBUVESELK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 25267658,;
   bGlobalFont=.t.,;
    Height=21, Width=294, Left=234, Top=51, InputMask=replicate('X',35)

  add object oDATINI_1_10 as StdField with uid="BNJRBUZBOH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento iniziale dell'ordine",;
    HelpContextID = 29065674,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=159, Top=88

  func oDATINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN>=.w_DATINI OR empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_11 as StdField with uid="YASEPPWKEQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento finale dell'ordine",;
    HelpContextID = 219054538,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=386, Top=88

  func oDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN>=.w_DATINI OR empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATINIFAT_1_12 as StdField with uid="MHOPTZDACQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATINIFAT", cQueryName = "DATINIFAT",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data iniziale di registrazione di primanota della fattura",;
    HelpContextID = 239371191,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=159, Top=126

  func oDATINIFAT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATFINFAT) OR (.w_DATFINFAT>=.w_DATINIFAT))
    endwith
    return bRes
  endfunc

  add object oDATFINFAT_1_13 as StdField with uid="KMMCVZADHB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATFINFAT", cQueryName = "DATFINFAT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data finale di registrazione di primanota della fattura",;
    HelpContextID = 49382327,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=386, Top=126

  func oDATFINFAT_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATFINFAT) OR (.w_DATFINFAT>=.w_DATINIFAT))
    endwith
    return bRes
  endfunc

  add object oIMP_FILTRO_1_14 as StdField with uid="FEUGIPJYRN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_IMP_FILTRO", cQueryName = "IMP_FILTRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo minimo di imponibile dell'ordine",;
    HelpContextID = 232431354,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=159, Top=163, cSayPict="v_PV(80)", cGetPict="v_GV(80)"


  add object oBtn_1_15 as StdButton with uid="YOLQEZXPPA",left=452, top=237, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare le registrazione di primanota";
    , HelpContextID = 176049642;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        .NotifyEvent('Aggiorna')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="KWFWCRWNHO",left=401, top=237, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 176049642;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .NotifyEvent('Stampa')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="KUWCMEGMLG",left=503, top=237, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 176049642;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_27 as cp_outputCombo with uid="ZLERXIVPWT",left=159, top=202, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 88997914

  add object oStr_1_2 as StdString with uid="NYIFVFJARA",Visible=.t., Left=118, Top=20,;
    Alignment=1, Width=35, Height=15,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="LGLOZIRBGQ",Visible=.t., Left=5, Top=53,;
    Alignment=1, Width=148, Height=15,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ECEQUNDXWF",Visible=.t., Left=18, Top=90,;
    Alignment=1, Width=135, Height=18,;
    Caption="Da data ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RIEEEJURUA",Visible=.t., Left=264, Top=90,;
    Alignment=1, Width=118, Height=18,;
    Caption="A data ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="RSODBDPLPM",Visible=.t., Left=18, Top=128,;
    Alignment=1, Width=135, Height=18,;
    Caption="Da data fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EAWCTJBYIJ",Visible=.t., Left=264, Top=128,;
    Alignment=1, Width=118, Height=18,;
    Caption="A data fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="EMTNFAKHJA",Visible=.t., Left=47, Top=165,;
    Alignment=1, Width=106, Height=18,;
    Caption="Importo ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ZDLOVMWIXH",Visible=.t., Left=67, Top=204,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di Stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_kio','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
