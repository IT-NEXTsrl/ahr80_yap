* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_klc                                                        *
*              Legenda                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-02                                                      *
* Last revis.: 2011-08-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_klc",oParentObject))

* --- Class definition
define class tgsai_klc as StdForm
  Top    = 314
  Left   = 406

  * --- Standard Properties
  Width  = 401
  Height = 266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-08-23"
  HelpContextID=216663913
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsai_klc"
  cComment = "Legenda "
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_COLREGOPE = space(10)
  w_COLNOTREF = space(10)
  w_COLREGREF = space(10)
  w_COLREGMOV = space(10)
  w_COLREGPRO = space(10)
  w_COLREGACC = space(10)
  w_COLACCREF = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_klcPag1","gsai_klc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COLREGOPE=space(10)
      .w_COLNOTREF=space(10)
      .w_COLREGREF=space(10)
      .w_COLREGMOV=space(10)
      .w_COLREGPRO=space(10)
      .w_COLREGACC=space(10)
      .w_COLACCREF=space(10)
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate(RGB(255,255,0))
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate(RGB(0,255,0))
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate(RGB(0,255,255))
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(RGB(210,166,255))
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate(RGB(255,0,0))
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate(RGB(251,125,0))
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate(RGB(255,204,255))
    endwith
    this.DoRTCalc(1,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate(RGB(255,255,0))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(RGB(0,255,0))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(RGB(0,255,255))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(RGB(210,166,255))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(RGB(255,0,0))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(RGB(251,125,0))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(RGB(255,204,255))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate(RGB(255,255,0))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(RGB(0,255,0))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(RGB(0,255,255))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(RGB(210,166,255))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(RGB(255,0,0))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(RGB(251,125,0))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(RGB(255,204,255))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOLREGOPE_1_4.value==this.w_COLREGOPE)
      this.oPgFrm.Page1.oPag.oCOLREGOPE_1_4.value=this.w_COLREGOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLNOTREF_1_7.value==this.w_COLNOTREF)
      this.oPgFrm.Page1.oPag.oCOLNOTREF_1_7.value=this.w_COLNOTREF
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLREGREF_1_10.value==this.w_COLREGREF)
      this.oPgFrm.Page1.oPag.oCOLREGREF_1_10.value=this.w_COLREGREF
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLREGMOV_1_13.value==this.w_COLREGMOV)
      this.oPgFrm.Page1.oPag.oCOLREGMOV_1_13.value=this.w_COLREGMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLREGPRO_1_16.value==this.w_COLREGPRO)
      this.oPgFrm.Page1.oPag.oCOLREGPRO_1_16.value=this.w_COLREGPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLREGACC_1_20.value==this.w_COLREGACC)
      this.oPgFrm.Page1.oPag.oCOLREGACC_1_20.value=this.w_COLREGACC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLACCREF_1_21.value==this.w_COLACCREF)
      this.oPgFrm.Page1.oPag.oCOLACCREF_1_21.value=this.w_COLACCREF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsai_klcPag1 as StdContainer
  Width  = 397
  height = 266
  stdWidth  = 397
  stdheight = 266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOLREGOPE_1_4 as StdField with uid="MJEHQOQHLX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_COLREGOPE", cQueryName = "COLREGOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 21163834,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=361, Top=34, InputMask=replicate('X',10)


  add object oObj_1_5 as cp_setobjprop with uid="PFFQLJUZQP",left=1, top=285, width=249,height=24,;
    caption='Registrazione con operazioni superiori a 3000 euro valorizzate',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLREGOPE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 156403928

  add object oCOLNOTREF_1_7 as StdField with uid="WYYPRLTCXB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COLNOTREF", cQueryName = "COLNOTREF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 207163595,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=361, Top=67, InputMask=replicate('X',10)


  add object oObj_1_8 as cp_setobjprop with uid="OWESXNIIYB",left=1, top=310, width=249,height=24,;
    caption='Nota di rettifica con riferimento',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLNOTREF",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 169886390

  add object oCOLREGREF_1_10 as StdField with uid="TWXSIHRUTD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COLREGREF", cQueryName = "COLREGREF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 247271627,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=361, Top=100, InputMask=replicate('X',10)


  add object oObj_1_11 as cp_setobjprop with uid="EUYRILQSYS",left=1, top=335, width=249,height=24,;
    caption='Registrazione rettificata',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLREGREF",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 8894912

  add object oCOLREGMOV_1_13 as StdField with uid="VSFGBALTBU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COLREGMOV", cQueryName = "COLREGMOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 21163563,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=361, Top=199, InputMask=replicate('X',10)


  add object oObj_1_14 as cp_setobjprop with uid="RIGMVTLUGP",left=1, top=360, width=249,height=24,;
    caption='Registrazione con pi� registri IVA movimentati',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLREGMOV",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 199297375

  add object oCOLREGPRO_1_16 as StdField with uid="UGTQSGVIMM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_COLREGPRO", cQueryName = "COLREGPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 21163672,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=361, Top=232, InputMask=replicate('X',10)


  add object oObj_1_17 as cp_setobjprop with uid="OLUZAGEJHZ",left=1, top=385, width=249,height=24,;
    caption='Registrazione provvisoria',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLREGPRO",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 65267726

  add object oCOLREGACC_1_20 as StdField with uid="TOMOULQWWJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COLREGACC", cQueryName = "COLREGACC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 21163879,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=361, Top=166, InputMask=replicate('X',10)

  add object oCOLACCREF_1_21 as StdField with uid="BUJHSLVLVX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_COLACCREF", cQueryName = "COLACCREF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 176951499,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=361, Top=133, InputMask=replicate('X',10)


  add object oObj_1_22 as cp_setobjprop with uid="SNEZZYJGVX",left=1, top=410, width=249,height=24,;
    caption='Acconto con riferimento',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLACCREF",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 167716315


  add object oObj_1_23 as cp_setobjprop with uid="POPNPPGVQJ",left=1, top=435, width=249,height=24,;
    caption='Registrazione con acconti associati',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLREGACC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 238768925

  add object oStr_1_1 as StdString with uid="PKXBLNMCWH",Visible=.t., Left=21, Top=12,;
    Alignment=0, Width=114, Height=18,;
    Caption="Sfondo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_3 as StdString with uid="NARFDMSICP",Visible=.t., Left=4, Top=35,;
    Alignment=1, Width=353, Height=18,;
    Caption="Registrazione con operazioni superiori a 3000 euro valorizzate"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="PZHAYIWRBC",Visible=.t., Left=46, Top=68,;
    Alignment=1, Width=311, Height=18,;
    Caption="Nota di rettifica con riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="HNHXQVHJVQ",Visible=.t., Left=46, Top=103,;
    Alignment=1, Width=311, Height=18,;
    Caption="Registrazione rettificata"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="HFNPVCTEEQ",Visible=.t., Left=46, Top=202,;
    Alignment=1, Width=311, Height=18,;
    Caption="Registrazione con pi� registri IVA movimentati"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="YZBPMUXYQW",Visible=.t., Left=46, Top=235,;
    Alignment=1, Width=311, Height=18,;
    Caption="Registrazione provvisoria"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="WKOPYERHJP",Visible=.t., Left=46, Top=168,;
    Alignment=1, Width=311, Height=18,;
    Caption="Registrazione con acconti associati"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="YKNNVNDFCB",Visible=.t., Left=46, Top=137,;
    Alignment=1, Width=311, Height=18,;
    Caption="Acconto con riferimento"  ;
  , bGlobalFont=.t.

  add object oBox_1_2 as StdBox with uid="TYWLNHTRLY",left=-1, top=27, width=392,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_klc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
