* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_mdp                                                        *
*              Dettaglio dati estratti antievasione IVA                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsai_mdp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsai_mdp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsai_mdp")
  return

* --- Class definition
define class tgsai_mdp as StdPCForm
  Width  = 680
  Height = 217
  Top    = 3
  Left   = 3
  cComment = "Dettaglio dati estratti antievasione IVA"
  cPrg = "gsai_mdp"
  HelpContextID=188086423
  add object cnt as tcgsai_mdp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsai_mdp as PCContext
  w_DTSERIAL = space(10)
  w_DTRIFPNT = space(10)
  w_DTNUMDOC = 0
  w_DTDATDOC = space(8)
  w_DTALFDOC = space(10)
  w_DTBNVATT = 0
  w_DTBNVAIM = 0
  w_DTSNVATT = 0
  w_DTSNVAIM = 0
  w_DTBNVPTT = 0
  w_DTBNVPIM = 0
  w_DTSNVPTT = 0
  w_DTSNVPIM = 0
  w_DTTIPOPE = space(1)
  w_DTTIPREG = space(1)
  proc Save(i_oFrom)
    this.w_DTSERIAL = i_oFrom.w_DTSERIAL
    this.w_DTRIFPNT = i_oFrom.w_DTRIFPNT
    this.w_DTNUMDOC = i_oFrom.w_DTNUMDOC
    this.w_DTDATDOC = i_oFrom.w_DTDATDOC
    this.w_DTALFDOC = i_oFrom.w_DTALFDOC
    this.w_DTBNVATT = i_oFrom.w_DTBNVATT
    this.w_DTBNVAIM = i_oFrom.w_DTBNVAIM
    this.w_DTSNVATT = i_oFrom.w_DTSNVATT
    this.w_DTSNVAIM = i_oFrom.w_DTSNVAIM
    this.w_DTBNVPTT = i_oFrom.w_DTBNVPTT
    this.w_DTBNVPIM = i_oFrom.w_DTBNVPIM
    this.w_DTSNVPTT = i_oFrom.w_DTSNVPTT
    this.w_DTSNVPIM = i_oFrom.w_DTSNVPIM
    this.w_DTTIPOPE = i_oFrom.w_DTTIPOPE
    this.w_DTTIPREG = i_oFrom.w_DTTIPREG
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DTSERIAL = this.w_DTSERIAL
    i_oTo.w_DTRIFPNT = this.w_DTRIFPNT
    i_oTo.w_DTNUMDOC = this.w_DTNUMDOC
    i_oTo.w_DTDATDOC = this.w_DTDATDOC
    i_oTo.w_DTALFDOC = this.w_DTALFDOC
    i_oTo.w_DTBNVATT = this.w_DTBNVATT
    i_oTo.w_DTBNVAIM = this.w_DTBNVAIM
    i_oTo.w_DTSNVATT = this.w_DTSNVATT
    i_oTo.w_DTSNVAIM = this.w_DTSNVAIM
    i_oTo.w_DTBNVPTT = this.w_DTBNVPTT
    i_oTo.w_DTBNVPIM = this.w_DTBNVPIM
    i_oTo.w_DTSNVPTT = this.w_DTSNVPTT
    i_oTo.w_DTSNVPIM = this.w_DTSNVPIM
    i_oTo.w_DTTIPOPE = this.w_DTTIPOPE
    i_oTo.w_DTTIPREG = this.w_DTTIPREG
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsai_mdp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 680
  Height = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=188086423
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ANTIVADT_IDX = 0
  cFile = "ANTIVADT"
  cKeySelect = "DTSERIAL,DTTIPOPE,DTTIPREG"
  cQueryFilter="DTTIPOPE='P' AND DTTIPREG='V'"
  cKeyWhere  = "DTSERIAL=this.w_DTSERIAL and DTTIPOPE=this.w_DTTIPOPE and DTTIPREG=this.w_DTTIPREG"
  cKeyDetail  = "DTSERIAL=this.w_DTSERIAL and DTTIPOPE=this.w_DTTIPOPE and DTTIPREG=this.w_DTTIPREG"
  cKeyWhereODBC = '"DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';
      +'+" and DTTIPOPE="+cp_ToStrODBC(this.w_DTTIPOPE)';
      +'+" and DTTIPREG="+cp_ToStrODBC(this.w_DTTIPREG)';

  cKeyDetailWhereODBC = '"DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';
      +'+" and DTTIPOPE="+cp_ToStrODBC(this.w_DTTIPOPE)';
      +'+" and DTTIPREG="+cp_ToStrODBC(this.w_DTTIPREG)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ANTIVADT.DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';
      +'+" and ANTIVADT.DTTIPOPE="+cp_ToStrODBC(this.w_DTTIPOPE)';
      +'+" and ANTIVADT.DTTIPREG="+cp_ToStrODBC(this.w_DTTIPREG)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ANTIVADT.DTDATDOC,ANTIVADT.DTNUMDOC,ANTIVADT.DTALFDOC'
  cPrg = "gsai_mdp"
  cComment = "Dettaglio dati estratti antievasione IVA"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DTSERIAL = space(10)
  w_DTRIFPNT = space(10)
  w_DTNUMDOC = 0
  w_DTDATDOC = ctod('  /  /  ')
  w_DTALFDOC = space(10)
  w_DTBNVATT = 0
  w_DTBNVAIM = 0
  w_DTSNVATT = 0
  w_DTSNVAIM = 0
  w_DTBNVPTT = 0
  w_DTBNVPIM = 0
  w_DTSNVPTT = 0
  w_DTSNVPIM = 0
  w_DTTIPOPE = space(1)
  w_DTTIPREG = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_mdpPag1","gsai_mdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ANTIVADT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVADT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVADT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsai_mdp'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ANTIVADT where DTSERIAL=KeySet.DTSERIAL
    *                            and DTTIPOPE=KeySet.DTTIPOPE
    *                            and DTTIPREG=KeySet.DTTIPREG
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2],this.bLoadRecFilter,this.ANTIVADT_IDX,"gsai_mdp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVADT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVADT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVADT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DTSERIAL',this.w_DTSERIAL  ,'DTTIPOPE',this.w_DTTIPOPE  ,'DTTIPREG',this.w_DTTIPREG  )
      select * from (i_cTable) ANTIVADT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DTSERIAL = NVL(DTSERIAL,space(10))
        .w_DTTIPOPE = NVL(DTTIPOPE,space(1))
        .w_DTTIPREG = NVL(DTTIPREG,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ANTIVADT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DTRIFPNT = NVL(DTRIFPNT,space(10))
          .w_DTNUMDOC = NVL(DTNUMDOC,0)
          .w_DTDATDOC = NVL(cp_ToDate(DTDATDOC),ctod("  /  /  "))
          .w_DTALFDOC = NVL(DTALFDOC,space(10))
          .w_DTBNVATT = NVL(DTBNVATT,0)
          .w_DTBNVAIM = NVL(DTBNVAIM,0)
          .w_DTSNVATT = NVL(DTSNVATT,0)
          .w_DTSNVAIM = NVL(DTSNVAIM,0)
          .w_DTBNVPTT = NVL(DTBNVPTT,0)
          .w_DTBNVPIM = NVL(DTBNVPIM,0)
          .w_DTSNVPTT = NVL(DTSNVPTT,0)
          .w_DTSNVPIM = NVL(DTSNVPIM,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_5.enabled = .oPgFrm.Page1.oPag.oBtn_2_5.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DTSERIAL=space(10)
      .w_DTRIFPNT=space(10)
      .w_DTNUMDOC=0
      .w_DTDATDOC=ctod("  /  /  ")
      .w_DTALFDOC=space(10)
      .w_DTBNVATT=0
      .w_DTBNVAIM=0
      .w_DTSNVATT=0
      .w_DTSNVAIM=0
      .w_DTBNVPTT=0
      .w_DTBNVPIM=0
      .w_DTSNVPTT=0
      .w_DTSNVPIM=0
      .w_DTTIPOPE=space(1)
      .w_DTTIPREG=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVADT')
    this.DoRTCalc(1,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_5.enabled = this.oPgFrm.Page1.oPag.oBtn_2_5.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDTBNVATT_2_6.enabled = i_bVal
      .Page1.oPag.oDTBNVAIM_2_7.enabled = i_bVal
      .Page1.oPag.oDTSNVATT_2_8.enabled = i_bVal
      .Page1.oPag.oDTSNVAIM_2_9.enabled = i_bVal
      .Page1.oPag.oDTBNVPTT_2_10.enabled = i_bVal
      .Page1.oPag.oDTBNVPIM_2_11.enabled = i_bVal
      .Page1.oPag.oDTSNVPTT_2_12.enabled = i_bVal
      .Page1.oPag.oDTSNVPIM_2_13.enabled = i_bVal
      .Page1.oPag.oBtn_2_5.enabled = .Page1.oPag.oBtn_2_5.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ANTIVADT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTSERIAL,"DTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTTIPOPE,"DTTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTTIPREG,"DTTIPREG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DTNUMDOC N(15);
      ,t_DTDATDOC D(8);
      ,t_DTALFDOC C(10);
      ,t_DTBNVATT N(18,4);
      ,t_DTBNVAIM N(18,4);
      ,t_DTSNVATT N(18,4);
      ,t_DTSNVAIM N(18,4);
      ,t_DTBNVPTT N(18,4);
      ,t_DTBNVPIM N(18,4);
      ,t_DTSNVPTT N(18,4);
      ,t_DTSNVPIM N(18,4);
      ,CPROWNUM N(10);
      ,t_DTRIFPNT C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsai_mdpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_2.controlsource=this.cTrsName+'.t_DTNUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDTDATDOC_2_3.controlsource=this.cTrsName+'.t_DTDATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDTALFDOC_2_4.controlsource=this.cTrsName+'.t_DTALFDOC'
    this.oPgFRm.Page1.oPag.oDTBNVATT_2_6.controlsource=this.cTrsName+'.t_DTBNVATT'
    this.oPgFRm.Page1.oPag.oDTBNVAIM_2_7.controlsource=this.cTrsName+'.t_DTBNVAIM'
    this.oPgFRm.Page1.oPag.oDTSNVATT_2_8.controlsource=this.cTrsName+'.t_DTSNVATT'
    this.oPgFRm.Page1.oPag.oDTSNVAIM_2_9.controlsource=this.cTrsName+'.t_DTSNVAIM'
    this.oPgFRm.Page1.oPag.oDTBNVPTT_2_10.controlsource=this.cTrsName+'.t_DTBNVPTT'
    this.oPgFRm.Page1.oPag.oDTBNVPIM_2_11.controlsource=this.cTrsName+'.t_DTBNVPIM'
    this.oPgFRm.Page1.oPag.oDTSNVPTT_2_12.controlsource=this.cTrsName+'.t_DTSNVPTT'
    this.oPgFRm.Page1.oPag.oDTSNVPIM_2_13.controlsource=this.cTrsName+'.t_DTSNVPIM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(91)
    this.AddVLine(206)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
      *
      * insert into ANTIVADT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVADT')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVADT')
        i_cFldBody=" "+;
                  "(DTSERIAL,DTRIFPNT,DTNUMDOC,DTDATDOC,DTALFDOC"+;
                  ",DTBNVATT,DTBNVAIM,DTSNVATT,DTSNVAIM,DTBNVPTT"+;
                  ",DTBNVPIM,DTSNVPTT,DTSNVPIM,DTTIPOPE,DTTIPREG,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DTSERIAL)+","+cp_ToStrODBC(this.w_DTRIFPNT)+","+cp_ToStrODBC(this.w_DTNUMDOC)+","+cp_ToStrODBC(this.w_DTDATDOC)+","+cp_ToStrODBC(this.w_DTALFDOC)+;
             ","+cp_ToStrODBC(this.w_DTBNVATT)+","+cp_ToStrODBC(this.w_DTBNVAIM)+","+cp_ToStrODBC(this.w_DTSNVATT)+","+cp_ToStrODBC(this.w_DTSNVAIM)+","+cp_ToStrODBC(this.w_DTBNVPTT)+;
             ","+cp_ToStrODBC(this.w_DTBNVPIM)+","+cp_ToStrODBC(this.w_DTSNVPTT)+","+cp_ToStrODBC(this.w_DTSNVPIM)+","+cp_ToStrODBC(this.w_DTTIPOPE)+","+cp_ToStrODBC(this.w_DTTIPREG)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVADT')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVADT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DTSERIAL',this.w_DTSERIAL,'DTTIPOPE',this.w_DTTIPOPE,'DTTIPREG',this.w_DTTIPREG)
        INSERT INTO (i_cTable) (;
                   DTSERIAL;
                  ,DTRIFPNT;
                  ,DTNUMDOC;
                  ,DTDATDOC;
                  ,DTALFDOC;
                  ,DTBNVATT;
                  ,DTBNVAIM;
                  ,DTSNVATT;
                  ,DTSNVAIM;
                  ,DTBNVPTT;
                  ,DTBNVPIM;
                  ,DTSNVPTT;
                  ,DTSNVPIM;
                  ,DTTIPOPE;
                  ,DTTIPREG;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DTSERIAL;
                  ,this.w_DTRIFPNT;
                  ,this.w_DTNUMDOC;
                  ,this.w_DTDATDOC;
                  ,this.w_DTALFDOC;
                  ,this.w_DTBNVATT;
                  ,this.w_DTBNVAIM;
                  ,this.w_DTSNVATT;
                  ,this.w_DTSNVAIM;
                  ,this.w_DTBNVPTT;
                  ,this.w_DTBNVPIM;
                  ,this.w_DTSNVPTT;
                  ,this.w_DTSNVPIM;
                  ,this.w_DTTIPOPE;
                  ,this.w_DTTIPREG;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_DTDATDOC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVADT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVADT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_DTDATDOC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ANTIVADT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVADT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DTRIFPNT="+cp_ToStrODBC(this.w_DTRIFPNT)+;
                     ",DTNUMDOC="+cp_ToStrODBC(this.w_DTNUMDOC)+;
                     ",DTDATDOC="+cp_ToStrODBC(this.w_DTDATDOC)+;
                     ",DTALFDOC="+cp_ToStrODBC(this.w_DTALFDOC)+;
                     ",DTBNVATT="+cp_ToStrODBC(this.w_DTBNVATT)+;
                     ",DTBNVAIM="+cp_ToStrODBC(this.w_DTBNVAIM)+;
                     ",DTSNVATT="+cp_ToStrODBC(this.w_DTSNVATT)+;
                     ",DTSNVAIM="+cp_ToStrODBC(this.w_DTSNVAIM)+;
                     ",DTBNVPTT="+cp_ToStrODBC(this.w_DTBNVPTT)+;
                     ",DTBNVPIM="+cp_ToStrODBC(this.w_DTBNVPIM)+;
                     ",DTSNVPTT="+cp_ToStrODBC(this.w_DTSNVPTT)+;
                     ",DTSNVPIM="+cp_ToStrODBC(this.w_DTSNVPIM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVADT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DTRIFPNT=this.w_DTRIFPNT;
                     ,DTNUMDOC=this.w_DTNUMDOC;
                     ,DTDATDOC=this.w_DTDATDOC;
                     ,DTALFDOC=this.w_DTALFDOC;
                     ,DTBNVATT=this.w_DTBNVATT;
                     ,DTBNVAIM=this.w_DTBNVAIM;
                     ,DTSNVATT=this.w_DTSNVATT;
                     ,DTSNVAIM=this.w_DTSNVAIM;
                     ,DTBNVPTT=this.w_DTBNVPTT;
                     ,DTBNVPIM=this.w_DTBNVPIM;
                     ,DTSNVPTT=this.w_DTSNVPTT;
                     ,DTSNVPIM=this.w_DTSNVPIM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_DTDATDOC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ANTIVADT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_DTDATDOC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DTRIFPNT with this.w_DTRIFPNT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_5.enabled =this.oPgFrm.Page1.oPag.oBtn_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDTBNVATT_2_6.visible=!this.oPgFrm.Page1.oPag.oDTBNVATT_2_6.mHide()
    this.oPgFrm.Page1.oPag.oDTBNVAIM_2_7.visible=!this.oPgFrm.Page1.oPag.oDTBNVAIM_2_7.mHide()
    this.oPgFrm.Page1.oPag.oDTSNVATT_2_8.visible=!this.oPgFrm.Page1.oPag.oDTSNVATT_2_8.mHide()
    this.oPgFrm.Page1.oPag.oDTSNVAIM_2_9.visible=!this.oPgFrm.Page1.oPag.oDTSNVAIM_2_9.mHide()
    this.oPgFrm.Page1.oPag.oDTBNVPTT_2_10.visible=!this.oPgFrm.Page1.oPag.oDTBNVPTT_2_10.mHide()
    this.oPgFrm.Page1.oPag.oDTBNVPIM_2_11.visible=!this.oPgFrm.Page1.oPag.oDTBNVPIM_2_11.mHide()
    this.oPgFrm.Page1.oPag.oDTSNVPTT_2_12.visible=!this.oPgFrm.Page1.oPag.oDTSNVPTT_2_12.mHide()
    this.oPgFrm.Page1.oPag.oDTSNVPIM_2_13.visible=!this.oPgFrm.Page1.oPag.oDTSNVPIM_2_13.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDTBNVATT_2_6.value==this.w_DTBNVATT)
      this.oPgFrm.Page1.oPag.oDTBNVATT_2_6.value=this.w_DTBNVATT
      replace t_DTBNVATT with this.oPgFrm.Page1.oPag.oDTBNVATT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTBNVAIM_2_7.value==this.w_DTBNVAIM)
      this.oPgFrm.Page1.oPag.oDTBNVAIM_2_7.value=this.w_DTBNVAIM
      replace t_DTBNVAIM with this.oPgFrm.Page1.oPag.oDTBNVAIM_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSNVATT_2_8.value==this.w_DTSNVATT)
      this.oPgFrm.Page1.oPag.oDTSNVATT_2_8.value=this.w_DTSNVATT
      replace t_DTSNVATT with this.oPgFrm.Page1.oPag.oDTSNVATT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSNVAIM_2_9.value==this.w_DTSNVAIM)
      this.oPgFrm.Page1.oPag.oDTSNVAIM_2_9.value=this.w_DTSNVAIM
      replace t_DTSNVAIM with this.oPgFrm.Page1.oPag.oDTSNVAIM_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTBNVPTT_2_10.value==this.w_DTBNVPTT)
      this.oPgFrm.Page1.oPag.oDTBNVPTT_2_10.value=this.w_DTBNVPTT
      replace t_DTBNVPTT with this.oPgFrm.Page1.oPag.oDTBNVPTT_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTBNVPIM_2_11.value==this.w_DTBNVPIM)
      this.oPgFrm.Page1.oPag.oDTBNVPIM_2_11.value=this.w_DTBNVPIM
      replace t_DTBNVPIM with this.oPgFrm.Page1.oPag.oDTBNVPIM_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSNVPTT_2_12.value==this.w_DTSNVPTT)
      this.oPgFrm.Page1.oPag.oDTSNVPTT_2_12.value=this.w_DTSNVPTT
      replace t_DTSNVPTT with this.oPgFrm.Page1.oPag.oDTSNVPTT_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSNVPIM_2_13.value==this.w_DTSNVPIM)
      this.oPgFrm.Page1.oPag.oDTSNVPIM_2_13.value=this.w_DTSNVPIM
      replace t_DTSNVPIM with this.oPgFrm.Page1.oPag.oDTSNVPIM_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_2.value==this.w_DTNUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_2.value=this.w_DTNUMDOC
      replace t_DTNUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTDATDOC_2_3.value==this.w_DTDATDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTDATDOC_2_3.value=this.w_DTDATDOC
      replace t_DTDATDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTDATDOC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTALFDOC_2_4.value==this.w_DTALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTALFDOC_2_4.value=this.w_DTALFDOC
      replace t_DTALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTALFDOC_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVADT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_DTDATDOC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_DTDATDOC))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DTRIFPNT=space(10)
      .w_DTNUMDOC=0
      .w_DTDATDOC=ctod("  /  /  ")
      .w_DTALFDOC=space(10)
      .w_DTBNVATT=0
      .w_DTBNVAIM=0
      .w_DTSNVATT=0
      .w_DTSNVAIM=0
      .w_DTBNVPTT=0
      .w_DTBNVPIM=0
      .w_DTSNVPTT=0
      .w_DTSNVPIM=0
    endwith
    this.DoRTCalc(1,15,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DTRIFPNT = t_DTRIFPNT
    this.w_DTNUMDOC = t_DTNUMDOC
    this.w_DTDATDOC = t_DTDATDOC
    this.w_DTALFDOC = t_DTALFDOC
    this.w_DTBNVATT = t_DTBNVATT
    this.w_DTBNVAIM = t_DTBNVAIM
    this.w_DTSNVATT = t_DTSNVATT
    this.w_DTSNVAIM = t_DTSNVAIM
    this.w_DTBNVPTT = t_DTBNVPTT
    this.w_DTBNVPIM = t_DTBNVPIM
    this.w_DTSNVPTT = t_DTSNVPTT
    this.w_DTSNVPIM = t_DTSNVPIM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DTRIFPNT with this.w_DTRIFPNT
    replace t_DTNUMDOC with this.w_DTNUMDOC
    replace t_DTDATDOC with this.w_DTDATDOC
    replace t_DTALFDOC with this.w_DTALFDOC
    replace t_DTBNVATT with this.w_DTBNVATT
    replace t_DTBNVAIM with this.w_DTBNVAIM
    replace t_DTSNVATT with this.w_DTSNVATT
    replace t_DTSNVAIM with this.w_DTSNVAIM
    replace t_DTBNVPTT with this.w_DTBNVPTT
    replace t_DTBNVPIM with this.w_DTBNVPIM
    replace t_DTSNVPTT with this.w_DTSNVPTT
    replace t_DTSNVPIM with this.w_DTSNVPIM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsai_mdpPag1 as StdContainer
  Width  = 676
  height = 217
  stdWidth  = 676
  stdheight = 217
  resizeXpos=634
  resizeYpos=209
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=1, width=281,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="DTDATDOC",Label1="Data doc.",Field2="DTNUMDOC",Label2="Numero",Field3="DTALFDOC",Label3="Alfa",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32693638

  add object oStr_1_2 as StdString with uid="UGVJBFGRSG",Visible=.t., Left=323, Top=66,;
    Alignment=1, Width=205, Height=18,;
    Caption="Stesso anno - servizi:"  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="TQZEZAWWRP",Visible=.t., Left=323, Top=89,;
    Alignment=1, Width=205, Height=18,;
    Caption="Imposta stesso anno - servizi:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="FMCSIPUMII",Visible=.t., Left=323, Top=112,;
    Alignment=1, Width=205, Height=18,;
    Caption="Anni precedenti - beni:"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="PMFQCLCVQJ",Visible=.t., Left=323, Top=135,;
    Alignment=1, Width=205, Height=18,;
    Caption="Imposta anni precedenti - beni:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="SWIXNUEGTU",Visible=.t., Left=323, Top=158,;
    Alignment=1, Width=205, Height=18,;
    Caption="Anni precedenti - servizi:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="TZKUHAICJF",Visible=.t., Left=372, Top=20,;
    Alignment=1, Width=156, Height=18,;
    Caption="Stesso anno - beni:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="RBAQDMYGHZ",Visible=.t., Left=323, Top=43,;
    Alignment=1, Width=205, Height=18,;
    Caption="Imposta stesso anno - beni:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="UEHGPTTFFB",Visible=.t., Left=323, Top=181,;
    Alignment=1, Width=205, Height=18,;
    Caption="Imposta anni precedenti - servizi:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=24,;
    width=277+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=25,width=276+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDTBNVATT_2_6.Refresh()
      this.Parent.oDTBNVAIM_2_7.Refresh()
      this.Parent.oDTSNVATT_2_8.Refresh()
      this.Parent.oDTSNVAIM_2_9.Refresh()
      this.Parent.oDTBNVPTT_2_10.Refresh()
      this.Parent.oDTBNVPIM_2_11.Refresh()
      this.Parent.oDTSNVPTT_2_12.Refresh()
      this.Parent.oDTSNVPIM_2_13.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_5 as StdButton with uid="XEDXHWOFBI",width=48,height=45,;
   left=295, top=2,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza la registrazione di Prima Nota";
    , HelpContextID = 160027658;
    , Caption='Re\<g.Cont';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_DTRIFPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_5.mCond()
    with this.Parent.oContained
      return (g_COGE='S' AND NOT EMPTY(.w_DTRIFPNT))
    endwith
  endfunc

  add object oDTBNVATT_2_6 as StdTrsField with uid="VGHRQPKBOJ",rtseq=6,rtrep=.t.,;
    cFormVar="w_DTBNVATT",value=0,;
    ToolTipText = "Note di variazione stesso anno - beni",;
    HelpContextID = 236424822,;
    cTotal="", bFixedPos=.t., cQueryName = "DTBNVATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=17, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTBNVATT_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
    endif
  endfunc

  add object oDTBNVAIM_2_7 as StdTrsField with uid="ZRTGPKIMJI",rtseq=7,rtrep=.t.,;
    cFormVar="w_DTBNVAIM",value=0,;
    ToolTipText = "Imposta relativa alle note di variazione stesso anno - beni",;
    HelpContextID = 32010627,;
    cTotal="", bFixedPos=.t., cQueryName = "DTBNVAIM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=40, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTBNVAIM_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
    endif
  endfunc

  add object oDTSNVATT_2_8 as StdTrsField with uid="LUIIUVGSOB",rtseq=8,rtrep=.t.,;
    cFormVar="w_DTSNVATT",value=0,;
    ToolTipText = "Note di variazione stesso anno - servizi",;
    HelpContextID = 236355190,;
    cTotal="", bFixedPos=.t., cQueryName = "DTSNVATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=63, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTSNVATT_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
    endif
  endfunc

  add object oDTSNVAIM_2_9 as StdTrsField with uid="WRUMBPBBFX",rtseq=9,rtrep=.t.,;
    cFormVar="w_DTSNVAIM",value=0,;
    ToolTipText = "Imposta relativa alle note di variazione stesso anno - servizi",;
    HelpContextID = 32080259,;
    cTotal="", bFixedPos=.t., cQueryName = "DTSNVAIM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=86, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTSNVAIM_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
    endif
  endfunc

  add object oDTBNVPTT_2_10 as StdTrsField with uid="AJAYMBSDBR",rtseq=10,rtrep=.t.,;
    cFormVar="w_DTBNVPTT",value=0,;
    ToolTipText = "Note di variazione anni precedenti - beni",;
    HelpContextID = 15233418,;
    cTotal="", bFixedPos=.t., cQueryName = "DTBNVPTT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=109, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTBNVPTT_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
    endif
  endfunc

  add object oDTBNVPIM_2_11 as StdTrsField with uid="WYWDYOSSDW",rtseq=11,rtrep=.t.,;
    cFormVar="w_DTBNVPIM",value=0,;
    ToolTipText = "Imposta relativa alle note di variazione anni precedenti - beni",;
    HelpContextID = 15233411,;
    cTotal="", bFixedPos=.t., cQueryName = "DTBNVPIM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=132, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTBNVPIM_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
    endif
  endfunc

  add object oDTSNVPTT_2_12 as StdTrsField with uid="ZGROWYBASB",rtseq=12,rtrep=.t.,;
    cFormVar="w_DTSNVPTT",value=0,;
    ToolTipText = "Note di variazione anni precedenti - servizi",;
    HelpContextID = 15303050,;
    cTotal="", bFixedPos=.t., cQueryName = "DTSNVPTT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=155, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTSNVPTT_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
    endif
  endfunc

  add object oDTSNVPIM_2_13 as StdTrsField with uid="PTJWQSDGFH",rtseq=13,rtrep=.t.,;
    cFormVar="w_DTSNVPIM",value=0,;
    ToolTipText = "Imposta relativa alle note di variazione anni precedenti - servizi",;
    HelpContextID = 15303043,;
    cTotal="", bFixedPos=.t., cQueryName = "DTSNVPIM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=178, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTSNVPIM_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='A')
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsai_mdpBodyRow as CPBodyRowCnt
  Width=267
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDTNUMDOC_2_2 as StdTrsField with uid="QLJYUFEEHA",rtseq=3,rtrep=.t.,;
    cFormVar="w_DTNUMDOC",value=0,;
    ToolTipText = "Numero documento",;
    HelpContextID = 195022471,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=85, Top=0, cSayPict=["999999999999999"], cGetPict=["999999999999999"]

  add object oDTDATDOC_2_3 as StdTrsField with uid="OHRWOSWGLF",rtseq=4,rtrep=.t.,;
    cFormVar="w_DTDATDOC",value=ctod("  /  /  "),;
    ToolTipText = "Data documento",;
    HelpContextID = 189034119,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=-2, Top=0

  add object oDTALFDOC_2_4 as StdTrsField with uid="REHQXKICCX",rtseq=5,rtrep=.t.,;
    cFormVar="w_DTALFDOC",value=space(10),;
    ToolTipText = "Serie documento",;
    HelpContextID = 203005575,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=200, Top=0, InputMask=replicate('X',10)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDTNUMDOC_2_2.When()
    return(.t.)
  proc oDTNUMDOC_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDTNUMDOC_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="DTTIPOPE='P' AND DTTIPREG='V'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".DTSERIAL=ANTIVADT.DTSERIAL";
  +" and "+i_cAliasName+".DTTIPOPE=ANTIVADT.DTTIPOPE";
  +" and "+i_cAliasName+".DTTIPREG=ANTIVADT.DTTIPREG";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsai_mdp','ANTIVADT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DTSERIAL=ANTIVADT.DTSERIAL";
  +" and "+i_cAliasName2+".DTTIPOPE=ANTIVADT.DTTIPOPE";
  +" and "+i_cAliasName2+".DTTIPREG=ANTIVADT.DTTIPREG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
