* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bme                                                        *
*              Aggiorna dati estratti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-12-05                                                      *
* Last revis.: 2011-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bme",oParentObject)
return(i_retval)

define class tgsai_bme as StdBatch
  * --- Local variables
  w_NUMAGG = 0
  w_DESERIAL = space(10)
  w_AREA = space(1)
  w_CPROWNUM = 0
  * --- WorkFile variables
  ANTIVDDE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorno dati estratti selezionati dallo zoom
    * --- Try
    local bErr_03806210
    bErr_03806210=bTrsErr
    this.Try_03806210()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      Ah_ErrorMsg("Errore nell'aggiornamento")
    endif
    bTrsErr=bTrsErr or bErr_03806210
    * --- End
    This.oParentObject.NotifyEvent("ActivatePage 2")
  endproc
  proc Try_03806210()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    Select (this.oParentObject.w_ZoomAg.cCursor)
    SCAN FOR XCHK = 1 AND NOT EMPTY(NVL(AREA,""))
    this.w_DESERIAL = DESERIAL
    this.w_AREA = AREA
    this.w_CPROWNUM = CPROWNUM
    this.w_NUMAGG = this.w_NUMAGG + 1
    if this.oParentObject.w_TIPREC="4"
      * --- Write into ANTIVDDE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ANTIVDDE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ANTIVDDE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DEVARIMP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWVARIMP),'ANTIVDDE','DEVARIMP');
        +",DEVARIMS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWVARIMS),'ANTIVDDE','DEVARIMS');
            +i_ccchkf ;
        +" where ";
            +"DESERIAL = "+cp_ToStrODBC(this.w_DESERIAL);
            +" and DE__AREA = "+cp_ToStrODBC(this.w_AREA);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            DEVARIMP = this.oParentObject.w_NEWVARIMP;
            ,DEVARIMS = this.oParentObject.w_NEWVARIMS;
            &i_ccchkf. ;
         where;
            DESERIAL = this.w_DESERIAL;
            and DE__AREA = this.w_AREA;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into ANTIVDDE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ANTIVDDE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ANTIVDDE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DEMODPAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWMODPAG),'ANTIVDDE','DEMODPAG');
            +i_ccchkf ;
        +" where ";
            +"DESERIAL = "+cp_ToStrODBC(this.w_DESERIAL);
            +" and DE__AREA = "+cp_ToStrODBC(this.w_AREA);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            DEMODPAG = this.oParentObject.w_NEWMODPAG;
            &i_ccchkf. ;
         where;
            DESERIAL = this.w_DESERIAL;
            and DE__AREA = this.w_AREA;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    Select (this.oParentObject.w_ZoomAg.cCursor)
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    Ah_ErrorMsg("Aggiornati %1 documenti estratti",,,ALLTRIM(STR(this.w_NUMAGG)))
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ANTIVDDE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
