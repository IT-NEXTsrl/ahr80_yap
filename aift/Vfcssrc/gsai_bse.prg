* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bse                                                        *
*              Estrazione dati                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-23                                                      *
* Last revis.: 2015-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bse",oParentObject)
return(i_retval)

define class tgsai_bse as StdBatch
  * --- Local variables
  w_SM = space(4)
  w_qry = .NULL.
  w_SERIAL = 0
  w_NUMMOV = 0
  w_NUMMOVD = 0
  w_cTempTable = space(50)
  w_AIMINFAT = 0
  w_AIMINCOR = 0
  w_AIMINFCO = 0
  w_GENERA = space(1)
  w_AILORFCO = space(1)
  w_REGAPP = space(1)
  w_REGOPE = space(1)
  w_DESCREGOLA = space(0)
  w_CURSOR = space(10)
  w_SPSERIAL = space(10)
  w_SPTIPREC = space(1)
  w_SPNOLEGG = space(1)
  w_SPIVANES = space(1)
  w_SPAUTFAT = space(1)
  w_SPREVCHA = space(1)
  w_AZIENDA = space(5)
  w_SPTIPSOG = space(1)
  w_NUMSER = 0
  w_AGGSER = space(10)
  w_SPTIPCON = space(1)
  w_SPCODCON = space(15)
  w_NORAGSOG = space(10)
  w_Dbserial = space(10)
  w_OldVersione = .f.
  w_Dbtipcon = space(1)
  w_Dbcodcon = space(15)
  w_DMSERIAL = space(10)
  w_DMTIPCON = space(1)
  w_DMCODCON = space(15)
  w_NUMESCL = 0
  w_NUMDAVER = 0
  w_I = 0
  w_NUMESCL = 0
  w_NUMDAVER = 0
  w_I = 0
  * --- WorkFile variables
  DATDESSP_idx=0
  DATESTSP_idx=0
  TMPPNT_IVA_idx=0
  TMPPNT_DETT_idx=0
  DAT_IVAN_idx=0
  DATESTBL_idx=0
  DATESDBL_idx=0
  DATESDSM_idx=0
  DATESTSM_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili spesometro
    * --- Tipo registrazioni
    * --- Separatore
    * --- Variabili black list
    * --- Autotrasportatori
    * --- Variabili acquisti da San Marino
    this.w_SM = alltrim(str(this.oparentobject.w_annosm))
    * --- --
    * --- Variabili black list
    * --- Variabili acquisti da San Marino
    this.oParentObject.w_Msg = ""
    this.oParentobject.oPgFrm.page2.setfocus()
    if this.oParentObject.w_EST_SPES="S"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_ESTBLACK="S"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_EST_SANM="S"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED(this.w_CURSOR)
      if NOT USED("__TMP__")
        Select * from (this.w_CURSOR) into cursor __TMP__
      else
        Select * from __TMP__ into cursor __TMP__ union all ; 
 Select * from (this.w_CURSOR) order by regapp, orregdes, spdatdoc, sptipcon, spcodcon, sprifpnt, ivcodiva
      endif
      USE IN (this.w_CURSOR)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    AddMsgNL("Estrazione spesometro",this)
    if EMPTY(NVL(this.oParentObject.w_PE__ANNO,space(4)))
      ah_ErrorMsg("Parametri non definiti per il periodo di estrazione impostato.%0Elaborazione spesometro interrotta")
    else
      this.w_AZIENDA = i_CODAZI
      AddMsgNL(Space(5)+"Fase 1: eliminazione estrazioni precedenti",this)
      if this.oParentObject.w_ELIDATISP="S"
        * --- Delete from DATDESSP
        i_nConn=i_TableProp[this.DATDESSP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATDESSP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.DATESTSP_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='SPSERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"SPSERIAL "," from "+i_cQueryTable+" where SP__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNOSP)+"",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".SPSERIAL = "+i_cQueryTable+".SPSERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".SPSERIAL = "+i_cQueryTable+".SPSERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from DATESTSP
        i_nConn=i_TableProp[this.DATESTSP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESTSP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SP__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNOSP);
                 )
        else
          delete from (i_cTable) where;
                SP__ANNO = this.oParentObject.w_ANNOSP;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Delete from DATDESSP
        i_nConn=i_TableProp[this.DATDESSP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATDESSP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".SPSERIAL = "+i_cQueryTable+".SPSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
          do vq_exec with '..\aift\exe\query\gsaidbdes',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from DATESTSP
        i_nConn=i_TableProp[this.DATESTSP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESTSP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".SPSERIAL = "+i_cQueryTable+".SPSERIAL";
        
          do vq_exec with '..\aift\exe\query\gsaidbde1s',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Read from DAT_IVAN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IAMINFAT,IAMINCOR,IAMINFCO,IALORFCO"+;
          " from "+i_cTable+" DAT_IVAN where ";
              +"IACODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
              +" and IA__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNOSP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IAMINFAT,IAMINCOR,IAMINFCO,IALORFCO;
          from (i_cTable) where;
              IACODAZI = this.w_AZIENDA;
              and IA__ANNO = this.oParentObject.w_ANNOSP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AIMINFAT = NVL(cp_ToDate(_read_.IAMINFAT),cp_NullValue(_read_.IAMINFAT))
        this.w_AIMINCOR = NVL(cp_ToDate(_read_.IAMINCOR),cp_NullValue(_read_.IAMINCOR))
        this.w_AIMINFCO = NVL(cp_ToDate(_read_.IAMINFCO),cp_NullValue(_read_.IAMINFCO))
        this.w_AILORFCO = NVL(cp_ToDate(_read_.IALORFCO),cp_NullValue(_read_.IALORFCO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Inizio elaborazione
      AddMsgNL(Space(5)+"Fase 2: estrazione dati",this)
      * --- Create temporary table TMPPNT_IVA
      i_nIdx=cp_AddTableDef('TMPPNT_IVA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\aift\exe\query\gsai_bdes',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPPNT_IVA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Query Gsai1bdes
      *     Vengono prese in considerazione solo le registrazioni "Confermate",
      *     intestate,con il flag Tipo estrazione diverso da "Esclusione forzata",con
      *     la somma di imponibile e imposta <>0 e con data registrazione nell'intervallo
      *     di elaborazione
      *     Valorizzazione campi:
      *     Genera: 1) Se registro Acquisti e Tipologia operazione IVA ciclo acquisti=Escluso 
      *     oppure se registro diverso da acquisti e Tipologia operazione IVA ciclo 
      *     vendite=escluso il campo viene valorizzato ad 'N'
      *     2) Se popolata la tabella operazione superiori a 3000 euro ed il flag Tipo estrazione 
      *     uguale a "Inclusione forzata" il campo viene valorizzato ad 'S'
      *     3) Se tipo documento ='FE' oppure 'NE' oppure 'NO' il campo viene 
      *     valorizzato a 'N', in tutti gli altri casi ad 'S'
      *     Spcomsup=Se popolata la tabella operazione superiori a 3000 euro 
      *     il campo viene valorizzato ad 'S' altrimenti ad 'N' 
      *     Regapp= Se tipo documento uguale ad 'FE' oppure 'NE' oppure 'NO'  
      *     oppure se registro acquisti e tipologia operazione IVA  ciclo acquisti=Escluso 
      *     oppure se registro diverso da acquisti e Tipologia operazione IVA ciclo 
      *     vendite=escluso il campo viene valorizzato ad 'X'
      *     Sptiprec= Se tipo documento='FA' e tipo registro uguale ad 'A' il campo assume 'FR' 
      *     Se tipo documento='FA' e tipo registro uguale ad 'V' il campo assume 'FE'
      *     Se il tipo documento='FC' il campo assume 'FE' 
      *     Se il tipo documento='NC' e tipo registro uguale ad 'A' il campo assume 'NR' 
      *     Se il tipo documento='NC' e tipo registro uguale ad 'V' il campo assume 'NE' 
      *     Se il tipo documento vale 'CO' il campo assume 'DF'
      *     in tutti gli altri casi assume 'NN'
      *     Gsai_bdes
      *     Vengono presi in considerazione solo i record con Spcomsup <>'N' (popolata
      *     la tabella operazioni superiori a 3000 euro) ed inclusione forzata
      *     oppure con il flag "Operazioni rilevanti IVA" (Codice conto) diversi da 'E' (Escludi)
      *     Genera=1) Se non valorizzata la tabella operazione superiori a 3000 euro e flag "Operazione rilevanti IVA" uguale ad 'E' il campo assume il valore 'N' 
      *     2) Se Tipo Record uguale 'NR'  e flag soggetto non residente attivo il campo assume il valore 'N'
      *     Sptiprec=Se attivo flag non residente, verifichiamo il Tipo Record se FR diventa 'SE' 
      *     Se 'FE' diventa 'FN' 
      *     Se 'NE' oppure 'DF' diventa 'FN' 
      *     in tutti gli altri casi rimane il valore impostato
      * --- Nel caso di record di tipo nota di credito ( 'NE' o 'NR') moltiplico l'importo
      *     e l'imposta per -1 come definito in analisi
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPRIFPNT,RIGAIVA"
        do vq_exec with 'Gsai9bdes',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"Ivimponi = _t2.Ivimponi";
            +",Ivimpiva = _t2.Ivimpiva";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.Ivimponi = _t2.Ivimponi";
            +",TMPPNT_IVA.Ivimpiva = _t2.Ivimpiva";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = t2.RIGAIVA";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"Ivimponi,";
            +"Ivimpiva";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.Ivimponi,";
            +"t2.Ivimpiva";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"Ivimponi = _t2.Ivimponi";
            +",Ivimpiva = _t2.Ivimpiva";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
                +" and "+i_cTable+".RIGAIVA = "+i_cQueryTable+".RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"Ivimponi = (select Ivimponi from "+i_cQueryTable+" where "+i_cWhere+")";
            +",Ivimpiva = (select Ivimpiva from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Gestione eccezioni
      AddMsgNL(Space(5)+"Fase 3: gestione eccezioni",this)
      * --- Parametri dati iva
      AddMsgNL(Space(5)+"Fase 3.1: gestione eccezioni per parametri su dati iva",this)
      * --- Select from gsaiebdes
      do vq_exec with 'gsaiebdes',this,'_Curs_gsaiebdes','',.f.,.t.
      if used('_Curs_gsaiebdes')
        select _Curs_gsaiebdes
        locate for 1=1
        do while not(eof())
        * --- in base hai filtri impostati devo costruire la where aggiungendo valori
        * --- Create temporary table TMPPNT_DETT
        i_nIdx=cp_AddTableDef('TMPPNT_DETT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"SPTIPCON, SPCODCON, SPRIFPNT,CPROWNUM "," from "+i_cTable;
              +" where 1=0";
              )
        this.TMPPNT_DETT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_qry = Createobject("cpquery")
        this.w_qry.mLoadFile("TMPPNT_IVA","TMPPNT_IVA","TMPPNT_IVA",i_ServerConn[1,2])     
        this.w_qry.cDistinct = "distinct"
        this.w_qry.mLoadField("SPTIPCON", "SPTIPCON", "C", 1, 0)     
        this.w_qry.mLoadField("SPCODCON", "SPCODCON", "C", 15, 0)     
        this.w_qry.mLoadField("SPRIFPNT", "SPRIFPNT", "C", 10, 0)     
        this.w_qry.mLoadField("CPROWNUM", "CPROWNUM", "N", 6, 0)     
        this.w_qry.mLoadField("'12avxgd'", "cpccchk", "C", 10, 0)     
        this.w_DESCREGOLA = "La regola � valida per "
        if NOT EMPTY(_Curs_gsaiebdes.PECAUCON)
          this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"la causale "+alltrim(_Curs_gsaiebdes.CCDESCRI)
          this.w_qry.mLoadWhere("PNCODCAU="+cp_tostrodbc(_Curs_gsaiebdes.PECAUCON)+" AND ","",.t.)     
        endif
        if _Curs_gsaiebdes.PETIPREG<>" "
          this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"il registro "+iif(_Curs_gsaiebdes.PETIPREG="V","vendite",iif(_Curs_gsaiebdes.PETIPREG="A","acquisti","corrispettivi con scorporo"))
          this.w_qry.mLoadWhere("IVTIPREG="+cp_tostrodbc(_Curs_gsaiebdes.PETIPREG)+" AND ","",.t.)     
          if _Curs_gsaiebdes.PENUMREG<>0
            this.w_DESCREGOLA = this.w_DESCREGOLA+" numero registro "+cp_tostrodbc(_Curs_gsaiebdes.PENUMREG)
            this.w_qry.mLoadWhere("IVNUMREG="+cp_tostrodbc(_Curs_gsaiebdes.PENUMREG)+" AND ","",.t.)     
          endif
        endif
        if NOT EMPTY(_Curs_gsaiebdes.PECODIVA)
          this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"il codice I.V.A. "+alltrim(_Curs_gsaiebdes.IVDESIVA)
          this.w_qry.mLoadWhere("IVCODIVA="+cp_tostrodbc(_Curs_gsaiebdes.PECODIVA)+" AND ","",.t.)     
        endif
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"la tipologia soggetto "+iif(_Curs_gsaiebdes.penonres="S","non residente","residente")
        this.w_qry.mLoadWhere("ANFLSGRE="+cp_tostrodbc(_Curs_gsaiebdes.PENONRES)+" AND ","",.t.)     
        this.w_GENERA = IIF(_Curs_gsaiebdes.PECONDIZ = "I", "S", "N")
        this.w_SPTIPREC = _Curs_gsaiebdes.PEQUADRO
        this.w_SPIVANES = iif(_Curs_gsaiebdes.PEVALORI="I","S","N")
        this.w_SPAUTFAT = iif(_Curs_gsaiebdes.PEVALORI="A","S","N")
        this.w_SPREVCHA = iif(_Curs_gsaiebdes.PEVALORI="R","S","N")
        this.w_SPNOLEGG = _Curs_gsaiebdes.PENOLEGG
        this.w_DESCREGOLA = this.w_DESCREGOLA+CHR(13)+"allora � stata"+IIF(_Curs_gsaiebdes.PECONDIZ="I"," inclusa la riga"," esclusa la riga")
        if _Curs_Gsaiebdes.PECONDIZ="I"
          if _Curs_gsaiebdes.PEVALORI $ "IAR"
            this.w_DESCREGOLA = this.w_DESCREGOLA+CHR(13)+"con valorizzazione"+IIF(_Curs_gsaiebdes.PEVALORI="A"," autofattura",IIF(_Curs_gsaiebdes.PEVALORI="I"," IVA non esposta"," reverse charge"))
          endif
          if _Curs_gsaiebdes.PENOLEGG $ "ABCDE"
            this.w_DESCREGOLA = this.w_DESCREGOLA+CHR(13)+IIF(_Curs_gsaiebdes.PEVALORI $ "IAR","e noleggio a","con valorizzazione noleggio a")+IIF(_Curs_gsaiebdes.PENOLEGG="A"," autovettura",IIF(_Curs_gsaiebdes.PENOLEGG="B"," caravan",IIF(_Curs_gsaiebdes.PENOLEGG="C"," altri veicoli",IIF(_Curs_gsaiebdes.PENOLEGG="D"," unit� da diporto"," aeromobili"))))
          endif
        endif
        this.w_REGOPE = IIF(_Curs_gsaiebdes.PECONDIZ="I", "S", " ")
        this.w_qry.mLoadWhere("TMPPNT_IVA.REGAPP<>'A' AND ","",.t.)     
        this.w_cTempTable = i_TableProp[this.TMPPNT_DETT_idx,2]
        this.w_qry.mDoQuery("nocursor", "Exec", .f., .f., this.w_cTempTable, .f., .t.)     
        * --- Creo la query per update
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPPNT_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                  +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                  +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                  +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
          +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
          +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
          +",SPREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','SPREGDES');
          +",SPTIPREC ="+cp_NullLink(cp_ToStrODBC(this.w_SPTIPREC),'TMPPNT_IVA','SPTIPREC');
          +",SPNOLEGG ="+cp_NullLink(cp_ToStrODBC(this.w_SPNOLEGG),'TMPPNT_IVA','SPNOLEGG');
          +",SPIVANES ="+cp_NullLink(cp_ToStrODBC(this.w_SPIVANES),'TMPPNT_IVA','SPIVANES');
          +",SPAUTFAT ="+cp_NullLink(cp_ToStrODBC(this.w_SPAUTFAT),'TMPPNT_IVA','SPAUTFAT');
          +",SPREVCHA ="+cp_NullLink(cp_ToStrODBC(this.w_SPREVCHA),'TMPPNT_IVA','SPREVCHA');
              +i_ccchkf;
              +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                  +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                  +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                  +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
          +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
          +",TMPPNT_IVA.REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
          +",TMPPNT_IVA.REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
          +",TMPPNT_IVA.SPREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','SPREGDES');
          +",TMPPNT_IVA.SPTIPREC ="+cp_NullLink(cp_ToStrODBC(this.w_SPTIPREC),'TMPPNT_IVA','SPTIPREC');
          +",TMPPNT_IVA.SPNOLEGG ="+cp_NullLink(cp_ToStrODBC(this.w_SPNOLEGG),'TMPPNT_IVA','SPNOLEGG');
          +",TMPPNT_IVA.SPIVANES ="+cp_NullLink(cp_ToStrODBC(this.w_SPIVANES),'TMPPNT_IVA','SPIVANES');
          +",TMPPNT_IVA.SPAUTFAT ="+cp_NullLink(cp_ToStrODBC(this.w_SPAUTFAT),'TMPPNT_IVA','SPAUTFAT');
          +",TMPPNT_IVA.SPREVCHA ="+cp_NullLink(cp_ToStrODBC(this.w_SPREVCHA),'TMPPNT_IVA','SPREVCHA');
              +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                  +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                  +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                  +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
          +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
          +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
          +",SPREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','SPREGDES');
          +",SPTIPREC ="+cp_NullLink(cp_ToStrODBC(this.w_SPTIPREC),'TMPPNT_IVA','SPTIPREC');
          +",SPNOLEGG ="+cp_NullLink(cp_ToStrODBC(this.w_SPNOLEGG),'TMPPNT_IVA','SPNOLEGG');
          +",SPIVANES ="+cp_NullLink(cp_ToStrODBC(this.w_SPIVANES),'TMPPNT_IVA','SPIVANES');
          +",SPAUTFAT ="+cp_NullLink(cp_ToStrODBC(this.w_SPAUTFAT),'TMPPNT_IVA','SPAUTFAT');
          +",SPREVCHA ="+cp_NullLink(cp_ToStrODBC(this.w_SPREVCHA),'TMPPNT_IVA','SPREVCHA');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
                  +" and "+i_cTable+".SPTIPCON = "+i_cQueryTable+".SPTIPCON";
                  +" and "+i_cTable+".SPCODCON = "+i_cQueryTable+".SPCODCON";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
          +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
          +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
          +",SPREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','SPREGDES');
          +",SPTIPREC ="+cp_NullLink(cp_ToStrODBC(this.w_SPTIPREC),'TMPPNT_IVA','SPTIPREC');
          +",SPNOLEGG ="+cp_NullLink(cp_ToStrODBC(this.w_SPNOLEGG),'TMPPNT_IVA','SPNOLEGG');
          +",SPIVANES ="+cp_NullLink(cp_ToStrODBC(this.w_SPIVANES),'TMPPNT_IVA','SPIVANES');
          +",SPAUTFAT ="+cp_NullLink(cp_ToStrODBC(this.w_SPAUTFAT),'TMPPNT_IVA','SPAUTFAT');
          +",SPREVCHA ="+cp_NullLink(cp_ToStrODBC(this.w_SPREVCHA),'TMPPNT_IVA','SPREVCHA');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Il campo Regapp assume il valore 'A'
        * --- Drop temporary table TMPPNT_DETT
        i_nIdx=cp_GetTableDefIdx('TMPPNT_DETT')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPPNT_DETT')
        endif
        this.w_qry = .null.
          select _Curs_gsaiebdes
          continue
        enddo
        use
      endif
      * --- Stampa di controllo
      *     Elenco fatture dove � stata applicato parametro su dati iva
      this.w_REGAPP = "A"
      VQ_EXEC("..\aift\exe\query\gsaisbdes", this, "__TMP1__")
      * --- Parametri causali note di variazione
      *     definisco note di variazione solo se non valorizzato OPERSUPE
      AddMsgNL(Space(5)+"Fase 3.2: gestione eccezioni per parametri su note di variazione",this)
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPRIFPNT,RIGAIVA"
        do vq_exec with 'Gsai8bdes',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"REGAPP ="+cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP');
            +",SPTIPREC = _t2.SPTIPREC";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
        +"TMPPNT_IVA.REGAPP ="+cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP');
            +",TMPPNT_IVA.SPTIPREC = _t2.SPTIPREC";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = t2.RIGAIVA";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"REGAPP,";
            +"SPTIPREC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP')+",";
            +"t2.SPTIPREC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
        +"REGAPP ="+cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP');
            +",SPTIPREC = _t2.SPTIPREC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
                +" and "+i_cTable+".RIGAIVA = "+i_cQueryTable+".RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"REGAPP ="+cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP');
            +",SPTIPREC = (select SPTIPREC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Il campo Sptiprec assume i seguenti valori:
      *     Se Sptiprec='FR' diventa 'NR' se 'FE' diventa 'NE' in tutti gli altri casi
      *     rimane il valore impostato, non vengono modificati i segni dell'imponibile
      *     e dell'imposta
      *     Regapp='B'
      * --- Stampa di controllo
      *     Elenco fatture dove � stata applicato parametro per note di variazione
      this.w_REGAPP = "B"
      VQ_EXEC("..\aift\exe\query\gsaisbdes", this, "__TMP2__")
      * --- Vengono analizzati i record con Genera='S' con tipo record uguale
      *     ad 'FE' o 'FR' con la somma di imponibile ed imposta < 0 il campo
      *     Sptiprec assume il valore di NE nel caso di 'FE' e di 'NR' in tutti gli altri casi
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPRIFPNT"
        do vq_exec with 'Gsai10bdes',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"Sptiprec = _t2.Sptiprec";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.Sptiprec = _t2.Sptiprec";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"Sptiprec";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.Sptiprec";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"Sptiprec = _t2.Sptiprec";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"Sptiprec = (select Sptiprec from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Verifico se � stato attivato nei parametri Spesometro il flag "Estrae fatture corrispettivi a saldo a zero"
      *     se attivato vengono prese in considerazioni tutte le registrazioni con tipologia documento='FC'
      *     con totale imponibile+ iva uguale a zero e flag genera uguale ad 'S'
      if this.oParentObject.w_FATCOR="S"
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SPRIFPNT,RIGAIVA"
          do vq_exec with '..\aift\exe\query\Gsai16bdes',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                  +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
              +i_ccchkf;
              +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                  +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
          +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
              +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
                  +" and "+"TMPPNT_IVA.RIGAIVA = t2.RIGAIVA";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
              +"GENERA";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                  +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
                  +" and "+i_cTable+".RIGAIVA = "+i_cQueryTable+".RIGAIVA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Verifica limite importi
      * --- Vengono considerati solo i record con Genera uguale ad 'S'
      *     con tipo record uguale a 'FE' o 'FR' o 'DF' o 'FN' o 'SE' con flag Tipo estrazione diverso da inclusione forzato
      *     e con i seguenti filtri 
      *     Flag "Verifica tipologia operazioni" nei parametri spesometro spento
      *     1) Flag privato non attivo e imponibile <Minimo Fatture  
      *     2) Flag privato attivo  tipo documento='CO' e (imponibile pi� imposta) < Minimo corrispettivi
      *     3) Flag privato attivo tipo documento <>'CO' flag lordo IVA attivo ed (imponibile pi� imposta) < Minimo fatture corrispettivi 
      *     4) Flag privato attivo tipo documento<>'CO' flag lordo IVA spento ed imponibile<Minimo fatture corrispettivi
      *     Flag "Verifica tipologia operazioni" nei parametri spesometro acceso
      *     1) Tipo documento='CO' e (imponibile pi� imposta) < Minimo corrispettivi
      *     2) Tipo documento ='FC' flag lordo IVA attivo ed (imponibile pi� imposta) < Minimo fatture corrispettivi 
      *     3) Tipo documento='FC' flag lordo IVA spento ed imponibile<Minimo fatture corrispettivi
      *     4) In tutti gli altri casi imponibile <Minimo Fatture
      AddMsgNL(Space(5)+"Fase 3.3: controlli e verifica limite importi comunicabili",this)
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPRIFPNT,SPTIPCON,SPCODCON,GENERA"
        do vq_exec with '..\aift\exe\query\gsairbde3s',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
        +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.SPTIPCON = t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = t2.SPCODCON";
                +" and "+"TMPPNT_IVA.GENERA = t2.GENERA";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"GENERA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
                +" and "+i_cTable+".SPTIPCON = "+i_cQueryTable+".SPTIPCON";
                +" and "+i_cTable+".SPCODCON = "+i_cQueryTable+".SPCODCON";
                +" and "+i_cTable+".GENERA = "+i_cQueryTable+".GENERA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      AddMsgNL(Space(5)+"Fase 4: gestione eccezioni per schede carburante",this)
      * --- Inserisco registrazione valide per conti inseriti nel tab schede carburante
      * --- Insert into TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\Gsai7bdes",this.TMPPNT_IVA_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Verifico limite di importo per schede carburante
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPRIFPNT,SPTIPCON,SPCODCON,GENERA"
        do vq_exec with '..\aift\exe\query\gsai15bdes',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
        +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.SPTIPCON = t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = t2.SPCODCON";
                +" and "+"TMPPNT_IVA.GENERA = t2.GENERA";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"GENERA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
                +" and "+i_cTable+".SPTIPCON = "+i_cQueryTable+".SPTIPCON";
                +" and "+i_cTable+".SPCODCON = "+i_cQueryTable+".SPCODCON";
                +" and "+i_cTable+".GENERA = "+i_cQueryTable+".GENERA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Elimino dal cursore temporaneo TMPPNT_IVA tutti i record che ha il flag
      *     "Genera" uguale ad 'N'
      * --- Delete from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"GENERA = "+cp_ToStrODBC("N");
               )
      else
        delete from (i_cTable) where;
              GENERA = "N";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Modifico il valore del campo "Iva non esposta" nel caso in cui la riga
      *     IVA abbia codificato un codice IVA di tipo "Cessione di beni-Registro Vendite"
      *     oppure "Acquisti-Acquisto beni usati" e la causale contabile abbia attivato
      *     il flag "Regime del margine"
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPRIFPNT,RIGAIVA"
        do vq_exec with 'Gsai11bdes',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"Spivanes ="+cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','Spivanes');
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
        +"TMPPNT_IVA.Spivanes ="+cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','Spivanes');
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = t2.RIGAIVA";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"Spivanes";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','Spivanes')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
                +" and "+"TMPPNT_IVA.RIGAIVA = _t2.RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
        +"Spivanes ="+cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','Spivanes');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
                +" and "+i_cTable+".RIGAIVA = "+i_cQueryTable+".RIGAIVA";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"Spivanes ="+cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','Spivanes');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Select from GSAI4BDES
      do vq_exec with 'GSAI4BDES',this,'_Curs_GSAI4BDES','',.f.,.t.
      if used('_Curs_GSAI4BDES')
        select _Curs_GSAI4BDES
        locate for 1=1
        do while not(eof())
        this.w_NUMMOV = NUMERO
          select _Curs_GSAI4BDES
          continue
        enddo
        use
      endif
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select count(distinct SPRIFPNT) as NUMERO  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select count(distinct SPRIFPNT) as NUMERO from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NUMMOVD = NUMERO
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
      if this.w_NUMMOV=0
        ah_ErrorMsg("Nessun movimento da generare")
      else
        * --- Try
        local bErr_02C040F0
        bErr_02C040F0=bTrsErr
        this.Try_02C040F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Errore nella crezione dei dati estratti (%1)", "!", "", i_trsmsg)
        endif
        bTrsErr=bTrsErr or bErr_02C040F0
        * --- End
      endif
      if USED("__TMP1__") OR USED("__TMP2__") OR USED("__TMP3__") 
        FOR this.w_I=1 to 3
        this.w_CURSOR = "__TMP"+STR(this.w_I,1)+"__"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        ENDFOR
        if USED("__TMP__") AND RECCOUNT("__TMP__")>0 AND ah_YesNo("Si vuole stampare il dettaglio dei parametri applicati e gli eventuali messaggi di avvertimento?")
          CP_CHPRN("..\aift\exe\query\gsaisbdes", , this.oParentObject)
        endif
        if USED("__TMP__")
          USE IN __TMP__
        endif
      endif
      if USED("ChkVersion")
        USE IN ChkVersion
      endif
      * --- Drop temporary table TMPPNT_IVA
      i_nIdx=cp_GetTableDefIdx('TMPPNT_IVA')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPNT_IVA')
      endif
    endif
  endproc
  proc Try_02C040F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_SPSERIAL = space(10)
    w_Conn=i_TableProp[this.DATESTSP_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "ANTSP", "i_codazi,w_SPSERIAL")
    this.w_SERIAL = VAL(this.w_SPSERIAL)-1
    this.w_SPSERIAL = cp_StrZeroPad( this.w_SERIAL+this.w_NUMMOV ,10)
    cp_NextTableProg(this, w_Conn, "ANTSP", "i_codazi,w_SPSERIAL")
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_OLDVERSIONE = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_OLDVERSIONE = .t.
      endif
    endif
    AddMsgNL(Space(5)+"Fase 5: generazione progressivi",this)
    * --- genero progressivo seriale
    if this.w_OLDVERSIONE
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select distinct SPTIPCON,SPCODCON,SPTIPSOG  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select distinct SPTIPCON,SPCODCON,SPTIPSOG from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NUMSER = this.w_NUMSER + 1
        this.w_SPTIPCON = _Curs_TMPPNT_IVA.SPTIPCON
        this.w_SPCODCON = _Curs_TMPPNT_IVA.SPCODCON
        this.w_SPTIPSOG = _Curs_TMPPNT_IVA.SPTIPSOG
        this.w_AGGSER = cp_StrZeroPad(this.w_NUMSER + this.w_SERIAL, 10)
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SPSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPPNT_IVA','SPSERIAL');
              +i_ccchkf ;
          +" where ";
              +"SPTIPCON = "+cp_ToStrODBC(this.w_SPTIPCON);
              +" and SPCODCON = "+cp_ToStrODBC(this.w_SPCODCON);
              +" and SPTIPSOG = "+cp_ToStrODBC(this.w_SPTIPSOG);
                 )
        else
          update (i_cTable) set;
              SPSERIAL = this.w_AGGSER;
              &i_ccchkf. ;
           where;
              SPTIPCON = this.w_SPTIPCON;
              and SPCODCON = this.w_SPCODCON;
              and SPTIPSOG = this.w_SPTIPSOG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPTIPCON,SPCODCON,SPTIPSOG"
        do vq_exec with 'GSAI5BDES',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.SPTIPSOG = _t2.SPTIPSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SPSERIAL = _t2.SPSERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.SPTIPSOG = _t2.SPTIPSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.SPSERIAL = _t2.SPSERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPTIPCON = t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = t2.SPCODCON";
                +" and "+"TMPPNT_IVA.SPTIPSOG = t2.SPTIPSOG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"SPSERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.SPSERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPTIPCON = _t2.SPTIPCON";
                +" and "+"TMPPNT_IVA.SPCODCON = _t2.SPCODCON";
                +" and "+"TMPPNT_IVA.SPTIPSOG = _t2.SPTIPSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"SPSERIAL = _t2.SPSERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPTIPCON = "+i_cQueryTable+".SPTIPCON";
                +" and "+i_cTable+".SPCODCON = "+i_cQueryTable+".SPCODCON";
                +" and "+i_cTable+".SPTIPSOG = "+i_cQueryTable+".SPTIPSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SPSERIAL = (select SPSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    AddMsgNL(Space(5)+"Fase 6: inserimento dati",this)
    * --- Insert into DATESTSP
    i_nConn=i_TableProp[this.DATESTSP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTSP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai2bdes",this.DATESTSP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_OLDVERSIONE
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPSERIAL,SPRIFPNT"
        do vq_exec with 'GSAI6BDE1S',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPSERIAL = _t2.SPSERIAL";
                +" and "+"TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPSERIAL = _t2.SPSERIAL";
                +" and "+"TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPSERIAL = t2.SPSERIAL";
                +" and "+"TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPSERIAL = _t2.SPSERIAL";
                +" and "+"TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPSERIAL = "+i_cQueryTable+".SPSERIAL";
                +" and "+i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SPSERIAL,SPRIFPNT"
        do vq_exec with 'GSAI6BDES',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.SPSERIAL = _t2.SPSERIAL";
                +" and "+"TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.SPSERIAL = _t2.SPSERIAL";
                +" and "+"TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.SPSERIAL = t2.SPSERIAL";
                +" and "+"TMPPNT_IVA.SPRIFPNT = t2.SPRIFPNT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.SPSERIAL = _t2.SPSERIAL";
                +" and "+"TMPPNT_IVA.SPRIFPNT = _t2.SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SPSERIAL = "+i_cQueryTable+".SPSERIAL";
                +" and "+i_cTable+".SPRIFPNT = "+i_cQueryTable+".SPRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into DATDESSP
    i_nConn=i_TableProp[this.DATDESSP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATDESSP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai3bdes",this.DATDESSP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Gsai3bde1s
    *     Spimpost: Se Max(Tipo Record)='FR' oppure 'FE' e la Max(Iva no Esposta)='S' allora
    *     l'imposta viene valorizzata a 0
    *     Escludigen:Se Min(Flag Noleggio) <>Max(Tipo Noleggio) il campo assume 1
    *     Se Min(Flag Iva non esposta)<>Max(Flag Iva non esposta)) il campo assume 1
    *     Se Min(Flag Autofattura)<>Max(Flag Autofattura) il campo assume 1
    *     Se Min(Flag Reverse Charge)<>Max(Flag Reverse Charge) il campo assume 1
    *     Se Min(Flag Tipo Record)<>Max(Flag Tipo Record) il campo assume 1  
    *     Se Max(Tipo Record uguale ad 'FE' e la Max(Flag Noleggio) diversa da nessuno 
    *     ed il Max(Flag autofattura) uguale ad 'S' il campo assume 1
    *     Se Max(Flag Tipo record) uguale a 'FR' e la Max(Flag Reverse Charge)
    *     uguale ad 'S' ed la Max(Flag Autofattura) uguale ad 'S'
    *     il campo assume 1
    *     Gsai3bdes
    *     Escludi da generazione:
    *     Se il campo Escludigen vale 1 il campo assume 'S'
    *     Se Tipo Record uguale ad 'FE' e il numero fattura � vuoto oppure
    *     l'importo e l'imposta sono uguali a 0 oppure uno dei due campi � minore
    *     di 0 il campo assume 'S'
    *     Se tipo record uguale ad 'FR' e l'importo e l'imposta sono uguali a 0 oppure uno dei due campi � minore
    *     di 0 il campo assume 'S'
    *     Se tipo record uguale ad 'NE' ed il numero fattura � uguale a 0 oppure 
    *     l'importo e l'imposta sono uguali a 0 il campo assume 'S'
    *     Se tipo record uguale ad 'NR' e l'importo e l'imposta sono uguali a 0 il campo assume 'S'
    *     Se tipo record uguale ad 'FN' e l'importo e l'imposta sono uguali a 0 oppure
    *     l'importo o l'imposta sono minori di zero il campo assume 'S'
    *     Se tipo record uguale ad 'SE' e l'importo e l'imposta sono uguali  a 0
    *     oppure l'importo o l'imposta sono minori di zero il campo
    *     assume il valore 'S'. Inoltre per i record SE e FN se derivano da Note
    *     di variazione il campo assume il valore 'S'
    * --- Aggiorno il flag "Da verificare" nella tabella "Anagrafica dati estratti"
    * --- Write into DATESTSP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DATESTSP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTSP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SPSERIAL"
      do vq_exec with 'GSAI13BDES',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DATESTSP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DATESTSP.SPSERIAL = _t2.SPSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SPDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTSP','SPDACONT');
          +i_ccchkf;
          +" from "+i_cTable+" DATESTSP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DATESTSP.SPSERIAL = _t2.SPSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTSP, "+i_cQueryTable+" _t2 set ";
      +"DATESTSP.SPDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTSP','SPDACONT');
          +Iif(Empty(i_ccchkf),"",",DATESTSP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DATESTSP.SPSERIAL = t2.SPSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTSP set (";
          +"SPDACONT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'DATESTSP','SPDACONT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DATESTSP.SPSERIAL = _t2.SPSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTSP set ";
      +"SPDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTSP','SPDACONT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SPSERIAL = "+i_cQueryTable+".SPSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SPDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTSP','SPDACONT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    VQ_EXEC("..\aift\exe\query\gsaisbde2s", this, "__TMP3__")
    if USED("__TMP3__")
      Select __TMP3__
      COUNT FOR REGAPP="F" TO this.w_NUMDAVER
      COUNT FOR REGAPP="G" TO this.w_NUMESCL
    endif
    ah_ErrorMsg("Generazione completata.%0Creati n. %1 dati estratti (di cui n. %3 da verificare e %4 esclusi) da n. %2 documenti", "!", "", ALLTRIM(STR(this.w_NUMMOV)), ALLTRIM(STR(this.w_NUMMOVD)), ALLTRIM(STR(this.w_NUMDAVER)), ALLTRIM(STR(this.w_NUMESCL)))
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Estrazione dati black list
    AddMsgNL("Estrazione black list",this)
    if EMPTY(NVL(this.oParentObject.w_BL__ANNO,space(4)))
      ah_ErrorMsg("Parametri non definiti per il periodo di estrazione impostato.%0Elaborazione black list interrotta")
    else
      AddMsgNL(Space(5)+"Fase 1: eliminazione estrazioni precedenti",this)
      if this.oParentObject.w_ELIDATI="S"
        if this.oParentObject.w_PERIOD="A"
          * --- Delete from DATESDBL
          i_nConn=i_TableProp[this.DATESDBL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DATESDBL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='DBSERIAL'
            cp_CreateTempTable(i_nConn,i_cTempTable,"DBSERIAL "," from "+i_cQueryTable+" where DB__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND DB__MESE=0 AND DBTRIMES=0",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_cWhere=i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
                  +")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from DATESTBL
          i_nConn=i_TableProp[this.DATESTBL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DB__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                  +" and DB__MESE = "+cp_ToStrODBC(0);
                  +" and DBTRIMES = "+cp_ToStrODBC(0);
                   )
          else
            delete from (i_cTable) where;
                  DB__ANNO = this.oParentObject.w_ANNO;
                  and DB__MESE = 0;
                  and DBTRIMES = 0;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        else
          * --- Delete from DATESDBL
          i_nConn=i_TableProp[this.DATESDBL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DATESDBL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='DBSERIAL'
            cp_CreateTempTable(i_nConn,i_cTempTable,"DBSERIAL "," from "+i_cQueryTable+" where DB__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND DB__MESE="+cp_ToStrODBC(this.oParentObject.w_MESE)+" AND DBTRIMES="+cp_ToStrODBC(this.oParentObject.w_TRIMES)+"",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_cWhere=i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
                  +")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from DATESTBL
          i_nConn=i_TableProp[this.DATESTBL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DB__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                  +" and DB__MESE = "+cp_ToStrODBC(this.oParentObject.w_MESE);
                  +" and DBTRIMES = "+cp_ToStrODBC(this.oParentObject.w_TRIMES);
                   )
          else
            delete from (i_cTable) where;
                  DB__ANNO = this.oParentObject.w_ANNO;
                  and DB__MESE = this.oParentObject.w_MESE;
                  and DBTRIMES = this.oParentObject.w_TRIMES;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      else
        * --- Delete from DATESDBL
        i_nConn=i_TableProp[this.DATESDBL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESDBL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
          do vq_exec with '..\aift\exe\query\Gsai15bedbl',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from DATESTBL
        i_nConn=i_TableProp[this.DATESTBL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
        
          do vq_exec with '..\aift\exe\query\gsai16bedbl',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Inizio elaborazione
      AddMsgNL(Space(5)+"Fase 2: estrazione dati",this)
      * --- Create temporary table TMPPNT_IVA
      i_nIdx=cp_AddTableDef('TMPPNT_IVA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\aift\exe\query\gsai_bedbl',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPPNT_IVA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Gestione eccezioni codici IVA
      AddMsgNL(Space(5)+"Fase 3: gestione eccezioni codici IVA",this)
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DBRIFPNT,IVCODIVA,DBTIPREG"
        do vq_exec with 'GSAI8BEDBL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.IVCODIVA = _t2.IVCODIVA";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBIMC003 = _t2.DBIMC003";
            +",DBIMS003 = _t2.DBIMS003";
            +",DBIMC004 = _t2.DBIMC004";
            +",DBIMS004 = _t2.DBIMS004";
            +",DBIMS005 = _t2.DBIMS005";
            +",GENERA = _t2.GENERA";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.IVCODIVA = _t2.IVCODIVA";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.DBIMC003 = _t2.DBIMC003";
            +",TMPPNT_IVA.DBIMS003 = _t2.DBIMS003";
            +",TMPPNT_IVA.DBIMC004 = _t2.DBIMC004";
            +",TMPPNT_IVA.DBIMS004 = _t2.DBIMS004";
            +",TMPPNT_IVA.DBIMS005 = _t2.DBIMS005";
            +",TMPPNT_IVA.GENERA = _t2.GENERA";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DBRIFPNT = t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.IVCODIVA = t2.IVCODIVA";
                +" and "+"TMPPNT_IVA.DBTIPREG = t2.DBTIPREG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"DBIMC003,";
            +"DBIMS003,";
            +"DBIMC004,";
            +"DBIMS004,";
            +"DBIMS005,";
            +"GENERA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DBIMC003,";
            +"t2.DBIMS003,";
            +"t2.DBIMC004,";
            +"t2.DBIMS004,";
            +"t2.DBIMS005,";
            +"t2.GENERA";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.IVCODIVA = _t2.IVCODIVA";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"DBIMC003 = _t2.DBIMC003";
            +",DBIMS003 = _t2.DBIMS003";
            +",DBIMC004 = _t2.DBIMC004";
            +",DBIMS004 = _t2.DBIMS004";
            +",DBIMS005 = _t2.DBIMS005";
            +",GENERA = _t2.GENERA";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DBRIFPNT = "+i_cQueryTable+".DBRIFPNT";
                +" and "+i_cTable+".IVCODIVA = "+i_cQueryTable+".IVCODIVA";
                +" and "+i_cTable+".DBTIPREG = "+i_cQueryTable+".DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBIMC003 = (select DBIMC003 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DBIMS003 = (select DBIMS003 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DBIMC004 = (select DBIMC004 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DBIMS004 = (select DBIMS004 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DBIMS005 = (select DBIMS005 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",GENERA = (select GENERA from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Elimino dal cursore temporaneo TMPPNT_IVA tutti i record che ha il flag
      *     "Genera" uguale ad 'N'
      * --- Delete from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"GENERA = "+cp_ToStrODBC("N");
               )
      else
        delete from (i_cTable) where;
              GENERA = "N";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Verifico se esistono registrazione dove l'importo totale risulta inferiore al limite
      *     dei parametri (Escludi operazioni inferiori a )
      if this.oParentObject.w_ImpEscBl<>0
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="DBRIFPNT,DBTIPREG"
          do vq_exec with '..\AIFT\EXE\QUERY\GSAI11BEDBL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                  +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DBESCLGE ="+cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','DBESCLGE');
              +i_ccchkf;
              +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                  +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
          +"TMPPNT_IVA.DBESCLGE ="+cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','DBESCLGE');
              +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMPPNT_IVA.DBRIFPNT = t2.DBRIFPNT";
                  +" and "+"TMPPNT_IVA.DBTIPREG = t2.DBTIPREG";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
              +"DBESCLGE";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','DBESCLGE')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                  +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
          +"DBESCLGE ="+cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','DBESCLGE');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".DBRIFPNT = "+i_cQueryTable+".DBRIFPNT";
                  +" and "+i_cTable+".DBTIPREG = "+i_cQueryTable+".DBTIPREG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DBESCLGE ="+cp_NullLink(cp_ToStrODBC("S"),'TMPPNT_IVA','DBESCLGE');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Select from GSAI4BEDBL
      do vq_exec with 'GSAI4BEDBL',this,'_Curs_GSAI4BEDBL','',.f.,.t.
      if used('_Curs_GSAI4BEDBL')
        select _Curs_GSAI4BEDBL
        locate for 1=1
        do while not(eof())
        this.w_NumMov = Numero
          select _Curs_GSAI4BEDBL
          continue
        enddo
        use
      endif
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select count(distinct DBRIFPNT) as NUMERO  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select count(distinct DBRIFPNT) as NUMERO from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NumMovd = Numero
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
      if this.w_NUMMOV=0
        ah_ErrorMsg("Nessun movimento da generare")
      else
        * --- Try
        local bErr_02C71A80
        bErr_02C71A80=bTrsErr
        this.Try_02C71A80()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Errore nella crezione dei dati estratti (%1)", "!", "", i_trsmsg)
        endif
        bTrsErr=bTrsErr or bErr_02C71A80
        * --- End
      endif
      if Used("__TMP__") AND Reccount("__TMP__")>0 AND ah_YesNo("Si vuole stampare l'elenco dei soggetti esclusi?")
        Cp_Chprn("..\aift\exe\query\gsai_bedbl", , this.oParentObject)
      endif
      if USED("ChkVersion")
        USE IN ChkVersion
      endif
      * --- Drop temporary table TMPPNT_IVA
      i_nIdx=cp_GetTableDefIdx('TMPPNT_IVA')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPNT_IVA')
      endif
    endif
  endproc
  proc Try_02C71A80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_Dbserial = Space(10)
    w_Conn=i_TableProp[this.DATESTBL_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "DEBLK", "i_codazi,w_Dbserial")
    this.w_Serial = Val(this.w_Dbserial)-1
    this.w_Dbserial = cp_StrZeroPad( this.w_Serial+this.w_NumMov ,10)
    cp_NextTableProg(this, w_Conn, "DEBLK", "i_codazi,w_Dbserial")
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_OldVersione = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_OldVersione = .t.
      endif
    endif
    AddMsgNL(Space(5)+"Fase 4: generazione progressivi",this)
    if this.w_OldVersione
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select distinct DBTIPCON,DBCODCON, NORAGSOG  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select distinct DBTIPCON,DBCODCON, NORAGSOG from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NumSer = this.w_NumSer + 1
        this.w_Dbtipcon = _Curs_TMPPNT_IVA.Dbtipcon
        this.w_Dbcodcon = _Curs_TMPPNT_IVA.Dbcodcon
        this.w_NoRagSog = _Curs_TMPPNT_IVA.NoRagsog
        this.w_AggSer = cp_StrZeroPad(this.w_NumSer + this.w_Serial, 10)
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DBSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPPNT_IVA','DBSERIAL');
              +i_ccchkf ;
          +" where ";
              +"DBTIPCON = "+cp_ToStrODBC(this.w_DBTIPCON);
              +" and DBCODCON = "+cp_ToStrODBC(this.w_DBCODCON);
              +" and NORAGSOG = "+cp_ToStrODBC(this.w_NORAGSOG);
                 )
        else
          update (i_cTable) set;
              DBSERIAL = this.w_AGGSER;
              &i_ccchkf. ;
           where;
              DBTIPCON = this.w_DBTIPCON;
              and DBCODCON = this.w_DBCODCON;
              and NORAGSOG = this.w_NORAGSOG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DBTIPCON,DBCODCON,NORAGSOG"
        do vq_exec with 'GSAI5BEDBL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DBTIPCON = _t2.DBTIPCON";
                +" and "+"TMPPNT_IVA.DBCODCON = _t2.DBCODCON";
                +" and "+"TMPPNT_IVA.NORAGSOG = _t2.NORAGSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBSERIAL = _t2.DBSERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DBTIPCON = _t2.DBTIPCON";
                +" and "+"TMPPNT_IVA.DBCODCON = _t2.DBCODCON";
                +" and "+"TMPPNT_IVA.NORAGSOG = _t2.NORAGSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.DBSERIAL = _t2.DBSERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DBTIPCON = t2.DBTIPCON";
                +" and "+"TMPPNT_IVA.DBCODCON = t2.DBCODCON";
                +" and "+"TMPPNT_IVA.NORAGSOG = t2.NORAGSOG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"DBSERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DBSERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DBTIPCON = _t2.DBTIPCON";
                +" and "+"TMPPNT_IVA.DBCODCON = _t2.DBCODCON";
                +" and "+"TMPPNT_IVA.NORAGSOG = _t2.NORAGSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"DBSERIAL = _t2.DBSERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DBTIPCON = "+i_cQueryTable+".DBTIPCON";
                +" and "+i_cTable+".DBCODCON = "+i_cQueryTable+".DBCODCON";
                +" and "+i_cTable+".NORAGSOG = "+i_cQueryTable+".NORAGSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBSERIAL = (select DBSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    AddMsgNL(Space(5)+"Fase 5: inserimento dati",this)
    * --- Insert into DATESTBL
    i_nConn=i_TableProp[this.DATESTBL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai2bedbl",this.DATESTBL_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorno il flag di "Esclusione da generazione" nel caso esiste almeno
    *     una riga del quadro Bl con importo negativo
    * --- Write into DATESTBL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DATESTBL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DBSERIAL"
      do vq_exec with 'GSAI1BEDBL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DATESTBL_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DATESTBL.DBSERIAL = _t2.DBSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DBSCLGEN = _t2.DBSCLGEN";
          +",DBDACONT = _t2.DBDACONT";
          +i_ccchkf;
          +" from "+i_cTable+" DATESTBL, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DATESTBL.DBSERIAL = _t2.DBSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTBL, "+i_cQueryTable+" _t2 set ";
          +"DATESTBL.DBSCLGEN = _t2.DBSCLGEN";
          +",DATESTBL.DBDACONT = _t2.DBDACONT";
          +Iif(Empty(i_ccchkf),"",",DATESTBL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DATESTBL.DBSERIAL = t2.DBSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTBL set (";
          +"DBSCLGEN,";
          +"DBDACONT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.DBSCLGEN,";
          +"t2.DBDACONT";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DATESTBL.DBSERIAL = _t2.DBSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTBL set ";
          +"DBSCLGEN = _t2.DBSCLGEN";
          +",DBDACONT = _t2.DBDACONT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DBSCLGEN = (select DBSCLGEN from "+i_cQueryTable+" where "+i_cWhere+")";
          +",DBDACONT = (select DBDACONT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_OldVersione
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DBSERIAL,DBRIFPNT,DBTIPREG"
        do vq_exec with 'GSAI6BED1BL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DBSERIAL = _t2.DBSERIAL";
                +" and "+"TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DBSERIAL = _t2.DBSERIAL";
                +" and "+"TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DBSERIAL = t2.DBSERIAL";
                +" and "+"TMPPNT_IVA.DBRIFPNT = t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.DBTIPREG = t2.DBTIPREG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DBSERIAL = _t2.DBSERIAL";
                +" and "+"TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
                +" and "+i_cTable+".DBRIFPNT = "+i_cQueryTable+".DBRIFPNT";
                +" and "+i_cTable+".DBTIPREG = "+i_cQueryTable+".DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DBSERIAL,DBRIFPNT,DBTIPREG"
        do vq_exec with 'GSAI6BEDBL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DBSERIAL = _t2.DBSERIAL";
                +" and "+"TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DBSERIAL = _t2.DBSERIAL";
                +" and "+"TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DBSERIAL = t2.DBSERIAL";
                +" and "+"TMPPNT_IVA.DBRIFPNT = t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.DBTIPREG = t2.DBTIPREG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DBSERIAL = _t2.DBSERIAL";
                +" and "+"TMPPNT_IVA.DBRIFPNT = _t2.DBRIFPNT";
                +" and "+"TMPPNT_IVA.DBTIPREG = _t2.DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
                +" and "+i_cTable+".DBRIFPNT = "+i_cQueryTable+".DBRIFPNT";
                +" and "+i_cTable+".DBTIPREG = "+i_cQueryTable+".DBTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into DATESDBL
    i_nConn=i_TableProp[this.DATESDBL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESDBL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai3bedbl",this.DATESDBL_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorno il flag "Da verificare" nella tabella "Anagrafica dati estratti black list"
    * --- Write into DATESTBL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DATESTBL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DBSERIAL"
      do vq_exec with 'GSAI13BEDBL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DATESTBL_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DATESTBL.DBSERIAL = _t2.DBSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DBDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTBL','DBDACONT');
          +i_ccchkf;
          +" from "+i_cTable+" DATESTBL, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DATESTBL.DBSERIAL = _t2.DBSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTBL, "+i_cQueryTable+" _t2 set ";
      +"DATESTBL.DBDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTBL','DBDACONT');
          +Iif(Empty(i_ccchkf),"",",DATESTBL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DATESTBL.DBSERIAL = t2.DBSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTBL set (";
          +"DBDACONT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'DATESTBL','DBDACONT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DATESTBL.DBSERIAL = _t2.DBSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTBL set ";
      +"DBDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTBL','DBDACONT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DBSERIAL = "+i_cQueryTable+".DBSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DBDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTBL','DBDACONT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    VQ_EXEC("..\aift\exe\query\gsai17bedbl", this, "__TMP__")
    ah_ErrorMsg("Generazione completata.%0Creati n. %1 dati estratti da n. %2 documenti", "!", "", Alltrim(Str(this.w_NumMov)), Alltrim(Str(this.w_NumMovd)))
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Estrazione dati acquisti da San Marino
    AddMsgNL("Estrazione acquisti da San Marino",this)
    if EMPTY(NVL(this.oParentObject.w_SM__ANNO,space(4)))
      ah_ErrorMsg("Parametri non definiti per il periodo di estrazione impostato.%0Elaborazione acquisti da San Marino interrotta")
    else
      AddMsgNL(Space(5)+"Fase 1: eliminazione estrazioni precedenti",this)
      if this.oParentObject.w_ELIDATISM="S"
        * --- Delete from DATESDSM
        i_nConn=i_TableProp[this.DATESDSM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESDSM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.DATESTSM_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='DSSERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"DMSERIAL AS DSSERIAL "," from "+i_cQueryTable+" where DM__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNOSM)+" AND DM__MESE="+cp_ToStrODBC(this.oParentObject.w_MESESM)+"",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".DSSERIAL = "+i_cQueryTable+".DSSERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".DSSERIAL = "+i_cQueryTable+".DSSERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from DATESTSM
        i_nConn=i_TableProp[this.DATESTSM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESTSM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DM__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNOSM);
                +" and DM__MESE = "+cp_ToStrODBC(this.oParentObject.w_MESESM);
                 )
        else
          delete from (i_cTable) where;
                DM__ANNO = this.oParentObject.w_ANNOSM;
                and DM__MESE = this.oParentObject.w_MESESM;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Delete from DATESDSM
        i_nConn=i_TableProp[this.DATESDSM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESDSM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".DSSERIAL = "+i_cQueryTable+".DSSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
          do vq_exec with '..\aift\exe\query\Gsai15bedsm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from DATESTSM
        i_nConn=i_TableProp[this.DATESTSM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESTSM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".DMSERIAL = "+i_cQueryTable+".DMSERIAL";
        
          do vq_exec with '..\aift\exe\query\gsai16bedsm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Inizio elaborazione
      AddMsgNL(Space(5)+"Fase 2: estrazione dati",this)
      * --- La query Gsai1bedsm utilizzata nella query Gsai_Bedsm estrae tutte
      *     le registrazioni di primanota intestate a fornitore con data registrazione nel
      *     periodo impostato nella maschera di estrazione confermate con causale 
      *     contabile con riferimento impostato a fornitori con registro IVA impostato 
      *     a Vendite e Acquisti. Vengono estrapolati i fornitori con codice ISO impostato
      *     a San Marino con codice paese estero uguale a '037' con tipologia 
      *     documento uguale "Fattura" "Fattura UE"
      * --- Create temporary table TMPPNT_IVA
      i_nIdx=cp_AddTableDef('TMPPNT_IVA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\aift\exe\query\gsai_bedsm',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPPNT_IVA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Gestione eccezioni
      * --- Parametri dati iva
      AddMsgNL(Space(5)+"Fase 3: gestione eccezioni per parametri su dati iva",this)
      * --- La generazione avviene solo nel caso in cui � stata inserita una eccezione
      *     per parametri dati IVA. Vengono visualizzate solo le causali contabili con
      *     riferimento a "Fornitore" con tipo registro uguale a "Vendite/Acquisti" e 
      *     con tipologia documento uguale a "Fatture" e "Fatture UE"
      * --- Select from gsaiebedsm
      do vq_exec with 'gsaiebedsm',this,'_Curs_gsaiebedsm','',.f.,.t.
      if used('_Curs_gsaiebedsm')
        select _Curs_gsaiebedsm
        locate for 1=1
        do while not(eof())
        * --- in base hai filtri impostati devo costruire la where aggiungendo valori
        * --- Create temporary table TMPPNT_DETT
        i_nIdx=cp_AddTableDef('TMPPNT_DETT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"DMTIPCON, DMCODCON, DSRIFPNT,CPROWNUM "," from "+i_cTable;
              +" where 1=0";
              )
        this.TMPPNT_DETT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_qry = Createobject("cpquery")
        this.w_qry.mLoadFile("TMPPNT_IVA","TMPPNT_IVA","TMPPNT_IVA",i_ServerConn[1,2])     
        this.w_qry.cDistinct = "distinct"
        this.w_qry.mLoadField("DMTIPCON", "DMTIPCON", "C", 1, 0)     
        this.w_qry.mLoadField("DMCODCON", "DMCODCON", "C", 15, 0)     
        this.w_qry.mLoadField("DSRIFPNT", "DSRIFPNT", "C", 10, 0)     
        this.w_qry.mLoadField("CPROWNUM", "CPROWNUM", "N", 6, 0)     
        this.w_qry.mLoadField("'12avxgd'", "cpccchk", "C", 10, 0)     
        this.w_DESCREGOLA = "La regola � valida per "
        if NOT EMPTY(_Curs_gsaiebedsm.SMCAUCON)
          this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"la causale "+alltrim(_Curs_gsaiebedsm.CCDESCRI)
          this.w_qry.mLoadWhere("PNCODCAU="+cp_tostrodbc(_Curs_gsaiebedsm.SMCAUCON)+" AND ","",.t.)     
        endif
        if _Curs_gsaiebedsm.SMTIPREG<>" "
          this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"il registro "+iif(_Curs_gsaiebedsm.SMTIPREG="V","vendite","acquisti")
          this.w_qry.mLoadWhere("IVTIPREG="+cp_tostrodbc(_Curs_gsaiebedsm.SMTIPREG)+" AND ","",.t.)     
          if _Curs_gsaiebedsm.SMNUMREG<>0
            this.w_DESCREGOLA = this.w_DESCREGOLA+" numero registro "+cp_tostrodbc(_Curs_gsaiebedsm.SMNUMREG)
            this.w_qry.mLoadWhere("IVNUMREG="+cp_tostrodbc(_Curs_gsaiebedsm.SMNUMREG)+" AND ","",.t.)     
          endif
        endif
        if NOT EMPTY(_Curs_gsaiebedsm.SMCODIVA)
          this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"il codice I.V.A. "+alltrim(_Curs_gsaiebedsm.IVDESIVA)
          this.w_qry.mLoadWhere("IVCODIVA="+cp_tostrodbc(_Curs_gsaiebedsm.SMCODIVA)+" AND ","",.t.)     
        endif
        this.w_GENERA = IIF(_Curs_gsaiebedsm.SMCONDIZ = "I", "S", "N")
        this.w_DESCREGOLA = this.w_DESCREGOLA+CHR(13)+"allora � stata"+IIF(_Curs_gsaiebedsm.SMCONDIZ="I"," inclusa la riga"," esclusa la riga")
        this.w_REGOPE = IIF(_Curs_gsaiebedsm.SMCONDIZ="I", "S", " ")
        this.w_qry.mLoadWhere("TMPPNT_IVA.REGAPP<>'A' AND ","",.t.)     
        this.w_cTempTable = i_TableProp[this.TMPPNT_DETT_idx,2]
        this.w_qry.mDoQuery("nocursor", "Exec", .f., .f., this.w_cTempTable, .f., .t.)     
        * --- Creo la query per update
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPPNT_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
                  +" and "+"TMPPNT_IVA.DMTIPCON = _t2.DMTIPCON";
                  +" and "+"TMPPNT_IVA.DMCODCON = _t2.DMCODCON";
                  +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
          +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
          +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
          +",DSREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DSREGDES');
              +i_ccchkf;
              +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
                  +" and "+"TMPPNT_IVA.DMTIPCON = _t2.DMTIPCON";
                  +" and "+"TMPPNT_IVA.DMCODCON = _t2.DMCODCON";
                  +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
          +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
          +",TMPPNT_IVA.REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
          +",TMPPNT_IVA.REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
          +",TMPPNT_IVA.DSREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DSREGDES');
              +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
                  +" and "+"TMPPNT_IVA.DMTIPCON = _t2.DMTIPCON";
                  +" and "+"TMPPNT_IVA.DMCODCON = _t2.DMCODCON";
                  +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
          +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
          +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
          +",DSREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DSREGDES');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".DSRIFPNT = "+i_cQueryTable+".DSRIFPNT";
                  +" and "+i_cTable+".DMTIPCON = "+i_cQueryTable+".DMTIPCON";
                  +" and "+i_cTable+".DMCODCON = "+i_cQueryTable+".DMCODCON";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
          +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
          +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
          +",DSREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DSREGDES');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Il campo Regapp assume il valore 'A'
        * --- Drop temporary table TMPPNT_DETT
        i_nIdx=cp_GetTableDefIdx('TMPPNT_DETT')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPPNT_DETT')
        endif
        this.w_qry = .null.
          select _Curs_gsaiebedsm
          continue
        enddo
        use
      endif
      * --- Stampa di controllo
      *     Elenco fatture dove � stata applicato parametro su dati iva
      this.w_REGAPP = "A"
      VQ_EXEC("..\aift\exe\query\gsaisbedsm", this, "__TMP1__")
      * --- Elimino dal cursore temporaneo TMPPNT_IVA tutti i record che ha il flag
      *     "Genera" uguale ad 'N'
      * --- Delete from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"GENERA = "+cp_ToStrODBC("N");
               )
      else
        delete from (i_cTable) where;
              GENERA = "N";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Select from GSAI4BEDSM
      do vq_exec with 'GSAI4BEDSM',this,'_Curs_GSAI4BEDSM','',.f.,.t.
      if used('_Curs_GSAI4BEDSM')
        select _Curs_GSAI4BEDSM
        locate for 1=1
        do while not(eof())
        this.w_NUMMOV = NUMERO
          select _Curs_GSAI4BEDSM
          continue
        enddo
        use
      endif
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select count(distinct DSRIFPNT) as NUMERO  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select count(distinct DSRIFPNT) as NUMERO from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NUMMOVD = NUMERO
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
      if this.w_NUMMOV=0
        ah_ErrorMsg("Nessun movimento da generare")
      else
        * --- Try
        local bErr_02CBBFD0
        bErr_02CBBFD0=bTrsErr
        this.Try_02CBBFD0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Errore nella crezione dei dati estratti (%1)", "!", "", i_trsmsg)
        endif
        bTrsErr=bTrsErr or bErr_02CBBFD0
        * --- End
      endif
      if USED("__TMP1__") OR USED("__TMP2__")
        FOR this.w_I=1 to 2
        this.w_CURSOR = "__TMP"+STR(this.w_I,1)+"__"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        ENDFOR
        if USED("__TMP__") AND RECCOUNT("__TMP__")>0 AND ah_YesNo("Si vuole stampare il dettaglio dei parametri applicati e gli eventuali messaggi di avvertimento?")
          CP_CHPRN("..\aift\exe\query\gsaisbedsm", , this.oParentObject)
        endif
        if USED("__TMP__")
          USE IN __TMP__
        endif
      endif
      if USED("ChkVersion")
        USE IN ChkVersion
      endif
      * --- Drop temporary table TMPPNT_IVA
      i_nIdx=cp_GetTableDefIdx('TMPPNT_IVA')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPNT_IVA')
      endif
    endif
  endproc
  proc Try_02CBBFD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_DMSERIAL = space(10)
    w_Conn=i_TableProp[this.DATESTSM_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "DEBSM", "i_codazi,w_DMSERIAL")
    this.w_SERIAL = VAL(this.w_DMSERIAL)-1
    this.w_DMSERIAL = cp_StrZeroPad( this.w_SERIAL+this.w_NUMMOV ,10)
    cp_NextTableProg(this, w_Conn, "DEBSM", "i_codazi,w_DMSERIAL")
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_OLDVERSIONE = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_OLDVERSIONE = .t.
      endif
    endif
    AddMsgNL(Space(5)+"Fase 4: generazione progressivi",this)
    * --- genero progressivo seriale
    if this.w_OLDVERSIONE
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select distinct DMTIPCON,DMCODCON  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select distinct DMTIPCON,DMCODCON from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NUMSER = this.w_NUMSER + 1
        this.w_DMTIPCON = _Curs_TMPPNT_IVA.DMTIPCON
        this.w_DMCODCON = _Curs_TMPPNT_IVA.DMCODCON
        this.w_AGGSER = cp_StrZeroPad(this.w_NUMSER + this.w_SERIAL, 10)
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DMSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPPNT_IVA','DMSERIAL');
              +i_ccchkf ;
          +" where ";
              +"DMTIPCON = "+cp_ToStrODBC(this.w_DMTIPCON);
              +" and DMCODCON = "+cp_ToStrODBC(this.w_DMCODCON);
                 )
        else
          update (i_cTable) set;
              DMSERIAL = this.w_AGGSER;
              &i_ccchkf. ;
           where;
              DMTIPCON = this.w_DMTIPCON;
              and DMCODCON = this.w_DMCODCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DMTIPCON,DMCODCON"
        do vq_exec with 'GSAI5BEDSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DMTIPCON = _t2.DMTIPCON";
                +" and "+"TMPPNT_IVA.DMCODCON = _t2.DMCODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DMSERIAL = _t2.DMSERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DMTIPCON = _t2.DMTIPCON";
                +" and "+"TMPPNT_IVA.DMCODCON = _t2.DMCODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.DMSERIAL = _t2.DMSERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DMTIPCON = t2.DMTIPCON";
                +" and "+"TMPPNT_IVA.DMCODCON = t2.DMCODCON";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"DMSERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DMSERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DMTIPCON = _t2.DMTIPCON";
                +" and "+"TMPPNT_IVA.DMCODCON = _t2.DMCODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"DMSERIAL = _t2.DMSERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DMTIPCON = "+i_cQueryTable+".DMTIPCON";
                +" and "+i_cTable+".DMCODCON = "+i_cQueryTable+".DMCODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DMSERIAL = (select DMSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    AddMsgNL(Space(5)+"Fase 5: inserimento dati",this)
    * --- Insert into DATESTSM
    i_nConn=i_TableProp[this.DATESTSM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTSM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai2bedsm",this.DATESTSM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Il campo Escludi da generazione viene valorizzato ad 'S' nei seguenti casi:
    *     1) Persona giuridica: Ragione sociale o stato estero sede legale o citt�
    *     estera sede legale o indirizzo estero sede legale vuota
    *     2) Persona fisica: Cognome o nome o data di nascita o comune o stato 
    *     estero di nascita o stato estero domicilio fiscale vuoto
    *     3) Altro: Entrambi i campi elencati sopra
    if this.w_OLDVERSIONE
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DMSERIAL,DSRIFPNT"
        do vq_exec with 'GSAI6BED1SM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DMSERIAL = _t2.DMSERIAL";
                +" and "+"TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DMSERIAL = _t2.DMSERIAL";
                +" and "+"TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DMSERIAL = t2.DMSERIAL";
                +" and "+"TMPPNT_IVA.DSRIFPNT = t2.DSRIFPNT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DMSERIAL = _t2.DMSERIAL";
                +" and "+"TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DMSERIAL = "+i_cQueryTable+".DMSERIAL";
                +" and "+i_cTable+".DSRIFPNT = "+i_cQueryTable+".DSRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DMSERIAL,DSRIFPNT"
        do vq_exec with 'GSAI6BEDSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DMSERIAL = _t2.DMSERIAL";
                +" and "+"TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DMSERIAL = _t2.DMSERIAL";
                +" and "+"TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DMSERIAL = t2.DMSERIAL";
                +" and "+"TMPPNT_IVA.DSRIFPNT = t2.DSRIFPNT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DMSERIAL = _t2.DMSERIAL";
                +" and "+"TMPPNT_IVA.DSRIFPNT = _t2.DSRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DMSERIAL = "+i_cQueryTable+".DMSERIAL";
                +" and "+i_cTable+".DSRIFPNT = "+i_cQueryTable+".DSRIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into DATESDSM
    i_nConn=i_TableProp[this.DATESDSM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESDSM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai3bedsm",this.DATESDSM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorno il flag "Da verificare" nella tabella "Anagrafica dati estratti"
    * --- Write into DATESTSM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DATESTSM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTSM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DMSERIAL"
      do vq_exec with 'GSAI13BEDSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DATESTSM_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DATESTSM.DMSERIAL = _t2.DMSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DMDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTSM','DMDACONT');
          +i_ccchkf;
          +" from "+i_cTable+" DATESTSM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DATESTSM.DMSERIAL = _t2.DMSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTSM, "+i_cQueryTable+" _t2 set ";
      +"DATESTSM.DMDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTSM','DMDACONT');
          +Iif(Empty(i_ccchkf),"",",DATESTSM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DATESTSM.DMSERIAL = t2.DMSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTSM set (";
          +"DMDACONT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'DATESTSM','DMDACONT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DATESTSM.DMSERIAL = _t2.DMSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTSM set ";
      +"DMDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTSM','DMDACONT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DMSERIAL = "+i_cQueryTable+".DMSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DMDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'DATESTSM','DMDACONT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    VQ_EXEC("..\aift\exe\query\gsaisbed2sm", this, "__TMP2__")
    if USED("__TMP2__")
      Select __TMP2__
      COUNT FOR REGAPP="F" TO this.w_NUMDAVER
      COUNT FOR REGAPP="G" TO this.w_NUMESCL
    endif
    ah_ErrorMsg("Generazione completata.%0Creati n. %1 dati estratti (di cui n. %3 da verificare e %4 esclusi) da n. %2 documenti", "!", "", ALLTRIM(STR(this.w_NUMMOV)), ALLTRIM(STR(this.w_NUMMOVD)), ALLTRIM(STR(this.w_NUMDAVER)), ALLTRIM(STR(this.w_NUMESCL)))
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='DATDESSP'
    this.cWorkTables[2]='DATESTSP'
    this.cWorkTables[3]='*TMPPNT_IVA'
    this.cWorkTables[4]='*TMPPNT_DETT'
    this.cWorkTables[5]='DAT_IVAN'
    this.cWorkTables[6]='DATESTBL'
    this.cWorkTables[7]='DATESDBL'
    this.cWorkTables[8]='DATESDSM'
    this.cWorkTables[9]='DATESTSM'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_gsaiebdes')
      use in _Curs_gsaiebdes
    endif
    if used('_Curs_GSAI4BDES')
      use in _Curs_GSAI4BDES
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    if used('_Curs_GSAI4BEDBL')
      use in _Curs_GSAI4BEDBL
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    if used('_Curs_gsaiebedsm')
      use in _Curs_gsaiebedsm
    endif
    if used('_Curs_GSAI4BEDSM')
      use in _Curs_GSAI4BEDSM
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
