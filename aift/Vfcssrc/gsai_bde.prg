* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bde                                                        *
*              Estrazione dati                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-23                                                      *
* Last revis.: 2011-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bde",oParentObject)
return(i_retval)

define class tgsai_bde as StdBatch
  * --- Local variables
  w_qry = .NULL.
  w_DESERIAL = space(10)
  w_SERIAL = 0
  w_NUMMOV = 0
  w_NUMMOVD = 0
  w_cTempTable = space(50)
  w_AIMINFAT = 0
  w_AIMINCOR = 0
  w_AIMINFCO = 0
  w_GENERA = space(1)
  w_AILORFCO = space(1)
  w_REGAPP = space(1)
  w_REGOPE = space(1)
  w_DESCREGOLA = space(0)
  w_CURSOR = space(10)
  w_CODATT = space(5)
  w_AZIENDA = space(5)
  w_OLDVERSIONE = .f.
  w_NUMSER = 0
  w_AGGSER = space(10)
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  w_NORAGSOG = space(10)
  w_NUMESCL = 0
  w_NUMDAVER = 0
  w_I = 0
  * --- WorkFile variables
  TMPPNT_IVA_idx=0
  ANTIVAAN_idx=0
  ANTIVADT_idx=0
  ANTIVADE_idx=0
  ANTIVDDE_idx=0
  ANTIVDPN_idx=0
  ANTIVDPI_idx=0
  ANTIVDPC_idx=0
  TMPPNT_DETT_idx=0
  TMPPNT_MAST_idx=0
  TMPRIPA1_idx=0
  OPERSUPE_idx=0
  TMPRIPA2_idx=0
  ANTIVDDR_idx=0
  TMPRIPA3_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Non estrarre dati per record di tipo 4-5
    * --- Data fino alla quale considerare le note di variazione
    * --- Tipo registrazioni
    * --- Non considerare i soggetti privati
    * --- Considera i soggetti privati dalla data
    * --- Considera per i contratti collegati le fatture dalle date
    * --- Determina modalit� di pagamento da importo
    * --- Segno credito/debito per aumento e diminuzione imponibile
    * --- Segno credito/debito per aumento e diminuzione imposta
    * --- --
    this.oParentObject.w_Msg = ""
    if EMPTY(NVL(this.oParentObject.w_PE__ANNO,space(4)))
      i_retcode = 'stop'
      return
    endif
    AddMsgNL("Fase 1: eliminazione estrazioni precedenti",this)
    if this.oParentObject.w_ELIDATI="S"
      * --- Delete from ANTIVDDR
      i_nConn=i_TableProp[this.ANTIVDDR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.ANTIVADE_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='RESERIAL'
        cp_CreateTempTable(i_nConn,i_cTempTable,"DESERIAL AS RESERIAL "," from "+i_cQueryTable+" where DE__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+"",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_cWhere=i_cTable+".RESERIAL = "+i_cQueryTable+".RESERIAL";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".RESERIAL = "+i_cQueryTable+".RESERIAL";
              +")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ANTIVDDE
      i_nConn=i_TableProp[this.ANTIVDDE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.ANTIVADE_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='DESERIAL'
        cp_CreateTempTable(i_nConn,i_cTempTable,"DESERIAL "," from "+i_cQueryTable+" where DE__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+"",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
              +")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ANTIVADE
      i_nConn=i_TableProp[this.ANTIVADE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVADE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"DE__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
               )
      else
        delete from (i_cTable) where;
              DE__ANNO = this.oParentObject.w_ANNO;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Delete from ANTIVDDR
      i_nConn=i_TableProp[this.ANTIVDDR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".RESERIAL = "+i_cQueryTable+".RESERIAL";
              +" and "+i_cTable+".RE__AREA = "+i_cQueryTable+".RE__AREA";
              +" and "+i_cTable+".REROWNUM = "+i_cQueryTable+".REROWNUM";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with '..\aift\exe\query\gsaidbde2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ANTIVDDE
      i_nConn=i_TableProp[this.ANTIVDDE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
              +" and "+i_cTable+".DE__AREA = "+i_cQueryTable+".DE__AREA";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with '..\aift\exe\query\gsaidbde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ANTIVADE
      i_nConn=i_TableProp[this.ANTIVADE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVADE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
      
        do vq_exec with '..\aift\exe\query\gsaidbde1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Verifico se l'azienda gestisce pi� attivit�, effettuo questa operazione per
    *     avere i seguenti campi: Minimo fatture ( Aiminfat ) Minimo corrispettivi ( Aimincor)
    *     e Minimo fatture corrispettivi ( Aiminfco )
    this.w_AIMINFAT = 0
    this.w_AIMINCOR = 0
    this.w_AIMINFCO = 0
    this.w_AILORFCO = "N"
    this.w_AZIENDA = i_CODAZI
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      if g_Attivi<>"S"
        this.w_CODATT = g_CATAZI
      else
        this.w_CODATT = SPACE(5)
      endif
      * --- Select from Gsai14bde
      do vq_exec with 'Gsai14bde',this,'_Curs_Gsai14bde','',.f.,.t.
      if used('_Curs_Gsai14bde')
        select _Curs_Gsai14bde
        locate for 1=1
        do while not(eof())
        this.w_AIMINFAT = _Curs_Gsai14bde.AIMINFAT
        this.w_AIMINCOR = _Curs_Gsai14bde.AIMINCOR
        this.w_AIMINFCO = _Curs_Gsai14bde.AIMINFCO
        this.w_AILORFCO = _Curs_Gsai14bde.AILORFCO
        Exit
          select _Curs_Gsai14bde
          continue
        enddo
        use
      endif
    else
      * --- Select from Gsai14bder
      do vq_exec with 'Gsai14bder',this,'_Curs_Gsai14bder','',.f.,.t.
      if used('_Curs_Gsai14bder')
        select _Curs_Gsai14bder
        locate for 1=1
        do while not(eof())
        this.w_AIMINFAT = _Curs_Gsai14bder.IAMINFAT
        this.w_AIMINCOR = _Curs_Gsai14bder.IAMINCOR
        this.w_AIMINFCO = _Curs_Gsai14bder.IAMINFCO
        this.w_AILORFCO = _Curs_Gsai14bder.IALORFCO
          select _Curs_Gsai14bder
          continue
        enddo
        use
      endif
    endif
    * --- Inizio elaborazione
    AddMsgNL("Fase 2: estrazione dati",this)
    * --- Create temporary table TMPPNT_IVA
    i_nIdx=cp_AddTableDef('TMPPNT_IVA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\aift\exe\query\gsai_bde',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPPNT_IVA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Gestione eccezioni
    AddMsgNL("Fase 3: gestione eccezioni",this)
    * --- Parametri dati iva
    AddMsgNL("Fase 3.1: gestione eccezioni per parametri su dati iva",this)
    * --- Select from gsaiebde
    do vq_exec with 'gsaiebde',this,'_Curs_gsaiebde','',.f.,.t.
    if used('_Curs_gsaiebde')
      select _Curs_gsaiebde
      locate for 1=1
      do while not(eof())
      * --- in base hai filtri impostati devo costruire la where aggiungendo valori
      * --- Create temporary table TMPPNT_DETT
      i_nIdx=cp_AddTableDef('TMPPNT_DETT') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"DETIPCON, DECODCON, DERIFPNT,CPROWNUM "," from "+i_cTable;
            +" where 1=0";
            )
      this.TMPPNT_DETT_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_qry = Createobject("cpquery")
      this.w_qry.mLoadFile("TMPPNT_IVA","TMPPNT_IVA","TMPPNT_IVA",i_ServerConn[1,2])     
      this.w_qry.cDistinct = "distinct"
      this.w_qry.mLoadField("DETIPCON", "DETIPCON", "C", 1, 0)     
      this.w_qry.mLoadField("DECODCON", "DECODCON", "C", 15, 0)     
      this.w_qry.mLoadField("DERIFPNT", "DERIFPNT", "C", 10, 0)     
      this.w_qry.mLoadField("CPROWNUM", "CPROWNUM", "N", 6, 0)     
      this.w_qry.mLoadField("'12avxgd'", "cpccchk", "C", 10, 0)     
      this.w_DESCREGOLA = "La regola � valida per "
      if NOT EMPTY(_Curs_gsaiebde.PECAUCON)
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"la causale "+alltrim(_Curs_gsaiebde.CCDESCRI)
        this.w_qry.mLoadWhere("PNCODCAU="+cp_tostrodbc(_Curs_gsaiebde.PECAUCON)+" AND ","",.t.)     
      endif
      if _Curs_gsaiebde.PETIPREG<>" "
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"il registro "+iif(_Curs_gsaiebde.PETIPREG="V","vendite",iif(_Curs_gsaiebde.PETIPREG="A","acquisti","corrispettivi con scorporo"))
        this.w_qry.mLoadWhere("IVTIPREG="+cp_tostrodbc(_Curs_gsaiebde.PETIPREG)+" AND ","",.t.)     
        if _Curs_gsaiebde.PENUMREG<>0
          this.w_DESCREGOLA = this.w_DESCREGOLA+" numero registro "+cp_tostrodbc(_Curs_gsaiebde.PENUMREG)
          this.w_qry.mLoadWhere("IVNUMREG="+cp_tostrodbc(_Curs_gsaiebde.PENUMREG)+" AND ","",.t.)     
        endif
      endif
      if NOT EMPTY(_Curs_gsaiebde.PECODIVA)
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"il codice I.V.A. "+alltrim(_Curs_gsaiebde.IVDESIVA)
        this.w_qry.mLoadWhere("IVCODIVA="+cp_tostrodbc(_Curs_gsaiebde.PECODIVA)+" AND ","",.t.)     
      endif
      this.w_GENERA = IIF(_Curs_gsaiebde.PECONDIZ="I", "S", "N")
      this.w_DESCREGOLA = this.w_DESCREGOLA+CHR(13)+"allora � stata"+IIF(_Curs_gsaiebde.PECONDIZ="I"," inclusa "," esclusa ")+"la riga"
      this.w_REGOPE = IIF(_Curs_gsaiebde.PECONDIZ="I", "S", " ")
      this.w_qry.mLoadWhere("TMPPNT_IVA.REGAPP<>'A' AND ","",.t.)     
      this.w_cTempTable = i_TableProp[this.TMPPNT_DETT_idx,2]
      this.w_qry.mDoQuery("nocursor", "Exec", .f., .f., this.w_cTempTable, .f., .t.)     
      * --- Creo la query per update
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPPNT_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
                +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
        +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
        +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
        +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
                +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
        +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
        +",TMPPNT_IVA.REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
        +",TMPPNT_IVA.REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
        +",TMPPNT_IVA.DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
                +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
        +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
        +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
        +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
                +" and "+i_cTable+".DETIPCON = "+i_cQueryTable+".DETIPCON";
                +" and "+i_cTable+".DECODCON = "+i_cQueryTable+".DECODCON";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
        +",REGAPP ="+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','REGAPP');
        +",REGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_REGOPE),'TMPPNT_IVA','REGOPE');
        +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Drop temporary table TMPPNT_DETT
      i_nIdx=cp_GetTableDefIdx('TMPPNT_DETT')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPNT_DETT')
      endif
      this.w_qry = .null.
        select _Curs_gsaiebde
        continue
      enddo
      use
    endif
    * --- Stampa di controllo
    *     Elenco fatture dove � stata applicato parametro su dati iva
    this.w_REGAPP = "A"
    VQ_EXEC("..\aift\exe\query\gsaisbde", this, "__TMP1__")
    * --- Parametri causali note di variazione
    *     definisco note di variazione solo se non valorizzato OPERSUPE
    AddMsgNL("Fase 3.2: gestione eccezioni per parametri su note di variazione",this)
    * --- Write into TMPPNT_IVA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DERIFPNT"
      do vq_exec with '..\aift\exe\query\gsai8bde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OSTIPFAT ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','OSTIPFAT');
      +",REGAPP ="+cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP');
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
      +"TMPPNT_IVA.OSTIPFAT ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','OSTIPFAT');
      +",TMPPNT_IVA.REGAPP ="+cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP');
          +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_IVA.DERIFPNT = t2.DERIFPNT";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
          +"OSTIPFAT,";
          +"REGAPP";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','OSTIPFAT')+",";
          +cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
      +"OSTIPFAT ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','OSTIPFAT');
      +",REGAPP ="+cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OSTIPFAT ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','OSTIPFAT');
      +",REGAPP ="+cp_NullLink(cp_ToStrODBC("B"),'TMPPNT_IVA','REGAPP');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino dal cursore temporaneo "TMPPNT_IVA" tutte le registrazioni
    *     di primanota non di tipo Nota Rettifica che hanno data registrazione > della
    *     data di fine estrazione
    * --- Create temporary table TMPRIPA3
    i_nIdx=cp_AddTableDef('TMPRIPA3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_HADYEVLVBR[1]
    indexes_HADYEVLVBR[1]='DECODCON,DETIPCON'
    vq_exec('..\AIFT\EXE\QUERY\GSAIRBDE1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_HADYEVLVBR,.f.)
    this.TMPRIPA3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Delete from TMPPNT_IVA
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ANNORIF <> "+cp_ToStrODBC(this.oParentObject.w_ANNO);
             )
    else
      delete from (i_cTable) where;
            ANNORIF <> this.oParentObject.w_ANNO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from TMPPNT_IVA
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='OSANNRET'
      cp_CreateTempTable(i_nConn,i_cTempTable,"DISTINCT OSANNRET "," from "+i_cQueryTable+" where OSANNRET<>"+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND OSANNRET<>0",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".OSANNRET = "+i_cQueryTable+".OSANNRET";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".OSANNRET = "+i_cQueryTable+".OSANNRET";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    AddMsgNL("Fase 3.3: eliminazione dati per note di variazione",this)
    * --- Controllo note di variazione di competenza del periodo ma con riferimento a fatture del periodo successivo
    VQ_EXEC("..\aift\exe\query\gsaisbde1", this, "__TMP4__")
    * --- Delete from TMPPNT_IVA
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
    
      do vq_exec with 'Gsai7bde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Stampa di controllo
    *     Elenco fatture dove � stata applicato parametro per note di variazione
    this.w_REGAPP = "B"
    VQ_EXEC("..\aift\exe\query\gsaisbde", this, "__TMP2__")
    * --- Metto da parte i riferimenti delle note di variazione colegate a fatture per evidenziarle nel figlio
    * --- Create temporary table TMPRIPA2
    i_nIdx=cp_AddTableDef('TMPRIPA2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\AIFT\EXE\QUERY\GSAI10BDE',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPRIPA2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Sposto le note di credito e le rettifiche con il riferimento alla registrazione di prima nota collegata
    AddMsgNL("Fase 3.4: gestione note di credito rettifiche",this)
    * --- Write into TMPPNT_IVA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DERIFPNT,CPROWNUM"
      do vq_exec with 'gsainbde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DERIFPNT = _t2.OSRIFFAT";
          +",PNNUMDOC = _t2.PNNUMDOC";
          +",PNALFDOC = _t2.PNALFDOC";
          +",IVIMPONI = _t2.SEGNO*IVIMPONI";
          +",IVIMPIVA = _t2.SEGNO*IVIMPIVA";
          +",GENERA = _t2.GENERA";
          +",CCTIPDOC = _t2.CCTIPDOC";
          +",OSTIPFAT = _t2.OSTIPFAT";
          +",OSRIFCON = _t2.OSRIFCON";
          +",OSTIPOPE = _t2.OSTIPOPE";
          +",PNCODCAU = _t2.PNCODCAU";
          +",DECOMSUP = _t2.DECOMSUP";
          +",DEDATOPE = _t2.DEDATOPE";
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
          +"TMPPNT_IVA.DERIFPNT = _t2.OSRIFFAT";
          +",TMPPNT_IVA.PNNUMDOC = _t2.PNNUMDOC";
          +",TMPPNT_IVA.PNALFDOC = _t2.PNALFDOC";
          +",TMPPNT_IVA.IVIMPONI = _t2.SEGNO*IVIMPONI";
          +",TMPPNT_IVA.IVIMPIVA = _t2.SEGNO*IVIMPIVA";
          +",TMPPNT_IVA.GENERA = _t2.GENERA";
          +",TMPPNT_IVA.CCTIPDOC = _t2.CCTIPDOC";
          +",TMPPNT_IVA.OSTIPFAT = _t2.OSTIPFAT";
          +",TMPPNT_IVA.OSRIFCON = _t2.OSRIFCON";
          +",TMPPNT_IVA.OSTIPOPE = _t2.OSTIPOPE";
          +",TMPPNT_IVA.PNCODCAU = _t2.PNCODCAU";
          +",TMPPNT_IVA.DECOMSUP = _t2.DECOMSUP";
          +",TMPPNT_IVA.DEDATOPE = _t2.DEDATOPE";
          +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_IVA.DERIFPNT = t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
          +"DERIFPNT,";
          +"PNNUMDOC,";
          +"PNALFDOC,";
          +"IVIMPONI,";
          +"IVIMPIVA,";
          +"GENERA,";
          +"CCTIPDOC,";
          +"OSTIPFAT,";
          +"OSRIFCON,";
          +"OSTIPOPE,";
          +"PNCODCAU,";
          +"DECOMSUP,";
          +"DEDATOPE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.OSRIFFAT,";
          +"t2.PNNUMDOC,";
          +"t2.PNALFDOC,";
          +"t2.SEGNO*IVIMPONI,";
          +"t2.SEGNO*IVIMPIVA,";
          +"t2.GENERA,";
          +"t2.CCTIPDOC,";
          +"t2.OSTIPFAT,";
          +"t2.OSRIFCON,";
          +"t2.OSTIPOPE,";
          +"t2.PNCODCAU,";
          +"t2.DECOMSUP,";
          +"t2.DEDATOPE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
          +"DERIFPNT = _t2.OSRIFFAT";
          +",PNNUMDOC = _t2.PNNUMDOC";
          +",PNALFDOC = _t2.PNALFDOC";
          +",IVIMPONI = _t2.SEGNO*IVIMPONI";
          +",IVIMPIVA = _t2.SEGNO*IVIMPIVA";
          +",GENERA = _t2.GENERA";
          +",CCTIPDOC = _t2.CCTIPDOC";
          +",OSTIPFAT = _t2.OSTIPFAT";
          +",OSRIFCON = _t2.OSRIFCON";
          +",OSTIPOPE = _t2.OSTIPOPE";
          +",PNCODCAU = _t2.PNCODCAU";
          +",DECOMSUP = _t2.DECOMSUP";
          +",DEDATOPE = _t2.DEDATOPE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DERIFPNT = (select OSRIFFAT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",PNNUMDOC = (select PNNUMDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",PNALFDOC = (select PNALFDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",IVIMPONI = (select SEGNO*IVIMPONI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",IVIMPIVA = (select SEGNO*IVIMPIVA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",GENERA = (select GENERA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CCTIPDOC = (select CCTIPDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",OSTIPFAT = (select OSTIPFAT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",OSRIFCON = (select OSRIFCON from "+i_cQueryTable+" where "+i_cWhere+")";
          +",OSTIPOPE = (select OSTIPOPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",PNCODCAU = (select PNCODCAU from "+i_cQueryTable+" where "+i_cWhere+")";
          +",DECOMSUP = (select DECOMSUP from "+i_cQueryTable+" where "+i_cWhere+")";
          +",DEDATOPE = (select DEDATOPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- costruisco la tabella del fatturato prima di eliminare le registrazioni per importo minimo
    * --- Create temporary table TMPRIPA1
    i_nIdx=cp_AddTableDef('TMPRIPA1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\AIFT\EXE\QUERY\GSAIFBDE',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPRIPA1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Verifica limite importi
    AddMsgNL("Fase 3.5: controlli e verifica limite importi comunicabili",this)
    AddMsgNL("Fase 3.5.1: controlli e verifica corrispettivi periodici e contratti collegati",this)
    this.w_DESCREGOLA = "Contratto collegato con documenti esterni al periodo di estrazione"
    * --- Write into TMPPNT_IVA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DERIFPNT,DETIPCON,DECODCON,GENERA"
      do vq_exec with '..\aift\exe\query\gsairbde4',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"REGAPP ="+cp_NullLink(cp_ToStrODBC("E"),'TMPPNT_IVA','REGAPP');
      +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
      +"TMPPNT_IVA.REGAPP ="+cp_NullLink(cp_ToStrODBC("E"),'TMPPNT_IVA','REGAPP');
      +",TMPPNT_IVA.DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
          +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_IVA.DERIFPNT = t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = t2.GENERA";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
          +"REGAPP,";
          +"DEREGDES";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("E"),'TMPPNT_IVA','REGAPP')+",";
          +cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
      +"REGAPP ="+cp_NullLink(cp_ToStrODBC("E"),'TMPPNT_IVA','REGAPP');
      +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
              +" and "+i_cTable+".DETIPCON = "+i_cQueryTable+".DETIPCON";
              +" and "+i_cTable+".DECODCON = "+i_cQueryTable+".DECODCON";
              +" and "+i_cTable+".GENERA = "+i_cQueryTable+".GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"REGAPP ="+cp_NullLink(cp_ToStrODBC("E"),'TMPPNT_IVA','REGAPP');
      +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into TMPPNT_IVA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DERIFPNT,DETIPCON,DECODCON,GENERA"
      do vq_exec with '..\aift\exe\query\gsairbde2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
      +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
          +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_IVA.DERIFPNT = t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = t2.GENERA";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
          +"GENERA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
      +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
              +" and "+i_cTable+".DETIPCON = "+i_cQueryTable+".DETIPCON";
              +" and "+i_cTable+".DECODCON = "+i_cQueryTable+".DECODCON";
              +" and "+i_cTable+".GENERA = "+i_cQueryTable+".GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_REGAPP = "E"
    VQ_EXEC("..\aift\exe\query\gsaisbde", this, "__TMP5__")
    AddMsgNL("Fase 3.5.2: controlli e verifica importo minimo fatture",this)
    * --- Write into TMPPNT_IVA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DERIFPNT,DETIPCON,DECODCON,GENERA"
      do vq_exec with '..\aift\exe\query\gsairbde3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
      +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
          +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_IVA.DERIFPNT = t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = t2.GENERA";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
          +"GENERA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
              +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
              +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
              +" and "+"TMPPNT_IVA.GENERA = _t2.GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
      +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
              +" and "+i_cTable+".DETIPCON = "+i_cQueryTable+".DETIPCON";
              +" and "+i_cTable+".DECODCON = "+i_cQueryTable+".DECODCON";
              +" and "+i_cTable+".GENERA = "+i_cQueryTable+".GENERA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Parametri controparte
    *     la regola per la controparte va eseguita dopo perch� in base alle esclusioni/inclusioni precedenti valuto soggettivamente i totali
    AddMsgNL("Fase 3.6: gestione eccezioni per parametri su controparte",this)
    * --- Select from gsaigbde
    do vq_exec with 'gsaigbde',this,'_Curs_gsaigbde','',.f.,.t.
    if used('_Curs_gsaigbde')
      select _Curs_gsaigbde
      locate for 1=1
      do while not(eof())
      * --- in base hai filtri impostati devo costruire la where aggiungendo valori
      * --- Create temporary table TMPPNT_DETT
      i_nIdx=cp_AddTableDef('TMPPNT_DETT') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"DETIPCON, DECODCON "," from "+i_cTable;
            +" where 1=0";
            )
      this.TMPPNT_DETT_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_qry = Createobject("cpquery")
      this.w_qry.mLoadFile("TMPRIPA1","TMPRIPA1","TMPRIPA1",i_ServerConn[1,2])     
      this.w_qry.cDistinct = "distinct"
      this.w_qry.mLoadField("DETIPCON", "DETIPCON", "C", 1, 0)     
      this.w_qry.mLoadField("DECODCON", "DECODCON", "C", 15, 0)     
      this.w_qry.mLoadField("'12avxgd'", "cpccchk", "C", 10, 0)     
      this.w_DESCREGOLA = "La regola � valida per i "+IIF(_Curs_gsaigbde.PETIPCON="C","clienti",IIF(_Curs_gsaigbde.PETIPCON="F","fornitori","soggetti"))
      if _Curs_gsaigbde.PETIPCON<>"N"
        this.w_qry.mLoadWhere("DETIPCON="+cp_tostrodbc(_Curs_gsaigbde.PETIPCON)+" AND ","",.t.)     
      endif
      if _Curs_gsaigbde.PEOPETRE<>"F"
        this.w_DESCREGOLA = this.w_DESCREGOLA+CHR(13)+"con operazioni rilevanti uguali a "+IIF(_Curs_gsaigbde.PEOPETRE="E","escludi",IIF(_Curs_gsaigbde.PEOPETRE="P","corrispettivi periodici",IIF(_Curs_gsaigbde.PEOPETRE="C","contratti collegati","non definibile")))
        this.w_qry.mLoadWhere("ANOPETRE="+cp_tostrodbc(_Curs_gsaigbde.PEOPETRE)+" AND ","",.t.)     
      endif
      if _Curs_gsaigbde.PETIPPRE<>"F"
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"tipologia prevalente uguale a "+IIF(_Curs_gsaigbde.PETIPPRE="B","beni",IIF(_Curs_gsaigbde.PETIPPRE="S","servizi","non definibile"))
        this.w_qry.mLoadWhere("ANTIPPRE="+cp_tostrodbc(_Curs_gsaigbde.PETIPPRE)+" AND ","",.t.)     
      endif
      if not empty(NVL( _Curs_gsaigbde.PECATCON, space(5)))
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"categoria contabile uguale a "+alltrim(_Curs_gsaigbde.C2DESCRI)
        this.w_qry.mLoadWhere("ANCATCON="+cp_tostrodbc(_Curs_gsaigbde.PECATCON)+" AND ","",.t.)     
      endif
      if _Curs_gsaigbde.PEFLPRIV="S"
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"di tipo privato"
        this.w_qry.mLoadWhere("ANFLPRIV='S' AND ","",.t.)     
      endif
      if not empty(NVL( _Curs_gsaigbde.PECONSUP, space(5)))
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"con mastro uguale a "+alltrim(_Curs_gsaigbde.MCDESCRI)
        this.w_qry.mLoadWhere("ANCONSUP="+cp_tostrodbc(_Curs_gsaigbde.PECONSUP)+" AND ","",.t.)     
      endif
      if _Curs_gsaigbde.PEOPERAT<>"N"
        this.w_DESCREGOLA = this.w_DESCREGOLA+Chr(13)+"a condizione che il totale importo dovuto sia "+IIF(_Curs_gsaigbde.PEOPERAT="M",">= ",IIF(_Curs_gsaigbde.PEOPERAT="I","< ","= "))+cp_ToStrODBC(_Curs_gsaigbde.PEIMPFAT)
        this.w_qry.mLoadWhere(IIF(_Curs_gsaigbde.PECONDIZ="O" AND _Curs_gsaigbde.PEOPERAR<>"N","(","")+"PEIMPFAT"+IIF(_Curs_gsaigbde.PEOPERAT="M",">=",IIF(_Curs_gsaigbde.PEOPERAT="I","<","="))+cp_ToStrODBC(_Curs_gsaigbde.PEIMPFAT)+IIF(_Curs_gsaigbde.PECONDIZ="O" AND _Curs_gsaigbde.PEOPERAR<>"N"," OR "," AND "),"",.t.)     
      endif
      if _Curs_gsaigbde.PEOPERAR<>"N"
        this.w_DESCREGOLA = this.w_DESCREGOLA+iif(_Curs_gsaigbde.PEOPERAT<>"N"," "+lower(_Curs_gsaigbde.PECONDIZ),"")+Chr(13)+"il numero delle registrazioni sia "+IIF(_Curs_gsaigbde.PEOPERAR="M",">= ",IIF(_Curs_gsaigbde.PEOPERAR="I","< ","= "))+cp_ToStrODBC(_Curs_gsaigbde.PENUMFAT)
        this.w_qry.mLoadWhere("PENUMFAT"+IIF(_Curs_gsaigbde.PEOPERAR="M",">=",IIF(_Curs_gsaigbde.PEOPERAR="I","<","="))+cp_ToStrODBC(_Curs_gsaigbde.PENUMFAT)+IIF(_Curs_gsaigbde.PECONDIZ="O" AND _Curs_gsaigbde.PEOPERAT<>"N",")","")+" AND ","",.t.)     
      endif
      this.w_DESCREGOLA = this.w_DESCREGOLA+CHR(13)+"allora � stata"+IIF(_Curs_gsaigbde.PECONEST="I"," inclusa "," esclusa ")+IIF(_Curs_gsaigbde.PETIPEST="T","la registrazione","la registrazione perch� "+IIF(_Curs_gsaigbde.PETIPEST="M",">= ",IIF(_Curs_gsaigbde.PETIPEST="I","< ","= "))+cp_ToStrODBC(_Curs_gsaigbde.PEIMPEST))
      this.w_cTempTable = i_TableProp[this.TMPPNT_DETT_idx,2]
      this.w_qry.mDoQuery("nocursor", "Exec", .f., .f., this.w_cTempTable, .f., .t.)     
      this.w_qry = .null.
      this.w_qry = Createobject("cpquery")
      * --- da paramtero si pu� solo includere
      this.w_GENERA = IIF(_Curs_gsaigbde.PECONEST="I", "S", "N")
      * --- Creo la query per update
      * --- Create temporary table TMPPNT_MAST
      i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"DETIPCON, DECODCON, DERIFPNT,REGAPP,REGOPE "," from "+i_cTable;
            +" where 1=0";
            )
      this.TMPPNT_MAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_qry.mLoadFile("GSAIIBDE.VQR","GSAIIBDE","GSAIIBDE*",i_ServerConn[1,2])     
      this.w_qry.mLoadField("DETIPCON", "DETIPCON", "C", 1, 0)     
      this.w_qry.mLoadField("DECODCON", "DECODCON", "C", 15, 0)     
      this.w_qry.mLoadField("DERIFPNT", "DERIFPNT", "C", 10, 0)     
      this.w_qry.mLoadField("REGAPP", "REGAPP", "C", 10, 0)     
      this.w_qry.mLoadField("REGOPE", "REGOPE", "C", 10, 0)     
      this.w_qry.mLoadField("'12avxgd'", "cpccchk", "C", 10, 0)     
      this.w_qry.mLoadWhere("REGAPP<>'C' AND ","",.t.)     
      this.w_qry.mLoadWhere("(REGAPP NOT IN ('A','X') OR ","",.t.)     
      this.w_qry.mLoadWhere("REGOPE = 'S') AND ","",.t.)     
      if _Curs_gsaigbde.PETIPEST<>"T"
        this.w_qry.mLoadWhere("PEIMPEST"+IIF(_Curs_gsaigbde.PETIPEST="M",">=",IIF(_Curs_gsaigbde.PETIPEST="I","<","="))+cp_ToStrODBC(_Curs_gsaigbde.PEIMPEST)+" AND ","",.t.)     
      endif
      this.w_cTempTable = i_TableProp[this.TMPPNT_MAST_idx,2]
      this.w_qry.mDoQuery("nocursor", "Exec", .f., .f., this.w_cTempTable, .f., .t.)     
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DERIFPNT,DETIPCON,DECODCON,REGAPP,REGOPE"
        do vq_exec with '..\aift\exe\query\gsaihbde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
                +" and "+"TMPPNT_IVA.REGAPP = _t2.REGAPP";
                +" and "+"TMPPNT_IVA.REGOPE = _t2.REGOPE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
        +",REGAPP ="+cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_IVA','REGAPP');
        +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
                +" and "+"TMPPNT_IVA.REGAPP = _t2.REGAPP";
                +" and "+"TMPPNT_IVA.REGOPE = _t2.REGOPE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
        +"TMPPNT_IVA.GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
        +",TMPPNT_IVA.REGAPP ="+cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_IVA','REGAPP');
        +",TMPPNT_IVA.DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DERIFPNT = t2.DERIFPNT";
                +" and "+"TMPPNT_IVA.DETIPCON = t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = t2.DECODCON";
                +" and "+"TMPPNT_IVA.REGAPP = t2.REGAPP";
                +" and "+"TMPPNT_IVA.REGOPE = t2.REGOPE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"GENERA,";
            +"REGAPP,";
            +"DEREGDES";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA')+",";
            +cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_IVA','REGAPP')+",";
            +cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
                +" and "+"TMPPNT_IVA.REGAPP = _t2.REGAPP";
                +" and "+"TMPPNT_IVA.REGOPE = _t2.REGOPE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
        +",REGAPP ="+cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_IVA','REGAPP');
        +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
                +" and "+i_cTable+".DETIPCON = "+i_cQueryTable+".DETIPCON";
                +" and "+i_cTable+".DECODCON = "+i_cQueryTable+".DECODCON";
                +" and "+i_cTable+".REGAPP = "+i_cQueryTable+".REGAPP";
                +" and "+i_cTable+".REGOPE = "+i_cQueryTable+".REGOPE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC(this.w_GENERA),'TMPPNT_IVA','GENERA');
        +",REGAPP ="+cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_IVA','REGAPP');
        +",DEREGDES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCREGOLA),'TMPPNT_IVA','DEREGDES');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Drop temporary table TMPPNT_MAST
      i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPNT_MAST')
      endif
      * --- Drop temporary table TMPPNT_DETT
      i_nIdx=cp_GetTableDefIdx('TMPPNT_DETT')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPNT_DETT')
      endif
      this.w_qry = .null.
        select _Curs_gsaigbde
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMPRIPA1
    i_nIdx=cp_GetTableDefIdx('TMPRIPA1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPRIPA1')
    endif
    * --- Stampa di controllo
    *     Elenco fatture dove � stata applicato parametro per controparte
    this.w_REGAPP = "C"
    VQ_EXEC("..\aift\exe\query\gsaisbde", this, "__TMP3__")
    * --- Elimino dal cursore temporaneo TMPPNT_IVA tutti i record che ha il flag
    *     "Genera" uguale ad 'N'
    AddMsgNL("Fase 4: eliminazione dati non generabili",this)
    if this.oParentObject.w_PEESTREC="S"
      * --- Se non devo estrarre i record 4 li elimino
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','GENERA');
            +i_ccchkf ;
        +" where ";
            +"OSTIPFAT = "+cp_ToStrODBC("N");
            +" and PN__ANNO < "+cp_ToStrODBC(this.oParentObject.w_ANNO);
               )
      else
        update (i_cTable) set;
            GENERA = "N";
            &i_ccchkf. ;
         where;
            OSTIPFAT = "N";
            and PN__ANNO < this.oParentObject.w_ANNO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Delete from TMPPNT_IVA
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GENERA = "+cp_ToStrODBC("N");
             )
    else
      delete from (i_cTable) where;
            GENERA = "N";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimino soggetti dove ho solo note di variazione nel dettaglio record 2 o 3
    * --- Delete from TMPPNT_IVA
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DETIPCON = "+i_cQueryTable+".DETIPCON";
            +" and "+i_cTable+".DECODCON = "+i_cQueryTable+".DECODCON";
    
      do vq_exec with '..\aift\exe\query\gsaiobde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- "Per ora elimino dal cursore temporaneo TMPPNT_IVA" tutte le registrazioni
    *     che hanno valorizzato a 'N' il campo Ostipfat - Note di variazione" 
    *     raggruppamenti per seriali con controllo importi
    *     controlli su raggrruppamenticontratti e note di variazione
    *     filtro sulle date
    * --- Select from GSAI4BDE
    do vq_exec with 'GSAI4BDE',this,'_Curs_GSAI4BDE','',.f.,.t.
    if used('_Curs_GSAI4BDE')
      select _Curs_GSAI4BDE
      locate for 1=1
      do while not(eof())
      this.w_NUMMOV = NUMERO
        select _Curs_GSAI4BDE
        continue
      enddo
      use
    endif
    * --- Select from TMPPNT_IVA
    i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2],.t.,this.TMPPNT_IVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count(distinct DERIFPNT) as NUMERO  from "+i_cTable+" TMPPNT_IVA ";
           ,"_Curs_TMPPNT_IVA")
    else
      select count(distinct DERIFPNT) as NUMERO from (i_cTable);
        into cursor _Curs_TMPPNT_IVA
    endif
    if used('_Curs_TMPPNT_IVA')
      select _Curs_TMPPNT_IVA
      locate for 1=1
      do while not(eof())
      this.w_NUMMOVD = NUMERO
        select _Curs_TMPPNT_IVA
        continue
      enddo
      use
    endif
    if this.w_NUMMOV=0
      ah_ErrorMsg("Nessun movimento da generare")
    else
      * --- Try
      local bErr_037DC998
      bErr_037DC998=bTrsErr
      this.Try_037DC998()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Errore nella crezione dei dati estratti (%1)", "!", "", i_trsmsg)
      endif
      bTrsErr=bTrsErr or bErr_037DC998
      * --- End
    endif
    if USED("__TMP1__") OR USED("__TMP2__") OR USED("__TMP3__") OR USED("__TMP4__") OR USED("__TMP5__") OR USED("__TMP6__")
      FOR this.w_I=1 to 6
      this.w_CURSOR = "__TMP"+STR(this.w_I,1)+"__"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ENDFOR
      if USED("__TMP__") AND RECCOUNT("__TMP__")>0 AND ah_YesNo("Si vuole stampare il dettaglio dei parametri applicati e gli eventuali messaggi di avvertimento?")
        CP_CHPRN("..\aift\exe\query\gsaisbde", , this.oParentObject)
      endif
      if USED("__TMP__")
        USE IN __TMP__
      endif
    endif
    if USED("ChkVersion")
      USE IN ChkVersion
    endif
    * --- Drop temporary table TMPPNT_IVA
    i_nIdx=cp_GetTableDefIdx('TMPPNT_IVA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPPNT_IVA')
    endif
    * --- Drop temporary table TMPRIPA2
    i_nIdx=cp_GetTableDefIdx('TMPRIPA2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPRIPA2')
    endif
    * --- Drop temporary table TMPRIPA3
    i_nIdx=cp_GetTableDefIdx('TMPRIPA3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPRIPA3')
    endif
  endproc
  proc Try_037DC998()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_DESERIAL = space(10)
    w_Conn=i_TableProp[this.ANTIVADE_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "ANTIE", "i_codazi,w_DESERIAL")
    this.w_SERIAL = VAL(this.w_DESERIAL)-1
    this.w_DESERIAL = cp_StrZeroPad( this.w_SERIAL+this.w_NUMMOV ,10)
    cp_NextTableProg(this, w_Conn, "ANTIE", "i_codazi,w_DESERIAL")
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_OLDVERSIONE = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_OLDVERSIONE = .t.
      endif
    endif
    AddMsgNL("Fase 5: generazione progressivi",this)
    * --- genero progressivo seriale
    if this.w_OLDVERSIONE
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2],.t.,this.TMPPNT_IVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select distinct DETIPCON,DECODCON  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select distinct DETIPCON,DECODCON from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NUMSER = this.w_NUMSER + 1
        this.w_DETIPCON = _Curs_TMPPNT_IVA.DETIPCON
        this.w_DECODCON = _Curs_TMPPNT_IVA.DECODCON
        this.w_AGGSER = cp_StrZeroPad(this.w_NUMSER + this.w_SERIAL, 10)
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DESERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPPNT_IVA','DESERIAL');
              +i_ccchkf ;
          +" where ";
              +"DETIPCON = "+cp_ToStrODBC(this.w_DETIPCON);
              +" and DECODCON = "+cp_ToStrODBC(this.w_DECODCON);
                 )
        else
          update (i_cTable) set;
              DESERIAL = this.w_AGGSER;
              &i_ccchkf. ;
           where;
              DETIPCON = this.w_DETIPCON;
              and DECODCON = this.w_DECODCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DETIPCON,DECODCON"
        do vq_exec with 'GSAI5BDE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DESERIAL = _t2.DESERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.DESERIAL = _t2.DESERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DETIPCON = t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = t2.DECODCON";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"DESERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DESERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DETIPCON = _t2.DETIPCON";
                +" and "+"TMPPNT_IVA.DECODCON = _t2.DECODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"DESERIAL = _t2.DESERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DETIPCON = "+i_cQueryTable+".DETIPCON";
                +" and "+i_cTable+".DECODCON = "+i_cQueryTable+".DECODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DESERIAL = (select DESERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    AddMsgNL("Fase 6: inserimento dati",this)
    * --- Insert into ANTIVADE
    i_nConn=i_TableProp[this.ANTIVADE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANTIVADE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai2bde",this.ANTIVADE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_OLDVERSIONE
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DESERIAL,DERIFPNT"
        do vq_exec with 'GSAI6BDE1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DESERIAL = _t2.DESERIAL";
                +" and "+"TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DESERIAL = _t2.DESERIAL";
                +" and "+"TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DESERIAL = t2.DESERIAL";
                +" and "+"TMPPNT_IVA.DERIFPNT = t2.DERIFPNT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DESERIAL = _t2.DESERIAL";
                +" and "+"TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
                +" and "+i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DESERIAL,DERIFPNT"
        do vq_exec with 'GSAI6BDE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DESERIAL = _t2.DESERIAL";
                +" and "+"TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DESERIAL = _t2.DESERIAL";
                +" and "+"TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DESERIAL = t2.DESERIAL";
                +" and "+"TMPPNT_IVA.DERIFPNT = t2.DERIFPNT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DESERIAL = _t2.DESERIAL";
                +" and "+"TMPPNT_IVA.DERIFPNT = _t2.DERIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
                +" and "+i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into ANTIVDDE
    i_nConn=i_TableProp[this.ANTIVDDE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai3bde",this.ANTIVDDE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Calcolo progressivo tabella Antivddr
    if this.w_OLDVERSIONE
      * --- Write into TMPRIPA2
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPRIPA2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPRIPA2_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DERIFPNT,RERIFFAT"
        do vq_exec with 'GSAI12BDE1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPRIPA2_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPRIPA2.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPRIPA2.RERIFFAT = _t2.RERIFFAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPRIPA2, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPRIPA2.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPRIPA2.RERIFFAT = _t2.RERIFFAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPRIPA2, "+i_cQueryTable+" _t2 set ";
            +"TMPRIPA2.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPRIPA2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPRIPA2.DERIFPNT = t2.DERIFPNT";
                +" and "+"TMPRIPA2.RERIFFAT = t2.RERIFFAT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPRIPA2 set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPRIPA2.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPRIPA2.RERIFFAT = _t2.RERIFFAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPRIPA2 set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
                +" and "+i_cTable+".RERIFFAT = "+i_cQueryTable+".RERIFFAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPRIPA2
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPRIPA2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPRIPA2_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DERIFPNT,RERIFFAT"
        do vq_exec with 'GSAI12BDE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPRIPA2_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPRIPA2.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPRIPA2.RERIFFAT = _t2.RERIFFAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPRIPA2, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPRIPA2.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPRIPA2.RERIFFAT = _t2.RERIFFAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPRIPA2, "+i_cQueryTable+" _t2 set ";
            +"TMPRIPA2.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPRIPA2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPRIPA2.DERIFPNT = t2.DERIFPNT";
                +" and "+"TMPRIPA2.RERIFFAT = t2.RERIFFAT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPRIPA2 set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPRIPA2.DERIFPNT = _t2.DERIFPNT";
                +" and "+"TMPRIPA2.RERIFFAT = _t2.RERIFFAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPRIPA2 set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DERIFPNT = "+i_cQueryTable+".DERIFPNT";
                +" and "+i_cTable+".RERIFFAT = "+i_cQueryTable+".RERIFFAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into ANTIVDDR
    i_nConn=i_TableProp[this.ANTIVDDR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai11bde",this.ANTIVDDR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorno il flag "Da verificare" nella tabella "Anagrafica dati estratti"
    * --- Write into ANTIVADE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ANTIVADE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANTIVADE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DESERIAL"
      do vq_exec with 'GSAI13BDE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ANTIVADE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ANTIVADE.DESERIAL = _t2.DESERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DEDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'ANTIVADE','DEDACONT');
          +i_ccchkf;
          +" from "+i_cTable+" ANTIVADE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ANTIVADE.DESERIAL = _t2.DESERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANTIVADE, "+i_cQueryTable+" _t2 set ";
      +"ANTIVADE.DEDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'ANTIVADE','DEDACONT');
          +Iif(Empty(i_ccchkf),"",",ANTIVADE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ANTIVADE.DESERIAL = t2.DESERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANTIVADE set (";
          +"DEDACONT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'ANTIVADE','DEDACONT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ANTIVADE.DESERIAL = _t2.DESERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANTIVADE set ";
      +"DEDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'ANTIVADE','DEDACONT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DEDACONT ="+cp_NullLink(cp_ToStrODBC("S"),'ANTIVADE','DEDACONT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    VQ_EXEC("..\aift\exe\query\gsaisbde2", this, "__TMP6__")
    if USED("__TMP6__")
      Select __TMP6__
      COUNT FOR REGAPP="F" TO this.w_NUMDAVER
      COUNT FOR REGAPP="G" TO this.w_NUMESCL
    endif
    ah_ErrorMsg("Generazione completata.%0Creati n. %1 dati estratti (di cui n. %3 da verificare e %4 esclusi) da n. %2 documenti", "!", "", ALLTRIM(STR(this.w_NUMMOV)), ALLTRIM(STR(this.w_NUMMOVD)), ALLTRIM(STR(this.w_NUMDAVER)), ALLTRIM(STR(this.w_NUMESCL)))
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED(this.w_CURSOR)
      if NOT USED("__TMP__")
        Select * from (this.w_CURSOR) into cursor __TMP__
      else
        Select * from __TMP__ into cursor __TMP__ union all ; 
 Select * from (this.w_CURSOR) order by regapp, orregdes, pndatdoc, detipcon, decodcon, derifpnt, ivcodiva
      endif
      USE IN (this.w_CURSOR)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,15)]
    this.cWorkTables[1]='*TMPPNT_IVA'
    this.cWorkTables[2]='ANTIVAAN'
    this.cWorkTables[3]='ANTIVADT'
    this.cWorkTables[4]='ANTIVADE'
    this.cWorkTables[5]='ANTIVDDE'
    this.cWorkTables[6]='ANTIVDPN'
    this.cWorkTables[7]='ANTIVDPI'
    this.cWorkTables[8]='ANTIVDPC'
    this.cWorkTables[9]='*TMPPNT_DETT'
    this.cWorkTables[10]='*TMPPNT_MAST'
    this.cWorkTables[11]='*TMPRIPA1'
    this.cWorkTables[12]='OPERSUPE'
    this.cWorkTables[13]='*TMPRIPA2'
    this.cWorkTables[14]='ANTIVDDR'
    this.cWorkTables[15]='*TMPRIPA3'
    return(this.OpenAllTables(15))

  proc CloseCursors()
    if used('_Curs_Gsai14bde')
      use in _Curs_Gsai14bde
    endif
    if used('_Curs_Gsai14bder')
      use in _Curs_Gsai14bder
    endif
    if used('_Curs_gsaiebde')
      use in _Curs_gsaiebde
    endif
    if used('_Curs_gsaigbde')
      use in _Curs_gsaigbde
    endif
    if used('_Curs_GSAI4BDE')
      use in _Curs_GSAI4BDE
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
