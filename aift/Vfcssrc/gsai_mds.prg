* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_mds                                                        *
*              Dettaglio file generato                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-10-31                                                      *
* Last revis.: 2013-11-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsai_mds")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsai_mds")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsai_mds")
  return

* --- Class definition
define class tgsai_mds as StdPCForm
  Width  = 1847
  Height = 277
  Top    = 10
  Left   = 10
  cComment = "Dettaglio file generato"
  cPrg = "gsai_mds"
  HelpContextID=80349033
  add object cnt as tcgsai_mds
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsai_mds as PCContext
  w_DGSERIAL = space(10)
  w_DGNOMFIL = space(254)
  w_DGPROINV = 0
  w_DGQUADFE = space(1)
  w_DGQUADFR = space(1)
  w_DGQUADNE = space(1)
  w_DGQUADNR = space(1)
  w_DGQUADDF = space(1)
  w_DGQUADFN = space(1)
  w_DGQUADSE = space(1)
  w_DGQUADTU = space(1)
  w_DGQUADFA = space(1)
  w_DGQUADSA = space(1)
  w_DGQUADBL = space(1)
  w_DGFA1001 = 0
  w_DGSA2001 = 0
  w_DGBL3001 = 0
  w_DGBL3002 = 0
  w_DGBL3003 = 0
  w_DGFE4001 = 0
  w_DGFE4002 = 0
  w_DGFR5001 = 0
  w_DGFR5002 = 0
  w_DGNE6001 = 0
  w_DGNR7001 = 0
  w_DGDF8001 = 0
  w_DGFN9001 = 0
  w_DGSE1001 = 0
  w_DGTU1101 = 0
  proc Save(i_oFrom)
    this.w_DGSERIAL = i_oFrom.w_DGSERIAL
    this.w_DGNOMFIL = i_oFrom.w_DGNOMFIL
    this.w_DGPROINV = i_oFrom.w_DGPROINV
    this.w_DGQUADFE = i_oFrom.w_DGQUADFE
    this.w_DGQUADFR = i_oFrom.w_DGQUADFR
    this.w_DGQUADNE = i_oFrom.w_DGQUADNE
    this.w_DGQUADNR = i_oFrom.w_DGQUADNR
    this.w_DGQUADDF = i_oFrom.w_DGQUADDF
    this.w_DGQUADFN = i_oFrom.w_DGQUADFN
    this.w_DGQUADSE = i_oFrom.w_DGQUADSE
    this.w_DGQUADTU = i_oFrom.w_DGQUADTU
    this.w_DGQUADFA = i_oFrom.w_DGQUADFA
    this.w_DGQUADSA = i_oFrom.w_DGQUADSA
    this.w_DGQUADBL = i_oFrom.w_DGQUADBL
    this.w_DGFA1001 = i_oFrom.w_DGFA1001
    this.w_DGSA2001 = i_oFrom.w_DGSA2001
    this.w_DGBL3001 = i_oFrom.w_DGBL3001
    this.w_DGBL3002 = i_oFrom.w_DGBL3002
    this.w_DGBL3003 = i_oFrom.w_DGBL3003
    this.w_DGFE4001 = i_oFrom.w_DGFE4001
    this.w_DGFE4002 = i_oFrom.w_DGFE4002
    this.w_DGFR5001 = i_oFrom.w_DGFR5001
    this.w_DGFR5002 = i_oFrom.w_DGFR5002
    this.w_DGNE6001 = i_oFrom.w_DGNE6001
    this.w_DGNR7001 = i_oFrom.w_DGNR7001
    this.w_DGDF8001 = i_oFrom.w_DGDF8001
    this.w_DGFN9001 = i_oFrom.w_DGFN9001
    this.w_DGSE1001 = i_oFrom.w_DGSE1001
    this.w_DGTU1101 = i_oFrom.w_DGTU1101
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DGSERIAL = this.w_DGSERIAL
    i_oTo.w_DGNOMFIL = this.w_DGNOMFIL
    i_oTo.w_DGPROINV = this.w_DGPROINV
    i_oTo.w_DGQUADFE = this.w_DGQUADFE
    i_oTo.w_DGQUADFR = this.w_DGQUADFR
    i_oTo.w_DGQUADNE = this.w_DGQUADNE
    i_oTo.w_DGQUADNR = this.w_DGQUADNR
    i_oTo.w_DGQUADDF = this.w_DGQUADDF
    i_oTo.w_DGQUADFN = this.w_DGQUADFN
    i_oTo.w_DGQUADSE = this.w_DGQUADSE
    i_oTo.w_DGQUADTU = this.w_DGQUADTU
    i_oTo.w_DGQUADFA = this.w_DGQUADFA
    i_oTo.w_DGQUADSA = this.w_DGQUADSA
    i_oTo.w_DGQUADBL = this.w_DGQUADBL
    i_oTo.w_DGFA1001 = this.w_DGFA1001
    i_oTo.w_DGSA2001 = this.w_DGSA2001
    i_oTo.w_DGBL3001 = this.w_DGBL3001
    i_oTo.w_DGBL3002 = this.w_DGBL3002
    i_oTo.w_DGBL3003 = this.w_DGBL3003
    i_oTo.w_DGFE4001 = this.w_DGFE4001
    i_oTo.w_DGFE4002 = this.w_DGFE4002
    i_oTo.w_DGFR5001 = this.w_DGFR5001
    i_oTo.w_DGFR5002 = this.w_DGFR5002
    i_oTo.w_DGNE6001 = this.w_DGNE6001
    i_oTo.w_DGNR7001 = this.w_DGNR7001
    i_oTo.w_DGDF8001 = this.w_DGDF8001
    i_oTo.w_DGFN9001 = this.w_DGFN9001
    i_oTo.w_DGSE1001 = this.w_DGSE1001
    i_oTo.w_DGTU1101 = this.w_DGTU1101
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsai_mds as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 1847
  Height = 277
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-04"
  HelpContextID=80349033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  GENDFILT_IDX = 0
  cFile = "GENDFILT"
  cKeySelect = "DGSERIAL"
  cKeyWhere  = "DGSERIAL=this.w_DGSERIAL"
  cKeyDetail  = "DGSERIAL=this.w_DGSERIAL"
  cKeyWhereODBC = '"DGSERIAL="+cp_ToStrODBC(this.w_DGSERIAL)';

  cKeyDetailWhereODBC = '"DGSERIAL="+cp_ToStrODBC(this.w_DGSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"GENDFILT.DGSERIAL="+cp_ToStrODBC(this.w_DGSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'GENDFILT.CPROWNUM '
  cPrg = "gsai_mds"
  cComment = "Dettaglio file generato"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 2
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DGSERIAL = space(10)
  w_DGNOMFIL = space(254)
  w_DGPROINV = 0
  w_DGQUADFE = space(1)
  w_DGQUADFR = space(1)
  w_DGQUADNE = space(1)
  w_DGQUADNR = space(1)
  w_DGQUADDF = space(1)
  w_DGQUADFN = space(1)
  w_DGQUADSE = space(1)
  w_DGQUADTU = space(1)
  w_DGQUADFA = space(1)
  w_DGQUADSA = space(1)
  w_DGQUADBL = space(1)
  w_DGFA1001 = 0
  w_DGSA2001 = 0
  w_DGBL3001 = 0
  w_DGBL3002 = 0
  w_DGBL3003 = 0
  w_DGFE4001 = 0
  w_DGFE4002 = 0
  w_DGFR5001 = 0
  w_DGFR5002 = 0
  w_DGNE6001 = 0
  w_DGNR7001 = 0
  w_DGDF8001 = 0
  w_DGFN9001 = 0
  w_DGSE1001 = 0
  w_DGTU1101 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_mdsPag1","gsai_mds",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDGSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='GENDFILT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GENDFILT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GENDFILT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsai_mds'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from GENDFILT where DGSERIAL=KeySet.DGSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.GENDFILT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENDFILT_IDX,2],this.bLoadRecFilter,this.GENDFILT_IDX,"gsai_mds")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GENDFILT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GENDFILT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GENDFILT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DGSERIAL',this.w_DGSERIAL  )
      select * from (i_cTable) GENDFILT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DGSERIAL = NVL(DGSERIAL,space(10))
        cp_LoadRecExtFlds(this,'GENDFILT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DGNOMFIL = NVL(DGNOMFIL,space(254))
          .w_DGPROINV = NVL(DGPROINV,0)
          .w_DGQUADFE = NVL(DGQUADFE,space(1))
          .w_DGQUADFR = NVL(DGQUADFR,space(1))
          .w_DGQUADNE = NVL(DGQUADNE,space(1))
          .w_DGQUADNR = NVL(DGQUADNR,space(1))
          .w_DGQUADDF = NVL(DGQUADDF,space(1))
          .w_DGQUADFN = NVL(DGQUADFN,space(1))
          .w_DGQUADSE = NVL(DGQUADSE,space(1))
          .w_DGQUADTU = NVL(DGQUADTU,space(1))
          .w_DGQUADFA = NVL(DGQUADFA,space(1))
          .w_DGQUADSA = NVL(DGQUADSA,space(1))
          .w_DGQUADBL = NVL(DGQUADBL,space(1))
          .w_DGFA1001 = NVL(DGFA1001,0)
          .w_DGSA2001 = NVL(DGSA2001,0)
          .w_DGBL3001 = NVL(DGBL3001,0)
          .w_DGBL3002 = NVL(DGBL3002,0)
          .w_DGBL3003 = NVL(DGBL3003,0)
          .w_DGFE4001 = NVL(DGFE4001,0)
          .w_DGFE4002 = NVL(DGFE4002,0)
          .w_DGFR5001 = NVL(DGFR5001,0)
          .w_DGFR5002 = NVL(DGFR5002,0)
          .w_DGNE6001 = NVL(DGNE6001,0)
          .w_DGNR7001 = NVL(DGNR7001,0)
          .w_DGDF8001 = NVL(DGDF8001,0)
          .w_DGFN9001 = NVL(DGFN9001,0)
          .w_DGSE1001 = NVL(DGSE1001,0)
          .w_DGTU1101 = NVL(DGTU1101,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DGSERIAL=space(10)
      .w_DGNOMFIL=space(254)
      .w_DGPROINV=0
      .w_DGQUADFE=space(1)
      .w_DGQUADFR=space(1)
      .w_DGQUADNE=space(1)
      .w_DGQUADNR=space(1)
      .w_DGQUADDF=space(1)
      .w_DGQUADFN=space(1)
      .w_DGQUADSE=space(1)
      .w_DGQUADTU=space(1)
      .w_DGQUADFA=space(1)
      .w_DGQUADSA=space(1)
      .w_DGQUADBL=space(1)
      .w_DGFA1001=0
      .w_DGSA2001=0
      .w_DGBL3001=0
      .w_DGBL3002=0
      .w_DGBL3003=0
      .w_DGFE4001=0
      .w_DGFE4002=0
      .w_DGFR5001=0
      .w_DGFR5002=0
      .w_DGNE6001=0
      .w_DGNR7001=0
      .w_DGDF8001=0
      .w_DGFN9001=0
      .w_DGSE1001=0
      .w_DGTU1101=0
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'GENDFILT')
    this.DoRTCalc(1,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDGSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDGSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDGSERIAL_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'GENDFILT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GENDFILT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGSERIAL,"DGSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DGNOMFIL C(254);
      ,t_DGPROINV N(4);
      ,t_DGQUADFE C(1);
      ,t_DGQUADFR C(1);
      ,t_DGQUADNE C(1);
      ,t_DGQUADNR C(1);
      ,t_DGQUADDF C(1);
      ,t_DGQUADFN C(1);
      ,t_DGQUADSE C(1);
      ,t_DGQUADTU C(1);
      ,t_DGQUADFA C(1);
      ,t_DGQUADSA C(1);
      ,t_DGQUADBL C(1);
      ,t_DGFA1001 N(6);
      ,t_DGSA2001 N(6);
      ,t_DGBL3001 N(6);
      ,t_DGBL3002 N(6);
      ,t_DGBL3003 N(6);
      ,t_DGFE4001 N(6);
      ,t_DGFE4002 N(6);
      ,t_DGFR5001 N(6);
      ,t_DGFR5002 N(6);
      ,t_DGNE6001 N(6);
      ,t_DGNR7001 N(6);
      ,t_DGDF8001 N(6);
      ,t_DGFN9001 N(6);
      ,t_DGSE1001 N(6);
      ,t_DGTU1101 N(6);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsai_mdsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGNOMFIL_2_1.controlsource=this.cTrsName+'.t_DGNOMFIL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGPROINV_2_2.controlsource=this.cTrsName+'.t_DGPROINV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFE_2_3.controlsource=this.cTrsName+'.t_DGQUADFE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFR_2_4.controlsource=this.cTrsName+'.t_DGQUADFR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADNE_2_5.controlsource=this.cTrsName+'.t_DGQUADNE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADNR_2_6.controlsource=this.cTrsName+'.t_DGQUADNR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADDF_2_7.controlsource=this.cTrsName+'.t_DGQUADDF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFN_2_8.controlsource=this.cTrsName+'.t_DGQUADFN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADSE_2_9.controlsource=this.cTrsName+'.t_DGQUADSE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADTU_2_10.controlsource=this.cTrsName+'.t_DGQUADTU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFA_2_11.controlsource=this.cTrsName+'.t_DGQUADFA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADSA_2_12.controlsource=this.cTrsName+'.t_DGQUADSA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADBL_2_13.controlsource=this.cTrsName+'.t_DGQUADBL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGFA1001_2_14.controlsource=this.cTrsName+'.t_DGFA1001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGSA2001_2_15.controlsource=this.cTrsName+'.t_DGSA2001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3001_2_16.controlsource=this.cTrsName+'.t_DGBL3001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3002_2_17.controlsource=this.cTrsName+'.t_DGBL3002'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3003_2_18.controlsource=this.cTrsName+'.t_DGBL3003'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGFE4001_2_19.controlsource=this.cTrsName+'.t_DGFE4001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGFE4002_2_20.controlsource=this.cTrsName+'.t_DGFE4002'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGFR5001_2_21.controlsource=this.cTrsName+'.t_DGFR5001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGFR5002_2_22.controlsource=this.cTrsName+'.t_DGFR5002'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGNE6001_2_23.controlsource=this.cTrsName+'.t_DGNE6001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGNR7001_2_24.controlsource=this.cTrsName+'.t_DGNR7001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGDF8001_2_25.controlsource=this.cTrsName+'.t_DGDF8001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGFN9001_2_26.controlsource=this.cTrsName+'.t_DGFN9001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGSE1001_2_27.controlsource=this.cTrsName+'.t_DGSE1001'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDGTU1101_2_28.controlsource=this.cTrsName+'.t_DGTU1101'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNOMFIL_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GENDFILT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENDFILT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GENDFILT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENDFILT_IDX,2])
      *
      * insert into GENDFILT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GENDFILT')
        i_extval=cp_InsertValODBCExtFlds(this,'GENDFILT')
        i_cFldBody=" "+;
                  "(DGSERIAL,DGNOMFIL,DGPROINV,DGQUADFE,DGQUADFR"+;
                  ",DGQUADNE,DGQUADNR,DGQUADDF,DGQUADFN,DGQUADSE"+;
                  ",DGQUADTU,DGQUADFA,DGQUADSA,DGQUADBL,DGFA1001"+;
                  ",DGSA2001,DGBL3001,DGBL3002,DGBL3003,DGFE4001"+;
                  ",DGFE4002,DGFR5001,DGFR5002,DGNE6001,DGNR7001"+;
                  ",DGDF8001,DGFN9001,DGSE1001,DGTU1101,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DGSERIAL)+","+cp_ToStrODBC(this.w_DGNOMFIL)+","+cp_ToStrODBC(this.w_DGPROINV)+","+cp_ToStrODBC(this.w_DGQUADFE)+","+cp_ToStrODBC(this.w_DGQUADFR)+;
             ","+cp_ToStrODBC(this.w_DGQUADNE)+","+cp_ToStrODBC(this.w_DGQUADNR)+","+cp_ToStrODBC(this.w_DGQUADDF)+","+cp_ToStrODBC(this.w_DGQUADFN)+","+cp_ToStrODBC(this.w_DGQUADSE)+;
             ","+cp_ToStrODBC(this.w_DGQUADTU)+","+cp_ToStrODBC(this.w_DGQUADFA)+","+cp_ToStrODBC(this.w_DGQUADSA)+","+cp_ToStrODBC(this.w_DGQUADBL)+","+cp_ToStrODBC(this.w_DGFA1001)+;
             ","+cp_ToStrODBC(this.w_DGSA2001)+","+cp_ToStrODBC(this.w_DGBL3001)+","+cp_ToStrODBC(this.w_DGBL3002)+","+cp_ToStrODBC(this.w_DGBL3003)+","+cp_ToStrODBC(this.w_DGFE4001)+;
             ","+cp_ToStrODBC(this.w_DGFE4002)+","+cp_ToStrODBC(this.w_DGFR5001)+","+cp_ToStrODBC(this.w_DGFR5002)+","+cp_ToStrODBC(this.w_DGNE6001)+","+cp_ToStrODBC(this.w_DGNR7001)+;
             ","+cp_ToStrODBC(this.w_DGDF8001)+","+cp_ToStrODBC(this.w_DGFN9001)+","+cp_ToStrODBC(this.w_DGSE1001)+","+cp_ToStrODBC(this.w_DGTU1101)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GENDFILT')
        i_extval=cp_InsertValVFPExtFlds(this,'GENDFILT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DGSERIAL',this.w_DGSERIAL)
        INSERT INTO (i_cTable) (;
                   DGSERIAL;
                  ,DGNOMFIL;
                  ,DGPROINV;
                  ,DGQUADFE;
                  ,DGQUADFR;
                  ,DGQUADNE;
                  ,DGQUADNR;
                  ,DGQUADDF;
                  ,DGQUADFN;
                  ,DGQUADSE;
                  ,DGQUADTU;
                  ,DGQUADFA;
                  ,DGQUADSA;
                  ,DGQUADBL;
                  ,DGFA1001;
                  ,DGSA2001;
                  ,DGBL3001;
                  ,DGBL3002;
                  ,DGBL3003;
                  ,DGFE4001;
                  ,DGFE4002;
                  ,DGFR5001;
                  ,DGFR5002;
                  ,DGNE6001;
                  ,DGNR7001;
                  ,DGDF8001;
                  ,DGFN9001;
                  ,DGSE1001;
                  ,DGTU1101;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DGSERIAL;
                  ,this.w_DGNOMFIL;
                  ,this.w_DGPROINV;
                  ,this.w_DGQUADFE;
                  ,this.w_DGQUADFR;
                  ,this.w_DGQUADNE;
                  ,this.w_DGQUADNR;
                  ,this.w_DGQUADDF;
                  ,this.w_DGQUADFN;
                  ,this.w_DGQUADSE;
                  ,this.w_DGQUADTU;
                  ,this.w_DGQUADFA;
                  ,this.w_DGQUADSA;
                  ,this.w_DGQUADBL;
                  ,this.w_DGFA1001;
                  ,this.w_DGSA2001;
                  ,this.w_DGBL3001;
                  ,this.w_DGBL3002;
                  ,this.w_DGBL3003;
                  ,this.w_DGFE4001;
                  ,this.w_DGFE4002;
                  ,this.w_DGFR5001;
                  ,this.w_DGFR5002;
                  ,this.w_DGNE6001;
                  ,this.w_DGNR7001;
                  ,this.w_DGDF8001;
                  ,this.w_DGFN9001;
                  ,this.w_DGSE1001;
                  ,this.w_DGTU1101;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.GENDFILT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENDFILT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DGNOMFIL))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'GENDFILT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'GENDFILT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DGNOMFIL))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update GENDFILT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'GENDFILT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DGNOMFIL="+cp_ToStrODBC(this.w_DGNOMFIL)+;
                     ",DGPROINV="+cp_ToStrODBC(this.w_DGPROINV)+;
                     ",DGQUADFE="+cp_ToStrODBC(this.w_DGQUADFE)+;
                     ",DGQUADFR="+cp_ToStrODBC(this.w_DGQUADFR)+;
                     ",DGQUADNE="+cp_ToStrODBC(this.w_DGQUADNE)+;
                     ",DGQUADNR="+cp_ToStrODBC(this.w_DGQUADNR)+;
                     ",DGQUADDF="+cp_ToStrODBC(this.w_DGQUADDF)+;
                     ",DGQUADFN="+cp_ToStrODBC(this.w_DGQUADFN)+;
                     ",DGQUADSE="+cp_ToStrODBC(this.w_DGQUADSE)+;
                     ",DGQUADTU="+cp_ToStrODBC(this.w_DGQUADTU)+;
                     ",DGQUADFA="+cp_ToStrODBC(this.w_DGQUADFA)+;
                     ",DGQUADSA="+cp_ToStrODBC(this.w_DGQUADSA)+;
                     ",DGQUADBL="+cp_ToStrODBC(this.w_DGQUADBL)+;
                     ",DGFA1001="+cp_ToStrODBC(this.w_DGFA1001)+;
                     ",DGSA2001="+cp_ToStrODBC(this.w_DGSA2001)+;
                     ",DGBL3001="+cp_ToStrODBC(this.w_DGBL3001)+;
                     ",DGBL3002="+cp_ToStrODBC(this.w_DGBL3002)+;
                     ",DGBL3003="+cp_ToStrODBC(this.w_DGBL3003)+;
                     ",DGFE4001="+cp_ToStrODBC(this.w_DGFE4001)+;
                     ",DGFE4002="+cp_ToStrODBC(this.w_DGFE4002)+;
                     ",DGFR5001="+cp_ToStrODBC(this.w_DGFR5001)+;
                     ",DGFR5002="+cp_ToStrODBC(this.w_DGFR5002)+;
                     ",DGNE6001="+cp_ToStrODBC(this.w_DGNE6001)+;
                     ",DGNR7001="+cp_ToStrODBC(this.w_DGNR7001)+;
                     ",DGDF8001="+cp_ToStrODBC(this.w_DGDF8001)+;
                     ",DGFN9001="+cp_ToStrODBC(this.w_DGFN9001)+;
                     ",DGSE1001="+cp_ToStrODBC(this.w_DGSE1001)+;
                     ",DGTU1101="+cp_ToStrODBC(this.w_DGTU1101)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'GENDFILT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DGNOMFIL=this.w_DGNOMFIL;
                     ,DGPROINV=this.w_DGPROINV;
                     ,DGQUADFE=this.w_DGQUADFE;
                     ,DGQUADFR=this.w_DGQUADFR;
                     ,DGQUADNE=this.w_DGQUADNE;
                     ,DGQUADNR=this.w_DGQUADNR;
                     ,DGQUADDF=this.w_DGQUADDF;
                     ,DGQUADFN=this.w_DGQUADFN;
                     ,DGQUADSE=this.w_DGQUADSE;
                     ,DGQUADTU=this.w_DGQUADTU;
                     ,DGQUADFA=this.w_DGQUADFA;
                     ,DGQUADSA=this.w_DGQUADSA;
                     ,DGQUADBL=this.w_DGQUADBL;
                     ,DGFA1001=this.w_DGFA1001;
                     ,DGSA2001=this.w_DGSA2001;
                     ,DGBL3001=this.w_DGBL3001;
                     ,DGBL3002=this.w_DGBL3002;
                     ,DGBL3003=this.w_DGBL3003;
                     ,DGFE4001=this.w_DGFE4001;
                     ,DGFE4002=this.w_DGFE4002;
                     ,DGFR5001=this.w_DGFR5001;
                     ,DGFR5002=this.w_DGFR5002;
                     ,DGNE6001=this.w_DGNE6001;
                     ,DGNR7001=this.w_DGNR7001;
                     ,DGDF8001=this.w_DGDF8001;
                     ,DGFN9001=this.w_DGFN9001;
                     ,DGSE1001=this.w_DGSE1001;
                     ,DGTU1101=this.w_DGTU1101;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GENDFILT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENDFILT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DGNOMFIL))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete GENDFILT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DGNOMFIL))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENDFILT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENDFILT_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDGSERIAL_1_1.value==this.w_DGSERIAL)
      this.oPgFrm.Page1.oPag.oDGSERIAL_1_1.value=this.w_DGSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNOMFIL_2_1.value==this.w_DGNOMFIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNOMFIL_2_1.value=this.w_DGNOMFIL
      replace t_DGNOMFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNOMFIL_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGPROINV_2_2.value==this.w_DGPROINV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGPROINV_2_2.value=this.w_DGPROINV
      replace t_DGPROINV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGPROINV_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFE_2_3.value==this.w_DGQUADFE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFE_2_3.value=this.w_DGQUADFE
      replace t_DGQUADFE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFR_2_4.value==this.w_DGQUADFR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFR_2_4.value=this.w_DGQUADFR
      replace t_DGQUADFR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFR_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADNE_2_5.value==this.w_DGQUADNE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADNE_2_5.value=this.w_DGQUADNE
      replace t_DGQUADNE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADNE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADNR_2_6.value==this.w_DGQUADNR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADNR_2_6.value=this.w_DGQUADNR
      replace t_DGQUADNR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADNR_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADDF_2_7.value==this.w_DGQUADDF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADDF_2_7.value=this.w_DGQUADDF
      replace t_DGQUADDF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADDF_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFN_2_8.value==this.w_DGQUADFN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFN_2_8.value=this.w_DGQUADFN
      replace t_DGQUADFN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFN_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADSE_2_9.value==this.w_DGQUADSE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADSE_2_9.value=this.w_DGQUADSE
      replace t_DGQUADSE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADSE_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADTU_2_10.value==this.w_DGQUADTU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADTU_2_10.value=this.w_DGQUADTU
      replace t_DGQUADTU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADTU_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFA_2_11.value==this.w_DGQUADFA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFA_2_11.value=this.w_DGQUADFA
      replace t_DGQUADFA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADFA_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADSA_2_12.value==this.w_DGQUADSA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADSA_2_12.value=this.w_DGQUADSA
      replace t_DGQUADSA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADSA_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADBL_2_13.value==this.w_DGQUADBL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADBL_2_13.value=this.w_DGQUADBL
      replace t_DGQUADBL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGQUADBL_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFA1001_2_14.value==this.w_DGFA1001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFA1001_2_14.value=this.w_DGFA1001
      replace t_DGFA1001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFA1001_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGSA2001_2_15.value==this.w_DGSA2001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGSA2001_2_15.value=this.w_DGSA2001
      replace t_DGSA2001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGSA2001_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3001_2_16.value==this.w_DGBL3001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3001_2_16.value=this.w_DGBL3001
      replace t_DGBL3001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3001_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3002_2_17.value==this.w_DGBL3002)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3002_2_17.value=this.w_DGBL3002
      replace t_DGBL3002 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3002_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3003_2_18.value==this.w_DGBL3003)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3003_2_18.value=this.w_DGBL3003
      replace t_DGBL3003 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGBL3003_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFE4001_2_19.value==this.w_DGFE4001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFE4001_2_19.value=this.w_DGFE4001
      replace t_DGFE4001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFE4001_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFE4002_2_20.value==this.w_DGFE4002)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFE4002_2_20.value=this.w_DGFE4002
      replace t_DGFE4002 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFE4002_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFR5001_2_21.value==this.w_DGFR5001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFR5001_2_21.value=this.w_DGFR5001
      replace t_DGFR5001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFR5001_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFR5002_2_22.value==this.w_DGFR5002)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFR5002_2_22.value=this.w_DGFR5002
      replace t_DGFR5002 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFR5002_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNE6001_2_23.value==this.w_DGNE6001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNE6001_2_23.value=this.w_DGNE6001
      replace t_DGNE6001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNE6001_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNR7001_2_24.value==this.w_DGNR7001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNR7001_2_24.value=this.w_DGNR7001
      replace t_DGNR7001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGNR7001_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGDF8001_2_25.value==this.w_DGDF8001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGDF8001_2_25.value=this.w_DGDF8001
      replace t_DGDF8001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGDF8001_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFN9001_2_26.value==this.w_DGFN9001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFN9001_2_26.value=this.w_DGFN9001
      replace t_DGFN9001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGFN9001_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGSE1001_2_27.value==this.w_DGSE1001)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGSE1001_2_27.value=this.w_DGSE1001
      replace t_DGSE1001 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGSE1001_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGTU1101_2_28.value==this.w_DGTU1101)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGTU1101_2_28.value=this.w_DGTU1101
      replace t_DGTU1101 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDGTU1101_2_28.value
    endif
    cp_SetControlsValueExtFlds(this,'GENDFILT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_DGNOMFIL))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DGNOMFIL)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DGNOMFIL=space(254)
      .w_DGPROINV=0
      .w_DGQUADFE=space(1)
      .w_DGQUADFR=space(1)
      .w_DGQUADNE=space(1)
      .w_DGQUADNR=space(1)
      .w_DGQUADDF=space(1)
      .w_DGQUADFN=space(1)
      .w_DGQUADSE=space(1)
      .w_DGQUADTU=space(1)
      .w_DGQUADFA=space(1)
      .w_DGQUADSA=space(1)
      .w_DGQUADBL=space(1)
      .w_DGFA1001=0
      .w_DGSA2001=0
      .w_DGBL3001=0
      .w_DGBL3002=0
      .w_DGBL3003=0
      .w_DGFE4001=0
      .w_DGFE4002=0
      .w_DGFR5001=0
      .w_DGFR5002=0
      .w_DGNE6001=0
      .w_DGNR7001=0
      .w_DGDF8001=0
      .w_DGFN9001=0
      .w_DGSE1001=0
      .w_DGTU1101=0
    endwith
    this.DoRTCalc(1,29,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DGNOMFIL = t_DGNOMFIL
    this.w_DGPROINV = t_DGPROINV
    this.w_DGQUADFE = t_DGQUADFE
    this.w_DGQUADFR = t_DGQUADFR
    this.w_DGQUADNE = t_DGQUADNE
    this.w_DGQUADNR = t_DGQUADNR
    this.w_DGQUADDF = t_DGQUADDF
    this.w_DGQUADFN = t_DGQUADFN
    this.w_DGQUADSE = t_DGQUADSE
    this.w_DGQUADTU = t_DGQUADTU
    this.w_DGQUADFA = t_DGQUADFA
    this.w_DGQUADSA = t_DGQUADSA
    this.w_DGQUADBL = t_DGQUADBL
    this.w_DGFA1001 = t_DGFA1001
    this.w_DGSA2001 = t_DGSA2001
    this.w_DGBL3001 = t_DGBL3001
    this.w_DGBL3002 = t_DGBL3002
    this.w_DGBL3003 = t_DGBL3003
    this.w_DGFE4001 = t_DGFE4001
    this.w_DGFE4002 = t_DGFE4002
    this.w_DGFR5001 = t_DGFR5001
    this.w_DGFR5002 = t_DGFR5002
    this.w_DGNE6001 = t_DGNE6001
    this.w_DGNR7001 = t_DGNR7001
    this.w_DGDF8001 = t_DGDF8001
    this.w_DGFN9001 = t_DGFN9001
    this.w_DGSE1001 = t_DGSE1001
    this.w_DGTU1101 = t_DGTU1101
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DGNOMFIL with this.w_DGNOMFIL
    replace t_DGPROINV with this.w_DGPROINV
    replace t_DGQUADFE with this.w_DGQUADFE
    replace t_DGQUADFR with this.w_DGQUADFR
    replace t_DGQUADNE with this.w_DGQUADNE
    replace t_DGQUADNR with this.w_DGQUADNR
    replace t_DGQUADDF with this.w_DGQUADDF
    replace t_DGQUADFN with this.w_DGQUADFN
    replace t_DGQUADSE with this.w_DGQUADSE
    replace t_DGQUADTU with this.w_DGQUADTU
    replace t_DGQUADFA with this.w_DGQUADFA
    replace t_DGQUADSA with this.w_DGQUADSA
    replace t_DGQUADBL with this.w_DGQUADBL
    replace t_DGFA1001 with this.w_DGFA1001
    replace t_DGSA2001 with this.w_DGSA2001
    replace t_DGBL3001 with this.w_DGBL3001
    replace t_DGBL3002 with this.w_DGBL3002
    replace t_DGBL3003 with this.w_DGBL3003
    replace t_DGFE4001 with this.w_DGFE4001
    replace t_DGFE4002 with this.w_DGFE4002
    replace t_DGFR5001 with this.w_DGFR5001
    replace t_DGFR5002 with this.w_DGFR5002
    replace t_DGNE6001 with this.w_DGNE6001
    replace t_DGNR7001 with this.w_DGNR7001
    replace t_DGDF8001 with this.w_DGDF8001
    replace t_DGFN9001 with this.w_DGFN9001
    replace t_DGSE1001 with this.w_DGSE1001
    replace t_DGTU1101 with this.w_DGTU1101
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsai_mdsPag1 as StdContainer
  Width  = 1843
  height = 277
  stdWidth  = 1843
  stdheight = 277
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDGSERIAL_1_1 as StdField with uid="DSOAUERDIL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DGSERIAL", cQueryName = "DGSERIAL",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 106924926,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=120, Top=25, InputMask=replicate('X',10)

  add object oStr_1_2 as StdString with uid="YHBZXFRKOV",Visible=.t., Left=6, Top=29,;
    Alignment=1, Width=113, Height=18,;
    Caption="Seriale file generato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="JBTFSXYWYE",Visible=.t., Left=19, Top=61,;
    Alignment=0, Width=150, Height=18,;
    Caption="Nome file inoltro telematico"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=10,top=92,;
    width=1804+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=11,top=93,width=1803+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsai_mdsBodyRow as CPBodyRowCnt
  Width=1794
  Height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDGNOMFIL_2_1 as StdTrsField with uid="MZHPAGGJUN",rtseq=2,rtrep=.t.,;
    cFormVar="w_DGNOMFIL",value=space(254),;
    HelpContextID = 106570882,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=1791, Left=-2, Top=0, InputMask=replicate('X',254)

  add object oDGPROINV_2_2 as StdTrsField with uid="XSPDNNBPFF",rtseq=3,rtrep=.t.,;
    cFormVar="w_DGPROINV",value=0,;
    HelpContextID = 109230964,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=19, cSayPict=["9999"], cGetPict=["9999"]

  add object oDGQUADFE_2_3 as StdTrsField with uid="LVFCVHYTAJ",rtseq=4,rtrep=.t.,;
    cFormVar="w_DGQUADFE",value=space(1),;
    HelpContextID = 60839035,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=118, Top=19, InputMask=replicate('X',1)

  add object oDGQUADFR_2_4 as StdTrsField with uid="GYUQSAHMJN",rtseq=5,rtrep=.t.,;
    cFormVar="w_DGQUADFR",value=space(1),;
    HelpContextID = 60839048,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=143, Top=19, InputMask=replicate('X',1)

  add object oDGQUADNE_2_5 as StdTrsField with uid="PQBSVPYFLL",rtseq=6,rtrep=.t.,;
    cFormVar="w_DGQUADNE",value=space(1),;
    HelpContextID = 207596421,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=168, Top=19, InputMask=replicate('X',1)

  add object oDGQUADNR_2_6 as StdTrsField with uid="XNFNRIPKRC",rtseq=7,rtrep=.t.,;
    cFormVar="w_DGQUADNR",value=space(1),;
    HelpContextID = 207596408,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=193, Top=19, InputMask=replicate('X',1)

  add object oDGQUADDF_2_7 as StdTrsField with uid="KPNBDGOXRG",rtseq=8,rtrep=.t.,;
    cFormVar="w_DGQUADDF",value=space(1),;
    HelpContextID = 60839036,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=218, Top=19, InputMask=replicate('X',1)

  add object oDGQUADFN_2_8 as StdTrsField with uid="QEXQSLTYYV",rtseq=9,rtrep=.t.,;
    cFormVar="w_DGQUADFN",value=space(1),;
    HelpContextID = 60839044,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=243, Top=19, InputMask=replicate('X',1)

  add object oDGQUADSE_2_9 as StdTrsField with uid="RUQEFYYGLC",rtseq=10,rtrep=.t.,;
    cFormVar="w_DGQUADSE",value=space(1),;
    HelpContextID = 60839035,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=268, Top=19, InputMask=replicate('X',1)

  add object oDGQUADTU_2_10 as StdTrsField with uid="NIGYCVXKOY",rtseq=11,rtrep=.t.,;
    cFormVar="w_DGQUADTU",value=space(1),;
    HelpContextID = 60839051,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=293, Top=19, InputMask=replicate('X',1)

  add object oDGQUADFA_2_11 as StdTrsField with uid="MJVYDUQWEL",rtseq=12,rtrep=.t.,;
    cFormVar="w_DGQUADFA",value=space(1),;
    HelpContextID = 60839031,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=318, Top=19, InputMask=replicate('X',1)

  add object oDGQUADSA_2_12 as StdTrsField with uid="VKPJCHRMGL",rtseq=13,rtrep=.t.,;
    cFormVar="w_DGQUADSA",value=space(1),;
    HelpContextID = 60839031,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=343, Top=19, InputMask=replicate('X',1)

  add object oDGQUADBL_2_13 as StdTrsField with uid="FOXBELSNOG",rtseq=14,rtrep=.t.,;
    cFormVar="w_DGQUADBL",value=space(1),;
    HelpContextID = 60839042,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=368, Top=19, InputMask=replicate('X',1)

  add object oDGFA1001_2_14 as StdTrsField with uid="TEVINBRHTH",rtseq=15,rtrep=.t.,;
    cFormVar="w_DGFA1001",value=0,;
    HelpContextID = 24402841,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=393, Top=19, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGSA2001_2_15 as StdTrsField with uid="GGGDJKWFQP",rtseq=16,rtrep=.t.,;
    cFormVar="w_DGSA2001",value=0,;
    HelpContextID = 23301017,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=453, Top=19, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGBL3001_2_16 as StdTrsField with uid="YBFQGVZJME",rtseq=17,rtrep=.t.,;
    cFormVar="w_DGBL3001",value=0,;
    HelpContextID = 21601177,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=513, Top=19, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGBL3002_2_17 as StdTrsField with uid="HEZSYYBGTM",rtseq=18,rtrep=.t.,;
    cFormVar="w_DGBL3002",value=0,;
    HelpContextID = 21601176,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGBL3003_2_18 as StdTrsField with uid="VRNJVGXLDD",rtseq=19,rtrep=.t.,;
    cFormVar="w_DGBL3003",value=0,;
    HelpContextID = 21601175,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=58, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGFE4001_2_19 as StdTrsField with uid="SXCCPILSVS",rtseq=20,rtrep=.t.,;
    cFormVar="w_DGFE4001",value=0,;
    HelpContextID = 20994969,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=118, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGFE4002_2_20 as StdTrsField with uid="XBMDJIWPEC",rtseq=21,rtrep=.t.,;
    cFormVar="w_DGFE4002",value=0,;
    HelpContextID = 20994968,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=178, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGFR5001_2_21 as StdTrsField with uid="CMZJULXSRP",rtseq=22,rtrep=.t.,;
    cFormVar="w_DGFR5001",value=0,;
    HelpContextID = 19094425,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=238, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGFR5002_2_22 as StdTrsField with uid="YSEZJTPIRZ",rtseq=23,rtrep=.t.,;
    cFormVar="w_DGFR5002",value=0,;
    HelpContextID = 19094424,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=298, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGNE6001_2_23 as StdTrsField with uid="LMOXHVFVJF",rtseq=24,rtrep=.t.,;
    cFormVar="w_DGNE6001",value=0,;
    HelpContextID = 18865049,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=358, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGNR7001_2_24 as StdTrsField with uid="AAUETILFPS",rtseq=25,rtrep=.t.,;
    cFormVar="w_DGNR7001",value=0,;
    HelpContextID = 16964505,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=418, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGDF8001_2_25 as StdTrsField with uid="XPOEEQQZIV",rtseq=26,rtrep=.t.,;
    cFormVar="w_DGDF8001",value=0,;
    HelpContextID = 16743321,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=478, Top=38, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGFN9001_2_26 as StdTrsField with uid="XRJFAYAQDX",rtseq=27,rtrep=.t.,;
    cFormVar="w_DGFN9001",value=0,;
    HelpContextID = 15162265,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=57, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGSE1001_2_27 as StdTrsField with uid="LCIGMTQEGG",rtseq=28,rtrep=.t.,;
    cFormVar="w_DGSE1001",value=0,;
    HelpContextID = 24087449,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=58, Top=57, cSayPict=["999999"], cGetPict=["999999"]

  add object oDGTU1101_2_28 as StdTrsField with uid="AOHTCFQCTQ",rtseq=29,rtrep=.t.,;
    cFormVar="w_DGTU1101",value=0,;
    HelpContextID = 6257561,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=118, Top=57, cSayPict=["999999"], cGetPict=["999999"]
  add object oLast as LastKeyMover
  * ---
  func oDGNOMFIL_2_1.When()
    return(.t.)
  proc oDGNOMFIL_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDGNOMFIL_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=1
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_mds','GENDFILT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DGSERIAL=GENDFILT.DGSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
