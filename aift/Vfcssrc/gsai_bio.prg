* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bio                                                        *
*              Inclusione fatture da analisi ordini                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-12-02                                                      *
* Last revis.: 2012-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bio",oParentObject,m.pParame)
return(i_retval)

define class tgsai_bio as StdBatch
  * --- Local variables
  pParame = space(1)
  w_COND = .f.
  w_NREC = 0
  w_APPO = space(10)
  w_AZIENDA = space(5)
  w_ANNO = space(4)
  w_CODATT = space(5)
  * --- WorkFile variables
  TMPTRFLUSSI_idx=0
  OPERSUPE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inclusione fatture da analisi ordini
    do case
      case this.pParame="S"
        * --- Create temporary table TMPTRFLUSSI
        i_nIdx=cp_AddTableDef('TMPTRFLUSSI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\AIFT\EXE\QUERY\GSAI_KIO',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPTRFLUSSI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Elimino i documenti (Ordini) che hanno la somma degli imponibili pi�
        *     bassa dell'importo da filtrare.La cancellazione avviene solo se �
        *     presente un importo da filtrare
        if this.oParentObject.w_IMP_FILTRO>0
          * --- Delete from TMPTRFLUSSI
          i_nConn=i_TableProp[this.TMPTRFLUSSI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPTRFLUSSI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
          
            do vq_exec with 'Gsai0kio',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        this.w_COND = .T.
        do while this.w_COND
          * --- Insert into TMPTRFLUSSI
          i_nConn=i_TableProp[this.TMPTRFLUSSI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPTRFLUSSI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSAI1KIO",this.TMPTRFLUSSI_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_COND = i_Rows>0
          * --- Write into TMPTRFLUSSI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPTRFLUSSI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPTRFLUSSI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPTRFLUSSI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PROCESSATO =PROCESSATO+ "+cp_ToStrODBC(1);
                +i_ccchkf ;
            +" where ";
                +"1 = "+cp_ToStrODBC(1);
                   )
          else
            update (i_cTable) set;
                PROCESSATO = PROCESSATO + 1;
                &i_ccchkf. ;
             where;
                1 = 1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        enddo
        * --- Eliminare i record che hanno ciclo documentale differente da quello selezionato
        * --- Delete from TMPTRFLUSSI
        i_nConn=i_TableProp[this.TMPTRFLUSSI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTRFLUSSI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
        
          do vq_exec with 'GSAI2KIO',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Elimino eventuali documenti che hanno un intestatario nell'ordine diverso 
        *     da quello presente nella fattura
        * --- Delete from TMPTRFLUSSI
        i_nConn=i_TableProp[this.TMPTRFLUSSI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTRFLUSSI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
        
          do vq_exec with 'GSAI7KIO',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        if this.oParentObject.w_TIPO_OPE="S"
          L_CICLO=this.oParentObject.w_CICLO
          L_TIPODOC=this.oParentObject.w_TIPODOC
          L_DATINI=this.oParentObject.w_DATINI
          L_DATFIN=this.oParentObject.w_DATFIN
          L_DATINIFAT=this.oParentObject.w_DATINIFAT
          L_DATFINFAT=this.oParentObject.w_DATFINFAT
          L_IMP_FILTRO=this.oParentObject.w_IMP_FILTRO
          w_QUERYP = ALLTRIM(this.oParentObject.w_OQRY)
          vq_exec(w_QUERYP, this, "__TMP__")
          CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP),"",this.oParentObject )
        else
          if Ah_Yesno("Si desidera aggiornare i dati relativi a 'Operazioni superiori a 3000 euro'?")
            * --- Elimino dal cursore temporaneo "TMPTRFLUSSI" tutti i documenti che
            *     hanno tipologia documento uguale a "Ordini" 
            * --- Delete from TMPTRFLUSSI
            i_nConn=i_TableProp[this.TMPTRFLUSSI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPTRFLUSSI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"MVCLADOC = "+cp_ToStrODBC("OR");
                     )
            else
              delete from (i_cTable) where;
                    MVCLADOC = "OR";

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Select from TMPTRFLUSSI
            i_nConn=i_TableProp[this.TMPTRFLUSSI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPTRFLUSSI_idx,2],.t.,this.TMPTRFLUSSI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select count(*) AS NREC  from "+i_cTable+" TMPTRFLUSSI ";
                   ,"_Curs_TMPTRFLUSSI")
            else
              select count(*) AS NREC from (i_cTable);
                into cursor _Curs_TMPTRFLUSSI
            endif
            if used('_Curs_TMPTRFLUSSI')
              select _Curs_TMPTRFLUSSI
              locate for 1=1
              do while not(eof())
              this.w_NREC = NREC
                select _Curs_TMPTRFLUSSI
                continue
              enddo
              use
            endif
            if this.w_NREC>0
              * --- Try
              local bErr_037DACB8
              bErr_037DACB8=bTrsErr
              this.Try_037DACB8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                ah_errormsg("Impossibile aggiornare le registrazioni contabili")
              endif
              bTrsErr=bTrsErr or bErr_037DACB8
              * --- End
            else
              Ah_ErrorMsg("Non esistono registrazioni contabili da aggiornare")
            endif
          else
            ah_errormsg("Operazione abbandonata",48)
          endif
        endif
        * --- Drop temporary table TMPTRFLUSSI
        i_nIdx=cp_GetTableDefIdx('TMPTRFLUSSI')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPTRFLUSSI')
        endif
      case this.pParame="I"
        this.w_AZIENDA = i_CODAZI
        this.w_ANNO = this.oParentObject.w_ANNO_FILTRO
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          if g_Attivi<>"S"
            this.w_CODATT = g_CATAZI
          else
            this.w_CODATT = SPACE(5)
          endif
          * --- Select from Gsai14bde
          do vq_exec with 'Gsai14bde',this,'_Curs_Gsai14bde','',.f.,.t.
          if used('_Curs_Gsai14bde')
            select _Curs_Gsai14bde
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_IMP_FILTRO = _Curs_Gsai14bde.AIMINFAT
            Exit
              select _Curs_Gsai14bde
              continue
            enddo
            use
          endif
        else
          * --- Select from Gsai14bder
          do vq_exec with 'Gsai14bder',this,'_Curs_Gsai14bder','',.f.,.t.
          if used('_Curs_Gsai14bder')
            select _Curs_Gsai14bder
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_IMP_FILTRO = _Curs_Gsai14bder.IAMINFAT
              select _Curs_Gsai14bder
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc
  proc Try_037DACB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Insert into OPERSUPE
    i_nConn=i_TableProp[this.OPERSUPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\AIFT\EXE\QUERY\GSAI5KIO",this.OPERSUPE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NREC = i_Rows
    this.w_APPO = "Operazione completata. %0N. %1 registrazioni contabili aggiornate"
    AH_ERRORMSG(this.w_APPO,64,,ALLTRIM(STR(this.w_NREC)))
    * --- commit
    cp_EndTrs(.t.)
    if Ah_Yesno("Si vuole stampare l'elenco delle registrazioni contabili aggiornate?")
      vq_exec("..\Aift\exe\query\Gsai6kio", this, "__TMP__")
      cp_chprn("..\Aift\exe\query\Gsai1Kio","",this)
    endif
    return


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPTRFLUSSI'
    this.cWorkTables[2]='OPERSUPE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_TMPTRFLUSSI')
      use in _Curs_TMPTRFLUSSI
    endif
    if used('_Curs_Gsai14bde')
      use in _Curs_Gsai14bde
    endif
    if used('_Curs_Gsai14bder')
      use in _Curs_Gsai14bder
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
