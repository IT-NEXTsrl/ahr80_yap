* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_sfc                                                        *
*              Elenco identificativi fiscali da controllare                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-11-14                                                      *
* Last revis.: 2013-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_sfc",oParentObject))

* --- Class definition
define class tgsai_sfc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 534
  Height = 195
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-21"
  HelpContextID=40503145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsai_sfc"
  cComment = "Elenco identificativi fiscali da controllare"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SP__ANNO = space(4)
  o_SP__ANNO = space(4)
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_SPTIPSOG = space(1)
  w_TIPO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_sfcPag1","gsai_sfc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSP__ANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SP__ANNO=space(4)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_SPTIPSOG=space(1)
      .w_TIPO=space(1)
        .w_SP__ANNO = STR(Year(i_datsys)-1,4)
        .w_DATA1 = Cp_chartodate('01-01-'+.w_SP__ANNO)
        .w_DATA2 = Cp_chartodate('31-12-'+.w_SP__ANNO)
        .w_SPTIPSOG = 'X'
        .w_TIPO = IIF(.w_SPTIPSOG='X','',.w_SPTIPSOG)
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_SP__ANNO<>.w_SP__ANNO
            .w_DATA1 = Cp_chartodate('01-01-'+.w_SP__ANNO)
        endif
        if .o_SP__ANNO<>.w_SP__ANNO
            .w_DATA2 = Cp_chartodate('31-12-'+.w_SP__ANNO)
        endif
        .DoRTCalc(4,4,.t.)
            .w_TIPO = IIF(.w_SPTIPSOG='X','',.w_SPTIPSOG)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSP__ANNO_1_1.value==this.w_SP__ANNO)
      this.oPgFrm.Page1.oPag.oSP__ANNO_1_1.value=this.w_SP__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_2.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_2.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_3.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_3.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oSPTIPSOG_1_5.RadioValue()==this.w_SPTIPSOG)
      this.oPgFrm.Page1.oPag.oSPTIPSOG_1_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_SP__ANNO)) or not(.w_SP__ANNO>'2005' and .w_SP__ANNO<'2100'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSP__ANNO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_SP__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_data1<=.w_data2 or (empty(.w_DATA2))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not(.w_DATA1<=.w_DATA2 or (empty(.w_DATA1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SP__ANNO = this.w_SP__ANNO
    return

enddefine

* --- Define pages as container
define class tgsai_sfcPag1 as StdContainer
  Width  = 530
  height = 195
  stdWidth  = 530
  stdheight = 195
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSP__ANNO_1_1 as StdField with uid="UQWWLVVPBH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SP__ANNO", cQueryName = "SP__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 267698571,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=131, Top=19, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oSP__ANNO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SP__ANNO>'2005' and .w_SP__ANNO<'2100')
    endwith
    return bRes
  endfunc

  add object oDATA1_1_2 as StdField with uid="WCPTHGVVXN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data di registrazione iniziale",;
    HelpContextID = 252936650,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=131, Top=47

  func oDATA1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_data1<=.w_data2 or (empty(.w_DATA2))))
    endwith
    return bRes
  endfunc

  add object oDATA2_1_3 as StdField with uid="YEPEJUCQJU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data di registrazione finale",;
    HelpContextID = 251888074,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=251, Top=47

  func oDATA2_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATA1<=.w_DATA2 or (empty(.w_DATA1)))
    endwith
    return bRes
  endfunc


  add object oSPTIPSOG_1_5 as StdCombo with uid="VMJQAYOIOV",rtseq=4,rtrep=.f.,left=131,top=77,width=152,height=21;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 169570707;
    , cFormVar="w_SPTIPSOG",RowSource=""+"Iva,"+"Privato,"+"Tutti", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPTIPSOG_1_5.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oSPTIPSOG_1_5.GetRadio()
    this.Parent.oContained.w_SPTIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPSOG_1_5.SetRadio()
    this.Parent.oContained.w_SPTIPSOG=trim(this.Parent.oContained.w_SPTIPSOG)
    this.value = ;
      iif(this.Parent.oContained.w_SPTIPSOG=='I',1,;
      iif(this.Parent.oContained.w_SPTIPSOG=='P',2,;
      iif(this.Parent.oContained.w_SPTIPSOG=='X',3,;
      0)))
  endfunc


  add object oBtn_1_8 as StdButton with uid="KWFWCRWNHO",left=414, top=140, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 50442774;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        do GSAI_BFC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="KUWCMEGMLG",left=466, top=140, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 50442774;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_12 as cp_outputCombo with uid="ZLERXIVPWT",left=131, top=107, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 137494502

  add object oStr_1_4 as StdString with uid="WMPOHYZZNW",Visible=.t., Left=67, Top=19,;
    Alignment=1, Width=60, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="QPHBBHXHWS",Visible=.t., Left=8, Top=78,;
    Alignment=1, Width=119, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="PHFMRQMDGQ",Visible=.t., Left=93, Top=48,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BGRCELDVIO",Visible=.t., Left=231, Top=48,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="ZDLOVMWIXH",Visible=.t., Left=41, Top=108,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di Stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_sfc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
