* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bce                                                        *
*              Controllo importi negativi                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-24                                                      *
* Last revis.: 2011-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ONLYNEG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bce",oParentObject,m.w_ONLYNEG)
return(i_retval)

define class tgsai_bce as StdBatch
  * --- Local variables
  w_ONLYNEG = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_DEESCLUD<>"S"
      do case
        case this.oParentObject.w_DEIMPDOC<0 OR this.oParentObject.w_DEIMPOST<0
          ah_errormsg("Attenzione la riga con importi negativi sar� considerata in generazione")
        case this.oParentObject.w_DEIMPDOC=0 AND not this.w_ONLYNEG
          ah_errormsg("Attenzione la riga con importi uguali a zero sar� considerata in generazione")
      endcase
    endif
  endproc


  proc Init(oParentObject,w_ONLYNEG)
    this.w_ONLYNEG=w_ONLYNEG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ONLYNEG"
endproc
