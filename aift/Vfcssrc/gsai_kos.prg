* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_kos                                                        *
*              Manutenzione operazioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-25                                                      *
* Last revis.: 2011-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_kos",oParentObject))

* --- Class definition
define class tgsai_kos as StdForm
  Top    = 2
  Left   = 2

  * --- Standard Properties
  Width  = 804
  Height = 555+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-12-12"
  HelpContextID=166332265
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=99

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  ZONE_IDX = 0
  MASTRI_IDX = 0
  VOCIIVA_IDX = 0
  NAZIONI_IDX = 0
  VALUTE_IDX = 0
  cpusers_IDX = 0
  cPrg = "gsai_kos"
  cComment = "Manutenzione operazioni "
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_ESE = space(4)
  w_INESE = ctod('  /  /  ')
  w_ANNO_FILTRO = space(4)
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_PROVVI = space(1)
  w_VALUTA = space(3)
  w_MESE = 0
  w_ANNO = 0
  w_CAUSA = space(5)
  o_CAUSA = space(5)
  w_TIPREG = space(1)
  w_CCTIPDOC = space(2)
  w_CCFLRIFE = space(1)
  w_TIPDOC = space(2)
  o_TIPDOC = space(2)
  w_TIPDOC = space(2)
  w_ESCDOCINTRA = space(1)
  w_DATAD1 = ctod('  /  /  ')
  w_DATAD1 = ctod('  /  /  ')
  w_DATAD2 = ctod('  /  /  ')
  w_DATAD2 = ctod('  /  /  ')
  w_SERIE = space(10)
  w_SERIE = space(2)
  w_NUMDOC = 0
  w_NUMDOC1 = 0
  w_NUMDOC = 0
  w_NUMDOC1 = 0
  w_SERIEP = space(2)
  w_SERIEP = space(10)
  w_NUMPRO = 0
  w_NUMPRO1 = 0
  w_NUMPRO = 0
  w_NUMPRO1 = 0
  w_TIPCONEFF = space(1)
  o_TIPCONEFF = space(1)
  w_TIPCON = space(1)
  w_TITIVA = space(1)
  w_PERSFIS = space(1)
  w_ZONA = space(3)
  w_NAZIONE = space(3)
  w_CODCON = space(15)
  o_CODCON = space(15)
  w_CODCON = space(15)
  w_CONSUP = space(15)
  w_CONSUP = space(15)
  w_TIPMAS = space(1)
  w_ANOPETRE = space(1)
  w_ANTIPPRE = space(1)
  w_REGIVA = space(1)
  w_NUMIVA = 0
  w_NUMIVA1 = 0
  w_DATAIVA1 = ctod('  /  /  ')
  w_DATAIVA2 = ctod('  /  /  ')
  w_CODIVA = space(5)
  o_CODIVA = space(5)
  w_PERIVA = 0
  o_PERIVA = 0
  w_FLAGZERO = space(1)
  w_IVFLGBSV = space(1)
  w_IVFLGBSA = space(1)
  w_TOTDOC = space(1)
  w_FILTIMPO = space(1)
  o_FILTIMPO = space(1)
  w_DAIMPO = 0
  w_AIMPO = 0
  w_OPERVALO = space(10)
  o_OPERVALO = space(10)
  w_OPERVERI = space(1)
  w_OPGENERA = space(10)
  w_UTEINSER = 0
  o_UTEINSER = 0
  w_DATINSERI = ctod('  /  /  ')
  w_DATINSERF = ctod('  /  /  ')
  w_UTEVARIA = 0
  o_UTEVARIA = 0
  w_DATVARIAI = ctod('  /  /  ')
  w_DATVARIAF = ctod('  /  /  ')
  w_DESCRICA = space(35)
  w_DESIVA = space(35)
  w_DESMAS = space(35)
  w_DESCRI = space(35)
  w_DESUTE = space(20)
  w_DESUTE1 = space(20)
  w_SERIALE = space(10)
  w_OSTIPOPE = space(1)
  o_OSTIPOPE = space(1)
  w_CHKRIFCON = space(1)
  o_CHKRIFCON = space(1)
  w_OSRIFCON = space(30)
  w_OSTIPFAT = space(1)
  o_OSTIPFAT = space(1)
  w_OSIMPFRA = space(1)
  w_OSFLGEXT = space(1)
  o_OSFLGEXT = space(1)
  w_OSFLGDVE = space(1)
  w_OSRIFFAT = space(10)
  w_CCDESCRI = space(35)
  w_ANDESCRI = space(40)
  w_TIPFAT = space(1)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_FLGEXT = space(1)
  w_TIPRET = space(1)
  w_TIPOPE = space(1)
  w_OSRIFACC = space(10)
  w_RIFMOVCOL = space(10)
  w_TIPACC = space(1)
  w_DATREG = ctod('  /  /  ')
  w_AZIENDA = space(3)
  w_PNCODVAL = space(3)
  w_PNNUMRER = space(15)
  w_ZOOMVP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_kosPag1","gsai_kos",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsai_kosPag2","gsai_kos",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Visualizzazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATA1_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMVP = this.oPgFrm.Pages(2).oPag.ZOOMVP
    DoDefault()
    proc Destroy()
      this.w_ZOOMVP = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='BUSIUNIT'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='ZONE'
    this.cWorkTables[7]='MASTRI'
    this.cWorkTables[8]='VOCIIVA'
    this.cWorkTables[9]='NAZIONI'
    this.cWorkTables[10]='VALUTE'
    this.cWorkTables[11]='cpusers'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsai_kos
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_ESE=space(4)
      .w_INESE=ctod("  /  /  ")
      .w_ANNO_FILTRO=space(4)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_PROVVI=space(1)
      .w_VALUTA=space(3)
      .w_MESE=0
      .w_ANNO=0
      .w_CAUSA=space(5)
      .w_TIPREG=space(1)
      .w_CCTIPDOC=space(2)
      .w_CCFLRIFE=space(1)
      .w_TIPDOC=space(2)
      .w_TIPDOC=space(2)
      .w_ESCDOCINTRA=space(1)
      .w_DATAD1=ctod("  /  /  ")
      .w_DATAD1=ctod("  /  /  ")
      .w_DATAD2=ctod("  /  /  ")
      .w_DATAD2=ctod("  /  /  ")
      .w_SERIE=space(10)
      .w_SERIE=space(2)
      .w_NUMDOC=0
      .w_NUMDOC1=0
      .w_NUMDOC=0
      .w_NUMDOC1=0
      .w_SERIEP=space(2)
      .w_SERIEP=space(10)
      .w_NUMPRO=0
      .w_NUMPRO1=0
      .w_NUMPRO=0
      .w_NUMPRO1=0
      .w_TIPCONEFF=space(1)
      .w_TIPCON=space(1)
      .w_TITIVA=space(1)
      .w_PERSFIS=space(1)
      .w_ZONA=space(3)
      .w_NAZIONE=space(3)
      .w_CODCON=space(15)
      .w_CODCON=space(15)
      .w_CONSUP=space(15)
      .w_CONSUP=space(15)
      .w_TIPMAS=space(1)
      .w_ANOPETRE=space(1)
      .w_ANTIPPRE=space(1)
      .w_REGIVA=space(1)
      .w_NUMIVA=0
      .w_NUMIVA1=0
      .w_DATAIVA1=ctod("  /  /  ")
      .w_DATAIVA2=ctod("  /  /  ")
      .w_CODIVA=space(5)
      .w_PERIVA=0
      .w_FLAGZERO=space(1)
      .w_IVFLGBSV=space(1)
      .w_IVFLGBSA=space(1)
      .w_TOTDOC=space(1)
      .w_FILTIMPO=space(1)
      .w_DAIMPO=0
      .w_AIMPO=0
      .w_OPERVALO=space(10)
      .w_OPERVERI=space(1)
      .w_OPGENERA=space(10)
      .w_UTEINSER=0
      .w_DATINSERI=ctod("  /  /  ")
      .w_DATINSERF=ctod("  /  /  ")
      .w_UTEVARIA=0
      .w_DATVARIAI=ctod("  /  /  ")
      .w_DATVARIAF=ctod("  /  /  ")
      .w_DESCRICA=space(35)
      .w_DESIVA=space(35)
      .w_DESMAS=space(35)
      .w_DESCRI=space(35)
      .w_DESUTE=space(20)
      .w_DESUTE1=space(20)
      .w_SERIALE=space(10)
      .w_OSTIPOPE=space(1)
      .w_CHKRIFCON=space(1)
      .w_OSRIFCON=space(30)
      .w_OSTIPFAT=space(1)
      .w_OSIMPFRA=space(1)
      .w_OSFLGEXT=space(1)
      .w_OSFLGDVE=space(1)
      .w_OSRIFFAT=space(10)
      .w_CCDESCRI=space(35)
      .w_ANDESCRI=space(40)
      .w_TIPFAT=space(1)
      .w_PNTIPCLF=space(1)
      .w_PNCODCLF=space(15)
      .w_FLGEXT=space(1)
      .w_TIPRET=space(1)
      .w_TIPOPE=space(1)
      .w_OSRIFACC=space(10)
      .w_RIFMOVCOL=space(10)
      .w_TIPACC=space(1)
      .w_DATREG=ctod("  /  /  ")
      .w_AZIENDA=space(3)
      .w_PNCODVAL=space(3)
      .w_PNNUMRER=space(15)
        .w_OBTEST = iif(EMPTY(.w_DATA2), i_datsys, .w_DATA2)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ESE))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_ANNO_FILTRO = STR(Year(i_datsys)-1,4)
        .w_DATA1 = Cp_chartodate('01-01-'+.w_ANNO_FILTRO)
        .w_DATA2 = Cp_chartodate('31-12-'+.w_ANNO_FILTRO)
        .w_PROVVI = 'N'
        .w_VALUTA = g_PERVAL
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_VALUTA))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,11,.f.)
        if not(empty(.w_CAUSA))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,14,.f.)
        .w_TIPDOC = IIF(NOT EMPTY(.w_CAUSA),'TT',IIF(EMPTY(.w_TIPDOC),'TT',.w_TIPDOC))
        .w_TIPDOC = IIF(NOT EMPTY(.w_CAUSA),'TT',IIF(EMPTY(.w_TIPDOC),'TT',.w_TIPDOC))
        .w_ESCDOCINTRA = IIF(.w_TIPDOC='FE' Or .w_TIPDOC='NE','N','S')
          .DoRTCalc(18,33,.f.)
        .w_TIPCONEFF = 'C'
        .w_TIPCON = IIF(.w_TIPCONEFF $ 'CI','C','F')
        .w_TITIVA = 'T'
        .w_PERSFIS = IIF(NOT EMPTY(.w_CODCON) OR .w_TIPCONEFF $ 'IZ',' ',.w_PERSFIS)
        .w_ZONA = IIF(NOT EMPTY(.w_CODCON) Or .w_TIPCONEFF $ 'IZ',space(3),.w_ZONA)
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_ZONA))
          .link_1_38('Full')
        endif
        .w_NAZIONE = IIF(NOT EMPTY(.w_CODCON) Or .w_TIPCONEFF $ 'IZ',space(3),.w_NAZIONE)
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_NAZIONE))
          .link_1_39('Full')
        endif
        .w_CODCON = space(15)
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_CODCON))
          .link_1_40('Full')
        endif
        .w_CODCON = space(15)
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_CODCON))
          .link_1_41('Full')
        endif
        .w_CONSUP = SPACE(15)
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_CONSUP))
          .link_1_42('Full')
        endif
        .w_CONSUP = SPACE(15)
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_CONSUP))
          .link_1_43('Full')
        endif
          .DoRTCalc(44,44,.f.)
        .w_ANOPETRE = 'T'
        .w_ANTIPPRE = 'T'
        .w_REGIVA = 'T'
        .DoRTCalc(48,52,.f.)
        if not(empty(.w_CODIVA))
          .link_1_52('Full')
        endif
        .w_PERIVA = IIF(NOT EMPTY(.w_CODIVA),0,.w_PERIVA)
        .w_FLAGZERO = IIF(NOT EMPTY(.w_CODIVA) OR .w_PERIVA<>0,'N','S')
        .w_IVFLGBSV = 'T'
        .w_IVFLGBSA = 'T'
        .w_TOTDOC = 'M'
        .w_FILTIMPO = 'M'
        .w_DAIMPO = 0
        .w_AIMPO = 0
        .w_OPERVALO = 'T'
        .w_OPERVERI = 'T'
        .w_OPGENERA = 'T'
        .DoRTCalc(64,64,.f.)
        if not(empty(.w_UTEINSER))
          .link_1_64('Full')
        endif
        .DoRTCalc(65,67,.f.)
        if not(empty(.w_UTEVARIA))
          .link_1_67('Full')
        endif
          .DoRTCalc(68,73,.f.)
        .w_DESUTE = IIF(.w_UTEINSER<>0,.w_DESUTE,'Tutti')
        .w_DESUTE1 = IIF(.w_UTEVARIA<>0,.w_DESUTE1,'Tutti')
      .oPgFrm.Page2.oPag.ZOOMVP.Calculate()
        .w_SERIALE = NVL(.w_ZOOMVP.getVar('PNSERIAL'), ' ')
        .w_OSTIPOPE = 'X'
        .w_CHKRIFCON = 'N'
        .w_OSRIFCON = Space(30)
        .w_OSTIPFAT = 'X'
        .w_OSIMPFRA = IIF(.w_OSTIPFAT='S','S','X')
        .w_OSFLGEXT = 'X'
        .w_OSFLGDVE = 'X'
        .w_OSRIFFAT = NVL(.w_ZOOMVP.getVar('OSRIFFAT'), ' ')
        .w_CCDESCRI = NVL(.w_ZOOMVP.getVar('CCDESCRI'), ' ')
        .w_ANDESCRI = NVL(.w_ZOOMVP.getVar('ANDESCRI'), ' ')
        .w_TIPFAT = NVL(.w_ZOOMVP.getVar('TIPFAT'), ' ')
        .w_PNTIPCLF = NVL(.w_ZOOMVP.getVar('PNTIPCLF'), ' ')
        .w_PNCODCLF = NVL(.w_ZOOMVP.getVar('PNCODCLF'), ' ')
        .w_FLGEXT = NVL(.w_ZOOMVP.getVar('FLGEXT'), ' ')
        .w_TIPRET = NVL(.w_ZOOMVP.getVar('TIPRET'), ' ')
        .w_TIPOPE = NVL(.w_ZOOMVP.getVar('TIPOPE'), ' ')
        .w_OSRIFACC = NVL(.w_ZOOMVP.getVar('OSRIFACC'), ' ')
        .w_RIFMOVCOL = IIF(.w_TIPFAT='N',.w_OSRIFFAT,IIF(.w_TIPFAT='A',.w_OSRIFACC,' '))
        .w_TIPACC = NVL(.w_ZOOMVP.getVar('TIPACC'), ' ')
        .w_DATREG = Cp_Todate(.w_ZOOMVP.getVar('PNDATREG'))
        .w_AZIENDA = i_CODAZI
        .w_PNCODVAL = NVL(.w_ZOOMVP.getVar('PNCODVAL'), ' ')
        .w_PNNUMRER = NVL(.w_ZOOMVP.getVar('PNNUMRER'), ' ')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_24.enabled = this.oPgFrm.Page2.oPag.oBtn_2_24.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_25.enabled = this.oPgFrm.Page2.oPag.oBtn_2_25.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_29.enabled = this.oPgFrm.Page2.oPag.oBtn_2_29.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_30.enabled = this.oPgFrm.Page2.oPag.oBtn_2_30.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_31.enabled = this.oPgFrm.Page2.oPag.oBtn_2_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_33.enabled = this.oPgFrm.Page2.oPag.oBtn_2_33.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,14,.t.)
        if .o_CAUSA<>.w_CAUSA
            .w_TIPDOC = IIF(NOT EMPTY(.w_CAUSA),'TT',IIF(EMPTY(.w_TIPDOC),'TT',.w_TIPDOC))
        endif
        if .o_CAUSA<>.w_CAUSA
            .w_TIPDOC = IIF(NOT EMPTY(.w_CAUSA),'TT',IIF(EMPTY(.w_TIPDOC),'TT',.w_TIPDOC))
        endif
        if .o_TIPDOC<>.w_TIPDOC
            .w_ESCDOCINTRA = IIF(.w_TIPDOC='FE' Or .w_TIPDOC='NE','N','S')
        endif
        .DoRTCalc(18,34,.t.)
        if .o_TIPCONEFF<>.w_TIPCONEFF
            .w_TIPCON = IIF(.w_TIPCONEFF $ 'CI','C','F')
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF.or. .o_CODCON<>.w_CODCON
            .w_TITIVA = 'T'
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF.or. .o_CODCON<>.w_CODCON
            .w_PERSFIS = IIF(NOT EMPTY(.w_CODCON) OR .w_TIPCONEFF $ 'IZ',' ',.w_PERSFIS)
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF.or. .o_CODCON<>.w_CODCON
            .w_ZONA = IIF(NOT EMPTY(.w_CODCON) Or .w_TIPCONEFF $ 'IZ',space(3),.w_ZONA)
          .link_1_38('Full')
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF.or. .o_CODCON<>.w_CODCON
            .w_NAZIONE = IIF(NOT EMPTY(.w_CODCON) Or .w_TIPCONEFF $ 'IZ',space(3),.w_NAZIONE)
          .link_1_39('Full')
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF
            .w_CODCON = space(15)
          .link_1_40('Full')
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF
            .w_CODCON = space(15)
          .link_1_41('Full')
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF.or. .o_CODCON<>.w_CODCON
            .w_CONSUP = SPACE(15)
          .link_1_42('Full')
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF.or. .o_CODCON<>.w_CODCON
            .w_CONSUP = SPACE(15)
          .link_1_43('Full')
        endif
        .DoRTCalc(44,44,.t.)
        if .o_TIPCONEFF<>.w_TIPCONEFF
            .w_ANOPETRE = 'T'
        endif
        if .o_TIPCONEFF<>.w_TIPCONEFF.or. .o_CODCON<>.w_CODCON
            .w_ANTIPPRE = 'T'
        endif
        .DoRTCalc(47,52,.t.)
        if .o_CODIVA<>.w_CODIVA
            .w_PERIVA = IIF(NOT EMPTY(.w_CODIVA),0,.w_PERIVA)
        endif
        if .o_CODIVA<>.w_CODIVA.or. .o_PERIVA<>.w_PERIVA
            .w_FLAGZERO = IIF(NOT EMPTY(.w_CODIVA) OR .w_PERIVA<>0,'N','S')
        endif
        .DoRTCalc(55,58,.t.)
        if .o_FILTIMPO<>.w_FILTIMPO
            .w_DAIMPO = 0
        endif
        if .o_FILTIMPO<>.w_FILTIMPO
            .w_AIMPO = 0
        endif
        .DoRTCalc(61,73,.t.)
        if .o_UTEINSER<>.w_UTEINSER
            .w_DESUTE = IIF(.w_UTEINSER<>0,.w_DESUTE,'Tutti')
        endif
        if .o_UTEVARIA<>.w_UTEVARIA
            .w_DESUTE1 = IIF(.w_UTEVARIA<>0,.w_DESUTE1,'Tutti')
        endif
        .oPgFrm.Page2.oPag.ZOOMVP.Calculate()
            .w_SERIALE = NVL(.w_ZOOMVP.getVar('PNSERIAL'), ' ')
        .DoRTCalc(77,77,.t.)
        if .o_OSTIPOPE<>.w_OSTIPOPE
            .w_CHKRIFCON = 'N'
        endif
        if .o_CHKRIFCON<>.w_CHKRIFCON
            .w_OSRIFCON = Space(30)
        endif
        .DoRTCalc(80,80,.t.)
        if .o_OSTIPFAT<>.w_OSTIPFAT
            .w_OSIMPFRA = IIF(.w_OSTIPFAT='S','S','X')
        endif
        .DoRTCalc(82,83,.t.)
            .w_OSRIFFAT = NVL(.w_ZOOMVP.getVar('OSRIFFAT'), ' ')
            .w_CCDESCRI = NVL(.w_ZOOMVP.getVar('CCDESCRI'), ' ')
            .w_ANDESCRI = NVL(.w_ZOOMVP.getVar('ANDESCRI'), ' ')
            .w_TIPFAT = NVL(.w_ZOOMVP.getVar('TIPFAT'), ' ')
            .w_PNTIPCLF = NVL(.w_ZOOMVP.getVar('PNTIPCLF'), ' ')
            .w_PNCODCLF = NVL(.w_ZOOMVP.getVar('PNCODCLF'), ' ')
            .w_FLGEXT = NVL(.w_ZOOMVP.getVar('FLGEXT'), ' ')
        if .o_OSFLGEXT<>.w_OSFLGEXT
          .Calculate_RTMBHBTFKJ()
        endif
            .w_TIPRET = NVL(.w_ZOOMVP.getVar('TIPRET'), ' ')
            .w_TIPOPE = NVL(.w_ZOOMVP.getVar('TIPOPE'), ' ')
            .w_OSRIFACC = NVL(.w_ZOOMVP.getVar('OSRIFACC'), ' ')
            .w_RIFMOVCOL = IIF(.w_TIPFAT='N',.w_OSRIFFAT,IIF(.w_TIPFAT='A',.w_OSRIFACC,' '))
            .w_TIPACC = NVL(.w_ZOOMVP.getVar('TIPACC'), ' ')
            .w_DATREG = Cp_Todate(.w_ZOOMVP.getVar('PNDATREG'))
        .DoRTCalc(97,97,.t.)
            .w_PNCODVAL = NVL(.w_ZOOMVP.getVar('PNCODVAL'), ' ')
            .w_PNNUMRER = NVL(.w_ZOOMVP.getVar('PNNUMRER'), ' ')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOMVP.Calculate()
    endwith
  return

  proc Calculate_RTMBHBTFKJ()
    with this
          * --- Aggiornamento variabili 
          .w_OSTIPOPE = IIF(.w_OSFLGEXT='F','X',.w_OSTIPOPE)
          .w_OSTIPFAT = IIF(.w_OSFLGEXT='F','X',.w_OSTIPFAT)
          .w_OSIMPFRA = IIF(.w_OSFLGEXT='F','X',.w_OSIMPFRA)
    endwith
  endproc
  proc Calculate_DRNNBNCRHR()
    with this
          * --- ActivatePage 1
          .w_OSTIPOPE = 'X'
          .w_OSTIPFAT = 'X'
          .w_OSFLGEXT = 'X'
          .w_OSFLGDVE = 'X'
          .w_CHKRIFCON = 'N'
          .w_OSRIFCON = SPACE(30)
          .w_CCDESCRI = SPACE(35)
          .w_ANDESCRI = SPACE(40)
          .w_OSIMPFRA = 'X'
    endwith
  endproc
  proc Calculate_XZSANNKROM()
    with this
          * --- ActivatePage 2
          GSAI_BZP(this;
              ,'P';
             )
    endwith
  endproc
  proc Calculate_LDQCSKVJLX()
    with this
          * --- Opervalo Changed
          .w_OPERVERI = IIF(.w_OPERVALO='N','T',.w_OPERVERI)
          .w_OPGENERA = IIF(.w_OPERVALO='N','T',.w_OPGENERA)
    endwith
  endproc
  proc Calculate_UWIILGXDLB()
    with this
          * --- Codcon Changed
          .w_ANOPETRE = IIF(EMPTY(.w_ANOPETRE) Or .w_TIPCONEFF $'IZ','T',.w_ANOPETRE)
    endwith
  endproc
  proc Calculate_RISMYXNUEW()
    with this
          * --- Codiva Changed
          .w_IVFLGBSV = IIF(EMPTY(.w_IVFLGBSV),'T',.w_IVFLGBSV)
          .w_IVFLGBSA = IIF(EMPTY(.w_IVFLGBSA),'T',.w_IVFLGBSA)
    endwith
  endproc
  proc Calculate_OVBBJINCRT()
    with this
          * --- Calcolo importo di filtro
          Gsai_Bzp(this;
              ,'N';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_15.enabled = this.oPgFrm.Page1.oPag.oTIPDOC_1_15.mCond()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_16.enabled = this.oPgFrm.Page1.oPag.oTIPDOC_1_16.mCond()
    this.oPgFrm.Page1.oPag.oESCDOCINTRA_1_17.enabled = this.oPgFrm.Page1.oPag.oESCDOCINTRA_1_17.mCond()
    this.oPgFrm.Page1.oPag.oTITIVA_1_36.enabled = this.oPgFrm.Page1.oPag.oTITIVA_1_36.mCond()
    this.oPgFrm.Page1.oPag.oPERSFIS_1_37.enabled = this.oPgFrm.Page1.oPag.oPERSFIS_1_37.mCond()
    this.oPgFrm.Page1.oPag.oZONA_1_38.enabled = this.oPgFrm.Page1.oPag.oZONA_1_38.mCond()
    this.oPgFrm.Page1.oPag.oNAZIONE_1_39.enabled = this.oPgFrm.Page1.oPag.oNAZIONE_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCONSUP_1_42.enabled = this.oPgFrm.Page1.oPag.oCONSUP_1_42.mCond()
    this.oPgFrm.Page1.oPag.oCONSUP_1_43.enabled = this.oPgFrm.Page1.oPag.oCONSUP_1_43.mCond()
    this.oPgFrm.Page1.oPag.oANOPETRE_1_45.enabled = this.oPgFrm.Page1.oPag.oANOPETRE_1_45.mCond()
    this.oPgFrm.Page1.oPag.oANTIPPRE_1_46.enabled = this.oPgFrm.Page1.oPag.oANTIPPRE_1_46.mCond()
    this.oPgFrm.Page1.oPag.oPERIVA_1_53.enabled = this.oPgFrm.Page1.oPag.oPERIVA_1_53.mCond()
    this.oPgFrm.Page1.oPag.oFLAGZERO_1_54.enabled = this.oPgFrm.Page1.oPag.oFLAGZERO_1_54.mCond()
    this.oPgFrm.Page1.oPag.oIVFLGBSV_1_55.enabled = this.oPgFrm.Page1.oPag.oIVFLGBSV_1_55.mCond()
    this.oPgFrm.Page1.oPag.oIVFLGBSA_1_56.enabled = this.oPgFrm.Page1.oPag.oIVFLGBSA_1_56.mCond()
    this.oPgFrm.Page1.oPag.oOPERVERI_1_62.enabled = this.oPgFrm.Page1.oPag.oOPERVERI_1_62.mCond()
    this.oPgFrm.Page1.oPag.oOPGENERA_1_63.enabled = this.oPgFrm.Page1.oPag.oOPGENERA_1_63.mCond()
    this.oPgFrm.Page2.oPag.oOSTIPOPE_2_5.enabled = this.oPgFrm.Page2.oPag.oOSTIPOPE_2_5.mCond()
    this.oPgFrm.Page2.oPag.oCHKRIFCON_2_6.enabled = this.oPgFrm.Page2.oPag.oCHKRIFCON_2_6.mCond()
    this.oPgFrm.Page2.oPag.oOSRIFCON_2_7.enabled = this.oPgFrm.Page2.oPag.oOSRIFCON_2_7.mCond()
    this.oPgFrm.Page2.oPag.oOSTIPFAT_2_8.enabled = this.oPgFrm.Page2.oPag.oOSTIPFAT_2_8.mCond()
    this.oPgFrm.Page2.oPag.oOSIMPFRA_2_9.enabled = this.oPgFrm.Page2.oPag.oOSIMPFRA_2_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_24.enabled = this.oPgFrm.Page2.oPag.oBtn_2_24.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_33.enabled = this.oPgFrm.Page2.oPag.oBtn_2_33.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_15.visible=!this.oPgFrm.Page1.oPag.oTIPDOC_1_15.mHide()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_16.visible=!this.oPgFrm.Page1.oPag.oTIPDOC_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDATAD1_1_18.visible=!this.oPgFrm.Page1.oPag.oDATAD1_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDATAD1_1_19.visible=!this.oPgFrm.Page1.oPag.oDATAD1_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDATAD2_1_20.visible=!this.oPgFrm.Page1.oPag.oDATAD2_1_20.mHide()
    this.oPgFrm.Page1.oPag.oDATAD2_1_21.visible=!this.oPgFrm.Page1.oPag.oDATAD2_1_21.mHide()
    this.oPgFrm.Page1.oPag.oSERIE_1_22.visible=!this.oPgFrm.Page1.oPag.oSERIE_1_22.mHide()
    this.oPgFrm.Page1.oPag.oSERIE_1_23.visible=!this.oPgFrm.Page1.oPag.oSERIE_1_23.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_24.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_24.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC1_1_25.visible=!this.oPgFrm.Page1.oPag.oNUMDOC1_1_25.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_26.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_26.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC1_1_27.visible=!this.oPgFrm.Page1.oPag.oNUMDOC1_1_27.mHide()
    this.oPgFrm.Page1.oPag.oSERIEP_1_28.visible=!this.oPgFrm.Page1.oPag.oSERIEP_1_28.mHide()
    this.oPgFrm.Page1.oPag.oSERIEP_1_29.visible=!this.oPgFrm.Page1.oPag.oSERIEP_1_29.mHide()
    this.oPgFrm.Page1.oPag.oNUMPRO_1_30.visible=!this.oPgFrm.Page1.oPag.oNUMPRO_1_30.mHide()
    this.oPgFrm.Page1.oPag.oNUMPRO1_1_31.visible=!this.oPgFrm.Page1.oPag.oNUMPRO1_1_31.mHide()
    this.oPgFrm.Page1.oPag.oNUMPRO_1_32.visible=!this.oPgFrm.Page1.oPag.oNUMPRO_1_32.mHide()
    this.oPgFrm.Page1.oPag.oNUMPRO1_1_33.visible=!this.oPgFrm.Page1.oPag.oNUMPRO1_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCODCON_1_40.visible=!this.oPgFrm.Page1.oPag.oCODCON_1_40.mHide()
    this.oPgFrm.Page1.oPag.oCODCON_1_41.visible=!this.oPgFrm.Page1.oPag.oCODCON_1_41.mHide()
    this.oPgFrm.Page1.oPag.oCONSUP_1_42.visible=!this.oPgFrm.Page1.oPag.oCONSUP_1_42.mHide()
    this.oPgFrm.Page1.oPag.oCONSUP_1_43.visible=!this.oPgFrm.Page1.oPag.oCONSUP_1_43.mHide()
    this.oPgFrm.Page1.oPag.oDAIMPO_1_59.visible=!this.oPgFrm.Page1.oPag.oDAIMPO_1_59.mHide()
    this.oPgFrm.Page1.oPag.oAIMPO_1_60.visible=!this.oPgFrm.Page1.oPag.oAIMPO_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_121.visible=!this.oPgFrm.Page1.oPag.oStr_1_121.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_122.visible=!this.oPgFrm.Page1.oPag.oStr_1_122.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_125.visible=!this.oPgFrm.Page1.oPag.oStr_1_125.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_133.visible=!this.oPgFrm.Page1.oPag.oStr_1_133.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_135.visible=!this.oPgFrm.Page1.oPag.oStr_1_135.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_136.visible=!this.oPgFrm.Page1.oPag.oStr_1_136.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_137.visible=!this.oPgFrm.Page1.oPag.oStr_1_137.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_138.visible=!this.oPgFrm.Page1.oPag.oStr_1_138.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_140.visible=!this.oPgFrm.Page1.oPag.oStr_1_140.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_143.visible=!this.oPgFrm.Page1.oPag.oStr_1_143.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_144.visible=!this.oPgFrm.Page1.oPag.oStr_1_144.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_145.visible=!this.oPgFrm.Page1.oPag.oStr_1_145.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_146.visible=!this.oPgFrm.Page1.oPag.oStr_1_146.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_147.visible=!this.oPgFrm.Page1.oPag.oStr_1_147.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_148.visible=!this.oPgFrm.Page1.oPag.oStr_1_148.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZOOMVP.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 1")
          .Calculate_DRNNBNCRHR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_XZSANNKROM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_OPERVALO Changed")
          .Calculate_LDQCSKVJLX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CODCON Changed")
          .Calculate_UWIILGXDLB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CODIVA Changed")
          .Calculate_RISMYXNUEW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_OVBBJINCRT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
      this.w_INESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALUTA))
          select VACODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALUTA_1_8'),i_cWhere,'GSAR_AVL',"DIVISE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSA
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUSA)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCTIPDOC,CCFLRIFE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUSA))
          select CCCODICE,CCDESCRI,CCTIPREG,CCTIPDOC,CCFLRIFE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSA)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUSA) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUSA_1_11'),i_cWhere,'GSCG_ACC',"CAUSALI CONTABILI",'GSAI_KOS.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCTIPDOC,CCFLRIFE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCTIPDOC,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCTIPDOC,CCFLRIFE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUSA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUSA)
            select CCCODICE,CCDESCRI,CCTIPREG,CCTIPDOC,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSA = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCRICA = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_CCTIPDOC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_CCFLRIFE = NVL(_Link_.CCFLRIFE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSA = space(5)
      endif
      this.w_DESCRICA = space(35)
      this.w_TIPREG = space(1)
      this.w_CCTIPDOC = space(2)
      this.w_CCFLRIFE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPREG<>'N' AND .w_CCTIPDOC<>'NO' AND .w_CCFLRIFE<>'N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, no IVA, non intestata oppure con tipologia documento non congruente")
        endif
        this.w_CAUSA = space(5)
        this.w_DESCRICA = space(35)
        this.w_TIPREG = space(1)
        this.w_CCTIPDOC = space(2)
        this.w_CCFLRIFE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZONA
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZONA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_ZONA)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_ZONA))
          select ZOCODZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZONA)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ZONA) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oZONA_1_38'),i_cWhere,'GSAR_AZO',"ZONE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZONA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_ZONA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_ZONA)
            select ZOCODZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZONA = NVL(_Link_.ZOCODZON,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ZONA = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZONA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NAZIONE
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NAZIONE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_NAZIONE)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_NAZIONE))
          select NACODNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NAZIONE)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NAZIONE) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oNAZIONE_1_39'),i_cWhere,'GSAR_ANZ',"NAZIONI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NAZIONE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_NAZIONE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_NAZIONE)
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NAZIONE = NVL(_Link_.NACODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NAZIONE = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NAZIONE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_40'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'GSCG_SOI.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente ")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(35))
      this.w_ANOPETRE = NVL(_Link_.ANOPETRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_ANOPETRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_41'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente ")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(35))
      this.w_ANOPETRE = NVL(_Link_.ANOPETRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_ANOPETRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSUP
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSUP))
          select MCCODICE,MCDESCRI,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CONSUP)+"%");

            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSUP_1_42'),i_cWhere,'GSAR_AMC',"MASTRI",'GSCG_SOI.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSUP)
            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(35))
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONSUP = space(15)
      endif
      this.w_DESMAS = space(35)
      this.w_TIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPMAS=.w_TIPCON
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Mastro contabile inesistente")
        endif
        this.w_CONSUP = space(15)
        this.w_DESMAS = space(35)
        this.w_TIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSUP
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSUP))
          select MCCODICE,MCDESCRI,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CONSUP)+"%");

            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSUP_1_43'),i_cWhere,'GSAR_AMC',"MASTRI",'gsmambrb.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSUP)
            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(35))
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONSUP = space(15)
      endif
      this.w_DESMAS = space(35)
      this.w_TIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPMAS=.w_TIPCON
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Mastro contabile inesistente")
        endif
        this.w_CONSUP = space(15)
        this.w_DESMAS = space(35)
        this.w_TIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVFLGBSV,IVFLGBSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CODIVA))
          select IVCODIVA,IVDESIVA,IVFLGBSV,IVFLGBSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVFLGBSV,IVFLGBSA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_CODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVFLGBSV,IVFLGBSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCODIVA_1_52'),i_cWhere,'GSAR_AIV',"CODICI IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVFLGBSV,IVFLGBSA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVFLGBSV,IVFLGBSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVFLGBSV,IVFLGBSA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA,IVFLGBSV,IVFLGBSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_IVFLGBSV = NVL(_Link_.IVFLGBSV,space(1))
      this.w_IVFLGBSA = NVL(_Link_.IVFLGBSA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_IVFLGBSV = space(1)
      this.w_IVFLGBSA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTEINSER
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTEINSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UTEINSER);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UTEINSER)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTEINSER) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oUTEINSER_1_64'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTEINSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTEINSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTEINSER)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTEINSER = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTEINSER = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTEINSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTEVARIA
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTEVARIA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UTEVARIA);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UTEVARIA)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTEVARIA) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oUTEVARIA_1_67'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTEVARIA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTEVARIA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTEVARIA)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTEVARIA = NVL(_Link_.CODE,0)
      this.w_DESUTE1 = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTEVARIA = 0
      endif
      this.w_DESUTE1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTEVARIA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_5.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_5.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_6.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_6.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVVI_1_7.RadioValue()==this.w_PROVVI)
      this.oPgFrm.Page1.oPag.oPROVVI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_8.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_8.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE_1_9.value==this.w_MESE)
      this.oPgFrm.Page1.oPag.oMESE_1_9.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_10.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_10.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSA_1_11.value==this.w_CAUSA)
      this.oPgFrm.Page1.oPag.oCAUSA_1_11.value=this.w_CAUSA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_15.RadioValue()==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_16.RadioValue()==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESCDOCINTRA_1_17.RadioValue()==this.w_ESCDOCINTRA)
      this.oPgFrm.Page1.oPag.oESCDOCINTRA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAD1_1_18.value==this.w_DATAD1)
      this.oPgFrm.Page1.oPag.oDATAD1_1_18.value=this.w_DATAD1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAD1_1_19.value==this.w_DATAD1)
      this.oPgFrm.Page1.oPag.oDATAD1_1_19.value=this.w_DATAD1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAD2_1_20.value==this.w_DATAD2)
      this.oPgFrm.Page1.oPag.oDATAD2_1_20.value=this.w_DATAD2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAD2_1_21.value==this.w_DATAD2)
      this.oPgFrm.Page1.oPag.oDATAD2_1_21.value=this.w_DATAD2
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE_1_22.value==this.w_SERIE)
      this.oPgFrm.Page1.oPag.oSERIE_1_22.value=this.w_SERIE
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE_1_23.value==this.w_SERIE)
      this.oPgFrm.Page1.oPag.oSERIE_1_23.value=this.w_SERIE
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_24.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_24.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC1_1_25.value==this.w_NUMDOC1)
      this.oPgFrm.Page1.oPag.oNUMDOC1_1_25.value=this.w_NUMDOC1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_26.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_26.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC1_1_27.value==this.w_NUMDOC1)
      this.oPgFrm.Page1.oPag.oNUMDOC1_1_27.value=this.w_NUMDOC1
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIEP_1_28.value==this.w_SERIEP)
      this.oPgFrm.Page1.oPag.oSERIEP_1_28.value=this.w_SERIEP
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIEP_1_29.value==this.w_SERIEP)
      this.oPgFrm.Page1.oPag.oSERIEP_1_29.value=this.w_SERIEP
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPRO_1_30.value==this.w_NUMPRO)
      this.oPgFrm.Page1.oPag.oNUMPRO_1_30.value=this.w_NUMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPRO1_1_31.value==this.w_NUMPRO1)
      this.oPgFrm.Page1.oPag.oNUMPRO1_1_31.value=this.w_NUMPRO1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPRO_1_32.value==this.w_NUMPRO)
      this.oPgFrm.Page1.oPag.oNUMPRO_1_32.value=this.w_NUMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPRO1_1_33.value==this.w_NUMPRO1)
      this.oPgFrm.Page1.oPag.oNUMPRO1_1_33.value=this.w_NUMPRO1
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCONEFF_1_34.RadioValue()==this.w_TIPCONEFF)
      this.oPgFrm.Page1.oPag.oTIPCONEFF_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTITIVA_1_36.RadioValue()==this.w_TITIVA)
      this.oPgFrm.Page1.oPag.oTITIVA_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERSFIS_1_37.RadioValue()==this.w_PERSFIS)
      this.oPgFrm.Page1.oPag.oPERSFIS_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZONA_1_38.value==this.w_ZONA)
      this.oPgFrm.Page1.oPag.oZONA_1_38.value=this.w_ZONA
    endif
    if not(this.oPgFrm.Page1.oPag.oNAZIONE_1_39.value==this.w_NAZIONE)
      this.oPgFrm.Page1.oPag.oNAZIONE_1_39.value=this.w_NAZIONE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_40.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_40.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_41.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_41.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSUP_1_42.value==this.w_CONSUP)
      this.oPgFrm.Page1.oPag.oCONSUP_1_42.value=this.w_CONSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSUP_1_43.value==this.w_CONSUP)
      this.oPgFrm.Page1.oPag.oCONSUP_1_43.value=this.w_CONSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oANOPETRE_1_45.RadioValue()==this.w_ANOPETRE)
      this.oPgFrm.Page1.oPag.oANOPETRE_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANTIPPRE_1_46.RadioValue()==this.w_ANTIPPRE)
      this.oPgFrm.Page1.oPag.oANTIPPRE_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREGIVA_1_47.RadioValue()==this.w_REGIVA)
      this.oPgFrm.Page1.oPag.oREGIVA_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMIVA_1_48.value==this.w_NUMIVA)
      this.oPgFrm.Page1.oPag.oNUMIVA_1_48.value=this.w_NUMIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMIVA1_1_49.value==this.w_NUMIVA1)
      this.oPgFrm.Page1.oPag.oNUMIVA1_1_49.value=this.w_NUMIVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAIVA1_1_50.value==this.w_DATAIVA1)
      this.oPgFrm.Page1.oPag.oDATAIVA1_1_50.value=this.w_DATAIVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAIVA2_1_51.value==this.w_DATAIVA2)
      this.oPgFrm.Page1.oPag.oDATAIVA2_1_51.value=this.w_DATAIVA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_52.value==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_52.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIVA_1_53.value==this.w_PERIVA)
      this.oPgFrm.Page1.oPag.oPERIVA_1_53.value=this.w_PERIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGZERO_1_54.RadioValue()==this.w_FLAGZERO)
      this.oPgFrm.Page1.oPag.oFLAGZERO_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVFLGBSV_1_55.RadioValue()==this.w_IVFLGBSV)
      this.oPgFrm.Page1.oPag.oIVFLGBSV_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVFLGBSA_1_56.RadioValue()==this.w_IVFLGBSA)
      this.oPgFrm.Page1.oPag.oIVFLGBSA_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDOC_1_57.RadioValue()==this.w_TOTDOC)
      this.oPgFrm.Page1.oPag.oTOTDOC_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILTIMPO_1_58.RadioValue()==this.w_FILTIMPO)
      this.oPgFrm.Page1.oPag.oFILTIMPO_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAIMPO_1_59.value==this.w_DAIMPO)
      this.oPgFrm.Page1.oPag.oDAIMPO_1_59.value=this.w_DAIMPO
    endif
    if not(this.oPgFrm.Page1.oPag.oAIMPO_1_60.value==this.w_AIMPO)
      this.oPgFrm.Page1.oPag.oAIMPO_1_60.value=this.w_AIMPO
    endif
    if not(this.oPgFrm.Page1.oPag.oOPERVALO_1_61.RadioValue()==this.w_OPERVALO)
      this.oPgFrm.Page1.oPag.oOPERVALO_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOPERVERI_1_62.RadioValue()==this.w_OPERVERI)
      this.oPgFrm.Page1.oPag.oOPERVERI_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOPGENERA_1_63.RadioValue()==this.w_OPGENERA)
      this.oPgFrm.Page1.oPag.oOPGENERA_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEINSER_1_64.value==this.w_UTEINSER)
      this.oPgFrm.Page1.oPag.oUTEINSER_1_64.value=this.w_UTEINSER
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINSERI_1_65.value==this.w_DATINSERI)
      this.oPgFrm.Page1.oPag.oDATINSERI_1_65.value=this.w_DATINSERI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINSERF_1_66.value==this.w_DATINSERF)
      this.oPgFrm.Page1.oPag.oDATINSERF_1_66.value=this.w_DATINSERF
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEVARIA_1_67.value==this.w_UTEVARIA)
      this.oPgFrm.Page1.oPag.oUTEVARIA_1_67.value=this.w_UTEVARIA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVARIAI_1_68.value==this.w_DATVARIAI)
      this.oPgFrm.Page1.oPag.oDATVARIAI_1_68.value=this.w_DATVARIAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVARIAF_1_69.value==this.w_DATVARIAF)
      this.oPgFrm.Page1.oPag.oDATVARIAF_1_69.value=this.w_DATVARIAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRICA_1_75.value==this.w_DESCRICA)
      this.oPgFrm.Page1.oPag.oDESCRICA_1_75.value=this.w_DESCRICA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_91.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_91.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAS_1_96.value==this.w_DESMAS)
      this.oPgFrm.Page1.oPag.oDESMAS_1_96.value=this.w_DESMAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_99.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_99.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_111.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_111.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE1_1_114.value==this.w_DESUTE1)
      this.oPgFrm.Page1.oPag.oDESUTE1_1_114.value=this.w_DESUTE1
    endif
    if not(this.oPgFrm.Page2.oPag.oOSTIPOPE_2_5.RadioValue()==this.w_OSTIPOPE)
      this.oPgFrm.Page2.oPag.oOSTIPOPE_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHKRIFCON_2_6.RadioValue()==this.w_CHKRIFCON)
      this.oPgFrm.Page2.oPag.oCHKRIFCON_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOSRIFCON_2_7.value==this.w_OSRIFCON)
      this.oPgFrm.Page2.oPag.oOSRIFCON_2_7.value=this.w_OSRIFCON
    endif
    if not(this.oPgFrm.Page2.oPag.oOSTIPFAT_2_8.RadioValue()==this.w_OSTIPFAT)
      this.oPgFrm.Page2.oPag.oOSTIPFAT_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOSIMPFRA_2_9.RadioValue()==this.w_OSIMPFRA)
      this.oPgFrm.Page2.oPag.oOSIMPFRA_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOSFLGEXT_2_10.RadioValue()==this.w_OSFLGEXT)
      this.oPgFrm.Page2.oPag.oOSFLGEXT_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOSFLGDVE_2_11.RadioValue()==this.w_OSFLGDVE)
      this.oPgFrm.Page2.oPag.oOSFLGDVE_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESCRI_2_15.value==this.w_CCDESCRI)
      this.oPgFrm.Page2.oPag.oCCDESCRI_2_15.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oANDESCRI_2_17.value==this.w_ANDESCRI)
      this.oPgFrm.Page2.oPag.oANDESCRI_2_17.value=this.w_ANDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_data1<=.w_data2 or (empty(.w_DATA2))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not(.w_DATA1<=.w_DATA2 or (empty(.w_DATA1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not((.w_MESE>=1 and .w_MESE<=12) OR .w_MESE=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESE_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di riferimento: selezionare un mese compreso tra 1 e 12")
          case   not((.w_ANNO>=1901 and .w_ANNO<=2099) OR .w_ANNO=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Anno Solare: selezionare un anno compreso tra 1901 e 2099")
          case   not(.w_TIPREG<>'N' AND .w_CCTIPDOC<>'NO' AND .w_CCFLRIFE<>'N')  and not(empty(.w_CAUSA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUSA_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, no IVA, non intestata oppure con tipologia documento non congruente")
          case   not((.w_DATAD1<=.w_DATAD2 or (empty(.w_DATAD2))))  and not(UPPER (g_APPLICATION)="ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAD1_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not((.w_DATAD1<=.w_DATAD2 or (empty(.w_DATAD2))))  and not(UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAD1_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not(.w_DATAD1<=.w_DATAD2 or (empty(.w_DATAD1)))  and not(UPPER (g_APPLICATION)="ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAD2_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not(.w_DATAD1<=.w_DATAD2 or (empty(.w_DATAD1)))  and not(UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAD2_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not(.w_NUMDOC1>=.w_NUMDOC or .w_NUMDOC1 = 0)  and not(UPPER (g_APPLICATION)="ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMDOC_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri documento incongruenti")
          case   not(.w_NUMDOC1>=.w_NUMDOC)  and not(UPPER (g_APPLICATION)="ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMDOC1_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri documento incongruenti")
          case   not(.w_NUMDOC1>=.w_NUMDOC or .w_NUMDOC1 = 0)  and not(UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMDOC_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri documento incongruenti")
          case   not(.w_NUMDOC1>=.w_NUMDOC)  and not(UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMDOC1_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri documento incongruenti")
          case   not(.w_NUMPRO1>=.w_NUMPRO OR .w_NUMPRO1=0)  and not(UPPER (g_APPLICATION)="ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPRO_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri protocollo incongruenti")
          case   not(.w_NUMPRO1>=.w_NUMPRO)  and not(UPPER (g_APPLICATION)="ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPRO1_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri protocollo incongruenti")
          case   not(.w_NUMPRO1>=.w_NUMPRO OR .w_NUMPRO1=0)  and not(UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPRO_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri protocollo incongruenti")
          case   not(.w_NUMPRO1>=.w_NUMPRO)  and not(UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPRO1_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri protocollo incongruenti")
          case   not(.w_TIPMAS=.w_TIPCON)  and not(UPPER (g_APPLICATION)="ADHOC REVOLUTION")  and (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')  and not(empty(.w_CONSUP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONSUP_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mastro contabile inesistente")
          case   not(.w_TIPMAS=.w_TIPCON)  and not(UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")  and (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')  and not(empty(.w_CONSUP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONSUP_1_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mastro contabile inesistente")
          case   not(.w_NUMIVA1>=.w_NUMIVA or .w_NUMIVA1 = 0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMIVA_1_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri registro IVA incongruenti")
          case   not(.w_NUMIVA1>=.w_NUMIVA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMIVA1_1_49.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numeri registro IVA incongruenti")
          case   not((.w_DATAIVA1<=.w_DATAIVA2 or (empty(.w_DATAIVA2))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAIVA1_1_50.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not(.w_DATAIVA1<=.w_DATAIVA2 or (empty(.w_DATAIVA1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAIVA2_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not((.w_DATINSERI<=.w_DATINSERF or (empty(.w_DATINSERF))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINSERI_1_65.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not(.w_DATINSERI<=.w_DATINSERF or (empty(.w_DATINSERI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINSERF_1_66.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not((.w_DATVARIAI<=.w_DATVARIAF or (empty(.w_DATVARIAF))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATVARIAI_1_68.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
          case   not(.w_DATVARIAI<=.w_DATVARIAF or (empty(.w_DATVARIAI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATVARIAF_1_69.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla data finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CAUSA = this.w_CAUSA
    this.o_TIPDOC = this.w_TIPDOC
    this.o_TIPCONEFF = this.w_TIPCONEFF
    this.o_CODCON = this.w_CODCON
    this.o_CODIVA = this.w_CODIVA
    this.o_PERIVA = this.w_PERIVA
    this.o_FILTIMPO = this.w_FILTIMPO
    this.o_OPERVALO = this.w_OPERVALO
    this.o_UTEINSER = this.w_UTEINSER
    this.o_UTEVARIA = this.w_UTEVARIA
    this.o_OSTIPOPE = this.w_OSTIPOPE
    this.o_CHKRIFCON = this.w_CHKRIFCON
    this.o_OSTIPFAT = this.w_OSTIPFAT
    this.o_OSFLGEXT = this.w_OSFLGEXT
    return

enddefine

* --- Define pages as container
define class tgsai_kosPag1 as StdContainer
  Width  = 802
  height = 555
  stdWidth  = 802
  stdheight = 555
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATA1_1_5 as StdField with uid="WCPTHGVVXN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data di registrazione iniziale",;
    HelpContextID = 110330314,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=74, Top=26

  func oDATA1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_data1<=.w_data2 or (empty(.w_DATA2))))
    endwith
    return bRes
  endfunc

  add object oDATA2_1_6 as StdField with uid="YEPEJUCQJU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data di registrazione finale",;
    HelpContextID = 109281738,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=193, Top=26

  func oDATA2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATA1<=.w_DATA2 or (empty(.w_DATA1)))
    endwith
    return bRes
  endfunc


  add object oPROVVI_1_7 as StdCombo with uid="EMLDBOXNIT",value=3,rtseq=7,rtrep=.f.,left=328,top=26,width=87,height=21;
    , ToolTipText = "Stato dei movimenti";
    , HelpContextID = 187613194;
    , cFormVar="w_PROVVI",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVVI_1_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,' ',;
    ' '))))
  endfunc
  func oPROVVI_1_7.GetRadio()
    this.Parent.oContained.w_PROVVI = this.RadioValue()
    return .t.
  endfunc

  func oPROVVI_1_7.SetRadio()
    this.Parent.oContained.w_PROVVI=trim(this.Parent.oContained.w_PROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVVI=='N',1,;
      iif(this.Parent.oContained.w_PROVVI=='S',2,;
      iif(this.Parent.oContained.w_PROVVI=='',3,;
      0)))
  endfunc

  add object oVALUTA_1_8 as StdField with uid="HHHFRPLLGJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice valuta inesistente",;
    ToolTipText = "Valuta",;
    HelpContextID = 55574698,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=497, Top=26, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALUTA_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALUTA_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALUTA_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"DIVISE",'',this.parent.oContained
  endproc
  proc oVALUTA_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALUTA
     i_obj.ecpSave()
  endproc

  add object oMESE_1_9 as StdField with uid="GVVKLVTRKW",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Mese di riferimento: selezionare un mese compreso tra 1 e 12",;
    ToolTipText = "Mese di riferimento dell'operazione",;
    HelpContextID = 161451322,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=604, Top=26, cSayPict='"@L"', cGetPict='"99"'

  func oMESE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_MESE>=1 and .w_MESE<=12) OR .w_MESE=0)
    endwith
    return bRes
  endfunc

  add object oANNO_1_10 as StdField with uid="SDDUVYUOGQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Anno Solare: selezionare un anno compreso tra 1901 e 2099",;
    ToolTipText = "Anno solare di riferimento dell'operazione",;
    HelpContextID = 160814330,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=688, Top=26, cSayPict='"9999"', cGetPict='"9999"'

  func oANNO_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ANNO>=1901 and .w_ANNO<=2099) OR .w_ANNO=0)
    endwith
    return bRes
  endfunc

  add object oCAUSA_1_11 as StdField with uid="IAVCMUXEKY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CAUSA", cQueryName = "CAUSA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, no IVA, non intestata oppure con tipologia documento non congruente",;
    ToolTipText = "Causale contabile",;
    HelpContextID = 92369370,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=74, Top=54, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUSA"

  func oCAUSA_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSA_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSA_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUSA_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"CAUSALI CONTABILI",'GSAI_KOS.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCAUSA_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUSA
     i_obj.ecpSave()
  endproc


  add object oTIPDOC_1_15 as StdCombo with uid="TKEEYIMRHG",rtseq=15,rtrep=.f.,left=497,top=54,width=123,height=21;
    , ToolTipText = "Se valorizzato filtro le registrazioni in base al tipo documento";
    , HelpContextID = 28358858;
    , cFormVar="w_TIPDOC",RowSource=""+"Fattura,"+"Nota di credito,"+"Fatt. corrispettivi,"+"Corrispettivi,"+"Fattura UE,"+"Nota di credito UE,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDOC_1_15.RadioValue()
    return(iif(this.value =1,'FA',;
    iif(this.value =2,'NC',;
    iif(this.value =3,'FC',;
    iif(this.value =4,'CO',;
    iif(this.value =5,'FE',;
    iif(this.value =6,'NE',;
    iif(this.value =7,'TT',;
    '  '))))))))
  endfunc
  func oTIPDOC_1_15.GetRadio()
    this.Parent.oContained.w_TIPDOC = this.RadioValue()
    return .t.
  endfunc

  func oTIPDOC_1_15.SetRadio()
    this.Parent.oContained.w_TIPDOC=trim(this.Parent.oContained.w_TIPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDOC=='FA',1,;
      iif(this.Parent.oContained.w_TIPDOC=='NC',2,;
      iif(this.Parent.oContained.w_TIPDOC=='FC',3,;
      iif(this.Parent.oContained.w_TIPDOC=='CO',4,;
      iif(this.Parent.oContained.w_TIPDOC=='FE',5,;
      iif(this.Parent.oContained.w_TIPDOC=='NE',6,;
      iif(this.Parent.oContained.w_TIPDOC=='TT',7,;
      0)))))))
  endfunc

  func oTIPDOC_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CAUSA))
    endwith
   endif
  endfunc

  func oTIPDOC_1_15.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc


  add object oTIPDOC_1_16 as StdCombo with uid="DYLYBVPVVG",rtseq=16,rtrep=.f.,left=497,top=54,width=123,height=21;
    , ToolTipText = "Se valorizzato filtro le registrazioni in base al tipo documento";
    , HelpContextID = 28358858;
    , cFormVar="w_TIPDOC",RowSource=""+"Fattura,"+"Nota di credito,"+"Fatt. corrispettivi,"+"Corrispettivi,"+"Fattura UE,"+"Nota di credito UE,"+"Fattura RC,"+"Nota credito RC,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDOC_1_16.RadioValue()
    return(iif(this.value =1,'FA',;
    iif(this.value =2,'NC',;
    iif(this.value =3,'FC',;
    iif(this.value =4,'CO',;
    iif(this.value =5,'FE',;
    iif(this.value =6,'NE',;
    iif(this.value =7,'AU',;
    iif(this.value =8,'NU',;
    iif(this.value =9,'TT',;
    '  '))))))))))
  endfunc
  func oTIPDOC_1_16.GetRadio()
    this.Parent.oContained.w_TIPDOC = this.RadioValue()
    return .t.
  endfunc

  func oTIPDOC_1_16.SetRadio()
    this.Parent.oContained.w_TIPDOC=trim(this.Parent.oContained.w_TIPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDOC=='FA',1,;
      iif(this.Parent.oContained.w_TIPDOC=='NC',2,;
      iif(this.Parent.oContained.w_TIPDOC=='FC',3,;
      iif(this.Parent.oContained.w_TIPDOC=='CO',4,;
      iif(this.Parent.oContained.w_TIPDOC=='FE',5,;
      iif(this.Parent.oContained.w_TIPDOC=='NE',6,;
      iif(this.Parent.oContained.w_TIPDOC=='AU',7,;
      iif(this.Parent.oContained.w_TIPDOC=='NU',8,;
      iif(this.Parent.oContained.w_TIPDOC=='TT',9,;
      0)))))))))
  endfunc

  func oTIPDOC_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CAUSA))
    endwith
   endif
  endfunc

  func oTIPDOC_1_16.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oESCDOCINTRA_1_17 as StdCheck with uid="HQUHRPNPHJ",rtseq=17,rtrep=.f.,left=629, top=57, caption="Escludi documenti INTRA",;
    ToolTipText = "Se attivato, non verranno  considerati i documenti INTRA",;
    HelpContextID = 240314324,;
    cFormVar="w_ESCDOCINTRA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESCDOCINTRA_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESCDOCINTRA_1_17.GetRadio()
    this.Parent.oContained.w_ESCDOCINTRA = this.RadioValue()
    return .t.
  endfunc

  func oESCDOCINTRA_1_17.SetRadio()
    this.Parent.oContained.w_ESCDOCINTRA=trim(this.Parent.oContained.w_ESCDOCINTRA)
    this.value = ;
      iif(this.Parent.oContained.w_ESCDOCINTRA=='S',1,;
      0)
  endfunc

  func oESCDOCINTRA_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDOC$'FA-NC-FC-CO-AU-NU-TT')
    endwith
   endif
  endfunc

  add object oDATAD1_1_18 as StdField with uid="ZYGCECBTHQ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATAD1", cQueryName = "DATAD1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data documento iniziale",;
    HelpContextID = 73630154,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=74, Top=105

  func oDATAD1_1_18.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oDATAD1_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATAD1<=.w_DATAD2 or (empty(.w_DATAD2))))
    endwith
    return bRes
  endfunc

  add object oDATAD1_1_19 as StdField with uid="ANKGIQJOKC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DATAD1", cQueryName = "DATAD1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data documento iniziale",;
    HelpContextID = 73630154,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=117, Top=105

  func oDATAD1_1_19.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oDATAD1_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATAD1<=.w_DATAD2 or (empty(.w_DATAD2))))
    endwith
    return bRes
  endfunc

  add object oDATAD2_1_20 as StdField with uid="JEVGMTVTGK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DATAD2", cQueryName = "DATAD2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data documento finale",;
    HelpContextID = 56852938,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=174, Top=105

  func oDATAD2_1_20.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oDATAD2_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAD1<=.w_DATAD2 or (empty(.w_DATAD1)))
    endwith
    return bRes
  endfunc

  add object oDATAD2_1_21 as StdField with uid="TNQEATPFYL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATAD2", cQueryName = "DATAD2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data documento finale",;
    HelpContextID = 56852938,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=117, Top=127

  func oDATAD2_1_21.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oDATAD2_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAD1<=.w_DATAD2 or (empty(.w_DATAD1)))
    endwith
    return bRes
  endfunc

  add object oSERIE_1_22 as StdField with uid="ZXAGJCFWLJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SERIE", cQueryName = "SERIE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 88841434,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=238, Top=105, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oSERIE_1_22.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oSERIE_1_23 as StdField with uid="NHEXZROUPI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SERIE", cQueryName = "SERIE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 88841434,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=291, Top=105, InputMask=replicate('X',2)

  func oSERIE_1_23.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oNUMDOC_1_24 as StdField with uid="MRWFKDKPQC",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri documento incongruenti",;
    ToolTipText = "Da numero documento (vuoto tutti)",;
    HelpContextID = 28368170,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=357, Top=105, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMDOC_1_24.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMDOC_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMDOC1>=.w_NUMDOC or .w_NUMDOC1 = 0)
    endwith
    return bRes
  endfunc

  add object oNUMDOC1_1_25 as StdField with uid="ELBKTJLXUB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_NUMDOC1", cQueryName = "NUMDOC1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri documento incongruenti",;
    ToolTipText = "A numero documento (vuoto tutti)",;
    HelpContextID = 28368170,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=438, Top=105, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMDOC1_1_25.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMDOC1_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMDOC1>=.w_NUMDOC)
    endwith
    return bRes
  endfunc

  add object oNUMDOC_1_26 as StdField with uid="RYAUNXPOFS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri documento incongruenti",;
    ToolTipText = "Da numero documento (vuoto tutti)",;
    HelpContextID = 28368170,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=359, Top=105, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMDOC_1_26.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMDOC_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMDOC1>=.w_NUMDOC or .w_NUMDOC1 = 0)
    endwith
    return bRes
  endfunc

  add object oNUMDOC1_1_27 as StdField with uid="HBCNVCLYKT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_NUMDOC1", cQueryName = "NUMDOC1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri documento incongruenti",;
    ToolTipText = "A numero documento (vuoto tutti)",;
    HelpContextID = 28368170,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=359, Top=127, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMDOC1_1_27.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMDOC1_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMDOC1>=.w_NUMDOC)
    endwith
    return bRes
  endfunc

  add object oSERIEP_1_28 as StdField with uid="PUNSCLZRBS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SERIEP", cQueryName = "SERIEP",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo",;
    HelpContextID = 88841434,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=560, Top=105, InputMask=replicate('X',2)

  func oSERIEP_1_28.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oSERIEP_1_29 as StdField with uid="YKEDOLQGFX",rtseq=29,rtrep=.f.,;
    cFormVar = "w_SERIEP", cQueryName = "SERIEP",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo",;
    HelpContextID = 88841434,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=560, Top=105, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oSERIEP_1_29.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oNUMPRO_1_30 as StdField with uid="FEMIVAKWIJ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NUMPRO", cQueryName = "NUMPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri protocollo incongruenti",;
    ToolTipText = "Da numero protocollo (vuoto tutti)",;
    HelpContextID = 91544874,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=640, Top=105, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMPRO_1_30.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMPRO_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMPRO1>=.w_NUMPRO OR .w_NUMPRO1=0)
    endwith
    return bRes
  endfunc

  add object oNUMPRO1_1_31 as StdField with uid="BHJPLKNGFF",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NUMPRO1", cQueryName = "NUMPRO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri protocollo incongruenti",;
    ToolTipText = "A numero protocollo (vuoto tutti)",;
    HelpContextID = 176890582,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=725, Top=105, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMPRO1_1_31.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMPRO1_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMPRO1>=.w_NUMPRO)
    endwith
    return bRes
  endfunc

  add object oNUMPRO_1_32 as StdField with uid="XYJFRATOTK",rtseq=32,rtrep=.f.,;
    cFormVar = "w_NUMPRO", cQueryName = "NUMPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri protocollo incongruenti",;
    ToolTipText = "Da numero protocollo (vuoto tutti)",;
    HelpContextID = 91544874,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=680, Top=105, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMPRO_1_32.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMPRO_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMPRO1>=.w_NUMPRO OR .w_NUMPRO1=0)
    endwith
    return bRes
  endfunc

  add object oNUMPRO1_1_33 as StdField with uid="MSMIGQZMLU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_NUMPRO1", cQueryName = "NUMPRO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri protocollo incongruenti",;
    ToolTipText = "A numero protocollo (vuoto tutti)",;
    HelpContextID = 176890582,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=680, Top=127, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMPRO1_1_33.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMPRO1_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMPRO1>=.w_NUMPRO)
    endwith
    return bRes
  endfunc


  add object oTIPCONEFF_1_34 as StdCombo with uid="YGNNWKIRET",rtseq=34,rtrep=.f.,left=74,top=159,width=141,height=21;
    , ToolTipText = "Intestatario";
    , HelpContextID = 156126172;
    , cFormVar="w_TIPCONEFF",RowSource=""+"Cliente,"+"Fornitore,"+"Intestatario eff. cliente,"+"Intestatario eff. fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCONEFF_1_34.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'I',;
    iif(this.value =4,'Z',;
    ' ')))))
  endfunc
  func oTIPCONEFF_1_34.GetRadio()
    this.Parent.oContained.w_TIPCONEFF = this.RadioValue()
    return .t.
  endfunc

  func oTIPCONEFF_1_34.SetRadio()
    this.Parent.oContained.w_TIPCONEFF=trim(this.Parent.oContained.w_TIPCONEFF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCONEFF=='C',1,;
      iif(this.Parent.oContained.w_TIPCONEFF=='F',2,;
      iif(this.Parent.oContained.w_TIPCONEFF=='I',3,;
      iif(this.Parent.oContained.w_TIPCONEFF=='Z',4,;
      0))))
  endfunc


  add object oTITIVA_1_36 as StdCombo with uid="HIDAGCDHXD",rtseq=36,rtrep=.f.,left=325,top=159,width=104,height=21;
    , ToolTipText = "Se attivo effettua controllo sulla partita IVA";
    , HelpContextID = 54229194;
    , cFormVar="w_TITIVA",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTITIVA_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    ' '))))
  endfunc
  func oTITIVA_1_36.GetRadio()
    this.Parent.oContained.w_TITIVA = this.RadioValue()
    return .t.
  endfunc

  func oTITIVA_1_36.SetRadio()
    this.Parent.oContained.w_TITIVA=trim(this.Parent.oContained.w_TITIVA)
    this.value = ;
      iif(this.Parent.oContained.w_TITIVA=='S',1,;
      iif(this.Parent.oContained.w_TITIVA=='N',2,;
      iif(this.Parent.oContained.w_TITIVA=='T',3,;
      0)))
  endfunc

  func oTITIVA_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')
    endwith
   endif
  endfunc

  add object oPERSFIS_1_37 as StdCheck with uid="PHNTAUGFAB",rtseq=37,rtrep=.f.,left=440, top=158, caption="Solo persone fisiche",;
    ToolTipText = "Se attivo filtra solo i clienti/fornitori che sono persone fisiche",;
    HelpContextID = 63857398,;
    cFormVar="w_PERSFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERSFIS_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPERSFIS_1_37.GetRadio()
    this.Parent.oContained.w_PERSFIS = this.RadioValue()
    return .t.
  endfunc

  func oPERSFIS_1_37.SetRadio()
    this.Parent.oContained.w_PERSFIS=trim(this.Parent.oContained.w_PERSFIS)
    this.value = ;
      iif(this.Parent.oContained.w_PERSFIS=='S',1,;
      0)
  endfunc

  func oPERSFIS_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')
    endwith
   endif
  endfunc

  add object oZONA_1_38 as StdField with uid="MLMLTBMRUZ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_ZONA", cQueryName = "ZONA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente",;
    ToolTipText = "Zona",;
    HelpContextID = 161731178,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=645, Top=159, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_ZONA"

  func oZONA_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')
    endwith
   endif
  endfunc

  func oZONA_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oZONA_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZONA_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oZONA_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"ZONE",'',this.parent.oContained
  endproc
  proc oZONA_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_ZONA
     i_obj.ecpSave()
  endproc

  add object oNAZIONE_1_39 as StdField with uid="PVSJLHTOEX",rtseq=39,rtrep=.f.,;
    cFormVar = "w_NAZIONE", cQueryName = "NAZIONE",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice nazione inesistente oppure obsoleto",;
    ToolTipText = "Nazione",;
    HelpContextID = 156557014,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=748, Top=159, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_NAZIONE"

  func oNAZIONE_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')
    endwith
   endif
  endfunc

  func oNAZIONE_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oNAZIONE_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNAZIONE_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oNAZIONE_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"NAZIONI",'',this.parent.oContained
  endproc
  proc oNAZIONE_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_NAZIONE
     i_obj.ecpSave()
  endproc

  add object oCODCON_1_40 as StdField with uid="MEFMTGTJSZ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente ",;
    ToolTipText = "Codice intestatario",;
    HelpContextID = 112358362,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=74, Top=188, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_40.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION" )
    endwith
  endfunc

  func oCODCON_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'GSCG_SOI.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oCODCON_1_41 as StdField with uid="LEPKFPFNCL",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente ",;
    ToolTipText = "Codice intestatario",;
    HelpContextID = 112358362,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=74, Top=188, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_41.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODCON_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_41.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oCONSUP_1_42 as StdField with uid="MPOCORUZOD",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CONSUP", cQueryName = "CONSUP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Mastro contabile inesistente",;
    ToolTipText = "Codice mastro intestatario",;
    HelpContextID = 71422938,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=74, Top=217, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSUP"

  func oCONSUP_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')
    endwith
   endif
  endfunc

  func oCONSUP_1_42.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oCONSUP_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSUP_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSUP_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSUP_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"MASTRI",'GSCG_SOI.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSUP_1_42.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSUP
     i_obj.ecpSave()
  endproc

  add object oCONSUP_1_43 as StdField with uid="MJWNALNWEX",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CONSUP", cQueryName = "CONSUP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Mastro contabile inesistente",;
    ToolTipText = "Codice mastro intestatario",;
    HelpContextID = 71422938,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=74, Top=217, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSUP"

  func oCONSUP_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')
    endwith
   endif
  endfunc

  func oCONSUP_1_43.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oCONSUP_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSUP_1_43.ecpDrop(oSource)
    this.Parent.oContained.link_1_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSUP_1_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSUP_1_43'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"MASTRI",'gsmambrb.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSUP_1_43.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSUP
     i_obj.ecpSave()
  endproc


  add object oANOPETRE_1_45 as StdCombo with uid="MGBBFYASRA",rtseq=45,rtrep=.f.,left=645,top=186,width=147,height=21;
    , ToolTipText = "Classificazione soggetto";
    , HelpContextID = 247151435;
    , cFormVar="w_ANOPETRE",RowSource=""+"Tutti tranne esclusi,"+"Corrispettivi periodici,"+"Contratti collegati,"+"Non definibile,"+"Escludi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANOPETRE_1_45.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'N',;
    iif(this.value =5,'E',;
    ' '))))))
  endfunc
  func oANOPETRE_1_45.GetRadio()
    this.Parent.oContained.w_ANOPETRE = this.RadioValue()
    return .t.
  endfunc

  func oANOPETRE_1_45.SetRadio()
    this.Parent.oContained.w_ANOPETRE=trim(this.Parent.oContained.w_ANOPETRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANOPETRE=='T',1,;
      iif(this.Parent.oContained.w_ANOPETRE=='P',2,;
      iif(this.Parent.oContained.w_ANOPETRE=='C',3,;
      iif(this.Parent.oContained.w_ANOPETRE=='N',4,;
      iif(this.Parent.oContained.w_ANOPETRE=='E',5,;
      0)))))
  endfunc

  func oANOPETRE_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')
    endwith
   endif
  endfunc


  add object oANTIPPRE_1_46 as StdCombo with uid="BTJSXLXSEN",rtseq=46,rtrep=.f.,left=645,top=216,width=147,height=21;
    , ToolTipText = "Classificazione operazioni tra beni e servizi";
    , HelpContextID = 191138635;
    , cFormVar="w_ANTIPPRE",RowSource=""+"Tutti,"+"Beni,"+"Servizi,"+"Non definibile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANTIPPRE_1_46.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'B',;
    iif(this.value =3,'S',;
    iif(this.value =4,'N',;
    ' ')))))
  endfunc
  func oANTIPPRE_1_46.GetRadio()
    this.Parent.oContained.w_ANTIPPRE = this.RadioValue()
    return .t.
  endfunc

  func oANTIPPRE_1_46.SetRadio()
    this.Parent.oContained.w_ANTIPPRE=trim(this.Parent.oContained.w_ANTIPPRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANTIPPRE=='T',1,;
      iif(this.Parent.oContained.w_ANTIPPRE=='B',2,;
      iif(this.Parent.oContained.w_ANTIPPRE=='S',3,;
      iif(this.Parent.oContained.w_ANTIPPRE=='N',4,;
      0))))
  endfunc

  func oANTIPPRE_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODCON) And .w_TIPCONEFF $ 'CF')
    endwith
   endif
  endfunc


  add object oREGIVA_1_47 as StdCombo with uid="ZQZITQNAHX",rtseq=47,rtrep=.f.,left=72,top=273,width=122,height=21;
    , ToolTipText = "Filtra sul tipo di registro specificato in causale";
    , HelpContextID = 54283498;
    , cFormVar="w_REGIVA",RowSource=""+"Vendite,"+"Acquisti,"+"Corr. scorporo,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oREGIVA_1_47.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oREGIVA_1_47.GetRadio()
    this.Parent.oContained.w_REGIVA = this.RadioValue()
    return .t.
  endfunc

  func oREGIVA_1_47.SetRadio()
    this.Parent.oContained.w_REGIVA=trim(this.Parent.oContained.w_REGIVA)
    this.value = ;
      iif(this.Parent.oContained.w_REGIVA=='V',1,;
      iif(this.Parent.oContained.w_REGIVA=='A',2,;
      iif(this.Parent.oContained.w_REGIVA=='C',3,;
      iif(this.Parent.oContained.w_REGIVA=='T',4,;
      0))))
  endfunc

  add object oNUMIVA_1_48 as StdField with uid="QGMOOTENQT",rtseq=48,rtrep=.f.,;
    cFormVar = "w_NUMIVA", cQueryName = "NUMIVA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri registro IVA incongruenti",;
    ToolTipText = "Da numero registro IVA (vuoto tutti)",;
    HelpContextID = 54254890,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=256, Top=273, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMIVA_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMIVA1>=.w_NUMIVA or .w_NUMIVA1 = 0)
    endwith
    return bRes
  endfunc

  add object oNUMIVA1_1_49 as StdField with uid="LASYKWSBHM",rtseq=49,rtrep=.f.,;
    cFormVar = "w_NUMIVA1", cQueryName = "NUMIVA1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numeri registro IVA incongruenti",;
    ToolTipText = "A numero registro IVA (vuoto tutti)",;
    HelpContextID = 54254890,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=361, Top=273, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMIVA1_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMIVA1>=.w_NUMIVA)
    endwith
    return bRes
  endfunc

  add object oDATAIVA1_1_50 as StdField with uid="DHSWKFVLDZ",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DATAIVA1", cQueryName = "DATAIVA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data competenza IVA iniziale",;
    HelpContextID = 15498855,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=493, Top=273

  func oDATAIVA1_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATAIVA1<=.w_DATAIVA2 or (empty(.w_DATAIVA2))))
    endwith
    return bRes
  endfunc

  add object oDATAIVA2_1_51 as StdField with uid="JQKORXCCGF",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DATAIVA2", cQueryName = "DATAIVA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Data competenza IVA finale",;
    HelpContextID = 15498856,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=597, Top=273

  func oDATAIVA2_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAIVA1<=.w_DATAIVA2 or (empty(.w_DATAIVA1)))
    endwith
    return bRes
  endfunc

  add object oCODIVA_1_52 as StdField with uid="OSWZJQXDWI",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente ",;
    ToolTipText = "Codice IVA",;
    HelpContextID = 54293466,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=72, Top=306, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCODIVA_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"CODICI IVA",'',this.parent.oContained
  endproc
  proc oCODIVA_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc

  add object oPERIVA_1_53 as StdField with uid="QKQDALBNFJ",rtseq=53,rtrep=.f.,;
    cFormVar = "w_PERIVA", cQueryName = "PERIVA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale aliquota IVA; se valorizzato a 0 implica nessuna selezione",;
    HelpContextID = 54238474,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=449, Top=306, cSayPict='"99.99"', cGetPict='"99.99"'

  func oPERIVA_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODIVA))
    endwith
   endif
  endfunc

  add object oFLAGZERO_1_54 as StdCheck with uid="OLBUIFDPPU",rtseq=54,rtrep=.f.,left=504, top=307, caption="Escludi aliquota a zero",;
    ToolTipText = "Se attivo vengono eliminate tutte le voci IVA che hanno aliquota uguale a 0",;
    HelpContextID = 16865701,;
    cFormVar="w_FLAGZERO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLAGZERO_1_54.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLAGZERO_1_54.GetRadio()
    this.Parent.oContained.w_FLAGZERO = this.RadioValue()
    return .t.
  endfunc

  func oFLAGZERO_1_54.SetRadio()
    this.Parent.oContained.w_FLAGZERO=trim(this.Parent.oContained.w_FLAGZERO)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGZERO=='S',1,;
      0)
  endfunc

  func oFLAGZERO_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODIVA) AND .w_PERIVA=0)
    endwith
   endif
  endfunc


  add object oIVFLGBSV_1_55 as StdCombo with uid="LPLHSIBGBY",rtseq=55,rtrep=.f.,left=256,top=343,width=143,height=21;
    , ToolTipText = "Tipologia operazione IVA ciclo vendite";
    , HelpContextID = 215397340;
    , cFormVar="w_IVFLGBSV",RowSource=""+"Non definito,"+"Escluso,"+"Cessione di beni,"+"Prestazione di servizi,"+"Tutti tranne esclusi,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVFLGBSV_1_55.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'E',;
    iif(this.value =3,'B',;
    iif(this.value =4,'S',;
    iif(this.value =5,'T',;
    iif(this.value =6,'X',;
    ' ')))))))
  endfunc
  func oIVFLGBSV_1_55.GetRadio()
    this.Parent.oContained.w_IVFLGBSV = this.RadioValue()
    return .t.
  endfunc

  func oIVFLGBSV_1_55.SetRadio()
    this.Parent.oContained.w_IVFLGBSV=trim(this.Parent.oContained.w_IVFLGBSV)
    this.value = ;
      iif(this.Parent.oContained.w_IVFLGBSV=='N',1,;
      iif(this.Parent.oContained.w_IVFLGBSV=='E',2,;
      iif(this.Parent.oContained.w_IVFLGBSV=='B',3,;
      iif(this.Parent.oContained.w_IVFLGBSV=='S',4,;
      iif(this.Parent.oContained.w_IVFLGBSV=='T',5,;
      iif(this.Parent.oContained.w_IVFLGBSV=='X',6,;
      0))))))
  endfunc

  func oIVFLGBSV_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODIVA))
    endwith
   endif
  endfunc


  add object oIVFLGBSA_1_56 as StdCombo with uid="GGWIUAGGUC",rtseq=56,rtrep=.f.,left=647,top=343,width=149,height=21;
    , ToolTipText = "Tipologia operazione IVA ciclo acquisti";
    , HelpContextID = 215397319;
    , cFormVar="w_IVFLGBSA",RowSource=""+"Non definito,"+"Escluso,"+"Acquisto di beni,"+"Acquisizione di servizi,"+"Tutti tranne esclusi,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVFLGBSA_1_56.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'E',;
    iif(this.value =3,'B',;
    iif(this.value =4,'S',;
    iif(this.value =5,'T',;
    iif(this.value =6,'X',;
    ' ')))))))
  endfunc
  func oIVFLGBSA_1_56.GetRadio()
    this.Parent.oContained.w_IVFLGBSA = this.RadioValue()
    return .t.
  endfunc

  func oIVFLGBSA_1_56.SetRadio()
    this.Parent.oContained.w_IVFLGBSA=trim(this.Parent.oContained.w_IVFLGBSA)
    this.value = ;
      iif(this.Parent.oContained.w_IVFLGBSA=='N',1,;
      iif(this.Parent.oContained.w_IVFLGBSA=='E',2,;
      iif(this.Parent.oContained.w_IVFLGBSA=='B',3,;
      iif(this.Parent.oContained.w_IVFLGBSA=='S',4,;
      iif(this.Parent.oContained.w_IVFLGBSA=='T',5,;
      iif(this.Parent.oContained.w_IVFLGBSA=='X',6,;
      0))))))
  endfunc

  func oIVFLGBSA_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODIVA))
    endwith
   endif
  endfunc


  add object oTOTDOC_1_57 as StdCombo with uid="TVHQPJOEXL",rtseq=57,rtrep=.f.,left=146,top=379,width=143,height=21;
    , ToolTipText = "Criterio di calcolo del filtro importo";
    , HelpContextID = 28340938;
    , cFormVar="w_TOTDOC",RowSource=""+"Imponibile+Imposta,"+"Imposta,"+"Imponibile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTOTDOC_1_57.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'I',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oTOTDOC_1_57.GetRadio()
    this.Parent.oContained.w_TOTDOC = this.RadioValue()
    return .t.
  endfunc

  func oTOTDOC_1_57.SetRadio()
    this.Parent.oContained.w_TOTDOC=trim(this.Parent.oContained.w_TOTDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TOTDOC=='E',1,;
      iif(this.Parent.oContained.w_TOTDOC=='I',2,;
      iif(this.Parent.oContained.w_TOTDOC=='M',3,;
      0)))
  endfunc


  add object oFILTIMPO_1_58 as StdCombo with uid="CXSGLKPSIU",rtseq=58,rtrep=.f.,left=405,top=379,width=116,height=21;
    , ToolTipText = "Tipologia di filtro da applicare sul totale della registrazione";
    , HelpContextID = 134281563;
    , cFormVar="w_FILTIMPO",RowSource=""+"Nessun filtro,"+"Maggiore di,"+"Minore di,"+"Compreso tra", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFILTIMPO_1_58.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'I',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oFILTIMPO_1_58.GetRadio()
    this.Parent.oContained.w_FILTIMPO = this.RadioValue()
    return .t.
  endfunc

  func oFILTIMPO_1_58.SetRadio()
    this.Parent.oContained.w_FILTIMPO=trim(this.Parent.oContained.w_FILTIMPO)
    this.value = ;
      iif(this.Parent.oContained.w_FILTIMPO=='N',1,;
      iif(this.Parent.oContained.w_FILTIMPO=='M',2,;
      iif(this.Parent.oContained.w_FILTIMPO=='I',3,;
      iif(this.Parent.oContained.w_FILTIMPO=='C',4,;
      0))))
  endfunc

  add object oDAIMPO_1_59 as StdField with uid="FEUGIPJYRN",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DAIMPO", cQueryName = "DAIMPO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo di inizio selezione",;
    HelpContextID = 93860298,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=647, Top=379, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oDAIMPO_1_59.mHide()
    with this.Parent.oContained
      return (.w_FILTIMPO = 'N')
    endwith
  endfunc

  add object oAIMPO_1_60 as StdField with uid="YWPRPLVFPY",rtseq=60,rtrep=.f.,;
    cFormVar = "w_AIMPO", cQueryName = "AIMPO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo di fine selezione",;
    HelpContextID = 77916666,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=647, Top=407, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAIMPO_1_60.mHide()
    with this.Parent.oContained
      return (.w_FILTIMPO $ 'NMI')
    endwith
  endfunc


  add object oOPERVALO_1_61 as StdCombo with uid="HGTDUVJQLS",rtseq=61,rtrep=.f.,left=72,top=450,width=173,height=21;
    , ToolTipText = "Tipologia operazioni";
    , HelpContextID = 53699019;
    , cFormVar="w_OPERVALO",RowSource=""+"Operazioni gi� valorizzate,"+"Operazioni non valorizzate,"+"Tutte,"+"Operazioni collegate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOPERVALO_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    iif(this.value =4,'C',;
    space(10))))))
  endfunc
  func oOPERVALO_1_61.GetRadio()
    this.Parent.oContained.w_OPERVALO = this.RadioValue()
    return .t.
  endfunc

  func oOPERVALO_1_61.SetRadio()
    this.Parent.oContained.w_OPERVALO=trim(this.Parent.oContained.w_OPERVALO)
    this.value = ;
      iif(this.Parent.oContained.w_OPERVALO=='S',1,;
      iif(this.Parent.oContained.w_OPERVALO=='N',2,;
      iif(this.Parent.oContained.w_OPERVALO=='T',3,;
      iif(this.Parent.oContained.w_OPERVALO=='C',4,;
      0))))
  endfunc


  add object oOPERVERI_1_62 as StdCombo with uid="HHHJPCDAST",rtseq=62,rtrep=.f.,left=402,top=450,width=73,height=21;
    , ToolTipText = "Operazioni da verificare";
    , HelpContextID = 13409839;
    , cFormVar="w_OPERVERI",RowSource=""+"Si,"+"No,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOPERVERI_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oOPERVERI_1_62.GetRadio()
    this.Parent.oContained.w_OPERVERI = this.RadioValue()
    return .t.
  endfunc

  func oOPERVERI_1_62.SetRadio()
    this.Parent.oContained.w_OPERVERI=trim(this.Parent.oContained.w_OPERVERI)
    this.value = ;
      iif(this.Parent.oContained.w_OPERVERI=='S',1,;
      iif(this.Parent.oContained.w_OPERVERI=='N',2,;
      iif(this.Parent.oContained.w_OPERVERI=='T',3,;
      0)))
  endfunc

  func oOPERVERI_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OPERVALO $ 'ST')
    endwith
   endif
  endfunc


  add object oOPGENERA_1_63 as StdCombo with uid="AZBRDEPLLK",rtseq=63,rtrep=.f.,left=700,top=450,width=73,height=21;
    , ToolTipText = "Operazioni valorizzate da procedura";
    , HelpContextID = 4177447;
    , cFormVar="w_OPGENERA",RowSource=""+"Si,"+"No,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOPGENERA_1_63.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    'N'))))
  endfunc
  func oOPGENERA_1_63.GetRadio()
    this.Parent.oContained.w_OPGENERA = this.RadioValue()
    return .t.
  endfunc

  func oOPGENERA_1_63.SetRadio()
    this.Parent.oContained.w_OPGENERA=trim(this.Parent.oContained.w_OPGENERA)
    this.value = ;
      iif(this.Parent.oContained.w_OPGENERA=='S',1,;
      iif(this.Parent.oContained.w_OPGENERA=='N',2,;
      iif(this.Parent.oContained.w_OPGENERA=='T',3,;
      0)))
  endfunc

  func oOPGENERA_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OPERVALO $ 'ST')
    endwith
   endif
  endfunc

  add object oUTEINSER_1_64 as StdField with uid="YZEZWDQMFD",rtseq=64,rtrep=.f.,;
    cFormVar = "w_UTEINSER", cQueryName = "UTEINSER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha inserito l'operazione",;
    HelpContextID = 239313560,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=168, Top=489, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_UTEINSER"

  func oUTEINSER_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTEINSER_1_64.ecpDrop(oSource)
    this.Parent.oContained.link_1_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTEINSER_1_64.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oUTEINSER_1_64'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDATINSERI_1_65 as StdField with uid="PLWYPGVMQE",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DATINSERI", cQueryName = "DATINSERI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Da data inserimento operazione",;
    HelpContextID = 239371032,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=479, Top=489

  func oDATINSERI_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINSERI<=.w_DATINSERF or (empty(.w_DATINSERF))))
    endwith
    return bRes
  endfunc

  add object oDATINSERF_1_66 as StdField with uid="QNYVWOEFWE",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DATINSERF", cQueryName = "DATINSERF",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "A data inserimento operazione",;
    HelpContextID = 239370984,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=587, Top=489

  func oDATINSERF_1_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINSERI<=.w_DATINSERF or (empty(.w_DATINSERI)))
    endwith
    return bRes
  endfunc

  add object oUTEVARIA_1_67 as StdField with uid="RPXNMPEEII",rtseq=67,rtrep=.f.,;
    cFormVar = "w_UTEVARIA", cQueryName = "UTEVARIA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha variato l'operazione",;
    HelpContextID = 58678649,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=168, Top=523, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_UTEVARIA"

  func oUTEVARIA_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTEVARIA_1_67.ecpDrop(oSource)
    this.Parent.oContained.link_1_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTEVARIA_1_67.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oUTEVARIA_1_67'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDATVARIAI_1_68 as StdField with uid="HWRAKEXYLG",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DATVARIAI", cQueryName = "DATVARIAI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "Da data variazione operazione",;
    HelpContextID = 58621177,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=479, Top=523

  func oDATVARIAI_1_68.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATVARIAI<=.w_DATVARIAF or (empty(.w_DATVARIAF))))
    endwith
    return bRes
  endfunc

  add object oDATVARIAF_1_69 as StdField with uid="GJPNKDBWTL",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DATVARIAF", cQueryName = "DATVARIAF",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla data finale",;
    ToolTipText = "A data variazione operazione",;
    HelpContextID = 58621225,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=587, Top=523

  func oDATVARIAF_1_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATVARIAI<=.w_DATVARIAF or (empty(.w_DATVARIAI)))
    endwith
    return bRes
  endfunc

  add object oDESCRICA_1_75 as StdField with uid="QBTFDMQPEE",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESCRICA", cQueryName = "DESCRICA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 75395703,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=137, Top=54, InputMask=replicate('X',35)

  add object oDESIVA_1_91 as StdField with uid="WPDIKLKHDL",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 54234570,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=136, Top=306, InputMask=replicate('X',35)


  add object oBtn_1_94 as StdButton with uid="ZXYGHBDURE",left=693, top=500, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 193049110;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_94.Click()
      =cp_StandardFunction(this,"PgDn")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_95 as StdButton with uid="KUWCMEGMLG",left=745, top=500, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 193049110;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_95.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMAS_1_96 as StdField with uid="MTWTUQTSEH",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DESMAS", cQueryName = "DESMAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 42438090,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=209, Top=217, InputMask=replicate('X',35)

  add object oDESCRI_1_99 as StdField with uid="JYMJZTSWYL",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193039818,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=209, Top=188, InputMask=replicate('X',35)

  add object oDESUTE_1_111 as StdField with uid="LSFTKVNZBB",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 256871882,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=237, Top=489, InputMask=replicate('X',20)

  add object oDESUTE1_1_114 as StdField with uid="PGTXIWASRV",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESUTE1", cQueryName = "DESUTE1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 11563574,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=237, Top=523, InputMask=replicate('X',20)

  add object oStr_1_71 as StdString with uid="MBRGDIBDJR",Visible=.t., Left=12, Top=4,;
    Alignment=0, Width=114, Height=19,;
    Caption="Registrazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_72 as StdString with uid="PHFMRQMDGQ",Visible=.t., Left=35, Top=27,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="BGRCELDVIO",Visible=.t., Left=173, Top=27,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="SCMMADWOHL",Visible=.t., Left=17, Top=55,;
    Alignment=1, Width=52, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="LUIROGNSWY",Visible=.t., Left=396, Top=55,;
    Alignment=1, Width=94, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="NGEKLKFMIP",Visible=.t., Left=12, Top=84,;
    Alignment=0, Width=78, Height=19,;
    Caption="Documento"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_79 as StdString with uid="PAICLABZEK",Visible=.t., Left=12, Top=134,;
    Alignment=0, Width=73, Height=19,;
    Caption="Intestatario"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_81 as StdString with uid="UYVTKUKDDI",Visible=.t., Left=39, Top=159,;
    Alignment=1, Width=30, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="PRYZKCSGCM",Visible=.t., Left=217, Top=159,;
    Alignment=1, Width=104, Height=18,;
    Caption="Titolari P.IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="QWQWTTFWBH",Visible=.t., Left=12, Top=245,;
    Alignment=0, Width=87, Height=19,;
    Caption="Dati IVA"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="BZQTVPGASC",Visible=.t., Left=4, Top=275,;
    Alignment=1, Width=65, Height=18,;
    Caption="Reg. IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="SOFHWDSKWV",Visible=.t., Left=203, Top=274,;
    Alignment=1, Width=52, Height=18,;
    Caption="Da num.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="EMCKUFFUXY",Visible=.t., Left=317, Top=274,;
    Alignment=1, Width=43, Height=18,;
    Caption="A num.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="VDBRMDHLFO",Visible=.t., Left=421, Top=274,;
    Alignment=1, Width=70, Height=18,;
    Caption="Comp. da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="LLMKTINHQR",Visible=.t., Left=575, Top=274,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="XCRWZTVVXB",Visible=.t., Left=14, Top=308,;
    Alignment=1, Width=55, Height=18,;
    Caption="Cod. IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="PPCYHHMJUV",Visible=.t., Left=398, Top=308,;
    Alignment=1, Width=47, Height=18,;
    Caption="Aliquota:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="WHGHOXNTCF",Visible=.t., Left=504, Top=84,;
    Alignment=0, Width=78, Height=19,;
    Caption="Protocollo"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_97 as StdString with uid="IYPFSOWZCO",Visible=.t., Left=5, Top=217,;
    Alignment=1, Width=64, Height=18,;
    Caption="Mastro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="ZZJKXHVOOI",Visible=.t., Left=603, Top=159,;
    Alignment=1, Width=35, Height=18,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="LOSXKCQKEV",Visible=.t., Left=20, Top=188,;
    Alignment=1, Width=49, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="EZTKFZXYQE",Visible=.t., Left=693, Top=159,;
    Alignment=1, Width=54, Height=18,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="JTOWJYRJRT",Visible=.t., Left=280, Top=27,;
    Alignment=1, Width=44, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_103 as StdString with uid="JLZCJDFVIB",Visible=.t., Left=441, Top=27,;
    Alignment=1, Width=49, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_104 as StdString with uid="UYAAQHCPRD",Visible=.t., Left=486, Top=188,;
    Alignment=1, Width=152, Height=18,;
    Caption="Operazioni rilevanti IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="DFDUXKNAOV",Visible=.t., Left=499, Top=217,;
    Alignment=1, Width=139, Height=18,;
    Caption="Tipologia prevalente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_106 as StdString with uid="LWQNIHICPH",Visible=.t., Left=17, Top=345,;
    Alignment=1, Width=238, Height=18,;
    Caption="Tipologia operazione IVA ciclo vendite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_107 as StdString with uid="YGFQPVGASA",Visible=.t., Left=403, Top=345,;
    Alignment=1, Width=238, Height=18,;
    Caption="Tipologia operazione IVA ciclo acquisti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="NPFIKEBJGE",Visible=.t., Left=12, Top=422,;
    Alignment=0, Width=238, Height=19,;
    Caption="Dati operazioni superiori a 3.000 euro"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_110 as StdString with uid="TXHDMCQMQR",Visible=.t., Left=7, Top=491,;
    Alignment=1, Width=159, Height=18,;
    Caption="Operazioni inserite da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="ABIWCPWXHZ",Visible=.t., Left=447, Top=491,;
    Alignment=1, Width=26, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="DCQLLTGLJL",Visible=.t., Left=7, Top=525,;
    Alignment=1, Width=159, Height=18,;
    Caption="Operazioni variate da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_115 as StdString with uid="ZPPKGENPBO",Visible=.t., Left=19, Top=451,;
    Alignment=1, Width=50, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_116 as StdString with uid="KTSNQWGZUB",Visible=.t., Left=562, Top=491,;
    Alignment=1, Width=19, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="WTXWCQPIMB",Visible=.t., Left=447, Top=525,;
    Alignment=1, Width=26, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_118 as StdString with uid="TEXRTOZIZV",Visible=.t., Left=562, Top=525,;
    Alignment=1, Width=19, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_120 as StdString with uid="URNPWCFWDE",Visible=.t., Left=11, Top=381,;
    Alignment=1, Width=133, Height=18,;
    Caption="Totale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_121 as StdString with uid="VBMFQWKYBY",Visible=.t., Left=556, Top=382,;
    Alignment=1, Width=85, Height=18,;
    Caption="Da importo:"  ;
  , bGlobalFont=.t.

  func oStr_1_121.mHide()
    with this.Parent.oContained
      return (.w_FILTIMPO $ 'NMI')
    endwith
  endfunc

  add object oStr_1_122 as StdString with uid="DPJRBKYTJK",Visible=.t., Left=556, Top=408,;
    Alignment=1, Width=85, Height=18,;
    Caption="A importo:"  ;
  , bGlobalFont=.t.

  func oStr_1_122.mHide()
    with this.Parent.oContained
      return (.w_FILTIMPO $ 'NMI')
    endwith
  endfunc

  add object oStr_1_123 as StdString with uid="ONORFDZXIF",Visible=.t., Left=296, Top=381,;
    Alignment=1, Width=99, Height=18,;
    Caption="Filtro importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_125 as StdString with uid="EMTNFAKHJA",Visible=.t., Left=556, Top=382,;
    Alignment=1, Width=85, Height=18,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  func oStr_1_125.mHide()
    with this.Parent.oContained
      return (.w_FILTIMPO $ 'NC')
    endwith
  endfunc

  add object oStr_1_126 as StdString with uid="AMWXDKXOMM",Visible=.t., Left=253, Top=451,;
    Alignment=1, Width=143, Height=18,;
    Caption="Operazioni da verificare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_127 as StdString with uid="UKOZLKAMLX",Visible=.t., Left=483, Top=451,;
    Alignment=1, Width=212, Height=18,;
    Caption="Operazioni valorizzate da procedura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_133 as StdString with uid="JUVFPEVHFK",Visible=.t., Left=153, Top=108,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  func oStr_1_133.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_134 as StdString with uid="OIIIWEMGOO",Visible=.t., Left=322, Top=108,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_135 as StdString with uid="PKXJJVLWXF",Visible=.t., Left=416, Top=108,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  func oStr_1_135.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_136 as StdString with uid="QQZSTOOZQK",Visible=.t., Left=256, Top=108,;
    Alignment=1, Width=32, Height=15,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  func oStr_1_136.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_137 as StdString with uid="CFAYJALDQR",Visible=.t., Left=602, Top=108,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  func oStr_1_137.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_138 as StdString with uid="TSGLOUSSXU",Visible=.t., Left=703, Top=108,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  func oStr_1_138.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_139 as StdString with uid="HYWCTZCURT",Visible=.t., Left=521, Top=108,;
    Alignment=1, Width=32, Height=18,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_140 as StdString with uid="KXVMPFYYLL",Visible=.t., Left=35, Top=108,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  func oStr_1_140.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_141 as StdString with uid="PVFDMADYQN",Visible=.t., Left=550, Top=27,;
    Alignment=1, Width=49, Height=15,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_142 as StdString with uid="YXMJGBBFBV",Visible=.t., Left=636, Top=27,;
    Alignment=1, Width=44, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_143 as StdString with uid="PCTYOPZQMN",Visible=.t., Left=80, Top=108,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  func oStr_1_143.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_144 as StdString with uid="KXWDPBEUGB",Visible=.t., Left=95, Top=131,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  func oStr_1_144.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_145 as StdString with uid="HNIRVUNPPA",Visible=.t., Left=203, Top=108,;
    Alignment=1, Width=32, Height=15,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  func oStr_1_145.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_146 as StdString with uid="YCUQQNDRLU",Visible=.t., Left=336, Top=131,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  func oStr_1_146.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_147 as StdString with uid="CMMHBXXQEW",Visible=.t., Left=645, Top=108,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  func oStr_1_147.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_148 as StdString with uid="LHKZTAZLAU",Visible=.t., Left=660, Top=131,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  func oStr_1_148.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oBox_1_70 as StdBox with uid="RHRVXWZSSY",left=1, top=20, width=796,height=2

  add object oBox_1_78 as StdBox with uid="IETHZIYDGG",left=1, top=99, width=797,height=2

  add object oBox_1_80 as StdBox with uid="TZFDYUOFNG",left=1, top=151, width=796,height=2

  add object oBox_1_83 as StdBox with uid="NBNLSSXGHT",left=1, top=263, width=796,height=2

  add object oBox_1_109 as StdBox with uid="DZTWFVCRTN",left=0, top=437, width=796,height=2
enddefine
define class tgsai_kosPag2 as StdContainer
  Width  = 802
  height = 555
  stdWidth  = 802
  stdheight = 555
  resizeXpos=517
  resizeYpos=29
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMVP as cp_szoombox with uid="IEEULTBTOF",left=3, top=14, width=796,height=300,;
    caption='ZOOMVP',;
   bGlobalFont=.t.,;
    cZoomFile="GSAI_KOS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cTable="PNT_MAST",cZoomOnZoom="",cMenuFile="GSAI_KOS",;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 70763114


  add object oOSTIPOPE_2_5 as StdCombo with uid="RJRRLIFHCM",rtseq=77,rtrep=.f.,left=161,top=410,width=162,height=21;
    , ToolTipText = "Tipologia operazione";
    , HelpContextID = 94072533;
    , cFormVar="w_OSTIPOPE",RowSource=""+"Nessuno,"+"Contratti collegati,"+"Corrispettivi periodici,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOSTIPOPE_2_5.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'C',;
    iif(this.value =3,'P',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oOSTIPOPE_2_5.GetRadio()
    this.Parent.oContained.w_OSTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oOSTIPOPE_2_5.SetRadio()
    this.Parent.oContained.w_OSTIPOPE=trim(this.Parent.oContained.w_OSTIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_OSTIPOPE=='N',1,;
      iif(this.Parent.oContained.w_OSTIPOPE=='C',2,;
      iif(this.Parent.oContained.w_OSTIPOPE=='P',3,;
      iif(this.Parent.oContained.w_OSTIPOPE=='X',4,;
      0))))
  endfunc

  func oOSTIPOPE_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSFLGEXT <> 'F')
    endwith
   endif
  endfunc

  add object oCHKRIFCON_2_6 as StdCheck with uid="ZQNGMZVADL",rtseq=78,rtrep=.f.,left=359, top=407, caption="Riferimento contratto",;
    ToolTipText = "Se attivo, avviene l'aggiornamento del riferimento contratto",;
    HelpContextID = 16579157,;
    cFormVar="w_CHKRIFCON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHKRIFCON_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHKRIFCON_2_6.GetRadio()
    this.Parent.oContained.w_CHKRIFCON = this.RadioValue()
    return .t.
  endfunc

  func oCHKRIFCON_2_6.SetRadio()
    this.Parent.oContained.w_CHKRIFCON=trim(this.Parent.oContained.w_CHKRIFCON)
    this.value = ;
      iif(this.Parent.oContained.w_CHKRIFCON=='S',1,;
      0)
  endfunc

  func oCHKRIFCON_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSTIPOPE $ 'XCP' AND .w_OSFLGEXT <> 'F')
    endwith
   endif
  endfunc

  add object oOSRIFCON_2_7 as StdField with uid="ESRXEADMNY",rtseq=79,rtrep=.f.,;
    cFormVar = "w_OSRIFCON", cQueryName = "OSRIFCON",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Codice riferimento contratto",;
    HelpContextID = 37457612,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=499, Top=410, InputMask=replicate('X',30)

  func oOSRIFCON_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHKRIFCON='S')
    endwith
   endif
  endfunc


  add object oOSTIPFAT_2_8 as StdCombo with uid="RZJLRTBRGB",rtseq=80,rtrep=.f.,left=161,top=448,width=162,height=21;
    , ToolTipText = "Tipologia fattura";
    , HelpContextID = 23367994;
    , cFormVar="w_OSTIPFAT",RowSource=""+"Saldo,"+"Acconto,"+"Nota rettifica,"+"Corrispettivi,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOSTIPFAT_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'A',;
    iif(this.value =3,'N',;
    iif(this.value =4,'C',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oOSTIPFAT_2_8.GetRadio()
    this.Parent.oContained.w_OSTIPFAT = this.RadioValue()
    return .t.
  endfunc

  func oOSTIPFAT_2_8.SetRadio()
    this.Parent.oContained.w_OSTIPFAT=trim(this.Parent.oContained.w_OSTIPFAT)
    this.value = ;
      iif(this.Parent.oContained.w_OSTIPFAT=='S',1,;
      iif(this.Parent.oContained.w_OSTIPFAT=='A',2,;
      iif(this.Parent.oContained.w_OSTIPFAT=='N',3,;
      iif(this.Parent.oContained.w_OSTIPFAT=='C',4,;
      iif(this.Parent.oContained.w_OSTIPFAT=='X',5,;
      0)))))
  endfunc

  func oOSTIPFAT_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSFLGEXT <> 'F')
    endwith
   endif
  endfunc


  add object oOSIMPFRA_2_9 as StdCombo with uid="EGGTCXJSOP",rtseq=81,rtrep=.f.,left=499,top=448,width=162,height=21;
    , ToolTipText = "Importo non frazionato";
    , HelpContextID = 23585063;
    , cFormVar="w_OSIMPFRA",RowSource=""+"Attivo,"+"Non attivo,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOSIMPFRA_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oOSIMPFRA_2_9.GetRadio()
    this.Parent.oContained.w_OSIMPFRA = this.RadioValue()
    return .t.
  endfunc

  func oOSIMPFRA_2_9.SetRadio()
    this.Parent.oContained.w_OSIMPFRA=trim(this.Parent.oContained.w_OSIMPFRA)
    this.value = ;
      iif(this.Parent.oContained.w_OSIMPFRA=='S',1,;
      iif(this.Parent.oContained.w_OSIMPFRA=='N',2,;
      iif(this.Parent.oContained.w_OSIMPFRA=='X',3,;
      0)))
  endfunc

  func oOSIMPFRA_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSFLGEXT <> 'F')
    endwith
   endif
  endfunc


  add object oOSFLGEXT_2_10 as StdCombo with uid="ZALKETARRL",rtseq=82,rtrep=.f.,left=161,top=484,width=162,height=21;
    , ToolTipText = "Tipologia estrazione";
    , HelpContextID = 265728314;
    , cFormVar="w_OSFLGEXT",RowSource=""+"Inclusione forzata,"+"Esclusione forzata,"+"Nessuna forzatura,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOSFLGEXT_2_10.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oOSFLGEXT_2_10.GetRadio()
    this.Parent.oContained.w_OSFLGEXT = this.RadioValue()
    return .t.
  endfunc

  func oOSFLGEXT_2_10.SetRadio()
    this.Parent.oContained.w_OSFLGEXT=trim(this.Parent.oContained.w_OSFLGEXT)
    this.value = ;
      iif(this.Parent.oContained.w_OSFLGEXT=='I',1,;
      iif(this.Parent.oContained.w_OSFLGEXT=='F',2,;
      iif(this.Parent.oContained.w_OSFLGEXT=='N',3,;
      iif(this.Parent.oContained.w_OSFLGEXT=='X',4,;
      0))))
  endfunc


  add object oOSFLGDVE_2_11 as StdCombo with uid="OEZNMTMMFV",rtseq=83,rtrep=.f.,left=161,top=522,width=162,height=21;
    , ToolTipText = "Se attivo, identifica le registrazioni che l'utente vuole ricontrollare";
    , HelpContextID = 248951083;
    , cFormVar="w_OSFLGDVE",RowSource=""+"Si,"+"No,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOSFLGDVE_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oOSFLGDVE_2_11.GetRadio()
    this.Parent.oContained.w_OSFLGDVE = this.RadioValue()
    return .t.
  endfunc

  func oOSFLGDVE_2_11.SetRadio()
    this.Parent.oContained.w_OSFLGDVE=trim(this.Parent.oContained.w_OSFLGDVE)
    this.value = ;
      iif(this.Parent.oContained.w_OSFLGDVE=='S',1,;
      iif(this.Parent.oContained.w_OSFLGDVE=='N',2,;
      iif(this.Parent.oContained.w_OSFLGDVE=='X',3,;
      0)))
  endfunc

  add object oCCDESCRI_2_15 as StdField with uid="UVNZRCCTJY",rtseq=85,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 244285551,;
   bGlobalFont=.t.,;
    Height=21, Width=375, Left=170, Top=325, InputMask=replicate('X',35)

  add object oANDESCRI_2_17 as StdField with uid="UMCQNKOTMO",rtseq=86,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 244288335,;
   bGlobalFont=.t.,;
    Height=21, Width=375, Left=170, Top=355, InputMask=replicate('X',40)


  add object oBtn_2_24 as StdButton with uid="TOYTSIORUM",left=698, top=509, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare i dati";
    , HelpContextID = 193049110;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_24.Click()
      with this.Parent.oContained
        GSAI_BZP(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OSTIPOPE $ 'NCP' OR .w_CHKRIFCON='S' OR .w_OSTIPFAT $ 'SANC' OR .w_OSFLGEXT $ 'IFN' OR .w_OSFLGDVE $ 'SN' OR .w_OSIMPFRA $ 'SN')
      endwith
    endif
  endfunc


  add object oBtn_2_25 as StdButton with uid="EGNAIUHSLT",left=748, top=509, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 193049110;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_29 as StdButton with uid="FLKGPBIKCV",left=748, top=325, width=48,height=45,;
    CpPicture="BMP\LEGENDA_COLORI.BMP", caption="", nPag=2;
    , ToolTipText = "Legenda colori";
    , HelpContextID = 193049110;
    , caption='\<Legenda';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_29.Click()
      with this.Parent.oContained
        do GSAI_KLC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_30 as StdButton with uid="GQMUPTTTCY",left=602, top=325, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare tutte le registrazioni";
    , HelpContextID = 193049110;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_30.Click()
      with this.Parent.oContained
        GSAI_BZP(this.Parent.oContained,"X")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_31 as StdButton with uid="ZCYHLDNLVF",left=650, top=325, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per deselezionare tutte le registrazioni";
    , HelpContextID = 193049110;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_31.Click()
      with this.Parent.oContained
        GSAI_BZP(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_32 as StdButton with uid="GUQBLQMZBT",left=698, top=325, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per invertire la selezione di tutte le registrazioni";
    , HelpContextID = 193049110;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_32.Click()
      with this.Parent.oContained
        GSAI_BZP(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_33 as StdButton with uid="GLCAOUJZUX",left=648, top=509, width=48,height=45,;
    CpPicture="BMP\PREDEFINITO.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare i dati con i valori di default";
    , HelpContextID = 164479542;
    , caption='\<Default',TabStop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_33.Click()
      with this.Parent.oContained
        GSAI_BZP(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OSTIPOPE = 'X' And .w_CHKRIFCON='N' And .w_OSTIPFAT = 'X' And .w_OSFLGEXT = 'X' And .w_OSFLGDVE = 'X' AND .w_OSIMPFRA='X' AND .w_OPERVALO<>'C')
      endwith
    endif
  endfunc


  add object oBtn_2_42 as StdButton with uid="GLPKHFOSFK",left=598, top=509, width=48,height=45,;
    CpPicture="bmp\CONTRATT.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare i riferimenti contratti";
    , HelpContextID = 164479542;
    , caption='\<Rif.Con',TabStop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_42.Click()
      with this.Parent.oContained
        GSAI_BZP(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OSTIPOPE = 'X' And .w_CHKRIFCON='N' And .w_OSTIPFAT = 'X' And .w_OSFLGEXT = 'X' And .w_OSFLGDVE = 'X' AND .w_OSIMPFRA='X' AND .w_OPERVALO='C' )
      endwith
    endif
  endfunc

  add object oStr_2_3 as StdString with uid="SAJYSCUIOK",Visible=.t., Left=-2, Top=381,;
    Alignment=1, Width=162, Height=18,;
    Caption="Opzioni aggiornamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_12 as StdString with uid="UHHZTZIQFA",Visible=.t., Left=23, Top=523,;
    Alignment=1, Width=133, Height=18,;
    Caption="Dati da verificare:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="XLZSBXJDHJ",Visible=.t., Left=10, Top=326,;
    Alignment=1, Width=155, Height=18,;
    Caption="Descrizione causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="VGJGOHJLOJ",Visible=.t., Left=10, Top=356,;
    Alignment=1, Width=155, Height=18,;
    Caption="Descrizione intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="IHUBKRCUIC",Visible=.t., Left=14, Top=412,;
    Alignment=1, Width=142, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="FHTCJTCBMC",Visible=.t., Left=53, Top=450,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipo fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="KJBTOEMDSW",Visible=.t., Left=17, Top=487,;
    Alignment=1, Width=139, Height=18,;
    Caption="Tipo estrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="ZTOGDWXYCN",Visible=.t., Left=348, Top=450,;
    Alignment=1, Width=144, Height=18,;
    Caption="Importo non frazionato:"  ;
  , bGlobalFont=.t.

  add object oBox_2_4 as StdBox with uid="QHEGMXSNWS",left=-2, top=400, width=804,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_kos','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
