* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bed                                                        *
*              Estrazione dati comunicazione paesi a fisclit� privilegiata     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-23                                                      *
* Last revis.: 2010-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bed",oParentObject,m.pParam)
return(i_retval)

define class tgsai_bed as StdBatch
  * --- Local variables
  pParam = space(20)
  w_DTSERIAL = space(10)
  w_SERIAL = 0
  w_NUMMOV = 0
  w_NUMMOVD = 0
  w_MESE_DATINI = 0
  w_ANNO_DATINI = 0
  w_OLDVERSIONE = .f.
  w_NUMSER = 0
  w_AGGSER = space(10)
  w_DTTIPCON = space(1)
  w_DTCODCON = space(15)
  w_NORAGSOG = space(10)
  * --- WorkFile variables
  TMPPNT_IVA_idx=0
  ANTIVAAN_idx=0
  ANTIVADT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_MESE_DATINI = MONTH(this.oParentObject.w_PADATINI)
    this.w_ANNO_DATINI = YEAR(this.oParentObject.w_PADATINI)
    this.oParentObject.w_Msg = ""
    if this.pParam="UPDATE"
      AddMsg("Fase 1: Aggiorno dati anagrafici"+chr(13),this)
      * --- Write into ANTIVAAN
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ANTIVAAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAAN_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DTSERIAL"
        do vq_exec with 'GSAIUBED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ANTIVAAN_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ANTIVAAN.DTSERIAL = _t2.DTSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DTDESCRI = _t2.DTDESCRI";
            +",DTPERFIS = _t2.DTPERFIS";
            +",DT__NOME = _t2.DT__NOME";
            +",DTDATNAS = _t2.DTDATNAS";
            +",DTLOCNAS = _t2.DTLOCNAS";
            +",DTPRONAS = _t2.DTPRONAS";
            +",DTCODEST = _t2.DTCODEST";
            +",DTSTAFED = _t2.DTSTAFED";
            +",DTLOCRES = _t2.DTLOCRES";
            +",DTINDIRI = _t2.DTINDIRI";
            +",DTPARIVA = _t2.DTPARIVA";
            +",DTCODFIS = _t2.DTCODFIS";
            +i_ccchkf;
            +" from "+i_cTable+" ANTIVAAN, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ANTIVAAN.DTSERIAL = _t2.DTSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANTIVAAN, "+i_cQueryTable+" _t2 set ";
            +"ANTIVAAN.DTDESCRI = _t2.DTDESCRI";
            +",ANTIVAAN.DTPERFIS = _t2.DTPERFIS";
            +",ANTIVAAN.DT__NOME = _t2.DT__NOME";
            +",ANTIVAAN.DTDATNAS = _t2.DTDATNAS";
            +",ANTIVAAN.DTLOCNAS = _t2.DTLOCNAS";
            +",ANTIVAAN.DTPRONAS = _t2.DTPRONAS";
            +",ANTIVAAN.DTCODEST = _t2.DTCODEST";
            +",ANTIVAAN.DTSTAFED = _t2.DTSTAFED";
            +",ANTIVAAN.DTLOCRES = _t2.DTLOCRES";
            +",ANTIVAAN.DTINDIRI = _t2.DTINDIRI";
            +",ANTIVAAN.DTPARIVA = _t2.DTPARIVA";
            +",ANTIVAAN.DTCODFIS = _t2.DTCODFIS";
            +Iif(Empty(i_ccchkf),"",",ANTIVAAN.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ANTIVAAN.DTSERIAL = t2.DTSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANTIVAAN set (";
            +"DTDESCRI,";
            +"DTPERFIS,";
            +"DT__NOME,";
            +"DTDATNAS,";
            +"DTLOCNAS,";
            +"DTPRONAS,";
            +"DTCODEST,";
            +"DTSTAFED,";
            +"DTLOCRES,";
            +"DTINDIRI,";
            +"DTPARIVA,";
            +"DTCODFIS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DTDESCRI,";
            +"t2.DTPERFIS,";
            +"t2.DT__NOME,";
            +"t2.DTDATNAS,";
            +"t2.DTLOCNAS,";
            +"t2.DTPRONAS,";
            +"t2.DTCODEST,";
            +"t2.DTSTAFED,";
            +"t2.DTLOCRES,";
            +"t2.DTINDIRI,";
            +"t2.DTPARIVA,";
            +"t2.DTCODFIS";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ANTIVAAN.DTSERIAL = _t2.DTSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANTIVAAN set ";
            +"DTDESCRI = _t2.DTDESCRI";
            +",DTPERFIS = _t2.DTPERFIS";
            +",DT__NOME = _t2.DT__NOME";
            +",DTDATNAS = _t2.DTDATNAS";
            +",DTLOCNAS = _t2.DTLOCNAS";
            +",DTPRONAS = _t2.DTPRONAS";
            +",DTCODEST = _t2.DTCODEST";
            +",DTSTAFED = _t2.DTSTAFED";
            +",DTLOCRES = _t2.DTLOCRES";
            +",DTINDIRI = _t2.DTINDIRI";
            +",DTPARIVA = _t2.DTPARIVA";
            +",DTCODFIS = _t2.DTCODFIS";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DTSERIAL = "+i_cQueryTable+".DTSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DTDESCRI = (select DTDESCRI from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTPERFIS = (select DTPERFIS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DT__NOME = (select DT__NOME from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTDATNAS = (select DTDATNAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTLOCNAS = (select DTLOCNAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTPRONAS = (select DTPRONAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTCODEST = (select DTCODEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTSTAFED = (select DTSTAFED from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTLOCRES = (select DTLOCRES from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTINDIRI = (select DTINDIRI from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTPARIVA = (select DTPARIVA from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTCODFIS = (select DTCODFIS from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      ah_ErrorMsg("Aggiornamento completato")
    else
      AddMsgNL("Fase 1: Inizializzazione e controlli",this)
      * --- Controllo se esistono gi� dei movimenti che hanno prodotto comunciazione
      vq_exec("..\aift\exe\query\gsai9bed", this, "_Curs_GenANTIVA")
      if USED( "_Curs_GenANTIVA")
        this.w_DTSERIAL = _Curs_GenANTIVA.DTSERIAL
        USE IN _Curs_GenANTIVA
        if NOT EMPTY(NVL(this.w_DTSERIAL,space(10)))
          ah_ErrorMsg("Per il periodo selezionato esistono gi� dei movimenti comunicati. Eliminare la comunicazione")
          i_retcode = 'stop'
          return
        endif
        this.w_DTSERIAL = space(10)
      endif
      * --- Inizio elaborazione
      AddMsgNL("Fase 2: estrazione dati",this)
      * --- Create temporary table TMPPNT_IVA
      i_nIdx=cp_AddTableDef('TMPPNT_IVA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\aift\exe\query\gsai_bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPPNT_IVA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Gestione eccezioni
      AddMsgNL("Fase 3: gestione eccezioni",this)
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DTRIFPNT,IVCODIVA,DTTIPREG"
        do vq_exec with 'GSAI8BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.IVCODIVA = _t2.IVCODIVA";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DTBIMPTT = _t2.DTBIMPTT";
            +",DTBIMPIM = _t2.DTBIMPIM";
            +",DTSIMPTT = _t2.DTSIMPTT";
            +",DTSIMPIM = _t2.DTSIMPIM";
            +",DTBNOIMP = _t2.DTBNOIMP";
            +",DTSNOIMP = _t2.DTSNOIMP";
            +",DTOPESEN = _t2.DTOPESEN";
            +",DTBNOSOG = _t2.DTBNOSOG";
            +",DTSNOSOG = _t2.DTSNOSOG";
            +",DTBNVAIM = _t2.DTBNVAIM";
            +",DTSNVAIM = _t2.DTSNVAIM";
            +",DTBNVPIM = _t2.DTBNVPIM";
            +",DTSNVPIM = _t2.DTSNVPIM";
            +",GENERA = _t2.GENERA";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.IVCODIVA = _t2.IVCODIVA";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.DTBIMPTT = _t2.DTBIMPTT";
            +",TMPPNT_IVA.DTBIMPIM = _t2.DTBIMPIM";
            +",TMPPNT_IVA.DTSIMPTT = _t2.DTSIMPTT";
            +",TMPPNT_IVA.DTSIMPIM = _t2.DTSIMPIM";
            +",TMPPNT_IVA.DTBNOIMP = _t2.DTBNOIMP";
            +",TMPPNT_IVA.DTSNOIMP = _t2.DTSNOIMP";
            +",TMPPNT_IVA.DTOPESEN = _t2.DTOPESEN";
            +",TMPPNT_IVA.DTBNOSOG = _t2.DTBNOSOG";
            +",TMPPNT_IVA.DTSNOSOG = _t2.DTSNOSOG";
            +",TMPPNT_IVA.DTBNVAIM = _t2.DTBNVAIM";
            +",TMPPNT_IVA.DTSNVAIM = _t2.DTSNVAIM";
            +",TMPPNT_IVA.DTBNVPIM = _t2.DTBNVPIM";
            +",TMPPNT_IVA.DTSNVPIM = _t2.DTSNVPIM";
            +",TMPPNT_IVA.GENERA = _t2.GENERA";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DTRIFPNT = t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.IVCODIVA = t2.IVCODIVA";
                +" and "+"TMPPNT_IVA.DTTIPREG = t2.DTTIPREG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"DTBIMPTT,";
            +"DTBIMPIM,";
            +"DTSIMPTT,";
            +"DTSIMPIM,";
            +"DTBNOIMP,";
            +"DTSNOIMP,";
            +"DTOPESEN,";
            +"DTBNOSOG,";
            +"DTSNOSOG,";
            +"DTBNVAIM,";
            +"DTSNVAIM,";
            +"DTBNVPIM,";
            +"DTSNVPIM,";
            +"GENERA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DTBIMPTT,";
            +"t2.DTBIMPIM,";
            +"t2.DTSIMPTT,";
            +"t2.DTSIMPIM,";
            +"t2.DTBNOIMP,";
            +"t2.DTSNOIMP,";
            +"t2.DTOPESEN,";
            +"t2.DTBNOSOG,";
            +"t2.DTSNOSOG,";
            +"t2.DTBNVAIM,";
            +"t2.DTSNVAIM,";
            +"t2.DTBNVPIM,";
            +"t2.DTSNVPIM,";
            +"t2.GENERA";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.IVCODIVA = _t2.IVCODIVA";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"DTBIMPTT = _t2.DTBIMPTT";
            +",DTBIMPIM = _t2.DTBIMPIM";
            +",DTSIMPTT = _t2.DTSIMPTT";
            +",DTSIMPIM = _t2.DTSIMPIM";
            +",DTBNOIMP = _t2.DTBNOIMP";
            +",DTSNOIMP = _t2.DTSNOIMP";
            +",DTOPESEN = _t2.DTOPESEN";
            +",DTBNOSOG = _t2.DTBNOSOG";
            +",DTSNOSOG = _t2.DTSNOSOG";
            +",DTBNVAIM = _t2.DTBNVAIM";
            +",DTSNVAIM = _t2.DTSNVAIM";
            +",DTBNVPIM = _t2.DTBNVPIM";
            +",DTSNVPIM = _t2.DTSNVPIM";
            +",GENERA = _t2.GENERA";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DTRIFPNT = "+i_cQueryTable+".DTRIFPNT";
                +" and "+i_cTable+".IVCODIVA = "+i_cQueryTable+".IVCODIVA";
                +" and "+i_cTable+".DTTIPREG = "+i_cQueryTable+".DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DTBIMPTT = (select DTBIMPTT from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTBIMPIM = (select DTBIMPIM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTSIMPTT = (select DTSIMPTT from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTSIMPIM = (select DTSIMPIM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTBNOIMP = (select DTBNOIMP from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTSNOIMP = (select DTSNOIMP from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTOPESEN = (select DTOPESEN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTBNOSOG = (select DTBNOSOG from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTSNOSOG = (select DTSNOSOG from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTBNVAIM = (select DTBNVAIM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTSNVAIM = (select DTSNVAIM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTBNVPIM = (select DTBNVPIM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DTSNVPIM = (select DTSNVPIM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",GENERA = (select GENERA from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Elimino dal cursore temporaneo TMPPNT_IVA tutti i record che ha il flag
      *     "Genera" uguale ad 'N'
      * --- Delete from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"GENERA = "+cp_ToStrODBC("N");
               )
      else
        delete from (i_cTable) where;
              GENERA = "N";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Controllo mesi a 0
      *     Controllo movimenti con imponibile valorizzato e imposta a 0
      VQ_EXEC("..\aift\exe\query\gsaisbed", this, "__TMP__")
      if USED("__TMP__")
        if RECCOUNT("__TMP__")>0 AND ah_YesNo("Nel periodo esistono delle registrazione con avvertimento.%0Si vuole procedere con la stampa di dettaglio?")
          CP_CHPRN("..\aift\exe\query\gsaisbed", , this.oParentObject)
        endif
        USE IN __TMP__
      endif
      * --- Select from GSAI4BED
      do vq_exec with 'GSAI4BED',this,'_Curs_GSAI4BED','',.f.,.t.
      if used('_Curs_GSAI4BED')
        select _Curs_GSAI4BED
        locate for 1=1
        do while not(eof())
        this.w_NUMMOV = NUMERO
          select _Curs_GSAI4BED
          continue
        enddo
        use
      endif
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2],.t.,this.TMPPNT_IVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select count(distinct DTRIFPNT) as NUMERO  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select count(distinct DTRIFPNT) as NUMERO from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NUMMOVD = NUMERO
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
      AddMsgNL("Fase 4: eliminazione estrazioni precedenti",this)
      if this.oParentObject.w_ELIDATI="S"
        * --- Delete from ANTIVADT
        i_nConn=i_TableProp[this.ANTIVADT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVADT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.ANTIVAAN_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='DTSERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"DTSERIAL "," from "+i_cQueryTable+" where DT__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND DT__MESE="+cp_ToStrODBC(this.oParentObject.w_MESE)+" AND DTTRIMES="+cp_ToStrODBC(this.oParentObject.w_TRIMES)+"",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".DTSERIAL = "+i_cQueryTable+".DTSERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".DTSERIAL = "+i_cQueryTable+".DTSERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from ANTIVAAN
        i_nConn=i_TableProp[this.ANTIVAAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAAN_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DT__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                +" and DT__MESE = "+cp_ToStrODBC(this.oParentObject.w_MESE);
                +" and DTTRIMES = "+cp_ToStrODBC(this.oParentObject.w_TRIMES);
                 )
        else
          delete from (i_cTable) where;
                DT__ANNO = this.oParentObject.w_ANNO;
                and DT__MESE = this.oParentObject.w_MESE;
                and DTTRIMES = this.oParentObject.w_TRIMES;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      if this.w_NUMMOV=0
        ah_ErrorMsg("Nessun movimento da generare")
      else
        * --- Try
        local bErr_04ADDA90
        bErr_04ADDA90=bTrsErr
        this.Try_04ADDA90()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Errore nella crezione dei dati estratti (%1)", "!", "", i_trsmsg)
        endif
        bTrsErr=bTrsErr or bErr_04ADDA90
        * --- End
      endif
      if USED("ChkVersion")
        USE IN ChkVersion
      endif
      * --- Drop temporary table TMPPNT_IVA
      i_nIdx=cp_GetTableDefIdx('TMPPNT_IVA')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPNT_IVA')
      endif
    endif
  endproc
  proc Try_04ADDA90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_DTSERIAL = space(10)
    w_Conn=i_TableProp[this.ANTIVAAN_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "ANTIV", "i_codazi,w_DTSERIAL")
    this.w_SERIAL = VAL(this.w_DTSERIAL)-1
    this.w_DTSERIAL = cp_StrZeroPad( this.w_SERIAL+this.w_NUMMOV ,10)
    cp_NextTableProg(this, w_Conn, "ANTIV", "i_codazi,w_DTSERIAL")
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_OLDVERSIONE = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_OLDVERSIONE = .t.
      endif
    endif
    AddMsgNL("Fase 5: generazione progressivi",this)
    if this.w_OLDVERSIONE
      * --- Select from TMPPNT_IVA
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2],.t.,this.TMPPNT_IVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select distinct DTTIPCON,DTCODCON, NORAGSOG  from "+i_cTable+" TMPPNT_IVA ";
             ,"_Curs_TMPPNT_IVA")
      else
        select distinct DTTIPCON,DTCODCON, NORAGSOG from (i_cTable);
          into cursor _Curs_TMPPNT_IVA
      endif
      if used('_Curs_TMPPNT_IVA')
        select _Curs_TMPPNT_IVA
        locate for 1=1
        do while not(eof())
        this.w_NUMSER = this.w_NUMSER + 1
        this.w_DTTIPCON = _Curs_TMPPNT_IVA.DTTIPCON
        this.w_DTCODCON = _Curs_TMPPNT_IVA.DTCODCON
        this.w_NORAGSOG = _Curs_TMPPNT_IVA.NORAGSOG
        this.w_AGGSER = cp_StrZeroPad(this.w_NUMSER + this.w_SERIAL, 10)
        * --- Write into TMPPNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DTSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPPNT_IVA','DTSERIAL');
              +i_ccchkf ;
          +" where ";
              +"DTTIPCON = "+cp_ToStrODBC(this.w_DTTIPCON);
              +" and DTCODCON = "+cp_ToStrODBC(this.w_DTCODCON);
              +" and NORAGSOG = "+cp_ToStrODBC(this.w_NORAGSOG);
                 )
        else
          update (i_cTable) set;
              DTSERIAL = this.w_AGGSER;
              &i_ccchkf. ;
           where;
              DTTIPCON = this.w_DTTIPCON;
              and DTCODCON = this.w_DTCODCON;
              and NORAGSOG = this.w_NORAGSOG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_TMPPNT_IVA
          continue
        enddo
        use
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DTTIPCON,DTCODCON,NORAGSOG"
        do vq_exec with 'GSAI5BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DTTIPCON = _t2.DTTIPCON";
                +" and "+"TMPPNT_IVA.DTCODCON = _t2.DTCODCON";
                +" and "+"TMPPNT_IVA.NORAGSOG = _t2.NORAGSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DTSERIAL = _t2.DTSERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DTTIPCON = _t2.DTTIPCON";
                +" and "+"TMPPNT_IVA.DTCODCON = _t2.DTCODCON";
                +" and "+"TMPPNT_IVA.NORAGSOG = _t2.NORAGSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.DTSERIAL = _t2.DTSERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DTTIPCON = t2.DTTIPCON";
                +" and "+"TMPPNT_IVA.DTCODCON = t2.DTCODCON";
                +" and "+"TMPPNT_IVA.NORAGSOG = t2.NORAGSOG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"DTSERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DTSERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DTTIPCON = _t2.DTTIPCON";
                +" and "+"TMPPNT_IVA.DTCODCON = _t2.DTCODCON";
                +" and "+"TMPPNT_IVA.NORAGSOG = _t2.NORAGSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"DTSERIAL = _t2.DTSERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DTTIPCON = "+i_cQueryTable+".DTTIPCON";
                +" and "+i_cTable+".DTCODCON = "+i_cQueryTable+".DTCODCON";
                +" and "+i_cTable+".NORAGSOG = "+i_cQueryTable+".NORAGSOG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DTSERIAL = (select DTSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    AddMsgNL("Fase 6: inserimento dati",this)
    * --- Insert into ANTIVAAN
    i_nConn=i_TableProp[this.ANTIVAAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAAN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai2bed",this.ANTIVAAN_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_OLDVERSIONE
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DTSERIAL,DTRIFPNT,DTTIPREG"
        do vq_exec with 'GSAI6BED1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DTSERIAL = _t2.DTSERIAL";
                +" and "+"TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DTSERIAL = _t2.DTSERIAL";
                +" and "+"TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DTSERIAL = t2.DTSERIAL";
                +" and "+"TMPPNT_IVA.DTRIFPNT = t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.DTTIPREG = t2.DTTIPREG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DTSERIAL = _t2.DTSERIAL";
                +" and "+"TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DTSERIAL = "+i_cQueryTable+".DTSERIAL";
                +" and "+i_cTable+".DTRIFPNT = "+i_cQueryTable+".DTRIFPNT";
                +" and "+i_cTable+".DTTIPREG = "+i_cQueryTable+".DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPPNT_IVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DTSERIAL,DTRIFPNT,DTTIPREG"
        do vq_exec with 'GSAI6BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_IVA.DTSERIAL = _t2.DTSERIAL";
                +" and "+"TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_IVA.DTSERIAL = _t2.DTSERIAL";
                +" and "+"TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_IVA.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_IVA.DTSERIAL = t2.DTSERIAL";
                +" and "+"TMPPNT_IVA.DTRIFPNT = t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.DTTIPREG = t2.DTTIPREG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_IVA.DTSERIAL = _t2.DTSERIAL";
                +" and "+"TMPPNT_IVA.DTRIFPNT = _t2.DTRIFPNT";
                +" and "+"TMPPNT_IVA.DTTIPREG = _t2.DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_IVA set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DTSERIAL = "+i_cQueryTable+".DTSERIAL";
                +" and "+i_cTable+".DTRIFPNT = "+i_cQueryTable+".DTRIFPNT";
                +" and "+i_cTable+".DTTIPREG = "+i_cQueryTable+".DTTIPREG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into ANTIVADT
    i_nConn=i_TableProp[this.ANTIVADT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANTIVADT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\aift\exe\query\gsai3bed",this.ANTIVADT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Generazione completata.%0Creati n. %1 dati estratti da n. %2 documenti", "!", "", ALLTRIM(STR(this.w_NUMMOV)), ALLTRIM(STR(this.w_NUMMOVD)))
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*TMPPNT_IVA'
    this.cWorkTables[2]='ANTIVAAN'
    this.cWorkTables[3]='ANTIVADT'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSAI4BED')
      use in _Curs_GSAI4BED
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
