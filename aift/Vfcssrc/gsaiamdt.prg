* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsaiamdt                                                        *
*              Dettaglio dati estratti antievasione IVA                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsaiamdt")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsaiamdt")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsaiamdt")
  return

* --- Class definition
define class tgsaiamdt as StdPCForm
  Width  = 689
  Height = 217
  Top    = 9
  Left   = 7
  cComment = "Dettaglio dati estratti antievasione IVA"
  cPrg = "gsaiamdt"
  HelpContextID=80217961
  add object cnt as tcgsaiamdt
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsaiamdt as PCContext
  w_DTSERIAL = space(10)
  w_DTNUMDOC = 0
  w_DTDATDOC = space(8)
  w_DTALFDOC = space(10)
  w_DTRIFPNT = space(10)
  w_DTBIMPTT = 0
  w_DTBIMPIM = 0
  w_DTSIMPTT = 0
  w_DTSIMPIM = 0
  w_DTBNOIMP = 0
  w_DTSNOIMP = 0
  w_DTOPESEN = 0
  w_DTBNOSOG = 0
  w_DTSNOSOG = 0
  w_DTTIPOPE = space(1)
  w_DTTIPREG = space(1)
  proc Save(i_oFrom)
    this.w_DTSERIAL = i_oFrom.w_DTSERIAL
    this.w_DTNUMDOC = i_oFrom.w_DTNUMDOC
    this.w_DTDATDOC = i_oFrom.w_DTDATDOC
    this.w_DTALFDOC = i_oFrom.w_DTALFDOC
    this.w_DTRIFPNT = i_oFrom.w_DTRIFPNT
    this.w_DTBIMPTT = i_oFrom.w_DTBIMPTT
    this.w_DTBIMPIM = i_oFrom.w_DTBIMPIM
    this.w_DTSIMPTT = i_oFrom.w_DTSIMPTT
    this.w_DTSIMPIM = i_oFrom.w_DTSIMPIM
    this.w_DTBNOIMP = i_oFrom.w_DTBNOIMP
    this.w_DTSNOIMP = i_oFrom.w_DTSNOIMP
    this.w_DTOPESEN = i_oFrom.w_DTOPESEN
    this.w_DTBNOSOG = i_oFrom.w_DTBNOSOG
    this.w_DTSNOSOG = i_oFrom.w_DTSNOSOG
    this.w_DTTIPOPE = i_oFrom.w_DTTIPOPE
    this.w_DTTIPREG = i_oFrom.w_DTTIPREG
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DTSERIAL = this.w_DTSERIAL
    i_oTo.w_DTNUMDOC = this.w_DTNUMDOC
    i_oTo.w_DTDATDOC = this.w_DTDATDOC
    i_oTo.w_DTALFDOC = this.w_DTALFDOC
    i_oTo.w_DTRIFPNT = this.w_DTRIFPNT
    i_oTo.w_DTBIMPTT = this.w_DTBIMPTT
    i_oTo.w_DTBIMPIM = this.w_DTBIMPIM
    i_oTo.w_DTSIMPTT = this.w_DTSIMPTT
    i_oTo.w_DTSIMPIM = this.w_DTSIMPIM
    i_oTo.w_DTBNOIMP = this.w_DTBNOIMP
    i_oTo.w_DTSNOIMP = this.w_DTSNOIMP
    i_oTo.w_DTOPESEN = this.w_DTOPESEN
    i_oTo.w_DTBNOSOG = this.w_DTBNOSOG
    i_oTo.w_DTSNOSOG = this.w_DTSNOSOG
    i_oTo.w_DTTIPOPE = this.w_DTTIPOPE
    i_oTo.w_DTTIPREG = this.w_DTTIPREG
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsaiamdt as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 689
  Height = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=80217961
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ANTIVADT_IDX = 0
  cFile = "ANTIVADT"
  cKeySelect = "DTSERIAL,DTTIPOPE,DTTIPREG"
  cQueryFilter="DTTIPOPE='A' AND DTTIPREG='A'"
  cKeyWhere  = "DTSERIAL=this.w_DTSERIAL and DTTIPOPE=this.w_DTTIPOPE and DTTIPREG=this.w_DTTIPREG"
  cKeyDetail  = "DTSERIAL=this.w_DTSERIAL and DTTIPOPE=this.w_DTTIPOPE and DTTIPREG=this.w_DTTIPREG"
  cKeyWhereODBC = '"DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';
      +'+" and DTTIPOPE="+cp_ToStrODBC(this.w_DTTIPOPE)';
      +'+" and DTTIPREG="+cp_ToStrODBC(this.w_DTTIPREG)';

  cKeyDetailWhereODBC = '"DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';
      +'+" and DTTIPOPE="+cp_ToStrODBC(this.w_DTTIPOPE)';
      +'+" and DTTIPREG="+cp_ToStrODBC(this.w_DTTIPREG)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ANTIVADT.DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';
      +'+" and ANTIVADT.DTTIPOPE="+cp_ToStrODBC(this.w_DTTIPOPE)';
      +'+" and ANTIVADT.DTTIPREG="+cp_ToStrODBC(this.w_DTTIPREG)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ANTIVADT.DTDATDOC,ANTIVADT.DTNUMDOC,ANTIVADT.DTALFDOC'
  cPrg = "gsaiamdt"
  cComment = "Dettaglio dati estratti antievasione IVA"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DTSERIAL = space(10)
  w_DTNUMDOC = 0
  w_DTDATDOC = ctod('  /  /  ')
  w_DTALFDOC = space(10)
  w_DTRIFPNT = space(10)
  w_DTBIMPTT = 0
  w_DTBIMPIM = 0
  w_DTSIMPTT = 0
  w_DTSIMPIM = 0
  w_DTBNOIMP = 0
  w_DTSNOIMP = 0
  w_DTOPESEN = 0
  w_DTBNOSOG = 0
  w_DTSNOSOG = 0
  w_DTTIPOPE = space(1)
  w_DTTIPREG = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsaiamdtPag1","gsaiamdt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ANTIVADT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVADT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVADT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsaiamdt'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ANTIVADT where DTSERIAL=KeySet.DTSERIAL
    *                            and DTTIPOPE=KeySet.DTTIPOPE
    *                            and DTTIPREG=KeySet.DTTIPREG
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2],this.bLoadRecFilter,this.ANTIVADT_IDX,"gsaiamdt")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVADT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVADT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVADT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DTSERIAL',this.w_DTSERIAL  ,'DTTIPOPE',this.w_DTTIPOPE  ,'DTTIPREG',this.w_DTTIPREG  )
      select * from (i_cTable) ANTIVADT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DTSERIAL = NVL(DTSERIAL,space(10))
        .w_DTTIPOPE = NVL(DTTIPOPE,space(1))
        .w_DTTIPREG = NVL(DTTIPREG,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ANTIVADT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DTNUMDOC = NVL(DTNUMDOC,0)
          .w_DTDATDOC = NVL(cp_ToDate(DTDATDOC),ctod("  /  /  "))
          .w_DTALFDOC = NVL(DTALFDOC,space(10))
          .w_DTRIFPNT = NVL(DTRIFPNT,space(10))
          .w_DTBIMPTT = NVL(DTBIMPTT,0)
          .w_DTBIMPIM = NVL(DTBIMPIM,0)
          .w_DTSIMPTT = NVL(DTSIMPTT,0)
          .w_DTSIMPIM = NVL(DTSIMPIM,0)
          .w_DTBNOIMP = NVL(DTBNOIMP,0)
          .w_DTSNOIMP = NVL(DTSNOIMP,0)
          .w_DTOPESEN = NVL(DTOPESEN,0)
          .w_DTBNOSOG = NVL(DTBNOSOG,0)
          .w_DTSNOSOG = NVL(DTSNOSOG,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_5.enabled = .oPgFrm.Page1.oPag.oBtn_2_5.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DTSERIAL=space(10)
      .w_DTNUMDOC=0
      .w_DTDATDOC=ctod("  /  /  ")
      .w_DTALFDOC=space(10)
      .w_DTRIFPNT=space(10)
      .w_DTBIMPTT=0
      .w_DTBIMPIM=0
      .w_DTSIMPTT=0
      .w_DTSIMPIM=0
      .w_DTBNOIMP=0
      .w_DTSNOIMP=0
      .w_DTOPESEN=0
      .w_DTBNOSOG=0
      .w_DTSNOSOG=0
      .w_DTTIPOPE=space(1)
      .w_DTTIPREG=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVADT')
    this.DoRTCalc(1,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_5.enabled = this.oPgFrm.Page1.oPag.oBtn_2_5.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDTBIMPTT_2_6.enabled = i_bVal
      .Page1.oPag.oDTBIMPIM_2_7.enabled = i_bVal
      .Page1.oPag.oDTSIMPTT_2_8.enabled = i_bVal
      .Page1.oPag.oDTSIMPIM_2_9.enabled = i_bVal
      .Page1.oPag.oDTBNOIMP_2_10.enabled = i_bVal
      .Page1.oPag.oDTSNOIMP_2_11.enabled = i_bVal
      .Page1.oPag.oDTOPESEN_2_12.enabled = i_bVal
      .Page1.oPag.oDTBNOSOG_2_13.enabled = i_bVal
      .Page1.oPag.oDTSNOSOG_2_14.enabled = i_bVal
      .Page1.oPag.oBtn_2_5.enabled = .Page1.oPag.oBtn_2_5.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ANTIVADT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTSERIAL,"DTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTTIPOPE,"DTTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTTIPREG,"DTTIPREG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DTNUMDOC N(15);
      ,t_DTDATDOC D(8);
      ,t_DTALFDOC C(10);
      ,t_DTBIMPTT N(18,4);
      ,t_DTBIMPIM N(18,4);
      ,t_DTSIMPTT N(18,4);
      ,t_DTSIMPIM N(18,4);
      ,t_DTBNOIMP N(18,4);
      ,t_DTSNOIMP N(18,4);
      ,t_DTOPESEN N(18,4);
      ,t_DTBNOSOG N(18,4);
      ,t_DTSNOSOG N(18,4);
      ,CPROWNUM N(10);
      ,t_DTRIFPNT C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsaiamdtbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_1.controlsource=this.cTrsName+'.t_DTNUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDTDATDOC_2_2.controlsource=this.cTrsName+'.t_DTDATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDTALFDOC_2_3.controlsource=this.cTrsName+'.t_DTALFDOC'
    this.oPgFRm.Page1.oPag.oDTBIMPTT_2_6.controlsource=this.cTrsName+'.t_DTBIMPTT'
    this.oPgFRm.Page1.oPag.oDTBIMPIM_2_7.controlsource=this.cTrsName+'.t_DTBIMPIM'
    this.oPgFRm.Page1.oPag.oDTSIMPTT_2_8.controlsource=this.cTrsName+'.t_DTSIMPTT'
    this.oPgFRm.Page1.oPag.oDTSIMPIM_2_9.controlsource=this.cTrsName+'.t_DTSIMPIM'
    this.oPgFRm.Page1.oPag.oDTBNOIMP_2_10.controlsource=this.cTrsName+'.t_DTBNOIMP'
    this.oPgFRm.Page1.oPag.oDTSNOIMP_2_11.controlsource=this.cTrsName+'.t_DTSNOIMP'
    this.oPgFRm.Page1.oPag.oDTOPESEN_2_12.controlsource=this.cTrsName+'.t_DTOPESEN'
    this.oPgFRm.Page1.oPag.oDTBNOSOG_2_13.controlsource=this.cTrsName+'.t_DTBNOSOG'
    this.oPgFRm.Page1.oPag.oDTSNOSOG_2_14.controlsource=this.cTrsName+'.t_DTSNOSOG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(91)
    this.AddVLine(206)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
      *
      * insert into ANTIVADT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVADT')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVADT')
        i_cFldBody=" "+;
                  "(DTSERIAL,DTNUMDOC,DTDATDOC,DTALFDOC,DTRIFPNT"+;
                  ",DTBIMPTT,DTBIMPIM,DTSIMPTT,DTSIMPIM,DTBNOIMP"+;
                  ",DTSNOIMP,DTOPESEN,DTBNOSOG,DTSNOSOG,DTTIPOPE"+;
                  ",DTTIPREG,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DTSERIAL)+","+cp_ToStrODBC(this.w_DTNUMDOC)+","+cp_ToStrODBC(this.w_DTDATDOC)+","+cp_ToStrODBC(this.w_DTALFDOC)+","+cp_ToStrODBC(this.w_DTRIFPNT)+;
             ","+cp_ToStrODBC(this.w_DTBIMPTT)+","+cp_ToStrODBC(this.w_DTBIMPIM)+","+cp_ToStrODBC(this.w_DTSIMPTT)+","+cp_ToStrODBC(this.w_DTSIMPIM)+","+cp_ToStrODBC(this.w_DTBNOIMP)+;
             ","+cp_ToStrODBC(this.w_DTSNOIMP)+","+cp_ToStrODBC(this.w_DTOPESEN)+","+cp_ToStrODBC(this.w_DTBNOSOG)+","+cp_ToStrODBC(this.w_DTSNOSOG)+","+cp_ToStrODBC(this.w_DTTIPOPE)+;
             ","+cp_ToStrODBC(this.w_DTTIPREG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVADT')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVADT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DTSERIAL',this.w_DTSERIAL,'DTTIPOPE',this.w_DTTIPOPE,'DTTIPREG',this.w_DTTIPREG)
        INSERT INTO (i_cTable) (;
                   DTSERIAL;
                  ,DTNUMDOC;
                  ,DTDATDOC;
                  ,DTALFDOC;
                  ,DTRIFPNT;
                  ,DTBIMPTT;
                  ,DTBIMPIM;
                  ,DTSIMPTT;
                  ,DTSIMPIM;
                  ,DTBNOIMP;
                  ,DTSNOIMP;
                  ,DTOPESEN;
                  ,DTBNOSOG;
                  ,DTSNOSOG;
                  ,DTTIPOPE;
                  ,DTTIPREG;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DTSERIAL;
                  ,this.w_DTNUMDOC;
                  ,this.w_DTDATDOC;
                  ,this.w_DTALFDOC;
                  ,this.w_DTRIFPNT;
                  ,this.w_DTBIMPTT;
                  ,this.w_DTBIMPIM;
                  ,this.w_DTSIMPTT;
                  ,this.w_DTSIMPIM;
                  ,this.w_DTBNOIMP;
                  ,this.w_DTSNOIMP;
                  ,this.w_DTOPESEN;
                  ,this.w_DTBNOSOG;
                  ,this.w_DTSNOSOG;
                  ,this.w_DTTIPOPE;
                  ,this.w_DTTIPREG;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_DTDATDOC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVADT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVADT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_DTDATDOC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ANTIVADT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVADT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DTNUMDOC="+cp_ToStrODBC(this.w_DTNUMDOC)+;
                     ",DTDATDOC="+cp_ToStrODBC(this.w_DTDATDOC)+;
                     ",DTALFDOC="+cp_ToStrODBC(this.w_DTALFDOC)+;
                     ",DTRIFPNT="+cp_ToStrODBC(this.w_DTRIFPNT)+;
                     ",DTBIMPTT="+cp_ToStrODBC(this.w_DTBIMPTT)+;
                     ",DTBIMPIM="+cp_ToStrODBC(this.w_DTBIMPIM)+;
                     ",DTSIMPTT="+cp_ToStrODBC(this.w_DTSIMPTT)+;
                     ",DTSIMPIM="+cp_ToStrODBC(this.w_DTSIMPIM)+;
                     ",DTBNOIMP="+cp_ToStrODBC(this.w_DTBNOIMP)+;
                     ",DTSNOIMP="+cp_ToStrODBC(this.w_DTSNOIMP)+;
                     ",DTOPESEN="+cp_ToStrODBC(this.w_DTOPESEN)+;
                     ",DTBNOSOG="+cp_ToStrODBC(this.w_DTBNOSOG)+;
                     ",DTSNOSOG="+cp_ToStrODBC(this.w_DTSNOSOG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVADT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DTNUMDOC=this.w_DTNUMDOC;
                     ,DTDATDOC=this.w_DTDATDOC;
                     ,DTALFDOC=this.w_DTALFDOC;
                     ,DTRIFPNT=this.w_DTRIFPNT;
                     ,DTBIMPTT=this.w_DTBIMPTT;
                     ,DTBIMPIM=this.w_DTBIMPIM;
                     ,DTSIMPTT=this.w_DTSIMPTT;
                     ,DTSIMPIM=this.w_DTSIMPIM;
                     ,DTBNOIMP=this.w_DTBNOIMP;
                     ,DTSNOIMP=this.w_DTSNOIMP;
                     ,DTOPESEN=this.w_DTOPESEN;
                     ,DTBNOSOG=this.w_DTBNOSOG;
                     ,DTSNOSOG=this.w_DTSNOSOG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_DTDATDOC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ANTIVADT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_DTDATDOC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVADT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVADT_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DTRIFPNT with this.w_DTRIFPNT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_5.enabled =this.oPgFrm.Page1.oPag.oBtn_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDTBIMPTT_2_6.visible=!this.oPgFrm.Page1.oPag.oDTBIMPTT_2_6.mHide()
    this.oPgFrm.Page1.oPag.oDTBIMPIM_2_7.visible=!this.oPgFrm.Page1.oPag.oDTBIMPIM_2_7.mHide()
    this.oPgFrm.Page1.oPag.oDTSIMPTT_2_8.visible=!this.oPgFrm.Page1.oPag.oDTSIMPTT_2_8.mHide()
    this.oPgFrm.Page1.oPag.oDTSIMPIM_2_9.visible=!this.oPgFrm.Page1.oPag.oDTSIMPIM_2_9.mHide()
    this.oPgFrm.Page1.oPag.oDTBNOIMP_2_10.visible=!this.oPgFrm.Page1.oPag.oDTBNOIMP_2_10.mHide()
    this.oPgFrm.Page1.oPag.oDTSNOIMP_2_11.visible=!this.oPgFrm.Page1.oPag.oDTSNOIMP_2_11.mHide()
    this.oPgFrm.Page1.oPag.oDTOPESEN_2_12.visible=!this.oPgFrm.Page1.oPag.oDTOPESEN_2_12.mHide()
    this.oPgFrm.Page1.oPag.oDTBNOSOG_2_13.visible=!this.oPgFrm.Page1.oPag.oDTBNOSOG_2_13.mHide()
    this.oPgFrm.Page1.oPag.oDTSNOSOG_2_14.visible=!this.oPgFrm.Page1.oPag.oDTSNOSOG_2_14.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDTBIMPTT_2_6.value==this.w_DTBIMPTT)
      this.oPgFrm.Page1.oPag.oDTBIMPTT_2_6.value=this.w_DTBIMPTT
      replace t_DTBIMPTT with this.oPgFrm.Page1.oPag.oDTBIMPTT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTBIMPIM_2_7.value==this.w_DTBIMPIM)
      this.oPgFrm.Page1.oPag.oDTBIMPIM_2_7.value=this.w_DTBIMPIM
      replace t_DTBIMPIM with this.oPgFrm.Page1.oPag.oDTBIMPIM_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSIMPTT_2_8.value==this.w_DTSIMPTT)
      this.oPgFrm.Page1.oPag.oDTSIMPTT_2_8.value=this.w_DTSIMPTT
      replace t_DTSIMPTT with this.oPgFrm.Page1.oPag.oDTSIMPTT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSIMPIM_2_9.value==this.w_DTSIMPIM)
      this.oPgFrm.Page1.oPag.oDTSIMPIM_2_9.value=this.w_DTSIMPIM
      replace t_DTSIMPIM with this.oPgFrm.Page1.oPag.oDTSIMPIM_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTBNOIMP_2_10.value==this.w_DTBNOIMP)
      this.oPgFrm.Page1.oPag.oDTBNOIMP_2_10.value=this.w_DTBNOIMP
      replace t_DTBNOIMP with this.oPgFrm.Page1.oPag.oDTBNOIMP_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSNOIMP_2_11.value==this.w_DTSNOIMP)
      this.oPgFrm.Page1.oPag.oDTSNOIMP_2_11.value=this.w_DTSNOIMP
      replace t_DTSNOIMP with this.oPgFrm.Page1.oPag.oDTSNOIMP_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTOPESEN_2_12.value==this.w_DTOPESEN)
      this.oPgFrm.Page1.oPag.oDTOPESEN_2_12.value=this.w_DTOPESEN
      replace t_DTOPESEN with this.oPgFrm.Page1.oPag.oDTOPESEN_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTBNOSOG_2_13.value==this.w_DTBNOSOG)
      this.oPgFrm.Page1.oPag.oDTBNOSOG_2_13.value=this.w_DTBNOSOG
      replace t_DTBNOSOG with this.oPgFrm.Page1.oPag.oDTBNOSOG_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSNOSOG_2_14.value==this.w_DTSNOSOG)
      this.oPgFrm.Page1.oPag.oDTSNOSOG_2_14.value=this.w_DTSNOSOG
      replace t_DTSNOSOG with this.oPgFrm.Page1.oPag.oDTSNOSOG_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_1.value==this.w_DTNUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_1.value=this.w_DTNUMDOC
      replace t_DTNUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTNUMDOC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTDATDOC_2_2.value==this.w_DTDATDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTDATDOC_2_2.value=this.w_DTDATDOC
      replace t_DTDATDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTDATDOC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTALFDOC_2_3.value==this.w_DTALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTALFDOC_2_3.value=this.w_DTALFDOC
      replace t_DTALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDTALFDOC_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVADT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_DTDATDOC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_DTDATDOC))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DTNUMDOC=0
      .w_DTDATDOC=ctod("  /  /  ")
      .w_DTALFDOC=space(10)
      .w_DTRIFPNT=space(10)
      .w_DTBIMPTT=0
      .w_DTBIMPIM=0
      .w_DTSIMPTT=0
      .w_DTSIMPIM=0
      .w_DTBNOIMP=0
      .w_DTSNOIMP=0
      .w_DTOPESEN=0
      .w_DTBNOSOG=0
      .w_DTSNOSOG=0
    endwith
    this.DoRTCalc(1,16,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DTNUMDOC = t_DTNUMDOC
    this.w_DTDATDOC = t_DTDATDOC
    this.w_DTALFDOC = t_DTALFDOC
    this.w_DTRIFPNT = t_DTRIFPNT
    this.w_DTBIMPTT = t_DTBIMPTT
    this.w_DTBIMPIM = t_DTBIMPIM
    this.w_DTSIMPTT = t_DTSIMPTT
    this.w_DTSIMPIM = t_DTSIMPIM
    this.w_DTBNOIMP = t_DTBNOIMP
    this.w_DTSNOIMP = t_DTSNOIMP
    this.w_DTOPESEN = t_DTOPESEN
    this.w_DTBNOSOG = t_DTBNOSOG
    this.w_DTSNOSOG = t_DTSNOSOG
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DTNUMDOC with this.w_DTNUMDOC
    replace t_DTDATDOC with this.w_DTDATDOC
    replace t_DTALFDOC with this.w_DTALFDOC
    replace t_DTRIFPNT with this.w_DTRIFPNT
    replace t_DTBIMPTT with this.w_DTBIMPTT
    replace t_DTBIMPIM with this.w_DTBIMPIM
    replace t_DTSIMPTT with this.w_DTSIMPTT
    replace t_DTSIMPIM with this.w_DTSIMPIM
    replace t_DTBNOIMP with this.w_DTBNOIMP
    replace t_DTSNOIMP with this.w_DTSNOIMP
    replace t_DTOPESEN with this.w_DTOPESEN
    replace t_DTBNOSOG with this.w_DTBNOSOG
    replace t_DTSNOSOG with this.w_DTSNOSOG
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsaiamdtPag1 as StdContainer
  Width  = 685
  height = 217
  stdWidth  = 685
  stdheight = 217
  resizeXpos=637
  resizeYpos=212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=2, width=281,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="DTDATDOC",Label1="Data doc.",Field2="DTNUMDOC",Label2="Numero",Field3="DTALFDOC",Label3="Alfa",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235872890

  add object oStr_1_2 as StdString with uid="KJIVJFWJHS",Visible=.t., Left=357, Top=6,;
    Alignment=1, Width=180, Height=18,;
    Caption="Operazioni imponibili beni:"  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="TJFBNLTAXP",Visible=.t., Left=342, Top=29,;
    Alignment=1, Width=195, Height=18,;
    Caption="Imposta operazioni imponibili beni:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="FRJNZASYDA",Visible=.t., Left=347, Top=52,;
    Alignment=1, Width=190, Height=18,;
    Caption="Operazioni imponibili servizi:"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="ROUVTXLZGV",Visible=.t., Left=312, Top=75,;
    Alignment=1, Width=225, Height=18,;
    Caption="Imposta operazioni imponibili servizi:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="SVFHTFWMYB",Visible=.t., Left=333, Top=98,;
    Alignment=1, Width=204, Height=18,;
    Caption="Operazioni non imponibili beni:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="ZRZMQRSAYE",Visible=.t., Left=323, Top=121,;
    Alignment=1, Width=214, Height=18,;
    Caption="Operazioni non imponibili servizi:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="BVXUQDSPVJ",Visible=.t., Left=404, Top=144,;
    Alignment=1, Width=133, Height=18,;
    Caption="Operazioni esenti:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="SBEIASYKUK",Visible=.t., Left=339, Top=167,;
    Alignment=1, Width=198, Height=18,;
    Caption="Operazioni non soggette beni:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="YJCYBNRRGP",Visible=.t., Left=329, Top=190,;
    Alignment=1, Width=208, Height=18,;
    Caption="Operazioni non soggette servizi:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=23,;
    width=277+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=24,width=276+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDTBIMPTT_2_6.Refresh()
      this.Parent.oDTBIMPIM_2_7.Refresh()
      this.Parent.oDTSIMPTT_2_8.Refresh()
      this.Parent.oDTSIMPIM_2_9.Refresh()
      this.Parent.oDTBNOIMP_2_10.Refresh()
      this.Parent.oDTSNOIMP_2_11.Refresh()
      this.Parent.oDTOPESEN_2_12.Refresh()
      this.Parent.oDTBNOSOG_2_13.Refresh()
      this.Parent.oDTSNOSOG_2_14.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_5 as StdButton with uid="XEDXHWOFBI",width=48,height=45,;
   left=289, top=2,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza la registrazione di Prima Nota";
    , HelpContextID = 108538870;
    , Caption='Re\<g.Cont';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_DTRIFPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_5.mCond()
    with this.Parent.oContained
      return (g_COGE='S' AND NOT EMPTY(.w_DTRIFPNT))
    endwith
  endfunc

  add object oDTBIMPTT_2_6 as StdTrsField with uid="NYPJPEAUMW",rtseq=6,rtrep=.t.,;
    cFormVar="w_DTBIMPTT",value=0,;
    ToolTipText = "Operazioni imponibili beni",;
    HelpContextID = 5599626,;
    cTotal="", bFixedPos=.t., cQueryName = "DTBIMPTT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=2, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTBIMPTT_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oDTBIMPIM_2_7 as StdTrsField with uid="KQBADHPBIU",rtseq=7,rtrep=.t.,;
    cFormVar="w_DTBIMPIM",value=0,;
    ToolTipText = "Imposta operazioni imponibili beni",;
    HelpContextID = 262835837,;
    cTotal="", bFixedPos=.t., cQueryName = "DTBIMPIM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=25, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTBIMPIM_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oDTSIMPTT_2_8 as StdTrsField with uid="HSCRBEAPRQ",rtseq=8,rtrep=.t.,;
    cFormVar="w_DTSIMPTT",value=0,;
    ToolTipText = "Operazioni imponibili servizi",;
    HelpContextID = 5669258,;
    cTotal="", bFixedPos=.t., cQueryName = "DTSIMPTT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=48, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTSIMPTT_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oDTSIMPIM_2_9 as StdTrsField with uid="ZUDQZHONAB",rtseq=9,rtrep=.t.,;
    cFormVar="w_DTSIMPIM",value=0,;
    ToolTipText = "Imposta operazioni imponibili servizi",;
    HelpContextID = 262766205,;
    cTotal="", bFixedPos=.t., cQueryName = "DTSIMPIM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=71, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTSIMPIM_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oDTBNOIMP_2_10 as StdTrsField with uid="RRNLWVRHPO",rtseq=10,rtrep=.t.,;
    cFormVar="w_DTBNOIMP",value=0,;
    ToolTipText = "Operazioni non imponibili beni",;
    HelpContextID = 109416058,;
    cTotal="", bFixedPos=.t., cQueryName = "DTBNOIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=94, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTBNOIMP_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oDTSNOIMP_2_11 as StdTrsField with uid="UKKUZRWMHK",rtseq=11,rtrep=.t.,;
    cFormVar="w_DTSNOIMP",value=0,;
    ToolTipText = "Operazioni non imponibili servizi",;
    HelpContextID = 109346426,;
    cTotal="", bFixedPos=.t., cQueryName = "DTSNOIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=117, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTSNOIMP_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oDTOPESEN_2_12 as StdTrsField with uid="FZMGBFRLVW",rtseq=12,rtrep=.t.,;
    cFormVar="w_DTOPESEN",value=0,;
    ToolTipText = "Operazioni esenti",;
    HelpContextID = 48054660,;
    cTotal="", bFixedPos=.t., cQueryName = "DTOPESEN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=140, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTOPESEN_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oDTBNOSOG_2_13 as StdTrsField with uid="NKXNSVZGQN",rtseq=13,rtrep=.t.,;
    cFormVar="w_DTBNOSOG",value=0,;
    ToolTipText = "Operazioni non soggette beni",;
    HelpContextID = 210079363,;
    cTotal="", bFixedPos=.t., cQueryName = "DTBNOSOG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=163, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTBNOSOG_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oDTSNOSOG_2_14 as StdTrsField with uid="JRQWNOGFTX",rtseq=14,rtrep=.t.,;
    cFormVar="w_DTSNOSOG",value=0,;
    ToolTipText = "Operazioni non soggette servizi",;
    HelpContextID = 210009731,;
    cTotal="", bFixedPos=.t., cQueryName = "DTSNOSOG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=540, Top=186, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oDTSNOSOG_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTTIPOPE='P')
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsaiamdtBodyRow as CPBodyRowCnt
  Width=267
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDTNUMDOC_2_1 as StdTrsField with uid="QLJYUFEEHA",rtseq=2,rtrep=.t.,;
    cFormVar="w_DTNUMDOC",value=0,;
    ToolTipText = "Numero documento",;
    HelpContextID = 194891399,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=85, Top=0, cSayPict=["999999999999999"], cGetPict=["999999999999999"]

  add object oDTDATDOC_2_2 as StdTrsField with uid="OHRWOSWGLF",rtseq=3,rtrep=.t.,;
    cFormVar="w_DTDATDOC",value=ctod("  /  /  "),;
    ToolTipText = "Data documento",;
    HelpContextID = 188903047,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=-2, Top=0

  add object oDTALFDOC_2_3 as StdTrsField with uid="REHQXKICCX",rtseq=4,rtrep=.t.,;
    cFormVar="w_DTALFDOC",value=space(10),;
    ToolTipText = "Serie documento",;
    HelpContextID = 202874503,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=200, Top=0, InputMask=replicate('X',10)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDTNUMDOC_2_1.When()
    return(.t.)
  proc oDTNUMDOC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDTNUMDOC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="DTTIPOPE='A' AND DTTIPREG='A'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".DTSERIAL=ANTIVADT.DTSERIAL";
  +" and "+i_cAliasName+".DTTIPOPE=ANTIVADT.DTTIPOPE";
  +" and "+i_cAliasName+".DTTIPREG=ANTIVADT.DTTIPREG";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsaiamdt','ANTIVADT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DTSERIAL=ANTIVADT.DTSERIAL";
  +" and "+i_cAliasName2+".DTTIPOPE=ANTIVADT.DTTIPOPE";
  +" and "+i_cAliasName2+".DTTIPREG=ANTIVADT.DTTIPREG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
