* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_msp                                                        *
*              Dettaglio dati estratti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-26                                                      *
* Last revis.: 2013-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsai_msp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsai_msp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsai_msp")
  return

* --- Class definition
define class tgsai_msp as StdPCForm
  Width  = 1095
  Height = 560
  Top    = 10
  Left   = 10
  cComment = "Dettaglio dati estratti"
  cPrg = "gsai_msp"
  HelpContextID=171309207
  add object cnt as tcgsai_msp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsai_msp as PCContext
  w_SPSERIAL = space(10)
  w_CPROWORD = 0
  w_SPTIPREC = space(2)
  w_SPDATDOC = space(8)
  w_SPDATREG = space(8)
  w_SPNUMFAT = space(26)
  w_SPIMPDOC = 0
  w_SPIMPOST = 0
  w_SPIVANES = space(1)
  w_SPAUTFAT = space(1)
  w_SPREVCHA = space(1)
  w_SPNUMRIE = 0
  w_SPESCLGE = space(1)
  w_SPFLGEXT = space(1)
  w_SPCOMSUP = space(1)
  w_SPTIPREG = space(1)
  w_SPRIFPNT = space(10)
  w_SPREGDES = space(10)
  w_SPTIPSOG = space(1)
  w_SPNOLEGG = space(1)
  w_NUM_DOC = 0
  w_TOTNUMDOC = 0
  proc Save(i_oFrom)
    this.w_SPSERIAL = i_oFrom.w_SPSERIAL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_SPTIPREC = i_oFrom.w_SPTIPREC
    this.w_SPDATDOC = i_oFrom.w_SPDATDOC
    this.w_SPDATREG = i_oFrom.w_SPDATREG
    this.w_SPNUMFAT = i_oFrom.w_SPNUMFAT
    this.w_SPIMPDOC = i_oFrom.w_SPIMPDOC
    this.w_SPIMPOST = i_oFrom.w_SPIMPOST
    this.w_SPIVANES = i_oFrom.w_SPIVANES
    this.w_SPAUTFAT = i_oFrom.w_SPAUTFAT
    this.w_SPREVCHA = i_oFrom.w_SPREVCHA
    this.w_SPNUMRIE = i_oFrom.w_SPNUMRIE
    this.w_SPESCLGE = i_oFrom.w_SPESCLGE
    this.w_SPFLGEXT = i_oFrom.w_SPFLGEXT
    this.w_SPCOMSUP = i_oFrom.w_SPCOMSUP
    this.w_SPTIPREG = i_oFrom.w_SPTIPREG
    this.w_SPRIFPNT = i_oFrom.w_SPRIFPNT
    this.w_SPREGDES = i_oFrom.w_SPREGDES
    this.w_SPTIPSOG = i_oFrom.w_SPTIPSOG
    this.w_SPNOLEGG = i_oFrom.w_SPNOLEGG
    this.w_NUM_DOC = i_oFrom.w_NUM_DOC
    this.w_TOTNUMDOC = i_oFrom.w_TOTNUMDOC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_SPSERIAL = this.w_SPSERIAL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_SPTIPREC = this.w_SPTIPREC
    i_oTo.w_SPDATDOC = this.w_SPDATDOC
    i_oTo.w_SPDATREG = this.w_SPDATREG
    i_oTo.w_SPNUMFAT = this.w_SPNUMFAT
    i_oTo.w_SPIMPDOC = this.w_SPIMPDOC
    i_oTo.w_SPIMPOST = this.w_SPIMPOST
    i_oTo.w_SPIVANES = this.w_SPIVANES
    i_oTo.w_SPAUTFAT = this.w_SPAUTFAT
    i_oTo.w_SPREVCHA = this.w_SPREVCHA
    i_oTo.w_SPNUMRIE = this.w_SPNUMRIE
    i_oTo.w_SPESCLGE = this.w_SPESCLGE
    i_oTo.w_SPFLGEXT = this.w_SPFLGEXT
    i_oTo.w_SPCOMSUP = this.w_SPCOMSUP
    i_oTo.w_SPTIPREG = this.w_SPTIPREG
    i_oTo.w_SPRIFPNT = this.w_SPRIFPNT
    i_oTo.w_SPREGDES = this.w_SPREGDES
    i_oTo.w_SPTIPSOG = this.w_SPTIPSOG
    i_oTo.w_SPNOLEGG = this.w_SPNOLEGG
    i_oTo.w_NUM_DOC = this.w_NUM_DOC
    i_oTo.w_TOTNUMDOC = this.w_TOTNUMDOC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsai_msp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 1095
  Height = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-12"
  HelpContextID=171309207
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DATDESSP_IDX = 0
  PNT_MAST_IDX = 0
  cFile = "DATDESSP"
  cKeySelect = "SPSERIAL"
  cKeyWhere  = "SPSERIAL=this.w_SPSERIAL"
  cKeyDetail  = "SPSERIAL=this.w_SPSERIAL"
  cKeyWhereODBC = '"SPSERIAL="+cp_ToStrODBC(this.w_SPSERIAL)';

  cKeyDetailWhereODBC = '"SPSERIAL="+cp_ToStrODBC(this.w_SPSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DATDESSP.SPSERIAL="+cp_ToStrODBC(this.w_SPSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DATDESSP.CPROWORD '
  cPrg = "gsai_msp"
  cComment = "Dettaglio dati estratti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SPSERIAL = space(10)
  w_CPROWORD = 0
  w_SPTIPREC = space(2)
  o_SPTIPREC = space(2)
  w_SPDATDOC = ctod('  /  /  ')
  w_SPDATREG = ctod('  /  /  ')
  o_SPDATREG = ctod('  /  /  ')
  w_SPNUMFAT = space(26)
  w_SPIMPDOC = 0
  w_SPIMPOST = 0
  w_SPIVANES = space(1)
  w_SPAUTFAT = space(1)
  w_SPREVCHA = space(1)
  w_SPNUMRIE = 0
  w_SPESCLGE = space(1)
  w_SPFLGEXT = space(1)
  w_SPCOMSUP = space(1)
  w_SPTIPREG = space(1)
  w_SPRIFPNT = space(10)
  w_SPREGDES = space(0)
  w_SPTIPSOG = space(1)
  w_SPNOLEGG = space(1)
  w_NUM_DOC = 0
  w_TOTNUMDOC = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_mspPag1","gsai_msp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='DATDESSP'
    * --- Area Manuale = Open Work Table
    * --- gsai_msp
    * colora le righe che sono escluse da generazione
    This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
      "IIF(NVL(t_SPESCLGE,0)=2,IIF(empty(nvl(t_SPTIPREG,'')),RGB(255,255,255),RGB(255,255,0)),RGB(255,0,0))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATDESSP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATDESSP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsai_msp'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DATDESSP where SPSERIAL=KeySet.SPSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DATDESSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATDESSP_IDX,2],this.bLoadRecFilter,this.DATDESSP_IDX,"gsai_msp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATDESSP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATDESSP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATDESSP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SPSERIAL',this.w_SPSERIAL  )
      select * from (i_cTable) DATDESSP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTNUMDOC = 0
        .w_SPSERIAL = NVL(SPSERIAL,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DATDESSP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTNUMDOC = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_SPTIPREC = NVL(SPTIPREC,space(2))
          .w_SPDATDOC = NVL(cp_ToDate(SPDATDOC),ctod("  /  /  "))
          .w_SPDATREG = NVL(cp_ToDate(SPDATREG),ctod("  /  /  "))
          .w_SPNUMFAT = NVL(SPNUMFAT,space(26))
          .w_SPIMPDOC = NVL(SPIMPDOC,0)
          .w_SPIMPOST = NVL(SPIMPOST,0)
          .w_SPIVANES = NVL(SPIVANES,space(1))
          .w_SPAUTFAT = NVL(SPAUTFAT,space(1))
          .w_SPREVCHA = NVL(SPREVCHA,space(1))
          .w_SPNUMRIE = NVL(SPNUMRIE,0)
          .w_SPESCLGE = NVL(SPESCLGE,space(1))
          .w_SPFLGEXT = NVL(SPFLGEXT,space(1))
          .w_SPCOMSUP = NVL(SPCOMSUP,space(1))
          .w_SPTIPREG = NVL(SPTIPREG,space(1))
          .w_SPRIFPNT = NVL(SPRIFPNT,space(10))
          .w_SPREGDES = NVL(SPREGDES,space(0))
        .w_SPTIPSOG = THIS.oParentObject .w_SPTIPSOG
          .w_SPNOLEGG = NVL(SPNOLEGG,space(1))
        .w_NUM_DOC = IIF(Not Empty(.w_Spdatreg),1,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTNUMDOC = .w_TOTNUMDOC+.w_NUM_DOC
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_SPTIPSOG = THIS.oParentObject .w_SPTIPSOG
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_18.enabled = .oPgFrm.Page1.oPag.oBtn_2_18.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_SPSERIAL=space(10)
      .w_CPROWORD=10
      .w_SPTIPREC=space(2)
      .w_SPDATDOC=ctod("  /  /  ")
      .w_SPDATREG=ctod("  /  /  ")
      .w_SPNUMFAT=space(26)
      .w_SPIMPDOC=0
      .w_SPIMPOST=0
      .w_SPIVANES=space(1)
      .w_SPAUTFAT=space(1)
      .w_SPREVCHA=space(1)
      .w_SPNUMRIE=0
      .w_SPESCLGE=space(1)
      .w_SPFLGEXT=space(1)
      .w_SPCOMSUP=space(1)
      .w_SPTIPREG=space(1)
      .w_SPRIFPNT=space(10)
      .w_SPREGDES=space(0)
      .w_SPTIPSOG=space(1)
      .w_SPNOLEGG=space(1)
      .w_NUM_DOC=0
      .w_TOTNUMDOC=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_SPTIPREC = 'FE'
        .DoRTCalc(4,5,.f.)
        .w_SPNUMFAT = IIF(.w_SPTIPREC='FE' OR .w_SPTIPREC='NE' OR .w_SPTIPREC='FN' OR .w_SPTIPREC='SE',.w_SPNUMFAT,SPACE(26))
        .DoRTCalc(7,8,.f.)
        .w_SPIVANES = 'N'
        .w_SPAUTFAT = 'N'
        .w_SPREVCHA = 'N'
        .DoRTCalc(12,12,.f.)
        .w_SPESCLGE = 'N'
        .w_SPFLGEXT = "N"
        .w_SPCOMSUP = 'N'
        .DoRTCalc(16,18,.f.)
        .w_SPTIPSOG = THIS.oParentObject .w_SPTIPSOG
        .w_SPNOLEGG = 'N'
        .w_NUM_DOC = IIF(Not Empty(.w_Spdatreg),1,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATDESSP')
    this.DoRTCalc(22,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_18.enabled = this.oPgFrm.Page1.oPag.oBtn_2_18.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oSPNUMRIE_2_11.enabled = i_bVal
      .Page1.oPag.oSPESCLGE_2_12.enabled = i_bVal
      .Page1.oPag.oSPFLGEXT_2_13.enabled = i_bVal
      .Page1.oPag.oSPREGDES_2_19.enabled = i_bVal
      .Page1.oPag.oBtn_2_18.enabled = .Page1.oPag.oBtn_2_18.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DATDESSP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATDESSP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPSERIAL,"SPSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(7);
      ,t_SPTIPREC N(3);
      ,t_SPDATDOC D(8);
      ,t_SPDATREG D(8);
      ,t_SPNUMFAT C(26);
      ,t_SPIMPDOC N(18,4);
      ,t_SPIMPOST N(18,4);
      ,t_SPIVANES N(3);
      ,t_SPAUTFAT N(3);
      ,t_SPREVCHA N(3);
      ,t_SPNUMRIE N(3);
      ,t_SPESCLGE N(3);
      ,t_SPFLGEXT N(3);
      ,t_SPREGDES M(10);
      ,t_SPNOLEGG N(3);
      ,CPROWNUM N(10);
      ,t_SPCOMSUP C(1);
      ,t_SPTIPREG C(1);
      ,t_SPRIFPNT C(10);
      ,t_SPTIPSOG C(1);
      ,t_NUM_DOC N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsai_mspbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPTIPREC_2_2.controlsource=this.cTrsName+'.t_SPTIPREC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPDATDOC_2_3.controlsource=this.cTrsName+'.t_SPDATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPDATREG_2_4.controlsource=this.cTrsName+'.t_SPDATREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPNUMFAT_2_5.controlsource=this.cTrsName+'.t_SPNUMFAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPIMPDOC_2_6.controlsource=this.cTrsName+'.t_SPIMPDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPIMPOST_2_7.controlsource=this.cTrsName+'.t_SPIMPOST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPIVANES_2_8.controlsource=this.cTrsName+'.t_SPIVANES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPAUTFAT_2_9.controlsource=this.cTrsName+'.t_SPAUTFAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPREVCHA_2_10.controlsource=this.cTrsName+'.t_SPREVCHA'
    this.oPgFRm.Page1.oPag.oSPNUMRIE_2_11.controlsource=this.cTrsName+'.t_SPNUMRIE'
    this.oPgFRm.Page1.oPag.oSPESCLGE_2_12.controlsource=this.cTrsName+'.t_SPESCLGE'
    this.oPgFRm.Page1.oPag.oSPFLGEXT_2_13.controlsource=this.cTrsName+'.t_SPFLGEXT'
    this.oPgFRm.Page1.oPag.oSPREGDES_2_19.controlsource=this.cTrsName+'.t_SPREGDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSPNOLEGG_2_21.controlsource=this.cTrsName+'.t_SPNOLEGG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(75)
    this.AddVLine(127)
    this.AddVLine(210)
    this.AddVLine(293)
    this.AddVLine(492)
    this.AddVLine(616)
    this.AddVLine(741)
    this.AddVLine(806)
    this.AddVLine(871)
    this.AddVLine(938)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATDESSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATDESSP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATDESSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATDESSP_IDX,2])
      *
      * insert into DATDESSP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATDESSP')
        i_extval=cp_InsertValODBCExtFlds(this,'DATDESSP')
        i_cFldBody=" "+;
                  "(SPSERIAL,CPROWORD,SPTIPREC,SPDATDOC,SPDATREG"+;
                  ",SPNUMFAT,SPIMPDOC,SPIMPOST,SPIVANES,SPAUTFAT"+;
                  ",SPREVCHA,SPNUMRIE,SPESCLGE,SPFLGEXT,SPCOMSUP"+;
                  ",SPTIPREG,SPRIFPNT,SPREGDES,SPNOLEGG,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SPSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_SPTIPREC)+","+cp_ToStrODBC(this.w_SPDATDOC)+","+cp_ToStrODBC(this.w_SPDATREG)+;
             ","+cp_ToStrODBC(this.w_SPNUMFAT)+","+cp_ToStrODBC(this.w_SPIMPDOC)+","+cp_ToStrODBC(this.w_SPIMPOST)+","+cp_ToStrODBC(this.w_SPIVANES)+","+cp_ToStrODBC(this.w_SPAUTFAT)+;
             ","+cp_ToStrODBC(this.w_SPREVCHA)+","+cp_ToStrODBC(this.w_SPNUMRIE)+","+cp_ToStrODBC(this.w_SPESCLGE)+","+cp_ToStrODBC(this.w_SPFLGEXT)+","+cp_ToStrODBC(this.w_SPCOMSUP)+;
             ","+cp_ToStrODBC(this.w_SPTIPREG)+","+cp_ToStrODBC(this.w_SPRIFPNT)+","+cp_ToStrODBC(this.w_SPREGDES)+","+cp_ToStrODBC(this.w_SPNOLEGG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATDESSP')
        i_extval=cp_InsertValVFPExtFlds(this,'DATDESSP')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'SPSERIAL',this.w_SPSERIAL)
        INSERT INTO (i_cTable) (;
                   SPSERIAL;
                  ,CPROWORD;
                  ,SPTIPREC;
                  ,SPDATDOC;
                  ,SPDATREG;
                  ,SPNUMFAT;
                  ,SPIMPDOC;
                  ,SPIMPOST;
                  ,SPIVANES;
                  ,SPAUTFAT;
                  ,SPREVCHA;
                  ,SPNUMRIE;
                  ,SPESCLGE;
                  ,SPFLGEXT;
                  ,SPCOMSUP;
                  ,SPTIPREG;
                  ,SPRIFPNT;
                  ,SPREGDES;
                  ,SPNOLEGG;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_SPSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_SPTIPREC;
                  ,this.w_SPDATDOC;
                  ,this.w_SPDATREG;
                  ,this.w_SPNUMFAT;
                  ,this.w_SPIMPDOC;
                  ,this.w_SPIMPOST;
                  ,this.w_SPIVANES;
                  ,this.w_SPAUTFAT;
                  ,this.w_SPREVCHA;
                  ,this.w_SPNUMRIE;
                  ,this.w_SPESCLGE;
                  ,this.w_SPFLGEXT;
                  ,this.w_SPCOMSUP;
                  ,this.w_SPTIPREG;
                  ,this.w_SPRIFPNT;
                  ,this.w_SPREGDES;
                  ,this.w_SPNOLEGG;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DATDESSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATDESSP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (Not Empty(t_Spdatreg)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DATDESSP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DATDESSP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (Not Empty(t_Spdatreg)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DATDESSP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DATDESSP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",SPTIPREC="+cp_ToStrODBC(this.w_SPTIPREC)+;
                     ",SPDATDOC="+cp_ToStrODBC(this.w_SPDATDOC)+;
                     ",SPDATREG="+cp_ToStrODBC(this.w_SPDATREG)+;
                     ",SPNUMFAT="+cp_ToStrODBC(this.w_SPNUMFAT)+;
                     ",SPIMPDOC="+cp_ToStrODBC(this.w_SPIMPDOC)+;
                     ",SPIMPOST="+cp_ToStrODBC(this.w_SPIMPOST)+;
                     ",SPIVANES="+cp_ToStrODBC(this.w_SPIVANES)+;
                     ",SPAUTFAT="+cp_ToStrODBC(this.w_SPAUTFAT)+;
                     ",SPREVCHA="+cp_ToStrODBC(this.w_SPREVCHA)+;
                     ",SPNUMRIE="+cp_ToStrODBC(this.w_SPNUMRIE)+;
                     ",SPESCLGE="+cp_ToStrODBC(this.w_SPESCLGE)+;
                     ",SPFLGEXT="+cp_ToStrODBC(this.w_SPFLGEXT)+;
                     ",SPCOMSUP="+cp_ToStrODBC(this.w_SPCOMSUP)+;
                     ",SPTIPREG="+cp_ToStrODBC(this.w_SPTIPREG)+;
                     ",SPRIFPNT="+cp_ToStrODBC(this.w_SPRIFPNT)+;
                     ",SPREGDES="+cp_ToStrODBC(this.w_SPREGDES)+;
                     ",SPNOLEGG="+cp_ToStrODBC(this.w_SPNOLEGG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DATDESSP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,SPTIPREC=this.w_SPTIPREC;
                     ,SPDATDOC=this.w_SPDATDOC;
                     ,SPDATREG=this.w_SPDATREG;
                     ,SPNUMFAT=this.w_SPNUMFAT;
                     ,SPIMPDOC=this.w_SPIMPDOC;
                     ,SPIMPOST=this.w_SPIMPOST;
                     ,SPIVANES=this.w_SPIVANES;
                     ,SPAUTFAT=this.w_SPAUTFAT;
                     ,SPREVCHA=this.w_SPREVCHA;
                     ,SPNUMRIE=this.w_SPNUMRIE;
                     ,SPESCLGE=this.w_SPESCLGE;
                     ,SPFLGEXT=this.w_SPFLGEXT;
                     ,SPCOMSUP=this.w_SPCOMSUP;
                     ,SPTIPREG=this.w_SPTIPREG;
                     ,SPRIFPNT=this.w_SPRIFPNT;
                     ,SPREGDES=this.w_SPREGDES;
                     ,SPNOLEGG=this.w_SPNOLEGG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATDESSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATDESSP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (Not Empty(t_Spdatreg)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DATDESSP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (Not Empty(t_Spdatreg)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATDESSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATDESSP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_SPTIPREC<>.w_SPTIPREC
          .w_SPNUMFAT = IIF(.w_SPTIPREC='FE' OR .w_SPTIPREC='NE' OR .w_SPTIPREC='FN' OR .w_SPTIPREC='SE',.w_SPNUMFAT,SPACE(26))
        endif
        .DoRTCalc(7,18,.t.)
          .w_SPTIPSOG = THIS.oParentObject .w_SPTIPSOG
        .DoRTCalc(20,20,.t.)
        if .o_SPDATREG<>.w_SPDATREG
          .w_TOTNUMDOC = .w_TOTNUMDOC-.w_num_doc
          .w_NUM_DOC = IIF(Not Empty(.w_Spdatreg),1,0)
          .w_TOTNUMDOC = .w_TOTNUMDOC+.w_num_doc
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_SPCOMSUP with this.w_SPCOMSUP
      replace t_SPTIPREG with this.w_SPTIPREG
      replace t_SPRIFPNT with this.w_SPRIFPNT
      replace t_SPTIPSOG with this.w_SPTIPSOG
      replace t_NUM_DOC with this.w_NUM_DOC
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_CJIQRMFQGP()
    with this
          * --- Assegno valore al campo SPTIPREC
          .w_SPTIPREC = IIF(.w_SPTIPSOG='T','TU','FE')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSPTIPREC_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSPTIPREC_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSPNUMFAT_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSPNUMFAT_2_5.mCond()
    this.oPgFrm.Page1.oPag.oSPNUMRIE_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oSPNUMRIE_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_18.enabled =this.oPgFrm.Page1.oPag.oBtn_2_18.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oSPFLGEXT_2_13.visible=!this.oPgFrm.Page1.oPag.oSPFLGEXT_2_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_18.visible=!this.oPgFrm.Page1.oPag.oBtn_2_18.mHide()
    this.oPgFrm.Page1.oPag.oSPREGDES_2_19.visible=!this.oPgFrm.Page1.oPag.oSPREGDES_2_19.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init Row")
          .Calculate_CJIQRMFQGP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSPNUMRIE_2_11.value==this.w_SPNUMRIE)
      this.oPgFrm.Page1.oPag.oSPNUMRIE_2_11.value=this.w_SPNUMRIE
      replace t_SPNUMRIE with this.oPgFrm.Page1.oPag.oSPNUMRIE_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSPESCLGE_2_12.RadioValue()==this.w_SPESCLGE)
      this.oPgFrm.Page1.oPag.oSPESCLGE_2_12.SetRadio()
      replace t_SPESCLGE with this.oPgFrm.Page1.oPag.oSPESCLGE_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSPFLGEXT_2_13.RadioValue()==this.w_SPFLGEXT)
      this.oPgFrm.Page1.oPag.oSPFLGEXT_2_13.SetRadio()
      replace t_SPFLGEXT with this.oPgFrm.Page1.oPag.oSPFLGEXT_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSPREGDES_2_19.value==this.w_SPREGDES)
      this.oPgFrm.Page1.oPag.oSPREGDES_2_19.value=this.w_SPREGDES
      replace t_SPREGDES with this.oPgFrm.Page1.oPag.oSPREGDES_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPTIPREC_2_2.RadioValue()==this.w_SPTIPREC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPTIPREC_2_2.SetRadio()
      replace t_SPTIPREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPTIPREC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPDATDOC_2_3.value==this.w_SPDATDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPDATDOC_2_3.value=this.w_SPDATDOC
      replace t_SPDATDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPDATDOC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPDATREG_2_4.value==this.w_SPDATREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPDATREG_2_4.value=this.w_SPDATREG
      replace t_SPDATREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPDATREG_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPNUMFAT_2_5.value==this.w_SPNUMFAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPNUMFAT_2_5.value=this.w_SPNUMFAT
      replace t_SPNUMFAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPNUMFAT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIMPDOC_2_6.value==this.w_SPIMPDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIMPDOC_2_6.value=this.w_SPIMPDOC
      replace t_SPIMPDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIMPDOC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIMPOST_2_7.value==this.w_SPIMPOST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIMPOST_2_7.value=this.w_SPIMPOST
      replace t_SPIMPOST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIMPOST_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIVANES_2_8.RadioValue()==this.w_SPIVANES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIVANES_2_8.SetRadio()
      replace t_SPIVANES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIVANES_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPAUTFAT_2_9.RadioValue()==this.w_SPAUTFAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPAUTFAT_2_9.SetRadio()
      replace t_SPAUTFAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPAUTFAT_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPREVCHA_2_10.RadioValue()==this.w_SPREVCHA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPREVCHA_2_10.SetRadio()
      replace t_SPREVCHA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPREVCHA_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPNOLEGG_2_21.RadioValue()==this.w_SPNOLEGG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPNOLEGG_2_21.SetRadio()
      replace t_SPNOLEGG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPNOLEGG_2_21.value
    endif
    cp_SetControlsValueExtFlds(this,'DATDESSP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((.w_SPTIPSOG='D' And (.w_SPTIPREC='FE' OR .w_SPTIPREC='FR')) Or .w_SPTIPSOG $ 'IPNT') and (.w_SPTIPSOG $ 'IPND') and (Not Empty(.w_Spdatreg))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPTIPREC_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quadro incongruente con il tipo soggetto")
      endcase
      if Not Empty(.w_Spdatreg)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SPTIPREC = this.w_SPTIPREC
    this.o_SPDATREG = this.w_SPDATREG
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(Not Empty(t_Spdatreg))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999999,cp_maxroword()+10)
      .w_SPTIPREC=space(2)
      .w_SPDATDOC=ctod("  /  /  ")
      .w_SPDATREG=ctod("  /  /  ")
      .w_SPNUMFAT=space(26)
      .w_SPIMPDOC=0
      .w_SPIMPOST=0
      .w_SPIVANES=space(1)
      .w_SPAUTFAT=space(1)
      .w_SPREVCHA=space(1)
      .w_SPNUMRIE=0
      .w_SPESCLGE=space(1)
      .w_SPFLGEXT=space(1)
      .w_SPCOMSUP=space(1)
      .w_SPTIPREG=space(1)
      .w_SPRIFPNT=space(10)
      .w_SPREGDES=space(0)
      .w_SPTIPSOG=space(1)
      .w_SPNOLEGG=space(1)
      .w_NUM_DOC=0
      .DoRTCalc(1,2,.f.)
        .w_SPTIPREC = 'FE'
      .DoRTCalc(4,5,.f.)
        .w_SPNUMFAT = IIF(.w_SPTIPREC='FE' OR .w_SPTIPREC='NE' OR .w_SPTIPREC='FN' OR .w_SPTIPREC='SE',.w_SPNUMFAT,SPACE(26))
      .DoRTCalc(7,8,.f.)
        .w_SPIVANES = 'N'
        .w_SPAUTFAT = 'N'
        .w_SPREVCHA = 'N'
      .DoRTCalc(12,12,.f.)
        .w_SPESCLGE = 'N'
        .w_SPFLGEXT = "N"
        .w_SPCOMSUP = 'N'
      .DoRTCalc(16,18,.f.)
        .w_SPTIPSOG = THIS.oParentObject .w_SPTIPSOG
        .w_SPNOLEGG = 'N'
        .w_NUM_DOC = IIF(Not Empty(.w_Spdatreg),1,0)
    endwith
    this.DoRTCalc(22,22,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_SPTIPREC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPTIPREC_2_2.RadioValue(.t.)
    this.w_SPDATDOC = t_SPDATDOC
    this.w_SPDATREG = t_SPDATREG
    this.w_SPNUMFAT = t_SPNUMFAT
    this.w_SPIMPDOC = t_SPIMPDOC
    this.w_SPIMPOST = t_SPIMPOST
    this.w_SPIVANES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIVANES_2_8.RadioValue(.t.)
    this.w_SPAUTFAT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPAUTFAT_2_9.RadioValue(.t.)
    this.w_SPREVCHA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPREVCHA_2_10.RadioValue(.t.)
    this.w_SPNUMRIE = t_SPNUMRIE
    this.w_SPESCLGE = this.oPgFrm.Page1.oPag.oSPESCLGE_2_12.RadioValue(.t.)
    this.w_SPFLGEXT = this.oPgFrm.Page1.oPag.oSPFLGEXT_2_13.RadioValue(.t.)
    this.w_SPCOMSUP = t_SPCOMSUP
    this.w_SPTIPREG = t_SPTIPREG
    this.w_SPRIFPNT = t_SPRIFPNT
    this.w_SPREGDES = t_SPREGDES
    this.w_SPTIPSOG = t_SPTIPSOG
    this.w_SPNOLEGG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPNOLEGG_2_21.RadioValue(.t.)
    this.w_NUM_DOC = t_NUM_DOC
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_SPTIPREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPTIPREC_2_2.ToRadio()
    replace t_SPDATDOC with this.w_SPDATDOC
    replace t_SPDATREG with this.w_SPDATREG
    replace t_SPNUMFAT with this.w_SPNUMFAT
    replace t_SPIMPDOC with this.w_SPIMPDOC
    replace t_SPIMPOST with this.w_SPIMPOST
    replace t_SPIVANES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPIVANES_2_8.ToRadio()
    replace t_SPAUTFAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPAUTFAT_2_9.ToRadio()
    replace t_SPREVCHA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPREVCHA_2_10.ToRadio()
    replace t_SPNUMRIE with this.w_SPNUMRIE
    replace t_SPESCLGE with this.oPgFrm.Page1.oPag.oSPESCLGE_2_12.ToRadio()
    replace t_SPFLGEXT with this.oPgFrm.Page1.oPag.oSPFLGEXT_2_13.ToRadio()
    replace t_SPCOMSUP with this.w_SPCOMSUP
    replace t_SPTIPREG with this.w_SPTIPREG
    replace t_SPRIFPNT with this.w_SPRIFPNT
    replace t_SPREGDES with this.w_SPREGDES
    replace t_SPTIPSOG with this.w_SPTIPSOG
    replace t_SPNOLEGG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSPNOLEGG_2_21.ToRadio()
    replace t_NUM_DOC with this.w_NUM_DOC
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTNUMDOC = .w_TOTNUMDOC-.w_num_doc
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsai_mspPag1 as StdContainer
  Width  = 1091
  height = 560
  stdWidth  = 1091
  stdheight = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=7, width=1072,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=11,Field1="CPROWORD",Label1="Riga",Field2="SPTIPREC",Label2="Quadro",Field3="SPDATDOC",Label3="Data doc.",Field4="SPDATREG",Label4="Data reg.",Field5="SPNUMFAT",Label5="Numero",Field6="SPIMPDOC",Label6="Importo",Field7="SPIMPOST",Label7="Imposta",Field8="SPIVANES",Label8="IVA no esp.",Field9="SPAUTFAT",Label9="Autofattura",Field10="SPREVCHA",Label10="R.C.",Field11="SPNOLEGG",Label11="Noleggio",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218964602

  add object oStr_1_2 as StdString with uid="GYXCNXPABH",Visible=.t., Left=249, Top=463,;
    Alignment=1, Width=128, Height=18,;
    Caption="Tipo estrazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (.w_SPCOMSUP='N')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="SKLEKASCVU",Visible=.t., Left=532, Top=424,;
    Alignment=0, Width=256, Height=18,;
    Caption="Parametri causali note di variazione"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.w_SPTIPREG<>'B')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="RFTSAFOPSG",Visible=.t., Left=532, Top=424,;
    Alignment=0, Width=256, Height=18,;
    Caption="Parametri dati IVA"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_SPTIPREG<>'A')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="YVOWUOQTIR",Visible=.t., Left=532, Top=424,;
    Alignment=0, Width=256, Height=18,;
    Caption="Parametri schede carburante"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_SPTIPREG<>'C')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=29,;
    width=1068+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=30,width=1067+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oSPNUMRIE_2_11.Refresh()
      this.Parent.oSPESCLGE_2_12.Refresh()
      this.Parent.oSPFLGEXT_2_13.Refresh()
      this.Parent.oSPREGDES_2_19.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oSPNUMRIE_2_11 as StdTrsField with uid="UHLVJRFZYL",rtseq=12,rtrep=.t.,;
    cFormVar="w_SPNUMRIE",value=0,;
    HelpContextID = 23080555,;
    cTotal="", bFixedPos=.t., cQueryName = "SPNUMRIE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=184, Top=420, cSayPict=["999"], cGetPict=["999"]

  func oSPNUMRIE_2_11.mCond()
    with this.Parent.oContained
      return (.w_Sptiprec $ 'FR-FE' And .w_Sptipsog='D')
    endwith
  endfunc

  add object oSPESCLGE_2_12 as StdTrsCombo with uid="WHNUJYTCDS",rtrep=.t.,;
    cFormVar="w_SPESCLGE", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 180199019,;
    Height=25, Width=52, Left=384, Top=419,;
    cTotal="", cQueryName = "SPESCLGE",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.


  func oSPESCLGE_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SPESCLGE,&i_cF..t_SPESCLGE),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oSPESCLGE_2_12.GetRadio()
    this.Parent.oContained.w_SPESCLGE = this.RadioValue()
    return .t.
  endfunc

  func oSPESCLGE_2_12.ToRadio()
    this.Parent.oContained.w_SPESCLGE=trim(this.Parent.oContained.w_SPESCLGE)
    return(;
      iif(this.Parent.oContained.w_SPESCLGE=='S',1,;
      iif(this.Parent.oContained.w_SPESCLGE=='N',2,;
      0)))
  endfunc

  func oSPESCLGE_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oSPFLGEXT_2_13 as StdTrsCombo with uid="AAFLWYGTAM",rtrep=.t.,;
    cFormVar="w_SPFLGEXT", RowSource=""+"Inclusione forzata,"+"Esclusione forzata,"+"Nessuna forzatura" , ;
    ToolTipText = "Flag estrazione",;
    HelpContextID = 66498170,;
    Height=25, Width=141, Left=384, Top=460,;
    cTotal="", cQueryName = "SPFLGEXT",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.


  func oSPFLGEXT_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SPFLGEXT,&i_cF..t_SPFLGEXT),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'F',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oSPFLGEXT_2_13.GetRadio()
    this.Parent.oContained.w_SPFLGEXT = this.RadioValue()
    return .t.
  endfunc

  func oSPFLGEXT_2_13.ToRadio()
    this.Parent.oContained.w_SPFLGEXT=trim(this.Parent.oContained.w_SPFLGEXT)
    return(;
      iif(this.Parent.oContained.w_SPFLGEXT=='I',1,;
      iif(this.Parent.oContained.w_SPFLGEXT=='F',2,;
      iif(this.Parent.oContained.w_SPFLGEXT=='N',3,;
      0))))
  endfunc

  func oSPFLGEXT_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSPFLGEXT_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SPCOMSUP='N')
    endwith
    endif
  endfunc

  add object oBtn_2_18 as StdButton with uid="XEDXHWOFBI",width=48,height=45,;
   left=5, top=501,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza la registrazione di Prima Nota";
    , HelpContextID = 91630582;
    , Caption='Re\<g.Cont',tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_18.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SPRIFPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_18.mCond()
    with this.Parent.oContained
      return (g_Coge='S' And Not Empty(.w_Sprifpnt))
    endwith
  endfunc

  func oBtn_2_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Sprifpnt) Or g_Coge<>'S')
    endwith
   endif
  endfunc

  add object oSPREGDES_2_19 as StdTrsMemo with uid="OMIWNMCFCG",rtseq=18,rtrep=.t.,;
    cFormVar="w_SPREGDES",value=space(0),;
    HelpContextID = 49311353,;
    cTotal="", bFixedPos=.t., cQueryName = "SPREGDES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=93, Width=368, Left=530, Top=454

  func oSPREGDES_2_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SPTIPREG<>'A')
    endwith
    endif
  endfunc

  add object oStr_2_14 as StdString with uid="JFIWBFWCGA",Visible=.t., Left=227, Top=423,;
    Alignment=1, Width=150, Height=18,;
    Caption="Escludi da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="BFQYRTBQRV",Visible=.t., Left=8, Top=423,;
    Alignment=1, Width=171, Height=18,;
    Caption="Numero fatture riepilogate:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsai_mspBodyRow as CPBodyRowCnt
  Width=1058
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="QHXVEDJWIT",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17142422,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, cSayPict=["9999999"], cGetPict=["9999999"]

  add object oSPTIPREC_2_2 as StdTrsCombo with uid="FXWRWOWDKD",rtrep=.t.,;
    cFormVar="w_SPTIPREC", RowSource=""+"FE,"+"FR,"+"DF,"+"NE,"+"NR,"+"FN,"+"SE,"+"TU" , ;
    ToolTipText = "Tipo record",;
    HelpContextID = 25464425,;
    Height=22, Width=48, Left=66, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Quadro incongruente con il tipo soggetto";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSPTIPREC_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SPTIPREC,&i_cF..t_SPTIPREC),this.value)
    return(iif(xVal =1,'FE',;
    iif(xVal =2,'FR',;
    iif(xVal =3,'DF',;
    iif(xVal =4,'NE',;
    iif(xVal =5,'NR',;
    iif(xVal =6,'FN',;
    iif(xVal =7,'SE',;
    iif(xVal =8,'TU',;
    space(2))))))))))
  endfunc
  func oSPTIPREC_2_2.GetRadio()
    this.Parent.oContained.w_SPTIPREC = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPREC_2_2.ToRadio()
    this.Parent.oContained.w_SPTIPREC=trim(this.Parent.oContained.w_SPTIPREC)
    return(;
      iif(this.Parent.oContained.w_SPTIPREC=='FE',1,;
      iif(this.Parent.oContained.w_SPTIPREC=='FR',2,;
      iif(this.Parent.oContained.w_SPTIPREC=='DF',3,;
      iif(this.Parent.oContained.w_SPTIPREC=='NE',4,;
      iif(this.Parent.oContained.w_SPTIPREC=='NR',5,;
      iif(this.Parent.oContained.w_SPTIPREC=='FN',6,;
      iif(this.Parent.oContained.w_SPTIPREC=='SE',7,;
      iif(this.Parent.oContained.w_SPTIPREC=='TU',8,;
      0)))))))))
  endfunc

  func oSPTIPREC_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSPTIPREC_2_2.mCond()
    with this.Parent.oContained
      return (.w_SPTIPSOG $ 'IPND')
    endwith
  endfunc

  func oSPTIPREC_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_SPTIPSOG='D' And (.w_SPTIPREC='FE' OR .w_SPTIPREC='FR')) Or .w_SPTIPSOG $ 'IPNT')
    endwith
    return bRes
  endfunc

  add object oSPDATDOC_2_3 as StdTrsField with uid="WPEOXIKOLH",rtseq=4,rtrep=.t.,;
    cFormVar="w_SPDATDOC",value=ctod("  /  /  "),;
    HelpContextID = 205812119,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=117, Top=0

  add object oSPDATREG_2_4 as StdTrsField with uid="MYNHCFOXID",rtseq=5,rtrep=.t.,;
    cFormVar="w_SPDATREG",value=ctod("  /  /  "),;
    HelpContextID = 29068909,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=203, Top=0

  add object oSPNUMFAT_2_5 as StdTrsField with uid="VPAUDPGKNL",rtseq=6,rtrep=.t.,;
    cFormVar="w_SPNUMFAT",value=space(26),;
    HelpContextID = 178246022,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=195, Left=283, Top=0, InputMask=replicate('X',26)

  func oSPNUMFAT_2_5.mCond()
    with this.Parent.oContained
      return (.w_SPTIPREC='FE' OR .w_SPTIPREC='NE' OR .w_SPTIPREC='FN' OR .w_SPTIPREC='SE' OR .w_SPTIPREC='TU')
    endwith
  endfunc

  add object oSPIMPDOC_2_6 as StdTrsField with uid="QLQTOMXUQX",rtseq=7,rtrep=.t.,;
    cFormVar="w_SPIMPDOC",value=0,;
    HelpContextID = 209199511,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=483, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oSPIMPOST_2_7 as StdTrsField with uid="COUYULQCFP",rtseq=8,rtrep=.t.,;
    cFormVar="w_SPIMPOST",value=0,;
    HelpContextID = 24650118,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=608, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oSPIVANES_2_8 as StdTrsCombo with uid="OZQZKTNEFO",rtrep=.t.,;
    cFormVar="w_SPIVANES", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 211869305,;
    Height=22, Width=59, Left=732, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSPIVANES_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SPIVANES,&i_cF..t_SPIVANES),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oSPIVANES_2_8.GetRadio()
    this.Parent.oContained.w_SPIVANES = this.RadioValue()
    return .t.
  endfunc

  func oSPIVANES_2_8.ToRadio()
    this.Parent.oContained.w_SPIVANES=trim(this.Parent.oContained.w_SPIVANES)
    return(;
      iif(this.Parent.oContained.w_SPIVANES=='S',1,;
      iif(this.Parent.oContained.w_SPIVANES=='N',2,;
      0)))
  endfunc

  func oSPIVANES_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oSPAUTFAT_2_9 as StdTrsCombo with uid="UWUSKXISES",rtrep=.t.,;
    cFormVar="w_SPAUTFAT", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 170959238,;
    Height=22, Width=61, Left=797, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSPAUTFAT_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SPAUTFAT,&i_cF..t_SPAUTFAT),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oSPAUTFAT_2_9.GetRadio()
    this.Parent.oContained.w_SPAUTFAT = this.RadioValue()
    return .t.
  endfunc

  func oSPAUTFAT_2_9.ToRadio()
    this.Parent.oContained.w_SPAUTFAT=trim(this.Parent.oContained.w_SPAUTFAT)
    return(;
      iif(this.Parent.oContained.w_SPAUTFAT=='S',1,;
      iif(this.Parent.oContained.w_SPAUTFAT=='N',2,;
      0)))
  endfunc

  func oSPAUTFAT_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oSPREVCHA_2_10 as StdTrsCombo with uid="GJLWMWEWXG",rtrep=.t.,;
    cFormVar="w_SPREVCHA", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 48262759,;
    Height=22, Width=61, Left=861, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSPREVCHA_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SPREVCHA,&i_cF..t_SPREVCHA),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oSPREVCHA_2_10.GetRadio()
    this.Parent.oContained.w_SPREVCHA = this.RadioValue()
    return .t.
  endfunc

  func oSPREVCHA_2_10.ToRadio()
    this.Parent.oContained.w_SPREVCHA=trim(this.Parent.oContained.w_SPREVCHA)
    return(;
      iif(this.Parent.oContained.w_SPREVCHA=='S',1,;
      iif(this.Parent.oContained.w_SPREVCHA=='N',2,;
      0)))
  endfunc

  func oSPREVCHA_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oSPNOLEGG_2_21 as StdTrsCombo with uid="FUCOARFBTK",rtrep=.t.,;
    cFormVar="w_SPNOLEGG", RowSource=""+"Autovettura,"+"Caravan,"+"Altri veicoli,"+"Unit� da diporto,"+"Aeromobili,"+"Nessuno" , ;
    HelpContextID = 71970413,;
    Height=22, Width=122, Left=931, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSPNOLEGG_2_21.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SPNOLEGG,&i_cF..t_SPNOLEGG),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'B',;
    iif(xVal =3,'C',;
    iif(xVal =4,'D',;
    iif(xVal =5,'E',;
    iif(xVal =6,'N',;
    'N')))))))
  endfunc
  func oSPNOLEGG_2_21.GetRadio()
    this.Parent.oContained.w_SPNOLEGG = this.RadioValue()
    return .t.
  endfunc

  func oSPNOLEGG_2_21.ToRadio()
    this.Parent.oContained.w_SPNOLEGG=trim(this.Parent.oContained.w_SPNOLEGG)
    return(;
      iif(this.Parent.oContained.w_SPNOLEGG=='A',1,;
      iif(this.Parent.oContained.w_SPNOLEGG=='B',2,;
      iif(this.Parent.oContained.w_SPNOLEGG=='C',3,;
      iif(this.Parent.oContained.w_SPNOLEGG=='D',4,;
      iif(this.Parent.oContained.w_SPNOLEGG=='E',5,;
      iif(this.Parent.oContained.w_SPNOLEGG=='N',6,;
      0)))))))
  endfunc

  func oSPNOLEGG_2_21.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_msp','DATDESSP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SPSERIAL=DATDESSP.SPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
