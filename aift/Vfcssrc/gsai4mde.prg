* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai4mde                                                        *
*              Dettaglio dati estratti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-26                                                      *
* Last revis.: 2011-11-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsai4mde")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsai4mde")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsai4mde")
  return

* --- Class definition
define class tgsai4mde as StdPCForm
  Width  = 809
  Height = 544
  Top    = 3
  Left   = 5
  cComment = "Dettaglio dati estratti"
  cPrg = "gsai4mde"
  HelpContextID=83167081
  add object cnt as tcgsai4mde
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsai4mde as PCContext
  w_DESERIAL = space(10)
  w_DEDATOPE = space(8)
  w_DE__AREA = space(1)
  w_DENUMFAT = space(15)
  w_DEIMPDOC = 0
  w_DEIMPOST = 0
  w_DERIFPNT = space(10)
  w_DETIPOPE = space(1)
  w_DERIFCON = space(30)
  w_DETIPFAT = space(1)
  w_DEFLGEXT = space(1)
  w_DEESCLUD = space(1)
  w_DEDATFAT = space(8)
  w_DENUMRET = space(15)
  w_DEVARIMP = space(1)
  w_DEVARIMS = space(1)
  w_IMPORTO_DOVUTO = 0
  w_NUM_DOC = 0
  w_IMPOSTA_DOVUTA = 0
  w_TOTIMPDOV = 0
  w_TOTNUMDOC = 0
  w_TOTIMPSDOV = 0
  w_DECOMSUP = space(1)
  w_DETIPREG = space(1)
  w_RIFDOC = space(10)
  w_DEREGDES = space(10)
  w_SERIALP = space(10)
  proc Save(i_oFrom)
    this.w_DESERIAL = i_oFrom.w_DESERIAL
    this.w_DEDATOPE = i_oFrom.w_DEDATOPE
    this.w_DE__AREA = i_oFrom.w_DE__AREA
    this.w_DENUMFAT = i_oFrom.w_DENUMFAT
    this.w_DEIMPDOC = i_oFrom.w_DEIMPDOC
    this.w_DEIMPOST = i_oFrom.w_DEIMPOST
    this.w_DERIFPNT = i_oFrom.w_DERIFPNT
    this.w_DETIPOPE = i_oFrom.w_DETIPOPE
    this.w_DERIFCON = i_oFrom.w_DERIFCON
    this.w_DETIPFAT = i_oFrom.w_DETIPFAT
    this.w_DEFLGEXT = i_oFrom.w_DEFLGEXT
    this.w_DEESCLUD = i_oFrom.w_DEESCLUD
    this.w_DEDATFAT = i_oFrom.w_DEDATFAT
    this.w_DENUMRET = i_oFrom.w_DENUMRET
    this.w_DEVARIMP = i_oFrom.w_DEVARIMP
    this.w_DEVARIMS = i_oFrom.w_DEVARIMS
    this.w_IMPORTO_DOVUTO = i_oFrom.w_IMPORTO_DOVUTO
    this.w_NUM_DOC = i_oFrom.w_NUM_DOC
    this.w_IMPOSTA_DOVUTA = i_oFrom.w_IMPOSTA_DOVUTA
    this.w_TOTIMPDOV = i_oFrom.w_TOTIMPDOV
    this.w_TOTNUMDOC = i_oFrom.w_TOTNUMDOC
    this.w_TOTIMPSDOV = i_oFrom.w_TOTIMPSDOV
    this.w_DECOMSUP = i_oFrom.w_DECOMSUP
    this.w_DETIPREG = i_oFrom.w_DETIPREG
    this.w_RIFDOC = i_oFrom.w_RIFDOC
    this.w_DEREGDES = i_oFrom.w_DEREGDES
    this.w_SERIALP = i_oFrom.w_SERIALP
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DESERIAL = this.w_DESERIAL
    i_oTo.w_DEDATOPE = this.w_DEDATOPE
    i_oTo.w_DE__AREA = this.w_DE__AREA
    i_oTo.w_DENUMFAT = this.w_DENUMFAT
    i_oTo.w_DEIMPDOC = this.w_DEIMPDOC
    i_oTo.w_DEIMPOST = this.w_DEIMPOST
    i_oTo.w_DERIFPNT = this.w_DERIFPNT
    i_oTo.w_DETIPOPE = this.w_DETIPOPE
    i_oTo.w_DERIFCON = this.w_DERIFCON
    i_oTo.w_DETIPFAT = this.w_DETIPFAT
    i_oTo.w_DEFLGEXT = this.w_DEFLGEXT
    i_oTo.w_DEESCLUD = this.w_DEESCLUD
    i_oTo.w_DEDATFAT = this.w_DEDATFAT
    i_oTo.w_DENUMRET = this.w_DENUMRET
    i_oTo.w_DEVARIMP = this.w_DEVARIMP
    i_oTo.w_DEVARIMS = this.w_DEVARIMS
    i_oTo.w_IMPORTO_DOVUTO = this.w_IMPORTO_DOVUTO
    i_oTo.w_NUM_DOC = this.w_NUM_DOC
    i_oTo.w_IMPOSTA_DOVUTA = this.w_IMPOSTA_DOVUTA
    i_oTo.w_TOTIMPDOV = this.w_TOTIMPDOV
    i_oTo.w_TOTNUMDOC = this.w_TOTNUMDOC
    i_oTo.w_TOTIMPSDOV = this.w_TOTIMPSDOV
    i_oTo.w_DECOMSUP = this.w_DECOMSUP
    i_oTo.w_DETIPREG = this.w_DETIPREG
    i_oTo.w_RIFDOC = this.w_RIFDOC
    i_oTo.w_DEREGDES = this.w_DEREGDES
    i_oTo.w_SERIALP = this.w_SERIALP
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsai4mde as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 809
  Height = 544
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-11-29"
  HelpContextID=83167081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ANTIVDDE_IDX = 0
  PNT_MAST_IDX = 0
  cFile = "ANTIVDDE"
  cKeySelect = "DESERIAL,DE__AREA"
  cKeyWhere  = "DESERIAL=this.w_DESERIAL and DE__AREA=this.w_DE__AREA"
  cKeyDetail  = "DESERIAL=this.w_DESERIAL and DE__AREA=this.w_DE__AREA"
  cKeyWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';
      +'+" and DE__AREA="+cp_ToStrODBC(this.w_DE__AREA)';

  cKeyDetailWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';
      +'+" and DE__AREA="+cp_ToStrODBC(this.w_DE__AREA)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ANTIVDDE.DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';
      +'+" and ANTIVDDE.DE__AREA="+cp_ToStrODBC(this.w_DE__AREA)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ANTIVDDE.CPROWNUM '
  cPrg = "gsai4mde"
  cComment = "Dettaglio dati estratti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 16
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DESERIAL = space(10)
  w_DEDATOPE = ctod('  /  /  ')
  o_DEDATOPE = ctod('  /  /  ')
  w_DE__AREA = space(1)
  w_DENUMFAT = space(15)
  w_DEIMPDOC = 0
  o_DEIMPDOC = 0
  w_DEIMPOST = 0
  o_DEIMPOST = 0
  w_DERIFPNT = space(10)
  w_DETIPOPE = space(1)
  w_DERIFCON = space(30)
  w_DETIPFAT = space(1)
  w_DEFLGEXT = space(1)
  w_DEESCLUD = space(1)
  o_DEESCLUD = space(1)
  w_DEDATFAT = ctod('  /  /  ')
  w_DENUMRET = space(15)
  w_DEVARIMP = space(1)
  w_DEVARIMS = space(1)
  w_IMPORTO_DOVUTO = 0
  w_NUM_DOC = 0
  w_IMPOSTA_DOVUTA = 0
  w_TOTIMPDOV = 0
  w_TOTNUMDOC = 0
  w_TOTIMPSDOV = 0
  w_DECOMSUP = space(1)
  w_DETIPREG = space(1)
  w_RIFDOC = space(10)
  w_DEREGDES = space(0)
  w_SERIALP = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai4mdePag1","gsai4mde",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='ANTIVDDE'
    * --- Area Manuale = Open Work Table
    * --- gsai4mde
    * colora le righe che sono escluse da generazione
    This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
      "IIF(EMPTY(NVL(t_DEESCLUD,0)),IIF(empty(nvl(t_DETIPREG,'')),RGB(255,255,255),RGB(255,255,0)),RGB(255,0,0))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVDDE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVDDE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsai4mde'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ANTIVDDE where DESERIAL=KeySet.DESERIAL
    *                            and DE__AREA=KeySet.DE__AREA
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ANTIVDDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDDE_IDX,2],this.bLoadRecFilter,this.ANTIVDDE_IDX,"gsai4mde")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVDDE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVDDE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVDDE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  ,'DE__AREA',this.w_DE__AREA  )
      select * from (i_cTable) ANTIVDDE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTIMPDOV = 0
        .w_TOTNUMDOC = 0
        .w_TOTIMPSDOV = 0
        .w_DESERIAL = NVL(DESERIAL,space(10))
        .w_DE__AREA = NVL(DE__AREA,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ANTIVDDE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTIMPDOV = 0
      this.w_TOTNUMDOC = 0
      this.w_TOTIMPSDOV = 0
      scan
        with this
          .w_RIFDOC = space(10)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DEDATOPE = NVL(cp_ToDate(DEDATOPE),ctod("  /  /  "))
          .w_DENUMFAT = NVL(DENUMFAT,space(15))
          .w_DEIMPDOC = NVL(DEIMPDOC,0)
          .w_DEIMPOST = NVL(DEIMPOST,0)
          .w_DERIFPNT = NVL(DERIFPNT,space(10))
          .link_2_5('Load')
          .w_DETIPOPE = NVL(DETIPOPE,space(1))
          .w_DERIFCON = NVL(DERIFCON,space(30))
          .w_DETIPFAT = NVL(DETIPFAT,space(1))
          .w_DEFLGEXT = NVL(DEFLGEXT,space(1))
          .w_DEESCLUD = NVL(DEESCLUD,space(1))
          .w_DEDATFAT = NVL(cp_ToDate(DEDATFAT),ctod("  /  /  "))
          .w_DENUMRET = NVL(DENUMRET,space(15))
          .w_DEVARIMP = NVL(DEVARIMP,space(1))
          .w_DEVARIMS = NVL(DEVARIMS,space(1))
        .w_IMPORTO_DOVUTO = IIF(.w_DEESCLUD='S', 0, IIF(.w_DEVARIMP="1",-1,1)*.w_DEIMPDOC)
        .w_NUM_DOC = IIF(.w_DEESCLUD='S', 0, IIF(Not Empty(.w_DEDATOPE),1,0))
        .w_IMPOSTA_DOVUTA = IIF(.w_DEESCLUD='S', 0, IIF(.w_DEVARIMS="1",-1,1)*.w_DEIMPOST)
          .w_DECOMSUP = NVL(DECOMSUP,space(1))
          .w_DETIPREG = NVL(DETIPREG,space(1))
          .w_DEREGDES = NVL(DEREGDES,space(0))
        .w_SERIALP = .w_DERIFPNT
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTIMPDOV = .w_TOTIMPDOV+.w_IMPORTO_DOVUTO
          .w_TOTNUMDOC = .w_TOTNUMDOC+.w_NUM_DOC
          .w_TOTIMPSDOV = .w_TOTIMPSDOV+.w_IMPOSTA_DOVUTA
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_8.enabled = .oPgFrm.Page1.oPag.oBtn_2_8.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_22.enabled = .oPgFrm.Page1.oPag.oBtn_2_22.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_26.enabled = .oPgFrm.Page1.oPag.oBtn_2_26.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DESERIAL=space(10)
      .w_DEDATOPE=ctod("  /  /  ")
      .w_DE__AREA=space(1)
      .w_DENUMFAT=space(15)
      .w_DEIMPDOC=0
      .w_DEIMPOST=0
      .w_DERIFPNT=space(10)
      .w_DETIPOPE=space(1)
      .w_DERIFCON=space(30)
      .w_DETIPFAT=space(1)
      .w_DEFLGEXT=space(1)
      .w_DEESCLUD=space(1)
      .w_DEDATFAT=ctod("  /  /  ")
      .w_DENUMRET=space(15)
      .w_DEVARIMP=space(1)
      .w_DEVARIMS=space(1)
      .w_IMPORTO_DOVUTO=0
      .w_NUM_DOC=0
      .w_IMPOSTA_DOVUTA=0
      .w_TOTIMPDOV=0
      .w_TOTNUMDOC=0
      .w_TOTIMPSDOV=0
      .w_DECOMSUP=space(1)
      .w_DETIPREG=space(1)
      .w_RIFDOC=space(10)
      .w_DEREGDES=space(0)
      .w_SERIALP=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,7,.f.)
        if not(empty(.w_DERIFPNT))
         .link_2_5('Full')
        endif
        .w_DETIPOPE = "N"
        .DoRTCalc(9,9,.f.)
        .w_DETIPFAT = "S"
        .w_DEFLGEXT = "N"
        .w_DEESCLUD = 'N'
        .DoRTCalc(13,14,.f.)
        .w_DEVARIMP = "1"
        .w_DEVARIMS = "1"
        .w_IMPORTO_DOVUTO = IIF(.w_DEESCLUD='S', 0, IIF(.w_DEVARIMP="1",-1,1)*.w_DEIMPDOC)
        .w_NUM_DOC = IIF(.w_DEESCLUD='S', 0, IIF(Not Empty(.w_DEDATOPE),1,0))
        .w_IMPOSTA_DOVUTA = IIF(.w_DEESCLUD='S', 0, IIF(.w_DEVARIMS="1",-1,1)*.w_DEIMPOST)
        .DoRTCalc(20,22,.f.)
        .w_DECOMSUP = 'N'
        .DoRTCalc(24,26,.f.)
        .w_SERIALP = .w_DERIFPNT
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVDDE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_8.enabled = this.oPgFrm.Page1.oPag.oBtn_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_22.enabled = this.oPgFrm.Page1.oPag.oBtn_2_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_26.enabled = this.oPgFrm.Page1.oPag.oBtn_2_26.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDETIPOPE_2_6.enabled = i_bVal
      .Page1.oPag.oDERIFCON_2_7.enabled = i_bVal
      .Page1.oPag.oDETIPFAT_2_9.enabled = i_bVal
      .Page1.oPag.oDEFLGEXT_2_10.enabled = i_bVal
      .Page1.oPag.oDEESCLUD_2_11.enabled = i_bVal
      .Page1.oPag.oDEREGDES_2_23.enabled = i_bVal
      .Page1.oPag.oBtn_2_8.enabled = .Page1.oPag.oBtn_2_8.mCond()
      .Page1.oPag.oBtn_2_22.enabled = .Page1.oPag.oBtn_2_22.mCond()
      .Page1.oPag.oBtn_2_26.enabled = .Page1.oPag.oBtn_2_26.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ANTIVDDE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVDDE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESERIAL,"DESERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__AREA,"DE__AREA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DEDATOPE D(8);
      ,t_DENUMFAT C(15);
      ,t_DEIMPDOC N(18,4);
      ,t_DEIMPOST N(18,4);
      ,t_DETIPOPE N(3);
      ,t_DERIFCON C(30);
      ,t_DETIPFAT N(3);
      ,t_DEFLGEXT N(3);
      ,t_DEESCLUD N(3);
      ,t_DEDATFAT D(8);
      ,t_DENUMRET C(15);
      ,t_DEVARIMP N(3);
      ,t_DEVARIMS N(3);
      ,t_DEREGDES M(10);
      ,CPROWNUM N(10);
      ,t_DERIFPNT C(10);
      ,t_IMPORTO_DOVUTO N(18,4);
      ,t_NUM_DOC N(4);
      ,t_IMPOSTA_DOVUTA N(18,4);
      ,t_DECOMSUP C(1);
      ,t_DETIPREG C(1);
      ,t_RIFDOC C(10);
      ,t_SERIALP C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsai4mdebodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATOPE_2_1.controlsource=this.cTrsName+'.t_DEDATOPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMFAT_2_2.controlsource=this.cTrsName+'.t_DENUMFAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPDOC_2_3.controlsource=this.cTrsName+'.t_DEIMPDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPOST_2_4.controlsource=this.cTrsName+'.t_DEIMPOST'
    this.oPgFRm.Page1.oPag.oDETIPOPE_2_6.controlsource=this.cTrsName+'.t_DETIPOPE'
    this.oPgFRm.Page1.oPag.oDERIFCON_2_7.controlsource=this.cTrsName+'.t_DERIFCON'
    this.oPgFRm.Page1.oPag.oDETIPFAT_2_9.controlsource=this.cTrsName+'.t_DETIPFAT'
    this.oPgFRm.Page1.oPag.oDEFLGEXT_2_10.controlsource=this.cTrsName+'.t_DEFLGEXT'
    this.oPgFRm.Page1.oPag.oDEESCLUD_2_11.controlsource=this.cTrsName+'.t_DEESCLUD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATFAT_2_12.controlsource=this.cTrsName+'.t_DEDATFAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMRET_2_13.controlsource=this.cTrsName+'.t_DENUMRET'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMP_2_14.controlsource=this.cTrsName+'.t_DEVARIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMS_2_15.controlsource=this.cTrsName+'.t_DEVARIMS'
    this.oPgFRm.Page1.oPag.oDEREGDES_2_23.controlsource=this.cTrsName+'.t_DEREGDES'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(85)
    this.AddVLine(202)
    this.AddVLine(319)
    this.AddVLine(437)
    this.AddVLine(519)
    this.AddVLine(641)
    this.AddVLine(712)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATOPE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVDDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDDE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVDDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDDE_IDX,2])
      *
      * insert into ANTIVDDE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVDDE')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVDDE')
        i_cFldBody=" "+;
                  "(DESERIAL,DEDATOPE,DE__AREA,DENUMFAT,DEIMPDOC"+;
                  ",DEIMPOST,DERIFPNT,DETIPOPE,DERIFCON,DETIPFAT"+;
                  ",DEFLGEXT,DEESCLUD,DEDATFAT,DENUMRET,DEVARIMP"+;
                  ",DEVARIMS,DECOMSUP,DETIPREG,DEREGDES,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DESERIAL)+","+cp_ToStrODBC(this.w_DEDATOPE)+","+cp_ToStrODBC(this.w_DE__AREA)+","+cp_ToStrODBC(this.w_DENUMFAT)+","+cp_ToStrODBC(this.w_DEIMPDOC)+;
             ","+cp_ToStrODBC(this.w_DEIMPOST)+","+cp_ToStrODBCNull(this.w_DERIFPNT)+","+cp_ToStrODBC(this.w_DETIPOPE)+","+cp_ToStrODBC(this.w_DERIFCON)+","+cp_ToStrODBC(this.w_DETIPFAT)+;
             ","+cp_ToStrODBC(this.w_DEFLGEXT)+","+cp_ToStrODBC(this.w_DEESCLUD)+","+cp_ToStrODBC(this.w_DEDATFAT)+","+cp_ToStrODBC(this.w_DENUMRET)+","+cp_ToStrODBC(this.w_DEVARIMP)+;
             ","+cp_ToStrODBC(this.w_DEVARIMS)+","+cp_ToStrODBC(this.w_DECOMSUP)+","+cp_ToStrODBC(this.w_DETIPREG)+","+cp_ToStrODBC(this.w_DEREGDES)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVDDE')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVDDE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DESERIAL',this.w_DESERIAL,'DE__AREA',this.w_DE__AREA)
        INSERT INTO (i_cTable) (;
                   DESERIAL;
                  ,DEDATOPE;
                  ,DE__AREA;
                  ,DENUMFAT;
                  ,DEIMPDOC;
                  ,DEIMPOST;
                  ,DERIFPNT;
                  ,DETIPOPE;
                  ,DERIFCON;
                  ,DETIPFAT;
                  ,DEFLGEXT;
                  ,DEESCLUD;
                  ,DEDATFAT;
                  ,DENUMRET;
                  ,DEVARIMP;
                  ,DEVARIMS;
                  ,DECOMSUP;
                  ,DETIPREG;
                  ,DEREGDES;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DESERIAL;
                  ,this.w_DEDATOPE;
                  ,this.w_DE__AREA;
                  ,this.w_DENUMFAT;
                  ,this.w_DEIMPDOC;
                  ,this.w_DEIMPOST;
                  ,this.w_DERIFPNT;
                  ,this.w_DETIPOPE;
                  ,this.w_DERIFCON;
                  ,this.w_DETIPFAT;
                  ,this.w_DEFLGEXT;
                  ,this.w_DEESCLUD;
                  ,this.w_DEDATFAT;
                  ,this.w_DENUMRET;
                  ,this.w_DEVARIMP;
                  ,this.w_DEVARIMS;
                  ,this.w_DECOMSUP;
                  ,this.w_DETIPREG;
                  ,this.w_DEREGDES;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ANTIVDDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDDE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DEDATOPE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVDDE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVDDE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DEDATOPE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ANTIVDDE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVDDE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DEDATOPE="+cp_ToStrODBC(this.w_DEDATOPE)+;
                     ",DENUMFAT="+cp_ToStrODBC(this.w_DENUMFAT)+;
                     ",DEIMPDOC="+cp_ToStrODBC(this.w_DEIMPDOC)+;
                     ",DEIMPOST="+cp_ToStrODBC(this.w_DEIMPOST)+;
                     ",DERIFPNT="+cp_ToStrODBCNull(this.w_DERIFPNT)+;
                     ",DETIPOPE="+cp_ToStrODBC(this.w_DETIPOPE)+;
                     ",DERIFCON="+cp_ToStrODBC(this.w_DERIFCON)+;
                     ",DETIPFAT="+cp_ToStrODBC(this.w_DETIPFAT)+;
                     ",DEFLGEXT="+cp_ToStrODBC(this.w_DEFLGEXT)+;
                     ",DEESCLUD="+cp_ToStrODBC(this.w_DEESCLUD)+;
                     ",DEDATFAT="+cp_ToStrODBC(this.w_DEDATFAT)+;
                     ",DENUMRET="+cp_ToStrODBC(this.w_DENUMRET)+;
                     ",DEVARIMP="+cp_ToStrODBC(this.w_DEVARIMP)+;
                     ",DEVARIMS="+cp_ToStrODBC(this.w_DEVARIMS)+;
                     ",DECOMSUP="+cp_ToStrODBC(this.w_DECOMSUP)+;
                     ",DETIPREG="+cp_ToStrODBC(this.w_DETIPREG)+;
                     ",DEREGDES="+cp_ToStrODBC(this.w_DEREGDES)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVDDE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DEDATOPE=this.w_DEDATOPE;
                     ,DENUMFAT=this.w_DENUMFAT;
                     ,DEIMPDOC=this.w_DEIMPDOC;
                     ,DEIMPOST=this.w_DEIMPOST;
                     ,DERIFPNT=this.w_DERIFPNT;
                     ,DETIPOPE=this.w_DETIPOPE;
                     ,DERIFCON=this.w_DERIFCON;
                     ,DETIPFAT=this.w_DETIPFAT;
                     ,DEFLGEXT=this.w_DEFLGEXT;
                     ,DEESCLUD=this.w_DEESCLUD;
                     ,DEDATFAT=this.w_DEDATFAT;
                     ,DENUMRET=this.w_DENUMRET;
                     ,DEVARIMP=this.w_DEVARIMP;
                     ,DEVARIMS=this.w_DEVARIMS;
                     ,DECOMSUP=this.w_DECOMSUP;
                     ,DETIPREG=this.w_DETIPREG;
                     ,DEREGDES=this.w_DEREGDES;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVDDE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDDE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DEDATOPE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ANTIVDDE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DEDATOPE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVDDE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVDDE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
          .link_2_5('Full')
        .DoRTCalc(8,16,.t.)
          .w_TOTIMPDOV = .w_TOTIMPDOV-.w_importo_dovuto
          .w_IMPORTO_DOVUTO = IIF(.w_DEESCLUD='S', 0, IIF(.w_DEVARIMP="1",-1,1)*.w_DEIMPDOC)
          .w_TOTIMPDOV = .w_TOTIMPDOV+.w_importo_dovuto
        if .o_DEDATOPE<>.w_DEDATOPE.or. .o_DEESCLUD<>.w_DEESCLUD
          .w_TOTNUMDOC = .w_TOTNUMDOC-.w_num_doc
          .w_NUM_DOC = IIF(.w_DEESCLUD='S', 0, IIF(Not Empty(.w_DEDATOPE),1,0))
          .w_TOTNUMDOC = .w_TOTNUMDOC+.w_num_doc
        endif
          .w_TOTIMPSDOV = .w_TOTIMPSDOV-.w_imposta_dovuta
          .w_IMPOSTA_DOVUTA = IIF(.w_DEESCLUD='S', 0, IIF(.w_DEVARIMS="1",-1,1)*.w_DEIMPOST)
          .w_TOTIMPSDOV = .w_TOTIMPSDOV+.w_imposta_dovuta
        if .o_DEESCLUD<>.w_DEESCLUD.or. .o_DEIMPOST<>.w_DEIMPOST.or. .o_DEIMPDOC<>.w_DEIMPDOC
          .Calculate_GSMXETPGAT()
        endif
        .DoRTCalc(20,26,.t.)
          .w_SERIALP = .w_DERIFPNT
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DERIFPNT with this.w_DERIFPNT
      replace t_IMPORTO_DOVUTO with this.w_IMPORTO_DOVUTO
      replace t_NUM_DOC with this.w_NUM_DOC
      replace t_IMPOSTA_DOVUTA with this.w_IMPOSTA_DOVUTA
      replace t_DECOMSUP with this.w_DECOMSUP
      replace t_DETIPREG with this.w_DETIPREG
      replace t_RIFDOC with this.w_RIFDOC
      replace t_SERIALP with this.w_SERIALP
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_GSMXETPGAT()
    with this
          * --- Controllo importi negativi
          GSAI_BCE(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_8.enabled =this.oPgFrm.Page1.oPag.oBtn_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_22.enabled =this.oPgFrm.Page1.oPag.oBtn_2_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_26.enabled =this.oPgFrm.Page1.oPag.oBtn_2_26.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDETIPOPE_2_6.visible=!this.oPgFrm.Page1.oPag.oDETIPOPE_2_6.mHide()
    this.oPgFrm.Page1.oPag.oDERIFCON_2_7.visible=!this.oPgFrm.Page1.oPag.oDERIFCON_2_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_8.visible=!this.oPgFrm.Page1.oPag.oBtn_2_8.mHide()
    this.oPgFrm.Page1.oPag.oDETIPFAT_2_9.visible=!this.oPgFrm.Page1.oPag.oDETIPFAT_2_9.mHide()
    this.oPgFrm.Page1.oPag.oDEFLGEXT_2_10.visible=!this.oPgFrm.Page1.oPag.oDEFLGEXT_2_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_22.visible=!this.oPgFrm.Page1.oPag.oBtn_2_22.mHide()
    this.oPgFrm.Page1.oPag.oDEREGDES_2_23.visible=!this.oPgFrm.Page1.oPag.oDEREGDES_2_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_26.visible=!this.oPgFrm.Page1.oPag.oBtn_2_26.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DERIFPNT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_lTable = "PNT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2], .t., this.PNT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DERIFPNT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DERIFPNT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNRIFDOC";
                   +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(this.w_DERIFPNT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_DERIFPNT)
            select PNSERIAL,PNRIFDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DERIFPNT = NVL(_Link_.PNSERIAL,space(10))
      this.w_RIFDOC = NVL(_Link_.PNRIFDOC,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_DERIFPNT = space(10)
      endif
      this.w_RIFDOC = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)
      cp_ShowWarn(i_cKey,this.PNT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DERIFPNT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDETIPOPE_2_6.RadioValue()==this.w_DETIPOPE)
      this.oPgFrm.Page1.oPag.oDETIPOPE_2_6.SetRadio()
      replace t_DETIPOPE with this.oPgFrm.Page1.oPag.oDETIPOPE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDERIFCON_2_7.value==this.w_DERIFCON)
      this.oPgFrm.Page1.oPag.oDERIFCON_2_7.value=this.w_DERIFCON
      replace t_DERIFCON with this.oPgFrm.Page1.oPag.oDERIFCON_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPFAT_2_9.RadioValue()==this.w_DETIPFAT)
      this.oPgFrm.Page1.oPag.oDETIPFAT_2_9.SetRadio()
      replace t_DETIPFAT with this.oPgFrm.Page1.oPag.oDETIPFAT_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFLGEXT_2_10.RadioValue()==this.w_DEFLGEXT)
      this.oPgFrm.Page1.oPag.oDEFLGEXT_2_10.SetRadio()
      replace t_DEFLGEXT with this.oPgFrm.Page1.oPag.oDEFLGEXT_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDEESCLUD_2_11.RadioValue()==this.w_DEESCLUD)
      this.oPgFrm.Page1.oPag.oDEESCLUD_2_11.SetRadio()
      replace t_DEESCLUD with this.oPgFrm.Page1.oPag.oDEESCLUD_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMPDOV_3_1.value==this.w_TOTIMPDOV)
      this.oPgFrm.Page1.oPag.oTOTIMPDOV_3_1.value=this.w_TOTIMPDOV
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTNUMDOC_3_2.value==this.w_TOTNUMDOC)
      this.oPgFrm.Page1.oPag.oTOTNUMDOC_3_2.value=this.w_TOTNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMPSDOV_3_3.value==this.w_TOTIMPSDOV)
      this.oPgFrm.Page1.oPag.oTOTIMPSDOV_3_3.value=this.w_TOTIMPSDOV
    endif
    if not(this.oPgFrm.Page1.oPag.oDEREGDES_2_23.value==this.w_DEREGDES)
      this.oPgFrm.Page1.oPag.oDEREGDES_2_23.value=this.w_DEREGDES
      replace t_DEREGDES with this.oPgFrm.Page1.oPag.oDEREGDES_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATOPE_2_1.value==this.w_DEDATOPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATOPE_2_1.value=this.w_DEDATOPE
      replace t_DEDATOPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATOPE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMFAT_2_2.value==this.w_DENUMFAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMFAT_2_2.value=this.w_DENUMFAT
      replace t_DENUMFAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMFAT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPDOC_2_3.value==this.w_DEIMPDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPDOC_2_3.value=this.w_DEIMPDOC
      replace t_DEIMPDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPDOC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPOST_2_4.value==this.w_DEIMPOST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPOST_2_4.value=this.w_DEIMPOST
      replace t_DEIMPOST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEIMPOST_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATFAT_2_12.value==this.w_DEDATFAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATFAT_2_12.value=this.w_DEDATFAT
      replace t_DEDATFAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDATFAT_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMRET_2_13.value==this.w_DENUMRET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMRET_2_13.value=this.w_DENUMRET
      replace t_DENUMRET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMRET_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMP_2_14.RadioValue()==this.w_DEVARIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMP_2_14.SetRadio()
      replace t_DEVARIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMP_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMS_2_15.RadioValue()==this.w_DEVARIMS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMS_2_15.SetRadio()
      replace t_DEVARIMS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMS_2_15.value
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVDDE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_DEDATOPE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DEDATOPE = this.w_DEDATOPE
    this.o_DEIMPDOC = this.w_DEIMPDOC
    this.o_DEIMPOST = this.w_DEIMPOST
    this.o_DEESCLUD = this.w_DEESCLUD
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DEDATOPE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DEDATOPE=ctod("  /  /  ")
      .w_DENUMFAT=space(15)
      .w_DEIMPDOC=0
      .w_DEIMPOST=0
      .w_DERIFPNT=space(10)
      .w_DETIPOPE=space(1)
      .w_DERIFCON=space(30)
      .w_DETIPFAT=space(1)
      .w_DEFLGEXT=space(1)
      .w_DEESCLUD=space(1)
      .w_DEDATFAT=ctod("  /  /  ")
      .w_DENUMRET=space(15)
      .w_DEVARIMP=space(1)
      .w_DEVARIMS=space(1)
      .w_IMPORTO_DOVUTO=0
      .w_NUM_DOC=0
      .w_IMPOSTA_DOVUTA=0
      .w_DECOMSUP=space(1)
      .w_DETIPREG=space(1)
      .w_RIFDOC=space(10)
      .w_DEREGDES=space(0)
      .w_SERIALP=space(10)
      .DoRTCalc(1,7,.f.)
      if not(empty(.w_DERIFPNT))
        .link_2_5('Full')
      endif
        .w_DETIPOPE = "N"
      .DoRTCalc(9,9,.f.)
        .w_DETIPFAT = "S"
        .w_DEFLGEXT = "N"
        .w_DEESCLUD = 'N'
      .DoRTCalc(13,14,.f.)
        .w_DEVARIMP = "1"
        .w_DEVARIMS = "1"
        .w_IMPORTO_DOVUTO = IIF(.w_DEESCLUD='S', 0, IIF(.w_DEVARIMP="1",-1,1)*.w_DEIMPDOC)
        .w_NUM_DOC = IIF(.w_DEESCLUD='S', 0, IIF(Not Empty(.w_DEDATOPE),1,0))
        .w_IMPOSTA_DOVUTA = IIF(.w_DEESCLUD='S', 0, IIF(.w_DEVARIMS="1",-1,1)*.w_DEIMPOST)
      .DoRTCalc(20,22,.f.)
        .w_DECOMSUP = 'N'
      .DoRTCalc(24,26,.f.)
        .w_SERIALP = .w_DERIFPNT
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DEDATOPE = t_DEDATOPE
    this.w_DENUMFAT = t_DENUMFAT
    this.w_DEIMPDOC = t_DEIMPDOC
    this.w_DEIMPOST = t_DEIMPOST
    this.w_DERIFPNT = t_DERIFPNT
    this.w_DETIPOPE = this.oPgFrm.Page1.oPag.oDETIPOPE_2_6.RadioValue(.t.)
    this.w_DERIFCON = t_DERIFCON
    this.w_DETIPFAT = this.oPgFrm.Page1.oPag.oDETIPFAT_2_9.RadioValue(.t.)
    this.w_DEFLGEXT = this.oPgFrm.Page1.oPag.oDEFLGEXT_2_10.RadioValue(.t.)
    this.w_DEESCLUD = this.oPgFrm.Page1.oPag.oDEESCLUD_2_11.RadioValue(.t.)
    this.w_DEDATFAT = t_DEDATFAT
    this.w_DENUMRET = t_DENUMRET
    this.w_DEVARIMP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMP_2_14.RadioValue(.t.)
    this.w_DEVARIMS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMS_2_15.RadioValue(.t.)
    this.w_IMPORTO_DOVUTO = t_IMPORTO_DOVUTO
    this.w_NUM_DOC = t_NUM_DOC
    this.w_IMPOSTA_DOVUTA = t_IMPOSTA_DOVUTA
    this.w_DECOMSUP = t_DECOMSUP
    this.w_DETIPREG = t_DETIPREG
    this.w_RIFDOC = t_RIFDOC
    this.w_DEREGDES = t_DEREGDES
    this.w_SERIALP = t_SERIALP
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DEDATOPE with this.w_DEDATOPE
    replace t_DENUMFAT with this.w_DENUMFAT
    replace t_DEIMPDOC with this.w_DEIMPDOC
    replace t_DEIMPOST with this.w_DEIMPOST
    replace t_DERIFPNT with this.w_DERIFPNT
    replace t_DETIPOPE with this.oPgFrm.Page1.oPag.oDETIPOPE_2_6.ToRadio()
    replace t_DERIFCON with this.w_DERIFCON
    replace t_DETIPFAT with this.oPgFrm.Page1.oPag.oDETIPFAT_2_9.ToRadio()
    replace t_DEFLGEXT with this.oPgFrm.Page1.oPag.oDEFLGEXT_2_10.ToRadio()
    replace t_DEESCLUD with this.oPgFrm.Page1.oPag.oDEESCLUD_2_11.ToRadio()
    replace t_DEDATFAT with this.w_DEDATFAT
    replace t_DENUMRET with this.w_DENUMRET
    replace t_DEVARIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMP_2_14.ToRadio()
    replace t_DEVARIMS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVARIMS_2_15.ToRadio()
    replace t_IMPORTO_DOVUTO with this.w_IMPORTO_DOVUTO
    replace t_NUM_DOC with this.w_NUM_DOC
    replace t_IMPOSTA_DOVUTA with this.w_IMPOSTA_DOVUTA
    replace t_DECOMSUP with this.w_DECOMSUP
    replace t_DETIPREG with this.w_DETIPREG
    replace t_RIFDOC with this.w_RIFDOC
    replace t_DEREGDES with this.w_DEREGDES
    replace t_SERIALP with this.w_SERIALP
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTIMPDOV = .w_TOTIMPDOV-.w_importo_dovuto
        .w_TOTNUMDOC = .w_TOTNUMDOC-.w_num_doc
        .w_TOTIMPSDOV = .w_TOTIMPSDOV-.w_imposta_dovuta
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsai4mdePag1 as StdContainer
  Width  = 805
  height = 544
  stdWidth  = 805
  stdheight = 544
  resizeXpos=683
  resizeYpos=233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=3, width=793,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="DEDATOPE",Label1="Data op.",Field2="DENUMFAT",Label2="Num. nota var.",Field3="DEIMPDOC",Label3="Importo dovuto",Field4="DEIMPOST",Label4="Imposta dovuta",Field5="DEDATFAT",Label5="Data fatt. rett.",Field6="DENUMRET",Label6="Num fatt. rett.",Field7="DEVARIMP",Label7="Var. impo.",Field8="DEVARIMS",Label8="Var. impos.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 232923770

  add object oStr_1_3 as StdString with uid="VLWOBLLDPG",Visible=.t., Left=382, Top=338,;
    Alignment=1, Width=147, Height=19,;
    Caption="Totale documenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="NAQEPHQRAW",Visible=.t., Left=382, Top=365,;
    Alignment=1, Width=147, Height=19,;
    Caption="Totale importo dovuto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="YONGEJOKIC",Visible=.t., Left=382, Top=392,;
    Alignment=1, Width=147, Height=19,;
    Caption="Totale imposta dovuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="IWZHVZMLWW",Visible=.t., Left=4, Top=372,;
    Alignment=1, Width=144, Height=18,;
    Caption="Riferimento contratto:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_DECOMSUP='N')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="GQIDBMZMXF",Visible=.t., Left=19, Top=344,;
    Alignment=1, Width=129, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_DECOMSUP='N')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="TESMLBAGGH",Visible=.t., Left=4, Top=399,;
    Alignment=1, Width=144, Height=18,;
    Caption="Tipo fattura:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_DECOMSUP='N')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="GYXCNXPABH",Visible=.t., Left=4, Top=434,;
    Alignment=1, Width=144, Height=18,;
    Caption="Tipo estrazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_DECOMSUP='N')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="SKLEKASCVU",Visible=.t., Left=88, Top=509,;
    Alignment=0, Width=256, Height=18,;
    Caption="Parametri causali note di variazione"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_DETIPREG<>'B')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="FTRRAOOTFE",Visible=.t., Left=88, Top=509,;
    Alignment=0, Width=256, Height=18,;
    Caption="Parametri controparte"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_DETIPREG<>'C')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="RFTSAFOPSG",Visible=.t., Left=88, Top=509,;
    Alignment=0, Width=256, Height=18,;
    Caption="Parametri dati IVA"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_DETIPREG<>'A')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="IJIJKBJTPC",Visible=.t., Left=88, Top=509,;
    Alignment=0, Width=256, Height=18,;
    Caption="Contratto collegato"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_DETIPREG<>'E')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=26,;
    width=789+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*16*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=27,width=788+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*16*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDETIPOPE_2_6.Refresh()
      this.Parent.oDERIFCON_2_7.Refresh()
      this.Parent.oDETIPFAT_2_9.Refresh()
      this.Parent.oDEFLGEXT_2_10.Refresh()
      this.Parent.oDEESCLUD_2_11.Refresh()
      this.Parent.oDEREGDES_2_23.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDETIPOPE_2_6 as StdTrsCombo with uid="LFTRWUNPLL",rtrep=.t.,;
    cFormVar="w_DETIPOPE", RowSource=""+"Nessuno,"+"Corrispettivi periodici,"+"Contratti collegati" , ;
    ToolTipText = "Tipo operazione",;
    HelpContextID = 257524347,;
    Height=25, Width=172, Left=151, Top=342,;
    cTotal="", cQueryName = "DETIPOPE",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDETIPOPE_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DETIPOPE,&i_cF..t_DETIPOPE),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'P',;
    iif(xVal =3,'C',;
    space(1)))))
  endfunc
  func oDETIPOPE_2_6.GetRadio()
    this.Parent.oContained.w_DETIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oDETIPOPE_2_6.ToRadio()
    this.Parent.oContained.w_DETIPOPE=trim(this.Parent.oContained.w_DETIPOPE)
    return(;
      iif(this.Parent.oContained.w_DETIPOPE=='N',1,;
      iif(this.Parent.oContained.w_DETIPOPE=='P',2,;
      iif(this.Parent.oContained.w_DETIPOPE=='C',3,;
      0))))
  endfunc

  func oDETIPOPE_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDETIPOPE_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DECOMSUP='N')
    endwith
    endif
  endfunc

  add object oDERIFCON_2_7 as StdTrsField with uid="QYHXSKMWJH",rtseq=9,rtrep=.t.,;
    cFormVar="w_DERIFCON",value=space(30),;
    HelpContextID = 222731644,;
    cTotal="", bFixedPos=.t., cQueryName = "DERIFCON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=151, Top=370, InputMask=replicate('X',30)

  func oDERIFCON_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DECOMSUP='N')
    endwith
    endif
  endfunc

  add object oBtn_2_8 as StdButton with uid="XEDXHWOFBI",width=48,height=45,;
   left=695, top=336,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza la registrazione di Prima Nota";
    , HelpContextID = 105589750;
    , Caption='Re\<g.Cont',tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_8.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_DERIFPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_8.mCond()
    with this.Parent.oContained
      return (g_Coge='S' And Not Empty(.w_Derifpnt))
    endwith
  endfunc

  func oBtn_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Derifpnt) Or g_Coge<>'S')
    endwith
   endif
  endfunc

  add object oDETIPFAT_2_9 as StdTrsCombo with uid="FHNXTUTUGO",rtrep=.t.,;
    cFormVar="w_DETIPFAT", RowSource=""+"Saldo,"+"Acconto,"+"Nota rettifica,"+"Corrispettivi" , ;
    ToolTipText = "Tipo fattura",;
    HelpContextID = 106529418,;
    Height=25, Width=172, Left=151, Top=398,;
    cTotal="", cQueryName = "DETIPFAT",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDETIPFAT_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DETIPFAT,&i_cF..t_DETIPFAT),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'A',;
    iif(xVal =3,'N',;
    iif(xVal =4,'C',;
    space(1))))))
  endfunc
  func oDETIPFAT_2_9.GetRadio()
    this.Parent.oContained.w_DETIPFAT = this.RadioValue()
    return .t.
  endfunc

  func oDETIPFAT_2_9.ToRadio()
    this.Parent.oContained.w_DETIPFAT=trim(this.Parent.oContained.w_DETIPFAT)
    return(;
      iif(this.Parent.oContained.w_DETIPFAT=='S',1,;
      iif(this.Parent.oContained.w_DETIPFAT=='A',2,;
      iif(this.Parent.oContained.w_DETIPFAT=='N',3,;
      iif(this.Parent.oContained.w_DETIPFAT=='C',4,;
      0)))))
  endfunc

  func oDETIPFAT_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDETIPFAT_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DECOMSUP='N')
    endwith
    endif
  endfunc

  add object oDEFLGEXT_2_10 as StdTrsCombo with uid="AAFLWYGTAM",rtrep=.t.,;
    cFormVar="w_DEFLGEXT", RowSource=""+"Inclusione forzata,"+"Esclusione forzata,"+"Nessuna forzatura" , ;
    ToolTipText = "Flag estrazione",;
    HelpContextID = 187981174,;
    Height=25, Width=172, Left=151, Top=433,;
    cTotal="", cQueryName = "DEFLGEXT",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDEFLGEXT_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DEFLGEXT,&i_cF..t_DEFLGEXT),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'F',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oDEFLGEXT_2_10.GetRadio()
    this.Parent.oContained.w_DEFLGEXT = this.RadioValue()
    return .t.
  endfunc

  func oDEFLGEXT_2_10.ToRadio()
    this.Parent.oContained.w_DEFLGEXT=trim(this.Parent.oContained.w_DEFLGEXT)
    return(;
      iif(this.Parent.oContained.w_DEFLGEXT=='I',1,;
      iif(this.Parent.oContained.w_DEFLGEXT=='F',2,;
      iif(this.Parent.oContained.w_DEFLGEXT=='N',3,;
      0))))
  endfunc

  func oDEFLGEXT_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDEFLGEXT_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DECOMSUP='N')
    endwith
    endif
  endfunc

  add object oDEESCLUD_2_11 as StdTrsCheck with uid="BXMTEATYME",rtrep=.t.,;
    cFormVar="w_DEESCLUD",  caption="Escludi documento da generazione",;
    ToolTipText = "Se attivo, esclude la riga del documento dalla generazione",;
    HelpContextID = 194155130,;
    Left=88, Top=468,;
    cTotal="", cQueryName = "DEESCLUD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oDEESCLUD_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DEESCLUD,&i_cF..t_DEESCLUD),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDEESCLUD_2_11.GetRadio()
    this.Parent.oContained.w_DEESCLUD = this.RadioValue()
    return .t.
  endfunc

  func oDEESCLUD_2_11.ToRadio()
    this.Parent.oContained.w_DEESCLUD=trim(this.Parent.oContained.w_DEESCLUD)
    return(;
      iif(this.Parent.oContained.w_DEESCLUD=='S',1,;
      0))
  endfunc

  func oDEESCLUD_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oBtn_2_22 as StdButton with uid="HSOTTQXQFK",width=48,height=45,;
   left=747, top=336,;
    CpPicture="bmp\doc.bmp", caption="", nPag=2;
    , ToolTipText = "Visualizza il documento che ha generato il movimento";
    , HelpContextID = 62852086;
    , Caption='\<Docum.',tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_22.Click()
      with this.Parent.oContained
        GSAR_BZD(this.Parent.oContained,.w_RIFDOC)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_22.mCond()
    with this.Parent.oContained
      return (g_MAGA='S' And Not Empty(.w_Rifdoc) And UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
  endfunc

  func oBtn_2_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Rifdoc) Or g_MAGA<>'S' Or UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
   endif
  endfunc

  add object oDEREGDES_2_23 as StdTrsMemo with uid="IFNPSOLYWR",rtseq=26,rtrep=.t.,;
    cFormVar="w_DEREGDES",value=space(0),;
    HelpContextID = 63267465,;
    cTotal="", bFixedPos=.t., cQueryName = "DEREGDES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=124, Width=437, Left=364, Top=417, tabstop = .f., readonly = .t.

  func oDEREGDES_2_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DETIPREG<>'C' AND .w_DETIPREG<>'A' AND .w_DETIPREG<>'E')
    endwith
    endif
  endfunc

  add object oBtn_2_26 as StdButton with uid="MCJECRFLJR",width=48,height=45,;
   left=747, top=336,;
    CpPicture="bmp\origine.bmp", caption="", nPag=2;
    , ToolTipText = "Visualizza il documento di origine";
    , HelpContextID = 267845658;
    , Caption='\<Origine',tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_26.Click()
      with this.Parent.oContained
        do GSAR_BZO with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_26.mCond()
    with this.Parent.oContained
      return (g_MAGA='S' And Not Empty(.w_Rifdoc) And UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc

  func oBtn_2_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Rifdoc) Or g_MAGA<>'S' Or UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTIMPDOV_3_1 as StdField with uid="WCTZFYREZH",rtseq=20,rtrep=.f.,;
    cFormVar="w_TOTIMPDOV",value=0,enabled=.f.,;
    HelpContextID = 2724581,;
    cQueryName = "TOTIMPDOV",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=536, Top=363, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oTOTNUMDOC_3_2 as StdField with uid="LBWRXLEHHJ",rtseq=21,rtrep=.f.,;
    cFormVar="w_TOTNUMDOC",value=0,enabled=.f.,;
    HelpContextID = 229544373,;
    cQueryName = "TOTNUMDOC",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=536, Top=336

  add object oTOTIMPSDOV_3_3 as StdField with uid="WVJEMYJWZW",rtseq=22,rtrep=.f.,;
    cFormVar="w_TOTIMPSDOV",value=0,enabled=.f.,;
    HelpContextID = 2746474,;
    cQueryName = "TOTIMPSDOV",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=536, Top=390, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]
enddefine

* --- Defining Body row
define class tgsai4mdeBodyRow as CPBodyRowCnt
  Width=779
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDEDATOPE_2_1 as StdTrsField with uid="ITSADEEHTB",rtseq=2,rtrep=.t.,;
    cFormVar="w_DEDATOPE",value=ctod("  /  /  "),;
    ToolTipText = "Data operazione",;
    HelpContextID = 261128827,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=-2, Top=0, BackStyle=IIF(type('i_udisabledbackcolor')$'UL' And type('i_udisabledforecolor')$'UL',0,1)

  add object oDENUMFAT_2_2 as StdTrsField with uid="ALWAHUHDST",rtseq=4,rtrep=.t.,;
    cFormVar="w_DENUMFAT",value=space(15),;
    ToolTipText = "Numero fattura",;
    HelpContextID = 104145546,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=80, Top=0, InputMask=replicate('X',15)

  add object oDEIMPDOC_2_3 as StdTrsField with uid="LKYEQBFJDI",rtseq=5,rtrep=.t.,;
    cFormVar="w_DEIMPDOC",value=0,;
    HelpContextID = 195243399,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=197, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDEIMPOST_2_4 as StdTrsField with uid="CKUJNZOTJL",rtseq=6,rtrep=.t.,;
    cFormVar="w_DEIMPOST",value=0,;
    HelpContextID = 257741450,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=315, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDEDATFAT_2_12 as StdTrsField with uid="XDRBDTOYVL",rtseq=13,rtrep=.t.,;
    cFormVar="w_DEDATFAT",value=ctod("  /  /  "),;
    HelpContextID = 110133898,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=432, Top=0

  add object oDENUMRET_2_13 as StdTrsField with uid="JQCZKZQZKS",rtseq=14,rtrep=.t.,;
    cFormVar="w_DENUMRET",value=space(15),;
    HelpContextID = 37036682,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=514, Top=0, InputMask=replicate('X',15)

  add object oDEVARIMP_2_14 as StdTrsCombo with uid="FLCGFBSFMU",rtrep=.t.,;
    cFormVar="w_DEVARIMP", RowSource=""+"Credito,"+"Debito" , ;
    HelpContextID = 109993338,;
    Height=21, Width=68, Left=635, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDEVARIMP_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DEVARIMP,&i_cF..t_DEVARIMP),this.value)
    return(iif(xVal =1,"1",;
    iif(xVal =2,"2",;
    space(1))))
  endfunc
  func oDEVARIMP_2_14.GetRadio()
    this.Parent.oContained.w_DEVARIMP = this.RadioValue()
    return .t.
  endfunc

  func oDEVARIMP_2_14.ToRadio()
    this.Parent.oContained.w_DEVARIMP=trim(this.Parent.oContained.w_DEVARIMP)
    return(;
      iif(this.Parent.oContained.w_DEVARIMP=="1",1,;
      iif(this.Parent.oContained.w_DEVARIMP=="2",2,;
      0)))
  endfunc

  func oDEVARIMP_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDEVARIMS_2_15 as StdTrsCombo with uid="OAXFZBGQDW",rtrep=.t.,;
    cFormVar="w_DEVARIMS", RowSource=""+"Credito,"+"Debito" , ;
    HelpContextID = 109993335,;
    Height=21, Width=68, Left=706, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDEVARIMS_2_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DEVARIMS,&i_cF..t_DEVARIMS),this.value)
    return(iif(xVal =1,"1",;
    iif(xVal =2,"2",;
    space(1))))
  endfunc
  func oDEVARIMS_2_15.GetRadio()
    this.Parent.oContained.w_DEVARIMS = this.RadioValue()
    return .t.
  endfunc

  func oDEVARIMS_2_15.ToRadio()
    this.Parent.oContained.w_DEVARIMS=trim(this.Parent.oContained.w_DEVARIMS)
    return(;
      iif(this.Parent.oContained.w_DEVARIMS=="1",1,;
      iif(this.Parent.oContained.w_DEVARIMS=="2",2,;
      0)))
  endfunc

  func oDEVARIMS_2_15.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDEDATOPE_2_1.When()
    return(.t.)
  proc oDEDATOPE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDEDATOPE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=15
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai4mde','ANTIVDDE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DESERIAL=ANTIVDDE.DESERIAL";
  +" and "+i_cAliasName2+".DE__AREA=ANTIVDDE.DE__AREA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
