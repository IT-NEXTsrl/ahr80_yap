* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bct                                                        *
*              Calcolo totali dati estratti antievasione IVA                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-23                                                      *
* Last revis.: 2010-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bct",oParentObject)
return(i_retval)

define class tgsai_bct as StdBatch
  * --- Local variables
  w_GSAI_MDT = .NULL.
  w_GSAI_MDP = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calolo i totali
    * --- Cessioni
    * --- Acquisti
    * --- Note di variazione cessioni
    * --- Note di variazione acquisti
    * --- --
    this.oParentObject.w_DTBIMPTT = 0
    this.oParentObject.w_DTBIMPIM = 0
    this.oParentObject.w_DTSIMPTT = 0
    this.oParentObject.w_DTSIMPIM = 0
    this.oParentObject.w_DTBNOIMP = 0
    this.oParentObject.w_DTSNOIMP = 0
    this.oParentObject.w_DTOPESEN = 0
    this.oParentObject.w_DTBNOSOG = 0
    this.oParentObject.w_DTSNOSOG = 0
    * --- --
    this.oParentObject.w_DTBIMPTTA = 0
    this.oParentObject.w_DTBIMPIMA = 0
    this.oParentObject.w_DTSIMPTTA = 0
    this.oParentObject.w_DTSIMPIMA = 0
    this.oParentObject.w_DTBNOIMPA = 0
    this.oParentObject.w_DTSNOIMPA = 0
    this.oParentObject.w_DTOPESENA = 0
    this.oParentObject.w_DTBNOSOGA = 0
    this.oParentObject.w_DTSNOSOGA = 0
    * --- --
    this.oParentObject.w_DTBNVATT = 0
    this.oParentObject.w_DTBNVAIM = 0
    this.oParentObject.w_DTSNVATT = 0
    this.oParentObject.w_DTSNVAIM = 0
    this.oParentObject.w_DTBNVPTT = 0
    this.oParentObject.w_DTBNVPIM = 0
    this.oParentObject.w_DTSNVPTT = 0
    this.oParentObject.w_DTSNVPIM = 0
    * --- --
    this.oParentObject.w_DTBNVATTA = 0
    this.oParentObject.w_DTBNVAIMA = 0
    this.oParentObject.w_DTSNVATTA = 0
    this.oParentObject.w_DTSNVAIMA = 0
    this.oParentObject.w_DTBNVPTTA = 0
    this.oParentObject.w_DTBNVPIMA = 0
    this.oParentObject.w_DTSNVPTTA = 0
    this.oParentObject.w_DTSNVPIMA = 0
    this.w_GSAI_MDT = this.oParentObject.GSAI_MDT
    select sum(t_DTBIMPTT) as DTBIMPTT, sum(t_DTBIMPIM) as DTBIMPIM, sum(t_DTSIMPTT) as DTSIMPTT, sum(t_DTSIMPIM) as DTSIMPIM, sum(t_DTBNOIMP) as DTBNOIMP, sum(t_DTSNOIMP) as DTSNOIMP, sum(t_DTOPESEN) as DTOPESEN, sum(t_DTBNOSOG) as DTBNOSOG, sum(t_DTSNOSOG) as DTSNOSOG from (this.w_GSAI_MDT.cTrsName) into cursor _Curs_Totali
    if used("_Curs_Totali")
      this.oParentObject.w_DTBIMPTT = _Curs_Totali.DTBIMPTT
      this.oParentObject.w_DTBIMPIM = _Curs_Totali.DTBIMPIM
      this.oParentObject.w_DTSIMPTT = _Curs_Totali.DTSIMPTT
      this.oParentObject.w_DTSIMPIM = _Curs_Totali.DTSIMPIM
      this.oParentObject.w_DTBNOIMP = _Curs_Totali.DTBNOIMP
      this.oParentObject.w_DTSNOIMP = _Curs_Totali.DTSNOIMP
      this.oParentObject.w_DTOPESEN = _Curs_Totali.DTOPESEN
      this.oParentObject.w_DTBNOSOG = _Curs_Totali.DTBNOSOG
      this.oParentObject.w_DTSNOSOG = _Curs_Totali.DTSNOSOG
      use in _Curs_Totali
    endif
    this.w_GSAI_MDT = this.oParentObject.GSAIAMDT
    select sum(t_DTBIMPTT) as DTBIMPTT, sum(t_DTBIMPIM) as DTBIMPIM, sum(t_DTSIMPTT) as DTSIMPTT, sum(t_DTSIMPIM) as DTSIMPIM, sum(t_DTBNOIMP) as DTBNOIMP, sum(t_DTSNOIMP) as DTSNOIMP, sum(t_DTOPESEN) as DTOPESEN, sum(t_DTBNOSOG) as DTBNOSOG, sum(t_DTSNOSOG) as DTSNOSOG from (this.w_GSAI_MDT.cTrsName) into cursor _Curs_Totali
    if used("_Curs_Totali")
      this.oParentObject.w_DTBIMPTTA = _Curs_Totali.DTBIMPTT
      this.oParentObject.w_DTBIMPIMA = _Curs_Totali.DTBIMPIM
      this.oParentObject.w_DTSIMPTTA = _Curs_Totali.DTSIMPTT
      this.oParentObject.w_DTSIMPIMA = _Curs_Totali.DTSIMPIM
      this.oParentObject.w_DTBNOIMPA = _Curs_Totali.DTBNOIMP
      this.oParentObject.w_DTSNOIMPA = _Curs_Totali.DTSNOIMP
      this.oParentObject.w_DTOPESENA = _Curs_Totali.DTOPESEN
      this.oParentObject.w_DTBNOSOGA = _Curs_Totali.DTBNOSOG
      this.oParentObject.w_DTSNOSOGA = _Curs_Totali.DTSNOSOG
      use in _Curs_Totali
    endif
    this.w_GSAI_MDP = this.oParentObject.GSAI_MDP
    select sum(t_DTBNVATT) as DTBNVATT, sum(t_DTBNVAIM) as DTBNVAIM, sum(t_DTSNVATT) as DTSNVATT, sum(t_DTSNVAIM) as DTSNVAIM, sum(t_DTBNVPTT) as DTBNVPTT, sum(t_DTBNVPIM) as DTBNVPIM, sum(t_DTSNVPTT) as DTSNVPTT, sum(t_DTSNVPIM) as DTSNVPIM from (this.w_GSAI_MDP.cTrsName) into cursor _Curs_Totali
    if used("_Curs_Totali")
      this.oParentObject.w_DTBNVATT = _Curs_Totali.DTBNVATT
      this.oParentObject.w_DTBNVAIM = _Curs_Totali.DTBNVAIM
      this.oParentObject.w_DTSNVATT = _Curs_Totali.DTSNVATT
      this.oParentObject.w_DTSNVAIM = _Curs_Totali.DTSNVAIM
      this.oParentObject.w_DTBNVPTT = _Curs_Totali.DTBNVPTT
      this.oParentObject.w_DTBNVPIM = _Curs_Totali.DTBNVPIM
      this.oParentObject.w_DTSNVPTT = _Curs_Totali.DTSNVPTT
      this.oParentObject.w_DTSNVPIM = _Curs_Totali.DTSNVPIM
      use in _Curs_Totali
    endif
    this.w_GSAI_MDP = this.oParentObject.GSAIAMDP
    select sum(t_DTBNVATT) as DTBNVATT, sum(t_DTBNVAIM) as DTBNVAIM, sum(t_DTSNVATT) as DTSNVATT, sum(t_DTSNVAIM) as DTSNVAIM, sum(t_DTBNVPTT) as DTBNVPTT, sum(t_DTBNVPIM) as DTBNVPIM, sum(t_DTSNVPTT) as DTSNVPTT, sum(t_DTSNVPIM) as DTSNVPIM from (this.w_GSAI_MDP.cTrsName) into cursor _Curs_Totali
    if used("_Curs_Totali")
      this.oParentObject.w_DTBNVATTA = _Curs_Totali.DTBNVATT
      this.oParentObject.w_DTBNVAIMA = _Curs_Totali.DTBNVAIM
      this.oParentObject.w_DTSNVATTA = _Curs_Totali.DTSNVATT
      this.oParentObject.w_DTSNVAIMA = _Curs_Totali.DTSNVAIM
      this.oParentObject.w_DTBNVPTTA = _Curs_Totali.DTBNVPTT
      this.oParentObject.w_DTBNVPIMA = _Curs_Totali.DTBNVPIM
      this.oParentObject.w_DTSNVPTTA = _Curs_Totali.DTSNVPTT
      this.oParentObject.w_DTSNVPIMA = _Curs_Totali.DTSNVPIM
      use in _Curs_Totali
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
