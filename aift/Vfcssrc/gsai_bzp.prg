* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bzp                                                        *
*              Aggiornamento riferimento operazioni da rettificare             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-14                                                      *
* Last revis.: 2011-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bzp",oParentObject,m.pParame)
return(i_retval)

define class tgsai_bzp as StdBatch
  * --- Local variables
  pParame = space(1)
  w_PNNUMRER = 0
  w_PNDATREG = ctod("  /  /  ")
  w_PNCOMPET = space(4)
  w_PNSERIAL = space(10)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_DESCLF = space(40)
  w_SERIALEDIFILTRO = space(10)
  w_CONF_ELAB = space(1)
  w_OSSERIAL = space(10)
  w_OSRIFFAT = space(10)
  w_MESS = space(100)
  w_ZOOM = space(10)
  w_CONTA = 0
  w_CONTACONT = 0
  w_NUMREGVAL = 0
  w_OPERSUPE = space(1)
  w_TIPOPE = space(1)
  w_RIFCON = space(30)
  w_TIPFAT = space(1)
  w_FLGEXT = space(1)
  w_FLGDVE = space(1)
  w_OSGENERA = space(1)
  w_OSMODUTE = space(1)
  w_OSUTEINS = 0
  w_OSDATINS = ctod("  /  /  ")
  w_OSUTEVAR = 0
  w_OSDATVAR = ctod("  /  /  ")
  w_oERRORLOG = .NULL.
  w_LOGNUM = 0
  w_SERIALE = space(10)
  w_NFLTATTR = 0
  w_TIPOCONTRATTO = space(1)
  w_TIPOPARAMETRO = space(1)
  w_PARAME = space(1)
  w_DATAREGISTRAZIONE = ctod("  /  /  ")
  w_OSRIFACC = space(10)
  w_MASK = .NULL.
  w_IMPFRA = space(1)
  w_ANNRET = 0
  w_PNCODVAL = space(3)
  w_OSTIPCLF = space(1)
  w_OSCODICE = space(15)
  w_RIFCON_FATT = space(30)
  w_TIPOPE_FATT = space(1)
  w_AZIENDA = space(5)
  w_CODATT = space(5)
  w_ANNO = space(4)
  w_PNOTA = .NULL.
  w_PNSERIAL = space(10)
  w_PNSERIAL1 = space(10)
  w_errormsg = space(100)
  w_OSRIFFAT = space(10)
  w_OSRIFACC = space(10)
  w_NEWOSTIPFAT = space(10)
  w_OLDOSTIPFAT = space(10)
  w_SALVATO = .f.
  * --- WorkFile variables
  OPERSUPE_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento - Eliminazione riferimento operazioni da rettificare
    do case
      case this.pParame="A" or this.pParame="F"
        this.w_PNNUMRER = 0
        this.w_PNCOMPET = space(4)
        this.w_PNDATREG = Ctod("  -  -    ")
        this.w_PNSERIAL = space(10)
        this.w_PNTIPCLF = This.oParentObject.w_PNTIPCLF
        this.w_PNCODCLF = This.oParentObject.w_PNCODCLF
        this.w_PNCODVAL = This.oParentObject.w_PNCODVAL
        this.w_DESCLF = This.oParentObject.w_ANDESCRI
        this.w_SERIALEDIFILTRO = This.oParentObject.w_SERIALE
        this.w_OSSERIAL = This.oParentObject.w_SERIALE
        this.w_PARAME = IIF(this.pParame="A","V","S")
        this.w_DATAREGISTRAZIONE = This.oParentObject.w_DATREG
        this.w_CONF_ELAB = "N"
        do GSCG_KOS with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONF_ELAB="S"
          * --- Aggiornamento tabella "Operazioni superiori a 3000 euro"
          if this.pParame="A"
            * --- Write into OPERSUPE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OPERSUPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OSRIFFAT ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'OPERSUPE','OSRIFFAT');
              +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OPERSUPE','OSUTEVAR');
              +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'OPERSUPE','OSDATVAR');
              +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC("S"),'OPERSUPE','OSMODUTE');
              +",OSGENERA ="+cp_NullLink(cp_ToStrODBC("N"),'OPERSUPE','OSGENERA');
                  +i_ccchkf ;
              +" where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                     )
            else
              update (i_cTable) set;
                  OSRIFFAT = this.w_PNSERIAL;
                  ,OSUTEVAR = i_CODUTE;
                  ,OSDATVAR = i_DATSYS;
                  ,OSMODUTE = "S";
                  ,OSGENERA = "N";
                  &i_ccchkf. ;
               where;
                  OSSERIAL = this.w_OSSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into OPERSUPE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OPERSUPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OSRIFACC ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'OPERSUPE','OSRIFACC');
              +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OPERSUPE','OSUTEVAR');
              +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'OPERSUPE','OSDATVAR');
              +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC("S"),'OPERSUPE','OSMODUTE');
              +",OSGENERA ="+cp_NullLink(cp_ToStrODBC("N"),'OPERSUPE','OSGENERA');
                  +i_ccchkf ;
              +" where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                     )
            else
              update (i_cTable) set;
                  OSRIFACC = this.w_PNSERIAL;
                  ,OSUTEVAR = i_CODUTE;
                  ,OSDATVAR = i_DATSYS;
                  ,OSMODUTE = "S";
                  ,OSGENERA = "N";
                  &i_ccchkf. ;
               where;
                  OSSERIAL = this.w_OSSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          Ah_ErrorMsg("Aggiornamento effettuato con successo")
           
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Interroga",.T.) 
 w_TIMER.Interval = 100
        endif
      case this.pParame="B" or this.pParame="L"
        this.w_OSRIFFAT = IIF(this.pParame="B",This.oParentObject.w_OSRIFFAT,This.oParentObject.w_OSRIFACC)
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNNUMRER,PNCOMPET,PNDATREG"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_OSRIFFAT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNNUMRER,PNCOMPET,PNDATREG;
            from (i_cTable) where;
                PNSERIAL = this.w_OSRIFFAT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PNNUMRER = NVL(cp_ToDate(_read_.PNNUMRER),cp_NullValue(_read_.PNNUMRER))
          this.w_PNCOMPET = NVL(cp_ToDate(_read_.PNCOMPET),cp_NullValue(_read_.PNCOMPET))
          this.w_PNDATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_OSSERIAL = This.oParentObject.w_SERIALE
        if this.pParame="B"
          if Ah_Yesno("Si vuole eliminare il riferimento al movimento da rettificare num.:%1 del %2?", ,ALLTRIM(STR(this.w_Pnnumrer,6,0)),DTOC(this.w_Pndatreg))
            * --- Write into OPERSUPE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OPERSUPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OSRIFFAT ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'OPERSUPE','OSRIFFAT');
              +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OPERSUPE','OSUTEVAR');
              +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'OPERSUPE','OSDATVAR');
              +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC("S"),'OPERSUPE','OSMODUTE');
              +",OSGENERA ="+cp_NullLink(cp_ToStrODBC("N"),'OPERSUPE','OSGENERA');
                  +i_ccchkf ;
              +" where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                     )
            else
              update (i_cTable) set;
                  OSRIFFAT = SPACE(10);
                  ,OSUTEVAR = i_CODUTE;
                  ,OSDATVAR = i_DATSYS;
                  ,OSMODUTE = "S";
                  ,OSGENERA = "N";
                  &i_ccchkf. ;
               where;
                  OSSERIAL = this.w_OSSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
             
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Interroga",.T.) 
 w_TIMER.Interval = 100
          endif
        else
          if Ah_Yesno("Si vuole eliminare il riferimento alla fattura num.:%1 del %2?", ,ALLTRIM(STR(this.w_Pnnumrer,6,0)),DTOC(this.w_Pndatreg))
            * --- Write into OPERSUPE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OPERSUPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OSRIFACC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'OPERSUPE','OSRIFACC');
              +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OPERSUPE','OSUTEVAR');
              +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'OPERSUPE','OSDATVAR');
              +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC("S"),'OPERSUPE','OSMODUTE');
              +",OSGENERA ="+cp_NullLink(cp_ToStrODBC("N"),'OPERSUPE','OSGENERA');
                  +i_ccchkf ;
              +" where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                     )
            else
              update (i_cTable) set;
                  OSRIFACC = SPACE(10);
                  ,OSUTEVAR = i_CODUTE;
                  ,OSDATVAR = i_DATSYS;
                  ,OSMODUTE = "S";
                  ,OSGENERA = "N";
                  &i_ccchkf. ;
               where;
                  OSSERIAL = this.w_OSSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
             
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Interroga",.T.) 
 w_TIMER.Interval = 100
          endif
        endif
      case this.pParame="S"
        this.w_ZOOM = This.oParentObject.w_ZOOMVP.cCursor
        SELECT(this.w_ZOOM)
        COUNT FOR XCHK=1 TO this.w_CONTA
        * --- Verifico se l'utente ha selezionato almeno un record da aggiornare
        if this.w_CONTA = 0
          ah_errormsg("Selezionare almeno un movimento di primanota da aggiornare",48)
          i_retcode = 'stop'
          return
        endif
        * --- Crea la classe delle Messaggistiche di Errore
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        * --- Try
        local bErr_03766A88
        bErr_03766A88=bTrsErr
        this.Try_03766A88()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_errormsg("Impossibile aggiornare le registrazioni contabili")
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_03766A88
        * --- End
        ah_errormsg("Elaborazione terminata con successo")
        if this.w_LOGNUM>0
          * --- Lancia report
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di generazione")     
        endif
        * --- Rimuove la colonna1 (xchk) poich� il panter non ce la fa e va in errore nella ricostruzione dello zoom
         
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Interroga",.T.) 
 w_TIMER.Interval = 100
      case this.pParame="P"
        * --- Prima di lanciare l'esecuzione dello zoom devo effettuare determinati
        *     controlli sugli importi. I controlli devono essere effettuati solo nel caso in cui
        *     l'utente abbia impostato il filtro su "Compreso tra"
        if this.oParentObject.w_FILTIMPO="C"
          do case
            case (this.oParentObject.w_DAIMPO<0 And this.oParentObject.w_AIMPO>0) Or (this.oParentObject.w_DAIMPO>0 And this.oParentObject.w_AIMPO<=0) 
              ah_errormsg("Segno importi non corrispondenti")
              This.oParentobject.oPgFrm.ActivePage=1
              i_retcode = 'stop'
              return
            case (this.oParentObject.w_DAIMPO>0 And Abs(this.oParentObject.w_DAIMPO)>Abs(this.oParentObject.w_AIMPO)) Or (this.oParentObject.w_DAIMPO=0 And this.oParentObject.w_AIMPO<0) Or (this.oParentObject.w_DAIMPO<0 And Abs(this.oParentObject.w_DAIMPO)<Abs(this.oParentObject.w_AIMPO))
              ah_errormsg("Intervallo importi non congruenti, l'importo finale � minore dell'importo iniziale ")
              This.oParentobject.oPgFrm.ActivePage=1
              i_retcode = 'stop'
              return
          endcase
        endif
        * --- Rimuove la colonna1 (xchk) poich� il panter non ce la fa e va in errore nella ricostruzione dello zoom
         
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Interroga",.T.) 
 w_TIMER.Interval = 100
      case this.pParame="R" or this.pParame="E"
        this.w_TIPOPARAMETRO = this.pParame
        this.w_SERIALE = This.oParentObject.w_Seriale
        if this.pParame="R"
          * --- Read from OPERSUPE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OPERSUPE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2],.t.,this.OPERSUPE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OSSERIAL"+;
              " from "+i_cTable+" OPERSUPE where ";
                  +"OSRIFFAT = "+cp_ToStrODBC(this.w_SERIALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OSSERIAL;
              from (i_cTable) where;
                  OSRIFFAT = this.w_SERIALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OSSERIAL = NVL(cp_ToDate(_read_.OSSERIAL),cp_NullValue(_read_.OSSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from OPERSUPE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OPERSUPE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2],.t.,this.OPERSUPE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OSSERIAL"+;
              " from "+i_cTable+" OPERSUPE where ";
                  +"OSRIFACC = "+cp_ToStrODBC(this.w_SERIALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OSSERIAL;
              from (i_cTable) where;
                  OSRIFACC = this.w_SERIALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OSSERIAL = NVL(cp_ToDate(_read_.OSSERIAL),cp_NullValue(_read_.OSSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if i_Rows>1
          this.w_OSSERIAL = space(10)
          vx_exec("STD\GSAI_KOS.VZM",this)
        endif
        if NOT EMPTY(NVL(this.w_OSSERIAL," "))
          * --- Questo oggetto sar� definito come Tesoreria
          * --- definisco l'oggetto come appartenente alla classe della Prima Nota
          this.w_PNOTA = GSCG_MPN()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PNOTA.bSec1)
            i_retcode = 'stop'
            return
          endif
          * --- inizializzo la chiave della Prima Nota
          this.w_PNOTA.w_PNSERIAL = this.w_OSSERIAL
          * --- creo il curosre delle solo chiavi
          this.w_PNOTA.QueryKeySet("PNSERIAL='"+this.w_OSSERIAL+ "'","")     
          * --- mi metto in interrogazione
          this.w_PNOTA.LoadRecWarn()     
        endif
      case this.pParame="X" or this.pParame="D" or this.pParame="I"
        this.w_ZOOM = This.oParentObject.w_ZOOMVP
        Select (this.w_ZOOM.cCursor)
        this.w_NFLTATTR = RECNO()
        UPDATE (this.w_ZOOM.cCursor) SET XCHK=ICASE(this.pParame=="X",1,this.pParame=="D",0,IIF(XCHK=1,0,1))
        Select (this.w_ZOOM.cCursor)
        if this.w_NFLTATTR<RECCOUNT()
          GO this.w_NFLTATTR
        endif
      case this.pParame="V"
        this.w_ZOOM = This.oParentObject.w_ZOOMVP.cCursor
        SELECT(this.w_ZOOM)
        COUNT FOR XCHK=1 TO this.w_CONTA
        * --- Verifico se l'utente ha selezionato almeno un record da aggiornare
        if this.w_CONTA = 0
          ah_errormsg("Selezionare almeno un movimento di primanota da aggiornare",48)
          i_retcode = 'stop'
          return
        endif
        * --- Verifico se sono state selezionate registrazioni con dati relativi
        *     a 'Operazioni superiori a 3000 euro'
        SELECT(this.w_ZOOM)
        COUNT FOR XCHK=1 AND OPERSUPE="S" TO this.w_NUMREGVAL
        if this.w_NUMREGVAL>0
          if Ah_Yesno("Sono state selezionate %1 registrazioni di primanota con dati%0relativi a 'Operazioni superiori a 3000 euro' valorizzati%0I dati presenti saranno sovrascritti%0Confermi l'aggiornamento?", ,ALLTRIM(STR(this.w_Numregval,6,0)))
            * --- Continuo l'elaborazione
          else
            ah_errormsg("Operazione abbandonata",48)
            i_retcode = 'stop'
            return
          endif
        endif
        Select (this.w_ZOOM) 
 Go Top 
 Scan for XCHK = 1
        this.w_OSSERIAL = PNSERIAL
        this.w_TIPOPE = iif(ANOPETRE="E", "N", ANOPETRE)
        this.w_RIFCON = space(30)
        this.w_TIPFAT = ICASE(PNTIPDOC $ "FA/FC/FE", "S", PNTIPDOC $ "NC/NE/NU", "N", PNTIPREG $ "C/E", "C", "S")
        this.w_IMPFRA = IIF(this.w_TIPFAT="S","S","N")
        this.w_OSRIFFAT = space(10)
        this.w_OSRIFACC = space(10)
        this.w_OSTIPCLF = PNTIPCLF
        this.w_OSCODICE = Space(15)
        this.w_ANNRET = IIF(this.w_TIPFAT="N",Year(PNDATREG),0)
        this.w_FLGEXT = iif(ANOPETRE="E", "F", "N")
        this.w_FLGDVE = "N"
        this.w_OSGENERA = "N"
        this.w_OSMODUTE = "S"
        this.w_OSUTEINS = i_CODUTE
        this.w_OSDATINS = i_DATSYS
        this.w_OSUTEVAR = i_CODUTE
        this.w_OSDATVAR = i_DATSYS
        * --- Try
        local bErr_03A94990
        bErr_03A94990=bTrsErr
        this.Try_03A94990()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into OPERSUPE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.OPERSUPE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OSTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'OPERSUPE','OSTIPOPE');
            +",OSRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_RIFCON),'OPERSUPE','OSRIFCON');
            +",OSTIPFAT ="+cp_NullLink(cp_ToStrODBC(this.w_TIPFAT),'OPERSUPE','OSTIPFAT');
            +",OSRIFFAT ="+cp_NullLink(cp_ToStrODBC(this.w_OSRIFFAT),'OPERSUPE','OSRIFFAT');
            +",OSFLGEXT ="+cp_NullLink(cp_ToStrODBC(this.w_FLGEXT),'OPERSUPE','OSFLGEXT');
            +",OSFLGDVE ="+cp_NullLink(cp_ToStrODBC(this.w_FLGDVE),'OPERSUPE','OSFLGDVE');
            +",OSGENERA ="+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
            +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
            +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
            +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
            +",OSRIFACC ="+cp_NullLink(cp_ToStrODBC(this.w_OSRIFACC),'OPERSUPE','OSRIFACC');
            +",OSIMPFRA ="+cp_NullLink(cp_ToStrODBC(this.w_IMPFRA),'OPERSUPE','OSIMPFRA');
            +",OSANNRET ="+cp_NullLink(cp_ToStrODBC(this.w_ANNRET),'OPERSUPE','OSANNRET');
            +",OSTIPCLF ="+cp_NullLink(cp_ToStrODBC(this.w_OSTIPCLF),'OPERSUPE','OSTIPCLF');
            +",OSCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_OSCODICE),'OPERSUPE','OSCODICE');
                +i_ccchkf ;
            +" where ";
                +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                   )
          else
            update (i_cTable) set;
                OSTIPOPE = this.w_TIPOPE;
                ,OSRIFCON = this.w_RIFCON;
                ,OSTIPFAT = this.w_TIPFAT;
                ,OSRIFFAT = this.w_OSRIFFAT;
                ,OSFLGEXT = this.w_FLGEXT;
                ,OSFLGDVE = this.w_FLGDVE;
                ,OSGENERA = this.w_OSGENERA;
                ,OSMODUTE = this.w_OSMODUTE;
                ,OSUTEVAR = this.w_OSUTEVAR;
                ,OSDATVAR = this.w_OSDATVAR;
                ,OSRIFACC = this.w_OSRIFACC;
                ,OSIMPFRA = this.w_IMPFRA;
                ,OSANNRET = this.w_ANNRET;
                ,OSTIPCLF = this.w_OSTIPCLF;
                ,OSCODICE = this.w_OSCODICE;
                &i_ccchkf. ;
             where;
                OSSERIAL = this.w_OSSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03A94990
        * --- End
        Endscan
        ah_errormsg("Elaborazione terminata con successo")
        * --- Rimuove la colonna1 (xchk) poich� il panter non ce la fa e va in errore nella ricostruzione dello zoom
         
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Interroga",.T.) 
 w_TIMER.Interval = 100
      case this.pParame="C"
        this.w_PNSERIAL = This.oParentObject.w_SERIALE
        this.w_PNSERIAL1 = This.oParentObject.w_SERIALE
        this.w_CONF_ELAB = "N"
        this.w_ZOOM = This.oParentObject.w_ZOOMVP.cCursor
        SELECT(this.w_ZOOM)
        Select (this.w_ZOOM)
        * --- Nel caso in cui il tipo fattura � stato impostato a 'Saldo-Acconto-Corrispettivi'
        *     ed � presente il riferimento operazioni da rettificare
        if Not Empty(Nvl(OSRIFFAT,Space(10))) Or Not Empty(Nvl(OSRIFACC,Space(10))) 
          this.w_errormsg = ah_msgformat ("Verificare registrazione contabile n. %1 del %2.", ALLTRIM(STR(PNNUMRER)) , DTOC(PNDATREG))
          if Not Empty(Nvl(OSRIFFAT,Space(10)))
            this.w_errormsg = this.w_errormsg + CHR(13)+CHR(10) + "In seguito all'aggiornamento � stato eliminato il riferimento all'operazione da rettificare."
          else
            this.w_errormsg = this.w_errormsg + CHR(13)+CHR(10) + "In seguito all'aggiornamento � stato eliminato il riferimento alla fattura."
          endif
        endif
        Select (this.w_ZOOM)
        this.w_SALVATO = .T.
        this.w_OSRIFFAT = nvl (OSRIFFAT, SPACE(10))
        this.w_OSRIFACC = nvl (OSRIFACC, SPACE(10))
        this.w_OSSERIAL = PNSERIAL
        * --- Read from OPERSUPE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OPERSUPE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2],.t.,this.OPERSUPE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OSTIPFAT"+;
            " from "+i_cTable+" OPERSUPE where ";
                +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OSTIPFAT;
            from (i_cTable) where;
                OSSERIAL = this.w_OSSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDOSTIPFAT = NVL(cp_ToDate(_read_.OSTIPFAT),cp_NullValue(_read_.OSTIPFAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do GSCG_KO1 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if NOT empty ( this.w_errormsg ) AND this.w_SALVATO
          * --- Read from OPERSUPE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OPERSUPE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2],.t.,this.OPERSUPE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OSTIPFAT"+;
              " from "+i_cTable+" OPERSUPE where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OSTIPFAT;
              from (i_cTable) where;
                  OSSERIAL = this.w_OSSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NEWOSTIPFAT = NVL(cp_ToDate(_read_.OSTIPFAT),cp_NullValue(_read_.OSTIPFAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_NEWOSTIPFAT<> nvl ( this.w_OLDOSTIPFAT ," ")
            ah_errormsg(this.w_errormsg,48)
            * --- Write into OPERSUPE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OPERSUPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OSRIFFAT ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'OPERSUPE','OSRIFFAT');
              +",OSRIFACC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'OPERSUPE','OSRIFACC');
                  +i_ccchkf ;
              +" where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                     )
            else
              update (i_cTable) set;
                  OSRIFFAT = SPACE(10);
                  ,OSRIFACC = SPACE(10);
                  &i_ccchkf. ;
               where;
                  OSSERIAL = this.w_OSSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        if this.w_SALVATO
          ah_errormsg( "Aggiornamento avvenuto con successo",48)
        endif
        * --- Rimuove la colonna1 (xchk) poich� il panter non ce la fa e va in errore nella ricostruzione dello zoom
         
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Interroga",.T.) 
 w_TIMER.Interval = 100
      case this.pParame="G" Or this.pParame="M"
        this.w_TIPOPARAMETRO = this.pParame
        do GSAI_KAF with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParame="H"
        this.w_ZOOM = This.oParentObject.w_ACCONTO.cCursor
        if This.oParentObject.w_TIPOPARAMETRO="G"
          this.w_OSRIFACC = This.oParentObject.w_PNSERIAL
        else
          this.w_OSRIFACC = Space(10)
        endif
        SELECT(this.w_ZOOM)
        COUNT FOR XCHK=1 TO this.w_CONTA
        * --- Verifico se l'utente ha selezionato almeno un record da aggiornare
        if this.w_CONTA = 0
          if This.oParentObject.w_TIPOPARAMETRO="G"
            ah_errormsg("Selezionare almeno un movimento di primanota da associare",48)
          else
            ah_errormsg("Selezionare almeno un movimento di acconto da eliminare",48)
          endif
          i_retcode = 'stop'
          return
        endif
        Select (this.w_ZOOM) 
 Go Top 
 Scan for XCHK = 1
        this.w_OSSERIAL = PNSERIAL
        * --- Write into OPERSUPE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OPERSUPE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OSRIFACC ="+cp_NullLink(cp_ToStrODBC(this.w_OSRIFACC),'OPERSUPE','OSRIFACC');
          +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OPERSUPE','OSUTEVAR');
          +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'OPERSUPE','OSDATVAR');
          +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC("S"),'OPERSUPE','OSMODUTE');
          +",OSGENERA ="+cp_NullLink(cp_ToStrODBC("N"),'OPERSUPE','OSGENERA');
              +i_ccchkf ;
          +" where ";
              +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                 )
        else
          update (i_cTable) set;
              OSRIFACC = this.w_OSRIFACC;
              ,OSUTEVAR = i_CODUTE;
              ,OSDATVAR = i_DATSYS;
              ,OSMODUTE = "S";
              ,OSGENERA = "N";
              &i_ccchkf. ;
           where;
              OSSERIAL = this.w_OSSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        Endscan
        * --- chiudo la maschera di associazione acconti
        this.w_MASK = This.oParentObject.oParentObject.oParentObject
         this.oParentObject.ecpQuit()
        Ah_ErrorMsg("Aggiornamento effettuato con successo")
         
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.w_Mask,"Interroga",.T.) 
 w_TIMER.Interval = 100
      case this.pParame="N"
        this.oParentObject.w_DAIMPO = 0
        this.w_AZIENDA = i_CODAZI
        this.w_ANNO = this.oParentObject.w_ANNO_FILTRO
        * --- Verifico se l'azienda gestisce pi� attivit�
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          if g_Attivi<>"S"
            this.w_CODATT = g_CATAZI
          else
            this.w_CODATT = SPACE(5)
          endif
          * --- Select from Gsai14bde
          do vq_exec with 'Gsai14bde',this,'_Curs_Gsai14bde','',.f.,.t.
          if used('_Curs_Gsai14bde')
            select _Curs_Gsai14bde
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_DAIMPO = _Curs_Gsai14bde.AIMINFAT
            Exit
              select _Curs_Gsai14bde
              continue
            enddo
            use
          endif
        else
          * --- Select from Gsai14bder
          do vq_exec with 'Gsai14bder',this,'_Curs_Gsai14bder','',.f.,.t.
          if used('_Curs_Gsai14bder')
            select _Curs_Gsai14bder
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_DAIMPO = _Curs_Gsai14bder.IAMINFAT
              select _Curs_Gsai14bder
              continue
            enddo
            use
          endif
        endif
      case this.pParame="O"
        this.w_ZOOM = This.oParentObject.w_ZOOMVP.cCursor
        SELECT(this.w_ZOOM)
        COUNT FOR XCHK=1 TO this.w_CONTA
        COUNT FOR XCHK=1 AND NOT EMPTY(OSRIFCON) TO this.w_CONTACONT
        * --- Verifico se l'utente ha selezionato almeno un record da aggiornare
        do case
          case this.w_CONTA=0
            ah_errormsg("Selezionare almeno un movimento di primanota da aggiornare",48)
            i_retcode = 'stop'
            return
          case this.w_CONTACONT>0
            ah_errormsg("Sono stati selezionati movimenti di primanota con riferimento contratto valorizzato",48)
            i_retcode = 'stop'
            return
        endcase
        * --- Crea la classe delle Messaggistiche di Errore
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        * --- Devo verificare se i movimenti di primanota di tipo acconto 
        Select (this.w_ZOOM) 
 Go Top 
 Scan for XCHK = 1
        this.w_OSSERIAL = PNSERIAL
        this.w_RIFCON = OSRIFCON
        this.w_TIPOPE = TIPOPE
        this.w_OSRIFACC = OSRIFACC
        this.w_OSUTEVAR = i_CODUTE
        this.w_OSDATVAR = i_DATSYS
        * --- Tramite Osrifacc risalgo alla fattura per verificare la valorizzazione del
        *     campo Ostipope ( Tipo operazione )
        * --- Read from OPERSUPE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OPERSUPE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2],.t.,this.OPERSUPE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OSRIFCON,OSTIPOPE"+;
            " from "+i_cTable+" OPERSUPE where ";
                +"OSSERIAL = "+cp_ToStrODBC(this.w_OSRIFACC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OSRIFCON,OSTIPOPE;
            from (i_cTable) where;
                OSSERIAL = this.w_OSRIFACC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RIFCON_FATT = NVL(cp_ToDate(_read_.OSRIFCON),cp_NullValue(_read_.OSRIFCON))
          this.w_TIPOPE_FATT = NVL(cp_ToDate(_read_.OSTIPOPE),cp_NullValue(_read_.OSTIPOPE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_TIPOPE $ "PC" AND this.w_TIPOPE_FATT $ "PC" AND (this.w_TIPOPE<>this.w_TIPOPE_FATT)
          this.w_TIPOPE_FATT = IIF(this.w_TIPOPE_FATT="C","Contratti collegati","Corrispettivi periodici")
          this.w_oERRORLOG.AddMsgLog("Verificare registrazione contabile n. %1 del %2",ALLTRIM(STR(PNNUMRER)),DTOC(PNDATREG))     
          this.w_oERRORLOG.AddMsgLog("La tipologia operazione della fattura di acconto � %1 mentre la fattura di saldo ha %2",ALLTRIM(OSTIPOPE),ALLTRIM(this.w_TIPOPE_FATT))     
          this.w_LOGNUM = 1
        else
          if NOT EMPTY(this.w_RIFCON_FATT)
            * --- Se � valorizzato il riferimento contratto sulla fattura associata all'acconto,
            *     effettuo solo l'aggiornamento sul movimento di primanota di tipo acconto
            this.w_TIPOPE = IIF(this.w_TIPOPE="N",this.w_TIPOPE_FATT,this.w_TIPOPE)
            * --- Write into OPERSUPE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OPERSUPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OSTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'OPERSUPE','OSTIPOPE');
              +",OSRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_OSRIFACC),'OPERSUPE','OSRIFCON');
              +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC("S"),'OPERSUPE','OSMODUTE');
              +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
              +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
                  +i_ccchkf ;
              +" where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                     )
            else
              update (i_cTable) set;
                  OSTIPOPE = this.w_TIPOPE;
                  ,OSRIFCON = this.w_OSRIFACC;
                  ,OSMODUTE = "S";
                  ,OSUTEVAR = this.w_OSUTEVAR;
                  ,OSDATVAR = this.w_OSDATVAR;
                  &i_ccchkf. ;
               where;
                  OSSERIAL = this.w_OSSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_oERRORLOG.AddMsgLog("Verificare registrazione contabile n. %1 del %2",ALLTRIM(STR(PNNUMRER)),DTOC(PNDATREG))     
            this.w_oERRORLOG.AddMsgLog("E' stato aggiornato il riferimento contratto della fattura di acconto con %1",this.w_OSRIFACC)     
            this.w_oERRORLOG.AddMsgLog("mentre sulla fattura di saldo collegata il riferimento contratto � rimasto con %1",ALLTRIM(this.w_RIFCON_FATT))     
            this.w_LOGNUM = 1
          else
            * --- In questo caso devo aggiornare sia il movimento di primanota di tipo acconto
            *     che il movimento di primanota di tipo fattura
            this.w_TIPOPE = IIF(this.w_TIPOPE="N",IIF(this.w_TIPOPE_FATT="N","C",this.w_TIPOPE_FATT),this.w_TIPOPE)
            * --- Write into OPERSUPE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OPERSUPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OSTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'OPERSUPE','OSTIPOPE');
              +",OSRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_OSRIFACC),'OPERSUPE','OSRIFCON');
              +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC("S"),'OPERSUPE','OSMODUTE');
              +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
              +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
                  +i_ccchkf ;
              +" where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                     )
            else
              update (i_cTable) set;
                  OSTIPOPE = this.w_TIPOPE;
                  ,OSRIFCON = this.w_OSRIFACC;
                  ,OSMODUTE = "S";
                  ,OSUTEVAR = this.w_OSUTEVAR;
                  ,OSDATVAR = this.w_OSDATVAR;
                  &i_ccchkf. ;
               where;
                  OSSERIAL = this.w_OSSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_TIPOPE_FATT = IIF(this.w_TIPOPE_FATT="N",IIF(this.w_TIPOPE="N","C",this.w_TIPOPE),this.w_TIPOPE_FATT)
            * --- Write into OPERSUPE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OPERSUPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OSTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOPE_FATT),'OPERSUPE','OSTIPOPE');
              +",OSRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_OSRIFACC),'OPERSUPE','OSRIFCON');
              +",OSGENERA ="+cp_NullLink(cp_ToStrODBC("S"),'OPERSUPE','OSGENERA');
                  +i_ccchkf ;
              +" where ";
                  +"OSSERIAL = "+cp_ToStrODBC(this.w_OSRIFACC);
                     )
            else
              update (i_cTable) set;
                  OSTIPOPE = this.w_TIPOPE_FATT;
                  ,OSRIFCON = this.w_OSRIFACC;
                  ,OSGENERA = "S";
                  &i_ccchkf. ;
               where;
                  OSSERIAL = this.w_OSRIFACC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        Endscan
        ah_errormsg("Elaborazione terminata")
        if this.w_LOGNUM>0
          * --- Lancia report
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di generazione")     
        endif
        * --- Rimuove la colonna1 (xchk) poich� il panter non ce la fa e va in errore nella ricostruzione dello zoom
         
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Interroga",.T.) 
 w_TIMER.Interval = 100
    endcase
  endproc
  proc Try_03766A88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    Select (this.w_ZOOM) 
 Go Top 
 Scan for XCHK = 1
    * --- La variabile w_Opersupe mi dice se nella tabella "Operazione superiori a 3000 euro"
    *     � presente almeno un record
    this.w_OSSERIAL = PNSERIAL
    this.w_OPERSUPE = OPERSUPE
    this.w_RIFCON = space(30)
    this.w_TIPOPE = iif(ANOPETRE="E", "N", ANOPETRE)
    this.w_TIPFAT = IIF(this.oParentObject.w_OSTIPFAT="X", IIF(PNTIPDOC $ "FA/FC/FE", "S", IIF(PNTIPDOC $ "NC/NE/NU", "N", IIF(PNTIPREG $ "C/E", "C", "S"))), this.oParentObject.w_OSTIPFAT)
    this.w_OSTIPCLF = PNTIPCLF
    this.w_ANNRET = IIF(this.w_TIPFAT="N",Year(PNDATREG),0)
    this.w_IMPFRA = IIF(this.w_TIPFAT="S","S","N")
    this.w_OSRIFFAT = space(10)
    this.w_OSRIFACC = space(10)
    this.w_FLGEXT = iif(ANOPETRE="E", "F", "N")
    this.w_FLGDVE = "N"
    this.w_OSGENERA = "N"
    this.w_OSMODUTE = "S"
    this.w_OSUTEINS = i_CODUTE
    this.w_OSDATINS = i_DATSYS
    this.w_OSUTEVAR = i_CODUTE
    this.w_OSDATVAR = i_DATSYS
    * --- L'aggiornamento della tabella "Opersupe" deve avvenire solo nel caso
    *     in cui l'utente abbia modificato una delle combo della maschera ad un valore
    *     differente di "Nessun Aggiornamento" oppure che sia stato attivato
    *     il check "Riferimento contratto" ed il valore della variabile w_Tipope
    *     sia uguale a 'Contratti collegati/Corrispettivi periodici'
    if this.w_OPERSUPE="N" And (this.oParentObject.w_OSTIPOPE $ "NCP" Or this.oParentObject.w_OSTIPFAT $ "SANC" Or this.oParentObject.w_OSFLGEXT $ "IFN" Or this.oParentObject.w_OSFLGDVE $ "SN" OR this.oParentObject.w_OSIMPFRA $ "SN" Or (this.oParentObject.w_CHKRIFCON="S" And (this.oParentObject.w_OSTIPOPE$"CP" Or this.w_TIPOPE$"CP") ) )
      * --- Nel caso in cui non esista nessun record nella tabella "Operazione superiori a 3000 euro"
      *     inserisco prima i dati di default ( Gestione Gscg_Aos ) e poi aggiorno successivamente
      *     con i valori impostati nella maschera
      * --- Try
      local bErr_03A74720
      bErr_03A74720=bTrsErr
      this.Try_03A74720()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03A74720
      * --- End
    endif
    * --- Verifico nella maschera di valorizzazione quale check � stato attivato
    * --- Tipo operazione
    if this.oParentObject.w_OSTIPOPE $ " NCP "
      * --- Write into OPERSUPE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OPERSUPE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OSTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OSTIPOPE),'OPERSUPE','OSTIPOPE');
        +",OSGENERA ="+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
        +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
        +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
        +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
            +i_ccchkf ;
        +" where ";
            +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
               )
      else
        update (i_cTable) set;
            OSTIPOPE = this.oParentObject.w_OSTIPOPE;
            ,OSGENERA = this.w_OSGENERA;
            ,OSMODUTE = this.w_OSMODUTE;
            ,OSUTEVAR = this.w_OSUTEVAR;
            ,OSDATVAR = this.w_OSDATVAR;
            &i_ccchkf. ;
         where;
            OSSERIAL = this.w_OSSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Effettuo una lettura sul database per avere il valore corretto
    *     del campo Ostipope. Potrebbe succedere che l'utente modifichi il valore del
    *     campo quando � attiva la maschera di valorizzazione
    * --- Read from OPERSUPE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OPERSUPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2],.t.,this.OPERSUPE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OSTIPOPE"+;
        " from "+i_cTable+" OPERSUPE where ";
            +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OSTIPOPE;
        from (i_cTable) where;
            OSSERIAL = this.w_OSSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPOPE = NVL(cp_ToDate(_read_.OSTIPOPE),cp_NullValue(_read_.OSTIPOPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Riferimento contratto
    if this.oParentObject.w_CHKRIFCON="S"
      * --- Prima di effettuare l'aggiornamento del riferimento contratto devo verificare
      *     se il tipo operazione � impostato a "Contratti collegati/Corrispettivi periodici"
      if this.oParentObject.w_OSTIPOPE$"CP" Or this.w_TIPOPE$"CP"
        * --- Write into OPERSUPE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OPERSUPE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OSRIFCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OSRIFCON),'OPERSUPE','OSRIFCON');
          +",OSGENERA ="+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
          +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
          +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
          +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
              +i_ccchkf ;
          +" where ";
              +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                 )
        else
          update (i_cTable) set;
              OSRIFCON = this.oParentObject.w_OSRIFCON;
              ,OSGENERA = this.w_OSGENERA;
              ,OSMODUTE = this.w_OSMODUTE;
              ,OSUTEVAR = this.w_OSUTEVAR;
              ,OSDATVAR = this.w_OSDATVAR;
              &i_ccchkf. ;
           where;
              OSSERIAL = this.w_OSSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        this.w_oERRORLOG.AddMsgLog("Verificare registrazione contabile n. %1 del %2",ALLTRIM(STR(PNNUMRER)),DTOC(PNDATREG))     
        this.w_oERRORLOG.AddMsgLog("Non aggiornato riferimento contratto in quanto tipologia operazione diversa da contratti collegati o corrispettivi periodici")     
        this.w_LOGNUM = 1
      endif
    endif
    * --- Tipo fattura
    if this.oParentObject.w_OSTIPFAT $ "SANC"
      * --- Nel caso in cui il tipo fattura � stato impostato a 'Saldo-Acconto-Corrispettivi'
      *     ed � presente il riferimento operazioni da rettificare, sbianco il campo
      *     avvertendo l'utente
      if ( this.oParentObject.w_OSTIPFAT $ "SAC" And Not Empty(Nvl(OSRIFFAT,Space(10))) ) Or ( this.oParentObject.w_OSTIPFAT $ "SNC" And Not Empty(Nvl(OSRIFACC,Space(10))) )
        * --- Write into OPERSUPE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OPERSUPE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OSTIPFAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OSTIPFAT),'OPERSUPE','OSTIPFAT');
          +",OSGENERA ="+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
          +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
          +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
          +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
          +",OSRIFFAT ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'OPERSUPE','OSRIFFAT');
          +",OSRIFACC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'OPERSUPE','OSRIFACC');
          +",OSANNRET ="+cp_NullLink(cp_ToStrODBC(this.w_ANNRET),'OPERSUPE','OSANNRET');
              +i_ccchkf ;
          +" where ";
              +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                 )
        else
          update (i_cTable) set;
              OSTIPFAT = this.oParentObject.w_OSTIPFAT;
              ,OSGENERA = this.w_OSGENERA;
              ,OSMODUTE = this.w_OSMODUTE;
              ,OSUTEVAR = this.w_OSUTEVAR;
              ,OSDATVAR = this.w_OSDATVAR;
              ,OSRIFFAT = SPACE(10);
              ,OSRIFACC = SPACE(10);
              ,OSANNRET = this.w_ANNRET;
              &i_ccchkf. ;
           where;
              OSSERIAL = this.w_OSSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_oERRORLOG.AddMsgLog(ah_msgformat ("Verificare registrazione contabile n. %1 del %2.", ALLTRIM(STR(PNNUMRER)) , DTOC(PNDATREG)))     
        if Not Empty(Nvl(OSRIFFAT,Space(10)))
          this.w_oERRORLOG.AddMsgLog("In seguito all'aggiornamento � stato eliminato il riferimento all'operazione da rettificare.")     
        else
          this.w_oERRORLOG.AddMsgLog("In seguito all'aggiornamento � stato eliminato il riferimento alla fattura.")     
        endif
        this.w_LOGNUM = 1
      else
        * --- Write into OPERSUPE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OPERSUPE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OSTIPFAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OSTIPFAT),'OPERSUPE','OSTIPFAT');
          +",OSGENERA ="+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
          +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
          +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
          +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
          +",OSANNRET ="+cp_NullLink(cp_ToStrODBC(this.w_ANNRET),'OPERSUPE','OSANNRET');
              +i_ccchkf ;
          +" where ";
              +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
                 )
        else
          update (i_cTable) set;
              OSTIPFAT = this.oParentObject.w_OSTIPFAT;
              ,OSGENERA = this.w_OSGENERA;
              ,OSMODUTE = this.w_OSMODUTE;
              ,OSUTEVAR = this.w_OSUTEVAR;
              ,OSDATVAR = this.w_OSDATVAR;
              ,OSANNRET = this.w_ANNRET;
              &i_ccchkf. ;
           where;
              OSSERIAL = this.w_OSSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    * --- Tipo estrazione
    if this.oParentObject.w_OSFLGEXT $ "IFN"
      * --- Write into OPERSUPE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OPERSUPE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OSFLGEXT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OSFLGEXT),'OPERSUPE','OSFLGEXT');
        +",OSGENERA ="+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
        +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
        +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
        +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
            +i_ccchkf ;
        +" where ";
            +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
               )
      else
        update (i_cTable) set;
            OSFLGEXT = this.oParentObject.w_OSFLGEXT;
            ,OSGENERA = this.w_OSGENERA;
            ,OSMODUTE = this.w_OSMODUTE;
            ,OSUTEVAR = this.w_OSUTEVAR;
            ,OSDATVAR = this.w_OSDATVAR;
            &i_ccchkf. ;
         where;
            OSSERIAL = this.w_OSSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Dati da verificare
    if this.oParentObject.w_OSFLGDVE $ "SN"
      * --- Write into OPERSUPE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OPERSUPE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OSFLGDVE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OSFLGDVE),'OPERSUPE','OSFLGDVE');
        +",OSGENERA ="+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
        +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
        +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
        +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
            +i_ccchkf ;
        +" where ";
            +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
               )
      else
        update (i_cTable) set;
            OSFLGDVE = this.oParentObject.w_OSFLGDVE;
            ,OSGENERA = this.w_OSGENERA;
            ,OSMODUTE = this.w_OSMODUTE;
            ,OSUTEVAR = this.w_OSUTEVAR;
            ,OSDATVAR = this.w_OSDATVAR;
            &i_ccchkf. ;
         where;
            OSSERIAL = this.w_OSSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_OSIMPFRA $ "SN"
      * --- Write into OPERSUPE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OPERSUPE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OPERSUPE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OSIMPFRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OSIMPFRA),'OPERSUPE','OSIMPFRA');
        +",OSGENERA ="+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
        +",OSMODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
        +",OSUTEVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
        +",OSDATVAR ="+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
            +i_ccchkf ;
        +" where ";
            +"OSSERIAL = "+cp_ToStrODBC(this.w_OSSERIAL);
               )
      else
        update (i_cTable) set;
            OSIMPFRA = this.oParentObject.w_OSIMPFRA;
            ,OSGENERA = this.w_OSGENERA;
            ,OSMODUTE = this.w_OSMODUTE;
            ,OSUTEVAR = this.w_OSUTEVAR;
            ,OSDATVAR = this.w_OSDATVAR;
            &i_ccchkf. ;
         where;
            OSSERIAL = this.w_OSSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    Endscan
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03A94990()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into OPERSUPE
    i_nConn=i_TableProp[this.OPERSUPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OPERSUPE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OSSERIAL"+",OSTIPOPE"+",OSRIFCON"+",OSTIPFAT"+",OSRIFFAT"+",OSFLGEXT"+",OSFLGDVE"+",OSGENERA"+",OSMODUTE"+",OSUTEINS"+",OSDATINS"+",OSUTEVAR"+",OSDATVAR"+",OSRIFACC"+",OSIMPFRA"+",OSANNRET"+",OSTIPCLF"+",OSCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OSSERIAL),'OPERSUPE','OSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'OPERSUPE','OSTIPOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFCON),'OPERSUPE','OSRIFCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPFAT),'OPERSUPE','OSTIPFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSRIFFAT),'OPERSUPE','OSRIFFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLGEXT),'OPERSUPE','OSFLGEXT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLGDVE),'OPERSUPE','OSFLGDVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSUTEINS),'OPERSUPE','OSUTEINS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSDATINS),'OPERSUPE','OSDATINS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSRIFACC),'OPERSUPE','OSRIFACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFRA),'OPERSUPE','OSIMPFRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANNRET),'OPERSUPE','OSANNRET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSTIPCLF),'OPERSUPE','OSTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSCODICE),'OPERSUPE','OSCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OSSERIAL',this.w_OSSERIAL,'OSTIPOPE',this.w_TIPOPE,'OSRIFCON',this.w_RIFCON,'OSTIPFAT',this.w_TIPFAT,'OSRIFFAT',this.w_OSRIFFAT,'OSFLGEXT',this.w_FLGEXT,'OSFLGDVE',this.w_FLGDVE,'OSGENERA',this.w_OSGENERA,'OSMODUTE',this.w_OSMODUTE,'OSUTEINS',this.w_OSUTEINS,'OSDATINS',this.w_OSDATINS,'OSUTEVAR',this.w_OSUTEVAR)
      insert into (i_cTable) (OSSERIAL,OSTIPOPE,OSRIFCON,OSTIPFAT,OSRIFFAT,OSFLGEXT,OSFLGDVE,OSGENERA,OSMODUTE,OSUTEINS,OSDATINS,OSUTEVAR,OSDATVAR,OSRIFACC,OSIMPFRA,OSANNRET,OSTIPCLF,OSCODICE &i_ccchkf. );
         values (;
           this.w_OSSERIAL;
           ,this.w_TIPOPE;
           ,this.w_RIFCON;
           ,this.w_TIPFAT;
           ,this.w_OSRIFFAT;
           ,this.w_FLGEXT;
           ,this.w_FLGDVE;
           ,this.w_OSGENERA;
           ,this.w_OSMODUTE;
           ,this.w_OSUTEINS;
           ,this.w_OSDATINS;
           ,this.w_OSUTEVAR;
           ,this.w_OSDATVAR;
           ,this.w_OSRIFACC;
           ,this.w_IMPFRA;
           ,this.w_ANNRET;
           ,this.w_OSTIPCLF;
           ,this.w_OSCODICE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A74720()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into OPERSUPE
    i_nConn=i_TableProp[this.OPERSUPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OPERSUPE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OPERSUPE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OSSERIAL"+",OSTIPOPE"+",OSRIFCON"+",OSTIPFAT"+",OSRIFFAT"+",OSFLGEXT"+",OSFLGDVE"+",OSGENERA"+",OSMODUTE"+",OSUTEINS"+",OSDATINS"+",OSUTEVAR"+",OSDATVAR"+",OSRIFACC"+",OSIMPFRA"+",OSANNRET"+",OSTIPCLF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OSSERIAL),'OPERSUPE','OSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'OPERSUPE','OSTIPOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFCON),'OPERSUPE','OSRIFCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPFAT),'OPERSUPE','OSTIPFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSRIFFAT),'OPERSUPE','OSRIFFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLGEXT),'OPERSUPE','OSFLGEXT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLGDVE),'OPERSUPE','OSFLGDVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSGENERA),'OPERSUPE','OSGENERA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSMODUTE),'OPERSUPE','OSMODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSUTEINS),'OPERSUPE','OSUTEINS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSDATINS),'OPERSUPE','OSDATINS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSUTEVAR),'OPERSUPE','OSUTEVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSDATVAR),'OPERSUPE','OSDATVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSRIFACC),'OPERSUPE','OSRIFACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFRA),'OPERSUPE','OSIMPFRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANNRET),'OPERSUPE','OSANNRET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSTIPCLF),'OPERSUPE','OSTIPCLF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OSSERIAL',this.w_OSSERIAL,'OSTIPOPE',this.w_TIPOPE,'OSRIFCON',this.w_RIFCON,'OSTIPFAT',this.w_TIPFAT,'OSRIFFAT',this.w_OSRIFFAT,'OSFLGEXT',this.w_FLGEXT,'OSFLGDVE',this.w_FLGDVE,'OSGENERA',this.w_OSGENERA,'OSMODUTE',this.w_OSMODUTE,'OSUTEINS',this.w_OSUTEINS,'OSDATINS',this.w_OSDATINS,'OSUTEVAR',this.w_OSUTEVAR)
      insert into (i_cTable) (OSSERIAL,OSTIPOPE,OSRIFCON,OSTIPFAT,OSRIFFAT,OSFLGEXT,OSFLGDVE,OSGENERA,OSMODUTE,OSUTEINS,OSDATINS,OSUTEVAR,OSDATVAR,OSRIFACC,OSIMPFRA,OSANNRET,OSTIPCLF &i_ccchkf. );
         values (;
           this.w_OSSERIAL;
           ,this.w_TIPOPE;
           ,this.w_RIFCON;
           ,this.w_TIPFAT;
           ,this.w_OSRIFFAT;
           ,this.w_FLGEXT;
           ,this.w_FLGDVE;
           ,this.w_OSGENERA;
           ,this.w_OSMODUTE;
           ,this.w_OSUTEINS;
           ,this.w_OSDATINS;
           ,this.w_OSUTEVAR;
           ,this.w_OSDATVAR;
           ,this.w_OSRIFACC;
           ,this.w_IMPFRA;
           ,this.w_ANNRET;
           ,this.w_OSTIPCLF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OPERSUPE'
    this.cWorkTables[2]='PNT_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_Gsai14bde')
      use in _Curs_Gsai14bde
    endif
    if used('_Curs_Gsai14bder')
      use in _Curs_Gsai14bder
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
