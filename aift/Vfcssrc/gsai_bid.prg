* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bid                                                        *
*              Importazione dati estratti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-01-04                                                      *
* Last revis.: 2012-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bid",oParentObject)
return(i_retval)

define class tgsai_bid as StdBatch
  * --- Local variables
  w_NomeCurs = space(10)
  w_TipoCampo = space(1)
  w_NomeCampo = space(15)
  w_ValoreCampo = .f.
  w_FormatoCampo = space(1)
  w_NumInt = 0
  w_NumDec = 0
  w_Deserial = space(10)
  w_CondRiga = space(0)
  w_ErrGen = .f.
  w_De__Anno = 0
  w_Detipcon = space(1)
  w_Decodcon = space(15)
  w_Detipsog = space(1)
  w_Dedescri = space(60)
  w_Desclgen = space(1)
  w_Dedacont = space(1)
  w_Decognom = space(40)
  w_De__Nome = space(40)
  w_Dedatnas = ctod("  /  /  ")
  w_Decomest = space(40)
  w_Depronas = space(2)
  w_Destadom = space(3)
  w_Destaest = space(3)
  w_Decitest = space(40)
  w_Deindest = space(40)
  w_Depariva = space(25)
  w_Decodfis = space(25)
  w_Dedatope = ctod("  /  /  ")
  w_Demodpag = space(1)
  w_Deimpdoc = 0
  w_Deimpost = 0
  w_Deesclud = space(1)
  w_De__Area = space(1)
  w_Demodpag = space(1)
  w_Denumfat = space(15)
  w_Deoperaz = space(1)
  w_Dedatfat = ctod("  /  /  ")
  w_Denumret = space(15)
  w_Devarimp = space(1)
  w_Devarims = space(1)
  w_Sogestpr = space(1)
  w_Cod_Rottura = space(250)
  w_ChiaveRecord = space(250)
  w_Cprownum = 0
  w_Elenco_Campi = space(250)
  w_Num_Rec = 0
  w_ErrNomCampo = space(254)
  w_IndiceCampo = 0
  w_NomeTabella = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  ANTIVADE_idx=0
  ANTIVDDE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importazioni dati estratti
    this.oParentObject.w_MSG = ""
    this.w_NomeCurs = Sys(2015)
    this.w_Elenco_Campi = "DE__ANNO-DETIPCON-DECODCON-DETIPSOG-SOGESTPR-DECODFIS-DEPARIVA-DEDESCRI-DECITEST-DESTAEST-DEINDEST-"
    this.w_Elenco_Campi = this.w_Elenco_Campi+"DECOGNOM-DE__NOME-DEDATNAS-DECOMEST-DEPRONAS-DESTADOM-DE__AREA-DEDATOPE-DEMODPAG-"
    this.w_Elenco_Campi = this.w_Elenco_Campi+"DEIMPDOC-DEESCLUD-DENUMFAT-DEIMPOST-DEOPERAZ-DEDATFAT-DENUMRET-DEVARIMP-DEVARIMS"
    this.w_ErrNomCampo = ""
    this.w_ErrGen = .F.
    * --- Lancio la routine Gsar_Bfx per poter creare un cursore dal foglio di excel
    if Gsar_Bfx(This,this.w_NomeCurs,this.oParentObject.w_NomeFile)>-1
      AField(DatiEstratti,this.w_NomeCurs)
      Alines(ControlloCampi,this.w_Elenco_Campi,4,"-")
      this.w_Num_Rec = 1
      do while this.w_Num_Rec<=Alen(ControlloCampi,1)
        if Ascan(DatiEstratti,ControlloCampi(this.w_Num_Rec),1,Alen(DatiEstratti,1),1,15)<1
          this.w_ErrGen = .t.
          this.w_ErrNomCampo = this.w_ErrNomCampo+", "+Alltrim(ControlloCampi(this.w_Num_Rec))
        endif
        this.w_Num_Rec = this.w_Num_Rec+1
      enddo
      if this.w_ErrGen
        Ah_ErrorMsg("Nel file selezionato mancano i seguenti campi: %1",,,SubStr(this.w_ErrNomCampo,3,Len(this.w_ErrNomCampo)-2))
      else
        if type("i_dcx")="U"
          * --- Lettura dizionario dati da XDC
          cp_ReadXDC()
        endif
        * --- Try
        local bErr_0382AA50
        bErr_0382AA50=bTrsErr
        this.Try_0382AA50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_0382AA50
        * --- End
      endif
      use in select (this.w_NomeCurs)
    else
      Ah_ErrorMsg("Errore nell'importazione dati estratti")
    endif
  endproc
  proc Try_0382AA50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    Select (this.w_NomeCurs)
    Go Top
    this.w_Cod_Rottura = " "
    Scan
    this.w_Num_Rec = 1
    this.w_CondRiga = " "
    do while this.w_Num_Rec<=Alen(ControlloCampi,1)
      this.w_NomeCampo = ControlloCampi(this.w_Num_Rec)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_Num_Rec = this.w_Num_Rec+1
    enddo
    this.w_Desclgen = "N"
    this.w_Dedacont = "N"
    this.w_Detipcon = evl(this.w_Detipcon,"C")
    this.w_Deesclud = evl(this.w_Deesclud,"N")
    do case
      case Empty(this.w_CondRiga)
        * --- Non importo la riga processata perch� nessun campo � valorizzato
      case Empty(this.w_Detipsog) or ! this.w_Detipsog $ "IPN"
        ADDMSG("Escluso record %1 tipo soggetto vuoto o incongruente%0",this,Alltrim(Str(Recno()+1)))
      case Empty(this.w_De__Area) or ! this.w_De__Area $ "124"
        ADDMSG("Escluso record %1 tipo record vuoto o incongruente%0",this,Alltrim(Str(Recno()+1)))
      case this.w_Detipsog="N" And ( Empty(this.w_Sogestpr) Or ! this.w_Sogestpr $ "NPS")
        ADDMSG("Escluso record %1 personalit� tipo soggetto vuota o incongruente%0",this,Alltrim(Str(Recno()+1)))
      case Empty(this.w_De__Anno) Or this.w_De__Anno<=2005 Or this.w_De__Anno>=2100
        ADDMSG("Escluso record %1 anno di riferimento vuoto o incongruente%0",this,Alltrim(Str(Recno()+1)))
      case Empty(this.w_Dedatope)
        ADDMSG("Escluso record %1 data operazione vuota o incongruente%0",this,Alltrim(Str(Recno()+1)))
      otherwise
        if Not Empty(this.w_Decodcon)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_DETIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_DECODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  ANTIPCON = this.w_DETIPCON;
                  and ANCODICE = this.w_DECODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
            if i_Rows=0
              Select (this.w_NomeCurs)
              ADDMSG("Verificare record %1 codice conto: %2 inesistente %0",this,Alltrim(Str(Recno()+1)),Alltrim(this.w_Decodcon))
              this.w_Decodcon = Space(15)
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_ErrGen = this.w_Detipsog="I" And Empty(this.w_Depariva)
        this.w_ErrGen = this.w_ErrGen Or (this.w_Detipsog="P" And Empty(this.w_Decodfis))
        this.w_ErrGen = this.w_ErrGen Or (this.w_Detipsog="N" And this.w_Sogestpr="N" And ( Empty(this.w_Dedescri) Or Empty(this.w_Destaest)))
        this.w_ErrGen = this.w_ErrGen Or (this.w_Detipsog="N" And this.w_Sogestpr="P" And (Empty(this.w_Decognom) Or Empty(this.w_De__Nome) Or Empty(this.w_Dedatnas) Or Empty(this.w_Decomest) Or Empty(this.w_Depronas) Or Empty(this.w_Destadom)))
        this.w_ErrGen = this.w_ErrGen Or (this.w_Detipsog="N" And this.w_Sogestpr="S" And (Empty(this.w_Decognom) Or Empty(this.w_De__Nome) Or Empty(this.w_Dedatnas) Or Empty(this.w_Decomest) Or Empty(this.w_Depronas) Or Empty(this.w_Destadom) Or Empty(this.w_Dedescri) Or Empty(this.w_Destaest)))
        if this.w_Errgen
          this.w_Desclgen = "S"
          this.w_Dedacont = "S"
          ADDMSG("Verificare record %1 dati anagrafici incompleti%0",this,Alltrim(Str(Recno()+1)))
        endif
        * --- Controllo la valorizzazione della variabile w_Sogestpr per sbiancare i campi
        *     non editabili nell'anagrafica
        do case
          case this.w_Sogestpr="N" And this.w_Detipsog="N"
            this.w_Decognom = Space(40)
            this.w_De__Nome = Space(40)
            this.w_Dedatnas = Cp_CharToDate("  -  -    ")
            this.w_Decomest = Space(40)
            this.w_Depronas = Space(2)
            this.w_Destadom = Space(3)
          case this.w_Sogestpr="P" And this.w_Detipsog="N"
            this.w_Dedescri = Space(60)
            this.w_Destaest = Space(3)
        endcase
        do case
          case this.w_Detipsog="I"
            this.w_ChiaveRecord = Alltrim(Str(this.w_De__Anno,4,0))+this.w_Detipsog+this.w_Detipcon+this.w_Decodcon+this.w_Depariva
          case this.w_Detipsog="P"
            this.w_ChiaveRecord = Alltrim(Str(this.w_De__Anno,4,0))+this.w_Detipsog+this.w_Detipcon+this.w_Decodcon+this.w_Decodfis
          case this.w_Detipsog="N" And this.w_Sogestpr="N"
            this.w_ChiaveRecord = Alltrim(Str(this.w_De__Anno,4,0))+this.w_Detipsog+this.w_Detipcon+this.w_Decodcon+this.w_Dedescri+this.w_Destaest
          case this.w_Detipsog="N" And this.w_Sogestpr="P"
            this.w_ChiaveRecord = Alltrim(Str(this.w_De__Anno,4,0))+this.w_Detipsog+this.w_Detipcon+this.w_Decodcon+this.w_Decognom+this.w_De__Nome+Dtoc(this.w_Dedatnas)+this.w_Decomest+this.w_Depronas+this.w_Destadom
          case this.w_Detipsog="N" And this.w_Sogestpr="S"
            this.w_ChiaveRecord = Alltrim(Str(this.w_De__Anno,4,0))+this.w_Detipsog+this.w_Detipcon+this.w_Decodcon+this.w_Decognom+this.w_De__Nome+Dtoc(this.w_Dedatnas)+this.w_Decomest+this.w_Depronas+this.w_Destadom+this.w_Dedescri+this.w_Destaest
        endcase
        * --- Non � possibile confrontare la chiave di rottura con <> perch�
        *     se la prima parte della chiave � identica, ma la seconda (Es. P.Iva)
        *     � differetnte il test risulta falso e il record viene importato erroneamente
        *     con il record precedente.
        if ! this.w_Cod_Rottura==this.w_ChiaveRecord
          this.w_Cprownum = 1
          this.w_DESERIAL = SPACE(10)
           
 i_Conn=i_TableProp[this.ANTIVADE_IDX, 3] 
 cp_NextTableProg(this, i_Conn, "ANTIE", "i_codazi,w_DESERIAL")
          * --- Insert into ANTIVADE
          i_nConn=i_TableProp[this.ANTIVADE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANTIVADE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANTIVADE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DESERIAL"+",DE__ANNO"+",DETIPCON"+",DECODCON"+",DETIPSOG"+",DECODFIS"+",DEPARIVA"+",DESCLGEN"+",DEDESCRI"+",DECITEST"+",DESTAEST"+",DEINDEST"+",DECOGNOM"+",DE__NOME"+",DEDATNAS"+",DECOMEST"+",DEPRONAS"+",DESTADOM"+",DECODGEN"+",DEDACONT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_DESERIAL),'ANTIVADE','DESERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DE__ANNO),'ANTIVADE','DE__ANNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DETIPCON),'ANTIVADE','DETIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DECODCON),'ANTIVADE','DECODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DETIPSOG),'ANTIVADE','DETIPSOG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DECODFIS),'ANTIVADE','DECODFIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEPARIVA),'ANTIVADE','DEPARIVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESCLGEN),'ANTIVADE','DESCLGEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEDESCRI),'ANTIVADE','DEDESCRI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DECITEST),'ANTIVADE','DECITEST');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESTAEST),'ANTIVADE','DESTAEST');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEINDEST),'ANTIVADE','DEINDEST');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DECOGNOM),'ANTIVADE','DECOGNOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DE__NOME),'ANTIVADE','DE__NOME');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEDATNAS),'ANTIVADE','DEDATNAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DECOMEST),'ANTIVADE','DECOMEST');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEPRONAS),'ANTIVADE','DEPRONAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESTADOM),'ANTIVADE','DESTADOM');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'ANTIVADE','DECODGEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEDACONT),'ANTIVADE','DEDACONT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL,'DE__ANNO',this.w_DE__ANNO,'DETIPCON',this.w_DETIPCON,'DECODCON',this.w_DECODCON,'DETIPSOG',this.w_DETIPSOG,'DECODFIS',this.w_DECODFIS,'DEPARIVA',this.w_DEPARIVA,'DESCLGEN',this.w_DESCLGEN,'DEDESCRI',this.w_DEDESCRI,'DECITEST',this.w_DECITEST,'DESTAEST',this.w_DESTAEST,'DEINDEST',this.w_DEINDEST)
            insert into (i_cTable) (DESERIAL,DE__ANNO,DETIPCON,DECODCON,DETIPSOG,DECODFIS,DEPARIVA,DESCLGEN,DEDESCRI,DECITEST,DESTAEST,DEINDEST,DECOGNOM,DE__NOME,DEDATNAS,DECOMEST,DEPRONAS,DESTADOM,DECODGEN,DEDACONT &i_ccchkf. );
               values (;
                 this.w_DESERIAL;
                 ,this.w_DE__ANNO;
                 ,this.w_DETIPCON;
                 ,this.w_DECODCON;
                 ,this.w_DETIPSOG;
                 ,this.w_DECODFIS;
                 ,this.w_DEPARIVA;
                 ,this.w_DESCLGEN;
                 ,this.w_DEDESCRI;
                 ,this.w_DECITEST;
                 ,this.w_DESTAEST;
                 ,this.w_DEINDEST;
                 ,this.w_DECOGNOM;
                 ,this.w_DE__NOME;
                 ,this.w_DEDATNAS;
                 ,this.w_DECOMEST;
                 ,this.w_DEPRONAS;
                 ,this.w_DESTADOM;
                 ,SPACE(10);
                 ,this.w_DEDACONT;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_Cod_Rottura = this.w_ChiaveRecord
        endif
        if this.w_De__Area $ "12" And ( Empty(this.w_Demodpag) Or ! this.w_Demodpag $ "123")
          this.w_Demodpag = "1"
          this.w_Deesclud = "S"
          ADDMSG("Verificare record %1 modalit� di pagamento vuota o incongruente%0",this,Alltrim(Str(Recno()+1)))
        endif
        if this.w_De__Area="2" And ( Empty(this.w_Deoperaz) Or ! this.w_Deoperaz $ "12")
          this.w_Deoperaz = "1"
          this.w_Deesclud = "S"
          ADDMSG("Verificare record %1 tipo operazione vuota o incongruente%0",this,Alltrim(Str(Recno()+1)))
        endif
        if this.w_De__Area="4" And ( Empty(this.w_Devarimp) Or ! this.w_Devarimp $ "12")
          this.w_Devarimp = "1"
          this.w_Deesclud = "S"
          ADDMSG("Verificare record %1 variazione imponibile vuota o incongruente%0",this,Alltrim(Str(Recno()+1)))
        endif
        if this.w_De__Area="4" And ( Empty(this.w_Devarims) Or ! this.w_Devarims $ "12")
          this.w_Devarims = "1"
          this.w_Deesclud = "S"
          ADDMSG("Verificare record %1 variazione imposta vuota o incongruente%0",this,Alltrim(Str(Recno()+1)))
        endif
        this.w_Denumfat = iif(this.w_De__Area $ "24",this.w_Denumfat,Space(15))
        this.w_Deimpost = iif(this.w_De__Area $ "24",this.w_Deimpost,0)
        this.w_Dedatfat = iif(this.w_De__Area="4",this.w_Dedatfat,Cp_CharToDate("  -  -    "))
        this.w_Denumret = iif(this.w_De__Area="4",this.w_Denumret,Space(15))
        this.w_Devarimp = iif(this.w_De__Area="4",this.w_Devarimp,Space(1))
        this.w_Devarims = iif(this.w_De__Area="4",this.w_Devarims,Space(1))
        ADDMSG("Importato record %1 %0",this,Alltrim(Str(Recno()+1)))
        * --- Insert into ANTIVDDE
        i_nConn=i_TableProp[this.ANTIVDDE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVDDE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANTIVDDE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DESERIAL"+",DE__AREA"+",CPROWNUM"+",DEDATOPE"+",DERIFPNT"+",DEMODPAG"+",DEIMPDOC"+",DEESCLUD"+",DETIPOPE"+",DERIFCON"+",DETIPFAT"+",DEFLGEXT"+",DENUMFAT"+",DEIMPOST"+",DEOPERAZ"+",DEDATFAT"+",DENUMRET"+",DEVARIMP"+",DEVARIMS"+",DECOMSUP"+",DETIPREG"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_DESERIAL),'ANTIVDDE','DESERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DE__AREA),'ANTIVDDE','DE__AREA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ANTIVDDE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEDATOPE),'ANTIVDDE','DEDATOPE');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'ANTIVDDE','DERIFPNT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEMODPAG),'ANTIVDDE','DEMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPDOC),'ANTIVDDE','DEIMPDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEESCLUD),'ANTIVDDE','DEESCLUD');
          +","+cp_NullLink(cp_ToStrODBC("N"),'ANTIVDDE','DETIPOPE');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(30)),'ANTIVDDE','DERIFCON');
          +","+cp_NullLink(cp_ToStrODBC("S"),'ANTIVDDE','DETIPFAT');
          +","+cp_NullLink(cp_ToStrODBC("N"),'ANTIVDDE','DEFLGEXT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DENUMFAT),'ANTIVDDE','DENUMFAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPOST),'ANTIVDDE','DEIMPOST');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEOPERAZ),'ANTIVDDE','DEOPERAZ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEDATFAT),'ANTIVDDE','DEDATFAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DENUMRET),'ANTIVDDE','DENUMRET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEVARIMP),'ANTIVDDE','DEVARIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEVARIMS),'ANTIVDDE','DEVARIMS');
          +","+cp_NullLink(cp_ToStrODBC("N"),'ANTIVDDE','DECOMSUP');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(1)),'ANTIVDDE','DETIPREG');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL,'DE__AREA',this.w_DE__AREA,'CPROWNUM',this.w_CPROWNUM,'DEDATOPE',this.w_DEDATOPE,'DERIFPNT',SPACE(10),'DEMODPAG',this.w_DEMODPAG,'DEIMPDOC',this.w_DEIMPDOC,'DEESCLUD',this.w_DEESCLUD,'DETIPOPE',"N",'DERIFCON',SPACE(30),'DETIPFAT',"S",'DEFLGEXT',"N")
          insert into (i_cTable) (DESERIAL,DE__AREA,CPROWNUM,DEDATOPE,DERIFPNT,DEMODPAG,DEIMPDOC,DEESCLUD,DETIPOPE,DERIFCON,DETIPFAT,DEFLGEXT,DENUMFAT,DEIMPOST,DEOPERAZ,DEDATFAT,DENUMRET,DEVARIMP,DEVARIMS,DECOMSUP,DETIPREG &i_ccchkf. );
             values (;
               this.w_DESERIAL;
               ,this.w_DE__AREA;
               ,this.w_CPROWNUM;
               ,this.w_DEDATOPE;
               ,SPACE(10);
               ,this.w_DEMODPAG;
               ,this.w_DEIMPDOC;
               ,this.w_DEESCLUD;
               ,"N";
               ,SPACE(30);
               ,"S";
               ,"N";
               ,this.w_DENUMFAT;
               ,this.w_DEIMPOST;
               ,this.w_DEOPERAZ;
               ,this.w_DEDATFAT;
               ,this.w_DENUMRET;
               ,this.w_DEVARIMP;
               ,this.w_DEVARIMS;
               ,"N";
               ,SPACE(1);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_Cprownum = this.w_Cprownum+1
    endcase
    Select (this.w_NomeCurs)
    Endscan
    ADDMSG("Importazione terminata",this)
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_TipoCampo = DatiEstratti(Ascan(DatiEstratti,this.w_NomeCampo,1,Alen(DatiEstratti,1),1,15),2)
    L_Macro=this.w_NomeCampo
    this.w_NomeTabella = "Antivade"
    this.w_IndiceCampo = i_dcx.getfieldidx (this.w_NomeTabella,this.w_NomeCampo)
    if this.w_IndiceCampo=0
      this.w_NomeTabella = "Antivdde"
      this.w_IndiceCampo = i_dcx.getfieldidx (this.w_NomeTabella,this.w_NomeCampo)
    endif
    this.w_FormatoCampo = i_dcx.getfieldtype(this.w_NomeTabella,this.w_IndiceCampo)
    this.w_NumInt = i_dcx.getfieldlen(this.w_NomeTabella,this.w_IndiceCampo)
    this.w_NumDec = i_dcx.getfielddec(this.w_NomeTabella,this.w_IndiceCampo)
    do case
      case this.w_TipoCampo $ "CM" And this.w_FormatoCampo="N" 
        this.w_ValoreCampo = Nvl(cp_Round(Val(&L_Macro),this.w_NumDec),0)
      case ! this.w_TipoCampo $ "CM" And this.w_FormatoCampo="C" 
        this.w_ValoreCampo = Nvl(Alltrim(Str(&L_Macro,this.w_NumInt,this.w_NumDec))," ")
      case ! this.w_TipoCampo $ "DT" And this.w_FormatoCampo="D" 
        do case
          case this.w_TipoCampo $ "CM"
            this.w_ValoreCampo = Cp_Todate(Cp_CharToDate(Nvl(&L_Macro,"  -  -    ")))
          case this.w_TipoCampo = "B"
            this.w_ValoreCampo = Cp_CharToDate("30-12-1899")+Nvl(&L_Macro,0)
            this.w_ValoreCampo = iif(this.w_ValoreCampo=Cp_CharToDate("30-12-1899"),Cp_CharToDate("  -  -    "),this.w_ValoreCampo)
          otherwise
            this.w_ValoreCampo = Cp_CharToDate("  -  -    ")
        endcase
      otherwise
        do case
          case this.w_FormatoCampo="N"
            this.w_ValoreCampo = Nvl(cp_Round(&L_Macro,this.w_NumDec),0)
          case this.w_FormatoCampo="D"
            this.w_ValoreCampo = Cp_Todate(Nvl(&L_Macro,Cp_CharToDate("  -  -    ")))
            this.w_ValoreCampo = iif(this.w_ValoreCampo=Cp_CharToDate("30-12-1899"),Cp_CharToDate("  -  -    "),this.w_ValoreCampo)
          otherwise
            this.w_ValoreCampo = Nvl(&L_Macro," ")
            if this.w_TipoCampo $ "CM" And Left(Alltrim(this.w_ValoreCampo),1)=="'"
              this.w_ValoreCampo = Substr(this.w_ValoreCampo,2,Len(this.w_ValoreCampo)-1)
            endif
        endcase
    endcase
    do case
      case this.w_FormatoCampo="N"
        this.w_CondRiga = this.w_CondRiga+iif(this.w_ValoreCampo=0," ",Str(this.w_ValoreCampo,this.w_NumInt,this.w_NumDec))
      case this.w_FormatoCampo="D"
        this.w_CondRiga = this.w_CondRiga+iif(this.w_ValoreCampo=Cp_CharToDate("  -  -    ")," ",Dtoc(this.w_ValoreCampo))
      otherwise
        this.w_CondRiga = this.w_CondRiga+this.w_ValoreCampo
    endcase
    L_Macro="This.w_"+Alltrim(L_Macro)+"=This.w_ValoreCampo"
    &L_Macro
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ANTIVADE'
    this.cWorkTables[3]='ANTIVDDE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
