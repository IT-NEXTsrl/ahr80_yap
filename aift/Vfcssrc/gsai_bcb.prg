* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bcb                                                        *
*              Calcolo totali dati estratti black list                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-23                                                      *
* Last revis.: 2014-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bcb",oParentObject,m.pParame)
return(i_retval)

define class tgsai_bcb as StdBatch
  * --- Local variables
  pParame = space(10)
  w_GSAI_MDB = .NULL.
  w_Db__Mese = 0
  w_DbTrimes = 0
  w_Dm__MeseSm = 0
  w_AnnoCom = 0
  w_MeseCom = 0
  w_TrimesCom = 0
  * --- WorkFile variables
  DATESTBL_idx=0
  DATESTSM_idx=0
  GENFILTE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calolo i totali
    do case
      case this.pParame="Totali"
        * --- Operazioni attive
        * --- Operazioni passive
        this.oParentObject.w_TAIMC003 = 0
        this.oParentObject.w_TAIMS003 = 0
        this.oParentObject.w_TAIMC004 = 0
        this.oParentObject.w_TAIMS004 = 0
        this.oParentObject.w_TAIMC005 = 0
        this.oParentObject.w_TAIMS005 = 0
        this.oParentObject.w_TPIMC003 = 0
        this.oParentObject.w_TPIMS003 = 0
        this.oParentObject.w_TPIMC004 = 0
        this.oParentObject.w_TPIMC005 = 0
        this.oParentObject.w_TPIMS005 = 0
        this.w_GSAI_MDB = this.oParentObject.GSAI_MDB
        select sum(t_DBIMC003) as DBIMC003, sum(t_DBIMS003) as DBIMS003, sum(t_DBIMC004) as DBIMC004,; 
 sum(t_DBIMS004) as DBIMS004, sum(t_DBIMC005) as DBIMC005,sum(t_DBIMS005) as DBIMS005; 
 from (this.w_GSAI_MDB.cTrsName) where t_Dbesclge=2 into cursor _Curs_Totali
        if used("_Curs_Totali")
          this.oParentObject.w_TAIMC003 = _Curs_Totali.DBIMC003
          this.oParentObject.w_TAIMS003 = _Curs_Totali.DBIMS003
          this.oParentObject.w_TAIMC004 = _Curs_Totali.DBIMC004
          this.oParentObject.w_TAIMS004 = _Curs_Totali.DBIMS004 
          this.oParentObject.w_TAIMC005 = _Curs_Totali.DBIMC005
          this.oParentObject.w_TAIMS005 = _Curs_Totali.DBIMS005 
          use in _Curs_Totali
        endif
        this.w_GSAI_MDB = this.oParentObject.GSAIAMDB
        select sum(t_DBIMC003) as DBIMC003, sum(t_DBIMS003) as DBIMS003, ; 
 sum(t_DBIMC004) as DBIMC004,sum(t_DBIMC005) as DBIMC005,sum(t_DBIMS005) as DBIMS005; 
 from (this.w_GSAI_MDB.cTrsName) where t_Dbesclge=2 into cursor _Curs_Totali
        if used("_Curs_Totali")
          this.oParentObject.w_TPIMC003 = _Curs_Totali.DBIMC003
          this.oParentObject.w_TPIMS003 = _Curs_Totali.DBIMS003
          this.oParentObject.w_TPIMC004 = _Curs_Totali.DBIMC004
          this.oParentObject.w_TPIMC005 = _Curs_Totali.DBIMC005
          this.oParentObject.w_TPIMS005 = _Curs_Totali.DBIMS005 
          use in _Curs_Totali
        endif
      case this.pParame="Periodo"
        if this.oParentObject.w_Estblack="S"
          do case
            case this.oParentObject.w_Period="M"
              this.oParentObject.w_Trimes = 0
              * --- Select from DATESTBL
              i_nConn=i_TableProp[this.DATESTBL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(DB__MESE) As DB__MESE  from "+i_cTable+" DATESTBL ";
                    +" where DB__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+"";
                     ,"_Curs_DATESTBL")
              else
                select MAX(DB__MESE) As DB__MESE from (i_cTable);
                 where DB__ANNO=this.oParentObject.w_ANNO;
                  into cursor _Curs_DATESTBL
              endif
              if used('_Curs_DATESTBL')
                select _Curs_DATESTBL
                locate for 1=1
                do while not(eof())
                this.w_Db__MESE = NVL(_Curs_DATESTBL.DB__MESE,0)
                  select _Curs_DATESTBL
                  continue
                enddo
                use
              endif
              this.oParentObject.w_Mese = iif(NVL(this.w_Db__Mese,0)<>0, this.w_Db__Mese + 1, this.oParentObject.w_Mese)
              this.oParentObject.w_Mese = iif(this.oParentObject.w_Mese>12, 12, this.oParentObject.w_Mese)
              this.oParentObject.w_DaData = iif(this.oParentObject.w_Mese<>0 And this.oParentObject.w_Anno<>0,Date(this.oParentObject.w_Anno, this.oParentObject.w_Mese, 1),Ctod("  -  -    "))
              this.oParentObject.w_Adata = iif(this.oParentObject.w_Mese<>0 And this.oParentObject.w_Anno<>0,Date(Year(this.oParentObject.w_DaData + 31), Month(this.oParentObject.w_DaData + 31),1)-1,Ctod("  -  -    "))
            case this.oParentObject.w_Period="T"
              this.oParentObject.w_Mese = 0
              * --- Select from DATESTBL
              i_nConn=i_TableProp[this.DATESTBL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DATESTBL_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(DBTRIMES) As DBTRIMES  from "+i_cTable+" DATESTBL ";
                    +" where DB__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+"";
                     ,"_Curs_DATESTBL")
              else
                select MAX(DBTRIMES) As DBTRIMES from (i_cTable);
                 where DB__ANNO=this.oParentObject.w_ANNO;
                  into cursor _Curs_DATESTBL
              endif
              if used('_Curs_DATESTBL')
                select _Curs_DATESTBL
                locate for 1=1
                do while not(eof())
                this.w_DbTrimes = NVL(_Curs_DATESTBL.DBTRIMES,0)
                  select _Curs_DATESTBL
                  continue
                enddo
                use
              endif
              this.oParentObject.w_Trimes = iif(Nvl(this.w_Dbtrimes,0)<>0, this.w_Dbtrimes + 1, this.oParentObject.w_Trimes)
              this.oParentObject.w_Trimes = iif(this.oParentObject.w_Trimes>4, 4, this.oParentObject.w_Trimes)
              this.oParentObject.w_DaData = iif(this.oParentObject.w_Trimes<>0 And this.oParentObject.w_Anno<>0,Date(this.oParentObject.w_Anno, (this.oParentObject.w_Trimes-1)*3+1, 1),Ctod("  -  -    "))
              this.oParentObject.w_Adata = iif(this.oParentObject.w_Trimes<>0 And this.oParentObject.w_Anno<>0,Date(Year(this.oParentObject.w_DaData + 93), Month(this.oParentObject.w_DaData + 93), 1)-1,Ctod("  -  -    "))
          endcase
        endif
        if this.oParentObject.w_Est_Sanm="S"
          do case
            case this.oParentObject.w_PeriodSm="M"
              this.oParentObject.w_TrimesSm = 0
              * --- Select from DATESTSM
              i_nConn=i_TableProp[this.DATESTSM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DATESTSM_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(DB__MESE) As DM__MESE  from "+i_cTable+" DATESTSM ";
                    +" where DM__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNOSM)+"";
                     ,"_Curs_DATESTSM")
              else
                select MAX(DB__MESE) As DM__MESE from (i_cTable);
                 where DM__ANNO=this.oParentObject.w_ANNOSM;
                  into cursor _Curs_DATESTSM
              endif
              if used('_Curs_DATESTSM')
                select _Curs_DATESTSM
                locate for 1=1
                do while not(eof())
                this.w_Dm__MeseSm = NVL(_Curs_DATESTBL.DM__MESE,0)
                  select _Curs_DATESTSM
                  continue
                enddo
                use
              endif
              this.oParentObject.w_MeseSm = iif(NVL(this.w_Dm__MeseSm,0)<>0, this.w_Dm__MeseSm + 1, this.oParentObject.w_MeseSm)
              this.oParentObject.w_MeseSm = iif(this.oParentObject.w_MeseSm>12, 12, this.oParentObject.w_MeseSm)
              this.oParentObject.w_DaDataSm = iif(this.oParentObject.w_MeseSm<>0 And this.oParentObject.w_AnnoSm<>0,Date(this.oParentObject.w_AnnoSm, this.oParentObject.w_MeseSm, 1),Ctod("  -  -    "))
              this.oParentObject.w_AdataSm = iif(this.oParentObject.w_MeseSm<>0 And this.oParentObject.w_AnnoSm<>0,Date(Year(this.oParentObject.w_DaDataSm + 31), Month(this.oParentObject.w_DaDataSm + 31),1)-1,Ctod("  -  -    "))
            case this.oParentObject.w_PeriodSm="T"
              this.oParentObject.w_MeseSm = 0
              this.oParentObject.w_TrimesSm = 0
          endcase
        endif
      case this.pParame="TipoCom"
        if This.oParentObject.cFunction="Load"
          this.w_AnnoCom = This.oParentObject.w_At__Anno
          this.w_MeseCom = This.oParentObject.w_At__Mese
          this.w_TrimesCom = This.oParentObject.w_Attrimes
          do case
            case This.OparentObject.w_Atgenspe="S"
              * --- Read from GENFILTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.GENFILTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GENFILTE_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ATSERIAL"+;
                  " from "+i_cTable+" GENFILTE where ";
                      +"AT__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                      +" and ATTIPINV = "+cp_ToStrODBC(0);
                      +" and ATGENSPE = "+cp_ToStrODBC("S");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ATSERIAL;
                  from (i_cTable) where;
                      AT__ANNO = this.oParentObject.w_ANNO;
                      and ATTIPINV = 0;
                      and ATGENSPE = "S";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_SERIALE = NVL(cp_ToDate(_read_.ATSERIAL),cp_NullValue(_read_.ATSERIAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            case This.OparentObject.w_Atgenblk="S"
              * --- Read from GENFILTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.GENFILTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GENFILTE_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ATSERIAL"+;
                  " from "+i_cTable+" GENFILTE where ";
                      +"AT__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                      +" and ATTIPINV = "+cp_ToStrODBC(0);
                      +" and AT__MESE = "+cp_ToStrODBC(this.w_MeseCom);
                      +" and ATTRIMES = "+cp_ToStrODBC(this.w_TrimesCom);
                      +" and ATGENBLK = "+cp_ToStrODBC("S");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ATSERIAL;
                  from (i_cTable) where;
                      AT__ANNO = this.oParentObject.w_ANNO;
                      and ATTIPINV = 0;
                      and AT__MESE = this.w_MeseCom;
                      and ATTRIMES = this.w_TrimesCom;
                      and ATGENBLK = "S";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_SERIALE = NVL(cp_ToDate(_read_.ATSERIAL),cp_NullValue(_read_.ATSERIAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            case This.OparentObject.w_Atgensma="S"
              * --- Read from GENFILTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.GENFILTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GENFILTE_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ATSERIAL"+;
                  " from "+i_cTable+" GENFILTE where ";
                      +"AT__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                      +" and ATTIPINV = "+cp_ToStrODBC(0);
                      +" and AT__MESE = "+cp_ToStrODBC(this.w_MeseCom);
                      +" and ATGENSMA = "+cp_ToStrODBC("S");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ATSERIAL;
                  from (i_cTable) where;
                      AT__ANNO = this.oParentObject.w_ANNO;
                      and ATTIPINV = 0;
                      and AT__MESE = this.w_MeseCom;
                      and ATGENSMA = "S";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_SERIALE = NVL(cp_ToDate(_read_.ATSERIAL),cp_NullValue(_read_.ATSERIAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
          endcase
        endif
    endcase
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DATESTBL'
    this.cWorkTables[2]='DATESTSM'
    this.cWorkTables[3]='GENFILTE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_DATESTBL')
      use in _Curs_DATESTBL
    endif
    if used('_Curs_DATESTBL')
      use in _Curs_DATESTBL
    endif
    if used('_Curs_DATESTSM')
      use in _Curs_DATESTSM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
