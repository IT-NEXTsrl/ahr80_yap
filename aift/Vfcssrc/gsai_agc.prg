* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_agc                                                        *
*              Flussi telematici antievasione IVA                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-25                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsai_agc
L_descri1='1) Rappr. legale o negoziale'
L_descri2='2) Rappr. di minore - curatore eredit�'
L_descri3='3) Curatore fallimentare'
L_descri4='4) Commissario liquidatore'
L_descri5='5) Commissario giudiziale'
L_descri6='6) Rappr. fiscale di soggetto non residente'
L_descri7='7) Erede del dichiarante'
L_descri8='8) Liquidatore volontario'
L_descri9='9) Sogg. dich. IVA operaz. straord.'
L_descri10='10) Rappr. fisc. sogg. non resid. D.L.331/1993'
L_descri11='11) Tutore'
L_descri12='12) Liquid. volont. ditta indiv.'
L_descri13='13) Ammin. condominio'
L_descri14='14) Sogg. per conto P.A.'
L_descri15='15) Comm. liquid. P.A.'
* --- Fine Area Manuale
return(createobject("tgsai_agc"))

* --- Class definition
define class tgsai_agc as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 906
  Height = 439+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-04"
  HelpContextID=42600297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=85

  * --- Constant Properties
  ANTIVAGC_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  ANTIVAPA_IDX = 0
  DAT_RAPP_IDX = 0
  cFile = "ANTIVAGC"
  cKeySelect = "AISERIAL"
  cKeyWhere  = "AISERIAL=this.w_AISERIAL"
  cKeyWhereODBC = '"AISERIAL="+cp_ToStrODBC(this.w_AISERIAL)';

  cKeyWhereODBCqualified = '"ANTIVAGC.AISERIAL="+cp_ToStrODBC(this.w_AISERIAL)';

  cPrg = "gsai_agc"
  cComment = "Flussi telematici antievasione IVA"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(10)
  w_PAPERIOD = space(1)
  w_CODAZI1 = space(5)
  w_AZPIVAZI = space(12)
  w_AISERIAL = space(10)
  w_AZCODFIS = space(16)
  w_AZPERAZI = space(1)
  o_AZPERAZI = space(1)
  w_AZCODNAZ = space(3)
  w_AZLOCAZI = space(30)
  w_AZPROAZI = space(2)
  w_AZNAGAZI = space(5)
  w_AZTELEFO = space(18)
  w_AZ_EMAIL = space(254)
  w_AZIVACOF = space(16)
  w_AZTELFAX = space(18)
  w_CODAZI2 = space(5)
  w_COGTIT1 = space(25)
  w_NOMTIT1 = space(25)
  w_DATNAS1 = ctod('  /  /  ')
  w_LOCTIT1 = space(30)
  w_PROTIT1 = space(2)
  w_SESSO1 = space(1)
  w_TELEFO1 = space(18)
  w_AI__ANNO = 0
  o_AI__ANNO = 0
  w_PERIOD = space(1)
  o_PERIOD = space(1)
  w_AI__MESE = 0
  o_AI__MESE = 0
  w_AITRIMES = 0
  o_AITRIMES = 0
  w_AIVARPER = space(1)
  w_AICORTER = space(1)
  o_AICORTER = space(1)
  w_AICOMINT = space(1)
  o_AICOMINT = space(1)
  w_AIPARIVA = space(11)
  w_AICODFIS = space(16)
  w_AI__MAIL = space(100)
  w_AITELEFO = space(12)
  w_AINUMFAX = space(12)
  w_AICOGNOM = space(24)
  w_Forza2 = .F.
  o_Forza2 = .F.
  w_RAPPFIRM = space(5)
  w_RFCODFIS = space(16)
  o_RFCODFIS = space(16)
  w_RFCODCAR = space(2)
  w_RF__NOME = space(40)
  w_RFCOGNOM = space(40)
  w_RF_SESSO = space(1)
  w_RFDATNAS = ctod('  /  /  ')
  w_RFCOMNAS = space(40)
  w_RFPRONAS = space(2)
  w_RFCOMRES = space(40)
  w_RFPRORES = space(2)
  w_RFCAPRES = space(9)
  w_RFINDRES = space(35)
  w_RFTELEFO = space(18)
  w_AICFISOT = space(16)
  w_AICODCAR = space(2)
  w_AI__NOME = space(20)
  w_AI_SESSO = space(1)
  w_AIDATNAS = ctod('  /  /  ')
  w_AILOCNAS = space(79)
  w_AIPRONAS = space(2)
  w_AIRAGSOC = space(60)
  w_AINATGIU = space(2)
  w_AISTAEST = space(24)
  w_AICODEST = space(3)
  w_AINUMIVA = space(24)
  w_AICDFSDR = space(11)
  w_AICOGSOT = space(24)
  w_AINOMSOT = space(20)
  w_AISESSOT = space(1)
  w_AIDTNSST = ctod('  /  /  ')
  w_AILCNSST = space(40)
  w_AIPRNSST = space(2)
  w_AILCRSST = space(40)
  w_AIPRRSST = space(2)
  w_AICPRSST = space(5)
  w_AIFRRSST = space(35)
  w_AITLRSST = space(12)
  w_DirName = space(200)
  w_AINUMMOD = 0
  w_AIFIRDIC = space(1)
  w_AITIPFOR = space(2)
  o_AITIPFOR = space(2)
  w_AICODINT = space(16)
  o_AICODINT = space(16)
  w_AICODCAF = space(5)
  w_AIIMPTRA = space(1)
  w_AIDATIMP = ctod('  /  /  ')
  w_AIFIRINT = space(1)
  w_AINOMFIL = space(254)
  o_AINOMFIL = space(254)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_AISERIAL = this.W_AISERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANTIVAGC','gsai_agc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_agcPag1","gsai_agc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Comunicazione/contribuente")
      .Pages(1).HelpContextID = 254321956
      .Pages(2).addobject("oPag","tgsai_agcPag2","gsai_agc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rappresentante firmatario")
      .Pages(2).HelpContextID = 258620074
      .Pages(3).addobject("oPag","tgsai_agcPag3","gsai_agc",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati trasmissione")
      .Pages(3).HelpContextID = 191846011
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TITOLARI'
    this.cWorkTables[3]='ANTIVAPA'
    this.cWorkTables[4]='DAT_RAPP'
    this.cWorkTables[5]='ANTIVAGC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVAGC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVAGC_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AISERIAL = NVL(AISERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANTIVAGC where AISERIAL=KeySet.AISERIAL
    *
    i_nConn = i_TableProp[this.ANTIVAGC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGC_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVAGC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVAGC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVAGC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AISERIAL',this.w_AISERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_PAPERIOD = space(1)
        .w_CODAZI1 = i_CODAZI
        .w_AZPIVAZI = space(12)
        .w_AZCODFIS = space(16)
        .w_AZPERAZI = space(1)
        .w_AZCODNAZ = space(3)
        .w_AZLOCAZI = space(30)
        .w_AZPROAZI = space(2)
        .w_AZNAGAZI = space(5)
        .w_AZTELEFO = space(18)
        .w_AZ_EMAIL = space(254)
        .w_AZIVACOF = space(16)
        .w_AZTELFAX = space(18)
        .w_CODAZI2 = i_CODAZI
        .w_COGTIT1 = space(25)
        .w_NOMTIT1 = space(25)
        .w_DATNAS1 = ctod("  /  /  ")
        .w_LOCTIT1 = space(30)
        .w_PROTIT1 = space(2)
        .w_SESSO1 = space(1)
        .w_TELEFO1 = space(18)
        .w_PERIOD = iif(EMPTY(NVL(.w_PAPERIOD,' ')), 'M', .w_PAPERIOD)
        .w_Forza2 = iif(.w_AZPERAZI<>'S',.w_Forza2,.f.)
        .w_RFCODFIS = space(16)
        .w_RFCODCAR = space(2)
        .w_RF__NOME = space(40)
        .w_RFCOGNOM = space(40)
        .w_RF_SESSO = space(1)
        .w_RFDATNAS = ctod("  /  /  ")
        .w_RFCOMNAS = space(40)
        .w_RFPRONAS = space(2)
        .w_RFCOMRES = space(40)
        .w_RFPRORES = space(2)
        .w_RFCAPRES = space(9)
        .w_RFINDRES = space(35)
        .w_RFTELEFO = space(18)
        .w_DirName = sys(5)+sys(2003)+'\'
          .link_1_1('Load')
          .link_1_3('Load')
        .w_AISERIAL = NVL(AISERIAL,space(10))
        .op_AISERIAL = .w_AISERIAL
          .link_1_16('Load')
        .w_AI__ANNO = NVL(AI__ANNO,0)
        .w_AI__MESE = NVL(AI__MESE,0)
        .w_AITRIMES = NVL(AITRIMES,0)
        .w_AIVARPER = NVL(AIVARPER,space(1))
        .w_AICORTER = NVL(AICORTER,space(1))
        .w_AICOMINT = NVL(AICOMINT,space(1))
        .w_AIPARIVA = NVL(AIPARIVA,space(11))
        .w_AICODFIS = NVL(AICODFIS,space(16))
        .w_AI__MAIL = NVL(AI__MAIL,space(100))
        .w_AITELEFO = NVL(AITELEFO,space(12))
        .w_AINUMFAX = NVL(AINUMFAX,space(12))
        .w_AICOGNOM = NVL(AICOGNOM,space(24))
        .w_RAPPFIRM = iif(.w_AZPERAZI<>'S',i_CODAZI,' ')
          .link_2_2('Load')
        .w_AICFISOT = NVL(AICFISOT,space(16))
        .w_AICODCAR = NVL(AICODCAR,space(2))
        .w_AI__NOME = NVL(AI__NOME,space(20))
        .w_AI_SESSO = NVL(AI_SESSO,space(1))
        .w_AIDATNAS = NVL(cp_ToDate(AIDATNAS),ctod("  /  /  "))
        .w_AILOCNAS = NVL(AILOCNAS,space(79))
        .w_AIPRONAS = NVL(AIPRONAS,space(2))
        .w_AIRAGSOC = NVL(AIRAGSOC,space(60))
        .w_AINATGIU = NVL(AINATGIU,space(2))
        .w_AISTAEST = NVL(AISTAEST,space(24))
        .w_AICODEST = NVL(AICODEST,space(3))
        .w_AINUMIVA = NVL(AINUMIVA,space(24))
        .w_AICDFSDR = NVL(AICDFSDR,space(11))
        .w_AICOGSOT = NVL(AICOGSOT,space(24))
        .w_AINOMSOT = NVL(AINOMSOT,space(20))
        .w_AISESSOT = NVL(AISESSOT,space(1))
        .w_AIDTNSST = NVL(cp_ToDate(AIDTNSST),ctod("  /  /  "))
        .w_AILCNSST = NVL(AILCNSST,space(40))
        .w_AIPRNSST = NVL(AIPRNSST,space(2))
        .w_AILCRSST = NVL(AILCRSST,space(40))
        .w_AIPRRSST = NVL(AIPRRSST,space(2))
        .w_AICPRSST = NVL(AICPRSST,space(5))
        .w_AIFRRSST = NVL(AIFRRSST,space(35))
        .w_AITLRSST = NVL(AITLRSST,space(12))
        .w_AINUMMOD = NVL(AINUMMOD,0)
        .w_AIFIRDIC = NVL(AIFIRDIC,space(1))
        .w_AITIPFOR = NVL(AITIPFOR,space(2))
        .w_AICODINT = NVL(AICODINT,space(16))
        .w_AICODCAF = NVL(AICODCAF,space(5))
        .w_AIIMPTRA = NVL(AIIMPTRA,space(1))
        .w_AIDATIMP = NVL(cp_ToDate(AIDATIMP),ctod("  /  /  "))
        .w_AIFIRINT = NVL(AIFIRINT,space(1))
        .w_AINOMFIL = NVL(AINOMFIL,space(254))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ANTIVAGC')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(10)
      .w_PAPERIOD = space(1)
      .w_CODAZI1 = space(5)
      .w_AZPIVAZI = space(12)
      .w_AISERIAL = space(10)
      .w_AZCODFIS = space(16)
      .w_AZPERAZI = space(1)
      .w_AZCODNAZ = space(3)
      .w_AZLOCAZI = space(30)
      .w_AZPROAZI = space(2)
      .w_AZNAGAZI = space(5)
      .w_AZTELEFO = space(18)
      .w_AZ_EMAIL = space(254)
      .w_AZIVACOF = space(16)
      .w_AZTELFAX = space(18)
      .w_CODAZI2 = space(5)
      .w_COGTIT1 = space(25)
      .w_NOMTIT1 = space(25)
      .w_DATNAS1 = ctod("  /  /  ")
      .w_LOCTIT1 = space(30)
      .w_PROTIT1 = space(2)
      .w_SESSO1 = space(1)
      .w_TELEFO1 = space(18)
      .w_AI__ANNO = 0
      .w_PERIOD = space(1)
      .w_AI__MESE = 0
      .w_AITRIMES = 0
      .w_AIVARPER = space(1)
      .w_AICORTER = space(1)
      .w_AICOMINT = space(1)
      .w_AIPARIVA = space(11)
      .w_AICODFIS = space(16)
      .w_AI__MAIL = space(100)
      .w_AITELEFO = space(12)
      .w_AINUMFAX = space(12)
      .w_AICOGNOM = space(24)
      .w_Forza2 = .f.
      .w_RAPPFIRM = space(5)
      .w_RFCODFIS = space(16)
      .w_RFCODCAR = space(2)
      .w_RF__NOME = space(40)
      .w_RFCOGNOM = space(40)
      .w_RF_SESSO = space(1)
      .w_RFDATNAS = ctod("  /  /  ")
      .w_RFCOMNAS = space(40)
      .w_RFPRONAS = space(2)
      .w_RFCOMRES = space(40)
      .w_RFPRORES = space(2)
      .w_RFCAPRES = space(9)
      .w_RFINDRES = space(35)
      .w_RFTELEFO = space(18)
      .w_AICFISOT = space(16)
      .w_AICODCAR = space(2)
      .w_AI__NOME = space(20)
      .w_AI_SESSO = space(1)
      .w_AIDATNAS = ctod("  /  /  ")
      .w_AILOCNAS = space(79)
      .w_AIPRONAS = space(2)
      .w_AIRAGSOC = space(60)
      .w_AINATGIU = space(2)
      .w_AISTAEST = space(24)
      .w_AICODEST = space(3)
      .w_AINUMIVA = space(24)
      .w_AICDFSDR = space(11)
      .w_AICOGSOT = space(24)
      .w_AINOMSOT = space(20)
      .w_AISESSOT = space(1)
      .w_AIDTNSST = ctod("  /  /  ")
      .w_AILCNSST = space(40)
      .w_AIPRNSST = space(2)
      .w_AILCRSST = space(40)
      .w_AIPRRSST = space(2)
      .w_AICPRSST = space(5)
      .w_AIFRRSST = space(35)
      .w_AITLRSST = space(12)
      .w_DirName = space(200)
      .w_AINUMMOD = 0
      .w_AIFIRDIC = space(1)
      .w_AITIPFOR = space(2)
      .w_AICODINT = space(16)
      .w_AICODCAF = space(5)
      .w_AIIMPTRA = space(1)
      .w_AIDATIMP = ctod("  /  /  ")
      .w_AIFIRINT = space(1)
      .w_AINOMFIL = space(254)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_CODAZI1))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,15,.f.)
        .w_CODAZI2 = i_CODAZI
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_CODAZI2))
          .link_1_16('Full')
          endif
          .DoRTCalc(17,23,.f.)
        .w_AI__ANNO = year(i_DATSYS)
        .w_PERIOD = iif(EMPTY(NVL(.w_PAPERIOD,' ')), 'M', .w_PAPERIOD)
        .w_AI__MESE = iif(NVL(.w_PERIOD,'M')='M', month(i_DATSYS), 0)
        .w_AITRIMES = iif(NVL(.w_PERIOD,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        .w_AIVARPER = ' '
        .w_AICORTER = iif(.w_AICOMINT='S', ' ', .w_AICORTER)
        .w_AICOMINT = iif(.w_AICORTER='S', ' ', .w_AICOMINT)
        .w_AIPARIVA = left(.w_AZPIVAZI, 11)
        .w_AICODFIS = IIF(LEN(ALLTRIM(.w_AZCODFIS))<=16, LEFT(ALLTRIM(.w_AZCODFIS),16), RIGHT(ALLTRIM(.w_AZCODFIS),16))
        .w_AI__MAIL = left(UPPER(.w_AZ_EMAIL), 100)
        .w_AITELEFO = left(iif(.w_AZPERAZI='S', .w_TELEFO1, .w_AZTELEFO), 12)
        .w_AINUMFAX = left(.w_AZTELFAX, 12)
        .w_AICOGNOM = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_COGTIT1), space(24)), 24)
        .w_Forza2 = iif(.w_AZPERAZI<>'S',.w_Forza2,.f.)
        .w_RAPPFIRM = iif(.w_AZPERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_RAPPFIRM))
          .link_2_2('Full')
          endif
          .DoRTCalc(39,51,.f.)
        .w_AICFISOT = iif(.w_AZPERAZI<>'S' or .w_Forza2, IIF(EMPTY(.w_RFCODFIS),.w_AZIVACOF,.w_RFCODFIS), space(16))
        .w_AICODCAR = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCODCAR,'1')
        .w_AI__NOME = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_NOMTIT1), space(20)), 20)
        .w_AI_SESSO = iif(.w_AZPERAZI='S' and not empty(.w_SESSO1), .w_SESSO1, 'M')
        .w_AIDATNAS = iif(.w_AZPERAZI='S', .w_DATNAS1, CTOD("  -  -    "))
        .w_AILOCNAS = left(iif(.w_AZPERAZI='S', UPPER(.w_LOCTIT1), space(79)), 79)
        .w_AIPRONAS = iif(.w_AZPERAZI='S', UPPER(.w_PROTIT1), space(2))
        .w_AIRAGSOC = LEFT(iif(.w_AZPERAZI<>'S', UPPER(g_RAGAZI), space(60)), 60)
        .w_AINATGIU = iif(.w_AZPERAZI<>'S', PADL(alltrim(UPPER(.w_AZNAGAZI)), 2, '0'), space(2))
          .DoRTCalc(61,63,.f.)
        .w_AICDFSDR = space(11)
        .w_AICOGSOT = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCOGNOM,space(24))
        .w_AINOMSOT = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RF__NOME,space(20))
        .w_AISESSOT = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RF_SESSO,'M')
        .w_AIDTNSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFDATNAS,Ctod('  -  -    '))
        .w_AILCNSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCOMNAS,space(40))
        .w_AIPRNSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFPRONAS,'  ')
        .w_AILCRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCOMRES,space(40))
        .w_AIPRRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFPRORES,'  ')
        .w_AICPRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCAPRES,space(5))
        .w_AIFRRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFINDRES,space(35))
        .w_AITLRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFTELEFO,space(12))
        .w_DirName = sys(5)+sys(2003)+'\'
          .DoRTCalc(77,77,.f.)
        .w_AIFIRDIC = '1'
        .w_AITIPFOR = '01'
        .w_AICODINT = iif(.w_AITIPFOR='01', space(16), .w_AICODINT)
        .w_AICODCAF = iif(EMPTY(.w_AICODINT), space(5), .w_AICODCAF)
        .w_AIIMPTRA = '1'
        .w_AIDATIMP = iif(EMPTY(.w_AICODINT), CTOD('  -  -    '), .w_AIDATIMP)
        .w_AIFIRINT = iif(EMPTY(.w_AICODINT), '0', '1')
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVAGC')
    this.DoRTCalc(85,85,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVAGC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGC_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"AIVGF","i_codazi,w_AISERIAL")
      .op_codazi = .w_codazi
      .op_AISERIAL = .w_AISERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAI__ANNO_1_24.enabled = i_bVal
      .Page1.oPag.oPERIOD_1_26.enabled_(i_bVal)
      .Page1.oPag.oAI__MESE_1_27.enabled = i_bVal
      .Page1.oPag.oAITRIMES_1_29.enabled = i_bVal
      .Page1.oPag.oAIVARPER_1_31.enabled = i_bVal
      .Page1.oPag.oAICORTER_1_32.enabled = i_bVal
      .Page1.oPag.oAICOMINT_1_33.enabled = i_bVal
      .Page1.oPag.oAIPARIVA_1_34.enabled = i_bVal
      .Page1.oPag.oAICODFIS_1_35.enabled = i_bVal
      .Page1.oPag.oAI__MAIL_1_37.enabled = i_bVal
      .Page1.oPag.oAITELEFO_1_38.enabled = i_bVal
      .Page1.oPag.oAINUMFAX_1_39.enabled = i_bVal
      .Page1.oPag.oAICOGNOM_1_43.enabled = i_bVal
      .Page2.oPag.oForza2_2_1.enabled = i_bVal
      .Page2.oPag.oAICFISOT_2_16.enabled = i_bVal
      .Page2.oPag.oAICODCAR_2_17.enabled = i_bVal
      .Page1.oPag.oAI__NOME_1_45.enabled = i_bVal
      .Page1.oPag.oAI_SESSO_1_47.enabled_(i_bVal)
      .Page1.oPag.oAIDATNAS_1_49.enabled = i_bVal
      .Page1.oPag.oAILOCNAS_1_51.enabled = i_bVal
      .Page1.oPag.oAIPRONAS_1_52.enabled = i_bVal
      .Page1.oPag.oAIRAGSOC_1_54.enabled = i_bVal
      .Page1.oPag.oAINATGIU_1_56.enabled = i_bVal
      .Page1.oPag.oAISTAEST_1_58.enabled = i_bVal
      .Page1.oPag.oAICODEST_1_60.enabled = i_bVal
      .Page1.oPag.oAINUMIVA_1_62.enabled = i_bVal
      .Page2.oPag.oAICDFSDR_2_19.enabled = i_bVal
      .Page2.oPag.oAICOGSOT_2_21.enabled = i_bVal
      .Page2.oPag.oAINOMSOT_2_23.enabled = i_bVal
      .Page2.oPag.oAISESSOT_2_25.enabled_(i_bVal)
      .Page2.oPag.oAIDTNSST_2_26.enabled = i_bVal
      .Page2.oPag.oAILCNSST_2_28.enabled = i_bVal
      .Page2.oPag.oAIPRNSST_2_30.enabled = i_bVal
      .Page2.oPag.oAILCRSST_2_32.enabled = i_bVal
      .Page2.oPag.oAIPRRSST_2_34.enabled = i_bVal
      .Page2.oPag.oAICPRSST_2_36.enabled = i_bVal
      .Page2.oPag.oAIFRRSST_2_38.enabled = i_bVal
      .Page2.oPag.oAITLRSST_2_40.enabled = i_bVal
      .Page3.oPag.oAIFIRDIC_3_3.enabled = i_bVal
      .Page3.oPag.oAITIPFOR_3_5.enabled = i_bVal
      .Page3.oPag.oAICODINT_3_6.enabled = i_bVal
      .Page3.oPag.oAICODCAF_3_8.enabled = i_bVal
      .Page3.oPag.oAIIMPTRA_3_10.enabled = i_bVal
      .Page3.oPag.oAIDATIMP_3_11.enabled = i_bVal
      .Page3.oPag.oAIFIRINT_3_12.enabled = i_bVal
      .Page3.oPag.oAINOMFIL_3_14.enabled = i_bVal
      .Page3.oPag.oBtn_3_16.enabled = i_bVal
      .Page3.oPag.oBtn_3_17.enabled = i_bVal
      .Page3.oPag.oBtn_3_20.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ANTIVAGC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVAGC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERIAL,"AISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI__ANNO,"AI__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI__MESE,"AI__MESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITRIMES,"AITRIMES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIVARPER,"AIVARPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICORTER,"AICORTER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOMINT,"AICOMINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPARIVA,"AIPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODFIS,"AICODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI__MAIL,"AI__MAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITELEFO,"AITELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINUMFAX,"AINUMFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOGNOM,"AICOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICFISOT,"AICFISOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODCAR,"AICODCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI__NOME,"AI__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI_SESSO,"AI_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATNAS,"AIDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AILOCNAS,"AILOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPRONAS,"AIPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIRAGSOC,"AIRAGSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINATGIU,"AINATGIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISTAEST,"AISTAEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODEST,"AICODEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINUMIVA,"AINUMIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICDFSDR,"AICDFSDR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOGSOT,"AICOGSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINOMSOT,"AINOMSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISESSOT,"AISESSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDTNSST,"AIDTNSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AILCNSST,"AILCNSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPRNSST,"AIPRNSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AILCRSST,"AILCRSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPRRSST,"AIPRRSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICPRSST,"AICPRSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIFRRSST,"AIFRRSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITLRSST,"AITLRSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINUMMOD,"AINUMMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIFIRDIC,"AIFIRDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITIPFOR,"AITIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODINT,"AICODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODCAF,"AICODCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIMPTRA,"AIIMPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATIMP,"AIDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIFIRINT,"AIFIRINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINOMFIL,"AINOMFIL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVAGC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGC_IDX,2])
    i_lTable = "ANTIVAGC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANTIVAGC_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVAGC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGC_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANTIVAGC_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"AIVGF","i_codazi,w_AISERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANTIVAGC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVAGC')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVAGC')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AISERIAL,AI__ANNO,AI__MESE,AITRIMES,AIVARPER"+;
                  ",AICORTER,AICOMINT,AIPARIVA,AICODFIS,AI__MAIL"+;
                  ",AITELEFO,AINUMFAX,AICOGNOM,AICFISOT,AICODCAR"+;
                  ",AI__NOME,AI_SESSO,AIDATNAS,AILOCNAS,AIPRONAS"+;
                  ",AIRAGSOC,AINATGIU,AISTAEST,AICODEST,AINUMIVA"+;
                  ",AICDFSDR,AICOGSOT,AINOMSOT,AISESSOT,AIDTNSST"+;
                  ",AILCNSST,AIPRNSST,AILCRSST,AIPRRSST,AICPRSST"+;
                  ",AIFRRSST,AITLRSST,AINUMMOD,AIFIRDIC,AITIPFOR"+;
                  ",AICODINT,AICODCAF,AIIMPTRA,AIDATIMP,AIFIRINT"+;
                  ",AINOMFIL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AISERIAL)+;
                  ","+cp_ToStrODBC(this.w_AI__ANNO)+;
                  ","+cp_ToStrODBC(this.w_AI__MESE)+;
                  ","+cp_ToStrODBC(this.w_AITRIMES)+;
                  ","+cp_ToStrODBC(this.w_AIVARPER)+;
                  ","+cp_ToStrODBC(this.w_AICORTER)+;
                  ","+cp_ToStrODBC(this.w_AICOMINT)+;
                  ","+cp_ToStrODBC(this.w_AIPARIVA)+;
                  ","+cp_ToStrODBC(this.w_AICODFIS)+;
                  ","+cp_ToStrODBC(this.w_AI__MAIL)+;
                  ","+cp_ToStrODBC(this.w_AITELEFO)+;
                  ","+cp_ToStrODBC(this.w_AINUMFAX)+;
                  ","+cp_ToStrODBC(this.w_AICOGNOM)+;
                  ","+cp_ToStrODBC(this.w_AICFISOT)+;
                  ","+cp_ToStrODBC(this.w_AICODCAR)+;
                  ","+cp_ToStrODBC(this.w_AI__NOME)+;
                  ","+cp_ToStrODBC(this.w_AI_SESSO)+;
                  ","+cp_ToStrODBC(this.w_AIDATNAS)+;
                  ","+cp_ToStrODBC(this.w_AILOCNAS)+;
                  ","+cp_ToStrODBC(this.w_AIPRONAS)+;
                  ","+cp_ToStrODBC(this.w_AIRAGSOC)+;
                  ","+cp_ToStrODBC(this.w_AINATGIU)+;
                  ","+cp_ToStrODBC(this.w_AISTAEST)+;
                  ","+cp_ToStrODBC(this.w_AICODEST)+;
                  ","+cp_ToStrODBC(this.w_AINUMIVA)+;
                  ","+cp_ToStrODBC(this.w_AICDFSDR)+;
                  ","+cp_ToStrODBC(this.w_AICOGSOT)+;
                  ","+cp_ToStrODBC(this.w_AINOMSOT)+;
                  ","+cp_ToStrODBC(this.w_AISESSOT)+;
                  ","+cp_ToStrODBC(this.w_AIDTNSST)+;
                  ","+cp_ToStrODBC(this.w_AILCNSST)+;
                  ","+cp_ToStrODBC(this.w_AIPRNSST)+;
                  ","+cp_ToStrODBC(this.w_AILCRSST)+;
                  ","+cp_ToStrODBC(this.w_AIPRRSST)+;
                  ","+cp_ToStrODBC(this.w_AICPRSST)+;
                  ","+cp_ToStrODBC(this.w_AIFRRSST)+;
                  ","+cp_ToStrODBC(this.w_AITLRSST)+;
                  ","+cp_ToStrODBC(this.w_AINUMMOD)+;
                  ","+cp_ToStrODBC(this.w_AIFIRDIC)+;
                  ","+cp_ToStrODBC(this.w_AITIPFOR)+;
                  ","+cp_ToStrODBC(this.w_AICODINT)+;
                  ","+cp_ToStrODBC(this.w_AICODCAF)+;
                  ","+cp_ToStrODBC(this.w_AIIMPTRA)+;
                  ","+cp_ToStrODBC(this.w_AIDATIMP)+;
                  ","+cp_ToStrODBC(this.w_AIFIRINT)+;
                  ","+cp_ToStrODBC(this.w_AINOMFIL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVAGC')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVAGC')
        cp_CheckDeletedKey(i_cTable,0,'AISERIAL',this.w_AISERIAL)
        INSERT INTO (i_cTable);
              (AISERIAL,AI__ANNO,AI__MESE,AITRIMES,AIVARPER,AICORTER,AICOMINT,AIPARIVA,AICODFIS,AI__MAIL,AITELEFO,AINUMFAX,AICOGNOM,AICFISOT,AICODCAR,AI__NOME,AI_SESSO,AIDATNAS,AILOCNAS,AIPRONAS,AIRAGSOC,AINATGIU,AISTAEST,AICODEST,AINUMIVA,AICDFSDR,AICOGSOT,AINOMSOT,AISESSOT,AIDTNSST,AILCNSST,AIPRNSST,AILCRSST,AIPRRSST,AICPRSST,AIFRRSST,AITLRSST,AINUMMOD,AIFIRDIC,AITIPFOR,AICODINT,AICODCAF,AIIMPTRA,AIDATIMP,AIFIRINT,AINOMFIL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AISERIAL;
                  ,this.w_AI__ANNO;
                  ,this.w_AI__MESE;
                  ,this.w_AITRIMES;
                  ,this.w_AIVARPER;
                  ,this.w_AICORTER;
                  ,this.w_AICOMINT;
                  ,this.w_AIPARIVA;
                  ,this.w_AICODFIS;
                  ,this.w_AI__MAIL;
                  ,this.w_AITELEFO;
                  ,this.w_AINUMFAX;
                  ,this.w_AICOGNOM;
                  ,this.w_AICFISOT;
                  ,this.w_AICODCAR;
                  ,this.w_AI__NOME;
                  ,this.w_AI_SESSO;
                  ,this.w_AIDATNAS;
                  ,this.w_AILOCNAS;
                  ,this.w_AIPRONAS;
                  ,this.w_AIRAGSOC;
                  ,this.w_AINATGIU;
                  ,this.w_AISTAEST;
                  ,this.w_AICODEST;
                  ,this.w_AINUMIVA;
                  ,this.w_AICDFSDR;
                  ,this.w_AICOGSOT;
                  ,this.w_AINOMSOT;
                  ,this.w_AISESSOT;
                  ,this.w_AIDTNSST;
                  ,this.w_AILCNSST;
                  ,this.w_AIPRNSST;
                  ,this.w_AILCRSST;
                  ,this.w_AIPRRSST;
                  ,this.w_AICPRSST;
                  ,this.w_AIFRRSST;
                  ,this.w_AITLRSST;
                  ,this.w_AINUMMOD;
                  ,this.w_AIFIRDIC;
                  ,this.w_AITIPFOR;
                  ,this.w_AICODINT;
                  ,this.w_AICODCAF;
                  ,this.w_AIIMPTRA;
                  ,this.w_AIDATIMP;
                  ,this.w_AIFIRINT;
                  ,this.w_AINOMFIL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANTIVAGC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGC_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANTIVAGC_IDX,i_nConn)
      *
      * update ANTIVAGC
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVAGC')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AI__ANNO="+cp_ToStrODBC(this.w_AI__ANNO)+;
             ",AI__MESE="+cp_ToStrODBC(this.w_AI__MESE)+;
             ",AITRIMES="+cp_ToStrODBC(this.w_AITRIMES)+;
             ",AIVARPER="+cp_ToStrODBC(this.w_AIVARPER)+;
             ",AICORTER="+cp_ToStrODBC(this.w_AICORTER)+;
             ",AICOMINT="+cp_ToStrODBC(this.w_AICOMINT)+;
             ",AIPARIVA="+cp_ToStrODBC(this.w_AIPARIVA)+;
             ",AICODFIS="+cp_ToStrODBC(this.w_AICODFIS)+;
             ",AI__MAIL="+cp_ToStrODBC(this.w_AI__MAIL)+;
             ",AITELEFO="+cp_ToStrODBC(this.w_AITELEFO)+;
             ",AINUMFAX="+cp_ToStrODBC(this.w_AINUMFAX)+;
             ",AICOGNOM="+cp_ToStrODBC(this.w_AICOGNOM)+;
             ",AICFISOT="+cp_ToStrODBC(this.w_AICFISOT)+;
             ",AICODCAR="+cp_ToStrODBC(this.w_AICODCAR)+;
             ",AI__NOME="+cp_ToStrODBC(this.w_AI__NOME)+;
             ",AI_SESSO="+cp_ToStrODBC(this.w_AI_SESSO)+;
             ",AIDATNAS="+cp_ToStrODBC(this.w_AIDATNAS)+;
             ",AILOCNAS="+cp_ToStrODBC(this.w_AILOCNAS)+;
             ",AIPRONAS="+cp_ToStrODBC(this.w_AIPRONAS)+;
             ",AIRAGSOC="+cp_ToStrODBC(this.w_AIRAGSOC)+;
             ",AINATGIU="+cp_ToStrODBC(this.w_AINATGIU)+;
             ",AISTAEST="+cp_ToStrODBC(this.w_AISTAEST)+;
             ",AICODEST="+cp_ToStrODBC(this.w_AICODEST)+;
             ",AINUMIVA="+cp_ToStrODBC(this.w_AINUMIVA)+;
             ",AICDFSDR="+cp_ToStrODBC(this.w_AICDFSDR)+;
             ",AICOGSOT="+cp_ToStrODBC(this.w_AICOGSOT)+;
             ",AINOMSOT="+cp_ToStrODBC(this.w_AINOMSOT)+;
             ",AISESSOT="+cp_ToStrODBC(this.w_AISESSOT)+;
             ",AIDTNSST="+cp_ToStrODBC(this.w_AIDTNSST)+;
             ",AILCNSST="+cp_ToStrODBC(this.w_AILCNSST)+;
             ",AIPRNSST="+cp_ToStrODBC(this.w_AIPRNSST)+;
             ",AILCRSST="+cp_ToStrODBC(this.w_AILCRSST)+;
             ",AIPRRSST="+cp_ToStrODBC(this.w_AIPRRSST)+;
             ",AICPRSST="+cp_ToStrODBC(this.w_AICPRSST)+;
             ",AIFRRSST="+cp_ToStrODBC(this.w_AIFRRSST)+;
             ",AITLRSST="+cp_ToStrODBC(this.w_AITLRSST)+;
             ",AINUMMOD="+cp_ToStrODBC(this.w_AINUMMOD)+;
             ",AIFIRDIC="+cp_ToStrODBC(this.w_AIFIRDIC)+;
             ",AITIPFOR="+cp_ToStrODBC(this.w_AITIPFOR)+;
             ",AICODINT="+cp_ToStrODBC(this.w_AICODINT)+;
             ",AICODCAF="+cp_ToStrODBC(this.w_AICODCAF)+;
             ",AIIMPTRA="+cp_ToStrODBC(this.w_AIIMPTRA)+;
             ",AIDATIMP="+cp_ToStrODBC(this.w_AIDATIMP)+;
             ",AIFIRINT="+cp_ToStrODBC(this.w_AIFIRINT)+;
             ",AINOMFIL="+cp_ToStrODBC(this.w_AINOMFIL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVAGC')
        i_cWhere = cp_PKFox(i_cTable  ,'AISERIAL',this.w_AISERIAL  )
        UPDATE (i_cTable) SET;
              AI__ANNO=this.w_AI__ANNO;
             ,AI__MESE=this.w_AI__MESE;
             ,AITRIMES=this.w_AITRIMES;
             ,AIVARPER=this.w_AIVARPER;
             ,AICORTER=this.w_AICORTER;
             ,AICOMINT=this.w_AICOMINT;
             ,AIPARIVA=this.w_AIPARIVA;
             ,AICODFIS=this.w_AICODFIS;
             ,AI__MAIL=this.w_AI__MAIL;
             ,AITELEFO=this.w_AITELEFO;
             ,AINUMFAX=this.w_AINUMFAX;
             ,AICOGNOM=this.w_AICOGNOM;
             ,AICFISOT=this.w_AICFISOT;
             ,AICODCAR=this.w_AICODCAR;
             ,AI__NOME=this.w_AI__NOME;
             ,AI_SESSO=this.w_AI_SESSO;
             ,AIDATNAS=this.w_AIDATNAS;
             ,AILOCNAS=this.w_AILOCNAS;
             ,AIPRONAS=this.w_AIPRONAS;
             ,AIRAGSOC=this.w_AIRAGSOC;
             ,AINATGIU=this.w_AINATGIU;
             ,AISTAEST=this.w_AISTAEST;
             ,AICODEST=this.w_AICODEST;
             ,AINUMIVA=this.w_AINUMIVA;
             ,AICDFSDR=this.w_AICDFSDR;
             ,AICOGSOT=this.w_AICOGSOT;
             ,AINOMSOT=this.w_AINOMSOT;
             ,AISESSOT=this.w_AISESSOT;
             ,AIDTNSST=this.w_AIDTNSST;
             ,AILCNSST=this.w_AILCNSST;
             ,AIPRNSST=this.w_AIPRNSST;
             ,AILCRSST=this.w_AILCRSST;
             ,AIPRRSST=this.w_AIPRRSST;
             ,AICPRSST=this.w_AICPRSST;
             ,AIFRRSST=this.w_AIFRRSST;
             ,AITLRSST=this.w_AITLRSST;
             ,AINUMMOD=this.w_AINUMMOD;
             ,AIFIRDIC=this.w_AIFIRDIC;
             ,AITIPFOR=this.w_AITIPFOR;
             ,AICODINT=this.w_AICODINT;
             ,AICODCAF=this.w_AICODCAF;
             ,AIIMPTRA=this.w_AIIMPTRA;
             ,AIDATIMP=this.w_AIDATIMP;
             ,AIFIRINT=this.w_AIFIRINT;
             ,AINOMFIL=this.w_AINOMFIL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVAGC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGC_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANTIVAGC_IDX,i_nConn)
      *
      * delete ANTIVAGC
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AISERIAL',this.w_AISERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVAGC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAGC_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,15,.t.)
          .link_1_16('Full')
        .DoRTCalc(17,25,.t.)
        if .o_PERIOD<>.w_PERIOD
            .w_AI__MESE = iif(NVL(.w_PERIOD,'M')='M', month(i_DATSYS), 0)
        endif
        if .o_PERIOD<>.w_PERIOD
            .w_AITRIMES = iif(NVL(.w_PERIOD,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        endif
        if .o_PERIOD<>.w_PERIOD
            .w_AIVARPER = ' '
        endif
        if .o_AICOMINT<>.w_AICOMINT
            .w_AICORTER = iif(.w_AICOMINT='S', ' ', .w_AICORTER)
        endif
        if .o_AICORTER<>.w_AICORTER
            .w_AICOMINT = iif(.w_AICORTER='S', ' ', .w_AICOMINT)
        endif
        .DoRTCalc(31,37,.t.)
        if .o_AZPERAZI<>.w_AZPERAZI
            .w_RAPPFIRM = iif(.w_AZPERAZI<>'S',i_CODAZI,' ')
          .link_2_2('Full')
        endif
        .DoRTCalc(39,51,.t.)
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AICFISOT = iif(.w_AZPERAZI<>'S' or .w_Forza2, IIF(EMPTY(.w_RFCODFIS),.w_AZIVACOF,.w_RFCODFIS), space(16))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_RFCODFIS<>.w_RFCODFIS
            .w_AICODCAR = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCODCAR,'1')
        endif
        .DoRTCalc(54,63,.t.)
        if .o_Forza2<>.w_Forza2
            .w_AICDFSDR = space(11)
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AICOGSOT = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCOGNOM,space(24))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AINOMSOT = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RF__NOME,space(20))
        endif
        if .o_Rfcodfis<>.w_Rfcodfis
            .w_AISESSOT = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RF_SESSO,'M')
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AIDTNSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFDATNAS,Ctod('  -  -    '))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AILCNSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCOMNAS,space(40))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AIPRNSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFPRONAS,'  ')
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AILCRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCOMRES,space(40))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AIPRRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFPRORES,'  ')
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AICPRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFCAPRES,space(5))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AIFRRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFINDRES,space(35))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_Rfcodfis<>.w_Rfcodfis
            .w_AITLRSST = iif(.w_AZPERAZI<>'S' and not empty(.w_RFCODFIS),.w_RFTELEFO,space(12))
        endif
        .DoRTCalc(76,79,.t.)
        if .o_AITIPFOR<>.w_AITIPFOR
            .w_AICODINT = iif(.w_AITIPFOR='01', space(16), .w_AICODINT)
        endif
        if .o_AICODINT<>.w_AICODINT
            .w_AICODCAF = iif(EMPTY(.w_AICODINT), space(5), .w_AICODCAF)
        endif
        if .o_AICODINT<>.w_AICODINT
            .w_AIIMPTRA = '1'
        endif
        if .o_AICODINT<>.w_AICODINT
            .w_AIDATIMP = iif(EMPTY(.w_AICODINT), CTOD('  -  -    '), .w_AIDATIMP)
        endif
        if .o_AICODINT<>.w_AICODINT
            .w_AIFIRINT = iif(EMPTY(.w_AICODINT), '0', '1')
        endif
        if .o_AINOMFIL<>.w_AINOMFIL
          .Calculate_ZFEOJBUZPN()
        endif
        if .o_AI__ANNO<>.w_AI__ANNO.or. .o_PERIOD<>.w_PERIOD
          .Calculate_SMYYCIVSJA()
        endif
        if .o_AI__MESE<>.w_AI__MESE.or. .o_AITRIMES<>.w_AITRIMES
          .Calculate_ZGVVYFTIUV()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"AIVGF","i_codazi,w_AISERIAL")
          .op_AISERIAL = .w_AISERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(85,85,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_ZJFMMVWDUN()
    with this
          * --- Inizializzo nome file
          .w_AINOMFIL = left(.w_DirName+'ANTIVA_'+alltrim(i_CODAZI)+'_'+.w_AISERIAL+space(254),254)
    endwith
  endproc
  proc Calculate_ZFEOJBUZPN()
    with this
          * --- Inizializzo nome file
          .w_AINOMFIL = iif(not empty(.w_AINOMFIL), .w_AINOMFIL, left(.w_DirName+'ANTIVA'+alltrim(i_CODAZI)+'_'+.w_AISERIAL+space(254),254))
    endwith
  endproc
  proc Calculate_KHSUCLYDCK()
    with this
          * --- Ripulisci identificativo comunicazione
          GSAI_BGD(this;
             )
    endwith
  endproc
  proc Calculate_SMYYCIVSJA()
    with this
          * --- Calcolo periodo di estrazione
          GSAI_BGP(this;
              ,'PER';
             )
    endwith
  endproc
  proc Calculate_ZGVVYFTIUV()
    with this
          * --- Controllo se periodo gi� estratto
          GSAI_BGP(this;
              ,'CHK';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAI__MESE_1_27.enabled = this.oPgFrm.Page1.oPag.oAI__MESE_1_27.mCond()
    this.oPgFrm.Page1.oPag.oAITRIMES_1_29.enabled = this.oPgFrm.Page1.oPag.oAITRIMES_1_29.mCond()
    this.oPgFrm.Page1.oPag.oAICOGNOM_1_43.enabled = this.oPgFrm.Page1.oPag.oAICOGNOM_1_43.mCond()
    this.oPgFrm.Page2.oPag.oForza2_2_1.enabled = this.oPgFrm.Page2.oPag.oForza2_2_1.mCond()
    this.oPgFrm.Page2.oPag.oAICFISOT_2_16.enabled = this.oPgFrm.Page2.oPag.oAICFISOT_2_16.mCond()
    this.oPgFrm.Page2.oPag.oAICODCAR_2_17.enabled = this.oPgFrm.Page2.oPag.oAICODCAR_2_17.mCond()
    this.oPgFrm.Page1.oPag.oAI__NOME_1_45.enabled = this.oPgFrm.Page1.oPag.oAI__NOME_1_45.mCond()
    this.oPgFrm.Page1.oPag.oAI_SESSO_1_47.enabled_(this.oPgFrm.Page1.oPag.oAI_SESSO_1_47.mCond())
    this.oPgFrm.Page1.oPag.oAIDATNAS_1_49.enabled = this.oPgFrm.Page1.oPag.oAIDATNAS_1_49.mCond()
    this.oPgFrm.Page1.oPag.oAILOCNAS_1_51.enabled = this.oPgFrm.Page1.oPag.oAILOCNAS_1_51.mCond()
    this.oPgFrm.Page1.oPag.oAIPRONAS_1_52.enabled = this.oPgFrm.Page1.oPag.oAIPRONAS_1_52.mCond()
    this.oPgFrm.Page1.oPag.oAIRAGSOC_1_54.enabled = this.oPgFrm.Page1.oPag.oAIRAGSOC_1_54.mCond()
    this.oPgFrm.Page1.oPag.oAINATGIU_1_56.enabled = this.oPgFrm.Page1.oPag.oAINATGIU_1_56.mCond()
    this.oPgFrm.Page2.oPag.oAICDFSDR_2_19.enabled = this.oPgFrm.Page2.oPag.oAICDFSDR_2_19.mCond()
    this.oPgFrm.Page2.oPag.oAICOGSOT_2_21.enabled = this.oPgFrm.Page2.oPag.oAICOGSOT_2_21.mCond()
    this.oPgFrm.Page2.oPag.oAINOMSOT_2_23.enabled = this.oPgFrm.Page2.oPag.oAINOMSOT_2_23.mCond()
    this.oPgFrm.Page2.oPag.oAISESSOT_2_25.enabled_(this.oPgFrm.Page2.oPag.oAISESSOT_2_25.mCond())
    this.oPgFrm.Page2.oPag.oAIDTNSST_2_26.enabled = this.oPgFrm.Page2.oPag.oAIDTNSST_2_26.mCond()
    this.oPgFrm.Page2.oPag.oAILCNSST_2_28.enabled = this.oPgFrm.Page2.oPag.oAILCNSST_2_28.mCond()
    this.oPgFrm.Page2.oPag.oAIPRNSST_2_30.enabled = this.oPgFrm.Page2.oPag.oAIPRNSST_2_30.mCond()
    this.oPgFrm.Page2.oPag.oAILCRSST_2_32.enabled = this.oPgFrm.Page2.oPag.oAILCRSST_2_32.mCond()
    this.oPgFrm.Page2.oPag.oAIPRRSST_2_34.enabled = this.oPgFrm.Page2.oPag.oAIPRRSST_2_34.mCond()
    this.oPgFrm.Page2.oPag.oAICPRSST_2_36.enabled = this.oPgFrm.Page2.oPag.oAICPRSST_2_36.mCond()
    this.oPgFrm.Page2.oPag.oAIFRRSST_2_38.enabled = this.oPgFrm.Page2.oPag.oAIFRRSST_2_38.mCond()
    this.oPgFrm.Page2.oPag.oAITLRSST_2_40.enabled = this.oPgFrm.Page2.oPag.oAITLRSST_2_40.mCond()
    this.oPgFrm.Page3.oPag.oAICODCAF_3_8.enabled = this.oPgFrm.Page3.oPag.oAICODCAF_3_8.mCond()
    this.oPgFrm.Page3.oPag.oAIIMPTRA_3_10.enabled = this.oPgFrm.Page3.oPag.oAIIMPTRA_3_10.mCond()
    this.oPgFrm.Page3.oPag.oAIDATIMP_3_11.enabled = this.oPgFrm.Page3.oPag.oAIDATIMP_3_11.mCond()
    this.oPgFrm.Page3.oPag.oAIFIRINT_3_12.enabled = this.oPgFrm.Page3.oPag.oAIFIRINT_3_12.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_17.enabled = this.oPgFrm.Page3.oPag.oBtn_3_17.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_20.enabled = this.oPgFrm.Page3.oPag.oBtn_3_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPERIOD_1_26.visible=!this.oPgFrm.Page1.oPag.oPERIOD_1_26.mHide()
    this.oPgFrm.Page1.oPag.oAI__MESE_1_27.visible=!this.oPgFrm.Page1.oPag.oAI__MESE_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oAITRIMES_1_29.visible=!this.oPgFrm.Page1.oPag.oAITRIMES_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oAIVARPER_1_31.visible=!this.oPgFrm.Page1.oPag.oAIVARPER_1_31.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("New record")
          .Calculate_ZJFMMVWDUN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete end")
          .Calculate_KHSUCLYDCK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init")
          .Calculate_SMYYCIVSJA()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANTIVAPA_IDX,3]
    i_lTable = "ANTIVAPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2], .t., this.ANTIVAPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAPERIOD";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_CODAZI)
            select PACODAZI,PAPERIOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PACODAZI,space(10))
      this.w_PAPERIOD = NVL(_Link_.PAPERIOD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_PAPERIOD = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.ANTIVAPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPIVAZI,AZCOFAZI,AZPERAZI,AZCODNAZ,AZLOCAZI,AZPROAZI,AZNAGAZI,AZTELEFO,AZ_EMAIL,AZTELFAX,AZIVACOF";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI1)
            select AZCODAZI,AZPIVAZI,AZCOFAZI,AZPERAZI,AZCODNAZ,AZLOCAZI,AZPROAZI,AZNAGAZI,AZTELEFO,AZ_EMAIL,AZTELFAX,AZIVACOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZCODFIS = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZCODNAZ = NVL(_Link_.AZCODNAZ,space(3))
      this.w_AZLOCAZI = NVL(_Link_.AZLOCAZI,space(30))
      this.w_AZPROAZI = NVL(_Link_.AZPROAZI,space(2))
      this.w_AZNAGAZI = NVL(_Link_.AZNAGAZI,space(5))
      this.w_AZTELEFO = NVL(_Link_.AZTELEFO,space(18))
      this.w_AZ_EMAIL = NVL(_Link_.AZ_EMAIL,space(254))
      this.w_AZTELFAX = NVL(_Link_.AZTELFAX,space(18))
      this.w_AZIVACOF = NVL(_Link_.AZIVACOF,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_AZPIVAZI = space(12)
      this.w_AZCODFIS = space(16)
      this.w_AZPERAZI = space(1)
      this.w_AZCODNAZ = space(3)
      this.w_AZLOCAZI = space(30)
      this.w_AZPROAZI = space(2)
      this.w_AZNAGAZI = space(5)
      this.w_AZTELEFO = space(18)
      this.w_AZ_EMAIL = space(254)
      this.w_AZTELFAX = space(18)
      this.w_AZIVACOF = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI2)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT1 = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT1 = NVL(_Link_.TTNOMTIT,space(25))
      this.w_DATNAS1 = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_LOCTIT1 = NVL(_Link_.TTLUONAS,space(30))
      this.w_PROTIT1 = NVL(_Link_.TTPRONAS,space(2))
      this.w_SESSO1 = NVL(_Link_.TT_SESSO,space(1))
      this.w_TELEFO1 = NVL(_Link_.TTTELEFO,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI2 = space(5)
      endif
      this.w_COGTIT1 = space(25)
      this.w_NOMTIT1 = space(25)
      this.w_DATNAS1 = ctod("  /  /  ")
      this.w_LOCTIT1 = space(30)
      this.w_PROTIT1 = space(2)
      this.w_SESSO1 = space(1)
      this.w_TELEFO1 = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAPPFIRM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_RAPP_IDX,3]
    i_lTable = "DAT_RAPP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2], .t., this.DAT_RAPP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAPPFIRM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAPPFIRM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFCODAZI,RFCODFIS,RFCODCAR,RFCOGNOM,RF__NOME,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFCOMRES,RFCAPRES,RFPRORES,RFINDRES,RFTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where RFCODAZI="+cp_ToStrODBC(this.w_RAPPFIRM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFCODAZI',this.w_RAPPFIRM)
            select RFCODAZI,RFCODFIS,RFCODCAR,RFCOGNOM,RF__NOME,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFCOMRES,RFCAPRES,RFPRORES,RFINDRES,RFTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAPPFIRM = NVL(_Link_.RFCODAZI,space(5))
      this.w_RFCODFIS = NVL(_Link_.RFCODFIS,space(16))
      this.w_RFCODCAR = NVL(_Link_.RFCODCAR,space(2))
      this.w_RFCOGNOM = NVL(_Link_.RFCOGNOM,space(40))
      this.w_RF__NOME = NVL(_Link_.RF__NOME,space(40))
      this.w_RF_SESSO = NVL(_Link_.RF_SESSO,space(1))
      this.w_RFDATNAS = NVL(cp_ToDate(_Link_.RFDATNAS),ctod("  /  /  "))
      this.w_RFCOMNAS = NVL(_Link_.RFCOMNAS,space(40))
      this.w_RFPRONAS = NVL(_Link_.RFPRONAS,space(2))
      this.w_RFCOMRES = NVL(_Link_.RFCOMRES,space(40))
      this.w_RFCAPRES = NVL(_Link_.RFCAPRES,space(9))
      this.w_RFPRORES = NVL(_Link_.RFPRORES,space(2))
      this.w_RFINDRES = NVL(_Link_.RFINDRES,space(35))
      this.w_RFTELEFO = NVL(_Link_.RFTELEFO,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_RAPPFIRM = space(5)
      endif
      this.w_RFCODFIS = space(16)
      this.w_RFCODCAR = space(2)
      this.w_RFCOGNOM = space(40)
      this.w_RF__NOME = space(40)
      this.w_RF_SESSO = space(1)
      this.w_RFDATNAS = ctod("  /  /  ")
      this.w_RFCOMNAS = space(40)
      this.w_RFPRONAS = space(2)
      this.w_RFCOMRES = space(40)
      this.w_RFCAPRES = space(9)
      this.w_RFPRORES = space(2)
      this.w_RFINDRES = space(35)
      this.w_RFTELEFO = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])+'\'+cp_ToStr(_Link_.RFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DAT_RAPP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAPPFIRM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAI__ANNO_1_24.value==this.w_AI__ANNO)
      this.oPgFrm.Page1.oPag.oAI__ANNO_1_24.value=this.w_AI__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIOD_1_26.RadioValue()==this.w_PERIOD)
      this.oPgFrm.Page1.oPag.oPERIOD_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAI__MESE_1_27.value==this.w_AI__MESE)
      this.oPgFrm.Page1.oPag.oAI__MESE_1_27.value=this.w_AI__MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oAITRIMES_1_29.value==this.w_AITRIMES)
      this.oPgFrm.Page1.oPag.oAITRIMES_1_29.value=this.w_AITRIMES
    endif
    if not(this.oPgFrm.Page1.oPag.oAIVARPER_1_31.RadioValue()==this.w_AIVARPER)
      this.oPgFrm.Page1.oPag.oAIVARPER_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICORTER_1_32.RadioValue()==this.w_AICORTER)
      this.oPgFrm.Page1.oPag.oAICORTER_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICOMINT_1_33.RadioValue()==this.w_AICOMINT)
      this.oPgFrm.Page1.oPag.oAICOMINT_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPARIVA_1_34.value==this.w_AIPARIVA)
      this.oPgFrm.Page1.oPag.oAIPARIVA_1_34.value=this.w_AIPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODFIS_1_35.value==this.w_AICODFIS)
      this.oPgFrm.Page1.oPag.oAICODFIS_1_35.value=this.w_AICODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oAI__MAIL_1_37.value==this.w_AI__MAIL)
      this.oPgFrm.Page1.oPag.oAI__MAIL_1_37.value=this.w_AI__MAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAITELEFO_1_38.value==this.w_AITELEFO)
      this.oPgFrm.Page1.oPag.oAITELEFO_1_38.value=this.w_AITELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oAINUMFAX_1_39.value==this.w_AINUMFAX)
      this.oPgFrm.Page1.oPag.oAINUMFAX_1_39.value=this.w_AINUMFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oAICOGNOM_1_43.value==this.w_AICOGNOM)
      this.oPgFrm.Page1.oPag.oAICOGNOM_1_43.value=this.w_AICOGNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oForza2_2_1.RadioValue()==this.w_Forza2)
      this.oPgFrm.Page2.oPag.oForza2_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAICFISOT_2_16.value==this.w_AICFISOT)
      this.oPgFrm.Page2.oPag.oAICFISOT_2_16.value=this.w_AICFISOT
    endif
    if not(this.oPgFrm.Page2.oPag.oAICODCAR_2_17.RadioValue()==this.w_AICODCAR)
      this.oPgFrm.Page2.oPag.oAICODCAR_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAI__NOME_1_45.value==this.w_AI__NOME)
      this.oPgFrm.Page1.oPag.oAI__NOME_1_45.value=this.w_AI__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oAI_SESSO_1_47.RadioValue()==this.w_AI_SESSO)
      this.oPgFrm.Page1.oPag.oAI_SESSO_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDATNAS_1_49.value==this.w_AIDATNAS)
      this.oPgFrm.Page1.oPag.oAIDATNAS_1_49.value=this.w_AIDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oAILOCNAS_1_51.value==this.w_AILOCNAS)
      this.oPgFrm.Page1.oPag.oAILOCNAS_1_51.value=this.w_AILOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPRONAS_1_52.value==this.w_AIPRONAS)
      this.oPgFrm.Page1.oPag.oAIPRONAS_1_52.value=this.w_AIPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIRAGSOC_1_54.value==this.w_AIRAGSOC)
      this.oPgFrm.Page1.oPag.oAIRAGSOC_1_54.value=this.w_AIRAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oAINATGIU_1_56.value==this.w_AINATGIU)
      this.oPgFrm.Page1.oPag.oAINATGIU_1_56.value=this.w_AINATGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oAISTAEST_1_58.value==this.w_AISTAEST)
      this.oPgFrm.Page1.oPag.oAISTAEST_1_58.value=this.w_AISTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODEST_1_60.value==this.w_AICODEST)
      this.oPgFrm.Page1.oPag.oAICODEST_1_60.value=this.w_AICODEST
    endif
    if not(this.oPgFrm.Page1.oPag.oAINUMIVA_1_62.value==this.w_AINUMIVA)
      this.oPgFrm.Page1.oPag.oAINUMIVA_1_62.value=this.w_AINUMIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oAICDFSDR_2_19.value==this.w_AICDFSDR)
      this.oPgFrm.Page2.oPag.oAICDFSDR_2_19.value=this.w_AICDFSDR
    endif
    if not(this.oPgFrm.Page2.oPag.oAICOGSOT_2_21.value==this.w_AICOGSOT)
      this.oPgFrm.Page2.oPag.oAICOGSOT_2_21.value=this.w_AICOGSOT
    endif
    if not(this.oPgFrm.Page2.oPag.oAINOMSOT_2_23.value==this.w_AINOMSOT)
      this.oPgFrm.Page2.oPag.oAINOMSOT_2_23.value=this.w_AINOMSOT
    endif
    if not(this.oPgFrm.Page2.oPag.oAISESSOT_2_25.RadioValue()==this.w_AISESSOT)
      this.oPgFrm.Page2.oPag.oAISESSOT_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAIDTNSST_2_26.value==this.w_AIDTNSST)
      this.oPgFrm.Page2.oPag.oAIDTNSST_2_26.value=this.w_AIDTNSST
    endif
    if not(this.oPgFrm.Page2.oPag.oAILCNSST_2_28.value==this.w_AILCNSST)
      this.oPgFrm.Page2.oPag.oAILCNSST_2_28.value=this.w_AILCNSST
    endif
    if not(this.oPgFrm.Page2.oPag.oAIPRNSST_2_30.value==this.w_AIPRNSST)
      this.oPgFrm.Page2.oPag.oAIPRNSST_2_30.value=this.w_AIPRNSST
    endif
    if not(this.oPgFrm.Page2.oPag.oAILCRSST_2_32.value==this.w_AILCRSST)
      this.oPgFrm.Page2.oPag.oAILCRSST_2_32.value=this.w_AILCRSST
    endif
    if not(this.oPgFrm.Page2.oPag.oAIPRRSST_2_34.value==this.w_AIPRRSST)
      this.oPgFrm.Page2.oPag.oAIPRRSST_2_34.value=this.w_AIPRRSST
    endif
    if not(this.oPgFrm.Page2.oPag.oAICPRSST_2_36.value==this.w_AICPRSST)
      this.oPgFrm.Page2.oPag.oAICPRSST_2_36.value=this.w_AICPRSST
    endif
    if not(this.oPgFrm.Page2.oPag.oAIFRRSST_2_38.value==this.w_AIFRRSST)
      this.oPgFrm.Page2.oPag.oAIFRRSST_2_38.value=this.w_AIFRRSST
    endif
    if not(this.oPgFrm.Page2.oPag.oAITLRSST_2_40.value==this.w_AITLRSST)
      this.oPgFrm.Page2.oPag.oAITLRSST_2_40.value=this.w_AITLRSST
    endif
    if not(this.oPgFrm.Page3.oPag.oAINUMMOD_3_2.value==this.w_AINUMMOD)
      this.oPgFrm.Page3.oPag.oAINUMMOD_3_2.value=this.w_AINUMMOD
    endif
    if not(this.oPgFrm.Page3.oPag.oAIFIRDIC_3_3.RadioValue()==this.w_AIFIRDIC)
      this.oPgFrm.Page3.oPag.oAIFIRDIC_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAITIPFOR_3_5.RadioValue()==this.w_AITIPFOR)
      this.oPgFrm.Page3.oPag.oAITIPFOR_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAICODINT_3_6.value==this.w_AICODINT)
      this.oPgFrm.Page3.oPag.oAICODINT_3_6.value=this.w_AICODINT
    endif
    if not(this.oPgFrm.Page3.oPag.oAICODCAF_3_8.value==this.w_AICODCAF)
      this.oPgFrm.Page3.oPag.oAICODCAF_3_8.value=this.w_AICODCAF
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIMPTRA_3_10.RadioValue()==this.w_AIIMPTRA)
      this.oPgFrm.Page3.oPag.oAIIMPTRA_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAIDATIMP_3_11.value==this.w_AIDATIMP)
      this.oPgFrm.Page3.oPag.oAIDATIMP_3_11.value=this.w_AIDATIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oAIFIRINT_3_12.RadioValue()==this.w_AIFIRINT)
      this.oPgFrm.Page3.oPag.oAIFIRINT_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAINOMFIL_3_14.value==this.w_AINOMFIL)
      this.oPgFrm.Page3.oPag.oAINOMFIL_3_14.value=this.w_AINOMFIL
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVAGC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_AINUMMOD>0)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il salvataggio avviene contestualmente alla generazione del file")
          case   (empty(.w_AI__ANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAI__ANNO_1_24.SetFocus()
            i_bnoObbl = !empty(.w_AI__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AI__MESE))  and not(NOT(.w_AI__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAI__MESE_1_27.SetFocus()
            i_bnoObbl = !empty(.w_AI__MESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AITRIMES))  and not(NOT(.w_AITRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAITRIMES_1_29.SetFocus()
            i_bnoObbl = !empty(.w_AITRIMES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AIPARIVA)) or not(CHKCFP(.w_AIPARIVA,"PI", "", "", "")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIPARIVA_1_34.SetFocus()
            i_bnoObbl = !empty(.w_AIPARIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AICODFIS)) or not(chkcfp(alltrim(.w_AICODFIS),'CF')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICODFIS_1_35.SetFocus()
            i_bnoObbl = !empty(.w_AICODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_AITELEFO))=0 and at('/',alltrim(.w_AITELEFO))=0 and at('-',alltrim(.w_AITELEFO))=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAITELEFO_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(at(' ',alltrim(.w_AINUMFAX))=0 and at('/',alltrim(.w_AINUMFAX))=0 and at('-',alltrim(.w_AINUMFAX))=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAINUMFAX_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   (empty(.w_AICOGNOM))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICOGNOM_1_43.SetFocus()
            i_bnoObbl = !empty(.w_AICOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AICFISOT)) or not(iif(not empty(.w_AICFISOT),chkcfp(alltrim(.w_AICFISOT),'CF'),.T.)))  and (.w_AZPERAZI<>'S' or .w_Forza2 or NOT EMPTY(.w_AICFISOT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAICFISOT_2_16.SetFocus()
            i_bnoObbl = !empty(.w_AICFISOT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AICODCAR)) or not(.w_AICODCAR<>'10'))  and (NOT EMPTY(.w_AICFISOT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAICODCAR_2_17.SetFocus()
            i_bnoObbl = !empty(.w_AICODCAR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice carica '10' per il rappresentate firmatario non � ammesso nella generazione del file telematico")
          case   (empty(.w_AI__NOME))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAI__NOME_1_45.SetFocus()
            i_bnoObbl = !empty(.w_AI__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIDATNAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIDATNAS_1_49.SetFocus()
            i_bnoObbl = !empty(.w_AIDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AILOCNAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAILOCNAS_1_51.SetFocus()
            i_bnoObbl = !empty(.w_AILOCNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIRAGSOC))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIRAGSOC_1_54.SetFocus()
            i_bnoObbl = !empty(.w_AIRAGSOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AINATGIU)) or not(.w_AINATGIU>='01' AND .w_AINATGIU<='44' OR .w_AINATGIU>='50' AND .w_AINATGIU<='58'))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAINATGIU_1_56.SetFocus()
            i_bnoObbl = !empty(.w_AINATGIU)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AICOGSOT))  and (NOT EMPTY(.w_AICFISOT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAICOGSOT_2_21.SetFocus()
            i_bnoObbl = !empty(.w_AICOGSOT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AINOMSOT))  and (NOT EMPTY(.w_AICFISOT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAINOMSOT_2_23.SetFocus()
            i_bnoObbl = !empty(.w_AINOMSOT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIDTNSST))  and (NOT EMPTY(.w_AICFISOT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAIDTNSST_2_26.SetFocus()
            i_bnoObbl = !empty(.w_AIDTNSST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AILCNSST))  and (NOT EMPTY(.w_AICFISOT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAILCNSST_2_28.SetFocus()
            i_bnoObbl = !empty(.w_AILCNSST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_AITLRSST))=0 and at('/',alltrim(.w_AITLRSST))=0 and at('-',alltrim(.w_AITLRSST))=0)  and (NOT EMPTY(.w_AICFISOT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAITLRSST_2_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   ((empty(.w_AIFIRDIC)) or not(.w_AIFIRDIC='1'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oAIFIRDIC_3_3.SetFocus()
            i_bnoObbl = !empty(.w_AIFIRDIC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Firma del dichiarante obbligatoria")
          case   not(.w_AITIPFOR='01' OR .w_AITIPFOR='10')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oAITIPFOR_3_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione non consentita con tipo fornitore persona fisica")
          case   not((empty(.w_AICODINT) or chkcfp(alltrim(.w_AICODINT),'CF')) AND (NOT EMPTY(.w_AICODINT) OR .w_AITIPFOR='01'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oAICODINT_3_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fiscale dell'intermediario obbligatorio")
          case   not(EMPTY(.w_AICODINT) OR .w_AIIMPTRA<>'0')  and (NOT EMPTY(.w_AICODINT))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oAIIMPTRA_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AIDATIMP))  and (NOT EMPTY(.w_AICODINT))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oAIDATIMP_3_11.SetFocus()
            i_bnoObbl = !empty(.w_AIDATIMP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AZPERAZI = this.w_AZPERAZI
    this.o_AI__ANNO = this.w_AI__ANNO
    this.o_PERIOD = this.w_PERIOD
    this.o_AI__MESE = this.w_AI__MESE
    this.o_AITRIMES = this.w_AITRIMES
    this.o_AICORTER = this.w_AICORTER
    this.o_AICOMINT = this.w_AICOMINT
    this.o_Forza2 = this.w_Forza2
    this.o_RFCODFIS = this.w_RFCODFIS
    this.o_AITIPFOR = this.w_AITIPFOR
    this.o_AICODINT = this.w_AICODINT
    this.o_AINOMFIL = this.w_AINOMFIL
    return

enddefine

* --- Define pages as container
define class tgsai_agcPag1 as StdContainer
  Width  = 904
  height = 441
  stdWidth  = 904
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAI__ANNO_1_24 as StdField with uid="BFKAZISJJU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_AI__ANNO", cQueryName = "AI__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 1362347,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=128, Top=54, cSayPict='"9999"', cGetPict='"9999"'

  add object oPERIOD_1_26 as StdRadio with uid="LYVLNUYUSV",rtseq=25,rtrep=.f.,left=216, top=44, width=110,height=37;
    , ToolTipText = "Periodicit� adempimento";
    , cFormVar="w_PERIOD", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPERIOD_1_26.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Mensile"
      this.Buttons(1).HelpContextID = 155950346
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Trimestrale"
      this.Buttons(2).HelpContextID = 155950346
      this.Buttons(2).Top=17
      this.SetAll("Width",108)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Periodicit� adempimento")
      StdRadio::init()
    endproc

  func oPERIOD_1_26.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oPERIOD_1_26.GetRadio()
    this.Parent.oContained.w_PERIOD = this.RadioValue()
    return .t.
  endfunc

  func oPERIOD_1_26.SetRadio()
    this.Parent.oContained.w_PERIOD=trim(this.Parent.oContained.w_PERIOD)
    this.value = ;
      iif(this.Parent.oContained.w_PERIOD=='M',1,;
      iif(this.Parent.oContained.w_PERIOD=='T',2,;
      0))
  endfunc

  func oPERIOD_1_26.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oAI__MESE_1_27 as StdField with uid="WZAUIMOPXS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_AI__MESE", cQueryName = "AI__MESE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese di riferimento",;
    HelpContextID = 128661067,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=342, Top=54, cSayPict='"99"', cGetPict='"99"'

  func oAI__MESE_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oAI__MESE_1_27.mHide()
    with this.Parent.oContained
      return (NOT(.w_AI__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))
    endwith
  endfunc

  add object oAITRIMES_1_29 as StdField with uid="ODFXSPRUNK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_AITRIMES", cQueryName = "AITRIMES",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Trimestre di riferimento",;
    HelpContextID = 257787481,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=398, Top=54, cSayPict='"9"', cGetPict='"9"'

  func oAITRIMES_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oAITRIMES_1_29.mHide()
    with this.Parent.oContained
      return (NOT(.w_AITRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))
    endwith
  endfunc

  add object oAIVARPER_1_31 as StdCheck with uid="SHCXJVPCYR",rtseq=28,rtrep=.f.,left=504, top=55, caption="Variazione di periodicit�",;
    ToolTipText = "Variazione di periodicit�",;
    HelpContextID = 48014936,;
    cFormVar="w_AIVARPER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAIVARPER_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAIVARPER_1_31.GetRadio()
    this.Parent.oContained.w_AIVARPER = this.RadioValue()
    return .t.
  endfunc

  func oAIVARPER_1_31.SetRadio()
    this.Parent.oContained.w_AIVARPER=trim(this.Parent.oContained.w_AIVARPER)
    this.value = ;
      iif(this.Parent.oContained.w_AIVARPER=='S',1,;
      0)
  endfunc

  func oAIVARPER_1_31.mHide()
    with this.Parent.oContained
      return (.w_PERIOD<>'M' OR .cFunction='Edit' AND .w_AIVARPER<>'S')
    endwith
  endfunc

  add object oAICORTER_1_32 as StdCheck with uid="OHMUXPSCPI",rtseq=29,rtrep=.f.,left=504, top=78, caption="Correttiva nei termini",;
    ToolTipText = "Correttiva nei termini",;
    HelpContextID = 115963480,;
    cFormVar="w_AICORTER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAICORTER_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAICORTER_1_32.GetRadio()
    this.Parent.oContained.w_AICORTER = this.RadioValue()
    return .t.
  endfunc

  func oAICORTER_1_32.SetRadio()
    this.Parent.oContained.w_AICORTER=trim(this.Parent.oContained.w_AICORTER)
    this.value = ;
      iif(this.Parent.oContained.w_AICORTER=='S',1,;
      0)
  endfunc

  add object oAICOMINT_1_33 as StdCheck with uid="XTZIJJTBQC",rtseq=30,rtrep=.f.,left=704, top=78, caption="Comunicazione integrativa",;
    ToolTipText = "Comunicazione integrativa",;
    HelpContextID = 73828774,;
    cFormVar="w_AICOMINT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAICOMINT_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAICOMINT_1_33.GetRadio()
    this.Parent.oContained.w_AICOMINT = this.RadioValue()
    return .t.
  endfunc

  func oAICOMINT_1_33.SetRadio()
    this.Parent.oContained.w_AICOMINT=trim(this.Parent.oContained.w_AICOMINT)
    this.value = ;
      iif(this.Parent.oContained.w_AICOMINT=='S',1,;
      0)
  endfunc

  add object oAIPARIVA_1_34 as StdField with uid="JPNFHTXPJM",rtseq=31,rtrep=.f.,;
    cFormVar = "w_AIPARIVA", cQueryName = "AIPARIVA",;
    bObbl = .t. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA del contribuente",;
    HelpContextID = 198985287,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=216, Top=132, cSayPict='REPLICATE("!",11)', cGetPict='REPLICATE("!",11)', InputMask=replicate('X',11)

  func oAIPARIVA_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AIPARIVA,"PI", "", "", ""))
    endwith
    return bRes
  endfunc

  add object oAICODFIS_1_35 as StdField with uid="DFQWHMDFGZ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_AICODFIS", cQueryName = "AICODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del contribuente",;
    HelpContextID = 134837849,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=464, Top=132, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oAICODFIS_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chkcfp(alltrim(.w_AICODFIS),'CF'))
    endwith
    return bRes
  endfunc

  add object oAI__MAIL_1_37 as StdField with uid="MTZLJHHSMZ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_AI__MAIL", cQueryName = "AI__MAIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo mail del contribuente",;
    HelpContextID = 61552210,;
   bGlobalFont=.t.,;
    Height=21, Width=479, Left=128, Top=178, cSayPict="replicate('!', 100)", cGetPict="replicate('!', 100)", InputMask=replicate('X',100)

  add object oAITELEFO_1_38 as StdField with uid="MUYEBPTRWW",rtseq=34,rtrep=.f.,;
    cFormVar = "w_AITELEFO", cQueryName = "AITELEFO",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono o cellulare del contribuente",;
    HelpContextID = 125863509,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=610, Top=178, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oAITELEFO_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_AITELEFO))=0 and at('/',alltrim(.w_AITELEFO))=0 and at('-',alltrim(.w_AITELEFO))=0)
    endwith
    return bRes
  endfunc

  add object oAINUMFAX_1_39 as StdField with uid="WSQUUGFPOX",rtseq=35,rtrep=.f.,;
    cFormVar = "w_AINUMFAX", cQueryName = "AINUMFAX",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di fax del contribuente",;
    HelpContextID = 123722146,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=753, Top=178, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oAINUMFAX_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_AINUMFAX))=0 and at('/',alltrim(.w_AINUMFAX))=0 and at('-',alltrim(.w_AINUMFAX))=0)
    endwith
    return bRes
  endfunc

  add object oAICOGNOM_1_43 as StdField with uid="USTYABUOUZ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_AICOGNOM", cQueryName = "AICOGNOM",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 264669613,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=128, Top=226, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oAICOGNOM_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAI__NOME_1_45 as StdField with uid="LKFBHLXRZV",rtseq=54,rtrep=.f.,;
    cFormVar = "w_AI__NOME", cQueryName = "AI__NOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 239389109,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=435, Top=226, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oAI__NOME_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAI_SESSO_1_47 as StdRadio with uid="FQZPXYVUYR",rtseq=55,rtrep=.f.,left=744, top=226, width=153,height=23;
    , ToolTipText = "Sesso";
    , cFormVar="w_AI_SESSO", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oAI_SESSO_1_47.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Maschio"
      this.Buttons(1).HelpContextID = 85931605
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Maschio","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Femmina"
      this.Buttons(2).HelpContextID = 85931605
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Femmina","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Sesso")
      StdRadio::init()
    endproc

  func oAI_SESSO_1_47.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAI_SESSO_1_47.GetRadio()
    this.Parent.oContained.w_AI_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oAI_SESSO_1_47.SetRadio()
    this.Parent.oContained.w_AI_SESSO=trim(this.Parent.oContained.w_AI_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_AI_SESSO=='M',1,;
      iif(this.Parent.oContained.w_AI_SESSO=='F',2,;
      0))
  endfunc

  func oAI_SESSO_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAIDATNAS_1_49 as StdField with uid="ECDVVXPNAO",rtseq=56,rtrep=.f.,;
    cFormVar = "w_AIDATNAS", cQueryName = "AIDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 16483929,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=128, Top=267

  func oAIDATNAS_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAILOCNAS_1_51 as StdField with uid="UCAJUOVOUI",rtseq=57,rtrep=.f.,;
    cFormVar = "w_AILOCNAS", cQueryName = "AILOCNAS",;
    bObbl = .t. , nPag = 1, value=space(79), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 391591,;
   bGlobalFont=.t.,;
    Height=21, Width=562, Left=233, Top=267, cSayPict="REPL('!',79)", cGetPict="REPL('!',79)", InputMask=replicate('X',79), bHasZoom = .t. 

  func oAILOCNAS_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oAILOCNAS_1_51.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_AILOCNAS",".w_AIPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIPRONAS_1_52 as StdField with uid="HHSQKKMXTC",rtseq=58,rtrep=.f.,;
    cFormVar = "w_AIPRONAS", cQueryName = "AIPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 12404313,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=823, Top=267, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oAIPRONAS_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oAIPRONAS_1_52.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_AIPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIRAGSOC_1_54 as StdField with uid="QITJWPXNAA",rtseq=59,rtrep=.f.,;
    cFormVar = "w_AIRAGSOC", cQueryName = "AIRAGSOC",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione o ragione sociale",;
    HelpContextID = 181639607,;
   bGlobalFont=.t.,;
    Height=21, Width=683, Left=128, Top=335, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oAIRAGSOC_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  add object oAINATGIU_1_56 as StdField with uid="YCLEQVMJZT",rtseq=60,rtrep=.f.,;
    cFormVar = "w_AINATGIU", cQueryName = "AINATGIU",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Natura giuridica",;
    HelpContextID = 167519835,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=826, Top=334, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oAINATGIU_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  func oAINATGIU_1_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AINATGIU>='01' AND .w_AINATGIU<='44' OR .w_AINATGIU>='50' AND .w_AINATGIU<='58')
    endwith
    return bRes
  endfunc

  add object oAISTAEST_1_58 as StdField with uid="PCRWZLCYGL",rtseq=61,rtrep=.f.,;
    cFormVar = "w_AISTAEST", cQueryName = "AISTAEST",;
    bObbl = .f. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato estero di residenza",;
    HelpContextID = 115308122,;
   bGlobalFont=.t.,;
    Height=21, Width=441, Left=128, Top=406, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  add object oAICODEST_1_60 as StdField with uid="ENFMCWDFRB",rtseq=62,rtrep=.f.,;
    cFormVar = "w_AICODEST", cQueryName = "AICODEST",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 118060634,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=575, Top=406, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  add object oAINUMIVA_1_62 as StdField with uid="VZPTGJOVDE",rtseq=63,rtrep=.f.,;
    cFormVar = "w_AINUMIVA", cQueryName = "AINUMIVA",;
    bObbl = .f. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Numero di identificazione IVA stato estero",;
    HelpContextID = 195044935,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=668, Top=406, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  add object oStr_1_25 as StdString with uid="TSXMGJAOXV",Visible=.t., Left=128, Top=40,;
    Alignment=2, Width=41, Height=18,;
    Caption="Anno"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="RVUTHYHKQX",Visible=.t., Left=334, Top=40,;
    Alignment=2, Width=35, Height=18,;
    Caption="Mese"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (NOT(.w_AI__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="UNBIODXVFL",Visible=.t., Left=384, Top=40,;
    Alignment=2, Width=56, Height=18,;
    Caption="Trimestre"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (NOT(.w_AITRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="IXLIJYIGMO",Visible=.t., Left=128, Top=136,;
    Alignment=1, Width=84, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="DIRMDFWZQS",Visible=.t., Left=128, Top=162,;
    Alignment=0, Width=165, Height=18,;
    Caption="Indirizzo di posta elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="HEUTCFKDNL",Visible=.t., Left=233, Top=248,;
    Alignment=0, Width=193, Height=18,;
    Caption="Comune (o stato estero) di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="BSANYZJWZM",Visible=.t., Left=753, Top=162,;
    Alignment=2, Width=139, Height=18,;
    Caption="FAX"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="OSGCYPTITY",Visible=.t., Left=128, Top=210,;
    Alignment=1, Width=58, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="LGSUOYWZDY",Visible=.t., Left=435, Top=210,;
    Alignment=0, Width=37, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="JOPERGGNNX",Visible=.t., Left=796, Top=207,;
    Alignment=0, Width=39, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="CNJREOUFHV",Visible=.t., Left=128, Top=248,;
    Alignment=2, Width=85, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="GTAPKTSGGM",Visible=.t., Left=785, Top=248,;
    Alignment=2, Width=109, Height=18,;
    Caption="Provincia di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="BOGQGLFCGC",Visible=.t., Left=128, Top=319,;
    Alignment=0, Width=188, Height=18,;
    Caption="Denominazione o ragione sociale"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="JPERFVQIEA",Visible=.t., Left=792, Top=315,;
    Alignment=2, Width=90, Height=18,;
    Caption="Natura giuridica"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="MSRCTUGDJC",Visible=.t., Left=128, Top=387,;
    Alignment=0, Width=139, Height=18,;
    Caption="Stato estero di residenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="QHYUXLEBQT",Visible=.t., Left=545, Top=387,;
    Alignment=0, Width=110, Height=18,;
    Caption="Codice stato estero"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="OWPHCRYLRA",Visible=.t., Left=659, Top=387,;
    Alignment=2, Width=232, Height=18,;
    Caption="Numero di identificazione IVA stato estero"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=7, Top=112,;
    Alignment=0, Width=198, Height=19,;
    Caption="Dati del contribuente"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_67 as StdString with uid="FMQSXZQXJQ",Visible=.t., Left=7, Top=13,;
    Alignment=0, Width=222, Height=19,;
    Caption="Periodo di riferimento"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_68 as StdString with uid="KMVBUPVSTE",Visible=.t., Left=610, Top=162,;
    Alignment=0, Width=139, Height=18,;
    Caption="Telefono o cellulare"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="BQYEOWHSUJ",Visible=.t., Left=7, Top=210,;
    Alignment=0, Width=88, Height=18,;
    Caption="Persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="KWZECAJTFT",Visible=.t., Left=7, Top=296,;
    Alignment=0, Width=263, Height=18,;
    Caption="Soggetti diversi dalle persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="KOBLZVTXTN",Visible=.t., Left=7, Top=367,;
    Alignment=0, Width=190, Height=18,;
    Caption="Soggetti non residenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="MSUZFRVMFD",Visible=.t., Left=379, Top=136,;
    Alignment=0, Width=81, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oBox_1_64 as StdBox with uid="RDMWYOEEFE",left=1, top=104, width=900,height=337

  add object oBox_1_66 as StdBox with uid="GEMAGDKVQQ",left=1, top=7, width=900,height=99

  add object oBox_1_72 as StdBox with uid="WMHLBIJLHD",left=119, top=205, width=782,height=2

  add object oBox_1_73 as StdBox with uid="RQGVVXVVOU",left=119, top=292, width=782,height=2

  add object oBox_1_74 as StdBox with uid="KFECAWBOQD",left=119, top=362, width=782,height=2
enddefine
define class tgsai_agcPag2 as StdContainer
  Width  = 904
  height = 441
  stdWidth  = 904
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oForza2_2_1 as StdCheck with uid="ANPGYPQFYE",rtseq=37,rtrep=.f.,left=798, top=10, caption="Forza editing",;
    HelpContextID = 167277482,;
    cFormVar="w_Forza2", bObbl = .f. , nPag = 2;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oForza2_2_1.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza2_2_1.GetRadio()
    this.Parent.oContained.w_Forza2 = this.RadioValue()
    return .t.
  endfunc

  func oForza2_2_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza2==.t.,1,;
      0)
  endfunc

  func oForza2_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAICFISOT_2_16 as StdField with uid="VAOMXTNVDK",rtseq=52,rtrep=.f.,;
    cFormVar = "w_AICFISOT", cQueryName = "AICFISOT",;
    bObbl = .t. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del sottoscrittore",;
    HelpContextID = 179276198,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=103, Top=91, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oAICFISOT_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S' or .w_Forza2 or NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  func oAICFISOT_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_AICFISOT),chkcfp(alltrim(.w_AICFISOT),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oAICODCAR_2_17 as StdCombo with uid="YYMESQRZRO",rtseq=53,rtrep=.f.,left=299,top=91,width=286,height=21;
    , ToolTipText = "Codice carica";
    , HelpContextID = 183929256;
    , cFormVar="w_AICODCAR",RowSource=""+""+l_descri1+","+""+l_descri2+","+""+l_descri3+","+""+l_descri4+","+""+l_descri5+","+""+l_descri6+","+""+l_descri7+","+""+l_descri8+","+""+l_descri9+","+""+l_descri10+","+""+l_descri11+","+""+l_descri12+","+""+l_descri13+","+""+l_descri14+","+""+l_descri15+"", bObbl = .t. , nPag = 2;
    , sErrorMsg = "Il codice carica '10' per il rappresentate firmatario non � ammesso nella generazione del file telematico";
  , bGlobalFont=.t.


  func oAICODCAR_2_17.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'10',;
    iif(this.value =11,'11',;
    iif(this.value =12,'12',;
    iif(this.value =13,'13',;
    iif(this.value =14,'14',;
    iif(this.value =15,'15',;
    '  '))))))))))))))))
  endfunc
  func oAICODCAR_2_17.GetRadio()
    this.Parent.oContained.w_AICODCAR = this.RadioValue()
    return .t.
  endfunc

  func oAICODCAR_2_17.SetRadio()
    this.Parent.oContained.w_AICODCAR=trim(this.Parent.oContained.w_AICODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_AICODCAR=='1',1,;
      iif(this.Parent.oContained.w_AICODCAR=='2',2,;
      iif(this.Parent.oContained.w_AICODCAR=='3',3,;
      iif(this.Parent.oContained.w_AICODCAR=='4',4,;
      iif(this.Parent.oContained.w_AICODCAR=='5',5,;
      iif(this.Parent.oContained.w_AICODCAR=='6',6,;
      iif(this.Parent.oContained.w_AICODCAR=='7',7,;
      iif(this.Parent.oContained.w_AICODCAR=='8',8,;
      iif(this.Parent.oContained.w_AICODCAR=='9',9,;
      iif(this.Parent.oContained.w_AICODCAR=='10',10,;
      iif(this.Parent.oContained.w_AICODCAR=='11',11,;
      iif(this.Parent.oContained.w_AICODCAR=='12',12,;
      iif(this.Parent.oContained.w_AICODCAR=='13',13,;
      iif(this.Parent.oContained.w_AICODCAR=='14',14,;
      iif(this.Parent.oContained.w_AICODCAR=='15',15,;
      0)))))))))))))))
  endfunc

  func oAICODCAR_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  func oAICODCAR_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AICODCAR<>'10')
    endwith
    return bRes
  endfunc

  add object oAICDFSDR_2_19 as StdField with uid="HZHHVCUOGN",rtseq=64,rtrep=.f.,;
    cFormVar = "w_AICDFSDR", cQueryName = "AICDFSDR",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale societ� dichiarante",;
    HelpContextID = 85882456,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=617, Top=91, cSayPict='REPLICATE("!",11)', cGetPict='REPLICATE("!",11)', InputMask=replicate('X',11)

  func oAICDFSDR_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  add object oAICOGSOT_2_21 as StdField with uid="RXOMZIHAKM",rtseq=65,rtrep=.f.,;
    cFormVar = "w_AICOGSOT", cQueryName = "AICOGSOT",;
    bObbl = .t. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome dichiarante diverso",;
    HelpContextID = 180783526,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=103, Top=160, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oAICOGSOT_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  add object oAINOMSOT_2_23 as StdField with uid="JCXFNSENGE",rtseq=66,rtrep=.f.,;
    cFormVar = "w_AINOMSOT", cQueryName = "AINOMSOT",;
    bObbl = .t. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome dichiarante diverso",;
    HelpContextID = 174447014,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=411, Top=160, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oAINOMSOT_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  add object oAISESSOT_2_25 as StdRadio with uid="ONWZPPROSI",rtseq=67,rtrep=.f.,left=743, top=160, width=163,height=23;
    , ToolTipText = "Sesso dichiarente diverso";
    , cFormVar="w_AISESSOT", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oAISESSOT_2_25.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Maschio"
      this.Buttons(1).HelpContextID = 168790438
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Maschio","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Femmina"
      this.Buttons(2).HelpContextID = 168790438
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Femmina","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Sesso dichiarente diverso")
      StdRadio::init()
    endproc

  func oAISESSOT_2_25.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAISESSOT_2_25.GetRadio()
    this.Parent.oContained.w_AISESSOT = this.RadioValue()
    return .t.
  endfunc

  func oAISESSOT_2_25.SetRadio()
    this.Parent.oContained.w_AISESSOT=trim(this.Parent.oContained.w_AISESSOT)
    this.value = ;
      iif(this.Parent.oContained.w_AISESSOT=='M',1,;
      iif(this.Parent.oContained.w_AISESSOT=='F',2,;
      0))
  endfunc

  func oAISESSOT_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  add object oAIDTNSST_2_26 as StdField with uid="BGQJMZVANZ",rtseq=68,rtrep=.f.,;
    cFormVar = "w_AIDTNSST", cQueryName = "AIDTNSST",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita dichiarante diverso",;
    HelpContextID = 95323738,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=103, Top=233

  func oAIDTNSST_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  add object oAILCNSST_2_28 as StdField with uid="MEMPOPBQLG",rtseq=69,rtrep=.f.,;
    cFormVar = "w_AILCNSST", cQueryName = "AILCNSST",;
    bObbl = .t. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita dichiarante diverso",;
    HelpContextID = 94242394,;
   bGlobalFont=.t.,;
    Height=21, Width=646, Left=198, Top=233, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oAILCNSST_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  proc oAILCNSST_2_28.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_AILCNSST",".w_AIPRNSST")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIPRNSST_2_30 as StdField with uid="ZSQNJWKRWC",rtseq=70,rtrep=.f.,;
    cFormVar = "w_AIPRNSST", cQueryName = "AIPRNSST",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita dichiarante diverso",;
    HelpContextID = 95241818,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=846, Top=233, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oAIPRNSST_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  proc oAIPRNSST_2_30.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_AIPRNSST")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAILCRSST_2_32 as StdField with uid="HDONDWBDBZ",rtseq=71,rtrep=.f.,;
    cFormVar = "w_AILCRSST", cQueryName = "AILCRSST",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di residenza dichiarante diverso",;
    HelpContextID = 98436698,;
   bGlobalFont=.t.,;
    Height=21, Width=621, Left=103, Top=322, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oAILCRSST_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  proc oAILCRSST_2_32.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AICPRSST",".w_AILCRSST",".w_AIPRRSST")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIPRRSST_2_34 as StdField with uid="GLXRBDGYCW",rtseq=72,rtrep=.f.,;
    cFormVar = "w_AIPRRSST", cQueryName = "AIPRRSST",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di residenza dichiarante diverso",;
    HelpContextID = 99436122,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=733, Top=322, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oAIPRRSST_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  proc oAIPRRSST_2_34.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_AIPRRSST")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAICPRSST_2_36 as StdField with uid="ZDSCFSPQEV",rtseq=73,rtrep=.f.,;
    cFormVar = "w_AICPRSST", cQueryName = "AICPRSST",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "CAP di residenza dichiarante diverso",;
    HelpContextID = 99251802,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=793, Top=322, cSayPict='"99999"', cGetPict='"99999"', InputMask=replicate('X',5), bHasZoom = .t. 

  func oAICPRSST_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  proc oAICPRSST_2_36.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AICPRSST",".w_AILCRSST",".w_AIPRRSST")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIFRRSST_2_38 as StdField with uid="RBRQOLOYDQ",rtseq=74,rtrep=.f.,;
    cFormVar = "w_AIFRRSST", cQueryName = "AIFRRSST",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Frazione, via e numero civico residenza dichiarante diverso",;
    HelpContextID = 99395162,;
   bGlobalFont=.t.,;
    Height=21, Width=621, Left=103, Top=400, cSayPict="REPL('!',35)", cGetPict="REPL('!',35)", InputMask=replicate('X',35)

  func oAIFRRSST_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  add object oAITLRSST_2_40 as StdField with uid="TSEHBDKVQM",rtseq=75,rtrep=.f.,;
    cFormVar = "w_AITLRSST", cQueryName = "AITLRSST",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Telefono dichiarante diverso",;
    HelpContextID = 99059290,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=737, Top=400, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oAITLRSST_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICFISOT))
    endwith
   endif
  endfunc

  func oAITLRSST_2_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_AITLRSST))=0 and at('/',alltrim(.w_AITLRSST))=0 and at('-',alltrim(.w_AITLRSST))=0)
    endwith
    return bRes
  endfunc

  add object oStr_2_18 as StdString with uid="FNFBPKWGUY",Visible=.t., Left=103, Top=73,;
    Alignment=0, Width=175, Height=18,;
    Caption="Codice fiscale del sottoscrittore"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="ZCIUYIIKHM",Visible=.t., Left=617, Top=73,;
    Alignment=0, Width=188, Height=18,;
    Caption="Codice fiscale societ� dichiarante"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="PPEHDYKWYQ",Visible=.t., Left=103, Top=142,;
    Alignment=0, Width=165, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="JFZOCGHHIH",Visible=.t., Left=411, Top=143,;
    Alignment=0, Width=144, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="WXSCBRPDLD",Visible=.t., Left=103, Top=215,;
    Alignment=0, Width=92, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="ZDPVXMBGXY",Visible=.t., Left=198, Top=215,;
    Alignment=0, Width=300, Height=18,;
    Caption="Comune (o stato estero) di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="UAQHHMUPJS",Visible=.t., Left=840, Top=215,;
    Alignment=0, Width=54, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="KEHOTQNQWV",Visible=.t., Left=103, Top=294,;
    Alignment=0, Width=314, Height=18,;
    Caption="Comune (o stato estero) di residenza"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="PJXYWSBIWK",Visible=.t., Left=727, Top=294,;
    Alignment=0, Width=65, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="GLZFVMHRWB",Visible=.t., Left=793, Top=294,;
    Alignment=0, Width=61, Height=18,;
    Caption="CAP"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="QDOTRWZQTG",Visible=.t., Left=103, Top=382,;
    Alignment=0, Width=324, Height=18,;
    Caption="Frazione, via e numero civico residenza"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="ENTNUJHRKK",Visible=.t., Left=732, Top=382,;
    Alignment=0, Width=157, Height=18,;
    Caption="Telefono o cellulare"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="HAGPFVSWCH",Visible=.t., Left=5, Top=42,;
    Alignment=0, Width=284, Height=19,;
    Caption="Dichiarante diverso dal contribuente"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_44 as StdString with uid="FANRLAONBU",Visible=.t., Left=800, Top=143,;
    Alignment=0, Width=39, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="LFXDIKGIJH",Visible=.t., Left=299, Top=73,;
    Alignment=0, Width=175, Height=18,;
    Caption="Codice carica"  ;
  , bGlobalFont=.t.

  add object oBox_2_43 as StdBox with uid="FKHDCRQNTU",left=0, top=33, width=899,height=406

  add object oBox_2_45 as StdBox with uid="MJVFLEHPAA",left=98, top=137, width=801,height=2

  add object oBox_2_46 as StdBox with uid="WQFMYGWVZQ",left=98, top=283, width=801,height=2
enddefine
define class tgsai_agcPag3 as StdContainer
  Width  = 904
  height = 441
  stdWidth  = 904
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAINUMMOD_3_2 as StdField with uid="GWWKVWAVVN",rtseq=77,rtrep=.f.,;
    cFormVar = "w_AINUMMOD", cQueryName = "AINUMMOD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di moduli presentati",;
    HelpContextID = 6281654,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=354, Top=41, cSayPict='"99999999"', cGetPict='"99999999"'

  add object oAIFIRDIC_3_3 as StdCheck with uid="PJOQBGGUFU",rtseq=78,rtrep=.f.,left=540, top=41, caption="Firma del dichiarante",;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 115582537,;
    cFormVar="w_AIFIRDIC", bObbl = .t. , nPag = 3;
    ,sErrorMsg = "Firma del dichiarante obbligatoria";
   , bGlobalFont=.t.


  func oAIFIRDIC_3_3.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIFIRDIC_3_3.GetRadio()
    this.Parent.oContained.w_AIFIRDIC = this.RadioValue()
    return .t.
  endfunc

  func oAIFIRDIC_3_3.SetRadio()
    this.Parent.oContained.w_AIFIRDIC=trim(this.Parent.oContained.w_AIFIRDIC)
    this.value = ;
      iif(this.Parent.oContained.w_AIFIRDIC=='1',1,;
      0)
  endfunc

  func oAIFIRDIC_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AIFIRDIC='1')
    endwith
    return bRes
  endfunc


  add object oAITIPFOR_3_5 as StdCombo with uid="TMIIFPZQPN",rtseq=79,rtrep=.f.,left=354,top=121,width=286,height=22;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 121338280;
    , cFormVar="w_AITIPFOR",RowSource=""+"01: Soggetti che inviano le dichiarazioni,"+"10: C.A.F. dip. e pens C.A.F. imp art3 C 2 altri int.", bObbl = .f. , nPag = 3;
    , sErrorMsg = "Selezione non consentita con tipo fornitore persona fisica";
  , bGlobalFont=.t.


  func oAITIPFOR_3_5.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'10',;
    space(2))))
  endfunc
  func oAITIPFOR_3_5.GetRadio()
    this.Parent.oContained.w_AITIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oAITIPFOR_3_5.SetRadio()
    this.Parent.oContained.w_AITIPFOR=trim(this.Parent.oContained.w_AITIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_AITIPFOR=='01',1,;
      iif(this.Parent.oContained.w_AITIPFOR=='10',2,;
      0))
  endfunc

  func oAITIPFOR_3_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AITIPFOR='01' OR .w_AITIPFOR='10')
    endwith
    return bRes
  endfunc

  add object oAICODINT_3_6 as StdField with uid="VHBKITNWRO",rtseq=80,rtrep=.f.,;
    cFormVar = "w_AICODINT", cQueryName = "AICODINT",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fiscale dell'intermediario obbligatorio",;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 83265958,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=354, Top=153, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oAICODINT_3_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_AICODINT) or chkcfp(alltrim(.w_AICODINT),'CF')) AND (NOT EMPTY(.w_AICODINT) OR .w_AITIPFOR='01'))
    endwith
    return bRes
  endfunc

  add object oAICODCAF_3_8 as StdField with uid="FCYYNVPVIH",rtseq=81,rtrep=.f.,;
    cFormVar = "w_AICODCAF", cQueryName = "AICODCAF",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Numero di iscrizione all'albo del C.A.F.",;
    HelpContextID = 183929268,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=774, Top=153, cSayPict='"99999"', cGetPict='"99999"', InputMask=replicate('X',5)

  func oAICODCAF_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODINT))
    endwith
   endif
  endfunc


  add object oAIIMPTRA_3_10 as StdCombo with uid="JKQBQAAXZG",rtseq=82,rtrep=.f.,left=354,top=192,width=286,height=22;
    , ToolTipText = "Impegno a trasmettere in via telematica la comunicazione";
    , HelpContextID = 113759815;
    , cFormVar="w_AIIMPTRA",RowSource=""+"1 - Dichiarazione predisposta dal contribuente,"+"2 - Dichiarazione predisposta da chi effettua l'invio", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oAIIMPTRA_3_10.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(1))))
  endfunc
  func oAIIMPTRA_3_10.GetRadio()
    this.Parent.oContained.w_AIIMPTRA = this.RadioValue()
    return .t.
  endfunc

  func oAIIMPTRA_3_10.SetRadio()
    this.Parent.oContained.w_AIIMPTRA=trim(this.Parent.oContained.w_AIIMPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_AIIMPTRA=='1',1,;
      iif(this.Parent.oContained.w_AIIMPTRA=='2',2,;
      0))
  endfunc

  func oAIIMPTRA_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODINT))
    endwith
   endif
  endfunc

  func oAIIMPTRA_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AICODINT) OR .w_AIIMPTRA<>'0')
    endwith
    return bRes
  endfunc

  add object oAIDATIMP_3_11 as StdField with uid="SYRFGCWSSA",rtseq=83,rtrep=.f.,;
    cFormVar = "w_AIDATIMP", cQueryName = "AIDATIMP",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'impegno",;
    HelpContextID = 67402154,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=354, Top=232

  func oAIDATIMP_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODINT))
    endwith
   endif
  endfunc

  add object oAIFIRINT_3_12 as StdCheck with uid="EXANRFONVC",rtseq=84,rtrep=.f.,left=540, top=232, caption="Firma dell'intermediario",;
    ToolTipText = "Firma dell'intermediario",;
    HelpContextID = 68966822,;
    cFormVar="w_AIFIRINT", bObbl = .f. , nPag = 3;
    ,sErrorMsg = "Firma dell'intermediario obbligatoria";
   , bGlobalFont=.t.


  func oAIFIRINT_3_12.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIFIRINT_3_12.GetRadio()
    this.Parent.oContained.w_AIFIRINT = this.RadioValue()
    return .t.
  endfunc

  func oAIFIRINT_3_12.SetRadio()
    this.Parent.oContained.w_AIFIRINT=trim(this.Parent.oContained.w_AIFIRINT)
    this.value = ;
      iif(this.Parent.oContained.w_AIFIRINT=='1',1,;
      0)
  endfunc

  func oAIFIRINT_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODINT))
    endwith
   endif
  endfunc

  add object oAINOMFIL_3_14 as StdField with uid="VPKTICWKHO",rtseq=85,rtrep=.f.,;
    cFormVar = "w_AINOMFIL", cQueryName = "AINOMFIL",;
    bObbl = .f. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file e percorso per inoltro telematico",;
    HelpContextID = 144320082,;
   bGlobalFont=.t.,;
    Height=21, Width=706, Left=128, Top=340, InputMask=replicate('X',254)


  add object oBtn_3_16 as StdButton with uid="TQEMIMWBDV",left=843, top=340, width=21,height=20,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file";
    , HelpContextID = 42399274;
  , bGlobalFont=.t.

    proc oBtn_3_16.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_17 as StdButton with uid="AYLATODQCL",left=816, top=390, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=3;
    , ToolTipText = "Genera file";
    , HelpContextID = 48345622;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_17.Click()
      with this.Parent.oContained
        do ..\BLACKBOX\GSAI_BGC with this.Parent.oContained,"GENERA"
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AI__ANNO) and not empty(.w_AINOMFIL) and not g_DEMO)
      endwith
    endif
  endfunc


  add object oBtn_3_20 as StdButton with uid="LSDYMVQXJB",left=762, top=390, width=48,height=45,;
    CpPicture="bmp\stampa.bmp", caption="", nPag=3;
    , ToolTipText = "Stampa modelli";
    , HelpContextID = 48345622;
    , caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_20.Click()
      with this.Parent.oContained
        do ..\BLACKBOX\GSAI_BGC with this.Parent.oContained, "STAMPA"
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AINUMMOD<>0 and not empty(.w_AI__ANNO) and not empty(.w_AINOMFIL) and not g_DEMO)
      endwith
    endif
  endfunc

  add object oStr_3_4 as StdString with uid="FUOMFIFISI",Visible=.t., Left=193, Top=45,;
    Alignment=1, Width=160, Height=18,;
    Caption="Numero di moduli presentati:"  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="NECDVAUAQY",Visible=.t., Left=133, Top=157,;
    Alignment=1, Width=220, Height=18,;
    Caption="Codice fiscale dell'intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="ABCTFAMFTH",Visible=.t., Left=559, Top=157,;
    Alignment=1, Width=214, Height=18,;
    Caption="N. di iscrizione all'albo del C.A.F.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="AOYTOWESTU",Visible=.t., Left=250, Top=236,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data dell'impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="SSBWQANJWI",Visible=.t., Left=46, Top=344,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_3_21 as StdString with uid="CMEGIEQADK",Visible=.t., Left=8, Top=11,;
    Alignment=0, Width=249, Height=19,;
    Caption="Firma della comunicazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_24 as StdString with uid="ERDKNGNDJP",Visible=.t., Left=8, Top=91,;
    Alignment=0, Width=325, Height=19,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_25 as StdString with uid="RCGCOENWMJ",Visible=.t., Left=134, Top=194,;
    Alignment=1, Width=219, Height=18,;
    Caption="Impegno a trasmettere in via telematica:"  ;
  , bGlobalFont=.t.

  add object oStr_3_26 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=266, Top=122,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oBox_3_22 as StdBox with uid="XTEZQYXSTV",left=3, top=7, width=897,height=74

  add object oBox_3_23 as StdBox with uid="DQNRVKFLMJ",left=3, top=79, width=897,height=194
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_agc','ANTIVAGC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AISERIAL=ANTIVAGC.AISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsai_agc
proc SfogliaDir (parent)
local PathName, oField
  PathName = cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
    parent.w_AINOMFIL=left(parent.w_DirName+'ANTIVA_'+alltrim(i_CODAZI)+'_'+parent.w_AISERIAL+space(254),254)
  endif
endproc
* --- Fine Area Manuale
