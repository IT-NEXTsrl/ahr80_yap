* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bac                                                        *
*              Aggiornamento intestatari operazioni superiori a 3000 euro      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-04                                                      *
* Last revis.: 2011-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bac",oParentObject,m.pParame)
return(i_retval)

define class tgsai_bac as StdBatch
  * --- Local variables
  pParame = space(1)
  w_ZOOM = space(10)
  w_NFLTATTR = 0
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_ANDESCRI = space(40)
  w_OPETRE = space(1)
  w_TIPPRE = space(1)
  w_MSGAVV = 0
  w_FLBLLS = space(1)
  w_FLSOAL = space(1)
  w_APPO = space(10)
  w_oERRORLOG = .NULL.
  w_OBJECT = .NULL.
  w_FLPRIV = space(1)
  w_FLSGRE = space(1)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento intestatari operazioni superiori a 3000 euro
    do case
      case this.pParame="S" or this.pParame="D" or this.pParame="I"
        this.w_ZOOM = This.oParentObject.w_ZOOMAOPE
        Select (this.w_ZOOM.cCursor)
        this.w_NFLTATTR = RECNO()
        UPDATE (this.w_ZOOM.cCursor) SET XCHK=ICASE(this.pParame=="S",1,this.pParame=="D",0,IIF(XCHK=1,0,1))
        Select (this.w_ZOOM.cCursor)
        if this.w_NFLTATTR<RECCOUNT()
          GO this.w_NFLTATTR
        endif
      case this.pParame="A"
        this.w_ZOOM = This.oParentObject.w_ZOOMAOPE.cCursor
        SELECT(this.w_ZOOM)
        COUNT FOR XCHK=1 TO w_CONTA
        * --- Verifico se l'utente ha selezionato almeno un record da aggiornare
        if w_CONTA = 0
          ah_errormsg("Selezionare almeno un conto da aggiornare",48)
          i_retcode = 'stop'
          return
        endif
        * --- Crea la classe delle Messaggistiche di Errore
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        this.w_MSGAVV = 0
        Select (this.w_ZOOM) 
 Go Top 
 Scan for XCHK = 1
        this.w_ANTIPCON = Antipcon
        this.w_ANCODICE = Ancodice
        this.w_ANDESCRI = Alltrim(Andescri)
        this.w_OPETRE = IIF(this.oParentObject.w_ANOPETRE="X",OPETRE,this.oParentObject.w_ANOPETRE)
        this.w_TIPPRE = IIF(this.oParentObject.w_ANTIPPRE="X",TIPPRE,this.oParentObject.w_ANTIPPRE)
        this.w_FLPRIV = IIF(this.oParentObject.w_ANFLPRIV="X",ANFLPRIV,this.oParentObject.w_ANFLPRIV)
        this.w_FLSGRE = IIF(this.oParentObject.w_ANFLSGRE="X",FLSGRE,this.oParentObject.w_ANFLSGRE)
        this.w_FLBLLS = FLBLLS
        this.w_FLSOAL = FLSOAL
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANOPETRE ="+cp_NullLink(cp_ToStrODBC(this.w_OPETRE),'CONTI','ANOPETRE');
          +",ANTIPPRE ="+cp_NullLink(cp_ToStrODBC(this.w_TIPPRE),'CONTI','ANTIPPRE');
          +",ANFLPRIV ="+cp_NullLink(cp_ToStrODBC(this.w_FLPRIV),'CONTI','ANFLPRIV');
          +",ANFLSGRE ="+cp_NullLink(cp_ToStrODBC(this.w_FLSGRE),'CONTI','ANFLSGRE');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON );
              +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                 )
        else
          update (i_cTable) set;
              ANOPETRE = this.w_OPETRE;
              ,ANTIPPRE = this.w_TIPPRE;
              ,ANFLPRIV = this.w_FLPRIV;
              ,ANFLSGRE = this.w_FLSGRE;
              &i_ccchkf. ;
           where;
              ANTIPCON = this.w_ANTIPCON ;
              and ANCODICE = this.w_ANCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if (this.w_FLBLLS="S" or this.w_FLSOAL="S") and (this.w_OPETRE $ "PC" or this.w_TIPPRE $ "BS")
          if this.w_ANTIPCON="C"
            this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
          else
            this.w_oERRORLOG.AddMsgLog("Verificare fornitore: %1 %2",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
          endif
          this.w_MSGAVV = this.w_MSGAVV+1
        endif
        Endscan
        ah_errormsg("Elaborazione terminata con successo")
        if this.w_ANTIPCON="C"
          this.w_APPO = "Operazione completata. %0N. %1 clienti aggiornati %0di cui %2 con avvertimento"
        else
          this.w_APPO = "Operazione completata. %0N. %1 fornitori aggiornati %0di cui %2 con avvertimento"
        endif
        AH_ERRORMSG(this.w_APPO,64,,ALLTRIM(STR(w_CONTA)),ALLTRIM(STR(this.w_MSGAVV)))
        if this.w_MSGAVV>0
          * --- Lancia report
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di generazione")     
        endif
        This.oParentObject.NotifyEvent("Interroga")
      case this.pParame="C"
        this.w_ANTIPCON = This.oParentObject.w_ANTIPCON
        this.w_ANCODICE = This.oParentObject.w_ANCODICE
        this.w_OBJECT = iif(this.w_ANTIPCON="F",GSAR_AFR(),GSAR_ACL())
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_ANTIPCON = this.w_ANTIPCON
        this.w_OBJECT.w_ANCODICE = this.w_ANCODICE
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
    endcase
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
