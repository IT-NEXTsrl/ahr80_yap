* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_kac                                                        *
*              Aggiornamento intestatari                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-03                                                      *
* Last revis.: 2011-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_kac",oParentObject))

* --- Class definition
define class tgsai_kac as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 785
  Height = 497+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-12-21"
  HelpContextID=132777833
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  CATECOMM_IDX = 0
  ZONE_IDX = 0
  NAZIONI_IDX = 0
  CACOCLFO_IDX = 0
  cPrg = "gsai_kac"
  cComment = "Aggiornamento intestatari"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_PBANCODI = space(15)
  w_PBANCODI = space(15)
  w_PEANCODI = space(15)
  w_PEANCODI = space(15)
  w_DESCR1 = space(40)
  w_DESCR2 = space(40)
  w_PANCATCOM = space(3)
  w_CATCOMD = space(35)
  w_PANCODZON = space(3)
  w_PANNAZION = space(3)
  w_CATCON = space(5)
  w_OPETRE = space(1)
  w_TIPPRE = space(1)
  w_DESZON = space(35)
  w_DESNAZ = space(35)
  w_DESCCC = space(35)
  w_ANFLBLLS = space(1)
  w_ANFLSOAL = space(1)
  w_ANOPETRE = space(1)
  w_ANTIPPRE = space(1)
  w_ANFLPRIV = space(1)
  w_ANFLSGRE = space(1)
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_PERFIS = space(1)
  w_FLSGRE = space(1)
  w_ZOOMAOPE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_kacPag1","gsai_kac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsai_kacPag2","gsai_kac",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Aggiornamento")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCON_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMAOPE = this.oPgFrm.Pages(2).oPag.ZOOMAOPE
    DoDefault()
    proc Destroy()
      this.w_ZOOMAOPE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CATECOMM'
    this.cWorkTables[3]='ZONE'
    this.cWorkTables[4]='NAZIONI'
    this.cWorkTables[5]='CACOCLFO'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_PBANCODI=space(15)
      .w_PBANCODI=space(15)
      .w_PEANCODI=space(15)
      .w_PEANCODI=space(15)
      .w_DESCR1=space(40)
      .w_DESCR2=space(40)
      .w_PANCATCOM=space(3)
      .w_CATCOMD=space(35)
      .w_PANCODZON=space(3)
      .w_PANNAZION=space(3)
      .w_CATCON=space(5)
      .w_OPETRE=space(1)
      .w_TIPPRE=space(1)
      .w_DESZON=space(35)
      .w_DESNAZ=space(35)
      .w_DESCCC=space(35)
      .w_ANFLBLLS=space(1)
      .w_ANFLSOAL=space(1)
      .w_ANOPETRE=space(1)
      .w_ANTIPPRE=space(1)
      .w_ANFLPRIV=space(1)
      .w_ANFLSGRE=space(1)
      .w_ANTIPCON=space(1)
      .w_ANCODICE=space(15)
      .w_PERFIS=space(1)
      .w_FLSGRE=space(1)
        .w_OBTEST = i_DATSYS
        .w_TIPCON = 'C'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PBANCODI))
          .link_1_4('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PBANCODI))
          .link_1_5('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PEANCODI))
          .link_1_6('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_PEANCODI))
          .link_1_7('Full')
        endif
        .DoRTCalc(7,9,.f.)
        if not(empty(.w_PANCATCOM))
          .link_1_12('Full')
        endif
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_PANCODZON))
          .link_1_15('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_PANNAZION))
          .link_1_16('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CATCON))
          .link_1_17('Full')
        endif
        .w_OPETRE = 'T'
        .w_TIPPRE = 'T'
          .DoRTCalc(16,18,.f.)
        .w_ANFLBLLS = 'S'
        .w_ANFLSOAL = 'S'
      .oPgFrm.Page2.oPag.ZOOMAOPE.Calculate()
        .w_ANOPETRE = 'X'
        .w_ANTIPPRE = iif(isalt(), 'S', 'X')
        .w_ANFLPRIV = 'X'
        .w_ANFLSGRE = 'X'
        .w_ANTIPCON = .w_ZOOMAOPE.getVar('ANTIPCON')
        .w_ANCODICE = .w_ZOOMAOPE.getVar('ANCODICE')
        .w_PERFIS = 'N'
        .w_FLSGRE = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_14.enabled = this.oPgFrm.Page2.oPag.oBtn_2_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_OBTEST = i_DATSYS
        .oPgFrm.Page2.oPag.ZOOMAOPE.Calculate()
        .DoRTCalc(2,22,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_ANFLPRIV = 'X'
        endif
        .DoRTCalc(24,24,.t.)
            .w_ANTIPCON = .w_ZOOMAOPE.getVar('ANTIPCON')
            .w_ANCODICE = .w_ZOOMAOPE.getVar('ANCODICE')
        if .o_TIPCON<>.w_TIPCON
            .w_PERFIS = 'N'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOMAOPE.Calculate()
    endwith
  return

  proc Calculate_DRNNBNCRHR()
    with this
          * --- ActivatePage 1
          .w_ANTIPCON = SPACE(1)
          .w_ANCODICE = SPACE(15)
          .w_ANOPETRE = 'X'
          .w_ANTIPPRE = 'X'
          .w_ANFLPRIV = 'X'
          .w_ANFLSGRE = 'X'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPBANCODI_1_4.enabled = this.oPgFrm.Page1.oPag.oPBANCODI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oPBANCODI_1_5.enabled = this.oPgFrm.Page1.oPag.oPBANCODI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oPEANCODI_1_6.enabled = this.oPgFrm.Page1.oPag.oPEANCODI_1_6.mCond()
    this.oPgFrm.Page1.oPag.oPEANCODI_1_7.enabled = this.oPgFrm.Page1.oPag.oPEANCODI_1_7.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPBANCODI_1_4.visible=!this.oPgFrm.Page1.oPag.oPBANCODI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oPBANCODI_1_5.visible=!this.oPgFrm.Page1.oPag.oPBANCODI_1_5.mHide()
    this.oPgFrm.Page1.oPag.oPEANCODI_1_6.visible=!this.oPgFrm.Page1.oPag.oPEANCODI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oPEANCODI_1_7.visible=!this.oPgFrm.Page1.oPag.oPEANCODI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDESCR1_1_10.visible=!this.oPgFrm.Page1.oPag.oDESCR1_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDESCR2_1_11.visible=!this.oPgFrm.Page1.oPag.oDESCR2_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCATCOMD_1_13.visible=!this.oPgFrm.Page1.oPag.oCATCOMD_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDESZON_1_23.visible=!this.oPgFrm.Page1.oPag.oDESZON_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESNAZ_1_24.visible=!this.oPgFrm.Page1.oPag.oDESNAZ_1_24.mHide()
    this.oPgFrm.Page1.oPag.oDESCCC_1_25.visible=!this.oPgFrm.Page1.oPag.oDESCCC_1_25.mHide()
    this.oPgFrm.Page2.oPag.oANFLPRIV_2_11.visible=!this.oPgFrm.Page2.oPag.oANFLPRIV_2_11.mHide()
    this.oPgFrm.Page1.oPag.oPERFIS_1_31.visible=!this.oPgFrm.Page1.oPag.oPERFIS_1_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_17.visible=!this.oPgFrm.Page2.oPag.oStr_2_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZOOMAOPE.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 1")
          .Calculate_DRNNBNCRHR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PBANCODI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PBANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPBANCODI_1_4'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PBANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PBANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PBANCODI = space(15)
      endif
      this.w_DESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PBANCODI = space(15)
        this.w_DESCR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PBANCODI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PBANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPBANCODI_1_5'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PBANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PBANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PBANCODI = space(15)
      endif
      this.w_DESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PBANCODI = space(15)
        this.w_DESCR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEANCODI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PEANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPEANCODI_1_6'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PEANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PEANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PEANCODI = space(15)
      endif
      this.w_DESCR2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBANCODI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PEANCODI = space(15)
        this.w_DESCR2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEANCODI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PEANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPEANCODI_1_7'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PEANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PEANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PEANCODI = space(15)
      endif
      this.w_DESCR2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBANCODI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PEANCODI = space(15)
        this.w_DESCR2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCATCOM
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_PANCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_PANCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_PANCATCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_PANCATCOM)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PANCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oPANCATCOM_1_12'),i_cWhere,'',"CATEGORIE COMMERCIALI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_PANCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_PANCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_CATCOMD = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PANCATCOM = space(3)
      endif
      this.w_CATCOMD = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCODZON
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_PANCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_PANCODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PANCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oPANCODZON_1_15'),i_cWhere,'',"ZONE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_PANCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_PANCODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PANCODZON = space(3)
      endif
      this.w_DESZON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANNAZION
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_PANNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_PANNAZION))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PANNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oPANNAZION_1_16'),i_cWhere,'',"NAZIONI",'GSVE0ANA.NAZIONI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_PANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_PANNAZION)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PANNAZION = space(3)
      endif
      this.w_DESNAZ = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_17'),i_cWhere,'',"CATEGORIE CONTABILI",'GSAR0AC2.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCCC = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCCC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_2.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPBANCODI_1_4.value==this.w_PBANCODI)
      this.oPgFrm.Page1.oPag.oPBANCODI_1_4.value=this.w_PBANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPBANCODI_1_5.value==this.w_PBANCODI)
      this.oPgFrm.Page1.oPag.oPBANCODI_1_5.value=this.w_PBANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEANCODI_1_6.value==this.w_PEANCODI)
      this.oPgFrm.Page1.oPag.oPEANCODI_1_6.value=this.w_PEANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEANCODI_1_7.value==this.w_PEANCODI)
      this.oPgFrm.Page1.oPag.oPEANCODI_1_7.value=this.w_PEANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_10.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_10.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_11.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_11.value=this.w_DESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCATCOM_1_12.value==this.w_PANCATCOM)
      this.oPgFrm.Page1.oPag.oPANCATCOM_1_12.value=this.w_PANCATCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCOMD_1_13.value==this.w_CATCOMD)
      this.oPgFrm.Page1.oPag.oCATCOMD_1_13.value=this.w_CATCOMD
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCODZON_1_15.value==this.w_PANCODZON)
      this.oPgFrm.Page1.oPag.oPANCODZON_1_15.value=this.w_PANCODZON
    endif
    if not(this.oPgFrm.Page1.oPag.oPANNAZION_1_16.value==this.w_PANNAZION)
      this.oPgFrm.Page1.oPag.oPANNAZION_1_16.value=this.w_PANNAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_17.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_17.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oOPETRE_1_18.RadioValue()==this.w_OPETRE)
      this.oPgFrm.Page1.oPag.oOPETRE_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPPRE_1_19.RadioValue()==this.w_TIPPRE)
      this.oPgFrm.Page1.oPag.oTIPPRE_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESZON_1_23.value==this.w_DESZON)
      this.oPgFrm.Page1.oPag.oDESZON_1_23.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ_1_24.value==this.w_DESNAZ)
      this.oPgFrm.Page1.oPag.oDESNAZ_1_24.value=this.w_DESNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCC_1_25.value==this.w_DESCCC)
      this.oPgFrm.Page1.oPag.oDESCCC_1_25.value=this.w_DESCCC
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLBLLS_1_26.RadioValue()==this.w_ANFLBLLS)
      this.oPgFrm.Page1.oPag.oANFLBLLS_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLSOAL_1_27.RadioValue()==this.w_ANFLSOAL)
      this.oPgFrm.Page1.oPag.oANFLSOAL_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANOPETRE_2_9.RadioValue()==this.w_ANOPETRE)
      this.oPgFrm.Page2.oPag.oANOPETRE_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANTIPPRE_2_10.RadioValue()==this.w_ANTIPPRE)
      this.oPgFrm.Page2.oPag.oANTIPPRE_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLPRIV_2_11.RadioValue()==this.w_ANFLPRIV)
      this.oPgFrm.Page2.oPag.oANFLPRIV_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLSGRE_2_12.RadioValue()==this.w_ANFLSGRE)
      this.oPgFrm.Page2.oPag.oANFLSGRE_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERFIS_1_31.RadioValue()==this.w_PERFIS)
      this.oPgFrm.Page1.oPag.oPERFIS_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSGRE_1_32.RadioValue()==this.w_FLSGRE)
      this.oPgFrm.Page1.oPag.oFLSGRE_1_32.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) )  and not(UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')  and (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')  and not(empty(.w_PBANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBANCODI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) )  and not(UPPER(g_APPLICATION)='AD HOC ENTERPRISE')  and (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')  and not(empty(.w_PBANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBANCODI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBANCODI))))  and not(UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')  and (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')  and not(empty(.w_PEANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEANCODI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBANCODI))))  and not(UPPER(g_APPLICATION)='AD HOC ENTERPRISE')  and (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')  and not(empty(.w_PEANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEANCODI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    return

enddefine

* --- Define pages as container
define class tgsai_kacPag1 as StdContainer
  Width  = 781
  height = 497
  stdWidth  = 781
  stdheight = 497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCON_1_2 as StdCombo with uid="UUDBDOJQXS",rtseq=2,rtrep=.f.,left=159,top=31,width=126,height=21;
    , ToolTipText = "Tipo: cliente o fornitore";
    , HelpContextID = 78756042;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_2.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCON_1_2.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_2.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PBANCODI)
        bRes2=.link_1_4('Full')
      endif
      if .not. empty(.w_PBANCODI)
        bRes2=.link_1_5('Full')
      endif
      if .not. empty(.w_PEANCODI)
        bRes2=.link_1_6('Full')
      endif
      if .not. empty(.w_PEANCODI)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oPBANCODI_1_4 as StdField with uid="MRSFOXHTFW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PBANCODI", cQueryName = "PBANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 194531391,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=159, Top=68, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PBANCODI"

  func oPBANCODI_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
   endif
  endfunc

  func oPBANCODI_1_4.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc

  func oPBANCODI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBANCODI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBANCODI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPBANCODI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'',this.parent.oContained
  endproc
  proc oPBANCODI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PBANCODI
     i_obj.ecpSave()
  endproc

  add object oPBANCODI_1_5 as StdField with uid="DFGMOVYBEF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PBANCODI", cQueryName = "PBANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 194531391,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=159, Top=68, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PBANCODI"

  func oPBANCODI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
   endif
  endfunc

  func oPBANCODI_1_5.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
  endfunc

  func oPBANCODI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBANCODI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBANCODI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPBANCODI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oPBANCODI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PBANCODI
     i_obj.ecpSave()
  endproc

  add object oPEANCODI_1_6 as StdField with uid="IIYGZMLHDJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PEANCODI", cQueryName = "PEANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 194532159,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=160, Top=106, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PEANCODI"

  func oPEANCODI_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
   endif
  endfunc

  func oPEANCODI_1_6.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc

  func oPEANCODI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEANCODI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEANCODI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPEANCODI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'',this.parent.oContained
  endproc
  proc oPEANCODI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PEANCODI
     i_obj.ecpSave()
  endproc

  add object oPEANCODI_1_7 as StdField with uid="MRCFGLSJLJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PEANCODI", cQueryName = "PEANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 194532159,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=160, Top=106, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PEANCODI"

  func oPEANCODI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
   endif
  endfunc

  func oPEANCODI_1_7.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
  endfunc

  func oPEANCODI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEANCODI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEANCODI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPEANCODI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oPEANCODI_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PEANCODI
     i_obj.ecpSave()
  endproc

  add object oDESCR1_1_10 as StdField with uid="CHLOPGXLBM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 25267658,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=297, Top=68, InputMask=replicate('X',40)

  func oDESCR1_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oDESCR2_1_11 as StdField with uid="MINMVUHPXL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 8490442,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=297, Top=106, InputMask=replicate('X',40)

  func oDESCR2_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oPANCATCOM_1_12 as StdField with uid="RCDPGMCMDR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PANCATCOM", cQueryName = "PANCATCOM",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria commerciale inesistente",;
    ToolTipText = "Categoria commerciale di selezione",;
    HelpContextID = 7218197,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=159, Top=144, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", oKey_1_1="CTCODICE", oKey_1_2="this.w_PANCATCOM"

  func oPANCATCOM_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCATCOM_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCATCOM_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oPANCATCOM_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE COMMERCIALI",'',this.parent.oContained
  endproc

  add object oCATCOMD_1_13 as StdField with uid="GEURVMYGRO",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CATCOMD", cQueryName = "CATCOMD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 172916262,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=211, Top=144, InputMask=replicate('X',35)

  func oCATCOMD_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oPANCODZON_1_15 as StdField with uid="QJDMIHOTHD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PANCODZON", cQueryName = "PANCODZON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente",;
    ToolTipText = "Zona selezionata",;
    HelpContextID = 246537179,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=159, Top=182, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_PANCODZON"

  func oPANCODZON_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCODZON_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCODZON_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oPANCODZON_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ZONE",'',this.parent.oContained
  endproc

  add object oPANNAZION_1_16 as StdField with uid="AFIAUNXGBJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PANNAZION", cQueryName = "PANNAZION",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice nazione inesistente",;
    ToolTipText = "Nazione selezionata",;
    HelpContextID = 159833051,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=159, Top=220, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", oKey_1_1="NACODNAZ", oKey_1_2="this.w_PANNAZION"

  func oPANNAZION_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANNAZION_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANNAZION_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oPANNAZION_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"NAZIONI",'GSVE0ANA.NAZIONI_VZM',this.parent.oContained
  endproc

  add object oCATCON_1_17 as StdField with uid="HVTGPSGIMF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria contabile inesistente",;
    ToolTipText = "Categoria contabile selezionata",;
    HelpContextID = 78741978,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=159, Top=258, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE CONTABILI",'GSAR0AC2.CACOCLFO_VZM',this.parent.oContained
  endproc


  add object oOPETRE_1_18 as StdCombo with uid="MGBBFYASRA",rtseq=14,rtrep=.f.,left=159,top=296,width=147,height=21;
    , ToolTipText = "Classificazione soggetto";
    , HelpContextID = 225534490;
    , cFormVar="w_OPETRE",RowSource=""+"Tutti tranne esclusi,"+"Corrispettivi periodici,"+"Contratti collegati,"+"Non definibile,"+"Escludi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOPETRE_1_18.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'N',;
    iif(this.value =5,'E',;
    ' '))))))
  endfunc
  func oOPETRE_1_18.GetRadio()
    this.Parent.oContained.w_OPETRE = this.RadioValue()
    return .t.
  endfunc

  func oOPETRE_1_18.SetRadio()
    this.Parent.oContained.w_OPETRE=trim(this.Parent.oContained.w_OPETRE)
    this.value = ;
      iif(this.Parent.oContained.w_OPETRE=='T',1,;
      iif(this.Parent.oContained.w_OPETRE=='P',2,;
      iif(this.Parent.oContained.w_OPETRE=='C',3,;
      iif(this.Parent.oContained.w_OPETRE=='N',4,;
      iif(this.Parent.oContained.w_OPETRE=='E',5,;
      0)))))
  endfunc


  add object oTIPPRE_1_19 as StdCombo with uid="BTJSXLXSEN",rtseq=15,rtrep=.f.,left=159,top=341,width=147,height=21;
    , ToolTipText = "Classificazione operazioni tra beni e servizi";
    , HelpContextID = 225753290;
    , cFormVar="w_TIPPRE",RowSource=""+"Tutti,"+"Beni,"+"Servizi,"+"Non definibile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPPRE_1_19.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'B',;
    iif(this.value =3,'S',;
    iif(this.value =4,'N',;
    ' ')))))
  endfunc
  func oTIPPRE_1_19.GetRadio()
    this.Parent.oContained.w_TIPPRE = this.RadioValue()
    return .t.
  endfunc

  func oTIPPRE_1_19.SetRadio()
    this.Parent.oContained.w_TIPPRE=trim(this.Parent.oContained.w_TIPPRE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPRE=='T',1,;
      iif(this.Parent.oContained.w_TIPPRE=='B',2,;
      iif(this.Parent.oContained.w_TIPPRE=='S',3,;
      iif(this.Parent.oContained.w_TIPPRE=='N',4,;
      0))))
  endfunc

  add object oDESZON_1_23 as StdField with uid="CMKPWYGMPX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 77237706,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=211, Top=182, InputMask=replicate('X',35)

  func oDESZON_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oDESNAZ_1_24 as StdField with uid="THHWJIIXZU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESNAZ", cQueryName = "DESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 159813066,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=211, Top=220, InputMask=replicate('X',35)

  func oDESNAZ_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oDESCCC_1_25 as StdField with uid="KHIMEAPONY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCCC", cQueryName = "DESCCC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 7441866,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=225, Top=258, InputMask=replicate('X',35)

  func oDESCCC_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oANFLBLLS_1_26 as StdCheck with uid="AEKSYTSVNV",rtseq=19,rtrep=.f.,left=159, top=378, caption="Escludi fiscalit� privilegiata",;
    ToolTipText = "Se attivato, vengono esclusi dalla ricerca i clienti/fornitori che hanno attivato il flag 'Fiscalit� privilegiata'",;
    HelpContextID = 125392039,;
    cFormVar="w_ANFLBLLS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANFLBLLS_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLBLLS_1_26.GetRadio()
    this.Parent.oContained.w_ANFLBLLS = this.RadioValue()
    return .t.
  endfunc

  func oANFLBLLS_1_26.SetRadio()
    this.Parent.oContained.w_ANFLBLLS=trim(this.Parent.oContained.w_ANFLBLLS)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLBLLS=='S',1,;
      0)
  endfunc

  add object oANFLSOAL_1_27 as StdCheck with uid="NPACGDEOLQ",rtseq=20,rtrep=.f.,left=404, top=378, caption="Escludi soggetto terzo",;
    ToolTipText = "Se attivato, vengono esclusi dalla ricerca i clienti/fornitori che hanno attivato il flag 'Soggetto terzo'",;
    HelpContextID = 57234606,;
    cFormVar="w_ANFLSOAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANFLSOAL_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLSOAL_1_27.GetRadio()
    this.Parent.oContained.w_ANFLSOAL = this.RadioValue()
    return .t.
  endfunc

  func oANFLSOAL_1_27.SetRadio()
    this.Parent.oContained.w_ANFLSOAL=trim(this.Parent.oContained.w_ANFLSOAL)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLSOAL=='S',1,;
      0)
  endfunc

  add object oPERFIS_1_31 as StdCheck with uid="CICXJCMWAS",rtseq=27,rtrep=.f.,left=159, top=408, caption="Includi persone fisiche non private",;
    ToolTipText = "Se attivo vengono visualizzati solo i clienti persone fisiche con il flag soggetto privato spento",;
    HelpContextID = 957706,;
    cFormVar="w_PERFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERFIS_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPERFIS_1_31.GetRadio()
    this.Parent.oContained.w_PERFIS = this.RadioValue()
    return .t.
  endfunc

  func oPERFIS_1_31.SetRadio()
    this.Parent.oContained.w_PERFIS=trim(this.Parent.oContained.w_PERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_PERFIS=='S',1,;
      0)
  endfunc

  func oPERFIS_1_31.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='F')
    endwith
  endfunc

  add object oFLSGRE_1_32 as StdCheck with uid="PQDMOUEJGB",rtseq=28,rtrep=.f.,left=404, top=408, caption="Escludi soggetto non residente",;
    ToolTipText = "Se attivato, vengono esclusi dalla ricerca i clienti/fornitori che hanno attivato il flag 'Soggetto non residente'",;
    HelpContextID = 226330282,;
    cFormVar="w_FLSGRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSGRE_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSGRE_1_32.GetRadio()
    this.Parent.oContained.w_FLSGRE = this.RadioValue()
    return .t.
  endfunc

  func oFLSGRE_1_32.SetRadio()
    this.Parent.oContained.w_FLSGRE=trim(this.Parent.oContained.w_FLSGRE)
    this.value = ;
      iif(this.Parent.oContained.w_FLSGRE=='S',1,;
      0)
  endfunc

  add object oStr_1_3 as StdString with uid="OPZMNBZJGE",Visible=.t., Left=54, Top=31,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MAJRIEMKEU",Visible=.t., Left=89, Top=68,;
    Alignment=1, Width=65, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XJEFRRYNMW",Visible=.t., Left=96, Top=108,;
    Alignment=1, Width=58, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="OPURTLUYAH",Visible=.t., Left=9, Top=146,;
    Alignment=1, Width=145, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="HSWSDVDMII",Visible=.t., Left=34, Top=260,;
    Alignment=1, Width=120, Height=15,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="LVHUTRYMYJ",Visible=.t., Left=123, Top=184,;
    Alignment=1, Width=31, Height=15,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="GZCJKQCEYX",Visible=.t., Left=106, Top=223,;
    Alignment=1, Width=48, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="UYAAQHCPRD",Visible=.t., Left=2, Top=298,;
    Alignment=1, Width=152, Height=18,;
    Caption="Operazioni rilevanti IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="DFDUXKNAOV",Visible=.t., Left=15, Top=341,;
    Alignment=1, Width=139, Height=18,;
    Caption="Tipologia prevalente:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsai_kacPag2 as StdContainer
  Width  = 781
  height = 497
  stdWidth  = 781
  stdheight = 497
  resizeXpos=351
  resizeYpos=150
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMAOPE as cp_szoombox with uid="SFIVKVVAUJ",left=0, top=4, width=780,height=319,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSAI_KAC",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAI_KAC",cZoomOnZoom="",;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 45219814


  add object oBtn_2_2 as StdButton with uid="GQMUPTTTCY",left=1, top=326, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare i conti";
    , HelpContextID = 226603542;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        GSAI_BAC(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_3 as StdButton with uid="ZCYHLDNLVF",left=49, top=326, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per deselezionare i conti";
    , HelpContextID = 226603542;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        GSAI_BAC(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_4 as StdButton with uid="GUQBLQMZBT",left=97, top=326, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per invertire la selezione di tutti i conti";
    , HelpContextID = 226603542;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      with this.Parent.oContained
        GSAI_BAC(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oANOPETRE_2_9 as StdCombo with uid="RJRRLIFHCM",rtseq=21,rtrep=.f.,left=171,top=410,width=162,height=21;
    , ToolTipText = "Operazioni rilevanti IVA";
    , HelpContextID = 12270411;
    , cFormVar="w_ANOPETRE",RowSource=""+"Nessun aggiornamento,"+"Escludi,"+"Corrispettivi periodici,"+"Contratti collegati,"+"Non definibile", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANOPETRE_2_9.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'E',;
    iif(this.value =3,'P',;
    iif(this.value =4,'C',;
    iif(this.value =5,'N',;
    space(1)))))))
  endfunc
  func oANOPETRE_2_9.GetRadio()
    this.Parent.oContained.w_ANOPETRE = this.RadioValue()
    return .t.
  endfunc

  func oANOPETRE_2_9.SetRadio()
    this.Parent.oContained.w_ANOPETRE=trim(this.Parent.oContained.w_ANOPETRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANOPETRE=='X',1,;
      iif(this.Parent.oContained.w_ANOPETRE=='E',2,;
      iif(this.Parent.oContained.w_ANOPETRE=='P',3,;
      iif(this.Parent.oContained.w_ANOPETRE=='C',4,;
      iif(this.Parent.oContained.w_ANOPETRE=='N',5,;
      0)))))
  endfunc


  add object oANTIPPRE_2_10 as StdCombo with uid="FYWICOIWSH",rtseq=22,rtrep=.f.,left=485,top=410,width=162,height=21;
    , ToolTipText = "Tipologia prevalente";
    , HelpContextID = 224693067;
    , cFormVar="w_ANTIPPRE",RowSource=""+"Nessun aggiornamento,"+"Non definibile,"+"Beni,"+"Servizi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANTIPPRE_2_10.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'N',;
    iif(this.value =3,'B',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oANTIPPRE_2_10.GetRadio()
    this.Parent.oContained.w_ANTIPPRE = this.RadioValue()
    return .t.
  endfunc

  func oANTIPPRE_2_10.SetRadio()
    this.Parent.oContained.w_ANTIPPRE=trim(this.Parent.oContained.w_ANTIPPRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANTIPPRE=='X',1,;
      iif(this.Parent.oContained.w_ANTIPPRE=='N',2,;
      iif(this.Parent.oContained.w_ANTIPPRE=='B',3,;
      iif(this.Parent.oContained.w_ANTIPPRE=='S',4,;
      0))))
  endfunc


  add object oANFLPRIV_2_11 as StdCombo with uid="ELXPNCIQHX",rtseq=23,rtrep=.f.,left=171,top=447,width=162,height=21;
    , ToolTipText = "Soggetto privato";
    , HelpContextID = 258386780;
    , cFormVar="w_ANFLPRIV",RowSource=""+"Nessun aggiornamento,"+"Non privato,"+"Soggetto privato", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANFLPRIV_2_11.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oANFLPRIV_2_11.GetRadio()
    this.Parent.oContained.w_ANFLPRIV = this.RadioValue()
    return .t.
  endfunc

  func oANFLPRIV_2_11.SetRadio()
    this.Parent.oContained.w_ANFLPRIV=trim(this.Parent.oContained.w_ANFLPRIV)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLPRIV=='X',1,;
      iif(this.Parent.oContained.w_ANFLPRIV=='N',2,;
      iif(this.Parent.oContained.w_ANFLPRIV=='S',3,;
      0)))
  endfunc

  func oANFLPRIV_2_11.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='F')
    endwith
  endfunc


  add object oANFLSGRE_2_12 as StdCombo with uid="YROIPMNQYV",rtseq=24,rtrep=.f.,left=491,top=447,width=162,height=21;
    , ToolTipText = "Soggetto non residente";
    , HelpContextID = 76983115;
    , cFormVar="w_ANFLSGRE",RowSource=""+"Nessun aggiornamento,"+"No,"+"Si", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANFLSGRE_2_12.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oANFLSGRE_2_12.GetRadio()
    this.Parent.oContained.w_ANFLSGRE = this.RadioValue()
    return .t.
  endfunc

  func oANFLSGRE_2_12.SetRadio()
    this.Parent.oContained.w_ANFLSGRE=trim(this.Parent.oContained.w_ANFLSGRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLSGRE=='X',1,;
      iif(this.Parent.oContained.w_ANFLSGRE=='N',2,;
      iif(this.Parent.oContained.w_ANFLSGRE=='S',3,;
      0)))
  endfunc


  add object oBtn_2_13 as StdButton with uid="TOYTSIORUM",left=675, top=451, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare i dati";
    , HelpContextID = 226603542;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSAI_BAC(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ANOPETRE<>'X' OR .w_ANTIPPRE<>'X' OR .w_ANFLPRIV<>'X' OR .w_ANFLSGRE<>'X')
      endwith
    endif
  endfunc


  add object oBtn_2_14 as StdButton with uid="EGNAIUHSLT",left=730, top=451, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 226603542;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_5 as StdString with uid="SAJYSCUIOK",Visible=.t., Left=-2, Top=378,;
    Alignment=1, Width=162, Height=18,;
    Caption="Opzioni aggiornamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="BUGWNNRRGK",Visible=.t., Left=11, Top=412,;
    Alignment=1, Width=155, Height=18,;
    Caption="Operazioni rilevanti IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="DYKIOEBUDM",Visible=.t., Left=340, Top=412,;
    Alignment=1, Width=140, Height=18,;
    Caption="Tipologia prevalente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="YQKMFTJODB",Visible=.t., Left=22, Top=449,;
    Alignment=1, Width=144, Height=18,;
    Caption="Soggetto privato:"  ;
  , bGlobalFont=.t.

  func oStr_2_17.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_18 as StdString with uid="BWNYIPPIMY",Visible=.t., Left=341, Top=449,;
    Alignment=1, Width=144, Height=18,;
    Caption="Soggetto non residente:"  ;
  , bGlobalFont=.t.

  add object oBox_2_6 as StdBox with uid="QHEGMXSNWS",left=-2, top=397, width=783,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_kac','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
