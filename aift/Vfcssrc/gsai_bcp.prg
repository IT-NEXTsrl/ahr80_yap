* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bcp                                                        *
*              Calcola periodo di estrazione dati antievasione IVA             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-25                                                      *
* Last revis.: 2010-07-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bcp",oParentObject,m.pPARAM)
return(i_retval)

define class tgsai_bcp as StdBatch
  * --- Local variables
  pPARAM = space(3)
  w_DT__MESE = 0
  w_DTTRIMES = 0
  * --- WorkFile variables
  ANTIVAAN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- calcolo periodo successivo all'ultima estrazione per l'anno selezionato
    if this.pPARAM="CHK"
      this.oParentObject.w_ELIDATI = " "
      if this.oParentObject.w_PERIOD="M"
        this.oParentObject.w_TRIMES = 0
        * --- Select from ANTIVAAN
        i_nConn=i_TableProp[this.ANTIVAAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAAN_idx,2],.t.,this.ANTIVAAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DT__MESE) as DT__MESE  from "+i_cTable+" ANTIVAAN ";
              +" where DT__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND DT__MESE="+cp_ToStrODBC(this.oParentObject.w_MESE)+"";
               ,"_Curs_ANTIVAAN")
        else
          select MAX(DT__MESE) as DT__MESE from (i_cTable);
           where DT__ANNO=this.oParentObject.w_ANNO AND DT__MESE=this.oParentObject.w_MESE;
            into cursor _Curs_ANTIVAAN
        endif
        if used('_Curs_ANTIVAAN')
          select _Curs_ANTIVAAN
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_ELIDATI = IIF(DT__MESE=this.oParentObject.w_MESE, "S", this.oParentObject.w_ELIDATI)
            select _Curs_ANTIVAAN
            continue
          enddo
          use
        endif
      else
        this.oParentObject.w_MESE = 0
        * --- Select from ANTIVAAN
        i_nConn=i_TableProp[this.ANTIVAAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAAN_idx,2],.t.,this.ANTIVAAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DTTRIMES) as DTTRIMES  from "+i_cTable+" ANTIVAAN ";
              +" where DT__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND DTTRIMES="+cp_ToStrODBC(this.oParentObject.w_TRIMES)+"";
               ,"_Curs_ANTIVAAN")
        else
          select MAX(DTTRIMES) as DTTRIMES from (i_cTable);
           where DT__ANNO=this.oParentObject.w_ANNO AND DTTRIMES=this.oParentObject.w_TRIMES;
            into cursor _Curs_ANTIVAAN
        endif
        if used('_Curs_ANTIVAAN')
          select _Curs_ANTIVAAN
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_ELIDATI = IIF(DTTRIMES=this.oParentObject.w_TRIMES, "S", this.oParentObject.w_ELIDATI)
            select _Curs_ANTIVAAN
            continue
          enddo
          use
        endif
      endif
    else
      if this.oParentObject.w_PERIOD="M"
        this.oParentObject.w_TRIMES = 0
        * --- Select from ANTIVAAN
        i_nConn=i_TableProp[this.ANTIVAAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAAN_idx,2],.t.,this.ANTIVAAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DT__MESE) as DT__MESE  from "+i_cTable+" ANTIVAAN ";
              +" where DT__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+"";
               ,"_Curs_ANTIVAAN")
        else
          select MAX(DT__MESE) as DT__MESE from (i_cTable);
           where DT__ANNO=this.oParentObject.w_ANNO;
            into cursor _Curs_ANTIVAAN
        endif
        if used('_Curs_ANTIVAAN')
          select _Curs_ANTIVAAN
          locate for 1=1
          do while not(eof())
          this.w_DT__MESE = NVL(_Curs_ANTIVAAN.DT__MESE,0)
            select _Curs_ANTIVAAN
            continue
          enddo
          use
        endif
        this.oParentObject.w_MESE = iif(NVL(this.w_DT__MESE,0)<>0, this.w_DT__MESE + 1, this.oParentObject.w_MESE)
        this.oParentObject.w_MESE = iif(this.oParentObject.w_MESE>12, 12, this.oParentObject.w_MESE)
        this.oParentObject.w_DADATA = iif(this.oParentObject.w_MESE<>0 AND this.oParentObject.w_ANNO<>0,DATE(this.oParentObject.w_ANNO, this.oParentObject.w_MESE, 1),CTOD("  -  -    "))
        this.oParentObject.w_ADATA = iif(this.oParentObject.w_MESE<>0 AND this.oParentObject.w_ANNO<>0,DATE(YEAR(this.oParentObject.w_DADATA + 31), MONTH(this.oParentObject.w_DADATA + 31),1)-1,CTOD("  -  -    "))
      else
        this.oParentObject.w_MESE = 0
        * --- Select from ANTIVAAN
        i_nConn=i_TableProp[this.ANTIVAAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANTIVAAN_idx,2],.t.,this.ANTIVAAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DTTRIMES) as DTTRIMES  from "+i_cTable+" ANTIVAAN ";
              +" where DT__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+"";
               ,"_Curs_ANTIVAAN")
        else
          select MAX(DTTRIMES) as DTTRIMES from (i_cTable);
           where DT__ANNO=this.oParentObject.w_ANNO;
            into cursor _Curs_ANTIVAAN
        endif
        if used('_Curs_ANTIVAAN')
          select _Curs_ANTIVAAN
          locate for 1=1
          do while not(eof())
          this.w_DTTRIMES = NVL(_Curs_ANTIVAAN.DTTRIMES,0)
            select _Curs_ANTIVAAN
            continue
          enddo
          use
        endif
        this.oParentObject.w_TRIMES = iif(NVL(this.w_DTTRIMES,0)<>0, this.w_DTTRIMES + 1, this.oParentObject.w_TRIMES)
        this.oParentObject.w_TRIMES = iif(this.oParentObject.w_TRIMES>4, 4, this.oParentObject.w_TRIMES)
        this.oParentObject.w_DADATA = iif(this.oParentObject.w_TRIMES<>0 AND this.oParentObject.w_ANNO<>0,DATE(this.oParentObject.w_ANNO, (this.oParentObject.w_TRIMES-1)*3+1, 1),CTOD("  -  -    "))
        this.oParentObject.w_ADATA = iif(this.oParentObject.w_TRIMES<>0 AND this.oParentObject.w_ANNO<>0,DATE(YEAR(this.oParentObject.w_DADATA + 93), MONTH(this.oParentObject.w_DADATA + 93), 1)-1,CTOD("  -  -    "))
      endif
    endif
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ANTIVAAN'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ANTIVAAN')
      use in _Curs_ANTIVAAN
    endif
    if used('_Curs_ANTIVAAN')
      use in _Curs_ANTIVAAN
    endif
    if used('_Curs_ANTIVAAN')
      use in _Curs_ANTIVAAN
    endif
    if used('_Curs_ANTIVAAN')
      use in _Curs_ANTIVAAN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
