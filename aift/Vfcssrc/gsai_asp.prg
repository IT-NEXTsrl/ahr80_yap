* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_asp                                                        *
*              Dati estratti spesometro                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-23                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_asp"))

* --- Class definition
define class tgsai_asp as StdForm
  Top    = 4
  Left   = 9

  * --- Standard Properties
  Width  = 1104
  Height = 572+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=158726295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Constant Properties
  DATESTSP_IDX = 0
  CONTI_IDX = 0
  SOGCOLNR_IDX = 0
  NAZIONI_IDX = 0
  cFile = "DATESTSP"
  cKeySelect = "SPSERIAL"
  cKeyWhere  = "SPSERIAL=this.w_SPSERIAL"
  cKeyWhereODBC = '"SPSERIAL="+cp_ToStrODBC(this.w_SPSERIAL)';

  cKeyWhereODBCqualified = '"DATESTSP.SPSERIAL="+cp_ToStrODBC(this.w_SPSERIAL)';

  cPrg = "gsai_asp"
  cComment = "Dati estratti spesometro"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SPSERIAL = space(10)
  w_SP__ANNO = 0
  w_SPTIPCON = space(1)
  o_SPTIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_SPCODCON = space(15)
  o_SPCODCON = space(15)
  w_SCCODCLF = space(15)
  w_CHCODCLF = space(15)
  w_SCCOGSCN = space(24)
  w_SCNOMSCN = space(20)
  w_SCDTNSCN = ctod('  /  /  ')
  w_SCLCRSCN = space(40)
  w_SCPRNSCN = space(2)
  w_SCSESDOM = space(3)
  w_ANPARIVA = space(12)
  w_ANFLPRIV = space(1)
  w_ANFLSGRE = space(1)
  w_ANCODFIS = space(16)
  w_ANPERFIS = space(1)
  w_ANDESCRI = space(40)
  w_ANCOGNOM = space(40)
  w_AN__NOME = space(20)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANNAZION = space(3)
  w_NACODEST = space(5)
  w_ANLOCALI = space(30)
  w_ANINDIRI = space(35)
  w_SPTIPSOG = space(1)
  o_SPTIPSOG = space(1)
  w_SPTIPSOG = space(1)
  w_SPESTPER = space(1)
  o_SPESTPER = space(1)
  w_SPSCLGEN = space(1)
  w_SPDESCRI = space(60)
  w_SPCOGNOM = space(40)
  w_SP__NOME = space(40)
  w_SPDATNAS = ctod('  /  /  ')
  w_SPCOMEST = space(40)
  w_SPPRONAS = space(2)
  w_SPSTADOM = space(3)
  w_SPSTAEST = space(3)
  w_SPCITEST = space(40)
  w_SPINDEST = space(40)
  w_SPPARIVA = space(25)
  w_SPCODFIS = space(25)
  w_SPCODGEN = space(10)
  w_SPDACONT = space(1)
  w_EDIT_DETIPSOG = .F.
  w_CONTROLS = .F.
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_SPSERIAL = this.W_SPSERIAL

  * --- Children pointers
  GSAI_MSP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DATESTSP','gsai_asp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_aspPag1","gsai_asp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati anagrafici")
      .Pages(1).HelpContextID = 152885497
      .Pages(2).addobject("oPag","tgsai_aspPag2","gsai_asp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio dati estratti")
      .Pages(2).HelpContextID = 55682340
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='SOGCOLNR'
    this.cWorkTables[3]='NAZIONI'
    this.cWorkTables[4]='DATESTSP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATESTSP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATESTSP_IDX,3]
  return

  function CreateChildren()
    this.GSAI_MSP = CREATEOBJECT('stdDynamicChild',this,'GSAI_MSP',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSAI_MSP.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAI_MSP)
      this.GSAI_MSP.DestroyChildrenChain()
      this.GSAI_MSP=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAI_MSP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAI_MSP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAI_MSP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAI_MSP.SetKey(;
            .w_SPSERIAL,"SPSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAI_MSP.ChangeRow(this.cRowID+'      1',1;
             ,.w_SPSERIAL,"SPSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAI_MSP)
        i_f=.GSAI_MSP.BuildFilter()
        if !(i_f==.GSAI_MSP.cQueryFilter)
          i_fnidx=.GSAI_MSP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MSP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MSP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MSP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MSP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SPSERIAL = NVL(SPSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DATESTSP where SPSERIAL=KeySet.SPSERIAL
    *
    i_nConn = i_TableProp[this.DATESTSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATESTSP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATESTSP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATESTSP '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SPSERIAL',this.w_SPSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_CHCODCLF = space(15)
        .w_SCCOGSCN = space(24)
        .w_SCNOMSCN = space(20)
        .w_SCDTNSCN = ctod("  /  /  ")
        .w_SCLCRSCN = space(40)
        .w_SCPRNSCN = space(2)
        .w_SCSESDOM = space(3)
        .w_ANPARIVA = space(12)
        .w_ANFLPRIV = space(1)
        .w_ANFLSGRE = space(1)
        .w_ANCODFIS = space(16)
        .w_ANPERFIS = space(1)
        .w_ANDESCRI = space(40)
        .w_ANCOGNOM = space(40)
        .w_AN__NOME = space(20)
        .w_ANDATNAS = ctod("  /  /  ")
        .w_ANLOCNAS = space(30)
        .w_ANPRONAS = space(2)
        .w_ANNAZION = space(3)
        .w_NACODEST = space(5)
        .w_ANLOCALI = space(30)
        .w_ANINDIRI = space(35)
        .w_EDIT_DETIPSOG = .T.
        .w_CONTROLS = .f.
        .w_SPSERIAL = NVL(SPSERIAL,space(10))
        .op_SPSERIAL = .w_SPSERIAL
        .w_SP__ANNO = NVL(SP__ANNO,0)
        .w_SPTIPCON = NVL(SPTIPCON,space(1))
        .w_SPCODCON = NVL(SPCODCON,space(15))
          if link_1_5_joined
            this.w_SPCODCON = NVL(ANCODICE105,NVL(this.w_SPCODCON,space(15)))
            this.w_ANPARIVA = NVL(ANPARIVA105,space(12))
            this.w_ANFLPRIV = NVL(ANFLPRIV105,space(1))
            this.w_ANFLSGRE = NVL(ANFLSGRE105,space(1))
            this.w_ANCODFIS = NVL(ANCODFIS105,space(16))
            this.w_ANPERFIS = NVL(ANPERFIS105,space(1))
            this.w_ANDESCRI = NVL(ANDESCRI105,space(40))
            this.w_ANCOGNOM = NVL(ANCOGNOM105,space(40))
            this.w_AN__NOME = NVL(AN__NOME105,space(20))
            this.w_ANDATNAS = NVL(cp_ToDate(ANDATNAS105),ctod("  /  /  "))
            this.w_ANLOCNAS = NVL(ANLOCNAS105,space(30))
            this.w_ANPRONAS = NVL(ANPRONAS105,space(2))
            this.w_ANNAZION = NVL(ANNAZION105,space(3))
            this.w_ANLOCALI = NVL(ANLOCALI105,space(30))
            this.w_ANINDIRI = NVL(ANINDIRI105,space(35))
          else
          .link_1_5('Load')
          endif
        .w_SCCODCLF = .w_SPCODCON
          .link_1_6('Load')
          .link_1_7('Load')
          .link_1_25('Load')
        .w_SPTIPSOG = NVL(SPTIPSOG,space(1))
        .w_SPTIPSOG = NVL(SPTIPSOG,space(1))
        .w_SPESTPER = NVL(SPESTPER,space(1))
        .w_SPSCLGEN = NVL(SPSCLGEN,space(1))
        .w_SPDESCRI = NVL(SPDESCRI,space(60))
        .w_SPCOGNOM = NVL(SPCOGNOM,space(40))
        .w_SP__NOME = NVL(SP__NOME,space(40))
        .w_SPDATNAS = NVL(cp_ToDate(SPDATNAS),ctod("  /  /  "))
        .w_SPCOMEST = NVL(SPCOMEST,space(40))
        .w_SPPRONAS = NVL(SPPRONAS,space(2))
        .w_SPSTADOM = NVL(SPSTADOM,space(3))
        .w_SPSTAEST = NVL(SPSTAEST,space(3))
        .w_SPCITEST = NVL(SPCITEST,space(40))
        .w_SPINDEST = NVL(SPINDEST,space(40))
        .w_SPPARIVA = NVL(SPPARIVA,space(25))
        .w_SPCODFIS = NVL(SPCODFIS,space(25))
        .w_SPCODGEN = NVL(SPCODGEN,space(10))
        .w_SPDACONT = NVL(SPDACONT,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DATESTSP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SPSERIAL = space(10)
      .w_SP__ANNO = 0
      .w_SPTIPCON = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_SPCODCON = space(15)
      .w_SCCODCLF = space(15)
      .w_CHCODCLF = space(15)
      .w_SCCOGSCN = space(24)
      .w_SCNOMSCN = space(20)
      .w_SCDTNSCN = ctod("  /  /  ")
      .w_SCLCRSCN = space(40)
      .w_SCPRNSCN = space(2)
      .w_SCSESDOM = space(3)
      .w_ANPARIVA = space(12)
      .w_ANFLPRIV = space(1)
      .w_ANFLSGRE = space(1)
      .w_ANCODFIS = space(16)
      .w_ANPERFIS = space(1)
      .w_ANDESCRI = space(40)
      .w_ANCOGNOM = space(40)
      .w_AN__NOME = space(20)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANNAZION = space(3)
      .w_NACODEST = space(5)
      .w_ANLOCALI = space(30)
      .w_ANINDIRI = space(35)
      .w_SPTIPSOG = space(1)
      .w_SPTIPSOG = space(1)
      .w_SPESTPER = space(1)
      .w_SPSCLGEN = space(1)
      .w_SPDESCRI = space(60)
      .w_SPCOGNOM = space(40)
      .w_SP__NOME = space(40)
      .w_SPDATNAS = ctod("  /  /  ")
      .w_SPCOMEST = space(40)
      .w_SPPRONAS = space(2)
      .w_SPSTADOM = space(3)
      .w_SPSTAEST = space(3)
      .w_SPCITEST = space(40)
      .w_SPINDEST = space(40)
      .w_SPPARIVA = space(25)
      .w_SPCODFIS = space(25)
      .w_SPCODGEN = space(10)
      .w_SPDACONT = space(1)
      .w_EDIT_DETIPSOG = .f.
      .w_CONTROLS = .f.
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_SP__ANNO = year(i_DATSYS)
        .w_SPTIPCON = 'C'
        .w_OBTEST = CTOD('01-01-1900')
        .w_SPCODCON = IIF(.w_SPTIPSOG='D',SPACE(15),.w_SPCODCON)
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_SPCODCON))
          .link_1_5('Full')
          endif
        .w_SCCODCLF = .w_SPCODCON
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_SCCODCLF))
          .link_1_6('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_CHCODCLF))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,25,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_25('Full')
          endif
          .DoRTCalc(26,28,.f.)
        .w_SPTIPSOG = IIF(EMPTY(.w_SPCODCON),'D',IIF(.w_ANFLPRIV='S' And .w_ANFLSGRE<>'S','P',IIF(.w_ANFLSGRE='S','N',IIF(.w_ANFLPRIV<>'S' And .w_ANFLSGRE<>'S','I',' '))))
        .w_SPTIPSOG = IIF(EMPTY(.w_SPCODCON),'D',IIF(.w_ANFLPRIV='S' And .w_ANFLSGRE<>'S','P',IIF(.w_ANFLSGRE='S','N',IIF(.w_ANFLPRIV<>'S' And .w_ANFLSGRE<>'S','I',' '))))
        .w_SPESTPER = 'N'
        .w_SPSCLGEN = 'N'
        .w_SPDESCRI = .w_ANDESCRI
        .w_SPCOGNOM = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCCOGSCN,LEFT(.w_ANCOGNOM,40)),SPACE(40))
        .w_SP__NOME = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCNOMSCN,LEFT(.w_AN__NOME,40)),SPACE(40))
        .w_SPDATNAS = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCDTNSCN,.w_ANDATNAS),CTOD('  -  -   '))
        .w_SPCOMEST = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCLCRSCN,.w_ANLOCNAS),SPACE(40))
        .w_SPPRONAS = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCPRNSCN,.w_ANPRONAS),SPACE(2))
        .w_SPSTADOM = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCSESDOM,LEFT(.w_NACODEST,3)),SPACE(3))
        .w_SPSTAEST = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'NS',LEFT(.w_NACODEST,3),SPACE(3))
        .w_SPCITEST = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'NS',.w_ANLOCALI,SPACE(40))
        .w_SPINDEST = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'NS',.w_ANINDIRI,SPACE(40))
        .w_SPPARIVA = ALLTRIM(.w_ANPARIVA)
        .w_SPCODFIS = .w_ANCODFIS
        .w_SPCODGEN = space(10)
        .w_SPDACONT = 'N'
        .w_EDIT_DETIPSOG = .T.
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATESTSP')
    this.DoRTCalc(48,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSP_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"ANTSP","i_codazi,w_SPSERIAL")
      .op_codazi = .w_codazi
      .op_SPSERIAL = .w_SPSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSP__ANNO_1_2.enabled = i_bVal
      .Page1.oPag.oSPTIPCON_1_3.enabled = i_bVal
      .Page1.oPag.oSPCODCON_1_5.enabled = i_bVal
      .Page1.oPag.oSPTIPSOG_1_29.enabled = i_bVal
      .Page1.oPag.oSPTIPSOG_1_30.enabled = i_bVal
      .Page1.oPag.oSPESTPER_1_31.enabled = i_bVal
      .Page1.oPag.oSPSCLGEN_1_33.enabled = i_bVal
      .Page1.oPag.oSPDESCRI_1_34.enabled = i_bVal
      .Page1.oPag.oSPCOGNOM_1_35.enabled = i_bVal
      .Page1.oPag.oSP__NOME_1_36.enabled = i_bVal
      .Page1.oPag.oSPDATNAS_1_50.enabled = i_bVal
      .Page1.oPag.oSPCOMEST_1_51.enabled = i_bVal
      .Page1.oPag.oSPPRONAS_1_52.enabled = i_bVal
      .Page1.oPag.oSPSTADOM_1_53.enabled = i_bVal
      .Page1.oPag.oSPSTAEST_1_54.enabled = i_bVal
      .Page1.oPag.oSPCITEST_1_56.enabled = i_bVal
      .Page1.oPag.oSPINDEST_1_57.enabled = i_bVal
      .Page1.oPag.oSPPARIVA_1_59.enabled = i_bVal
      .Page1.oPag.oSPCODFIS_1_61.enabled = i_bVal
      .Page1.oPag.oSPDACONT_1_70.enabled = i_bVal
    endwith
    this.GSAI_MSP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DATESTSP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAI_MSP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATESTSP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPSERIAL,"SPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SP__ANNO,"SP__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPTIPCON,"SPTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPCODCON,"SPCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPTIPSOG,"SPTIPSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPTIPSOG,"SPTIPSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPESTPER,"SPESTPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPSCLGEN,"SPSCLGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPDESCRI,"SPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPCOGNOM,"SPCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SP__NOME,"SP__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPDATNAS,"SPDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPCOMEST,"SPCOMEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPPRONAS,"SPPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPSTADOM,"SPSTADOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPSTAEST,"SPSTAEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPCITEST,"SPCITEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPINDEST,"SPINDEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPPARIVA,"SPPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPCODFIS,"SPCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPCODGEN,"SPCODGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPDACONT,"SPDACONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESTSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSP_IDX,2])
    i_lTable = "DATESTSP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DATESTSP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATESTSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DATESTSP_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"ANTSP","i_codazi,w_SPSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DATESTSP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATESTSP')
        i_extval=cp_InsertValODBCExtFlds(this,'DATESTSP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SPSERIAL,SP__ANNO,SPTIPCON,SPCODCON,SPTIPSOG"+;
                  ",SPESTPER,SPSCLGEN,SPDESCRI,SPCOGNOM,SP__NOME"+;
                  ",SPDATNAS,SPCOMEST,SPPRONAS,SPSTADOM,SPSTAEST"+;
                  ",SPCITEST,SPINDEST,SPPARIVA,SPCODFIS,SPCODGEN"+;
                  ",SPDACONT,UTCC,UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SPSERIAL)+;
                  ","+cp_ToStrODBC(this.w_SP__ANNO)+;
                  ","+cp_ToStrODBC(this.w_SPTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_SPCODCON)+;
                  ","+cp_ToStrODBC(this.w_SPTIPSOG)+;
                  ","+cp_ToStrODBC(this.w_SPESTPER)+;
                  ","+cp_ToStrODBC(this.w_SPSCLGEN)+;
                  ","+cp_ToStrODBC(this.w_SPDESCRI)+;
                  ","+cp_ToStrODBC(this.w_SPCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_SP__NOME)+;
                  ","+cp_ToStrODBC(this.w_SPDATNAS)+;
                  ","+cp_ToStrODBC(this.w_SPCOMEST)+;
                  ","+cp_ToStrODBC(this.w_SPPRONAS)+;
                  ","+cp_ToStrODBC(this.w_SPSTADOM)+;
                  ","+cp_ToStrODBC(this.w_SPSTAEST)+;
                  ","+cp_ToStrODBC(this.w_SPCITEST)+;
                  ","+cp_ToStrODBC(this.w_SPINDEST)+;
                  ","+cp_ToStrODBC(this.w_SPPARIVA)+;
                  ","+cp_ToStrODBC(this.w_SPCODFIS)+;
                  ","+cp_ToStrODBC(this.w_SPCODGEN)+;
                  ","+cp_ToStrODBC(this.w_SPDACONT)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATESTSP')
        i_extval=cp_InsertValVFPExtFlds(this,'DATESTSP')
        cp_CheckDeletedKey(i_cTable,0,'SPSERIAL',this.w_SPSERIAL)
        INSERT INTO (i_cTable);
              (SPSERIAL,SP__ANNO,SPTIPCON,SPCODCON,SPTIPSOG,SPESTPER,SPSCLGEN,SPDESCRI,SPCOGNOM,SP__NOME,SPDATNAS,SPCOMEST,SPPRONAS,SPSTADOM,SPSTAEST,SPCITEST,SPINDEST,SPPARIVA,SPCODFIS,SPCODGEN,SPDACONT,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SPSERIAL;
                  ,this.w_SP__ANNO;
                  ,this.w_SPTIPCON;
                  ,this.w_SPCODCON;
                  ,this.w_SPTIPSOG;
                  ,this.w_SPESTPER;
                  ,this.w_SPSCLGEN;
                  ,this.w_SPDESCRI;
                  ,this.w_SPCOGNOM;
                  ,this.w_SP__NOME;
                  ,this.w_SPDATNAS;
                  ,this.w_SPCOMEST;
                  ,this.w_SPPRONAS;
                  ,this.w_SPSTADOM;
                  ,this.w_SPSTAEST;
                  ,this.w_SPCITEST;
                  ,this.w_SPINDEST;
                  ,this.w_SPPARIVA;
                  ,this.w_SPCODFIS;
                  ,this.w_SPCODGEN;
                  ,this.w_SPDACONT;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DATESTSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DATESTSP_IDX,i_nConn)
      *
      * update DATESTSP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DATESTSP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SP__ANNO="+cp_ToStrODBC(this.w_SP__ANNO)+;
             ",SPTIPCON="+cp_ToStrODBC(this.w_SPTIPCON)+;
             ",SPCODCON="+cp_ToStrODBCNull(this.w_SPCODCON)+;
             ",SPTIPSOG="+cp_ToStrODBC(this.w_SPTIPSOG)+;
             ",SPESTPER="+cp_ToStrODBC(this.w_SPESTPER)+;
             ",SPSCLGEN="+cp_ToStrODBC(this.w_SPSCLGEN)+;
             ",SPDESCRI="+cp_ToStrODBC(this.w_SPDESCRI)+;
             ",SPCOGNOM="+cp_ToStrODBC(this.w_SPCOGNOM)+;
             ",SP__NOME="+cp_ToStrODBC(this.w_SP__NOME)+;
             ",SPDATNAS="+cp_ToStrODBC(this.w_SPDATNAS)+;
             ",SPCOMEST="+cp_ToStrODBC(this.w_SPCOMEST)+;
             ",SPPRONAS="+cp_ToStrODBC(this.w_SPPRONAS)+;
             ",SPSTADOM="+cp_ToStrODBC(this.w_SPSTADOM)+;
             ",SPSTAEST="+cp_ToStrODBC(this.w_SPSTAEST)+;
             ",SPCITEST="+cp_ToStrODBC(this.w_SPCITEST)+;
             ",SPINDEST="+cp_ToStrODBC(this.w_SPINDEST)+;
             ",SPPARIVA="+cp_ToStrODBC(this.w_SPPARIVA)+;
             ",SPCODFIS="+cp_ToStrODBC(this.w_SPCODFIS)+;
             ",SPCODGEN="+cp_ToStrODBC(this.w_SPCODGEN)+;
             ",SPDACONT="+cp_ToStrODBC(this.w_SPDACONT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DATESTSP')
        i_cWhere = cp_PKFox(i_cTable  ,'SPSERIAL',this.w_SPSERIAL  )
        UPDATE (i_cTable) SET;
              SP__ANNO=this.w_SP__ANNO;
             ,SPTIPCON=this.w_SPTIPCON;
             ,SPCODCON=this.w_SPCODCON;
             ,SPTIPSOG=this.w_SPTIPSOG;
             ,SPESTPER=this.w_SPESTPER;
             ,SPSCLGEN=this.w_SPSCLGEN;
             ,SPDESCRI=this.w_SPDESCRI;
             ,SPCOGNOM=this.w_SPCOGNOM;
             ,SP__NOME=this.w_SP__NOME;
             ,SPDATNAS=this.w_SPDATNAS;
             ,SPCOMEST=this.w_SPCOMEST;
             ,SPPRONAS=this.w_SPPRONAS;
             ,SPSTADOM=this.w_SPSTADOM;
             ,SPSTAEST=this.w_SPSTAEST;
             ,SPCITEST=this.w_SPCITEST;
             ,SPINDEST=this.w_SPINDEST;
             ,SPPARIVA=this.w_SPPARIVA;
             ,SPCODFIS=this.w_SPCODFIS;
             ,SPCODGEN=this.w_SPCODGEN;
             ,SPDACONT=this.w_SPDACONT;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAI_MSP : Saving
      this.GSAI_MSP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_SPSERIAL,"SPSERIAL";
             )
      this.GSAI_MSP.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAI_MSP : Deleting
    this.GSAI_MSP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_SPSERIAL,"SPSERIAL";
           )
    this.GSAI_MSP.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATESTSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DATESTSP_IDX,i_nConn)
      *
      * delete DATESTSP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SPSERIAL',this.w_SPSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_SPTIPSOG<>.w_SPTIPSOG
            .w_SPCODCON = IIF(.w_SPTIPSOG='D',SPACE(15),.w_SPCODCON)
          .link_1_5('Full')
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPCON<>.w_SPTIPCON
            .w_SCCODCLF = .w_SPCODCON
          .link_1_6('Full')
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPCON<>.w_SPTIPCON
          .link_1_7('Full')
        endif
        .DoRTCalc(8,24,.t.)
          .link_1_25('Full')
        .DoRTCalc(26,28,.t.)
        if .o_SPTIPCON<>.w_SPTIPCON.or. .o_SPCODCON<>.w_SPCODCON
            .w_SPTIPSOG = IIF(EMPTY(.w_SPCODCON),'D',IIF(.w_ANFLPRIV='S' And .w_ANFLSGRE<>'S','P',IIF(.w_ANFLSGRE='S','N',IIF(.w_ANFLPRIV<>'S' And .w_ANFLSGRE<>'S','I',' '))))
        endif
        if .o_SPTIPCON<>.w_SPTIPCON.or. .o_SPCODCON<>.w_SPCODCON
            .w_SPTIPSOG = IIF(EMPTY(.w_SPCODCON),'D',IIF(.w_ANFLPRIV='S' And .w_ANFLSGRE<>'S','P',IIF(.w_ANFLSGRE='S','N',IIF(.w_ANFLPRIV<>'S' And .w_ANFLSGRE<>'S','I',' '))))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG
          .Calculate_AJQKCDGKDI()
        endif
        .DoRTCalc(31,32,.t.)
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPDESCRI = .w_ANDESCRI
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPCOGNOM = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCCOGSCN,LEFT(.w_ANCOGNOM,40)),SPACE(40))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SP__NOME = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCNOMSCN,LEFT(.w_AN__NOME,40)),SPACE(40))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPDATNAS = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCDTNSCN,.w_ANDATNAS),CTOD('  -  -   '))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPCOMEST = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCLCRSCN,.w_ANLOCNAS),SPACE(40))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPPRONAS = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCPRNSCN,.w_ANPRONAS),SPACE(2))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPSTADOM = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'PS',IIF(Not Empty(NVL(.w_CHCODCLF,'')),.w_SCSESDOM,LEFT(.w_NACODEST,3)),SPACE(3))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPSTAEST = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'NS',LEFT(.w_NACODEST,3),SPACE(3))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPCITEST = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'NS',.w_ANLOCALI,SPACE(40))
        endif
        if .o_SPCODCON<>.w_SPCODCON.or. .o_SPTIPSOG<>.w_SPTIPSOG.or. .o_SPESTPER<>.w_SPESTPER
            .w_SPINDEST = IIF(.w_SPTIPSOG='N' AND .w_SPESTPER$'NS',.w_ANINDIRI,SPACE(40))
        endif
        if .o_SPCODCON<>.w_SPCODCON
            .w_SPPARIVA = ALLTRIM(.w_ANPARIVA)
        endif
        if .o_SPCODCON<>.w_SPCODCON
            .w_SPCODFIS = .w_ANCODFIS
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"ANTSP","i_codazi,w_SPSERIAL")
          .op_SPSERIAL = .w_SPSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(45,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_AJQKCDGKDI()
    with this
          * --- Controllo soggetto estero persona fisica/giuridica/altro
          .w_SPESTPER = iif(.w_SPTIPSOG='T','S',iif(.w_ANPERFIS='S' OR EMPTY(NVL(.w_SPCODCON,'')) AND !EMPTY(.w_SPCOGNOM) AND EMPTY(.w_SPDESCRI), 'P', iif(!EMPTY(NVL(.w_SPCODCON,'')) AND .w_ANPERFIS<>'S' AND Empty(NVL(.w_CHCODCLF,'')) OR EMPTY(NVL(.w_SPCODCON,'')) AND EMPTY(.w_SPCOGNOM),'N','S')))
    endwith
  endproc
  proc Calculate_QZHCUKRTAL()
    with this
          * --- Editabilit� campo Sptipsog
          .w_EDIT_DETIPSOG = Upper(this.GSAI_MSP.class)='STDDYNAMICCHILD' OR This.GSAI_MSP.w_Totnumdoc=0
          .w_CONTROLS = This.mEnablecontrols()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSPTIPCON_1_3.enabled = this.oPgFrm.Page1.oPag.oSPTIPCON_1_3.mCond()
    this.oPgFrm.Page1.oPag.oSPCODCON_1_5.enabled = this.oPgFrm.Page1.oPag.oSPCODCON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oSPTIPSOG_1_29.enabled = this.oPgFrm.Page1.oPag.oSPTIPSOG_1_29.mCond()
    this.oPgFrm.Page1.oPag.oSPTIPSOG_1_30.enabled = this.oPgFrm.Page1.oPag.oSPTIPSOG_1_30.mCond()
    this.oPgFrm.Page1.oPag.oSPESTPER_1_31.enabled = this.oPgFrm.Page1.oPag.oSPESTPER_1_31.mCond()
    this.oPgFrm.Page1.oPag.oSPDESCRI_1_34.enabled = this.oPgFrm.Page1.oPag.oSPDESCRI_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSPCOGNOM_1_35.enabled = this.oPgFrm.Page1.oPag.oSPCOGNOM_1_35.mCond()
    this.oPgFrm.Page1.oPag.oSP__NOME_1_36.enabled = this.oPgFrm.Page1.oPag.oSP__NOME_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSPDATNAS_1_50.enabled = this.oPgFrm.Page1.oPag.oSPDATNAS_1_50.mCond()
    this.oPgFrm.Page1.oPag.oSPCOMEST_1_51.enabled = this.oPgFrm.Page1.oPag.oSPCOMEST_1_51.mCond()
    this.oPgFrm.Page1.oPag.oSPPRONAS_1_52.enabled = this.oPgFrm.Page1.oPag.oSPPRONAS_1_52.mCond()
    this.oPgFrm.Page1.oPag.oSPSTADOM_1_53.enabled = this.oPgFrm.Page1.oPag.oSPSTADOM_1_53.mCond()
    this.oPgFrm.Page1.oPag.oSPSTAEST_1_54.enabled = this.oPgFrm.Page1.oPag.oSPSTAEST_1_54.mCond()
    this.oPgFrm.Page1.oPag.oSPCITEST_1_56.enabled = this.oPgFrm.Page1.oPag.oSPCITEST_1_56.mCond()
    this.oPgFrm.Page1.oPag.oSPINDEST_1_57.enabled = this.oPgFrm.Page1.oPag.oSPINDEST_1_57.mCond()
    this.oPgFrm.Page1.oPag.oSPPARIVA_1_59.enabled = this.oPgFrm.Page1.oPag.oSPPARIVA_1_59.mCond()
    this.oPgFrm.Page1.oPag.oSPCODFIS_1_61.enabled = this.oPgFrm.Page1.oPag.oSPCODFIS_1_61.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oSPPARIVA_1_59
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page1.oPag.oSPCODFIS_1_61
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSPTIPSOG_1_29.visible=!this.oPgFrm.Page1.oPag.oSPTIPSOG_1_29.mHide()
    this.oPgFrm.Page1.oPag.oSPTIPSOG_1_30.visible=!this.oPgFrm.Page1.oPag.oSPTIPSOG_1_30.mHide()
    this.oPgFrm.Page1.oPag.oSPESTPER_1_31.visible=!this.oPgFrm.Page1.oPag.oSPESTPER_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsai_asp
    * Valorizzo il tipo soggetto sul dettaglio dati estratti
    IF Upper(CEVENT)='ACTIVATEPAGE 2'
       This.GSAI_MSP.w_Sptipsog = This.w_Sptipsog
       if Upper(this.GSAI_MSP.class)='STDDYNAMICCHILD' OR This.GSAI_MSP.w_Totnumdoc=0
         This.GSAI_MSP.w_Sptiprec = iif(This.w_Sptipsog='T','TU','FE')
       endif
       This.Gsai_Msp.SetControlsValue() 
       This.Gsai_Msp.mcalc(.t.)
    endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ActivatePage 1") or lower(cEvent)==lower("Edit Started")
          .Calculate_QZHCUKRTAL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SPCODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SPCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SPTIPCON;
                     ,'ANCODICE',trim(this.w_SPCODCON))
          select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SPCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SPCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSPCODCON_1_5'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SPCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SPCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SPTIPCON;
                       ,'ANCODICE',this.w_SPCODCON)
            select ANTIPCON,ANCODICE,ANPARIVA,ANFLPRIV,ANFLSGRE,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,ANDATNAS,ANLOCNAS,ANPRONAS,ANNAZION,ANLOCALI,ANINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SPCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANPARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_ANFLPRIV = NVL(_Link_.ANFLPRIV,space(1))
      this.w_ANFLSGRE = NVL(_Link_.ANFLSGRE,space(1))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
      this.w_ANPERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANCOGNOM = NVL(_Link_.ANCOGNOM,space(40))
      this.w_AN__NOME = NVL(_Link_.AN__NOME,space(20))
      this.w_ANDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_ANLOCNAS = NVL(_Link_.ANLOCNAS,space(30))
      this.w_ANPRONAS = NVL(_Link_.ANPRONAS,space(2))
      this.w_ANNAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANLOCALI = NVL(_Link_.ANLOCALI,space(30))
      this.w_ANINDIRI = NVL(_Link_.ANINDIRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SPCODCON = space(15)
      endif
      this.w_ANPARIVA = space(12)
      this.w_ANFLPRIV = space(1)
      this.w_ANFLSGRE = space(1)
      this.w_ANCODFIS = space(16)
      this.w_ANPERFIS = space(1)
      this.w_ANDESCRI = space(40)
      this.w_ANCOGNOM = space(40)
      this.w_AN__NOME = space(20)
      this.w_ANDATNAS = ctod("  /  /  ")
      this.w_ANLOCNAS = space(30)
      this.w_ANPRONAS = space(2)
      this.w_ANNAZION = space(3)
      this.w_ANLOCALI = space(30)
      this.w_ANINDIRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SPCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 15 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+15<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ANCODICE as ANCODICE105"+ ",link_1_5.ANPARIVA as ANPARIVA105"+ ",link_1_5.ANFLPRIV as ANFLPRIV105"+ ",link_1_5.ANFLSGRE as ANFLSGRE105"+ ",link_1_5.ANCODFIS as ANCODFIS105"+ ",link_1_5.ANPERFIS as ANPERFIS105"+ ",link_1_5.ANDESCRI as ANDESCRI105"+ ",link_1_5.ANCOGNOM as ANCOGNOM105"+ ",link_1_5.AN__NOME as AN__NOME105"+ ",link_1_5.ANDATNAS as ANDATNAS105"+ ",link_1_5.ANLOCNAS as ANLOCNAS105"+ ",link_1_5.ANPRONAS as ANPRONAS105"+ ",link_1_5.ANNAZION as ANNAZION105"+ ",link_1_5.ANLOCALI as ANLOCALI105"+ ",link_1_5.ANINDIRI as ANINDIRI105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on DATESTSP.SPCODCON=link_1_5.ANCODICE"+" and DATESTSP.SPTIPCON=link_1_5.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and DATESTSP.SPCODCON=link_1_5.ANCODICE(+)"'+'+" and DATESTSP.SPTIPCON=link_1_5.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODCLF
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_lTable = "SOGCOLNR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2], .t., this.SOGCOLNR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODCLF="+cp_ToStrODBC(this.w_SCCODCLF);
                   +" and SCTIPCLF="+cp_ToStrODBC(this.w_SPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCTIPCLF',this.w_SPTIPCON;
                       ,'SCCODCLF',this.w_SCCODCLF)
            select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_SCCOGSCN = NVL(_Link_.SCCOGSCN,space(24))
      this.w_SCNOMSCN = NVL(_Link_.SCNOMSCN,space(20))
      this.w_SCDTNSCN = NVL(cp_ToDate(_Link_.SCDTNSCN),ctod("  /  /  "))
      this.w_SCLCRSCN = NVL(_Link_.SCLCRSCN,space(40))
      this.w_SCPRNSCN = NVL(_Link_.SCPRNSCN,space(2))
      this.w_SCSESDOM = NVL(_Link_.SCSESDOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCLF = space(15)
      endif
      this.w_CHCODCLF = space(15)
      this.w_SCCOGSCN = space(24)
      this.w_SCNOMSCN = space(20)
      this.w_SCDTNSCN = ctod("  /  /  ")
      this.w_SCLCRSCN = space(40)
      this.w_SCPRNSCN = space(2)
      this.w_SCSESDOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])+'\'+cp_ToStr(_Link_.SCTIPCLF,1)+'\'+cp_ToStr(_Link_.SCCODCLF,1)
      cp_ShowWarn(i_cKey,this.SOGCOLNR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CHCODCLF
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_lTable = "SOGCOLNR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2], .t., this.SOGCOLNR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODCLF="+cp_ToStrODBC(this.w_CHCODCLF);
                   +" and SCTIPCLF="+cp_ToStrODBC(this.w_SPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCTIPCLF',this.w_SPTIPCON;
                       ,'SCCODCLF',this.w_CHCODCLF)
            select SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_CHCODCLF = NVL(_Link_.SCCODCLF,space(15))
      this.w_SCCOGSCN = NVL(_Link_.SCCOGSCN,space(24))
      this.w_SCNOMSCN = NVL(_Link_.SCNOMSCN,space(20))
      this.w_SCDTNSCN = NVL(cp_ToDate(_Link_.SCDTNSCN),ctod("  /  /  "))
      this.w_SCLCRSCN = NVL(_Link_.SCLCRSCN,space(40))
      this.w_SCPRNSCN = NVL(_Link_.SCPRNSCN,space(2))
      this.w_SCSESDOM = NVL(_Link_.SCSESDOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CHCODCLF = space(15)
      endif
      this.w_CHCODCLF = space(15)
      this.w_SCCOGSCN = space(24)
      this.w_SCNOMSCN = space(20)
      this.w_SCDTNSCN = ctod("  /  /  ")
      this.w_SCLCRSCN = space(40)
      this.w_SCPRNSCN = space(2)
      this.w_SCSESDOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])+'\'+cp_ToStr(_Link_.SCTIPCLF,1)+'\'+cp_ToStr(_Link_.SCCODCLF,1)
      cp_ShowWarn(i_cKey,this.SOGCOLNR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODEST";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NACODEST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_NACODEST = NVL(_Link_.NACODEST,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_NACODEST = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSP__ANNO_1_2.value==this.w_SP__ANNO)
      this.oPgFrm.Page1.oPag.oSP__ANNO_1_2.value=this.w_SP__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oSPTIPCON_1_3.RadioValue()==this.w_SPTIPCON)
      this.oPgFrm.Page1.oPag.oSPTIPCON_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPCODCON_1_5.value==this.w_SPCODCON)
      this.oPgFrm.Page1.oPag.oSPCODCON_1_5.value=this.w_SPCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSPTIPSOG_1_29.RadioValue()==this.w_SPTIPSOG)
      this.oPgFrm.Page1.oPag.oSPTIPSOG_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPTIPSOG_1_30.RadioValue()==this.w_SPTIPSOG)
      this.oPgFrm.Page1.oPag.oSPTIPSOG_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPESTPER_1_31.RadioValue()==this.w_SPESTPER)
      this.oPgFrm.Page1.oPag.oSPESTPER_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPSCLGEN_1_33.RadioValue()==this.w_SPSCLGEN)
      this.oPgFrm.Page1.oPag.oSPSCLGEN_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPDESCRI_1_34.value==this.w_SPDESCRI)
      this.oPgFrm.Page1.oPag.oSPDESCRI_1_34.value=this.w_SPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSPCOGNOM_1_35.value==this.w_SPCOGNOM)
      this.oPgFrm.Page1.oPag.oSPCOGNOM_1_35.value=this.w_SPCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSP__NOME_1_36.value==this.w_SP__NOME)
      this.oPgFrm.Page1.oPag.oSP__NOME_1_36.value=this.w_SP__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oSPDATNAS_1_50.value==this.w_SPDATNAS)
      this.oPgFrm.Page1.oPag.oSPDATNAS_1_50.value=this.w_SPDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oSPCOMEST_1_51.value==this.w_SPCOMEST)
      this.oPgFrm.Page1.oPag.oSPCOMEST_1_51.value=this.w_SPCOMEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSPPRONAS_1_52.value==this.w_SPPRONAS)
      this.oPgFrm.Page1.oPag.oSPPRONAS_1_52.value=this.w_SPPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oSPSTADOM_1_53.value==this.w_SPSTADOM)
      this.oPgFrm.Page1.oPag.oSPSTADOM_1_53.value=this.w_SPSTADOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSPSTAEST_1_54.value==this.w_SPSTAEST)
      this.oPgFrm.Page1.oPag.oSPSTAEST_1_54.value=this.w_SPSTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSPCITEST_1_56.value==this.w_SPCITEST)
      this.oPgFrm.Page1.oPag.oSPCITEST_1_56.value=this.w_SPCITEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSPINDEST_1_57.value==this.w_SPINDEST)
      this.oPgFrm.Page1.oPag.oSPINDEST_1_57.value=this.w_SPINDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSPPARIVA_1_59.value==this.w_SPPARIVA)
      this.oPgFrm.Page1.oPag.oSPPARIVA_1_59.value=this.w_SPPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oSPCODFIS_1_61.value==this.w_SPCODFIS)
      this.oPgFrm.Page1.oPag.oSPCODFIS_1_61.value=this.w_SPCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSPDACONT_1_70.RadioValue()==this.w_SPDACONT)
      this.oPgFrm.Page1.oPag.oSPDACONT_1_70.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DATESTSP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_SP__ANNO)) or not(.w_SP__ANNO>2005 and .w_SP__ANNO<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSP__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_SP__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPTIPSOG))  and not(! .w_Edit_Detipsog And .w_Sptipsog$'IPN')  and (.w_Edit_Detipsog And .cFunction<>'Query')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPTIPSOG_1_29.SetFocus()
            i_bnoObbl = !empty(.w_SPTIPSOG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPTIPSOG))  and not(.w_Edit_Detipsog Or .w_Sptipsog='T' Or .w_Sptipsog='D')  and (.cFunction<>'Query')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPTIPSOG_1_30.SetFocus()
            i_bnoObbl = !empty(.w_SPTIPSOG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPDESCRI))  and (.cFunction<>'Query' And .w_SPTIPSOG='N' And .w_SPESTPER$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPDESCRI_1_34.SetFocus()
            i_bnoObbl = !empty(.w_SPDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPCOGNOM))  and (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPCOGNOM_1_35.SetFocus()
            i_bnoObbl = !empty(.w_SPCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SP__NOME))  and (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSP__NOME_1_36.SetFocus()
            i_bnoObbl = !empty(.w_SP__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPDATNAS))  and (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPDATNAS_1_50.SetFocus()
            i_bnoObbl = !empty(.w_SPDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPCOMEST))  and (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPCOMEST_1_51.SetFocus()
            i_bnoObbl = !empty(.w_SPCOMEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPPRONAS))  and (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPPRONAS_1_52.SetFocus()
            i_bnoObbl = !empty(.w_SPPRONAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPSTADOM))  and (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPSTADOM_1_53.SetFocus()
            i_bnoObbl = !empty(.w_SPSTADOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPSTAEST))  and (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'NS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPSTAEST_1_54.SetFocus()
            i_bnoObbl = !empty(.w_SPSTAEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPPARIVA) and (.w_SPTIPSOG='I'))  and (.cFunction<>'Query'  )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPPARIVA_1_59.SetFocus()
            i_bnoObbl = !empty(.w_SPPARIVA) or !(.w_SPTIPSOG='I')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SPCODFIS) and (.w_SPTIPSOG='P'))  and (.cFunction<>'Query'  )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPCODFIS_1_61.SetFocus()
            i_bnoObbl = !empty(.w_SPCODFIS) or !(.w_SPTIPSOG='P')
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAI_MSP.CheckForm()
      if i_bres
        i_bres=  .GSAI_MSP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SPTIPCON = this.w_SPTIPCON
    this.o_SPCODCON = this.w_SPCODCON
    this.o_SPTIPSOG = this.w_SPTIPSOG
    this.o_SPESTPER = this.w_SPESTPER
    * --- GSAI_MSP : Depends On
    this.GSAI_MSP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsai_aspPag1 as StdContainer
  Width  = 1100
  height = 572
  stdWidth  = 1100
  stdheight = 572
  resizeXpos=408
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSP__ANNO_1_2 as StdField with uid="HYNPMMDCOL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SP__ANNO", cQueryName = "SP__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 68469131,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=283, Top=21, cSayPict='"9999"', cGetPict='"9999"'

  func oSP__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SP__ANNO>2005 and .w_SP__ANNO<2100)
    endwith
    return bRes
  endfunc


  add object oSPTIPCON_1_3 as StdCombo with uid="FAVUWBBZYH",rtseq=3,rtrep=.f.,left=286,top=101,width=118,height=21;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 238776716;
    , cFormVar="w_SPTIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPTIPCON_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSPTIPCON_1_3.GetRadio()
    this.Parent.oContained.w_SPTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPCON_1_3.SetRadio()
    this.Parent.oContained.w_SPTIPCON=trim(this.Parent.oContained.w_SPTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_SPTIPCON=='C',1,;
      iif(this.Parent.oContained.w_SPTIPCON=='F',2,;
      0))
  endfunc

  func oSPTIPCON_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oSPTIPCON_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SPCODCON)
        bRes2=.link_1_5('Full')
      endif
      if .not. empty(.w_SCCODCLF)
        bRes2=.link_1_6('Full')
      endif
      if .not. empty(.w_CHCODCLF)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSPCODCON_1_5 as StdField with uid="EAHXPIIFWI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SPCODCON", cQueryName = "SPCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 251036044,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=422, Top=101, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SPCODCON"

  func oSPCODCON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oSPCODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSPCODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSPCODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSPCODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oSPCODCON_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SPTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SPCODCON
     i_obj.ecpSave()
  endproc


  add object oSPTIPSOG_1_29 as StdCombo with uid="ZEGKZBAVTV",rtseq=29,rtrep=.f.,left=671,top=100,width=178,height=22;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 238776723;
    , cFormVar="w_SPTIPSOG",RowSource=""+"Iva,"+"Privato,"+"Non residente,"+"Documento riepilogativo,"+"Operazione turismo", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSPTIPSOG_1_29.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'N',;
    iif(this.value =4,'D',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oSPTIPSOG_1_29.GetRadio()
    this.Parent.oContained.w_SPTIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPSOG_1_29.SetRadio()
    this.Parent.oContained.w_SPTIPSOG=trim(this.Parent.oContained.w_SPTIPSOG)
    this.value = ;
      iif(this.Parent.oContained.w_SPTIPSOG=='I',1,;
      iif(this.Parent.oContained.w_SPTIPSOG=='P',2,;
      iif(this.Parent.oContained.w_SPTIPSOG=='N',3,;
      iif(this.Parent.oContained.w_SPTIPSOG=='D',4,;
      iif(this.Parent.oContained.w_SPTIPSOG=='T',5,;
      0)))))
  endfunc

  func oSPTIPSOG_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Edit_Detipsog And .cFunction<>'Query')
    endwith
   endif
  endfunc

  func oSPTIPSOG_1_29.mHide()
    with this.Parent.oContained
      return (! .w_Edit_Detipsog And .w_Sptipsog$'IPN')
    endwith
  endfunc


  add object oSPTIPSOG_1_30 as StdCombo with uid="CWECBDCLBP",rtseq=30,rtrep=.f.,left=671,top=100,width=178,height=22;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 238776723;
    , cFormVar="w_SPTIPSOG",RowSource=""+"Iva,"+"Privato,"+"Non residente", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSPTIPSOG_1_30.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oSPTIPSOG_1_30.GetRadio()
    this.Parent.oContained.w_SPTIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPSOG_1_30.SetRadio()
    this.Parent.oContained.w_SPTIPSOG=trim(this.Parent.oContained.w_SPTIPSOG)
    this.value = ;
      iif(this.Parent.oContained.w_SPTIPSOG=='I',1,;
      iif(this.Parent.oContained.w_SPTIPSOG=='P',2,;
      iif(this.Parent.oContained.w_SPTIPSOG=='N',3,;
      0)))
  endfunc

  func oSPTIPSOG_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oSPTIPSOG_1_30.mHide()
    with this.Parent.oContained
      return (.w_Edit_Detipsog Or .w_Sptipsog='T' Or .w_Sptipsog='D')
    endwith
  endfunc


  add object oSPESTPER_1_31 as StdCombo with uid="IZUNFFGLMN",rtseq=31,rtrep=.f.,left=671,top=127,width=178,height=22;
    , ToolTipText = "Abilita campi persona fisica o persona giuridica nel caso di soggetto estero";
    , HelpContextID = 252550776;
    , cFormVar="w_SPESTPER",RowSource=""+"Persona giuridica,"+"Persona fisica,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSPESTPER_1_31.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oSPESTPER_1_31.GetRadio()
    this.Parent.oContained.w_SPESTPER = this.RadioValue()
    return .t.
  endfunc

  func oSPESTPER_1_31.SetRadio()
    this.Parent.oContained.w_SPESTPER=trim(this.Parent.oContained.w_SPESTPER)
    this.value = ;
      iif(this.Parent.oContained.w_SPESTPER=='N',1,;
      iif(this.Parent.oContained.w_SPESTPER=='P',2,;
      iif(this.Parent.oContained.w_SPESTPER=='S',3,;
      0)))
  endfunc

  func oSPESTPER_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SPTIPSOG='N')
    endwith
   endif
  endfunc

  func oSPESTPER_1_31.mHide()
    with this.Parent.oContained
      return (.w_SPTIPSOG $ 'IPD')
    endwith
  endfunc

  add object oSPSCLGEN_1_33 as StdCheck with uid="FZBDCCDEUF",rtseq=32,rtrep=.f.,left=545, top=24, caption="Escludi da generazione",;
    ToolTipText = "Se attivo, esclude i dati estratti dalla generazione",;
    HelpContextID = 92175988,;
    cFormVar="w_SPSCLGEN", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oSPSCLGEN_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSPSCLGEN_1_33.GetRadio()
    this.Parent.oContained.w_SPSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oSPSCLGEN_1_33.SetRadio()
    this.Parent.oContained.w_SPSCLGEN=trim(this.Parent.oContained.w_SPSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_SPSCLGEN=='S',1,;
      0)
  endfunc

  add object oSPDESCRI_1_34 as StdField with uid="UNDUYNBBES",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SPDESCRI", cQueryName = "SPDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale",;
    HelpContextID = 235958673,;
   bGlobalFont=.t.,;
    Height=21, Width=572, Left=286, Top=170, InputMask=replicate('X',60)

  func oSPDESCRI_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG='N' And .w_SPESTPER$'NS')
    endwith
   endif
  endfunc

  add object oSPCOGNOM_1_35 as StdField with uid="VBWHBYPETD",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SPCOGNOM", cQueryName = "SPCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 63340941,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=286, Top=210, InputMask=replicate('X',40)

  func oSPCOGNOM_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
    endwith
   endif
  endfunc

  add object oSP__NOME_1_36 as StdField with uid="TZTOWOZVVJ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_SP__NOME", cQueryName = "SP__NOME",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 38060437,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=586, Top=210, InputMask=replicate('X',40)

  func oSP__NOME_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
    endwith
   endif
  endfunc

  add object oSPDATNAS_1_50 as StdField with uid="VTKSSEXLQD",rtseq=36,rtrep=.f.,;
    cFormVar = "w_SPDATNAS", cQueryName = "SPDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 50622855,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=286, Top=250

  func oSPDATNAS_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
    endwith
   endif
  endfunc

  add object oSPCOMEST_1_51 as StdField with uid="OEGUAGVHVP",rtseq=37,rtrep=.f.,;
    cFormVar = "w_SPCOMEST", cQueryName = "SPCOMEST",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 208044422,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=399, Top=250, InputMask=replicate('X',40), bHasZoom = .t. 

  func oSPCOMEST_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
    endwith
   endif
  endfunc

  proc oSPCOMEST_1_51.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_SPCOMEST",".w_SPPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSPPRONAS_1_52 as StdField with uid="HCPUHURXUB",rtseq=38,rtrep=.f.,;
    cFormVar = "w_SPPRONAS", cQueryName = "SPPRONAS",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 54702471,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=766, Top=250, InputMask=replicate('X',2), bHasZoom = .t. 

  func oSPPRONAS_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
    endwith
   endif
  endfunc

  proc oSPPRONAS_1_52.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_SPPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSPSTADOM_1_53 as StdField with uid="JZJZNJGHQI",rtseq=39,rtrep=.f.,;
    cFormVar = "w_SPSTADOM", cQueryName = "SPSTADOM",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero del domicilio fiscale",;
    HelpContextID = 237011341,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=286, Top=291, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oSPSTADOM_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'PS')
    endwith
   endif
  endfunc

  add object oSPSTAEST_1_54 as StdField with uid="VNIBPXSZVB",rtseq=40,rtrep=.f.,;
    cFormVar = "w_SPSTAEST", cQueryName = "SPSTAEST",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero della sede legale",;
    HelpContextID = 220234118,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=286, Top=343, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oSPSTAEST_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'NS')
    endwith
   endif
  endfunc

  add object oSPCITEST_1_56 as StdField with uid="VGAZWLEGEC",rtseq=41,rtrep=.f.,;
    cFormVar = "w_SPCITEST", cQueryName = "SPCITEST",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Citt� estera della sede legale",;
    HelpContextID = 201097606,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=396, Top=343, InputMask=replicate('X',40)

  func oSPCITEST_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'NS')
    endwith
   endif
  endfunc

  add object oSPINDEST_1_57 as StdField with uid="PPBQNCJMIR",rtseq=42,rtrep=.f.,;
    cFormVar = "w_SPINDEST", cQueryName = "SPINDEST",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero della sede legale",;
    HelpContextID = 217522566,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=286, Top=385, InputMask=replicate('X',40)

  func oSPINDEST_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SPTIPSOG$'NT' AND .w_SPESTPER$'NS')
    endwith
   endif
  endfunc

  add object oSPPARIVA_1_59 as StdField with uid="AOJRIDAGNZ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SPPARIVA", cQueryName = "SPPARIVA",;
    bObbl = .t. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 131878503,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=286, Top=442, InputMask=replicate('X',25)

  func oSPPARIVA_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query'  )
    endwith
   endif
  endfunc

  func oSPPARIVA_1_59.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_SPTIPSOG='I'
    endwith
    return i_bres
  endfunc

  add object oSPCODFIS_1_61 as StdField with uid="NEICLLJLQF",rtseq=44,rtrep=.f.,;
    cFormVar = "w_SPCODFIS", cQueryName = "SPCODFIS",;
    bObbl = .t. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 67731065,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=605, Top=442, InputMask=replicate('X',25)

  func oSPCODFIS_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query'  )
    endwith
   endif
  endfunc

  func oSPCODFIS_1_61.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_SPTIPSOG='P'
    endwith
    return i_bres
  endfunc

  add object oSPDACONT_1_70 as StdCheck with uid="NMLXKBMXSE",rtseq=46,rtrep=.f.,left=545, top=50, caption="Da verificare",;
    ToolTipText = "Se attivo, i dati estratti sono da verificare",;
    HelpContextID = 51671430,;
    cFormVar="w_SPDACONT", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oSPDACONT_1_70.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSPDACONT_1_70.GetRadio()
    this.Parent.oContained.w_SPDACONT = this.RadioValue()
    return .t.
  endfunc

  func oSPDACONT_1_70.SetRadio()
    this.Parent.oContained.w_SPDACONT=trim(this.Parent.oContained.w_SPDACONT)
    this.value = ;
      iif(this.Parent.oContained.w_SPDACONT=='S',1,;
      0)
  endfunc

  add object oStr_1_37 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=206, Top=21,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="FZWRBKVHFS",Visible=.t., Left=20, Top=23,;
    Alignment=0, Width=118, Height=18,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=135, Top=135,;
    Alignment=0, Width=111, Height=19,;
    Caption="Dati anagrafici"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="FMGZLDBDTV",Visible=.t., Left=566, Top=105,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="XCNUOVRHIB",Visible=.t., Left=135, Top=442,;
    Alignment=0, Width=111, Height=18,;
    Caption="Identificativi fiscali"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="LHYQUAFBCO",Visible=.t., Left=288, Top=429,;
    Alignment=0, Width=158, Height=13,;
    Caption="Partita IVA"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="BSYUVARGFL",Visible=.t., Left=605, Top=429,;
    Alignment=0, Width=158, Height=13,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="KIZOFHIWHV",Visible=.t., Left=286, Top=154,;
    Alignment=0, Width=130, Height=17,;
    Caption="Ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_SPTIPSOG<>'N')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="KOKISJQUOL",Visible=.t., Left=286, Top=194,;
    Alignment=0, Width=130, Height=17,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="LIFDJEITLU",Visible=.t., Left=587, Top=194,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=286, Top=236,;
    Alignment=0, Width=76, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_58 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=399, Top=236,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune (o stato estero) di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="DMKPRROUYT",Visible=.t., Left=758, Top=236,;
    Alignment=0, Width=52, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_62 as StdString with uid="NOWYYKZWAZ",Visible=.t., Left=286, Top=278,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_63 as StdString with uid="TPWEYQUKDS",Visible=.t., Left=135, Top=281,;
    Alignment=0, Width=111, Height=18,;
    Caption="Domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="KBZOHAZPBS",Visible=.t., Left=135, Top=351,;
    Alignment=0, Width=111, Height=18,;
    Caption="Sede legale"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="HYIMBFNHGG",Visible=.t., Left=286, Top=328,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_67 as StdString with uid="WQAUIMBAAE",Visible=.t., Left=396, Top=328,;
    Alignment=0, Width=83, Height=13,;
    Caption="Citt� estera"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_68 as StdString with uid="HGABSVUXLY",Visible=.t., Left=288, Top=371,;
    Alignment=0, Width=83, Height=13,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="FALBAPPRTT",Visible=.t., Left=422, Top=129,;
    Alignment=0, Width=178, Height=18,;
    Caption="Soggetto collettivo non residente"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (Empty(NVL(.w_CHCODCLF,'')) OR .w_SPTIPSOG<>'N')
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="IKBATICWMT",Visible=.t., Left=286, Top=154,;
    Alignment=0, Width=130, Height=17,;
    Caption="Descrizione soggetto"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (.w_SPTIPSOG='N')
    endwith
  endfunc

  add object oBox_1_39 as StdBox with uid="RDMWYOEEFE",left=129, top=92, width=763,height=394

  add object oBox_1_41 as StdBox with uid="PIUTDLGCGZ",left=277, top=92, width=1,height=391

  add object oBox_1_46 as StdBox with uid="OFIETXWFMW",left=130, top=418, width=760,height=1

  add object oBox_1_64 as StdBox with uid="URMYZLQHZM",left=130, top=317, width=760,height=1
enddefine
define class tgsai_aspPag2 as StdContainer
  Width  = 1100
  height = 572
  stdWidth  = 1100
  stdheight = 572
  resizeXpos=614
  resizeYpos=295
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="VNMLGJNPFU",left=0, top=6, width=1098, height=566, bOnScreen=.t.;

enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_asp','DATESTSP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SPSERIAL=DATESTSP.SPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
