* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bsd                                                        *
*              Ripulisci identificativo comunciazione                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-07-09                                                      *
* Last revis.: 2011-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bsd",oParentObject)
return(i_retval)

define class tgsai_bsd as StdBatch
  * --- Local variables
  * --- WorkFile variables
  ANTIVADE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_03805DF0
    bErr_03805DF0=bTrsErr
    this.Try_03805DF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03805DF0
    * --- End
  endproc
  proc Try_03805DF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ANTIVADE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ANTIVADE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANTIVADE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ANTIVADE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DECODGEN ="+cp_NullLink(cp_ToStrODBC(space(10)),'ANTIVADE','DECODGEN');
          +i_ccchkf ;
      +" where ";
          +"DECODGEN = "+cp_ToStrODBC(this.oParentObject.w_AISERIAL);
             )
    else
      update (i_cTable) set;
          DECODGEN = space(10);
          &i_ccchkf. ;
       where;
          DECODGEN = this.oParentObject.w_AISERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ANTIVADE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
