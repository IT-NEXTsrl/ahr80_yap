* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_agt                                                        *
*              Generazione file comunicazione polivalente                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-25                                                      *
* Last revis.: 2015-04-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsai_agt
L_descri1='1) Rappr. legale o negoziale'
L_descri2='2) Rappr. di minore - curatore eredit�'
L_descri3='3) Curatore fallimentare'
L_descri4='4) Commissario liquidatore'
L_descri5='5) Commissario giudiziale'
L_descri6='6) Rappr. fiscale di soggetto non residente'
L_descri7='7) Erede del dichiarante'
L_descri8='8) Liquidatore volontario'
L_descri9='9) Sogg. dich. IVA operaz. straord.'
L_descri10='10) Rappr. fisc. sogg. non resid. D.L.331/1993'
L_descri11='11) Tutore'
L_descri12='12) Liquid. volont. ditta indiv.'
* --- Fine Area Manuale
return(createobject("tgsai_agt"))

* --- Class definition
define class tgsai_agt as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 794
  Height = 421+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-04-20"
  HelpContextID=42600297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=94

  * --- Constant Properties
  GENFILTE_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  SEDIAZIE_IDX = 0
  DATPARSP_IDX = 0
  DATPARSM_IDX = 0
  cFile = "GENFILTE"
  cKeySelect = "ATSERIAL"
  cKeyWhere  = "ATSERIAL=this.w_ATSERIAL"
  cKeyWhereODBC = '"ATSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)';

  cKeyWhereODBCqualified = '"GENFILTE.ATSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)';

  cPrg = "gsai_agt"
  cComment = "Generazione file comunicazione polivalente"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ATSERIAL = space(10)
  w_ATGENSPE = space(1)
  o_ATGENSPE = space(1)
  w_ATGENBLK = space(1)
  o_ATGENBLK = space(1)
  w_ATGENSMA = space(1)
  o_ATGENSMA = space(1)
  w_AT__ANNO = 0
  o_AT__ANNO = 0
  w_ATTIPPER = space(1)
  o_ATTIPPER = space(1)
  w_ATTIPPER = space(1)
  w_AT__MESE = 0
  o_AT__MESE = 0
  w_ATTRIMES = 0
  o_ATTRIMES = 0
  w_PA__ANNO = space(4)
  w_SM_ANNO = space(4)
  w_ATIMPMIN = space(1)
  w_ATCONIMP = space(1)
  w_ATSCLIMP = space(1)
  w_ANNO = 0
  w_SERIALE = space(10)
  o_SERIALE = space(10)
  w_ATTIPINV = 0
  o_ATTIPINV = 0
  w_ATPROTEC = space(17)
  w_ATPRODOC = 0
  w_ATTIPAGR = space(1)
  o_ATTIPAGR = space(1)
  w_ATQUADFA = space(1)
  w_ATQUADSA = space(1)
  w_ATQUAATU = space(1)
  w_ATQUADBL = space(1)
  w_ATTIPDAT = space(1)
  o_ATTIPDAT = space(1)
  w_ATQUADFE = space(1)
  w_ATQUADFR = space(1)
  w_ATQUADNE = space(1)
  w_ATQUADNR = space(1)
  w_ATQUADDF = space(1)
  w_ATQUADFN = space(1)
  w_ATQUADTU = space(1)
  w_ATQUADSE = space(1)
  w_ATIMPMAX = space(1)
  w_ATIVANOS = space(1)
  w_SMIMPMIN = space(1)
  w_SMSCLIMP = space(1)
  w_SMIMPMAX = space(1)
  w_CODAZI = space(5)
  w_AZPIVAZI = space(12)
  w_AZCOFAZI = space(16)
  w_AZPERAZI = space(1)
  w_AZCODNAZ = space(3)
  w_AZLOCAZI = space(30)
  w_AZPROAZI = space(2)
  w_AZNAGAZI = space(5)
  w_AZTELEFO = space(18)
  w_AZTELFAX = space(18)
  w_AZ_EMAIL = space(254)
  w_AZIVACOF = space(16)
  w_CODAZI2 = space(5)
  w_COGTIT1 = space(25)
  w_NOMTIT1 = space(25)
  w_DATNAS1 = ctod('  /  /  ')
  w_LOCTIT1 = space(30)
  w_PROTIT1 = space(2)
  w_SESSO1 = space(1)
  w_TELEFO1 = space(18)
  w_ATCODFIS = space(16)
  w_ATPARIVA = space(11)
  w_ATCODATT = space(6)
  w_AT__MAIL = space(50)
  w_ATTELEFO = space(12)
  w_ATNUMFAX = space(12)
  w_ATCOGNOM = space(24)
  w_AT__NOME = space(20)
  w_AT_SESSO = space(1)
  w_ATDATNAS = ctod('  /  /  ')
  w_ATLOCNAS = space(40)
  w_ATPRONAS = space(2)
  w_ATRAGSOC = space(60)
  w_FORZA_EDIT = .F.
  o_FORZA_EDIT = .F.
  w_ATCFISOT = space(16)
  o_ATCFISOT = space(16)
  w_ATCODCAR = space(2)
  w_ATDATIPR = ctod('  /  /  ')
  w_ATDATFPR = ctod('  /  /  ')
  w_ATCOGSOT = space(24)
  o_ATCOGSOT = space(24)
  w_ATNOMSOT = space(20)
  w_ATSESSOT = space(1)
  w_ATDTNSST = ctod('  /  /  ')
  w_ATLCNSST = space(40)
  w_ATPRRSST = space(2)
  w_ATRAGSST = space(60)
  o_ATRAGSST = space(60)
  w_DirName = space(200)
  w_ATNUMMOD = 0
  w_MAXNORIG = 0
  w_ATTIPFOR = space(2)
  o_ATTIPFOR = space(2)
  w_ATCODINT = space(16)
  o_ATCODINT = space(16)
  w_ATIMPTRA = space(1)
  w_ATCODCAF = space(5)
  w_ATDATIMP = ctod('  /  /  ')
  w_ATNOMFIL = space(254)
  o_ATNOMFIL = space(254)
  w_ESTDATVER = space(1)
  w_STAPDF = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_ATSERIAL = this.W_ATSERIAL

  * --- Children pointers
  Gsai_Mds = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GENFILTE','gsai_agt')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_agtPag1","gsai_agt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Comunicazione")
      .Pages(1).HelpContextID = 77045031
      .Pages(2).addobject("oPag","tgsai_agtPag2","gsai_agt",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati del contribuente")
      .Pages(2).HelpContextID = 73875540
      .Pages(3).addobject("oPag","tgsai_agtPag3","gsai_agt",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Rappresentante firmatario")
      .Pages(3).HelpContextID = 258620074
      .Pages(4).addobject("oPag","tgsai_agtPag4","gsai_agt",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Dati trasmissione")
      .Pages(4).HelpContextID = 191846011
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsai_agt
    Getblackbversion()
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TITOLARI'
    this.cWorkTables[3]='SEDIAZIE'
    this.cWorkTables[4]='DATPARSP'
    this.cWorkTables[5]='DATPARSM'
    this.cWorkTables[6]='GENFILTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GENFILTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GENFILTE_IDX,3]
  return

  function CreateChildren()
    this.Gsai_Mds = CREATEOBJECT('stdDynamicChild',this,'Gsai_Mds',this.oPgFrm.Page4.oPag.oLinkPC_4_29)
    this.Gsai_Mds.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.Gsai_Mds)
      this.Gsai_Mds.DestroyChildrenChain()
      this.Gsai_Mds=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_29')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.Gsai_Mds.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.Gsai_Mds.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.Gsai_Mds.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.Gsai_Mds.SetKey(;
            .w_ATSERIAL,"DGSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .Gsai_Mds.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATSERIAL,"DGSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.Gsai_Mds)
        i_f=.Gsai_Mds.BuildFilter()
        if !(i_f==.Gsai_Mds.cQueryFilter)
          i_fnidx=.Gsai_Mds.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.Gsai_Mds.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.Gsai_Mds.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.Gsai_Mds.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.Gsai_Mds.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ATSERIAL = NVL(ATSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GENFILTE where ATSERIAL=KeySet.ATSERIAL
    *
    i_nConn = i_TableProp[this.GENFILTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENFILTE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GENFILTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GENFILTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GENFILTE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ATIMPMIN = space(1)
        .w_ATCONIMP = space(1)
        .w_ATSCLIMP = space(1)
        .w_SERIALE = space(10)
        .w_ATIMPMAX = space(1)
        .w_ATIVANOS = space(1)
        .w_SMIMPMIN = space(1)
        .w_SMSCLIMP = space(1)
        .w_SMIMPMAX = space(1)
        .w_CODAZI = i_CODAZI
        .w_AZPIVAZI = space(12)
        .w_AZCOFAZI = space(16)
        .w_AZPERAZI = space(1)
        .w_AZCODNAZ = space(3)
        .w_AZLOCAZI = space(30)
        .w_AZPROAZI = space(2)
        .w_AZNAGAZI = space(5)
        .w_AZTELEFO = space(18)
        .w_AZTELFAX = space(18)
        .w_AZ_EMAIL = space(254)
        .w_AZIVACOF = space(16)
        .w_CODAZI2 = i_CODAZI
        .w_COGTIT1 = space(25)
        .w_NOMTIT1 = space(25)
        .w_DATNAS1 = ctod("  /  /  ")
        .w_LOCTIT1 = space(30)
        .w_PROTIT1 = space(2)
        .w_SESSO1 = space(1)
        .w_TELEFO1 = space(18)
        .w_FORZA_EDIT = iif(.w_AZPERAZI<>'S',.w_Forza_Edit,.f.)
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_MAXNORIG = 30000
        .w_ESTDATVER = 'N'
        .w_ATSERIAL = NVL(ATSERIAL,space(10))
        .op_ATSERIAL = .w_ATSERIAL
        .w_ATGENSPE = NVL(ATGENSPE,space(1))
        .w_ATGENBLK = NVL(ATGENBLK,space(1))
        .w_ATGENSMA = NVL(ATGENSMA,space(1))
        .w_AT__ANNO = NVL(AT__ANNO,0)
        .w_ATTIPPER = NVL(ATTIPPER,space(1))
        .w_ATTIPPER = NVL(ATTIPPER,space(1))
        .w_AT__MESE = NVL(AT__MESE,0)
        .w_ATTRIMES = NVL(ATTRIMES,0)
        .w_PA__ANNO = Str(.w_AT__ANNO,4,0)
          .link_1_14('Load')
        .w_SM_ANNO = Str(.w_AT__ANNO,4,0)
          .link_1_15('Load')
        .w_ANNO = .w_AT__ANNO
        .w_ATTIPINV = NVL(ATTIPINV,0)
        .w_ATPROTEC = NVL(ATPROTEC,space(17))
        .w_ATPRODOC = NVL(ATPRODOC,0)
        .w_ATTIPAGR = NVL(ATTIPAGR,space(1))
        .w_ATQUADFA = NVL(ATQUADFA,space(1))
        .w_ATQUADSA = NVL(ATQUADSA,space(1))
        .w_ATQUAATU = NVL(ATQUAATU,space(1))
        .w_ATQUADBL = NVL(ATQUADBL,space(1))
        .w_ATTIPDAT = NVL(ATTIPDAT,space(1))
        .w_ATQUADFE = NVL(ATQUADFE,space(1))
        .w_ATQUADFR = NVL(ATQUADFR,space(1))
        .w_ATQUADNE = NVL(ATQUADNE,space(1))
        .w_ATQUADNR = NVL(ATQUADNR,space(1))
        .w_ATQUADDF = NVL(ATQUADDF,space(1))
        .w_ATQUADFN = NVL(ATQUADFN,space(1))
        .w_ATQUADTU = NVL(ATQUADTU,space(1))
        .w_ATQUADSE = NVL(ATQUADSE,space(1))
          .link_2_1('Load')
          .link_2_13('Load')
        .w_ATCODFIS = NVL(ATCODFIS,space(16))
        .w_ATPARIVA = NVL(ATPARIVA,space(11))
        .w_ATCODATT = NVL(ATCODATT,space(6))
        .w_AT__MAIL = NVL(AT__MAIL,space(50))
        .w_ATTELEFO = NVL(ATTELEFO,space(12))
        .w_ATNUMFAX = NVL(ATNUMFAX,space(12))
        .w_ATCOGNOM = NVL(ATCOGNOM,space(24))
        .w_AT__NOME = NVL(AT__NOME,space(20))
        .w_AT_SESSO = NVL(AT_SESSO,space(1))
        .w_ATDATNAS = NVL(cp_ToDate(ATDATNAS),ctod("  /  /  "))
        .w_ATLOCNAS = NVL(ATLOCNAS,space(40))
        .w_ATPRONAS = NVL(ATPRONAS,space(2))
        .w_ATRAGSOC = NVL(ATRAGSOC,space(60))
        .w_ATCFISOT = NVL(ATCFISOT,space(16))
        .w_ATCODCAR = NVL(ATCODCAR,space(2))
        .w_ATDATIPR = NVL(cp_ToDate(ATDATIPR),ctod("  /  /  "))
        .w_ATDATFPR = NVL(cp_ToDate(ATDATFPR),ctod("  /  /  "))
        .w_ATCOGSOT = NVL(ATCOGSOT,space(24))
        .w_ATNOMSOT = NVL(ATNOMSOT,space(20))
        .w_ATSESSOT = NVL(ATSESSOT,space(1))
        .w_ATDTNSST = NVL(cp_ToDate(ATDTNSST),ctod("  /  /  "))
        .w_ATLCNSST = NVL(ATLCNSST,space(40))
        .w_ATPRRSST = NVL(ATPRRSST,space(2))
        .w_ATRAGSST = NVL(ATRAGSST,space(60))
        .w_ATNUMMOD = NVL(ATNUMMOD,0)
        .w_ATTIPFOR = NVL(ATTIPFOR,space(2))
        .w_ATCODINT = NVL(ATCODINT,space(16))
        .w_ATIMPTRA = NVL(ATIMPTRA,space(1))
        .w_ATCODCAF = NVL(ATCODCAF,space(5))
        .w_ATDATIMP = NVL(cp_ToDate(ATDATIMP),ctod("  /  /  "))
        .w_ATNOMFIL = NVL(ATNOMFIL,space(254))
        .w_STAPDF = 'N'
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GENFILTE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page4.oPag.oBtn_4_19.enabled = this.oPgFrm.Page4.oPag.oBtn_4_19.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_20.enabled = this.oPgFrm.Page4.oPag.oBtn_4_20.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATSERIAL = space(10)
      .w_ATGENSPE = space(1)
      .w_ATGENBLK = space(1)
      .w_ATGENSMA = space(1)
      .w_AT__ANNO = 0
      .w_ATTIPPER = space(1)
      .w_ATTIPPER = space(1)
      .w_AT__MESE = 0
      .w_ATTRIMES = 0
      .w_PA__ANNO = space(4)
      .w_SM_ANNO = space(4)
      .w_ATIMPMIN = space(1)
      .w_ATCONIMP = space(1)
      .w_ATSCLIMP = space(1)
      .w_ANNO = 0
      .w_SERIALE = space(10)
      .w_ATTIPINV = 0
      .w_ATPROTEC = space(17)
      .w_ATPRODOC = 0
      .w_ATTIPAGR = space(1)
      .w_ATQUADFA = space(1)
      .w_ATQUADSA = space(1)
      .w_ATQUAATU = space(1)
      .w_ATQUADBL = space(1)
      .w_ATTIPDAT = space(1)
      .w_ATQUADFE = space(1)
      .w_ATQUADFR = space(1)
      .w_ATQUADNE = space(1)
      .w_ATQUADNR = space(1)
      .w_ATQUADDF = space(1)
      .w_ATQUADFN = space(1)
      .w_ATQUADTU = space(1)
      .w_ATQUADSE = space(1)
      .w_ATIMPMAX = space(1)
      .w_ATIVANOS = space(1)
      .w_SMIMPMIN = space(1)
      .w_SMSCLIMP = space(1)
      .w_SMIMPMAX = space(1)
      .w_CODAZI = space(5)
      .w_AZPIVAZI = space(12)
      .w_AZCOFAZI = space(16)
      .w_AZPERAZI = space(1)
      .w_AZCODNAZ = space(3)
      .w_AZLOCAZI = space(30)
      .w_AZPROAZI = space(2)
      .w_AZNAGAZI = space(5)
      .w_AZTELEFO = space(18)
      .w_AZTELFAX = space(18)
      .w_AZ_EMAIL = space(254)
      .w_AZIVACOF = space(16)
      .w_CODAZI2 = space(5)
      .w_COGTIT1 = space(25)
      .w_NOMTIT1 = space(25)
      .w_DATNAS1 = ctod("  /  /  ")
      .w_LOCTIT1 = space(30)
      .w_PROTIT1 = space(2)
      .w_SESSO1 = space(1)
      .w_TELEFO1 = space(18)
      .w_ATCODFIS = space(16)
      .w_ATPARIVA = space(11)
      .w_ATCODATT = space(6)
      .w_AT__MAIL = space(50)
      .w_ATTELEFO = space(12)
      .w_ATNUMFAX = space(12)
      .w_ATCOGNOM = space(24)
      .w_AT__NOME = space(20)
      .w_AT_SESSO = space(1)
      .w_ATDATNAS = ctod("  /  /  ")
      .w_ATLOCNAS = space(40)
      .w_ATPRONAS = space(2)
      .w_ATRAGSOC = space(60)
      .w_FORZA_EDIT = .f.
      .w_ATCFISOT = space(16)
      .w_ATCODCAR = space(2)
      .w_ATDATIPR = ctod("  /  /  ")
      .w_ATDATFPR = ctod("  /  /  ")
      .w_ATCOGSOT = space(24)
      .w_ATNOMSOT = space(20)
      .w_ATSESSOT = space(1)
      .w_ATDTNSST = ctod("  /  /  ")
      .w_ATLCNSST = space(40)
      .w_ATPRRSST = space(2)
      .w_ATRAGSST = space(60)
      .w_DirName = space(200)
      .w_ATNUMMOD = 0
      .w_MAXNORIG = 0
      .w_ATTIPFOR = space(2)
      .w_ATCODINT = space(16)
      .w_ATIMPTRA = space(1)
      .w_ATCODCAF = space(5)
      .w_ATDATIMP = ctod("  /  /  ")
      .w_ATNOMFIL = space(254)
      .w_ESTDATVER = space(1)
      .w_STAPDF = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_ATGENSPE = 'S'
        .w_ATGENBLK = 'N'
        .w_ATGENSMA = 'N'
        .w_AT__ANNO = IIF(.w_ATGENSPE='S',year(i_DATSYS)-1,year(i_DATSYS))
        .w_ATTIPPER = 'A'
        .w_ATTIPPER = 'A'
        .w_AT__MESE = iif(Nvl(.w_Attipper,'M')='M', month(i_DATSYS), 0)
        .w_ATTRIMES = iif(NVL(.w_Attipper,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        .w_PA__ANNO = Str(.w_AT__ANNO,4,0)
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_PA__ANNO))
          .link_1_14('Full')
          endif
        .w_SM_ANNO = Str(.w_AT__ANNO,4,0)
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_SM_ANNO))
          .link_1_15('Full')
          endif
          .DoRTCalc(12,14,.f.)
        .w_ANNO = .w_AT__ANNO
          .DoRTCalc(16,16,.f.)
        .w_ATTIPINV = iif(Empty(.w_Seriale),0,1)
        .w_ATPROTEC = SPACE(17)
        .w_ATPRODOC = 0
        .w_ATTIPAGR = iif(.w_Atgenblk='S','S','N')
        .w_ATQUADFA = iif(.w_Attipagr='S' And .w_Atgenspe='S','1','0')
        .w_ATQUADSA = iif(.w_Attipagr='S' And .w_Atgenspe='S','1','0')
        .w_ATQUAATU = iif(.w_Attipagr='S' And .w_Atgenspe='S','1','0')
        .w_ATQUADBL = iif(.w_Attipagr='S' And (.w_Atgenspe='S' Or .w_Atgenblk='S'),'1','0')
        .w_ATTIPDAT = iif(.w_Atgenspe='S' Or .w_Atgensma='S','S','N')
        .w_ATQUADFE = iif(.w_Attipdat='S' And .w_Atgenspe='S' ,'1','0')
        .w_ATQUADFR = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        .w_ATQUADNE = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        .w_ATQUADNR = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        .w_ATQUADDF = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        .w_ATQUADFN = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        .w_ATQUADTU = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        .w_ATQUADSE = iif(.w_Attipdat='S' And (.w_Atgenspe='S' Or .w_Atgensma='S'),'1','0')
          .DoRTCalc(34,38,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_CODAZI))
          .link_2_1('Full')
          endif
          .DoRTCalc(40,50,.f.)
        .w_CODAZI2 = i_CODAZI
        .DoRTCalc(51,51,.f.)
          if not(empty(.w_CODAZI2))
          .link_2_13('Full')
          endif
          .DoRTCalc(52,58,.f.)
        .w_ATCODFIS = IIF(LEN(ALLTRIM(.w_AZCOFAZI))<=16, LEFT(ALLTRIM(.w_AZCOFAZI),16), RIGHT(ALLTRIM(.w_AZCOFAZI),16))
        .w_ATPARIVA = left(.w_AZPIVAZI, 11)
        .w_ATCODATT = iif(Empty(.w_Atcodatt),iif(g_Attivi='S', Space(6), Left(Alltrim(Calattso(g_Catazi,Alltrim(Str(.w_At__Anno)),12)),6)),.w_Atcodatt)
        .w_AT__MAIL = left(UPPER(.w_AZ_EMAIL), 50)
        .w_ATTELEFO = left(iif(.w_AZPERAZI='S', .w_TELEFO1, .w_AZTELEFO), 12)
        .w_ATNUMFAX = left(.w_AZTELFAX, 12)
        .w_ATCOGNOM = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_COGTIT1), space(24)), 24)
        .w_AT__NOME = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_NOMTIT1), space(20)), 20)
        .w_AT_SESSO = iif(.w_AZPERAZI='S' and not empty(.w_SESSO1), .w_SESSO1, 'M')
        .w_ATDATNAS = iif(.w_AZPERAZI='S', .w_DATNAS1, CTOD("  -  -    "))
        .w_ATLOCNAS = left(iif(.w_AZPERAZI='S', UPPER(.w_LOCTIT1), space(40)), 40)
        .w_ATPRONAS = iif(.w_AZPERAZI='S', UPPER(.w_PROTIT1), space(2))
        .w_ATRAGSOC = LEFT(iif(.w_AZPERAZI<>'S', UPPER(g_RAGAZI), space(60)), 60)
        .w_FORZA_EDIT = iif(.w_AZPERAZI<>'S',.w_Forza_Edit,.f.)
        .w_ATCFISOT = space(16)
        .w_ATCODCAR = '1'
        .w_ATDATIPR = Cp_CharToDate('  -  -    ')
          .DoRTCalc(76,76,.f.)
        .w_ATCOGSOT = space(24)
        .w_ATNOMSOT = iif(Empty(.w_Atcogsot),space(20),.w_Atnomsot)
        .w_ATSESSOT = iif(Empty(.w_Atcogsot),'M',.w_Atsessot)
        .w_ATDTNSST = iif(Empty(.w_Atcogsot),Ctod('  -  -    '),.w_Atdtnsst)
        .w_ATLCNSST = iif(Empty(.w_Atcogsot),space(40),.w_Atlcnsst)
        .w_ATPRRSST = iif(Empty(.w_Atcogsot),space(2),.w_Atprrsst)
        .w_ATRAGSST = Space(24)
        .w_DirName = sys(5)+sys(2003)+'\'
          .DoRTCalc(85,85,.f.)
        .w_MAXNORIG = 30000
        .w_ATTIPFOR = '01'
        .w_ATCODINT = iif(.w_Attipfor='01', space(16), .w_Atcodint)
        .w_ATIMPTRA = '1'
        .w_ATCODCAF = iif(Empty(.w_Atcodint), space(5), .w_Atcodcaf)
        .w_ATDATIMP = iif(Empty(.w_Atcodint), Ctod('  -  -    '), .w_Atdatimp)
          .DoRTCalc(92,92,.f.)
        .w_ESTDATVER = 'N'
        .w_STAPDF = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'GENFILTE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page4.oPag.oBtn_4_19.enabled = this.oPgFrm.Page4.oPag.oBtn_4_19.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_20.enabled = this.oPgFrm.Page4.oPag.oBtn_4_20.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENFILTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENFILTE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"ATCGF","i_codazi,w_ATSERIAL")
      .op_codazi = .w_codazi
      .op_ATSERIAL = .w_ATSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oATGENSPE_1_2.enabled = i_bVal
      .Page1.oPag.oATGENBLK_1_3.enabled = i_bVal
      .Page1.oPag.oATGENSMA_1_4.enabled = i_bVal
      .Page1.oPag.oAT__ANNO_1_9.enabled = i_bVal
      .Page1.oPag.oATTIPPER_1_11.enabled = i_bVal
      .Page1.oPag.oAT__MESE_1_12.enabled = i_bVal
      .Page1.oPag.oATTRIMES_1_13.enabled = i_bVal
      .Page1.oPag.oATTIPINV_1_22.enabled = i_bVal
      .Page1.oPag.oATPROTEC_1_23.enabled = i_bVal
      .Page1.oPag.oATPRODOC_1_24.enabled = i_bVal
      .Page1.oPag.oATTIPAGR_1_30.enabled = i_bVal
      .Page1.oPag.oATQUADFA_1_31.enabled = i_bVal
      .Page1.oPag.oATQUADSA_1_32.enabled = i_bVal
      .Page1.oPag.oATQUAATU_1_33.enabled = i_bVal
      .Page1.oPag.oATQUADBL_1_34.enabled = i_bVal
      .Page1.oPag.oATTIPDAT_1_35.enabled = i_bVal
      .Page1.oPag.oATQUADFE_1_36.enabled = i_bVal
      .Page1.oPag.oATQUADFR_1_37.enabled = i_bVal
      .Page1.oPag.oATQUADNE_1_38.enabled = i_bVal
      .Page1.oPag.oATQUADNR_1_39.enabled = i_bVal
      .Page1.oPag.oATQUADDF_1_40.enabled = i_bVal
      .Page1.oPag.oATQUADFN_1_41.enabled = i_bVal
      .Page1.oPag.oATQUADTU_1_43.enabled = i_bVal
      .Page1.oPag.oATQUADSE_1_44.enabled = i_bVal
      .Page2.oPag.oATCODFIS_2_21.enabled = i_bVal
      .Page2.oPag.oATPARIVA_2_22.enabled = i_bVal
      .Page2.oPag.oATCODATT_2_23.enabled = i_bVal
      .Page2.oPag.oAT__MAIL_2_24.enabled = i_bVal
      .Page2.oPag.oATTELEFO_2_25.enabled = i_bVal
      .Page2.oPag.oATNUMFAX_2_26.enabled = i_bVal
      .Page2.oPag.oATCOGNOM_2_29.enabled = i_bVal
      .Page2.oPag.oAT__NOME_2_30.enabled = i_bVal
      .Page2.oPag.oAT_SESSO_2_31.enabled = i_bVal
      .Page2.oPag.oATDATNAS_2_32.enabled = i_bVal
      .Page2.oPag.oATLOCNAS_2_37.enabled = i_bVal
      .Page2.oPag.oATPRONAS_2_38.enabled = i_bVal
      .Page2.oPag.oATRAGSOC_2_40.enabled = i_bVal
      .Page3.oPag.oFORZA_EDIT_3_1.enabled = i_bVal
      .Page3.oPag.oATCFISOT_3_2.enabled = i_bVal
      .Page3.oPag.oATCODCAR_3_3.enabled = i_bVal
      .Page3.oPag.oATDATIPR_3_4.enabled = i_bVal
      .Page3.oPag.oATDATFPR_3_5.enabled = i_bVal
      .Page3.oPag.oATCOGSOT_3_7.enabled = i_bVal
      .Page3.oPag.oATNOMSOT_3_9.enabled = i_bVal
      .Page3.oPag.oATSESSOT_3_10.enabled = i_bVal
      .Page3.oPag.oATDTNSST_3_12.enabled = i_bVal
      .Page3.oPag.oATLCNSST_3_14.enabled = i_bVal
      .Page3.oPag.oATPRRSST_3_16.enabled = i_bVal
      .Page3.oPag.oATRAGSST_3_26.enabled = i_bVal
      .Page4.oPag.oMAXNORIG_4_5.enabled = i_bVal
      .Page4.oPag.oATTIPFOR_4_7.enabled = i_bVal
      .Page4.oPag.oATCODINT_4_8.enabled = i_bVal
      .Page4.oPag.oATIMPTRA_4_9.enabled = i_bVal
      .Page4.oPag.oATCODCAF_4_11.enabled = i_bVal
      .Page4.oPag.oATDATIMP_4_13.enabled = i_bVal
      .Page4.oPag.oATNOMFIL_4_15.enabled = i_bVal
      .Page4.oPag.oESTDATVER_4_18.enabled = i_bVal
      .Page4.oPag.oBtn_4_17.enabled = i_bVal
      .Page4.oPag.oBtn_4_19.enabled = .Page4.oPag.oBtn_4_19.mCond()
      .Page4.oPag.oBtn_4_20.enabled = .Page4.oPag.oBtn_4_20.mCond()
      .Page4.oPag.oBtn_4_21.enabled = i_bVal
    endwith
    this.Gsai_Mds.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GENFILTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.Gsai_Mds.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GENFILTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSERIAL,"ATSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATGENSPE,"ATGENSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATGENBLK,"ATGENBLK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATGENSMA,"ATGENSMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT__ANNO,"AT__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPPER,"ATTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPPER,"ATTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT__MESE,"AT__MESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTRIMES,"ATTRIMES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPINV,"ATTIPINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPROTEC,"ATPROTEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPRODOC,"ATPRODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPAGR,"ATTIPAGR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADFA,"ATQUADFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADSA,"ATQUADSA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUAATU,"ATQUAATU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADBL,"ATQUADBL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPDAT,"ATTIPDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADFE,"ATQUADFE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADFR,"ATQUADFR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADNE,"ATQUADNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADNR,"ATQUADNR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADDF,"ATQUADDF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADFN,"ATQUADFN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADTU,"ATQUADTU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATQUADSE,"ATQUADSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODFIS,"ATCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPARIVA,"ATPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODATT,"ATCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT__MAIL,"AT__MAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTELEFO,"ATTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNUMFAX,"ATNUMFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCOGNOM,"ATCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT__NOME,"AT__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_SESSO,"AT_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATNAS,"ATDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATLOCNAS,"ATLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPRONAS,"ATPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATRAGSOC,"ATRAGSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCFISOT,"ATCFISOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODCAR,"ATCODCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATIPR,"ATDATIPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATFPR,"ATDATFPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCOGSOT,"ATCOGSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNOMSOT,"ATNOMSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSESSOT,"ATSESSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDTNSST,"ATDTNSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATLCNSST,"ATLCNSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPRRSST,"ATPRRSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATRAGSST,"ATRAGSST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNUMMOD,"ATNUMMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPFOR,"ATTIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODINT,"ATCODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATIMPTRA,"ATIMPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODCAF,"ATCODCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATIMP,"ATDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNOMFIL,"ATNOMFIL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GENFILTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENFILTE_IDX,2])
    i_lTable = "GENFILTE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GENFILTE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GENFILTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENFILTE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GENFILTE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"ATCGF","i_codazi,w_ATSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GENFILTE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GENFILTE')
        i_extval=cp_InsertValODBCExtFlds(this,'GENFILTE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ATSERIAL,ATGENSPE,ATGENBLK,ATGENSMA,AT__ANNO"+;
                  ",ATTIPPER,AT__MESE,ATTRIMES,ATTIPINV,ATPROTEC"+;
                  ",ATPRODOC,ATTIPAGR,ATQUADFA,ATQUADSA,ATQUAATU"+;
                  ",ATQUADBL,ATTIPDAT,ATQUADFE,ATQUADFR,ATQUADNE"+;
                  ",ATQUADNR,ATQUADDF,ATQUADFN,ATQUADTU,ATQUADSE"+;
                  ",ATCODFIS,ATPARIVA,ATCODATT,AT__MAIL,ATTELEFO"+;
                  ",ATNUMFAX,ATCOGNOM,AT__NOME,AT_SESSO,ATDATNAS"+;
                  ",ATLOCNAS,ATPRONAS,ATRAGSOC,ATCFISOT,ATCODCAR"+;
                  ",ATDATIPR,ATDATFPR,ATCOGSOT,ATNOMSOT,ATSESSOT"+;
                  ",ATDTNSST,ATLCNSST,ATPRRSST,ATRAGSST,ATNUMMOD"+;
                  ",ATTIPFOR,ATCODINT,ATIMPTRA,ATCODCAF,ATDATIMP"+;
                  ",ATNOMFIL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ATSERIAL)+;
                  ","+cp_ToStrODBC(this.w_ATGENSPE)+;
                  ","+cp_ToStrODBC(this.w_ATGENBLK)+;
                  ","+cp_ToStrODBC(this.w_ATGENSMA)+;
                  ","+cp_ToStrODBC(this.w_AT__ANNO)+;
                  ","+cp_ToStrODBC(this.w_ATTIPPER)+;
                  ","+cp_ToStrODBC(this.w_AT__MESE)+;
                  ","+cp_ToStrODBC(this.w_ATTRIMES)+;
                  ","+cp_ToStrODBC(this.w_ATTIPINV)+;
                  ","+cp_ToStrODBC(this.w_ATPROTEC)+;
                  ","+cp_ToStrODBC(this.w_ATPRODOC)+;
                  ","+cp_ToStrODBC(this.w_ATTIPAGR)+;
                  ","+cp_ToStrODBC(this.w_ATQUADFA)+;
                  ","+cp_ToStrODBC(this.w_ATQUADSA)+;
                  ","+cp_ToStrODBC(this.w_ATQUAATU)+;
                  ","+cp_ToStrODBC(this.w_ATQUADBL)+;
                  ","+cp_ToStrODBC(this.w_ATTIPDAT)+;
                  ","+cp_ToStrODBC(this.w_ATQUADFE)+;
                  ","+cp_ToStrODBC(this.w_ATQUADFR)+;
                  ","+cp_ToStrODBC(this.w_ATQUADNE)+;
                  ","+cp_ToStrODBC(this.w_ATQUADNR)+;
                  ","+cp_ToStrODBC(this.w_ATQUADDF)+;
                  ","+cp_ToStrODBC(this.w_ATQUADFN)+;
                  ","+cp_ToStrODBC(this.w_ATQUADTU)+;
                  ","+cp_ToStrODBC(this.w_ATQUADSE)+;
                  ","+cp_ToStrODBC(this.w_ATCODFIS)+;
                  ","+cp_ToStrODBC(this.w_ATPARIVA)+;
                  ","+cp_ToStrODBC(this.w_ATCODATT)+;
                  ","+cp_ToStrODBC(this.w_AT__MAIL)+;
                  ","+cp_ToStrODBC(this.w_ATTELEFO)+;
                  ","+cp_ToStrODBC(this.w_ATNUMFAX)+;
                  ","+cp_ToStrODBC(this.w_ATCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_AT__NOME)+;
                  ","+cp_ToStrODBC(this.w_AT_SESSO)+;
                  ","+cp_ToStrODBC(this.w_ATDATNAS)+;
                  ","+cp_ToStrODBC(this.w_ATLOCNAS)+;
                  ","+cp_ToStrODBC(this.w_ATPRONAS)+;
                  ","+cp_ToStrODBC(this.w_ATRAGSOC)+;
                  ","+cp_ToStrODBC(this.w_ATCFISOT)+;
                  ","+cp_ToStrODBC(this.w_ATCODCAR)+;
                  ","+cp_ToStrODBC(this.w_ATDATIPR)+;
                  ","+cp_ToStrODBC(this.w_ATDATFPR)+;
                  ","+cp_ToStrODBC(this.w_ATCOGSOT)+;
                  ","+cp_ToStrODBC(this.w_ATNOMSOT)+;
                  ","+cp_ToStrODBC(this.w_ATSESSOT)+;
                  ","+cp_ToStrODBC(this.w_ATDTNSST)+;
                  ","+cp_ToStrODBC(this.w_ATLCNSST)+;
                  ","+cp_ToStrODBC(this.w_ATPRRSST)+;
                  ","+cp_ToStrODBC(this.w_ATRAGSST)+;
                  ","+cp_ToStrODBC(this.w_ATNUMMOD)+;
                  ","+cp_ToStrODBC(this.w_ATTIPFOR)+;
                  ","+cp_ToStrODBC(this.w_ATCODINT)+;
                  ","+cp_ToStrODBC(this.w_ATIMPTRA)+;
                  ","+cp_ToStrODBC(this.w_ATCODCAF)+;
                  ","+cp_ToStrODBC(this.w_ATDATIMP)+;
                  ","+cp_ToStrODBC(this.w_ATNOMFIL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GENFILTE')
        i_extval=cp_InsertValVFPExtFlds(this,'GENFILTE')
        cp_CheckDeletedKey(i_cTable,0,'ATSERIAL',this.w_ATSERIAL)
        INSERT INTO (i_cTable);
              (ATSERIAL,ATGENSPE,ATGENBLK,ATGENSMA,AT__ANNO,ATTIPPER,AT__MESE,ATTRIMES,ATTIPINV,ATPROTEC,ATPRODOC,ATTIPAGR,ATQUADFA,ATQUADSA,ATQUAATU,ATQUADBL,ATTIPDAT,ATQUADFE,ATQUADFR,ATQUADNE,ATQUADNR,ATQUADDF,ATQUADFN,ATQUADTU,ATQUADSE,ATCODFIS,ATPARIVA,ATCODATT,AT__MAIL,ATTELEFO,ATNUMFAX,ATCOGNOM,AT__NOME,AT_SESSO,ATDATNAS,ATLOCNAS,ATPRONAS,ATRAGSOC,ATCFISOT,ATCODCAR,ATDATIPR,ATDATFPR,ATCOGSOT,ATNOMSOT,ATSESSOT,ATDTNSST,ATLCNSST,ATPRRSST,ATRAGSST,ATNUMMOD,ATTIPFOR,ATCODINT,ATIMPTRA,ATCODCAF,ATDATIMP,ATNOMFIL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ATSERIAL;
                  ,this.w_ATGENSPE;
                  ,this.w_ATGENBLK;
                  ,this.w_ATGENSMA;
                  ,this.w_AT__ANNO;
                  ,this.w_ATTIPPER;
                  ,this.w_AT__MESE;
                  ,this.w_ATTRIMES;
                  ,this.w_ATTIPINV;
                  ,this.w_ATPROTEC;
                  ,this.w_ATPRODOC;
                  ,this.w_ATTIPAGR;
                  ,this.w_ATQUADFA;
                  ,this.w_ATQUADSA;
                  ,this.w_ATQUAATU;
                  ,this.w_ATQUADBL;
                  ,this.w_ATTIPDAT;
                  ,this.w_ATQUADFE;
                  ,this.w_ATQUADFR;
                  ,this.w_ATQUADNE;
                  ,this.w_ATQUADNR;
                  ,this.w_ATQUADDF;
                  ,this.w_ATQUADFN;
                  ,this.w_ATQUADTU;
                  ,this.w_ATQUADSE;
                  ,this.w_ATCODFIS;
                  ,this.w_ATPARIVA;
                  ,this.w_ATCODATT;
                  ,this.w_AT__MAIL;
                  ,this.w_ATTELEFO;
                  ,this.w_ATNUMFAX;
                  ,this.w_ATCOGNOM;
                  ,this.w_AT__NOME;
                  ,this.w_AT_SESSO;
                  ,this.w_ATDATNAS;
                  ,this.w_ATLOCNAS;
                  ,this.w_ATPRONAS;
                  ,this.w_ATRAGSOC;
                  ,this.w_ATCFISOT;
                  ,this.w_ATCODCAR;
                  ,this.w_ATDATIPR;
                  ,this.w_ATDATFPR;
                  ,this.w_ATCOGSOT;
                  ,this.w_ATNOMSOT;
                  ,this.w_ATSESSOT;
                  ,this.w_ATDTNSST;
                  ,this.w_ATLCNSST;
                  ,this.w_ATPRRSST;
                  ,this.w_ATRAGSST;
                  ,this.w_ATNUMMOD;
                  ,this.w_ATTIPFOR;
                  ,this.w_ATCODINT;
                  ,this.w_ATIMPTRA;
                  ,this.w_ATCODCAF;
                  ,this.w_ATDATIMP;
                  ,this.w_ATNOMFIL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GENFILTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENFILTE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GENFILTE_IDX,i_nConn)
      *
      * update GENFILTE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GENFILTE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ATGENSPE="+cp_ToStrODBC(this.w_ATGENSPE)+;
             ",ATGENBLK="+cp_ToStrODBC(this.w_ATGENBLK)+;
             ",ATGENSMA="+cp_ToStrODBC(this.w_ATGENSMA)+;
             ",AT__ANNO="+cp_ToStrODBC(this.w_AT__ANNO)+;
             ",ATTIPPER="+cp_ToStrODBC(this.w_ATTIPPER)+;
             ",AT__MESE="+cp_ToStrODBC(this.w_AT__MESE)+;
             ",ATTRIMES="+cp_ToStrODBC(this.w_ATTRIMES)+;
             ",ATTIPINV="+cp_ToStrODBC(this.w_ATTIPINV)+;
             ",ATPROTEC="+cp_ToStrODBC(this.w_ATPROTEC)+;
             ",ATPRODOC="+cp_ToStrODBC(this.w_ATPRODOC)+;
             ",ATTIPAGR="+cp_ToStrODBC(this.w_ATTIPAGR)+;
             ",ATQUADFA="+cp_ToStrODBC(this.w_ATQUADFA)+;
             ",ATQUADSA="+cp_ToStrODBC(this.w_ATQUADSA)+;
             ",ATQUAATU="+cp_ToStrODBC(this.w_ATQUAATU)+;
             ",ATQUADBL="+cp_ToStrODBC(this.w_ATQUADBL)+;
             ",ATTIPDAT="+cp_ToStrODBC(this.w_ATTIPDAT)+;
             ",ATQUADFE="+cp_ToStrODBC(this.w_ATQUADFE)+;
             ",ATQUADFR="+cp_ToStrODBC(this.w_ATQUADFR)+;
             ",ATQUADNE="+cp_ToStrODBC(this.w_ATQUADNE)+;
             ",ATQUADNR="+cp_ToStrODBC(this.w_ATQUADNR)+;
             ",ATQUADDF="+cp_ToStrODBC(this.w_ATQUADDF)+;
             ",ATQUADFN="+cp_ToStrODBC(this.w_ATQUADFN)+;
             ",ATQUADTU="+cp_ToStrODBC(this.w_ATQUADTU)+;
             ",ATQUADSE="+cp_ToStrODBC(this.w_ATQUADSE)+;
             ",ATCODFIS="+cp_ToStrODBC(this.w_ATCODFIS)+;
             ",ATPARIVA="+cp_ToStrODBC(this.w_ATPARIVA)+;
             ",ATCODATT="+cp_ToStrODBC(this.w_ATCODATT)+;
             ",AT__MAIL="+cp_ToStrODBC(this.w_AT__MAIL)+;
             ",ATTELEFO="+cp_ToStrODBC(this.w_ATTELEFO)+;
             ",ATNUMFAX="+cp_ToStrODBC(this.w_ATNUMFAX)+;
             ",ATCOGNOM="+cp_ToStrODBC(this.w_ATCOGNOM)+;
             ",AT__NOME="+cp_ToStrODBC(this.w_AT__NOME)+;
             ",AT_SESSO="+cp_ToStrODBC(this.w_AT_SESSO)+;
             ",ATDATNAS="+cp_ToStrODBC(this.w_ATDATNAS)+;
             ",ATLOCNAS="+cp_ToStrODBC(this.w_ATLOCNAS)+;
             ",ATPRONAS="+cp_ToStrODBC(this.w_ATPRONAS)+;
             ",ATRAGSOC="+cp_ToStrODBC(this.w_ATRAGSOC)+;
             ",ATCFISOT="+cp_ToStrODBC(this.w_ATCFISOT)+;
             ",ATCODCAR="+cp_ToStrODBC(this.w_ATCODCAR)+;
             ",ATDATIPR="+cp_ToStrODBC(this.w_ATDATIPR)+;
             ",ATDATFPR="+cp_ToStrODBC(this.w_ATDATFPR)+;
             ",ATCOGSOT="+cp_ToStrODBC(this.w_ATCOGSOT)+;
             ",ATNOMSOT="+cp_ToStrODBC(this.w_ATNOMSOT)+;
             ",ATSESSOT="+cp_ToStrODBC(this.w_ATSESSOT)+;
             ",ATDTNSST="+cp_ToStrODBC(this.w_ATDTNSST)+;
             ",ATLCNSST="+cp_ToStrODBC(this.w_ATLCNSST)+;
             ",ATPRRSST="+cp_ToStrODBC(this.w_ATPRRSST)+;
             ",ATRAGSST="+cp_ToStrODBC(this.w_ATRAGSST)+;
             ",ATNUMMOD="+cp_ToStrODBC(this.w_ATNUMMOD)+;
             ",ATTIPFOR="+cp_ToStrODBC(this.w_ATTIPFOR)+;
             ",ATCODINT="+cp_ToStrODBC(this.w_ATCODINT)+;
             ",ATIMPTRA="+cp_ToStrODBC(this.w_ATIMPTRA)+;
             ",ATCODCAF="+cp_ToStrODBC(this.w_ATCODCAF)+;
             ",ATDATIMP="+cp_ToStrODBC(this.w_ATDATIMP)+;
             ",ATNOMFIL="+cp_ToStrODBC(this.w_ATNOMFIL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GENFILTE')
        i_cWhere = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
        UPDATE (i_cTable) SET;
              ATGENSPE=this.w_ATGENSPE;
             ,ATGENBLK=this.w_ATGENBLK;
             ,ATGENSMA=this.w_ATGENSMA;
             ,AT__ANNO=this.w_AT__ANNO;
             ,ATTIPPER=this.w_ATTIPPER;
             ,AT__MESE=this.w_AT__MESE;
             ,ATTRIMES=this.w_ATTRIMES;
             ,ATTIPINV=this.w_ATTIPINV;
             ,ATPROTEC=this.w_ATPROTEC;
             ,ATPRODOC=this.w_ATPRODOC;
             ,ATTIPAGR=this.w_ATTIPAGR;
             ,ATQUADFA=this.w_ATQUADFA;
             ,ATQUADSA=this.w_ATQUADSA;
             ,ATQUAATU=this.w_ATQUAATU;
             ,ATQUADBL=this.w_ATQUADBL;
             ,ATTIPDAT=this.w_ATTIPDAT;
             ,ATQUADFE=this.w_ATQUADFE;
             ,ATQUADFR=this.w_ATQUADFR;
             ,ATQUADNE=this.w_ATQUADNE;
             ,ATQUADNR=this.w_ATQUADNR;
             ,ATQUADDF=this.w_ATQUADDF;
             ,ATQUADFN=this.w_ATQUADFN;
             ,ATQUADTU=this.w_ATQUADTU;
             ,ATQUADSE=this.w_ATQUADSE;
             ,ATCODFIS=this.w_ATCODFIS;
             ,ATPARIVA=this.w_ATPARIVA;
             ,ATCODATT=this.w_ATCODATT;
             ,AT__MAIL=this.w_AT__MAIL;
             ,ATTELEFO=this.w_ATTELEFO;
             ,ATNUMFAX=this.w_ATNUMFAX;
             ,ATCOGNOM=this.w_ATCOGNOM;
             ,AT__NOME=this.w_AT__NOME;
             ,AT_SESSO=this.w_AT_SESSO;
             ,ATDATNAS=this.w_ATDATNAS;
             ,ATLOCNAS=this.w_ATLOCNAS;
             ,ATPRONAS=this.w_ATPRONAS;
             ,ATRAGSOC=this.w_ATRAGSOC;
             ,ATCFISOT=this.w_ATCFISOT;
             ,ATCODCAR=this.w_ATCODCAR;
             ,ATDATIPR=this.w_ATDATIPR;
             ,ATDATFPR=this.w_ATDATFPR;
             ,ATCOGSOT=this.w_ATCOGSOT;
             ,ATNOMSOT=this.w_ATNOMSOT;
             ,ATSESSOT=this.w_ATSESSOT;
             ,ATDTNSST=this.w_ATDTNSST;
             ,ATLCNSST=this.w_ATLCNSST;
             ,ATPRRSST=this.w_ATPRRSST;
             ,ATRAGSST=this.w_ATRAGSST;
             ,ATNUMMOD=this.w_ATNUMMOD;
             ,ATTIPFOR=this.w_ATTIPFOR;
             ,ATCODINT=this.w_ATCODINT;
             ,ATIMPTRA=this.w_ATIMPTRA;
             ,ATCODCAF=this.w_ATCODCAF;
             ,ATDATIMP=this.w_ATDATIMP;
             ,ATNOMFIL=this.w_ATNOMFIL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- Gsai_Mds : Saving
      this.Gsai_Mds.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATSERIAL,"DGSERIAL";
             )
      this.Gsai_Mds.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- Gsai_Mds : Deleting
    this.Gsai_Mds.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATSERIAL,"DGSERIAL";
           )
    this.Gsai_Mds.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GENFILTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENFILTE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GENFILTE_IDX,i_nConn)
      *
      * delete GENFILTE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENFILTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENFILTE_IDX,2])
    if i_bUpd
      with this
        if .o_Atgenspe<>.w_Atgenspe
          .Calculate_USMLWCNYJF()
        endif
        if .o_Atgenblk<>.w_Atgenblk
          .Calculate_SJMBKXJEUG()
        endif
        if .o_Atgensma<>.w_Atgensma
          .Calculate_IQNTGIWTDD()
        endif
        if .o_Atgensma<>.w_Atgensma.or. .o_Atgenspe<>.w_Atgenspe.or. .o_Atgenblk<>.w_Atgenblk
          .Calculate_TXOSQAUHJQ()
        endif
        .DoRTCalc(1,4,.t.)
        if .o_Atgenspe<>.w_Atgenspe.or. .o_Atgenblk<>.w_Atgenblk.or. .o_Atgensma<>.w_Atgensma
            .w_AT__ANNO = IIF(.w_ATGENSPE='S',year(i_DATSYS)-1,year(i_DATSYS))
        endif
        .DoRTCalc(6,7,.t.)
        if .o_Attipper<>.w_Attipper.or. .o_Atgenspe<>.w_Atgenspe
            .w_AT__MESE = iif(Nvl(.w_Attipper,'M')='M', month(i_DATSYS), 0)
        endif
        if .o_Atgensma<>.w_Atgensma.or. .o_Atgenspe<>.w_Atgenspe.or. .o_Attipper<>.w_Attipper
            .w_ATTRIMES = iif(NVL(.w_Attipper,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        endif
            .w_PA__ANNO = Str(.w_AT__ANNO,4,0)
          .link_1_14('Full')
            .w_SM_ANNO = Str(.w_AT__ANNO,4,0)
          .link_1_15('Full')
        .DoRTCalc(12,14,.t.)
            .w_ANNO = .w_AT__ANNO
        Local l_Dep1,l_Dep2
        l_Dep1= .o_At__Anno<>.w_At__Anno .or. .o_At__Mese<>.w_At__Mese .or. .o_Attrimes<>.w_Attrimes .or. .o_Attipper<>.w_Attipper .or. .o_Atgenspe<>.w_Atgenspe        l_Dep2= .o_Atgensma<>.w_Atgensma .or. .o_Atgenblk<>.w_Atgenblk
        if m.l_Dep1 .or. m.l_Dep2
          .Calculate_RLORVZHNOA()
        endif
        .DoRTCalc(16,16,.t.)
        if .o_SERIALE<>.w_SERIALE
            .w_ATTIPINV = iif(Empty(.w_Seriale),0,1)
        endif
        if .o_ATTIPINV<>.w_ATTIPINV
            .w_ATPROTEC = SPACE(17)
        endif
        if .o_ATTIPINV<>.w_ATTIPINV
            .w_ATPRODOC = 0
        endif
        if .o_ATTIPINV<>.w_ATTIPINV
          .Calculate_EDPWMPARMF()
        endif
        if .o_Atgenblk<>.w_Atgenblk
            .w_ATTIPAGR = iif(.w_Atgenblk='S','S','N')
        endif
        if .o_Attipagr<>.w_Attipagr.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADFA = iif(.w_Attipagr='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipagr<>.w_Attipagr.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADSA = iif(.w_Attipagr='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipagr<>.w_Attipagr.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUAATU = iif(.w_Attipagr='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipagr<>.w_Attipagr.or. .o_Atgenspe<>.w_Atgenspe.or. .o_Atgenblk<>.w_Atgenblk
            .w_ATQUADBL = iif(.w_Attipagr='S' And (.w_Atgenspe='S' Or .w_Atgenblk='S'),'1','0')
        endif
        if .o_Atgenspe<>.w_Atgenspe.or. .o_Atgensma<>.w_Atgensma
            .w_ATTIPDAT = iif(.w_Atgenspe='S' Or .w_Atgensma='S','S','N')
        endif
        if .o_Attipdat<>.w_Attipdat.or. .o_Atgenspe<>.w_Atgenspe.or. .o_Attipper<>.w_Attipper
            .w_ATQUADFE = iif(.w_Attipdat='S' And .w_Atgenspe='S' ,'1','0')
        endif
        if .o_Attipdat<>.w_Attipdat.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADFR = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipdat<>.w_Attipdat.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADNE = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipdat<>.w_Attipdat.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADNR = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipdat<>.w_Attipdat.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADDF = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipdat<>.w_Attipdat.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADFN = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipdat<>.w_Attipdat.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADTU = iif(.w_Attipdat='S' And .w_Atgenspe='S','1','0')
        endif
        if .o_Attipdat<>.w_Attipdat.or. .o_Atgensma<>.w_Atgensma.or. .o_Atgenspe<>.w_Atgenspe
            .w_ATQUADSE = iif(.w_Attipdat='S' And (.w_Atgenspe='S' Or .w_Atgensma='S'),'1','0')
        endif
        if .o_Attipagr<>.w_Attipagr
          .Calculate_IWVNPLMZEC()
        endif
        if .o_Attipdat<>.w_Attipdat
          .Calculate_HJUUQJSPGT()
        endif
        .DoRTCalc(34,38,.t.)
          .link_2_1('Full')
        .DoRTCalc(40,50,.t.)
          .link_2_13('Full')
        .DoRTCalc(52,60,.t.)
        if .o_AT__ANNO<>.w_AT__ANNO
            .w_ATCODATT = iif(Empty(.w_Atcodatt),iif(g_Attivi='S', Space(6), Left(Alltrim(Calattso(g_Catazi,Alltrim(Str(.w_At__Anno)),12)),6)),.w_Atcodatt)
        endif
        .DoRTCalc(62,73,.t.)
        if .o_Forza_Edit<>.w_Forza_Edit.or. .o_Atcfisot<>.w_Atcfisot
            .w_ATCODCAR = '1'
        endif
        if .o_At__Anno<>.w_At__Anno.or. .o_Atcfisot<>.w_Atcfisot
            .w_ATDATIPR = Cp_CharToDate('  -  -    ')
        endif
        .DoRTCalc(76,76,.t.)
        if .o_Forza_Edit<>.w_Forza_Edit.or. .o_Atragsst<>.w_Atragsst
            .w_ATCOGSOT = space(24)
        endif
        if .o_Forza_Edit<>.w_Forza_Edit.or. .o_Atragsst<>.w_Atragsst.or. .o_Atcogsot<>.w_Atcogsot
            .w_ATNOMSOT = iif(Empty(.w_Atcogsot),space(20),.w_Atnomsot)
        endif
        if .o_Forza_Edit<>.w_Forza_Edit.or. .o_Atragsst<>.w_Atragsst.or. .o_Atcogsot<>.w_Atcogsot
            .w_ATSESSOT = iif(Empty(.w_Atcogsot),'M',.w_Atsessot)
        endif
        if .o_Forza_Edit<>.w_Forza_Edit.or. .o_Atragsst<>.w_Atragsst.or. .o_Atcogsot<>.w_Atcogsot
            .w_ATDTNSST = iif(Empty(.w_Atcogsot),Ctod('  -  -    '),.w_Atdtnsst)
        endif
        if .o_Forza_Edit<>.w_Forza_Edit.or. .o_Atragsst<>.w_Atragsst.or. .o_Atcogsot<>.w_Atcogsot
            .w_ATLCNSST = iif(Empty(.w_Atcogsot),space(40),.w_Atlcnsst)
        endif
        if .o_Forza_Edit<>.w_Forza_Edit.or. .o_Atragsst<>.w_Atragsst.or. .o_Atcogsot<>.w_Atcogsot
            .w_ATPRRSST = iif(Empty(.w_Atcogsot),space(2),.w_Atprrsst)
        endif
        if .o_Forza_Edit<>.w_Forza_Edit.or. .o_Atcogsot<>.w_Atcogsot
            .w_ATRAGSST = Space(24)
        endif
        if .o_ATGENSPE<>.w_ATGENSPE.or. .o_ATGENBLK<>.w_ATGENBLK.or. .o_ATGENSMA<>.w_ATGENSMA.or. .o_AT__ANNO<>.w_AT__ANNO
          .Calculate_ZELNCQUIYD()
        endif
        if .o_AT__ANNO<>.w_AT__ANNO.or. .o_ATNOMFIL<>.w_ATNOMFIL
          .Calculate_ZJFMMVWDUN()
        endif
        .DoRTCalc(84,87,.t.)
        if .o_ATTIPFOR<>.w_ATTIPFOR
            .w_ATCODINT = iif(.w_Attipfor='01', space(16), .w_Atcodint)
        endif
        if .o_ATCODINT<>.w_ATCODINT
            .w_ATIMPTRA = '1'
        endif
        if .o_ATCODINT<>.w_ATCODINT
            .w_ATCODCAF = iif(Empty(.w_Atcodint), space(5), .w_Atcodcaf)
        endif
        if .o_ATCODINT<>.w_ATCODINT
            .w_ATDATIMP = iif(Empty(.w_Atcodint), Ctod('  -  -    '), .w_Atdatimp)
        endif
        .DoRTCalc(92,93,.t.)
            .w_STAPDF = 'N'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"ATCGF","i_codazi,w_ATSERIAL")
          .op_ATSERIAL = .w_ATSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_USMLWCNYJF()
    with this
          * --- Valorizzazione tipo comunicazione
          .w_Atgenblk = iif(.w_Atgenspe='S' Or .w_Atgensma='S','N','S')
          .w_Atgensma = iif(.w_Atgenspe='S' Or .w_Atgenblk='S','N','S')
    endwith
  endproc
  proc Calculate_SJMBKXJEUG()
    with this
          * --- Valorizzazione tipo comunicazione
          .w_Atgenspe = iif(.w_Atgenblk='S' Or .w_Atgensma='S','N','S')
          .w_Atgensma = iif(.w_Atgenblk='S' Or .w_Atgenspe='S','N','S')
    endwith
  endproc
  proc Calculate_IQNTGIWTDD()
    with this
          * --- Valorizzazione tipo comunicazione
          .w_Atgenspe = iif(.w_Atgenblk='S' Or .w_Atgensma='S','N','S')
          .w_Atgenblk = iif(.w_Atgenspe='S' Or .w_Atgensma='S','N','S')
    endwith
  endproc
  proc Calculate_TXOSQAUHJQ()
    with this
          * --- Valorizzazione tipo periodo
          .w_Attipper = iif(.w_Atgenspe='S','A','M')
    endwith
  endproc
  proc Calculate_RLORVZHNOA()
    with this
          * --- Valorizzazione Tipologia documento
          Gsai_Bcb(this;
              ,'TipoCom';
             )
    endwith
  endproc
  proc Calculate_EDPWMPARMF()
    with this
          * --- Ricalcola valore tipologia  invio
          .w_ATTIPINV = iif(Not Empty(.w_Seriale) and .w_Attipinv<>0,.w_Attipinv,iif(Empty(.w_Seriale),0,1))
    endwith
  endproc
  proc Calculate_IWVNPLMZEC()
    with this
          * --- Valorizzazione quadri Analitici
          .w_Attipdat = iif(.w_Attipagr='S','N','S')
          .w_AtquadFE = iif(.w_Attipagr='S' ,'0',.w_AtquadFE)
          .w_AtquadFR = iif(.w_Attipagr='S','0',.w_AtquadFR)
          .w_AtquadNE = iif(.w_Attipagr='S' ,'0',.w_AtquadNE)
          .w_AtquadNR = iif(.w_Attipagr='S' ,'0',.w_AtquadNR)
          .w_AtquadDF = iif(.w_Attipagr='S','0',.w_AtquadDF)
          .w_AtquadFN = iif(.w_Attipagr='S','0',.w_AtquadFN)
          .w_AtquadTU = iif(.w_Attipagr='S' ,'0',.w_AtquadTU)
          .w_AtquadSE = iif(.w_Attipagr='S','0',.w_AtquadSE)
    endwith
  endproc
  proc Calculate_HJUUQJSPGT()
    with this
          * --- Valorizzazione quadri Aggregati
          .w_Attipagr = iif(.w_Attipdat='S','N','S')
          .w_AtquadFA = iif(.w_Attipdat='S','0',.w_AtquadFA)
          .w_AtquadSA = iif(.w_Attipdat='S' ,'0',.w_AtquadSA)
          .w_AtquaaTU = iif(.w_Attipdat='S' ,'0',.w_AtquaaTU)
          .w_AtquadBL = iif(.w_Attipdat='S' ,'0',.w_AtquadBL)
    endwith
  endproc
  proc Calculate_ZELNCQUIYD()
    with this
          * --- Pulisco valore campo Nome file
          .w_ATNOMFIL = Space(254)
    endwith
  endproc
  proc Calculate_ZJFMMVWDUN()
    with this
          * --- Inizializzo nome file
          .w_ATNOMFIL = iif(not empty(.w_ATNOMFIL), .w_ATNOMFIL, left(.w_DirName+'SP'+alltrim(i_CODAZI)+STR(.w_AT__ANNO,4)+.w_ATSERIAL+'.ART21'+space(254),254))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATGENSPE_1_2.enabled = this.oPgFrm.Page1.oPag.oATGENSPE_1_2.mCond()
    this.oPgFrm.Page1.oPag.oATGENBLK_1_3.enabled = this.oPgFrm.Page1.oPag.oATGENBLK_1_3.mCond()
    this.oPgFrm.Page1.oPag.oATGENSMA_1_4.enabled = this.oPgFrm.Page1.oPag.oATGENSMA_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAT__ANNO_1_9.enabled = this.oPgFrm.Page1.oPag.oAT__ANNO_1_9.mCond()
    this.oPgFrm.Page1.oPag.oATTIPPER_1_11.enabled = this.oPgFrm.Page1.oPag.oATTIPPER_1_11.mCond()
    this.oPgFrm.Page1.oPag.oAT__MESE_1_12.enabled = this.oPgFrm.Page1.oPag.oAT__MESE_1_12.mCond()
    this.oPgFrm.Page1.oPag.oATTRIMES_1_13.enabled = this.oPgFrm.Page1.oPag.oATTRIMES_1_13.mCond()
    this.oPgFrm.Page1.oPag.oATTIPINV_1_22.enabled = this.oPgFrm.Page1.oPag.oATTIPINV_1_22.mCond()
    this.oPgFrm.Page1.oPag.oATPROTEC_1_23.enabled = this.oPgFrm.Page1.oPag.oATPROTEC_1_23.mCond()
    this.oPgFrm.Page1.oPag.oATPRODOC_1_24.enabled = this.oPgFrm.Page1.oPag.oATPRODOC_1_24.mCond()
    this.oPgFrm.Page1.oPag.oATTIPAGR_1_30.enabled = this.oPgFrm.Page1.oPag.oATTIPAGR_1_30.mCond()
    this.oPgFrm.Page1.oPag.oATQUADFA_1_31.enabled = this.oPgFrm.Page1.oPag.oATQUADFA_1_31.mCond()
    this.oPgFrm.Page1.oPag.oATQUADSA_1_32.enabled = this.oPgFrm.Page1.oPag.oATQUADSA_1_32.mCond()
    this.oPgFrm.Page1.oPag.oATQUAATU_1_33.enabled = this.oPgFrm.Page1.oPag.oATQUAATU_1_33.mCond()
    this.oPgFrm.Page1.oPag.oATQUADBL_1_34.enabled = this.oPgFrm.Page1.oPag.oATQUADBL_1_34.mCond()
    this.oPgFrm.Page1.oPag.oATTIPDAT_1_35.enabled = this.oPgFrm.Page1.oPag.oATTIPDAT_1_35.mCond()
    this.oPgFrm.Page1.oPag.oATQUADFE_1_36.enabled = this.oPgFrm.Page1.oPag.oATQUADFE_1_36.mCond()
    this.oPgFrm.Page1.oPag.oATQUADFR_1_37.enabled = this.oPgFrm.Page1.oPag.oATQUADFR_1_37.mCond()
    this.oPgFrm.Page1.oPag.oATQUADNE_1_38.enabled = this.oPgFrm.Page1.oPag.oATQUADNE_1_38.mCond()
    this.oPgFrm.Page1.oPag.oATQUADNR_1_39.enabled = this.oPgFrm.Page1.oPag.oATQUADNR_1_39.mCond()
    this.oPgFrm.Page1.oPag.oATQUADDF_1_40.enabled = this.oPgFrm.Page1.oPag.oATQUADDF_1_40.mCond()
    this.oPgFrm.Page1.oPag.oATQUADFN_1_41.enabled = this.oPgFrm.Page1.oPag.oATQUADFN_1_41.mCond()
    this.oPgFrm.Page1.oPag.oATQUADTU_1_43.enabled = this.oPgFrm.Page1.oPag.oATQUADTU_1_43.mCond()
    this.oPgFrm.Page1.oPag.oATQUADSE_1_44.enabled = this.oPgFrm.Page1.oPag.oATQUADSE_1_44.mCond()
    this.oPgFrm.Page2.oPag.oATCOGNOM_2_29.enabled = this.oPgFrm.Page2.oPag.oATCOGNOM_2_29.mCond()
    this.oPgFrm.Page2.oPag.oAT__NOME_2_30.enabled = this.oPgFrm.Page2.oPag.oAT__NOME_2_30.mCond()
    this.oPgFrm.Page2.oPag.oAT_SESSO_2_31.enabled = this.oPgFrm.Page2.oPag.oAT_SESSO_2_31.mCond()
    this.oPgFrm.Page2.oPag.oATDATNAS_2_32.enabled = this.oPgFrm.Page2.oPag.oATDATNAS_2_32.mCond()
    this.oPgFrm.Page2.oPag.oATLOCNAS_2_37.enabled = this.oPgFrm.Page2.oPag.oATLOCNAS_2_37.mCond()
    this.oPgFrm.Page2.oPag.oATPRONAS_2_38.enabled = this.oPgFrm.Page2.oPag.oATPRONAS_2_38.mCond()
    this.oPgFrm.Page2.oPag.oATRAGSOC_2_40.enabled = this.oPgFrm.Page2.oPag.oATRAGSOC_2_40.mCond()
    this.oPgFrm.Page3.oPag.oATCFISOT_3_2.enabled = this.oPgFrm.Page3.oPag.oATCFISOT_3_2.mCond()
    this.oPgFrm.Page3.oPag.oATCODCAR_3_3.enabled = this.oPgFrm.Page3.oPag.oATCODCAR_3_3.mCond()
    this.oPgFrm.Page3.oPag.oATDATIPR_3_4.enabled = this.oPgFrm.Page3.oPag.oATDATIPR_3_4.mCond()
    this.oPgFrm.Page3.oPag.oATDATFPR_3_5.enabled = this.oPgFrm.Page3.oPag.oATDATFPR_3_5.mCond()
    this.oPgFrm.Page3.oPag.oATCOGSOT_3_7.enabled = this.oPgFrm.Page3.oPag.oATCOGSOT_3_7.mCond()
    this.oPgFrm.Page3.oPag.oATNOMSOT_3_9.enabled = this.oPgFrm.Page3.oPag.oATNOMSOT_3_9.mCond()
    this.oPgFrm.Page3.oPag.oATSESSOT_3_10.enabled = this.oPgFrm.Page3.oPag.oATSESSOT_3_10.mCond()
    this.oPgFrm.Page3.oPag.oATDTNSST_3_12.enabled = this.oPgFrm.Page3.oPag.oATDTNSST_3_12.mCond()
    this.oPgFrm.Page3.oPag.oATLCNSST_3_14.enabled = this.oPgFrm.Page3.oPag.oATLCNSST_3_14.mCond()
    this.oPgFrm.Page3.oPag.oATPRRSST_3_16.enabled = this.oPgFrm.Page3.oPag.oATPRRSST_3_16.mCond()
    this.oPgFrm.Page3.oPag.oATRAGSST_3_26.enabled = this.oPgFrm.Page3.oPag.oATRAGSST_3_26.mCond()
    this.oPgFrm.Page4.oPag.oATIMPTRA_4_9.enabled = this.oPgFrm.Page4.oPag.oATIMPTRA_4_9.mCond()
    this.oPgFrm.Page4.oPag.oATCODCAF_4_11.enabled = this.oPgFrm.Page4.oPag.oATCODCAF_4_11.mCond()
    this.oPgFrm.Page4.oPag.oATDATIMP_4_13.enabled = this.oPgFrm.Page4.oPag.oATDATIMP_4_13.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_19.enabled = this.oPgFrm.Page4.oPag.oBtn_4_19.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_20.enabled = this.oPgFrm.Page4.oPag.oBtn_4_20.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_21.enabled = this.oPgFrm.Page4.oPag.oBtn_4_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oATTIPPER_1_10.visible=!this.oPgFrm.Page1.oPag.oATTIPPER_1_10.mHide()
    this.oPgFrm.Page1.oPag.oATTIPPER_1_11.visible=!this.oPgFrm.Page1.oPag.oATTIPPER_1_11.mHide()
    this.oPgFrm.Page1.oPag.oAT__MESE_1_12.visible=!this.oPgFrm.Page1.oPag.oAT__MESE_1_12.mHide()
    this.oPgFrm.Page1.oPag.oATTRIMES_1_13.visible=!this.oPgFrm.Page1.oPag.oATTRIMES_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_19.visible=!this.oPgFrm.Page4.oPag.oBtn_4_19.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_20.visible=!this.oPgFrm.Page4.oPag.oBtn_4_20.mHide()
    this.oPgFrm.Page4.oPag.oLinkPC_4_29.visible=!this.oPgFrm.Page4.oPag.oLinkPC_4_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("New record")
          .Calculate_RLORVZHNOA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_ZJFMMVWDUN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PA__ANNO
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DATPARSP_IDX,3]
    i_lTable = "DATPARSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2], .t., this.DATPARSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PA__ANNO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PA__ANNO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PE__ANNO,PEIMPMIN,PECONIMP,PESCLIMP,PEIMPMAX,PEIVANOS";
                   +" from "+i_cTable+" "+i_lTable+" where PE__ANNO="+cp_ToStrODBC(this.w_PA__ANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PE__ANNO',this.w_PA__ANNO)
            select PE__ANNO,PEIMPMIN,PECONIMP,PESCLIMP,PEIMPMAX,PEIVANOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PA__ANNO = NVL(_Link_.PE__ANNO,space(4))
      this.w_ATIMPMIN = NVL(_Link_.PEIMPMIN,space(1))
      this.w_ATCONIMP = NVL(_Link_.PECONIMP,space(1))
      this.w_ATSCLIMP = NVL(_Link_.PESCLIMP,space(1))
      this.w_ATIMPMAX = NVL(_Link_.PEIMPMAX,space(1))
      this.w_ATIVANOS = NVL(_Link_.PEIVANOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PA__ANNO = space(4)
      endif
      this.w_ATIMPMIN = space(1)
      this.w_ATCONIMP = space(1)
      this.w_ATSCLIMP = space(1)
      this.w_ATIMPMAX = space(1)
      this.w_ATIVANOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2])+'\'+cp_ToStr(_Link_.PE__ANNO,1)
      cp_ShowWarn(i_cKey,this.DATPARSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PA__ANNO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SM_ANNO
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DATPARSM_IDX,3]
    i_lTable = "DATPARSM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DATPARSM_IDX,2], .t., this.DATPARSM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DATPARSM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SM_ANNO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SM_ANNO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SM__ANNO,SMIMPMIN,SMSCLIMP,SMIMPMAX";
                   +" from "+i_cTable+" "+i_lTable+" where SM__ANNO="+cp_ToStrODBC(this.w_SM_ANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SM__ANNO',this.w_SM_ANNO)
            select SM__ANNO,SMIMPMIN,SMSCLIMP,SMIMPMAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SM_ANNO = NVL(_Link_.SM__ANNO,space(4))
      this.w_SMIMPMIN = NVL(_Link_.SMIMPMIN,space(1))
      this.w_SMSCLIMP = NVL(_Link_.SMSCLIMP,space(1))
      this.w_SMIMPMAX = NVL(_Link_.SMIMPMAX,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SM_ANNO = space(4)
      endif
      this.w_SMIMPMIN = space(1)
      this.w_SMSCLIMP = space(1)
      this.w_SMIMPMAX = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DATPARSM_IDX,2])+'\'+cp_ToStr(_Link_.SM__ANNO,1)
      cp_ShowWarn(i_cKey,this.DATPARSM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SM_ANNO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPIVAZI,AZCOFAZI,AZPERAZI,AZCODNAZ,AZLOCAZI,AZPROAZI,AZNAGAZI,AZTELEFO,AZ_EMAIL,AZTELFAX,AZIVACOF";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZPIVAZI,AZCOFAZI,AZPERAZI,AZCODNAZ,AZLOCAZI,AZPROAZI,AZNAGAZI,AZTELEFO,AZ_EMAIL,AZTELFAX,AZIVACOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZCOFAZI = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZCODNAZ = NVL(_Link_.AZCODNAZ,space(3))
      this.w_AZLOCAZI = NVL(_Link_.AZLOCAZI,space(30))
      this.w_AZPROAZI = NVL(_Link_.AZPROAZI,space(2))
      this.w_AZNAGAZI = NVL(_Link_.AZNAGAZI,space(5))
      this.w_AZTELEFO = NVL(_Link_.AZTELEFO,space(18))
      this.w_AZ_EMAIL = NVL(_Link_.AZ_EMAIL,space(254))
      this.w_AZTELFAX = NVL(_Link_.AZTELFAX,space(18))
      this.w_AZIVACOF = NVL(_Link_.AZIVACOF,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZPIVAZI = space(12)
      this.w_AZCOFAZI = space(16)
      this.w_AZPERAZI = space(1)
      this.w_AZCODNAZ = space(3)
      this.w_AZLOCAZI = space(30)
      this.w_AZPROAZI = space(2)
      this.w_AZNAGAZI = space(5)
      this.w_AZTELEFO = space(18)
      this.w_AZ_EMAIL = space(254)
      this.w_AZTELFAX = space(18)
      this.w_AZIVACOF = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI2)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT1 = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT1 = NVL(_Link_.TTNOMTIT,space(25))
      this.w_DATNAS1 = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_LOCTIT1 = NVL(_Link_.TTLUONAS,space(30))
      this.w_PROTIT1 = NVL(_Link_.TTPRONAS,space(2))
      this.w_SESSO1 = NVL(_Link_.TT_SESSO,space(1))
      this.w_TELEFO1 = NVL(_Link_.TTTELEFO,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI2 = space(5)
      endif
      this.w_COGTIT1 = space(25)
      this.w_NOMTIT1 = space(25)
      this.w_DATNAS1 = ctod("  /  /  ")
      this.w_LOCTIT1 = space(30)
      this.w_PROTIT1 = space(2)
      this.w_SESSO1 = space(1)
      this.w_TELEFO1 = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATGENSPE_1_2.RadioValue()==this.w_ATGENSPE)
      this.oPgFrm.Page1.oPag.oATGENSPE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATGENBLK_1_3.RadioValue()==this.w_ATGENBLK)
      this.oPgFrm.Page1.oPag.oATGENBLK_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATGENSMA_1_4.RadioValue()==this.w_ATGENSMA)
      this.oPgFrm.Page1.oPag.oATGENSMA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAT__ANNO_1_9.value==this.w_AT__ANNO)
      this.oPgFrm.Page1.oPag.oAT__ANNO_1_9.value=this.w_AT__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPPER_1_10.RadioValue()==this.w_ATTIPPER)
      this.oPgFrm.Page1.oPag.oATTIPPER_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPPER_1_11.RadioValue()==this.w_ATTIPPER)
      this.oPgFrm.Page1.oPag.oATTIPPER_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAT__MESE_1_12.value==this.w_AT__MESE)
      this.oPgFrm.Page1.oPag.oAT__MESE_1_12.value=this.w_AT__MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oATTRIMES_1_13.value==this.w_ATTRIMES)
      this.oPgFrm.Page1.oPag.oATTRIMES_1_13.value=this.w_ATTRIMES
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPINV_1_22.RadioValue()==this.w_ATTIPINV)
      this.oPgFrm.Page1.oPag.oATTIPINV_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATPROTEC_1_23.value==this.w_ATPROTEC)
      this.oPgFrm.Page1.oPag.oATPROTEC_1_23.value=this.w_ATPROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oATPRODOC_1_24.value==this.w_ATPRODOC)
      this.oPgFrm.Page1.oPag.oATPRODOC_1_24.value=this.w_ATPRODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPAGR_1_30.RadioValue()==this.w_ATTIPAGR)
      this.oPgFrm.Page1.oPag.oATTIPAGR_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADFA_1_31.RadioValue()==this.w_ATQUADFA)
      this.oPgFrm.Page1.oPag.oATQUADFA_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADSA_1_32.RadioValue()==this.w_ATQUADSA)
      this.oPgFrm.Page1.oPag.oATQUADSA_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUAATU_1_33.RadioValue()==this.w_ATQUAATU)
      this.oPgFrm.Page1.oPag.oATQUAATU_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADBL_1_34.RadioValue()==this.w_ATQUADBL)
      this.oPgFrm.Page1.oPag.oATQUADBL_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPDAT_1_35.RadioValue()==this.w_ATTIPDAT)
      this.oPgFrm.Page1.oPag.oATTIPDAT_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADFE_1_36.RadioValue()==this.w_ATQUADFE)
      this.oPgFrm.Page1.oPag.oATQUADFE_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADFR_1_37.RadioValue()==this.w_ATQUADFR)
      this.oPgFrm.Page1.oPag.oATQUADFR_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADNE_1_38.RadioValue()==this.w_ATQUADNE)
      this.oPgFrm.Page1.oPag.oATQUADNE_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADNR_1_39.RadioValue()==this.w_ATQUADNR)
      this.oPgFrm.Page1.oPag.oATQUADNR_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADDF_1_40.RadioValue()==this.w_ATQUADDF)
      this.oPgFrm.Page1.oPag.oATQUADDF_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADFN_1_41.RadioValue()==this.w_ATQUADFN)
      this.oPgFrm.Page1.oPag.oATQUADFN_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADTU_1_43.RadioValue()==this.w_ATQUADTU)
      this.oPgFrm.Page1.oPag.oATQUADTU_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATQUADSE_1_44.RadioValue()==this.w_ATQUADSE)
      this.oPgFrm.Page1.oPag.oATQUADSE_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODFIS_2_21.value==this.w_ATCODFIS)
      this.oPgFrm.Page2.oPag.oATCODFIS_2_21.value=this.w_ATCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oATPARIVA_2_22.value==this.w_ATPARIVA)
      this.oPgFrm.Page2.oPag.oATPARIVA_2_22.value=this.w_ATPARIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODATT_2_23.value==this.w_ATCODATT)
      this.oPgFrm.Page2.oPag.oATCODATT_2_23.value=this.w_ATCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oAT__MAIL_2_24.value==this.w_AT__MAIL)
      this.oPgFrm.Page2.oPag.oAT__MAIL_2_24.value=this.w_AT__MAIL
    endif
    if not(this.oPgFrm.Page2.oPag.oATTELEFO_2_25.value==this.w_ATTELEFO)
      this.oPgFrm.Page2.oPag.oATTELEFO_2_25.value=this.w_ATTELEFO
    endif
    if not(this.oPgFrm.Page2.oPag.oATNUMFAX_2_26.value==this.w_ATNUMFAX)
      this.oPgFrm.Page2.oPag.oATNUMFAX_2_26.value=this.w_ATNUMFAX
    endif
    if not(this.oPgFrm.Page2.oPag.oATCOGNOM_2_29.value==this.w_ATCOGNOM)
      this.oPgFrm.Page2.oPag.oATCOGNOM_2_29.value=this.w_ATCOGNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oAT__NOME_2_30.value==this.w_AT__NOME)
      this.oPgFrm.Page2.oPag.oAT__NOME_2_30.value=this.w_AT__NOME
    endif
    if not(this.oPgFrm.Page2.oPag.oAT_SESSO_2_31.RadioValue()==this.w_AT_SESSO)
      this.oPgFrm.Page2.oPag.oAT_SESSO_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATDATNAS_2_32.value==this.w_ATDATNAS)
      this.oPgFrm.Page2.oPag.oATDATNAS_2_32.value=this.w_ATDATNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oATLOCNAS_2_37.value==this.w_ATLOCNAS)
      this.oPgFrm.Page2.oPag.oATLOCNAS_2_37.value=this.w_ATLOCNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oATPRONAS_2_38.value==this.w_ATPRONAS)
      this.oPgFrm.Page2.oPag.oATPRONAS_2_38.value=this.w_ATPRONAS
    endif
    if not(this.oPgFrm.Page2.oPag.oATRAGSOC_2_40.value==this.w_ATRAGSOC)
      this.oPgFrm.Page2.oPag.oATRAGSOC_2_40.value=this.w_ATRAGSOC
    endif
    if not(this.oPgFrm.Page3.oPag.oFORZA_EDIT_3_1.RadioValue()==this.w_FORZA_EDIT)
      this.oPgFrm.Page3.oPag.oFORZA_EDIT_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oATCFISOT_3_2.value==this.w_ATCFISOT)
      this.oPgFrm.Page3.oPag.oATCFISOT_3_2.value=this.w_ATCFISOT
    endif
    if not(this.oPgFrm.Page3.oPag.oATCODCAR_3_3.RadioValue()==this.w_ATCODCAR)
      this.oPgFrm.Page3.oPag.oATCODCAR_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oATDATIPR_3_4.value==this.w_ATDATIPR)
      this.oPgFrm.Page3.oPag.oATDATIPR_3_4.value=this.w_ATDATIPR
    endif
    if not(this.oPgFrm.Page3.oPag.oATDATFPR_3_5.value==this.w_ATDATFPR)
      this.oPgFrm.Page3.oPag.oATDATFPR_3_5.value=this.w_ATDATFPR
    endif
    if not(this.oPgFrm.Page3.oPag.oATCOGSOT_3_7.value==this.w_ATCOGSOT)
      this.oPgFrm.Page3.oPag.oATCOGSOT_3_7.value=this.w_ATCOGSOT
    endif
    if not(this.oPgFrm.Page3.oPag.oATNOMSOT_3_9.value==this.w_ATNOMSOT)
      this.oPgFrm.Page3.oPag.oATNOMSOT_3_9.value=this.w_ATNOMSOT
    endif
    if not(this.oPgFrm.Page3.oPag.oATSESSOT_3_10.RadioValue()==this.w_ATSESSOT)
      this.oPgFrm.Page3.oPag.oATSESSOT_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oATDTNSST_3_12.value==this.w_ATDTNSST)
      this.oPgFrm.Page3.oPag.oATDTNSST_3_12.value=this.w_ATDTNSST
    endif
    if not(this.oPgFrm.Page3.oPag.oATLCNSST_3_14.value==this.w_ATLCNSST)
      this.oPgFrm.Page3.oPag.oATLCNSST_3_14.value=this.w_ATLCNSST
    endif
    if not(this.oPgFrm.Page3.oPag.oATPRRSST_3_16.value==this.w_ATPRRSST)
      this.oPgFrm.Page3.oPag.oATPRRSST_3_16.value=this.w_ATPRRSST
    endif
    if not(this.oPgFrm.Page3.oPag.oATRAGSST_3_26.value==this.w_ATRAGSST)
      this.oPgFrm.Page3.oPag.oATRAGSST_3_26.value=this.w_ATRAGSST
    endif
    if not(this.oPgFrm.Page4.oPag.oATNUMMOD_4_4.value==this.w_ATNUMMOD)
      this.oPgFrm.Page4.oPag.oATNUMMOD_4_4.value=this.w_ATNUMMOD
    endif
    if not(this.oPgFrm.Page4.oPag.oMAXNORIG_4_5.value==this.w_MAXNORIG)
      this.oPgFrm.Page4.oPag.oMAXNORIG_4_5.value=this.w_MAXNORIG
    endif
    if not(this.oPgFrm.Page4.oPag.oATTIPFOR_4_7.RadioValue()==this.w_ATTIPFOR)
      this.oPgFrm.Page4.oPag.oATTIPFOR_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oATCODINT_4_8.value==this.w_ATCODINT)
      this.oPgFrm.Page4.oPag.oATCODINT_4_8.value=this.w_ATCODINT
    endif
    if not(this.oPgFrm.Page4.oPag.oATIMPTRA_4_9.RadioValue()==this.w_ATIMPTRA)
      this.oPgFrm.Page4.oPag.oATIMPTRA_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oATCODCAF_4_11.value==this.w_ATCODCAF)
      this.oPgFrm.Page4.oPag.oATCODCAF_4_11.value=this.w_ATCODCAF
    endif
    if not(this.oPgFrm.Page4.oPag.oATDATIMP_4_13.value==this.w_ATDATIMP)
      this.oPgFrm.Page4.oPag.oATDATIMP_4_13.value=this.w_ATDATIMP
    endif
    if not(this.oPgFrm.Page4.oPag.oATNOMFIL_4_15.value==this.w_ATNOMFIL)
      this.oPgFrm.Page4.oPag.oATNOMFIL_4_15.value=this.w_ATNOMFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oESTDATVER_4_18.RadioValue()==this.w_ESTDATVER)
      this.oPgFrm.Page4.oPag.oESTDATVER_4_18.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'GENFILTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AT__ANNO))  and (.cFunction='Load' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAT__ANNO_1_9.SetFocus()
            i_bnoObbl = !empty(.w_AT__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AT__MESE)) or not(.w_At__Mese>=1 And .w_At__Mese<=12))  and not(.w_Atgenspe='S' Or .w_Attipper $ 'AT')  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAT__MESE_1_12.SetFocus()
            i_bnoObbl = !empty(.w_AT__MESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ATTRIMES)) or not(.w_Attrimes>=1 AND .w_Attrimes<=4))  and not(.w_Atgenspe='S' Or .w_Atgensma='S' Or .w_Attipper $ 'AM')  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTRIMES_1_13.SetFocus()
            i_bnoObbl = !empty(.w_ATTRIMES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Not Empty(.w_Seriale) and .w_Attipinv<>0 )  and (.cFunction='Load' And not Empty(.w_Seriale))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTIPINV_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Nel periodo � gi� presente un invio ordinario")
          case   ((empty(.w_ATPROTEC)) or not(VAL(.w_ATPROTEC)<>0))  and (.w_ATTIPINV<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATPROTEC_1_23.SetFocus()
            i_bnoObbl = !empty(.w_ATPROTEC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATPRODOC))  and (.w_ATTIPINV<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATPRODOC_1_24.SetFocus()
            i_bnoObbl = !empty(.w_ATPRODOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ATCODFIS)) or not(chkcfp(alltrim(.w_ATCODFIS),'CF')))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCODFIS_2_21.SetFocus()
            i_bnoObbl = !empty(.w_ATCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ATPARIVA)) or not(CHKCFP(.w_ATPARIVA,"PI", "", "", "")))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATPARIVA_2_22.SetFocus()
            i_bnoObbl = !empty(.w_ATPARIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATCODATT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCODATT_2_23.SetFocus()
            i_bnoObbl = !empty(.w_ATCODATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_ATTELEFO))=0 and at('/',alltrim(.w_ATTELEFO))=0 and at('-',alltrim(.w_ATTELEFO))=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATTELEFO_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(at(' ',alltrim(.w_ATNUMFAX))=0 and at('/',alltrim(.w_ATNUMFAX))=0 and at('-',alltrim(.w_ATNUMFAX))=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATNUMFAX_2_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   (empty(.w_ATCOGNOM))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCOGNOM_2_29.SetFocus()
            i_bnoObbl = !empty(.w_ATCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AT__NOME))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAT__NOME_2_30.SetFocus()
            i_bnoObbl = !empty(.w_AT__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATDATNAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATDATNAS_2_32.SetFocus()
            i_bnoObbl = !empty(.w_ATDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATLOCNAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATLOCNAS_2_37.SetFocus()
            i_bnoObbl = !empty(.w_ATLOCNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATPRONAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATPRONAS_2_38.SetFocus()
            i_bnoObbl = !empty(.w_ATPRONAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATRAGSOC))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATRAGSOC_2_40.SetFocus()
            i_bnoObbl = !empty(.w_ATRAGSOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_Atcfisot),chkcfp(alltrim(.w_Atcfisot),'CF'),.T.))  and (.w_Azperazi<>'S' or .w_Forza_Edit or Not Empty(.w_Atcfisot))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oATCFISOT_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATCODCAR))  and (Not Empty(.w_Atcfisot))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oATCODCAR_3_3.SetFocus()
            i_bnoObbl = !empty(.w_ATCODCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATNOMSOT))  and (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oATNOMSOT_3_9.SetFocus()
            i_bnoObbl = !empty(.w_ATNOMSOT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATSESSOT))  and (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oATSESSOT_3_10.SetFocus()
            i_bnoObbl = !empty(.w_ATSESSOT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATDTNSST))  and (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oATDTNSST_3_12.SetFocus()
            i_bnoObbl = !empty(.w_ATDTNSST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATLCNSST))  and (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oATLCNSST_3_14.SetFocus()
            i_bnoObbl = !empty(.w_ATLCNSST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATPRRSST))  and (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oATPRRSST_3_16.SetFocus()
            i_bnoObbl = !empty(.w_ATPRRSST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MAXNORIG))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMAXNORIG_4_5.SetFocus()
            i_bnoObbl = !empty(.w_MAXNORIG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_Atcodint) or Chkcfp(alltrim(.w_Atcodint),'CF')) AND (Not Empty(.w_Atcodint) Or .w_Attipfor='01'))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oATCODINT_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fiscale dell'intermediario obbligatorio")
          case   not(Empty(.w_Atcodint) Or .w_Atimptra<>'0')  and (Not Empty(.w_Atcodint))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oATIMPTRA_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATDATIMP))  and (Not Empty(.w_Atcodint))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oATDATIMP_4_13.SetFocus()
            i_bnoObbl = !empty(.w_ATDATIMP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .Gsai_Mds.CheckForm()
      if i_bres
        i_bres=  .Gsai_Mds.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATGENSPE = this.w_ATGENSPE
    this.o_ATGENBLK = this.w_ATGENBLK
    this.o_ATGENSMA = this.w_ATGENSMA
    this.o_AT__ANNO = this.w_AT__ANNO
    this.o_ATTIPPER = this.w_ATTIPPER
    this.o_AT__MESE = this.w_AT__MESE
    this.o_ATTRIMES = this.w_ATTRIMES
    this.o_SERIALE = this.w_SERIALE
    this.o_ATTIPINV = this.w_ATTIPINV
    this.o_ATTIPAGR = this.w_ATTIPAGR
    this.o_ATTIPDAT = this.w_ATTIPDAT
    this.o_FORZA_EDIT = this.w_FORZA_EDIT
    this.o_ATCFISOT = this.w_ATCFISOT
    this.o_ATCOGSOT = this.w_ATCOGSOT
    this.o_ATRAGSST = this.w_ATRAGSST
    this.o_ATTIPFOR = this.w_ATTIPFOR
    this.o_ATCODINT = this.w_ATCODINT
    this.o_ATNOMFIL = this.w_ATNOMFIL
    * --- Gsai_Mds : Depends On
    this.Gsai_Mds.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsai_agtPag1 as StdContainer
  Width  = 790
  height = 426
  stdWidth  = 790
  stdheight = 426
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATGENSPE_1_2 as StdCheck with uid="JNZCSNILSE",rtseq=2,rtrep=.f.,left=8, top=17, caption="Operazioni rilevanti ai fini IVA (Spesometro)",;
    ToolTipText = "Se attivo, genera il file per le operazioni rilevanti ai fini IVA",;
    HelpContextID = 94355787,;
    cFormVar="w_ATGENSPE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATGENSPE_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATGENSPE_1_2.GetRadio()
    this.Parent.oContained.w_ATGENSPE = this.RadioValue()
    return .t.
  endfunc

  func oATGENSPE_1_2.SetRadio()
    this.Parent.oContained.w_ATGENSPE=trim(this.Parent.oContained.w_ATGENSPE)
    this.value = ;
      iif(this.Parent.oContained.w_ATGENSPE=='S',1,;
      0)
  endfunc

  func oATGENSPE_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oATGENBLK_1_3 as StdCheck with uid="KBIGCSDDBL",rtseq=3,rtrep=.f.,left=8, top=46, caption="Operazioni con soggetti black list",;
    ToolTipText = "Se attivo, genera il file per le operazioni con soggetti black list",;
    HelpContextID = 190856879,;
    cFormVar="w_ATGENBLK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATGENBLK_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATGENBLK_1_3.GetRadio()
    this.Parent.oContained.w_ATGENBLK = this.RadioValue()
    return .t.
  endfunc

  func oATGENBLK_1_3.SetRadio()
    this.Parent.oContained.w_ATGENBLK=trim(this.Parent.oContained.w_ATGENBLK)
    this.value = ;
      iif(this.Parent.oContained.w_ATGENBLK=='S',1,;
      0)
  endfunc

  func oATGENBLK_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oATGENSMA_1_4 as StdCheck with uid="ROESRVWPWW",rtseq=4,rtrep=.f.,left=8, top=75, caption="Acquisti da San Marino",;
    ToolTipText = "Se attivo, genera il file per gli acquisti da San Marino",;
    HelpContextID = 174079673,;
    cFormVar="w_ATGENSMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATGENSMA_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATGENSMA_1_4.GetRadio()
    this.Parent.oContained.w_ATGENSMA = this.RadioValue()
    return .t.
  endfunc

  func oATGENSMA_1_4.SetRadio()
    this.Parent.oContained.w_ATGENSMA=trim(this.Parent.oContained.w_ATGENSMA)
    this.value = ;
      iif(this.Parent.oContained.w_ATGENSMA=='S',1,;
      0)
  endfunc

  func oATGENSMA_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oAT__ANNO_1_9 as StdField with uid="BFKAZISJJU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_AT__ANNO", cQueryName = "AT__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 1359531,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=472, Top=19, cSayPict='"9999"', cGetPict='"9999"'

  func oAT__ANNO_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' )
    endwith
   endif
  endfunc


  add object oATTIPPER_1_10 as StdCombo with uid="HPUDPDLJYK",rtseq=6,rtrep=.f.,left=472,top=52,width=137,height=22, enabled=.f.;
    , ToolTipText = "Periodicit� generazione";
    , HelpContextID = 46436696;
    , cFormVar="w_ATTIPPER",RowSource=""+"Annuale,"+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATTIPPER_1_10.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oATTIPPER_1_10.GetRadio()
    this.Parent.oContained.w_ATTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oATTIPPER_1_10.SetRadio()
    this.Parent.oContained.w_ATTIPPER=trim(this.Parent.oContained.w_ATTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPPER=='A',1,;
      iif(this.Parent.oContained.w_ATTIPPER=='M',2,;
      iif(this.Parent.oContained.w_ATTIPPER=='T',3,;
      0)))
  endfunc

  func oATTIPPER_1_10.mHide()
    with this.Parent.oContained
      return (.w_Atgenblk='S')
    endwith
  endfunc


  add object oATTIPPER_1_11 as StdCombo with uid="DXTJUQYRJJ",rtseq=7,rtrep=.f.,left=472,top=52,width=137,height=22;
    , ToolTipText = "Periodicit� generazione";
    , HelpContextID = 46436696;
    , cFormVar="w_ATTIPPER",RowSource=""+"Mensile,"+"Trimestrale,"+"Annuale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATTIPPER_1_11.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oATTIPPER_1_11.GetRadio()
    this.Parent.oContained.w_ATTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oATTIPPER_1_11.SetRadio()
    this.Parent.oContained.w_ATTIPPER=trim(this.Parent.oContained.w_ATTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPPER=='M',1,;
      iif(this.Parent.oContained.w_ATTIPPER=='T',2,;
      iif(this.Parent.oContained.w_ATTIPPER=='A',3,;
      0)))
  endfunc

  func oATTIPPER_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' )
    endwith
   endif
  endfunc

  func oATTIPPER_1_11.mHide()
    with this.Parent.oContained
      return (.w_Atgenspe='S' or .w_Atgensma='S')
    endwith
  endfunc

  add object oAT__MESE_1_12 as StdField with uid="WZAUIMOPXS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AT__MESE", cQueryName = "AT__MESE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese di riferimento",;
    HelpContextID = 128663883,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=685, Top=54, cSayPict='"99"', cGetPict='"99"'

  func oAT__MESE_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oAT__MESE_1_12.mHide()
    with this.Parent.oContained
      return (.w_Atgenspe='S' Or .w_Attipper $ 'AT')
    endwith
  endfunc

  func oAT__MESE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_At__Mese>=1 And .w_At__Mese<=12)
    endwith
    return bRes
  endfunc

  add object oATTRIMES_1_13 as StdField with uid="ODFXSPRUNK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ATTRIMES", cQueryName = "ATTRIMES",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Trimestre di riferimento",;
    HelpContextID = 257790297,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=685, Top=54, cSayPict='"9"', cGetPict='"9"'

  func oATTRIMES_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oATTRIMES_1_13.mHide()
    with this.Parent.oContained
      return (.w_Atgenspe='S' Or .w_Atgensma='S' Or .w_Attipper $ 'AM')
    endwith
  endfunc

  func oATTRIMES_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Attrimes>=1 AND .w_Attrimes<=4)
    endwith
    return bRes
  endfunc


  add object oATTIPINV_1_22 as StdCombo with uid="XTZIJJTBQC",value=1,rtseq=17,rtrep=.f.,left=619,top=155,width=139,height=22;
    , ToolTipText = "Tipologia di comunicazione";
    , HelpContextID = 71003812;
    , cFormVar="w_ATTIPINV",RowSource=""+"Ordinaria,"+"Sostitutiva,"+"Annullamento", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Nel periodo � gi� presente un invio ordinario";
  , bGlobalFont=.t.


  func oATTIPINV_1_22.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oATTIPINV_1_22.GetRadio()
    this.Parent.oContained.w_ATTIPINV = this.RadioValue()
    return .t.
  endfunc

  func oATTIPINV_1_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPINV==0,1,;
      iif(this.Parent.oContained.w_ATTIPINV==1,2,;
      iif(this.Parent.oContained.w_ATTIPINV==2,3,;
      0)))
  endfunc

  func oATTIPINV_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' And not Empty(.w_Seriale))
    endwith
   endif
  endfunc

  func oATTIPINV_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Not Empty(.w_Seriale) and .w_Attipinv<>0 )
    endwith
    return bRes
  endfunc

  add object oATPROTEC_1_23 as StdField with uid="ZJVNFQQLIO",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ATPROTEC", cQueryName = "ATPROTEC",;
    bObbl = .t. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Protocollo della comunicazione da sostituire o annullare",;
    HelpContextID = 113070409,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=291, Top=190, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oATPROTEC_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPINV<>0)
    endwith
   endif
  endfunc

  func oATPROTEC_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ATPROTEC)<>0)
    endwith
    return bRes
  endfunc

  add object oATPRODOC_1_24 as StdField with uid="TSSGDCKFDL",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ATPRODOC", cQueryName = "ATPRODOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo documento da sostituire o da annullare",;
    HelpContextID = 155365047,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=696, Top=190, cSayPict='"999999"', cGetPict='"999999"'

  func oATPRODOC_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPINV<>0)
    endwith
   endif
  endfunc

  add object oATTIPAGR_1_30 as StdCheck with uid="PESYMSNTDY",rtseq=20,rtrep=.f.,left=247, top=245, caption="Dati Aggregati",;
    ToolTipText = "Dati Aggregati",;
    HelpContextID = 63213912,;
    cFormVar="w_ATTIPAGR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTIPAGR_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATTIPAGR_1_30.GetRadio()
    this.Parent.oContained.w_ATTIPAGR = this.RadioValue()
    return .t.
  endfunc

  func oATTIPAGR_1_30.SetRadio()
    this.Parent.oContained.w_ATTIPAGR=trim(this.Parent.oContained.w_ATTIPAGR)
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPAGR=='S',1,;
      0)
  endfunc

  func oATTIPAGR_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADFA_1_31 as StdCheck with uid="JTXCMOHRII",rtseq=21,rtrep=.f.,left=247, top=279, caption="Quadro FA",;
    ToolTipText = "Quadro FA",;
    HelpContextID = 98591047,;
    cFormVar="w_ATQUADFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADFA_1_31.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADFA_1_31.GetRadio()
    this.Parent.oContained.w_ATQUADFA = this.RadioValue()
    return .t.
  endfunc

  func oATQUADFA_1_31.SetRadio()
    this.Parent.oContained.w_ATQUADFA=trim(this.Parent.oContained.w_ATQUADFA)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADFA=='1',1,;
      0)
  endfunc

  func oATQUADFA_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPAGR='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADSA_1_32 as StdCheck with uid="RSZQRXDGWX",rtseq=22,rtrep=.f.,left=377, top=279, caption="Quadro SA",;
    ToolTipText = "Quadro SA",;
    HelpContextID = 98591047,;
    cFormVar="w_ATQUADSA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADSA_1_32.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADSA_1_32.GetRadio()
    this.Parent.oContained.w_ATQUADSA = this.RadioValue()
    return .t.
  endfunc

  func oATQUADSA_1_32.SetRadio()
    this.Parent.oContained.w_ATQUADSA=trim(this.Parent.oContained.w_ATQUADSA)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADSA=='1',1,;
      0)
  endfunc

  func oATQUADSA_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPAGR='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUAATU_1_33 as StdCheck with uid="CHJZKAYPSQ",rtseq=23,rtrep=.f.,left=524, top=279, caption="Quadro TU",;
    ToolTipText = "Quadro TU",;
    HelpContextID = 48259419,;
    cFormVar="w_ATQUAATU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUAATU_1_33.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUAATU_1_33.GetRadio()
    this.Parent.oContained.w_ATQUAATU = this.RadioValue()
    return .t.
  endfunc

  func oATQUAATU_1_33.SetRadio()
    this.Parent.oContained.w_ATQUAATU=trim(this.Parent.oContained.w_ATQUAATU)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUAATU=='1',1,;
      0)
  endfunc

  func oATQUAATU_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPAGR='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADBL_1_34 as StdCheck with uid="JNTHSZZGZT",rtseq=24,rtrep=.f.,left=654, top=279, caption="Quadro BL",;
    ToolTipText = "Quadro BL",;
    HelpContextID = 98591058,;
    cFormVar="w_ATQUADBL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADBL_1_34.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADBL_1_34.GetRadio()
    this.Parent.oContained.w_ATQUADBL = this.RadioValue()
    return .t.
  endfunc

  func oATQUADBL_1_34.SetRadio()
    this.Parent.oContained.w_ATQUADBL=trim(this.Parent.oContained.w_ATQUADBL)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADBL=='1',1,;
      0)
  endfunc

  func oATQUADBL_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPAGR='S' And (.w_Atgenspe='S' Or .w_Atgenblk='S'))
    endwith
   endif
  endfunc

  add object oATTIPDAT_1_35 as StdCheck with uid="OBYJJTWLUO",rtseq=25,rtrep=.f.,left=247, top=315, caption="Dati Analitici",;
    ToolTipText = "Dati analitici",;
    HelpContextID = 113545562,;
    cFormVar="w_ATTIPDAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTIPDAT_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATTIPDAT_1_35.GetRadio()
    this.Parent.oContained.w_ATTIPDAT = this.RadioValue()
    return .t.
  endfunc

  func oATTIPDAT_1_35.SetRadio()
    this.Parent.oContained.w_ATTIPDAT=trim(this.Parent.oContained.w_ATTIPDAT)
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPDAT=='S',1,;
      0)
  endfunc

  func oATTIPDAT_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADFE_1_36 as StdCheck with uid="PVQVOKWGWH",rtseq=26,rtrep=.f.,left=247, top=350, caption="Quadro FE",;
    ToolTipText = "Quadro FE",;
    HelpContextID = 98591051,;
    cFormVar="w_ATQUADFE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADFE_1_36.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADFE_1_36.GetRadio()
    this.Parent.oContained.w_ATQUADFE = this.RadioValue()
    return .t.
  endfunc

  func oATQUADFE_1_36.SetRadio()
    this.Parent.oContained.w_ATQUADFE=trim(this.Parent.oContained.w_ATQUADFE)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADFE=='1',1,;
      0)
  endfunc

  func oATQUADFE_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPDAT='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADFR_1_37 as StdCheck with uid="HXUFSDMBRV",rtseq=27,rtrep=.f.,left=377, top=351, caption="Quadro FR",;
    ToolTipText = "Quadro FR",;
    HelpContextID = 98591064,;
    cFormVar="w_ATQUADFR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADFR_1_37.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADFR_1_37.GetRadio()
    this.Parent.oContained.w_ATQUADFR = this.RadioValue()
    return .t.
  endfunc

  func oATQUADFR_1_37.SetRadio()
    this.Parent.oContained.w_ATQUADFR=trim(this.Parent.oContained.w_ATQUADFR)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADFR=='1',1,;
      0)
  endfunc

  func oATQUADFR_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPDAT='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADNE_1_38 as StdCheck with uid="EZKNNZTTSF",rtseq=28,rtrep=.f.,left=524, top=350, caption="Quadro NE",;
    ToolTipText = "Quadro NE",;
    HelpContextID = 169844405,;
    cFormVar="w_ATQUADNE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADNE_1_38.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADNE_1_38.GetRadio()
    this.Parent.oContained.w_ATQUADNE = this.RadioValue()
    return .t.
  endfunc

  func oATQUADNE_1_38.SetRadio()
    this.Parent.oContained.w_ATQUADNE=trim(this.Parent.oContained.w_ATQUADNE)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADNE=='1',1,;
      0)
  endfunc

  func oATQUADNE_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPDAT='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADNR_1_39 as StdCheck with uid="NKIQKUZNML",rtseq=29,rtrep=.f.,left=654, top=350, caption="Quadro NR",;
    ToolTipText = "Quadro NR",;
    HelpContextID = 169844392,;
    cFormVar="w_ATQUADNR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADNR_1_39.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADNR_1_39.GetRadio()
    this.Parent.oContained.w_ATQUADNR = this.RadioValue()
    return .t.
  endfunc

  func oATQUADNR_1_39.SetRadio()
    this.Parent.oContained.w_ATQUADNR=trim(this.Parent.oContained.w_ATQUADNR)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADNR=='1',1,;
      0)
  endfunc

  func oATQUADNR_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPDAT='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADDF_1_40 as StdCheck with uid="BBKJQEQRPL",rtseq=30,rtrep=.f.,left=247, top=388, caption="Quadro DF",;
    ToolTipText = "Quadro DF",;
    HelpContextID = 98591052,;
    cFormVar="w_ATQUADDF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADDF_1_40.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADDF_1_40.GetRadio()
    this.Parent.oContained.w_ATQUADDF = this.RadioValue()
    return .t.
  endfunc

  func oATQUADDF_1_40.SetRadio()
    this.Parent.oContained.w_ATQUADDF=trim(this.Parent.oContained.w_ATQUADDF)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADDF=='1',1,;
      0)
  endfunc

  func oATQUADDF_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPDAT='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADFN_1_41 as StdCheck with uid="YDNKGDUBEF",rtseq=31,rtrep=.f.,left=377, top=388, caption="Quadro FN",;
    ToolTipText = "Quadro FN",;
    HelpContextID = 98591060,;
    cFormVar="w_ATQUADFN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADFN_1_41.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADFN_1_41.GetRadio()
    this.Parent.oContained.w_ATQUADFN = this.RadioValue()
    return .t.
  endfunc

  func oATQUADFN_1_41.SetRadio()
    this.Parent.oContained.w_ATQUADFN=trim(this.Parent.oContained.w_ATQUADFN)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADFN=='1',1,;
      0)
  endfunc

  func oATQUADFN_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPDAT='S' And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADTU_1_43 as StdCheck with uid="BMATQULOUV",rtseq=32,rtrep=.f.,left=524, top=388, caption="Quadro TU",;
    ToolTipText = "Quadro TU",;
    HelpContextID = 98591067,;
    cFormVar="w_ATQUADTU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADTU_1_43.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADTU_1_43.GetRadio()
    this.Parent.oContained.w_ATQUADTU = this.RadioValue()
    return .t.
  endfunc

  func oATQUADTU_1_43.SetRadio()
    this.Parent.oContained.w_ATQUADTU=trim(this.Parent.oContained.w_ATQUADTU)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADTU=='1',1,;
      0)
  endfunc

  func oATQUADTU_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPDAT='S'  And .w_Atgenspe='S')
    endwith
   endif
  endfunc

  add object oATQUADSE_1_44 as StdCheck with uid="BOEUTGPJNJ",rtseq=33,rtrep=.f.,left=654, top=388, caption="Quadro SE",;
    ToolTipText = "Quadro SE",;
    HelpContextID = 98591051,;
    cFormVar="w_ATQUADSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATQUADSE_1_44.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oATQUADSE_1_44.GetRadio()
    this.Parent.oContained.w_ATQUADSE = this.RadioValue()
    return .t.
  endfunc

  func oATQUADSE_1_44.SetRadio()
    this.Parent.oContained.w_ATQUADSE=trim(this.Parent.oContained.w_ATQUADSE)
    this.value = ;
      iif(this.Parent.oContained.w_ATQUADSE=='1',1,;
      0)
  endfunc

  func oATQUADSE_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPDAT='S')
    endwith
   endif
  endfunc

  add object oStr_1_25 as StdString with uid="TSXMGJAOXV",Visible=.t., Left=344, Top=21,;
    Alignment=1, Width=125, Height=18,;
    Caption="Anno di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ILYQQCORZL",Visible=.t., Left=335, Top=156,;
    Alignment=1, Width=279, Height=18,;
    Caption="Tipologia di comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ZBWKKGSIJD",Visible=.t., Left=14, Top=193,;
    Alignment=1, Width=270, Height=18,;
    Caption="Protocollo comunicazione da sostituire/annullare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ZBUGPNAZTE",Visible=.t., Left=441, Top=193,;
    Alignment=1, Width=249, Height=18,;
    Caption="Protocollo documento da sostituire/annullare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="RLAXCGLDMM",Visible=.t., Left=380, Top=56,;
    Alignment=1, Width=89, Height=18,;
    Caption="Periodicit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="WTJGTBXCGF",Visible=.t., Left=8, Top=157,;
    Alignment=0, Width=161, Height=19,;
    Caption="Tipo di comunicazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="TUXVMHTVAD",Visible=.t., Left=8, Top=247,;
    Alignment=0, Width=211, Height=19,;
    Caption="Formato comunicazione e quadri"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="RVUTHYHKQX",Visible=.t., Left=614, Top=56,;
    Alignment=1, Width=68, Height=18,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_Atgenspe='S' Or .w_Attipper $ 'AT')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="ZFTWFSBDOO",Visible=.t., Left=614, Top=56,;
    Alignment=1, Width=68, Height=18,;
    Caption="Trimestre:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_Atgenspe='S' Or .w_Atgensma='S' Or .w_Attipper $ 'AM')
    endwith
  endfunc

  add object oBox_1_46 as StdBox with uid="GYWLTVHRKY",left=1, top=147, width=788,height=85
enddefine
define class tgsai_agtPag2 as StdContainer
  Width  = 790
  height = 426
  stdWidth  = 790
  stdheight = 426
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATCODFIS_2_21 as StdField with uid="DFQWHMDFGZ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_ATCODFIS", cQueryName = "ATCODFIS",;
    bObbl = .t. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del contribuente",;
    HelpContextID = 133594791,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=110, Top=79, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oATCODFIS_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chkcfp(alltrim(.w_ATCODFIS),'CF'))
    endwith
    return bRes
  endfunc

  add object oATPARIVA_2_22 as StdField with uid="JPNFHTXPJM",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ATPARIVA", cQueryName = "ATPARIVA",;
    bObbl = .t. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA del contribuente",;
    HelpContextID = 198988103,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=386, Top=79, cSayPict='REPLICATE("!",11)', cGetPict='REPLICATE("!",11)', InputMask=replicate('X',11)

  func oATPARIVA_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_ATPARIVA,"PI", "", "", ""))
    endwith
    return bRes
  endfunc

  add object oATCODATT_2_23 as StdField with uid="RNKMVQTHKN",rtseq=61,rtrep=.f.,;
    cFormVar = "w_ATCODATT", cQueryName = "ATCODATT",;
    bObbl = .t. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 50954586,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=670, Top=79, cSayPict='repl("!",6)', cGetPict='repl("!",6)', InputMask=replicate('X',6)

  add object oAT__MAIL_2_24 as StdField with uid="MTZLJHHSMZ",rtseq=62,rtrep=.f.,;
    cFormVar = "w_AT__MAIL", cQueryName = "AT__MAIL",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo mail del contribuente",;
    HelpContextID = 206880430,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=110, Top=127, cSayPict="replicate('!', 50)", cGetPict="replicate('!', 50)", InputMask=replicate('X',50)

  add object oATTELEFO_2_25 as StdField with uid="MUYEBPTRWW",rtseq=63,rtrep=.f.,;
    cFormVar = "w_ATTELEFO", cQueryName = "ATTELEFO",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono o cellulare del contribuente",;
    HelpContextID = 125866325,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=110, Top=170, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oATTELEFO_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_ATTELEFO))=0 and at('/',alltrim(.w_ATTELEFO))=0 and at('-',alltrim(.w_ATTELEFO))=0)
    endwith
    return bRes
  endfunc

  add object oATNUMFAX_2_26 as StdField with uid="WSQUUGFPOX",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ATNUMFAX", cQueryName = "ATNUMFAX",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di fax del contribuente",;
    HelpContextID = 144716126,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=386, Top=170, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oATNUMFAX_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_ATNUMFAX))=0 and at('/',alltrim(.w_ATNUMFAX))=0 and at('-',alltrim(.w_ATNUMFAX))=0)
    endwith
    return bRes
  endfunc

  add object oATCOGNOM_2_29 as StdField with uid="USTYABUOUZ",rtseq=65,rtrep=.f.,;
    cFormVar = "w_ATCOGNOM", cQueryName = "ATCOGNOM",;
    bObbl = .t. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 264666797,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=110, Top=239, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oATCOGNOM_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oAT__NOME_2_30 as StdField with uid="LKFBHLXRZV",rtseq=66,rtrep=.f.,;
    cFormVar = "w_AT__NOME", cQueryName = "AT__NOME",;
    bObbl = .t. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 239386293,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=386, Top=237, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oAT__NOME_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc


  add object oAT_SESSO_2_31 as StdCombo with uid="UFMSDHWBVZ",rtseq=67,rtrep=.f.,left=670,top=238,width=91,height=22;
    , HelpContextID = 85934421;
    , cFormVar="w_AT_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAT_SESSO_2_31.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAT_SESSO_2_31.GetRadio()
    this.Parent.oContained.w_AT_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oAT_SESSO_2_31.SetRadio()
    this.Parent.oContained.w_AT_SESSO=trim(this.Parent.oContained.w_AT_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_AT_SESSO=='M',1,;
      iif(this.Parent.oContained.w_AT_SESSO=='F',2,;
      0))
  endfunc

  func oAT_SESSO_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oATDATNAS_2_32 as StdField with uid="ECDVVXPNAO",rtseq=68,rtrep=.f.,;
    cFormVar = "w_ATDATNAS", cQueryName = "ATDATNAS",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 16486745,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=110, Top=291

  func oATDATNAS_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oATLOCNAS_2_37 as StdField with uid="UCAJUOVOUI",rtseq=69,rtrep=.f.,;
    cFormVar = "w_ATLOCNAS", cQueryName = "ATLOCNAS",;
    bObbl = .t. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 268046681,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=242, Top=291, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oATLOCNAS_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oATLOCNAS_2_37.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_ATLOCNAS",".w_ATPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oATPRONAS_2_38 as StdField with uid="HHSQKKMXTC",rtseq=70,rtrep=.f.,;
    cFormVar = "w_ATPRONAS", cQueryName = "ATPRONAS",;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 12407129,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=670, Top=291, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oATPRONAS_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oATPRONAS_2_38.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_ATPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oATRAGSOC_2_40 as StdField with uid="QITJWPXNAA",rtseq=71,rtrep=.f.,;
    cFormVar = "w_ATRAGSOC", cQueryName = "ATRAGSOC",;
    bObbl = .t. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione o ragione sociale",;
    HelpContextID = 181636791,;
   bGlobalFont=.t.,;
    Height=21, Width=461, Left=110, Top=377, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oATRAGSOC_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  add object oStr_2_27 as StdString with uid="IXLIJYIGMO",Visible=.t., Left=299, Top=81,;
    Alignment=1, Width=84, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="HEUTCFKDNL",Visible=.t., Left=242, Top=273,;
    Alignment=0, Width=193, Height=18,;
    Caption="Comune/stato estero di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="OSGCYPTITY",Visible=.t., Left=110, Top=223,;
    Alignment=1, Width=58, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="LGSUOYWZDY",Visible=.t., Left=386, Top=223,;
    Alignment=0, Width=37, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="JOPERGGNNX",Visible=.t., Left=670, Top=223,;
    Alignment=0, Width=39, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="CNJREOUFHV",Visible=.t., Left=110, Top=273,;
    Alignment=2, Width=85, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="GTAPKTSGGM",Visible=.t., Left=670, Top=273,;
    Alignment=0, Width=109, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="BOGQGLFCGC",Visible=.t., Left=110, Top=355,;
    Alignment=0, Width=188, Height=18,;
    Caption="Denominazione o ragione sociale"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=7, Top=50,;
    Alignment=0, Width=395, Height=19,;
    Caption="Dati del soggetto cui si riferisce la comunicazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="BQYEOWHSUJ",Visible=.t., Left=5, Top=203,;
    Alignment=0, Width=220, Height=18,;
    Caption="Dati anagrafici persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="KWZECAJTFT",Visible=.t., Left=5, Top=328,;
    Alignment=0, Width=283, Height=18,;
    Caption="Dati anagrafici soggetti diversi da persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="MSUZFRVMFD",Visible=.t., Left=6, Top=81,;
    Alignment=1, Width=102, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="KHXJAKVZPQ",Visible=.t., Left=520, Top=81,;
    Alignment=1, Width=146, Height=18,;
    Caption="Codice attivit� prevalente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="DIRMDFWZQS",Visible=.t., Left=111, Top=107,;
    Alignment=0, Width=165, Height=18,;
    Caption="Indirizzo di posta elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="BSANYZJWZM",Visible=.t., Left=386, Top=152,;
    Alignment=0, Width=103, Height=18,;
    Caption="FAX"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="KMVBUPVSTE",Visible=.t., Left=111, Top=152,;
    Alignment=0, Width=139, Height=18,;
    Caption="Numero di telefono"  ;
  , bGlobalFont=.t.

  add object oBox_2_45 as StdBox with uid="WMHLBIJLHD",left=0, top=196, width=782,height=2

  add object oBox_2_46 as StdBox with uid="RQGVVXVVOU",left=0, top=320, width=782,height=2

  add object oBox_2_48 as StdBox with uid="RDMWYOEEFE",left=0, top=43, width=789,height=378
enddefine
define class tgsai_agtPag3 as StdContainer
  Width  = 790
  height = 426
  stdWidth  = 790
  stdheight = 426
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFORZA_EDIT_3_1 as StdCheck with uid="ANPGYPQFYE",rtseq=72,rtrep=.f.,left=671, top=17, caption="Forza editing",;
    HelpContextID = 15058218,;
    cFormVar="w_FORZA_EDIT", bObbl = .f. , nPag = 3;
    , Tabstop=.F.;
   , bGlobalFont=.t.


  func oFORZA_EDIT_3_1.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oFORZA_EDIT_3_1.GetRadio()
    this.Parent.oContained.w_FORZA_EDIT = this.RadioValue()
    return .t.
  endfunc

  func oFORZA_EDIT_3_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FORZA_EDIT==.t.,1,;
      0)
  endfunc

  add object oATCFISOT_3_2 as StdField with uid="VAOMXTNVDK",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ATCFISOT", cQueryName = "ATCFISOT",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del sottoscrittore",;
    HelpContextID = 179273382,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=17, Top=106, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oATCFISOT_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Azperazi<>'S' or .w_Forza_Edit or Not Empty(.w_Atcfisot))
    endwith
   endif
  endfunc

  func oATCFISOT_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Atcfisot),chkcfp(alltrim(.w_Atcfisot),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oATCODCAR_3_3 as StdCombo with uid="YYMESQRZRO",rtseq=74,rtrep=.f.,left=215,top=104,width=286,height=22;
    , ToolTipText = "Codice carica";
    , HelpContextID = 84509016;
    , cFormVar="w_ATCODCAR",RowSource=""+""+l_descri1+","+""+l_descri2+","+""+l_descri3+","+""+l_descri4+","+""+l_descri5+","+""+l_descri6+","+""+l_descri7+","+""+l_descri8+","+""+l_descri9+","+""+l_descri10+","+""+l_descri11+","+""+l_descri12+"", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oATCODCAR_3_3.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'10',;
    iif(this.value =11,'11',;
    iif(this.value =12,'12',;
    '  ')))))))))))))
  endfunc
  func oATCODCAR_3_3.GetRadio()
    this.Parent.oContained.w_ATCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oATCODCAR_3_3.SetRadio()
    this.Parent.oContained.w_ATCODCAR=trim(this.Parent.oContained.w_ATCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_ATCODCAR=='1',1,;
      iif(this.Parent.oContained.w_ATCODCAR=='2',2,;
      iif(this.Parent.oContained.w_ATCODCAR=='3',3,;
      iif(this.Parent.oContained.w_ATCODCAR=='4',4,;
      iif(this.Parent.oContained.w_ATCODCAR=='5',5,;
      iif(this.Parent.oContained.w_ATCODCAR=='6',6,;
      iif(this.Parent.oContained.w_ATCODCAR=='7',7,;
      iif(this.Parent.oContained.w_ATCODCAR=='8',8,;
      iif(this.Parent.oContained.w_ATCODCAR=='9',9,;
      iif(this.Parent.oContained.w_ATCODCAR=='10',10,;
      iif(this.Parent.oContained.w_ATCODCAR=='11',11,;
      iif(this.Parent.oContained.w_ATCODCAR=='12',12,;
      0))))))))))))
  endfunc

  func oATCODCAR_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcfisot))
    endwith
   endif
  endfunc

  add object oATDATIPR_3_4 as StdField with uid="HCXWAYBUDK",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ATDATIPR", cQueryName = "ATDATIPR",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio procedura o data di decesso contribuente",;
    HelpContextID = 67399336,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=515, Top=106

  func oATDATIPR_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcfisot))
    endwith
   endif
  endfunc

  add object oATDATFPR_3_5 as StdField with uid="KNWFXNMGVC",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ATDATFPR", cQueryName = "ATDATFPR",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 117730984,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=653, Top=106

  func oATDATFPR_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcfisot))
    endwith
   endif
  endfunc

  add object oATCOGSOT_3_7 as StdField with uid="RXOMZIHAKM",rtseq=77,rtrep=.f.,;
    cFormVar = "w_ATCOGSOT", cQueryName = "ATCOGSOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome dichiarante diverso",;
    HelpContextID = 180780710,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=57, Top=214, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oATCOGSOT_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Atragsst) and (.w_Azperazi<>'S' or .w_Forza_Edit))
    endwith
   endif
  endfunc

  add object oATNOMSOT_3_9 as StdField with uid="JCXFNSENGE",rtseq=78,rtrep=.f.,;
    cFormVar = "w_ATNOMSOT", cQueryName = "ATNOMSOT",;
    bObbl = .t. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome dichiarante diverso",;
    HelpContextID = 174444198,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=306, Top=214, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oATNOMSOT_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
    endwith
   endif
  endfunc


  add object oATSESSOT_3_10 as StdCombo with uid="WCRTCQLBZV",rtseq=79,rtrep=.f.,left=653,top=212,width=91,height=22;
    , HelpContextID = 168787622;
    , cFormVar="w_ATSESSOT",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oATSESSOT_3_10.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oATSESSOT_3_10.GetRadio()
    this.Parent.oContained.w_ATSESSOT = this.RadioValue()
    return .t.
  endfunc

  func oATSESSOT_3_10.SetRadio()
    this.Parent.oContained.w_ATSESSOT=trim(this.Parent.oContained.w_ATSESSOT)
    this.value = ;
      iif(this.Parent.oContained.w_ATSESSOT=='M',1,;
      iif(this.Parent.oContained.w_ATSESSOT=='F',2,;
      0))
  endfunc

  func oATSESSOT_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
    endwith
   endif
  endfunc

  add object oATDTNSST_3_12 as StdField with uid="BGQJMZVANZ",rtseq=80,rtrep=.f.,;
    cFormVar = "w_ATDTNSST", cQueryName = "ATDTNSST",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita dichiarante diverso",;
    HelpContextID = 95326554,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=57, Top=272

  func oATDTNSST_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
    endwith
   endif
  endfunc

  add object oATLCNSST_3_14 as StdField with uid="MEMPOPBQLG",rtseq=81,rtrep=.f.,;
    cFormVar = "w_ATLCNSST", cQueryName = "ATLCNSST",;
    bObbl = .t. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita dichiarante diverso",;
    HelpContextID = 94245210,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=306, Top=272, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oATLCNSST_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
    endwith
   endif
  endfunc

  proc oATLCNSST_3_14.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_ATLCNSST",".w_ATPRRSST")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oATPRRSST_3_16 as StdField with uid="ZSQNJWKRWC",rtseq=82,rtrep=.f.,;
    cFormVar = "w_ATPRRSST", cQueryName = "ATPRRSST",;
    bObbl = .t. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita dichiarante diverso",;
    HelpContextID = 99438938,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=653, Top=272, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oATPRRSST_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or .w_Forza_Edit))
    endwith
   endif
  endfunc

  proc oATPRRSST_3_16.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_ATPRRSST")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oATRAGSST_3_26 as StdField with uid="JAPUIPIJOS",rtseq=83,rtrep=.f.,;
    cFormVar = "w_ATRAGSST", cQueryName = "ATRAGSST",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione sottoscrittore ",;
    HelpContextID = 86798682,;
   bGlobalFont=.t.,;
    Height=21, Width=461, Left=57, Top=380, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oATRAGSST_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Atcogsot) and (.w_Azperazi<>'S' or (.w_Azperazi='S' and .w_Forza_Edit)))
    endwith
   endif
  endfunc

  add object oStr_3_6 as StdString with uid="FNFBPKWGUY",Visible=.t., Left=17, Top=88,;
    Alignment=0, Width=123, Height=18,;
    Caption="Codice fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="PPEHDYKWYQ",Visible=.t., Left=57, Top=196,;
    Alignment=0, Width=165, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="JFZOCGHHIH",Visible=.t., Left=306, Top=197,;
    Alignment=0, Width=144, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="WXSCBRPDLD",Visible=.t., Left=57, Top=252,;
    Alignment=0, Width=92, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="ZDPVXMBGXY",Visible=.t., Left=306, Top=254,;
    Alignment=0, Width=300, Height=18,;
    Caption="Comune/stato estero di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="UAQHHMUPJS",Visible=.t., Left=653, Top=254,;
    Alignment=0, Width=54, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="HAGPFVSWCH",Visible=.t., Left=8, Top=47,;
    Alignment=0, Width=342, Height=19,;
    Caption="Dati del soggetto tenuto alla comunicazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_20 as StdString with uid="FANRLAONBU",Visible=.t., Left=653, Top=197,;
    Alignment=0, Width=39, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="LFXDIKGIJH",Visible=.t., Left=215, Top=88,;
    Alignment=0, Width=175, Height=18,;
    Caption="Codice carica"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="FVFXJKADRN",Visible=.t., Left=515, Top=88,;
    Alignment=0, Width=133, Height=18,;
    Caption="Data inizio procedura"  ;
  , bGlobalFont=.t.

  add object oStr_3_24 as StdString with uid="QPOXUSGFDW",Visible=.t., Left=653, Top=88,;
    Alignment=0, Width=133, Height=18,;
    Caption="Data fine procedura"  ;
  , bGlobalFont=.t.

  add object oStr_3_25 as StdString with uid="UXDKNDZMJF",Visible=.t., Left=57, Top=357,;
    Alignment=0, Width=185, Height=18,;
    Caption="Denominazione o ragione sociale"  ;
  , bGlobalFont=.t.

  add object oStr_3_27 as StdString with uid="ZNXMFUEIMT",Visible=.t., Left=7, Top=170,;
    Alignment=0, Width=220, Height=18,;
    Caption="Dati anagrafici persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_3_28 as StdString with uid="YPGHVSGUWQ",Visible=.t., Left=7, Top=324,;
    Alignment=0, Width=283, Height=18,;
    Caption="Dati anagrafici soggetti diversi da persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="IATNFDRAWZ",Visible=.t., Left=8, Top=66,;
    Alignment=0, Width=534, Height=18,;
    Caption="(Soggetto che effettua la comunicazione, se diverso dal soggetto cui si riferisce la comunicazione)"  ;
  , bGlobalFont=.t.

  add object oBox_3_19 as StdBox with uid="FKHDCRQNTU",left=0, top=41, width=789,height=385

  add object oBox_3_21 as StdBox with uid="MJVFLEHPAA",left=1, top=159, width=787,height=2

  add object oBox_3_29 as StdBox with uid="TWAYJKNXYB",left=1, top=309, width=787,height=2
enddefine
define class tgsai_agtPag4 as StdContainer
  Width  = 790
  height = 426
  stdWidth  = 790
  stdheight = 426
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATNUMMOD_4_4 as StdField with uid="GWWKVWAVVN",rtseq=85,rtrep=.f.,;
    cFormVar = "w_ATNUMMOD", cQueryName = "ATNUMMOD",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale degli invii",;
    HelpContextID = 6278838,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=303, Top=48, cSayPict='"9999"', cGetPict='"9999"'

  add object oMAXNORIG_4_5 as StdField with uid="YWPJFYVBEH",rtseq=86,rtrep=.f.,;
    cFormVar = "w_MAXNORIG", cQueryName = "MAXNORIG",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo documenti per file",;
    HelpContextID = 189153523,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=303, Top=83, cSayPict='"99999"', cGetPict='"99999"'


  add object oATTIPFOR_4_7 as StdCombo with uid="TMIIFPZQPN",rtseq=87,rtrep=.f.,left=303,top=155,width=279,height=22;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 121335464;
    , cFormVar="w_ATTIPFOR",RowSource=""+"01: Soggetti che inviano la propria comunicazione,"+"10: Intermediari", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oATTIPFOR_4_7.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'10',;
    space(2))))
  endfunc
  func oATTIPFOR_4_7.GetRadio()
    this.Parent.oContained.w_ATTIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oATTIPFOR_4_7.SetRadio()
    this.Parent.oContained.w_ATTIPFOR=trim(this.Parent.oContained.w_ATTIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPFOR=='01',1,;
      iif(this.Parent.oContained.w_ATTIPFOR=='10',2,;
      0))
  endfunc

  add object oATCODINT_4_8 as StdField with uid="VHBKITNWRO",rtseq=88,rtrep=.f.,;
    cFormVar = "w_ATCODINT", cQueryName = "ATCODINT",;
    bObbl = .f. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fiscale dell'intermediario obbligatorio",;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 83263142,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=303, Top=192, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oATCODINT_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_Atcodint) or Chkcfp(alltrim(.w_Atcodint),'CF')) AND (Not Empty(.w_Atcodint) Or .w_Attipfor='01'))
    endwith
    return bRes
  endfunc


  add object oATIMPTRA_4_9 as StdCombo with uid="JKQBQAAXZG",rtseq=89,rtrep=.f.,left=303,top=227,width=279,height=22;
    , ToolTipText = "Impegno a trasmettere in via telematica la comunicazione";
    , HelpContextID = 113762631;
    , cFormVar="w_ATIMPTRA",RowSource=""+"1 - Dichiarazione predisposta dal contribuente,"+"2 - Dichiarazione predisposta da chi effettua l'invio", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oATIMPTRA_4_9.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(1))))
  endfunc
  func oATIMPTRA_4_9.GetRadio()
    this.Parent.oContained.w_ATIMPTRA = this.RadioValue()
    return .t.
  endfunc

  func oATIMPTRA_4_9.SetRadio()
    this.Parent.oContained.w_ATIMPTRA=trim(this.Parent.oContained.w_ATIMPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_ATIMPTRA=='1',1,;
      iif(this.Parent.oContained.w_ATIMPTRA=='2',2,;
      0))
  endfunc

  func oATIMPTRA_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcodint))
    endwith
   endif
  endfunc

  func oATIMPTRA_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Empty(.w_Atcodint) Or .w_Atimptra<>'0')
    endwith
    return bRes
  endfunc

  add object oATCODCAF_4_11 as StdField with uid="FCYYNVPVIH",rtseq=90,rtrep=.f.,;
    cFormVar = "w_ATCODCAF", cQueryName = "ATCODCAF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Numero di iscrizione all'albo del C.A.F.",;
    HelpContextID = 84509004,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=303, Top=267, cSayPict='"99999"', cGetPict='"99999"', InputMask=replicate('X',5)

  func oATCODCAF_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcodint))
    endwith
   endif
  endfunc

  add object oATDATIMP_4_13 as StdField with uid="SYRFGCWSSA",rtseq=91,rtrep=.f.,;
    cFormVar = "w_ATDATIMP", cQueryName = "ATDATIMP",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'impegno",;
    HelpContextID = 67399338,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=556, Top=267

  func oATDATIMP_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Atcodint))
    endwith
   endif
  endfunc

  add object oATNOMFIL_4_15 as StdField with uid="VPKTICWKHO",rtseq=92,rtrep=.f.,;
    cFormVar = "w_ATNOMFIL", cQueryName = "ATNOMFIL",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file e percorso per inoltro telematico",;
    HelpContextID = 124112558,;
   bGlobalFont=.t.,;
    Height=21, Width=636, Left=88, Top=327, InputMask=replicate('X',254)


  add object oBtn_4_17 as StdButton with uid="TQEMIMWBDV",left=754, top=327, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file";
    , HelpContextID = 42399274;
  , bGlobalFont=.t.

    proc oBtn_4_17.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oESTDATVER_4_18 as StdCheck with uid="NHMVZZZTAC",rtseq=93,rtrep=.f.,left=88, top=362, caption="Estrarre anche i soggetti da verificare",;
    ToolTipText = "Estrarre anche i soggetti da verificare",;
    HelpContextID = 97490347,;
    cFormVar="w_ESTDATVER", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oESTDATVER_4_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESTDATVER_4_18.GetRadio()
    this.Parent.oContained.w_ESTDATVER = this.RadioValue()
    return .t.
  endfunc

  func oESTDATVER_4_18.SetRadio()
    this.Parent.oContained.w_ESTDATVER=trim(this.Parent.oContained.w_ESTDATVER)
    this.value = ;
      iif(this.Parent.oContained.w_ESTDATVER=='S',1,;
      0)
  endfunc


  add object oBtn_4_19 as StdButton with uid="TSNZLTUIAB",left=616, top=370, width=48,height=45,;
    CpPicture="bmp\PREVIEW.BMP", caption="", nPag=4;
    , ToolTipText = "Stampa gestionale";
    , HelpContextID = 48345622;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_19.Click()
      with this.Parent.oContained
        GSAI_BSF (this.Parent.oContained,"StampaMod")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_AT__ANNO) and Not empty(.w_ATNOMFIL) and Not g_DEMO AND ( .w_ATTIPAGR='S' OR .w_ATTIPDAT='S'  ) )
      endwith
    endif
  endfunc

  func oBtn_4_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY (.w_ATSERIAL))
     endwith
    endif
  endfunc


  add object oBtn_4_20 as StdButton with uid="PJPVILDQVM",left=674, top=370, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Stampa frontespizio e riepilogo";
    , HelpContextID = 48345622;
    , caption='M\<odello';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_20.Click()
      with this.Parent.oContained
        GSAI_BSF (this.Parent.oContained,"StampaFront")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AT__ANNO) and not empty(.w_ATNOMFIL) and not g_DEMO AND (.w_ATTIPAGR='S' OR .w_ATTIPDAT='S'  ) AND .cFunction<> 'Load' )
      endwith
    endif
  endfunc

  func oBtn_4_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY (.w_ATSERIAL))
     endwith
    endif
  endfunc


  add object oBtn_4_21 as StdButton with uid="AYLATODQCL",left=727, top=370, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=4;
    , ToolTipText = "Genera file";
    , HelpContextID = 48345622;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_21.Click()
      with this.Parent.oContained
        do Gsai_Bgt with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AT__ANNO) and not empty(.w_ATNOMFIL) and not g_DEMO AND (.w_ATTIPAGR='S' OR .w_ATTIPDAT='S'))
      endwith
    endif
  endfunc


  add object oLinkPC_4_29 as stdDynamicChildContainer with uid="GQDKEUAHSD",left=435, top=347, width=157, height=74, bOnScreen=.t.;
    , tabstop=.f.

  func oLinkPC_4_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc

  add object oStr_4_6 as StdString with uid="FUOMFIFISI",Visible=.t., Left=142, Top=51,;
    Alignment=1, Width=160, Height=18,;
    Caption="Totale degli invii presentati:"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="NECDVAUAQY",Visible=.t., Left=82, Top=194,;
    Alignment=1, Width=220, Height=18,;
    Caption="Codice fiscale dell'intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="ABCTFAMFTH",Visible=.t., Left=88, Top=268,;
    Alignment=1, Width=214, Height=18,;
    Caption="N. di iscrizione all'albo del C.A.F.:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="AOYTOWESTU",Visible=.t., Left=425, Top=268,;
    Alignment=1, Width=127, Height=18,;
    Caption="Data dell'impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="SSBWQANJWI",Visible=.t., Left=6, Top=331,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_4_22 as StdString with uid="CMEGIEQADK",Visible=.t., Left=15, Top=18,;
    Alignment=0, Width=174, Height=19,;
    Caption="Comunicazione su pi� invii"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_25 as StdString with uid="ERDKNGNDJP",Visible=.t., Left=15, Top=123,;
    Alignment=0, Width=249, Height=19,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_26 as StdString with uid="RCGCOENWMJ",Visible=.t., Left=83, Top=229,;
    Alignment=1, Width=219, Height=18,;
    Caption="Impegno a trasmettere in via telematica:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=215, Top=156,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_28 as StdString with uid="ATUGIPJKAJ",Visible=.t., Left=66, Top=83,;
    Alignment=1, Width=236, Height=18,;
    Caption="Numero massimo documenti per file:"  ;
  , bGlobalFont=.t.

  add object oBox_4_23 as StdBox with uid="XTEZQYXSTV",left=10, top=14, width=778,height=105

  add object oBox_4_24 as StdBox with uid="DQNRVKFLMJ",left=10, top=117, width=778,height=192
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_agt','GENFILTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ATSERIAL=GENFILTE.ATSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsai_agt
proc SfogliaDir (parent)
local PathName, oField
  PathName = cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
    parent.w_ATNOMFIL=left(parent.w_DirName+'SP'+alltrim(i_CODAZI)+STR(parent.w_AT__ANNO,4)+parent.w_ATSERIAL+'.ART21'+space(254),254)
  endif
endproc
* --- Fine Area Manuale
