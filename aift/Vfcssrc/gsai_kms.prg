* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_kms                                                        *
*              Manutenzione dati estratti spesometro                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-14                                                      *
* Last revis.: 2014-01-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_kms",oParentObject))

* --- Class definition
define class tgsai_kms as StdForm
  Top    = 1
  Left   = 4

  * --- Standard Properties
  Width  = 791
  Height = 538+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-01-29"
  HelpContextID=199886697
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  cPrg = "gsai_kms"
  cComment = "Manutenzione dati estratti spesometro"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SP__ANNO = 0
  w_SPDACONT = space(1)
  w_SPSCLGEN = space(1)
  w_SPTIPSOG = space(1)
  o_SPTIPSOG = space(1)
  w_SPPARIVA = space(25)
  w_SPCODFIS = space(25)
  w_SPTIPCON = space(1)
  o_SPTIPCON = space(1)
  w_TIPCON = space(1)
  w_SPCODCON = space(15)
  w_SPCODCONF = space(15)
  w_CONSUP = space(15)
  w_CATCON = space(5)
  w_SPDESCRI = space(40)
  w_SPTIPREG = space(1)
  w_SPESCLGE = space(1)
  w_SPTIPREC = space(1)
  w_SPIVANES = space(1)
  w_SPAUTFAT = space(1)
  w_SPREVCHA = space(1)
  w_SPNOLEGG = space(1)
  w_SPINFUNO = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_SPDESCRIF = space(40)
  w_DESMAS = space(35)
  w_TIPMAS = space(1)
  w_NULIV = 0
  w_DESCON = space(35)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_SPSERIAL = space(10)
  w_CPROWNUM = 0
  w_ORDINA = space(1)
  w_OBJECT = .F.
  w_TRASH = .F.
  w_GEST = .F.
  w_ZoomDe = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_kmsPag1","gsai_kms",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsai_kmsPag2","gsai_kms",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Visualizzazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSP__ANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDe = this.oPgFrm.Pages(2).oPag.ZoomDe
    DoDefault()
    proc Destroy()
      this.w_ZoomDe = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='CACOCLFO'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SP__ANNO=0
      .w_SPDACONT=space(1)
      .w_SPSCLGEN=space(1)
      .w_SPTIPSOG=space(1)
      .w_SPPARIVA=space(25)
      .w_SPCODFIS=space(25)
      .w_SPTIPCON=space(1)
      .w_TIPCON=space(1)
      .w_SPCODCON=space(15)
      .w_SPCODCONF=space(15)
      .w_CONSUP=space(15)
      .w_CATCON=space(5)
      .w_SPDESCRI=space(40)
      .w_SPTIPREG=space(1)
      .w_SPESCLGE=space(1)
      .w_SPTIPREC=space(1)
      .w_SPIVANES=space(1)
      .w_SPAUTFAT=space(1)
      .w_SPREVCHA=space(1)
      .w_SPNOLEGG=space(1)
      .w_SPINFUNO=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_SPDESCRIF=space(40)
      .w_DESMAS=space(35)
      .w_TIPMAS=space(1)
      .w_NULIV=0
      .w_DESCON=space(35)
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_SPSERIAL=space(10)
      .w_CPROWNUM=0
      .w_ORDINA=space(1)
      .w_OBJECT=.f.
      .w_TRASH=.f.
      .w_GEST=.f.
        .w_SP__ANNO = year(i_DATSYS)-1
        .w_SPDACONT = 'T'
        .w_SPSCLGEN = 'T'
        .w_SPTIPSOG = 'X'
          .DoRTCalc(5,6,.f.)
        .w_SPTIPCON = 'N'
        .w_TIPCON = .w_SPTIPCON
        .w_SPCODCON = Space(15)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_SPCODCON))
          .link_1_11('Full')
        endif
        .w_SPCODCONF = Space(15)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_SPCODCONF))
          .link_1_12('Full')
        endif
        .w_CONSUP = SPACE(15)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CONSUP))
          .link_1_13('Full')
        endif
        .w_CATCON = SPACE(5)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CATCON))
          .link_1_14('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_SPTIPREG = 'N'
        .w_SPESCLGE = 'T'
        .w_SPTIPREC = 'TT'
        .w_SPIVANES = 'T'
        .w_SPAUTFAT = 'T'
        .w_SPREVCHA = 'T'
        .w_SPNOLEGG = 'T'
        .w_SPINFUNO = 'N'
        .w_OBTEST = CTOD('01-01-1900')
      .oPgFrm.Page2.oPag.ZoomDe.Calculate()
          .DoRTCalc(23,29,.f.)
        .w_SPSERIAL = NVL(.w_ZOOMDE.getVar('SPSERIAL'), ' ')
        .w_CPROWNUM = NVL(.w_ZOOMDE.getVar('CPROWNUM'), ' ')
        .w_ORDINA = NVL(.w_ZOOMDE.getVar('ORDINA'), ' ')
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
    endwith
    this.DoRTCalc(33,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_SPTIPSOG<>.w_SPTIPSOG
            .w_SPTIPCON = 'N'
        endif
            .w_TIPCON = .w_SPTIPCON
        if .o_SPTIPCON<>.w_SPTIPCON
            .w_SPCODCON = Space(15)
          .link_1_11('Full')
        endif
        if .o_SPTIPCON<>.w_SPTIPCON
            .w_SPCODCONF = Space(15)
          .link_1_12('Full')
        endif
        if .o_SPTIPCON<>.w_SPTIPCON.or. .o_SPTIPSOG<>.w_SPTIPSOG
            .w_CONSUP = SPACE(15)
          .link_1_13('Full')
        endif
        if .o_SPTIPSOG<>.w_SPTIPSOG
            .w_CATCON = SPACE(5)
          .link_1_14('Full')
        endif
        .oPgFrm.Page2.oPag.ZoomDe.Calculate()
        .DoRTCalc(13,29,.t.)
            .w_SPSERIAL = NVL(.w_ZOOMDE.getVar('SPSERIAL'), ' ')
            .w_CPROWNUM = NVL(.w_ZOOMDE.getVar('CPROWNUM'), ' ')
            .w_ORDINA = NVL(.w_ZOOMDE.getVar('ORDINA'), ' ')
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(33,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomDe.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
    endwith
  return

  proc Calculate_OEBUCQVPFH()
    with this
          * --- Apertura gestioni collegate
          .w_OBJECT = Gsai_Asp()
          .w_TRASH = .w_Object.EcpFilter()
          .w_Object.w_Spserial = .w_Spserial
          .w_TRASH = .w_Object.EcpSave()
          .w_Object.opGFRM.ActivePage = iif(.w_Ordina='A',1,2)
          .w_GEST = iif(.w_Ordina='B',.w_Object.Gsai_Msp,.f.)
          .w_TRASH = .w_Gest.Search('Cprownum='+Alltrim(Str(.w_Cprownum,6,0)))
          .w_TRASH = IIF(.w_Trash<0,.f.,.w_Gest.SetRow(.w_Trash))
          .w_TRASH = IIF(.w_Trash,.w_Gest.Refresh(),.f.)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSPTIPCON_1_9.enabled = this.oPgFrm.Page1.oPag.oSPTIPCON_1_9.mCond()
    this.oPgFrm.Page1.oPag.oSPCODCON_1_11.enabled = this.oPgFrm.Page1.oPag.oSPCODCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oSPCODCONF_1_12.enabled = this.oPgFrm.Page1.oPag.oSPCODCONF_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCONSUP_1_13.enabled = this.oPgFrm.Page1.oPag.oCONSUP_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCATCON_1_14.enabled = this.oPgFrm.Page1.oPag.oCATCON_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomDe.Event(cEvent)
        if lower(cEvent)==lower("w_zoomde selected")
          .Calculate_OEBUCQVPFH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SPCODCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SPCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SPTIPCON;
                     ,'ANCODICE',trim(this.w_SPCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SPCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SPTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SPCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSPCODCON_1_11'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SPCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SPCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SPTIPCON;
                       ,'ANCODICE',this.w_SPCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SPCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_SPDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SPCODCON = space(15)
      endif
      this.w_SPDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SPCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SPCODCONF
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SPCODCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SPCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SPTIPCON;
                     ,'ANCODICE',trim(this.w_SPCODCONF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SPCODCONF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SPCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SPCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SPTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SPCODCONF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSPCODCONF_1_12'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SPCODCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SPCODCONF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SPTIPCON;
                       ,'ANCODICE',this.w_SPCODCONF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SPCODCONF = NVL(_Link_.ANCODICE,space(15))
      this.w_SPDESCRIF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SPCODCONF = space(15)
      endif
      this.w_SPDESCRIF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SPCODCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSUP
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSUP))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CONSUP)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSUP_1_13'),i_cWhere,'GSAR_AMC',"MASTRI",'GSAI_MCO.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSUP)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(35))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONSUP = space(15)
      endif
      this.w_DESMAS = space(35)
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND ((.w_TIPCON='N' AND (.w_TIPMAS='C' or .w_TIPMAS='F')) OR (.w_TIPCON=.w_TIPMAS))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CONSUP = space(15)
        this.w_DESMAS = space(35)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_14'),i_cWhere,'GSAR_AC2',"CATEGORIE CONTABILI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSP__ANNO_1_2.value==this.w_SP__ANNO)
      this.oPgFrm.Page1.oPag.oSP__ANNO_1_2.value=this.w_SP__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oSPDACONT_1_3.RadioValue()==this.w_SPDACONT)
      this.oPgFrm.Page1.oPag.oSPDACONT_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPSCLGEN_1_4.RadioValue()==this.w_SPSCLGEN)
      this.oPgFrm.Page1.oPag.oSPSCLGEN_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPTIPSOG_1_6.RadioValue()==this.w_SPTIPSOG)
      this.oPgFrm.Page1.oPag.oSPTIPSOG_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPPARIVA_1_7.value==this.w_SPPARIVA)
      this.oPgFrm.Page1.oPag.oSPPARIVA_1_7.value=this.w_SPPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oSPCODFIS_1_8.value==this.w_SPCODFIS)
      this.oPgFrm.Page1.oPag.oSPCODFIS_1_8.value=this.w_SPCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSPTIPCON_1_9.RadioValue()==this.w_SPTIPCON)
      this.oPgFrm.Page1.oPag.oSPTIPCON_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPCODCON_1_11.value==this.w_SPCODCON)
      this.oPgFrm.Page1.oPag.oSPCODCON_1_11.value=this.w_SPCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSPCODCONF_1_12.value==this.w_SPCODCONF)
      this.oPgFrm.Page1.oPag.oSPCODCONF_1_12.value=this.w_SPCODCONF
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSUP_1_13.value==this.w_CONSUP)
      this.oPgFrm.Page1.oPag.oCONSUP_1_13.value=this.w_CONSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_14.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_14.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSPDESCRI_1_15.value==this.w_SPDESCRI)
      this.oPgFrm.Page1.oPag.oSPDESCRI_1_15.value=this.w_SPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSPTIPREG_1_19.RadioValue()==this.w_SPTIPREG)
      this.oPgFrm.Page1.oPag.oSPTIPREG_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPESCLGE_1_20.RadioValue()==this.w_SPESCLGE)
      this.oPgFrm.Page1.oPag.oSPESCLGE_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPTIPREC_1_21.RadioValue()==this.w_SPTIPREC)
      this.oPgFrm.Page1.oPag.oSPTIPREC_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPIVANES_1_22.RadioValue()==this.w_SPIVANES)
      this.oPgFrm.Page1.oPag.oSPIVANES_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPAUTFAT_1_23.RadioValue()==this.w_SPAUTFAT)
      this.oPgFrm.Page1.oPag.oSPAUTFAT_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPREVCHA_1_24.RadioValue()==this.w_SPREVCHA)
      this.oPgFrm.Page1.oPag.oSPREVCHA_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPNOLEGG_1_25.RadioValue()==this.w_SPNOLEGG)
      this.oPgFrm.Page1.oPag.oSPNOLEGG_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPINFUNO_1_26.RadioValue()==this.w_SPINFUNO)
      this.oPgFrm.Page1.oPag.oSPINFUNO_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPDESCRIF_1_37.value==this.w_SPDESCRIF)
      this.oPgFrm.Page1.oPag.oSPDESCRIF_1_37.value=this.w_SPDESCRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAS_1_38.value==this.w_DESMAS)
      this.oPgFrm.Page1.oPag.oDESMAS_1_38.value=this.w_DESMAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_43.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_43.value=this.w_DESCON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_SP__ANNO)) or not(.w_SP__ANNO>2005 and .w_SP__ANNO<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSP__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_SP__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NULIV=1 AND ((.w_TIPCON='N' AND (.w_TIPMAS='C' or .w_TIPMAS='F')) OR (.w_TIPCON=.w_TIPMAS)))  and (.w_SPTIPSOG $ 'IPNT')  and not(empty(.w_CONSUP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONSUP_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SPTIPSOG = this.w_SPTIPSOG
    this.o_SPTIPCON = this.w_SPTIPCON
    return

enddefine

* --- Define pages as container
define class tgsai_kmsPag1 as StdContainer
  Width  = 787
  height = 538
  stdWidth  = 787
  stdheight = 538
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSP__ANNO_1_2 as StdField with uid="UQWWLVVPBH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SP__ANNO", cQueryName = "SP__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 158646667,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=41, cSayPict='"9999"', cGetPict='"9999"'

  func oSP__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SP__ANNO>2005 and .w_SP__ANNO<2100)
    endwith
    return bRes
  endfunc


  add object oSPDACONT_1_3 as StdCombo with uid="HHHJPCDAST",rtseq=2,rtrep=.f.,left=342,top=41,width=73,height=21;
    , ToolTipText = "Dati estratti da verificare";
    , HelpContextID = 141848966;
    , cFormVar="w_SPDACONT",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPDACONT_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSPDACONT_1_3.GetRadio()
    this.Parent.oContained.w_SPDACONT = this.RadioValue()
    return .t.
  endfunc

  func oSPDACONT_1_3.SetRadio()
    this.Parent.oContained.w_SPDACONT=trim(this.Parent.oContained.w_SPDACONT)
    this.value = ;
      iif(this.Parent.oContained.w_SPDACONT=='S',1,;
      iif(this.Parent.oContained.w_SPDACONT=='N',2,;
      iif(this.Parent.oContained.w_SPDACONT=='T',3,;
      0)))
  endfunc


  add object oSPSCLGEN_1_4 as StdCombo with uid="BQLKCHSUNV",rtseq=3,rtrep=.f.,left=596,top=41,width=73,height=21;
    , ToolTipText = "Escludi da generazione";
    , HelpContextID = 1998452;
    , cFormVar="w_SPSCLGEN",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPSCLGEN_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSPSCLGEN_1_4.GetRadio()
    this.Parent.oContained.w_SPSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oSPSCLGEN_1_4.SetRadio()
    this.Parent.oContained.w_SPSCLGEN=trim(this.Parent.oContained.w_SPSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_SPSCLGEN=='S',1,;
      iif(this.Parent.oContained.w_SPSCLGEN=='N',2,;
      iif(this.Parent.oContained.w_SPSCLGEN=='T',3,;
      0)))
  endfunc


  add object oSPTIPSOG_1_6 as StdCombo with uid="VMJQAYOIOV",rtseq=4,rtrep=.f.,left=125,top=77,width=152,height=21;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 60518803;
    , cFormVar="w_SPTIPSOG",RowSource=""+"Iva,"+"Privato,"+"Non residente,"+"Tutti,"+"Documento riepilogativo,"+"Operazione turismo", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPTIPSOG_1_6.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'N',;
    iif(this.value =4,'X',;
    iif(this.value =5,'D',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oSPTIPSOG_1_6.GetRadio()
    this.Parent.oContained.w_SPTIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPSOG_1_6.SetRadio()
    this.Parent.oContained.w_SPTIPSOG=trim(this.Parent.oContained.w_SPTIPSOG)
    this.value = ;
      iif(this.Parent.oContained.w_SPTIPSOG=='I',1,;
      iif(this.Parent.oContained.w_SPTIPSOG=='P',2,;
      iif(this.Parent.oContained.w_SPTIPSOG=='N',3,;
      iif(this.Parent.oContained.w_SPTIPSOG=='X',4,;
      iif(this.Parent.oContained.w_SPTIPSOG=='D',5,;
      iif(this.Parent.oContained.w_SPTIPSOG=='T',6,;
      0))))))
  endfunc

  add object oSPPARIVA_1_7 as StdField with uid="MUIDWSGMOT",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SPPARIVA", cQueryName = "SPPARIVA",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 41700967,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=124, Top=109, InputMask=replicate('X',25)

  add object oSPCODFIS_1_8 as StdField with uid="FPPNBONOQK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SPCODFIS", cQueryName = "SPCODFIS",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 245988985,;
   bGlobalFont=.t.,;
    Height=21, Width=186, Left=420, Top=109, InputMask=replicate('X',25)


  add object oSPTIPCON_1_9 as StdCombo with uid="JNYAXOMVNG",rtseq=7,rtrep=.f.,left=125,top=144,width=109,height=21;
    , HelpContextID = 60518796;
    , cFormVar="w_SPTIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Nessun filtro", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPTIPCON_1_9.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oSPTIPCON_1_9.GetRadio()
    this.Parent.oContained.w_SPTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPCON_1_9.SetRadio()
    this.Parent.oContained.w_SPTIPCON=trim(this.Parent.oContained.w_SPTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_SPTIPCON=='C',1,;
      iif(this.Parent.oContained.w_SPTIPCON=='F',2,;
      iif(this.Parent.oContained.w_SPTIPCON=='N',3,;
      0)))
  endfunc

  func oSPTIPCON_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SPTIPSOG $ 'IPNT')
    endwith
   endif
  endfunc

  func oSPTIPCON_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SPCODCON)
        bRes2=.link_1_11('Full')
      endif
      if .not. empty(.w_SPCODCONF)
        bRes2=.link_1_12('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSPCODCON_1_11 as StdField with uid="UCJURWGOQP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SPCODCON", cQueryName = "SPCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto iniziale",;
    HelpContextID = 72778124,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=273, Top=144, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SPCODCON"

  func oSPCODCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SPTIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oSPCODCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oSPCODCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSPCODCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSPCODCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oSPCODCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SPTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SPCODCON
     i_obj.ecpSave()
  endproc

  add object oSPCODCONF_1_12 as StdField with uid="INMNTQUMMI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SPCODCONF", cQueryName = "SPCODCONF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto finale",;
    HelpContextID = 72777004,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=273, Top=183, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SPCODCONF"

  func oSPCODCONF_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SPTIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oSPCODCONF_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oSPCODCONF_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSPCODCONF_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSPCODCONF_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oSPCODCONF_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SPTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SPCODCONF
     i_obj.ecpSave()
  endproc

  add object oCONSUP_1_13 as StdField with uid="MPOCORUZOD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CONSUP", cQueryName = "CONSUP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice mastro",;
    HelpContextID = 104977370,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=125, Top=217, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSUP"

  func oCONSUP_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SPTIPSOG $ 'IPNT')
    endwith
   endif
  endfunc

  func oCONSUP_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSUP_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSUP_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSUP_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"MASTRI",'GSAI_MCO.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSUP_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSUP
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_14 as StdField with uid="IOOIXEIKPM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile",;
    HelpContextID = 145850842,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=125, Top=252, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SPTIPSOG $ 'IPNT')
    endwith
   endif
  endfunc

  func oCATCON_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"CATEGORIE CONTABILI",'',this.parent.oContained
  endproc
  proc oCATCON_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc

  add object oSPDESCRI_1_15 as StdField with uid="MGGNLWGUXP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SPDESCRI", cQueryName = "SPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210734703,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=420, Top=144, InputMask=replicate('X',40)


  add object oSPTIPREG_1_19 as StdCombo with uid="ZPOLZYFKDH",rtseq=14,rtrep=.f.,left=240,top=328,width=161,height=21;
    , ToolTipText = "Tipo regola applicata";
    , HelpContextID = 191139437;
    , cFormVar="w_SPTIPREG",RowSource=""+"Nessun filtro,"+"Dati IVA,"+"Note di variazione,"+"Scheda carburante", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPTIPREG_1_19.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    iif(this.value =3,'B',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oSPTIPREG_1_19.GetRadio()
    this.Parent.oContained.w_SPTIPREG = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPREG_1_19.SetRadio()
    this.Parent.oContained.w_SPTIPREG=trim(this.Parent.oContained.w_SPTIPREG)
    this.value = ;
      iif(this.Parent.oContained.w_SPTIPREG=='N',1,;
      iif(this.Parent.oContained.w_SPTIPREG=='A',2,;
      iif(this.Parent.oContained.w_SPTIPREG=='B',3,;
      iif(this.Parent.oContained.w_SPTIPREG=='C',4,;
      0))))
  endfunc


  add object oSPESCLGE_1_20 as StdCombo with uid="LRIDQJCTEG",rtseq=15,rtrep=.f.,left=646,top=328,width=73,height=21;
    , ToolTipText = "Escludi documento da generazione";
    , HelpContextID = 77438571;
    , cFormVar="w_SPESCLGE",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPESCLGE_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSPESCLGE_1_20.GetRadio()
    this.Parent.oContained.w_SPESCLGE = this.RadioValue()
    return .t.
  endfunc

  func oSPESCLGE_1_20.SetRadio()
    this.Parent.oContained.w_SPESCLGE=trim(this.Parent.oContained.w_SPESCLGE)
    this.value = ;
      iif(this.Parent.oContained.w_SPESCLGE=='S',1,;
      iif(this.Parent.oContained.w_SPESCLGE=='N',2,;
      iif(this.Parent.oContained.w_SPESCLGE=='T',3,;
      0)))
  endfunc


  add object oSPTIPREC_1_21 as StdCombo with uid="TYXJZZXOXZ",rtseq=16,rtrep=.f.,left=240,top=359,width=73,height=21;
    , HelpContextID = 191139433;
    , cFormVar="w_SPTIPREC",RowSource=""+"Tutti,"+"FE,"+"FR,"+"DF,"+"NE,"+"NR,"+"FN,"+"SE,"+"TU", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPTIPREC_1_21.RadioValue()
    return(iif(this.value =1,'TT',;
    iif(this.value =2,'FE',;
    iif(this.value =3,'FR',;
    iif(this.value =4,'DF',;
    iif(this.value =5,'NE',;
    iif(this.value =6,'NR',;
    iif(this.value =7,'FN',;
    iif(this.value =8,'SE',;
    iif(this.value =9,'TU',;
    space(1)))))))))))
  endfunc
  func oSPTIPREC_1_21.GetRadio()
    this.Parent.oContained.w_SPTIPREC = this.RadioValue()
    return .t.
  endfunc

  func oSPTIPREC_1_21.SetRadio()
    this.Parent.oContained.w_SPTIPREC=trim(this.Parent.oContained.w_SPTIPREC)
    this.value = ;
      iif(this.Parent.oContained.w_SPTIPREC=='TT',1,;
      iif(this.Parent.oContained.w_SPTIPREC=='FE',2,;
      iif(this.Parent.oContained.w_SPTIPREC=='FR',3,;
      iif(this.Parent.oContained.w_SPTIPREC=='DF',4,;
      iif(this.Parent.oContained.w_SPTIPREC=='NE',5,;
      iif(this.Parent.oContained.w_SPTIPREC=='NR',6,;
      iif(this.Parent.oContained.w_SPTIPREC=='FN',7,;
      iif(this.Parent.oContained.w_SPTIPREC=='SE',8,;
      iif(this.Parent.oContained.w_SPTIPREC=='TU',9,;
      0)))))))))
  endfunc


  add object oSPIVANES_1_22 as StdCombo with uid="BGRZNDXVDS",rtseq=17,rtrep=.f.,left=646,top=359,width=73,height=21;
    , HelpContextID = 109108857;
    , cFormVar="w_SPIVANES",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPIVANES_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSPIVANES_1_22.GetRadio()
    this.Parent.oContained.w_SPIVANES = this.RadioValue()
    return .t.
  endfunc

  func oSPIVANES_1_22.SetRadio()
    this.Parent.oContained.w_SPIVANES=trim(this.Parent.oContained.w_SPIVANES)
    this.value = ;
      iif(this.Parent.oContained.w_SPIVANES=='S',1,;
      iif(this.Parent.oContained.w_SPIVANES=='N',2,;
      iif(this.Parent.oContained.w_SPIVANES=='T',3,;
      0)))
  endfunc


  add object oSPAUTFAT_1_23 as StdCombo with uid="UNCWLSGURB",rtseq=18,rtrep=.f.,left=240,top=390,width=73,height=21;
    , HelpContextID = 5284230;
    , cFormVar="w_SPAUTFAT",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPAUTFAT_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSPAUTFAT_1_23.GetRadio()
    this.Parent.oContained.w_SPAUTFAT = this.RadioValue()
    return .t.
  endfunc

  func oSPAUTFAT_1_23.SetRadio()
    this.Parent.oContained.w_SPAUTFAT=trim(this.Parent.oContained.w_SPAUTFAT)
    this.value = ;
      iif(this.Parent.oContained.w_SPAUTFAT=='S',1,;
      iif(this.Parent.oContained.w_SPAUTFAT=='N',2,;
      iif(this.Parent.oContained.w_SPAUTFAT=='T',3,;
      0)))
  endfunc


  add object oSPREVCHA_1_24 as StdCombo with uid="SMAXZUHAMS",rtseq=19,rtrep=.f.,left=646,top=390,width=73,height=21;
    , HelpContextID = 213937767;
    , cFormVar="w_SPREVCHA",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPREVCHA_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSPREVCHA_1_24.GetRadio()
    this.Parent.oContained.w_SPREVCHA = this.RadioValue()
    return .t.
  endfunc

  func oSPREVCHA_1_24.SetRadio()
    this.Parent.oContained.w_SPREVCHA=trim(this.Parent.oContained.w_SPREVCHA)
    this.value = ;
      iif(this.Parent.oContained.w_SPREVCHA=='S',1,;
      iif(this.Parent.oContained.w_SPREVCHA=='N',2,;
      iif(this.Parent.oContained.w_SPREVCHA=='T',3,;
      0)))
  endfunc


  add object oSPNOLEGG_1_25 as StdCombo with uid="QCMOKPECRW",rtseq=20,rtrep=.f.,left=240,top=421,width=121,height=21;
    , HelpContextID = 237645421;
    , cFormVar="w_SPNOLEGG",RowSource=""+"Tutti,"+"Autovettura,"+"Caravan,"+"Altri veicoli,"+"Unit� da diporto,"+"Aeromobili,"+"Nessuno", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPNOLEGG_1_25.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'A',;
    iif(this.value =3,'B',;
    iif(this.value =4,'C',;
    iif(this.value =5,'D',;
    iif(this.value =6,'E',;
    iif(this.value =7,'N',;
    space(1)))))))))
  endfunc
  func oSPNOLEGG_1_25.GetRadio()
    this.Parent.oContained.w_SPNOLEGG = this.RadioValue()
    return .t.
  endfunc

  func oSPNOLEGG_1_25.SetRadio()
    this.Parent.oContained.w_SPNOLEGG=trim(this.Parent.oContained.w_SPNOLEGG)
    this.value = ;
      iif(this.Parent.oContained.w_SPNOLEGG=='T',1,;
      iif(this.Parent.oContained.w_SPNOLEGG=='A',2,;
      iif(this.Parent.oContained.w_SPNOLEGG=='B',3,;
      iif(this.Parent.oContained.w_SPNOLEGG=='C',4,;
      iif(this.Parent.oContained.w_SPNOLEGG=='D',5,;
      iif(this.Parent.oContained.w_SPNOLEGG=='E',6,;
      iif(this.Parent.oContained.w_SPNOLEGG=='N',7,;
      0)))))))
  endfunc


  add object oSPINFUNO_1_26 as StdCombo with uid="AWDRXCZDOT",rtseq=21,rtrep=.f.,left=240,top=452,width=121,height=21;
    , ToolTipText = "Consente di filtrare i dettagli dove il valore assoluto di importo o di imposta o entrambi risulti inferiore a 1";
    , HelpContextID = 37167499;
    , cFormVar="w_SPINFUNO",RowSource=""+"Solo importo,"+"Solo imposta,"+"Entrambi,"+"Nessun filtro", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSPINFUNO_1_26.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'M',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oSPINFUNO_1_26.GetRadio()
    this.Parent.oContained.w_SPINFUNO = this.RadioValue()
    return .t.
  endfunc

  func oSPINFUNO_1_26.SetRadio()
    this.Parent.oContained.w_SPINFUNO=trim(this.Parent.oContained.w_SPINFUNO)
    this.value = ;
      iif(this.Parent.oContained.w_SPINFUNO=='I',1,;
      iif(this.Parent.oContained.w_SPINFUNO=='M',2,;
      iif(this.Parent.oContained.w_SPINFUNO=='E',3,;
      iif(this.Parent.oContained.w_SPINFUNO=='N',4,;
      0))))
  endfunc


  add object oBtn_1_27 as StdButton with uid="KWFWCRWNHO",left=629, top=491, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 159494678;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="ZXYGHBDURE",left=681, top=491, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 52399338;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"PgDn")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="KUWCMEGMLG",left=733, top=491, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 159494678;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSPDESCRIF_1_37 as StdField with uid="XMWLZOYUXF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SPDESCRIF", cQueryName = "SPDESCRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210735823,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=420, Top=183, InputMask=replicate('X',40)

  add object oDESMAS_1_38 as StdField with uid="MTWTUQTSEH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESMAS", cQueryName = "DESMAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 75992522,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=273, Top=217, InputMask=replicate('X',35)

  add object oDESCON_1_43 as StdField with uid="IXFYEJXSIO",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 145853898,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=183, Top=252, InputMask=replicate('X',35)


  add object oObj_1_56 as cp_outputCombo with uid="ZLERXIVPWT",left=240, top=481, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 246546406

  add object oStr_1_1 as StdString with uid="WMPOHYZZNW",Visible=.t., Left=61, Top=43,;
    Alignment=1, Width=60, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="PVLCYLAQYI",Visible=.t., Left=21, Top=147,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QPHBBHXHWS",Visible=.t., Left=2, Top=80,;
    Alignment=1, Width=119, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ZALCOBSEZR",Visible=.t., Left=99, Top=330,;
    Alignment=1, Width=137, Height=18,;
    Caption="Tipo regola applicata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="AMWXDKXOMM",Visible=.t., Left=181, Top=43,;
    Alignment=1, Width=159, Height=18,;
    Caption="Dati estratti da verificare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="JELVCYJCSJ",Visible=.t., Left=425, Top=43,;
    Alignment=1, Width=170, Height=18,;
    Caption="Escludi da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="YWXTBQXIDL",Visible=.t., Left=407, Top=330,;
    Alignment=1, Width=233, Height=18,;
    Caption="Escludi documento da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="LHPVJGRFMB",Visible=.t., Left=143, Top=361,;
    Alignment=1, Width=93, Height=18,;
    Caption="Quadro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="BNSTAFZAAA",Visible=.t., Left=244, Top=148,;
    Alignment=1, Width=27, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="QTJDMGJXFC",Visible=.t., Left=244, Top=187,;
    Alignment=1, Width=27, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="IYPFSOWZCO",Visible=.t., Left=57, Top=218,;
    Alignment=1, Width=64, Height=18,;
    Caption="Mastro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="DDBCOIUNGE",Visible=.t., Left=9, Top=253,;
    Alignment=1, Width=112, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="DNNRUQSVTF",Visible=.t., Left=5, Top=10,;
    Alignment=0, Width=128, Height=19,;
    Caption="Filtri di testata"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="PRTNDFMYKU",Visible=.t., Left=5, Top=288,;
    Alignment=0, Width=128, Height=19,;
    Caption="Filtri di dettaglio"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="LZLBLFXKYK",Visible=.t., Left=537, Top=361,;
    Alignment=1, Width=103, Height=18,;
    Caption="IVA non esposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="ITFFWXKMGU",Visible=.t., Left=133, Top=393,;
    Alignment=1, Width=103, Height=18,;
    Caption="Autofattura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="SNINPYYUKV",Visible=.t., Left=537, Top=391,;
    Alignment=1, Width=103, Height=18,;
    Caption="Reverse charge:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="ZMQTPIFQAJ",Visible=.t., Left=160, Top=424,;
    Alignment=1, Width=76, Height=18,;
    Caption="Noleggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="GMWTAIXLRL",Visible=.t., Left=64, Top=113,;
    Alignment=1, Width=58, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="IYWQINFNOK",Visible=.t., Left=336, Top=113,;
    Alignment=1, Width=81, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="WGMMVTFNPA",Visible=.t., Left=23, Top=455,;
    Alignment=1, Width=213, Height=18,;
    Caption=" Importo/imposta in valore assoluto <1:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="ZDLOVMWIXH",Visible=.t., Left=150, Top=485,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di Stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_17 as StdBox with uid="JSYJQHTSZX",left=-2, top=307, width=782,height=1

  add object oBox_1_46 as StdBox with uid="VRIVZBVKMU",left=-1, top=29, width=782,height=1
enddefine
define class tgsai_kmsPag2 as StdContainer
  Width  = 787
  height = 538
  stdWidth  = 787
  stdheight = 538
  resizeXpos=527
  resizeYpos=258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomDe as cp_zoombox with uid="EVGGBUEBEF",left=1, top=8, width=783,height=529,;
    caption='ZoomDe',;
   bGlobalFont=.t.,;
    cMenuFile="",cTable="DATESTSP",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSAI_KMS",bRetriveAllRows=.f.,cZoomOnZoom="",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 231366038
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_kms','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
