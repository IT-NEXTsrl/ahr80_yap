* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_adt                                                        *
*              Dati estratti antievasione IVA                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2011-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_adt"))

* --- Class definition
define class tgsai_adt as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 772
  Height = 449+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-07-04"
  HelpContextID=92931945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=77

  * --- Constant Properties
  ANTIVAAN_IDX = 0
  CONTI_IDX = 0
  NAZIONI_IDX = 0
  ANTIVAPA_IDX = 0
  cFile = "ANTIVAAN"
  cKeySelect = "DTSERIAL"
  cKeyWhere  = "DTSERIAL=this.w_DTSERIAL"
  cKeyWhereODBC = '"DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';

  cKeyWhereODBCqualified = '"ANTIVAAN.DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';

  cPrg = "gsai_adt"
  cComment = "Dati estratti antievasione IVA"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI1 = space(10)
  o_CODAZI1 = space(10)
  w_PAPERIOD = space(1)
  w_DTSERIAL = space(10)
  w_DT__ANNO = 0
  w_PERIOD = space(1)
  o_PERIOD = space(1)
  w_DT__MESE = 0
  w_DTTRIMES = 0
  w_DTTIPCON = space(1)
  w_DTCODCON = space(15)
  o_DTCODCON = space(15)
  w_ANPERFIS = space(1)
  w_ANDESCRI = space(40)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANCOGNOM = space(20)
  w_AN__NOME = space(20)
  w_ANINDIRI = space(35)
  w_ANCOFISC = space(20)
  w_ANCODFIS = space(16)
  w_ANPARIVA = space(12)
  w_ANLOCALI = space(30)
  w_ANNAZION = space(3)
  w_NADESNAZ = space(35)
  w_NACODEST = space(5)
  w_NACODISO = space(3)
  w_DTPERFIS = space(1)
  w_DTDESCRI = space(60)
  w_DT__NOME = space(20)
  w_DTDATNAS = ctod('  /  /  ')
  w_DTLOCNAS = space(100)
  w_DTPRONAS = space(2)
  w_DTCODEST = space(3)
  w_DTSTAFED = space(24)
  w_DTLOCRES = space(40)
  w_DTINDIRI = space(40)
  w_DTCODFIS = space(25)
  w_DTPARIVA = space(25)
  w_DTCODGEN = space(10)
  w_TIPREGV = space(1)
  w_TIPOPE1 = space(1)
  w_TIPOPE2 = space(1)
  w_DTBIMPTT = 0
  w_DTBIMPIM = 0
  w_DTSIMPTT = 0
  w_DTSIMPIM = 0
  w_DTBNOIMP = 0
  w_DTSNOIMP = 0
  w_DTOPESEN = 0
  w_DTBNOSOG = 0
  w_DTSNOSOG = 0
  w_DTSNVATT = 0
  w_DTSNVAIM = 0
  w_DTBNVPTT = 0
  w_DTBNVPIM = 0
  w_DTSNVPTT = 0
  w_DTSNVPIM = 0
  w_DTBNVATT = 0
  w_DTBNVAIM = 0
  w_OBTEST = ctod('  /  /  ')
  w_TIPREGA = space(1)
  w_DTBIMPTTA = 0
  w_DTBIMPIMA = 0
  w_DTSIMPTTA = 0
  w_DTSIMPIMA = 0
  w_DTBNOIMPA = 0
  w_DTSNOIMPA = 0
  w_DTOPESENA = 0
  w_DTBNOSOGA = 0
  w_DTSNOSOGA = 0
  w_DTSNVATTA = 0
  w_DTSNVAIMA = 0
  w_DTBNVPTTA = 0
  w_DTBNVPIMA = 0
  w_DTSNVPTTA = 0
  w_DTSNVPIMA = 0
  w_DTBNVATTA = 0
  w_DTBNVAIMA = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DTSERIAL = this.W_DTSERIAL

  * --- Children pointers
  GSAI_MDT = .NULL.
  GSAI_MDP = .NULL.
  GSAIAMDP = .NULL.
  GSAIAMDT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANTIVAAN','gsai_adt')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_adtPag1","gsai_adt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati anagrafici")
      .Pages(1).HelpContextID = 136108281
      .Pages(2).addobject("oPag","tgsai_adtPag2","gsai_adt",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Totale operazioni")
      .Pages(2).HelpContextID = 128401571
      .Pages(3).addobject("oPag","tgsai_adtPag3","gsai_adt",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Operazioni del periodo")
      .Pages(3).HelpContextID = 211077387
      .Pages(4).addobject("oPag","tgsai_adtPag4","gsai_adt",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Note di variazione periodi precedenti")
      .Pages(4).HelpContextID = 230907129
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='NAZIONI'
    this.cWorkTables[3]='ANTIVAPA'
    this.cWorkTables[4]='ANTIVAAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANTIVAAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANTIVAAN_IDX,3]
  return

  function CreateChildren()
    this.GSAI_MDT = CREATEOBJECT('stdDynamicChild',this,'GSAI_MDT',this.oPgFrm.Page3.oPag.oLinkPC_3_3)
    this.GSAI_MDP = CREATEOBJECT('stdDynamicChild',this,'GSAI_MDP',this.oPgFrm.Page4.oPag.oLinkPC_4_2)
    this.GSAIAMDP = CREATEOBJECT('stdDynamicChild',this,'GSAIAMDP',this.oPgFrm.Page4.oPag.oLinkPC_4_3)
    this.GSAIAMDT = CREATEOBJECT('stdDynamicChild',this,'GSAIAMDT',this.oPgFrm.Page3.oPag.oLinkPC_3_7)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAI_MDT)
      this.GSAI_MDT.DestroyChildrenChain()
      this.GSAI_MDT=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_3')
    if !ISNULL(this.GSAI_MDP)
      this.GSAI_MDP.DestroyChildrenChain()
      this.GSAI_MDP=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_2')
    if !ISNULL(this.GSAIAMDP)
      this.GSAIAMDP.DestroyChildrenChain()
      this.GSAIAMDP=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_3')
    if !ISNULL(this.GSAIAMDT)
      this.GSAIAMDT.DestroyChildrenChain()
      this.GSAIAMDT=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_7')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAI_MDT.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAI_MDP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAIAMDP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAIAMDT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAI_MDT.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAI_MDP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAIAMDP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAIAMDT.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAI_MDT.NewDocument()
    this.GSAI_MDP.NewDocument()
    this.GSAIAMDP.NewDocument()
    this.GSAIAMDT.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAI_MDT.SetKey(;
            .w_DTSERIAL,"DTSERIAL";
            ,.w_TIPOPE1,"DTTIPOPE";
            ,.w_TIPREGV,"DTTIPREG";
            )
      this.GSAI_MDP.SetKey(;
            .w_DTSERIAL,"DTSERIAL";
            ,.w_TIPOPE2,"DTTIPOPE";
            ,.w_TIPREGV,"DTTIPREG";
            )
      this.GSAIAMDP.SetKey(;
            .w_DTSERIAL,"DTSERIAL";
            ,.w_TIPOPE2,"DTTIPOPE";
            ,.w_TIPREGA,"DTTIPREG";
            )
      this.GSAIAMDT.SetKey(;
            .w_DTSERIAL,"DTSERIAL";
            ,.w_TIPOPE1,"DTTIPOPE";
            ,.w_TIPREGA,"DTTIPREG";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAI_MDT.ChangeRow(this.cRowID+'      1',1;
             ,.w_DTSERIAL,"DTSERIAL";
             ,.w_TIPOPE1,"DTTIPOPE";
             ,.w_TIPREGV,"DTTIPREG";
             )
      .GSAI_MDP.ChangeRow(this.cRowID+'      1',1;
             ,.w_DTSERIAL,"DTSERIAL";
             ,.w_TIPOPE2,"DTTIPOPE";
             ,.w_TIPREGV,"DTTIPREG";
             )
      .GSAIAMDP.ChangeRow(this.cRowID+'      1',1;
             ,.w_DTSERIAL,"DTSERIAL";
             ,.w_TIPOPE2,"DTTIPOPE";
             ,.w_TIPREGA,"DTTIPREG";
             )
      .GSAIAMDT.ChangeRow(this.cRowID+'      1',1;
             ,.w_DTSERIAL,"DTSERIAL";
             ,.w_TIPOPE1,"DTTIPOPE";
             ,.w_TIPREGA,"DTTIPREG";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAI_MDT)
        i_f=.GSAI_MDT.BuildFilter()
        if !(i_f==.GSAI_MDT.cQueryFilter)
          i_fnidx=.GSAI_MDT.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MDT.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MDT.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MDT.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MDT.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAI_MDP)
        i_f=.GSAI_MDP.BuildFilter()
        if !(i_f==.GSAI_MDP.cQueryFilter)
          i_fnidx=.GSAI_MDP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MDP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MDP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MDP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MDP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAIAMDP)
        i_f=.GSAIAMDP.BuildFilter()
        if !(i_f==.GSAIAMDP.cQueryFilter)
          i_fnidx=.GSAIAMDP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAIAMDP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAIAMDP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAIAMDP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAIAMDP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAIAMDT)
        i_f=.GSAIAMDT.BuildFilter()
        if !(i_f==.GSAIAMDT.cQueryFilter)
          i_fnidx=.GSAIAMDT.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAIAMDT.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAIAMDT.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAIAMDT.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAIAMDT.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DTSERIAL = NVL(DTSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANTIVAAN where DTSERIAL=KeySet.DTSERIAL
    *
    i_nConn = i_TableProp[this.ANTIVAAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAAN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANTIVAAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANTIVAAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANTIVAAN '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DTSERIAL',this.w_DTSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI1 = i_CODAZI
        .w_PAPERIOD = space(1)
        .w_ANPERFIS = space(1)
        .w_ANDESCRI = space(40)
        .w_ANDATNAS = ctod("  /  /  ")
        .w_ANLOCNAS = space(30)
        .w_ANPRONAS = space(2)
        .w_ANCOGNOM = space(20)
        .w_AN__NOME = space(20)
        .w_ANINDIRI = space(35)
        .w_ANCOFISC = space(20)
        .w_ANCODFIS = space(16)
        .w_ANPARIVA = space(12)
        .w_ANLOCALI = space(30)
        .w_ANNAZION = space(3)
        .w_NADESNAZ = space(35)
        .w_NACODEST = space(5)
        .w_NACODISO = space(3)
        .w_TIPREGV = 'V'
        .w_TIPOPE1 = 'A'
        .w_TIPOPE2 = 'P'
        .w_DTBIMPTT = 0
        .w_DTBIMPIM = 0
        .w_DTSIMPTT = 0
        .w_DTSIMPIM = 0
        .w_DTBNOIMP = 0
        .w_DTSNOIMP = 0
        .w_DTOPESEN = 0
        .w_DTBNOSOG = 0
        .w_DTSNOSOG = 0
        .w_DTSNVATT = 0
        .w_DTSNVAIM = 0
        .w_DTBNVPTT = 0
        .w_DTBNVPIM = 0
        .w_DTSNVPTT = 0
        .w_DTSNVPIM = 0
        .w_DTBNVATT = 0
        .w_DTBNVAIM = 0
        .w_OBTEST = CTOD('01-01-1900')
        .w_TIPREGA = 'A'
        .w_DTBIMPTTA = 0
        .w_DTBIMPIMA = 0
        .w_DTSIMPTTA = 0
        .w_DTSIMPIMA = 0
        .w_DTBNOIMPA = 0
        .w_DTSNOIMPA = 0
        .w_DTOPESENA = 0
        .w_DTBNOSOGA = 0
        .w_DTSNOSOGA = 0
        .w_DTSNVATTA = 0
        .w_DTSNVAIMA = 0
        .w_DTBNVPTTA = 0
        .w_DTBNVPIMA = 0
        .w_DTSNVPTTA = 0
        .w_DTSNVPIMA = 0
        .w_DTBNVATTA = 0
        .w_DTBNVAIMA = 0
          .link_1_1('Load')
        .w_DTSERIAL = NVL(DTSERIAL,space(10))
        .op_DTSERIAL = .w_DTSERIAL
        .w_DT__ANNO = NVL(DT__ANNO,0)
        .w_PERIOD = iif(EMPTY(NVL(.w_PAPERIOD, ' ')), 'M', .w_PAPERIOD)
        .w_DT__MESE = NVL(DT__MESE,0)
        .w_DTTRIMES = NVL(DTTRIMES,0)
        .w_DTTIPCON = NVL(DTTIPCON,space(1))
        .w_DTCODCON = NVL(DTCODCON,space(15))
          if link_1_11_joined
            this.w_DTCODCON = NVL(ANCODICE111,NVL(this.w_DTCODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI111,space(40))
            this.w_ANINDIRI = NVL(ANINDIRI111,space(35))
            this.w_ANDATNAS = NVL(cp_ToDate(ANDATNAS111),ctod("  /  /  "))
            this.w_ANLOCNAS = NVL(ANLOCNAS111,space(30))
            this.w_ANPRONAS = NVL(ANPRONAS111,space(2))
            this.w_ANCOGNOM = NVL(ANCOGNOM111,space(20))
            this.w_AN__NOME = NVL(AN__NOME111,space(20))
            this.w_ANPERFIS = NVL(ANPERFIS111,space(1))
            this.w_ANPARIVA = NVL(ANPARIVA111,space(12))
            this.w_ANCOFISC = NVL(ANCOFISC111,space(20))
            this.w_ANLOCALI = NVL(ANLOCALI111,space(30))
            this.w_ANNAZION = NVL(ANNAZION111,space(3))
            this.w_ANCODFIS = NVL(ANCODFIS111,space(16))
          else
          .link_1_11('Load')
          endif
          .link_1_24('Load')
        .w_DTPERFIS = NVL(DTPERFIS,space(1))
        .w_DTDESCRI = NVL(DTDESCRI,space(60))
        .w_DT__NOME = NVL(DT__NOME,space(20))
        .w_DTDATNAS = NVL(cp_ToDate(DTDATNAS),ctod("  /  /  "))
        .w_DTLOCNAS = NVL(DTLOCNAS,space(100))
        .w_DTPRONAS = NVL(DTPRONAS,space(2))
        .w_DTCODEST = NVL(DTCODEST,space(3))
        .w_DTSTAFED = NVL(DTSTAFED,space(24))
        .w_DTLOCRES = NVL(DTLOCRES,space(40))
        .w_DTINDIRI = NVL(DTINDIRI,space(40))
        .w_DTCODFIS = NVL(DTCODFIS,space(25))
        .w_DTPARIVA = NVL(DTPARIVA,space(25))
        .w_DTCODGEN = NVL(DTCODGEN,space(10))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ANTIVAAN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI1 = space(10)
      .w_PAPERIOD = space(1)
      .w_DTSERIAL = space(10)
      .w_DT__ANNO = 0
      .w_PERIOD = space(1)
      .w_DT__MESE = 0
      .w_DTTRIMES = 0
      .w_DTTIPCON = space(1)
      .w_DTCODCON = space(15)
      .w_ANPERFIS = space(1)
      .w_ANDESCRI = space(40)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANCOGNOM = space(20)
      .w_AN__NOME = space(20)
      .w_ANINDIRI = space(35)
      .w_ANCOFISC = space(20)
      .w_ANCODFIS = space(16)
      .w_ANPARIVA = space(12)
      .w_ANLOCALI = space(30)
      .w_ANNAZION = space(3)
      .w_NADESNAZ = space(35)
      .w_NACODEST = space(5)
      .w_NACODISO = space(3)
      .w_DTPERFIS = space(1)
      .w_DTDESCRI = space(60)
      .w_DT__NOME = space(20)
      .w_DTDATNAS = ctod("  /  /  ")
      .w_DTLOCNAS = space(100)
      .w_DTPRONAS = space(2)
      .w_DTCODEST = space(3)
      .w_DTSTAFED = space(24)
      .w_DTLOCRES = space(40)
      .w_DTINDIRI = space(40)
      .w_DTCODFIS = space(25)
      .w_DTPARIVA = space(25)
      .w_DTCODGEN = space(10)
      .w_TIPREGV = space(1)
      .w_TIPOPE1 = space(1)
      .w_TIPOPE2 = space(1)
      .w_DTBIMPTT = 0
      .w_DTBIMPIM = 0
      .w_DTSIMPTT = 0
      .w_DTSIMPIM = 0
      .w_DTBNOIMP = 0
      .w_DTSNOIMP = 0
      .w_DTOPESEN = 0
      .w_DTBNOSOG = 0
      .w_DTSNOSOG = 0
      .w_DTSNVATT = 0
      .w_DTSNVAIM = 0
      .w_DTBNVPTT = 0
      .w_DTBNVPIM = 0
      .w_DTSNVPTT = 0
      .w_DTSNVPIM = 0
      .w_DTBNVATT = 0
      .w_DTBNVAIM = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_TIPREGA = space(1)
      .w_DTBIMPTTA = 0
      .w_DTBIMPIMA = 0
      .w_DTSIMPTTA = 0
      .w_DTSIMPIMA = 0
      .w_DTBNOIMPA = 0
      .w_DTSNOIMPA = 0
      .w_DTOPESENA = 0
      .w_DTBNOSOGA = 0
      .w_DTSNOSOGA = 0
      .w_DTSNVATTA = 0
      .w_DTSNVAIMA = 0
      .w_DTBNVPTTA = 0
      .w_DTBNVPIMA = 0
      .w_DTSNVPTTA = 0
      .w_DTSNVPIMA = 0
      .w_DTBNVATTA = 0
      .w_DTBNVAIMA = 0
      if .cFunction<>"Filter"
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI1))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_DT__ANNO = year(i_DATSYS)
        .w_PERIOD = iif(EMPTY(NVL(.w_PAPERIOD, ' ')), 'M', .w_PAPERIOD)
        .w_DT__MESE = iif(NVL(.w_PERIOD,'M')='M', month(i_DATSYS), 0)
        .w_DTTRIMES = iif(NVL(.w_PERIOD,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        .w_DTTIPCON = 'C'
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_DTCODCON))
          .link_1_11('Full')
          endif
        .DoRTCalc(10,22,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_24('Full')
          endif
          .DoRTCalc(23,25,.f.)
        .w_DTPERFIS = iif(.w_ANPERFIS='S', 'S', 'N')
        .w_DTDESCRI = iif(NVL(.w_ANPERFIS,' ')='S', .w_ANCOGNOM, .w_ANDESCRI)
        .w_DT__NOME = iif(NVL(.w_DTPERFIS,' ')='S', .w_AN__NOME, space(100))
        .w_DTDATNAS = iif(NVL(.w_DTPERFIS,' ')='S', .w_ANDATNAS, CTOD("  -  -    "))
        .w_DTLOCNAS = iif(NVL(.w_DTPERFIS,' ')='S', .w_ANLOCNAS, space(100))
        .w_DTPRONAS = iif(NVL(.w_DTPERFIS,' ')='S', .w_ANPRONAS, space(2))
        .w_DTCODEST = .w_NACODEST
        .w_DTSTAFED = .w_NADESNAZ
        .w_DTLOCRES = .w_ANLOCALI
        .w_DTINDIRI = .w_ANINDIRI
        .w_DTCODFIS = iif(EMPTY(.w_ANCOFISC), .w_ANCODFIS, .w_ANCOFISC)
        .w_DTPARIVA = IIF(NOT EMPTY(NVL(.w_ANPARIVA,'')),ALLTRIM(.w_NACODISO),'')+ALLTRIM(.w_ANPARIVA)
        .w_DTCODGEN = space(10)
        .w_TIPREGV = 'V'
        .w_TIPOPE1 = 'A'
        .w_TIPOPE2 = 'P'
          .DoRTCalc(42,58,.f.)
        .w_OBTEST = CTOD('01-01-1900')
        .w_TIPREGA = 'A'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANTIVAAN')
    this.DoRTCalc(61,77,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVAAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAAN_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"ANTIV","i_codazi,w_DTSERIAL")
      .op_codazi = .w_codazi
      .op_DTSERIAL = .w_DTSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDT__ANNO_1_4.enabled = i_bVal
      .Page1.oPag.oPERIOD_1_6.enabled_(i_bVal)
      .Page1.oPag.oDT__MESE_1_7.enabled = i_bVal
      .Page1.oPag.oDTTRIMES_1_8.enabled = i_bVal
      .Page1.oPag.oDTTIPCON_1_10.enabled = i_bVal
      .Page1.oPag.oDTCODCON_1_11.enabled = i_bVal
      .Page1.oPag.oDTPERFIS_1_28.enabled = i_bVal
      .Page1.oPag.oDTDESCRI_1_29.enabled = i_bVal
      .Page1.oPag.oDT__NOME_1_30.enabled = i_bVal
      .Page1.oPag.oDTDATNAS_1_31.enabled = i_bVal
      .Page1.oPag.oDTLOCNAS_1_32.enabled = i_bVal
      .Page1.oPag.oDTPRONAS_1_33.enabled = i_bVal
      .Page1.oPag.oDTCODEST_1_34.enabled = i_bVal
      .Page1.oPag.oDTSTAFED_1_35.enabled = i_bVal
      .Page1.oPag.oDTLOCRES_1_36.enabled = i_bVal
      .Page1.oPag.oDTINDIRI_1_37.enabled = i_bVal
      .Page1.oPag.oDTCODFIS_1_38.enabled = i_bVal
      .Page1.oPag.oDTPARIVA_1_39.enabled = i_bVal
    endwith
    this.GSAI_MDT.SetStatus(i_cOp)
    this.GSAI_MDP.SetStatus(i_cOp)
    this.GSAIAMDP.SetStatus(i_cOp)
    this.GSAIAMDT.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ANTIVAAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAI_MDT.SetChildrenStatus(i_cOp)
  *  this.GSAI_MDP.SetChildrenStatus(i_cOp)
  *  this.GSAIAMDP.SetChildrenStatus(i_cOp)
  *  this.GSAIAMDT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANTIVAAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTSERIAL,"DTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DT__ANNO,"DT__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DT__MESE,"DT__MESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTTRIMES,"DTTRIMES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTTIPCON,"DTTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCODCON,"DTCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTPERFIS,"DTPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTDESCRI,"DTDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DT__NOME,"DT__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTDATNAS,"DTDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTLOCNAS,"DTLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTPRONAS,"DTPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCODEST,"DTCODEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTSTAFED,"DTSTAFED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTLOCRES,"DTLOCRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTINDIRI,"DTINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCODFIS,"DTCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTPARIVA,"DTPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCODGEN,"DTCODGEN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANTIVAAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAAN_IDX,2])
    i_lTable = "ANTIVAAN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANTIVAAN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAI_SGC with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANTIVAAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAAN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANTIVAAN_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"ANTIV","i_codazi,w_DTSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANTIVAAN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANTIVAAN')
        i_extval=cp_InsertValODBCExtFlds(this,'ANTIVAAN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DTSERIAL,DT__ANNO,DT__MESE,DTTRIMES,DTTIPCON"+;
                  ",DTCODCON,DTPERFIS,DTDESCRI,DT__NOME,DTDATNAS"+;
                  ",DTLOCNAS,DTPRONAS,DTCODEST,DTSTAFED,DTLOCRES"+;
                  ",DTINDIRI,DTCODFIS,DTPARIVA,DTCODGEN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DTSERIAL)+;
                  ","+cp_ToStrODBC(this.w_DT__ANNO)+;
                  ","+cp_ToStrODBC(this.w_DT__MESE)+;
                  ","+cp_ToStrODBC(this.w_DTTRIMES)+;
                  ","+cp_ToStrODBC(this.w_DTTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_DTCODCON)+;
                  ","+cp_ToStrODBC(this.w_DTPERFIS)+;
                  ","+cp_ToStrODBC(this.w_DTDESCRI)+;
                  ","+cp_ToStrODBC(this.w_DT__NOME)+;
                  ","+cp_ToStrODBC(this.w_DTDATNAS)+;
                  ","+cp_ToStrODBC(this.w_DTLOCNAS)+;
                  ","+cp_ToStrODBC(this.w_DTPRONAS)+;
                  ","+cp_ToStrODBC(this.w_DTCODEST)+;
                  ","+cp_ToStrODBC(this.w_DTSTAFED)+;
                  ","+cp_ToStrODBC(this.w_DTLOCRES)+;
                  ","+cp_ToStrODBC(this.w_DTINDIRI)+;
                  ","+cp_ToStrODBC(this.w_DTCODFIS)+;
                  ","+cp_ToStrODBC(this.w_DTPARIVA)+;
                  ","+cp_ToStrODBC(this.w_DTCODGEN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANTIVAAN')
        i_extval=cp_InsertValVFPExtFlds(this,'ANTIVAAN')
        cp_CheckDeletedKey(i_cTable,0,'DTSERIAL',this.w_DTSERIAL)
        INSERT INTO (i_cTable);
              (DTSERIAL,DT__ANNO,DT__MESE,DTTRIMES,DTTIPCON,DTCODCON,DTPERFIS,DTDESCRI,DT__NOME,DTDATNAS,DTLOCNAS,DTPRONAS,DTCODEST,DTSTAFED,DTLOCRES,DTINDIRI,DTCODFIS,DTPARIVA,DTCODGEN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DTSERIAL;
                  ,this.w_DT__ANNO;
                  ,this.w_DT__MESE;
                  ,this.w_DTTRIMES;
                  ,this.w_DTTIPCON;
                  ,this.w_DTCODCON;
                  ,this.w_DTPERFIS;
                  ,this.w_DTDESCRI;
                  ,this.w_DT__NOME;
                  ,this.w_DTDATNAS;
                  ,this.w_DTLOCNAS;
                  ,this.w_DTPRONAS;
                  ,this.w_DTCODEST;
                  ,this.w_DTSTAFED;
                  ,this.w_DTLOCRES;
                  ,this.w_DTINDIRI;
                  ,this.w_DTCODFIS;
                  ,this.w_DTPARIVA;
                  ,this.w_DTCODGEN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANTIVAAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAAN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANTIVAAN_IDX,i_nConn)
      *
      * update ANTIVAAN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANTIVAAN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DT__ANNO="+cp_ToStrODBC(this.w_DT__ANNO)+;
             ",DT__MESE="+cp_ToStrODBC(this.w_DT__MESE)+;
             ",DTTRIMES="+cp_ToStrODBC(this.w_DTTRIMES)+;
             ",DTTIPCON="+cp_ToStrODBC(this.w_DTTIPCON)+;
             ",DTCODCON="+cp_ToStrODBCNull(this.w_DTCODCON)+;
             ",DTPERFIS="+cp_ToStrODBC(this.w_DTPERFIS)+;
             ",DTDESCRI="+cp_ToStrODBC(this.w_DTDESCRI)+;
             ",DT__NOME="+cp_ToStrODBC(this.w_DT__NOME)+;
             ",DTDATNAS="+cp_ToStrODBC(this.w_DTDATNAS)+;
             ",DTLOCNAS="+cp_ToStrODBC(this.w_DTLOCNAS)+;
             ",DTPRONAS="+cp_ToStrODBC(this.w_DTPRONAS)+;
             ",DTCODEST="+cp_ToStrODBC(this.w_DTCODEST)+;
             ",DTSTAFED="+cp_ToStrODBC(this.w_DTSTAFED)+;
             ",DTLOCRES="+cp_ToStrODBC(this.w_DTLOCRES)+;
             ",DTINDIRI="+cp_ToStrODBC(this.w_DTINDIRI)+;
             ",DTCODFIS="+cp_ToStrODBC(this.w_DTCODFIS)+;
             ",DTPARIVA="+cp_ToStrODBC(this.w_DTPARIVA)+;
             ",DTCODGEN="+cp_ToStrODBC(this.w_DTCODGEN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANTIVAAN')
        i_cWhere = cp_PKFox(i_cTable  ,'DTSERIAL',this.w_DTSERIAL  )
        UPDATE (i_cTable) SET;
              DT__ANNO=this.w_DT__ANNO;
             ,DT__MESE=this.w_DT__MESE;
             ,DTTRIMES=this.w_DTTRIMES;
             ,DTTIPCON=this.w_DTTIPCON;
             ,DTCODCON=this.w_DTCODCON;
             ,DTPERFIS=this.w_DTPERFIS;
             ,DTDESCRI=this.w_DTDESCRI;
             ,DT__NOME=this.w_DT__NOME;
             ,DTDATNAS=this.w_DTDATNAS;
             ,DTLOCNAS=this.w_DTLOCNAS;
             ,DTPRONAS=this.w_DTPRONAS;
             ,DTCODEST=this.w_DTCODEST;
             ,DTSTAFED=this.w_DTSTAFED;
             ,DTLOCRES=this.w_DTLOCRES;
             ,DTINDIRI=this.w_DTINDIRI;
             ,DTCODFIS=this.w_DTCODFIS;
             ,DTPARIVA=this.w_DTPARIVA;
             ,DTCODGEN=this.w_DTCODGEN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAI_MDT : Saving
      this.GSAI_MDT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DTSERIAL,"DTSERIAL";
             ,this.w_TIPOPE1,"DTTIPOPE";
             ,this.w_TIPREGV,"DTTIPREG";
             )
      this.GSAI_MDT.mReplace()
      * --- GSAI_MDP : Saving
      this.GSAI_MDP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DTSERIAL,"DTSERIAL";
             ,this.w_TIPOPE2,"DTTIPOPE";
             ,this.w_TIPREGV,"DTTIPREG";
             )
      this.GSAI_MDP.mReplace()
      * --- GSAIAMDP : Saving
      this.GSAIAMDP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DTSERIAL,"DTSERIAL";
             ,this.w_TIPOPE2,"DTTIPOPE";
             ,this.w_TIPREGA,"DTTIPREG";
             )
      this.GSAIAMDP.mReplace()
      * --- GSAIAMDT : Saving
      this.GSAIAMDT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DTSERIAL,"DTSERIAL";
             ,this.w_TIPOPE1,"DTTIPOPE";
             ,this.w_TIPREGA,"DTTIPREG";
             )
      this.GSAIAMDT.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAI_MDT : Deleting
    this.GSAI_MDT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DTSERIAL,"DTSERIAL";
           ,this.w_TIPOPE1,"DTTIPOPE";
           ,this.w_TIPREGV,"DTTIPREG";
           )
    this.GSAI_MDT.mDelete()
    * --- GSAI_MDP : Deleting
    this.GSAI_MDP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DTSERIAL,"DTSERIAL";
           ,this.w_TIPOPE2,"DTTIPOPE";
           ,this.w_TIPREGV,"DTTIPREG";
           )
    this.GSAI_MDP.mDelete()
    * --- GSAIAMDP : Deleting
    this.GSAIAMDP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DTSERIAL,"DTSERIAL";
           ,this.w_TIPOPE2,"DTTIPOPE";
           ,this.w_TIPREGA,"DTTIPREG";
           )
    this.GSAIAMDP.mDelete()
    * --- GSAIAMDT : Deleting
    this.GSAIAMDT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DTSERIAL,"DTSERIAL";
           ,this.w_TIPOPE1,"DTTIPOPE";
           ,this.w_TIPREGA,"DTTIPREG";
           )
    this.GSAIAMDT.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANTIVAAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAAN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANTIVAAN_IDX,i_nConn)
      *
      * delete ANTIVAAN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DTSERIAL',this.w_DTSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANTIVAAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAAN_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
        if .o_CODAZI1<>.w_CODAZI1
            .w_PERIOD = iif(EMPTY(NVL(.w_PAPERIOD, ' ')), 'M', .w_PAPERIOD)
        endif
        if .o_PERIOD<>.w_PERIOD
            .w_DT__MESE = iif(NVL(.w_PERIOD,'M')='M', month(i_DATSYS), 0)
        endif
        if .o_PERIOD<>.w_PERIOD
            .w_DTTRIMES = iif(NVL(.w_PERIOD,'M')='T', CEILING(month(i_DATSYS)/3), 0)
        endif
        .DoRTCalc(8,21,.t.)
          .link_1_24('Full')
        .DoRTCalc(23,25,.t.)
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTPERFIS = iif(.w_ANPERFIS='S', 'S', 'N')
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTDESCRI = iif(NVL(.w_ANPERFIS,' ')='S', .w_ANCOGNOM, .w_ANDESCRI)
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DT__NOME = iif(NVL(.w_DTPERFIS,' ')='S', .w_AN__NOME, space(100))
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTDATNAS = iif(NVL(.w_DTPERFIS,' ')='S', .w_ANDATNAS, CTOD("  -  -    "))
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTLOCNAS = iif(NVL(.w_DTPERFIS,' ')='S', .w_ANLOCNAS, space(100))
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTPRONAS = iif(NVL(.w_DTPERFIS,' ')='S', .w_ANPRONAS, space(2))
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTCODEST = .w_NACODEST
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTSTAFED = .w_NADESNAZ
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTLOCRES = .w_ANLOCALI
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTINDIRI = .w_ANINDIRI
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTCODFIS = iif(EMPTY(.w_ANCOFISC), .w_ANCODFIS, .w_ANCOFISC)
        endif
        if .o_DTCODCON<>.w_DTCODCON
            .w_DTPARIVA = IIF(NOT EMPTY(NVL(.w_ANPARIVA,'')),ALLTRIM(.w_NACODISO),'')+ALLTRIM(.w_ANPARIVA)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"ANTIV","i_codazi,w_DTSERIAL")
          .op_DTSERIAL = .w_DTSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(38,77,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_UCADHAFXIZ()
    with this
          * --- Calcolo totali
          GSAI_BCT(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDT__NOME_1_30.enabled = this.oPgFrm.Page1.oPag.oDT__NOME_1_30.mCond()
    this.oPgFrm.Page1.oPag.oDTDATNAS_1_31.enabled = this.oPgFrm.Page1.oPag.oDTDATNAS_1_31.mCond()
    this.oPgFrm.Page1.oPag.oDTLOCNAS_1_32.enabled = this.oPgFrm.Page1.oPag.oDTLOCNAS_1_32.mCond()
    this.oPgFrm.Page1.oPag.oDTPRONAS_1_33.enabled = this.oPgFrm.Page1.oPag.oDTPRONAS_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPERIOD_1_6.visible=!this.oPgFrm.Page1.oPag.oPERIOD_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDT__MESE_1_7.visible=!this.oPgFrm.Page1.oPag.oDT__MESE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDTTRIMES_1_8.visible=!this.oPgFrm.Page1.oPag.oDTTRIMES_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsai_adt
    * instanzio il figlio della seconda pagina immediatamente
    IF Upper(CEVENT)='INIT'
      if Upper(this.GSAI_MDT.class)='STDDYNAMICCHILD'
        This.oPgFrm.Pages[3].opag.uienable(.T.)
        This.oPgFrm.ActivePage=1
      Endif
      if Upper(this.GSAI_MDP.class)='STDDYNAMICCHILD'
        This.oPgFrm.Pages[4].opag.uienable(.T.)
        This.oPgFrm.ActivePage=1
      Endif
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_UCADHAFXIZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANTIVAPA_IDX,3]
    i_lTable = "ANTIVAPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2], .t., this.ANTIVAPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAPERIOD";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_CODAZI1)
            select PACODAZI,PAPERIOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.PACODAZI,space(10))
      this.w_PAPERIOD = NVL(_Link_.PAPERIOD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(10)
      endif
      this.w_PAPERIOD = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANTIVAPA_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.ANTIVAPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DTCODCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DTCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DTCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DTTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPERFIS,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DTTIPCON;
                     ,'ANCODICE',trim(this.w_DTCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPERFIS,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DTCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DTCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDTCODCON_1_11'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DTTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPERFIS,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPERFIS,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPERFIS,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DTTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPERFIS,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DTCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPERFIS,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DTCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DTTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DTTIPCON;
                       ,'ANCODICE',this.w_DTCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,ANDATNAS,ANLOCNAS,ANPRONAS,ANCOGNOM,AN__NOME,ANPERFIS,ANPARIVA,ANCOFISC,ANLOCALI,ANNAZION,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DTCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANINDIRI = NVL(_Link_.ANINDIRI,space(35))
      this.w_ANDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_ANLOCNAS = NVL(_Link_.ANLOCNAS,space(30))
      this.w_ANPRONAS = NVL(_Link_.ANPRONAS,space(2))
      this.w_ANCOGNOM = NVL(_Link_.ANCOGNOM,space(20))
      this.w_AN__NOME = NVL(_Link_.AN__NOME,space(20))
      this.w_ANPERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_ANPARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_ANCOFISC = NVL(_Link_.ANCOFISC,space(20))
      this.w_ANLOCALI = NVL(_Link_.ANLOCALI,space(30))
      this.w_ANNAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_DTCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_ANINDIRI = space(35)
      this.w_ANDATNAS = ctod("  /  /  ")
      this.w_ANLOCNAS = space(30)
      this.w_ANPRONAS = space(2)
      this.w_ANCOGNOM = space(20)
      this.w_AN__NOME = space(20)
      this.w_ANPERFIS = space(1)
      this.w_ANPARIVA = space(12)
      this.w_ANCOFISC = space(20)
      this.w_ANLOCALI = space(30)
      this.w_ANNAZION = space(3)
      this.w_ANCODFIS = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DTCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.ANCODICE as ANCODICE111"+ ",link_1_11.ANDESCRI as ANDESCRI111"+ ",link_1_11.ANINDIRI as ANINDIRI111"+ ",link_1_11.ANDATNAS as ANDATNAS111"+ ",link_1_11.ANLOCNAS as ANLOCNAS111"+ ",link_1_11.ANPRONAS as ANPRONAS111"+ ",link_1_11.ANCOGNOM as ANCOGNOM111"+ ",link_1_11.AN__NOME as AN__NOME111"+ ",link_1_11.ANPERFIS as ANPERFIS111"+ ",link_1_11.ANPARIVA as ANPARIVA111"+ ",link_1_11.ANCOFISC as ANCOFISC111"+ ",link_1_11.ANLOCALI as ANLOCALI111"+ ",link_1_11.ANNAZION as ANNAZION111"+ ",link_1_11.ANCODFIS as ANCODFIS111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on ANTIVAAN.DTCODCON=link_1_11.ANCODICE"+" and ANTIVAAN.DTTIPCON=link_1_11.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and ANTIVAAN.DTCODCON=link_1_11.ANCODICE(+)"'+'+" and ANTIVAAN.DTTIPCON=link_1_11.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODEST,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NADESNAZ,NACODEST,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_NADESNAZ = NVL(_Link_.NADESNAZ,space(35))
      this.w_NACODEST = NVL(_Link_.NACODEST,space(5))
      this.w_NACODISO = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_NADESNAZ = space(35)
      this.w_NACODEST = space(5)
      this.w_NACODISO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDT__ANNO_1_4.value==this.w_DT__ANNO)
      this.oPgFrm.Page1.oPag.oDT__ANNO_1_4.value=this.w_DT__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIOD_1_6.RadioValue()==this.w_PERIOD)
      this.oPgFrm.Page1.oPag.oPERIOD_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDT__MESE_1_7.value==this.w_DT__MESE)
      this.oPgFrm.Page1.oPag.oDT__MESE_1_7.value=this.w_DT__MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDTTRIMES_1_8.value==this.w_DTTRIMES)
      this.oPgFrm.Page1.oPag.oDTTRIMES_1_8.value=this.w_DTTRIMES
    endif
    if not(this.oPgFrm.Page1.oPag.oDTTIPCON_1_10.RadioValue()==this.w_DTTIPCON)
      this.oPgFrm.Page1.oPag.oDTTIPCON_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDTCODCON_1_11.value==this.w_DTCODCON)
      this.oPgFrm.Page1.oPag.oDTCODCON_1_11.value=this.w_DTCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDTPERFIS_1_28.RadioValue()==this.w_DTPERFIS)
      this.oPgFrm.Page1.oPag.oDTPERFIS_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDTDESCRI_1_29.value==this.w_DTDESCRI)
      this.oPgFrm.Page1.oPag.oDTDESCRI_1_29.value=this.w_DTDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDT__NOME_1_30.value==this.w_DT__NOME)
      this.oPgFrm.Page1.oPag.oDT__NOME_1_30.value=this.w_DT__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oDTDATNAS_1_31.value==this.w_DTDATNAS)
      this.oPgFrm.Page1.oPag.oDTDATNAS_1_31.value=this.w_DTDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDTLOCNAS_1_32.value==this.w_DTLOCNAS)
      this.oPgFrm.Page1.oPag.oDTLOCNAS_1_32.value=this.w_DTLOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDTPRONAS_1_33.value==this.w_DTPRONAS)
      this.oPgFrm.Page1.oPag.oDTPRONAS_1_33.value=this.w_DTPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDTCODEST_1_34.value==this.w_DTCODEST)
      this.oPgFrm.Page1.oPag.oDTCODEST_1_34.value=this.w_DTCODEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDTSTAFED_1_35.value==this.w_DTSTAFED)
      this.oPgFrm.Page1.oPag.oDTSTAFED_1_35.value=this.w_DTSTAFED
    endif
    if not(this.oPgFrm.Page1.oPag.oDTLOCRES_1_36.value==this.w_DTLOCRES)
      this.oPgFrm.Page1.oPag.oDTLOCRES_1_36.value=this.w_DTLOCRES
    endif
    if not(this.oPgFrm.Page1.oPag.oDTINDIRI_1_37.value==this.w_DTINDIRI)
      this.oPgFrm.Page1.oPag.oDTINDIRI_1_37.value=this.w_DTINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDTCODFIS_1_38.value==this.w_DTCODFIS)
      this.oPgFrm.Page1.oPag.oDTCODFIS_1_38.value=this.w_DTCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDTPARIVA_1_39.value==this.w_DTPARIVA)
      this.oPgFrm.Page1.oPag.oDTPARIVA_1_39.value=this.w_DTPARIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBIMPTT_2_1.value==this.w_DTBIMPTT)
      this.oPgFrm.Page2.oPag.oDTBIMPTT_2_1.value=this.w_DTBIMPTT
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBIMPIM_2_3.value==this.w_DTBIMPIM)
      this.oPgFrm.Page2.oPag.oDTBIMPIM_2_3.value=this.w_DTBIMPIM
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSIMPTT_2_5.value==this.w_DTSIMPTT)
      this.oPgFrm.Page2.oPag.oDTSIMPTT_2_5.value=this.w_DTSIMPTT
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSIMPIM_2_7.value==this.w_DTSIMPIM)
      this.oPgFrm.Page2.oPag.oDTSIMPIM_2_7.value=this.w_DTSIMPIM
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNOIMP_2_9.value==this.w_DTBNOIMP)
      this.oPgFrm.Page2.oPag.oDTBNOIMP_2_9.value=this.w_DTBNOIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNOIMP_2_11.value==this.w_DTSNOIMP)
      this.oPgFrm.Page2.oPag.oDTSNOIMP_2_11.value=this.w_DTSNOIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oDTOPESEN_2_13.value==this.w_DTOPESEN)
      this.oPgFrm.Page2.oPag.oDTOPESEN_2_13.value=this.w_DTOPESEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNOSOG_2_15.value==this.w_DTBNOSOG)
      this.oPgFrm.Page2.oPag.oDTBNOSOG_2_15.value=this.w_DTBNOSOG
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNOSOG_2_17.value==this.w_DTSNOSOG)
      this.oPgFrm.Page2.oPag.oDTSNOSOG_2_17.value=this.w_DTSNOSOG
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNVATT_2_19.value==this.w_DTSNVATT)
      this.oPgFrm.Page2.oPag.oDTSNVATT_2_19.value=this.w_DTSNVATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNVAIM_2_21.value==this.w_DTSNVAIM)
      this.oPgFrm.Page2.oPag.oDTSNVAIM_2_21.value=this.w_DTSNVAIM
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNVPTT_2_23.value==this.w_DTBNVPTT)
      this.oPgFrm.Page2.oPag.oDTBNVPTT_2_23.value=this.w_DTBNVPTT
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNVPIM_2_25.value==this.w_DTBNVPIM)
      this.oPgFrm.Page2.oPag.oDTBNVPIM_2_25.value=this.w_DTBNVPIM
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNVPTT_2_27.value==this.w_DTSNVPTT)
      this.oPgFrm.Page2.oPag.oDTSNVPTT_2_27.value=this.w_DTSNVPTT
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNVPIM_2_29.value==this.w_DTSNVPIM)
      this.oPgFrm.Page2.oPag.oDTSNVPIM_2_29.value=this.w_DTSNVPIM
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNVATT_2_31.value==this.w_DTBNVATT)
      this.oPgFrm.Page2.oPag.oDTBNVATT_2_31.value=this.w_DTBNVATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNVAIM_2_33.value==this.w_DTBNVAIM)
      this.oPgFrm.Page2.oPag.oDTBNVAIM_2_33.value=this.w_DTBNVAIM
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBIMPTTA_2_42.value==this.w_DTBIMPTTA)
      this.oPgFrm.Page2.oPag.oDTBIMPTTA_2_42.value=this.w_DTBIMPTTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBIMPIMA_2_44.value==this.w_DTBIMPIMA)
      this.oPgFrm.Page2.oPag.oDTBIMPIMA_2_44.value=this.w_DTBIMPIMA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSIMPTTA_2_46.value==this.w_DTSIMPTTA)
      this.oPgFrm.Page2.oPag.oDTSIMPTTA_2_46.value=this.w_DTSIMPTTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSIMPIMA_2_48.value==this.w_DTSIMPIMA)
      this.oPgFrm.Page2.oPag.oDTSIMPIMA_2_48.value=this.w_DTSIMPIMA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNOIMPA_2_50.value==this.w_DTBNOIMPA)
      this.oPgFrm.Page2.oPag.oDTBNOIMPA_2_50.value=this.w_DTBNOIMPA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNOIMPA_2_52.value==this.w_DTSNOIMPA)
      this.oPgFrm.Page2.oPag.oDTSNOIMPA_2_52.value=this.w_DTSNOIMPA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTOPESENA_2_54.value==this.w_DTOPESENA)
      this.oPgFrm.Page2.oPag.oDTOPESENA_2_54.value=this.w_DTOPESENA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNOSOGA_2_56.value==this.w_DTBNOSOGA)
      this.oPgFrm.Page2.oPag.oDTBNOSOGA_2_56.value=this.w_DTBNOSOGA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNOSOGA_2_58.value==this.w_DTSNOSOGA)
      this.oPgFrm.Page2.oPag.oDTSNOSOGA_2_58.value=this.w_DTSNOSOGA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNVATTA_2_62.value==this.w_DTSNVATTA)
      this.oPgFrm.Page2.oPag.oDTSNVATTA_2_62.value=this.w_DTSNVATTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNVAIMA_2_64.value==this.w_DTSNVAIMA)
      this.oPgFrm.Page2.oPag.oDTSNVAIMA_2_64.value=this.w_DTSNVAIMA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNVPTTA_2_66.value==this.w_DTBNVPTTA)
      this.oPgFrm.Page2.oPag.oDTBNVPTTA_2_66.value=this.w_DTBNVPTTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNVPIMA_2_68.value==this.w_DTBNVPIMA)
      this.oPgFrm.Page2.oPag.oDTBNVPIMA_2_68.value=this.w_DTBNVPIMA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNVPTTA_2_70.value==this.w_DTSNVPTTA)
      this.oPgFrm.Page2.oPag.oDTSNVPTTA_2_70.value=this.w_DTSNVPTTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTSNVPIMA_2_72.value==this.w_DTSNVPIMA)
      this.oPgFrm.Page2.oPag.oDTSNVPIMA_2_72.value=this.w_DTSNVPIMA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNVATTA_2_74.value==this.w_DTBNVATTA)
      this.oPgFrm.Page2.oPag.oDTBNVATTA_2_74.value=this.w_DTBNVATTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDTBNVAIMA_2_76.value==this.w_DTBNVAIMA)
      this.oPgFrm.Page2.oPag.oDTBNVAIMA_2_76.value=this.w_DTBNVAIMA
    endif
    cp_SetControlsValueExtFlds(this,'ANTIVAAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DT__MESE>=1 AND .w_DT__MESE<=12)  and not(NOT(.w_DT__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDT__MESE_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTTRIMES>=1 AND .w_DTTRIMES<=4)  and not(NOT(.w_DTTRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDTTRIMES_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DTDESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDTDESCRI_1_29.SetFocus()
            i_bnoObbl = !empty(.w_DTDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DT__NOME))  and (.w_DTPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDT__NOME_1_30.SetFocus()
            i_bnoObbl = !empty(.w_DT__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAI_MDT.CheckForm()
      if i_bres
        i_bres=  .GSAI_MDT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSAI_MDP.CheckForm()
      if i_bres
        i_bres=  .GSAI_MDP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSAIAMDP.CheckForm()
      if i_bres
        i_bres=  .GSAIAMDP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSAIAMDT.CheckForm()
      if i_bres
        i_bres=  .GSAIAMDT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI1 = this.w_CODAZI1
    this.o_PERIOD = this.w_PERIOD
    this.o_DTCODCON = this.w_DTCODCON
    * --- GSAI_MDT : Depends On
    this.GSAI_MDT.SaveDependsOn()
    * --- GSAI_MDP : Depends On
    this.GSAI_MDP.SaveDependsOn()
    * --- GSAIAMDP : Depends On
    this.GSAIAMDP.SaveDependsOn()
    * --- GSAIAMDT : Depends On
    this.GSAIAMDT.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=EMPTY(this.w_DTCODGEN)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Movimento incluso in generazione per file telematico"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=EMPTY(this.w_DTCODGEN)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Movimento incluso in generazione per file telematico"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsai_adtPag1 as StdContainer
  Width  = 768
  height = 449
  stdWidth  = 768
  stdheight = 449
  resizeXpos=589
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDT__ANNO_1_4 as StdField with uid="HYNPMMDCOL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DT__ANNO", cQueryName = "DT__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 51691131,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=289, Top=17

  add object oPERIOD_1_6 as StdRadio with uid="LYVLNUYUSV",rtseq=5,rtrep=.f.,left=475, top=15, width=124,height=34;
    , ToolTipText = "Periodicit� adempimento";
    , cFormVar="w_PERIOD", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPERIOD_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Mensile"
      this.Buttons(1).HelpContextID = 206281994
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Trimestrale"
      this.Buttons(2).HelpContextID = 206281994
      this.Buttons(2).Top=16
      this.SetAll("Width",122)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Periodicit� adempimento")
      StdRadio::init()
    endproc

  func oPERIOD_1_6.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oPERIOD_1_6.GetRadio()
    this.Parent.oContained.w_PERIOD = this.RadioValue()
    return .t.
  endfunc

  func oPERIOD_1_6.SetRadio()
    this.Parent.oContained.w_PERIOD=trim(this.Parent.oContained.w_PERIOD)
    this.value = ;
      iif(this.Parent.oContained.w_PERIOD=='M',1,;
      iif(this.Parent.oContained.w_PERIOD=='T',2,;
      0))
  endfunc

  func oPERIOD_1_6.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oDT__MESE_1_7 as StdField with uid="NCANBKUKPL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DT__MESE", cQueryName = "DT__MESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese/trimestre di riferimento",;
    HelpContextID = 78332283,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=439, Top=17

  func oDT__MESE_1_7.mHide()
    with this.Parent.oContained
      return (NOT(.w_DT__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))
    endwith
  endfunc

  func oDT__MESE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DT__MESE>=1 AND .w_DT__MESE<=12)
    endwith
    return bRes
  endfunc

  add object oDTTRIMES_1_8 as StdField with uid="KIDSWEQFCY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DTTRIMES", cQueryName = "DTTRIMES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese/trimestre di riferimento",;
    HelpContextID = 207458697,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=439, Top=17

  func oDTTRIMES_1_8.mHide()
    with this.Parent.oContained
      return (NOT(.w_DTTRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))
    endwith
  endfunc

  func oDTTRIMES_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DTTRIMES>=1 AND .w_DTTRIMES<=4)
    endwith
    return bRes
  endfunc


  add object oDTTIPCON_1_10 as StdCombo with uid="FAVUWBBZYH",rtseq=8,rtrep=.f.,left=160,top=57,width=118,height=22;
    , HelpContextID = 221998716;
    , cFormVar="w_DTTIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDTTIPCON_1_10.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDTTIPCON_1_10.GetRadio()
    this.Parent.oContained.w_DTTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDTTIPCON_1_10.SetRadio()
    this.Parent.oContained.w_DTTIPCON=trim(this.Parent.oContained.w_DTTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DTTIPCON=='C',1,;
      iif(this.Parent.oContained.w_DTTIPCON=='F',2,;
      0))
  endfunc

  func oDTTIPCON_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DTCODCON)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDTCODCON_1_11 as StdField with uid="EAHXPIIFWI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DTCODCON", cQueryName = "DTCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto cliente",;
    HelpContextID = 234258044,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=289, Top=57, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DTTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DTCODCON"

  func oDTCODCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oDTCODCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDTCODCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DTTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DTTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDTCODCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oDTCODCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DTTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DTCODCON
     i_obj.ecpSave()
  endproc

  add object oDTPERFIS_1_28 as StdCheck with uid="ZVJNYFUHNI",rtseq=26,rtrep=.f.,left=475, top=57, caption="Persona Fisica",;
    ToolTipText = "Se Attivo: il soggetto � una persona fisica",;
    HelpContextID = 169848439,;
    cFormVar="w_DTPERFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDTPERFIS_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDTPERFIS_1_28.GetRadio()
    this.Parent.oContained.w_DTPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oDTPERFIS_1_28.SetRadio()
    this.Parent.oContained.w_DTPERFIS=trim(this.Parent.oContained.w_DTPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_DTPERFIS=='S',1,;
      0)
  endfunc

  add object oDTDESCRI_1_29 as StdField with uid="UNDUYNBBES",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DTDESCRI", cQueryName = "DTDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Cognome / ragione sociale",;
    HelpContextID = 49254783,;
   bGlobalFont=.t.,;
    Height=21, Width=572, Left=160, Top=97, InputMask=replicate('X',60)

  add object oDT__NOME_1_30 as StdField with uid="RUZLUWTFBL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DT__NOME", cQueryName = "DT__NOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 21282437,;
   bGlobalFont=.t.,;
    Height=21, Width=572, Left=160, Top=133, InputMask=replicate('X',20)

  func oDT__NOME_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTPERFIS='S')
    endwith
   endif
  endfunc

  add object oDTDATNAS_1_31 as StdField with uid="VTKSSEXLQD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DTDATNAS", cQueryName = "DTDATNAS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 234590601,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=169

  func oDTDATNAS_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTPERFIS='S')
    endwith
   endif
  endfunc

  add object oDTLOCNAS_1_32 as StdField with uid="OEGUAGVHVP",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DTLOCNAS", cQueryName = "DTLOCNAS",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 217715081,;
   bGlobalFont=.t.,;
    Height=21, Width=467, Left=245, Top=169, InputMask=replicate('X',100), bHasZoom = .t. 

  func oDTLOCNAS_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTPERFIS='S')
    endwith
   endif
  endfunc

  proc oDTLOCNAS_1_32.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_DTLOCNAS",".w_DTPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDTPRONAS_1_33 as StdField with uid="HCPUHURXUB",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DTPRONAS", cQueryName = "DTPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 230510985,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=716, Top=169, InputMask=replicate('X',2), bHasZoom = .t. 

  func oDTPRONAS_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DTPERFIS='S')
    endwith
   endif
  endfunc

  proc oDTPRONAS_1_33.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_DTPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDTCODEST_1_34 as StdField with uid="JZJZNJGHQI",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DTCODEST", cQueryName = "DTCODEST",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 67731850,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=160, Top=212, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  add object oDTSTAFED_1_35 as StdField with uid="BZAIWJCWBP",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DTSTAFED", cQueryName = "DTSTAFED",;
    bObbl = .f. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato federato, provincia, contea",;
    HelpContextID = 81756538,;
   bGlobalFont=.t.,;
    Height=21, Width=481, Left=245, Top=212, InputMask=replicate('X',24)

  add object oDTLOCRES_1_36 as StdField with uid="VGAZWLEGEC",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DTLOCRES", cQueryName = "DTLOCRES",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 16388489,;
   bGlobalFont=.t.,;
    Height=21, Width=572, Left=160, Top=246, InputMask=replicate('X',40)

  add object oDTINDIRI_1_37 as StdField with uid="FRHFTAFYQF",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DTINDIRI", cQueryName = "DTINDIRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero",;
    HelpContextID = 134799743,;
   bGlobalFont=.t.,;
    Height=21, Width=572, Left=160, Top=280, InputMask=replicate('X',40)

  add object oDTCODFIS_1_38 as StdField with uid="NEICLLJLQF",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DTCODFIS", cQueryName = "DTCODFIS",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 183926391,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=475, Top=322, InputMask=replicate('X',25)

  add object oDTPARIVA_1_39 as StdField with uid="AOJRIDAGNZ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DTPARIVA", cQueryName = "DTPARIVA",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 148656503,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=160, Top=322, InputMask=replicate('X',25)

  add object oStr_1_5 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=212, Top=17,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="JGEELRUKLD",Visible=.t., Left=360, Top=17,;
    Alignment=1, Width=74, Height=18,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (NOT(.w_DT__MESE<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='M'))
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=160, Top=155,;
    Alignment=0, Width=76, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=245, Top=155,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune (o stato estero) di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="NOWYYKZWAZ",Visible=.t., Left=160, Top=199,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_44 as StdString with uid="EENVNVZVGQ",Visible=.t., Left=245, Top=199,;
    Alignment=0, Width=158, Height=13,;
    Caption="Stato federato, provincia, contea"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="ZATVJAEMJI",Visible=.t., Left=160, Top=233,;
    Alignment=0, Width=158, Height=13,;
    Caption="Localit� di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="DMKPRROUYT",Visible=.t., Left=710, Top=155,;
    Alignment=0, Width=52, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="DJUVQMCMIP",Visible=.t., Left=160, Top=267,;
    Alignment=0, Width=158, Height=13,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="XCNUOVRHIB",Visible=.t., Left=6, Top=322,;
    Alignment=0, Width=111, Height=18,;
    Caption="Identificativi fiscali"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="LHYQUAFBCO",Visible=.t., Left=160, Top=309,;
    Alignment=0, Width=158, Height=13,;
    Caption="Codice IVA"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_51 as StdString with uid="BSYUVARGFL",Visible=.t., Left=475, Top=309,;
    Alignment=0, Width=158, Height=13,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_53 as StdString with uid="TPWEYQUKDS",Visible=.t., Left=6, Top=212,;
    Alignment=0, Width=111, Height=18,;
    Caption="Domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="LIFDJEITLU",Visible=.t., Left=160, Top=119,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_56 as StdString with uid="KIZOFHIWHV",Visible=.t., Left=160, Top=83,;
    Alignment=0, Width=130, Height=13,;
    Caption="Cognome/Ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=7, Top=98,;
    Alignment=0, Width=111, Height=18,;
    Caption="Dati anagrafici"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="FZWRBKVHFS",Visible=.t., Left=7, Top=19,;
    Alignment=0, Width=118, Height=18,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="SWZCULOHUB",Visible=.t., Left=360, Top=17,;
    Alignment=1, Width=74, Height=18,;
    Caption="Trimestre:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (NOT(.w_DTTRIMES<>0 AND .cFunction<>'Load' OR .cFunction='Load' AND .w_PERIOD='T'))
    endwith
  endfunc

  add object oBox_1_48 as StdBox with uid="RDMWYOEEFE",left=1, top=54, width=763,height=296

  add object oBox_1_52 as StdBox with uid="CMPEHMCRJP",left=1, top=304, width=763,height=2

  add object oBox_1_54 as StdBox with uid="ZGZUPYKXSJ",left=1, top=197, width=763,height=2

  add object oBox_1_58 as StdBox with uid="PIUTDLGCGZ",left=149, top=54, width=1,height=296
enddefine
define class tgsai_adtPag2 as StdContainer
  Width  = 768
  height = 449
  stdWidth  = 768
  stdheight = 449
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDTBIMPTT_2_1 as StdField with uid="NYPJPEAUMW",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DTBIMPTT", cQueryName = "DTBIMPTT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni imponibili beni",;
    HelpContextID = 261321098,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=31, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBIMPIM_2_3 as StdField with uid="KQBADHPBIU",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DTBIMPIM", cQueryName = "DTBIMPIM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta operazioni imponibili beni",;
    HelpContextID = 7114365,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=54, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSIMPTT_2_5 as StdField with uid="HSCRBEAPRQ",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DTSIMPTT", cQueryName = "DTSIMPTT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni imponibili servizi",;
    HelpContextID = 261390730,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=77, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSIMPIM_2_7 as StdField with uid="ZUDQZHONAB",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DTSIMPIM", cQueryName = "DTSIMPIM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta operazioni imponibili servizi",;
    HelpContextID = 7044733,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=100, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNOIMP_2_9 as StdField with uid="RRNLWVRHPO",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DTBNOIMP", cQueryName = "DTBNOIMP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni non imponibili beni",;
    HelpContextID = 122130042,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=123, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNOIMP_2_11 as StdField with uid="UKKUZRWMHK",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DTSNOIMP", cQueryName = "DTSNOIMP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni non imponibili servizi",;
    HelpContextID = 122060410,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=146, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTOPESEN_2_13 as StdField with uid="FZMGBFRLVW",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DTOPESEN", cQueryName = "DTOPESEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni esenti",;
    HelpContextID = 35340676,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=169, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNOSOG_2_15 as StdField with uid="NKXNSVZGQN",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DTBNOSOG", cQueryName = "DTBNOSOG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni non soggette beni",;
    HelpContextID = 222793347,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=192, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNOSOG_2_17 as StdField with uid="JRQWNOGFTX",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DTSNOSOG", cQueryName = "DTSNOSOG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni non soggette servizi",;
    HelpContextID = 222723715,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=215, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNVATT_2_19 as StdField with uid="LUIIUVGSOB",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DTSNVATT", cQueryName = "DTSNVATT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Note di variazione stesso anno - servizi",;
    HelpContextID = 19497354,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=612, Top=77, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNVAIM_2_21 as StdField with uid="WRUMBPBBFX",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DTSNVAIM", cQueryName = "DTSNVAIM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta relativa alle note di variazione stesso anno - servizi",;
    HelpContextID = 248938109,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=612, Top=100, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNVPTT_2_23 as StdField with uid="AJAYMBSDBR",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DTBNVPTT", cQueryName = "DTBNVPTT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Note di variazione anni precedenti - beni",;
    HelpContextID = 2650506,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=612, Top=123, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNVPIM_2_25 as StdField with uid="WYWDYOSSDW",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DTBNVPIM", cQueryName = "DTBNVPIM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta relativa alle note di variazione anni precedenti - beni",;
    HelpContextID = 265784957,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=612, Top=146, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNVPTT_2_27 as StdField with uid="ZGROWYBASB",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DTSNVPTT", cQueryName = "DTSNVPTT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Note di variazione anni precedenti - servizi",;
    HelpContextID = 2720138,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=612, Top=169, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNVPIM_2_29 as StdField with uid="PTJWQSDGFH",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DTSNVPIM", cQueryName = "DTSNVPIM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta relativa alle note di variazione anni precedenti - servizi",;
    HelpContextID = 265715325,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=612, Top=192, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNVATT_2_31 as StdField with uid="VGHRQPKBOJ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DTBNVATT", cQueryName = "DTBNVATT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Note di variazione stesso anno - beni",;
    HelpContextID = 19427722,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=613, Top=31, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNVAIM_2_33 as StdField with uid="ZRTGPKIMJI",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DTBNVAIM", cQueryName = "DTBNVAIM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta relativa alle note di variazione stesso anno - beni",;
    HelpContextID = 249007741,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=613, Top=54, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBIMPTTA_2_42 as StdField with uid="NREKQALSKM",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DTBIMPTTA", cQueryName = "DTBIMPTTA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni imponibili beni",;
    HelpContextID = 261322138,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=239, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBIMPIMA_2_44 as StdField with uid="WADMJZQSOH",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DTBIMPIMA", cQueryName = "DTBIMPIMA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta operazioni imponibili beni",;
    HelpContextID = 7113325,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=262, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSIMPTTA_2_46 as StdField with uid="YEYILAWARV",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DTSIMPTTA", cQueryName = "DTSIMPTTA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni imponibili servizi",;
    HelpContextID = 261391770,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=285, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSIMPIMA_2_48 as StdField with uid="SGHUDWVQKS",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DTSIMPIMA", cQueryName = "DTSIMPIMA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta operazioni imponibili servizi",;
    HelpContextID = 7043693,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=308, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNOIMPA_2_50 as StdField with uid="ENVWUGRJNT",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DTBNOIMPA", cQueryName = "DTBNOIMPA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni non imponibili beni",;
    HelpContextID = 122129002,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=331, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNOIMPA_2_52 as StdField with uid="LUXXHCQMMZ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DTSNOIMPA", cQueryName = "DTSNOIMPA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni non imponibili servizi",;
    HelpContextID = 122059370,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=354, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTOPESENA_2_54 as StdField with uid="DJXMZNOABW",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DTOPESENA", cQueryName = "DTOPESENA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni esenti",;
    HelpContextID = 35341716,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=377, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNOSOGA_2_56 as StdField with uid="BQYNIKVPQH",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DTBNOSOGA", cQueryName = "DTBNOSOGA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni non soggette beni",;
    HelpContextID = 222792307,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=400, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNOSOGA_2_58 as StdField with uid="TJVFUDNSJT",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DTSNOSOGA", cQueryName = "DTSNOSOGA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni non soggette servizi",;
    HelpContextID = 222722675,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=229, Top=423, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNVATTA_2_62 as StdField with uid="WPXUYPQUPZ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DTSNVATTA", cQueryName = "DTSNVATTA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Note di variazione stesso anno - servizi",;
    HelpContextID = 19498394,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=611, Top=285, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNVAIMA_2_64 as StdField with uid="TLITWWVRSM",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DTSNVAIMA", cQueryName = "DTSNVAIMA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta relativa alle note di variazione stesso anno - servizi",;
    HelpContextID = 248937069,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=611, Top=308, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNVPTTA_2_66 as StdField with uid="NPPDMDOUBO",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DTBNVPTTA", cQueryName = "DTBNVPTTA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Note di variazione anni precedenti - beni",;
    HelpContextID = 2651546,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=611, Top=331, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNVPIMA_2_68 as StdField with uid="PUGAIOUZIX",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DTBNVPIMA", cQueryName = "DTBNVPIMA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta relativa alle note di variazione anni precedenti - beni",;
    HelpContextID = 265783917,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=611, Top=354, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNVPTTA_2_70 as StdField with uid="UTHJJEPTPG",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DTSNVPTTA", cQueryName = "DTSNVPTTA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Note di variazione anni precedenti - servizi",;
    HelpContextID = 2721178,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=611, Top=377, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTSNVPIMA_2_72 as StdField with uid="MXXDJTSNFB",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DTSNVPIMA", cQueryName = "DTSNVPIMA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta relativa alle note di variazione anni precedenti - servizi",;
    HelpContextID = 265714285,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=611, Top=400, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNVATTA_2_74 as StdField with uid="MJADGPDMYS",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DTBNVATTA", cQueryName = "DTBNVATTA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Note di variazione stesso anno - beni",;
    HelpContextID = 19428762,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=612, Top=239, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDTBNVAIMA_2_76 as StdField with uid="ZMLSEQEJFI",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DTBNVAIMA", cQueryName = "DTBNVAIMA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta relativa alle note di variazione stesso anno - beni",;
    HelpContextID = 249006701,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=612, Top=262, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oStr_2_2 as StdString with uid="KJIVJFWJHS",Visible=.t., Left=72, Top=35,;
    Alignment=1, Width=156, Height=18,;
    Caption="Imponibili beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="TJFBNLTAXP",Visible=.t., Left=20, Top=58,;
    Alignment=1, Width=208, Height=18,;
    Caption="Imposta imponibili beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="FRJNZASYDA",Visible=.t., Left=20, Top=81,;
    Alignment=1, Width=208, Height=18,;
    Caption="Imponibili servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="ROUVTXLZGV",Visible=.t., Left=20, Top=104,;
    Alignment=1, Width=208, Height=18,;
    Caption="Imposta imponibili servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="SVFHTFWMYB",Visible=.t., Left=20, Top=127,;
    Alignment=1, Width=208, Height=18,;
    Caption="Non imponibili beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="ZRZMQRSAYE",Visible=.t., Left=20, Top=150,;
    Alignment=1, Width=208, Height=18,;
    Caption="Non imponibili servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="BVXUQDSPVJ",Visible=.t., Left=20, Top=173,;
    Alignment=1, Width=208, Height=18,;
    Caption="Esenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="SBEIASYKUK",Visible=.t., Left=20, Top=196,;
    Alignment=1, Width=208, Height=18,;
    Caption="Non soggette beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="YJCYBNRRGP",Visible=.t., Left=20, Top=219,;
    Alignment=1, Width=208, Height=18,;
    Caption="Non soggette servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="UGVJBFGRSG",Visible=.t., Left=390, Top=81,;
    Alignment=1, Width=221, Height=18,;
    Caption="Stesso anno - servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="TQZEZAWWRP",Visible=.t., Left=390, Top=104,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imposta stesso anno - servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="FMCSIPUMII",Visible=.t., Left=390, Top=127,;
    Alignment=1, Width=221, Height=18,;
    Caption="Anni precedenti - beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="PMFQCLCVQJ",Visible=.t., Left=390, Top=150,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imposta anni precedenti - beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="SWIXNUEGTU",Visible=.t., Left=390, Top=173,;
    Alignment=1, Width=221, Height=18,;
    Caption="Anni precedenti - servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="UEHGPTTFFB",Visible=.t., Left=390, Top=196,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imposta anni precedenti - servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="TZKUHAICJF",Visible=.t., Left=390, Top=35,;
    Alignment=1, Width=221, Height=18,;
    Caption="Stesso anno - beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="RBAQDMYGHZ",Visible=.t., Left=390, Top=58,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imposta stesso anno - beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="OUTDQSDSGJ",Visible=.t., Left=390, Top=10,;
    Alignment=2, Width=359, Height=18,;
    Caption="Totali note di variazione"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="CFTQVAIOEL",Visible=.t., Left=15, Top=10,;
    Alignment=2, Width=359, Height=18,;
    Caption="Totali operazioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="RMKGMCPSHX",Visible=.t., Left=11, Top=32,;
    Alignment=0, Width=61, Height=18,;
    Caption="Cessioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="WZGWQNLTMO",Visible=.t., Left=78, Top=243,;
    Alignment=1, Width=150, Height=18,;
    Caption="Imponibili beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="HWIBMUANVD",Visible=.t., Left=20, Top=266,;
    Alignment=1, Width=208, Height=18,;
    Caption="Imposta imponibili beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="NOCFJZOTLO",Visible=.t., Left=20, Top=289,;
    Alignment=1, Width=208, Height=18,;
    Caption="Imponibili servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="CULNMFLEYG",Visible=.t., Left=20, Top=312,;
    Alignment=1, Width=208, Height=18,;
    Caption="Imposta imponibili servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="XKTFKHSOCW",Visible=.t., Left=20, Top=335,;
    Alignment=1, Width=208, Height=18,;
    Caption="Non imponibili beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_53 as StdString with uid="BHWWNJXUCD",Visible=.t., Left=20, Top=358,;
    Alignment=1, Width=208, Height=18,;
    Caption="Non imponibili servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="GXLSHFQNZT",Visible=.t., Left=20, Top=381,;
    Alignment=1, Width=208, Height=18,;
    Caption="Esenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="WPOSRULQTB",Visible=.t., Left=20, Top=404,;
    Alignment=1, Width=208, Height=18,;
    Caption="Non soggette beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="PUIGBYABFK",Visible=.t., Left=20, Top=427,;
    Alignment=1, Width=208, Height=18,;
    Caption="Non soggette servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="HISPWWRVZU",Visible=.t., Left=11, Top=239,;
    Alignment=0, Width=61, Height=18,;
    Caption="Acquisti"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="KPKHFFHSYK",Visible=.t., Left=389, Top=289,;
    Alignment=1, Width=221, Height=18,;
    Caption="Stesso anno - servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="XLSKHKNMMD",Visible=.t., Left=389, Top=312,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imposta stesso anno - servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_67 as StdString with uid="HMBIDECKGN",Visible=.t., Left=389, Top=335,;
    Alignment=1, Width=221, Height=18,;
    Caption="Anni precedenti - beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_69 as StdString with uid="JQKIFHDLKW",Visible=.t., Left=389, Top=358,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imposta anni precedenti - beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_71 as StdString with uid="KLUBZHPSPB",Visible=.t., Left=389, Top=381,;
    Alignment=1, Width=221, Height=18,;
    Caption="Anni precedenti - servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_73 as StdString with uid="SWHMVCBMYY",Visible=.t., Left=389, Top=404,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imposta anni precedenti - servizi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_75 as StdString with uid="JEFOHSDUGD",Visible=.t., Left=389, Top=243,;
    Alignment=1, Width=221, Height=18,;
    Caption="Stesso anno - beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_77 as StdString with uid="OZLCHQRXTX",Visible=.t., Left=389, Top=266,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imposta stesso anno - beni:"  ;
  , bGlobalFont=.t.

  add object oBox_2_35 as StdBox with uid="HHETSLKHRL",left=8, top=6, width=750,height=443

  add object oBox_2_37 as StdBox with uid="CUPZZJRHFH",left=380, top=5, width=1,height=443

  add object oBox_2_38 as StdBox with uid="NMXKEYXPCK",left=8, top=30, width=750,height=2

  add object oBox_2_61 as StdBox with uid="RYUADPVLLI",left=8, top=237, width=750,height=2
enddefine
define class tgsai_adtPag3 as StdContainer
  Width  = 768
  height = 449
  stdWidth  = 768
  stdheight = 449
  resizeXpos=470
  resizeYpos=128
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_3 as stdDynamicChildContainer with uid="BYMTYKEXBP",left=77, top=10, width=681, height=215, bOnScreen=.t.;



  add object oLinkPC_3_7 as stdDynamicChildContainer with uid="EBAKWQBHHB",left=77, top=232, width=683, height=215, bOnScreen=.t.;


  add object oStr_3_4 as StdString with uid="APOTZFRNHQ",Visible=.t., Left=17, Top=16,;
    Alignment=0, Width=50, Height=18,;
    Caption="Cessioni"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="CQWJWJVJJG",Visible=.t., Left=17, Top=235,;
    Alignment=0, Width=50, Height=18,;
    Caption="Acquisti"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsai_mdt",lower(this.oContained.GSAI_MDT.class))=0
        this.oContained.GSAI_MDT.createrealchild()
      endif
      if type('this.oContained')='O' and at("gsaiamdt",lower(this.oContained.GSAIAMDT.class))=0
        this.oContained.GSAIAMDT.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsai_adtPag4 as StdContainer
  Width  = 768
  height = 449
  stdWidth  = 768
  stdheight = 449
  resizeXpos=570
  resizeYpos=133
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_2 as stdDynamicChildContainer with uid="JLJBBUNFHS",left=87, top=9, width=674, height=215, bOnScreen=.t.;



  add object oLinkPC_4_3 as stdDynamicChildContainer with uid="HSBSNXGHPF",left=87, top=231, width=673, height=215, bOnScreen=.t.;


  add object oStr_4_4 as StdString with uid="HMTWJYRCLR",Visible=.t., Left=18, Top=16,;
    Alignment=0, Width=50, Height=18,;
    Caption="Cessioni"  ;
  , bGlobalFont=.t.

  add object oStr_4_5 as StdString with uid="XNRDCTJNJL",Visible=.t., Left=18, Top=235,;
    Alignment=0, Width=50, Height=18,;
    Caption="Acquisti"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsai_mdp",lower(this.oContained.GSAI_MDP.class))=0
        this.oContained.GSAI_MDP.createrealchild()
      endif
      if type('this.oContained')='O' and at("gsaiamdp",lower(this.oContained.GSAIAMDP.class))=0
        this.oContained.GSAIAMDP.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_adt','ANTIVAAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DTSERIAL=ANTIVAAN.DTSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
