* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsaiskms                                                        *
*              Manutenzione dati estratti San Marino                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-14                                                      *
* Last revis.: 2014-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsaiskms",oParentObject))

* --- Class definition
define class tgsaiskms as StdForm
  Top    = 1
  Left   = 4

  * --- Standard Properties
  Width  = 791
  Height = 461+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-01-31"
  HelpContextID=198575977
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  DATPARBL_IDX = 0
  cPrg = "gsaiskms"
  cComment = "Manutenzione dati estratti San Marino"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DM__ANNO = 0
  w_DM__MESE = 0
  w_DMDACONT = space(1)
  w_DMSCLGEN = space(1)
  w_DMPERFIS = space(1)
  w_DMPARIVA = space(25)
  w_DMTIPCON = space(1)
  o_DMTIPCON = space(1)
  w_TIPCON = space(1)
  w_DMCODCON = space(15)
  w_DMCODCONF = space(15)
  w_CONSUP = space(15)
  w_CATCON = space(5)
  w_DMDESCRI = space(40)
  w_DMESCLGE = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DMDESCRIF = space(40)
  w_DESMAS = space(35)
  w_TIPMAS = space(1)
  w_NULIV = 0
  w_DESCON = space(35)
  w_DMSERIAL = space(10)
  w_CPROWNUM = 0
  w_ORDINA = space(1)
  w_OBJECT = .F.
  w_TRASH = .F.
  w_GEST = .F.
  w_ZoomDe = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsaiskmsPag1","gsaiskms",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsaiskmsPag2","gsaiskms",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Visualizzazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDM__ANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDe = this.oPgFrm.Pages(2).oPag.ZoomDe
    DoDefault()
    proc Destroy()
      this.w_ZoomDe = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='CACOCLFO'
    this.cWorkTables[4]='DATPARBL'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DM__ANNO=0
      .w_DM__MESE=0
      .w_DMDACONT=space(1)
      .w_DMSCLGEN=space(1)
      .w_DMPERFIS=space(1)
      .w_DMPARIVA=space(25)
      .w_DMTIPCON=space(1)
      .w_TIPCON=space(1)
      .w_DMCODCON=space(15)
      .w_DMCODCONF=space(15)
      .w_CONSUP=space(15)
      .w_CATCON=space(5)
      .w_DMDESCRI=space(40)
      .w_DMESCLGE=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DMDESCRIF=space(40)
      .w_DESMAS=space(35)
      .w_TIPMAS=space(1)
      .w_NULIV=0
      .w_DESCON=space(35)
      .w_DMSERIAL=space(10)
      .w_CPROWNUM=0
      .w_ORDINA=space(1)
      .w_OBJECT=.f.
      .w_TRASH=.f.
      .w_GEST=.f.
        .w_DM__ANNO = year(i_DATSYS)
        .w_DM__MESE = Month(i_DATSYS)
        .w_DMDACONT = 'T'
        .w_DMSCLGEN = 'T'
        .w_DMPERFIS = 'X'
          .DoRTCalc(6,6,.f.)
        .w_DMTIPCON = 'N'
        .w_TIPCON = .w_DMTIPCON
        .w_DMCODCON = Space(15)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DMCODCON))
          .link_1_11('Full')
        endif
        .w_DMCODCONF = Space(15)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_DMCODCONF))
          .link_1_12('Full')
        endif
        .w_CONSUP = SPACE(15)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CONSUP))
          .link_1_13('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CATCON))
          .link_1_14('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_DMESCLGE = 'T'
        .w_OBTEST = CTOD('01-01-1900')
          .DoRTCalc(16,20,.f.)
        .w_DMSERIAL = NVL(.w_ZOOMDE.getVar('DMSERIAL'), ' ')
        .w_CPROWNUM = NVL(.w_ZOOMDE.getVar('CPROWNUM'), ' ')
        .w_ORDINA = NVL(.w_ZOOMDE.getVar('ORDINA'), ' ')
      .oPgFrm.Page2.oPag.ZoomDe.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
    endwith
    this.DoRTCalc(24,26,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
            .w_TIPCON = .w_DMTIPCON
        if .o_DMTIPCON<>.w_DMTIPCON
            .w_DMCODCON = Space(15)
          .link_1_11('Full')
        endif
        if .o_DMTIPCON<>.w_DMTIPCON
            .w_DMCODCONF = Space(15)
          .link_1_12('Full')
        endif
        if .o_DMTIPCON<>.w_DMTIPCON
            .w_CONSUP = SPACE(15)
          .link_1_13('Full')
        endif
        .DoRTCalc(12,20,.t.)
            .w_DMSERIAL = NVL(.w_ZOOMDE.getVar('DMSERIAL'), ' ')
            .w_CPROWNUM = NVL(.w_ZOOMDE.getVar('CPROWNUM'), ' ')
            .w_ORDINA = NVL(.w_ZOOMDE.getVar('ORDINA'), ' ')
        .oPgFrm.Page2.oPag.ZoomDe.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(24,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomDe.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
    endwith
  return

  proc Calculate_OEBUCQVPFH()
    with this
          * --- Apertura gestioni collegate Zoomde
          .w_OBJECT = Gsai_Adm()
          .w_TRASH = .w_Object.EcpFilter()
          .w_Object.w_Dmserial = .w_Dmserial
          .w_TRASH = .w_Object.EcpSave()
          .w_Object.opGFRM.ActivePage = iif(.w_Ordina='A',1,2)
          .w_GEST = iif(.w_Ordina='B',.w_Object.Gsai_Msa,.f.)
          .w_TRASH = .w_Gest.Search('Cprownum='+Alltrim(Str(.w_Cprownum,6,0)))
          .w_TRASH = IIF(.w_Trash<0,.f.,.w_Gest.SetRow(.w_Trash))
          .w_TRASH = IIF(.w_Trash,.w_Gest.Refresh(),.f.)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDMCODCON_1_11.enabled = this.oPgFrm.Page1.oPag.oDMCODCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oDMCODCONF_1_12.enabled = this.oPgFrm.Page1.oPag.oDMCODCONF_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_zoomde selected")
          .Calculate_OEBUCQVPFH()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.ZoomDe.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DMCODCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DMCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DMTIPCON;
                     ,'ANCODICE',trim(this.w_DMCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DMCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_DMTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DMCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDMCODCON_1_11'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'Gsai_Adm.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DMTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DMCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DMCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DMTIPCON;
                       ,'ANCODICE',this.w_DMCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DMCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DMDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DMCODCON = space(15)
      endif
      this.w_DMDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DMCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DMCODCONF
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DMCODCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DMCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DMTIPCON;
                     ,'ANCODICE',trim(this.w_DMCODCONF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DMCODCONF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DMCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DMCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_DMTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DMCODCONF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDMCODCONF_1_12'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'Gsai_Adm.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DMTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DMCODCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DMCODCONF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DMTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DMTIPCON;
                       ,'ANCODICE',this.w_DMCODCONF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DMCODCONF = NVL(_Link_.ANCODICE,space(15))
      this.w_DMDESCRIF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DMCODCONF = space(15)
      endif
      this.w_DMDESCRIF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DMCODCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSUP
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSUP))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CONSUP)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSUP_1_13'),i_cWhere,'GSAR_AMC',"MASTRI",'GSAI_MCO.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSUP)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(35))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONSUP = space(15)
      endif
      this.w_DESMAS = space(35)
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND ((.w_TIPCON='N' AND (.w_TIPMAS='C' or .w_TIPMAS='F')) OR (.w_TIPCON=.w_TIPMAS))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CONSUP = space(15)
        this.w_DESMAS = space(35)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_14'),i_cWhere,'GSAR_AC2',"CATEGORIE CONTABILI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDM__ANNO_1_2.value==this.w_DM__ANNO)
      this.oPgFrm.Page1.oPag.oDM__ANNO_1_2.value=this.w_DM__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDM__MESE_1_3.value==this.w_DM__MESE)
      this.oPgFrm.Page1.oPag.oDM__MESE_1_3.value=this.w_DM__MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDMDACONT_1_4.RadioValue()==this.w_DMDACONT)
      this.oPgFrm.Page1.oPag.oDMDACONT_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDMSCLGEN_1_5.RadioValue()==this.w_DMSCLGEN)
      this.oPgFrm.Page1.oPag.oDMSCLGEN_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDMPERFIS_1_6.RadioValue()==this.w_DMPERFIS)
      this.oPgFrm.Page1.oPag.oDMPERFIS_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDMPARIVA_1_7.value==this.w_DMPARIVA)
      this.oPgFrm.Page1.oPag.oDMPARIVA_1_7.value=this.w_DMPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDMTIPCON_1_9.RadioValue()==this.w_DMTIPCON)
      this.oPgFrm.Page1.oPag.oDMTIPCON_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDMCODCON_1_11.value==this.w_DMCODCON)
      this.oPgFrm.Page1.oPag.oDMCODCON_1_11.value=this.w_DMCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDMCODCONF_1_12.value==this.w_DMCODCONF)
      this.oPgFrm.Page1.oPag.oDMCODCONF_1_12.value=this.w_DMCODCONF
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSUP_1_13.value==this.w_CONSUP)
      this.oPgFrm.Page1.oPag.oCONSUP_1_13.value=this.w_CONSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_14.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_14.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDMDESCRI_1_15.value==this.w_DMDESCRI)
      this.oPgFrm.Page1.oPag.oDMDESCRI_1_15.value=this.w_DMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDMESCLGE_1_18.RadioValue()==this.w_DMESCLGE)
      this.oPgFrm.Page1.oPag.oDMESCLGE_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDMDESCRIF_1_28.value==this.w_DMDESCRIF)
      this.oPgFrm.Page1.oPag.oDMDESCRIF_1_28.value=this.w_DMDESCRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAS_1_29.value==this.w_DESMAS)
      this.oPgFrm.Page1.oPag.oDESMAS_1_29.value=this.w_DESMAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_34.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_34.value=this.w_DESCON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DM__ANNO)) or not(.w_DM__ANNO>2005 and .w_DM__ANNO<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDM__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DM__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DM__MESE)) or not(.w_DM__MESE>=1 AND .w_DM__MESE<=12))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDM__MESE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DM__MESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NULIV=1 AND ((.w_TIPCON='N' AND (.w_TIPMAS='C' or .w_TIPMAS='F')) OR (.w_TIPCON=.w_TIPMAS)))  and not(empty(.w_CONSUP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONSUP_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DMTIPCON = this.w_DMTIPCON
    return

enddefine

* --- Define pages as container
define class tgsaiskmsPag1 as StdContainer
  Width  = 787
  height = 461
  stdWidth  = 787
  stdheight = 461
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDM__ANNO_1_2 as StdField with uid="UQWWLVVPBH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DM__ANNO", cQueryName = "DM__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 157336955,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=41, cSayPict='"9999"', cGetPict='"9999"'

  func oDM__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DM__ANNO>2005 and .w_DM__ANNO<2100)
    endwith
    return bRes
  endfunc

  add object oDM__MESE_1_3 as StdField with uid="JVLRLQLKWY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DM__MESE", cQueryName = "DM__MESE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese di riferimento",;
    HelpContextID = 241121915,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=192, Top=41

  func oDM__MESE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DM__MESE>=1 AND .w_DM__MESE<=12)
    endwith
    return bRes
  endfunc


  add object oDMDACONT_1_4 as StdCombo with uid="HHHJPCDAST",rtseq=3,rtrep=.f.,left=650,top=36,width=83,height=22;
    , ToolTipText = "Dati estratti da verificare";
    , HelpContextID = 140539254;
    , cFormVar="w_DMDACONT",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDMDACONT_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDMDACONT_1_4.GetRadio()
    this.Parent.oContained.w_DMDACONT = this.RadioValue()
    return .t.
  endfunc

  func oDMDACONT_1_4.SetRadio()
    this.Parent.oContained.w_DMDACONT=trim(this.Parent.oContained.w_DMDACONT)
    this.value = ;
      iif(this.Parent.oContained.w_DMDACONT=='S',1,;
      iif(this.Parent.oContained.w_DMDACONT=='N',2,;
      iif(this.Parent.oContained.w_DMDACONT=='T',3,;
      0)))
  endfunc


  add object oDMSCLGEN_1_5 as StdCombo with uid="BQLKCHSUNV",rtseq=4,rtrep=.f.,left=650,top=72,width=83,height=22;
    , ToolTipText = "Escludi da generazione";
    , HelpContextID = 3308164;
    , cFormVar="w_DMSCLGEN",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDMSCLGEN_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDMSCLGEN_1_5.GetRadio()
    this.Parent.oContained.w_DMSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oDMSCLGEN_1_5.SetRadio()
    this.Parent.oContained.w_DMSCLGEN=trim(this.Parent.oContained.w_DMSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_DMSCLGEN=='S',1,;
      iif(this.Parent.oContained.w_DMSCLGEN=='N',2,;
      iif(this.Parent.oContained.w_DMSCLGEN=='T',3,;
      0)))
  endfunc


  add object oDMPERFIS_1_6 as StdCombo with uid="VMJQAYOIOV",rtseq=5,rtrep=.f.,left=125,top=119,width=152,height=22;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 261376649;
    , cFormVar="w_DMPERFIS",RowSource=""+"Persona giuridica,"+"Persona fisica,"+"Altro,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDMPERFIS_1_6.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'S',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oDMPERFIS_1_6.GetRadio()
    this.Parent.oContained.w_DMPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oDMPERFIS_1_6.SetRadio()
    this.Parent.oContained.w_DMPERFIS=trim(this.Parent.oContained.w_DMPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_DMPERFIS=='N',1,;
      iif(this.Parent.oContained.w_DMPERFIS=='P',2,;
      iif(this.Parent.oContained.w_DMPERFIS=='S',3,;
      iif(this.Parent.oContained.w_DMPERFIS=='X',4,;
      0))))
  endfunc

  add object oDMPARIVA_1_7 as StdField with uid="MUIDWSGMOT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DMPARIVA", cQueryName = "DMPARIVA",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 43010679,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=532, Top=120, InputMask=replicate('X',25)


  add object oDMTIPCON_1_9 as StdCombo with uid="JNYAXOMVNG",rtseq=7,rtrep=.f.,left=125,top=160,width=109,height=22;
    , HelpContextID = 59209084;
    , cFormVar="w_DMTIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Nessun filtro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDMTIPCON_1_9.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oDMTIPCON_1_9.GetRadio()
    this.Parent.oContained.w_DMTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDMTIPCON_1_9.SetRadio()
    this.Parent.oContained.w_DMTIPCON=trim(this.Parent.oContained.w_DMTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DMTIPCON=='C',1,;
      iif(this.Parent.oContained.w_DMTIPCON=='F',2,;
      iif(this.Parent.oContained.w_DMTIPCON=='N',3,;
      0)))
  endfunc

  func oDMTIPCON_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DMCODCON)
        bRes2=.link_1_11('Full')
      endif
      if .not. empty(.w_DMCODCONF)
        bRes2=.link_1_12('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDMCODCON_1_11 as StdField with uid="UCJURWGOQP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DMCODCON", cQueryName = "DMCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto iniziale",;
    HelpContextID = 71468412,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=273, Top=161, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DMTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DMCODCON"

  func oDMCODCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DMTIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oDMCODCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oDMCODCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDMCODCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DMTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DMTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDMCODCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'Gsai_Adm.CONTI_VZM',this.parent.oContained
  endproc
  proc oDMCODCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DMTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DMCODCON
     i_obj.ecpSave()
  endproc

  add object oDMCODCONF_1_12 as StdField with uid="INMNTQUMMI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DMCODCONF", cQueryName = "DMCODCONF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto finale",;
    HelpContextID = 71467292,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=273, Top=200, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DMTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DMCODCONF"

  func oDMCODCONF_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DMTIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oDMCODCONF_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oDMCODCONF_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDMCODCONF_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DMTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DMTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDMCODCONF_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'Gsai_Adm.CONTI_VZM',this.parent.oContained
  endproc
  proc oDMCODCONF_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DMTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DMCODCONF
     i_obj.ecpSave()
  endproc

  add object oCONSUP_1_13 as StdField with uid="MPOCORUZOD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CONSUP", cQueryName = "CONSUP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice mastro",;
    HelpContextID = 103666650,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=125, Top=241, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSUP"

  func oCONSUP_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSUP_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSUP_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSUP_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"MASTRI",'GSAI_MCO.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSUP_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSUP
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_14 as StdField with uid="IOOIXEIKPM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile",;
    HelpContextID = 144540122,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=125, Top=279, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"CATEGORIE CONTABILI",'',this.parent.oContained
  endproc
  proc oCATCON_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc

  add object oDMDESCRI_1_15 as StdField with uid="MGGNLWGUXP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DMDESCRI", cQueryName = "DMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 212044415,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=420, Top=161, InputMask=replicate('X',40)


  add object oDMESCLGE_1_18 as StdCombo with uid="LRIDQJCTEG",rtseq=14,rtrep=.f.,left=242,top=359,width=73,height=22;
    , ToolTipText = "Escludi documento da generazione";
    , HelpContextID = 78748283;
    , cFormVar="w_DMESCLGE",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDMESCLGE_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDMESCLGE_1_18.GetRadio()
    this.Parent.oContained.w_DMESCLGE = this.RadioValue()
    return .t.
  endfunc

  func oDMESCLGE_1_18.SetRadio()
    this.Parent.oContained.w_DMESCLGE=trim(this.Parent.oContained.w_DMESCLGE)
    this.value = ;
      iif(this.Parent.oContained.w_DMESCLGE=='S',1,;
      iif(this.Parent.oContained.w_DMESCLGE=='N',2,;
      iif(this.Parent.oContained.w_DMESCLGE=='T',3,;
      0)))
  endfunc


  add object oBtn_1_19 as StdButton with uid="KWFWCRWNHO",left=629, top=407, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 160805398;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="ZXYGHBDURE",left=681, top=407, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 51088618;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"PgDn")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="KUWCMEGMLG",left=733, top=407, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 160805398;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDMDESCRIF_1_28 as StdField with uid="XMWLZOYUXF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DMDESCRIF", cQueryName = "DMDESCRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 212045535,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=420, Top=200, InputMask=replicate('X',40)

  add object oDESMAS_1_29 as StdField with uid="MTWTUQTSEH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESMAS", cQueryName = "DESMAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 74681802,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=273, Top=241, InputMask=replicate('X',35)

  add object oDESCON_1_34 as StdField with uid="IXFYEJXSIO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 144543178,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=183, Top=279, InputMask=replicate('X',35)


  add object oObj_1_41 as cp_outputCombo with uid="ZLERXIVPWT",left=239, top=415, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 247857126

  add object oStr_1_1 as StdString with uid="WMPOHYZZNW",Visible=.t., Left=61, Top=43,;
    Alignment=1, Width=60, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="PVLCYLAQYI",Visible=.t., Left=21, Top=164,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QPHBBHXHWS",Visible=.t., Left=2, Top=123,;
    Alignment=1, Width=119, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="AMWXDKXOMM",Visible=.t., Left=489, Top=39,;
    Alignment=1, Width=159, Height=18,;
    Caption="Dati estratti da verificare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JELVCYJCSJ",Visible=.t., Left=478, Top=76,;
    Alignment=1, Width=170, Height=18,;
    Caption="Escludi da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="YWXTBQXIDL",Visible=.t., Left=7, Top=362,;
    Alignment=1, Width=233, Height=18,;
    Caption="Escludi documento da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="BNSTAFZAAA",Visible=.t., Left=244, Top=165,;
    Alignment=1, Width=27, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="QTJDMGJXFC",Visible=.t., Left=244, Top=204,;
    Alignment=1, Width=27, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="IYPFSOWZCO",Visible=.t., Left=57, Top=242,;
    Alignment=1, Width=64, Height=18,;
    Caption="Mastro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="DDBCOIUNGE",Visible=.t., Left=9, Top=280,;
    Alignment=1, Width=112, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="DNNRUQSVTF",Visible=.t., Left=5, Top=10,;
    Alignment=0, Width=128, Height=19,;
    Caption="Filtri di testata"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="PRTNDFMYKU",Visible=.t., Left=5, Top=325,;
    Alignment=0, Width=128, Height=19,;
    Caption="Filtri di dettaglio"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="VDNVYCBIMJ",Visible=.t., Left=135, Top=417,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="XDYEYKDMBK",Visible=.t., Left=176, Top=41,;
    Alignment=2, Width=13, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="GMWTAIXLRL",Visible=.t., Left=303, Top=123,;
    Alignment=1, Width=221, Height=18,;
    Caption="Identificativo fiscale/Identificativo IVA:"  ;
  , bGlobalFont=.t.

  add object oBox_1_17 as StdBox with uid="JSYJQHTSZX",left=-2, top=348, width=782,height=1

  add object oBox_1_37 as StdBox with uid="VRIVZBVKMU",left=-1, top=29, width=782,height=1
enddefine
define class tgsaiskmsPag2 as StdContainer
  Width  = 787
  height = 461
  stdWidth  = 787
  stdheight = 461
  resizeXpos=527
  resizeYpos=258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomDe as cp_zoombox with uid="EVGGBUEBEF",left=3, top=7, width=783,height=453,;
    caption='ZoomDe',;
   bGlobalFont=.t.,;
    cMenuFile="",cTable="DATESTSM",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSAISKMS",bQueryOnDblClick=.f.,bRetriveAllRows=.f.,cZoomOnZoom="",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 232676758
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsaiskms','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
