* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_kme                                                        *
*              Manutenzione dati estratti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-14                                                      *
* Last revis.: 2011-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_kme",oParentObject))

* --- Class definition
define class tgsai_kme as StdForm
  Top    = 1
  Left   = 4

  * --- Standard Properties
  Width  = 791
  Height = 538+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-12-21"
  HelpContextID=199886697
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  cPrg = "gsai_kme"
  cComment = "Manutenzione dati estratti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DE__ANNO = 0
  w_DEDACONT = space(1)
  w_DESCLGEN = space(1)
  w_DETIPSOG = space(1)
  w_DETIPCON = space(1)
  o_DETIPCON = space(1)
  w_TIPCON = space(1)
  w_DECODCON = space(15)
  w_DECODCONF = space(15)
  w_CONSUP = space(15)
  w_CATCON = space(5)
  w_ANOPETRE = space(1)
  w_ANTIPPRE = space(1)
  w_OSTIPOPE = space(1)
  o_OSTIPOPE = space(1)
  w_OSRIFCON = space(30)
  w_OSTIPFAT = space(1)
  w_DEDESCRI = space(40)
  w_DETIPREG = space(1)
  w_DEMODPAG = space(1)
  o_DEMODPAG = space(1)
  w_DEESCLUD = space(1)
  w_TIPREC = space(1)
  o_TIPREC = space(1)
  w_IMPO_NEG = space(1)
  w_VARIMP = space(1)
  o_VARIMP = space(1)
  w_VARIMS = space(1)
  o_VARIMS = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DESERIAL = space(10)
  w_ORDINA = space(1)
  w_AREA = space(1)
  w_CPROWNUM = 0
  w_OBJECT = .F.
  w_TRASH = .F.
  w_GEST = .F.
  w_DEDESCRIF = space(40)
  w_DESMAS = space(35)
  w_TIPMAS = space(1)
  w_NULIV = 0
  w_DESCON = space(35)
  w_SELECT = space(1)
  o_SELECT = space(1)
  w_AGGIORNAMENTO = .F.
  w_NEWMODPAG = space(1)
  w_NEWVARIMP = space(1)
  w_NEWVARIMS = space(1)
  w_ZoomAg = .NULL.
  w_ZoomDe = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_kmePag1","gsai_kme",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsai_kmePag2","gsai_kme",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Visualizzazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDE__ANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomAg = this.oPgFrm.Pages(2).oPag.ZoomAg
    this.w_ZoomDe = this.oPgFrm.Pages(2).oPag.ZoomDe
    DoDefault()
    proc Destroy()
      this.w_ZoomAg = .NULL.
      this.w_ZoomDe = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='CACOCLFO'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DE__ANNO=0
      .w_DEDACONT=space(1)
      .w_DESCLGEN=space(1)
      .w_DETIPSOG=space(1)
      .w_DETIPCON=space(1)
      .w_TIPCON=space(1)
      .w_DECODCON=space(15)
      .w_DECODCONF=space(15)
      .w_CONSUP=space(15)
      .w_CATCON=space(5)
      .w_ANOPETRE=space(1)
      .w_ANTIPPRE=space(1)
      .w_OSTIPOPE=space(1)
      .w_OSRIFCON=space(30)
      .w_OSTIPFAT=space(1)
      .w_DEDESCRI=space(40)
      .w_DETIPREG=space(1)
      .w_DEMODPAG=space(1)
      .w_DEESCLUD=space(1)
      .w_TIPREC=space(1)
      .w_IMPO_NEG=space(1)
      .w_VARIMP=space(1)
      .w_VARIMS=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESERIAL=space(10)
      .w_ORDINA=space(1)
      .w_AREA=space(1)
      .w_CPROWNUM=0
      .w_OBJECT=.f.
      .w_TRASH=.f.
      .w_GEST=.f.
      .w_DEDESCRIF=space(40)
      .w_DESMAS=space(35)
      .w_TIPMAS=space(1)
      .w_NULIV=0
      .w_DESCON=space(35)
      .w_SELECT=space(1)
      .w_AGGIORNAMENTO=.f.
      .w_NEWMODPAG=space(1)
      .w_NEWVARIMP=space(1)
      .w_NEWVARIMS=space(1)
        .w_DE__ANNO = year(i_DATSYS)
        .w_DEDACONT = 'T'
        .w_DESCLGEN = 'T'
        .w_DETIPSOG = 'T'
        .w_DETIPCON = 'N'
        .w_TIPCON = .w_DETIPCON
        .w_DECODCON = Space(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DECODCON))
          .link_1_9('Full')
        endif
        .w_DECODCONF = Space(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_DECODCONF))
          .link_1_10('Full')
        endif
        .w_CONSUP = SPACE(15)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CONSUP))
          .link_1_11('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CATCON))
          .link_1_12('Full')
        endif
        .w_ANOPETRE = 'T'
        .w_ANTIPPRE = 'T'
        .w_OSTIPOPE = 'T'
        .w_OSRIFCON = SPACE(30)
        .w_OSTIPFAT = 'T'
          .DoRTCalc(16,16,.f.)
        .w_DETIPREG = 'N'
        .w_DEMODPAG = iif(.w_TIPREC='4' or empty(.w_DEMODPAG), 'N', .w_DEMODPAG)
        .w_DEESCLUD = 'T'
        .w_TIPREC = 'T'
        .w_IMPO_NEG = 'T'
        .w_VARIMP = 'T'
        .w_VARIMS = 'T'
        .w_OBTEST = CTOD('01-01-1900')
      .oPgFrm.Page2.oPag.ZoomAg.Calculate()
      .oPgFrm.Page2.oPag.ZoomDe.Calculate()
        .w_DESERIAL = NVL(.w_ZOOMDE.getVar('DESERIAL'), ' ')
        .w_ORDINA = NVL(.w_ZOOMDE.getVar('ORDINA'), ' ')
        .w_AREA = NVL(.w_ZOOMDE.getVar('AREA'), ' ')
        .w_CPROWNUM = NVL(.w_ZOOMDE.getVar('CPROWNUM'), ' ')
      .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
          .DoRTCalc(29,36,.f.)
        .w_SELECT = 'N'
        .w_AGGIORNAMENTO = .f.
        .w_NEWMODPAG = iif(.w_DEMODPAG='1', '2', iif(.w_DEMODPAG='2', '1', ' '))
        .w_NEWVARIMP = iif(.w_VARIMP='1','2','1')
        .w_NEWVARIMS = iif(.w_VARIMS='1','2','1')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_18.enabled = this.oPgFrm.Page2.oPag.oBtn_2_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_TIPCON = .w_DETIPCON
        if .o_DETIPCON<>.w_DETIPCON
            .w_DECODCON = Space(15)
          .link_1_9('Full')
        endif
        if .o_DETIPCON<>.w_DETIPCON
            .w_DECODCONF = Space(15)
          .link_1_10('Full')
        endif
        if .o_DETIPCON<>.w_DETIPCON
            .w_CONSUP = SPACE(15)
          .link_1_11('Full')
        endif
        .DoRTCalc(10,13,.t.)
        if .o_OSTIPOPE<>.w_OSTIPOPE
            .w_OSRIFCON = SPACE(30)
        endif
        .DoRTCalc(15,17,.t.)
        if .o_TIPREC<>.w_TIPREC
            .w_DEMODPAG = iif(.w_TIPREC='4' or empty(.w_DEMODPAG), 'N', .w_DEMODPAG)
        endif
        .DoRTCalc(19,21,.t.)
        if .o_TIPREC<>.w_TIPREC
            .w_VARIMP = 'T'
        endif
        if .o_TIPREC<>.w_TIPREC
            .w_VARIMS = 'T'
        endif
        .oPgFrm.Page2.oPag.ZoomAg.Calculate()
        .oPgFrm.Page2.oPag.ZoomDe.Calculate()
        .DoRTCalc(24,24,.t.)
            .w_DESERIAL = NVL(.w_ZOOMDE.getVar('DESERIAL'), ' ')
            .w_ORDINA = NVL(.w_ZOOMDE.getVar('ORDINA'), ' ')
            .w_AREA = NVL(.w_ZOOMDE.getVar('AREA'), ' ')
            .w_CPROWNUM = NVL(.w_ZOOMDE.getVar('CPROWNUM'), ' ')
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        if .o_SELECT<>.w_SELECT
          .Calculate_HZFEEPETPA()
        endif
        .DoRTCalc(29,38,.t.)
        if .o_DEMODPAG<>.w_DEMODPAG
            .w_NEWMODPAG = iif(.w_DEMODPAG='1', '2', iif(.w_DEMODPAG='2', '1', ' '))
        endif
        if .o_VARIMP<>.w_VARIMP
            .w_NEWVARIMP = iif(.w_VARIMP='1','2','1')
        endif
        if .o_VARIMS<>.w_VARIMS
            .w_NEWVARIMS = iif(.w_VARIMS='1','2','1')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomAg.Calculate()
        .oPgFrm.Page2.oPag.ZoomDe.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
    endwith
  return

  proc Calculate_QUFUDOFKCK()
    with this
          * --- Visualizzo zoom
          .w_ZOOMDE.visible = !.w_AGGIORNAMENTO
          .w_ZoomAg.visible = .w_AGGIORNAMENTO
          RemCalc(this;
             )
    endwith
  endproc
  proc Calculate_OEBUCQVPFH()
    with this
          * --- Apertura gestioni collegate
          .w_OBJECT = Gsai_Ade()
          .w_TRASH = .w_Object.EcpFilter()
          .w_Object.w_Deserial = .w_Deserial
          .w_TRASH = .w_Object.EcpSave()
          .w_Object.opGFRM.ActivePage = Icase(.w_Area='1',2,.w_Area='2',3,.w_Area='4',4,1)
          .w_GEST = Icase(.w_Area='1',.w_Object.Gsai_Mde,.w_Area='2',.w_Object.Gsai2Mde,.w_Area='4',.w_Object.Gsai4Mde,.f.)
          .w_TRASH = IIF(Empty(.w_Area),-1,.w_Gest.Search('Cprownum='+Alltrim(Str(.w_Cprownum,6,0))))
          .w_TRASH = IIF(.w_Trash<0,.f.,.w_Gest.SetRow(.w_Trash))
          .w_TRASH = IIF(.w_Trash,.w_Gest.Refresh(),.f.)
    endwith
  endproc
  proc Calculate_HZFEEPETPA()
    with this
          * --- Seleziona/Deseleziona tutto
          SelectDeselectZoom(this;
             )
    endwith
  endproc
  proc Calculate_JHVQHXEVHB()
    with this
          * --- Ripristina aggiornamento
          .w_AGGIORNAMENTO = .f.
    endwith
  endproc
  proc Calculate_VCTTZVUYIK()
    with this
          * --- Verifica se area � valorizzata
          ChkAreaSel(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDECODCON_1_9.enabled = this.oPgFrm.Page1.oPag.oDECODCON_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDECODCONF_1_10.enabled = this.oPgFrm.Page1.oPag.oDECODCONF_1_10.mCond()
    this.oPgFrm.Page1.oPag.oOSRIFCON_1_16.enabled = this.oPgFrm.Page1.oPag.oOSRIFCON_1_16.mCond()
    this.oPgFrm.Page1.oPag.oDEMODPAG_1_23.enabled = this.oPgFrm.Page1.oPag.oDEMODPAG_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oVARIMP_1_28.visible=!this.oPgFrm.Page1.oPag.oVARIMP_1_28.mHide()
    this.oPgFrm.Page1.oPag.oVARIMS_1_29.visible=!this.oPgFrm.Page1.oPag.oVARIMS_1_29.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.oPgFrm.Page2.oPag.oSELECT_2_12.visible=!this.oPgFrm.Page2.oPag.oSELECT_2_12.mHide()
    this.oPgFrm.Page2.oPag.oNEWMODPAG_2_14.visible=!this.oPgFrm.Page2.oPag.oNEWMODPAG_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_15.visible=!this.oPgFrm.Page2.oPag.oStr_2_15.mHide()
    this.oPgFrm.Page2.oPag.oNEWVARIMP_2_16.visible=!this.oPgFrm.Page2.oPag.oNEWVARIMP_2_16.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_18.visible=!this.oPgFrm.Page2.oPag.oBtn_2_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page2.oPag.oNEWVARIMS_2_19.visible=!this.oPgFrm.Page2.oPag.oNEWVARIMS_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_20.visible=!this.oPgFrm.Page2.oPag.oStr_2_20.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_QUFUDOFKCK()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.ZoomAg.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomDe.Event(cEvent)
        if lower(cEvent)==lower("w_zoomde selected")
          .Calculate_OEBUCQVPFH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 1")
          .Calculate_JHVQHXEVHB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoomag row checked")
          .Calculate_VCTTZVUYIK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DECODCON
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DECODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DETIPCON;
                     ,'ANCODICE',trim(this.w_DECODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DECODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DECODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DECODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_DETIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DECODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDECODCON_1_9'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DETIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DECODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DETIPCON;
                       ,'ANCODICE',this.w_DECODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DEDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DECODCON = space(15)
      endif
      this.w_DEDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DECODCONF
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DECODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DETIPCON;
                     ,'ANCODICE',trim(this.w_DECODCONF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DECODCONF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DECODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DECODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_DETIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DECODCONF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDECODCONF_1_10'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DETIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DECODCONF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DETIPCON;
                       ,'ANCODICE',this.w_DECODCONF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODCONF = NVL(_Link_.ANCODICE,space(15))
      this.w_DEDESCRIF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DECODCONF = space(15)
      endif
      this.w_DEDESCRIF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSUP
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSUP))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CONSUP)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSUP_1_11'),i_cWhere,'GSAR_AMC',"MASTRI",'GSAI_MCO.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSUP)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(35))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONSUP = space(15)
      endif
      this.w_DESMAS = space(35)
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND ((.w_TIPCON='N' AND (.w_TIPMAS='C' or .w_TIPMAS='F')) OR (.w_TIPCON=.w_TIPMAS))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CONSUP = space(15)
        this.w_DESMAS = space(35)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_12'),i_cWhere,'GSAR_AC2',"CATEGORIE CONTABILI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDE__ANNO_1_2.value==this.w_DE__ANNO)
      this.oPgFrm.Page1.oPag.oDE__ANNO_1_2.value=this.w_DE__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDACONT_1_3.RadioValue()==this.w_DEDACONT)
      this.oPgFrm.Page1.oPag.oDEDACONT_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLGEN_1_4.RadioValue()==this.w_DESCLGEN)
      this.oPgFrm.Page1.oPag.oDESCLGEN_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPSOG_1_6.RadioValue()==this.w_DETIPSOG)
      this.oPgFrm.Page1.oPag.oDETIPSOG_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPCON_1_7.RadioValue()==this.w_DETIPCON)
      this.oPgFrm.Page1.oPag.oDETIPCON_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDECODCON_1_9.value==this.w_DECODCON)
      this.oPgFrm.Page1.oPag.oDECODCON_1_9.value=this.w_DECODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDECODCONF_1_10.value==this.w_DECODCONF)
      this.oPgFrm.Page1.oPag.oDECODCONF_1_10.value=this.w_DECODCONF
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSUP_1_11.value==this.w_CONSUP)
      this.oPgFrm.Page1.oPag.oCONSUP_1_11.value=this.w_CONSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_12.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_12.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oANOPETRE_1_13.RadioValue()==this.w_ANOPETRE)
      this.oPgFrm.Page1.oPag.oANOPETRE_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANTIPPRE_1_14.RadioValue()==this.w_ANTIPPRE)
      this.oPgFrm.Page1.oPag.oANTIPPRE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSTIPOPE_1_15.RadioValue()==this.w_OSTIPOPE)
      this.oPgFrm.Page1.oPag.oOSTIPOPE_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOSRIFCON_1_16.value==this.w_OSRIFCON)
      this.oPgFrm.Page1.oPag.oOSRIFCON_1_16.value=this.w_OSRIFCON
    endif
    if not(this.oPgFrm.Page1.oPag.oOSTIPFAT_1_17.RadioValue()==this.w_OSTIPFAT)
      this.oPgFrm.Page1.oPag.oOSTIPFAT_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDESCRI_1_18.value==this.w_DEDESCRI)
      this.oPgFrm.Page1.oPag.oDEDESCRI_1_18.value=this.w_DEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPREG_1_22.RadioValue()==this.w_DETIPREG)
      this.oPgFrm.Page1.oPag.oDETIPREG_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEMODPAG_1_23.RadioValue()==this.w_DEMODPAG)
      this.oPgFrm.Page1.oPag.oDEMODPAG_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEESCLUD_1_25.RadioValue()==this.w_DEESCLUD)
      this.oPgFrm.Page1.oPag.oDEESCLUD_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPREC_1_26.RadioValue()==this.w_TIPREC)
      this.oPgFrm.Page1.oPag.oTIPREC_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPO_NEG_1_27.RadioValue()==this.w_IMPO_NEG)
      this.oPgFrm.Page1.oPag.oIMPO_NEG_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVARIMP_1_28.RadioValue()==this.w_VARIMP)
      this.oPgFrm.Page1.oPag.oVARIMP_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVARIMS_1_29.RadioValue()==this.w_VARIMS)
      this.oPgFrm.Page1.oPag.oVARIMS_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDESCRIF_1_42.value==this.w_DEDESCRIF)
      this.oPgFrm.Page1.oPag.oDEDESCRIF_1_42.value=this.w_DEDESCRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAS_1_43.value==this.w_DESMAS)
      this.oPgFrm.Page1.oPag.oDESMAS_1_43.value=this.w_DESMAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_48.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_48.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oSELECT_2_12.RadioValue()==this.w_SELECT)
      this.oPgFrm.Page2.oPag.oSELECT_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWMODPAG_2_14.RadioValue()==this.w_NEWMODPAG)
      this.oPgFrm.Page2.oPag.oNEWMODPAG_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWVARIMP_2_16.RadioValue()==this.w_NEWVARIMP)
      this.oPgFrm.Page2.oPag.oNEWVARIMP_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWVARIMS_2_19.RadioValue()==this.w_NEWVARIMS)
      this.oPgFrm.Page2.oPag.oNEWVARIMS_2_19.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DE__ANNO)) or not(.w_DE__ANNO>2005 and .w_DE__ANNO<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDE__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DE__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NULIV=1 AND ((.w_TIPCON='N' AND (.w_TIPMAS='C' or .w_TIPMAS='F')) OR (.w_TIPCON=.w_TIPMAS)))  and not(empty(.w_CONSUP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONSUP_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DETIPCON = this.w_DETIPCON
    this.o_OSTIPOPE = this.w_OSTIPOPE
    this.o_DEMODPAG = this.w_DEMODPAG
    this.o_TIPREC = this.w_TIPREC
    this.o_VARIMP = this.w_VARIMP
    this.o_VARIMS = this.w_VARIMS
    this.o_SELECT = this.w_SELECT
    return

enddefine

* --- Define pages as container
define class tgsai_kmePag1 as StdContainer
  Width  = 787
  height = 538
  stdWidth  = 787
  stdheight = 538
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDE__ANNO_1_2 as StdField with uid="UQWWLVVPBH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DE__ANNO", cQueryName = "DE__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 158649723,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=161, Top=41, cSayPict='"9999"', cGetPict='"9999"'

  func oDE__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DE__ANNO>2005 and .w_DE__ANNO<2100)
    endwith
    return bRes
  endfunc


  add object oDEDACONT_1_3 as StdCombo with uid="HHHJPCDAST",rtseq=2,rtrep=.f.,left=387,top=41,width=73,height=21;
    , ToolTipText = "Dati estratti da verificare";
    , HelpContextID = 141852022;
    , cFormVar="w_DEDACONT",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDEDACONT_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDEDACONT_1_3.GetRadio()
    this.Parent.oContained.w_DEDACONT = this.RadioValue()
    return .t.
  endfunc

  func oDEDACONT_1_3.SetRadio()
    this.Parent.oContained.w_DEDACONT=trim(this.Parent.oContained.w_DEDACONT)
    this.value = ;
      iif(this.Parent.oContained.w_DEDACONT=='S',1,;
      iif(this.Parent.oContained.w_DEDACONT=='N',2,;
      iif(this.Parent.oContained.w_DEDACONT=='T',3,;
      0)))
  endfunc


  add object oDESCLGEN_1_4 as StdCombo with uid="BQLKCHSUNV",rtseq=3,rtrep=.f.,left=667,top=41,width=73,height=21;
    , ToolTipText = "Escludi da generazione";
    , HelpContextID = 1995396;
    , cFormVar="w_DESCLGEN",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDESCLGEN_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDESCLGEN_1_4.GetRadio()
    this.Parent.oContained.w_DESCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oDESCLGEN_1_4.SetRadio()
    this.Parent.oContained.w_DESCLGEN=trim(this.Parent.oContained.w_DESCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_DESCLGEN=='S',1,;
      iif(this.Parent.oContained.w_DESCLGEN=='N',2,;
      iif(this.Parent.oContained.w_DESCLGEN=='T',3,;
      0)))
  endfunc


  add object oDETIPSOG_1_6 as StdCombo with uid="VMJQAYOIOV",rtseq=4,rtrep=.f.,left=161,top=71,width=109,height=21;
    , ToolTipText = "Tipo soggetto";
    , HelpContextID = 207913597;
    , cFormVar="w_DETIPSOG",RowSource=""+"Iva,"+"Privato,"+"Non residente,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPSOG_1_6.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'N',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oDETIPSOG_1_6.GetRadio()
    this.Parent.oContained.w_DETIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oDETIPSOG_1_6.SetRadio()
    this.Parent.oContained.w_DETIPSOG=trim(this.Parent.oContained.w_DETIPSOG)
    this.value = ;
      iif(this.Parent.oContained.w_DETIPSOG=='I',1,;
      iif(this.Parent.oContained.w_DETIPSOG=='P',2,;
      iif(this.Parent.oContained.w_DETIPSOG=='N',3,;
      iif(this.Parent.oContained.w_DETIPSOG=='T',4,;
      0))))
  endfunc


  add object oDETIPCON_1_7 as StdCombo with uid="JNYAXOMVNG",rtseq=5,rtrep=.f.,left=161,top=103,width=109,height=21;
    , HelpContextID = 60521852;
    , cFormVar="w_DETIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Nessun filtro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPCON_1_7.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oDETIPCON_1_7.GetRadio()
    this.Parent.oContained.w_DETIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDETIPCON_1_7.SetRadio()
    this.Parent.oContained.w_DETIPCON=trim(this.Parent.oContained.w_DETIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DETIPCON=='C',1,;
      iif(this.Parent.oContained.w_DETIPCON=='F',2,;
      iif(this.Parent.oContained.w_DETIPCON=='N',3,;
      0)))
  endfunc

  func oDETIPCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DECODCON)
        bRes2=.link_1_9('Full')
      endif
      if .not. empty(.w_DECODCONF)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDECODCON_1_9 as StdField with uid="UCJURWGOQP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DECODCON", cQueryName = "DECODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto iniziale",;
    HelpContextID = 72781180,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=309, Top=103, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DETIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DECODCON"

  func oDECODCON_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DETIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oDECODCON_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oDECODCON_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDECODCON_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DETIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DETIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDECODCON_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oDECODCON_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DETIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DECODCON
     i_obj.ecpSave()
  endproc

  add object oDECODCONF_1_10 as StdField with uid="INMNTQUMMI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DECODCONF", cQueryName = "DECODCONF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto finale",;
    HelpContextID = 72780060,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=309, Top=135, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DETIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DECODCONF"

  func oDECODCONF_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DETIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oDECODCONF_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oDECODCONF_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDECODCONF_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DETIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DETIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDECODCONF_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oDECODCONF_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DETIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DECODCONF
     i_obj.ecpSave()
  endproc

  add object oCONSUP_1_11 as StdField with uid="MPOCORUZOD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CONSUP", cQueryName = "CONSUP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice mastro",;
    HelpContextID = 163458086,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=167, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSUP"

  func oCONSUP_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSUP_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSUP_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSUP_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"MASTRI",'GSAI_MCO.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSUP_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSUP
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_12 as StdField with uid="IOOIXEIKPM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile",;
    HelpContextID = 122584614,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=161, Top=199, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"CATEGORIE CONTABILI",'',this.parent.oContained
  endproc
  proc oCATCON_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc


  add object oANOPETRE_1_13 as StdCombo with uid="MGBBFYASRA",rtseq=11,rtrep=.f.,left=161,top=231,width=147,height=21;
    , ToolTipText = "Classificazione soggetto";
    , HelpContextID = 213597003;
    , cFormVar="w_ANOPETRE",RowSource=""+"Tutti tranne esclusi,"+"Corrispettivi periodici,"+"Contratti collegati,"+"Non definibile,"+"Escludi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANOPETRE_1_13.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'N',;
    iif(this.value =5,'E',;
    ' '))))))
  endfunc
  func oANOPETRE_1_13.GetRadio()
    this.Parent.oContained.w_ANOPETRE = this.RadioValue()
    return .t.
  endfunc

  func oANOPETRE_1_13.SetRadio()
    this.Parent.oContained.w_ANOPETRE=trim(this.Parent.oContained.w_ANOPETRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANOPETRE=='T',1,;
      iif(this.Parent.oContained.w_ANOPETRE=='P',2,;
      iif(this.Parent.oContained.w_ANOPETRE=='C',3,;
      iif(this.Parent.oContained.w_ANOPETRE=='N',4,;
      iif(this.Parent.oContained.w_ANOPETRE=='E',5,;
      0)))))
  endfunc


  add object oANTIPPRE_1_14 as StdCombo with uid="BTJSXLXSEN",rtseq=12,rtrep=.f.,left=455,top=231,width=147,height=21;
    , ToolTipText = "Classificazione operazioni tra beni e servizi";
    , HelpContextID = 157584203;
    , cFormVar="w_ANTIPPRE",RowSource=""+"Tutti,"+"Beni,"+"Servizi,"+"Non definibile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANTIPPRE_1_14.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'B',;
    iif(this.value =3,'S',;
    iif(this.value =4,'N',;
    ' ')))))
  endfunc
  func oANTIPPRE_1_14.GetRadio()
    this.Parent.oContained.w_ANTIPPRE = this.RadioValue()
    return .t.
  endfunc

  func oANTIPPRE_1_14.SetRadio()
    this.Parent.oContained.w_ANTIPPRE=trim(this.Parent.oContained.w_ANTIPPRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANTIPPRE=='T',1,;
      iif(this.Parent.oContained.w_ANTIPPRE=='B',2,;
      iif(this.Parent.oContained.w_ANTIPPRE=='S',3,;
      iif(this.Parent.oContained.w_ANTIPPRE=='N',4,;
      0))))
  endfunc


  add object oOSTIPOPE_1_15 as StdCombo with uid="NLRBCQDKHM",rtseq=13,rtrep=.f.,left=240,top=301,width=147,height=21;
    , ToolTipText = "Tipologia operazione";
    , HelpContextID = 140808491;
    , cFormVar="w_OSTIPOPE",RowSource=""+"Nessuno,"+"Corrispettivi periodici,"+"Contratti collegati,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOSTIPOPE_1_15.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oOSTIPOPE_1_15.GetRadio()
    this.Parent.oContained.w_OSTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oOSTIPOPE_1_15.SetRadio()
    this.Parent.oContained.w_OSTIPOPE=trim(this.Parent.oContained.w_OSTIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_OSTIPOPE=='N',1,;
      iif(this.Parent.oContained.w_OSTIPOPE=='P',2,;
      iif(this.Parent.oContained.w_OSTIPOPE=='C',3,;
      iif(this.Parent.oContained.w_OSTIPOPE=='T',4,;
      0))))
  endfunc

  add object oOSRIFCON_1_16 as StdField with uid="KCDSJCMYFT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_OSRIFCON", cQueryName = "OSRIFCON",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Codice riferimento contratto",;
    HelpContextID = 71012044,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=551, Top=302, InputMask=replicate('X',30)

  func oOSRIFCON_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSTIPOPE<>'N' )
    endwith
   endif
  endfunc


  add object oOSTIPFAT_1_17 as StdCombo with uid="AKMVZDKDWX",rtseq=15,rtrep=.f.,left=240,top=332,width=147,height=21;
    , HelpContextID = 258249018;
    , cFormVar="w_OSTIPFAT",RowSource=""+"Saldo,"+"Acconto,"+"Nota rettifica,"+"Corrispettivi,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOSTIPFAT_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'A',;
    iif(this.value =3,'N',;
    iif(this.value =4,'C',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oOSTIPFAT_1_17.GetRadio()
    this.Parent.oContained.w_OSTIPFAT = this.RadioValue()
    return .t.
  endfunc

  func oOSTIPFAT_1_17.SetRadio()
    this.Parent.oContained.w_OSTIPFAT=trim(this.Parent.oContained.w_OSTIPFAT)
    this.value = ;
      iif(this.Parent.oContained.w_OSTIPFAT=='S',1,;
      iif(this.Parent.oContained.w_OSTIPFAT=='A',2,;
      iif(this.Parent.oContained.w_OSTIPFAT=='N',3,;
      iif(this.Parent.oContained.w_OSTIPFAT=='C',4,;
      iif(this.Parent.oContained.w_OSTIPFAT=='T',5,;
      0)))))
  endfunc

  add object oDEDESCRI_1_18 as StdField with uid="MGGNLWGUXP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DEDESCRI", cQueryName = "DEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210731647,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=454, Top=103, InputMask=replicate('X',40)


  add object oDETIPREG_1_22 as StdCombo with uid="ZPOLZYFKDH",rtseq=17,rtrep=.f.,left=240,top=362,width=161,height=21;
    , ToolTipText = "Tipo regola applicata";
    , HelpContextID = 191136381;
    , cFormVar="w_DETIPREG",RowSource=""+"Nessun filtro,"+"Dati IVA,"+"Note di variazione,"+"Controparte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPREG_1_22.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    iif(this.value =3,'B',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oDETIPREG_1_22.GetRadio()
    this.Parent.oContained.w_DETIPREG = this.RadioValue()
    return .t.
  endfunc

  func oDETIPREG_1_22.SetRadio()
    this.Parent.oContained.w_DETIPREG=trim(this.Parent.oContained.w_DETIPREG)
    this.value = ;
      iif(this.Parent.oContained.w_DETIPREG=='N',1,;
      iif(this.Parent.oContained.w_DETIPREG=='A',2,;
      iif(this.Parent.oContained.w_DETIPREG=='B',3,;
      iif(this.Parent.oContained.w_DETIPREG=='C',4,;
      0))))
  endfunc


  add object oDEMODPAG_1_23 as StdCombo with uid="WUFQQLGNUI",rtseq=18,rtrep=.f.,left=599,top=362,width=161,height=21;
    , ToolTipText = "Modalit� di pagamento";
    , HelpContextID = 145363581;
    , cFormVar="w_DEMODPAG",RowSource=""+"Importo non frazionato,"+"Importo frazionato,"+"Corrispettivi periodici,"+"Nessun filtro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDEMODPAG_1_23.RadioValue()
    return(iif(this.value =1,"1",;
    iif(this.value =2,"2",;
    iif(this.value =3,"3",;
    iif(this.value =4,"N",;
    space(1))))))
  endfunc
  func oDEMODPAG_1_23.GetRadio()
    this.Parent.oContained.w_DEMODPAG = this.RadioValue()
    return .t.
  endfunc

  func oDEMODPAG_1_23.SetRadio()
    this.Parent.oContained.w_DEMODPAG=trim(this.Parent.oContained.w_DEMODPAG)
    this.value = ;
      iif(this.Parent.oContained.w_DEMODPAG=="1",1,;
      iif(this.Parent.oContained.w_DEMODPAG=="2",2,;
      iif(this.Parent.oContained.w_DEMODPAG=="3",3,;
      iif(this.Parent.oContained.w_DEMODPAG=="N",4,;
      0))))
  endfunc

  func oDEMODPAG_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPREC<>'4')
    endwith
   endif
  endfunc


  add object oDEESCLUD_1_25 as StdCombo with uid="LRIDQJCTEG",rtseq=19,rtrep=.f.,left=240,top=396,width=73,height=21;
    , ToolTipText = "Escludi documento da generazione";
    , HelpContextID = 77435514;
    , cFormVar="w_DEESCLUD",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDEESCLUD_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDEESCLUD_1_25.GetRadio()
    this.Parent.oContained.w_DEESCLUD = this.RadioValue()
    return .t.
  endfunc

  func oDEESCLUD_1_25.SetRadio()
    this.Parent.oContained.w_DEESCLUD=trim(this.Parent.oContained.w_DEESCLUD)
    this.value = ;
      iif(this.Parent.oContained.w_DEESCLUD=='S',1,;
      iif(this.Parent.oContained.w_DEESCLUD=='N',2,;
      iif(this.Parent.oContained.w_DEESCLUD=='T',3,;
      0)))
  endfunc


  add object oTIPREC_1_26 as StdCombo with uid="TYXJZZXOXZ",rtseq=20,rtrep=.f.,left=599,top=396,width=129,height=21;
    , ToolTipText = "Tipo record";
    , HelpContextID = 196953910;
    , cFormVar="w_TIPREC",RowSource=""+"Tutti,"+"Record tipo 1,"+"Record tipo 2/3,"+"Record tipo 4/5", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPREC_1_26.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'4',;
    space(1))))))
  endfunc
  func oTIPREC_1_26.GetRadio()
    this.Parent.oContained.w_TIPREC = this.RadioValue()
    return .t.
  endfunc

  func oTIPREC_1_26.SetRadio()
    this.Parent.oContained.w_TIPREC=trim(this.Parent.oContained.w_TIPREC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPREC=='T',1,;
      iif(this.Parent.oContained.w_TIPREC=='1',2,;
      iif(this.Parent.oContained.w_TIPREC=='2',3,;
      iif(this.Parent.oContained.w_TIPREC=='4',4,;
      0))))
  endfunc


  add object oIMPO_NEG_1_27 as StdCombo with uid="IKYSYIAVVO",rtseq=21,rtrep=.f.,left=240,top=433,width=73,height=21;
    , HelpContextID = 140135117;
    , cFormVar="w_IMPO_NEG",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIMPO_NEG_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oIMPO_NEG_1_27.GetRadio()
    this.Parent.oContained.w_IMPO_NEG = this.RadioValue()
    return .t.
  endfunc

  func oIMPO_NEG_1_27.SetRadio()
    this.Parent.oContained.w_IMPO_NEG=trim(this.Parent.oContained.w_IMPO_NEG)
    this.value = ;
      iif(this.Parent.oContained.w_IMPO_NEG=='S',1,;
      iif(this.Parent.oContained.w_IMPO_NEG=='N',2,;
      iif(this.Parent.oContained.w_IMPO_NEG=='T',3,;
      0)))
  endfunc


  add object oVARIMP_1_28 as StdCombo with uid="CJVZHSPAXG",rtseq=22,rtrep=.f.,left=551,top=438,width=100,height=21;
    , ToolTipText = "Tipo di variazione imponibile credito/debito";
    , HelpContextID = 154427222;
    , cFormVar="w_VARIMP",RowSource=""+"Tutti,"+"Credito,"+"Debito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVARIMP_1_28.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,"1",;
    iif(this.value =3,"2",;
    space(1)))))
  endfunc
  func oVARIMP_1_28.GetRadio()
    this.Parent.oContained.w_VARIMP = this.RadioValue()
    return .t.
  endfunc

  func oVARIMP_1_28.SetRadio()
    this.Parent.oContained.w_VARIMP=trim(this.Parent.oContained.w_VARIMP)
    this.value = ;
      iif(this.Parent.oContained.w_VARIMP=='T',1,;
      iif(this.Parent.oContained.w_VARIMP=="1",2,;
      iif(this.Parent.oContained.w_VARIMP=="2",3,;
      0)))
  endfunc

  func oVARIMP_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPREC<>'4')
    endwith
  endfunc


  add object oVARIMS_1_29 as StdCombo with uid="FYVCXEIDFQ",rtseq=23,rtrep=.f.,left=551,top=465,width=100,height=21;
    , ToolTipText = "Tipo di variazione imposta credito/debito";
    , HelpContextID = 204758870;
    , cFormVar="w_VARIMS",RowSource=""+"Tutti,"+"Credito,"+"Debito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVARIMS_1_29.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,"1",;
    iif(this.value =3,"2",;
    space(1)))))
  endfunc
  func oVARIMS_1_29.GetRadio()
    this.Parent.oContained.w_VARIMS = this.RadioValue()
    return .t.
  endfunc

  func oVARIMS_1_29.SetRadio()
    this.Parent.oContained.w_VARIMS=trim(this.Parent.oContained.w_VARIMS)
    this.value = ;
      iif(this.Parent.oContained.w_VARIMS=='T',1,;
      iif(this.Parent.oContained.w_VARIMS=="1",2,;
      iif(this.Parent.oContained.w_VARIMS=="2",3,;
      0)))
  endfunc

  func oVARIMS_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPREC<>'4')
    endwith
  endfunc


  add object oBtn_1_30 as StdButton with uid="RXYYDTQKLP",left=733, top=442, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati da aggiornare";
    , HelpContextID = 159494678;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        do ActivateZoomUpdate with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DEMODPAG$'N3' AND .w_TIPREC<>'4')
     endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="KWFWCRWNHO",left=629, top=491, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 159494678;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="ZXYGHBDURE",left=681, top=491, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 52399338;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"PgDn")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="KUWCMEGMLG",left=734, top=491, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 159494678;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDEDESCRIF_1_42 as StdField with uid="XMWLZOYUXF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DEDESCRIF", cQueryName = "DEDESCRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210732767,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=454, Top=135, InputMask=replicate('X',40)

  add object oDESMAS_1_43 as StdField with uid="MTWTUQTSEH",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESMAS", cQueryName = "DESMAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 192442934,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=309, Top=167, InputMask=replicate('X',35)

  add object oDESCON_1_48 as StdField with uid="IXFYEJXSIO",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 122581558,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=219, Top=199, InputMask=replicate('X',35)


  add object oObj_1_57 as cp_outputCombo with uid="ZLERXIVPWT",left=240, top=498, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 246546406

  add object oStr_1_1 as StdString with uid="WMPOHYZZNW",Visible=.t., Left=97, Top=43,;
    Alignment=1, Width=60, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="PVLCYLAQYI",Visible=.t., Left=57, Top=104,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="QPHBBHXHWS",Visible=.t., Left=38, Top=72,;
    Alignment=1, Width=119, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ZALCOBSEZR",Visible=.t., Left=99, Top=363,;
    Alignment=1, Width=137, Height=18,;
    Caption="Tipo regola applicata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ZNONHAVWWD",Visible=.t., Left=395, Top=363,;
    Alignment=1, Width=198, Height=18,;
    Caption="Modalit� di pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="AMWXDKXOMM",Visible=.t., Left=211, Top=43,;
    Alignment=1, Width=173, Height=18,;
    Caption="Dati estratti da verificare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="JELVCYJCSJ",Visible=.t., Left=465, Top=43,;
    Alignment=1, Width=197, Height=18,;
    Caption="Escludi da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="YWXTBQXIDL",Visible=.t., Left=3, Top=397,;
    Alignment=1, Width=233, Height=18,;
    Caption="Escludi documento da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="LHPVJGRFMB",Visible=.t., Left=500, Top=397,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tipo record:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="QURJZUSJFW",Visible=.t., Left=90, Top=434,;
    Alignment=1, Width=146, Height=18,;
    Caption="Importo negativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BNSTAFZAAA",Visible=.t., Left=280, Top=104,;
    Alignment=1, Width=27, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="QTJDMGJXFC",Visible=.t., Left=280, Top=137,;
    Alignment=1, Width=27, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="IYPFSOWZCO",Visible=.t., Left=93, Top=168,;
    Alignment=1, Width=64, Height=18,;
    Caption="Mastro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="DDBCOIUNGE",Visible=.t., Left=19, Top=200,;
    Alignment=1, Width=138, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="UYAAQHCPRD",Visible=.t., Left=5, Top=233,;
    Alignment=1, Width=152, Height=18,;
    Caption="Operazioni rilevanti IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="DFDUXKNAOV",Visible=.t., Left=312, Top=233,;
    Alignment=1, Width=139, Height=18,;
    Caption="Tipologia prevalente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="FKMIIQQLAV",Visible=.t., Left=112, Top=303,;
    Alignment=1, Width=124, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="YFMZZRZKLA",Visible=.t., Left=419, Top=305,;
    Alignment=1, Width=128, Height=18,;
    Caption="Riferimento contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="XUFDNCRDOS",Visible=.t., Left=112, Top=334,;
    Alignment=1, Width=124, Height=18,;
    Caption="Tipo fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="DNNRUQSVTF",Visible=.t., Left=5, Top=12,;
    Alignment=0, Width=128, Height=19,;
    Caption="Filtri di testata"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="PRTNDFMYKU",Visible=.t., Left=5, Top=266,;
    Alignment=0, Width=128, Height=19,;
    Caption="Filtri di dettaglio"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_58 as StdString with uid="ZDLOVMWIXH",Visible=.t., Left=150, Top=501,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di Stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="EQVSICIIGH",Visible=.t., Left=408, Top=438,;
    Alignment=1, Width=139, Height=18,;
    Caption="Variazione imponibile:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_TIPREC<>'4')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="FAQPSYIIVS",Visible=.t., Left=408, Top=465,;
    Alignment=1, Width=139, Height=18,;
    Caption="Variazione imposta:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_TIPREC<>'4')
    endwith
  endfunc

  add object oBox_1_20 as StdBox with uid="JSYJQHTSZX",left=-2, top=283, width=782,height=1

  add object oBox_1_56 as StdBox with uid="VRIVZBVKMU",left=-1, top=29, width=782,height=1
enddefine
define class tgsai_kmePag2 as StdContainer
  Width  = 787
  height = 538
  stdWidth  = 787
  stdheight = 538
  resizeXpos=387
  resizeYpos=258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomAg as cp_szoombox with uid="WUTNYJHGAP",left=0, top=5, width=783,height=475,;
    caption='ZoomAg',;
   bGlobalFont=.t.,;
    cMenuFile="",cTable="ANTIVADE",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cZoomFile="GSAI_KME",bRetriveAllRows=.f.,cZoomOnZoom="",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 261774742


  add object ZoomDe as cp_zoombox with uid="EVGGBUEBEF",left=0, top=5, width=785,height=532,;
    caption='ZoomDe',;
   bGlobalFont=.t.,;
    cMenuFile="",cTable="ANTIVADE",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSAI_KME",bRetriveAllRows=.f.,cZoomOnZoom="",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 231366038

  add object oSELECT_2_12 as StdRadio with uid="QMTYSLFKMI",rtseq=37,rtrep=.f.,left=3, top=485, width=139,height=35;
    , cFormVar="w_SELECT", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELECT_2_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Deseleziona tutto"
      this.Buttons(1).HelpContextID = 210764582
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Seleziona tutto"
      this.Buttons(2).HelpContextID = 210764582
      this.Buttons(2).Top=16
      this.SetAll("Width",137)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECT_2_12.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oSELECT_2_12.GetRadio()
    this.Parent.oContained.w_SELECT = this.RadioValue()
    return .t.
  endfunc

  func oSELECT_2_12.SetRadio()
    this.Parent.oContained.w_SELECT=trim(this.Parent.oContained.w_SELECT)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT=='N',1,;
      iif(this.Parent.oContained.w_SELECT=='S',2,;
      0))
  endfunc

  func oSELECT_2_12.mHide()
    with this.Parent.oContained
      return (!.w_AGGIORNAMENTO)
    endwith
  endfunc


  add object oNEWMODPAG_2_14 as StdCombo with uid="ETUYQNWORI",rtseq=39,rtrep=.f.,left=359,top=485,width=161,height=21, enabled=.f.;
    , ToolTipText = "Modalit� di pagamento da aggiornare";
    , HelpContextID = 223917959;
    , cFormVar="w_NEWMODPAG",RowSource=""+"Importo non frazionato,"+"Importo frazionato", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNEWMODPAG_2_14.RadioValue()
    return(iif(this.value =1,"1",;
    iif(this.value =2,"2",;
    space(1))))
  endfunc
  func oNEWMODPAG_2_14.GetRadio()
    this.Parent.oContained.w_NEWMODPAG = this.RadioValue()
    return .t.
  endfunc

  func oNEWMODPAG_2_14.SetRadio()
    this.Parent.oContained.w_NEWMODPAG=trim(this.Parent.oContained.w_NEWMODPAG)
    this.value = ;
      iif(this.Parent.oContained.w_NEWMODPAG=="1",1,;
      iif(this.Parent.oContained.w_NEWMODPAG=="2",2,;
      0))
  endfunc

  func oNEWMODPAG_2_14.mHide()
    with this.Parent.oContained
      return (!.w_AGGIORNAMENTO OR .w_DEMODPAG<>'1' AND .w_DEMODPAG<>'2')
    endwith
  endfunc


  add object oNEWVARIMP_2_16 as StdCombo with uid="VEPTZGFAUJ",rtseq=40,rtrep=.f.,left=359,top=511,width=100,height=21;
    , ToolTipText = "Tipo di variazione imponibile credito/debito";
    , HelpContextID = 92162013;
    , cFormVar="w_NEWVARIMP",RowSource=""+"Credito,"+"Debito", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNEWVARIMP_2_16.RadioValue()
    return(iif(this.value =1,"1",;
    iif(this.value =2,"2",;
    space(1))))
  endfunc
  func oNEWVARIMP_2_16.GetRadio()
    this.Parent.oContained.w_NEWVARIMP = this.RadioValue()
    return .t.
  endfunc

  func oNEWVARIMP_2_16.SetRadio()
    this.Parent.oContained.w_NEWVARIMP=trim(this.Parent.oContained.w_NEWVARIMP)
    this.value = ;
      iif(this.Parent.oContained.w_NEWVARIMP=="1",1,;
      iif(this.Parent.oContained.w_NEWVARIMP=="2",2,;
      0))
  endfunc

  func oNEWVARIMP_2_16.mHide()
    with this.Parent.oContained
      return (!.w_AGGIORNAMENTO OR .w_TIPREC<>'4')
    endwith
  endfunc


  add object oBtn_2_18 as StdButton with uid="CQRONLTTLT",left=734, top=485, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare i dati estratti selezionati";
    , HelpContextID = 159494678;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_18.Click()
      with this.Parent.oContained
        do GSAI_BME with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_AGGIORNAMENTO)
     endwith
    endif
  endfunc


  add object oNEWVARIMS_2_19 as StdCombo with uid="DFZAUYQTLS",rtseq=41,rtrep=.f.,left=608,top=511,width=100,height=21;
    , ToolTipText = "Tipo di variazione imposta credito/debito";
    , HelpContextID = 92161965;
    , cFormVar="w_NEWVARIMS",RowSource=""+"Credito,"+"Debito", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNEWVARIMS_2_19.RadioValue()
    return(iif(this.value =1,"1",;
    iif(this.value =2,"2",;
    space(1))))
  endfunc
  func oNEWVARIMS_2_19.GetRadio()
    this.Parent.oContained.w_NEWVARIMS = this.RadioValue()
    return .t.
  endfunc

  func oNEWVARIMS_2_19.SetRadio()
    this.Parent.oContained.w_NEWVARIMS=trim(this.Parent.oContained.w_NEWVARIMS)
    this.value = ;
      iif(this.Parent.oContained.w_NEWVARIMS=="1",1,;
      iif(this.Parent.oContained.w_NEWVARIMS=="2",2,;
      0))
  endfunc

  func oNEWVARIMS_2_19.mHide()
    with this.Parent.oContained
      return (!.w_AGGIORNAMENTO OR .w_TIPREC<>'4')
    endwith
  endfunc

  add object oStr_2_15 as StdString with uid="QFBDXKZYFR",Visible=.t., Left=155, Top=487,;
    Alignment=1, Width=198, Height=18,;
    Caption="Modalit� di pagamento:"  ;
  , bGlobalFont=.t.

  func oStr_2_15.mHide()
    with this.Parent.oContained
      return (!.w_AGGIORNAMENTO OR .w_DEMODPAG<>'1' AND .w_DEMODPAG<>'2')
    endwith
  endfunc

  add object oStr_2_20 as StdString with uid="CVERCUWJHS",Visible=.t., Left=214, Top=513,;
    Alignment=1, Width=139, Height=18,;
    Caption="Variazione imponibile:"  ;
  , bGlobalFont=.t.

  func oStr_2_20.mHide()
    with this.Parent.oContained
      return (!.w_AGGIORNAMENTO OR .w_TIPREC<>'4')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="FMGTOPFEQG",Visible=.t., Left=465, Top=513,;
    Alignment=1, Width=139, Height=18,;
    Caption="Variazione imposta:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (!.w_AGGIORNAMENTO OR .w_TIPREC<>'4')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_kme','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsai_kme
proc SelectDeselectZoom(oObj)
if oObj.w_SELECT = 'S'
  UPDATE (oObj.w_ZoomAg.cCursor) SET XCHK = 1 WHERE NOT EMPTY(NVL(AREA,''))
else
  UPDATE (oObj.w_ZoomAg.cCursor) SET XCHK = 0
endif
SELECT(oObj.w_ZoomAg.cCursor)
go top
oObj.w_ZoomAg.Refresh()
endproc

proc ChkAreaSel(oObj)
local nPosCurs
SELECT(oObj.w_ZoomAg.cCursor)
if EMPTY(NVL(AREA,''))
  SELECT(oObj.w_ZoomAg.cCursor)
  nPosCurs=recno()
  skip
  do while not eof() and not EMPTY(NVL(AREA,''))
    replace XCHK with 1
    SELECT(oObj.w_ZoomAg.cCursor)
    skip   
  enddo
  SELECT(oObj.w_ZoomAg.cCursor)
  go nPosCurs
  replace XCHK with 0  
  oObj.w_ZoomAg.Refresh()
endif
endproc

proc ActivateZoomUpdate(oObj)
oObj.w_AGGIORNAMENTO=.t.
oObj.oPgFrm.Page1.oPag.oBtn_1_32.Click()
endproc

proc RemCalc(oObj)
oObj.mCalc(.t.)
endproc
* --- Fine Area Manuale
