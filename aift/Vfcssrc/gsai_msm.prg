* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_msm                                                        *
*              Dettaglio parametri dati IVA acquisti da San Marino             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-28                                                      *
* Last revis.: 2014-02-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsai_msm")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsai_msm")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsai_msm")
  return

* --- Class definition
define class tgsai_msm as StdPCForm
  Width  = 656
  Height = 416
  Top    = 10
  Left   = 10
  cComment = "Dettaglio parametri dati IVA acquisti da San Marino"
  cPrg = "gsai_msm"
  HelpContextID=171309207
  add object cnt as tcgsai_msm
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsai_msm as PCContext
  w_SM__ANNO = space(4)
  w_SMCAUCON = space(5)
  w_CCDESCRI = space(35)
  w_SMTIPREG = space(1)
  w_SMNUMREG = 0
  w_SMCODIVA = space(5)
  w_SMCONDIZ = space(1)
  w_OBTEST = space(8)
  w_CCTIPREG = space(1)
  w_CCFLRIFE = space(1)
  w_CCTIPDOC = space(2)
  proc Save(i_oFrom)
    this.w_SM__ANNO = i_oFrom.w_SM__ANNO
    this.w_SMCAUCON = i_oFrom.w_SMCAUCON
    this.w_CCDESCRI = i_oFrom.w_CCDESCRI
    this.w_SMTIPREG = i_oFrom.w_SMTIPREG
    this.w_SMNUMREG = i_oFrom.w_SMNUMREG
    this.w_SMCODIVA = i_oFrom.w_SMCODIVA
    this.w_SMCONDIZ = i_oFrom.w_SMCONDIZ
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CCTIPREG = i_oFrom.w_CCTIPREG
    this.w_CCFLRIFE = i_oFrom.w_CCFLRIFE
    this.w_CCTIPDOC = i_oFrom.w_CCTIPDOC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_SM__ANNO = this.w_SM__ANNO
    i_oTo.w_SMCAUCON = this.w_SMCAUCON
    i_oTo.w_CCDESCRI = this.w_CCDESCRI
    i_oTo.w_SMTIPREG = this.w_SMTIPREG
    i_oTo.w_SMNUMREG = this.w_SMNUMREG
    i_oTo.w_SMCODIVA = this.w_SMCODIVA
    i_oTo.w_SMCONDIZ = this.w_SMCONDIZ
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CCTIPREG = this.w_CCTIPREG
    i_oTo.w_CCFLRIFE = this.w_CCFLRIFE
    i_oTo.w_CCTIPDOC = this.w_CCTIPDOC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsai_msm as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 656
  Height = 416
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-03"
  HelpContextID=171309207
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DATPIVSM_IDX = 0
  CAU_CONT_IDX = 0
  VOCIIVA_IDX = 0
  cFile = "DATPIVSM"
  cKeySelect = "SM__ANNO"
  cKeyWhere  = "SM__ANNO=this.w_SM__ANNO"
  cKeyDetail  = "SM__ANNO=this.w_SM__ANNO"
  cKeyWhereODBC = '"SM__ANNO="+cp_ToStrODBC(this.w_SM__ANNO)';

  cKeyDetailWhereODBC = '"SM__ANNO="+cp_ToStrODBC(this.w_SM__ANNO)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DATPIVSM.SM__ANNO="+cp_ToStrODBC(this.w_SM__ANNO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DATPIVSM.CPROWNUM '
  cPrg = "gsai_msm"
  cComment = "Dettaglio parametri dati IVA acquisti da San Marino"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SM__ANNO = space(4)
  w_SMCAUCON = space(5)
  w_CCDESCRI = space(35)
  w_SMTIPREG = space(1)
  o_SMTIPREG = space(1)
  w_SMNUMREG = 0
  w_SMCODIVA = space(5)
  w_SMCONDIZ = space(1)
  o_SMCONDIZ = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CCTIPREG = space(1)
  w_CCFLRIFE = space(1)
  w_CCTIPDOC = space(2)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_msmPag1","gsai_msm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='DATPIVSM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATPIVSM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATPIVSM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsai_msm'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DATPIVSM where SM__ANNO=KeySet.SM__ANNO
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DATPIVSM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSM_IDX,2],this.bLoadRecFilter,this.DATPIVSM_IDX,"gsai_msm")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATPIVSM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATPIVSM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATPIVSM '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SM__ANNO',this.w_SM__ANNO  )
      select * from (i_cTable) DATPIVSM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_SM__ANNO = NVL(SM__ANNO,space(4))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DATPIVSM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CCDESCRI = space(35)
          .w_CCTIPREG = space(1)
          .w_CCFLRIFE = space(1)
          .w_CCTIPDOC = space(2)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_SMCAUCON = NVL(SMCAUCON,space(5))
          if link_2_1_joined
            this.w_SMCAUCON = NVL(CCCODICE201,NVL(this.w_SMCAUCON,space(5)))
            this.w_CCDESCRI = NVL(CCDESCRI201,space(35))
            this.w_CCTIPREG = NVL(CCTIPREG201,space(1))
            this.w_CCFLRIFE = NVL(CCFLRIFE201,space(1))
            this.w_CCTIPDOC = NVL(CCTIPDOC201,space(2))
          else
          .link_2_1('Load')
          endif
          .w_SMTIPREG = NVL(SMTIPREG,space(1))
          .w_SMNUMREG = NVL(SMNUMREG,0)
          .w_SMCODIVA = NVL(SMCODIVA,space(5))
          * evitabile
          *.link_2_5('Load')
          .w_SMCONDIZ = NVL(SMCONDIZ,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_SM__ANNO=space(4)
      .w_SMCAUCON=space(5)
      .w_CCDESCRI=space(35)
      .w_SMTIPREG=space(1)
      .w_SMNUMREG=0
      .w_SMCODIVA=space(5)
      .w_SMCONDIZ=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_CCTIPREG=space(1)
      .w_CCFLRIFE=space(1)
      .w_CCTIPDOC=space(2)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_SMCAUCON))
         .link_2_1('Full')
        endif
        .DoRTCalc(3,3,.f.)
        .w_SMTIPREG = 'N'
        .w_SMNUMREG = 0
        .w_SMCODIVA = iif(.w_Smcondiz='B',Space(5),.w_Smcodiva)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SMCODIVA))
         .link_2_5('Full')
        endif
        .w_SMCONDIZ = 'I'
        .w_OBTEST = CTOD('01-01-1900')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATPIVSM')
    this.DoRTCalc(9,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DATPIVSM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATPIVSM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SM__ANNO,"SM__ANNO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_SMCAUCON C(5);
      ,t_CCDESCRI C(35);
      ,t_SMTIPREG N(3);
      ,t_SMNUMREG N(2);
      ,t_SMCODIVA C(5);
      ,t_SMCONDIZ N(3);
      ,CPROWNUM N(10);
      ,t_CCTIPREG C(1);
      ,t_CCFLRIFE C(1);
      ,t_CCTIPDOC C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsai_msmbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMCAUCON_2_1.controlsource=this.cTrsName+'.t_SMCAUCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_2.controlsource=this.cTrsName+'.t_CCDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMTIPREG_2_3.controlsource=this.cTrsName+'.t_SMTIPREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMNUMREG_2_4.controlsource=this.cTrsName+'.t_SMNUMREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMCODIVA_2_5.controlsource=this.cTrsName+'.t_SMCODIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSMCONDIZ_2_6.controlsource=this.cTrsName+'.t_SMCONDIZ'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(72)
    this.AddVLine(336)
    this.AddVLine(444)
    this.AddVLine(483)
    this.AddVLine(552)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCAUCON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATPIVSM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATPIVSM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSM_IDX,2])
      *
      * insert into DATPIVSM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATPIVSM')
        i_extval=cp_InsertValODBCExtFlds(this,'DATPIVSM')
        i_cFldBody=" "+;
                  "(SM__ANNO,SMCAUCON,SMTIPREG,SMNUMREG,SMCODIVA"+;
                  ",SMCONDIZ,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SM__ANNO)+","+cp_ToStrODBCNull(this.w_SMCAUCON)+","+cp_ToStrODBC(this.w_SMTIPREG)+","+cp_ToStrODBC(this.w_SMNUMREG)+","+cp_ToStrODBCNull(this.w_SMCODIVA)+;
             ","+cp_ToStrODBC(this.w_SMCONDIZ)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATPIVSM')
        i_extval=cp_InsertValVFPExtFlds(this,'DATPIVSM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'SM__ANNO',this.w_SM__ANNO)
        INSERT INTO (i_cTable) (;
                   SM__ANNO;
                  ,SMCAUCON;
                  ,SMTIPREG;
                  ,SMNUMREG;
                  ,SMCODIVA;
                  ,SMCONDIZ;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_SM__ANNO;
                  ,this.w_SMCAUCON;
                  ,this.w_SMTIPREG;
                  ,this.w_SMNUMREG;
                  ,this.w_SMCODIVA;
                  ,this.w_SMCONDIZ;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DATPIVSM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not Empty(t_SMCAUCON) Or iif(vartype(t_SMTIPREG)='N',iif(t_SMTIPREG=1,'N','X'),t_SMTIPREG)<>'N' Or not Empty(t_SMCODIVA)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DATPIVSM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DATPIVSM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not Empty(t_SMCAUCON) Or iif(vartype(t_SMTIPREG)='N',iif(t_SMTIPREG=1,'N','X'),t_SMTIPREG)<>'N' Or not Empty(t_SMCODIVA)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DATPIVSM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DATPIVSM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " SMCAUCON="+cp_ToStrODBCNull(this.w_SMCAUCON)+;
                     ",SMTIPREG="+cp_ToStrODBC(this.w_SMTIPREG)+;
                     ",SMNUMREG="+cp_ToStrODBC(this.w_SMNUMREG)+;
                     ",SMCODIVA="+cp_ToStrODBCNull(this.w_SMCODIVA)+;
                     ",SMCONDIZ="+cp_ToStrODBC(this.w_SMCONDIZ)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DATPIVSM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      SMCAUCON=this.w_SMCAUCON;
                     ,SMTIPREG=this.w_SMTIPREG;
                     ,SMNUMREG=this.w_SMNUMREG;
                     ,SMCODIVA=this.w_SMCODIVA;
                     ,SMCONDIZ=this.w_SMCONDIZ;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATPIVSM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not Empty(t_SMCAUCON) Or iif(vartype(t_SMTIPREG)='N',iif(t_SMTIPREG=1,'N','X'),t_SMTIPREG)<>'N' Or not Empty(t_SMCODIVA)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DATPIVSM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not Empty(t_SMCAUCON) Or iif(vartype(t_SMTIPREG)='N',iif(t_SMTIPREG=1,'N','X'),t_SMTIPREG)<>'N' Or not Empty(t_SMCODIVA)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATPIVSM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPIVSM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_SMTIPREG<>.w_SMTIPREG
          .w_SMNUMREG = 0
        endif
        if .o_SMCONDIZ<>.w_SMCONDIZ
          .w_SMCODIVA = iif(.w_Smcondiz='B',Space(5),.w_Smcodiva)
          .link_2_5('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CCTIPREG with this.w_CCTIPREG
      replace t_CCFLRIFE with this.w_CCFLRIFE
      replace t_CCTIPDOC with this.w_CCTIPDOC
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSMNUMREG_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSMNUMREG_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSMCODIVA_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSMCODIVA_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SMCAUCON
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_SMCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE,CCTIPDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_SMCAUCON))
          select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE,CCTIPDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_SMCAUCON)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_SMCAUCON)+"%");

            select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SMCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oSMCAUCON_2_1'),i_cWhere,'GSCG_ACC',"CAUSALI CONTABILI",'GSAI_MSM.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE,CCTIPDOC";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_SMCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_SMCAUCON)
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLRIFE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(35))
      this.w_CCTIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_CCFLRIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_CCTIPDOC = NVL(_Link_.CCTIPDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SMCAUCON = space(5)
      endif
      this.w_CCDESCRI = space(35)
      this.w_CCTIPREG = space(1)
      this.w_CCFLRIFE = space(1)
      this.w_CCTIPDOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NVL(.w_CCTIPREG,'N') $ 'AV' AND (.w_CCTIPDOC='FA' OR .w_CCTIPDOC='FE' OR .w_CCTIPDOC='AU')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SMCAUCON = space(5)
        this.w_CCDESCRI = space(35)
        this.w_CCTIPREG = space(1)
        this.w_CCFLRIFE = space(1)
        this.w_CCTIPDOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CCCODICE as CCCODICE201"+ ",link_2_1.CCDESCRI as CCDESCRI201"+ ",link_2_1.CCTIPREG as CCTIPREG201"+ ",link_2_1.CCFLRIFE as CCFLRIFE201"+ ",link_2_1.CCTIPDOC as CCTIPDOC201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on DATPIVSM.SMCAUCON=link_2_1.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and DATPIVSM.SMCAUCON=link_2_1.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SMCODIVA
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_SMCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_SMCODIVA))
          select IVCODIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oSMCODIVA_2_5'),i_cWhere,'GSAR_AIV',"CODICI IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_SMCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_SMCODIVA)
            select IVCODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMCODIVA = NVL(_Link_.IVCODIVA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SMCODIVA = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCAUCON_2_1.value==this.w_SMCAUCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCAUCON_2_1.value=this.w_SMCAUCON
      replace t_SMCAUCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCAUCON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_2.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_2.value=this.w_CCDESCRI
      replace t_CCDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMTIPREG_2_3.RadioValue()==this.w_SMTIPREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMTIPREG_2_3.SetRadio()
      replace t_SMTIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMTIPREG_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMNUMREG_2_4.value==this.w_SMNUMREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMNUMREG_2_4.value=this.w_SMNUMREG
      replace t_SMNUMREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMNUMREG_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCODIVA_2_5.value==this.w_SMCODIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCODIVA_2_5.value=this.w_SMCODIVA
      replace t_SMCODIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCODIVA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCONDIZ_2_6.RadioValue()==this.w_SMCONDIZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCONDIZ_2_6.SetRadio()
      replace t_SMCONDIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCONDIZ_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'DATPIVSM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(NVL(.w_CCTIPREG,'N') $ 'AV' AND (.w_CCTIPDOC='FA' OR .w_CCTIPDOC='FE' OR .w_CCTIPDOC='AU')) and not(empty(.w_SMCAUCON)) and (not Empty(.w_SMCAUCON) Or iif(vartype(.w_SMTIPREG)='N',iif(.w_SMTIPREG=1,'N','X'),.w_SMTIPREG)<>'N' Or not Empty(.w_SMCODIVA))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCAUCON_2_1
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(iif(.w_SMNUMREG<>0, iif(isahe(),controllonr(.w_SMTIPREG,.w_SMNUMREG),CHKREGIV(.w_SMTIPREG,.w_SMNUMREG)=1), .t.)) and (.w_SMTIPREG $ 'VAC') and (not Empty(.w_SMCAUCON) Or iif(vartype(.w_SMTIPREG)='N',iif(.w_SMTIPREG=1,'N','X'),.w_SMTIPREG)<>'N' Or not Empty(.w_SMCODIVA))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMNUMREG_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il numero di registro inserito non esiste")
      endcase
      if not Empty(.w_SMCAUCON) Or iif(vartype(.w_SMTIPREG)='N',iif(.w_SMTIPREG=1,'N','X'),.w_SMTIPREG)<>'N' Or not Empty(.w_SMCODIVA)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SMTIPREG = this.w_SMTIPREG
    this.o_SMCONDIZ = this.w_SMCONDIZ
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not Empty(t_SMCAUCON) Or iif(vartype(t_SMTIPREG)='N',iif(t_SMTIPREG=1,'N','X'),t_SMTIPREG)<>'N' Or not Empty(t_SMCODIVA))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_SMCAUCON=space(5)
      .w_CCDESCRI=space(35)
      .w_SMTIPREG=space(1)
      .w_SMNUMREG=0
      .w_SMCODIVA=space(5)
      .w_SMCONDIZ=space(1)
      .w_CCTIPREG=space(1)
      .w_CCFLRIFE=space(1)
      .w_CCTIPDOC=space(2)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_SMCAUCON))
        .link_2_1('Full')
      endif
      .DoRTCalc(3,3,.f.)
        .w_SMTIPREG = 'N'
        .w_SMNUMREG = 0
        .w_SMCODIVA = iif(.w_Smcondiz='B',Space(5),.w_Smcodiva)
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_SMCODIVA))
        .link_2_5('Full')
      endif
        .w_SMCONDIZ = 'I'
    endwith
    this.DoRTCalc(8,11,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_SMCAUCON = t_SMCAUCON
    this.w_CCDESCRI = t_CCDESCRI
    this.w_SMTIPREG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMTIPREG_2_3.RadioValue(.t.)
    this.w_SMNUMREG = t_SMNUMREG
    this.w_SMCODIVA = t_SMCODIVA
    this.w_SMCONDIZ = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCONDIZ_2_6.RadioValue(.t.)
    this.w_CCTIPREG = t_CCTIPREG
    this.w_CCFLRIFE = t_CCFLRIFE
    this.w_CCTIPDOC = t_CCTIPDOC
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_SMCAUCON with this.w_SMCAUCON
    replace t_CCDESCRI with this.w_CCDESCRI
    replace t_SMTIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMTIPREG_2_3.ToRadio()
    replace t_SMNUMREG with this.w_SMNUMREG
    replace t_SMCODIVA with this.w_SMCODIVA
    replace t_SMCONDIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSMCONDIZ_2_6.ToRadio()
    replace t_CCTIPREG with this.w_CCTIPREG
    replace t_CCFLRIFE with this.w_CCFLRIFE
    replace t_CCTIPDOC with this.w_CCTIPDOC
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsai_msmPag1 as StdContainer
  Width  = 652
  height = 416
  stdWidth  = 652
  stdheight = 416
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=5, width=638,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="SMCAUCON",Label1="Causale",Field2="CCDESCRI",Label2="Descrizione",Field3="SMTIPREG",Label3="Tipo registro",Field4="SMNUMREG",Label4="N.reg.",Field5="SMCODIVA",Label5="Cod. IVA",Field6="SMCONDIZ",Label6="Condizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49470854

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=28,;
    width=634+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=29,width=633+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CAU_CONT|VOCIIVA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CAU_CONT'
        oDropInto=this.oBodyCol.oRow.oSMCAUCON_2_1
      case cFile='VOCIIVA'
        oDropInto=this.oBodyCol.oRow.oSMCODIVA_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsai_msmBodyRow as CPBodyRowCnt
  Width=624
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oSMCAUCON_2_1 as StdTrsField with uid="XSUIXHGZBK",rtseq=2,rtrep=.t.,;
    cFormVar="w_SMCAUCON",value=space(5),;
    ToolTipText = "Causale contabile",;
    HelpContextID = 221545612,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_SMCAUCON"

  func oSMCAUCON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMCAUCON_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSMCAUCON_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oSMCAUCON_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"CAUSALI CONTABILI",'GSAI_MSM.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oSMCAUCON_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_SMCAUCON
    i_obj.ecpSave()
  endproc

  add object oCCDESCRI_2_2 as StdTrsField with uid="CDIJUAKKGX",rtseq=3,rtrep=.t.,;
    cFormVar="w_CCDESCRI",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione causale",;
    HelpContextID = 223379345,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=258, Left=65, Top=0, InputMask=replicate('X',35)

  add object oSMTIPREG_2_3 as StdTrsCombo with uid="BXXQXWIDLA",rtrep=.t.,;
    cFormVar="w_SMTIPREG", RowSource=""+"Nessun filtro,"+"Vendite,"+"Acquisti" , ;
    ToolTipText = "Tipo registro",;
    HelpContextID = 242971795,;
    Height=22, Width=102, Left=329, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSMTIPREG_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SMTIPREG,&i_cF..t_SMTIPREG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'V',;
    iif(xVal =3,'A',;
    space(1)))))
  endfunc
  func oSMTIPREG_2_3.GetRadio()
    this.Parent.oContained.w_SMTIPREG = this.RadioValue()
    return .t.
  endfunc

  func oSMTIPREG_2_3.ToRadio()
    this.Parent.oContained.w_SMTIPREG=trim(this.Parent.oContained.w_SMTIPREG)
    return(;
      iif(this.Parent.oContained.w_SMTIPREG=='N',1,;
      iif(this.Parent.oContained.w_SMTIPREG=='V',2,;
      iif(this.Parent.oContained.w_SMTIPREG=='A',3,;
      0))))
  endfunc

  func oSMTIPREG_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oSMNUMREG_2_4 as StdTrsField with uid="UEMHAMRFNW",rtseq=5,rtrep=.t.,;
    cFormVar="w_SMNUMREG",value=0,;
    ToolTipText = "Numero registro",;
    HelpContextID = 245355667,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il numero di registro inserito non esiste",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=437, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oSMNUMREG_2_4.mCond()
    with this.Parent.oContained
      return (.w_SMTIPREG $ 'VAC')
    endwith
  endfunc

  func oSMNUMREG_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_SMNUMREG<>0, iif(isahe(),controllonr(.w_SMTIPREG,.w_SMNUMREG),CHKREGIV(.w_SMTIPREG,.w_SMNUMREG)=1), .t.))
    endwith
    return bRes
  endfunc

  add object oSMCODIVA_2_5 as StdTrsField with uid="IUFAPDQDPD",rtseq=6,rtrep=.t.,;
    cFormVar="w_SMCODIVA",value=space(5),;
    ToolTipText = "Codice IVA",;
    HelpContextID = 137790617,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=476, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_SMCODIVA"

  func oSMCODIVA_2_5.mCond()
    with this.Parent.oContained
      return (.w_Smcondiz $ 'IE')
    endwith
  endfunc

  func oSMCODIVA_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMCODIVA_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSMCODIVA_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oSMCODIVA_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"CODICI IVA",'',this.parent.oContained
  endproc
  proc oSMCODIVA_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_SMCODIVA
    i_obj.ecpSave()
  endproc

  add object oSMCONDIZ_2_6 as StdTrsCombo with uid="OYKLTNRZIB",rtrep=.t.,;
    cFormVar="w_SMCONDIZ", RowSource=""+"Includi,"+"Escludi" , ;
    ToolTipText = "Condizione di estrazione",;
    HelpContextID = 57244544,;
    Height=22, Width=72, Left=547, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSMCONDIZ_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SMCONDIZ,&i_cF..t_SMCONDIZ),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'E',;
    space(1))))
  endfunc
  func oSMCONDIZ_2_6.GetRadio()
    this.Parent.oContained.w_SMCONDIZ = this.RadioValue()
    return .t.
  endfunc

  func oSMCONDIZ_2_6.ToRadio()
    this.Parent.oContained.w_SMCONDIZ=trim(this.Parent.oContained.w_SMCONDIZ)
    return(;
      iif(this.Parent.oContained.w_SMCONDIZ=='I',1,;
      iif(this.Parent.oContained.w_SMCONDIZ=='E',2,;
      0)))
  endfunc

  func oSMCONDIZ_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oSMCAUCON_2_1.When()
    return(.t.)
  proc oSMCAUCON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oSMCAUCON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_msm','DATPIVSM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SM__ANNO=DATPIVSM.SM__ANNO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
