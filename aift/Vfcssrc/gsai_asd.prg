* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_asd                                                        *
*              Parametri di estrazione spesometro                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-27                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_asd"))

* --- Class definition
define class tgsai_asd as StdForm
  Top    = 4
  Left   = 8

  * --- Standard Properties
  Width  = 850
  Height = 459+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=109709161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  DATPARSP_IDX = 0
  cFile = "DATPARSP"
  cKeySelect = "PE__ANNO"
  cKeyWhere  = "PE__ANNO=this.w_PE__ANNO"
  cKeyWhereODBC = '"PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';

  cKeyWhereODBCqualified = '"DATPARSP.PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';

  cPrg = "gsai_asd"
  cComment = "Parametri di estrazione spesometro"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PE__ANNO = space(4)
  o_PE__ANNO = space(4)
  w_AT__ANNO = 0
  w_PESEPARA = space(1)
  w_PEDATINI = ctod('  /  /  ')
  w_PEDATFIN = ctod('  /  /  ')
  w_PEDATVAR = ctod('  /  /  ')
  w_PEAUTRAS = space(1)
  o_PEAUTRAS = space(1)
  w_PEDATINA = ctod('  /  /  ')
  w_PEDATFIA = ctod('  /  /  ')
  w_PEIMPSCH = 0
  w_PEREGPNT = space(1)
  w_PESOGPRI = space(1)
  w_PEFATCOR = space(1)
  w_PESCLIMP = space(1)
  o_PESCLIMP = space(1)
  w_PEIMPMIN = space(1)
  o_PEIMPMIN = space(1)
  w_PEIMPMAX = space(1)
  w_PECONIMP = space(1)
  w_PEIVANOS = space(1)
  w_RESCHK = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Children pointers
  GSAI_MSI = .NULL.
  GSAI_MSC = .NULL.
  GSAI_MSS = .NULL.
  GSAI_MSF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DATPARSP','gsai_asd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_asdPag1","gsai_asd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri generali")
      .Pages(1).HelpContextID = 235239911
      .Pages(2).addobject("oPag","tgsai_asdPag2","gsai_asd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri dati IVA")
      .Pages(2).HelpContextID = 162124639
      .Pages(3).addobject("oPag","tgsai_asdPag3","gsai_asd",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Parametri causali note di variazione")
      .Pages(3).HelpContextID = 32458254
      .Pages(4).addobject("oPag","tgsai_asdPag4","gsai_asd",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Parametri schede carburante")
      .Pages(4).HelpContextID = 46655842
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPE__ANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DATPARSP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATPARSP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATPARSP_IDX,3]
  return

  function CreateChildren()
    this.GSAI_MSI = CREATEOBJECT('stdDynamicChild',this,'GSAI_MSI',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSAI_MSC = CREATEOBJECT('stdDynamicChild',this,'GSAI_MSC',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSAI_MSS = CREATEOBJECT('stdDynamicChild',this,'GSAI_MSS',this.oPgFrm.Page4.oPag.oLinkPC_4_1)
    this.GSAI_MSF = CREATEOBJECT('stdDynamicChild',this,'GSAI_MSF',this.oPgFrm.Page4.oPag.oLinkPC_4_2)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAI_MSI)
      this.GSAI_MSI.DestroyChildrenChain()
      this.GSAI_MSI=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    if !ISNULL(this.GSAI_MSC)
      this.GSAI_MSC.DestroyChildrenChain()
      this.GSAI_MSC=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    if !ISNULL(this.GSAI_MSS)
      this.GSAI_MSS.DestroyChildrenChain()
      this.GSAI_MSS=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_1')
    if !ISNULL(this.GSAI_MSF)
      this.GSAI_MSF.DestroyChildrenChain()
      this.GSAI_MSF=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_2')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAI_MSI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAI_MSC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAI_MSS.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAI_MSF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAI_MSI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAI_MSC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAI_MSS.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAI_MSF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAI_MSI.NewDocument()
    this.GSAI_MSC.NewDocument()
    this.GSAI_MSS.NewDocument()
    this.GSAI_MSF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAI_MSI.SetKey(;
            .w_PE__ANNO,"PE__ANNO";
            )
      this.GSAI_MSC.SetKey(;
            .w_PE__ANNO,"PE__ANNO";
            )
      this.GSAI_MSS.SetKey(;
            .w_PE__ANNO,"SC__ANNO";
            )
      this.GSAI_MSF.SetKey(;
            .w_PE__ANNO,"SC__ANNO";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAI_MSI.ChangeRow(this.cRowID+'      1',1;
             ,.w_PE__ANNO,"PE__ANNO";
             )
      .GSAI_MSC.ChangeRow(this.cRowID+'      1',1;
             ,.w_PE__ANNO,"PE__ANNO";
             )
      .GSAI_MSS.ChangeRow(this.cRowID+'      1',1;
             ,.w_PE__ANNO,"SC__ANNO";
             )
      .GSAI_MSF.ChangeRow(this.cRowID+'      1',1;
             ,.w_PE__ANNO,"SC__ANNO";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAI_MSI)
        i_f=.GSAI_MSI.BuildFilter()
        if !(i_f==.GSAI_MSI.cQueryFilter)
          i_fnidx=.GSAI_MSI.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MSI.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MSI.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MSI.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MSI.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAI_MSC)
        i_f=.GSAI_MSC.BuildFilter()
        if !(i_f==.GSAI_MSC.cQueryFilter)
          i_fnidx=.GSAI_MSC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MSC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MSC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MSC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MSC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAI_MSS)
        i_f=.GSAI_MSS.BuildFilter()
        if !(i_f==.GSAI_MSS.cQueryFilter)
          i_fnidx=.GSAI_MSS.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MSS.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MSS.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MSS.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MSS.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAI_MSF)
        i_f=.GSAI_MSF.BuildFilter()
        if !(i_f==.GSAI_MSF.cQueryFilter)
          i_fnidx=.GSAI_MSF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAI_MSF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAI_MSF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAI_MSF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAI_MSF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PE__ANNO = NVL(PE__ANNO,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DATPARSP where PE__ANNO=KeySet.PE__ANNO
    *
    i_nConn = i_TableProp[this.DATPARSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATPARSP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATPARSP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATPARSP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RESCHK = 0
        .w_PE__ANNO = NVL(PE__ANNO,space(4))
        .w_AT__ANNO = Int(Val(.w_Pe__Anno))
        .w_PESEPARA = NVL(PESEPARA,space(1))
        .w_PEDATINI = NVL(cp_ToDate(PEDATINI),ctod("  /  /  "))
        .w_PEDATFIN = NVL(cp_ToDate(PEDATFIN),ctod("  /  /  "))
        .w_PEDATVAR = NVL(cp_ToDate(PEDATVAR),ctod("  /  /  "))
        .w_PEAUTRAS = NVL(PEAUTRAS,space(1))
        .w_PEDATINA = NVL(cp_ToDate(PEDATINA),ctod("  /  /  "))
        .w_PEDATFIA = NVL(cp_ToDate(PEDATFIA),ctod("  /  /  "))
        .w_PEIMPSCH = NVL(PEIMPSCH,0)
        .w_PEREGPNT = NVL(PEREGPNT,space(1))
        .w_PESOGPRI = NVL(PESOGPRI,space(1))
        .w_PEFATCOR = NVL(PEFATCOR,space(1))
        .w_PESCLIMP = NVL(PESCLIMP,space(1))
        .w_PEIMPMIN = NVL(PEIMPMIN,space(1))
        .w_PEIMPMAX = NVL(PEIMPMAX,space(1))
        .w_PECONIMP = NVL(PECONIMP,space(1))
        .w_PEIVANOS = NVL(PEIVANOS,space(1))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(.w_PE__ANNO)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'DATPARSP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PE__ANNO = space(4)
      .w_AT__ANNO = 0
      .w_PESEPARA = space(1)
      .w_PEDATINI = ctod("  /  /  ")
      .w_PEDATFIN = ctod("  /  /  ")
      .w_PEDATVAR = ctod("  /  /  ")
      .w_PEAUTRAS = space(1)
      .w_PEDATINA = ctod("  /  /  ")
      .w_PEDATFIA = ctod("  /  /  ")
      .w_PEIMPSCH = 0
      .w_PEREGPNT = space(1)
      .w_PESOGPRI = space(1)
      .w_PEFATCOR = space(1)
      .w_PESCLIMP = space(1)
      .w_PEIMPMIN = space(1)
      .w_PEIMPMAX = space(1)
      .w_PECONIMP = space(1)
      .w_PEIVANOS = space(1)
      .w_RESCHK = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_AT__ANNO = Int(Val(.w_Pe__Anno))
          .DoRTCalc(3,3,.f.)
        .w_PEDATINI = IIF(EMPTY(.w_PE__ANNO),CTOD('  -  -    '),CTOD('01-01-'+.w_PE__ANNO))
        .w_PEDATFIN = IIF(EMPTY(.w_PE__ANNO),CTOD('  -  -    '),CTOD('31-12-'+.w_PE__ANNO))
        .w_PEDATVAR = .w_PEDATFIN
        .w_PEAUTRAS = 'N'
          .DoRTCalc(8,10,.f.)
        .w_PEREGPNT = 'N'
        .w_PESOGPRI = 'S'
        .w_PEFATCOR = 'S'
        .w_PESCLIMP = 'N'
        .w_PEIMPMIN = 'N'
        .w_PEIMPMAX = 'N'
        .w_PECONIMP = 'N'
        .w_PEIVANOS = 'N'
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(.w_PE__ANNO)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .w_RESCHK = 0
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATPARSP')
    this.DoRTCalc(20,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPE__ANNO_1_2.enabled = i_bVal
      .Page1.oPag.oPESEPARA_1_4.enabled = i_bVal
      .Page1.oPag.oPEDATINI_1_6.enabled = i_bVal
      .Page1.oPag.oPEDATFIN_1_8.enabled = i_bVal
      .Page1.oPag.oPEAUTRAS_1_11.enabled = i_bVal
      .Page1.oPag.oPEDATINA_1_12.enabled = i_bVal
      .Page1.oPag.oPEDATFIA_1_13.enabled = i_bVal
      .Page4.oPag.oPEIMPSCH_4_4.enabled = i_bVal
      .Page1.oPag.oPESOGPRI_1_15.enabled = i_bVal
      .Page1.oPag.oPEFATCOR_1_16.enabled = i_bVal
      .Page1.oPag.oPESCLIMP_1_17.enabled = i_bVal
      .Page1.oPag.oPEIMPMIN_1_18.enabled = i_bVal
      .Page1.oPag.oPEIMPMAX_1_19.enabled = i_bVal
      .Page1.oPag.oPECONIMP_1_20.enabled = i_bVal
      .Page1.oPag.oPEIVANOS_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      .Page1.oPag.oObj_1_25.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPE__ANNO_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPE__ANNO_1_2.enabled = .t.
      endif
    endwith
    this.GSAI_MSI.SetStatus(i_cOp)
    this.GSAI_MSC.SetStatus(i_cOp)
    this.GSAI_MSS.SetStatus(i_cOp)
    this.GSAI_MSF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DATPARSP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAI_MSI.SetChildrenStatus(i_cOp)
  *  this.GSAI_MSC.SetChildrenStatus(i_cOp)
  *  this.GSAI_MSS.SetChildrenStatus(i_cOp)
  *  this.GSAI_MSF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATPARSP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PE__ANNO,"PE__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PESEPARA,"PESEPARA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEDATINI,"PEDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEDATFIN,"PEDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEDATVAR,"PEDATVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEAUTRAS,"PEAUTRAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEDATINA,"PEDATINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEDATFIA,"PEDATFIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEIMPSCH,"PEIMPSCH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEREGPNT,"PEREGPNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PESOGPRI,"PESOGPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEFATCOR,"PEFATCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PESCLIMP,"PESCLIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEIMPMIN,"PEIMPMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEIMPMAX,"PEIMPMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PECONIMP,"PECONIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEIVANOS,"PEIVANOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATPARSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2])
    i_lTable = "DATPARSP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DATPARSP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATPARSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DATPARSP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DATPARSP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATPARSP')
        i_extval=cp_InsertValODBCExtFlds(this,'DATPARSP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PE__ANNO,PESEPARA,PEDATINI,PEDATFIN,PEDATVAR"+;
                  ",PEAUTRAS,PEDATINA,PEDATFIA,PEIMPSCH,PEREGPNT"+;
                  ",PESOGPRI,PEFATCOR,PESCLIMP,PEIMPMIN,PEIMPMAX"+;
                  ",PECONIMP,PEIVANOS,UTCC,UTCV,UTDC"+;
                  ",UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PE__ANNO)+;
                  ","+cp_ToStrODBC(this.w_PESEPARA)+;
                  ","+cp_ToStrODBC(this.w_PEDATINI)+;
                  ","+cp_ToStrODBC(this.w_PEDATFIN)+;
                  ","+cp_ToStrODBC(this.w_PEDATVAR)+;
                  ","+cp_ToStrODBC(this.w_PEAUTRAS)+;
                  ","+cp_ToStrODBC(this.w_PEDATINA)+;
                  ","+cp_ToStrODBC(this.w_PEDATFIA)+;
                  ","+cp_ToStrODBC(this.w_PEIMPSCH)+;
                  ","+cp_ToStrODBC(this.w_PEREGPNT)+;
                  ","+cp_ToStrODBC(this.w_PESOGPRI)+;
                  ","+cp_ToStrODBC(this.w_PEFATCOR)+;
                  ","+cp_ToStrODBC(this.w_PESCLIMP)+;
                  ","+cp_ToStrODBC(this.w_PEIMPMIN)+;
                  ","+cp_ToStrODBC(this.w_PEIMPMAX)+;
                  ","+cp_ToStrODBC(this.w_PECONIMP)+;
                  ","+cp_ToStrODBC(this.w_PEIVANOS)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATPARSP')
        i_extval=cp_InsertValVFPExtFlds(this,'DATPARSP')
        cp_CheckDeletedKey(i_cTable,0,'PE__ANNO',this.w_PE__ANNO)
        INSERT INTO (i_cTable);
              (PE__ANNO,PESEPARA,PEDATINI,PEDATFIN,PEDATVAR,PEAUTRAS,PEDATINA,PEDATFIA,PEIMPSCH,PEREGPNT,PESOGPRI,PEFATCOR,PESCLIMP,PEIMPMIN,PEIMPMAX,PECONIMP,PEIVANOS,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PE__ANNO;
                  ,this.w_PESEPARA;
                  ,this.w_PEDATINI;
                  ,this.w_PEDATFIN;
                  ,this.w_PEDATVAR;
                  ,this.w_PEAUTRAS;
                  ,this.w_PEDATINA;
                  ,this.w_PEDATFIA;
                  ,this.w_PEIMPSCH;
                  ,this.w_PEREGPNT;
                  ,this.w_PESOGPRI;
                  ,this.w_PEFATCOR;
                  ,this.w_PESCLIMP;
                  ,this.w_PEIMPMIN;
                  ,this.w_PEIMPMAX;
                  ,this.w_PECONIMP;
                  ,this.w_PEIVANOS;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DATPARSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DATPARSP_IDX,i_nConn)
      *
      * update DATPARSP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DATPARSP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PESEPARA="+cp_ToStrODBC(this.w_PESEPARA)+;
             ",PEDATINI="+cp_ToStrODBC(this.w_PEDATINI)+;
             ",PEDATFIN="+cp_ToStrODBC(this.w_PEDATFIN)+;
             ",PEDATVAR="+cp_ToStrODBC(this.w_PEDATVAR)+;
             ",PEAUTRAS="+cp_ToStrODBC(this.w_PEAUTRAS)+;
             ",PEDATINA="+cp_ToStrODBC(this.w_PEDATINA)+;
             ",PEDATFIA="+cp_ToStrODBC(this.w_PEDATFIA)+;
             ",PEIMPSCH="+cp_ToStrODBC(this.w_PEIMPSCH)+;
             ",PEREGPNT="+cp_ToStrODBC(this.w_PEREGPNT)+;
             ",PESOGPRI="+cp_ToStrODBC(this.w_PESOGPRI)+;
             ",PEFATCOR="+cp_ToStrODBC(this.w_PEFATCOR)+;
             ",PESCLIMP="+cp_ToStrODBC(this.w_PESCLIMP)+;
             ",PEIMPMIN="+cp_ToStrODBC(this.w_PEIMPMIN)+;
             ",PEIMPMAX="+cp_ToStrODBC(this.w_PEIMPMAX)+;
             ",PECONIMP="+cp_ToStrODBC(this.w_PECONIMP)+;
             ",PEIVANOS="+cp_ToStrODBC(this.w_PEIVANOS)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DATPARSP')
        i_cWhere = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  )
        UPDATE (i_cTable) SET;
              PESEPARA=this.w_PESEPARA;
             ,PEDATINI=this.w_PEDATINI;
             ,PEDATFIN=this.w_PEDATFIN;
             ,PEDATVAR=this.w_PEDATVAR;
             ,PEAUTRAS=this.w_PEAUTRAS;
             ,PEDATINA=this.w_PEDATINA;
             ,PEDATFIA=this.w_PEDATFIA;
             ,PEIMPSCH=this.w_PEIMPSCH;
             ,PEREGPNT=this.w_PEREGPNT;
             ,PESOGPRI=this.w_PESOGPRI;
             ,PEFATCOR=this.w_PEFATCOR;
             ,PESCLIMP=this.w_PESCLIMP;
             ,PEIMPMIN=this.w_PEIMPMIN;
             ,PEIMPMAX=this.w_PEIMPMAX;
             ,PECONIMP=this.w_PECONIMP;
             ,PEIVANOS=this.w_PEIVANOS;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAI_MSI : Saving
      this.GSAI_MSI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PE__ANNO,"PE__ANNO";
             )
      this.GSAI_MSI.mReplace()
      * --- GSAI_MSC : Saving
      this.GSAI_MSC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PE__ANNO,"PE__ANNO";
             )
      this.GSAI_MSC.mReplace()
      * --- GSAI_MSS : Saving
      this.GSAI_MSS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PE__ANNO,"SC__ANNO";
             )
      this.GSAI_MSS.mReplace()
      * --- GSAI_MSF : Saving
      this.GSAI_MSF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PE__ANNO,"SC__ANNO";
             )
      this.GSAI_MSF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAI_MSI : Deleting
    this.GSAI_MSI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PE__ANNO,"PE__ANNO";
           )
    this.GSAI_MSI.mDelete()
    * --- GSAI_MSC : Deleting
    this.GSAI_MSC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PE__ANNO,"PE__ANNO";
           )
    this.GSAI_MSC.mDelete()
    * --- GSAI_MSS : Deleting
    this.GSAI_MSS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PE__ANNO,"SC__ANNO";
           )
    this.GSAI_MSS.mDelete()
    * --- GSAI_MSF : Deleting
    this.GSAI_MSF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PE__ANNO,"SC__ANNO";
           )
    this.GSAI_MSF.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATPARSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DATPARSP_IDX,i_nConn)
      *
      * delete DATPARSP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATPARSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATPARSP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_PE__ANNO<>.w_PE__ANNO
            .w_AT__ANNO = Int(Val(.w_Pe__Anno))
        endif
        .DoRTCalc(3,3,.t.)
        if .o_PE__ANNO<>.w_PE__ANNO
            .w_PEDATINI = IIF(EMPTY(.w_PE__ANNO),CTOD('  -  -    '),CTOD('01-01-'+.w_PE__ANNO))
        endif
        if .o_PE__ANNO<>.w_PE__ANNO
            .w_PEDATFIN = IIF(EMPTY(.w_PE__ANNO),CTOD('  -  -    '),CTOD('31-12-'+.w_PE__ANNO))
        endif
            .w_PEDATVAR = .w_PEDATFIN
        .DoRTCalc(7,15,.t.)
        if .o_PEIMPMIN<>.w_PEIMPMIN.or. .o_PESCLIMP<>.w_PESCLIMP
            .w_PEIMPMAX = 'N'
        endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(.w_PE__ANNO)
        if .o_PE__ANNO<>.w_PE__ANNO.or. .o_PEAUTRAS<>.w_PEAUTRAS
          .Calculate_HWSSNUEFOD()
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(.w_PE__ANNO)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
    endwith
  return

  proc Calculate_HWSSNUEFOD()
    with this
          * --- w_Pe__Anno Changed,w_Peautras Changed
          .w_PEDATINA = iif(.w_Peautras='S',Cp_CharToDate('01-10-'+Str(Val(.w_Pe__Anno)-1,4,0)),Cp_CharToDate('  -  -    '))
          .w_PEDATFIA = iif(.w_Peautras='S',Cp_CharToDate('30-09-'+.w_Pe__Anno),Cp_CharToDate('  -  -    '))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPESCLIMP_1_17.enabled = this.oPgFrm.Page1.oPag.oPESCLIMP_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPEIMPMIN_1_18.enabled = this.oPgFrm.Page1.oPag.oPEIMPMIN_1_18.mCond()
    this.oPgFrm.Page1.oPag.oPEIMPMAX_1_19.enabled = this.oPgFrm.Page1.oPag.oPEIMPMAX_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPEAUTRAS_1_11.visible=!this.oPgFrm.Page1.oPag.oPEAUTRAS_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPEDATINA_1_12.visible=!this.oPgFrm.Page1.oPag.oPEDATINA_1_12.mHide()
    this.oPgFrm.Page1.oPag.oPEDATFIA_1_13.visible=!this.oPgFrm.Page1.oPag.oPEDATFIA_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPE__ANNO_1_2.value==this.w_PE__ANNO)
      this.oPgFrm.Page1.oPag.oPE__ANNO_1_2.value=this.w_PE__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPESEPARA_1_4.value==this.w_PESEPARA)
      this.oPgFrm.Page1.oPag.oPESEPARA_1_4.value=this.w_PESEPARA
    endif
    if not(this.oPgFrm.Page1.oPag.oPEDATINI_1_6.value==this.w_PEDATINI)
      this.oPgFrm.Page1.oPag.oPEDATINI_1_6.value=this.w_PEDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEDATFIN_1_8.value==this.w_PEDATFIN)
      this.oPgFrm.Page1.oPag.oPEDATFIN_1_8.value=this.w_PEDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPEAUTRAS_1_11.RadioValue()==this.w_PEAUTRAS)
      this.oPgFrm.Page1.oPag.oPEAUTRAS_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPEDATINA_1_12.value==this.w_PEDATINA)
      this.oPgFrm.Page1.oPag.oPEDATINA_1_12.value=this.w_PEDATINA
    endif
    if not(this.oPgFrm.Page1.oPag.oPEDATFIA_1_13.value==this.w_PEDATFIA)
      this.oPgFrm.Page1.oPag.oPEDATFIA_1_13.value=this.w_PEDATFIA
    endif
    if not(this.oPgFrm.Page4.oPag.oPEIMPSCH_4_4.value==this.w_PEIMPSCH)
      this.oPgFrm.Page4.oPag.oPEIMPSCH_4_4.value=this.w_PEIMPSCH
    endif
    if not(this.oPgFrm.Page1.oPag.oPESOGPRI_1_15.RadioValue()==this.w_PESOGPRI)
      this.oPgFrm.Page1.oPag.oPESOGPRI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPEFATCOR_1_16.RadioValue()==this.w_PEFATCOR)
      this.oPgFrm.Page1.oPag.oPEFATCOR_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPESCLIMP_1_17.RadioValue()==this.w_PESCLIMP)
      this.oPgFrm.Page1.oPag.oPESCLIMP_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPEIMPMIN_1_18.RadioValue()==this.w_PEIMPMIN)
      this.oPgFrm.Page1.oPag.oPEIMPMIN_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPEIMPMAX_1_19.RadioValue()==this.w_PEIMPMAX)
      this.oPgFrm.Page1.oPag.oPEIMPMAX_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPECONIMP_1_20.RadioValue()==this.w_PECONIMP)
      this.oPgFrm.Page1.oPag.oPECONIMP_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPEIVANOS_1_21.RadioValue()==this.w_PEIVANOS)
      this.oPgFrm.Page1.oPag.oPEIVANOS_1_21.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DATPARSP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PE__ANNO)) or not(Val(.w_PE__ANNO)>2005 and Val(.w_PE__ANNO)<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPE__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_PE__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PEDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEDATINI_1_6.SetFocus()
            i_bnoObbl = !empty(.w_PEDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PEDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEDATFIN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PEDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PEDATINA)) or not(.w_PEDATINA<=.w_PEDATFIA))  and not(.w_PEAUTRAS ='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEDATINA_1_12.SetFocus()
            i_bnoObbl = !empty(.w_PEDATINA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PEDATFIA)) or not(.w_PEDATFIA>=.w_PEDATINA))  and not(.w_PEAUTRAS ='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEDATFIA_1_13.SetFocus()
            i_bnoObbl = !empty(.w_PEDATFIA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAI_MSI.CheckForm()
      if i_bres
        i_bres=  .GSAI_MSI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSAI_MSC.CheckForm()
      if i_bres
        i_bres=  .GSAI_MSC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSAI_MSS.CheckForm()
      if i_bres
        i_bres=  .GSAI_MSS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSAI_MSF.CheckForm()
      if i_bres
        i_bres=  .GSAI_MSF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsai_asd
      IF i_bRes And .w_PEAUTRAS='S'
          .w_RESCHK=0
          this.NotifyEvent('ControlliFinali')
           Wait Clear
      		if .w_RESCHK<>0
      			i_bRes=.f.
      		endif
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PE__ANNO = this.w_PE__ANNO
    this.o_PEAUTRAS = this.w_PEAUTRAS
    this.o_PESCLIMP = this.w_PESCLIMP
    this.o_PEIMPMIN = this.w_PEIMPMIN
    * --- GSAI_MSI : Depends On
    this.GSAI_MSI.SaveDependsOn()
    * --- GSAI_MSC : Depends On
    this.GSAI_MSC.SaveDependsOn()
    * --- GSAI_MSS : Depends On
    this.GSAI_MSS.SaveDependsOn()
    * --- GSAI_MSF : Depends On
    this.GSAI_MSF.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsai_asdPag1 as StdContainer
  Width  = 846
  height = 459
  stdWidth  = 846
  stdheight = 459
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPE__ANNO_1_2 as StdField with uid="IGYXCNZHDV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PE__ANNO", cQueryName = "PE__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di adempimento",;
    HelpContextID = 68471995,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=158, Top=18, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oPE__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_PE__ANNO)>2005 and Val(.w_PE__ANNO)<2100)
    endwith
    return bRes
  endfunc

  add object oPESEPARA_1_4 as StdField with uid="AYXOTFSTCA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PESEPARA", cQueryName = "PESEPARA",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Separatore",;
    HelpContextID = 264270647,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=429, Top=18, InputMask=replicate('X',1)

  add object oPEDATINI_1_6 as StdField with uid="NBUDVFHDWR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PEDATINI", cQueryName = "PEDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio adempimento",;
    HelpContextID = 134511809,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=158, Top=56

  add object oPEDATFIN_1_8 as StdField with uid="YXCOCVUCSK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PEDATFIN", cQueryName = "PEDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine adempimento",;
    HelpContextID = 184843452,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=439, Top=56

  add object oPEAUTRAS_1_11 as StdCheck with uid="GXUGYEIOQQ",rtseq=7,rtrep=.f.,left=158, top=96, caption="Autotrasportatori",;
    ToolTipText = "Se attivo, la data di registrazione delle fatture e note di credito autotrasportatori viene posticipata alla fine del trimestre successivo",;
    HelpContextID = 17781577,;
    cFormVar="w_PEAUTRAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPEAUTRAS_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPEAUTRAS_1_11.GetRadio()
    this.Parent.oContained.w_PEAUTRAS = this.RadioValue()
    return .t.
  endfunc

  func oPEAUTRAS_1_11.SetRadio()
    this.Parent.oContained.w_PEAUTRAS=trim(this.Parent.oContained.w_PEAUTRAS)
    this.value = ;
      iif(this.Parent.oContained.w_PEAUTRAS=='S',1,;
      0)
  endfunc

  func oPEAUTRAS_1_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oPEDATINA_1_12 as StdField with uid="KNHXYQJYZC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PEDATINA", cQueryName = "PEDATINA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio adempimento per autotrasportatori",;
    HelpContextID = 134511817,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=439, Top=94

  func oPEDATINA_1_12.mHide()
    with this.Parent.oContained
      return (.w_PEAUTRAS ='N')
    endwith
  endfunc

  func oPEDATINA_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PEDATINA<=.w_PEDATFIA)
    endwith
    return bRes
  endfunc

  add object oPEDATFIA_1_13 as StdField with uid="KCRCOQAQIV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PEDATFIA", cQueryName = "PEDATFIA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine adempimento per autotrasportatori",;
    HelpContextID = 184843465,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=693, Top=94

  func oPEDATFIA_1_13.mHide()
    with this.Parent.oContained
      return (.w_PEAUTRAS ='N')
    endwith
  endfunc

  func oPEDATFIA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PEDATFIA>=.w_PEDATINA)
    endwith
    return bRes
  endfunc

  add object oPESOGPRI_1_15 as StdCheck with uid="JNSQHXMDJS",rtseq=12,rtrep=.f.,left=158, top=130, caption="Verifica limite per tipologia operazione",;
    ToolTipText = "Se attivo, l'importo controllato � quello stabilito per la tipologia documento senza tener conto della classificazione intestatario privato/non privato ",;
    HelpContextID = 238711615,;
    cFormVar="w_PESOGPRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPESOGPRI_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPESOGPRI_1_15.GetRadio()
    this.Parent.oContained.w_PESOGPRI = this.RadioValue()
    return .t.
  endfunc

  func oPESOGPRI_1_15.SetRadio()
    this.Parent.oContained.w_PESOGPRI=trim(this.Parent.oContained.w_PESOGPRI)
    this.value = ;
      iif(this.Parent.oContained.w_PESOGPRI=='S',1,;
      0)
  endfunc

  add object oPEFATCOR_1_16 as StdCheck with uid="JIHWOQEUHO",rtseq=13,rtrep=.f.,left=439, top=130, caption="Estrae fatture corrispettivi a saldo a zero",;
    ToolTipText = "Se attivo, per le fatture comprese nei corrispettivi la procedura estrae solo le righe con importo positivo se il castelletto iva ha saldo a zero",;
    HelpContextID = 235166904,;
    cFormVar="w_PEFATCOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPEFATCOR_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPEFATCOR_1_16.GetRadio()
    this.Parent.oContained.w_PEFATCOR = this.RadioValue()
    return .t.
  endfunc

  func oPEFATCOR_1_16.SetRadio()
    this.Parent.oContained.w_PEFATCOR=trim(this.Parent.oContained.w_PEFATCOR)
    this.value = ;
      iif(this.Parent.oContained.w_PEFATCOR=='S',1,;
      0)
  endfunc

  add object oPESCLIMP_1_17 as StdCheck with uid="FDOGWEUJQP",rtseq=14,rtrep=.f.,left=158, top=185, caption="Escludi importi a zero",;
    ToolTipText = "Se attivo, la procedura esclude dalla generazione i records il cui valore di importo/imponibile e imposta troncato risulta a zero",;
    HelpContextID = 142707898,;
    cFormVar="w_PESCLIMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPESCLIMP_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPESCLIMP_1_17.GetRadio()
    this.Parent.oContained.w_PESCLIMP = this.RadioValue()
    return .t.
  endfunc

  func oPESCLIMP_1_17.SetRadio()
    this.Parent.oContained.w_PESCLIMP=trim(this.Parent.oContained.w_PESCLIMP)
    this.value = ;
      iif(this.Parent.oContained.w_PESCLIMP=='S',1,;
      0)
  endfunc

  func oPESCLIMP_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PEIMPMIN='N')
    endwith
   endif
  endfunc

  add object oPEIMPMIN_1_18 as StdCheck with uid="YMEIFQQAID",rtseq=15,rtrep=.f.,left=357, top=185, caption="Valorizza ad intero",;
    ToolTipText = "Se l'importo/imponibile e l'imposta risultano, in valore assoluto, inferiori ad un euro, verranno arrotondati al valore intero (1 o -1)",;
    HelpContextID = 70790332,;
    cFormVar="w_PEIMPMIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPEIMPMIN_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPEIMPMIN_1_18.GetRadio()
    this.Parent.oContained.w_PEIMPMIN = this.RadioValue()
    return .t.
  endfunc

  func oPEIMPMIN_1_18.SetRadio()
    this.Parent.oContained.w_PEIMPMIN=trim(this.Parent.oContained.w_PEIMPMIN)
    this.value = ;
      iif(this.Parent.oContained.w_PEIMPMIN=='S',1,;
      0)
  endfunc

  func oPEIMPMIN_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PESCLIMP='N')
    endwith
   endif
  endfunc

  add object oPEIMPMAX_1_19 as StdCheck with uid="QISLYEVIIM",rtseq=16,rtrep=.f.,left=537, top=185, caption="Valorizza ad intero solo il maggiore",;
    ToolTipText = "Se l'importo/imponibile e l'imposta risultano, in valore assoluto, inferiori ad un euro, verr� arrotondato al valore intero solo il maggiore ",;
    HelpContextID = 197645134,;
    cFormVar="w_PEIMPMAX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPEIMPMAX_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPEIMPMAX_1_19.GetRadio()
    this.Parent.oContained.w_PEIMPMAX = this.RadioValue()
    return .t.
  endfunc

  func oPEIMPMAX_1_19.SetRadio()
    this.Parent.oContained.w_PEIMPMAX=trim(this.Parent.oContained.w_PEIMPMAX)
    this.value = ;
      iif(this.Parent.oContained.w_PEIMPMAX=='S',1,;
      0)
  endfunc

  func oPEIMPMAX_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PESCLIMP='N' And .w_PEIMPMIN='S')
    endwith
   endif
  endfunc

  add object oPECONIMP_1_20 as StdCheck with uid="RZYLXKPXBB",rtseq=17,rtrep=.f.,left=158, top=215, caption="Conferma importo",;
    ToolTipText = "Se attivo, la procedura,  nel caso di note di variazione emesse/ricevute negative di importo in valore assoluto  maggiore di 999999, valorizza il flag �Conferma importo�",;
    HelpContextID = 139889850,;
    cFormVar="w_PECONIMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPECONIMP_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPECONIMP_1_20.GetRadio()
    this.Parent.oContained.w_PECONIMP = this.RadioValue()
    return .t.
  endfunc

  func oPECONIMP_1_20.SetRadio()
    this.Parent.oContained.w_PECONIMP=trim(this.Parent.oContained.w_PECONIMP)
    this.value = ;
      iif(this.Parent.oContained.w_PECONIMP=='S',1,;
      0)
  endfunc

  add object oPEIVANOS_1_21 as StdCheck with uid="DAWWKJOOAQ",rtseq=18,rtrep=.f.,left=357, top=215, caption="Iva non esposta",;
    ToolTipText = "Se attivo, le operazioni con Iva non esposta rappresentano un di cui delle operazioni imponibili, non imponibili ed esenti",;
    HelpContextID = 69151927,;
    cFormVar="w_PEIVANOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPEIVANOS_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPEIVANOS_1_21.GetRadio()
    this.Parent.oContained.w_PEIVANOS = this.RadioValue()
    return .t.
  endfunc

  func oPEIVANOS_1_21.SetRadio()
    this.Parent.oContained.w_PEIVANOS=trim(this.Parent.oContained.w_PEIVANOS)
    this.value = ;
      iif(this.Parent.oContained.w_PEIVANOS=='S',1,;
      0)
  endfunc


  add object oObj_1_23 as cp_zoombox with uid="JXWYSYGEOA",left=2, top=242, width=838,height=216,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="Gsai_Asd",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cTable="GENFILTE",cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    nPag=1;
    , ToolTipText = "Elenco generazioni";
    , HelpContextID = 68288486


  add object oObj_1_25 as cp_runprogram with uid="UTMUKFCDVA",left=75, top=583, width=249,height=21,;
    caption='GSAI_BCF',;
   bGlobalFont=.t.,;
    prg="GSAI_BCF",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 28532908

  add object oStr_1_1 as StdString with uid="WDXOCNLHEF",Visible=.t., Left=338, Top=20,;
    Alignment=1, Width=88, Height=18,;
    Caption="Separatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ROFVVBCFOA",Visible=.t., Left=3, Top=20,;
    Alignment=1, Width=152, Height=18,;
    Caption="Anno di adempimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KKVUMUKHUC",Visible=.t., Left=-2, Top=60,;
    Alignment=1, Width=157, Height=18,;
    Caption="Data inizio adempimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="IXNDHNQMFA",Visible=.t., Left=279, Top=57,;
    Alignment=1, Width=157, Height=18,;
    Caption="Data fine adempimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="KFUCRUVXWC",Visible=.t., Left=13, Top=162,;
    Alignment=0, Width=304, Height=19,;
    Caption="Generazione file comunicazione polivalente"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="NXLPAPRPJT",Visible=.t., Left=531, Top=95,;
    Alignment=1, Width=157, Height=18,;
    Caption="Data fine adempimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_PEAUTRAS ='N')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="ILNNSJLUAM",Visible=.t., Left=279, Top=95,;
    Alignment=1, Width=157, Height=18,;
    Caption="Data inizio adempimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_PEAUTRAS ='N')
    endwith
  endfunc

  add object oBox_1_29 as StdBox with uid="LRNPQZMIOP",left=-2, top=177, width=847,height=2
enddefine
define class tgsai_asdPag2 as StdContainer
  Width  = 846
  height = 459
  stdWidth  = 846
  stdheight = 459
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="VSCAHZKCHZ",left=2, top=2, width=804, height=455, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsai_msi",lower(this.oContained.GSAI_MSI.class))=0
        this.oContained.GSAI_MSI.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsai_asdPag3 as StdContainer
  Width  = 846
  height = 459
  stdWidth  = 846
  stdheight = 459
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="FVLSZZNNTG",left=2, top=3, width=364, height=418, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsai_msc",lower(this.oContained.GSAI_MSC.class))=0
        this.oContained.GSAI_MSC.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsai_asdPag4 as StdContainer
  Width  = 846
  height = 459
  stdWidth  = 846
  stdheight = 459
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="UNEERYCCCY",left=4, top=39, width=355, height=420, bOnScreen=.t.;



  add object oLinkPC_4_2 as stdDynamicChildContainer with uid="OTLKREZSNN",left=362, top=38, width=480, height=422, bOnScreen=.t.;


  add object oPEIMPSCH_4_4 as StdField with uid="SGPKMORVYE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PEIMPSCH", cQueryName = "PEIMPSCH",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Limite importo schede carburante ( Se 0 nessun controllo)",;
    HelpContextID = 29872958,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=238, Top=11, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oStr_4_3 as StdString with uid="QZISDKAYAL",Visible=.t., Left=8, Top=13,;
    Alignment=1, Width=225, Height=18,;
    Caption="Limite importo schede carburante:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsai_mss",lower(this.oContained.GSAI_MSS.class))=0
        this.oContained.GSAI_MSS.createrealchild()
      endif
      if type('this.oContained')='O' and at("gsai_msf",lower(this.oContained.GSAI_MSF.class))=0
        this.oContained.GSAI_MSF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_asd','DATPARSP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PE__ANNO=DATPARSP.PE__ANNO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
