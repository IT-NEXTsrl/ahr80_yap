* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai1kos                                                        *
*              Associa documenti a contratto                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-11-02                                                      *
* Last revis.: 2012-06-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai1kos",oParentObject))

* --- Class definition
define class tgsai1kos as StdForm
  Top    = 18
  Left   = 10

  * --- Standard Properties
  Width  = 780
  Height = 421
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-13"
  HelpContextID=169346921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gsai1kos"
  cComment = "Associa documenti a contratto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READAZI = space(5)
  w_ESEPRE = space(5)
  w_INIESE = ctod('  /  /  ')
  w_TIPASS = space(1)
  w_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_PARAME = space(3)
  w_TIPOPE = space(1)
  w_SERIALE = space(10)
  w_RIFCON = space(10)
  w_ZOOMVP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai1kosPag1","gsai1kos",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPASS_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMVP = this.oPgFrm.Pages(1).oPag.ZOOMVP
    DoDefault()
    proc Destroy()
      this.w_ZOOMVP = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READAZI=space(5)
      .w_ESEPRE=space(5)
      .w_INIESE=ctod("  /  /  ")
      .w_TIPASS=space(1)
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_PARAME=space(3)
      .w_TIPOPE=space(1)
      .w_SERIALE=space(10)
      .w_RIFCON=space(10)
        .w_READAZI = i_CODAZI
        .w_ESEPRE = calcespr(i_CODAZI,g_iniese,.t.)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ESEPRE))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_TIPASS = 'P'
        .w_DOCINI = .w_INIESE
        .w_DOCFIN = I_datsys
      .oPgFrm.Page1.oPag.ZOOMVP.Calculate(.w_TIPASS)
        .w_PARAME = .w_ZOOMVP.getVar('MVFLVEAC')+.w_ZOOMVP.getVar('MVCLADOC')
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_TIPOPE = 'C'
        .w_SERIALE = .w_ZOOMVP.getVar('MVSERIAL')
        .w_RIFCON = .w_ZOOMVP.getVar('PNSERIAL')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .oPgFrm.Page1.oPag.ZOOMVP.Calculate(.w_TIPASS)
        .DoRTCalc(3,6,.t.)
            .w_PARAME = .w_ZOOMVP.getVar('MVFLVEAC')+.w_ZOOMVP.getVar('MVCLADOC')
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(8,8,.t.)
            .w_SERIALE = .w_ZOOMVP.getVar('MVSERIAL')
            .w_RIFCON = .w_ZOOMVP.getVar('PNSERIAL')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMVP.Calculate(.w_TIPASS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return

  proc Calculate_KUONZCJGNK()
    with this
          * --- Aggiorna da GSAI_BZP
          GSAI_BZP(this;
              ,'V';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsai1kos
    if cevent='Aggiorna'
       Select MVSERIAL,XCHK from (This.w_ZOOMVP.Ccursor) WHERE XCHK=1 into cursor TMP_DOC
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMVP.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_KUONZCJGNK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsai1kos
    if cevent='Aggiorna'
       update (This.w_ZOOMVP.Ccursor) set XCHK=1 where MVSERIAL in (Select MVSERIAL from  TMP_DOC)
       Use in TMP_DOC
       Do GSAI_BZP with This,'O'
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESEPRE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESEPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESEPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESEPRE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_READAZI;
                       ,'ESCODESE',this.w_ESEPRE)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESEPRE = NVL(_Link_.ESCODESE,space(5))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESEPRE = space(5)
      endif
      this.w_INIESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESEPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPASS_1_4.RadioValue()==this.w_TIPASS)
      this.oPgFrm.Page1.oPag.oTIPASS_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_5.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_5.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_6.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_6.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPE_1_11.RadioValue()==this.w_TIPOPE)
      this.oPgFrm.Page1.oPag.oTIPOPE_1_11.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � successiva a quella finale")
          case   not(.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � successiva a quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsai1kosPag1 as StdContainer
  Width  = 776
  height = 421
  stdWidth  = 776
  stdheight = 421
  resizeXpos=538
  resizeYpos=221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPASS_1_4 as StdCombo with uid="TUBZIKNPQC",rtseq=4,rtrep=.f.,left=113,top=9,width=131,height=22;
    , ToolTipText = "Tipo associazione";
    , HelpContextID = 27375818;
    , cFormVar="w_TIPASS",RowSource=""+"Per contratto,"+"Per pratica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPASS_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oTIPASS_1_4.GetRadio()
    this.Parent.oContained.w_TIPASS = this.RadioValue()
    return .t.
  endfunc

  func oTIPASS_1_4.SetRadio()
    this.Parent.oContained.w_TIPASS=trim(this.Parent.oContained.w_TIPASS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPASS=='C',1,;
      iif(this.Parent.oContained.w_TIPASS=='P',2,;
      0))
  endfunc

  add object oDOCINI_1_5 as StdField with uid="ZGFEWOOULR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � successiva a quella finale",;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 199918538,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=353, Top=8

  func oDOCINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_6 as StdField with uid="MSJYJXJTGQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � successiva a quella finale",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 121471946,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=478, Top=8

  func oDOCFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
    endwith
    return bRes
  endfunc


  add object ZOOMVP as cp_szoombox with uid="IEEULTBTOF",left=7, top=56, width=761,height=300,;
    caption='ZOOMVP',;
   bGlobalFont=.t.,;
    cZoomFile="GSAI1KOS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cTable="DOC_MAST",cZoomOnZoom="",cMenuFile="GSAL_KOS",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 73777770


  add object oBtn_1_8 as StdButton with uid="NIMUMFUHHH",left=720, top=6, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i dati con i valori di default";
    , HelpContextID = 7567126;
    , caption='\<Ricerca',TabStop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      this.parent.oContained.NotifyEvent("Interroga")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_10 as cp_runprogram with uid="WDYTVGLWHR",left=-2, top=461, width=191,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BZM(w_SERIALE, w_PARAME)",;
    cEvent = "w_ZOOMVP selected",;
    nPag=1;
    , HelpContextID = 8650726


  add object oTIPOPE_1_11 as StdCombo with uid="HSFRIGZOFN",rtseq=8,rtrep=.f.,left=113,top=383,width=131,height=22;
    , ToolTipText = "Tipo associazione";
    , HelpContextID = 264485066;
    , cFormVar="w_TIPOPE",RowSource=""+"Corrispettivi periodici,"+"Contratti collegati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPE_1_11.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oTIPOPE_1_11.GetRadio()
    this.Parent.oContained.w_TIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPE_1_11.SetRadio()
    this.Parent.oContained.w_TIPOPE=trim(this.Parent.oContained.w_TIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPE=='P',1,;
      iif(this.Parent.oContained.w_TIPOPE=='C',2,;
      0))
  endfunc


  add object oBtn_1_12 as StdButton with uid="GLPKHFOSFK",left=251, top=370, width=48,height=45,;
    CpPicture="bmp\CONTRATT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare creare/aggiornare riferimenti contratti";
    , HelpContextID = 161464886;
    , caption='\<Rif.Con',TabStop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      this.parent.oContained.NotifyEvent("Aggiorna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="RWVHEPYZPP",left=623, top=370, width=48,height=45,;
    CpPicture="BMP\teso.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per per accedere alla registrazione contabile associata";
    , HelpContextID = 245107702;
    , TabStop=.f.,Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_RIFCON)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_RIFCON))
      endwith
    endif
  endfunc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_RIFCON) or g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="LNGADNPUCO",left=671, top=370, width=48,height=45,;
    CpPicture="BMP\SCHEDE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per per accedere alla manutenzione operazioni";
    , HelpContextID = 160354763;
    , TabStop=.f.,Caption='\<Operazioni';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        do ApriOperazioni with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="KUWCMEGMLG",left=720, top=370, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 168910778;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_19 as StdString with uid="IEICAJBEVU",Visible=.t., Left=9, Top=10,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipo associazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="OOQMYWSQRY",Visible=.t., Left=255, Top=8,;
    Alignment=1, Width=94, Height=15,;
    Caption="Documenti dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="KTZRCRKILE",Visible=.t., Left=447, Top=8,;
    Alignment=1, Width=28, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="XGKQQPFXUM",Visible=.t., Left=9, Top=383,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai1kos','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsai1kos
Proc ApriOperazioni(obj)
Local oggetto
oggetto=GSAI_KOS()
oggetto.w_DATA1=obj.w_DOCINI
oggetto.w_DATA2=obj.w_DOCFIN
oggetto.w_OPERVALO='S'
oggetto.w_FLAGZERO='N'
oggetto.MCALC(.T.)
oggetto=.Null.
Endproc
* --- Fine Area Manuale
