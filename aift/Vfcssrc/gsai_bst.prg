* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_bst                                                        *
*              Stampa dati file generato spesometro                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-10-30                                                      *
* Last revis.: 2014-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsai_bst",oParentObject,m.pParame)
return(i_retval)

define class tgsai_bst as StdBatch
  * --- Local variables
  pParame = space(10)
  w_Old_Dir = space(250)
  w_FIleSel = space(10)
  w_Mess = space(50)
  w_Cont_File = space(254)
  w_Num_Rec = 0
  w_TipoGen = space(1)
  w_Quadro = space(2)
  w_NumRecQuadro = 0
  w_NumRic = 0
  w_NumeroElementi = 0
  w_IdenRigo = space(8)
  w_NumeroOcc = 0
  w_CreaCursQuadroFE = .f.
  w_CreaCursQuadroFR = .f.
  w_CreaCursQuadroNE = .f.
  w_CreaCursQuadroNR = .f.
  w_CreaCursQuadroDF = .f.
  w_CreaCursQuadroFN = .f.
  w_CreaCursQuadroSE = .f.
  w_CreaCursQuadroTU = .f.
  w_CreaCursQuadroFA = .f.
  w_CreaCursQuadroSA = .f.
  w_CreaCursQuadroBL = .f.
  w_Posiz = 0
  w_RecordQuadro = .f.
  w_ValoreCampo = space(16)
  w_ValoreCampoData = space(16)
  w_IdenRigoData = space(8)
  w_NumRicerca = 0
  w_EsisteRecord = .f.
  w_ContaRecordObbl = 0
  w_NumCampiObbl = 0
  w_ValoreCampoObbl = space(100)
  w_NumRecArray = 0
  w_NumTotaleArray = 0
  w_TotaleOcc = 0
  w_StampaModello = space(1)
  w_FormatoData = .f.
  w_FormatoImporto = space(1)
  w_LunghezzaCampo = .f.
  w_CheckCampo = .f.
  w_NumeroModello = 0
  w_At__Anno = space(4)
  w_Atperiodo = space(2)
  w_Atcodfis = space(16)
  w_Atinvord = space(1)
  w_Atinvsos = space(1)
  w_Atinvann = space(1)
  w_Atprotec = space(17)
  w_Atprodoc = space(6)
  w_Tipagr = space(1)
  w_Tipdat = space(1)
  w_AtquadFA = space(1)
  w_AtquadSA = space(1)
  w_AtquadBL = space(1)
  w_AtquadFE = space(1)
  w_AtquadFR = space(1)
  w_AtquadNE = space(1)
  w_AtquadNR = space(1)
  w_AtquadDF = space(1)
  w_AtquadFN = space(1)
  w_AtquadSE = space(1)
  w_AtquadTU = space(1)
  w_Atpariva = space(11)
  w_Atcodatt = space(6)
  w_At__mail = space(50)
  w_Attelefo = space(12)
  w_Atnumfax = space(12)
  w_Atcognom = space(24)
  w_At__Nome = space(20)
  w_AtsessoM = space(1)
  w_AtsessoF = space(1)
  w_AtdatnasD = space(2)
  w_AtdatnasM = space(2)
  w_AtdatnasY = space(4)
  w_Atlocnas = space(40)
  w_Atpronas = space(2)
  w_Atragsoc = space(60)
  w_Atcfisot = space(16)
  w_Atcodcar = space(2)
  w_AtdatiprD = space(2)
  w_AtdatiprM = space(2)
  w_AtdatiprY = space(4)
  w_AtdatfprD = space(2)
  w_AtdatfprM = space(2)
  w_AtdatfprY = space(4)
  w_Atcogsot = space(24)
  w_Atnomsot = space(20)
  w_AtsessotM = space(1)
  w_AtsessotF = space(1)
  w_AtdtnsstD = space(2)
  w_AtdtnsstM = space(2)
  w_AtdtnsstY = space(4)
  w_Atlcnsst = space(40)
  w_Atprrsst = space(2)
  w_Atragsst = space(60)
  w_Atcodint = space(16)
  w_Atimptra = space(1)
  w_Atcodcaf = space(5)
  w_AtdatimpD = space(2)
  w_AtdatimpM = space(2)
  w_AtdatimpY = space(4)
  w_NumRecBl = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pParame="SceltaFile"
        this.w_FIleSel = ""
        this.w_Old_Dir = Sys(5)+Sys(2003)
        if Directory(justpath(this.oParentObject.w_Nomefile))
          Cd justpath(this.oParentObject.w_Nomefile)
        endif
        this.w_FIleSel = Getfile("ART21")
        if Not Empty(this.w_FileSel)
          this.oParentObject.w_NomeFile = this.w_FileSel
        endif
        Cd (this.w_Old_Dir)
        * --- Controllo se il file esiste
        if NOT file( this.oParentObject.w_NomeFile )
          * --- Se il file non esiste
          this.w_Mess = "Il file selezionato non esiste"
          ah_errormsg(this.w_Mess,48)
          i_retcode = 'stop'
          return
        endif
        this.w_Cont_File = FileToStr(this.oParentObject.w_NomeFile)
        Alines(Quadri,this.w_Cont_File)
        this.w_Cont_File = ""
        this.w_Num_Rec = 1
        do while this.w_Num_Rec<= Alen(Quadri)
          if Left(Quadri[this.w_Num_Rec],1)="B"
            this.oParentObject.w_Attipagr = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],116,1))="1","S","N")
            this.oParentObject.w_Attipdat = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],117,1))="1","S","N")
            this.oParentObject.w_QuadFA = Alltrim(Substr(Quadri[this.w_Num_Rec],118,1))
            this.oParentObject.w_QuadSA = Alltrim(Substr(Quadri[this.w_Num_Rec],119,1))
            this.oParentObject.w_QuadBL = Alltrim(Substr(Quadri[this.w_Num_Rec],120,1))
            this.oParentObject.w_QuadTUA = iif(this.oParentObject.w_Attipagr="S",Alltrim(Substr(Quadri[this.w_Num_Rec],128,1)),"0")
            this.oParentObject.w_QuadFE = Alltrim(Substr(Quadri[this.w_Num_Rec],121,1))
            this.oParentObject.w_QuadFR = Alltrim(Substr(Quadri[this.w_Num_Rec],122,1))
            this.oParentObject.w_QuadNE = Alltrim(Substr(Quadri[this.w_Num_Rec],123,1))
            this.oParentObject.w_QuadNR = Alltrim(Substr(Quadri[this.w_Num_Rec],124,1))
            this.oParentObject.w_QuadDF = Alltrim(Substr(Quadri[this.w_Num_Rec],125,1))
            this.oParentObject.w_QuadFN = Alltrim(Substr(Quadri[this.w_Num_Rec],126,1))
            this.oParentObject.w_QuadTU = iif(this.oParentObject.w_Attipdat="S",Alltrim(Substr(Quadri[this.w_Num_Rec],128,1)),"0")
            this.oParentObject.w_QuadSE = Alltrim(Substr(Quadri[this.w_Num_Rec],127,1))
            this.oParentObject.w_Atgenspe = iif(Empty(Alltrim(Substr(Quadri[this.w_Num_Rec],380,2))),"S","N")
            this.oParentObject.w_Atgenblk = iif(Not Empty(Alltrim(Substr(Quadri[this.w_Num_Rec],380,2))) And this.oParentObject.w_QuadBL="1","S","N")
            this.oParentObject.w_Atgensma = iif(Not Empty(Alltrim(Substr(Quadri[this.w_Num_Rec],380,2))) And this.oParentObject.w_QuadSE="1","S","N")
            this.oParentObject.w_Attipinv = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],90,1))="1",0,iif(Alltrim(Substr(Quadri[this.w_Num_Rec],91,1))="1",1,2))
            * --- Esiste solo un record di tipo B quindi posso uscire dal ciclo
            Exit
          endif
          this.w_Num_Rec = this.w_Num_Rec+1
        enddo
      case this.pParame="StampaDett"
        * --- Controllo se il file esiste
        if NOT file( this.oParentObject.w_NomeFile )
          * --- Se il file non esiste
          this.w_Mess = "Il file selezionato non esiste"
          ah_errormsg(this.w_Mess,48)
          i_retcode = 'stop'
          return
        endif
        this.w_Cont_File = FileToStr(this.oParentObject.w_NomeFile)
        Alines(Quadri,this.w_Cont_File)
        this.w_Cont_File = ""
        this.w_Num_Rec = 1
        this.w_TipoGen = " "
        this.w_CreaCursQuadroFE = .t.
        this.w_CreaCursQuadroFR = .t.
        this.w_CreaCursQuadroNE = .t.
        this.w_CreaCursQuadroNR = .t.
        this.w_CreaCursQuadroDF = .t.
        this.w_CreaCursQuadroFN = .t.
        this.w_CreaCursQuadroSE = .t.
        this.w_CreaCursQuadroTU = .t.
        this.w_CreaCursQuadroFA = .t.
        this.w_CreaCursQuadroSA = .t.
        this.w_CreaCursQuadroBL = .t.
        do while this.w_Num_Rec<= Alen(Quadri)
          do case
            case Left(Quadri[this.w_Num_Rec],1)="B"
              * --- Stampo il frontespizio
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case Left(Quadri[this.w_Num_Rec],1)="C" Or Left(Quadri[this.w_Num_Rec],1)="D" 
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case Left(Quadri[this.w_Num_Rec],1)="E"
              this.Pag13()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
          this.w_Num_Rec = this.w_Num_Rec+1
        enddo
        if Used("QuadroFE") And Reccount("QuadroFE")>0
          this.w_StampaModello = "S"
          this.Pag9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroFR") And Reccount("QuadroFR")>0
          this.w_StampaModello = "S"
          this.Pag10()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroNE") And Reccount("QuadroNE")>0
          this.w_StampaModello = "S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroNR") And Reccount("QuadroNR")>0
          this.w_StampaModello = "S"
          this.Pag11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroDF") And Reccount("QuadroDF")>0
          this.w_StampaModello = "S"
          this.Pag8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroFN") And Reccount("QuadroFN")>0
          this.w_StampaModello = "S"
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroSE") And Reccount("QuadroSE")>0
          this.w_StampaModello = "S"
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroFA") And Reccount("QuadroFA")>0
          this.w_StampaModello = "S"
          this.Pag14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroSA") And Reccount("QuadroSA")>0
          this.w_StampaModello = "S"
          this.Pag15()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroBL") And Reccount("QuadroBL")>0
          this.w_StampaModello = "S"
          this.Pag16()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroTU") And Reccount("QuadroTU")>0
          this.w_StampaModello = "S"
          this.Pag12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Used("QuadroTA") And Reccount("QuadroTA")>0
          this.w_StampaModello = "S"
          this.Pag13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_TipoGen = iif(Left(Quadri[this.w_Num_Rec],1)="C","R",iif(Left(Quadri[this.w_Num_Rec],1)="D","A"," "))
    this.w_NumeroModello = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
    do case
      case this.w_TipoGen="R"
        * --- La generazione del file � avvenuta in maniera aggregata quindi devono
        *     essere ricercate solo i quadri FA/SA/BL/TU
        if this.oParentObject.w_QuadFA="1"
          this.w_Quadro = "FA"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 3
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 16
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 3
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadSA="1"
          this.w_Quadro = "SA"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 10
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 4
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 1
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadBL="1"
          * --- Vista la composizione del quadro BL l'ha chiamata a pagina 3 deve
          *     essere fatta pi� volte
          this.w_NumRecBl = 1
          this.w_Quadro = "BL"
          do while this.w_NumRecBl<=8
            * --- Contatore numero record
            this.w_NumRic = 1
            * --- Contatore numero elementi
            this.w_NumeroOcc = 1
            * --- Totale record per quadro
            this.w_NumRecQuadro = 1
            * --- Totale elementi per quadro
            do case
              case this.w_NumRecBl=1
                this.w_RecordQuadro = .f.
                this.w_NumeroElementi = 10
              case this.w_NumRecBl=2
                this.w_NumeroElementi = 4
              case this.w_NumRecBl>=3 Or this.w_NumRecBl<=6
                this.w_NumeroElementi = 2
              case this.w_NumRecBl=7
                this.w_NumeroElementi = 1
              case this.w_NumRecBl=8
                this.w_NumeroElementi = 2
            endcase
            this.w_StampaModello = "N"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_NumRecBl = this.w_NumRecBl+1
          enddo
        endif
      case this.w_TipoGen="A"
        * --- La generazione del file � avvenuta in maniera analitica quindi devono
        *     essere ricercate solo i quadri FE/FR/NE/NR/DF/FN/SE/TU
        if this.oParentObject.w_QuadFE="1"
          this.w_Quadro = "FE"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 6
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 11
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 3
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadFR="1"
          this.w_Quadro = "FR"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 6
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 9
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 2
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadNE="1"
          this.w_Quadro = "NE"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 10
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 7
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 2
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadNR="1"
          this.w_Quadro = "NR"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 10
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 5
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 1
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadDF="1"
          this.w_Quadro = "DF"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 10
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 4
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 1
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadFN="1"
          this.w_Quadro = "FN"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 3
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 16
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 2
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadSE="1"
          this.w_Quadro = "SE"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 3
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 16
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 2
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_QuadTU="1" Or this.oParentObject.w_QuadTUA="1"
          this.w_Quadro = "TU"
          * --- Contatore numero record
          this.w_NumRic = 1
          * --- Contatore numero elementi
          this.w_NumeroOcc = 1
          * --- Totale record per quadro
          this.w_NumRecQuadro = 3
          * --- Totale elementi per quadro
          this.w_NumeroElementi = 13
          * --- Variabile utilizzata per verificare se esiste almeno un record per il quadro FE
          this.w_RecordQuadro = .f.
          this.w_NumCampiObbl = 1
          this.w_StampaModello = "N"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do while this.w_NumRic<=this.w_NumRecQuadro
      do while this.w_NumeroOcc<=this.w_NumeroElementi
        if this.w_Quadro="BL"
          this.w_IdenRigo = this.w_Quadro+"00"+alltrim(Str(this.w_NumRecBl,2))+iif(this.w_NumeroOcc>=10,"0","00")+Alltrim(Str(this.w_NumeroOcc,2))
        else
          this.w_IdenRigo = this.w_Quadro+iif(this.w_NumRic>=10,"0","00")+alltrim(Str(this.w_NumRic,2))+iif(this.w_NumeroOcc>=10,"0","00")+Alltrim(Str(this.w_NumeroOcc,2))
        endif
        * --- Controllo se esiste nel tracciato il campo corrispondente
        this.w_Posiz = At(this.w_IdenRigo,Quadri[this.w_Num_Rec],1)
        if this.w_Posiz<>0
          do case
            case this.w_Quadro="FE"
              this.Pag9()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="FR"
              this.Pag10()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="NE"
              this.Pag6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="NR"
              this.Pag11()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="DF"
              this.Pag8()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="SE"
              this.Pag7()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="FN"
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="TU"
              this.Pag12()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="TA"
              this.Pag13()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="FA"
              this.Pag14()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="SA"
              this.Pag15()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_Quadro="BL"
              this.Pag16()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],this.w_Posiz+8,16))
          this.w_FormatoData = iif ((this.w_Quadro="FE" And (this.w_NumeroOcc=7 Or this.w_NumeroOcc=8)) Or (this.w_Quadro="FR" And (this.w_NumeroOcc=3 Or this.w_NumeroOcc=4)) ,.t.,.f.)
          this.w_FormatoData = iif(this.w_Quadro="NE" And (this.w_NumeroOcc=3 Or this.w_NumeroOcc=4),.t.,this.w_FormatoData)
          this.w_FormatoData = iif(this.w_Quadro="NR" And (this.w_NumeroOcc=2 Or this.w_NumeroOcc=3) ,.t.,this.w_FormatoData)
          this.w_FormatoData = iif(this.w_Quadro="DF" And this.w_NumeroOcc=2,.t.,this.w_FormatoData)
          this.w_FormatoData = iif(this.w_Quadro="FN" And (this.w_NumeroOcc=3 Or this.w_NumeroOcc=11 Or this.w_NumeroOcc=12),.t.,this.w_FormatoData)
          this.w_FormatoData = iif(this.w_Quadro="SE" And (this.w_NumeroOcc=3 Or this.w_NumeroOcc=12 Or this.w_NumeroOcc=13),.t.,this.w_FormatoData)
          this.w_FormatoData = iif(this.w_Quadro="TU" And (this.w_NumeroOcc=3 Or this.w_NumeroOcc=9 Or this.w_NumeroOcc=10),.t.,this.w_FormatoData)
          this.w_FormatoData = iif(this.w_Quadro="BL" And this.w_NumeroOcc=3 And this.w_NumRecBl=1,.t.,this.w_FormatoData)
          this.w_FormatoImporto = iif( (this.w_Quadro="FE" And (this.w_NumeroOcc=10 or this.w_NumeroOcc=11 )) Or (this.w_Quadro="FR" And (this.w_NumeroOcc=8 or this.w_NumeroOcc=9 )),.t.,.f.)
          this.w_FormatoImporto = iif(this.w_Quadro="NE" And (this.w_NumeroOcc=6 or this.w_NumeroOcc=7 ) ,.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="NR" And (this.w_NumeroOcc=4 or this.w_NumeroOcc=5 ) ,.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="DF" And this.w_NumeroOcc=3 ,.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="FN" And (this.w_NumeroOcc=15 Or this.w_NumeroOcc=16),.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="SE" And (this.w_NumeroOcc=15 Or this.w_NumeroOcc=16),.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="TU" And (this.w_NumeroOcc=12 Or this.w_NumeroOcc=13),.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="FA" And (this.w_NumeroOcc=4 Or this.w_NumeroOcc=5),.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="FA" And (this.w_NumeroOcc>=7 And this.w_NumeroOcc<=16),.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="SA" And (this.w_NumeroOcc=2 Or this.w_NumeroOcc=3),.t.,this.w_FormatoImporto)
          this.w_FormatoImporto = iif(this.w_Quadro="BL" And (this.w_NumRecBl>=3 And this.w_NumRecBl<=8) And (this.w_NumeroOcc=1 Or this.w_NumeroOcc=2),.t.,this.w_FormatoImporto)
          this.w_LunghezzaCampo = iif((this.w_Quadro="FE" And this.w_NumeroOcc=9) Or (this.w_Quadro="NE" And this.w_NumeroOcc=5),.t.,.f.)
          this.w_LunghezzaCampo = iif(this.w_Quadro="FN" And (this.w_NumeroOcc=1 Or this.w_NumeroOcc=2 Or this.w_NumeroOcc=4 Or this.w_NumeroOcc=7) ,.t.,this.w_LunghezzaCampo)
          this.w_LunghezzaCampo = iif(this.w_Quadro="FN" And (this.w_NumeroOcc=8 Or this.w_NumeroOcc=10 Or this.w_NumeroOcc=13 ) ,.t.,this.w_LunghezzaCampo)
          this.w_LunghezzaCampo = iif(this.w_Quadro="SE" And (this.w_NumeroOcc=1 Or this.w_NumeroOcc=2 Or this.w_NumeroOcc=4 Or this.w_NumeroOcc=7) ,.t.,this.w_LunghezzaCampo)
          this.w_LunghezzaCampo = iif(this.w_Quadro="SE" And (this.w_NumeroOcc=8 Or this.w_NumeroOcc=10 Or this.w_NumeroOcc=14) ,.t.,this.w_LunghezzaCampo)
          this.w_LunghezzaCampo = iif(this.w_Quadro="TU" And (this.w_NumeroOcc=1 Or this.w_NumeroOcc=2 Or this.w_NumeroOcc=4 Or this.w_NumeroOcc=6) ,.t.,this.w_LunghezzaCampo)
          this.w_LunghezzaCampo = iif(this.w_Quadro="TU" And (this.w_NumeroOcc=8 Or this.w_NumeroOcc=11) ,.t.,this.w_LunghezzaCampo)
          this.w_LunghezzaCampo = iif(this.w_Quadro="BL" And this.w_NumRecBl=1 And (this.w_NumeroOcc=1 Or this.w_NumeroOcc=2 Or this.w_NumeroOcc=4 Or this.w_NumeroOcc=7 Or this.w_NumeroOcc=8 Or this.w_NumeroOcc=10) ,.t.,this.w_LunghezzaCampo)
          this.w_CheckCampo = iif(this.w_Quadro="FE" And (this.w_NumeroOcc=3 Or this.w_NumeroOcc=4 Or this.w_NumeroOcc=6),.t.,.f.)
          this.w_CheckCampo = iif(this.w_Quadro="FR" And ( this.w_NumeroOcc=2 Or this.w_NumeroOcc=5 Or this.w_NumeroOcc=6 Or this.w_NumeroOcc=7),.t.,this.w_CheckCampo)
          this.w_CheckCampo = iif(this.w_Quadro="FA" And this.w_NumeroOcc=3 ,.t.,this.w_CheckCampo)
          this.w_CheckCampo = iif(this.w_Quadro="BL" And this.w_NumRecBl=2 And(this.w_NumeroOcc>=2 And this.w_NumeroOcc<=4) ,.t.,this.w_CheckCampo)
          do case
            case this.w_CheckCampo
              this.w_ValoreCampo = iif(this.w_ValoreCampo="1","X"," ")
              Replace (this.w_IdenRigo) with (this.w_ValoreCampo)
            case this.w_FormatoData
              this.w_IdenRigoData = this.w_IdenRigo+"D"
              this.w_ValoreCampoData = Substr(this.w_ValoreCampo,1,2)
              Replace (this.w_IdenRigoData) with (this.w_ValoreCampoData)
              this.w_IdenRigoData = this.w_IdenRigo+"M"
              this.w_ValoreCampoData = Substr(this.w_ValoreCampo,3,2)
              Replace (this.w_IdenRigoData) with (this.w_ValoreCampoData)
              this.w_IdenRigoData = this.w_IdenRigo+"Y"
              this.w_ValoreCampoData = Substr(this.w_ValoreCampo,5,4)
              Replace (this.w_IdenRigoData) with (this.w_ValoreCampoData)
            case this.w_LunghezzaCampo
              this.w_NumRicerca = 2
              do while At(this.w_IdenRigo,Quadri[this.w_Num_Rec],this.w_NumRicerca)<>0
                this.w_Posiz = At(this.w_IdenRigo,Quadri[this.w_Num_Rec],this.w_NumRicerca)
                this.w_ValoreCampo = this.w_ValoreCampo+Alltrim(Substr(Quadri[this.w_Num_Rec],this.w_Posiz+9,15))
                this.w_NumRicerca = this.w_NumRicerca+1
              enddo
              Replace (this.w_IdenRigo) with (this.w_ValoreCampo)
            case this.w_FormatoImporto
              this.w_ValoreCampo = Int(Val(this.w_ValoreCampo))
              Replace (this.w_IdenRigo) with (this.w_ValoreCampo)
            otherwise
              Replace (this.w_IdenRigo) with (this.w_ValoreCampo)
          endcase
          this.w_RecordQuadro = .t.
        else
          * --- Verifichiamo se nella riga successiva dell'array alla posizione 90 � presente 
          *     il campo che stiamo ricercando
          if (Substr(Quadri[this.w_Num_Rec+1],90,8)=this.w_IdenRigo Or Substr(Quadri[this.w_Num_Rec+1],114,8)=this.w_IdenRigo) And this.w_NumeroModello=Alltrim(Substr(Quadri[this.w_Num_Rec+1],18,8))
            this.w_Num_Rec = this.w_Num_Rec+1
            this.w_NumeroOcc = this.w_NumeroOcc-1
          endif
        endif
        this.w_NumeroOcc = this.w_NumeroOcc+1
      enddo
      this.w_EsisteRecord = .f.
      this.w_ContaRecordObbl = 1
      do while this.w_ContaRecordObbl<=this.w_NumCampiObbl And this.w_RecordQuadro And (this.w_Quadro<>"TA" And this.w_Quadro<>"BL")
        if (this.w_Quadro="FN" Or this.w_Quadro="SE") And this.w_ContaRecordObbl=2
          * --- Nel caso di quadro FN i campi che devono essere controllati
          *     sono il cognome e la denominazione sociale
          IdenRigoObbl=this.w_Quadro+iif(this.w_NumRic>=10,"0","00")+alltrim(Str(this.w_NumRic,2))+"007"
        else
          IdenRigoObbl=this.w_Quadro+iif(this.w_NumRic>=10,"0","00")+alltrim(Str(this.w_NumRic,2))+iif(this.w_ContaRecordObbl>=10,"0","00")+Alltrim(Str(this.w_ContaRecordObbl,2))
        endif
        this.w_ValoreCampoObbl = &IdenRigoObbl
        if Not Empty(this.w_ValoreCampoObbl)
          this.w_EsisteRecord = .t.
          Exit
        endif
        this.w_ContaRecordObbl = this.w_ContaRecordObbl+1
      enddo
      if ! this.w_EsisteRecord
        Exit
      endif
      this.w_NumeroOcc = 1
      this.w_NumRic = this.w_NumRic+1
    enddo
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Create Cursor Frontespizio (Atcodfis C(16),Atinvord C(1),Atinvsos C(1),Atinvann C(1),At__Anno C(4),Atperiodo C(2),; 
 Atprotec C(17),Atprodoc C(6),Attipagr C(1),Attipdat C(1),AtquadFA C(1),AtquadSA C(1),AtquadBL C(1),; 
 AtquadFE C(1),AtquadFR C(1),AtquadNE C(1),AtquadNR C(1),AtquadDF C(1),AtquadFN C(1),AtquadTU C(1),; 
 AtquadSE C(1),Atpariva C(11),Atcodatt C(6),At__Mail C(50),Attelefo C(12),Atnumfax C(12),; 
 Atcognom C(24),At__Nome C(20),AtsessoM C(1),AtsessoF C(1),AtdatnasD C(2),AtdatnasM C(2),AtdatnasY C(4),; 
 Atlocnas C(40),Atpronas C(2),Atragsoc C(60),Atcfisot C(16),Atcodcar C(2),AtdatiprD C(2),AtdatiprM C(2),AtdatiprY C(4),; 
 AtdatfprD C(2),AtdatfprM C(2),AtdatfprY C(4),Atcogsot C(24),Atnomsot C(20),AtsessotM C(1),AtsessotF C(1),; 
 AtdtnsstD C(2),AtdtnsstM C(2),AtdtnsstY C(4),Atlcnsst C(40),Atprrsst C(2),Atragsst C(60),Atcodint C(16),Atimptra C(1),; 
 Atcodcaf C(5),AtdatimpD C(2),AtdatimpM C(2),AtdatimpY C(4))
    this.w_Atcodfis = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
    this.w_At__Anno = Alltrim(Substr(Quadri[this.w_Num_Rec],376,4))
    this.w_Atperiodo = Alltrim(Substr(Quadri[this.w_Num_Rec],380,2))
    this.w_Atinvord = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],90,1))="1","X"," ")
    this.w_Atinvsos = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],91,1))="1","X"," ")
    this.w_Atinvann = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],92,1))="1","X"," ")
    this.w_Atprotec = Alltrim(Substr(Quadri[this.w_Num_Rec],93,17))
    this.w_Atprodoc = Alltrim(Substr(Quadri[this.w_Num_Rec],110,6))
    this.w_Tipagr = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],116,1))="1","X"," ")
    this.w_Tipdat = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],117,1))="1","X"," ")
    this.w_AtquadFA = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],118,1))="1","X"," ")
    this.w_AtquadSA = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],119,1))="1","X"," ")
    this.w_AtquadBL = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],120,1))="1","X"," ")
    this.w_AtquadFE = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],121,1))="1","X"," ")
    this.w_AtquadFR = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],122,1))="1","X"," ")
    this.w_AtquadNE = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],123,1))="1","X"," ")
    this.w_AtquadNR = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],124,1))="1","X"," ")
    this.w_AtquadDF = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],125,1))="1","X"," ")
    this.w_AtquadFN = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],126,1))="1","X"," ")
    this.w_AtquadSE = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],127,1))="1","X"," ")
    this.w_AtquadTU = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],128,1))="1","X"," ")
    this.w_Atpariva = Alltrim(Substr(Quadri[this.w_Num_Rec],130,11))
    this.w_Atcodatt = Alltrim(Substr(Quadri[this.w_Num_Rec],141,6))
    this.w_At__mail = Alltrim(Substr(Quadri[this.w_Num_Rec],171,50))
    this.w_Attelefo = Alltrim(Substr(Quadri[this.w_Num_Rec],147,12))
    this.w_Atnumfax = Alltrim(Substr(Quadri[this.w_Num_Rec],159,12))
    this.w_Atcognom = Alltrim(Substr(Quadri[this.w_Num_Rec],221,24))
    this.w_At__Nome = Alltrim(Substr(Quadri[this.w_Num_Rec],245,20))
    this.w_AtsessoM = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],265,1))="M","X"," ")
    this.w_AtsessoF = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],265,1))="F","X"," ")
    this.w_AtdatnasD = Alltrim(Substr(Quadri[this.w_Num_Rec],266,2))
    this.w_AtdatnasM = Alltrim(Substr(Quadri[this.w_Num_Rec],268,2))
    this.w_AtdatnasY = Alltrim(Substr(Quadri[this.w_Num_Rec],270,4))
    this.w_Atlocnas = Alltrim(Substr(Quadri[this.w_Num_Rec],274,40))
    this.w_Atpronas = Alltrim(Substr(Quadri[this.w_Num_Rec],314,2))
    this.w_Atragsoc = Alltrim(Substr(Quadri[this.w_Num_Rec],316,60))
    this.w_Atcfisot = Alltrim(Substr(Quadri[this.w_Num_Rec],382,16))
    this.w_Atcodcar = Alltrim(Substr(Quadri[this.w_Num_Rec],398,2))
    this.w_AtdatiprD = Alltrim(Substr(Quadri[this.w_Num_Rec],400,2))
    this.w_AtdatiprM = Alltrim(Substr(Quadri[this.w_Num_Rec],402,2))
    this.w_AtdatiprY = Alltrim(Substr(Quadri[this.w_Num_Rec],404,4))
    this.w_AtdatfprD = Alltrim(Substr(Quadri[this.w_Num_Rec],408,2))
    this.w_AtdatfprM = Alltrim(Substr(Quadri[this.w_Num_Rec],410,2))
    this.w_AtdatfprY = Alltrim(Substr(Quadri[this.w_Num_Rec],412,4))
    this.w_Atcogsot = Alltrim(Substr(Quadri[this.w_Num_Rec],416,24))
    this.w_Atnomsot = Alltrim(Substr(Quadri[this.w_Num_Rec],440,20))
    this.w_AtsessotM = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],460,1))="M","X"," ")
    this.w_AtsessotF = iif(Alltrim(Substr(Quadri[this.w_Num_Rec],460,1))="F","X"," ")
    this.w_AtdtnsstD = Alltrim(Substr(Quadri[this.w_Num_Rec],461,2))
    this.w_AtdtnsstM = Alltrim(Substr(Quadri[this.w_Num_Rec],463,2))
    this.w_AtdtnsstY = Alltrim(Substr(Quadri[this.w_Num_Rec],465,4))
    this.w_Atlcnsst = Alltrim(Substr(Quadri[this.w_Num_Rec],469,40))
    this.w_Atprrsst = Alltrim(Substr(Quadri[this.w_Num_Rec],509,2))
    this.w_Atragsst = Alltrim(Substr(Quadri[this.w_Num_Rec],511,60))
    this.w_Atcodint = Alltrim(Substr(Quadri[this.w_Num_Rec],571,16))
    this.w_Atimptra = Alltrim(Substr(Quadri[this.w_Num_Rec],592,1))
    this.w_Atcodcaf = Alltrim(Substr(Quadri[this.w_Num_Rec],587,5))
    this.w_AtdatimpD = Alltrim(Substr(Quadri[this.w_Num_Rec],594,2))
    this.w_AtdatimpM = Alltrim(Substr(Quadri[this.w_Num_Rec],596,2))
    this.w_AtdatimpY = Alltrim(Substr(Quadri[this.w_Num_Rec],598,4))
    Select Frontespizio
    WrCursor ("Frontespizio")
    Append Blank
    Replace Atcodfis with this.w_Atcodfis,Atinvord with this.w_Atinvord,Atinvsos with this.w_Atinvsos,Atinvann with this.w_Atinvann,; 
 At__Anno with this.w_At__Anno,Atperiodo with this.w_Atperiodo,Atprotec with this.w_Atprotec,Atprodoc with this.w_Atprodoc,; 
 Attipagr with this.w_Tipagr,Attipdat with this.w_Tipdat,AtquadFA with this.w_AtquadFA,AtquadSA with this.w_AtquadSA,; 
 AtquadBL with this.w_AtquadBL,AtquadFE with this.w_AtquadFE,AtquadFR with this.w_AtquadFR,AtquadNE with this.w_AtquadNE,; 
 AtquadNR with this.w_AtquadNR,AtquadDF with this.w_AtquadDF,AtquadFN with this.w_AtquadFN,AtquadTU with this.w_AtquadTU,; 
 AtquadSE with this.w_AtquadSE,Atpariva with this.w_Atpariva,Atcodatt with this.w_Atcodatt,; 
 At__Mail with this.w_At__Mail,Attelefo with this.w_Attelefo,Atnumfax with this.w_Atnumfax,; 
 Atcognom with this.w_Atcognom,At__Nome with this.w_At__Nome,AtsessoM with this.w_AtsessoM,; 
 AtsessoF with this.w_AtsessoF,AtdatnasD with this.w_AtdatnasD,AtdatnasM with this.w_AtdatnasM,AtdatnasY with this.w_AtdatnasY,; 
 Atlocnas with this.w_Atlocnas,Atpronas with this.w_Atpronas,Atragsoc with this.w_Atragsoc,Atcfisot with this.w_Atcfisot,; 
 Atcodcar with this.w_Atcodcar,AtdatiprD with this.w_AtdatiprD,AtdatiprM with this.w_AtdatiprM,AtdatiprY with this.w_AtdatiprY,; 
 AtdatfprD with this.w_AtdatfprD,AtdatfprM with this.w_AtdatfprM,AtdatfprY with this.w_AtdatfprY,Atcogsot with this.w_Atcogsot,; 
 Atnomsot with this.w_Atnomsot,AtsessotM with this.w_AtsessotM,AtsessotF with this.w_AtsessotF,; 
 AtdtnsstD with this.w_AtdtnsstD,AtdtnsstM with this.w_AtdtnsstM,AtdtnsstY with this.w_AtdtnsstY,Atlcnsst with this.w_Atlcnsst,; 
 Atprrsst with this.w_Atprrsst,Atragsst with this.w_Atragsst,Atcodint with this.w_Atcodint,Atimptra with this.w_Atimptra,; 
 Atcodcaf with this.w_Atcodcaf,AtdatimpD with this.w_AtdatimpD,AtdatimpM with this.w_AtdatimpM,AtdatimpY with this.w_AtdatimpY
    Select *,1 as Pagina From Frontespizio; 
 union all; 
 Select *,0 as Pagina From Frontespizio; 
 into cursor __tmp__ order by Pagina
    Use in select ("QuadroFE")
    Cp_Chprn("..\aift\exe\query\Quadro_Frontespizio", , this.oParentObject)
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroFN
          Create Cursor QuadroFN (CodFis C(16),Mod C(8),FN001001 C(40),FN001002 C(40),FN001003D C(2),; 
 FN001003M C(2),FN001003Y C(4),FN001004 C(40),FN001005 C(2),FN001006 C (3),FN001007 C(60),; 
 FN001008 C(40),FN001009 C(3),FN001010 C(40),FN001011D C(2),FN001011M C(2),FN001011Y C(4),; 
 FN001012D C(2),FN001012M C(2),FN001012Y C(4),FN001013 C(26),FN001014 C(1),FN001015 N(18),FN001016 N(18),; 
 FN002001 C(40),FN002002 C(40),FN002003D C(2),FN002003M C(2),FN002003Y C(4),; 
 FN002004 C(40),FN002005 C(2),FN002006 C (3),FN002007 C(60),FN002008 C(40),; 
 FN002009 C(3),FN002010 C(40),FN002011D C(2),FN002011M C(2),FN002011Y C(4),; 
 FN002012D C(2),FN002012M C(2),FN002012Y C(4),FN002013 C(26),FN002014 C(1),FN002015 N(18),FN002016 N(18),; 
 FN003001 C(40),FN003002 C(40),FN003003D C(2),FN003003M C(2),FN003003Y C(4),; 
 FN003004 C(40),FN003005 C(2),FN003006 C (3),FN003007 C(60),FN003008 C(40),; 
 FN003009 C(3),FN003010 C(40),FN003011D C(2),FN003011M C(2),FN003011Y C(4),; 
 FN003012D C(2),FN003012M C(2),FN003012Y C(4),FN003013 C(26),FN003014 C(1),FN003015 N(18),FN003016 N(18))
          this.w_CreaCursQuadroFN = .f.
          Select QuadroFN
          WrCursor ("QuadroFN")
        endif
        if ! this.w_RecordQuadro
          Select QuadroFN
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroFN into cursor __TMP__ order by Mod
        Use in select ("QuadroFN")
        Cp_Chprn("..\aift\exe\query\QuadroFN", , this.oParentObject)
    endcase
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroNE
          Create Cursor QuadroNE (CodFis C(16),Mod C(8),Ne001001 C(16),Ne001002 C(16),; 
 Ne001003D C(2),Ne001003M C(2),Ne001003Y C(4),Ne001004D C(2),Ne001004M C(2),Ne001004Y C(4),; 
 Ne001005 C(30),Ne001006 N(18,4),Ne001007 N(18,4),; 
 Ne002001 C(16),Ne002002 C(16),; 
 Ne002003D C(2),Ne002003M C(2),Ne002003Y C(4),Ne002004D C(2),Ne002004M C(2),Ne002004Y C(4),; 
 Ne002005 C(30),Ne002006 N(18,4),Ne002007 N(18,4),; 
 Ne003001 C(16),Ne003002 C(16),; 
 Ne003003D C(2),Ne003003M C(2),Ne003003Y C(4),Ne003004D C(2),Ne003004M C(2),Ne003004Y C(4),; 
 Ne003005 C(30),Ne003006 N(18,4),Ne003007 N(18,4),; 
 Ne004001 C(16),Ne004002 C(16),; 
 Ne004003D C(2),Ne004003M C(2),Ne004003Y C(4),Ne004004D C(2),Ne004004M C(2),Ne004004Y C(4),; 
 Ne004005 C(30),Ne004006 N(18,4),Ne004007 N(18,4),; 
 Ne005001 C(16),Ne005002 C(16),; 
 Ne005003D C(2),Ne005003M C(2),Ne005003Y C(4),Ne005004D C(2),Ne005004M C(2),Ne005004Y C(4),; 
 Ne005005 C(30),Ne005006 N(18,4),Ne005007 N(18,4),; 
 Ne006001 C(16),Ne006002 C(16),; 
 Ne006003D C(2),Ne006003M C(2),Ne006003Y C(4),Ne006004D C(2),Ne006004M C(2),Ne006004Y C(4),; 
 Ne006005 C(30),Ne006006 N(18,4),Ne006007 N(18,4),; 
 Ne007001 C(16),Ne007002 C(16),; 
 Ne007003D C(2),Ne007003M C(2),Ne007003Y C(4),Ne007004D C(2),Ne007004M C(2),Ne007004Y C(4),; 
 Ne007005 C(30),Ne007006 N(18,4),Ne007007 N(18,4),; 
 Ne008001 C(16),Ne008002 C(16),; 
 Ne008003D C(2),Ne008003M C(2),Ne008003Y C(4),Ne008004D C(2),Ne008004M C(2),Ne008004Y C(4),; 
 Ne008005 C(30),Ne008006 N(18,4),Ne008007 N(18,4),; 
 Ne009001 C(16),Ne009002 C(16),; 
 Ne009003D C(2),Ne009003M C(2),Ne009003Y C(4),Ne009004D C(2),Ne009004M C(2),Ne009004Y C(4),; 
 Ne009005 C(30),Ne009006 N(18,4),Ne009007 N(18,4),; 
 Ne010001 C(16),Ne010002 C(16),; 
 Ne010003D C(2),Ne010003M C(2),Ne010003Y C(4),Ne010004D C(2),Ne010004M C(2),Ne010004Y C(4),; 
 Ne010005 C(30),Ne010006 N(18,4),Ne010007 N(18,4))
          this.w_CreaCursQuadroNE = .f.
          Select QuadroNE
          WrCursor ("QuadroNE")
        endif
        if ! this.w_RecordQuadro
          Select QuadroNE
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select *,1 as Pagina From QuadroNE; 
 union all; 
 Select *,2 as Pagina From QuadroNE; 
 into cursor __tmp__ order by Mod,Pagina
        Use in select ("QuadroNE")
        Cp_Chprn("..\aift\exe\query\QuadroNE", , this.oParentObject)
    endcase
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroSE
          Create Cursor QuadroSE (CodFis C(16),Mod C(8),SE001001 C(40),SE001002 C(40),SE001003D C(2),; 
 SE001003M C(2),SE001003Y C(4),SE001004 C(40),SE001005 C(2),SE001006 C (3),SE001007 C(60),; 
 SE001008 C(40),SE001009 C(3),SE001010 C(40),SE001011 C(16),SE001012D C(2),SE001012M C(2),SE001012Y C(4),; 
 SE001013D C(2),SE001013M C(2),SE001013Y C(4),SE001014 C(26),SE001015 N(18),SE001016 N(18),; 
 SE002001 C(40),SE002002 C(40),SE002003D C(2),SE002003M C(2),SE002003Y C(4),; 
 SE002004 C(40),SE002005 C(2),SE002006 C (3),SE002007 C(60),SE002008 C(40),SE002009 C(3),; 
 SE002010 C(40),SE002011 C(16),SE002012D C(2),SE002012M C(2),SE002012Y C(4),; 
 SE002013D C(2),SE002013M C(2),SE002013Y C(4),SE002014 C(26),SE002015 N(18),SE002016 N(18),; 
 SE003001 C(40),SE003002 C(40),SE003003D C(2),SE003003M C(2),SE003003Y C(4),; 
 SE003004 C(40),SE003005 C(2),SE003006 C (3),SE003007 C(60),SE003008 C(40),SE003009 C(3),; 
 SE003010 C(40),SE003011 C(16),SE003012D C(2),SE003012M C(2),SE003012Y C(4),; 
 SE003013D C(2),SE003013M C(2),SE003013Y C(4),SE003014 C(26),SE003015 N(18),SE003016 N(18))
          this.w_CreaCursQuadroSE = .f.
          Select QuadroSE
          WrCursor ("QuadroSE")
        endif
        if ! this.w_RecordQuadro
          Select QuadroSE
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroSE into cursor __TMP__ order by Mod
        Use in select ("QuadroSE")
        Cp_Chprn("..\aift\exe\query\QuadroSE", , this.oParentObject)
    endcase
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroDF
          Create Cursor QuadroDF (CodFis C(16),Mod C(8),Df001001 C(16),Df001002D C(2),Df001002M C(2),; 
 Df001002Y C(4),Df001003 N(18,4),Df001004 C(1),; 
 Df002001 C(16),Df002002D C(2),Df002002M C(2),Df002002Y C(4),Df002003 N(18,4),Df002004 C(1),; 
 Df003001 C(16),Df003002D C(2),Df003002M C(2),Df003002Y C(4),Df003003 N(18,4),Df003004 C(1),; 
 Df004001 C(16),Df004002D C(2),Df004002M C(2),Df004002Y C(4),Df004003 N(18,4),Df004004 C(1),; 
 Df005001 C(16),Df005002D C(2),Df005002M C(2),Df005002Y C(4),Df005003 N(18,4),Df005004 C(1),; 
 Df006001 C(16),Df006002D C(2),Df006002M C(2),Df006002Y C(4),Df006003 N(18,4),Df006004 C(1),; 
 Df007001 C(16),Df007002D C(2),Df007002M C(2),Df007002Y C(4),Df007003 N(18,4),Df007004 C(1),; 
 Df008001 C(16),Df008002D C(2),Df008002M C(2),Df008002Y C(4),Df008003 N(18,4),Df008004 C(1),; 
 Df009001 C(16),Df009002D C(2),Df009002M C(2),Df009002Y C(4),Df009003 N(18,4),Df009004 C(1),; 
 Df010001 C(16),Df010002D C(2),Df010002M C(2),Df010002Y C(4),Df010003 N(18,4),Df010004 C(1))
          this.w_CreaCursQuadroDF = .f.
          Select QuadroDF
          WrCursor ("QuadroDF")
        endif
        if ! this.w_RecordQuadro
          Select QuadroDF
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroDF into cursor __TMP__ order by Mod
        Use in select ("QuadroDF")
        Cp_Chprn("..\aift\exe\query\QuadroDF", , this.oParentObject)
    endcase
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroFE
          Create Cursor QuadroFE (CodFis C(16),Mod C(8),Fe001001 C(16),Fe001002 C(16),Fe001003 C(1),Fe001004 C(1),Fe001005 C(2),; 
 Fe001006 C(1),Fe001007D C(2),Fe001007M C(2),Fe001007Y C(4),Fe001008D C(2),Fe001008M C(2),Fe001008Y C(4),; 
 Fe001009 C(30),Fe001010 N(18,4),Fe001011 N(18,4),; 
 Fe002001 C(16),Fe002002 C(16),Fe002003 C(1),Fe002004 C(1),Fe002005 C(2),; 
 Fe002006 C(1),Fe002007D C(2),Fe002007M C(2),Fe002007Y C(4),Fe002008D C(2),Fe002008M C(2),Fe002008Y C(4),; 
 Fe002009 C(30),Fe002010 N(18,4),Fe002011 N(18,4),; 
 Fe003001 C(16),Fe003002 C(16),Fe003003 C(1),Fe003004 C(1),Fe003005 C(2),; 
 Fe003006 C(1),Fe003007D C(2),Fe003007M C(2),Fe003007Y C(4),Fe003008D C(2),Fe003008M C(2),Fe003008Y C(4),; 
 Fe003009 C(30),Fe003010 N(18,4),Fe003011 N(18,4),; 
 Fe004001 C(16),Fe004002 C(16),Fe004003 C(1),Fe004004 C(1),Fe004005 C(2),; 
 Fe004006 C(1),Fe004007D C(2),Fe004007M C(2),Fe004007Y C(4),Fe004008D C(2),Fe004008M C(2),Fe004008Y C(4),; 
 Fe004009 C(30),Fe004010 N(18,4),Fe004011 N(18,4),; 
 Fe005001 C(16),Fe005002 C(16),Fe005003 C(1),Fe005004 C(1),Fe005005 C(2),; 
 Fe005006 C(1),Fe005007D C(2),Fe005007M C(2),Fe005007Y C(4),Fe005008D C(2),Fe005008M C(2),Fe005008Y C(4),; 
 Fe005009 C(30),Fe005010 N(18,4),Fe005011 N(18,4),; 
 Fe006001 C(16),Fe006002 C(16),Fe006003 C(1),Fe006004 C(1),Fe006005 C(2),; 
 Fe006006 C(1),Fe006007D C(2),Fe006007M C(2),Fe006007Y C(4),Fe006008D C(2),Fe006008M C(2),Fe006008Y C(4),; 
 Fe006009 C(30),Fe006010 N(18,4),Fe006011 N(18,4))
          this.w_CreaCursQuadroFE = .f.
          Select QuadroFE
          WrCursor ("QuadroFE")
        endif
        if ! this.w_RecordQuadro
          Select QuadroFE
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroFE into cursor __TMP__ order by Mod
        Use in select ("QuadroFE")
        Cp_Chprn("..\aift\exe\query\QuadroFE", , this.oParentObject)
    endcase
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroFR
          Create Cursor QuadroFR (CodFis C(16),Mod C(8),Fr001001 C(16),Fr001002 C(1),Fr001003D C(2),Fr001003M C(2),; 
 Fr001003Y C(4),Fr001004D C(2),Fr001004M C(2),Fr001004Y C(4),Fr001005 C(1),Fr001006 C(1),Fr001007 C(1),; 
 Fr001008 N(18,4),Fr001009 N(18,4),; 
 Fr002001 C(16),Fr002002 C(1),Fr002003D C(2),Fr002003M C(2),Fr002003Y C(4),; 
 Fr002004D C(2),Fr002004M C(2),Fr002004Y C(4),Fr002005 C(1),Fr002006 C(1),Fr002007 C(1),; 
 Fr002008 N(18,4),Fr002009 N(18,4),; 
 Fr003001 C(16),Fr003002 C(1),Fr003003D C(2),Fr003003M C(2),Fr003003Y C(4),; 
 Fr003004D C(2),Fr003004M C(2),Fr003004Y C(4),Fr003005 C(1),Fr003006 C(1),Fr003007 C(1),; 
 Fr003008 N(18,4),Fr003009 N(18,4),; 
 Fr004001 C(16),Fr004002 C(1),Fr004003D C(2),Fr004003M C(2),Fr004003Y C(4),; 
 Fr004004D C(2),Fr004004M C(2),Fr004004Y C(4),Fr004005 C(1),Fr004006 C(1),Fr004007 C(1),; 
 Fr004008 N(18,4),Fr004009 N(18,4),; 
 Fr005001 C(16),Fr005002 C(1),Fr005003D C(2),Fr005003M C(2),Fr005003Y C(4),; 
 Fr005004D C(2),Fr005004M C(2),Fr005004Y C(4),Fr005005 C(1),Fr005006 C(1),Fr005007 C(1),; 
 Fr005008 N(18,4),Fr005009 N(18,4),; 
 Fr006001 C(16),Fr006002 C(1),Fr006003D C(2),Fr006003M C(2),Fr006003Y C(4),; 
 Fr006004D C(2),Fr006004M C(2),Fr006004Y C(4),Fr006005 C(1),Fr006006 C(1),Fr006007 C(1),; 
 Fr006008 N(18,4),Fr006009 N(18,4))
          this.w_CreaCursQuadroFR = .f.
          Select QuadroFR
          WrCursor ("QuadroFR")
        endif
        if ! this.w_RecordQuadro
          Select QuadroFR
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroFR into cursor __TMP__ order by Mod
        Use in select ("QuadroFR")
        Cp_Chprn("..\aift\exe\query\QuadroFR", , this.oParentObject)
    endcase
  endproc


  procedure Pag11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroNR
          Create Cursor QuadroNR (CodFis C(16),Mod C(8),Nr001001 C(16),Nr001002D C(2),Nr001002M C(2),; 
 Nr001002Y C(4),Nr001003D C(2),Nr001003M C(2),Nr001003Y C(4),Nr001004 N(18,4),Nr001005 N(18,4),; 
 Nr002001 C(16),Nr002002D C(2),Nr002002M C(2),Nr002002Y C(4),; 
 Nr002003D C(2),Nr002003M C(2),Nr002003Y C(4),Nr002004 N(18,4),Nr002005 N(18,4),; 
 Nr003001 C(16),Nr003002D C(2),Nr003002M C(2),Nr003002Y C(4),; 
 Nr003003D C(2),Nr003003M C(2),Nr003003Y C(4),Nr003004 N(18,4),Nr003005 N(18,4),; 
 Nr004001 C(16),Nr004002D C(2),Nr004002M C(2),Nr004002Y C(4),; 
 Nr004003D C(2),Nr004003M C(2),Nr004003Y C(4),Nr004004 N(18,4),Nr004005 N(18,4),; 
 Nr005001 C(16),Nr005002D C(2),Nr005002M C(2),Nr005002Y C(4),; 
 Nr005003D C(2),Nr005003M C(2),Nr005003Y C(4),Nr005004 N(18,4),Nr005005 N(18,4),; 
 Nr006001 C(16),Nr006002D C(2),Nr006002M C(2),Nr006002Y C(4),; 
 Nr006003D C(2),Nr006003M C(2),Nr006003Y C(4),Nr006004 N(18,4),Nr006005 N(18,4),; 
 Nr007001 C(16),Nr007002D C(2),Nr007002M C(2),Nr007002Y C(4),; 
 Nr007003D C(2),Nr007003M C(2),Nr007003Y C(4),Nr007004 N(18,4),Nr007005 N(18,4),; 
 Nr008001 C(16),Nr008002D C(2),Nr008002M C(2),Nr008002Y C(4),; 
 Nr008003D C(2),Nr008003M C(2),Nr008003Y C(4),Nr008004 N(18,4),Nr008005 N(18,4),; 
 Nr009001 C(16),Nr009002D C(2),Nr009002M C(2),Nr009002Y C(4),; 
 Nr009003D C(2),Nr009003M C(2),Nr009003Y C(4),Nr009004 N(18,4),Nr009005 N(18,4),; 
 Nr010001 C(16),Nr010002D C(2),Nr010002M C(2),Nr010002Y C(4),; 
 Nr010003D C(2),Nr010003M C(2),Nr010003Y C(4),Nr010004 N(18,4),Nr010005 N(18,4))
          this.w_CreaCursQuadroNR = .f.
          Select QuadroNR
          WrCursor ("QuadroNR")
        endif
        if ! this.w_RecordQuadro
          Select QuadroNR
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroNR into cursor __TMP__ order by Mod
        Use in select ("QuadroNR")
        Cp_Chprn("..\aift\exe\query\QuadroNR", , this.oParentObject)
    endcase
  endproc


  procedure Pag12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroTU
          Create Cursor QuadroTU (CodFis C(16),Mod C(8),TU001001 C(40),TU001002 C(40),TU001003D C(2),; 
 TU001003M C(2),TU001003Y C(4),TU001004 C(40),TU001005 C(2),; 
 TU001006 C(40),TU001007 C(3),TU001008 C(40),TU001009D C(2),TU001009M C(2),TU001009Y C(4),; 
 TU001010D C(2),TU001010M C(2),TU001010Y C(4),TU001011 C(26),TU001012 N(18),TU001013 N(18),; 
 TU002001 C(40),TU002002 C(40),TU002003D C(2),TU002003M C(2),TU002003Y C(4),; 
 TU002004 C(40),TU002005 C(2),TU002006 C(40),TU002007 C(3),; 
 TU002008 C(40),TU002009D C(2),TU002009M C(2),TU002009Y C(4),; 
 TU002010D C(2),TU002010M C(2),TU002010Y C(4),TU002011 C(26),TU002012 N(18),TU002013 N(18),; 
 TU003001 C(40),TU003002 C(40),TU003003D C(2),TU003003M C(2),TU003003Y C(4),; 
 TU003004 C(40),TU003005 C(2),TU003006 C(40),TU003007 C(3),; 
 TU003008 C(40),TU003009D C(2),TU003009M C(2),TU003009Y C(4),; 
 TU003010D C(2),TU003010M C(2),TU003010Y C(4),TU003011 C(26),TU003012 N(18),TU003013 N(18))
          this.w_CreaCursQuadroTU = .f.
          Select QuadroTU
          WrCursor ("QuadroTU")
        endif
        if ! this.w_RecordQuadro
          Select QuadroTU
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroTU into cursor __TMP__ order by Mod
        Use in select ("QuadroTU")
        Cp_Chprn("..\aift\exe\query\QuadroTU", , this.oParentObject)
    endcase
  endproc


  procedure Pag13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        Create Cursor QuadroTA (Atcodfis C(16),TA001001 C(16),TA002001 C(16),TA003001 C(16),TA003002 C(16),; 
 TA003003 C(16),TA004001 C(16),TA004002 C(16),TA005001 C(16),TA005002 C(16),TA006001 C(16),TA007001 C(16),; 
 TA008001 C(16),TA009001 C(16),TA010001 C(16),TA011001 C(16))
        Select QuadroTA
        WrCursor ("QuadroTA")
        Append Blank
        this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
        Replace AtcodFis with this.w_ValoreCampo
        this.w_Quadro = "TA"
        this.w_NumeroOcc = 1
        this.w_NumeroElementi = 15
        do while this.w_NumeroOcc<=this.w_NumeroElementi
          do case
            case this.w_NumeroOcc=1
              this.w_IdenRigo = "TA001001"
            case this.w_NumeroOcc=2
              this.w_IdenRigo = "TA002001"
            case this.w_NumeroOcc=3
              this.w_IdenRigo = "TA003001"
            case this.w_NumeroOcc=4
              this.w_IdenRigo = "TA003002"
            case this.w_NumeroOcc=5
              this.w_IdenRigo = "TA003003"
            case this.w_NumeroOcc=6
              this.w_IdenRigo = "TA004001"
            case this.w_NumeroOcc=7
              this.w_IdenRigo = "TA004002"
            case this.w_NumeroOcc=8
              this.w_IdenRigo = "TA005001"
            case this.w_NumeroOcc=9
              this.w_IdenRigo = "TA005002"
            case this.w_NumeroOcc=10
              this.w_IdenRigo = "TA006001"
            case this.w_NumeroOcc=11
              this.w_IdenRigo = "TA007001"
            case this.w_NumeroOcc=12
              this.w_IdenRigo = "TA008001"
            case this.w_NumeroOcc=13
              this.w_IdenRigo = "TA009001"
            case this.w_NumeroOcc=14
              this.w_IdenRigo = "TA010001"
            case this.w_NumeroOcc=15
              this.w_IdenRigo = "TA011001"
          endcase
          * --- Controllo se esiste nel tracciato il campo corrispondente
          this.w_Posiz = At(this.w_IdenRigo,Quadri[this.w_Num_Rec],1)
          if this.w_Posiz<>0
            this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],this.w_Posiz+8,16))
            Replace (this.w_IdenRigo) with (this.w_ValoreCampo)
          endif
          this.w_NumeroOcc = this.w_NumeroOcc+1
        enddo
      case this.w_StampaModello="S"
        Select * from QuadroTA into cursor __TMP__
        Use in select ("QuadroTA")
        Cp_Chprn("..\aift\exe\query\QuadroTA", , this.oParentObject)
    endcase
  endproc


  procedure Pag14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroFA
          Create Cursor QuadroFA (CodFis C(16),Mod C(8),Fa001001 C(16),Fa001002 C(16),Fa001003 C(1),Fa001004 N(16),; 
 Fa001005 N(16),Fa001006 C(1),Fa001007 N(18,4),Fa001008 N(18,4),Fa001009 N(18,4),Fa001010 N(18,4),; 
 Fa001011 N(18,4),Fa001012 N(18,4),Fa001013 N(18,4),Fa001014 N(18,4),Fa001015 N(18,4),Fa001016 N(18,4),; 
 Fa002001 C(16),Fa002002 C(16),Fa002003 C(1),Fa002004 N(16),Fa002005 N(16),Fa002006 C(1),; 
 Fa002007 N(18,4),Fa002008 N(18,4),Fa002009 N(18,4),Fa002010 N(18,4),; 
 Fa002011 N(18,4),Fa002012 N(18,4),Fa002013 N(18,4),Fa002014 N(18,4),Fa002015 N(18,4),Fa002016 N(18,4),; 
 Fa003001 C(16),Fa003002 C(16),Fa003003 C(1),Fa003004 N(16),Fa003005 N(16),Fa003006 C(1),; 
 Fa003007 N(18,4),Fa003008 N(18,4),Fa003009 N(18,4),Fa003010 N(18,4),; 
 Fa003011 N(18,4),Fa003012 N(18,4),Fa003013 N(18,4),Fa003014 N(18,4),Fa003015 N(18,4),Fa003016 N(18,4))
          this.w_CreaCursQuadroFA = .f.
          Select QuadroFA
          WrCursor ("QuadroFA")
        endif
        if ! this.w_RecordQuadro
          Select QuadroFA
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroFA into cursor __TMP__ order by Mod
        Use in select ("QuadroFA")
        Cp_Chprn("..\aift\exe\query\QuadroFA", , this.oParentObject)
    endcase
  endproc


  procedure Pag15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroSA
          Create Cursor QuadroSA (CodFis C(16),Mod C(8),Sa001001 C(16),Sa001002 N(16),Sa001003 N(18,4),Sa001004 C(1),; 
 Sa002001 C(16),Sa002002 N(16),Sa002003 N(18,4),Sa002004 C(1),; 
 Sa003001 C(16),Sa003002 N(16),Sa003003 N(18,4),Sa003004 C(1),; 
 Sa004001 C(16),Sa004002 N(16),Sa004003 N(18,4),Sa004004 C(1),; 
 Sa005001 C(16),Sa005002 N(16),Sa005003 N(18,4),Sa005004 C(1),; 
 Sa006001 C(16),Sa006002 N(16),Sa006003 N(18,4),Sa006004 C(1),; 
 Sa007001 C(16),Sa007002 N(16),Sa007003 N(18,4),Sa007004 C(1),; 
 Sa008001 C(16),Sa008002 N(16),Sa008003 N(18,4),Sa008004 C(1),; 
 Sa009001 C(16),Sa009002 N(16),Sa009003 N(18,4),Sa009004 C(1),; 
 Sa010001 C(16),Sa010002 N(16),Sa010003 N(18,4),Sa010004 C(1))
          this.w_CreaCursQuadroSA = .f.
          Select QuadroSA
          WrCursor ("QuadroSA")
        endif
        if ! this.w_RecordQuadro
          Select QuadroSA
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroSA into cursor __TMP__ order by Mod
        Use in select ("QuadroSA")
        Cp_Chprn("..\aift\exe\query\QuadroSA", , this.oParentObject)
    endcase
  endproc


  procedure Pag16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_StampaModello="N"
        if this.w_CreaCursQuadroBL
          Create Cursor QuadroBL (CodFis C(16),Mod C(8),Bl001001 C(40),Bl001002 C(40),Bl001003D C(2),Bl001003M C(2),; 
 Bl001003Y C(4),Bl001004 C(40),Bl001005 C(2),Bl001006 C(3),Bl001007 C(60),Bl001008 C(40),Bl001009 C(3),Bl001010 C(40),; 
 Bl002001 C(16),Bl002002 C(1),Bl002003 C(1),Bl002004 C(1),Bl003001 N(18,4),Bl003002 N(18,4),Bl004001 N(18,4),; 
 Bl004002 N(18,4),Bl005001 N(18,4),Bl005002 N(18,4),Bl006001 N(18,4),Bl006002 N(18,4),Bl007001 N(18,4),; 
 Bl008001 N(18,4),Bl008002 N(18,4))
          this.w_CreaCursQuadroBL = .f.
          Select QuadroBL
          WrCursor ("QuadroBL")
        endif
        if ! this.w_RecordQuadro
          Select QuadroBL
          Append Blank
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],2,16))
          Replace CodFis with this.w_ValoreCampo
          this.w_ValoreCampo = Alltrim(Substr(Quadri[this.w_Num_Rec],18,8))
          Replace Mod with this.w_ValoreCampo
        endif
      case this.w_StampaModello="S"
        Select * from QuadroBL into cursor __TMP__ order by Mod
        Use in select ("QuadroBL")
        Cp_Chprn("..\aift\exe\query\QuadroBL", , this.oParentObject)
    endcase
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
