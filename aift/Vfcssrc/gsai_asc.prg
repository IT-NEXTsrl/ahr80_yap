* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsai_asc                                                        *
*              Soggetti collettivi non residenti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-07-20                                                      *
* Last revis.: 2011-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsai_asc"))

* --- Class definition
define class tgsai_asc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 761
  Height = 205+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-09-12"
  HelpContextID=109709161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  SOGCOLNR_IDX = 0
  CONTI_IDX = 0
  cFile = "SOGCOLNR"
  cKeySelect = "SCTIPCLF,SCCODCLF"
  cKeyWhere  = "SCTIPCLF=this.w_SCTIPCLF and SCCODCLF=this.w_SCCODCLF"
  cKeyWhereODBC = '"SCTIPCLF="+cp_ToStrODBC(this.w_SCTIPCLF)';
      +'+" and SCCODCLF="+cp_ToStrODBC(this.w_SCCODCLF)';

  cKeyWhereODBCqualified = '"SOGCOLNR.SCTIPCLF="+cp_ToStrODBC(this.w_SCTIPCLF)';
      +'+" and SOGCOLNR.SCCODCLF="+cp_ToStrODBC(this.w_SCCODCLF)';

  cPrg = "gsai_asc"
  cComment = "Soggetti collettivi non residenti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_SCTIPCLF = space(1)
  w_SCCODCLF = space(15)
  w_SCCOGSCN = space(24)
  w_SCNOMSCN = space(20)
  w_SCDTNSCN = ctod('  /  /  ')
  w_SCLCRSCN = space(40)
  w_SCPRNSCN = space(2)
  w_SCSESDOM = space(3)
  w_ANDESCRI = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SOGCOLNR','gsai_asc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsai_ascPag1","gsai_asc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Soggetti collettivi non residenti")
      .Pages(1).HelpContextID = 232856873
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCTIPCLF_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='SOGCOLNR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SOGCOLNR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SOGCOLNR_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SCTIPCLF = NVL(SCTIPCLF,space(1))
      .w_SCCODCLF = NVL(SCCODCLF,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SOGCOLNR where SCTIPCLF=KeySet.SCTIPCLF
    *                            and SCCODCLF=KeySet.SCCODCLF
    *
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SOGCOLNR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SOGCOLNR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SOGCOLNR '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCTIPCLF',this.w_SCTIPCLF  ,'SCCODCLF',this.w_SCCODCLF  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_ANDESCRI = space(40)
        .w_SCTIPCLF = NVL(SCTIPCLF,space(1))
        .w_SCCODCLF = NVL(SCCODCLF,space(15))
          if link_1_4_joined
            this.w_SCCODCLF = NVL(ANCODICE104,NVL(this.w_SCCODCLF,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI104,space(40))
          else
          .link_1_4('Load')
          endif
        .w_SCCOGSCN = NVL(SCCOGSCN,space(24))
        .w_SCNOMSCN = NVL(SCNOMSCN,space(20))
        .w_SCDTNSCN = NVL(cp_ToDate(SCDTNSCN),ctod("  /  /  "))
        .w_SCLCRSCN = NVL(SCLCRSCN,space(40))
        .w_SCPRNSCN = NVL(SCPRNSCN,space(2))
        .w_SCSESDOM = NVL(SCSESDOM,space(3))
        cp_LoadRecExtFlds(this,'SOGCOLNR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST = ctod("  /  /  ")
      .w_SCTIPCLF = space(1)
      .w_SCCODCLF = space(15)
      .w_SCCOGSCN = space(24)
      .w_SCNOMSCN = space(20)
      .w_SCDTNSCN = ctod("  /  /  ")
      .w_SCLCRSCN = space(40)
      .w_SCPRNSCN = space(2)
      .w_SCSESDOM = space(3)
      .w_ANDESCRI = space(40)
      if .cFunction<>"Filter"
        .w_OBTEST = CTOD('01-01-1900')
        .w_SCTIPCLF = 'C'
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_SCCODCLF))
          .link_1_4('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'SOGCOLNR')
    this.DoRTCalc(4,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCTIPCLF_1_2.enabled = i_bVal
      .Page1.oPag.oSCCODCLF_1_4.enabled = i_bVal
      .Page1.oPag.oSCCOGSCN_1_6.enabled = i_bVal
      .Page1.oPag.oSCNOMSCN_1_7.enabled = i_bVal
      .Page1.oPag.oSCDTNSCN_1_9.enabled = i_bVal
      .Page1.oPag.oSCLCRSCN_1_11.enabled = i_bVal
      .Page1.oPag.oSCPRNSCN_1_13.enabled = i_bVal
      .Page1.oPag.oSCSESDOM_1_15.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSCTIPCLF_1_2.enabled = .f.
        .Page1.oPag.oSCCODCLF_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSCTIPCLF_1_2.enabled = .t.
        .Page1.oPag.oSCCODCLF_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SOGCOLNR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPCLF,"SCTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODCLF,"SCCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCOGSCN,"SCCOGSCN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCNOMSCN,"SCNOMSCN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDTNSCN,"SCDTNSCN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCLCRSCN,"SCLCRSCN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPRNSCN,"SCPRNSCN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCSESDOM,"SCSESDOM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    i_lTable = "SOGCOLNR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SOGCOLNR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SOGCOLNR_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SOGCOLNR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SOGCOLNR')
        i_extval=cp_InsertValODBCExtFlds(this,'SOGCOLNR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN"+;
                  ",SCLCRSCN,SCPRNSCN,SCSESDOM "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SCTIPCLF)+;
                  ","+cp_ToStrODBCNull(this.w_SCCODCLF)+;
                  ","+cp_ToStrODBC(this.w_SCCOGSCN)+;
                  ","+cp_ToStrODBC(this.w_SCNOMSCN)+;
                  ","+cp_ToStrODBC(this.w_SCDTNSCN)+;
                  ","+cp_ToStrODBC(this.w_SCLCRSCN)+;
                  ","+cp_ToStrODBC(this.w_SCPRNSCN)+;
                  ","+cp_ToStrODBC(this.w_SCSESDOM)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SOGCOLNR')
        i_extval=cp_InsertValVFPExtFlds(this,'SOGCOLNR')
        cp_CheckDeletedKey(i_cTable,0,'SCTIPCLF',this.w_SCTIPCLF,'SCCODCLF',this.w_SCCODCLF)
        INSERT INTO (i_cTable);
              (SCTIPCLF,SCCODCLF,SCCOGSCN,SCNOMSCN,SCDTNSCN,SCLCRSCN,SCPRNSCN,SCSESDOM  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SCTIPCLF;
                  ,this.w_SCCODCLF;
                  ,this.w_SCCOGSCN;
                  ,this.w_SCNOMSCN;
                  ,this.w_SCDTNSCN;
                  ,this.w_SCLCRSCN;
                  ,this.w_SCPRNSCN;
                  ,this.w_SCSESDOM;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SOGCOLNR_IDX,i_nConn)
      *
      * update SOGCOLNR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SOGCOLNR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SCCOGSCN="+cp_ToStrODBC(this.w_SCCOGSCN)+;
             ",SCNOMSCN="+cp_ToStrODBC(this.w_SCNOMSCN)+;
             ",SCDTNSCN="+cp_ToStrODBC(this.w_SCDTNSCN)+;
             ",SCLCRSCN="+cp_ToStrODBC(this.w_SCLCRSCN)+;
             ",SCPRNSCN="+cp_ToStrODBC(this.w_SCPRNSCN)+;
             ",SCSESDOM="+cp_ToStrODBC(this.w_SCSESDOM)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SOGCOLNR')
        i_cWhere = cp_PKFox(i_cTable  ,'SCTIPCLF',this.w_SCTIPCLF  ,'SCCODCLF',this.w_SCCODCLF  )
        UPDATE (i_cTable) SET;
              SCCOGSCN=this.w_SCCOGSCN;
             ,SCNOMSCN=this.w_SCNOMSCN;
             ,SCDTNSCN=this.w_SCDTNSCN;
             ,SCLCRSCN=this.w_SCLCRSCN;
             ,SCPRNSCN=this.w_SCPRNSCN;
             ,SCSESDOM=this.w_SCSESDOM;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SOGCOLNR_IDX,i_nConn)
      *
      * delete SOGCOLNR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SCTIPCLF',this.w_SCTIPCLF  ,'SCCODCLF',this.w_SCCODCLF  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SOGCOLNR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SOGCOLNR_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SCCODCLF
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SCCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SCTIPCLF;
                     ,'ANCODICE',trim(this.w_SCCODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSCCODCLF_1_4'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SCTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SCCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SCTIPCLF;
                       ,'ANCODICE',this.w_SCCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCLF = space(15)
      endif
      this.w_ANDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ANCODICE as ANCODICE104"+ ",link_1_4.ANDESCRI as ANDESCRI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on SOGCOLNR.SCCODCLF=link_1_4.ANCODICE"+" and SOGCOLNR.SCTIPCLF=link_1_4.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and SOGCOLNR.SCCODCLF=link_1_4.ANCODICE(+)"'+'+" and SOGCOLNR.SCTIPCLF=link_1_4.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCTIPCLF_1_2.RadioValue()==this.w_SCTIPCLF)
      this.oPgFrm.Page1.oPag.oSCTIPCLF_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODCLF_1_4.value==this.w_SCCODCLF)
      this.oPgFrm.Page1.oPag.oSCCODCLF_1_4.value=this.w_SCCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCOGSCN_1_6.value==this.w_SCCOGSCN)
      this.oPgFrm.Page1.oPag.oSCCOGSCN_1_6.value=this.w_SCCOGSCN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCNOMSCN_1_7.value==this.w_SCNOMSCN)
      this.oPgFrm.Page1.oPag.oSCNOMSCN_1_7.value=this.w_SCNOMSCN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDTNSCN_1_9.value==this.w_SCDTNSCN)
      this.oPgFrm.Page1.oPag.oSCDTNSCN_1_9.value=this.w_SCDTNSCN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCLCRSCN_1_11.value==this.w_SCLCRSCN)
      this.oPgFrm.Page1.oPag.oSCLCRSCN_1_11.value=this.w_SCLCRSCN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCPRNSCN_1_13.value==this.w_SCPRNSCN)
      this.oPgFrm.Page1.oPag.oSCPRNSCN_1_13.value=this.w_SCPRNSCN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCSESDOM_1_15.value==this.w_SCSESDOM)
      this.oPgFrm.Page1.oPag.oSCSESDOM_1_15.value=this.w_SCSESDOM
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_17.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_17.value=this.w_ANDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'SOGCOLNR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SCCODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODCLF_1_4.SetFocus()
            i_bnoObbl = !empty(.w_SCCODCLF)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsai_ascPag1 as StdContainer
  Width  = 757
  height = 206
  stdWidth  = 757
  stdheight = 206
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSCTIPCLF_1_2 as StdCombo with uid="BKTEBBQHLY",rtseq=2,rtrep=.f.,left=99,top=12,width=117,height=21;
    , ToolTipText = "Tipo cliente/fornitore di riferimento";
    , HelpContextID = 238780052;
    , cFormVar="w_SCTIPCLF",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSCTIPCLF_1_2.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSCTIPCLF_1_2.GetRadio()
    this.Parent.oContained.w_SCTIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oSCTIPCLF_1_2.SetRadio()
    this.Parent.oContained.w_SCTIPCLF=trim(this.Parent.oContained.w_SCTIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_SCTIPCLF=='C',1,;
      iif(this.Parent.oContained.w_SCTIPCLF=='F',2,;
      0))
  endfunc

  func oSCTIPCLF_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SCCODCLF)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSCCODCLF_1_4 as StdField with uid="COYEXGBMLA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCCODCLF", cQueryName = "SCTIPCLF,SCCODCLF",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 251039380,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=324, Top=12, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SCTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_SCCODCLF"

  func oSCCODCLF_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODCLF_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODCLF_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SCTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SCTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSCCODCLF_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'',this.parent.oContained
  endproc
  proc oSCCODCLF_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SCTIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_SCCODCLF
     i_obj.ecpSave()
  endproc

  add object oSCCOGSCN_1_6 as StdField with uid="KNQFKMQLZY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCCOGSCN", cQueryName = "SCCOGSCN",;
    bObbl = .f. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome soggetto collettivo non residente",;
    HelpContextID = 20541812,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=22, Top=68, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  add object oSCNOMSCN_1_7 as StdField with uid="FUECVCPGXR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCNOMSCN", cQueryName = "SCNOMSCN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome soggetto collettivo non residente",;
    HelpContextID = 26878324,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=256, Top=68, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  add object oSCDTNSCN_1_9 as StdField with uid="TGGJONGBER",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SCDTNSCN", cQueryName = "SCDTNSCN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita soggetto collettivo non residente",;
    HelpContextID = 28213620,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=22, Top=123

  add object oSCLCRSCN_1_11 as StdField with uid="ZRBVSQMYKT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SCLCRSCN", cQueryName = "SCLCRSCN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune o stato estero di nascita soggetto collettivo non residente",;
    HelpContextID = 31326580,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=256, Top=123, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40)

  add object oSCPRNSCN_1_13 as StdField with uid="WAUQFWXHJQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SCPRNSCN", cQueryName = "SCPRNSCN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita soggetto collettivo non residente",;
    HelpContextID = 28131700,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=596, Top=123, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  add object oSCSESDOM_1_15 as StdField with uid="ZRIOQFTLRS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SCSESDOM", cQueryName = "SCSESDOM",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Stato estero del domicilio del soggetto collettivo non residente",;
    HelpContextID = 219123341,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=22, Top=177, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  add object oANDESCRI_1_17 as StdField with uid="GTTIEDOMXC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32475983,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=458, Top=12, InputMask=replicate('X',40)

  add object oStr_1_3 as StdString with uid="SOGTSJLGKY",Visible=.t., Left=7, Top=14,;
    Alignment=1, Width=90, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="QHWZUYZJSV",Visible=.t., Left=221, Top=14,;
    Alignment=1, Width=100, Height=18,;
    Caption="Codice conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="DCKVULBWYI",Visible=.t., Left=256, Top=49,;
    Alignment=0, Width=70, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="OOHQBJBJGE",Visible=.t., Left=22, Top=103,;
    Alignment=0, Width=114, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NYOGTXWYPC",Visible=.t., Left=256, Top=103,;
    Alignment=0, Width=243, Height=18,;
    Caption="Comune o stato estero di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="AXCHIDZYWD",Visible=.t., Left=596, Top=103,;
    Alignment=0, Width=83, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="FGQEXUEEPY",Visible=.t., Left=22, Top=158,;
    Alignment=0, Width=176, Height=18,;
    Caption="Stato estero del domicilio"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="YYBAWELUCA",Visible=.t., Left=22, Top=49,;
    Alignment=0, Width=80, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oBox_1_19 as StdBox with uid="MNIZFWKPRJ",left=1, top=39, width=756,height=167
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsai_asc','SOGCOLNR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCTIPCLF=SOGCOLNR.SCTIPCLF";
  +" and "+i_cAliasName2+".SCCODCLF=SOGCOLNR.SCCODCLF";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
