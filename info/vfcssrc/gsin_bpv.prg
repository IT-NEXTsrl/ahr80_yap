* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bpv                                                        *
*              Interfaccia per prosp. del venduto                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_62]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-24                                                      *
* Last revis.: 2011-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperaz,pTabella,pQUERY
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bpv",oParentObject,m.pOperaz,m.pTabella,m.pQUERY)
return(i_retval)

define class tgsin_bpv as StdBatch
  * --- Local variables
  pOperaz = space(2)
  pTabella = space(50)
  pQUERY = space(20)
  w_COSTMARG = space(1)
  w_LISTINO = space(8)
  w_DATSTA = ctod("  /  /  ")
  w_DADATA = ctod("  /  /  ")
  w_ADATA = ctod("  /  /  ")
  w_DATINV = ctod("  /  /  ")
  w_DATAIN = ctod("  /  /  ")
  w_AGGIORN = space(1)
  w_VALUTA = space(3)
  w_CODESE = space(4)
  w_CAOVAL1 = 0
  w_CAOVAL2 = 0
  w_STVAL = space(1)
  w_VALU2 = space(3)
  w_CAMBIO2 = 0
  w_CAMBIO1 = 0
  w_CAMBIO = 0
  w_CAOESE = 0
  w_DECTOT2 = 0
  w_OREP = space(50)
  w_OQRY = space(50)
  w_ONUME = 0
  w_NUMERO = space(6)
  w_CODART = space(20)
  w_CODART1 = space(20)
  w_GRUMER = space(5)
  w_PAGAM = space(5)
  w_AGENTE = space(5)
  w_ZONA = space(3)
  w_CATCOMM = space(3)
  w_CLIENTE = space(15)
  w_STATO = space(1)
  w_SPEACC = space(1)
  w_DATINV = ctod("  /  /  ")
  w_FLPUNT = space(1)
  w_CODMAR = space(5)
  w_CODFAM = space(5)
  w_CODCAT = space(5)
  w_CODTRA = space(2)
  w_CODFA = space(2)
  w_CODRIC = space(2)
  w_CODNOT = space(2)
  w_MESSAGE = space(200)
  w_CODVET = space(5)
  * --- WorkFile variables
  PAR_INFO_idx=0
  TMPVEND1_idx=0
  INVENTAR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch che legge dai parametri infolink (INPARVEN) le impostazioni per il lancio
    *     del batch del prospetto del venduto (GSVE_BPV)
    * --- Leggo da INPARVEN le impostazioni necessarie all'elaborazione
    * --- Read from PAR_INFO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_INFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_INFO_idx,2],.t.,this.PAR_INFO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "INLISTIN,INAGGIOR,INCAOVAL,INCAMBIO,INDECTOT,INNUMERO,INFLPUNT,INCODESE,INSPEACC"+;
        " from "+i_cTable+" PAR_INFO where ";
            +"INCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        INLISTIN,INAGGIOR,INCAOVAL,INCAMBIO,INDECTOT,INNUMERO,INFLPUNT,INCODESE,INSPEACC;
        from (i_cTable) where;
            INCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LISTINO = NVL(cp_ToDate(_read_.INLISTIN),cp_NullValue(_read_.INLISTIN))
      this.w_AGGIORN = NVL(cp_ToDate(_read_.INAGGIOR),cp_NullValue(_read_.INAGGIOR))
      this.w_CAOVAL2 = NVL(cp_ToDate(_read_.INCAOVAL),cp_NullValue(_read_.INCAOVAL))
      this.w_CAMBIO = NVL(cp_ToDate(_read_.INCAMBIO),cp_NullValue(_read_.INCAMBIO))
      this.w_CAMBIO1 = NVL(cp_ToDate(_read_.INCAMBIO),cp_NullValue(_read_.INCAMBIO))
      this.w_CAMBIO2 = NVL(cp_ToDate(_read_.INCAMBIO),cp_NullValue(_read_.INCAMBIO))
      this.w_CAOESE = NVL(cp_ToDate(_read_.INCAOVAL),cp_NullValue(_read_.INCAOVAL))
      this.w_DECTOT2 = NVL(cp_ToDate(_read_.INDECTOT),cp_NullValue(_read_.INDECTOT))
      this.w_NUMERO = NVL(cp_ToDate(_read_.INNUMERO),cp_NullValue(_read_.INNUMERO))
      this.w_FLPUNT = NVL(cp_ToDate(_read_.INFLPUNT),cp_NullValue(_read_.INFLPUNT))
      this.w_CODESE = NVL(cp_ToDate(_read_.INCODESE),cp_NullValue(_read_.INCODESE))
      this.w_SPEACC = NVL(cp_ToDate(_read_.INSPEACC),cp_NullValue(_read_.INSPEACC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_AGGIORN) OR (this.w_AGGIORN $ "CMPA-CMPP-COUL-COST-LICO-FICO-LISC" AND; 
 (EMPTY(this.w_NUMERO) OR EMPTY(this.w_LISTINO))) OR (this.w_AGGIORN $ "LIST" AND EMPTY(this.w_LISTINO))
      ah_ErrorMsg("Specificare i criteri di costificazione nei parametri InfoLink%0Occorre specificare tali parametri per le aziende selezionate",,"")
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\info\exe\gsin1bpv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      i_retcode = 'stop'
      return
    endif
    if this.w_FLPUNT="S"
      * --- Read from INVENTAR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INVENTAR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "INDATINV"+;
          " from "+i_cTable+" INVENTAR where ";
              +"INNUMINV = "+cp_ToStrODBC(this.w_NUMERO);
              +" and INCODESE = "+cp_ToStrODBC(this.w_CODESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          INDATINV;
          from (i_cTable) where;
              INNUMINV = this.w_NUMERO;
              and INCODESE = this.w_CODESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATINV = NVL(cp_ToDate(_read_.INDATINV),cp_NullValue(_read_.INDATINV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DADATA = this.w_DATINV+1
      this.w_DATAIN = this.w_DADATA
      if This.oParentObject.oParentObject.w_DATAFIN >= this.w_DATAIN
        this.w_ADATA = This.oParentObject.oParentObject.w_DATAFIN
      endif
    else
      this.w_DADATA = THIS.oParentObject.oParentObject.w_DATAINI
      this.w_ADATA = This.oParentObject.oParentObject.w_DATAFIN
    endif
    * --- Valorizzo le variabili per il GSVE_BPV...
    this.w_COSTMARG = "S"
    this.w_STVAL = "C"
    this.w_VALU2 = g_PERVAL
    this.w_OQRY = "..\INFO\EXE\QUERY\GSIN_BPV.VQR"
    this.w_STATO = "N"
    this.w_SPEACC = iif(empty(nvl(this.w_SPEACC,"")), "N", this.w_SPEACC)
    this.w_CODTRA = "DT"
    this.w_CODFA = "FA"
    this.w_CODRIC = "RF"
    this.w_CODNOT = "NC"
    this.w_DATSTA = i_datsys
    this.w_CODVET = Space(5)
    * --- Lancio il Batch GSVE_BPV (Elaborazione Stampa Prospetto del Venduto)
    do GSVE_BPV with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pOperaz,pTabella,pQUERY)
    this.pOperaz=pOperaz
    this.pTabella=pTabella
    this.pQUERY=pQUERY
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_INFO'
    this.cWorkTables[2]='*TMPVEND1'
    this.cWorkTables[3]='INVENTAR'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperaz,pTabella,pQUERY"
endproc
