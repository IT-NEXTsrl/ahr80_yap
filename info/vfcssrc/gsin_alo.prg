* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_alo                                                        *
*              Log esportazioni                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-25                                                      *
* Last revis.: 2009-06-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsin_alo"))

* --- Class definition
define class tgsin_alo as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 501
  Height = 314+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-30"
  HelpContextID=41308311
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  INF_LOG_IDX = 0
  cFile = "INF_LOG"
  cKeySelect = "LODGRUPP,LOCODUTE,LODATAES,LOORAESE"
  cKeyWhere  = "LODGRUPP=this.w_LODGRUPP and LOCODUTE=this.w_LOCODUTE and LODATAES=this.w_LODATAES and LOORAESE=this.w_LOORAESE"
  cKeyWhereODBC = '"LODGRUPP="+cp_ToStrODBC(this.w_LODGRUPP)';
      +'+" and LOCODUTE="+cp_ToStrODBC(this.w_LOCODUTE)';
      +'+" and LODATAES="+cp_ToStrODBC(this.w_LODATAES,"D")';
      +'+" and LOORAESE="+cp_ToStrODBC(this.w_LOORAESE)';

  cKeyWhereODBCqualified = '"INF_LOG.LODGRUPP="+cp_ToStrODBC(this.w_LODGRUPP)';
      +'+" and INF_LOG.LOCODUTE="+cp_ToStrODBC(this.w_LOCODUTE)';
      +'+" and INF_LOG.LODATAES="+cp_ToStrODBC(this.w_LODATAES,"D")';
      +'+" and INF_LOG.LOORAESE="+cp_ToStrODBC(this.w_LOORAESE)';

  cPrg = "gsin_alo"
  cComment = "Log esportazioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LODGRUPP = space(15)
  w_LOCODUTE = 0
  w_LODATAES = ctod('  /  /  ')
  w_LOORAESE = space(8)
  w_LOORAFIN = space(8)
  w_LOLOGESE = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INF_LOG','gsin_alo')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsin_aloPag1","gsin_alo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Log")
      .Pages(1).HelpContextID = 41759926
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLODGRUPP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='INF_LOG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INF_LOG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INF_LOG_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_LODGRUPP = NVL(LODGRUPP,space(15))
      .w_LOCODUTE = NVL(LOCODUTE,0)
      .w_LODATAES = NVL(LODATAES,ctod("  /  /  "))
      .w_LOORAESE = NVL(LOORAESE,space(8))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from INF_LOG where LODGRUPP=KeySet.LODGRUPP
    *                            and LOCODUTE=KeySet.LOCODUTE
    *                            and LODATAES=KeySet.LODATAES
    *                            and LOORAESE=KeySet.LOORAESE
    *
    i_nConn = i_TableProp[this.INF_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INF_LOG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INF_LOG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INF_LOG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LODGRUPP',this.w_LODGRUPP  ,'LOCODUTE',this.w_LOCODUTE  ,'LODATAES',this.w_LODATAES  ,'LOORAESE',this.w_LOORAESE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LODGRUPP = NVL(LODGRUPP,space(15))
        .w_LOCODUTE = NVL(LOCODUTE,0)
        .w_LODATAES = NVL(cp_ToDate(LODATAES),ctod("  /  /  "))
        .w_LOORAESE = NVL(LOORAESE,space(8))
        .w_LOORAFIN = NVL(LOORAFIN,space(8))
        .w_LOLOGESE = NVL(LOLOGESE,space(0))
        cp_LoadRecExtFlds(this,'INF_LOG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LODGRUPP = space(15)
      .w_LOCODUTE = 0
      .w_LODATAES = ctod("  /  /  ")
      .w_LOORAESE = space(8)
      .w_LOORAFIN = space(8)
      .w_LOLOGESE = space(0)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'INF_LOG')
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLODGRUPP_1_1.enabled = i_bVal
      .Page1.oPag.oLOCODUTE_1_2.enabled = i_bVal
      .Page1.oPag.oLODATAES_1_4.enabled = i_bVal
      .Page1.oPag.oLOORAESE_1_6.enabled = i_bVal
      .Page1.oPag.oLOLOGESE_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLODGRUPP_1_1.enabled = .f.
        .Page1.oPag.oLOCODUTE_1_2.enabled = .f.
        .Page1.oPag.oLODATAES_1_4.enabled = .f.
        .Page1.oPag.oLOORAESE_1_6.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLODGRUPP_1_1.enabled = .t.
        .Page1.oPag.oLOCODUTE_1_2.enabled = .t.
        .Page1.oPag.oLODATAES_1_4.enabled = .t.
        .Page1.oPag.oLOORAESE_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'INF_LOG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INF_LOG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LODGRUPP,"LODGRUPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LOCODUTE,"LOCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LODATAES,"LODATAES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LOORAESE,"LOORAESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LOORAFIN,"LOORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LOLOGESE,"LOLOGESE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INF_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
    i_lTable = "INF_LOG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INF_LOG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSIN_SLO with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.INF_LOG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INF_LOG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INF_LOG')
        i_extval=cp_InsertValODBCExtFlds(this,'INF_LOG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LODGRUPP,LOCODUTE,LODATAES,LOORAESE,LOORAFIN"+;
                  ",LOLOGESE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LODGRUPP)+;
                  ","+cp_ToStrODBC(this.w_LOCODUTE)+;
                  ","+cp_ToStrODBC(this.w_LODATAES)+;
                  ","+cp_ToStrODBC(this.w_LOORAESE)+;
                  ","+cp_ToStrODBC(this.w_LOORAFIN)+;
                  ","+cp_ToStrODBC(this.w_LOLOGESE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INF_LOG')
        i_extval=cp_InsertValVFPExtFlds(this,'INF_LOG')
        cp_CheckDeletedKey(i_cTable,0,'LODGRUPP',this.w_LODGRUPP,'LOCODUTE',this.w_LOCODUTE,'LODATAES',this.w_LODATAES,'LOORAESE',this.w_LOORAESE)
        INSERT INTO (i_cTable);
              (LODGRUPP,LOCODUTE,LODATAES,LOORAESE,LOORAFIN,LOLOGESE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LODGRUPP;
                  ,this.w_LOCODUTE;
                  ,this.w_LODATAES;
                  ,this.w_LOORAESE;
                  ,this.w_LOORAFIN;
                  ,this.w_LOLOGESE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.INF_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.INF_LOG_IDX,i_nConn)
      *
      * update INF_LOG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'INF_LOG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LOORAFIN="+cp_ToStrODBC(this.w_LOORAFIN)+;
             ",LOLOGESE="+cp_ToStrODBC(this.w_LOLOGESE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'INF_LOG')
        i_cWhere = cp_PKFox(i_cTable  ,'LODGRUPP',this.w_LODGRUPP  ,'LOCODUTE',this.w_LOCODUTE  ,'LODATAES',this.w_LODATAES  ,'LOORAESE',this.w_LOORAESE  )
        UPDATE (i_cTable) SET;
              LOORAFIN=this.w_LOORAFIN;
             ,LOLOGESE=this.w_LOLOGESE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INF_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.INF_LOG_IDX,i_nConn)
      *
      * delete INF_LOG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LODGRUPP',this.w_LODGRUPP  ,'LOCODUTE',this.w_LOCODUTE  ,'LODATAES',this.w_LODATAES  ,'LOORAESE',this.w_LOORAESE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INF_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLODGRUPP_1_1.value==this.w_LODGRUPP)
      this.oPgFrm.Page1.oPag.oLODGRUPP_1_1.value=this.w_LODGRUPP
    endif
    if not(this.oPgFrm.Page1.oPag.oLOCODUTE_1_2.value==this.w_LOCODUTE)
      this.oPgFrm.Page1.oPag.oLOCODUTE_1_2.value=this.w_LOCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oLODATAES_1_4.value==this.w_LODATAES)
      this.oPgFrm.Page1.oPag.oLODATAES_1_4.value=this.w_LODATAES
    endif
    if not(this.oPgFrm.Page1.oPag.oLOORAESE_1_6.value==this.w_LOORAESE)
      this.oPgFrm.Page1.oPag.oLOORAESE_1_6.value=this.w_LOORAESE
    endif
    if not(this.oPgFrm.Page1.oPag.oLOORAFIN_1_7.value==this.w_LOORAFIN)
      this.oPgFrm.Page1.oPag.oLOORAFIN_1_7.value=this.w_LOORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLOLOGESE_1_9.value==this.w_LOLOGESE)
      this.oPgFrm.Page1.oPag.oLOLOGESE_1_9.value=this.w_LOLOGESE
    endif
    cp_SetControlsValueExtFlds(this,'INF_LOG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  func CanAdd()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Non � consentito il caricamento di log"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsin_aloPag1 as StdContainer
  Width  = 497
  height = 316
  stdWidth  = 497
  stdheight = 316
  resizeXpos=241
  resizeYpos=278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLODGRUPP_1_1 as StdField with uid="SSMOACMWXJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LODGRUPP", cQueryName = "LODGRUPP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo di regole",;
    HelpContextID = 52304634,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=121, Top=15, InputMask=replicate('X',15)

  add object oLOCODUTE_1_2 as StdField with uid="GVVRQEIJTJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LOCODUTE", cQueryName = "LODGRUPP,LOCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 66464517,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=405, Top=15

  add object oLODATAES_1_4 as StdField with uid="XBAASAEZSM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LODATAES", cQueryName = "LODGRUPP,LOCODUTE,LODATAES",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di esecuzione del gruppo di regole",;
    HelpContextID = 117709559,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=121, Top=44

  add object oLOORAESE_1_6 as StdField with uid="IKZBHBASNB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LOORAESE", cQueryName = "LODGRUPP,LOCODUTE,LODATAES,LOORAESE",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora di esecuzione del gruppo di regole",;
    HelpContextID = 69364485,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=405, Top=44, InputMask=replicate('X',8)

  add object oLOORAFIN_1_7 as StdField with uid="IIHQQAFEGR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LOORAFIN", cQueryName = "LOORAFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora fine esecuzione del gruppo di regole",;
    HelpContextID = 215848196,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=405, Top=67, InputMask=replicate('X',8)

  add object oLOLOGESE_1_9 as StdMemo with uid="WOCEFBNKMV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LOLOGESE", cQueryName = "LOLOGESE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Log",;
    HelpContextID = 63281925,;
   bGlobalFont=.t.,;
    Height=217, Width=493, Left=1, Top=98, tabstop = .f., readonly = .t.

  add object oStr_1_3 as StdString with uid="DFTBTHGNGE",Visible=.t., Left=37, Top=17,;
    Alignment=1, Width=78, Height=18,;
    Caption="Cod. gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="DUWXTKNDND",Visible=.t., Left=5, Top=47,;
    Alignment=1, Width=110, Height=18,;
    Caption="Data esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="TBTBPWDRFZ",Visible=.t., Left=236, Top=47,;
    Alignment=1, Width=165, Height=18,;
    Caption="Ora inizio esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="JASSXULMJG",Visible=.t., Left=5, Top=78,;
    Alignment=0, Width=217, Height=18,;
    Caption="Messaggi importazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ZWUBMBGKNQ",Visible=.t., Left=308, Top=17,;
    Alignment=1, Width=93, Height=18,;
    Caption="Cod. utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VQLJLYZSNL",Visible=.t., Left=242, Top=70,;
    Alignment=1, Width=159, Height=18,;
    Caption="Ora fine esecuzione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="ZLROCRZJCJ",left=2, top=95, width=493,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_alo','INF_LOG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LODGRUPP=INF_LOG.LODGRUPP";
  +" and "+i_cAliasName2+".LOCODUTE=INF_LOG.LOCODUTE";
  +" and "+i_cAliasName2+".LODATAES=INF_LOG.LODATAES";
  +" and "+i_cAliasName2+".LOORAESE=INF_LOG.LOORAESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
