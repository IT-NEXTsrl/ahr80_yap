* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bas                                                        *
*              Allineamento struttura dw                                       *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_103]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-11                                                      *
* Last revis.: 2014-11-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperaz,pTabella,pQUERY
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bas",oParentObject,m.pOperaz,m.pTabella,m.pQUERY)
return(i_retval)

define class tgsin_bas as StdBatch
  * --- Local variables
  pOperaz = space(2)
  pTabella = space(50)
  pQUERY = space(20)
  w_VarError = space(255)
  w_ODBC = space(25)
  w_STRINGA = space(90)
  w_CRIPTATA = space(1)
  w_CONNECT = 0
  w_CONNECTION = space(25)
  w_fieldsq = space(0)
  w_FIELDST = space(255)
  w_VALORI = space(255)
  w_WHERE = space(255)
  w_WHERE2 = space(255)
  w_DB = space(10)
  w_MULTIAZI = space(1)
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_CAMPICON = space(150)
  w_CAMPICON2 = space(254)
  w_NOMECAMP = space(50)
  w_CONCATOK = .f.
  w_COLONNA = space(254)
  w_COLONUPD = space(254)
  w_DIMFIELD = 0
  w_LOOP = 0
  w_INDEXFIELD = 0
  w_LUNMEMO = 0
  w_CMDSQL = space(254)
  w_CMDSQL_MODCOL = space(254)
  w_CMDSQL_FIELD = space(254)
  w_LUNMEMO = 0
  w_LUNMEMO = 0
  w_LUNMEMO = 0
  w_CMD = space(254)
  w_NUMINDX = 0
  w_CAMPINDX = space(150)
  w_INDEXCRE = .f.
  w_TOTFIELD = 0
  * --- WorkFile variables
  INF_TAB_idx=0
  INF_TAAZ_idx=0
  INF_GRUD_idx=0
  INF_INDI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- La routine allinea la struttura attuale delle tabelle _DW con l'eventuale nuova struttura
    Messaggio=""
    this.w_DATAINI = this.oParentObject.oParentObject.w_DATAINI
    this.w_DATAFIN = this.oParentObject.oParentObject.w_DATAFIN
    this.oParentObject.w_RAGSOC = alltrim(this.oParentObject.w_RAGSOC)
    * --- Inizializzazione variabili gestione campi di concatenazione in tabelle multiazienda
    this.w_CAMPICON2 = ""
    this.w_NOMECAMP = ""
    this.w_CONCATOK = .T.
    this.oParentObject.w_errore = ""
    * --- Blocco istruzioni
    * --- Eseguo la connessione al Database
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Leggo informazione di 'multiazienda' sulla tabella
    * --- Read from INF_TAB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_TAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATMULTAZ"+;
        " from "+i_cTable+" INF_TAB where ";
            +"ATNTABLE = "+cp_ToStrODBC(this.pTABELLA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATMULTAZ;
        from (i_cTable) where;
            ATNTABLE = this.pTABELLA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MULTIAZI = NVL(cp_ToDate(_read_.ATMULTAZ),cp_NullValue(_read_.ATMULTAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se tabella multiazienda leggo i campi da concatenare, e aggiungo il carattere 'Z' ad
    *     ognuno di essi
    if this.w_MULTIAZI = "S"
      * --- Read from INF_TAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCAMCON"+;
          " from "+i_cTable+" INF_TAB where ";
              +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCAMCON;
          from (i_cTable) where;
              ATNTABLE = this.pTabella;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAMPICON = NVL(cp_ToDate(_read_.ATCAMCON),cp_NullValue(_read_.ATCAMCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      i=1
      do while ! empty(alltrim(this.w_CAMPICON))
        dimension FIELDLIST (i)
        if "," $ this.w_CAMPICON
          this.w_NOMECAMP = substr(this.w_CAMPICON,1,at(",",this.w_CAMPICON)-1)+"Z"
          this.w_CAMPICON = substr(this.w_CAMPICON,at(",",this.w_CAMPICON)+1)
        else
          this.w_NOMECAMP = alltrim(this.w_CAMPICON)+"Z"
          this.w_CAMPICON = ""
        endif
        this.w_CAMPICON2 = this.w_CAMPICON2+this.w_NOMECAMP+","
        store alltrim(this.w_NOMECAMP) to FIELDLIST(i)
        i=i+1
      enddo
    endif
    * --- Verifico se la tabella � gi� presente nel database
    R = cp_SQL(this.w_CONNECT,"Select *  from "+this.ptabella+" where 1=0",.f.,.f.,.t.)
    USE IN SELECT("SQLRESULT")
    if R > 0
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_02A20300
      bErr_02A20300=bTrsErr
      this.Try_02A20300()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if empty(this.oParentObject.w_errore)
          this.oParentObject.w_errore = ah_MsgFormat("Errore generico in allineamento struttura tabelle%0%1",Message())
        endif
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_02A20300
      * --- End
    else
      * --- Creo la tabella nel database
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Mi disconetto dalla nuova connessione.
      Sqldisconnect(this.w_CONNECT)
    endif
    if used("CursTab")
      Select CursTab 
 use
    endif
    if used(this.oParentObject.w_RENOMCUR)
      Select(this.oParentObject.w_RENOMCUR) 
 use
    endif
  endproc
  proc Try_02A20300()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Ricavo la struttura della tabella dalla query di creazione
    Declare STRUCTAB (1,5)
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ricavo la struttura della tabella dalla tabella attuale
    Declare STRUCTABT (1,5)
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Verifico le strutture ottenute
    this.w_COLONNA = ""
    this.w_COLONUPD = ""
    this.w_TOTFIELD = alen(STRUCTABT,1)
    this.w_LOOP = 1
    do while this.w_LOOP<=ALEN(STRUCTAB,1)
      this.w_NOMECAMP = STRUCTAB( this.w_LOOP ,1)
      this.w_INDEXFIELD = ASCAN(STRUCTABT,this.w_NOMECAMP,1,this.w_TOTFIELD,1, 15)
      if this.w_INDEXFIELD=0
        * --- Devo inserire nella tabella il nuovo campo
        this.w_COLONNA = this.w_COLONNA + STRUCTAB( this.w_LOOP ,1)
        if STRUCTAB( this.w_LOOP ,2) == "M"
          this.w_LUNMEMO = IIF(TYPE("G_INFOMEMOLENGTH")<>"U", G_INFOMEMOLENGTH, IIF(this.w_DB<>"Oracle", 4000, 2000)) 
          this.w_COLONNA = this.w_COLONNA+" "+db_FieldType(this.w_DB,"C",this.w_LUNMEMO,0)+", "
        else
          this.w_COLONNA = this.w_COLONNA +" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB( this.w_LOOP ,2)$"I-B-F","N",STRUCTAB(this.w_LOOP,2)),STRUCTAB( this.w_LOOP ,3),STRUCTAB( this.w_LOOP ,4))+", "
        endif
        this.w_COLONUPD = this.w_COLONUPD + STRUCTAB( this.w_LOOP ,1)
        this.w_DIMFIELD = STRUCTAB( this.w_LOOP ,3)
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Il campo � gi� presente, controllo se devo eseguire alter column
        if STRUCTAB( this.w_LOOP,2) <> STRUCTABT(this.w_INDEXFIELD, 2) Or STRUCTAB( this.w_LOOP, 3) <> STRUCTABT( this.w_INDEXFIELD, 3) Or STRUCTAB( this.w_LOOP, 4) <> STRUCTABT( this.w_INDEXFIELD, 4)
          this.w_CMDSQL_FIELD = db_FieldType(this.w_DB, STRUCTAB(this.w_LOOP, 2), STRUCTAB(this.w_LOOP, 3), STRUCTAB(this.w_LOOP, 4))
          this.w_CMDSQL_MODCOL = db_ModifyColumn(this.w_NOMECAMP, this.w_CMDSQL_FIELD, this.w_DB, this.w_CONNECT)
          if !Empty(this.w_CMDSQL_MODCOL) AND !Empty(this.w_CMDSQL_FIELD)
            this.w_CMDSQL = "alter table "+ this.pTabella + " " + this.w_CMDSQL_MODCOL
            righe = sqlexec(this.w_CONNECT, this.w_CMDSQL)
            if RIGHE < 0
              this.oParentObject.w_errore = ah_MsgFormat("Errore in allineamento struttura tabella%0%1",Message())
              * --- Raise
              i_Error=ah_MsgFormat("Errore in allineamento struttura tabella")
              return
            endif
          endif
        endif
      endif
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Verifico la presenza dei campi multiaziendali nella tabella
    if ! empty(this.w_fieldsq)
      ALINES(STRUQUE1, this.w_fieldsq,3,",")
      this.w_LOOP = 1
      do while this.w_LOOP<= ALEN(STRUQUE1,1)
        this.w_NOMECAMP = STRUQUE1( this.w_LOOP )
        if ASCAN(STRUCTABT,this.w_NOMECAMP,-1,-1,-1,15)=0 AND NOT EMPTY(this.w_NOMECAMP)
          * --- Devo inserire nella tabella il nuovo campo
          if rightc(this.w_NOMECAMP,1)="Z"
            posiz = ASCAN(STRUCTABT,alltrim(substr(this.w_NOMECAMP,1,len(this.w_NOMECAMP)-1)),-1,-1,-1,15)
            if posiz <> 0
              this.w_COLONNA = this.w_COLONNA+this.w_NOMECAMP
              if STRUCTAB( this.w_LOOP ,2) == "M"
                this.w_LUNMEMO = IIF(TYPE("G_INFOMEMOLENGTH")<>"U", G_INFOMEMOLENGTH, IIF(this.w_DB<>"Oracle", 4000, 2000)) 
                this.w_COLONNA = this.w_COLONNA+" "+db_FieldType(this.w_DB,"C",this.w_LUNMEMO,0)+", "
              else
                this.w_COLONNA = this.w_COLONNA +" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTABT(posiz,2)$"I-B-F","N",STRUCTABT(posiz,2)),STRUCTABT(posiz,3)+5,STRUCTABT(posiz,4))+", "
              endif
              this.w_COLONUPD = this.w_COLONUPD + this.w_NOMECAMP
              this.w_DIMFIELD = STRUCTABT(posiz,3)
              posiz = ASCAN(STRUCTAB,alltrim(substr(this.w_NOMECAMP,1,len(this.w_NOMECAMP)-1)),-1,-1,-1,9)
              * --- Verifico il tipo del campo ed assegno il relativo valore di default.
              if inlist(STRUCTAB(posiz,2),"C","M")
                this.w_COLONUPD = this.w_COLONUPD+" = '"+REPLICATE("@",this.w_DIMFIELD)+"'"+", "
              endif
              if inlist(STRUCTAB(posiz,2),"N","I","F","B")
                this.w_COLONUPD = this.w_COLONUPD+" = 0"+", "
              endif
              if inlist(STRUCTAB(posiz,2) ,"D","T")
                this.w_COLONUPD = this.w_COLONUPD+" = "+cp_ToStrODBC(i_inidat)+", "
              endif
            endif
          endif
          do case
            case this.w_NOMECAMP = "CODAZIEN"
              this.w_COLONNA = this.w_COLONNA + "CODAZIEN" + " " + db_FieldType(this.w_DB,"C",5,0) + ", "
              this.w_COLONUPD = this.w_COLONUPD + this.w_NOMECAMP+" = '"+REPLICATE("@",5)+"'"+", "
            case this.w_NOMECAMP = "DESAZIEN"
              this.w_COLONNA = this.w_COLONNA + "DESAZIEN" + " " + db_FieldType(this.w_DB,"C",80,0) + ", "
              this.w_COLONUPD = this.w_COLONUPD + this.w_NOMECAMP+ " = '"+ REPLICATE("@",40)+"'"+", "
          endcase
        endif
        this.w_LOOP = this.w_LOOP + 1
      enddo
    endif
    * --- Inserisco le nuove colonne nella tabella
    if ! empty(this.w_COLONNA)
      this.w_COLONNA = left(this.w_COLONNA,len(this.w_COLONNA)-2)
      * --- Modifico la sintassi per i vari tipi di DB
      * --- Oracle
      if Upper(this.w_DB)="ORACLE" and occurs(",",this.w_COLONNA) > 0
        this.w_COLONNA = "("+this.w_COLONNA+")"
      endif
      * --- DB2
      if Upper(this.w_DB)="DB2" and occurs(",",this.w_COLONNA) > 0
        this.w_COLONNA = STRTRAN(this.w_COLONNA, ",", " add")
      endif
      * --- Inserisco nella tabella i nuovi campi 
      righe = sqlexec(this.w_CONNECT,"Alter table "+this.pTabella+" add "+this.w_COLONNA)
      if RIGHE < 0
        this.oParentObject.w_errore = ah_MsgFormat("Errore in allineamento struttura tabella%0%1",Message())
        * --- Raise
        i_Error=ah_MsgFormat("Errore in allineamento struttura tabella")
        return
      endif
      * --- Aggiorno i nuovi campi inseriti
      this.w_COLONUPD = left(this.w_COLONUPD,len(this.w_COLONUPD)-2)
      righe = sqlexec(this.w_CONNECT,"Update "+this.pTabella+" set "+this.w_COLONUPD)
      if RIGHE < 0
        this.oParentObject.w_errore = ah_MsgFormat("Errore in aggiornamento struttura tabella%0%1",Message())
        * --- Raise
        i_Error=ah_MsgFormat("Errore in aggiornamento struttura tabella")
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_MSG = ah_MsgFormat("Allineamento completato con successo")
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguo la connessione al database se diverso da quello del gestionale
    * --- Try
    local bErr_02A9C218
    bErr_02A9C218=bTrsErr
    this.Try_02A9C218()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_02A9C218
    * --- End
  endproc
  proc Try_02A9C218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifico il nome della tabella nel nostro standard
    this.pTabella = alltrim(this.pTabella)+"_DW"
    * --- Acquisisco il nome dell'ODBC o la stringa di connessione relativo alla tabella passata come parametro (pTabella)
    * --- Prima controllo se esiste una connessione di "default" per la tabella inerente  all'azienda in uso
    * --- Read from INF_TAAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TACOODBC,ATCOSTRI,ATCRIPTE"+;
        " from "+i_cTable+" INF_TAAZ where ";
            +"TACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TACOODBC,ATCOSTRI,ATCRIPTE;
        from (i_cTable) where;
            TACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
      this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
      this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo nell'archivio Tabelle / Connessioni 
      * --- Read from INF_TAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAB where ";
              +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              ATNTABLE = this.pTabella;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.ATCOODBC),cp_NullValue(_read_.ATCOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo se esiste per la tabella una connessione globale (cio� indipendente dall'azienda)
      * --- Read from INF_TAAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TACOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAAZ where ";
              +"TACODAZI = "+cp_ToStrODBC(space(5));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TACOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              TACODAZI = space(5);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Database  diverso da quello usato dalla procedura.
      *     Creo una nuova connessione
      if this.w_CRIPTATA = "C"
        this.w_STRINGA = CifraCnf( ALLTRIM(this.w_STRINGA) , "D" )
      endif
      this.w_CONNECTION = alltrim(this.w_ODBC + this.w_STRINGA)
      this.w_CONNECT = cp_openDatabaseConnection(this.w_connection)
      this.w_DB = SQLXGetDatabaseType(this.w_CONNECT)
    else
      * --- Stessa connessione della procedura
      this.w_CONNECT = i_TableProp[this.INF_GRUD_idx,3]
      this.w_DB = SQLXGetDatabaseType(this.w_CONNECT)
    endif
    if this.w_CONNECT <= 0
      ah_ErrorMsg("Connessione fallita","!","")
      this.oParentObject.w_errore = message()
      i_retcode = 'stop'
      return
    else
      ah_Msg("Connessione riuscita...",.T.)
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione Tabella 
    * --- Try
    local bErr_00E1E138
    bErr_00E1E138=bTrsErr
    this.Try_00E1E138()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if Messaggio="Errata"
        Messaggio=""
        this.oParentObject.w_errore = ah_MsgFormat("Errore nella creazione della tabella %1%0Il campo %2 ha un tipo non riconosciuto",alltrim(this.pTabella),ALLTRIM(this.w_VarError))
      else
        if messaggio= "Query_vuota"
          Messaggio=""
          this.oParentObject.w_errore = ah_MsgFormar("Errore nella creazione della tabella %1%0Query vuota",iif(alltrim(this.pTabella)<>"_DW",alltrim(this.pTabella),"???"+alltrim(this.pTabella)))
        else
          this.oParentObject.w_errore = ah_MsgFormat("Errore nella creazione della tabella %1%0%2",alltrim(this.pTabella),message())
        endif
      endif
      * --- accept error
      bTrsErr=.f.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_00E1E138
    * --- End
  endproc
  proc Try_00E1E138()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    Declare STRUCTAB (1,5)
    this.w_fieldsq = ""
    if not(empty(this.pquery))
      vq_exec(this.pQuery,this,"CursTab")
    else
      Messaggio = "Query_vuota"
      * --- Raise
      i_Error=ah_MsgFormat("Query vuota")
      return
    endif
    if Used("CursTab")
      Afields(STRUCTAB,"CursTab")
      for nfieldsq = 1 to alen(STRUCTAB,1) 
      i_TmpErr=on("ERROR") 
 ON ERROR Messaggio = "Errata"
      this.w_VarError = STRUCTAB(nfieldsq,1)
      this.w_fieldsq = this.w_fieldsq + STRUCTAB(nfieldsq,1)
      if STRUCTAB(nfieldsq,2) == "M"
        this.w_LUNMEMO = IIF(TYPE("G_INFOMEMOLENGTH")<>"U", G_INFOMEMOLENGTH, IIF(this.w_DB<>"Oracle", 4000, 2000)) 
        this.w_FIELDSQ = this.w_FIELDSQ+" "+db_FieldType(this.w_DB,"C",this.w_LUNMEMO,0)+", "
      else
        this.w_fieldsq = this.w_fieldsq+" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(nfieldsq,2)$"I-B-F","N",STRUCTAB(nfieldsq,2)),STRUCTAB(nfieldsq,3),STRUCTAB(nfieldsq,4))+", "
      endif
      ON ERROR &i_TmpErr
      if Messaggio="Errata"
        * --- Raise
        i_Error=ah_MsgFormat("Errore creazione tabella")
        return
      endif
      endfor
      if this.w_MULTIAZI="S"
        this.w_fieldsq = this.w_fieldsq + "CODAZIEN" + " " + db_FieldType(this.w_DB,"C",5,0) + ", "
        this.w_fieldsq = this.w_fieldsq + "DESAZIEN" + " " + db_FieldType(this.w_DB,"C",80,0) + ", "
        * --- Verifico la correttezza dei campi di concatenazione
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
          * --- Accodo i campi di concatenazione
          i=1
          do while i <= alen(FIELDLIST)
            * --- Calcolo l'effettivo numero di riga
            posiz = ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1,15) 
            this.w_fieldsq = this.w_fieldsq + FIELDLIST(i)
            if STRUCTAB(posiz,2) == "M"
              this.w_LUNMEMO = IIF(TYPE("G_INFOMEMOLENGTH")<>"U", G_INFOMEMOLENGTH, IIF(this.w_DB<>"Oracle", 4000, 2000)) 
              this.w_FIELDSQ = this.w_FIELDSQ+" "+db_FieldType(this.w_DB,"C",this.w_LUNMEMO,0)+", "
            else
              this.w_fieldsq = this.w_fieldsq+" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(posiz,2)$"I-B-F","N",STRUCTAB(posiz,2)),STRUCTAB(posiz,3)+5,STRUCTAB(posiz,4))+", "
            endif
            i=i+1
          enddo
        endif
      endif
      this.w_fieldsq = left(this.w_fieldsq,len(this.w_fieldsq)-2)
      righe = sqlexec(this.w_CONNECT,"Create table "+this.pTabella+" "+"("+this.w_fieldsq+")")
    else
      bTrsErr = .t.
    endif
    if righe<0 or bTrsErr
      * --- Raise
      i_Error=ah_MsgFormat("Errore creazione tabella")
      return
    endif
    this.oParentObject.w_MSG = ah_MsgFormat("Tabella %1 creata",alltrim(this.ptabella))
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione indici
    * --- Controllo se la tabella � stata creata
    sqlexec(this.w_CONNECT,"Select * from "+this.pTABELLA,"RISULTATO")
    if ! USED("RISULTATO") 
      ah_ErrorMsg("La tabella %1 non � stata creata%0L'operazione sar� sospesa","!","",alltrim(this.ptabella))
      i_retcode = 'stop'
      return
    endif
    * --- Select from INF_INDI
    i_nConn=i_TableProp[this.INF_INDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_INDI_idx,2],.t.,this.INF_INDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select GICAMPIN,CPROWNUM  from "+i_cTable+" INF_INDI ";
          +" where GITABNAM="+cp_ToStrODBC(this.pTABELLA)+"";
          +" order by CPROWORD";
           ,"_Curs_INF_INDI")
    else
      select GICAMPIN,CPROWNUM from (i_cTable);
       where GITABNAM=this.pTABELLA;
       order by CPROWORD;
        into cursor _Curs_INF_INDI
    endif
    if used('_Curs_INF_INDI')
      select _Curs_INF_INDI
      locate for 1=1
      do while not(eof())
      this.w_NUMINDX = _Curs_INF_INDI.CPROWNUM
      this.w_CAMPINDX = _Curs_INF_INDI.GICAMPIN
      this.w_CMD = "create index "+alltrim(this.pTABELLA)+"_inf"+alltrim(str(this.w_NUMINDX)) + " on " +alltrim(this.pTABELLA) + "  (" +alltrim(this.w_CAMPINDX)+")"
      sqlexec(this.w_CONNECT,this.w_CMD)
      this.w_INDEXCRE = .T.
        select _Curs_INF_INDI
        continue
      enddo
      use
    endif
    if this.w_INDEXCRE
      this.oParentObject.w_MSG = ah_MsgFormat("Indice creato")
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo correttezza nome campi e tipo dei campi da concatenare
    this.w_TOTFIELD = alen(STRUCTAB,1)
    if TYPE ("FIELDLIST")<>"U"
      for i=1 to alen(FIELDLIST)
      if ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1,15) > 0
        posiz = ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1,15)
      else
        posiz = 0
      endif
      if posiz = 0 or STRUCTAB(posiz,2) <> "C"
        this.w_CONCATOK = .F.
        * --- Inserisce il messaggio nel Log
        AddMsgNL("Attenzione: Il campo %1 non � valido o � di tipo diverso da carattere",this.oParentObject,FIELDLIST(i))
      endif
      next
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione lista campi  da query (senza tipi!)
    this.w_fieldsq = ""
    vq_exec(this.pQuery,this,"CursTab")
    Afields(STRUCTAB,"CursTab")
    if this.w_MULTIAZI="S"
      this.w_fieldsq = "CODAZIEN" + ", "+ "DESAZIEN" + ", "
      * --- Verifico la correttezza dei campi di concatenazione
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
        * --- Accodo i campi di concatenazione
        i=1
        do while i <= alen(FIELDLIST)
          this.w_fieldsq = this.w_fieldsq+FIELDLIST(i)+", "
          i = i +1
        enddo
      endif
    endif
    if used("CursTab")
      Select CursTab 
 use
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione lista campi  della tabella (senza tipi!)
    this.w_fieldst = ""
    righe=SqlExec(this.w_connect,"Select * from "+this.pTabella+" where 1=2","CursTab")
    if righe<0 
      * --- Raise
      i_Error="Errore creazione lista campi tabella"
      return
    endif
    * --- Inserisco la struttura della tabella in un array
    Afields(STRUCTABT,"CursTab")
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico il tipo del campo ed assegno il relativo valore di default.
    if inlist(STRUCTAB( this.w_LOOP ,2),"C","M")
      this.w_COLONUPD = this.w_COLONUPD+" = '"+REPLICATE("@",this.w_DIMFIELD)+"'"+", "
    endif
    if inlist(STRUCTAB( this.w_LOOP ,2),"N","I","F","B")
      this.w_COLONUPD = this.w_COLONUPD+" = 0"+", "
    endif
    if inlist(STRUCTAB( this.w_LOOP ,2) ,"D","T")
      this.w_COLONUPD = this.w_COLONUPD+" = "+cp_ToStrODBC(i_inidat)+", "
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOperaz,pTabella,pQUERY)
    this.pOperaz=pOperaz
    this.pTabella=pTabella
    this.pQUERY=pQUERY
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='INF_TAB'
    this.cWorkTables[2]='INF_TAAZ'
    this.cWorkTables[3]='INF_GRUD'
    this.cWorkTables[4]='INF_INDI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_INF_INDI')
      use in _Curs_INF_INDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperaz,pTabella,pQUERY"
endproc
