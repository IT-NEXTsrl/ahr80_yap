* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bcb                                                        *
*              Routine controllo blocco                                        *
*                                                                              *
*      Author: Zucchetti S.p.A                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][60]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-18                                                      *
* Last revis.: 2005-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bcb",oParentObject,m.pParam)
return(i_retval)

define class tgsin_bcb as StdBatch
  * --- Local variables
  pParam = space(1)
  w_CNBLOCCO = space(1)
  * --- WorkFile variables
  INFOCNBL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pParam="C"
        * --- Try
        local bErr_03464908
        bErr_03464908=bTrsErr
        this.Try_03464908()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- accept error
          bTrsErr=.f.
          Ah_ErrorMsg("Un altro utente ha lanciato la generazione di InfoLink o sta eseguendo InfoPublisher%0Attendere la fine dell'altra elaborazione prima di eseguire la procedura")
          i_retcode = 'stop'
          i_retval = .f.
          return
        endif
        bTrsErr=bTrsErr or bErr_03464908
        * --- End
      otherwise
        * --- Ripristino blocco
        * --- Write into INFOCNBL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.INFOCNBL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INFOCNBL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.INFOCNBL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CNBLOCCO ="+cp_NullLink(cp_ToStrODBC(" "),'INFOCNBL','CNBLOCCO');
              +i_ccchkf ;
          +" where ";
              +"CNSERIAL = "+cp_ToStrODBC("0000000001");
              +" and CNBLOCCO = "+cp_ToStrODBC("S");
                 )
        else
          update (i_cTable) set;
              CNBLOCCO = " ";
              &i_ccchkf. ;
           where;
              CNSERIAL = "0000000001";
              and CNBLOCCO = "S";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        i_retcode = 'stop'
        i_retval = .t.
        return
    endcase
  endproc
  proc Try_03464908()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_0345E938
    bErr_0345E938=bTrsErr
    this.Try_0345E938()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0345E938
    * --- End
    * --- Write into INFOCNBL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.INFOCNBL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INFOCNBL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.INFOCNBL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CNBLOCCO ="+cp_NullLink(cp_ToStrODBC("S"),'INFOCNBL','CNBLOCCO');
          +i_ccchkf ;
      +" where ";
          +"CNSERIAL = "+cp_ToStrODBC("0000000001");
          +" and CNBLOCCO = "+cp_ToStrODBC(" ");
             )
    else
      update (i_cTable) set;
          CNBLOCCO = "S";
          &i_ccchkf. ;
       where;
          CNSERIAL = "0000000001";
          and CNBLOCCO = " ";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_Rows=0
      * --- Raise
      i_Error=AH_MsgFormat("Errore aggiornamento")
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    i_retcode = 'stop'
    i_retval = .t.
    return
    return
  proc Try_0345E938()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INFOCNBL
    i_nConn=i_TableProp[this.INFOCNBL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INFOCNBL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INFOCNBL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CNSERIAL"+",CNBLOCCO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("0000000001"),'INFOCNBL','CNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(" "),'INFOCNBL','CNBLOCCO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CNSERIAL',"0000000001",'CNBLOCCO'," ")
      insert into (i_cTable) (CNSERIAL,CNBLOCCO &i_ccchkf. );
         values (;
           "0000000001";
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INFOCNBL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
