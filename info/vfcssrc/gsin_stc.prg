* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_stc                                                        *
*              Stampa tabelle/connessioni                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-03                                                      *
* Last revis.: 2011-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsin_stc",oParentObject))

* --- Class definition
define class tgsin_stc as StdForm
  Top    = 14
  Left   = 67

  * --- Standard Properties
  Width  = 504
  Height = 142
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-05"
  HelpContextID=74035049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  _IDX = 0
  INF_GRUD_IDX = 0
  INF_GRUM_IDX = 0
  INF_TAB_IDX = 0
  cPrg = "gsin_stc"
  cComment = "Stampa tabelle/connessioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ATNTABLE1 = space(50)
  w_ATNTABLE2 = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsin_stcPag1","gsin_stc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATNTABLE1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='INF_GRUD'
    this.cWorkTables[2]='INF_GRUM'
    this.cWorkTables[3]='INF_TAB'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATNTABLE1=space(50)
      .w_ATNTABLE2=space(50)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ATNTABLE1))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ATNTABLE2))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ATNTABLE1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_TAB_IDX,3]
    i_lTable = "INF_TAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2], .t., this.INF_TAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATNTABLE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIN_ATC',True,'INF_TAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATNTABLE like "+cp_ToStrODBC(trim(this.w_ATNTABLE1)+"%");

          i_ret=cp_SQL(i_nConn,"select ATNTABLE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATNTABLE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATNTABLE',trim(this.w_ATNTABLE1))
          select ATNTABLE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATNTABLE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATNTABLE1)==trim(_Link_.ATNTABLE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATNTABLE1) and !this.bDontReportError
            deferred_cp_zoom('INF_TAB','*','ATNTABLE',cp_AbsName(oSource.parent,'oATNTABLE1_1_1'),i_cWhere,'GSIN_ATC',"Archivio tab. connessioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATNTABLE";
                     +" from "+i_cTable+" "+i_lTable+" where ATNTABLE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATNTABLE',oSource.xKey(1))
            select ATNTABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATNTABLE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATNTABLE";
                   +" from "+i_cTable+" "+i_lTable+" where ATNTABLE="+cp_ToStrODBC(this.w_ATNTABLE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATNTABLE',this.w_ATNTABLE1)
            select ATNTABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATNTABLE1 = NVL(_Link_.ATNTABLE,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ATNTABLE1 = space(50)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_ATNTABLE2) or (.w_ATNTABLE2>=.w_ATNTABLE1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
        endif
        this.w_ATNTABLE1 = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])+'\'+cp_ToStr(_Link_.ATNTABLE,1)
      cp_ShowWarn(i_cKey,this.INF_TAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATNTABLE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATNTABLE2
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_TAB_IDX,3]
    i_lTable = "INF_TAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2], .t., this.INF_TAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATNTABLE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIN_ATC',True,'INF_TAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATNTABLE like "+cp_ToStrODBC(trim(this.w_ATNTABLE2)+"%");

          i_ret=cp_SQL(i_nConn,"select ATNTABLE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATNTABLE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATNTABLE',trim(this.w_ATNTABLE2))
          select ATNTABLE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATNTABLE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATNTABLE2)==trim(_Link_.ATNTABLE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATNTABLE2) and !this.bDontReportError
            deferred_cp_zoom('INF_TAB','*','ATNTABLE',cp_AbsName(oSource.parent,'oATNTABLE2_1_2'),i_cWhere,'GSIN_ATC',"Archivio tab. connessioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATNTABLE";
                     +" from "+i_cTable+" "+i_lTable+" where ATNTABLE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATNTABLE',oSource.xKey(1))
            select ATNTABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATNTABLE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATNTABLE";
                   +" from "+i_cTable+" "+i_lTable+" where ATNTABLE="+cp_ToStrODBC(this.w_ATNTABLE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATNTABLE',this.w_ATNTABLE2)
            select ATNTABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATNTABLE2 = NVL(_Link_.ATNTABLE,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ATNTABLE2 = space(50)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_ATNTABLE1) or (.w_ATNTABLE2>=.w_ATNTABLE1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
        endif
        this.w_ATNTABLE2 = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])+'\'+cp_ToStr(_Link_.ATNTABLE,1)
      cp_ShowWarn(i_cKey,this.INF_TAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATNTABLE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATNTABLE1_1_1.value==this.w_ATNTABLE1)
      this.oPgFrm.Page1.oPag.oATNTABLE1_1_1.value=this.w_ATNTABLE1
    endif
    if not(this.oPgFrm.Page1.oPag.oATNTABLE2_1_2.value==this.w_ATNTABLE2)
      this.oPgFrm.Page1.oPag.oATNTABLE2_1_2.value=this.w_ATNTABLE2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_ATNTABLE2) or (.w_ATNTABLE2>=.w_ATNTABLE1))  and not(empty(.w_ATNTABLE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATNTABLE1_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
          case   not(empty(.w_ATNTABLE1) or (.w_ATNTABLE2>=.w_ATNTABLE1))  and not(empty(.w_ATNTABLE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATNTABLE2_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsin_stcPag1 as StdContainer
  Width  = 500
  height = 142
  stdWidth  = 500
  stdheight = 142
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATNTABLE1_1_1 as StdField with uid="CPVOANEXXO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ATNTABLE1", cQueryName = "ATNTABLE1",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore del codice iniziale oppure inesistente",;
    ToolTipText = "Archivio tab. connessioni da inizio selezione",;
    HelpContextID = 234910629,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=109, Top=11, InputMask=replicate('X',50), bHasZoom = .t. , cLinkFile="INF_TAB", cZoomOnZoom="GSIN_ATC", oKey_1_1="ATNTABLE", oKey_1_2="this.w_ATNTABLE1"

  func oATNTABLE1_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oATNTABLE1_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATNTABLE1_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_TAB','*','ATNTABLE',cp_AbsName(this.parent,'oATNTABLE1_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIN_ATC',"Archivio tab. connessioni",'',this.parent.oContained
  endproc
  proc oATNTABLE1_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSIN_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATNTABLE=this.parent.oContained.w_ATNTABLE1
     i_obj.ecpSave()
  endproc

  add object oATNTABLE2_1_2 as StdField with uid="ZNJLKOUPQB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ATNTABLE2", cQueryName = "ATNTABLE2",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure inesistente.",;
    ToolTipText = "Archivio tab. connessioni da fine selezione",;
    HelpContextID = 234910613,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=109, Top=36, InputMask=replicate('X',50), bHasZoom = .t. , cLinkFile="INF_TAB", cZoomOnZoom="GSIN_ATC", oKey_1_1="ATNTABLE", oKey_1_2="this.w_ATNTABLE2"

  func oATNTABLE2_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oATNTABLE2_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATNTABLE2_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_TAB','*','ATNTABLE',cp_AbsName(this.parent,'oATNTABLE2_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIN_ATC',"Archivio tab. connessioni",'',this.parent.oContained
  endproc
  proc oATNTABLE2_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSIN_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATNTABLE=this.parent.oContained.w_ATNTABLE2
     i_obj.ecpSave()
  endproc


  add object oObj_1_3 as cp_outputCombo with uid="ZLERXIVPWT",left=109, top=67, width=385,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103962598


  add object oBtn_1_4 as StdButton with uid="APIWEFNJEL",left=393, top=92, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 67754534;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="ZJIUAZYUNY",left=444, top=92, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 66717626;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_6 as StdString with uid="DUDWIOZGTJ",Visible=.t., Left=4, Top=15,;
    Alignment=1, Width=102, Height=18,;
    Caption="Da tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QUDTPLGWFU",Visible=.t., Left=4, Top=40,;
    Alignment=1, Width=102, Height=18,;
    Caption="A tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="HCVGMFFTWH",Visible=.t., Left=4, Top=69,;
    Alignment=1, Width=102, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_stc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
