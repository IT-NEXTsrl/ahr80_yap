* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_kle                                                        *
*              Esportazione dati                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-24                                                      *
* Last revis.: 2010-04-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsin_kle",oParentObject))

* --- Class definition
define class tgsin_kle as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 541
  Height = 358+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-02"
  HelpContextID=216641385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  INF_GRUM_IDX = 0
  cPrg = "gsin_kle"
  cComment = "Esportazione dati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODGRU = space(15)
  w_DESGRU = space(45)
  w_PERIODO = space(10)
  o_PERIODO = space(10)
  w_DATAINI = ctod('  /  /  ')
  o_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_SELEZ = space(1)
  w_MSG = space(0)
  w_AZIENDE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsin_klePag1","gsin_kle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsin_klePag2","gsin_kle",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Log")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODGRU_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AZIENDE = this.oPgFrm.Pages(1).oPag.AZIENDE
    DoDefault()
    proc Destroy()
      this.w_AZIENDE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='INF_GRUM'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSIN_BIM with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODGRU=space(15)
      .w_DESGRU=space(45)
      .w_PERIODO=space(10)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_SELEZ=space(1)
      .w_MSG=space(0)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODGRU))
          .link_1_2('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_PERIODO = iif(isalt(), 'AL', 'MC')
        .w_DATAINI = iif(.w_PERIODO='AL',cp_CharToDate('  -  -    '),iif(.w_PERIODO<>'MA',.w_DATAINI,i_DATSYS))
        .w_DATAFIN = iif(.w_PERIODO='AL',cp_CharToDate('  -  -    '),iif(.w_PERIODO<>'MA',.w_DATAFIN,.w_DATAINI))
        .w_SELEZ = 'D'
      .oPgFrm.Page1.oPag.AZIENDE.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
    endwith
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_PERIODO<>.w_PERIODO
            .w_DATAINI = iif(.w_PERIODO='AL',cp_CharToDate('  -  -    '),iif(.w_PERIODO<>'MA',.w_DATAINI,i_DATSYS))
        endif
        if .o_PERIODO<>.w_PERIODO.or. .o_DATAINI<>.w_DATAINI
            .w_DATAFIN = iif(.w_PERIODO='AL',cp_CharToDate('  -  -    '),iif(.w_PERIODO<>'MA',.w_DATAFIN,.w_DATAINI))
        endif
        .oPgFrm.Page1.oPag.AZIENDE.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        * --- Area Manuale = Calculate
        * --- gsin_kle
        CALCDATES(.w_PERIODO)
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AZIENDE.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
    endwith
  return

  proc Calculate_AOAEMYGJAH()
    with this
          * --- Chiusura cursori
          CHIUSURA_CURSORI_INFO(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATAINI_1_5.enabled = this.oPgFrm.Page1.oPag.oDATAINI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oDATAFIN_1_6.enabled = this.oPgFrm.Page1.oPag.oDATAFIN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AZIENDE.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_AOAEMYGJAH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODGRU
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_GRUM_IDX,3]
    i_lTable = "INF_GRUM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2], .t., this.INF_GRUM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIN_MGR',True,'INF_GRUM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_CODGRU))
          select GRCODICE,GRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('INF_GRUM','*','GRCODICE',cp_AbsName(oSource.parent,'oCODGRU_1_2'),i_cWhere,'GSIN_MGR',"Gruppi di regole",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_CODGRU)
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GRCODICE,space(15))
      this.w_DESGRU = NVL(_Link_.GRDESCRI,space(45))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(15)
      endif
      this.w_DESGRU = space(45)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.INF_GRUM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_2.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_2.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_3.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_3.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_4.RadioValue()==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_5.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_5.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_6.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_6.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ_1_7.RadioValue()==this.w_SELEZ)
      this.oPgFrm.Page1.oPag.oSELEZ_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_3.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_3.value=this.w_MSG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODGRU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODGRU_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODGRU)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PERIODO = this.w_PERIODO
    this.o_DATAINI = this.w_DATAINI
    return

enddefine

* --- Define pages as container
define class tgsin_klePag1 as StdContainer
  Width  = 537
  height = 358
  stdWidth  = 537
  stdheight = 358
  resizeXpos=250
  resizeYpos=237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODGRU_1_2 as StdField with uid="QGQDINWOEP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo di regole da elaborare",;
    HelpContextID = 226616358,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=113, Top=12, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="INF_GRUM", cZoomOnZoom="GSIN_MGR", oKey_1_1="GRCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_GRUM','*','GRCODICE',cp_AbsName(this.parent,'oCODGRU_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIN_MGR',"Gruppi di regole",'',this.parent.oContained
  endproc
  proc oCODGRU_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSIN_MGR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GRCODICE=this.parent.oContained.w_CODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_3 as StdField with uid="XAJYXANOSL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    HelpContextID = 226675254,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=247, Top=12, InputMask=replicate('X',45)


  add object oPERIODO_1_4 as StdCombo with uid="IANZWSKGUW",rtseq=3,rtrep=.f.,left=113,top=40,width=132,height=21;
    , ToolTipText = "Imposta il periodo di esportazione";
    , HelpContextID = 61555978;
    , cFormVar="w_PERIODO",RowSource=""+"Giorno corrente,"+"Settimana corrente,"+"Mese corrente,"+"Trimestre corrente,"+"Semestre corrente,"+"Anno corrente,"+"Manuale,"+"No filtro date", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPERIODO_1_4.RadioValue()
    return(iif(this.value =1,'GC',;
    iif(this.value =2,'SC',;
    iif(this.value =3,'MC',;
    iif(this.value =4,'TC',;
    iif(this.value =5,'SA',;
    iif(this.value =6,'AC',;
    iif(this.value =7,'MA',;
    iif(this.value =8,'AL',;
    space(10))))))))))
  endfunc
  func oPERIODO_1_4.GetRadio()
    this.Parent.oContained.w_PERIODO = this.RadioValue()
    return .t.
  endfunc

  func oPERIODO_1_4.SetRadio()
    this.Parent.oContained.w_PERIODO=trim(this.Parent.oContained.w_PERIODO)
    this.value = ;
      iif(this.Parent.oContained.w_PERIODO=='GC',1,;
      iif(this.Parent.oContained.w_PERIODO=='SC',2,;
      iif(this.Parent.oContained.w_PERIODO=='MC',3,;
      iif(this.Parent.oContained.w_PERIODO=='TC',4,;
      iif(this.Parent.oContained.w_PERIODO=='SA',5,;
      iif(this.Parent.oContained.w_PERIODO=='AC',6,;
      iif(this.Parent.oContained.w_PERIODO=='MA',7,;
      iif(this.Parent.oContained.w_PERIODO=='AL',8,;
      0))))))))
  endfunc

  add object oDATAINI_1_5 as StdField with uid="XQRUKUVUZO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 169028042,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=261, Top=40

  func oDATAINI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERIODO='MA')
    endwith
   endif
  endfunc

  add object oDATAFIN_1_6 as StdField with uid="COCAFYDSXO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 256059850,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=344, Top=40

  func oDATAFIN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERIODO='MA')
    endwith
   endif
  endfunc

  add object oSELEZ_1_7 as StdRadio with uid="HNXVSCEFDD",rtseq=6,rtrep=.f.,left=4, top=313, width=184,height=34;
    , cFormVar="w_SELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ_1_7.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 117417178
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 117417178
      this.Buttons(2).Top=16
      this.SetAll("Width",182)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZ_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZ_1_7.GetRadio()
    this.Parent.oContained.w_SELEZ = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ_1_7.SetRadio()
    this.Parent.oContained.w_SELEZ=trim(this.Parent.oContained.w_SELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ=='S',1,;
      iif(this.Parent.oContained.w_SELEZ=='D',2,;
      0))
  endfunc


  add object oBtn_1_8 as StdButton with uid="RIHNVYPWOK",left=433, top=312, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 198419591;
    , tabstop=.f.,Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        do GSIN_BIM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODGRU))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="LKDJRNWUMO",left=484, top=312, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 209323962;
    , tabstop=.f.,Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AZIENDE as cp_szoombox with uid="ZGDHQPGTJI",left=4, top=99, width=529,height=210,;
    caption='AZIENDE',;
   bGlobalFont=.t.,;
    cTable="INF_TAAZ",cZoomFile="GSIN_KLE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    nPag=1;
    , HelpContextID = 205537030


  add object oObj_1_14 as cp_runprogram with uid="SZHKSPXNHZ",left=-6, top=371, width=232,height=21,;
    caption='GSIN_BCC',;
   bGlobalFont=.t.,;
    prg="GSIN_BCC('C')",;
    cEvent = "w_CODGRU Changed",;
    nPag=1;
    , HelpContextID = 190396585


  add object oObj_1_15 as cp_runprogram with uid="LJCOFAJUQU",left=-6, top=391, width=232,height=21,;
    caption='GSIN_BCC',;
   bGlobalFont=.t.,;
    prg="GSIN_BCC('S')",;
    cEvent = "w_SELEZ Changed",;
    nPag=1;
    , HelpContextID = 190396585

  add object oStr_1_1 as StdString with uid="JYQQROVYEN",Visible=.t., Left=1, Top=15,;
    Alignment=1, Width=109, Height=18,;
    Caption="Gruppo di regole:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="TVCGPJBMZD",Visible=.t., Left=1, Top=45,;
    Alignment=1, Width=109, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="RLTNVUAOIF",Visible=.t., Left=7, Top=76,;
    Alignment=0, Width=381, Height=17,;
    Caption="Elenco codici azienda (per tabelle multi aziendali)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_11 as StdBox with uid="ZQLUZJZBJJ",left=3, top=92, width=531,height=3
enddefine
define class tgsin_klePag2 as StdContainer
  Width  = 537
  height = 358
  stdWidth  = 537
  stdheight = 358
  resizeXpos=405
  resizeYpos=267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_3 as StdMemo with uid="LKCQOHSJWH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 216327994,;
   bGlobalFont=.t.,;
    Height=316, Width=523, Left=2, Top=31, readonly = .t.

  add object oStr_2_2 as StdString with uid="MVBJFTGXCF",Visible=.t., Left=3, Top=7,;
    Alignment=0, Width=232, Height=18,;
    Caption="Messaggi importazione"  ;
  , bGlobalFont=.t.

  add object oBox_2_1 as StdBox with uid="BMXOSUHAMA",left=1, top=24, width=524,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_kle','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsin_kle
PROCEDURE CALCDATES

   PARAMETERS tipo

   local lastday,firstday

   DO CASE

     CASE tipo = 'GC'

        .w_DATAINI=date()

        .w_DATAFIN=date()

     CASE tipo = 'SC'

        .w_DATAINI=DATE()-DOW(DATE())

        .w_DATAFIN=DATE()+(7-DOW(DATE()))

     CASE tipo = 'MC'

        .w_DATAINI=cp_CharToDate('01-'+str(month(date()))+'-'+alltrim(str(year(date()))))

        do case

         case month(date()) = 1

            lastday='31-'

         case month(date()) = 2

            lastday='28-'

         case month(date()) = 3

            lastday='31-'

         case month(date()) = 4

            lastday='30-'

         case month(date()) = 5

            lastday='31-'

         case month(date()) = 6

            lastday='30-'

         case month(date()) = 7

            lastday='31-'

         case month(date()) = 8

            lastday='31-'

         case month(date()) = 9

            lastday='30-'

         case month(date()) = 10

            lastday='31-'

         case month(date()) = 11

            lastday='30-'

         case month(date()) = 12

            lastday='31-'

        endcase

        .w_DATAFIN=cp_CharToDate(lastday+str(month(date()))+'-'+alltrim(str(year(date()))))

      CASE tipo = 'TC'

         do case

           case BETWEEN(month(date()),1,3)

              firstday='01-01-'

              lastday='31-03-'

           case BETWEEN(month(date()),4,6)

              firstday='01-04-'

              lastday='30-06-'

           case BETWEEN(month(date()),7,9)

              firstday='01-07-'

              lastday='30-09-'

           case BETWEEN(month(date()),10,12)

              firstday='01-10-'

              lastday='31-12-'

          endcase

        .w_DATAINI=cp_CharToDate(firstday+alltrim(str(year(date()))) )

        .w_DATAFIN=cp_CharToDate(lastday+alltrim(str(year(date()))))

      CASE tipo = 'SA'

         do case

           case BETWEEN(month(date()),1,6)

              firstday='01-01-'

              lastday='30-06-'

           case BETWEEN(month(date()),7,12)

              firstday='01-07-'

              lastday='31-12-'

         endcase

        .w_DATAINI=cp_CharToDate(firstday+alltrim(str(year(date()))))

        .w_DATAFIN=cp_CharToDate(lastday+alltrim(str(year(date()))))

      CASE tipo = 'AC'

        .w_DATAINI=cp_CharToDate('01-01-'+alltrim(str(year(date()))) )

        .w_DATAFIN=cp_CharToDate('31-12-'+alltrim(str(year(date()))))

    ENDCASE

ENDPROC



Proc CHIUSURA_CURSORI_INFO(pParent)

   USE IN SELECT('Aziende')

EndProc
* --- Fine Area Manuale
