* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bdc                                                        *
*              Crea cursore mappe da DBF                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-17                                                      *
* Last revis.: 2010-03-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bdc",oParentObject)
return(i_retval)

define class tgsin_bdc as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per creare ed inserire i dati da un dbf a una tabella
    NameDBF="..\INFO\MAPPE\com2008.dbf"
    select * from &NameDBF into cursor CurComun
    USE IN JUSTSTEM(NAMEDBF)
    NameDBF="..\INFO\MAPPE\prov2008.dbf"
    select * from &NameDBF into cursor CurProvi
    USE IN JUSTSTEM(NAMEDBF)
    NameDBF="..\INFO\MAPPE\reg2008.dbf"
    select * from &NameDBF into cursor CurRegio
    USE IN JUSTSTEM(NAMEDBF)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
