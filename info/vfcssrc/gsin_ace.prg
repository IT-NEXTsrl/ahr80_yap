* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_ace                                                        *
*              Controllo elaborazione                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-18                                                      *
* Last revis.: 2006-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsin_ace")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsin_ace")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsin_ace")
  return

* --- Class definition
define class tgsin_ace as StdPCForm
  Width  = 288
  Height = 46
  Top    = 10
  Left   = 10
  cComment = "Controllo elaborazione"
  cPrg = "gsin_ace"
  HelpContextID=109686633
  add object cnt as tcgsin_ace
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsin_ace as PCContext
  w_CNSERIAL = space(10)
  w_CNBLOCCO = space(1)
  proc Save(oFrom)
    this.w_CNSERIAL = oFrom.w_CNSERIAL
    this.w_CNBLOCCO = oFrom.w_CNBLOCCO
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CNSERIAL = this.w_CNSERIAL
    oTo.w_CNBLOCCO = this.w_CNBLOCCO
    PCContext::Load(oTo)
enddefine

define class tcgsin_ace as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 288
  Height = 46
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2006-01-04"
  HelpContextID=109686633
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  INFOCNBL_IDX = 0
  cFile = "INFOCNBL"
  cKeySelect = "CNSERIAL"
  cKeyWhere  = "CNSERIAL=this.w_CNSERIAL"
  cKeyWhereODBC = '"CNSERIAL="+cp_ToStrODBC(this.w_CNSERIAL)';

  cKeyWhereODBCqualified = '"INFOCNBL.CNSERIAL="+cp_ToStrODBC(this.w_CNSERIAL)';

  cPrg = "gsin_ace"
  cComment = "Controllo elaborazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CNSERIAL = space(10)
  w_CNBLOCCO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsin_acePag1","gsin_ace",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 51239178
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCNBLOCCO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='INFOCNBL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INFOCNBL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INFOCNBL_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsin_ace'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from INFOCNBL where CNSERIAL=KeySet.CNSERIAL
    *
    i_nConn = i_TableProp[this.INFOCNBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INFOCNBL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INFOCNBL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INFOCNBL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INFOCNBL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CNSERIAL',this.w_CNSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CNSERIAL = NVL(CNSERIAL,space(10))
        .w_CNBLOCCO = NVL(CNBLOCCO,space(1))
        cp_LoadRecExtFlds(this,'INFOCNBL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CNSERIAL = space(10)
      .w_CNBLOCCO = space(1)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'INFOCNBL')
    this.DoRTCalc(1,2,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCNBLOCCO_1_2.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'INFOCNBL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INFOCNBL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNSERIAL,"CNSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNBLOCCO,"CNBLOCCO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INFOCNBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INFOCNBL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.INFOCNBL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INFOCNBL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INFOCNBL')
        i_extval=cp_InsertValODBCExtFlds(this,'INFOCNBL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CNSERIAL,CNBLOCCO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CNSERIAL)+;
                  ","+cp_ToStrODBC(this.w_CNBLOCCO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INFOCNBL')
        i_extval=cp_InsertValVFPExtFlds(this,'INFOCNBL')
        cp_CheckDeletedKey(i_cTable,0,'CNSERIAL',this.w_CNSERIAL)
        INSERT INTO (i_cTable);
              (CNSERIAL,CNBLOCCO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CNSERIAL;
                  ,this.w_CNBLOCCO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.INFOCNBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INFOCNBL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.INFOCNBL_IDX,i_nConn)
      *
      * update INFOCNBL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'INFOCNBL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CNBLOCCO="+cp_ToStrODBC(this.w_CNBLOCCO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'INFOCNBL')
        i_cWhere = cp_PKFox(i_cTable  ,'CNSERIAL',this.w_CNSERIAL  )
        UPDATE (i_cTable) SET;
              CNBLOCCO=this.w_CNBLOCCO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INFOCNBL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INFOCNBL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.INFOCNBL_IDX,i_nConn)
      *
      * delete INFOCNBL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CNSERIAL',this.w_CNSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INFOCNBL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INFOCNBL_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCNBLOCCO_1_2.RadioValue()==this.w_CNBLOCCO)
      this.oPgFrm.Page1.oPag.oCNBLOCCO_1_2.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'INFOCNBL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsin_acePag1 as StdContainer
  Width  = 284
  height = 46
  stdWidth  = 284
  stdheight = 46
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCNBLOCCO_1_2 as StdCheck with uid="YTMAVZVDOP",rtseq=2,rtrep=.f.,left=17, top=9, caption="Elaborazione in uso da altro utente",;
    ToolTipText = "Se attiva l'elaborazione � stata lanciata da un altro utente",;
    HelpContextID = 28754805,;
    cFormVar="w_CNBLOCCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCNBLOCCO_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCNBLOCCO_1_2.GetRadio()
    this.Parent.oContained.w_CNBLOCCO = this.RadioValue()
    return .t.
  endfunc

  func oCNBLOCCO_1_2.SetRadio()
    this.Parent.oContained.w_CNBLOCCO=trim(this.Parent.oContained.w_CNBLOCCO)
    this.value = ;
      iif(this.Parent.oContained.w_CNBLOCCO=='S',1,;
      0)
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_ace','INFOCNBL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CNSERIAL=INFOCNBL.CNSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
