* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bdr_com                                                    *
*              Elimina tabelle elab commessa                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-28                                                      *
* Last revis.: 2005-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bdr_com",oParentObject)
return(i_retval)

define class tgsin_bdr_com as StdBatch
  * --- Local variables
  * --- WorkFile variables
  MATMPMOV_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_04A4CFD0
    bErr_04A4CFD0=bTrsErr
    this.Try_04A4CFD0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04A4CFD0
    * --- End
    wait clear
  endproc
  proc Try_04A4CFD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Drop temporary table MATMPMOV
    i_nIdx=cp_GetTableDefIdx('MATMPMOV')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('MATMPMOV')
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MATMPMOV'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
