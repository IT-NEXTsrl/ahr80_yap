* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bpa                                                        *
*              Interfaccia pros. acquisti                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-14                                                      *
* Last revis.: 2004-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperaz,pTabella,pQUERY
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bpa",oParentObject,m.pOperaz,m.pTabella,m.pQUERY)
return(i_retval)

define class tgsin_bpa as StdBatch
  * --- Local variables
  pOperaz = space(2)
  pTabella = space(50)
  pQUERY = space(20)
  w_DATSTA = ctod("  /  /  ")
  w_DADATA = ctod("  /  /  ")
  w_ADATA = ctod("  /  /  ")
  w_CODTRA = space(2)
  w_CODFA = space(2)
  w_CODNOT = space(2)
  w_AGGIORN = space(1)
  w_CAOVAL1 = 0
  w_CAOVAL2 = 0
  w_STVAL = space(1)
  w_VALU2 = space(3)
  w_CAMBIO2 = 0
  w_CAMBIO1 = 0
  w_CAMBIO = 0
  w_CAOESE = 0
  w_DECTOT = 0
  w_OREP = space(50)
  w_OQRY = space(50)
  w_ONUME = 0
  w_PAGAM = space(5)
  w_ZONA = space(3)
  w_CATCOMM = space(3)
  w_CLIENTE = space(15)
  w_STATO = space(1)
  w_CODART = space(20)
  w_CODART1 = space(20)
  w_GRUMER = space(5)
  w_CODMAR = space(5)
  w_CODFAM = space(5)
  w_CODCAT = space(5)
  w_DATREG = ctod("  /  /  ")
  w_DATREG1 = ctod("  /  /  ")
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch che lancia l'elaborazione del prospetto degli acquisti (GSVE_BPA)
    * --- Valorizzo le variabili per il GSVE_BPA...
    this.w_STVAL = "C"
    this.w_VALU2 = g_PERVAL
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALU2);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALU2;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_OQRY = "..\INFO\EXE\QUERY\GSIN_BPA.VQR"
    this.w_STATO = "N"
    this.w_CODTRA = "DT"
    this.w_CODFA = "FA"
    this.w_CODNOT = "NC"
    this.w_DATSTA = i_datsys
    * --- Lancio il Batch GSVE_BPV (Elaborazione Stampa Prospetto del Venduto)
    do GSAC_BPV with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pOperaz,pTabella,pQUERY)
    this.pOperaz=pOperaz
    this.pTabella=pTabella
    this.pQUERY=pQUERY
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperaz,pTabella,pQUERY"
endproc
