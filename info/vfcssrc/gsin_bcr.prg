* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bcr                                                        *
*              Creazione- inserimento                                          *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][60]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-15                                                      *
* Last revis.: 2015-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperaz,pTabella,pQUERY
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bcr",oParentObject,m.pOperaz,m.pTabella,m.pQUERY)
return(i_retval)

define class tgsin_bcr as StdBatch
  * --- Local variables
  pOperaz = space(2)
  pTabella = space(50)
  pQUERY = space(20)
  w_VarError = space(255)
  w_ODBC = space(25)
  w_STRINGA = space(90)
  w_CRIPTATA = space(1)
  w_CONNECT = 0
  w_CONNECTION = space(25)
  w_FIELDS = space(0)
  w_VALUES = space(0)
  w_VALUES_BIND = space(0)
  w_WHERE = space(255)
  w_WHERE2 = space(255)
  w_DB = space(10)
  w_MULTIAZI = space(1)
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_CAMPICON = space(150)
  w_CAMPICON2 = space(254)
  w_NOMECAMP = space(50)
  w_CONCATOK = .f.
  w_NUMRECORD = 0
  w_ORNOMCUR = space(20)
  w_AZRIPCOM = space(1)
  w_NUMMOV = 0
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DATECUR = ctod("  /  /  ")
  w_COUNT = 0
  w_IMPAVEEF = 0
  w_IMPDAREF = 0
  w_IMPAVEPR = 0
  w_IMPDARPR = 0
  w_RESAVEEF = 0
  w_RESDAREF = 0
  w_RESAVEPR = 0
  w_RESDARPR = 0
  w_LUNMEMO = 0
  w_LUNMEMO = 0
  w_CMD = space(254)
  w_NUMINDX = 0
  w_CAMPINDX = space(150)
  w_INDEXCRE = .f.
  w_TOTFIELD = 0
  * --- WorkFile variables
  INF_GRUD_idx=0
  INF_TAB_idx=0
  INF_INDI_idx=0
  INF_TAAZ_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Messaggio=""
    this.w_DATAINI = this.oParentObject.oParentObject.w_DATAINI
    this.w_DATAFIN = this.oParentObject.oParentObject.w_DATAFIN
    * --- Inizializzazione variabili gestione campi di concatenazione in tabelle multiazienda
    this.w_CAMPICON2 = ""
    this.w_NOMECAMP = ""
    this.w_CONCATOK = .T.
    * --- Try
    local bErr_03BD85B0
    bErr_03BD85B0=bTrsErr
    this.Try_03BD85B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03BD85B0
    * --- End
  endproc
  proc Try_03BD85B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifico il nome della tabella nel nostro standard
    this.pTabella = alltrim(this.pTabella)+"_DW"
    * --- Acquisisco il nome dell'ODBC o la stringa di connessione relativo alla tabella passata come parametro (pTabella)
    * --- Prima controllo se esiste una connessione di "default" per la tabella inerente  all'azienda in uso
    * --- Read from INF_TAAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TACOODBC,ATCOSTRI,ATCRIPTE"+;
        " from "+i_cTable+" INF_TAAZ where ";
            +"TACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TACOODBC,ATCOSTRI,ATCRIPTE;
        from (i_cTable) where;
            TACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
      this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
      this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo nell'archivio Tabelle / Connessioni 
      * --- Read from INF_TAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAB where ";
              +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              ATNTABLE = this.pTabella;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.ATCOODBC),cp_NullValue(_read_.ATCOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo se esiste per la tabella una connessione globale (cio� indipendente dall'azienda)
      * --- Read from INF_TAAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TACOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAAZ where ";
              +"TACODAZI = "+cp_ToStrODBC(space(5));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TACOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              TACODAZI = space(5);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Database  diverso da quello usato dalla procedura.
      *     Creo una nuova connessione
      if this.w_CRIPTATA = "C"
        this.w_STRINGA = CifraCnf( ALLTRIM(this.w_STRINGA) , "D" )
      endif
      this.w_CONNECTION = alltrim(this.w_ODBC + this.w_STRINGA)
      this.w_CONNECT = cp_openDatabaseConnection(this.w_connection)
      this.w_DB = SQLXGetDatabaseType(this.w_CONNECT)
    else
      * --- Stessa connessione della procedura
      this.w_CONNECT = i_TableProp[this.INF_GRUD_idx,3]
      this.w_DB = SQLXGetDatabaseType(this.w_CONNECT)
    endif
    if this.w_CONNECT <= 0
      AH_ERRORMSG("Connessione fallita",48)
      this.oParentObject.w_errore = "%1"
      this.oParentObject.w_PARMSG1 = alltrim(message())
      i_retcode = 'stop'
      return
    else
      ah_msg( "Connessione riuscita..." )
    endif
    this.oParentObject.w_RENOMCUR = alltrim(this.oParentObject.w_RENOMCUR)
    * --- Verifico se la tabella � multi aziendale o no
    * --- Read from INF_TAB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_TAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATMULTAZ"+;
        " from "+i_cTable+" INF_TAB where ";
            +"ATNTABLE = "+cp_ToStrODBC(this.pTABELLA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATMULTAZ;
        from (i_cTable) where;
            ATNTABLE = this.pTABELLA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MULTIAZI = NVL(cp_ToDate(_read_.ATMULTAZ),cp_NullValue(_read_.ATMULTAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se tabella multiazienda leggo i campi da concatenare, e aggiungo il carattere 'Z' ad
    *     ognuno di essi
    if this.w_MULTIAZI = "S"
      * --- Read from INF_TAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCAMCON"+;
          " from "+i_cTable+" INF_TAB where ";
              +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCAMCON;
          from (i_cTable) where;
              ATNTABLE = this.pTabella;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAMPICON = NVL(cp_ToDate(_read_.ATCAMCON),cp_NullValue(_read_.ATCAMCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      i=1
      do while ! empty(alltrim(this.w_CAMPICON))
        dimension FIELDLIST (i)
        if "," $ this.w_CAMPICON
          this.w_NOMECAMP = substr(this.w_CAMPICON,1,at(",",this.w_CAMPICON)-1)+"Z"
          this.w_CAMPICON = substr(this.w_CAMPICON,at(",",this.w_CAMPICON)+1)
        else
          this.w_NOMECAMP = alltrim(this.w_CAMPICON)+"Z"
          this.w_CAMPICON = ""
        endif
        this.w_CAMPICON2 = this.w_CAMPICON2+this.w_NOMECAMP+","
        store alltrim(this.w_NOMECAMP) to FIELDLIST(i)
        i=i+1
      enddo
    endif
    do case
      case this.pOPERAZ="IN"
        if ! used(this.oParentObject.w_RENOMCUR) 
          * --- Cursore Dati non creato
          if empty(this.w_ODBC) and empty(this.w_STRINGA)
            * --- Stessa connessione
            * --- begin transaction
            cp_BeginTrs()
            * --- Try
            local bErr_03BDB040
            bErr_03BDB040=bTrsErr
            this.Try_03BDB040()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              i_Error=AH_MsgFormat(MSG_INSERT_ERROR) 
              this.oParentObject.w_errore = "Errore inserimento dati: %0%1"
              this.oParentObject.w_PARMSG1 = alltrim(message())
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              cp_EndTrs(.t.)
            endif
            bTrsErr=bTrsErr or bErr_03BDB040
            * --- End
          else
            * --- Diversa Connessione
            * --- begin transaction
            cp_BeginTrs()
            * --- Try
            local bErr_03BDF2D0
            bErr_03BDF2D0=bTrsErr
            this.Try_03BDF2D0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              i_Error=AH_MsgFormat(MSG_INSERT_ERROR) 
              this.oParentObject.w_errore = "Errore inserimento dati: %0%1"
              this.oParentObject.w_PARMSG1 = alltrim(message())
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              cp_EndTrs(.t.)
            endif
            bTrsErr=bTrsErr or bErr_03BDF2D0
            * --- End
          endif
        else
          * --- begin transaction
          cp_BeginTrs()
          * --- Try
          local bErr_04F846F0
          bErr_04F846F0=bTrsErr
          this.Try_04F846F0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            i_Error=AH_MsgFormat(MSG_INSERT_ERROR) 
            this.oParentObject.w_errore = "Errore inserimento dati: %0%1"
            this.oParentObject.w_PARMSG1 = alltrim(message())
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_04F846F0
          * --- End
        endif
      case this.pOPERAZ="CR"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Mi disconetto dalla nuova connessione.
      Sqldisconnect(this.w_CONNECT)
    endif
    * --- Chiudo i cursori
    USE IN SELECT("CursTab") 
 USE IN SELECT("Risultato") 
 USE IN SELECT("Sqlresult") 
 USE IN SELECT(this.oParentObject.w_RENOMCUR)
    return
  proc Try_03BDB040()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    local i_cTmpName 
 i_nConn=i_TableProp[this.INF_INDI_idx,3] 
 i_cTmpName=cp_getTempTableName(i_nConn) 
 vq_exec(this.pQuery,this,.f.,.f.,.f.,.t.,.f.,.f.,i_cTmpName) 
 cp_sql(i_nConn,"select * from "+this.pTabella+" where 1=0") 
 i_cFields="" 
 i_cFields2="" 
 for i_i=1 to fcount()
     
 i_cFields=i_cFields+iif(substr(field(i_i),len(field(i_i))) = "Z" and (field(i_i)+"," $ this.w_CAMPICON2 or field(i_i)+" " $ this.w_CAMPICON2),"",","+field(i_i)) 
 i_cFields2=i_cFields2+iif(field(i_i)="CODAZIEN",",'"+i_CODAZI+"' as CODAZIEN",iif(field(i_i)="DESAZIEN",","+cp_ToStrODBC(this.oParentObject.w_RAGSOC)+" as DESAZIEN",iif(substr(field(i_i),len(field(i_i))) = "Z" and (field(i_i)+"," $ this.w_CAMPICON2 or field(i_i)+" " $ this.w_CAMPICON2),"",","+field(i_i))))
     next 
 use
    if this.w_MULTIAZI="S"
      * --- Controllo la correttezza dei campi di concatenazione
      if TYPE ("FIELDLIST")<>"U"
        i=1
        do while i <= alen(FIELDLIST)
          if ! (substr(fieldlist(i),1,len(fieldlist(i))-1) $ i_cFields)
            this.w_CONCATOK = .F.
          endif
          i = i + 1
        enddo
      endif
      if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
        * --- Accodo i campi di concatenazione
        i=1
        do while i <= alen(FIELDLIST)
           i_cFields=i_cFields+","+fieldlist(i) 
          do case
            case this.w_DB="SQLServer"
              i_cFields2=i_cFields2+","+substr(fieldlist(i),1,len(fieldlist(i))-1)+"+'"+i_CODAZI+"' as "+fieldlist(i)
            case this.w_DB="Oracle"
              i_cFields2=i_cFields2+","+substr(fieldlist(i),1,len(fieldlist(i))-1)+"||'"+i_CODAZI+"' as "+fieldlist(i)
            otherwise
              i_cFields2=i_cFields2+",concat("+substr(fieldlist(i),1,len(fieldlist(i))-1)+",'"+i_CODAZI+"') as "+fieldlist(i)
          endcase
          i = i + 1
        enddo
      endif
    endif
    righe=cp_TrsSQL(i_nConn,"insert into "+this.pTabella+" ("+substr(i_cFields,2)+") (select "+substr(i_cFields2,2)+" from "+i_cTmpName+")")
     cp_sql(i_nConn,"drop table "+i_cTmpName)
    if righe<0 or bTrsErr
      * --- Raise
      i_Error=AH_MsgFormat("Errore inserimento dati")
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_MSG = ah_msgformat("Inserimento completato con successo")
    return
  proc Try_03BDF2D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    set date to american
    this.w_FIELDS = ""
    this.w_VALUES_BIND = ""
    Declare STRUCTAB (1,5)
    vq_exec(this.pQuery,this,"CursTab")
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NUMRECORD = reccount("CursTab")
    Select CursTab 
 Go Top 
 Scan
    this.w_VALUES = ""
    for i=1 to fcount()
    w_campo=field(i) 
 valore=&w_campo
    if type("valore") $ "CM"
      valore = strtran(valore,'"',space(1))
    endif
    this.w_VALUES = this.w_VALUES+cp_ToStrODBC(VALORE)+", "
    nvalues="BV_"+w_campo+"= "+"CursTab."+w_campo
    &nvalues
    endfor
    if this.w_MULTIAZI="S"
      this.w_VALUES = this.w_VALUES+cp_ToStrODBC(i_CODAZI)+", "
      BV_CODAZIEN = i_CODAZI
      this.w_VALUES = this.w_VALUES+cp_ToStrODBC(this.oParentObject.w_RAGSOC)+", "
      BV_DESAZIEN = this.oParentObject.w_RAGSOC
      if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
        for i=1 to alen(FIELDLIST)
        w_campo=substr(fieldlist(i),1,len(fieldlist(i))-1) 
 w_campoZ=substr(fieldlist(i),1,len(fieldlist(i))) 
 valore=&w_campo
        if type("valore") $ "CM"
          valore = strtran(valore,'"',space(1))
        endif
        this.w_VALUES = this.w_VALUES+strtran(cp_ToStrODBC(VALORE)+cp_ToStrODBC(i_CODAZI),"''","")+", "
        nvalues="BV_"+w_campoZ+"= "+"CursTab."+w_campo+"+i_CODAZI"
        &nvalues
        next
      endif
    endif
    this.w_VALUES = LEFT(this.w_VALUES,LEN(this.w_VALUES)-1)
    if mod(recno(), iif(this.w_NUMRECORD>10000, 1000, iif(this.w_NUMRECORD>1000, 100, iif(this.w_NUMRECORD>100, 10, 1))))=0
      ah_msg( "Inserimento record n� %1 / %2",.t.,.f.,.f.,alltrim(str(recno())),alltrim(str(this.w_NUMRECORD)) )
    endif
    if i_nACTIVATEPROFILER>0
      righe=cp_SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALUES_BIND+")")
    else
      righe=SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALUES_BIND+")")
    endif
    if righe<0 or bTrsErr
      * --- Raise
      i_Error=AH_MsgFormat("Errore inserimento dati")
      return
    endif
    Select CursTab
    endscan
    set date to italian
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_MSG = ah_msgformat("Inserimento completato con successo")
    return
  proc Try_04F846F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_FIELDS = ""
    Declare STRUCTAB (1,5)
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NUMRECORD = reccount(this.oParentObject.w_RENOMCUR)
    Select (this.oParentObject.w_RENOMCUR) 
 Go Top 
 Scan
    this.w_VALUES = ""
    for i=1 to fcount()
    w_campo=field(i) 
 valore=&w_campo
    if type("valore") $ "CM"
      valore = strtran(valore,'"',space(1))
    endif
    this.w_VALUES = this.w_VALUES+cp_ToStrODBC(VALORE)+", "
    nvalues="BV_"+w_campo+"= "+this.oParentObject.w_RENOMCUR+"."+w_campo
    &nvalues
    endfor
    if this.w_MULTIAZI="S"
      this.w_VALUES = this.w_VALUES+cp_ToStrODBC(i_CODAZI)+", "
      BV_CODAZIEN = i_CODAZI
      this.w_VALUES = this.w_VALUES+cp_ToStrODBC(this.oParentObject.w_RAGSOC)+", "
      BV_DESAZIEN = this.oParentObject.w_RAGSOC
      if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
        for i=1 to alen(FIELDLIST)
        w_campo=substr(fieldlist(i),1,len(fieldlist(i))-1) 
 w_campoZ=substr(fieldlist(i),1,len(fieldlist(i))) 
 valore=&w_campo
        if type("valore") $ "CM"
          valore = strtran(valore,'"',space(1))
        endif
        this.w_VALUES = this.w_VALUES+strtran(cp_ToStrODBC(VALORE)+cp_ToStrODBC(i_CODAZI),"''","")+", "
        nvalues="BV_"+w_campoZ+"= "+this.oParentObject.w_RENOMCUR+"."+w_campo
        &nvalues
        next
      endif
    endif
    this.w_VALUES = LEFT(this.w_VALUES,LEN(this.w_VALUES)-2)
    if vartype(g_AZRIPCOM)="C" AND g_AZRIPCOM="S"
      * --- Determino quanti movimenti devo creare
      if g_APPLICATION = "ADHOC REVOLUTION"
        this.w_DATINI = NVL(BV_MRINICOM, BV_CMDATREG)
        this.w_DATFIN = NVL(BV_MRFINCOM, BV_CMDATREG)
      else
        this.w_DATINI = NVL(BV_MRINICOM, BV_DATA)
        this.w_DATFIN = NVL(BV_MRFINCOM, BV_DATA)
      endif
      * --- Se ho datetime devo trasformarli
      if TYPE("this.w_DATINI") = "T"
        this.w_DATINI = TTOD(this.w_DATINI)
      endif
      if TYPE("this.w_DATFIN") = "T"
        this.w_DATFIN = TTOD(this.w_DATFIN)
      endif
      this.w_NUMMOV = this.w_DATFIN - this.w_DATINI + 1
      this.w_NUMMOV = IIF(this.w_NUMMOV<1, 1, this.w_NUMMOV)
      if this.w_NUMMOV>1
        this.w_COUNT = this.w_NUMMOV
        this.w_DATECUR = this.w_DATINI
        * --- Salvo i valori originali
        Select (this.oParentObject.w_RENOMCUR)
        if g_APPLICATION = "ADHOC REVOLUTION"
          this.w_IMPAVEEF = BV_IMPAVEEF
          this.w_IMPDAREF = BV_IMPDAREF
          this.w_IMPAVEPR = BV_IMPAVEPR
          this.w_IMPDARPR = BV_IMPDARPR
          * --- Ripartisco gli importi
           
 BV_IMPAVEEF = cp_ROUND(this.w_IMPAVEEF / this.w_NUMMOV, g_PERPVL) 
 BV_IMPDAREF = cp_ROUND(this.w_IMPDAREF / this.w_NUMMOV, g_PERPVL) 
 BV_IMPAVEPR = cp_ROUND(this.w_IMPAVEPR / this.w_NUMMOV, g_PERPVL) 
 BV_IMPDARPR = cp_ROUND(this.w_IMPDARPR / this.w_NUMMOV, g_PERPVL)
        else
          this.w_IMPAVEEF = BV_CONSUNTIVO
          this.w_IMPDAREF = BV_BUDGET
          this.w_IMPAVEPR = BV_IMPEGNO
          * --- Ripartisco gli importi
           
 BV_CONSUNTIVO = cp_ROUND(this.w_IMPAVEEF / this.w_NUMMOV, g_PERPVL) 
 BV_BUDGET = cp_ROUND(this.w_IMPDAREF / this.w_NUMMOV, g_PERPVL) 
 BV_IMPEGNO = cp_ROUND(this.w_IMPAVEPR / this.w_NUMMOV, g_PERPVL)
        endif
        * --- Azzero i resti
        this.w_RESAVEEF = 0
        this.w_RESDAREF = 0
        this.w_RESAVEPR = 0
        this.w_RESDARPR = 0
        * --- Eseguo l'esplosione
        do while this.w_COUNT>0
          * --- Modifico la data di registrazione
          if g_APPLICATION = "ADHOC REVOLUTION"
            BV_CMDATREG = this.w_DATECUR
          else
            BV_DATA = this.w_DATECUR
          endif
          if this.w_COUNT=1
            * --- Se sto inserendo l'ultimo inserisco anche i resti
            if g_APPLICATION = "ADHOC REVOLUTION"
               
 BV_IMPAVEEF = this.w_IMPAVEEF - this.w_RESAVEEF 
 BV_IMPDAREF = this.w_IMPDAREF - this.w_RESDAREF 
 BV_IMPAVEPR = this.w_IMPAVEPR - this.w_RESAVEPR 
 BV_IMPDARPR = this.w_IMPDARPR - this.w_RESDARPR
            else
               
 BV_CONSUNTIVO = this.w_IMPAVEEF - this.w_RESAVEEF 
 BV_BUDGET = this.w_IMPDAREF - this.w_RESDAREF 
 BV_IMPEGNO = this.w_IMPAVEPR - this.w_RESAVEPR
            endif
          else
            * --- Incremento i resti
            if g_APPLICATION = "ADHOC REVOLUTION"
              this.w_RESAVEEF = this.w_RESAVEEF + BV_IMPAVEEF
              this.w_RESDAREF = this.w_RESDAREF + BV_IMPDAREF
              this.w_RESAVEPR = this.w_RESAVEPR + BV_IMPAVEPR
              this.w_RESDARPR = this.w_RESDARPR + BV_IMPDARPR
            else
              this.w_RESAVEEF = this.w_RESAVEEF + BV_CONSUNTIVO
              this.w_RESDAREF = this.w_RESDAREF + BV_BUDGET
              this.w_RESAVEPR = this.w_RESAVEPR + BV_IMPEGNO
            endif
          endif
          * --- Aggiungo la nuova riga al cursore finale
          if this.w_COUNT=1 and mod(recno(), iif(this.w_NUMRECORD>10000, 1000, iif(this.w_NUMRECORD>1000, 100, iif(this.w_NUMRECORD>100, 10, 1))))=0
            ah_msg( "Inserimento record n� %1 / %2",.t.,.f.,.f.,alltrim(str(recno())),alltrim(str(this.w_NUMRECORD)) )
          endif
          if i_nACTIVATEPROFILER>0
            righe=cp_SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALUES_BIND+")")
          else
            righe=SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALUES_BIND+")")
          endif
          if righe<0 or bTrsErr
            * --- Raise
            i_Error=AH_MsgFormat("Errore inserimento dati")
            return
          endif
          this.w_DATECUR = this.w_DATECUR + 1
          this.w_COUNT = this.w_COUNT - 1
        enddo
      else
        * --- Modifico la data di registrazione
        if g_APPLICATION = "ADHOC REVOLUTION"
          BV_CMDATREG = this.w_DATINI
        else
          BV_DATA = this.w_DATINI
        endif
        if mod(recno(), iif(this.w_NUMRECORD>10000, 1000, iif(this.w_NUMRECORD>1000, 100, iif(this.w_NUMRECORD>100, 10, 1))))=0
          ah_msg( "Inserimento record n� %1 / %2",.t.,.f.,.f.,alltrim(str(recno())),alltrim(str(this.w_NUMRECORD)) )
        endif
        if i_nACTIVATEPROFILER>0
          righe=cp_SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALUES_BIND+")")
        else
          righe=SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALUES_BIND+")")
        endif
        if righe<0 or bTrsErr
          * --- Raise
          i_Error=AH_MsgFormat("Errore inserimento dati")
          return
        endif
      endif
    else
      if mod(recno(), iif(this.w_NUMRECORD>10000, 1000, iif(this.w_NUMRECORD>1000, 100, iif(this.w_NUMRECORD>100, 10, 1))))=0
        ah_msg( "Inserimento record n� %1 / %2",.t.,.f.,.f.,alltrim(str(recno())),alltrim(str(this.w_NUMRECORD)) )
      endif
      if i_nACTIVATEPROFILER>0
        righe=cp_SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALUES_BIND+")")
      else
        righe=SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALUES_BIND+")")
      endif
      if righe<0 or bTrsErr
        * --- Raise
        i_Error=AH_MsgFormat("Errore inserimento dati")
        return
      endif
    endif
    Select (this.oParentObject.w_RENOMCUR)
    endscan
    * --- commit
    cp_EndTrs(.t.)
    if vartype(g_AZRIPCOM)="C" AND g_AZRIPCOM="S"
      release g_AZRIPCOM
    endif
    this.oParentObject.w_MSG = ah_msgformat("Inserimento completato con successo")
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione Tabella 
    * --- Try
    local bErr_05039848
    bErr_05039848=bTrsErr
    this.Try_05039848()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if Messaggio="Errata"
        Messaggio=""
        this.oParentObject.w_PARMSG1 = alltrim(this.pTabella)
        this.oParentObject.w_PARMSG2 = ALLTRIM(this.w_VarError)
        this.oParentObject.w_errore = "Errore nella creazione della tabella %1 %0Il campo %2 ha un tipo non riconosciuto"
      else
        if messaggio= "Query_vuota"
          Messaggio=""
          if alltrim(this.pTabella)<>"_DW"
            this.oParentObject.w_PARMSG1 = alltrim(this.pTabella)
            this.oParentObject.w_PARMSG2 = SPACE(0)
            this.oParentObject.w_errore = "Errore nella creazione della tabella %1 %0Query vuota"
          else
            this.oParentObject.w_PARMSG1 = alltrim(this.pTabella)
            this.oParentObject.w_PARMSG2 = SPACE(0)
            this.oParentObject.w_errore = "Errore nella creazione della tabella ???%1 %0Query vuota"
          endif
        else
          this.oParentObject.w_PARMSG1 = alltrim(this.pTabella)
          this.oParentObject.w_PARMSG2 = alltrim(message())
          this.oParentObject.w_errore = "Errore nella creazione della tabella %1%0%2"
        endif
      endif
      * --- accept error
      bTrsErr=.f.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_05039848
    * --- End
  endproc
  proc Try_05039848()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    Declare STRUCTAB (1,5)
    this.w_FIELDS = ""
    if not(empty(this.pquery))
      vq_exec(this.pQuery,this,"CursTab")
    else
      Messaggio = "Query_vuota"
      * --- Raise
      i_Error=AH_MsgFormat("Query vuota")
      return
    endif
    if Used("CursTab")
      Afields(STRUCTAB,"CursTab")
      for nFields = 1 to alen(STRUCTAB,1) 
      i_TmpErr=on("ERROR") 
 ON ERROR Messaggio = "Errata"
      this.w_VarError = STRUCTAB(nFields,1)
      this.w_FIELDS = this.w_FIELDS + STRUCTAB(nFields,1)
      if STRUCTAB(nFields,2) == "M"
        this.w_LUNMEMO = IIF(TYPE("G_INFOMEMOLENGTH")<>"U", G_INFOMEMOLENGTH, IIF(this.w_DB<>"Oracle", 4000, 2000)) 
        this.w_FIELDS = this.w_FIELDS+" "+db_FieldType(this.w_DB,"C",this.w_LUNMEMO,0)+", "
      else
        this.w_FIELDS = this.w_FIELDS+" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(nFields,2)$"I-B-F","N",STRUCTAB(nFields,2)),STRUCTAB(nFields,3),STRUCTAB(nFields,4))+", "
      endif
      ON ERROR &i_TmpErr
      if Messaggio="Errata"
        * --- Raise
        i_Error=AH_MsgFormat("Errore creazione tabella")
        return
      endif
      endfor
      if this.w_MULTIAZI="S"
        this.w_FIELDS = this.w_FIELDS + "CODAZIEN" + " " + db_FieldType(this.w_DB,"C",5,0) + ", "
        this.w_FIELDS = this.w_FIELDS + "DESAZIEN" + " " + db_FieldType(this.w_DB,"C",80,0) + ", "
        * --- Verifico la correttezza dei campi di concatenazione
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
          * --- Accodo i campi di concatenazione
          i=1
          do while i <= alen(FIELDLIST)
            * --- Calcolo l'effettivo numero di riga
            posiz = int(ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1) / 18) + 1
            this.w_FIELDS = this.w_FIELDS + FIELDLIST(i)
            if STRUCTAB(posiz,2) == "M"
              this.w_LUNMEMO = IIF(TYPE("G_INFOMEMOLENGTH")<>"U", G_INFOMEMOLENGTH, IIF(this.w_DB<>"Oracle", 4000, 2000)) 
              this.w_FIELDS = this.w_FIELDS+" "+db_FieldType(this.w_DB,"C",this.w_LUNMEMO,0)+", "
            else
              this.w_FIELDS = this.w_FIELDS+" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(posiz,2)$"I-B-F","N",STRUCTAB(posiz,2)),STRUCTAB(posiz,3)+5,STRUCTAB(posiz,4))+", "
            endif
            i=i+1
          enddo
        endif
      endif
      this.w_FIELDS = left(this.w_FIELDS,len(this.w_FIELDS)-2)
      righe = sqlexec(this.w_CONNECT,"Create table "+this.pTabella+" "+"("+this.w_FIELDS+")")
    else
      bTrsErr = .t.
    endif
    if righe<0 or bTrsErr
      * --- Raise
      i_Error=AH_MsgFormat("Errore creazione tabella")
      return
    endif
    this.oParentObject.w_MSG = ah_msgformat("Tabella %1 creata", alltrim(this.ptabella))
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione lista campi  tabella (senza tipi!)
    if ! used (this.oParentObject.w_RENOMCUR) 
      Afields(STRUCTAB,"CursTab")
    else
      Afields(STRUCTAB,this.oParentObject.w_RENOMCUR)
    endif
    for nFields = 1 to alen(STRUCTAB,1) 
    this.w_FIELDS = this.w_FIELDS + STRUCTAB(nFields,1)+", "
    this.w_VALUES_BIND = this.w_VALUES_BIND +"?BV_"+STRUCTAB(nFields,1)+ ","
    endfor
    if this.w_MULTIAZI="S"
      this.w_FIELDS = this.w_FIELDS + "CODAZIEN" + ", "+ "DESAZIEN" + ", "
      this.w_VALUES_BIND = this.w_VALUES_BIND +"?BV_"+"CODAZIEN" + ", "+ "?BV_"+"DESAZIEN" + ", "
      * --- Verifico la correttezza dei campi di concatenazione
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
        * --- Accodo i campi di concatenazione
        i=1
        do while i <= alen(FIELDLIST)
          * --- Campi
          this.w_FIELDS = this.w_FIELDS+FIELDLIST(i)+", "
          * --- Valori
          this.w_VALUES_BIND = this.w_VALUES_BIND +"?BV_"+FIELDLIST(i)+ ","
          i = i +1
        enddo
      endif
    endif
    this.w_FIELDS = left(this.w_FIELDS,len(this.w_FIELDS)-2)
    this.w_VALUES_BIND = left(ALLTRIM(this.w_VALUES_BIND),len(ALLTRIM(this.w_VALUES_BIND))-1)
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione indici
    * --- Controllo se la tabella � stata creata
    sqlexec(this.w_CONNECT,"Select * from "+this.pTABELLA,"RISULTATO")
    if ! USED("RISULTATO") 
      Ah_errormsg("La tabella %1 non � stata creata%0L'operazione sar� sospesa",48, ,alltrim(this.ptabella))
      i_retcode = 'stop'
      return
    endif
    * --- Select from INF_INDI
    i_nConn=i_TableProp[this.INF_INDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_INDI_idx,2],.t.,this.INF_INDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select GICAMPIN,CPROWNUM  from "+i_cTable+" INF_INDI ";
          +" where GITABNAM="+cp_ToStrODBC(this.pTABELLA)+"";
          +" order by CPROWORD";
           ,"_Curs_INF_INDI")
    else
      select GICAMPIN,CPROWNUM from (i_cTable);
       where GITABNAM=this.pTABELLA;
       order by CPROWORD;
        into cursor _Curs_INF_INDI
    endif
    if used('_Curs_INF_INDI')
      select _Curs_INF_INDI
      locate for 1=1
      do while not(eof())
      this.w_NUMINDX = _Curs_INF_INDI.CPROWNUM
      this.w_CAMPINDX = _Curs_INF_INDI.GICAMPIN
      this.w_CMD = "create index "+alltrim(this.pTABELLA)+"_inf"+alltrim(str(this.w_NUMINDX)) + " on " +alltrim(this.pTABELLA) + "  (" +alltrim(this.w_CAMPINDX)+")"
      sqlexec(this.w_CONNECT,this.w_CMD)
      this.w_INDEXCRE = .T.
        select _Curs_INF_INDI
        continue
      enddo
      use
    endif
    if this.w_INDEXCRE
      this.oParentObject.w_MSG = ah_msgformat("Indice creato")
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo correttezza nome campi e tipo dei campi da concatenare
    this.w_TOTFIELD = alen(STRUCTAB,1)
    if TYPE ("FIELDLIST")<>"U"
      for i=1 to alen(FIELDLIST)
      if ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1) > 0
        posiz = int(ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1) / 18) + 1
      else
        posiz = 0
      endif
      if posiz = 0 or STRUCTAB(posiz,2) <> "C"
        this.w_CONCATOK = .F.
        * --- Inserisce il messaggio nel Log
        AddMsgNL("Attenzione: il campo %1 non � valido o � di tipo diverso da carattere.",this.oParentObject,FIELDLIST(i))
      endif
      next
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOperaz,pTabella,pQUERY)
    this.pOperaz=pOperaz
    this.pTabella=pTabella
    this.pQUERY=pQUERY
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='INF_GRUD'
    this.cWorkTables[2]='INF_TAB'
    this.cWorkTables[3]='INF_INDI'
    this.cWorkTables[4]='INF_TAAZ'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_INF_INDI')
      use in _Curs_INF_INDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperaz,pTabella,pQUERY"
endproc
