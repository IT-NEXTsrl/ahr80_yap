* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_ban                                                        *
*              Esplosione analitica x competenza                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-01-10                                                      *
* Last revis.: 2011-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperaz,pTabella,pQUERY
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_ban",oParentObject,m.pOperaz,m.pTabella,m.pQUERY)
return(i_retval)

define class tgsin_ban as StdBatch
  * --- Local variables
  pOperaz = space(2)
  pTabella = space(50)
  pQUERY = space(20)
  w_ORNOMCUR = space(20)
  w_AZRIPCOM = space(1)
  w_NUMMOV = 0
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DATECUR = ctod("  /  /  ")
  w_COUNT = 0
  w_IMPAVEEF = 0
  w_IMPDAREF = 0
  w_IMPAVEPR = 0
  w_IMPDARPR = 0
  w_RESAVEEF = 0
  w_RESDAREF = 0
  w_RESAVEPR = 0
  w_RESDARPR = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  PAR_INFO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplosione dei movimenti di analitica per competenza
    *     Ogni movimento con competenza viene esploso in competenza giornaliera
    * --- Verifico se l'azienda gestisce l'analitica per competenza
    * --- Read from PAR_INFO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_INFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_INFO_idx,2],.t.,this.PAR_INFO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "INRIPCOM"+;
        " from "+i_cTable+" PAR_INFO where ";
            +"INCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        INRIPCOM;
        from (i_cTable) where;
            INCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZRIPCOM = NVL(cp_ToDate(_read_.INRIPCOM),cp_NullValue(_read_.INRIPCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_AZRIPCOM="S"
      public g_AZRIPCOM 
 g_AZRIPCOM="S"
      * --- Devo eseguire l'esplosione
      this.w_ORNOMCUR = this.oParentObject.w_RENOMCUR
      this.oParentObject.w_RENOMCUR = SYS(2015)
      * --- Creo un nuovo cursore scrivibile con la struttura del cursore della query
      SELECT * FROM (this.w_ORNOMCUR) INTO CURSOR (this.oParentObject.w_RENOMCUR) READWRITE
      * --- Ciclo su i movimenti
      SELECT (this.w_ORNOMCUR)
      LOCATE FOR 1=1
      * --- Chiudo il cursore
      USE IN SELECT(this.w_ORNOMCUR)
    endif
  endproc


  proc Init(oParentObject,pOperaz,pTabella,pQUERY)
    this.pOperaz=pOperaz
    this.pTabella=pTabella
    this.pQUERY=pQUERY
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='PAR_INFO'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperaz,pTabella,pQUERY"
endproc
