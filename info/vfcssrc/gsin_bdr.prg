* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bdr                                                        *
*              Cancellazione record - eliminazione tabelle                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-01                                                      *
* Last revis.: 2006-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOOP,pTABELLA,pQUERY
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bdr",oParentObject,m.pTIPOOP,m.pTABELLA,m.pQUERY)
return(i_retval)

define class tgsin_bdr as StdBatch
  * --- Local variables
  pTIPOOP = space(2)
  pTABELLA = space(50)
  pQUERY = space(20)
  w_ODBC = space(25)
  w_STRINGA = space(90)
  w_CRIPTATA = space(1)
  w_CONNECT = 0
  w_CONNECTION = space(25)
  w_WHERE = space(255)
  w_WHERE2 = space(255)
  w_DATFIELD = space(25)
  w_MULTIAZI = space(1)
  w_FILTROIN = space(100)
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_FILTRO = space(100)
  w_OLD_SET_DATE = space(100)
  * --- WorkFile variables
  INF_TAB_idx=0
  INF_GRUD_idx=0
  INF_TAAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la cancellazione di dati o l'elimazione delle tabelle
    this.w_DATAINI = this.oParentObject.oParentObject.w_DATAINI
    this.w_DATAFIN = this.oParentObject.oParentObject.w_DATAFIN
    * --- Modifico il nome della tabella nel nostro standard
    this.pTABELLA = alltrim(this.pTABELLA)+"_DW"
    * --- Acquisisco il nome dell'ODBC o la stringa di connessione relativo alla tabella passata come parametro (pTabella)
    * --- Prima controllo se esiste una connessione di "default" per la tabella inerente  all'azienda in uso
    
    * --- Read from INF_TAAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TACOODBC,ATCOSTRI,ATCRIPTE"+;
        " from "+i_cTable+" INF_TAAZ where ";
            +"TACODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TACOODBC,ATCOSTRI,ATCRIPTE;
        from (i_cTable) where;
            TACODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
      this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
      this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo nell'archivio Tabelle / Connessioni 
      * --- Read from INF_TAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAB where ";
              +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              ATNTABLE = this.pTabella;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.ATCOODBC),cp_NullValue(_read_.ATCOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo se esiste per la tabella una connessione globale (cio� indipendente dall'azienda)
      * --- Read from INF_TAAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TACOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAAZ where ";
              +"TACODAZI = "+cp_ToStrODBC(space(5));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TACOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              TACODAZI = space(5);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Database  diverso da quello usato dalla procedura.
      *     Creo una nuova connessione
      if this.w_CRIPTATA = "C"
        this.w_STRINGA = CifraCnf( ALLTRIM(this.w_STRINGA) , "D" )
      endif
      this.w_CONNECTION = alltrim(this.w_ODBC + this.w_STRINGA)
      this.w_CONNECT = cp_openDatabaseConnection(this.w_connection)
    else
      * --- Stessa connessione della procedura
      this.w_CONNECT = i_TableProp[this.INF_GRUD_idx,3]
    endif
    if this.w_CONNECT <= 0
      AH_ERRORMSG("Connessione fallita",48)
      this.oParentObject.w_errore = "%1"
      this.oParentObject.w_PARMSG1 = alltrim(message())
      i_retcode = 'stop'
      return
    else
      ah_msg( "Connessione riuscita..." )
    endif
    ah_msg("%1",.t., , ,alltrim(this.pTabella) )
    do case
      case this.pTIPOOP="DA"
        * --- Leggo il nome del campo su cui filtrare per data
        * --- Read from INF_TAB
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.INF_TAB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATDAFILT,ATMULTAZ"+;
            " from "+i_cTable+" INF_TAB where ";
                +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATDAFILT,ATMULTAZ;
            from (i_cTable) where;
                ATNTABLE = this.pTabella;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATFIELD = NVL(cp_ToDate(_read_.ATDAFILT),cp_NullValue(_read_.ATDAFILT))
          this.w_MULTIAZI = NVL(cp_ToDate(_read_.ATMULTAZ),cp_NullValue(_read_.ATMULTAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_MULTIAZI="S"
          * --- Costruisco il filtro per le aziende
          this.w_FILTROIN = " CODAZIEN ="+CP_TOSTRODBC( Alltrim( i_CODAZI ) )
        endif
        this.w_DATFIELD = ALLTRIM(this.w_DATFIELD)
        * --- Costruisco la condizione di filtro
        if EMPTY(NVL(this.w_DATFIELD,""))
          if AH_yesno("Non � stato indicato il campo data su cui eseguire il filtro. %0Si vuole cancellare tutti i dati della tabella?")
            if this.w_MULTIAZI="S"
              this.w_FILTRO = ""
            else
              this.w_FILTRO = "1 = 1"
            endif
          else
            i_retcode = 'stop'
            return
          endif
        else
          do case
            case not empty (this.oparentobject.oparentobject.w_DATAINI) and not empty (this.oparentobject.oparentobject.w_DATAFIN)
              this.w_FILTRO = alltrim(this.w_DATFIELD +"  >= "+cp_ToStrODBC(this.oparentobject.oparentobject.w_DATAINI)+" and "+ this.w_DATFIELD + "<= "+cp_ToStrODBC(this.oparentobject.oparentobject.w_DATAFIN))
            case not empty (this.oparentobject.oparentobject.w_DATAINI) and empty (this.oparentobject.oparentobject.w_DATAFIN)
              this.w_FILTRO = alltrim(this.w_DATFIELD +"  >= "+cp_ToStrODBC(this.oparentobject.oparentobject.w_DATAINI))
            case empty (this.oparentobject.oparentobject.w_DATAINI) and not empty (this.oparentobject.oparentobject.w_DATAFIN)
              this.w_FILTRO = alltrim(this.w_DATFIELD + "<= "+cp_ToStrODBC(this.oparentobject.oparentobject.w_DATAFIN))
            otherwise
              this.w_FILTRO = ""
          endcase
        endif
        * --- Cancello tutti i records della tabella aventi la data compresa nell'intervallo selezionato
        * --- Try
        local bErr_0345D9E8
        bErr_0345D9E8=bTrsErr
        this.Try_0345D9E8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.oParentObject.w_errore = "Errore cancellazione dati"
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0345D9E8
        * --- End
      case this.pTIPOOP="DD"
        * --- Elimino la tabella
        * --- Try
        local bErr_0345A328
        bErr_0345A328=bTrsErr
        this.Try_0345A328()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.oParentObject.w_errore = "%1"
          this.oParentObject.w_PARMSG1 = alltrim(message())
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0345A328
        * --- End
      case this.pTIPOOP="DT"
        * --- Cancello tutti i records della tabella
        * --- Read from INF_TAB
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.INF_TAB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATMULTAZ"+;
            " from "+i_cTable+" INF_TAB where ";
                +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATMULTAZ;
            from (i_cTable) where;
                ATNTABLE = this.pTabella;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MULTIAZI = NVL(cp_ToDate(_read_.ATMULTAZ),cp_NullValue(_read_.ATMULTAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_MULTIAZI="S"
          * --- Costruisco il filtro per le aziende
          this.w_FILTROIN = " CODAZIEN ="+CP_TOSTRODBC( Alltrim( i_CODAZI ) )
        endif
        * --- Try
        local bErr_03459518
        bErr_03459518=bTrsErr
        this.Try_03459518()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.oParentObject.w_errore = "%1"
          this.oParentObject.w_PARMSG1 = alltrim(message())
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03459518
        * --- End
      case this.pTIPOOP="DQ"
        * --- Cancello i records della tabella presenti nella query
        * --- Read from INF_TAB
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.INF_TAB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATMULTAZ"+;
            " from "+i_cTable+" INF_TAB where ";
                +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATMULTAZ;
            from (i_cTable) where;
                ATNTABLE = this.pTabella;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MULTIAZI = NVL(cp_ToDate(_read_.ATMULTAZ),cp_NullValue(_read_.ATMULTAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty(this.w_ODBC) and empty(this.w_STRINGA) and this.w_MULTIAZI<>"S"
          i_commit = .f.
          if type("nTrsConnCnt")="U" or nTrsConnCnt=0 
            cp_BeginTrs() 
 i_commit = .t.
          endif
          local i_qry,i_loader 
 set proc to cp_query additive 
 i_qry=createobject("cpquery") 
 i_loader=createobject("cpqueryloader") 
 i_loader.LoadQuery(this.pQuery,i_qry,this) 
 i_cTempTable=i_qry.mDoQuery("nocursor","Get",.null.) 
 i_Rows=cp_TrsSQL(i_nConn,"delete from "+this.pTabella; 
 +" where exists( select 1 from ("+i_cTempTable+") cptmp_1xab23 "; 
 +")")
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr or i_rows < 0
            this.oParentObject.w_errore = "Errore cancellazione dati: %0%1"
            this.oParentObject.w_PARMSG1 = alltrim(message())
            i_Error=AH_MsgFormat(MSG_DELETE_ERROR)
          else
            this.oParentObject.w_MSG = AH_MsgFormat("Cancellazione completata con successo")
          endif
        else
          if this.w_MULTIAZI="S"
            * --- Costruisco il filtro per le aziende
            this.w_FILTROIN = " And CODAZIEN ="+CP_TOSTRODBC( Alltrim( i_CODAZI ) )
          else
            this.w_FILTROIN = " And 1=1"
          endif
          * --- begin transaction
          cp_BeginTrs()
          * --- Try
          local bErr_04BA4F58
          bErr_04BA4F58=bTrsErr
          this.Try_04BA4F58()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_errore = "Errore cancellazione dati: %0%1"
            this.oParentObject.w_PARMSG1 = alltrim(message())
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_04BA4F58
          * --- End
        endif
    endcase
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Mi disconetto dalla nuova connessione.
      Sqldisconnect(this.w_CONNECT)
    endif
    if used("CursTab")
      Select CursTab 
 use
    endif
  endproc
  proc Try_0345D9E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if empty(this.w_FILTRO)
      righe = SQLEXEC(this.w_CONNECT,"delete from "+this.pTabella+IIF(this.w_MULTIAZI="S"," where "+this.w_FILTROIN,""))
    else
      righe = SQLEXEC(this.w_CONNECT,"delete from "+this.pTabella+" where " + this.w_FILTRO+IIF(this.w_MULTIAZI="S"," and "+this.w_FILTROIN,""))
    endif
    if righe < 0
      * --- Raise
      i_Error=AH_MsgFormat("Errore cancellazione dati")
      return
    endif
    return
  proc Try_0345A328()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    righe=SQLEXEC(this.w_CONNECT,"drop table "+this.pTabella)
    if righe < 0
      * --- Raise
      i_Error=AH_MsgFormat("Errore eliminazione tabella")
      return
    endif
    return
  proc Try_03459518()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_MULTIAZI="S"
      righe=SQLEXEC(this.w_CONNECT,"delete from "+this.pTabella + " where " + this.w_FILTROIN)
    else
      righe=SQLEXEC(this.w_CONNECT,"delete from "+this.pTabella)
    endif
    if righe < 0
      * --- Raise
      i_Error=AH_MsgFormat("Errore eliminazione tabella")
      return
    endif
    return
  proc Try_04BA4F58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    Declare STRUCTAB (1,5)
    vq_exec(this.pQuery,this,"CursTab")
    this.w_OLD_SET_DATE = Set("Date")
    set date to american
    Select CursTab 
 Go Top 
 Scan
    this.w_WHERE = ""
    this.w_WHERE2 = ""
    for i=1 to fcount() 
 w_campo=field(i) 
 valore=&w_campo 
 if type("valore") $ "CM" 
 valore = strtran(valore,'"',space(1)) 
 endif
    if len(this.w_WHERE)+len(iif(type("valore")="C",valore,iif(type("valore")="T",ttoc(valore),str(valore)))) > 252
      this.w_WHERE2 = this.w_WHERE2 + w_campo + " = "+cp_ToStrODBC(valore)+" and "
    else
      this.w_WHERE = this.w_WHERE + w_campo + " = "+cp_ToStrODBC(valore)+" and "
    endif
    endfor
    this.w_WHERE = LEFT(this.w_WHERE,LEN(this.w_WHERE)-5)
    this.w_WHERE2 = iif(len(this.w_WHERE2)>5," and "+LEFT(this.w_WHERE2,LEN(this.w_WHERE2)-5),"")
    ah_msg( "Cancellazione record n� %1",.t.,.f.,.f.,str(recno()) )
    righe=SqlExec(this.w_connect,"Delete from "+this.pTabella+" where "+this.w_WHERE+this.w_WHERE2+this.w_FILTROIN)
    if righe<0 or bTrsErr
      * --- Raise
      i_Error=AH_MsgFormat("Errore cancellazione dati")
      return
    endif
    endscan
    set date to ( this.w_OLD_SET_DATE )
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_MSG = AH_MsgFormat("Cancellazione completata con successo")
    return


  proc Init(oParentObject,pTIPOOP,pTABELLA,pQUERY)
    this.pTIPOOP=pTIPOOP
    this.pTABELLA=pTABELLA
    this.pQUERY=pQUERY
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='INF_TAB'
    this.cWorkTables[2]='INF_GRUD'
    this.cWorkTables[3]='INF_TAAZ'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOOP,pTABELLA,pQUERY"
endproc
