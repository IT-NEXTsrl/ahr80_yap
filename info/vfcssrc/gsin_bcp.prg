* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bcp                                                        *
*              Calcolo - caricamento periodi                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][60]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-30                                                      *
* Last revis.: 2010-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bcp",oParentObject)
return(i_retval)

define class tgsin_bcp as StdBatch
  * --- Local variables
  w_NUMRECOR = 0
  w_GIORNO = ctod("  /  /  ")
  w_SETTIMA = ctod("  /  /  ")
  w_MESE = ctod("  /  /  ")
  w_BIMESTRE = ctod("  /  /  ")
  w_TRIMESTR = ctod("  /  /  ")
  w_QUADRIME = ctod("  /  /  ")
  w_SEMESTRE = ctod("  /  /  ")
  w_ANNO = ctod("  /  /  ")
  w_INIDAT = ctod("  /  /  ")
  w_FINDAT = ctod("  /  /  ")
  * --- WorkFile variables
  INF_PERI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento dati Periodi - Nella tabella "INF_PERI"
    * --- Controllo se i periodi sono gi� stati caricati
    * --- Select from INF_PERI
    i_nConn=i_TableProp[this.INF_PERI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_PERI_idx,2],.t.,this.INF_PERI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" INF_PERI ";
           ,"_Curs_INF_PERI")
    else
      select * from (i_cTable);
        into cursor _Curs_INF_PERI
    endif
    if used('_Curs_INF_PERI')
      select _Curs_INF_PERI
      locate for 1=1
      do while not(eof())
      this.w_NUMRECOR = this.w_NUMRECOR + 1
        select _Curs_INF_PERI
        continue
      enddo
      use
    endif
    if this.w_NUMRECOR > 0
      if AH_YESNO("Sono presenti dei periodi%0Si vogliono ricaricare tutti i periodi?") 
        * --- Delete from INF_PERI
        i_nConn=i_TableProp[this.INF_PERI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INF_PERI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Data giorno iniziale - Inizializzazione dei periodi iniziali
    this.w_INIDAT = cp_CharToDate(IIF(TYPE("g_INFOINIDAT")<>"C", "01-01-1990", g_INFOINIDAT))
    this.w_FINDAT = cp_CharToDate(IIF(TYPE("g_INFOFINDAT")<>"C", "01-01-2020", g_INFOFINDAT))
    this.w_GIORNO = this.w_INIDAT
    this.w_SETTIMA = this.w_INIDAT
    this.w_MESE = this.w_INIDAT
    this.w_BIMESTRE = this.w_INIDAT
    this.w_TRIMESTR = this.w_INIDAT
    this.w_QUADRIME = this.w_INIDAT
    this.w_SEMESTRE = this.w_INIDAT
    this.w_ANNO = this.w_INIDAT
    do while this.w_GIORNO < this.w_FINDAT
      ah_msg("Inserimento dati giorno: %1",.t.,.f.,.f.,DTOC(this.w_GIORNO) )
      * --- Insert into INF_PERI
      i_nConn=i_TableProp[this.INF_PERI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_PERI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_PERI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COD_DAY"+",COD_WEEK"+",COD_MONTH"+",COD_2M"+",COD_3M"+",COD_4M"+",COD_6M"+",COD_YEAR"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_GIORNO),'INF_PERI','COD_DAY');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SETTIMA),'INF_PERI','COD_WEEK');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MESE),'INF_PERI','COD_MONTH');
        +","+cp_NullLink(cp_ToStrODBC(this.w_BIMESTRE),'INF_PERI','COD_2M');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TRIMESTR),'INF_PERI','COD_3M');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QUADRIME),'INF_PERI','COD_4M');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SEMESTRE),'INF_PERI','COD_6M');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ANNO),'INF_PERI','COD_YEAR');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COD_DAY',this.w_GIORNO,'COD_WEEK',this.w_SETTIMA,'COD_MONTH',this.w_MESE,'COD_2M',this.w_BIMESTRE,'COD_3M',this.w_TRIMESTR,'COD_4M',this.w_QUADRIME,'COD_6M',this.w_SEMESTRE,'COD_YEAR',this.w_ANNO)
        insert into (i_cTable) (COD_DAY,COD_WEEK,COD_MONTH,COD_2M,COD_3M,COD_4M,COD_6M,COD_YEAR &i_ccchkf. );
           values (;
             this.w_GIORNO;
             ,this.w_SETTIMA;
             ,this.w_MESE;
             ,this.w_BIMESTRE;
             ,this.w_TRIMESTR;
             ,this.w_QUADRIME;
             ,this.w_SEMESTRE;
             ,this.w_ANNO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_GIORNO = this.w_GIORNO + 1
      if this.w_GIORNO = (this.w_SETTIMA +7)
        this.w_SETTIMA = this.w_GIORNO
      endif
      if MONTH(this.w_GIORNO) <> MONTH(this.w_MESE)
        this.w_MESE = cp_CharToDate("01-"+STR(MONTH(this.w_GIORNO))+"-"+STR(YEAR(this.w_GIORNO)))
      endif
      if alltrim(STR(MONTH(this.w_GIORNO))) $ ("3-5-7-9-11")
        this.w_BIMESTRE = cp_CharToDate("01-"+STR(MONTH(this.w_GIORNO))+"-"+STR(YEAR(this.w_GIORNO)))
      endif
      if alltrim(STR(MONTH(this.w_GIORNO))) $ ("4-7-10")
        this.w_TRIMESTR = cp_CharToDate("01-"+STR(MONTH(this.w_GIORNO))+"-"+STR(YEAR(this.w_GIORNO)))
      endif
      if alltrim(STR(MONTH(this.w_GIORNO))) $ ("5-9")
        this.w_QUADRIME = cp_CharToDate("01-"+STR(MONTH(this.w_GIORNO))+"-"+STR(YEAR(this.w_GIORNO)))
      endif
      if MONTH(this.w_GIORNO) = 7
        this.w_SEMESTRE = cp_CharToDate("01-"+STR(MONTH(this.w_GIORNO))+"-"+STR(YEAR(this.w_GIORNO)))
      endif
      if YEAR(this.w_GIORNO) <> YEAR(this.w_ANNO)
        this.w_ANNO = cp_CharToDate("01-01-"+STR(YEAR(this.w_GIORNO)))
        this.w_QUADRIME = cp_CharToDate("01-01-"+STR(YEAR(this.w_GIORNO)))
        this.w_SEMESTRE = cp_CharToDate("01-01-"+STR(YEAR(this.w_GIORNO)))
      endif
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INF_PERI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_INF_PERI')
      use in _Curs_INF_PERI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
