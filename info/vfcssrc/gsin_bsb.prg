* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bsb                                                        *
*              Carica regole esportaz. (InfoLink)                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-29                                                      *
* Last revis.: 2007-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOOP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bsb",oParentObject,m.pTIPOOP)
return(i_retval)

define class tgsin_bsb as StdBatch
  * --- Local variables
  pTIPOOP = space(1)
  w_MESS = space(150)
  * --- WorkFile variables
  INF_REGD_idx=0
  INF_REGM_idx=0
  INF_TAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_034574A8
    bErr_034574A8=bTrsErr
    this.Try_034574A8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_ERRORCAR = "T"
    endif
    bTrsErr=bTrsErr or bErr_034574A8
    * --- End
  endproc
  proc Try_034574A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.pTIPOOP="D"
        * --- Try
        local bErr_04A5B610
        bErr_04A5B610=bTrsErr
        this.Try_04A5B610()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into INF_REGD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.INF_REGD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INF_REGD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_REGD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_REGD','CPROWORD');
            +",RETIPSTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPSTE),'INF_REGD','RETIPSTE');
            +",REDESOPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESOPE),'INF_REGD','REDESOPE');
            +",RENOMCUR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RENOMCUR),'INF_REGD','RENOMCUR');
            +",REPARAME ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REPARAME),'INF_REGD','REPARAME');
                +i_ccchkf ;
            +" where ";
                +"RECODICE = "+cp_ToStrODBC(this.oParentObject.w_RECODICE);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                CPROWORD = this.oParentObject.w_CPROWORD;
                ,RETIPSTE = this.oParentObject.w_RETIPSTE;
                ,REDESOPE = this.oParentObject.w_REDESOPE;
                ,RENOMCUR = this.oParentObject.w_RENOMCUR;
                ,REPARAME = this.oParentObject.w_REPARAME;
                &i_ccchkf. ;
             where;
                RECODICE = this.oParentObject.w_RECODICE;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04A5B610
        * --- End
        if this.oParentObject.w_RETIPSTE="TA"
          * --- Try
          local bErr_04C7FDF8
          bErr_04C7FDF8=bTrsErr
          this.Try_04C7FDF8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into INF_TAB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.INF_TAB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_TAB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATDAFILT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATDAFILT),'INF_TAB','ATDAFILT');
              +",ATMULTAZ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATMULTAZ),'INF_TAB','ATMULTAZ');
              +",ATCRIPTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCRIPTE),'INF_TAB','ATCRIPTE');
              +",ATCOODBC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCOODBC),'INF_TAB','ATCOODBC');
              +",ATCOSTRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCOSTRI),'INF_TAB','ATCOSTRI');
              +",ATCAMCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCAMCON),'INF_TAB','ATCAMCON');
                  +i_ccchkf ;
              +" where ";
                  +"ATNTABLE = "+cp_ToStrODBC(this.oParentObject.w_ATNTABLE);
                     )
            else
              update (i_cTable) set;
                  ATDAFILT = this.oParentObject.w_ATDAFILT;
                  ,ATMULTAZ = this.oParentObject.w_ATMULTAZ;
                  ,ATCRIPTE = this.oParentObject.w_ATCRIPTE;
                  ,ATCOODBC = this.oParentObject.w_ATCOODBC;
                  ,ATCOSTRI = this.oParentObject.w_ATCOSTRI;
                  ,ATCAMCON = this.oParentObject.w_ATCAMCON;
                  &i_ccchkf. ;
               where;
                  ATNTABLE = this.oParentObject.w_ATNTABLE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04C7FDF8
          * --- End
        endif
      case this.pTIPOOP="M"
        * --- Try
        local bErr_04C8D1D8
        bErr_04C8D1D8=bTrsErr
        this.Try_04C8D1D8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into INF_REGM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.INF_REGM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INF_REGM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_REGM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"REDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESCRI),'INF_REGM','REDESCRI');
            +",RE__TIPO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPMOD),'INF_REGM','RE__TIPO');
            +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UTCV),'INF_REGM','UTCV');
            +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'INF_REGM','UTDV');
            +",REDTINVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDTINVA),'INF_REGM','REDTINVA');
            +",REDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDTOBSO),'INF_REGM','REDTOBSO');
                +i_ccchkf ;
            +" where ";
                +"RECODICE = "+cp_ToStrODBC(this.oParentObject.w_RECODICE);
                   )
          else
            update (i_cTable) set;
                REDESCRI = this.oParentObject.w_REDESCRI;
                ,RE__TIPO = this.oParentObject.w_RETIPMOD;
                ,UTCV = this.oParentObject.w_UTCV;
                ,UTDV = SetInfoDate( g_CALUTD );
                ,REDTINVA = this.oParentObject.w_REDTINVA;
                ,REDTOBSO = this.oParentObject.w_REDTOBSO;
                &i_ccchkf. ;
             where;
                RECODICE = this.oParentObject.w_RECODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04C8D1D8
        * --- End
        * --- Inserisco la prima riga di dettaglio
        * --- Try
        local bErr_04C91DA8
        bErr_04C91DA8=bTrsErr
        this.Try_04C91DA8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into INF_REGD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.INF_REGD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INF_REGD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_REGD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_REGD','CPROWORD');
            +",RETIPSTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPSTE),'INF_REGD','RETIPSTE');
            +",REDESOPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESOPE),'INF_REGD','REDESOPE');
            +",RENOMCUR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RENOMCUR),'INF_REGD','RENOMCUR');
            +",REPARAME ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REPARAME),'INF_REGD','REPARAME');
                +i_ccchkf ;
            +" where ";
                +"RECODICE = "+cp_ToStrODBC(this.oParentObject.w_RECODICE);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                CPROWORD = this.oParentObject.w_CPROWORD;
                ,RETIPSTE = this.oParentObject.w_RETIPSTE;
                ,REDESOPE = this.oParentObject.w_REDESOPE;
                ,RENOMCUR = this.oParentObject.w_RENOMCUR;
                ,REPARAME = this.oParentObject.w_REPARAME;
                &i_ccchkf. ;
             where;
                RECODICE = this.oParentObject.w_RECODICE;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04C91DA8
        * --- End
        if this.oParentObject.w_RETIPSTE="TA"
          * --- Try
          local bErr_04C2A568
          bErr_04C2A568=bTrsErr
          this.Try_04C2A568()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into INF_TAB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.INF_TAB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_TAB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATDAFILT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATDAFILT),'INF_TAB','ATDAFILT');
              +",ATMULTAZ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATMULTAZ),'INF_TAB','ATMULTAZ');
              +",ATCRIPTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCRIPTE),'INF_TAB','ATCRIPTE');
              +",ATCOODBC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCOODBC),'INF_TAB','ATCOODBC');
              +",ATCOSTRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCOSTRI),'INF_TAB','ATCOSTRI');
              +",ATCAMCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCAMCON),'INF_TAB','ATCAMCON');
                  +i_ccchkf ;
              +" where ";
                  +"ATNTABLE = "+cp_ToStrODBC(this.oParentObject.w_ATNTABLE);
                     )
            else
              update (i_cTable) set;
                  ATDAFILT = this.oParentObject.w_ATDAFILT;
                  ,ATMULTAZ = this.oParentObject.w_ATMULTAZ;
                  ,ATCRIPTE = this.oParentObject.w_ATCRIPTE;
                  ,ATCOODBC = this.oParentObject.w_ATCOODBC;
                  ,ATCOSTRI = this.oParentObject.w_ATCOSTRI;
                  ,ATCAMCON = this.oParentObject.w_ATCAMCON;
                  &i_ccchkf. ;
               where;
                  ATNTABLE = this.oParentObject.w_ATNTABLE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04C2A568
          * --- End
        endif
    endcase
    return
  proc Try_04A5B610()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_REGD
    i_nConn=i_TableProp[this.INF_REGD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_REGD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_REGD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RECODICE"+",CPROWNUM"+",CPROWORD"+",RETIPSTE"+",REDESOPE"+",RENOMCUR"+",REPARAME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RECODICE),'INF_REGD','RECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'INF_REGD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_REGD','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPSTE),'INF_REGD','RETIPSTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESOPE),'INF_REGD','REDESOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RENOMCUR),'INF_REGD','RENOMCUR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REPARAME),'INF_REGD','REPARAME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RECODICE',this.oParentObject.w_RECODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'CPROWORD',this.oParentObject.w_CPROWORD,'RETIPSTE',this.oParentObject.w_RETIPSTE,'REDESOPE',this.oParentObject.w_REDESOPE,'RENOMCUR',this.oParentObject.w_RENOMCUR,'REPARAME',this.oParentObject.w_REPARAME)
      insert into (i_cTable) (RECODICE,CPROWNUM,CPROWORD,RETIPSTE,REDESOPE,RENOMCUR,REPARAME &i_ccchkf. );
         values (;
           this.oParentObject.w_RECODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_CPROWORD;
           ,this.oParentObject.w_RETIPSTE;
           ,this.oParentObject.w_REDESOPE;
           ,this.oParentObject.w_RENOMCUR;
           ,this.oParentObject.w_REPARAME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04C7FDF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_TAB
    i_nConn=i_TableProp[this.INF_TAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_TAB_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ATNTABLE"+",ATDAFILT"+",ATMULTAZ"+",ATCRIPTE"+",ATCOODBC"+",ATCOSTRI"+",ATCAMCON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATNTABLE),'INF_TAB','ATNTABLE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATDAFILT),'INF_TAB','ATDAFILT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATMULTAZ),'INF_TAB','ATMULTAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCRIPTE),'INF_TAB','ATCRIPTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCOODBC),'INF_TAB','ATCOODBC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCOSTRI),'INF_TAB','ATCOSTRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCAMCON),'INF_TAB','ATCAMCON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ATNTABLE',this.oParentObject.w_ATNTABLE,'ATDAFILT',this.oParentObject.w_ATDAFILT,'ATMULTAZ',this.oParentObject.w_ATMULTAZ,'ATCRIPTE',this.oParentObject.w_ATCRIPTE,'ATCOODBC',this.oParentObject.w_ATCOODBC,'ATCOSTRI',this.oParentObject.w_ATCOSTRI,'ATCAMCON',this.oParentObject.w_ATCAMCON)
      insert into (i_cTable) (ATNTABLE,ATDAFILT,ATMULTAZ,ATCRIPTE,ATCOODBC,ATCOSTRI,ATCAMCON &i_ccchkf. );
         values (;
           this.oParentObject.w_ATNTABLE;
           ,this.oParentObject.w_ATDAFILT;
           ,this.oParentObject.w_ATMULTAZ;
           ,this.oParentObject.w_ATCRIPTE;
           ,this.oParentObject.w_ATCOODBC;
           ,this.oParentObject.w_ATCOSTRI;
           ,this.oParentObject.w_ATCAMCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04C8D1D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserisco la testata
    * --- Insert into INF_REGM
    i_nConn=i_TableProp[this.INF_REGM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_REGM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_REGM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RECODICE"+",REDESCRI"+",RE__TIPO"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",REDTINVA"+",REDTOBSO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RECODICE),'INF_REGM','RECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESCRI),'INF_REGM','REDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPMOD),'INF_REGM','RE__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'INF_REGM','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'INF_REGM','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'INF_REGM','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'INF_REGM','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDTINVA),'INF_REGM','REDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDTOBSO),'INF_REGM','REDTOBSO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RECODICE',this.oParentObject.w_RECODICE,'REDESCRI',this.oParentObject.w_REDESCRI,'RE__TIPO',this.oParentObject.w_RETIPMOD,'UTCC',i_CODUTE,'UTCV',0,'UTDC',SetInfoDate( g_CALUTD ),'UTDV',cp_CharToDate("  -  -    "),'REDTINVA',this.oParentObject.w_REDTINVA,'REDTOBSO',this.oParentObject.w_REDTOBSO)
      insert into (i_cTable) (RECODICE,REDESCRI,RE__TIPO,UTCC,UTCV,UTDC,UTDV,REDTINVA,REDTOBSO &i_ccchkf. );
         values (;
           this.oParentObject.w_RECODICE;
           ,this.oParentObject.w_REDESCRI;
           ,this.oParentObject.w_RETIPMOD;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.oParentObject.w_REDTINVA;
           ,this.oParentObject.w_REDTOBSO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04C91DA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from INF_REGD
    i_nConn=i_TableProp[this.INF_REGD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_REGD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RECODICE = "+cp_ToStrODBC(this.oParentObject.w_RECODICE);
             )
    else
      delete from (i_cTable) where;
            RECODICE = this.oParentObject.w_RECODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into INF_REGD
    i_nConn=i_TableProp[this.INF_REGD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_REGD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_REGD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RECODICE"+",CPROWNUM"+",CPROWORD"+",RETIPSTE"+",REDESOPE"+",RENOMCUR"+",REPARAME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RECODICE),'INF_REGD','RECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'INF_REGD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_REGD','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPSTE),'INF_REGD','RETIPSTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESOPE),'INF_REGD','REDESOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RENOMCUR),'INF_REGD','RENOMCUR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REPARAME),'INF_REGD','REPARAME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RECODICE',this.oParentObject.w_RECODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'CPROWORD',this.oParentObject.w_CPROWORD,'RETIPSTE',this.oParentObject.w_RETIPSTE,'REDESOPE',this.oParentObject.w_REDESOPE,'RENOMCUR',this.oParentObject.w_RENOMCUR,'REPARAME',this.oParentObject.w_REPARAME)
      insert into (i_cTable) (RECODICE,CPROWNUM,CPROWORD,RETIPSTE,REDESOPE,RENOMCUR,REPARAME &i_ccchkf. );
         values (;
           this.oParentObject.w_RECODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_CPROWORD;
           ,this.oParentObject.w_RETIPSTE;
           ,this.oParentObject.w_REDESOPE;
           ,this.oParentObject.w_RENOMCUR;
           ,this.oParentObject.w_REPARAME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04C2A568()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_TAB
    i_nConn=i_TableProp[this.INF_TAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_TAB_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ATNTABLE"+",ATDAFILT"+",ATMULTAZ"+",ATCRIPTE"+",ATCOODBC"+",ATCOSTRI"+",ATCAMCON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATNTABLE),'INF_TAB','ATNTABLE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATDAFILT),'INF_TAB','ATDAFILT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATMULTAZ),'INF_TAB','ATMULTAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCRIPTE),'INF_TAB','ATCRIPTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCOODBC),'INF_TAB','ATCOODBC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCOSTRI),'INF_TAB','ATCOSTRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCAMCON),'INF_TAB','ATCAMCON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ATNTABLE',this.oParentObject.w_ATNTABLE,'ATDAFILT',this.oParentObject.w_ATDAFILT,'ATMULTAZ',this.oParentObject.w_ATMULTAZ,'ATCRIPTE',this.oParentObject.w_ATCRIPTE,'ATCOODBC',this.oParentObject.w_ATCOODBC,'ATCOSTRI',this.oParentObject.w_ATCOSTRI,'ATCAMCON',this.oParentObject.w_ATCAMCON)
      insert into (i_cTable) (ATNTABLE,ATDAFILT,ATMULTAZ,ATCRIPTE,ATCOODBC,ATCOSTRI,ATCAMCON &i_ccchkf. );
         values (;
           this.oParentObject.w_ATNTABLE;
           ,this.oParentObject.w_ATDAFILT;
           ,this.oParentObject.w_ATMULTAZ;
           ,this.oParentObject.w_ATCRIPTE;
           ,this.oParentObject.w_ATCOODBC;
           ,this.oParentObject.w_ATCOSTRI;
           ,this.oParentObject.w_ATCAMCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pTIPOOP)
    this.pTIPOOP=pTIPOOP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='INF_REGD'
    this.cWorkTables[2]='INF_REGM'
    this.cWorkTables[3]='INF_TAB'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOOP"
endproc
