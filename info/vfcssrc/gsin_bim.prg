* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bim                                                        *
*              Esecuzione gruppo di regole                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-24                                                      *
* Last revis.: 2011-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bim",oParentObject)
return(i_retval)

define class tgsin_bim as StdBatch
  * --- Local variables
  w_ORAESEC = space(8)
  w_OPERAZ = space(2)
  w_RECODICE = space(15)
  w_REDESCRI = space(45)
  w_RETIPSTE = space(2)
  w_REDESOPE = space(100)
  w_RENOMCUR = space(20)
  w_PARAMETR = space(1)
  w_RAGSOC = space(40)
  w_PARMSG1 = space(100)
  w_PARMSG2 = space(5)
  w_PARMSG3 = space(200)
  w_PARMSG4 = space(200)
  w_PARMSG5 = space(200)
  w_CODAZI = space(5)
  w_MONOAZI = .f.
  b_VALUTE = .f.
  b_VALUTE = .f.
  w_ORAESECF = space(8)
  w_MEMO = space(0)
  w_EMPTYPARAM = space(254)
  w_MULTIAZI = space(1)
  w_EXIT = .f.
  w_CHKPARAM = .f.
  w_REGOLATT = space(20)
  w_ERRORE = space(255)
  w_MSG = space(200)
  w_TABELLA = space(50)
  w_QUERY = space(20)
  * --- WorkFile variables
  INF_LOG_idx=0
  INF_TAB_idx=0
  INFOCNBL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'esecuzione delle regole presenti nel gruppo di regole selezionato.
    if NOT GSIN_BCB(this,"C")
      i_retcode = 'stop'
      return
    endif
    * --- Leggo ragione sociale azienda
    this.w_RAGSOC = STRTRAN (g_RAGAZI,"'"," ")
    this.oParentobject.oPgFrm.ActivePage = 2
    this.CancLog(this)
    vq_exec("..\INFO\EXE\GSIN_BIM.VQR",this,"ELABORA")
    CE = WRCURSOR("ELABORA")
    this.w_ORAESEC = time()
    AddMsgNL("Fase 1: caricamento delle regole appartenenti al gruppo %1",this,this.oParentObject.w_CODGRU)
    if USED("ELABORA")
      this.w_MONOAZI = .T.
      this.w_CODAZI = i_CODAZI
       
 Select TACODAZI,TADESAZI From ( this.oParentObject.w_AZIENDE.cCursor ) Where xChk=1 Into Cursor _TmpAzi_
      if RECCOUNT("_TmpAzi_")=0 and RECCOUNT(this.oParentObject.w_AZIENDE.cCursor)>0
        Ah_ErrorMsg("Non � stata selezionata nessuna azienda")
      else
        do while Not Eof( "_TmpAzi_" )
           
 Select _TmpAzi_ 
 =cp_ChangeAzi(_TmpAzi_.TACODAZI)
          GSAR_BAM(this,"modifica")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- --Ricalcola array valute
          this.b_VALUTE = CAVALUTE()
          this.w_RAGSOC = STRTRAN (alltrim(_TmpAzi_.TADESAZI) ,"'"," ")
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_MONOAZI = .F.
          if this.w_EXIT
            * --- Se ho un errore esco anche dal ciclo delle aziende...
            Exit
          endif
           
 Select _TmpAzi_ 
 Skip
        enddo
        * --- Rimuovo cursore..
         
 Select _TmpAzi_ 
 Use
        * --- Se l'utente non seleziona nessuna azienda procedo cmq con l'esecuzione
        *     dell'azienda corrente..
        =cp_ChangeAzi( this.w_CODAZI )
        GSAR_BAM(this,"modifica")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- --Ricalcola array valute
        this.b_VALUTE = CAVALUTE()
        this.w_RAGSOC = STRTRAN (g_RAGAZI,"'"," ")
        if this.w_MONOAZI
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    if ! empty(this.w_ERRORE) or ! empty(this.w_MSG)
      if ! empty(this.w_ERRORE)
        AddMsgNL(this.w_ERRORE,this, this.w_PARMSG1, this.w_PARMSG2, this.w_PARMSG3)
        this.w_ERRORE = ""
      else
        AddMsgNL("Notifica: %1",this,this.w_MSG )
        this.w_MSG = ""
      endif
    endif
    AddMsgNL("Fase 3: fine esecuzione del gruppo di regole %1",this,this.oParentObject.w_CODGRU )
    * --- Inserisco nell'anagrafica (GSIN_ALO) il Log
    this.w_ORAESECF = time()
    this.w_MEMO = this.oparentobject.w_MSG
    if "ORACLE"=upper(CP_DBTYPE) AND LEN(this.oparentobject.w_MSG) > 2000
      this.w_MEMO = RIGHT(ALLTRIM(this.oparentobject.w_MSG),2000)
    endif
    * --- Try
    local bErr_04C2ECA8
    bErr_04C2ECA8=bTrsErr
    this.Try_04C2ECA8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_04C04008
      bErr_04C04008=bTrsErr
      this.Try_04C04008()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04C04008
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_04C2ECA8
    * --- End
    * --- Ripristino blocco
    GSIN_BCB(this," ")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Azzera temporaneo
    if USED("ELABORA")
      SELECT ELABORA
      USE
    endif
    this.bUpdateParentObject = False 
 this.oParentObject.Refresh()
  endproc
  proc Try_04C2ECA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_LOG
    i_nConn=i_TableProp[this.INF_LOG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_LOG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_LOG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LODGRUPP"+",LODATAES"+",LOORAESE"+",LOCODUTE"+",LOLOGESE"+",LOORAFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODGRU),'INF_LOG','LODGRUPP');
      +","+cp_NullLink(cp_ToStrODBC(DATE()),'INF_LOG','LODATAES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORAESEC),'INF_LOG','LOORAESE');
      +","+cp_NullLink(cp_ToStrODBC(g_CODUTE),'INF_LOG','LOCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MEMO),'INF_LOG','LOLOGESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORAESECF),'INF_LOG','LOORAFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LODGRUPP',this.oParentObject.w_CODGRU,'LODATAES',DATE(),'LOORAESE',this.w_ORAESEC,'LOCODUTE',g_CODUTE,'LOLOGESE',this.w_MEMO,'LOORAFIN',this.w_ORAESECF)
      insert into (i_cTable) (LODGRUPP,LODATAES,LOORAESE,LOCODUTE,LOLOGESE,LOORAFIN &i_ccchkf. );
         values (;
           this.oParentObject.w_CODGRU;
           ,DATE();
           ,this.w_ORAESEC;
           ,g_CODUTE;
           ,this.w_MEMO;
           ,this.w_ORAESECF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04C04008()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_LOG
    i_nConn=i_TableProp[this.INF_LOG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_LOG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_LOG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LODGRUPP"+",LODATAES"+",LOORAESE"+",LOCODUTE"+",LOLOGESE"+",LOORAFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODGRU),'INF_LOG','LODGRUPP');
      +","+cp_NullLink(cp_ToStrODBC(DATE()),'INF_LOG','LODATAES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORAESEC),'INF_LOG','LOORAESE');
      +","+cp_NullLink(cp_ToStrODBC(g_CODUTE),'INF_LOG','LOCODUTE');
      +","+cp_NullLink(cp_ToStrODBC("Informazioni non disponibili"),'INF_LOG','LOLOGESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORAESECF),'INF_LOG','LOORAFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LODGRUPP',this.oParentObject.w_CODGRU,'LODATAES',DATE(),'LOORAESE',this.w_ORAESEC,'LOCODUTE',g_CODUTE,'LOLOGESE',"Informazioni non disponibili",'LOORAFIN',this.w_ORAESECF)
      insert into (i_cTable) (LODGRUPP,LODATAES,LOORAESE,LOCODUTE,LOLOGESE,LOORAFIN &i_ccchkf. );
         values (;
           this.oParentObject.w_CODGRU;
           ,DATE();
           ,this.w_ORAESEC;
           ,g_CODUTE;
           ,"Informazioni non disponibili";
           ,this.w_ORAESECF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli su parametri Routine
    if (empty(this.w_TABELLA) or (! FILE(this.w_QUERY) AND UPPER(this.w_RENOMCUR)="TMPINFO") or (IIF(UPPER(this.w_RENOMCUR)<>"TMPINFO", !used(ALLTRIM(this.w_RENOMCUR)),.f.))) and this.w_OPERAZ $ "CR-IN-DQ"
      this.w_CHKPARAM = .F.
      this.w_EMPTYPARAM = iif(empty(this.w_TABELLA) and ! FILE(this.w_QUERY), "%1Manca il nome della tabella %0%1Percorso o nome della query errato",iif(empty(this.w_TABELLA),"%1Manca il nome della tabella","%1Percorso o nome della query errato"))
      this.w_EMPTYPARAM = AH_MsgFormat(this.w_EMPTYPARAM, chr(149))
      AddMsgNL("Controllare la regola %1",this,this.w_RECODICE )
      ah_errormsg("Non sono presenti tutti i parametri richiesti per l'esecuzione della routine %1 %0Controllare la regola %2%0%3", 48, , this.w_REDESOPE, this.w_RECODICE, this.w_EMPTYPARAM)
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_RETIPSTE="BA"
        * --- Esegue batch di Elaborazione
        * --- Try
        local bErr_04C28618
        bErr_04C28618=bTrsErr
        this.Try_04C28618()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Salvo il messaggio nella Clipboard e avviso l'utente
          *     w_EXIT per uscire dalla Scan di Pag4
          _ClipText = i_ErrMsg
          this.w_PARMSG1 = alltrim(this.w_REDESOPE)
          this.w_PARMSG2 = Alltrim(i_CODAZI)
          this.w_PARMSG3 = alltrim(Message())
          this.w_ERRORE = "Errore durante l'esecuzione della routine %1 per azienda [%2]%0%3"
          ah_ERRORMSG( "Si � verificato un errore [dettagli nella ClipBoard CTRL-V] durante l'esecuzione della routine %1 per azienda [%2]%0Verificare le impostazioni delle regole e riprovare" ,48, , this.w_REDESOPE, Alltrim(i_CODAZI))
          this.w_EXIT = .T.
        endif
        bTrsErr=bTrsErr or bErr_04C28618
        * --- End
      case this.w_RETIPSTE="MA"
        * --- Esegue Maschera di Elaborazione
        FILTMP = this.w_REDESOPE
        do &FILTMP with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_RETIPSTE $ "QU" 
        if this.w_PARAMETR<>"S"
          * --- Esegue Query
          vq_exec(ALLTRIM(this.w_REDESOPE), this, ALLTRIM(this.w_RENOMCUR))
        else
          * --- Parametro
          if ! empty(this.w_RENOMCUR) and this.w_RENOMCUR<>"TMPINFO"
            this.w_QUERY = ""
          else
            this.w_QUERY = this.w_REDESOPE
          endif
        endif
      case this.w_RETIPSTE $ "TA"
        this.w_TABELLA = this.w_REDESOPE
        * --- Read from INF_TAB
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.INF_TAB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATMULTAZ"+;
            " from "+i_cTable+" INF_TAB where ";
                +"ATNTABLE = "+cp_ToStrODBC(this.w_TABELLA+"_DW");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATMULTAZ;
            from (i_cTable) where;
                ATNTABLE = this.w_TABELLA+"_DW";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MULTIAZI = NVL(cp_ToDate(_read_.ATMULTAZ),cp_NullValue(_read_.ATMULTAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
    endcase
  endproc
  proc Try_04C28618()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    FILTMP = this.w_REDESOPE
     
 L_errsav=on("ERROR") 
 L_ERR=.F. 
 On Error L_ERR = .T.
    if this.w_OPERAZ <>"AL"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_CHKPARAM 
        SELECT ELABORA
        &FILTMP(this,this.w_OPERAZ,this.w_TABELLA,this.w_QUERY)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Raise
        i_Error=AH_MsgFormat("Parametri mancanti")
        return
      endif
      SELECT ELABORA
    else
      do &FILTMP with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Ripristino la vecchia gestione errori
    on error &L_errsav
    if L_ERR
      * --- Raise
      i_Error=Message() + " - " + Message(1)
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cicla sul risultato delle Regole
    AddMsgNL(repl(chr(45),80) ,this)
    AddMsgNL("Fase 2: esecuzione regole contenute nel gruppo per azienda [%1]" ,this,Alltrim(i_CODAZI))
    this.w_EXIT = .F.
    this.w_CHKPARAM = .T.
    this.w_REGOLATT = ""
    this.w_ERRORE = ""
    this.w_MSG = ""
    this.w_TABELLA = ""
    this.w_QUERY = ""
    SELECT ELABORA
    GO TOP
    SCAN FOR (NOT EMPTY(NVL(REDESOPE," ")) OR REPARAME="S") AND NOT EMPTY(NVL(RETIPSTE," "))
    * --- Per ciascuna Riga Legge: Tipo, Operazione, Cursore
    this.w_OPERAZ = RE__TIPO
    this.w_RECODICE = RECODICE
    this.w_REDESCRI = NVL(REDESCRI," ")
    this.w_RETIPSTE = RETIPSTE
    this.w_REDESOPE = ALLTRIM(REDESOPE)
    this.w_PARAMETR = REPARAME
    if this.w_RETIPSTE = "QU"
      this.w_RENOMCUR = IIF(EMPTY(NVL(RENOMCUR," ")),"TMPINFO", RENOMCUR)
    endif
    if ! empty(this.w_ERRORE) or ! empty(this.w_MSG)
      if ! empty(this.w_ERRORE)
        AddMsgNL(this.w_ERRORE,this, this.w_PARMSG1, this.w_PARMSG2, this.w_PARMSG3)
        this.w_ERRORE = ""
      else
        AddMsgNL("Notifica: %1",this,this.w_MSG )
        this.w_MSG = ""
      endif
    endif
    if this.w_REGOLATT <> RECODICE
      AddMsgNL(repl(chr(45),80) ,this)
      this.w_REGOLATT = this.w_RECODICE
      AddMsgNL("Esecuzione regola: %1",this,this.w_RECODICE)
    endif
    do case
      case this.w_RETIPSTE="BA"
        AddMsgNL("%1Tipo passo: batch %2 descrizione operazione: %3",this, STR(CPROWORD)+space(2), space(9), this.w_REDESOPE)
      case this.w_RETIPSTE="MA"
        AddMsgNL("%1Tipo passo: maschera %2 descrizione operazione: %3",this, STR(CPROWORD)+space(2), space(9),this.w_REDESOPE)
      case this.w_RETIPSTE="QU"
        AddMsgNL("%1Tipo passo: query %2 descrizione operazione: %3",this, STR(CPROWORD)+space(2), space(9),this.w_REDESOPE)
      otherwise
        AddMsgNL("%1Tipo passo: tabella %2 descrizione operazione: %3", this, STR(CPROWORD)+space(2) ,space(9),this.w_REDESOPE)
    endcase
    * --- Se prima azienda eseguo cmq tutte le regole, per le eventuali
    *     successivie svolgo solo le regole moulti azienda
    *     (Inserimento / Cancellazione Filtro Data / Cancellazione Da Query / Cancellazione Dati Tabella )
    if this.w_MONOAZI
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Prima di escludere un passo devo aver valorizzato la tabella e quindi
      *     sapere se � o meno multi azienda...
      if this.w_OPERAZ $ "AL-IN-DA-DQ-DT" And ( this.w_MULTIAZI="S" Or Empty( this.w_TABELLA ) Or this.w_RETIPSTE $ "TA")
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_EXIT
      exit
    endif
    SELECT ELABORA
    ENDSCAN
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='INF_LOG'
    this.cWorkTables[2]='INF_TAB'
    this.cWorkTables[3]='INFOCNBL'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- gsin_bim
  Procedure CancLog(parent)
    parent.oparentobject.w_MSG=''
  Endproc  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
