* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_atc                                                        *
*              Archivio tabelle connessioni                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-22                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsin_atc"))

* --- Class definition
define class tgsin_atc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 632
  Height = 298+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=92909417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  INF_TAB_IDX = 0
  cFile = "INF_TAB"
  cKeySelect = "ATNTABLE"
  cKeyWhere  = "ATNTABLE=this.w_ATNTABLE"
  cKeyWhereODBC = '"ATNTABLE="+cp_ToStrODBC(this.w_ATNTABLE)';

  cKeyWhereODBCqualified = '"INF_TAB.ATNTABLE="+cp_ToStrODBC(this.w_ATNTABLE)';

  cPrg = "gsin_atc"
  cComment = "Archivio tabelle connessioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ATNTABLE = space(50)
  w_ATMULTAZ = space(1)
  o_ATMULTAZ = space(1)
  w_ATCAMCON = space(150)
  w_ATDAFILT = space(25)
  w_ATCRIPTE = space(1)
  w_ATCOODBC = space(25)
  w_ATCOSTRI = space(90)
  o_ATCOSTRI = space(90)

  * --- Children pointers
  GSIN_MIT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INF_TAB','gsin_atc')
    stdPageFrame::Init()
    *set procedure to GSIN_MIT additive
    with this
      .Pages(1).addobject("oPag","tgsin_atcPag1","gsin_atc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Archivio tabelle connessioni")
      .Pages(1).HelpContextID = 151836451
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATNTABLE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSIN_MIT
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='INF_TAB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INF_TAB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INF_TAB_IDX,3]
  return

  function CreateChildren()
    this.GSIN_MIT = CREATEOBJECT('stdDynamicChild',this,'GSIN_MIT',this.oPgFrm.Page1.oPag.oLinkPC_1_12)
    this.GSIN_MIT.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSIN_MIT)
      this.GSIN_MIT.DestroyChildrenChain()
      this.GSIN_MIT=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_12')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSIN_MIT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSIN_MIT.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSIN_MIT.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSIN_MIT.SetKey(;
            .w_ATNTABLE,"GITABNAM";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSIN_MIT.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATNTABLE,"GITABNAM";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSIN_MIT)
        i_f=.GSIN_MIT.BuildFilter()
        if !(i_f==.GSIN_MIT.cQueryFilter)
          i_fnidx=.GSIN_MIT.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSIN_MIT.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSIN_MIT.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSIN_MIT.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSIN_MIT.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ATNTABLE = NVL(ATNTABLE,space(50))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from INF_TAB where ATNTABLE=KeySet.ATNTABLE
    *
    i_nConn = i_TableProp[this.INF_TAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INF_TAB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INF_TAB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INF_TAB '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ATNTABLE',this.w_ATNTABLE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ATNTABLE = NVL(ATNTABLE,space(50))
        .w_ATMULTAZ = NVL(ATMULTAZ,space(1))
        .w_ATCAMCON = NVL(ATCAMCON,space(150))
        .w_ATDAFILT = NVL(ATDAFILT,space(25))
        .w_ATCRIPTE = NVL(ATCRIPTE,space(1))
        .w_ATCOODBC = NVL(ATCOODBC,space(25))
        .w_ATCOSTRI = NVL(ATCOSTRI,space(90))
        cp_LoadRecExtFlds(this,'INF_TAB')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATNTABLE = space(50)
      .w_ATMULTAZ = space(1)
      .w_ATCAMCON = space(150)
      .w_ATDAFILT = space(25)
      .w_ATCRIPTE = space(1)
      .w_ATCOODBC = space(25)
      .w_ATCOSTRI = space(90)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_ATMULTAZ = 'N'
        .w_ATCAMCON = iif(.w_ATMULTAZ='S',.w_ATCAMCON,'')
          .DoRTCalc(4,4,.f.)
        .w_ATCRIPTE = 'N'
        .w_ATCOODBC = space(25)
      endif
    endwith
    cp_BlankRecExtFlds(this,'INF_TAB')
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oATNTABLE_1_1.enabled = i_bVal
      .Page1.oPag.oATMULTAZ_1_2.enabled = i_bVal
      .Page1.oPag.oATCAMCON_1_3.enabled = i_bVal
      .Page1.oPag.oATDAFILT_1_4.enabled = i_bVal
      .Page1.oPag.oATCOODBC_1_7.enabled = i_bVal
      .Page1.oPag.oATCOSTRI_1_11.enabled = i_bVal
      .Page1.oPag.oBtn_1_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oATNTABLE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oATNTABLE_1_1.enabled = .t.
      endif
    endwith
    this.GSIN_MIT.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'INF_TAB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSIN_MIT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INF_TAB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNTABLE,"ATNTABLE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATMULTAZ,"ATMULTAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCAMCON,"ATCAMCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDAFILT,"ATDAFILT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCRIPTE,"ATCRIPTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCOODBC,"ATCOODBC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCOSTRI,"ATCOSTRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INF_TAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])
    i_lTable = "INF_TAB"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INF_TAB_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSIN_STC with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gsin_atc
    this.w_ATNTABLE = alltrim(strtran(this.w_ATNTABLE,"_DW",""))+"_DW"
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_TAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.INF_TAB_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INF_TAB
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INF_TAB')
        i_extval=cp_InsertValODBCExtFlds(this,'INF_TAB')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ATNTABLE,ATMULTAZ,ATCAMCON,ATDAFILT,ATCRIPTE"+;
                  ",ATCOODBC,ATCOSTRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ATNTABLE)+;
                  ","+cp_ToStrODBC(this.w_ATMULTAZ)+;
                  ","+cp_ToStrODBC(this.w_ATCAMCON)+;
                  ","+cp_ToStrODBC(this.w_ATDAFILT)+;
                  ","+cp_ToStrODBC(this.w_ATCRIPTE)+;
                  ","+cp_ToStrODBC(this.w_ATCOODBC)+;
                  ","+cp_ToStrODBC(this.w_ATCOSTRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INF_TAB')
        i_extval=cp_InsertValVFPExtFlds(this,'INF_TAB')
        cp_CheckDeletedKey(i_cTable,0,'ATNTABLE',this.w_ATNTABLE)
        INSERT INTO (i_cTable);
              (ATNTABLE,ATMULTAZ,ATCAMCON,ATDAFILT,ATCRIPTE,ATCOODBC,ATCOSTRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ATNTABLE;
                  ,this.w_ATMULTAZ;
                  ,this.w_ATCAMCON;
                  ,this.w_ATDAFILT;
                  ,this.w_ATCRIPTE;
                  ,this.w_ATCOODBC;
                  ,this.w_ATCOSTRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.INF_TAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.INF_TAB_IDX,i_nConn)
      *
      * update INF_TAB
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'INF_TAB')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ATMULTAZ="+cp_ToStrODBC(this.w_ATMULTAZ)+;
             ",ATCAMCON="+cp_ToStrODBC(this.w_ATCAMCON)+;
             ",ATDAFILT="+cp_ToStrODBC(this.w_ATDAFILT)+;
             ",ATCRIPTE="+cp_ToStrODBC(this.w_ATCRIPTE)+;
             ",ATCOODBC="+cp_ToStrODBC(this.w_ATCOODBC)+;
             ",ATCOSTRI="+cp_ToStrODBC(this.w_ATCOSTRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'INF_TAB')
        i_cWhere = cp_PKFox(i_cTable  ,'ATNTABLE',this.w_ATNTABLE  )
        UPDATE (i_cTable) SET;
              ATMULTAZ=this.w_ATMULTAZ;
             ,ATCAMCON=this.w_ATCAMCON;
             ,ATDAFILT=this.w_ATDAFILT;
             ,ATCRIPTE=this.w_ATCRIPTE;
             ,ATCOODBC=this.w_ATCOODBC;
             ,ATCOSTRI=this.w_ATCOSTRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSIN_MIT : Saving
      this.GSIN_MIT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATNTABLE,"GITABNAM";
             )
      this.GSIN_MIT.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSIN_MIT : Deleting
    this.GSIN_MIT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATNTABLE,"GITABNAM";
           )
    this.GSIN_MIT.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INF_TAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.INF_TAB_IDX,i_nConn)
      *
      * delete INF_TAB
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ATNTABLE',this.w_ATNTABLE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INF_TAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_TAB_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_ATMULTAZ<>.w_ATMULTAZ
            .w_ATCAMCON = iif(.w_ATMULTAZ='S',.w_ATCAMCON,'')
        endif
        .DoRTCalc(4,5,.t.)
        if .o_ATCOSTRI<>.w_ATCOSTRI
            .w_ATCOODBC = space(25)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATCAMCON_1_3.enabled = this.oPgFrm.Page1.oPag.oATCAMCON_1_3.mCond()
    this.oPgFrm.Page1.oPag.oATCOODBC_1_7.enabled = this.oPgFrm.Page1.oPag.oATCOODBC_1_7.mCond()
    this.oPgFrm.Page1.oPag.oATCOSTRI_1_11.enabled = this.oPgFrm.Page1.oPag.oATCOSTRI_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATNTABLE_1_1.value==this.w_ATNTABLE)
      this.oPgFrm.Page1.oPag.oATNTABLE_1_1.value=this.w_ATNTABLE
    endif
    if not(this.oPgFrm.Page1.oPag.oATMULTAZ_1_2.RadioValue()==this.w_ATMULTAZ)
      this.oPgFrm.Page1.oPag.oATMULTAZ_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAMCON_1_3.value==this.w_ATCAMCON)
      this.oPgFrm.Page1.oPag.oATCAMCON_1_3.value=this.w_ATCAMCON
    endif
    if not(this.oPgFrm.Page1.oPag.oATDAFILT_1_4.value==this.w_ATDAFILT)
      this.oPgFrm.Page1.oPag.oATDAFILT_1_4.value=this.w_ATDAFILT
    endif
    if not(this.oPgFrm.Page1.oPag.oATCRIPTE_1_5.RadioValue()==this.w_ATCRIPTE)
      this.oPgFrm.Page1.oPag.oATCRIPTE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCOODBC_1_7.value==this.w_ATCOODBC)
      this.oPgFrm.Page1.oPag.oATCOODBC_1_7.value=this.w_ATCOODBC
    endif
    if not(this.oPgFrm.Page1.oPag.oATCOSTRI_1_11.value==this.w_ATCOSTRI)
      this.oPgFrm.Page1.oPag.oATCOSTRI_1_11.value=this.w_ATCOSTRI
    endif
    cp_SetControlsValueExtFlds(this,'INF_TAB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((len(alltrim(.w_ATNTABLE))< 48 or right(alltrim(.w_ATNTABLE),3)='_DW'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATNTABLE_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il nome della tabella non pu� superare i 47 caratteri")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSIN_MIT.CheckForm()
      if i_bres
        i_bres=  .GSIN_MIT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATMULTAZ = this.w_ATMULTAZ
    this.o_ATCOSTRI = this.w_ATCOSTRI
    * --- GSIN_MIT : Depends On
    this.GSIN_MIT.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsin_atcPag1 as StdContainer
  Width  = 628
  height = 303
  stdWidth  = 628
  stdheight = 303
  resizeXpos=346
  resizeYpos=254
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATNTABLE_1_1 as StdField with uid="OIFVKIUMTY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ATNTABLE", cQueryName = "ATNTABLE",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "Il nome della tabella non pu� superare i 47 caratteri",;
    ToolTipText = "Nome della tabella",;
    HelpContextID = 253785781,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=115, Top=8, cSayPict='"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"', cGetPict='"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"', InputMask=replicate('X',50)

  func oATNTABLE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((len(alltrim(.w_ATNTABLE))< 48 or right(alltrim(.w_ATNTABLE),3)='_DW'))
    endwith
    return bRes
  endfunc

  add object oATMULTAZ_1_2 as StdCheck with uid="DEOVIBOFEO",rtseq=2,rtrep=.f.,left=115, top=33, caption="Multi aziendale",;
    ToolTipText = "Check tabella multi aziendale",;
    HelpContextID = 59799904,;
    cFormVar="w_ATMULTAZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATMULTAZ_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oATMULTAZ_1_2.GetRadio()
    this.Parent.oContained.w_ATMULTAZ = this.RadioValue()
    return .t.
  endfunc

  func oATMULTAZ_1_2.SetRadio()
    this.Parent.oContained.w_ATMULTAZ=trim(this.Parent.oContained.w_ATMULTAZ)
    this.value = ;
      iif(this.Parent.oContained.w_ATMULTAZ=='S',1,;
      0)
  endfunc

  add object oATCAMCON_1_3 as StdField with uid="BHDLHBQZWJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ATCAMCON", cQueryName = "ATCAMCON",;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Indicare i nomi dei campi da concatenare con l'azienda separati da virgole ', '",;
    HelpContextID = 225715884,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=428, Top=33, InputMask=replicate('X',150)

  func oATCAMCON_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATMULTAZ='S')
    endwith
   endif
  endfunc

  add object oATDAFILT_1_4 as StdField with uid="ZTENRFWBHS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ATDAFILT", cQueryName = "ATDAFILT",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Campo usato per filtrare su intervallo di date",;
    HelpContextID = 132388518,;
   bGlobalFont=.t.,;
    Height=21, Width=183, Left=115, Top=58, InputMask=replicate('X',25)


  add object oATCRIPTE_1_5 as StdCombo with uid="UCAPDINIPS",rtseq=5,rtrep=.f.,left=115,top=86,width=124,height=21, enabled=.f.;
    , ToolTipText = "Tipo di protezione";
    , HelpContextID = 257743179;
    , cFormVar="w_ATCRIPTE",RowSource=""+"Stringa cifrata,"+"Nessuna cifratura", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATCRIPTE_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oATCRIPTE_1_5.GetRadio()
    this.Parent.oContained.w_ATCRIPTE = this.RadioValue()
    return .t.
  endfunc

  func oATCRIPTE_1_5.SetRadio()
    this.Parent.oContained.w_ATCRIPTE=trim(this.Parent.oContained.w_ATCRIPTE)
    this.value = ;
      iif(this.Parent.oContained.w_ATCRIPTE=='C',1,;
      iif(this.Parent.oContained.w_ATCRIPTE=='N',2,;
      0))
  endfunc

  add object oATCOODBC_1_7 as StdField with uid="RMJNGOOKQO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ATCOODBC", cQueryName = "ATCOODBC",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome della connessione ODBC",;
    HelpContextID = 62511433,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=428, Top=58, InputMask=replicate('X',25)

  func oATCOODBC_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_ATCOSTRI) AND .w_ATCRIPTE<>'C')
    endwith
   endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="BLYJZKVELC",left=577, top=83, width=48,height=45,;
    CpPicture="bmp\CIFRA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per cifrare la stringa di connessione";
    , HelpContextID = 92909322;
    , Caption='\<Cifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      do GSIN_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ATCRIPTE='C' or g_APPLICATION = "ADHOC REVOLUTION")
     endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="AINRTRMIGZ",left=577, top=83, width=48,height=45,;
    CpPicture="bmp\DECIFRA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per decifrare la stringa di connessione";
    , HelpContextID = 54913590;
    , Caption='\<Decifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      do GSIN_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ATCRIPTE<>'C')
     endwith
    endif
  endfunc

  add object oATCOSTRI_1_11 as StdField with uid="WDLYNXHBWA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ATCOSTRI", cQueryName = "ATCOSTRI",;
    bObbl = .f. , nPag = 1, value=space(90), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione",;
    HelpContextID = 66705743,;
   bGlobalFont=.t.,;
    Height=21, Width=619, Left=5, Top=128, InputMask=replicate('X',90)

  func oATCOSTRI_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_ATCOODBC) AND .w_ATCRIPTE<>'C')
    endwith
   endif
  endfunc


  add object oLinkPC_1_12 as stdDynamicChildContainer with uid="MNEZYQPWZD",left=2, top=172, width=623, height=131, bOnScreen=.t.;


  add object oStr_1_6 as StdString with uid="QAWEBDGHMM",Visible=.t., Left=2, Top=8,;
    Alignment=1, Width=110, Height=18,;
    Caption="Nome tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="DDPAIACCXG",Visible=.t., Left=358, Top=58,;
    Alignment=1, Width=68, Height=18,;
    Caption="ODBC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="JQWXTFDHCL",Visible=.t., Left=6, Top=151,;
    Alignment=0, Width=185, Height=17,;
    Caption="Gestione indici"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="KIGDBVVPMD",Visible=.t., Left=2, Top=86,;
    Alignment=1, Width=110, Height=18,;
    Caption="Tipo di protezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="IECYLKSJMC",Visible=.t., Left=6, Top=112,;
    Alignment=0, Width=218, Height=17,;
    Caption="Stringa di connessione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="PKILAWNJHN",Visible=.t., Left=2, Top=58,;
    Alignment=1, Width=110, Height=18,;
    Caption="Campo filtro date:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="LRRSUHCJIX",Visible=.t., Left=236, Top=36,;
    Alignment=1, Width=190, Height=18,;
    Caption="Campi decod. multi azienda:"  ;
  , bGlobalFont=.t.

  add object oBox_1_13 as StdBox with uid="RFYPKMCRUC",left=1, top=166, width=627,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_atc','INF_TAB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ATNTABLE=INF_TAB.ATNTABLE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
