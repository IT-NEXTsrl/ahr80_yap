* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_mre                                                        *
*              Regole di elaborazione                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-15                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsin_mre"))

* --- Class definition
define class tgsin_mre as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 658
  Height = 365+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=113880937
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  INF_REGM_IDX = 0
  INF_REGD_IDX = 0
  cFile = "INF_REGM"
  cFileDetail = "INF_REGD"
  cKeySelect = "RECODICE"
  cKeyWhere  = "RECODICE=this.w_RECODICE"
  cKeyDetail  = "RECODICE=this.w_RECODICE"
  cKeyWhereODBC = '"RECODICE="+cp_ToStrODBC(this.w_RECODICE)';

  cKeyDetailWhereODBC = '"RECODICE="+cp_ToStrODBC(this.w_RECODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"INF_REGD.RECODICE="+cp_ToStrODBC(this.w_RECODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INF_REGD.CPROWORD '
  cPrg = "gsin_mre"
  cComment = "Regole di elaborazione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RECODICE = space(15)
  w_REDESCRI = space(45)
  w_REDESSUP = space(0)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_RE__TIPO = space(2)
  o_RE__TIPO = space(2)
  w_CPROWORD = 0
  w_RETIPSTE = space(2)
  o_RETIPSTE = space(2)
  w_BATCH = space(10)
  o_BATCH = space(10)
  w_REDESOPE = space(100)
  w_REPARAME = space(1)
  o_REPARAME = space(1)
  w_RENOMCUR = space(8)
  w_REDTINVA = ctod('  /  /  ')
  w_REDTOBSO = ctod('  /  /  ')
  w_NFILE = space(50)
  o_NFILE = space(50)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INF_REGM','gsin_mre')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsin_mrePag1","gsin_mre",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Regole di elaborazione")
      .Pages(1).HelpContextID = 96161140
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRECODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='INF_REGM'
    this.cWorkTables[2]='INF_REGD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INF_REGM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INF_REGM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_RECODICE = NVL(RECODICE,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from INF_REGM where RECODICE=KeySet.RECODICE
    *
    i_nConn = i_TableProp[this.INF_REGM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_REGM_IDX,2],this.bLoadRecFilter,this.INF_REGM_IDX,"gsin_mre")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INF_REGM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INF_REGM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"INF_REGD.","INF_REGM.")
      i_cTable = i_cTable+' INF_REGM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RECODICE',this.w_RECODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_RECODICE = NVL(RECODICE,space(15))
        .w_REDESCRI = NVL(REDESCRI,space(45))
        .w_REDESSUP = NVL(REDESSUP,space(0))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_RE__TIPO = NVL(RE__TIPO,space(2))
        .w_REDTINVA = NVL(cp_ToDate(REDTINVA),ctod("  /  /  "))
        .w_REDTOBSO = NVL(cp_ToDate(REDTOBSO),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'INF_REGM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from INF_REGD where RECODICE=KeySet.RECODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.INF_REGD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_REGD_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('INF_REGD')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "INF_REGD.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" INF_REGD"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'RECODICE',this.w_RECODICE  )
        select * from (i_cTable) INF_REGD where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_NFILE = space(50)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_RETIPSTE = NVL(RETIPSTE,space(2))
        .w_BATCH = iif(.w_RETIPSTE='BA',iif(.w_RE__TIPO $ 'DADQDDDT','GSIN_BDR',iif(.w_RE__TIPO $ 'CRIN','GSIN_BCR','')),'')
          .w_REDESOPE = NVL(REDESOPE,space(100))
          .w_REPARAME = NVL(REPARAME,space(1))
          .w_RENOMCUR = NVL(RENOMCUR,space(8))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RECODICE=space(15)
      .w_REDESCRI=space(45)
      .w_REDESSUP=space(0)
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_RE__TIPO=space(2)
      .w_CPROWORD=10
      .w_RETIPSTE=space(2)
      .w_BATCH=space(10)
      .w_REDESOPE=space(100)
      .w_REPARAME=space(1)
      .w_RENOMCUR=space(8)
      .w_REDTINVA=ctod("  /  /  ")
      .w_REDTOBSO=ctod("  /  /  ")
      .w_NFILE=space(50)
      if .cFunction<>"Filter"
        .DoRTCalc(1,7,.f.)
        .w_RE__TIPO = 'CR'
        .DoRTCalc(9,9,.f.)
        .w_RETIPSTE = 'TA'
        .w_BATCH = iif(.w_RETIPSTE='BA',iif(.w_RE__TIPO $ 'DADQDDDT','GSIN_BDR',iif(.w_RE__TIPO $ 'CRIN','GSIN_BCR','')),'')
        .w_REDESOPE = IIF(.w_RETIPSTE<>'TA',iif(.w_RETIPSTE='BA' and .w_RE__TIPO $ 'CRINDADQDDDT',.w_BATCH, SYS(2014, .w_NFILE)),strtran(.w_NFILE,'_DW',''))
        .w_REPARAME = 'N'
        .w_RENOMCUR = SPACE(8)
        .w_REDTINVA = iif(.cFunction='Load',i_datsys,.w_REDTINVA)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INF_REGM')
    this.DoRTCalc(16,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRECODICE_1_1.enabled = i_bVal
      .Page1.oPag.oREDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oREDESSUP_1_4.enabled = i_bVal
      .Page1.oPag.oRE__TIPO_1_9.enabled = i_bVal
      .Page1.oPag.oREDTINVA_3_1.enabled = i_bVal
      .Page1.oPag.oREDTOBSO_3_3.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRECODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRECODICE_1_1.enabled = .t.
        .Page1.oPag.oREDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'INF_REGM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INF_REGM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RECODICE,"RECODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REDESCRI,"REDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REDESSUP,"REDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RE__TIPO,"RE__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REDTINVA,"REDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REDTOBSO,"REDTOBSO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INF_REGM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_REGM_IDX,2])
    i_lTable = "INF_REGM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INF_REGM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSIN_SRE with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_RETIPSTE N(3);
      ,t_REDESOPE C(100);
      ,t_REPARAME N(3);
      ,t_RENOMCUR C(8);
      ,CPROWNUM N(10);
      ,t_BATCH C(10);
      ,t_NFILE C(50);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsin_mrebodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRETIPSTE_2_2.controlsource=this.cTrsName+'.t_RETIPSTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREDESOPE_2_4.controlsource=this.cTrsName+'.t_REDESOPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREPARAME_2_5.controlsource=this.cTrsName+'.t_REPARAME'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRENOMCUR_2_6.controlsource=this.cTrsName+'.t_RENOMCUR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(52)
    this.AddVLine(139)
    this.AddVLine(498)
    this.AddVLine(543)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_REGM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_REGM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INF_REGM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INF_REGM')
        i_extval=cp_InsertValODBCExtFlds(this,'INF_REGM')
        local i_cFld
        i_cFld=" "+;
                  "(RECODICE,REDESCRI,REDESSUP,UTCC,UTCV"+;
                  ",UTDC,UTDV,RE__TIPO,REDTINVA,REDTOBSO"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_RECODICE)+;
                    ","+cp_ToStrODBC(this.w_REDESCRI)+;
                    ","+cp_ToStrODBC(this.w_REDESSUP)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_RE__TIPO)+;
                    ","+cp_ToStrODBC(this.w_REDTINVA)+;
                    ","+cp_ToStrODBC(this.w_REDTOBSO)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INF_REGM')
        i_extval=cp_InsertValVFPExtFlds(this,'INF_REGM')
        cp_CheckDeletedKey(i_cTable,0,'RECODICE',this.w_RECODICE)
        INSERT INTO (i_cTable);
              (RECODICE,REDESCRI,REDESSUP,UTCC,UTCV,UTDC,UTDV,RE__TIPO,REDTINVA,REDTOBSO &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_RECODICE;
                  ,this.w_REDESCRI;
                  ,this.w_REDESSUP;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_RE__TIPO;
                  ,this.w_REDTINVA;
                  ,this.w_REDTOBSO;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_REGD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_REGD_IDX,2])
      *
      * insert into INF_REGD
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(RECODICE,CPROWORD,RETIPSTE,REDESOPE,REPARAME"+;
                  ",RENOMCUR,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RECODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_RETIPSTE)+","+cp_ToStrODBC(this.w_REDESOPE)+","+cp_ToStrODBC(this.w_REPARAME)+;
             ","+cp_ToStrODBC(this.w_RENOMCUR)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RECODICE',this.w_RECODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_RECODICE,this.w_CPROWORD,this.w_RETIPSTE,this.w_REDESOPE,this.w_REPARAME"+;
                ",this.w_RENOMCUR,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.INF_REGM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_REGM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update INF_REGM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'INF_REGM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " REDESCRI="+cp_ToStrODBC(this.w_REDESCRI)+;
             ",REDESSUP="+cp_ToStrODBC(this.w_REDESSUP)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",RE__TIPO="+cp_ToStrODBC(this.w_RE__TIPO)+;
             ",REDTINVA="+cp_ToStrODBC(this.w_REDTINVA)+;
             ",REDTOBSO="+cp_ToStrODBC(this.w_REDTOBSO)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'INF_REGM')
          i_cWhere = cp_PKFox(i_cTable  ,'RECODICE',this.w_RECODICE  )
          UPDATE (i_cTable) SET;
              REDESCRI=this.w_REDESCRI;
             ,REDESSUP=this.w_REDESSUP;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,RE__TIPO=this.w_RE__TIPO;
             ,REDTINVA=this.w_REDTINVA;
             ,REDTOBSO=this.w_REDTOBSO;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND (NOT EMPTY(t_REDESOPE) OR NOT EMPTY(t_RENOMCUR))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.INF_REGD_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.INF_REGD_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from INF_REGD
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INF_REGD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",RETIPSTE="+cp_ToStrODBC(this.w_RETIPSTE)+;
                     ",REDESOPE="+cp_ToStrODBC(this.w_REDESOPE)+;
                     ",REPARAME="+cp_ToStrODBC(this.w_REPARAME)+;
                     ",RENOMCUR="+cp_ToStrODBC(this.w_RENOMCUR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,RETIPSTE=this.w_RETIPSTE;
                     ,REDESOPE=this.w_REDESOPE;
                     ,REPARAME=this.w_REPARAME;
                     ,RENOMCUR=this.w_RENOMCUR;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND (NOT EMPTY(t_REDESOPE) OR NOT EMPTY(t_RENOMCUR))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.INF_REGD_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.INF_REGD_IDX,2])
        *
        * delete INF_REGD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.INF_REGM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.INF_REGM_IDX,2])
        *
        * delete INF_REGM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND (NOT EMPTY(t_REDESOPE) OR NOT EMPTY(t_RENOMCUR))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INF_REGM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_REGM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
        if .o_RETIPSTE<>.w_RETIPSTE.or. .o_RE__TIPO<>.w_RE__TIPO
          .w_BATCH = iif(.w_RETIPSTE='BA',iif(.w_RE__TIPO $ 'DADQDDDT','GSIN_BDR',iif(.w_RE__TIPO $ 'CRIN','GSIN_BCR','')),'')
        endif
        if .o_NFILE<>.w_NFILE.or. .o_BATCH<>.w_BATCH
          .w_REDESOPE = IIF(.w_RETIPSTE<>'TA',iif(.w_RETIPSTE='BA' and .w_RE__TIPO $ 'CRINDADQDDDT',.w_BATCH, SYS(2014, .w_NFILE)),strtran(.w_NFILE,'_DW',''))
        endif
        .DoRTCalc(13,13,.t.)
        if .o_REPARAME<>.w_REPARAME
          .w_RENOMCUR = SPACE(8)
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_BATCH with this.w_BATCH
      replace t_NFILE with this.w_NFILE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREDESOPE_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREDESOPE_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRENOMCUR_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oRENOMCUR_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oBtn_2_9.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oBtn_2_9.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRECODICE_1_1.value==this.w_RECODICE)
      this.oPgFrm.Page1.oPag.oRECODICE_1_1.value=this.w_RECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESCRI_1_3.value==this.w_REDESCRI)
      this.oPgFrm.Page1.oPag.oREDESCRI_1_3.value=this.w_REDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESSUP_1_4.value==this.w_REDESSUP)
      this.oPgFrm.Page1.oPag.oREDESSUP_1_4.value=this.w_REDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oRE__TIPO_1_9.RadioValue()==this.w_RE__TIPO)
      this.oPgFrm.Page1.oPag.oRE__TIPO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREDTINVA_3_1.value==this.w_REDTINVA)
      this.oPgFrm.Page1.oPag.oREDTINVA_3_1.value=this.w_REDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oREDTOBSO_3_3.value==this.w_REDTOBSO)
      this.oPgFrm.Page1.oPag.oREDTOBSO_3_3.value=this.w_REDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRETIPSTE_2_2.RadioValue()==this.w_RETIPSTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRETIPSTE_2_2.SetRadio()
      replace t_RETIPSTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRETIPSTE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREDESOPE_2_4.value==this.w_REDESOPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREDESOPE_2_4.value=this.w_REDESOPE
      replace t_REDESOPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREDESOPE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREPARAME_2_5.RadioValue()==this.w_REPARAME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREPARAME_2_5.SetRadio()
      replace t_REPARAME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREPARAME_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRENOMCUR_2_6.value==this.w_RENOMCUR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRENOMCUR_2_6.value=this.w_RENOMCUR
      replace t_RENOMCUR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRENOMCUR_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'INF_REGM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RECODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oRECODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_RECODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RE__TIPO) or not(.w_RE__TIPO $ 'ALCRDAINDDDQDT'))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oRE__TIPO_1_9.SetFocus()
            i_bnoObbl = !empty(.w_RE__TIPO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo regola inconsistente")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_REDESOPE) OR iif(.w_RETIPSTE<>'TA',CHKREGOL(.w_RETIPSTE, .w_REDESOPE, .w_CPROWORD),.T.)) and (empty(.w_RENOMCUR)) and (.w_CPROWORD<>0 AND (NOT EMPTY(.w_REDESOPE) OR NOT EMPTY(.w_RENOMCUR)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREDESOPE_2_4
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(AT(' ', ALLTRIM(.w_RENOMCUR))=0) and (.w_RETIPSTE $ 'QU' AND ((NOT EMPTY(.w_REDESOPE) AND .w_REPARAME<>'S') or (EMPTY(.w_REDESOPE) AND .w_REPARAME='S' ))) and (.w_CPROWORD<>0 AND (NOT EMPTY(.w_REDESOPE) OR NOT EMPTY(.w_RENOMCUR)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRENOMCUR_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Nome del cursore non corretto")
      endcase
      if .w_CPROWORD<>0 AND (NOT EMPTY(.w_REDESOPE) OR NOT EMPTY(.w_RENOMCUR))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RE__TIPO = this.w_RE__TIPO
    this.o_RETIPSTE = this.w_RETIPSTE
    this.o_BATCH = this.w_BATCH
    this.o_REPARAME = this.w_REPARAME
    this.o_NFILE = this.w_NFILE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND (NOT EMPTY(t_REDESOPE) OR NOT EMPTY(t_RENOMCUR)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_RETIPSTE=space(2)
      .w_BATCH=space(10)
      .w_REDESOPE=space(100)
      .w_REPARAME=space(1)
      .w_RENOMCUR=space(8)
      .w_NFILE=space(50)
      .DoRTCalc(1,9,.f.)
        .w_RETIPSTE = 'TA'
        .w_BATCH = iif(.w_RETIPSTE='BA',iif(.w_RE__TIPO $ 'DADQDDDT','GSIN_BDR',iif(.w_RE__TIPO $ 'CRIN','GSIN_BCR','')),'')
        .w_REDESOPE = IIF(.w_RETIPSTE<>'TA',iif(.w_RETIPSTE='BA' and .w_RE__TIPO $ 'CRINDADQDDDT',.w_BATCH, SYS(2014, .w_NFILE)),strtran(.w_NFILE,'_DW',''))
        .w_REPARAME = 'N'
        .w_RENOMCUR = SPACE(8)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_8.Calculate()
    endwith
    this.DoRTCalc(15,17,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_RETIPSTE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRETIPSTE_2_2.RadioValue(.t.)
    this.w_BATCH = t_BATCH
    this.w_REDESOPE = t_REDESOPE
    this.w_REPARAME = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREPARAME_2_5.RadioValue(.t.)
    this.w_RENOMCUR = t_RENOMCUR
    this.w_NFILE = t_NFILE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_RETIPSTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRETIPSTE_2_2.ToRadio()
    replace t_BATCH with this.w_BATCH
    replace t_REDESOPE with this.w_REDESOPE
    replace t_REPARAME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREPARAME_2_5.ToRadio()
    replace t_RENOMCUR with this.w_RENOMCUR
    replace t_NFILE with this.w_NFILE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsin_mrePag1 as StdContainer
  Width  = 654
  height = 365
  stdWidth  = 654
  stdheight = 365
  resizeXpos=384
  resizeYpos=267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRECODICE_1_1 as StdField with uid="OZBUUDMRGM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RECODICE", cQueryName = "RECODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice regola",;
    HelpContextID = 113888091,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=67, Top=20, InputMask=replicate('X',15)

  add object oREDESCRI_1_3 as StdField with uid="SOXANTAAIO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_REDESCRI", cQueryName = "REDESCRI",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione regola",;
    HelpContextID = 28302175,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=188, Top=20, InputMask=replicate('X',45)

  add object oREDESSUP_1_4 as StdMemo with uid="QJDQYVOPTO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_REDESSUP", cQueryName = "REDESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione estesa regola",;
    HelpContextID = 28302182,;
   bGlobalFont=.t.,;
    Height=69, Width=406, Left=188, Top=47


  add object oRE__TIPO_1_9 as StdCombo with uid="YRZUSWWRPS",rtseq=8,rtrep=.f.,left=188,top=125,width=162,height=21;
    , ToolTipText = "Tipo operazione";
    , HelpContextID = 131828581;
    , cFormVar="w_RE__TIPO",RowSource=""+"Creazione,"+"Inserimento,"+"Cancella filtro date,"+"Drop tabella,"+"Cancella da query,"+"Cancella records tabella,"+"Altro", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Tipo regola inconsistente";
  , bGlobalFont=.t.


  func oRE__TIPO_1_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RE__TIPO,&i_cF..t_RE__TIPO),this.value)
    return(iif(xVal =1,'CR',;
    iif(xVal =2,'IN',;
    iif(xVal =3,'DA',;
    iif(xVal =4,'DD',;
    iif(xVal =5,'DQ',;
    iif(xVal =6,'DT',;
    iif(xVal =7,'AL',;
    space(2)))))))))
  endfunc
  func oRE__TIPO_1_9.GetRadio()
    this.Parent.oContained.w_RE__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oRE__TIPO_1_9.ToRadio()
    this.Parent.oContained.w_RE__TIPO=trim(this.Parent.oContained.w_RE__TIPO)
    return(;
      iif(this.Parent.oContained.w_RE__TIPO=='CR',1,;
      iif(this.Parent.oContained.w_RE__TIPO=='IN',2,;
      iif(this.Parent.oContained.w_RE__TIPO=='DA',3,;
      iif(this.Parent.oContained.w_RE__TIPO=='DD',4,;
      iif(this.Parent.oContained.w_RE__TIPO=='DQ',5,;
      iif(this.Parent.oContained.w_RE__TIPO=='DT',6,;
      iif(this.Parent.oContained.w_RE__TIPO=='AL',7,;
      0))))))))
  endfunc

  func oRE__TIPO_1_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oRE__TIPO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RE__TIPO $ 'ALCRDAINDDDQDT')
    endwith
    return bRes
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=158, width=645,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="N.step",Field2="RETIPSTE",Label2="Tipo",Field3="REDESOPE",Label3="Operazione",Field4="REPARAME",Label4="Par.",Field5="RENOMCUR",Label5="Cursore",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202209914

  add object oStr_1_2 as StdString with uid="DVHWCQFYWN",Visible=.t., Left=1, Top=22,;
    Alignment=1, Width=63, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="LZHMGLZNUZ",Visible=.t., Left=126, Top=128,;
    Alignment=1, Width=57, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=178,;
    width=641+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=179,width=640+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBox_2_10 as StdBox with uid="YUZHJCDVZN",left=-6, top=-48, width=0,height=177

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oREDTINVA_3_1 as StdField with uid="BPWAKCAQOS",rtseq=15,rtrep=.f.,;
    cFormVar="w_REDTINVA",value=ctod("  /  /  "),;
    HelpContextID = 203348823,;
    cQueryName = "REDTINVA",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=292, Top=341

  add object oREDTOBSO_3_3 as StdField with uid="RTUNJVNROT",rtseq=16,rtrep=.f.,;
    cFormVar="w_REDTOBSO",value=ctod("  /  /  "),;
    HelpContextID = 8313701,;
    cQueryName = "REDTOBSO",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=534, Top=341

  add object oStr_3_2 as StdString with uid="IUETRQMIRK",Visible=.t., Left=142, Top=345,;
    Alignment=1, Width=149, Height=18,;
    Caption="Data inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="OILBNMIQUT",Visible=.t., Left=427, Top=345,;
    Alignment=1, Width=106, Height=18,;
    Caption="Obsoleto da:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsin_mreBodyRow as CPBodyRowCnt
  Width=631
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="KPYNOAFCOG",rtseq=9,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Step di esecuzione all'interno della regola",;
    HelpContextID = 234538346,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oRETIPSTE_2_2 as StdTrsCombo with uid="IKSJOJMYUR",rtrep=.t.,;
    cFormVar="w_RETIPSTE", RowSource=""+"Batch,"+"Maschera,"+"Query,"+"Tabella" , ;
    ToolTipText = "Tipo di elaborazione associata allo 'step'",;
    HelpContextID = 25484123,;
    Height=21, Width=82, Left=48, Top=1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRETIPSTE_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RETIPSTE,&i_cF..t_RETIPSTE),this.value)
    return(iif(xVal =1,'BA',;
    iif(xVal =2,'MA',;
    iif(xVal =3,'QU',;
    iif(xVal =4,'TA',;
    space(2))))))
  endfunc
  func oRETIPSTE_2_2.GetRadio()
    this.Parent.oContained.w_RETIPSTE = this.RadioValue()
    return .t.
  endfunc

  func oRETIPSTE_2_2.ToRadio()
    this.Parent.oContained.w_RETIPSTE=trim(this.Parent.oContained.w_RETIPSTE)
    return(;
      iif(this.Parent.oContained.w_RETIPSTE=='BA',1,;
      iif(this.Parent.oContained.w_RETIPSTE=='MA',2,;
      iif(this.Parent.oContained.w_RETIPSTE=='QU',3,;
      iif(this.Parent.oContained.w_RETIPSTE=='TA',4,;
      0)))))
  endfunc

  func oRETIPSTE_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oREDESOPE_2_4 as StdTrsField with uid="PMAMBRSVSM",rtseq=12,rtrep=.t.,;
    cFormVar="w_REDESOPE",value=space(100),;
    ToolTipText = "Step da eseguire",;
    HelpContextID = 229628763,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=349, Left=135, Top=0, InputMask=replicate('X',100)

  func oREDESOPE_2_4.mCond()
    with this.Parent.oContained
      return (empty(.w_RENOMCUR))
    endwith
  endfunc

  func oREDESOPE_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_REDESOPE) OR iif(.w_RETIPSTE<>'TA',CHKREGOL(.w_RETIPSTE, .w_REDESOPE, .w_CPROWORD),.T.))
    endwith
    return bRes
  endfunc

  add object oREPARAME_2_5 as StdTrsCheck with uid="OPJIDJLANE",rtrep=.t.,;
    cFormVar="w_REPARAME",  caption="",;
    ToolTipText = "Se attivo indica che � un parametro",;
    HelpContextID = 6513829,;
    Left=499, Top=2, Width=28,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oREPARAME_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REPARAME,&i_cF..t_REPARAME),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oREPARAME_2_5.GetRadio()
    this.Parent.oContained.w_REPARAME = this.RadioValue()
    return .t.
  endfunc

  func oREPARAME_2_5.ToRadio()
    this.Parent.oContained.w_REPARAME=trim(this.Parent.oContained.w_REPARAME)
    return(;
      iif(this.Parent.oContained.w_REPARAME=='S',1,;
      0))
  endfunc

  func oREPARAME_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oRENOMCUR_2_6 as StdTrsField with uid="RIZMOQRBQY",rtseq=14,rtrep=.t.,;
    cFormVar="w_RENOMCUR",value=space(8),;
    ToolTipText = "Nome del cursore generato dalla query",;
    HelpContextID = 22707048,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Nome del cursore non corretto",;
   bGlobalFont=.t.,;
    Height=17, Width=80, Left=546, Top=0, cSayPict=['!!!!!!!!'], cGetPict=['!!!!!!!!'], InputMask=replicate('X',8)

  func oRENOMCUR_2_6.mCond()
    with this.Parent.oContained
      return (.w_RETIPSTE $ 'QU' AND ((NOT EMPTY(.w_REDESOPE) AND .w_REPARAME<>'S') or (EMPTY(.w_REDESOPE) AND .w_REPARAME='S' )))
    endwith
  endfunc

  func oRENOMCUR_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (AT(' ', ALLTRIM(.w_RENOMCUR))=0)
    endwith
    return bRes
  endfunc

  add object oObj_2_8 as cp_askfile with uid="KXDOSGDXWA",width=17,height=17,;
   left=516, top=1,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_NFILE",;
    nPag=2;
    , ToolTipText = "Premere per selezionare la query o il programma da utilizzare";
    , HelpContextID = 113679914

  add object oBtn_2_9 as StdButton with uid="OVZUPRJPSB",width=17,height=17,;
   left=516, top=1,;
    caption="...", nPag=2;
    , HelpContextID = 113679914;
  , bGlobalFont=.t.

    proc oBtn_2_9.Click()
      do GSIN_KST with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETIPSTE<>'TA')
    endwith
   endif
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_mre','INF_REGM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RECODICE=INF_REGM.RECODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
