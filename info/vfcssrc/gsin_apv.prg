* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_apv                                                        *
*              Parametri InfoLink (prosp. vend.)                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_49]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-23                                                      *
* Last revis.: 2012-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsin_apv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsin_apv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsin_apv")
  return

* --- Class definition
define class tgsin_apv as StdPCForm
  Width  = 554
  Height = 299
  Top    = 10
  Left   = 10
  cComment = "Parametri InfoLink (prosp. vend.)"
  cPrg = "gsin_apv"
  HelpContextID=160018281
  add object cnt as tcgsin_apv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsin_apv as PCContext
  w_CODAZI = space(5)
  w_INAGGIOR = space(4)
  w_INFLPUNT = space(1)
  w_INCODESE = space(4)
  w_INLISTIN = space(8)
  w_INNUMERO = space(6)
  w_INSIMVAL = space(5)
  w_INVALUTA = space(3)
  w_INLISTIN = space(8)
  w_INTESTNL = space(1)
  w_INCAMBIO = 0
  w_INSIMVAL = space(5)
  w_INVALUTA = space(3)
  w_INSPEACC = space(1)
  w_INCAMBIO = 0
  w_INRIPCOM = space(1)
  w_INCODAZI = space(5)
  w_INDECTOT = 0
  w_INCAOVAL = 0
  proc Save(oFrom)
    this.w_CODAZI = oFrom.w_CODAZI
    this.w_INAGGIOR = oFrom.w_INAGGIOR
    this.w_INFLPUNT = oFrom.w_INFLPUNT
    this.w_INCODESE = oFrom.w_INCODESE
    this.w_INLISTIN = oFrom.w_INLISTIN
    this.w_INNUMERO = oFrom.w_INNUMERO
    this.w_INSIMVAL = oFrom.w_INSIMVAL
    this.w_INVALUTA = oFrom.w_INVALUTA
    this.w_INLISTIN = oFrom.w_INLISTIN
    this.w_INTESTNL = oFrom.w_INTESTNL
    this.w_INCAMBIO = oFrom.w_INCAMBIO
    this.w_INSIMVAL = oFrom.w_INSIMVAL
    this.w_INVALUTA = oFrom.w_INVALUTA
    this.w_INSPEACC = oFrom.w_INSPEACC
    this.w_INCAMBIO = oFrom.w_INCAMBIO
    this.w_INRIPCOM = oFrom.w_INRIPCOM
    this.w_INCODAZI = oFrom.w_INCODAZI
    this.w_INDECTOT = oFrom.w_INDECTOT
    this.w_INCAOVAL = oFrom.w_INCAOVAL
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CODAZI = this.w_CODAZI
    oTo.w_INAGGIOR = this.w_INAGGIOR
    oTo.w_INFLPUNT = this.w_INFLPUNT
    oTo.w_INCODESE = this.w_INCODESE
    oTo.w_INLISTIN = this.w_INLISTIN
    oTo.w_INNUMERO = this.w_INNUMERO
    oTo.w_INSIMVAL = this.w_INSIMVAL
    oTo.w_INVALUTA = this.w_INVALUTA
    oTo.w_INLISTIN = this.w_INLISTIN
    oTo.w_INTESTNL = this.w_INTESTNL
    oTo.w_INCAMBIO = this.w_INCAMBIO
    oTo.w_INSIMVAL = this.w_INSIMVAL
    oTo.w_INVALUTA = this.w_INVALUTA
    oTo.w_INSPEACC = this.w_INSPEACC
    oTo.w_INCAMBIO = this.w_INCAMBIO
    oTo.w_INRIPCOM = this.w_INRIPCOM
    oTo.w_INCODAZI = this.w_INCODAZI
    oTo.w_INDECTOT = this.w_INDECTOT
    oTo.w_INCAOVAL = this.w_INCAOVAL
    PCContext::Load(oTo)
enddefine

define class tcgsin_apv as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 554
  Height = 299
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-12"
  HelpContextID=160018281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  PAR_INFO_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  INVENTAR_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  cFile = "PAR_INFO"
  cKeySelect = "INCODAZI"
  cKeyWhere  = "INCODAZI=this.w_INCODAZI"
  cKeyWhereODBC = '"INCODAZI="+cp_ToStrODBC(this.w_INCODAZI)';

  cKeyWhereODBCqualified = '"PAR_INFO.INCODAZI="+cp_ToStrODBC(this.w_INCODAZI)';

  cPrg = "gsin_apv"
  cComment = "Parametri InfoLink (prosp. vend.)"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_INAGGIOR = space(4)
  o_INAGGIOR = space(4)
  w_INFLPUNT = space(1)
  w_INCODESE = space(4)
  w_INLISTIN = space(8)
  w_INNUMERO = space(6)
  w_INSIMVAL = space(5)
  w_INVALUTA = space(3)
  o_INVALUTA = space(3)
  w_INLISTIN = space(8)
  w_INTESTNL = space(1)
  w_INCAMBIO = 0
  w_INSIMVAL = space(5)
  w_INVALUTA = space(3)
  w_INSPEACC = space(1)
  w_INCAMBIO = 0
  w_INRIPCOM = space(1)
  w_INCODAZI = space(5)
  w_INDECTOT = 0
  w_INCAOVAL = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsin_apvPag1","gsin_apv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 101570826
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINAGGIOR_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='LISTINI'
    this.cWorkTables[3]='INVENTAR'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='PAR_INFO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_INFO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_INFO_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsin_apv'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_INFO where INCODAZI=KeySet.INCODAZI
    *
    i_nConn = i_TableProp[this.PAR_INFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_INFO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_INFO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_INFO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_INFO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'INCODAZI',this.w_INCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_INAGGIOR = NVL(INAGGIOR,space(4))
        .w_INFLPUNT = NVL(INFLPUNT,space(1))
        .w_INCODESE = NVL(INCODESE,space(4))
          * evitabile
          *.link_1_5('Load')
        .w_INLISTIN = NVL(INLISTIN,space(8))
          * evitabile
          *.link_1_7('Load')
        .w_INNUMERO = NVL(INNUMERO,space(6))
          * evitabile
          *.link_1_9('Load')
        .w_INSIMVAL = NVL(INSIMVAL,space(5))
        .w_INVALUTA = NVL(INVALUTA,space(3))
          * evitabile
          *.link_1_12('Load')
        .w_INLISTIN = NVL(INLISTIN,space(8))
          * evitabile
          *.link_1_14('Load')
        .w_INTESTNL = NVL(INTESTNL,space(1))
        .w_INCAMBIO = NVL(INCAMBIO,0)
        .w_INSIMVAL = NVL(INSIMVAL,space(5))
        .w_INVALUTA = NVL(INVALUTA,space(3))
          * evitabile
          *.link_1_20('Load')
        .w_INSPEACC = NVL(INSPEACC,space(1))
        .w_INCAMBIO = NVL(INCAMBIO,0)
        .w_INRIPCOM = NVL(INRIPCOM,space(1))
        .w_INCODAZI = NVL(INCODAZI,space(5))
        .w_INDECTOT = NVL(INDECTOT,0)
        .w_INCAOVAL = NVL(INCAOVAL,0)
        cp_LoadRecExtFlds(this,'PAR_INFO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CODAZI = space(5)
      .w_INAGGIOR = space(4)
      .w_INFLPUNT = space(1)
      .w_INCODESE = space(4)
      .w_INLISTIN = space(8)
      .w_INNUMERO = space(6)
      .w_INSIMVAL = space(5)
      .w_INVALUTA = space(3)
      .w_INLISTIN = space(8)
      .w_INTESTNL = space(1)
      .w_INCAMBIO = 0
      .w_INSIMVAL = space(5)
      .w_INVALUTA = space(3)
      .w_INSPEACC = space(1)
      .w_INCAMBIO = 0
      .w_INRIPCOM = space(1)
      .w_INCODAZI = space(5)
      .w_INDECTOT = 0
      .w_INCAOVAL = 0
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .w_INAGGIOR = 'CMPA'
        .w_INFLPUNT = 'N'
        .w_INCODESE = g_CODESE
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_INCODESE))
          .link_1_5('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_INLISTIN))
          .link_1_7('Full')
          endif
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_INNUMERO))
          .link_1_9('Full')
          endif
        .w_INSIMVAL = IIF(EMPTY(.w_INVALUTA),' ',.w_INSIMVAL)
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_INVALUTA))
          .link_1_12('Full')
          endif
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_INLISTIN))
          .link_1_14('Full')
          endif
          .DoRTCalc(10,10,.f.)
        .w_INCAMBIO = IIF(.w_INCAOVAL<>0,.w_INCAOVAL,GETCAM(g_PERVAL,I_DATSYS))
        .w_INSIMVAL = IIF(EMPTY(.w_INVALUTA),' ',.w_INSIMVAL)
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_INVALUTA))
          .link_1_20('Full')
          endif
        .w_INSPEACC = 'N'
        .w_INCAMBIO = IIF(.w_INCAOVAL<>0,.w_INCAOVAL,GETCAM(g_PERVAL,I_DATSYS))
        .w_INRIPCOM = ' '
        .w_INCODAZI = i_CODAZI
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_INFO')
    this.DoRTCalc(18,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oINAGGIOR_1_2.enabled = i_bVal
      .Page1.oPag.oINFLPUNT_1_4.enabled = i_bVal
      .Page1.oPag.oINCODESE_1_5.enabled = i_bVal
      .Page1.oPag.oINLISTIN_1_7.enabled = i_bVal
      .Page1.oPag.oINNUMERO_1_9.enabled = i_bVal
      .Page1.oPag.oINLISTIN_1_14.enabled = i_bVal
      .Page1.oPag.oINCAMBIO_1_17.enabled = i_bVal
      .Page1.oPag.oINVALUTA_1_20.enabled = i_bVal
      .Page1.oPag.oINSPEACC_1_23.enabled = i_bVal
      .Page1.oPag.oINCAMBIO_1_24.enabled = i_bVal
      .Page1.oPag.oINRIPCOM_1_25.enabled = i_bVal
      .Page1.oPag.oBtn_1_30.enabled = .Page1.oPag.oBtn_1_30.mCond()
      .Page1.oPag.oBtn_1_31.enabled = .Page1.oPag.oBtn_1_31.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'PAR_INFO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_INFO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INAGGIOR,"INAGGIOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INFLPUNT,"INFLPUNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCODESE,"INCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INLISTIN,"INLISTIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INNUMERO,"INNUMERO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INSIMVAL,"INSIMVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INVALUTA,"INVALUTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INLISTIN,"INLISTIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INTESTNL,"INTESTNL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCAMBIO,"INCAMBIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INSIMVAL,"INSIMVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INVALUTA,"INVALUTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INSPEACC,"INSPEACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCAMBIO,"INCAMBIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INRIPCOM,"INRIPCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCODAZI,"INCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INDECTOT,"INDECTOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCAOVAL,"INCAOVAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_INFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_INFO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_INFO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_INFO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_INFO')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_INFO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(INAGGIOR,INFLPUNT,INCODESE,INLISTIN,INNUMERO"+;
                  ",INSIMVAL,INVALUTA,INTESTNL,INCAMBIO,INSPEACC"+;
                  ",INRIPCOM,INCODAZI,INDECTOT,INCAOVAL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_INAGGIOR)+;
                  ","+cp_ToStrODBC(this.w_INFLPUNT)+;
                  ","+cp_ToStrODBCNull(this.w_INCODESE)+;
                  ","+cp_ToStrODBCNull(this.w_INLISTIN)+;
                  ","+cp_ToStrODBCNull(this.w_INNUMERO)+;
                  ","+cp_ToStrODBC(this.w_INSIMVAL)+;
                  ","+cp_ToStrODBCNull(this.w_INVALUTA)+;
                  ","+cp_ToStrODBC(this.w_INTESTNL)+;
                  ","+cp_ToStrODBC(this.w_INCAMBIO)+;
                  ","+cp_ToStrODBC(this.w_INSPEACC)+;
                  ","+cp_ToStrODBC(this.w_INRIPCOM)+;
                  ","+cp_ToStrODBC(this.w_INCODAZI)+;
                  ","+cp_ToStrODBC(this.w_INDECTOT)+;
                  ","+cp_ToStrODBC(this.w_INCAOVAL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_INFO')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_INFO')
        cp_CheckDeletedKey(i_cTable,0,'INCODAZI',this.w_INCODAZI)
        INSERT INTO (i_cTable);
              (INAGGIOR,INFLPUNT,INCODESE,INLISTIN,INNUMERO,INSIMVAL,INVALUTA,INTESTNL,INCAMBIO,INSPEACC,INRIPCOM,INCODAZI,INDECTOT,INCAOVAL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_INAGGIOR;
                  ,this.w_INFLPUNT;
                  ,this.w_INCODESE;
                  ,this.w_INLISTIN;
                  ,this.w_INNUMERO;
                  ,this.w_INSIMVAL;
                  ,this.w_INVALUTA;
                  ,this.w_INTESTNL;
                  ,this.w_INCAMBIO;
                  ,this.w_INSPEACC;
                  ,this.w_INRIPCOM;
                  ,this.w_INCODAZI;
                  ,this.w_INDECTOT;
                  ,this.w_INCAOVAL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_INFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_INFO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_INFO_IDX,i_nConn)
      *
      * update PAR_INFO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_INFO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " INAGGIOR="+cp_ToStrODBC(this.w_INAGGIOR)+;
             ",INFLPUNT="+cp_ToStrODBC(this.w_INFLPUNT)+;
             ",INCODESE="+cp_ToStrODBCNull(this.w_INCODESE)+;
             ",INLISTIN="+cp_ToStrODBCNull(this.w_INLISTIN)+;
             ",INNUMERO="+cp_ToStrODBCNull(this.w_INNUMERO)+;
             ",INSIMVAL="+cp_ToStrODBC(this.w_INSIMVAL)+;
             ",INVALUTA="+cp_ToStrODBCNull(this.w_INVALUTA)+;
             ",INTESTNL="+cp_ToStrODBC(this.w_INTESTNL)+;
             ",INCAMBIO="+cp_ToStrODBC(this.w_INCAMBIO)+;
             ",INSPEACC="+cp_ToStrODBC(this.w_INSPEACC)+;
             ",INRIPCOM="+cp_ToStrODBC(this.w_INRIPCOM)+;
             ",INDECTOT="+cp_ToStrODBC(this.w_INDECTOT)+;
             ",INCAOVAL="+cp_ToStrODBC(this.w_INCAOVAL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_INFO')
        i_cWhere = cp_PKFox(i_cTable  ,'INCODAZI',this.w_INCODAZI  )
        UPDATE (i_cTable) SET;
              INAGGIOR=this.w_INAGGIOR;
             ,INFLPUNT=this.w_INFLPUNT;
             ,INCODESE=this.w_INCODESE;
             ,INLISTIN=this.w_INLISTIN;
             ,INNUMERO=this.w_INNUMERO;
             ,INSIMVAL=this.w_INSIMVAL;
             ,INVALUTA=this.w_INVALUTA;
             ,INTESTNL=this.w_INTESTNL;
             ,INCAMBIO=this.w_INCAMBIO;
             ,INSPEACC=this.w_INSPEACC;
             ,INRIPCOM=this.w_INRIPCOM;
             ,INDECTOT=this.w_INDECTOT;
             ,INCAOVAL=this.w_INCAOVAL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_INFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_INFO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_INFO_IDX,i_nConn)
      *
      * delete PAR_INFO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'INCODAZI',this.w_INCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_INFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_INFO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_INAGGIOR<>.w_INAGGIOR
            .w_INFLPUNT = 'N'
        endif
        if .o_INAGGIOR<>.w_INAGGIOR
          .link_1_5('Full')
        endif
        .DoRTCalc(5,6,.t.)
            .w_INSIMVAL = IIF(EMPTY(.w_INVALUTA),' ',.w_INSIMVAL)
          .link_1_12('Full')
        .DoRTCalc(9,10,.t.)
        if .o_INVALUTA<>.w_INVALUTA
            .w_INCAMBIO = IIF(.w_INCAOVAL<>0,.w_INCAOVAL,GETCAM(g_PERVAL,I_DATSYS))
        endif
            .w_INSIMVAL = IIF(EMPTY(.w_INVALUTA),' ',.w_INSIMVAL)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oINCODESE_1_5.enabled = this.oPgFrm.Page1.oPag.oINCODESE_1_5.mCond()
    this.oPgFrm.Page1.oPag.oINNUMERO_1_9.enabled = this.oPgFrm.Page1.oPag.oINNUMERO_1_9.mCond()
    this.oPgFrm.Page1.oPag.oINCAMBIO_1_17.enabled = this.oPgFrm.Page1.oPag.oINCAMBIO_1_17.mCond()
    this.oPgFrm.Page1.oPag.oINCAMBIO_1_24.enabled = this.oPgFrm.Page1.oPag.oINCAMBIO_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oINFLPUNT_1_4.visible=!this.oPgFrm.Page1.oPag.oINFLPUNT_1_4.mHide()
    this.oPgFrm.Page1.oPag.oINCODESE_1_5.visible=!this.oPgFrm.Page1.oPag.oINCODESE_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oINNUMERO_1_9.visible=!this.oPgFrm.Page1.oPag.oINNUMERO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oINSIMVAL_1_11.visible=!this.oPgFrm.Page1.oPag.oINSIMVAL_1_11.mHide()
    this.oPgFrm.Page1.oPag.oINVALUTA_1_12.visible=!this.oPgFrm.Page1.oPag.oINVALUTA_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oINLISTIN_1_14.visible=!this.oPgFrm.Page1.oPag.oINLISTIN_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oINCAMBIO_1_17.visible=!this.oPgFrm.Page1.oPag.oINCAMBIO_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oINCAMBIO_1_24.visible=!this.oPgFrm.Page1.oPag.oINCAMBIO_1_24.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=INCODESE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_INCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_INCODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oINCODESE_1_5'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_INCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_INCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INVALUTA = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_INCODESE = space(4)
      endif
      this.w_INVALUTA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INLISTIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INLISTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_INLISTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_INLISTIN))
          select LSCODLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INLISTIN)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INLISTIN) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oINLISTIN_1_7'),i_cWhere,'',"",'GSMAPDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INLISTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_INLISTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_INLISTIN)
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INLISTIN = NVL(_Link_.LSCODLIS,space(8))
      this.w_INVALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_INTESTNL = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_INLISTIN = space(8)
      endif
      this.w_INVALUTA = space(3)
      this.w_INTESTNL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INLISTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INNUMERO
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INNUMERO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AIN',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_INNUMERO)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_INCODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_INCODESE;
                     ,'INNUMINV',trim(this.w_INNUMERO))
          select INCODESE,INNUMINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INNUMERO)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INNUMERO) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oINNUMERO_1_9'),i_cWhere,'GSMA_AIN',"Inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_INCODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_INCODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INNUMERO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_INNUMERO);
                   +" and INCODESE="+cp_ToStrODBC(this.w_INCODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_INCODESE;
                       ,'INNUMINV',this.w_INNUMERO)
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INNUMERO = NVL(_Link_.INNUMINV,space(6))
      this.w_INCODESE = NVL(_Link_.INCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_INNUMERO = space(6)
      endif
      this.w_INCODESE = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INNUMERO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INVALUTA
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INVALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INVALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_INVALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_INVALUTA)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INVALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_INDECTOT = NVL(_Link_.VADECTOT,0)
      this.w_INCAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_INSIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_INVALUTA = space(3)
      endif
      this.w_INDECTOT = 0
      this.w_INCAOVAL = 0
      this.w_INSIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INVALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INLISTIN
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INLISTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_INLISTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_INLISTIN))
          select LSCODLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INLISTIN)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INLISTIN) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oINLISTIN_1_14'),i_cWhere,'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INLISTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_INLISTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_INLISTIN)
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INLISTIN = NVL(_Link_.LSCODLIS,space(8))
      this.w_INVALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_INTESTNL = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_INLISTIN = space(8)
      endif
      this.w_INVALUTA = space(3)
      this.w_INTESTNL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INLISTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INVALUTA
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INVALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_INVALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_INVALUTA))
          select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INVALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INVALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oINVALUTA_1_20'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INVALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_INVALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_INVALUTA)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INVALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_INDECTOT = NVL(_Link_.VADECTOT,0)
      this.w_INCAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_INSIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_INVALUTA = space(3)
      endif
      this.w_INDECTOT = 0
      this.w_INCAOVAL = 0
      this.w_INSIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INVALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINAGGIOR_1_2.RadioValue()==this.w_INAGGIOR)
      this.oPgFrm.Page1.oPag.oINAGGIOR_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINFLPUNT_1_4.RadioValue()==this.w_INFLPUNT)
      this.oPgFrm.Page1.oPag.oINFLPUNT_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINCODESE_1_5.value==this.w_INCODESE)
      this.oPgFrm.Page1.oPag.oINCODESE_1_5.value=this.w_INCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oINLISTIN_1_7.value==this.w_INLISTIN)
      this.oPgFrm.Page1.oPag.oINLISTIN_1_7.value=this.w_INLISTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oINNUMERO_1_9.value==this.w_INNUMERO)
      this.oPgFrm.Page1.oPag.oINNUMERO_1_9.value=this.w_INNUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oINSIMVAL_1_11.value==this.w_INSIMVAL)
      this.oPgFrm.Page1.oPag.oINSIMVAL_1_11.value=this.w_INSIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oINVALUTA_1_12.value==this.w_INVALUTA)
      this.oPgFrm.Page1.oPag.oINVALUTA_1_12.value=this.w_INVALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oINLISTIN_1_14.value==this.w_INLISTIN)
      this.oPgFrm.Page1.oPag.oINLISTIN_1_14.value=this.w_INLISTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oINCAMBIO_1_17.value==this.w_INCAMBIO)
      this.oPgFrm.Page1.oPag.oINCAMBIO_1_17.value=this.w_INCAMBIO
    endif
    if not(this.oPgFrm.Page1.oPag.oINSIMVAL_1_19.value==this.w_INSIMVAL)
      this.oPgFrm.Page1.oPag.oINSIMVAL_1_19.value=this.w_INSIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oINVALUTA_1_20.value==this.w_INVALUTA)
      this.oPgFrm.Page1.oPag.oINVALUTA_1_20.value=this.w_INVALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oINSPEACC_1_23.RadioValue()==this.w_INSPEACC)
      this.oPgFrm.Page1.oPag.oINSPEACC_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINCAMBIO_1_24.value==this.w_INCAMBIO)
      this.oPgFrm.Page1.oPag.oINCAMBIO_1_24.value=this.w_INCAMBIO
    endif
    if not(this.oPgFrm.Page1.oPag.oINRIPCOM_1_25.RadioValue()==this.w_INRIPCOM)
      this.oPgFrm.Page1.oPag.oINRIPCOM_1_25.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PAR_INFO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_INCODESE))  and not(.w_INAGGIOR='LIST' OR .w_INAGGIOR='UCST')  and (.w_INAGGIOR<>'LIST')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINCODESE_1_5.SetFocus()
            i_bnoObbl = !empty(.w_INCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_INNUMERO))  and not(.w_INAGGIOR='LIST' OR .w_INAGGIOR='UCST')  and (.w_INAGGIOR<>'LIST' AND NOT EMPTY(.w_INCODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINNUMERO_1_9.SetFocus()
            i_bnoObbl = !empty(.w_INNUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_INCAMBIO<>0)  and not(.w_INCAOVAL<>0 OR .w_INAGGIOR<>'LIST' OR Empty(nvl(.w_INVALUTA,' ')) OR .w_INAGGIOR='UCST')  and (.w_INCAOVAL=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINCAMBIO_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_INCAMBIO<>0)  and not(.w_INCAOVAL<>0 or Empty(nvl(.w_INVALUTA,' ')))  and (.w_INCAOVAL=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINCAMBIO_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INAGGIOR = this.w_INAGGIOR
    this.o_INVALUTA = this.w_INVALUTA
    return

enddefine

* --- Define pages as container
define class tgsin_apvPag1 as StdContainer
  Width  = 550
  height = 299
  stdWidth  = 550
  stdheight = 299
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oINAGGIOR_1_2 as StdCombo with uid="LWDLEXEQST",rtseq=2,rtrep=.f.,left=103,top=49,width=218,height=21;
    , HelpContextID = 70366168;
    , cFormVar="w_INAGGIOR",RowSource=""+"Costo medio ponderato annuo,"+"Costo medio ponderato periodico,"+"Costo ultimo,"+"Costo standard,"+"LIFO continuo,"+"FIFO continuo,"+"LIFO a scatti,"+"Ultimo costo standard,"+"Listino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINAGGIOR_1_2.RadioValue()
    return(iif(this.value =1,'CMPA',;
    iif(this.value =2,'CMPP',;
    iif(this.value =3,'COUL',;
    iif(this.value =4,'COST',;
    iif(this.value =5,'LICO',;
    iif(this.value =6,'FICO',;
    iif(this.value =7,'LISC',;
    iif(this.value =8,'UCST',;
    iif(this.value =9,'LIST',;
    space(4)))))))))))
  endfunc
  func oINAGGIOR_1_2.GetRadio()
    this.Parent.oContained.w_INAGGIOR = this.RadioValue()
    return .t.
  endfunc

  func oINAGGIOR_1_2.SetRadio()
    this.Parent.oContained.w_INAGGIOR=trim(this.Parent.oContained.w_INAGGIOR)
    this.value = ;
      iif(this.Parent.oContained.w_INAGGIOR=='CMPA',1,;
      iif(this.Parent.oContained.w_INAGGIOR=='CMPP',2,;
      iif(this.Parent.oContained.w_INAGGIOR=='COUL',3,;
      iif(this.Parent.oContained.w_INAGGIOR=='COST',4,;
      iif(this.Parent.oContained.w_INAGGIOR=='LICO',5,;
      iif(this.Parent.oContained.w_INAGGIOR=='FICO',6,;
      iif(this.Parent.oContained.w_INAGGIOR=='LISC',7,;
      iif(this.Parent.oContained.w_INAGGIOR=='UCST',8,;
      iif(this.Parent.oContained.w_INAGGIOR=='LIST',9,;
      0)))))))))
  endfunc

  add object oINFLPUNT_1_4 as StdCheck with uid="XSZOXXCHYP",rtseq=3,rtrep=.f.,left=343, top=50, caption="Elab. FIFO/LIFO puntuale",;
    HelpContextID = 13042650,;
    cFormVar="w_INFLPUNT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINFLPUNT_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oINFLPUNT_1_4.GetRadio()
    this.Parent.oContained.w_INFLPUNT = this.RadioValue()
    return .t.
  endfunc

  func oINFLPUNT_1_4.SetRadio()
    this.Parent.oContained.w_INFLPUNT=trim(this.Parent.oContained.w_INFLPUNT)
    this.value = ;
      iif(this.Parent.oContained.w_INFLPUNT=='S',1,;
      0)
  endfunc

  func oINFLPUNT_1_4.mHide()
    with this.Parent.oContained
      return (NOT(.w_INAGGIOR $ 'FICO-LICO'))
    endwith
  endfunc

  proc oINFLPUNT_1_4.mAfter
    with this.Parent.oContained
      IIF(.w_INFLPUNT='S',AH_ERRORMSG("Indipendentemente dal periodo impostato nella maschera di esportazione dati%0verranno considerati nell'elaborazione del prospetto del venduto solo i movimenti%0con data registrazione maggiore della data finale dell'inventario selezionato"),.w_INFLPUNT=.w_INFLPUNT)
    endwith
  endproc

  add object oINCODESE_1_5 as StdField with uid="EYXMMUIYBG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INCODESE", cQueryName = "INCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 644043,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=103, Top=85, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_INCODESE"

  func oINCODESE_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INAGGIOR<>'LIST')
    endwith
   endif
  endfunc

  func oINCODESE_1_5.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR='LIST' OR .w_INAGGIOR='UCST')
    endwith
  endfunc

  func oINCODESE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
      if .not. empty(.w_INNUMERO)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oINCODESE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINCODESE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oINCODESE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oINLISTIN_1_7 as StdField with uid="RGEGMVRYWH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INLISTIN", cQueryName = "INLISTIN",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 760876,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=103, Top=162, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_INLISTIN"

  func oINLISTIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oINLISTIN_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINLISTIN_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oINLISTIN_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSMAPDV.LISTINI_VZM',this.parent.oContained
  endproc

  add object oINNUMERO_1_9 as StdField with uid="VJITWLUHBL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_INNUMERO", cQueryName = "INNUMERO",;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 10519509,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=243, Top=85, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="GSMA_AIN", oKey_1_1="INCODESE", oKey_1_2="this.w_INCODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_INNUMERO"

  func oINNUMERO_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INAGGIOR<>'LIST' AND NOT EMPTY(.w_INCODESE))
    endwith
   endif
  endfunc

  func oINNUMERO_1_9.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR='LIST' OR .w_INAGGIOR='UCST')
    endwith
  endfunc

  func oINNUMERO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oINNUMERO_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINNUMERO_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_INCODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_INCODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oINNUMERO_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AIN',"Inventari",'',this.parent.oContained
  endproc
  proc oINNUMERO_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AIN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_INCODESE
     i_obj.w_INNUMINV=this.parent.oContained.w_INNUMERO
     i_obj.ecpSave()
  endproc

  add object oINSIMVAL_1_11 as StdField with uid="PLDCOLVXQT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_INSIMVAL", cQueryName = "INSIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 26530770,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=297, Top=85, InputMask=replicate('X',5)

  func oINSIMVAL_1_11.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR<>'LIST' OR .w_INAGGIOR='UCTS')
    endwith
  endfunc

  add object oINVALUTA_1_12 as StdField with uid="PGWTLBCARM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_INVALUTA", cQueryName = "INVALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 8192967,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=243, Top=85, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_INVALUTA"

  func oINVALUTA_1_12.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR<>'LIST' OR .w_INAGGIOR ='UCST')
    endwith
  endfunc

  func oINVALUTA_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oINLISTIN_1_14 as StdField with uid="XPEVIIIUHB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_INLISTIN", cQueryName = "INLISTIN",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 760876,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=103, Top=85, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_INLISTIN"

  func oINLISTIN_1_14.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR<>'LIST' OR .w_INAGGIOR='UCST')
    endwith
  endfunc

  func oINLISTIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oINLISTIN_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINLISTIN_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oINLISTIN_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this.parent.oContained
  endproc
  proc oINLISTIN_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_INLISTIN
     i_obj.ecpSave()
  endproc

  add object oINCAMBIO_1_17 as StdField with uid="KODRSYQLHH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_INCAMBIO", cQueryName = "INCAMBIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 41167915,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=445, Top=85, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oINCAMBIO_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INCAOVAL=0)
    endwith
   endif
  endfunc

  func oINCAMBIO_1_17.mHide()
    with this.Parent.oContained
      return (.w_INCAOVAL<>0 OR .w_INAGGIOR<>'LIST' OR Empty(nvl(.w_INVALUTA,' ')) OR .w_INAGGIOR='UCST')
    endwith
  endfunc

  func oINCAMBIO_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_INCAMBIO<>0)
    endwith
    return bRes
  endfunc

  add object oINSIMVAL_1_19 as StdField with uid="WSAKGNWIMU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_INSIMVAL", cQueryName = "INSIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 26530770,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=297, Top=162, InputMask=replicate('X',5)

  add object oINVALUTA_1_20 as StdField with uid="CAZRWOHKAZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_INVALUTA", cQueryName = "INVALUTA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 8192967,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=242, Top=162, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_INVALUTA"

  func oINVALUTA_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oINVALUTA_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINVALUTA_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oINVALUTA_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oINVALUTA_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_INVALUTA
     i_obj.ecpSave()
  endproc

  add object oINSPEACC_1_23 as StdCheck with uid="EDLHTYINBN",rtseq=14,rtrep=.f.,left=45, top=185, caption="Includi spese accessorie",;
    ToolTipText = "Se attivo include spese accessorie ripartite.",;
    HelpContextID = 203150281,;
    cFormVar="w_INSPEACC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINSPEACC_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oINSPEACC_1_23.GetRadio()
    this.Parent.oContained.w_INSPEACC = this.RadioValue()
    return .t.
  endfunc

  func oINSPEACC_1_23.SetRadio()
    this.Parent.oContained.w_INSPEACC=trim(this.Parent.oContained.w_INSPEACC)
    this.value = ;
      iif(this.Parent.oContained.w_INSPEACC=='S',1,;
      0)
  endfunc

  add object oINCAMBIO_1_24 as StdField with uid="HWTRVWMFVX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_INCAMBIO", cQueryName = "INCAMBIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 41167915,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=443, Top=162, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oINCAMBIO_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INCAOVAL=0)
    endwith
   endif
  endfunc

  func oINCAMBIO_1_24.mHide()
    with this.Parent.oContained
      return (.w_INCAOVAL<>0 or Empty(nvl(.w_INVALUTA,' ')))
    endwith
  endfunc

  func oINCAMBIO_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_INCAMBIO<>0)
    endwith
    return bRes
  endfunc

  add object oINRIPCOM_1_25 as StdCheck with uid="JBZTHLEECY",rtseq=16,rtrep=.f.,left=45, top=240, caption="Ripartizione analitica per competenza",;
    ToolTipText = "Se attivo: abilita la ripartizione per c. C/R per data competenza",;
    HelpContextID = 247776211,;
    cFormVar="w_INRIPCOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINRIPCOM_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oINRIPCOM_1_25.GetRadio()
    this.Parent.oContained.w_INRIPCOM = this.RadioValue()
    return .t.
  endfunc

  func oINRIPCOM_1_25.SetRadio()
    this.Parent.oContained.w_INRIPCOM=trim(this.Parent.oContained.w_INRIPCOM)
    this.value = ;
      iif(this.Parent.oContained.w_INRIPCOM=='S',1,;
      0)
  endfunc


  add object oBtn_1_30 as StdButton with uid="DIVTEGYBCI",left=441, top=247, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le impostazioni";
    , HelpContextID = 159989530;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="LNMWHHBLEZ",left=495, top=247, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 152700858;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="NRMYFYTEQY",Visible=.t., Left=7, Top=51,;
    Alignment=1, Width=95, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="ISIXYOPOSU",Visible=.t., Left=7, Top=89,;
    Alignment=1, Width=95, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR='LIST' OR .w_INAGGIOR='UCST')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="IKUTASPCAL",Visible=.t., Left=7, Top=166,;
    Alignment=1, Width=95, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="XPWRWSVSGE",Visible=.t., Left=177, Top=89,;
    Alignment=1, Width=63, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR='LIST' OR .w_INAGGIOR='UCST')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="HJMNQPMXRX",Visible=.t., Left=184, Top=89,;
    Alignment=1, Width=55, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR<>'LIST')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="DLYZWFSFQI",Visible=.t., Left=379, Top=89,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_INCAOVAL<>0 OR .w_INAGGIOR<>'LIST' OR Empty(nvl(.w_INVALUTA,' ')))
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="FSFDYBBMOV",Visible=.t., Left=7, Top=89,;
    Alignment=1, Width=95, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_INAGGIOR<>'LIST')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="VLYFGMJQQD",Visible=.t., Left=184, Top=166,;
    Alignment=1, Width=55, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="KRHTRCNXGI",Visible=.t., Left=377, Top=166,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_INCAOVAL<>0 OR EMPTY(NVL(.w_INVALUTA,' ')))
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="VGDTTYUJFH",Visible=.t., Left=12, Top=20,;
    Alignment=0, Width=381, Height=18,;
    Caption="Valorizzazione dati articoli prospetto del venduto"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="BJOFYFRPGS",Visible=.t., Left=10, Top=135,;
    Alignment=0, Width=358, Height=18,;
    Caption="Valorizzazione dati servizi prospetto del venduto"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="ABHBEBPVTS",Visible=.t., Left=10, Top=211,;
    Alignment=0, Width=358, Height=18,;
    Caption="Valorizzazione dati movimenti di analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_26 as StdBox with uid="EJSDSVTOPF",left=5, top=37, width=537,height=3

  add object oBox_1_27 as StdBox with uid="IZYXKXQHKL",left=5, top=152, width=537,height=3

  add object oBox_1_35 as StdBox with uid="FKVQLZSHNP",left=5, top=228, width=537,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_apv','PAR_INFO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".INCODAZI=PAR_INFO.INCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
