* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bcc                                                        *
*              Carica visual zoom                                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][60]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-10                                                      *
* Last revis.: 2003-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPERAZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bcc",oParentObject,m.pOPERAZ)
return(i_retval)

define class tgsin_bcc as StdBatch
  * --- Local variables
  pOPERAZ = space(1)
  w_ZOOM = space(10)
  w_TABELLA = space(20)
  w_ODBC = space(25)
  w_STRINGA = space(90)
  w_CONNECT = 0
  w_CONNECTION = space(25)
  w_CRIPTATA = space(1)
  w_CODREGOL = space(15)
  w_MULTIAZI = space(1)
  w_TIPOREG = space(2)
  w_TIPOOPER = space(2)
  * --- WorkFile variables
  INF_GRUD_idx=0
  INF_REGD_idx=0
  INF_TAAZ_idx=0
  INF_TAB_idx=0
  INF_REGM_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamato dalla maschera di esportazione (GSIN_KLE)
    do case
      case this.pOPERAZ="S"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPERAZ="C"
        create cursor TABELLE (NOMETAB C(50), TIPOOPER C(2))
        create cursor AZIENDE (TACODAZI C(5), TADESAZI C(40))
        this.w_ZOOM = this.oparentobject.w_AZIENDE
        this.w_CODREGOL = this.oParentObject.w_CODGRU
        ZAP IN ( this.w_ZOOM.cCursor )
        NC = this.w_ZOOM.cCursor
        VQ_EXEC("..\INFO\EXE\QUERY\GSIN_BCC",this,"TABELLE")
        if Used("TABELLE")
          Select TABELLE 
 Go Top 
 Scan
          * --- Select from AZIENDA
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select AZCODAZI AS TACODAZI,AZRAGAZI AS TADESAZI  from "+i_cTable+" AZIENDA ";
                 ,"_Curs_AZIENDA")
          else
            select AZCODAZI AS TACODAZI,AZRAGAZI AS TADESAZI from (i_cTable);
              into cursor _Curs_AZIENDA
          endif
          if used('_Curs_AZIENDA')
            select _Curs_AZIENDA
            locate for 1=1
            do while not(eof())
            scatter memvar 
 insert into (NC) from memvar
              select _Curs_AZIENDA
              continue
            enddo
            use
          endif
          EXIT
          EndScan
          Select TABELLE 
 Use
          UPDATE ( this.w_ZOOM.cCursor ) SET XCHK = 1 WHERE TACODAZI=i_CODAZI
        endif
        SELECT ( this.w_ZOOM.cCursor )
        GO TOP
        this.w_zoom.grd.refresh
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Acquisisco il nome dell'ODBC o la stringa di connessione relativo alla tabella (w_Tabella)
    * --- Prima controllo se esiste una connessione di "default" per la tabella inerente  all'azienda in uso
    * --- Read from INF_TAAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TACOODBC,ATCOSTRI,ATCRIPTE"+;
        " from "+i_cTable+" INF_TAAZ where ";
            +"TACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TACOODBC,ATCOSTRI,ATCRIPTE;
        from (i_cTable) where;
            TACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
      this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
      this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo nell'archivio Tabelle / Connessioni 
      * --- Read from INF_TAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAB where ";
              +"ATNTABLE = "+cp_ToStrODBC(this.w_Tabella);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              ATNTABLE = this.w_Tabella;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.ATCOODBC),cp_NullValue(_read_.ATCOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo se esiste per la tabella una connessione globale (cio� indipendente dall'azienda)
      * --- Read from INF_TAAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TACOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAAZ where ";
              +"TACODAZI = "+cp_ToStrODBC(space(5));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TACOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              TACODAZI = space(5);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Database  diverso da quello usato dalla procedura.
      *     Creo una nuova connessione
      if this.w_CRIPTATA = "C"
        this.w_STRINGA = CifraCnf( ALLTRIM(this.w_STRINGA) , "D" )
      endif
      this.w_CONNECTION = alltrim(this.w_ODBC + this.w_STRINGA)
      this.w_CONNECT = cp_openDatabaseConnection(this.w_connection)
    else
      * --- Stessa connessione della procedura
      this.w_CONNECT = i_TableProp[this.INF_GRUD_idx,3]
    endif
    if this.w_CONNECT <= 0
      AH_ERRORMSG("Connessione fallita",48)
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona / Deseleziona le aziende
    * --- Selezione o deselezione di tutti i programmi visualizzati nella maschera.
    this.w_ZOOM = this.oParentObject.w_AZIENDE
    MM = this.w_ZOOM.cCursor
    if this.oParentObject.w_SELEZ = "S"
      UPDATE (MM) SET XCHK = 1
    else
      if Type("g_UserScheduler")<>"L" or not g_UserScheduler
        UPDATE (MM) SET XCHK = 0
      endif
    endif
    SELECT(this.w_ZOOM.cCursor)
    GO TOP
    this.w_ZOOM.refresh()
  endproc


  proc Init(oParentObject,pOPERAZ)
    this.pOPERAZ=pOPERAZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='INF_GRUD'
    this.cWorkTables[2]='INF_REGD'
    this.cWorkTables[3]='INF_TAAZ'
    this.cWorkTables[4]='INF_TAB'
    this.cWorkTables[5]='INF_REGM'
    this.cWorkTables[6]='AZIENDA'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_AZIENDA')
      use in _Curs_AZIENDA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPERAZ"
endproc
