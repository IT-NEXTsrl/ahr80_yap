* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bgr                                                        *
*              Carica gruppo di regole (InfoLink)                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-30                                                      *
* Last revis.: 2005-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOOPGR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bgr",oParentObject,m.pTIPOOPGR)
return(i_retval)

define class tgsin_bgr as StdBatch
  * --- Local variables
  pTIPOOPGR = space(1)
  w_MESS = space(150)
  * --- WorkFile variables
  INF_GRUD_idx=0
  INF_GRUM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_03457DA8
    bErr_03457DA8=bTrsErr
    this.Try_03457DA8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_ERRORGRU = "T"
    endif
    bTrsErr=bTrsErr or bErr_03457DA8
    * --- End
  endproc
  proc Try_03457DA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.pTIPOOPGR="D"
        * --- Try
        local bErr_0345BF48
        bErr_0345BF48=bTrsErr
        this.Try_0345BF48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into INF_GRUD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.INF_GRUD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INF_GRUD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_GRUD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"GRNUMSEQ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRNUMSEQ),'INF_GRUD','GRNUMSEQ');
            +",GRCODREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRCODREG),'INF_GRUD','GRCODREG');
            +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_GRUD','CPROWORD');
                +i_ccchkf ;
            +" where ";
                +"GRCODICE = "+cp_ToStrODBC(this.oParentObject.w_GRCODICE);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                GRNUMSEQ = this.oParentObject.w_GRNUMSEQ;
                ,GRCODREG = this.oParentObject.w_GRCODREG;
                ,CPROWORD = this.oParentObject.w_CPROWORD;
                &i_ccchkf. ;
             where;
                GRCODICE = this.oParentObject.w_GRCODICE;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_0345BF48
        * --- End
      case this.pTIPOOPGR="M"
        * --- Try
        local bErr_04C03F78
        bErr_04C03F78=bTrsErr
        this.Try_04C03F78()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into INF_GRUM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.INF_GRUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INF_GRUM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_GRUM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"GRDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRDESCRI),'INF_GRUM','GRDESCRI');
            +",GRDTINVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRDTINVA),'INF_GRUM','GRDTINVA');
            +",GRDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRDTOBSO),'INF_GRUM','GRDTOBSO');
                +i_ccchkf ;
            +" where ";
                +"GRCODICE = "+cp_ToStrODBC(this.oParentObject.w_GRCODICE);
                   )
          else
            update (i_cTable) set;
                GRDESCRI = this.oParentObject.w_GRDESCRI;
                ,GRDTINVA = this.oParentObject.w_GRDTINVA;
                ,GRDTOBSO = this.oParentObject.w_GRDTOBSO;
                &i_ccchkf. ;
             where;
                GRCODICE = this.oParentObject.w_GRCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04C03F78
        * --- End
        * --- Inserisco la prima riga di dettaglio
        * --- Try
        local bErr_0345DD48
        bErr_0345DD48=bTrsErr
        this.Try_0345DD48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into INF_GRUD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.INF_GRUD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INF_GRUD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_GRUD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"GRNUMSEQ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRNUMSEQ),'INF_GRUD','GRNUMSEQ');
            +",GRCODREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRCODREG),'INF_GRUD','GRCODREG');
            +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_GRUD','CPROWORD');
                +i_ccchkf ;
            +" where ";
                +"GRCODICE = "+cp_ToStrODBC(this.oParentObject.w_GRCODICE);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                GRNUMSEQ = this.oParentObject.w_GRNUMSEQ;
                ,GRCODREG = this.oParentObject.w_GRCODREG;
                ,CPROWORD = this.oParentObject.w_CPROWORD;
                &i_ccchkf. ;
             where;
                GRCODICE = this.oParentObject.w_GRCODICE;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_0345DD48
        * --- End
    endcase
    return
  proc Try_0345BF48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_GRUD
    i_nConn=i_TableProp[this.INF_GRUD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_GRUD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_GRUD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GRCODICE"+",CPROWNUM"+",GRNUMSEQ"+",GRCODREG"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRCODICE),'INF_GRUD','GRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'INF_GRUD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRNUMSEQ),'INF_GRUD','GRNUMSEQ');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRCODREG),'INF_GRUD','GRCODREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_GRUD','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GRCODICE',this.oParentObject.w_GRCODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'GRNUMSEQ',this.oParentObject.w_GRNUMSEQ,'GRCODREG',this.oParentObject.w_GRCODREG,'CPROWORD',this.oParentObject.w_CPROWORD)
      insert into (i_cTable) (GRCODICE,CPROWNUM,GRNUMSEQ,GRCODREG,CPROWORD &i_ccchkf. );
         values (;
           this.oParentObject.w_GRCODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_GRNUMSEQ;
           ,this.oParentObject.w_GRCODREG;
           ,this.oParentObject.w_CPROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04C03F78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserisco la testata
    * --- Insert into INF_GRUM
    i_nConn=i_TableProp[this.INF_GRUM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_GRUM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_GRUM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GRCODICE"+",GRDESCRI"+",GRDTINVA"+",GRDTOBSO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRCODICE),'INF_GRUM','GRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRDESCRI),'INF_GRUM','GRDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRDTINVA),'INF_GRUM','GRDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRDTOBSO),'INF_GRUM','GRDTOBSO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GRCODICE',this.oParentObject.w_GRCODICE,'GRDESCRI',this.oParentObject.w_GRDESCRI,'GRDTINVA',this.oParentObject.w_GRDTINVA,'GRDTOBSO',this.oParentObject.w_GRDTOBSO)
      insert into (i_cTable) (GRCODICE,GRDESCRI,GRDTINVA,GRDTOBSO &i_ccchkf. );
         values (;
           this.oParentObject.w_GRCODICE;
           ,this.oParentObject.w_GRDESCRI;
           ,this.oParentObject.w_GRDTINVA;
           ,this.oParentObject.w_GRDTOBSO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0345DD48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from INF_GRUD
    i_nConn=i_TableProp[this.INF_GRUD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_GRUD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GRCODICE = "+cp_ToStrODBC(this.oParentObject.w_GRCODICE);
             )
    else
      delete from (i_cTable) where;
            GRCODICE = this.oParentObject.w_GRCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into INF_GRUD
    i_nConn=i_TableProp[this.INF_GRUD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_GRUD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_GRUD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GRCODICE"+",CPROWNUM"+",GRNUMSEQ"+",GRCODREG"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRCODICE),'INF_GRUD','GRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'INF_GRUD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRNUMSEQ),'INF_GRUD','GRNUMSEQ');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GRCODREG),'INF_GRUD','GRCODREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_GRUD','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GRCODICE',this.oParentObject.w_GRCODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'GRNUMSEQ',this.oParentObject.w_GRNUMSEQ,'GRCODREG',this.oParentObject.w_GRCODREG,'CPROWORD',this.oParentObject.w_CPROWORD)
      insert into (i_cTable) (GRCODICE,CPROWNUM,GRNUMSEQ,GRCODREG,CPROWORD &i_ccchkf. );
         values (;
           this.oParentObject.w_GRCODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_GRNUMSEQ;
           ,this.oParentObject.w_GRCODREG;
           ,this.oParentObject.w_CPROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pTIPOOPGR)
    this.pTIPOOPGR=pTIPOOPGR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='INF_GRUD'
    this.cWorkTables[2]='INF_GRUM'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOOPGR"
endproc
