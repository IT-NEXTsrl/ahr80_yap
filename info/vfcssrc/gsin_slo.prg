* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_slo                                                        *
*              Stampa log esportazioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_61]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-03                                                      *
* Last revis.: 2013-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsin_slo",oParentObject))

* --- Class definition
define class tgsin_slo as StdForm
  Top    = 23
  Left   = 54

  * --- Standard Properties
  Width  = 541
  Height = 260
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-01-18"
  HelpContextID=60182679
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  INF_GRUD_IDX = 0
  INF_GRUM_IDX = 0
  INF_LOG_IDX = 0
  CPUSERS_IDX = 0
  cPrg = "gsin_slo"
  cComment = "Stampa log esportazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LODGRUPP1 = space(15)
  w_LODGRUPP2 = space(15)
  w_LOCODUTE1 = 0
  w_LOCODUTE2 = 0
  w_LODATAES1 = ctod('  /  /  ')
  w_LODATAES2 = ctod('  /  /  ')
  w_LOORAESE1 = space(8)
  w_LOORAESE2 = space(8)
  w_DESCGRU1 = space(40)
  w_DESCGRU2 = space(40)
  w_DESCUTE1 = space(40)
  w_DESCUTE2 = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsin_sloPag1","gsin_slo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLODGRUPP1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='INF_GRUD'
    this.cWorkTables[2]='INF_GRUM'
    this.cWorkTables[3]='INF_LOG'
    this.cWorkTables[4]='CPUSERS'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LODGRUPP1=space(15)
      .w_LODGRUPP2=space(15)
      .w_LOCODUTE1=0
      .w_LOCODUTE2=0
      .w_LODATAES1=ctod("  /  /  ")
      .w_LODATAES2=ctod("  /  /  ")
      .w_LOORAESE1=space(8)
      .w_LOORAESE2=space(8)
      .w_DESCGRU1=space(40)
      .w_DESCGRU2=space(40)
      .w_DESCUTE1=space(40)
      .w_DESCUTE2=space(40)
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_LODGRUPP1))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_LODGRUPP2))
          .link_1_4('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_LOCODUTE1))
          .link_1_6('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_LOCODUTE2))
          .link_1_8('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_LODATAES1))
          .link_1_10('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_LODATAES2))
          .link_1_12('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_LOORAESE1))
          .link_1_14('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_LOORAESE2))
          .link_1_16('Full')
        endif
    endwith
    this.DoRTCalc(9,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LODGRUPP1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_GRUM_IDX,3]
    i_lTable = "INF_GRUM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2], .t., this.INF_GRUM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LODGRUPP1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsin_mgr',True,'INF_GRUM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_LODGRUPP1)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_LODGRUPP1))
          select GRCODICE,GRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LODGRUPP1)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LODGRUPP1) and !this.bDontReportError
            deferred_cp_zoom('INF_GRUM','*','GRCODICE',cp_AbsName(oSource.parent,'oLODGRUPP1_1_2'),i_cWhere,'gsin_mgr',"Gruppi di regole",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LODGRUPP1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_LODGRUPP1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_LODGRUPP1)
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LODGRUPP1 = NVL(_Link_.GRCODICE,space(15))
      this.w_DESCGRU1 = NVL(_Link_.GRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LODGRUPP1 = space(15)
      endif
      this.w_DESCGRU1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LODGRUPP2) or (.w_LODGRUPP2>=.w_LODGRUPP1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
        endif
        this.w_LODGRUPP1 = space(15)
        this.w_DESCGRU1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.INF_GRUM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LODGRUPP1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LODGRUPP2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_GRUM_IDX,3]
    i_lTable = "INF_GRUM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2], .t., this.INF_GRUM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LODGRUPP2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsin_mgr',True,'INF_GRUM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_LODGRUPP2)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_LODGRUPP2))
          select GRCODICE,GRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LODGRUPP2)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LODGRUPP2) and !this.bDontReportError
            deferred_cp_zoom('INF_GRUM','*','GRCODICE',cp_AbsName(oSource.parent,'oLODGRUPP2_1_4'),i_cWhere,'gsin_mgr',"Gruppi di regole",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LODGRUPP2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_LODGRUPP2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_LODGRUPP2)
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LODGRUPP2 = NVL(_Link_.GRCODICE,space(15))
      this.w_DESCGRU2 = NVL(_Link_.GRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LODGRUPP2 = space(15)
      endif
      this.w_DESCGRU2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LODGRUPP1) or (.w_LODGRUPP2>=.w_LODGRUPP1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
        endif
        this.w_LODGRUPP2 = space(15)
        this.w_DESCGRU2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_GRUM_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.INF_GRUM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LODGRUPP2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOCODUTE1
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOCODUTE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_LOCODUTE1);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_LOCODUTE1)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LOCODUTE1) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oLOCODUTE1_1_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOCODUTE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_LOCODUTE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_LOCODUTE1)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOCODUTE1 = NVL(_Link_.CODE,0)
      this.w_DESCUTE1 = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LOCODUTE1 = 0
      endif
      this.w_DESCUTE1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LOCODUTE2) or (.w_LOCODUTE2>=.w_LOCODUTE1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
        endif
        this.w_LOCODUTE1 = 0
        this.w_DESCUTE1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOCODUTE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOCODUTE2
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOCODUTE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_LOCODUTE2);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_LOCODUTE2)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LOCODUTE2) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oLOCODUTE2_1_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOCODUTE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_LOCODUTE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_LOCODUTE2)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOCODUTE2 = NVL(_Link_.CODE,0)
      this.w_DESCUTE2 = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LOCODUTE2 = 0
      endif
      this.w_DESCUTE2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LOCODUTE1) or (.w_LOCODUTE2>=.w_LOCODUTE1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
        endif
        this.w_LOCODUTE2 = 0
        this.w_DESCUTE2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOCODUTE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LODATAES1
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_LOG_IDX,3]
    i_lTable = "INF_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2], .t., this.INF_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LODATAES1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIN_ALO',True,'INF_LOG')
        if i_nConn<>0
          i_cWhere = " LODATAES="+cp_ToStrODBC(this.w_LODATAES1);

          i_ret=cp_SQL(i_nConn,"select LODATAES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LODATAES',this.w_LODATAES1)
          select LODATAES;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LODATAES1) and !this.bDontReportError
            deferred_cp_zoom('INF_LOG','*','LODATAES',cp_AbsName(oSource.parent,'oLODATAES1_1_10'),i_cWhere,'GSIN_ALO',"Alo importazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LODATAES";
                     +" from "+i_cTable+" "+i_lTable+" where LODATAES="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LODATAES',oSource.xKey(1))
            select LODATAES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LODATAES1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LODATAES";
                   +" from "+i_cTable+" "+i_lTable+" where LODATAES="+cp_ToStrODBC(this.w_LODATAES1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LODATAES',this.w_LODATAES1)
            select LODATAES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LODATAES1 = NVL(cp_todate(_Link_.LODATAES),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_LODATAES1 = ctod("  /  /  ")
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LODATAES2) or (.w_LODATAES2>=.w_LODATAES1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
        endif
        this.w_LODATAES1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])+'\'+cp_ToStr(_Link_.LODATAES,1)
      cp_ShowWarn(i_cKey,this.INF_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LODATAES1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LODATAES2
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_LOG_IDX,3]
    i_lTable = "INF_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2], .t., this.INF_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LODATAES2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIN_ALO',True,'INF_LOG')
        if i_nConn<>0
          i_cWhere = " LODATAES="+cp_ToStrODBC(this.w_LODATAES2);

          i_ret=cp_SQL(i_nConn,"select LODATAES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LODATAES',this.w_LODATAES2)
          select LODATAES;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LODATAES2) and !this.bDontReportError
            deferred_cp_zoom('INF_LOG','*','LODATAES',cp_AbsName(oSource.parent,'oLODATAES2_1_12'),i_cWhere,'GSIN_ALO',"Alo importazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LODATAES";
                     +" from "+i_cTable+" "+i_lTable+" where LODATAES="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LODATAES',oSource.xKey(1))
            select LODATAES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LODATAES2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LODATAES";
                   +" from "+i_cTable+" "+i_lTable+" where LODATAES="+cp_ToStrODBC(this.w_LODATAES2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LODATAES',this.w_LODATAES2)
            select LODATAES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LODATAES2 = NVL(cp_todate(_Link_.LODATAES),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_LODATAES2 = ctod("  /  /  ")
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LODATAES1) or (.w_LODATAES2>=.w_LODATAES1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
        endif
        this.w_LODATAES2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])+'\'+cp_ToStr(_Link_.LODATAES,1)
      cp_ShowWarn(i_cKey,this.INF_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LODATAES2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOORAESE1
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_LOG_IDX,3]
    i_lTable = "INF_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2], .t., this.INF_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOORAESE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIN_ALO',True,'INF_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOORAESE like "+cp_ToStrODBC(trim(this.w_LOORAESE1)+"%");

          i_ret=cp_SQL(i_nConn,"select LOORAESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOORAESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOORAESE',trim(this.w_LOORAESE1))
          select LOORAESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOORAESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOORAESE1)==trim(_Link_.LOORAESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOORAESE1) and !this.bDontReportError
            deferred_cp_zoom('INF_LOG','*','LOORAESE',cp_AbsName(oSource.parent,'oLOORAESE1_1_14'),i_cWhere,'GSIN_ALO',"Alo importazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOORAESE";
                     +" from "+i_cTable+" "+i_lTable+" where LOORAESE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOORAESE',oSource.xKey(1))
            select LOORAESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOORAESE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOORAESE";
                   +" from "+i_cTable+" "+i_lTable+" where LOORAESE="+cp_ToStrODBC(this.w_LOORAESE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOORAESE',this.w_LOORAESE1)
            select LOORAESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOORAESE1 = NVL(_Link_.LOORAESE,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_LOORAESE1 = space(8)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LOORAESE2) or (.w_LOORAESE2>=.w_LOORAESE1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
        endif
        this.w_LOORAESE1 = space(8)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])+'\'+cp_ToStr(_Link_.LOORAESE,1)
      cp_ShowWarn(i_cKey,this.INF_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOORAESE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOORAESE2
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_LOG_IDX,3]
    i_lTable = "INF_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2], .t., this.INF_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOORAESE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIN_ALO',True,'INF_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOORAESE like "+cp_ToStrODBC(trim(this.w_LOORAESE2)+"%");

          i_ret=cp_SQL(i_nConn,"select LOORAESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOORAESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOORAESE',trim(this.w_LOORAESE2))
          select LOORAESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOORAESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOORAESE2)==trim(_Link_.LOORAESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOORAESE2) and !this.bDontReportError
            deferred_cp_zoom('INF_LOG','*','LOORAESE',cp_AbsName(oSource.parent,'oLOORAESE2_1_16'),i_cWhere,'GSIN_ALO',"Alo importazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOORAESE";
                     +" from "+i_cTable+" "+i_lTable+" where LOORAESE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOORAESE',oSource.xKey(1))
            select LOORAESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOORAESE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOORAESE";
                   +" from "+i_cTable+" "+i_lTable+" where LOORAESE="+cp_ToStrODBC(this.w_LOORAESE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOORAESE',this.w_LOORAESE2)
            select LOORAESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOORAESE2 = NVL(_Link_.LOORAESE,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_LOORAESE2 = space(8)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_LOORAESE1) or (.w_LOORAESE2>=.w_LOORAESE1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
        endif
        this.w_LOORAESE2 = space(8)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_LOG_IDX,2])+'\'+cp_ToStr(_Link_.LOORAESE,1)
      cp_ShowWarn(i_cKey,this.INF_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOORAESE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLODGRUPP1_1_2.value==this.w_LODGRUPP1)
      this.oPgFrm.Page1.oPag.oLODGRUPP1_1_2.value=this.w_LODGRUPP1
    endif
    if not(this.oPgFrm.Page1.oPag.oLODGRUPP2_1_4.value==this.w_LODGRUPP2)
      this.oPgFrm.Page1.oPag.oLODGRUPP2_1_4.value=this.w_LODGRUPP2
    endif
    if not(this.oPgFrm.Page1.oPag.oLOCODUTE1_1_6.value==this.w_LOCODUTE1)
      this.oPgFrm.Page1.oPag.oLOCODUTE1_1_6.value=this.w_LOCODUTE1
    endif
    if not(this.oPgFrm.Page1.oPag.oLOCODUTE2_1_8.value==this.w_LOCODUTE2)
      this.oPgFrm.Page1.oPag.oLOCODUTE2_1_8.value=this.w_LOCODUTE2
    endif
    if not(this.oPgFrm.Page1.oPag.oLODATAES1_1_10.value==this.w_LODATAES1)
      this.oPgFrm.Page1.oPag.oLODATAES1_1_10.value=this.w_LODATAES1
    endif
    if not(this.oPgFrm.Page1.oPag.oLODATAES2_1_12.value==this.w_LODATAES2)
      this.oPgFrm.Page1.oPag.oLODATAES2_1_12.value=this.w_LODATAES2
    endif
    if not(this.oPgFrm.Page1.oPag.oLOORAESE1_1_14.value==this.w_LOORAESE1)
      this.oPgFrm.Page1.oPag.oLOORAESE1_1_14.value=this.w_LOORAESE1
    endif
    if not(this.oPgFrm.Page1.oPag.oLOORAESE2_1_16.value==this.w_LOORAESE2)
      this.oPgFrm.Page1.oPag.oLOORAESE2_1_16.value=this.w_LOORAESE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGRU1_1_21.value==this.w_DESCGRU1)
      this.oPgFrm.Page1.oPag.oDESCGRU1_1_21.value=this.w_DESCGRU1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGRU2_1_22.value==this.w_DESCGRU2)
      this.oPgFrm.Page1.oPag.oDESCGRU2_1_22.value=this.w_DESCGRU2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCUTE1_1_23.value==this.w_DESCUTE1)
      this.oPgFrm.Page1.oPag.oDESCUTE1_1_23.value=this.w_DESCUTE1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCUTE2_1_24.value==this.w_DESCUTE2)
      this.oPgFrm.Page1.oPag.oDESCUTE2_1_24.value=this.w_DESCUTE2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_LODGRUPP2) or (.w_LODGRUPP2>=.w_LODGRUPP1))  and not(empty(.w_LODGRUPP1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLODGRUPP1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
          case   not(empty(.w_LODGRUPP1) or (.w_LODGRUPP2>=.w_LODGRUPP1))  and not(empty(.w_LODGRUPP2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLODGRUPP2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
          case   not(empty(.w_LOCODUTE2) or (.w_LOCODUTE2>=.w_LOCODUTE1))  and not(empty(.w_LOCODUTE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOCODUTE1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
          case   not(empty(.w_LOCODUTE1) or (.w_LOCODUTE2>=.w_LOCODUTE1))  and not(empty(.w_LOCODUTE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOCODUTE2_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
          case   not(empty(.w_LODATAES2) or (.w_LODATAES2>=.w_LODATAES1))  and not(empty(.w_LODATAES1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLODATAES1_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
          case   not(empty(.w_LODATAES1) or (.w_LODATAES2>=.w_LODATAES1))  and not(empty(.w_LODATAES2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLODATAES2_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
          case   not(empty(.w_LOORAESE2) or (.w_LOORAESE2>=.w_LOORAESE1))  and not(empty(.w_LOORAESE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOORAESE1_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore del codice iniziale oppure inesistente")
          case   not(empty(.w_LOORAESE1) or (.w_LOORAESE2>=.w_LOORAESE1))  and not(empty(.w_LOORAESE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOORAESE2_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure inesistente.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsin_sloPag1 as StdContainer
  Width  = 537
  height = 260
  stdWidth  = 537
  stdheight = 260
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_outputCombo with uid="ZLERXIVPWT",left=147, top=180, width=384,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 30255130

  add object oLODGRUPP1_1_2 as StdField with uid="CPVOANEXXO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LODGRUPP1", cQueryName = "LODGRUPP1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore del codice iniziale oppure inesistente",;
    ToolTipText = "Codice gruppo a inizio selezione",;
    HelpContextID = 33429482,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=147, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="INF_GRUM", cZoomOnZoom="gsin_mgr", oKey_1_1="GRCODICE", oKey_1_2="this.w_LODGRUPP1"

  func oLODGRUPP1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oLODGRUPP1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLODGRUPP1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_GRUM','*','GRCODICE',cp_AbsName(this.parent,'oLODGRUPP1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsin_mgr',"Gruppi di regole",'',this.parent.oContained
  endproc
  proc oLODGRUPP1_1_2.mZoomOnZoom
    local i_obj
    i_obj=gsin_mgr()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GRCODICE=this.parent.oContained.w_LODGRUPP1
     i_obj.ecpSave()
  endproc

  add object oLODGRUPP2_1_4 as StdField with uid="ZNJLKOUPQB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LODGRUPP2", cQueryName = "LODGRUPP2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure inesistente.",;
    ToolTipText = "Codice gruppo da fine selezione",;
    HelpContextID = 33429466,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=147, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="INF_GRUM", cZoomOnZoom="gsin_mgr", oKey_1_1="GRCODICE", oKey_1_2="this.w_LODGRUPP2"

  func oLODGRUPP2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oLODGRUPP2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLODGRUPP2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_GRUM','*','GRCODICE',cp_AbsName(this.parent,'oLODGRUPP2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'gsin_mgr',"Gruppi di regole",'',this.parent.oContained
  endproc
  proc oLODGRUPP2_1_4.mZoomOnZoom
    local i_obj
    i_obj=gsin_mgr()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GRCODICE=this.parent.oContained.w_LODGRUPP2
     i_obj.ecpSave()
  endproc

  add object oLOCODUTE1_1_6 as StdField with uid="NZBEFVEBUJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LOCODUTE1", cQueryName = "LOCODUTE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore del codice iniziale oppure inesistente",;
    ToolTipText = "Codice utente a inizio selezione",;
    HelpContextID = 47589365,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=147, Top=66, bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_LOCODUTE1"

  func oLOCODUTE1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOCODUTE1_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOCODUTE1_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oLOCODUTE1_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oLOCODUTE2_1_8 as StdField with uid="YLDBKNLEDT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LOCODUTE2", cQueryName = "LOCODUTE2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure inesistente.",;
    ToolTipText = "Codice utente da fine selezione",;
    HelpContextID = 47589349,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=147, Top=91, bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_LOCODUTE2"

  func oLOCODUTE2_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOCODUTE2_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOCODUTE2_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oLOCODUTE2_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oLODATAES1_1_10 as StdField with uid="YGPCYUVHHR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LODATAES1", cQueryName = "LODATAES1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore del codice iniziale oppure inesistente",;
    ToolTipText = "Data di esecuzione del gruppo regole a inizio selezione",;
    HelpContextID = 98834407,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=147, Top=118, bHasZoom = .t. , cLinkFile="INF_LOG", cZoomOnZoom="GSIN_ALO", oKey_1_1="LODATAES", oKey_1_2="this.w_LODATAES1"

  func oLODATAES1_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oLODATAES1_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLODATAES1_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_LOG','*','LODATAES',cp_AbsName(this.parent,'oLODATAES1_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIN_ALO',"Alo importazioni",'',this.parent.oContained
  endproc
  proc oLODATAES1_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSIN_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LODATAES=this.parent.oContained.w_LODATAES1
     i_obj.ecpSave()
  endproc

  add object oLODATAES2_1_12 as StdField with uid="LGKIRLHVCR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LODATAES2", cQueryName = "LODATAES2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure inesistente.",;
    ToolTipText = "Data di esecuzione del gruppo regole da fine selezione",;
    HelpContextID = 98834391,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=147, Top=143, bHasZoom = .t. , cLinkFile="INF_LOG", cZoomOnZoom="GSIN_ALO", oKey_1_1="LODATAES", oKey_1_2="this.w_LODATAES2"

  func oLODATAES2_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oLODATAES2_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLODATAES2_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_LOG','*','LODATAES',cp_AbsName(this.parent,'oLODATAES2_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIN_ALO',"Alo importazioni",'',this.parent.oContained
  endproc
  proc oLODATAES2_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSIN_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LODATAES=this.parent.oContained.w_LODATAES2
     i_obj.ecpSave()
  endproc

  add object oLOORAESE1_1_14 as StdField with uid="YXXPNIJKWC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LOORAESE1", cQueryName = "LOORAESE1",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore del codice iniziale oppure inesistente",;
    ToolTipText = "Ora di esecuzione del gruppo regole a inizio selezione",;
    HelpContextID = 50489333,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=420, Top=118, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="INF_LOG", cZoomOnZoom="GSIN_ALO", oKey_1_1="LOORAESE", oKey_1_2="this.w_LOORAESE1"

  func oLOORAESE1_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOORAESE1_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOORAESE1_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_LOG','*','LOORAESE',cp_AbsName(this.parent,'oLOORAESE1_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIN_ALO',"Alo importazioni",'',this.parent.oContained
  endproc
  proc oLOORAESE1_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSIN_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LOORAESE=this.parent.oContained.w_LOORAESE1
     i_obj.ecpSave()
  endproc

  add object oLOORAESE2_1_16 as StdField with uid="NKPMKCCRYF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LOORAESE2", cQueryName = "LOORAESE2",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure inesistente.",;
    ToolTipText = "Ora di esecuzione del gruppo regole da fine selezione",;
    HelpContextID = 50489317,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=420, Top=143, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="INF_LOG", cZoomOnZoom="GSIN_ALO", oKey_1_1="LOORAESE", oKey_1_2="this.w_LOORAESE2"

  func oLOORAESE2_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOORAESE2_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOORAESE2_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_LOG','*','LOORAESE',cp_AbsName(this.parent,'oLOORAESE2_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIN_ALO',"Alo importazioni",'',this.parent.oContained
  endproc
  proc oLOORAESE2_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSIN_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LOORAESE=this.parent.oContained.w_LOORAESE2
     i_obj.ecpSave()
  endproc


  add object oBtn_1_18 as StdButton with uid="APIWEFNJEL",left=426, top=210, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 60211430;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)))
      endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="ZJIUAZYUNY",left=481, top=210, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 67500102;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCGRU1_1_21 as StdField with uid="DJAWOSWBAV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCGRU1", cQueryName = "DESCGRU1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 172935783,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=259, Top=14, InputMask=replicate('X',40)

  add object oDESCGRU2_1_22 as StdField with uid="QAVOUQSNAT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCGRU2", cQueryName = "DESCGRU2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 172935784,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=259, Top=39, InputMask=replicate('X',40)

  add object oDESCUTE1_1_23 as StdField with uid="STTDPLKGTS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCUTE1", cQueryName = "DESCUTE1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 221170279,;
   bGlobalFont=.t.,;
    Height=21, Width=322, Left=209, Top=66, InputMask=replicate('X',40)

  add object oDESCUTE2_1_24 as StdField with uid="RWQJYIWMKT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCUTE2", cQueryName = "DESCUTE2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 221170280,;
   bGlobalFont=.t.,;
    Height=21, Width=322, Left=209, Top=91, InputMask=replicate('X',40)

  add object oStr_1_3 as StdString with uid="DUDWIOZGTJ",Visible=.t., Left=5, Top=18,;
    Alignment=1, Width=136, Height=18,;
    Caption="Da codice gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="QUDTPLGWFU",Visible=.t., Left=5, Top=43,;
    Alignment=1, Width=136, Height=18,;
    Caption="A codice gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="XAYUQQAOFU",Visible=.t., Left=5, Top=70,;
    Alignment=1, Width=136, Height=18,;
    Caption="Da codice utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="VZYFHQKNLM",Visible=.t., Left=5, Top=95,;
    Alignment=1, Width=136, Height=18,;
    Caption="A codice utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="POSKNUKYZO",Visible=.t., Left=5, Top=122,;
    Alignment=1, Width=136, Height=18,;
    Caption="Da data di esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="WEWQXJQSDP",Visible=.t., Left=5, Top=147,;
    Alignment=1, Width=136, Height=18,;
    Caption="A data di esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="EUCZFBZZLV",Visible=.t., Left=262, Top=122,;
    Alignment=1, Width=154, Height=18,;
    Caption="Da ora di esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="QQIUVDOIBI",Visible=.t., Left=262, Top=147,;
    Alignment=1, Width=154, Height=18,;
    Caption="A ora di esecuzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="HCVGMFFTWH",Visible=.t., Left=5, Top=182,;
    Alignment=1, Width=136, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_slo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
