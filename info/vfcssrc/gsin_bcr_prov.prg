* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_bcr_prov                                                   *
*              Creazione- inserimento province                                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-15                                                      *
* Last revis.: 2005-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperaz,pTabella,pQUERY
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsin_bcr_prov",oParentObject,m.pOperaz,m.pTabella,m.pQUERY)
return(i_retval)

define class tgsin_bcr_prov as StdBatch
  * --- Local variables
  pOperaz = space(2)
  w_CMD = space(254)
  w_NUMINDX = 0
  w_CAMPINDX = space(150)
  w_INDEXCRE = .f.
  pTabella = space(50)
  pQUERY = space(20)
  w_VarError = space(255)
  w_ODBC = space(25)
  w_STRINGA = space(90)
  w_CRIPTATA = space(1)
  w_CONNECT = 0
  w_CONNECTION = space(25)
  w_FIELDS = space(255)
  w_FIELDS2 = space(255)
  w_VALORI = space(255)
  w_WHERE = space(255)
  w_WHERE2 = space(255)
  w_DB = space(10)
  w_MULTIAZI = space(1)
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_CAMPICON = space(150)
  w_CAMPICON2 = space(254)
  w_NOMECAMP = space(50)
  w_CONCATOK = .f.
  w_INSERIMENTO = .f.
  w_TOTFIELD = 0
  * --- WorkFile variables
  INF_GRUD_idx=0
  INF_TAB_idx=0
  INF_INDI_idx=0
  INF_TAAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Messaggio=""
    this.w_DATAINI = this.oParentObject.oParentObject.w_DATAINI
    this.w_DATAFIN = this.oParentObject.oParentObject.w_DATAFIN
    this.oParentObject.w_RAGSOC = alltrim(this.oParentObject.w_RAGSOC)
    * --- Inizializzazione variabili gestione campi di concatenazione in tabelle multiazienda
    this.w_CAMPICON2 = ""
    this.w_NOMECAMP = ""
    this.w_CONCATOK = .T.
    * --- Try
    local bErr_04C27BF8
    bErr_04C27BF8=bTrsErr
    this.Try_04C27BF8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04C27BF8
    * --- End
  endproc
  proc Try_04C27BF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifico il nome della tabella nel nostro standard
    this.pTabella = alltrim(this.pTabella)+"_DW"
    * --- Acquisisco il nome dell'ODBC o la stringa di connessione relativo alla tabella passata come parametro (pTabella)
    * --- Prima controllo se esiste una connessione di "default" per la tabella inerente  all'azienda in uso
    * --- Read from INF_TAAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TACOODBC,ATCOSTRI,ATCRIPTE"+;
        " from "+i_cTable+" INF_TAAZ where ";
            +"TACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TACOODBC,ATCOSTRI,ATCRIPTE;
        from (i_cTable) where;
            TACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
      this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
      this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo nell'archivio Tabelle / Connessioni 
      * --- Read from INF_TAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAB where ";
              +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              ATNTABLE = this.pTabella;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.ATCOODBC),cp_NullValue(_read_.ATCOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if empty(this.w_ODBC) and empty(this.w_STRINGA)
      * --- Controllo se esiste per la tabella una connessione globale (cio� indipendente dall'azienda)
      * --- Read from INF_TAAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAAZ_idx,2],.t.,this.INF_TAAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TACOODBC,ATCOSTRI,ATCRIPTE"+;
          " from "+i_cTable+" INF_TAAZ where ";
              +"TACODAZI = "+cp_ToStrODBC(space(5));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TACOODBC,ATCOSTRI,ATCRIPTE;
          from (i_cTable) where;
              TACODAZI = space(5);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ODBC = NVL(cp_ToDate(_read_.TACOODBC),cp_NullValue(_read_.TACOODBC))
        this.w_STRINGA = NVL(cp_ToDate(_read_.ATCOSTRI),cp_NullValue(_read_.ATCOSTRI))
        this.w_CRIPTATA = NVL(cp_ToDate(_read_.ATCRIPTE),cp_NullValue(_read_.ATCRIPTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Database  diverso da quello usato dalla procedura.
      *     Creo una nuova connessione
      if this.w_CRIPTATA = "C"
        this.w_STRINGA = CifraCnf( ALLTRIM(this.w_STRINGA) , "D" )
      endif
      this.w_CONNECTION = alltrim(this.w_ODBC + this.w_STRINGA)
      this.w_CONNECT = cp_openDatabaseConnection(this.w_connection)
      this.w_DB = SQLXGetDatabaseType(this.w_CONNECT)
    else
      * --- Stessa connessione della procedura
      this.w_CONNECT = i_TableProp[this.INF_GRUD_idx,3]
      this.w_DB = SQLXGetDatabaseType(this.w_CONNECT)
    endif
    if this.w_CONNECT <= 0
      ah_ERRORMSG("Connessione fallita")
      this.oParentObject.w_errore = "%1"
      this.oParentObject.w_PARMSG1 = alltrim(message())
      i_retcode = 'stop'
      return
    else
      ah_msg( "Connessione riuscita..." )
    endif
    this.oParentObject.w_RENOMCUR = alltrim(this.oParentObject.w_RENOMCUR)
    * --- Verifico se la tabella � multi aziendale o no
    * --- Read from INF_TAB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_TAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATMULTAZ"+;
        " from "+i_cTable+" INF_TAB where ";
            +"ATNTABLE = "+cp_ToStrODBC(this.pTABELLA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATMULTAZ;
        from (i_cTable) where;
            ATNTABLE = this.pTABELLA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MULTIAZI = NVL(cp_ToDate(_read_.ATMULTAZ),cp_NullValue(_read_.ATMULTAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se tabella multiazienda leggo i campi da concatenare, e aggiungo il carattere 'Z' ad
    *     ognuno di essi
    if this.w_MULTIAZI = "S"
      * --- Read from INF_TAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_TAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_TAB_idx,2],.t.,this.INF_TAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCAMCON"+;
          " from "+i_cTable+" INF_TAB where ";
              +"ATNTABLE = "+cp_ToStrODBC(this.pTabella);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCAMCON;
          from (i_cTable) where;
              ATNTABLE = this.pTabella;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAMPICON = NVL(cp_ToDate(_read_.ATCAMCON),cp_NullValue(_read_.ATCAMCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      i=1
      do while ! empty(alltrim(this.w_CAMPICON))
        dimension FIELDLIST (i)
        if "," $ this.w_CAMPICON
          this.w_NOMECAMP = substr(this.w_CAMPICON,1,at(",",this.w_CAMPICON)-1)+"Z"
          this.w_CAMPICON = substr(this.w_CAMPICON,at(",",this.w_CAMPICON)+1)
        else
          this.w_NOMECAMP = alltrim(this.w_CAMPICON)+"Z"
          this.w_CAMPICON = ""
        endif
        this.w_CAMPICON2 = this.w_CAMPICON2+this.w_NOMECAMP+","
        store alltrim(this.w_NOMECAMP) to FIELDLIST(i)
        i=i+1
      enddo
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_04C3F0F8
    bErr_04C3F0F8=bTrsErr
    this.Try_04C3F0F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      i_Error=MSG_INSERT_ERROR 
      this.oParentObject.w_errore = "Errore inserimento dati: %0%1"
      this.oParentObject.w_PARMSG1 = alltrim(message())
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_04C3F0F8
    * --- End
    if ! empty(this.w_ODBC) or ! empty(this.w_STRINGA)
      * --- Mi disconetto dalla nuova connessione.
      Sqldisconnect(this.w_CONNECT)
    endif
    if used("CursTab")
      Select CursTab 
 use
    endif
    if used("RISULTATO")
      Select RISULTATO 
 use
    endif
    if used(this.oParentObject.w_RENOMCUR)
      Select select(this.oParentObject.w_RENOMCUR) 
 use
    endif
    return
  proc Try_04C3F0F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_FIELDS = ""
    Declare STRUCTAB (1,5)
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select (this.oParentObject.w_RENOMCUR) 
 Go Top 
 Scan
    this.w_VALORI = ""
    for i=1 to fcount() 
 w_campo=field(i) 
 valore=&w_campo 
 if type("valore") $ "CM" 
 valore = strtran(valore,'"',space(1)) 
 endif
    this.w_VALORI = this.w_VALORI+cp_ToStrODBC(VALORE)+", "
    endfor
    if this.w_MULTIAZI="S"
      this.w_VALORI = this.w_VALORI+cp_ToStrODBC(space(5))+", "
      this.w_VALORI = this.w_VALORI+cp_ToStrODBC("")+", "
      if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
        for i=1 to alen(FIELDLIST) 
 w_campo=substr(fieldlist(i),1,len(fieldlist(i))-1) 
 valore=&w_campo 
 if type("valore") $ "CM" 
 valore = strtran(valore,'"',space(1)) 
 endif
        this.w_VALORI = this.w_VALORI+strtran(cp_ToStrODBC(VALORE)+cp_ToStrODBC(space(5)),"''","")+", "
        next
      endif
    endif
    this.w_INSERIMENTO = .t.
    righe=SqlExec(this.w_connect,"select count(*) as NUMREC from "+this.pTabella+" where CPCODPRO="+cp_ToStrODBC(CPCODPRO)+" ","CNTPROVINCE")
    if USED("CNTPROVINCE")
      if NUMREC>0
        this.w_INSERIMENTO = .f.
      endif
      Select CNTPROVINCE 
 Use
    endif
    Select select(this.oParentObject.w_RENOMCUR)
    if this.w_INSERIMENTO
      this.w_VALORI = LEFT(this.w_VALORI,LEN(this.w_VALORI)-2)
      ah_msg( "Inserimento record n� %1",.t.,.f.,.f.,str(recno()) )
      righe=SqlExec(this.w_connect,"Insert into "+this.pTabella+" ("+this.w_FIELDS+") values "+"("+this.w_VALORI+")")
      if righe<0 or bTrsErr
        * --- Raise
        i_Error=AH_MsgFormat("Errore inserimento dati")
        return
      endif
    endif
    EndScan
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_MSG = AH_MsgFormat("Inserimento completato con successo")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione Tabella 
    * --- Try
    local bErr_03457DA8
    bErr_03457DA8=bTrsErr
    this.Try_03457DA8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if Messaggio="Errata"
        Messaggio=""
        this.oParentObject.w_PARMSG1 = alltrim(this.pTabella)
        this.oParentObject.w_PARMSG2 = ALLTRIM(this.w_VarError)
        this.oParentObject.w_errore = "Errore nella creazione della tabella %1. %0Il campo %2 ha un tipo non riconosciuto"
      else
        if messaggio= "Query_vuota"
          Messaggio=""
          if alltrim(this.pTabella)<>"_DW"
            this.oParentObject.w_PARMSG1 = alltrim(this.pTabella)
            this.oParentObject.w_errore = "Errore nella creazione della tabella %1. %0Query vuota"
          else
            this.oParentObject.w_PARMSG1 = alltrim(this.pTabella)
            this.oParentObject.w_errore = "Errore nella creazione della tabella ???%1. %0Query vuota"
          endif
        else
          this.oParentObject.w_PARMSG1 = alltrim(this.pTabella)+chr(13)+message()
          this.oParentObject.w_errore = "Errore nella creazione della tabella %1"
        endif
      endif
      * --- accept error
      bTrsErr=.f.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_03457DA8
    * --- End
  endproc
  proc Try_03457DA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    Declare STRUCTAB (1,5)
    this.w_FIELDS = ""
    this.w_FIELDS2 = ""
    if not(empty(this.pquery))
      vq_exec(this.pQuery,this,"CursTab")
    else
      Messaggio = "Query_vuota"
      * --- Raise
      i_Error=AH_MsgFormat("Query vuota")
      return
    endif
    if Used("CursTab")
      Afields(STRUCTAB,"CursTab")
      for nFields = 1 to alen(STRUCTAB,1) 
      i_TmpErr=on("ERROR") 
 ON ERROR Messaggio = "Errata"
      this.w_VarError = STRUCTAB(nFields,1)
      if len(this.w_FIELDS+" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(nFields,2)$"I-B-F","N",STRUCTAB(nFields,2)),STRUCTAB(nFields,3),STRUCTAB(nFields,4))+", ") < 255 and empty(this.w_FIELDS2)
        this.w_FIELDS = this.w_FIELDS + STRUCTAB(nFields,1)
        this.w_FIELDS = this.w_FIELDS+" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(nFields,2)$"I-B-F","N",STRUCTAB(nFields,2)),STRUCTAB(nFields,3),STRUCTAB(nFields,4))+", "
      else
        this.w_FIELDS2 = this.w_FIELDS2 + STRUCTAB(nFields,1)
        this.w_FIELDS2 = this.w_FIELDS2 +" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(nFields,2)$"I-B-F","N",STRUCTAB(nFields,2)),STRUCTAB(nFields,3),STRUCTAB(nFields,4))+", "
      endif
      ON ERROR &i_TmpErr
      if Messaggio="Errata"
        * --- Raise
        i_Error=AH_MsgFormat("Errore creazione tabella")
        return
      endif
      endfor
      if this.w_MULTIAZI="S"
        if len(this.w_FIELDS+ "CODAZIEN" + " " + db_FieldType(this.w_DB,"C",5,0) + ", ") < 255 and empty(this.w_FIELDS2)
          this.w_FIELDS = this.w_FIELDS + "CODAZIEN" + " " + db_FieldType(this.w_DB,"C",5,0) + ", "
        else
          this.w_FIELDS2 = this.w_FIELDS2 + "CODAZIEN" + " " + db_FieldType(this.w_DB,"C",5,0) + ", "
        endif
        if len(this.w_FIELDS + "DESAZIEN" + " " + db_FieldType(this.w_DB,"C",40,0) + ", ")< 255 and empty(this.w_FIELDS2)
          this.w_FIELDS = this.w_FIELDS + "DESAZIEN" + " " + db_FieldType(this.w_DB,"C",40,0) + ", "
        else
          this.w_FIELDS2 = this.w_FIELDS2 + "DESAZIEN" + " " + db_FieldType(this.w_DB,"C",40,0) + ", "
        endif
        * --- Verifico la correttezza dei campi di concatenazione
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
          * --- Accodo i campi di concatenazione
          i=1
          do while i <= alen(FIELDLIST)
            * --- Calcolo l'effettivo numero di riga
            posiz = int(ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1) / 18) + 1
            if len(this.w_FIELDS + FIELDLIST(i)) < 255 and empty(this.w_FIELDS2)
              this.w_FIELDS = this.w_FIELDS + FIELDLIST(i)
            else
              this.w_FIELDS2 = this.w_FIELDS2 + FIELDLIST(i)
            endif
            if len(this.w_FIELDS +" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(posiz,2)$"I-B-F","N",STRUCTAB(posiz,2)),STRUCTAB(posiz,3)+5,STRUCTAB(posiz,4))+", ") < 255 and empty(this.w_FIELDS2)
              this.w_FIELDS = this.w_FIELDS+" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(posiz,2)$"I-B-F","N",STRUCTAB(posiz,2)),STRUCTAB(posiz,3)+5,STRUCTAB(posiz,4))+", "
            else
              this.w_FIELDS2 = this.w_FIELDS2 +" "+db_FieldType(this.w_DB,iif(this.w_DB<>"SQLServer" and STRUCTAB(posiz,2)$"I-B-F","N",STRUCTAB(posiz,2)),STRUCTAB(posiz,3)+5,STRUCTAB(posiz,4))+", "
            endif
            i=i+1
          enddo
        endif
      endif
      if len(this.w_FIELDS) <=255 and empty(this.w_FIELDS2)
        this.w_FIELDS = left(this.w_FIELDS,len(this.w_FIELDS)-2)
      else
        this.w_FIELDS2 = left(this.w_FIELDS2,len(this.w_FIELDS2)-2)
      endif
      righe = sqlexec(this.w_CONNECT,"Create table "+this.pTabella+" "+"("+this.w_FIELDS+this.w_FIELDS2+")")
    else
      bTrsErr = .t.
    endif
    if righe<0 or bTrsErr
      * --- Raise
      i_Error=AH_MsgFormat("Errore creazione tabella")
      return
    endif
    this.oParentObject.w_MSG = AH_MsgFormat("Tabella %1 creata", alltrim(this.ptabella))
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione lista campi  tabella (senza tipi!)
    if ! used (this.oParentObject.w_RENOMCUR) 
      Afields(STRUCTAB,"CursTab")
    else
      Afields(STRUCTAB,this.oParentObject.w_RENOMCUR)
    endif
    for nFields = 1 to alen(STRUCTAB,1) 
    this.w_FIELDS = this.w_FIELDS + STRUCTAB(nFields,1)+", "
    endfor
    if this.w_MULTIAZI="S"
      this.w_FIELDS = this.w_FIELDS + "CODAZIEN" + ", "+ "DESAZIEN" + ", "
      * --- Verifico la correttezza dei campi di concatenazione
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_CONCATOK and TYPE ("FIELDLIST")<>"U"
        * --- Accodo i campi di concatenazione
        i=1
        do while i <= alen(FIELDLIST)
          if len(this.w_fields) < 240
            this.w_FIELDS = this.w_FIELDS+FIELDLIST(i)+", "
          else
            this.w_FIELDS2 = this.w_FIELDS2 +FIELDLIST(i)+", "
          endif
          i = i +1
        enddo
      endif
    endif
    this.w_FIELDS = left(this.w_FIELDS,len(this.w_FIELDS)-2)
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione indici
    * --- Controllo se la tabella � stata creata
    sqlexec(this.w_CONNECT,"Select * from "+this.pTABELLA,"RISULTATO")
    if ! USED("RISULTATO") 
      ah_errormsg("La tabella %1 non � stata creata%0L'operazione sar� sospesa",48,"", alltrim(this.ptabella))
      i_retcode = 'stop'
      return
    endif
    * --- Select from INF_INDI
    i_nConn=i_TableProp[this.INF_INDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_INDI_idx,2],.t.,this.INF_INDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select GICAMPIN,CPROWNUM  from "+i_cTable+" INF_INDI ";
          +" where GITABNAM="+cp_ToStrODBC(this.pTABELLA)+"";
          +" order by CPROWORD";
           ,"_Curs_INF_INDI")
    else
      select GICAMPIN,CPROWNUM from (i_cTable);
       where GITABNAM=this.pTABELLA;
       order by CPROWORD;
        into cursor _Curs_INF_INDI
    endif
    if used('_Curs_INF_INDI')
      select _Curs_INF_INDI
      locate for 1=1
      do while not(eof())
      this.w_NUMINDX = _Curs_INF_INDI.CPROWNUM
      this.w_CAMPINDX = _Curs_INF_INDI.GICAMPIN
      this.w_CMD = "create index "+alltrim(this.pTABELLA)+"_inf"+alltrim(str(this.w_NUMINDX)) + " on " +alltrim(this.pTABELLA) + "  (" +alltrim(this.w_CAMPINDX)+")"
      sqlexec(this.w_CONNECT,this.w_CMD)
      this.w_INDEXCRE = .T.
        select _Curs_INF_INDI
        continue
      enddo
      use
    endif
    if this.w_INDEXCRE
      this.oParentObject.w_MSG = AH_MsgFormat("Indice creato")
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo correttezza nome campi e tipo dei campi da concatenare
    this.w_TOTFIELD = alen(STRUCTAB,1)
    if TYPE ("FIELDLIST")<>"U"
      for i=1 to alen(FIELDLIST)
      if ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1) > 0
        posiz = int(ASCAN(STRUCTAB,alltrim(substr(FIELDLIST(i),1,len(FIELDLIST(i))-1)),1,this.w_TOTFIELD,1) / 18) + 1
      else
        posiz = 0
      endif
      if posiz = 0 or STRUCTAB(posiz,2) <> "C"
        this.w_CONCATOK = .F.
        * --- Inserisce il messaggio nel Log
        AddMsgNl("Attenzione: Il campo %1 non � valido o � di tipo diverso da carattere.",this.oParentObject,FIELDLIST(i))
      endif
      next
    endif
  endproc


  proc Init(oParentObject,pOperaz,pTabella,pQUERY)
    this.pOperaz=pOperaz
    this.pTabella=pTabella
    this.pQUERY=pQUERY
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='INF_GRUD'
    this.cWorkTables[2]='INF_TAB'
    this.cWorkTables[3]='INF_INDI'
    this.cWorkTables[4]='INF_TAAZ'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_INF_INDI')
      use in _Curs_INF_INDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperaz,pTabella,pQUERY"
endproc
