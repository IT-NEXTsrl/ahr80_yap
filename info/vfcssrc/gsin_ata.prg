* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsin_ata                                                        *
*              Archivio default connessione / azienda                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-07                                                      *
* Last revis.: 2012-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsin_ata"))

* --- Class definition
define class tgsin_ata as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 655
  Height = 119+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-30"
  HelpContextID=92909417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  INF_TAAZ_IDX = 0
  AZIENDA_IDX = 0
  cFile = "INF_TAAZ"
  cKeySelect = "TACODAZI"
  cKeyWhere  = "TACODAZI=this.w_TACODAZI"
  cKeyWhereODBC = '"TACODAZI="+cp_ToStrODBC(this.w_TACODAZI)';

  cKeyWhereODBCqualified = '"INF_TAAZ.TACODAZI="+cp_ToStrODBC(this.w_TACODAZI)';

  cPrg = "gsin_ata"
  cComment = "Archivio default connessione / azienda"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TACODAZI = space(5)
  w_ATCRIPTE = space(1)
  w_TACOODBC = space(25)
  o_TACOODBC = space(25)
  w_ATCOSTRI = space(250)
  o_ATCOSTRI = space(250)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INF_TAAZ','gsin_ata')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsin_ataPag1","gsin_ata",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Archivio default connessione/azienda")
      .Pages(1).HelpContextID = 67833253
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTACODAZI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='INF_TAAZ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INF_TAAZ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INF_TAAZ_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TACODAZI = NVL(TACODAZI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from INF_TAAZ where TACODAZI=KeySet.TACODAZI
    *
    i_nConn = i_TableProp[this.INF_TAAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_TAAZ_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INF_TAAZ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INF_TAAZ.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INF_TAAZ '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TACODAZI',this.w_TACODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TACODAZI = NVL(TACODAZI,space(5))
        .w_ATCRIPTE = NVL(ATCRIPTE,space(1))
        .w_TACOODBC = NVL(TACOODBC,space(25))
        .w_ATCOSTRI = NVL(ATCOSTRI,space(250))
        cp_LoadRecExtFlds(this,'INF_TAAZ')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TACODAZI = space(5)
      .w_ATCRIPTE = space(1)
      .w_TACOODBC = space(25)
      .w_ATCOSTRI = space(250)
      if .cFunction<>"Filter"
        .w_TACODAZI = space(5)
        .w_ATCRIPTE = 'N'
        .w_TACOODBC = space(25)
        .w_ATCOSTRI = space(250)
      endif
    endwith
    cp_BlankRecExtFlds(this,'INF_TAAZ')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTACODAZI_1_1.enabled = i_bVal
      .Page1.oPag.oTACOODBC_1_4.enabled = i_bVal
      .Page1.oPag.oATCOSTRI_1_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_5.enabled = i_bVal
      .Page1.oPag.oBtn_1_6.enabled = i_bVal
      .Page1.oPag.oBtn_1_11.enabled = .Page1.oPag.oBtn_1_11.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oTACODAZI_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTACODAZI_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'INF_TAAZ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INF_TAAZ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TACODAZI,"TACODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCRIPTE,"ATCRIPTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TACOODBC,"TACOODBC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCOSTRI,"ATCOSTRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INF_TAAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_TAAZ_IDX,2])
    i_lTable = "INF_TAAZ"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INF_TAAZ_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_TAAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_TAAZ_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.INF_TAAZ_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INF_TAAZ
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INF_TAAZ')
        i_extval=cp_InsertValODBCExtFlds(this,'INF_TAAZ')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TACODAZI,ATCRIPTE,TACOODBC,ATCOSTRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TACODAZI)+;
                  ","+cp_ToStrODBC(this.w_ATCRIPTE)+;
                  ","+cp_ToStrODBC(this.w_TACOODBC)+;
                  ","+cp_ToStrODBC(this.w_ATCOSTRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INF_TAAZ')
        i_extval=cp_InsertValVFPExtFlds(this,'INF_TAAZ')
        cp_CheckDeletedKey(i_cTable,0,'TACODAZI',this.w_TACODAZI)
        INSERT INTO (i_cTable);
              (TACODAZI,ATCRIPTE,TACOODBC,ATCOSTRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TACODAZI;
                  ,this.w_ATCRIPTE;
                  ,this.w_TACOODBC;
                  ,this.w_ATCOSTRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.INF_TAAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_TAAZ_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.INF_TAAZ_IDX,i_nConn)
      *
      * update INF_TAAZ
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'INF_TAAZ')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ATCRIPTE="+cp_ToStrODBC(this.w_ATCRIPTE)+;
             ",TACOODBC="+cp_ToStrODBC(this.w_TACOODBC)+;
             ",ATCOSTRI="+cp_ToStrODBC(this.w_ATCOSTRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'INF_TAAZ')
        i_cWhere = cp_PKFox(i_cTable  ,'TACODAZI',this.w_TACODAZI  )
        UPDATE (i_cTable) SET;
              ATCRIPTE=this.w_ATCRIPTE;
             ,TACOODBC=this.w_TACOODBC;
             ,ATCOSTRI=this.w_ATCOSTRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INF_TAAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_TAAZ_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.INF_TAAZ_IDX,i_nConn)
      *
      * delete INF_TAAZ
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TACODAZI',this.w_TACODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INF_TAAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_TAAZ_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_ATCOSTRI<>.w_ATCOSTRI
            .w_TACOODBC = space(25)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTACOODBC_1_4.enabled = this.oPgFrm.Page1.oPag.oTACOODBC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oATCOSTRI_1_8.enabled = this.oPgFrm.Page1.oPag.oATCOSTRI_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.visible=!this.oPgFrm.Page1.oPag.oBtn_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_6.visible=!this.oPgFrm.Page1.oPag.oBtn_1_6.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTACODAZI_1_1.value==this.w_TACODAZI)
      this.oPgFrm.Page1.oPag.oTACODAZI_1_1.value=this.w_TACODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oATCRIPTE_1_2.RadioValue()==this.w_ATCRIPTE)
      this.oPgFrm.Page1.oPag.oATCRIPTE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTACOODBC_1_4.value==this.w_TACOODBC)
      this.oPgFrm.Page1.oPag.oTACOODBC_1_4.value=this.w_TACOODBC
    endif
    if not(this.oPgFrm.Page1.oPag.oATCOSTRI_1_8.value==this.w_ATCOSTRI)
      this.oPgFrm.Page1.oPag.oATCOSTRI_1_8.value=this.w_ATCOSTRI
    endif
    cp_SetControlsValueExtFlds(this,'INF_TAAZ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY( .w_TACODAZI ) OR !EMPTY( NVL( LOOKTAB( "AZIENDA" , "AZCODAZI" , "AZCODAZI" , .w_TACODAZI ) , "" ) ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTACODAZI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TACOODBC = this.w_TACOODBC
    this.o_ATCOSTRI = this.w_ATCOSTRI
    return

enddefine

* --- Define pages as container
define class tgsin_ataPag1 as StdContainer
  Width  = 651
  height = 119
  stdWidth  = 651
  stdheight = 119
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTACODAZI_1_1 as StdField with uid="ESQCBYUBYF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TACODAZI", cQueryName = "TACODAZI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice azienda",;
    HelpContextID = 640895,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=100, Top=19, InputMask=replicate('X',5), bHasZoom = .t. 

  func oTACODAZI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY( .w_TACODAZI ) OR !EMPTY( NVL( LOOKTAB( "AZIENDA" , "AZCODAZI" , "AZCODAZI" , .w_TACODAZI ) , "" ) ))
    endwith
    return bRes
  endfunc

  proc oTACODAZI_1_1.mZoom
    do GSIN_KZA with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oATCRIPTE_1_2 as StdCombo with uid="VVPIBVQFUR",rtseq=2,rtrep=.f.,left=524,top=20,width=124,height=21, enabled=.f.;
    , ToolTipText = "Tipo di protezione";
    , HelpContextID = 257743179;
    , cFormVar="w_ATCRIPTE",RowSource=""+"Stringa cifrata,"+"Nessuna cifratura", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATCRIPTE_1_2.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oATCRIPTE_1_2.GetRadio()
    this.Parent.oContained.w_ATCRIPTE = this.RadioValue()
    return .t.
  endfunc

  func oATCRIPTE_1_2.SetRadio()
    this.Parent.oContained.w_ATCRIPTE=trim(this.Parent.oContained.w_ATCRIPTE)
    this.value = ;
      iif(this.Parent.oContained.w_ATCRIPTE=='C',1,;
      iif(this.Parent.oContained.w_ATCRIPTE=='N',2,;
      0))
  endfunc

  add object oTACOODBC_1_4 as StdField with uid="INOQYKNTNF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TACOODBC", cQueryName = "TACOODBC",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome connessione ODBC",;
    HelpContextID = 205928583,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=100, Top=60, InputMask=replicate('X',25)

  func oTACOODBC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_ATCOSTRI) AND .w_ATCRIPTE<>'C')
    endwith
   endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="BLYJZKVELC",left=599, top=47, width=48,height=45,;
    CpPicture="bmp\CIFRA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per cifrare la stringa di connessione";
    , HelpContextID = 251716058;
    , Caption='\<Cifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      do GSIN_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ATCRIPTE='C')
     endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="MOBSVIKWGK",left=599, top=47, width=48,height=45,;
    CpPicture="bmp\DECIFRA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per decifrare la stringa di connessione";
    , HelpContextID = 54913590;
    , Caption='\<Decifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      do GSIN_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ATCRIPTE<>'C')
     endwith
    endif
  endfunc

  add object oATCOSTRI_1_8 as StdField with uid="KMCSTRCDVB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ATCOSTRI", cQueryName = "ATCOSTRI",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione",;
    HelpContextID = 66705743,;
   bGlobalFont=.t.,;
    Height=21, Width=548, Left=100, Top=95, InputMask=replicate('X',250)

  func oATCOSTRI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_TACOODBC)  AND .w_ATCRIPTE<>'C')
    endwith
   endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="OFVJCTCDVD",left=546, top=47, width=48,height=45,;
    CpPicture="COPY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per memorizzare la stringa di connessione";
    , HelpContextID = 252263386;
    , Caption='\<Copia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        _CLIPTEXT = ALLTRIM( .w_ATCOSTRI )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="NWKDXHVHMP",Visible=.t., Left=7, Top=22,;
    Alignment=1, Width=92, Height=19,;
    Caption="Cod. azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="OGRAUAALQA",Visible=.t., Left=37, Top=64,;
    Alignment=1, Width=62, Height=18,;
    Caption="ODBC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="WQJDIDNCKU",Visible=.t., Left=11, Top=97,;
    Alignment=1, Width=88, Height=18,;
    Caption="Stringa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="KIGDBVVPMD",Visible=.t., Left=376, Top=20,;
    Alignment=1, Width=144, Height=18,;
    Caption="Tipo di protezione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsin_ata','INF_TAAZ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TACODAZI=INF_TAAZ.TACODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
