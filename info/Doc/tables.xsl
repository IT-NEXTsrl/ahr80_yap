<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:param name="default_extension" select="'.xml'"/>  <!-- usato nei link del tag Plan -->

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <xsl:apply-templates select="Plan"/>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Plan">
  <HTML>
    <HEAD>
      <STYLE>
        BODY {font-family:Times New Roman; color:black; background:white; margin:20px}
        TABLE {font-family:Times New Roman; color:black; background:white}
        A:hover {color:red}
      </STYLE>
    </HEAD>
    <BODY>
      <P ALIGN="CENTER"><FONT COLOR="#000080" SIZE="6"><B>Tables</B></FONT></P>
      <xsl:apply-templates select="PlanItemlist/PlanItem/ItemTable">
        <xsl:sort select="TableName"/>
      </xsl:apply-templates>
      <DIV ALIGN="CENTER">
        <A HREF="{PlanName}_Main{$default_extension}">Project overview</A>
      </DIV>
    </BODY>
  </HTML>
</xsl:template>


<xsl:template match="ItemTable">
  <A NAME="Table_{TableName}">
    <FONT SIZE="5"><xsl:value-of select="TableName"/></FONT>
  </A>

  <!-- Descrizione tabella -->

  <BLOCKQUOTE>
    <TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
      <TR BGCOLOR="#6495ED">
        <TD><FONT COLOR="WHITE"><B>Description</B></FONT></TD>
        <TD><FONT COLOR="WHITE"><B>Phname</B></FONT></TD>
        <TD><FONT COLOR="WHITE"><B>Company</B></FONT></TD>
        <xsl:if test="TableYear">
          <TD><FONT COLOR="WHITE"><B>Year</B></FONT></TD>
        </xsl:if>
        <xsl:if test="TableUser">
          <TD><FONT COLOR="WHITE"><B>User</B></FONT></TD>
        </xsl:if>
        <xsl:if test="TableCheck">
          <TD><FONT COLOR="WHITE"><B>Check</B></FONT></TD>
        </xsl:if>
      </TR>
      <TR BGCOLOR="#F4FBFF">
        <TD><xsl:apply-templates select="TableComment"/></TD>
        <TD><xsl:apply-templates select="TablePhname"/></TD>
        <TD ALIGN="CENTER"><xsl:apply-templates select="TableCompany"/></TD>
        <xsl:if test="TableYear">
          <TD ALIGN="CENTER"><xsl:apply-templates select="TableYear"/></TD>
        </xsl:if>
        <xsl:if test="TableUser">
          <TD ALIGN="CENTER"><xsl:apply-templates select="TableUser"/></TD>
        </xsl:if>
        <xsl:if test="TableCheck">
          <TD><xsl:apply-templates select="TableCheck"/></TD>
        </xsl:if>
      </TR>
    </TABLE>
  </BLOCKQUOTE>

  <!-- Descrizione indici -->

  <xsl:if test="TableIndex">
    <BLOCKQUOTE>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <TR BGCOLOR="#85ACDC">
          <TD COLSPAN="2" ALIGN="CENTER" STYLE="BORDER-BOTTOM: 1px SOLID #000000;">
            <B>INDEXES</B>
          </TD>
        </TR>
        <xsl:for-each select="TableIndex">
          <TR>
            <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;" BGCOLOR="#B0C4DE">
              <B><xsl:value-of select="position()"/></B>
            </TD>
            <TD STYLE="BORDER-TOP: 1px SOLID #000000;" BGCOLOR="#EDFCFF">
              <xsl:value-of select="."/>
            </TD>
          </TR>
        </xsl:for-each>
      </TABLE>
    </BLOCKQUOTE>
  </xsl:if>

  <!-- Descrizione autonumber -->

  <xsl:if test="TableAutonum">
    <BLOCKQUOTE>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <TR BGCOLOR="#85ACDC">
          <TD COLSPAN="3" ALIGN="CENTER" STYLE="BORDER-BOTTOM: 1px SOLID #000000;">
            <B>AUTONUMBERS</B>
          </TD>
        </TR>
        <TR BGCOLOR="#B0C4DE">
          <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
            <B>Field</B>
          </TD>
          <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
            <B>Table</B>
          </TD>
          <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
            <B>Condition</B>
          </TD>
        </TR>
        <xsl:for-each select="TableAutonum">
          <TR BGCOLOR="#EDFCFF">
            <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
              <A HREF="#{../TableName}_{.}"><xsl:value-of select="."/></A>
            </TD>
            <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
              <xsl:apply-templates select="@Tbl"/>
            </TD>
            <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
              <xsl:apply-templates select="@Cnd"/>
            </TD>
          </TR>
        </xsl:for-each>
      </TABLE>
    </BLOCKQUOTE>
  </xsl:if>

  <!-- Links -->

  <xsl:if test="TableLink">
    <BLOCKQUOTE>
      <xsl:apply-templates select="TableLink">
        <xsl:sort select="@Type"/>
      </xsl:apply-templates>
    </BLOCKQUOTE>
  </xsl:if>

  <!-- Foreign keys -->

  <xsl:if test="TableForeignkeys/TableForeignkey">
    <BLOCKQUOTE>
      <xsl:apply-templates select="TableForeignkeys"/>
    </BLOCKQUOTE>
  </xsl:if>

  <!-- Fields -->

  <BLOCKQUOTE>
    <xsl:apply-templates select="TableFields"/>
  </BLOCKQUOTE>

  <TABLE WIDTH="100%"><TR><TD ALIGN="RIGHT">
    <A HREF="#Table_{TableName}">Go to '<xsl:value-of select="TableName"/>'</A>
  </TD></TR></TABLE>
  <HR/>
</xsl:template>


<!-- Descrizione link -->

<xsl:template match="TableLink">
  <xsl:variable name="table"
                select="/Plan/PlanItemlist/PlanItem[ItemUID=current()/LinkUID]/ItemTable/TableName"/>
  <TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0">
    <TR>
      <TD COLSPAN="5">
        <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="100%">
          <TR BGCOLOR="#85ACDC">
            <TD ALIGN="LEFT" STYLE="BORDER-BOTTOM: 1px SOLID #000000;">
              <B><xsl:value-of select="@Type"/> with </B>
              <A HREF="#Table_{$table}"><xsl:value-of select="$table"/></A>
            </TD>
          </TR>
        </TABLE>
      </TD>
    </TR>
    <TR>
      <TD VALIGN="TOP">
        <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="100%">
          <TR BGCOLOR="#B0C4DE">
            <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
              <B>Fk&#160;Key</B>
            </TD>
            <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
              <B>Fk&#160;Ref</B>
            </TD>
          </TR>
          <xsl:for-each select="LinkFk">
            <TR BGCOLOR="#EDFCFF">
              <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
                <A HREF="#{$table}_{@Key}"><xsl:value-of select="@Key"/></A>
              </TD>
              <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
                <A HREF="#{../../TableName}_{@Ref}"><xsl:value-of select="@Ref"/></A>
              </TD>
            </TR>
          </xsl:for-each>
        </TABLE>
      </TD>
      <xsl:if test="@Type='Relationship' and LinkWrite">
        <TD STYLE="BORDER-TOP: 1px SOLID #000000;">&#160;</TD>
        <TD VALIGN="TOP">
          <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="100%">
            <TR BGCOLOR="#B0C4DE">
              <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
                <B>Write&#160;from</B>
              </TD>
              <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
                <B>To&#160;field</B>
              </TD>
              <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
                <B>Oper.</B>
              </TD>
            </TR>
            <xsl:for-each select="LinkWrite">
              <TR BGCOLOR="#EDFCFF">
                <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
                  <A HREF="#{../../TableName}_{@From}"><xsl:value-of select="@From"/></A>
                </TD>
                <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
                  <A HREF="#{$table}_{@To}"><xsl:value-of select="@To"/></A>
                </TD>
                <TD STYLE="BORDER-TOP: 1px SOLID #000000;" ALIGN="CENTER">
                  <xsl:apply-templates select="@Operation"/>
                </TD>
              </TR>
            </xsl:for-each>
          </TABLE>
        </TD>
      </xsl:if>
      <xsl:if test="@Type='Relationship' and LinkRead">
        <TD STYLE="BORDER-TOP: 1px SOLID #000000;">&#160;</TD>
        <TD VALIGN="TOP">
          <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="100%">
            <TR BGCOLOR="#B0C4DE">
              <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
                <B>Read&#160;from</B>
              </TD>
              <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
                <B>To&#160;field</B>
              </TD>
            </TR>
            <xsl:for-each select="LinkRead">
              <TR BGCOLOR="#EDFCFF">
                <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
                  <A HREF="#{$table}_{@From}"><xsl:value-of select="@From"/></A>
                </TD>
                <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
                  <A HREF="#{../../TableName}_{@To}"><xsl:value-of select="@To"/></A>
                </TD>
              </TR>
            </xsl:for-each>
          </TABLE>
        </TD>
      </xsl:if>
    </TR>
    <xsl:if test="LinkCreate">
      <TR>
        <TD COLSPAN="5">
          <TABLE CELLPADDING="0" CELLSPACING="0">
            <TR>
              <TD><xsl:apply-templates select="LinkCreate"/></TD>
              <TD VALIGN="TOP">Create record if it does not exist</TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
    </xsl:if>
  </TABLE>

  <TABLE>
    <xsl:if test="LinkNote!=''">
      <TR>
        <TD STYLE="BORDER-BOTTOM: 1px SOLID #000000;"><I>Notes</I></TD>
      </TR>
      <TR>
        <TD><xsl:apply-templates select="LinkNote"/></TD>
      </TR>
    </xsl:if>
    <TR>
      <TD>&#160;</TD>
    </TR>
  </TABLE>
</xsl:template>


<!-- Descrizione foreign keys -->

<xsl:template match="TableForeignkeys">
  <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
    <TR BGCOLOR="#85ACDC">
      <TD COLSPAN="4" ALIGN="CENTER" STYLE="BORDER-BOTTOM: 1px SOLID #000000;">
        <B>FOREIGN&#160;KEYS</B>
      </TD>
    </TR>
    <TR BGCOLOR="#B0C4DE">
      <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
        <B>Table</B>
      </TD>
      <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
        <B>Weak&#160;entity</B>
      </TD>
      <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
        <B>Key</B>
      </TD>
      <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
        <B>Ref</B>
      </TD>
    </TR>
    <xsl:for-each select="TableForeignkey">
      <xsl:variable name="table" select="ForeignkeyTable"/>
      <TR BGCOLOR="#EDFCFF">
        <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
          <A HREF="#Table_{$table}"><xsl:value-of select="$table"/></A>
        </TD>
        <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;" ALIGN="CENTER">
          <xsl:apply-templates select="ForeignkeyWeakentity"/>
        </TD>
        <TD STYLE="BORDER-TOP: 1px SOLID #000000; BORDER-RIGHT: 1px SOLID #000000;">
          <xsl:for-each select="ForeignkeyKey">
            <A HREF="#{$table}_{.}"><xsl:value-of select="."/></A><BR/>
          </xsl:for-each>
        </TD>
        <TD STYLE="BORDER-TOP: 1px SOLID #000000;">
          <xsl:for-each select="ForeignkeyRef">
            <A HREF="#{../../../TableName}_{.}"><xsl:value-of select="."/></A><BR/>
          </xsl:for-each>
        </TD>
      </TR>
    </xsl:for-each>
  </TABLE>
</xsl:template>


<!-- Descrizione campi -->

<xsl:template match="TableFields">
  <TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
    <TR BGCOLOR="#6495ED" VALIGN="TOP">
      <TD ROWSPAN="{1+number(TableField/FieldNote!='')}"><FONT COLOR="WHITE"><B>Field</B></FONT></TD>
      <TD><FONT COLOR="WHITE"><B>Description</B></FONT></TD>
      <TD><FONT COLOR="WHITE"><B>Type</B></FONT></TD>
      <TD><FONT COLOR="WHITE"><B>Len</B></FONT></TD>
      <TD><FONT COLOR="WHITE"><B>Dec</B></FONT></TD>
      <TD><FONT COLOR="WHITE"><B>Key</B></FONT></TD>
      <xsl:if test="TableField/FieldNotnull">
        <TD><FONT COLOR="WHITE"><B>Not&#160;null</B></FONT></TD>
      </xsl:if>
      <xsl:if test="TableField/FieldRepeated">
        <TD><FONT COLOR="WHITE"><B>Repeated</B></FONT></TD>
      </xsl:if>
      <xsl:if test="TableField/FieldDefaultval">
        <TD><FONT COLOR="WHITE"><B>Default</B></FONT></TD>
      </xsl:if>
      <xsl:if test="TableField/FieldCheck">
        <TD><FONT COLOR="WHITE"><B>Check</B></FONT></TD>
      </xsl:if>
    </TR>
    <xsl:if test="TableField/FieldNote!=''">
      <TR BGCOLOR="#6495ED">
        <TD COLSPAN="9">
          <FONT COLOR="WHITE"><B>Notes</B></FONT>
          <xsl:if test="TableField/FieldNote=''">
            <FONT COLOR="WHITE"><B> (when available)</B></FONT>
          </xsl:if>
        </TD>
      </TR>
    </xsl:if>
    <xsl:for-each select="TableField">
      <xsl:variable name="bgcolor">
        <xsl:choose><xsl:when test="position() mod 2">#F4FBFF</xsl:when>
        <xsl:otherwise>#E8FAFF</xsl:otherwise></xsl:choose>
      </xsl:variable>
      <TR BGCOLOR="{$bgcolor}" VALIGN="TOP">
        <TD ROWSPAN="{1+number(FieldNote!='')}">
          <A NAME="{../../TableName}_{FieldName}"/>
          <xsl:value-of select="FieldName"/>
        </TD>
        <TD><xsl:apply-templates select="FieldComment"/></TD>
        <TD><xsl:apply-templates select="FieldType"/></TD>
        <TD><xsl:apply-templates select="FieldLen"/></TD>
        <TD><xsl:apply-templates select="FieldDec"/></TD>
        <TD><xsl:apply-templates select="FieldKey"/></TD>
        <xsl:if test="FieldNotnull">
          <TD ALIGN="CENTER"><xsl:apply-templates select="FieldNotnull"/></TD>
        </xsl:if>
        <xsl:if test="FieldRepeated">
          <TD ALIGN="CENTER"><xsl:apply-templates select="FieldRepeated"/></TD>
        </xsl:if>
        <xsl:if test="FieldDefaultval">
          <TD><xsl:apply-templates select="FieldDefaultval"/></TD>
        </xsl:if>
        <xsl:if test="FieldCheck">
          <TD><xsl:apply-templates select="FieldCheck"/></TD>
        </xsl:if>
      </TR>
      <xsl:if test="FieldNote!=''">
        <TR BGCOLOR="{$bgcolor}">
          <TD COLSPAN="9"><xsl:apply-templates select="FieldNote"/></TD>
        </TR>
      </xsl:if>
    </xsl:for-each>
  </TABLE>
</xsl:template>


<!-- Gestione checkbox TRUE/FALSE -->

<xsl:template match="FieldNotnull | FieldRepeated |
                     TableUser | TableYear | TableCompany | ForeignkeyWeakentity |
                     LinkBlinkedismaster | LinkBismaster | LinkCreate">
  <xsl:call-template name="LogicalCheckBox"/>
</xsl:template>


<!-- Gestione celle vuote -->

<xsl:template match="FieldDefaultval | FieldCheck |
                     TableCheck | TableAutonum/@* |
                     LinkWrite/@Operation">
  <xsl:call-template name="ValueOrNotAvailable">
    <xsl:with-param name="NA" select="'&#160;'"/>
  </xsl:call-template>
</xsl:template>


<xsl:template match="FieldComment | FieldType | FieldLen | FieldDec | FieldKey |
                     TableComment | TablePhname">
  <xsl:call-template name="ValueOrNotAvailable"/>
</xsl:template>


</xsl:stylesheet>
