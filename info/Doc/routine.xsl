<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <xsl:apply-templates select="Routine"/>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Routine">
  <HTML>
  <HEAD>
  <STYLE>
    .Item {font-family:Tahoma}
    BODY  {font-family:Times New Roman; color:black; background:white; margin:20px}
    TABLE {font-family:Times New Roman; color:black; background:white}
    .FIELD_TABLE {border: 1px solid; margin-top:5px; margin-bottom:5px; background:moccasin}
    .FIELD_CELL {padding-left:10px; padding-right:10px}
    .ERROR_MSG {color:red; font-weight:normal; font-style:italic}
    A:hover {color:red}
    .BATCH_LINK:hover {color:DARKORANGE}
    .BATCH_LINK {color:DARKSLATEBLUE; font-weight:bold; text-decoration:none}
    .PLUS { }
  </STYLE>
  <SCRIPT language="JavaScript" type="text/javascript"><xsl:text disable-output-escaping='yes'><![CDATA[
    function swap_style(id) {
      var t = document.getElementById(id+'_h').style.display;
      document.getElementById(id+'_h').style.display = document.getElementById(id+'_s').style.display;
      document.getElementById(id+'_s').style.display = t;
    }
  ]]></xsl:text></SCRIPT>

  </HEAD>

  <BODY BGCOLOR="#FFFFFF">
  <A NAME="top-of-file"/>

  <!-- ********** Titolo ********** -->

  <P ALIGN="CENTER">
    <FONT COLOR="#000080" SIZE="6"><B><xsl:value-of select="Title"/></B></FONT>
  </P>

  <!-- ********** Dati globali ********** -->

  <TABLE BORDER="0" WIDTH="100%" CELLSPACING="0" STYLE="margin-top:10px; margin-bottom:30px">
    <xsl:if test="Version!=''">
      <TR>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
          <B>Version:</B>
        </TD>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
          <xsl:value-of select="Version"/>
        </TD>
      </TR>
    </xsl:if>
    <xsl:if test="Author!=''">
      <TR>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
          <B>Author:</B>
        </TD>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
          <xsl:value-of select="Author"/>
        </TD>
      </TR>
    </xsl:if>
    <xsl:if test="Created!=''">
      <TR>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
          <B>Created:</B>
        </TD>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
          <xsl:value-of select="Created"/>
        </TD>
      </TR>
    </xsl:if>
    <xsl:if test="Lastrevision!=''">
      <TR>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
          <B>Last modify:</B>
        </TD>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
          <xsl:value-of select="Lastrevision"/>
        </TD>
      </TR>
    </xsl:if>
    <xsl:if test="User!=''">
      <TR>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
          <B>User:</B>
        </TD>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
          <xsl:value-of select="User"/>
        </TD>
      </TR>
    </xsl:if>
  </TABLE>

  <!-- ********** Visualizzazione delle note generali se presenti ********** -->

  <xsl:if test="Note!=''">
    <TABLE BORDER="0" WIDTH="80%" CELLPADDING="3" CELLSPACING="3" ALIGN="CENTER">
      <TR><TD COLSPAN="8" xsl:use-attribute-sets="global-notes">
        <xsl:apply-templates select="Note"/>
      </TD></TR>
    </TABLE>
  </xsl:if>

  <!-- ********** Indice delle pagine strutturato su 3 max pagine per riga ********** -->

  <TABLE BORDER="0" ALIGN="CENTER" WIDTH="100%" CELLPADDING="4" CELLSPACING="0">
    <xsl:for-each select="Pag">
      <xsl:if test="position() mod 3 = 1">
        <TR>
          <xsl:call-template name="PageIndex">
            <xsl:with-param name="name" select="'Procedure'"/>
            <xsl:with-param name="page" select="."/>
            <xsl:with-param name="next" select="count(following-sibling::Pag[position() &lt;= 2])"/>
          </xsl:call-template>
        </TR>
      </xsl:if>
    </xsl:for-each>
  </TABLE>

  <!-- ********** Elenco delle procedure ********** -->

  <xsl:apply-templates select="Pag"/>

  </BODY>
  </HTML>
</xsl:template>


<!-- ********** Gestione del tag "Pag" ********** -->

<xsl:template match="Pag">
  <TABLE BORDER="0" WIDTH="100%" ALIGN="CENTER">
    <TR><TD BGCOLOR="#B0C4DE" ALIGN="CENTER">
      <FONT COLOR="#000080" SIZE="4">
        <A NAME="{Title/@LinkID}">
          <xsl:call-template name="PageTitle">
            <xsl:with-param name="page" select="."/>
            <xsl:with-param name="name" select="'Procedure'"/>
          </xsl:call-template>
        </A>
      </FONT>
    </TD></TR>
    <TR><TD>&#160;</TD></TR>

    <xsl:if test="Note!=''">
      <TR><TD>
        <TABLE ALIGN="CENTER" WIDTH="80%" BORDER="0" CELLPADDING="0" CELLSPACING="0">
          <TR><TD xsl:use-attribute-sets="page-notes">
            <xsl:apply-templates select="Note"/>
          </TD></TR>
        </TABLE>
      </TD></TR>
      <TR><TD>&#160;</TD></TR>
    </xsl:if>

    <TR><TD>
      <xsl:apply-templates select="Item"/>
    </TD></TR>

    <TR><TD>
      <TABLE WIDTH="100%" BORDER="0" CELLPADDING="0" CELLSPACING="20">
        <TR><TD ALIGN="LEFT">
          <A HREF="#{Title/@LinkID}">Go to
            <xsl:call-template name="PageTitle">
              <xsl:with-param name="page" select="."/>
              <xsl:with-param name="name" select="'Procedure'"/>
            </xsl:call-template>
          </A>
        </TD>
        <TD ALIGN="RIGHT">
          <A HREF="#top-of-file">Go to top</A>
        </TD></TR>
      </TABLE>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- ******** VAR ******** -->

<xsl:template match="Item[@type='var']">
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px">
      <xsl:if test="Operation='def'">
        <xsl:choose>
          <xsl:when test="Parameter='TRUE'"><B>Parameter </B></xsl:when>
          <xsl:when test="Local='TRUE'"><B>Local Var </B></xsl:when>
          <xsl:when test="Global='TRUE'"><B>Global Var </B></xsl:when>
          <xsl:otherwise><B>Extern Var </B></xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:value-of select="Text"/>
      <xsl:if test="Comment!=''">
      : <SPAN STYLE="color:green"><xsl:value-of select="Comment"/></SPAN>
      </xsl:if>
    </TD>
  </TR>
</xsl:template>


<!-- ******** EXEC ******** -->

<xsl:template match="Item[@type='exec']">
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px">
      <xsl:if test="Check!='stm'">
        <B>Exec
        <xsl:choose>
          <xsl:when test="Check='bat'">Routine </xsl:when>
          <xsl:when test="Check='rep'">Report </xsl:when>
          <xsl:when test="Check='scr'">Dialog Window </xsl:when>
          <xsl:when test="Check='man'">Manual Block </xsl:when>
          <xsl:when test="Check='ext'">Program </xsl:when>
          <xsl:when test="Check='page'">Page </xsl:when>
          <xsl:when test="Check='utk'">UTK Object </xsl:when>
        </xsl:choose>
        </B>
      </xsl:if>
      <xsl:value-of select="Expression"/>
    </TD>
  </TR>
</xsl:template>


<!-- ******** CREATE TEMPORARY TABLE ******** -->

<xsl:template match="Item[@type='temptable']">
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px">
      <B>Create temporary table </B>
      <xsl:value-of select="TempTable"/>
      <xsl:if test="SelectType='query'">
        <B> from query </B><xsl:value-of select="TableName"/>
      </xsl:if>
      <xsl:if test="SelectType='sql'">
        <B> from ( select </B><xsl:value-of select="Field"/>
        <B> from </B><xsl:value-of select="TableName"/>
        <xsl:if test="Where!='' or Order!='' or Group!=''">
          <BR/>
        </xsl:if>
        <xsl:if test="Where!=''">
          <B> where </B><xsl:value-of select="Where"/>
        </xsl:if>
        <xsl:if test="Order!=''">
          <B> order by </B><xsl:value-of select="Order"/>
        </xsl:if>
        <xsl:if test="Group!=''">
          <B> group by </B><xsl:value-of select="Group"/>
        </xsl:if>
        <B> )</B>
      </xsl:if>
      <xsl:if test="Comment!=''">
      : <SPAN STYLE="color:green"><xsl:value-of select="Comment"/></SPAN>
      </xsl:if>
    </TD>
  </TR>
</xsl:template>


<!-- ******** DROP TEMPORARY TABLE ******** -->

<xsl:template match="Item[@type='droptemptable']">
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px">
      <B>Drop temporary table </B><xsl:value-of select="TempTable"/>
      <xsl:if test="Comment!=''">
      : <SPAN STYLE="color:green"><xsl:value-of select="Comment"/></SPAN>
      </xsl:if>
    </TD>
  </TR>
</xsl:template>


<!-- ******** IF CASE TRY-CATCH WHILE BEGIN ******** -->

<xsl:template match="Item[@type='if' or @type='case' or @type='try' or @type='while' or @type='begin']">
  <xsl:variable name="first">
    <xsl:choose>
      <xsl:when test="@type='if'">If </xsl:when>
      <xsl:when test="@type='case'">Case </xsl:when>
      <xsl:when test="@type='try'">Try </xsl:when>
      <xsl:when test="@type='while'">While </xsl:when>
      <xsl:when test="@type='begin'">Begin </xsl:when>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="middle">
    <xsl:choose>
      <xsl:when test="@type='if'">Else If </xsl:when>
      <xsl:when test="@type='case'">Case </xsl:when>
      <xsl:when test="@type='try'">Catch </xsl:when>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="last">
    <xsl:choose>
      <xsl:when test="@type='if'">Else </xsl:when>
      <xsl:when test="@type='case'">Otherwise </xsl:when>
      <xsl:when test="@type='try'">Catch </xsl:when>
    </xsl:choose>
  </xsl:variable>
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px">
      <xsl:for-each select="condition | else">
        <DIV ID="{concat(generate-id(), @number, '_h')}" STYLE="display:none">
          <A CLASS="BATCH_LINK" TITLE="Expand node" href="javascript:swap_style('{concat(generate-id(), @number)}')">
            <SPAN CLASS="PLUS">+</SPAN>
            <xsl:choose>
              <xsl:when test="position()=1"><xsl:value-of select="$first"/></xsl:when>
              <xsl:when test="position()=last()"><xsl:value-of select="$last"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="$middle"/></xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="../@type='begin'"><xsl:value-of select="../Comment"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="@value"/></xsl:otherwise>
            </xsl:choose>
          </A>
          <xsl:if test="Comment!=''">
          : <SPAN STYLE="color:green"><xsl:value-of select="Comment"/></SPAN>
          </xsl:if>
        </DIV>
        <DIV ID="{concat(generate-id(), @number, '_s')}" STYLE="display:block">
          <A CLASS="BATCH_LINK" TITLE="Collapse node" href="javascript:swap_style('{concat(generate-id(), @number)}')">
            <SPAN CLASS="PLUS">-</SPAN>
            <xsl:choose>
              <xsl:when test="position()=1"><xsl:value-of select="$first"/></xsl:when>
              <xsl:when test="position()=last()"><xsl:value-of select="$last"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="$middle"/></xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="../@type='begin'"><xsl:value-of select="../Comment"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="@value"/></xsl:otherwise>
            </xsl:choose>
          </A>
          <TABLE WIDTH="100%" CELLPADDING="0" CELLSPACING="0" CLASS="Item">
            <xsl:apply-templates select="Item"/>
          </TABLE>
          <xsl:if test="position()=last() and ../@type='begin'">
            End <xsl:value-of select="../Comment"/>
          </xsl:if>
          <xsl:if test="position()=last() and ../@type!='begin'">
            End <xsl:value-of select="$first"/>
          </xsl:if>
        </DIV>
      </xsl:for-each>
    </TD>
  </TR>
</xsl:template>

<!-- ******** WRITE READ DELETE INSERT ******** -->

<xsl:template match="Item[@type='write' or @type='read' or @type='delete' or @type='insert']">
  <xsl:variable name="instr">
    <xsl:choose>
      <xsl:when test="@type='write'">Write into </xsl:when>
      <xsl:when test="@type='read'">Read from </xsl:when>
      <xsl:when test="@type='delete'">Delete from </xsl:when>
      <xsl:when test="@type='insert'">Insert into </xsl:when>
    </xsl:choose>
  </xsl:variable>
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px">

      <xsl:if test="Type='values' or @type='read'">  <!-- Read non ha l'elemento <Type> -->

        <DIV ID="{concat(generate-id(), '_s')}" STYLE="display:block">
          <A CLASS="BATCH_LINK" TITLE="Expand node" href="javascript:swap_style('{generate-id()}')">
            <SPAN CLASS="PLUS">+</SPAN>
            <xsl:value-of select="$instr"/>
            <xsl:value-of select="TableName"/>
          </A>
          <xsl:if test="Comment!=''">
          : <SPAN STYLE="color:green"><xsl:value-of select="Comment"/></SPAN>
          </xsl:if>
        </DIV>
        <DIV ID="{concat(generate-id(), '_h')}" STYLE="display:none">
          <A CLASS="BATCH_LINK" TITLE="Collapse node" href="javascript:swap_style('{generate-id()}')">
            <SPAN CLASS="PLUS">-</SPAN>
            <xsl:value-of select="$instr"/>
            <xsl:value-of select="TableName"/>
          </A>
          <TABLE WIDTH="100%" CELLPADDING="0" CELLSPACING="0" CLASS="Item" STYLE="margin-bottom:5px">
            <TR><TD CLASS="FIELD_CELL">
              <TABLE FRAME="border" CELLSPACING="0" CLASS="FIELD_TABLE">
                <TR>
                  <TH CLASS="FIELD_CELL">Key fields</TH>
                  <xsl:if test="@type='delete' or @type='write'">
                    <TH CLASS="FIELD_CELL">Relation</TH>
                  </xsl:if>
                  <TH CLASS="FIELD_CELL">Key values</TH>
                </TR>
                <xsl:for-each select="KeyFields/Field">
                  <TR>
                    <TD CLASS="FIELD_CELL"><xsl:value-of select="@name"/></TD>
                    <xsl:if test="../../@type='delete' or ../../@type='write'">
                      <TD CLASS="FIELD_CELL">
                        <xsl:choose>
                          <xsl:when test="@operation!=''"><xsl:value-of select="@operation"/></xsl:when>
                          <xsl:otherwise>=</xsl:otherwise>
                        </xsl:choose>
                      </TD>
                    </xsl:if>
                    <TD CLASS="FIELD_CELL"><xsl:value-of select="@value"/></TD>
                  </TR>
                </xsl:for-each>
              </TABLE>
            </TD></TR>
            <xsl:if test="@type='write' or @type='read'">
              <TR><TD CLASS="FIELD_CELL">
                <TABLE FRAME="border" CELLSPACING="0" CLASS="FIELD_TABLE">
                  <xsl:if test="@type='write'">
                    <TR>
                      <TH CLASS="FIELD_CELL">Write fields</TH>
                      <TH CLASS="FIELD_CELL">Values to write</TH>
                      <TH CLASS="FIELD_CELL">Operation</TH>
                    </TR>
                    <xsl:for-each select="WriteFields/Field">
                      <TR>
                        <TD CLASS="FIELD_CELL"><xsl:value-of select="@name"/></TD>
                        <TD CLASS="FIELD_CELL"><xsl:value-of select="@value"/></TD>
                        <TD CLASS="FIELD_CELL"><xsl:value-of select="@operation"/></TD>
                      </TR>
                    </xsl:for-each>
                  </xsl:if>
                  <xsl:if test="@type='read'">
                    <TR>
                      <TH CLASS="FIELD_CELL">Read fields</TH>
                      <TH CLASS="FIELD_CELL">Into variables</TH>
                    </TR>
                    <xsl:for-each select="ReadFields/Field">
                      <TR>
                        <TD CLASS="FIELD_CELL"><xsl:value-of select="@name"/></TD>
                        <TD CLASS="FIELD_CELL"><xsl:value-of select="@value"/></TD>
                      </TR>
                    </xsl:for-each>
                  </xsl:if>
                </TABLE>
              </TD></TR>
            </xsl:if>
            <xsl:if test="Expression!=''">
              <TR><TD CLASS="FIELD_CELL">
                <FONT CLASS="ERROR_MSG">Error Message: <xsl:value-of select="Expression"/></FONT>
              </TD></TR>
            </xsl:if>
          </TABLE>
          End <xsl:value-of select="substring-before($instr,' ')"/>
        </DIV>

      </xsl:if>

      <xsl:if test="Type='query' or Type='sql'">
        <B><xsl:value-of select="$instr"/></B>
        <xsl:value-of select="TableName"/>
      </xsl:if>

      <xsl:if test="Type='query'">
        <B> from query </B><xsl:value-of select="SelectTable"/>
      </xsl:if>

      <xsl:if test="Type='sql'">
        <B> from ( select </B><xsl:value-of select="Field"/>
        <B> from </B><xsl:value-of select="SelectTable"/>
        <xsl:if test="Where!=''">
          <B> where </B><xsl:value-of select="Where"/>
        </xsl:if>
        <B> )</B>
      </xsl:if>

    </TD>
  </TR>
</xsl:template>


<!-- ******** SELECT ******** -->

<xsl:template match="Item[@type='select']">
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px">

      <DIV ID="{concat(generate-id(), '_s')}" STYLE="display:block">
        <A CLASS="BATCH_LINK" TITLE="Expand node" href="javascript:swap_style('{generate-id()}')">
          <SPAN CLASS="PLUS">+</SPAN>
          Select from <xsl:value-of select="TableName"/>
        </A>
        <xsl:if test="condition[1]/Comment!=''">
        : <SPAN STYLE="color:green"><xsl:value-of select="condition[1]/Comment"/></SPAN>
        </xsl:if>
      </DIV>
      <DIV ID="{concat(generate-id(), '_h')}" STYLE="display:none">
        <A CLASS="BATCH_LINK" TITLE="Collapse node" href="javascript:swap_style('{generate-id()}')">
          <SPAN CLASS="PLUS">-</SPAN>
          Select <xsl:value-of select="Field"/> from
          <xsl:if test="Type!='fltb'">query </xsl:if>
          <xsl:value-of select="TableName"/>
          <xsl:if test="Start!='' or OrderKey!='' or While!='' or Filter!=''">
            <BR/>
            <xsl:if test="Start!=''">
              start <xsl:value-of select="Start"/>
              <xsl:if test="OrderKey!='' or While!='' or Filter!=''">;</xsl:if>
            </xsl:if>
            <xsl:if test="OrderKey!=''">
              order keys <xsl:value-of select="OrderKey"/>
              <xsl:if test="While!='' or Filter!=''">;</xsl:if>
            </xsl:if>
            <xsl:if test="While!=''">
              while <xsl:value-of select="While"/>
              <xsl:if test="Filter!=''">;</xsl:if>
            </xsl:if>
            <xsl:if test="Filter!=''">
              filter <xsl:value-of select="Filter"/>
            </xsl:if>
          </xsl:if>
          <xsl:if test="Where!='' or Order!='' or Group!=''">
            <BR/>
            <xsl:if test="Where!=''">
              where <xsl:value-of select="Where"/>
              <xsl:if test="Order!='' or Group!=''">;</xsl:if>
            </xsl:if>
            <xsl:if test="Order!=''">
              order by <xsl:value-of select="Order"/>
              <xsl:if test="Group!=''">;</xsl:if>
            </xsl:if>
            <xsl:if test="Group!=''">
              group by <xsl:value-of select="Group"/>
            </xsl:if>
          </xsl:if>
        </A>
        <TABLE WIDTH="100%" CELLPADDING="0" CELLSPACING="0" CLASS="Item">
          <xsl:apply-templates select="condition[1]/Item"/>
        </TABLE>
        End Select
      </DIV>

    </TD>
  </TR>
</xsl:template>


<!-- ********* REM ********* -->

<xsl:template match="Item[@type='remark']">
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px; color:green">
       <xsl:value-of select="Text"/>
    </TD>
  </TR>
</xsl:template>


<!-- ******** Other items ******** -->

<xsl:template match="Item">
  <TR CLASS="Item">
    <TD STYLE="padding-left:20px">
       <xsl:value-of select="Text"/>
    </TD>
  </TR>
</xsl:template>


</xsl:stylesheet>

