* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag1btw                                                        *
*              Treeview impianti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-09                                                      *
* Last revis.: 2015-10-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag1btw",oParentObject,m.pTipOp)
return(i_retval)

define class tgsag1btw as StdBatch
  * --- Local variables
  pTipOp = space(10)
  w_TRW_KEY = space(16)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_RIFKEY = space(100)
  w_POSIZ = 0
  w_LVLKEY = space(200)
  w_OBJ_MASK = .NULL.
  w_OBJ_CTRL = .NULL.
  w_NUMREC = 0
  w_ROOT = space(16)
  w_TIPOSELE = space(1)
  w_CODCLI = space(20)
  w_CODSED = space(5)
  w_CODIMP = space(15)
  w_CODCOMP = space(20)
  w_PROGCOMP = 0
  w_CODNOM = space(15)
  w_INDEX = 0
  w_TIPRIC = space(1)
  w_LVLKEYRIC = space(10)
  w_RETVAL = space(20)
  w_FLSEARCH = .f.
  * --- WorkFile variables
  ASS_ATTR_idx=0
  RELAZIONI_idx=0
  NODI_idx=0
  IMP_DETT_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pTipOp="Reload"
        * --- Create temporary table RELAZIONI
        i_nIdx=cp_AddTableDef('RELAZIONI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('gsag_ktw',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.RELAZIONI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Create temporary table NODI
        i_nIdx=cp_AddTableDef('NODI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.RELAZIONI_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.RELAZIONI_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"FIGLIO AS CODICE, GESTGUID, DESCRI, MODELLO,CPROWORD "," from "+i_cTable;
              +" where FIGLIO<>' '";
              +" order by CPROWORD";
              )
        this.NODI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Create temporary table NODI
        if cp_ExistTableDef('NODI')
           * add to existing table
          i_nIdx=cp_AddTableDef('NODI',.t.) && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSAG4KTW',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.t.)
          if this.NODI_idx=0
            this.NODI_idx=i_nIdx
          endif
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          i_nIdx=cp_AddTableDef('NODI',.t.) && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSAG4KTW',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.NODI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
        * --- Definizione parametri da passare alla CP_EXPDB
        * --- Cursore utilizzato dalla Tree View
        this.w_COUTCURS = this.oParentObject.w_CURSORNA
        * --- Cursore di partenza
        this.w_INPCURS = "QUERY"
        * --- Tabella di riferimento
        this.w_CRIFTABLE = "NODI"
        * --- Chiave anagrafica componenti
        this.w_RIFKEY = "CODICE"
        * --- Tabella di esplosione
        this.w_CEXPTABLE = "RELAZIONI"
        * --- Chiave della movimentazione contenente la struttura
        this.w_EXPKEY = "PADRE,FIGLIO"
        * --- Chiave ripetuta nella movimentazione contenente la struttura
        this.w_REPKEY = "FIGLIO"
        * --- Campi per espolosione
        this.w_EXPFIELD = ""
        * --- Altri campi (ordinamento)
        this.w_OTHERFLD = "CPROWORD"
        * --- Il nodo radice pu� essere un cliente, un impianto o un componente.
        * --- Il campo CODICE � costruito in questo modo:
        *     Cliente:
        *     '0'[CONCAT]CONTI.ANCODICE
        *     Sede:
        *     '1'[CONCAT][CASE( [NOTEMPTYSTR(IMP_MAST.IM__SEDE)],0 ,'#####' ,IMP_MAST.IM__SEDE)][CONCAT][SPACES(10)]
        *     Impianto:
        *     '2'[CONCAT]IMP_MAST.IMCODICE[CONCAT][SPACES(5)]
        *     Componnente
        *     '3'[CONCAT]IMP_DETT.IMCODICE[CONCAT][RIGHT('_____'[CONCAT][TRIM([STR([NVL( IMP_DETT.CPROWNUM ,0 )])])],5,1)]
        do case
          case NOT EMPTY( this.oParentObject.w_CODCON ) AND EMPTY( this.oParentObject.w_IMPIANTO ) AND EMPTY( this.oParentObject.w_COMPIMP )
            * --- Se � specificato solo il codice cliente w_CODICE bisogna selezionare il nodo cliente
            this.w_ROOT = "0"+this.oParentObject.w_CODCON
          case NOT EMPTY( this.oParentObject.w_CODCON ) AND NOT EMPTY( this.oParentObject.w_IMPIANTO ) AND EMPTY( this.oParentObject.w_COMPIMP )
            * --- Se � specificato l'impianto, ma senza il componente
            this.w_ROOT = "0"+this.oParentObject.w_CODCON
          otherwise
            * --- Se � specificato anche il componente
            this.w_ROOT = "0"+this.oParentObject.w_CODCON
        endcase
        * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
        VQ_EXEC("GSAG5KTW.VQR",this, this.w_INPCURS )
        SELECT ( this.w_INPCURS )
        if RECCOUNT()>0
          * --- Costruisco il cursore dei dati della Tree View
          CP_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,"",this.w_OTHERFLD)
          USE IN SELECT("__tmp__")
        else
          * --- Se la prima commessa specificata non ha elementi
          if Not Used ( this.oParentObject.w_CURSORNA )
            ah_ErrorMsg("L'impianto non ha nessun elemento",,"")
            i_retcode = 'stop'
            return
          endif
          Select ( this.oParentObject.w_CURSORNA )
          Zap
          ah_ErrorMsg("L'impianto non ha nessun elemento",,"")
          * --- Riempio la Treeview
        endif
        Select ( this.oParentObject.w_CURSORNA ) 
 go top
* --- Area Manuale = 
replace cpbmpname with  "BMP\Utepos.ico" FOR LEFT(CODICE,1)='0'
replace cpbmpname with  "BMP\WFLD.ico" FOR LEFT(CODICE,1)='1'
replace cpbmpname with  "BMP\azienda.ico" FOR LEFT(CODICE,1)='2'
replace cpbmpname with  "BMP\PRO-PARAMETRI.ico" FOR LEFT(CODICE,1)='3'
* --- Fine Area Manuale
        this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
        this.oParentObject.notifyevent("Esegui")
        USE IN SELECT("QUERY")
        * --- Drop temporary table TMPVEND1
        i_nIdx=cp_GetTableDefIdx('TMPVEND1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND1')
        endif
        * --- Drop temporary table TMPVEND2
        i_nIdx=cp_GetTableDefIdx('TMPVEND2')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND2')
        endif
      case this.pTipop="Close"
        * --- Elimina i cursori
        USE IN SELECT(this.oParentObject.w_CURSORNA)
        This.bUpdateParentObject = .F.
      case UPPER(LEFT(this.pTipOp,5))=="RIGHT"
        this.w_TIPOSELE = LEFT(g_oMenu.oParentObject.w_COD_TREE,1)
        do case
          case this.w_TIPOSELE="0"
            this.w_CODCLI = RIGHT(ALLTRIM(g_oMenu.oParentObject.w_COD_TREE), LEN(ALLTRIM(g_oMenu.oParentObject.w_COD_TREE))-1)
            this.w_CODSED = ""
            this.w_CODIMP = ""
            this.w_CODCOMP = ""
          case this.w_TIPOSELE="1"
            this.w_CODSED = RIGHT(ALLTRIM(g_oMenu.oParentObject.w_COD_TREE), LEN(ALLTRIM(g_oMenu.oParentObject.w_COD_TREE))-1)
            * --- Recupero il codice cliente
            this.w_TRW_KEY = ALLTRIM(g_oMenu.oParentObject.w_COD_TREE)
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CODCLI = RIGHT(this.w_RETVAL, LEN(this.w_RETVAL)-1)
            this.w_CODIMP = ""
            this.w_CODCOMP = ""
          case this.w_TIPOSELE="2"
            this.w_CODIMP = RIGHT(ALLTRIM(g_oMenu.oParentObject.w_COD_TREE), LEN(ALLTRIM(g_oMenu.oParentObject.w_COD_TREE))-1)
            * --- Recupero il codice sede
            this.w_TRW_KEY = ALLTRIM(g_oMenu.oParentObject.w_COD_TREE)
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CODSED = RIGHT(this.w_RETVAL, LEN(this.w_RETVAL)-1)
            * --- Recupero il codice cliente
            this.w_TRW_KEY = ALLTRIM(this.w_RETVAL)
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CODCLI = RIGHT(this.w_RETVAL, LEN(this.w_RETVAL)-1)
            this.w_CODCOMP = ""
          case this.w_TIPOSELE="3"
            this.w_CODCOMP = RIGHT(ALLTRIM(g_oMenu.oParentObject.w_COD_TREE), LEN(ALLTRIM(g_oMenu.oParentObject.w_COD_TREE))-1)
            this.w_PROGCOMP = VAL(RIGHT(this.w_CODCOMP, LEN(this.w_CODCOMP)-RAT("_", this.w_CODCOMP)))
            this.w_CODCOMP = LEFT(this.w_CODCOMP, AT("_", this.w_CODCOMP)-1)
            * --- Lettura descrizione associata al componente selezionato
            * --- Read from IMP_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.IMP_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.IMP_DETT_idx,2],.t.,this.IMP_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "IMDESCON"+;
                " from "+i_cTable+" IMP_DETT where ";
                    +"IMCODICE = "+cp_ToStrODBC(this.w_CODCOMP);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_PROGCOMP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                IMDESCON;
                from (i_cTable) where;
                    IMCODICE = this.w_CODCOMP;
                    and CPROWNUM = this.w_PROGCOMP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCOMP = NVL(cp_ToDate(_read_.IMDESCON),cp_NullValue(_read_.IMDESCON))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Recupero il codice impianto
            this.w_TRW_KEY = ALLTRIM(g_oMenu.oParentObject.w_COD_TREE)
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CODIMP = RIGHT(this.w_RETVAL, LEN(this.w_RETVAL)-1)
            * --- Recupero il codice sede
            this.w_TRW_KEY = ALLTRIM(this.w_RETVAL)
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CODSED = RIGHT(this.w_RETVAL, LEN(this.w_RETVAL)-1)
            * --- Recupero il codice cliente
            this.w_TRW_KEY = ALLTRIM(this.w_RETVAL)
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CODCLI = RIGHT(this.w_RETVAL, LEN(this.w_RETVAL)-1)
        endcase
        * --- Recupero il codice cliente, sede, impianto e compomente selezionato
        do case
          case RIGHT(this.pTipOp, 2)=="EE" OR RIGHT(this.pTipOp, 3)=="EES" OR RIGHT(this.pTipOp, 2)=="SA" OR RIGHT(this.pTipOp, 3)=="SAS"
            * --- Read from OFF_NOMI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NOCODICE"+;
                " from "+i_cTable+" OFF_NOMI where ";
                    +"NOCODCLI = "+cp_ToStrODBC(this.w_CODCLI);
                    +" and NOTIPCLI = "+cp_ToStrODBC("C");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NOCODICE;
                from (i_cTable) where;
                    NOCODCLI = this.w_CODCLI;
                    and NOTIPCLI = "C";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODNOM = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if RIGHT(this.pTipOp, 2)=="EE" OR RIGHT(this.pTipOp, 3)=="EES"
              this.w_OBJ_MASK = GSAG_KRA()
            else
              this.w_OBJ_MASK = GSAG_SAT()
            endif
            this.w_OBJ_MASK.w_STATO = "K"
            this.w_OBJ_MASK.w_DATINI = ctod("  -  -  ")
            this.w_OBJ_MASK.w_DATFIN = ctod("  -  -  ")
            this.w_OBJ_MASK.w_DATA_INI = cp_CharToDatetime(DTOC(i_IniDat)+" 00:00:00")
            this.w_OBJ_MASK.w_DATA_FIN = cp_CharToDatetime(DTOC(i_findat)+" 23:59:59")
            if this.w_TIPOSELE="0" OR this.w_TIPOSELE="1"
              SetValueLinked("MT", this.w_OBJ_MASK, "w_CODNOM", this.w_CODNOM)
            endif
            if (!EMPTY(NVL(this.w_CODSED, " ")) AND VAL(this.w_TIPOSELE)<=1) OR ( !EMPTY(NVL(this.w_CODSED, " ")) AND VAL(this.w_TIPOSELE)>1 AND (RIGHT(this.pTipOp, 3)=="EES" OR RIGHT(this.pTipOp, 3)=="SAS"))
              SetValueLinked("MT", this.w_OBJ_MASK, "w_CODSED", this.w_CODSED, this.w_OBJ_MASK.w_TIPNOM, this.w_OBJ_MASK.w_CODCLI)
            endif
            if !EMPTY(NVL(this.w_CODIMP, " "))
              SetValueLinked("MT", this.w_OBJ_MASK, "w_IMPIANTO", this.w_CODIMP)
            endif
            if !EMPTY(NVL(this.w_CODCOMP, " "))
              * --- Il campo non ha il link ma � simulato con la afterinput
              this.w_OBJ_CTRL = this.w_OBJ_MASK.GetCtrl("w_COMPIMP")
              this.w_OBJ_CTRL.Value = this.w_CODCOMP
              this.w_OBJ_CTRL.Valid()     
            endif
            if RIGHT(this.pTipOp, 2)=="EE" OR RIGHT(this.pTipOp, 3)=="EES"
              this.w_OBJ_MASK.Notifyevent("Ricerca")     
            endif
          case RIGHT(this.pTipOp, 2)=="EC" OR RIGHT(this.pTipOp, 2)=="SC"
            if RIGHT(this.pTipOp, 2)=="EC"
              this.w_OBJ_MASK = GSAG_KVC()
              do case
                case this.w_TIPOSELE="0" OR this.w_TIPOSELE="1"
                  SetValueLinked("MT", this.w_OBJ_MASK, "w_CODCLI", this.w_CODCLI, this.w_OBJ_MASK.w_TIPCON)
                  SetValueLinked("MT", this.w_OBJ_MASK, "w_CODICE", this.w_CODCLI, this.w_OBJ_MASK.w_TIPCON)
                  SetValueLinked("MT", this.w_OBJ_MASK, "w_CODNOM", this.w_CODCLI, this.w_OBJ_MASK.w_TIPCON)
                  SetValueLinked("MT", this.w_OBJ_MASK, "w_CODSED", this.w_CODSED, this.w_OBJ_MASK.w_TIPCON, this.w_OBJ_MASK.w_CODCLI)
                  this.w_OBJ_MASK.notifyevent("Search")
                case this.w_TIPOSELE="2"
                  SetValueLinked("MT", this.w_OBJ_MASK, "w_IMPIANTO", this.w_CODIMP)
                  this.w_OBJ_MASK.notifyevent("Search")
                case this.w_TIPOSELE="3"
                  if !EMPTY(NVL(this.w_CODIMP, " "))
                    SetValueLinked("MT", this.w_OBJ_MASK, "w_IMPIANTO", this.w_CODIMP)
                  endif
                  if !EMPTY(NVL(this.w_CODCOMP, " "))
                    * --- Il campo non ha il link ma � simulato con la afterinput
                    this.w_OBJ_CTRL = this.w_OBJ_MASK.GetCtrl("w_COMPIMP")
                    this.w_OBJ_CTRL.Value = this.w_CODCOMP
                    this.w_OBJ_CTRL.Valid()     
                    this.w_OBJ_MASK.notifyevent("Search")
                  endif
              endcase
            else
              this.w_OBJ_MASK = GSAG_SCO()
              if this.w_TIPOSELE="0" OR this.w_TIPOSELE="1"
                SetValueLinked("MT", this.w_OBJ_MASK, "w_ELCODCLI", this.w_CODCLI, this.w_OBJ_MASK.w_TIPCON)
                SetValueLinked("MT", this.w_OBJ_MASK, "w_CODSED", this.w_CODSED, this.w_OBJ_MASK.w_TIPCON, this.w_OBJ_MASK.w_ELCODCLI)
              endif
              if !EMPTY(NVL(this.w_CODIMP, " "))
                SetValueLinked("MT", this.w_OBJ_MASK, "w_IMPIANTO", this.w_CODIMP)
                if !EMPTY(NVL(this.w_CODCOMP, " "))
                  * --- Il campo non ha il link ma � simulato con la afterinput
                  this.w_OBJ_CTRL = this.w_OBJ_MASK.GetCtrl("w_COMPIMP")
                  this.w_OBJ_CTRL.Value = this.w_CODCOMP
                  this.w_OBJ_CTRL.Valid()     
                endif
              endif
            endif
        endcase
      case this.pTipop="Search"
        if !USED ( this.oParentObject.w_CURSORNA )
          * --- Treeview vuota, notifico l'evento per riempirla e poi faccio la ricerca
          this.oParentObject.NotifyEvent("Reload")
        endif
        * --- Ricerca del Nodo
        * --- Cerco il Nodo nel Cursore
        Select ( this.oParentObject.w_CURSORNA )
        Go Top
        Locate For LEFT(CODICE,1)="3" AND ALLTRIM(Descri)==ALLTRIM(this.oParentObject.w_COMPIMP)
        if Found()
          this.w_LVLKEY = LvlKey
          * --- Se trovato scorro il cursore ed esplodo ogni elemento
          Select ( this.oParentObject.w_CURSORNA )
          Go Top
          this.w_INDEX = 1
          do while ALLTRIM(this.oParentObject.w_COMPIMP)<>ALLTRIM(Descri)
            * --- Lo espando solo se il suo LvlKey � prefisso del Lvlkey da visualizzare
            if Alltrim(LvlKey) = Left ( Alltrim(this.w_LVLkEY) , Len( Alltrim(LvlKey) ) )
              this.oParentObject.w_TREEVIEW.oTree.Nodes(This.w_Index).Expanded = .t.
            endif
            this.w_INDEX = this.w_INDEX+1
            Select ( this.oParentObject.w_CURSORNA )
            Skip
          enddo
          * --- Lo seleziono
          this.oParentObject.w_TREEVIEW.oTree.Nodes(this.w_INDEX).selected = .T.
          * --- Refresho la Griglia
          this.oParentObject.w_TREEVIEW.SetFocus()     
        else
          ah_ErrorMsg( "Nessun elemento trovato" , 48)
        endif
    endcase
    * --- Drop temporary table NODI
    i_nIdx=cp_GetTableDefIdx('NODI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('NODI')
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FLSEARCH = .T.
    this.w_RETVAL = ""
    * --- Recupero il record del codice selezionato
    this.w_TIPRIC = ALLTRIM( STR( VAL( LEFT(this.w_TRW_KEY ,1) )-1 ) )
    SELECT (g_omenu.oParentObject.w_TREEVIEW.cCursor)
    L_OLDREC=RECNO()
    GO TOP
    LOCATE FOR CODICE=this.w_TRW_KEY
    if FOUND()
      this.w_LVLKEYRIC = ALLTRIM(LVLKEY)
      this.w_LVLKEYRIC = LEFT(this.w_LVLKEYRIC, RAT(".", this.w_LVLKEYRIC)-1)
      * --- Effettuo la ricerca del padre del record selezionato
      do while this.w_FLSEARCH
        GO TOP
        LOCATE FOR LVLKEY=this.w_LVLKEYRIC AND LEFT(CODICE,1)=this.w_TIPRIC
        if FOUND()
          this.w_RETVAL = ALLTRIM(CODICE)
          this.w_FLSEARCH = .F.
        else
          * --- Nel caso di componenti devo risalire di un livello poich� potrebbero essere annidati
          if this.w_TIPRIC="2" AND EMPTY(this.w_RETVAL) AND LEN(this.w_LVLKEYRIC)>2
            this.w_LVLKEYRIC = LEFT(this.w_LVLKEYRIC, RAT(".", this.w_LVLKEYRIC)-1)
            this.w_FLSEARCH = .T.
          else
            this.w_FLSEARCH = .F.
          endif
        endif
      enddo
    endif
    GO (L_OLDREC)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ASS_ATTR'
    this.cWorkTables[2]='*RELAZIONI'
    this.cWorkTables[3]='*NODI'
    this.cWorkTables[4]='IMP_DETT'
    this.cWorkTables[5]='OFF_NOMI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
