* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcs                                                        *
*              Calcola prezzo, contratto e sconti                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-02                                                      *
* Last revis.: 2010-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcs",oParentObject)
return(i_retval)

define class tgsag_bcs as StdBatch
  * --- Local variables
  w_RETVAL = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RETVAL = 0
    if !EMPTY(this.oParentObject.w_ELCODART)
      if isAHE()
        this.w_RETVAL = this.oParentObject.w_LIPREZZO
      else
        DECLARE L_ArrRet (16,1)
        this.w_RETVAL = gsar_bpz(this, this.oParentObject.w_ELCODART, this.oParentObject.w_ELCODCLI, "C", this.oParentObject.w_ELCODLIS, this.oParentObject.w_ELQTAMOV, this.oParentObject.w_ELCODVAL, this.oParentObject.w_ELDATINI, this.oParentObject.w_ELUNIMIS)
        this.oParentObject.w_ELSCONT1 = L_ArrRet[1,1]
        this.oParentObject.w_ELSCONT2 = L_ArrRet[2,1]
        this.oParentObject.w_ELSCONT3 = L_ArrRet[3,1]
        this.oParentObject.w_ELSCONT4 = L_ArrRet[4,1]
        this.oParentObject.w_ELCONCOD = L_ArrRet[9,1]
        RELEASE L_ArrRet
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
