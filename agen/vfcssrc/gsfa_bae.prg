* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bae                                                        *
*              Associazione attivit� eventi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-23                                                      *
* Last revis.: 2012-04-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERATT,pCURSORE,pNOMSG,pANNEVE,pSEREVE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bae",oParentObject,m.pSERATT,m.pCURSORE,m.pNOMSG,m.pANNEVE,m.pSEREVE)
return(i_retval)

define class tgsfa_bae as StdBatch
  * --- Local variables
  pSERATT = space(20)
  pCURSORE = space(10)
  pNOMSG = space(1)
  pANNEVE = space(4)
  pSEREVE = space(10)
  w_EV__ANNO = space(4)
  w_EVSERIAL = space(10)
  w_OKPROC = .f.
  w_OKAGG = .f.
  * --- WorkFile variables
  ASS_ATEV_idx=0
  ANEVENTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Associa attivit� a elenco eventi presenti nel cursore
    * --- Seriale attivit�
    * --- Cursore eventi
    * --- in alternativa chiave del singolo evento da associare
    if Vartype(this.pCURSORE)="C" and used((this.pCURSORE))
      * --- Verifico se devo processare eventi
      if this.pNOMSG=" "
        this.w_OKAGG = Ah_yesno("Si desidera considerare l'evento come processato?")
      else
        this.w_OKAGG = iif(this.pNOMSG="S",.t.,.f.)
      endif
       
 Select (this.pCURSORE) 
 Go top 
 Scan
      this.w_EV__ANNO = EV__ANNO
      this.w_EVSERIAL = EVSERIAL
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
       
 Endscan
    endif
    if Vartype(this.pSEREVE)="C" and Vartype(this.pANNEVE)="C" 
      if VarType(this.pNOMSG)="L"
        this.w_OKAGG = Ah_yesno("Si desidera processare gli eventi?")
      else
        this.w_OKAGG = iif(this.pNOMSG="S",.t.,.f.)
      endif
      this.w_EV__ANNO = this.pANNEVE
      this.w_EVSERIAL = this.pSEREVE
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_037D6FE8
    bErr_037D6FE8=bTrsErr
    this.Try_037D6FE8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_037D6FE8
    * --- End
    if this.w_OKAGG
      * --- Write into ANEVENTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ANEVENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"EV_STATO ="+cp_NullLink(cp_ToStrODBC("P"),'ANEVENTI','EV_STATO');
            +i_ccchkf ;
        +" where ";
            +"EVSERIAL = "+cp_ToStrODBC(this.w_EVSERIAL);
            +" and EV__ANNO = "+cp_ToStrODBC(this.w_EV__ANNO);
               )
      else
        update (i_cTable) set;
            EV_STATO = "P";
            &i_ccchkf. ;
         where;
            EVSERIAL = this.w_EVSERIAL;
            and EV__ANNO = this.w_EV__ANNO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc
  proc Try_037D6FE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ASS_ATEV
    i_nConn=i_TableProp[this.ASS_ATEV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATEV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ASS_ATEV_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AECODATT"+",AECODEVE"+",AE__ANNO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.pSERATT),'ASS_ATEV','AECODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVSERIAL),'ASS_ATEV','AECODEVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EV__ANNO),'ASS_ATEV','AE__ANNO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AECODATT',this.pSERATT,'AECODEVE',this.w_EVSERIAL,'AE__ANNO',this.w_EV__ANNO)
      insert into (i_cTable) (AECODATT,AECODEVE,AE__ANNO &i_ccchkf. );
         values (;
           this.pSERATT;
           ,this.w_EVSERIAL;
           ,this.w_EV__ANNO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pSERATT,pCURSORE,pNOMSG,pANNEVE,pSEREVE)
    this.pSERATT=pSERATT
    this.pCURSORE=pCURSORE
    this.pNOMSG=pNOMSG
    this.pANNEVE=pANNEVE
    this.pSEREVE=pSEREVE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ASS_ATEV'
    this.cWorkTables[2]='ANEVENTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERATT,pCURSORE,pNOMSG,pANNEVE,pSEREVE"
endproc
