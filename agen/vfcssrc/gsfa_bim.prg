* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bim                                                        *
*              Import mail formato adhoc                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-25                                                      *
* Last revis.: 2010-04-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPath,pCodPer,pCodGru,pErr_Log,pEventType
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bim",oParentObject,m.pPath,m.pCodPer,m.pCodGru,m.pErr_Log,m.pEventType)
return(i_retval)

define class tgsfa_bim as StdBatch
  * --- Local variables
  pPath = space(254)
  pCodPer = space(5)
  pCodGru = space(5)
  pErr_Log = .NULL.
  pEventType = space(10)
  w_NUM_ITEM = 0
  w_EVCURNAM = space(10)
  w_FACURNAME = space(10)
  w_TETIPDIR = space(1)
  w_IMXMLDOM = .NULL.
  w_FLERRORS = .f.
  w_MAILDATE = ctot("")
  w_MAILFROM = space(254)
  w_MAILSUBJ = space(254)
  w_MAILBODY = space(0)
  w_EVSERIAL = space(10)
  w_NSER_ATT = 0
  w_NUMRECIP = 0
  w_NUMATTAC = 0
  w_ATTACNAM = space(254)
  * --- WorkFile variables
  TIPEVENT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FLERRORS = .F.
    this.w_NSER_ATT = 0
    LOCAL L_OLDERR, L_Err
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    private L_OldPath
    L_OldPath = Sys(5)+Sys(2003)
    CD (this.pPath)
    if L_Err
      this.pErr_Log.AddMsgLog("Percorso inesistente o non raggiungibile")     
      this.w_FLERRORS = .T.
    else
      this.w_NUM_ITEM = ADIR(L_ArrFile, "*.xml")
    endif
    CD (L_OldPath)
    release L_OldPath
    if this.w_NUM_ITEM>0
      this.w_IMXMLDOM = CREATEOBJECT("Msxml2.DOMDocument")
      if L_Err
        this.pErr_Log.AddMsgLog("Impossibile trovare componente MSXML2")     
        this.w_FLERRORS = .T.
      endif
      if !this.w_FLERRORS
        this.w_EVCURNAM = SYS(2015)
        this.w_FACURNAME = SYS(2015)
        vq_exec("..\agen\exe\QUERY\GSFA_BIM.VQR",this,this.w_EVCURNAM)
        CREATE CURSOR (this.w_FACURNAME) ( EVSERIAL C(10), EV__PATH C(254) )
        * --- Read from TIPEVENT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIPEVENT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2],.t.,this.TIPEVENT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TETIPDIR"+;
            " from "+i_cTable+" TIPEVENT where ";
                +"TETIPEVE = "+cp_ToStrODBC(this.pEventType);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TETIPDIR;
            from (i_cTable) where;
                TETIPEVE = this.pEventType;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TETIPDIR = NVL(cp_ToDate(_read_.TETIPDIR),cp_NullValue(_read_.TETIPDIR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    if !this.w_FLERRORS
      * --- Analisi file da importare
      do while this.w_NUM_ITEM>0
        * --- Caricamento file nell'oggetto DOM
        this.w_IMXMLDOM.Load(ADDBS(ALLTRIM(this.pPath))+ALLTRIM(L_ArrFile(this.w_NUM_ITEM, 1)))     
        if !IsNull(this.w_IMXMLDOM.DocumentElement)
          if UPPER(ALLTRIM(this.w_IMXMLDOM.DocumentElement.NodeName))=UPPER("AdHocMailFormat")
            this.w_NSER_ATT = this.w_NSER_ATT + 1
            this.w_EVSERIAL = ""
            this.w_MAILDATE = ""
            this.w_MAILFROM = ""
            this.w_MAILSUBJ = ""
            this.w_MAILBODY = ""
            this.w_EVSERIAL = "E"+RIGHT("000000000"+ALLTRIM(STR(this.w_NSER_ATT)), 9)
            this.w_MAILDATE = this.w_IMXMLDOM.DocumentElement.SelectNodes("Date").Item(0).Text
            this.w_MAILFROM = this.w_IMXMLDOM.DocumentElement.SelectNodes("From").Item(0).Text
            if !isNull(this.w_IMXMLDOM.DocumentElement.SelectNodes("Subject").Item(0))
              * --- Mail senza nessun oggetto non hanno il relativo tag
              this.w_MAILSUBJ = this.w_IMXMLDOM.DocumentElement.SelectNodes("Subject").Item(0).Text
            endif
            if !isNull(this.w_IMXMLDOM.DocumentElement.SelectNodes("BodyText").Item(0))
              * --- Mail senza nessun testo non hanno il relativo tag
              this.w_MAILBODY = this.w_IMXMLDOM.DocumentElement.SelectNodes("BodyText").Item(0).Text
            endif
            if this.w_TETIPDIR="U"
              * --- Mail in uscita occorre inserire un evento per ogni destinatario in a e cc con solito seriale
              this.w_NUMRECIP = -1
              this.w_NUMRECIP = this.w_IMXMLDOM.DocumentElement.SelectNodes("Recipients/Cc").Length
              do while this.w_NUMRECIP>0
                this.w_MAILFROM = this.w_IMXMLDOM.DocumentElement.SelectNodes("Recipients/Cc").Item(this.w_NUMRECIP-1).Text
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_NUMRECIP = this.w_NUMRECIP - 1
              enddo
              this.w_NUMRECIP = -1
              this.w_NUMRECIP = this.w_IMXMLDOM.DocumentElement.SelectNodes("Recipients/To").Length
              do while this.w_NUMRECIP>0
                this.w_MAILFROM = this.w_IMXMLDOM.DocumentElement.SelectNodes("Recipients/To").Item(this.w_NUMRECIP-1).Text
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_NUMRECIP = this.w_NUMRECIP - 1
              enddo
            else
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Inserisco sicuramente un allegato per far riferimento alla mail in formato html
            *     Se esistono inserisco anche ulteriori allegati alla mail o allegati derivanti
            *     da immagini annegate nel corpo html della mail
            INSERT INTO ( this.w_FACURNAME) VALUES (this.w_EVSERIAL, ADDBS(ALLTRIM(this.pPath))+ADDBS(JUSTSTEM(ALLTRIM(L_ArrFile(this.w_NUM_ITEM, 1))))+FORCEEXT(ALLTRIM(L_ArrFile(this.w_NUM_ITEM, 1)), "html"))
            this.w_NUMATTAC = -1
            this.w_NUMATTAC = this.w_IMXMLDOM.DocumentElement.SelectNodes("Attachments/File").Length
            do while this.w_NUMATTAC>0
              this.w_ATTACNAM = this.w_IMXMLDOM.DocumentElement.SelectNodes("Attachments/File").Item(this.w_NUMATTAC - 1).Text
              INSERT INTO ( this.w_FACURNAME) VALUES (this.w_EVSERIAL, this.w_ATTACNAM)
              this.w_NUMATTAC = this.w_NUMATTAC - 1
            enddo
          else
            this.pErr_Log.AddMsgLog("Tipo Xml non appartenente alla categoria e-mail ad hoc")     
          endif
          DELETE FILE ( ADDBS(ALLTRIM(this.pPath))+ALLTRIM(L_ArrFile(this.w_NUM_ITEM, 1)) )
        else
          this.pErr_Log.AddMsgLog("Xml non valido")     
        endif
        this.w_NUM_ITEM = this.w_NUM_ITEM - 1
      enddo
      * --- Chiamata a routine standard d'inserimento eventi
      this.w_FLERRORS = this.w_FLERRORS OR !GSFA_BEV(this, this.w_EVCURNAM , this.w_FACURNAME, this.pErr_Log, "X")
    endif
    release L_ArrFile
    ON ERROR &L_OLDERR
    i_retcode = 'stop'
    i_retval = IIF(this.w_FLERRORS, "E", "")
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    INSERT INTO ( this.w_EVCURNAM ) (EVSERIAL, EVTIPEVE, EVDIREVE, EVOGGETT, EV__NOTE, EV_EMAIL, EVDATINI, EVPERSON, EVGRUPPO ) VALUES (this.w_EVSERIAL, this.pEventType, this.w_TETIPDIR, this.w_MAILSUBJ, this.w_MAILBODY, this.w_MAILFROM, CTOT(CHRTRAN(this.w_MAILDATE, ".",":")), ALLTRIM(this.pCodPer), ALLTRIM(this.pCodGru) )
  endproc


  proc Init(oParentObject,pPath,pCodPer,pCodGru,pErr_Log,pEventType)
    this.pPath=pPath
    this.pCodPer=pCodPer
    this.pCodGru=pCodGru
    this.pErr_Log=pErr_Log
    this.pEventType=pEventType
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIPEVENT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPath,pCodPer,pCodGru,pErr_Log,pEventType"
endproc
