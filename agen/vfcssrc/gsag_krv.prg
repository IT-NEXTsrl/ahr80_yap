* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_krv                                                        *
*              Rinvio                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-20                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_krv",oParentObject))

* --- Class definition
define class tgsag_krv as StdForm
  Top    = 15
  Left   = 50

  * --- Standard Properties
  Width  = 584
  Height = 535
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=116008809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  CAUMATTI_IDX = 0
  PAR_AGEN_IDX = 0
  PRO_ITER_IDX = 0
  cPrg = "gsag_krv"
  cComment = "Rinvio"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PAR_AGEN = space(5)
  w_PACALDAT = space(1)
  w_DATARINV = ctod('  /  /  ')
  o_DATARINV = ctod('  /  /  ')
  w_ORERINV = space(2)
  o_ORERINV = space(2)
  w_MINRINV = space(2)
  w_TIPGEN = space(1)
  o_TIPGEN = space(1)
  w_TIPOATT = space(20)
  w_CAITER = space(10)
  w_CONFERMA = space(1)
  w_ATOGGETT = space(254)
  w_FL_ATTCOMPL = space(1)
  o_FL_ATTCOMPL = space(1)
  w_ATNOTPIA = space(0)
  w_FLGNOT = space(1)
  o_FLGNOT = space(1)
  w_ATSERIAL = space(15)
  o_ATSERIAL = space(15)
  w_ATCAUATT = space(20)
  w_ATOGGETTOR = space(254)
  w_DATINIOR = ctod('  /  /  ')
  w_OREINIOR = space(2)
  w_MININIOR = space(2)
  w_GIOINIOR = space(3)
  w_GIORINV = space(3)
  w_CARAGGST = space(1)
  w_PACHKFES = space(1)
  w_PACHKCPL = space(1)
  w_CACHKINI = space(1)
  o_CACHKINI = space(1)
  w_CACHKFOR = space(1)
  w_ITDECR = space(60)
  w_FLGEDIT = space(1)
  w_CAUATT = space(20)
  w_STATUS = space(1)
  w_PACHKATT = 0
  w_ONLYFIRST = space(1)
  w_FRINV = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')

  * --- Children pointers
  GSAG_MRP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSAG_MRP additive
    with this
      .Pages(1).addobject("oPag","tgsag_krvPag1","gsag_krv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATARINV_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSAG_MRP
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='PAR_AGEN'
    this.cWorkTables[3]='PRO_ITER'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSAG_MRP = CREATEOBJECT('stdDynamicChild',this,'GSAG_MRP',this.oPgFrm.Page1.oPag.oLinkPC_1_23)
    this.GSAG_MRP.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAG_MRP)
      this.GSAG_MRP.DestroyChildrenChain()
      this.GSAG_MRP=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_23')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAG_MRP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAG_MRP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAG_MRP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAG_MRP.SetKey(;
            .w_ATSERIAL,"PASERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAG_MRP.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATSERIAL,"PASERIAL";
             )
      .WriteTo_GSAG_MRP()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAG_MRP)
        i_f=.GSAG_MRP.BuildFilter()
        if !(i_f==.GSAG_MRP.cQueryFilter)
          i_fnidx=.GSAG_MRP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAG_MRP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAG_MRP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAG_MRP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAG_MRP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAG_MRP()
  if at('gsag_mrp',lower(this.GSAG_MRP.class))<>0
    if this.GSAG_MRP.w_OB_TEST<>this.w_DATARINV
      this.GSAG_MRP.w_OB_TEST = this.w_DATARINV
      this.GSAG_MRP.mCalc(.t.)
    endif
  endif
  return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSAG_MRP(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSAG_MRP.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSAG_MRP(i_ask)
    if this.GSAG_MRP.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP))
      cp_BeginTrs()
      this.GSAG_MRP.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PAR_AGEN=space(5)
      .w_PACALDAT=space(1)
      .w_DATARINV=ctod("  /  /  ")
      .w_ORERINV=space(2)
      .w_MINRINV=space(2)
      .w_TIPGEN=space(1)
      .w_TIPOATT=space(20)
      .w_CAITER=space(10)
      .w_CONFERMA=space(1)
      .w_ATOGGETT=space(254)
      .w_FL_ATTCOMPL=space(1)
      .w_ATNOTPIA=space(0)
      .w_FLGNOT=space(1)
      .w_ATSERIAL=space(15)
      .w_ATCAUATT=space(20)
      .w_ATOGGETTOR=space(254)
      .w_DATINIOR=ctod("  /  /  ")
      .w_OREINIOR=space(2)
      .w_MININIOR=space(2)
      .w_GIOINIOR=space(3)
      .w_GIORINV=space(3)
      .w_CARAGGST=space(1)
      .w_PACHKFES=space(1)
      .w_PACHKCPL=space(1)
      .w_CACHKINI=space(1)
      .w_CACHKFOR=space(1)
      .w_ITDECR=space(60)
      .w_FLGEDIT=space(1)
      .w_CAUATT=space(20)
      .w_STATUS=space(1)
      .w_PACHKATT=0
      .w_ONLYFIRST=space(1)
      .w_FRINV=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_DATARINV=oParentObject.w_DATARINV
      .w_ORERINV=oParentObject.w_ORERINV
      .w_MINRINV=oParentObject.w_MINRINV
      .w_TIPGEN=oParentObject.w_TIPGEN
      .w_TIPOATT=oParentObject.w_TIPOATT
      .w_CAITER=oParentObject.w_CAITER
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_ATOGGETT=oParentObject.w_ATOGGETT
      .w_FL_ATTCOMPL=oParentObject.w_FL_ATTCOMPL
      .w_ATNOTPIA=oParentObject.w_ATNOTPIA
      .w_FLGNOT=oParentObject.w_FLGNOT
      .w_ATSERIAL=oParentObject.w_ATSERIAL
      .w_ATCAUATT=oParentObject.w_ATCAUATT
      .w_ATOGGETTOR=oParentObject.w_ATOGGETTOR
      .w_DATINIOR=oParentObject.w_DATINIOR
      .w_OREINIOR=oParentObject.w_OREINIOR
      .w_MININIOR=oParentObject.w_MININIOR
      .w_CACHKINI=oParentObject.w_CACHKINI
      .w_CACHKFOR=oParentObject.w_CACHKFOR
      .w_ONLYFIRST=oParentObject.w_ONLYFIRST
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,3,.f.)
        .w_ORERINV = '08'
          .DoRTCalc(5,5,.f.)
        .w_TIPGEN = 'T'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_TIPOATT))
          .link_1_10('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CAITER))
          .link_1_11('Full')
        endif
      .GSAG_MRP.NewDocument()
      .GSAG_MRP.ChangeRow('1',1,.w_ATSERIAL,"PASERIAL")
      if not(.GSAG_MRP.bLoaded)
        .GSAG_MRP.SetKey(.w_ATSERIAL,"PASERIAL")
      endif
          .DoRTCalc(9,19,.f.)
        .w_GIOINIOR = iif(!EMPTY(.w_DATINIOR), g_GIORNO[DOW(.w_DATINIOR)] , SPACE(3))
        .w_GIORINV = iif(!EMPTY(.w_DATARINV), g_GIORNO[DOW(.w_DATARINV)] , SPACE(3))
          .DoRTCalc(22,24,.f.)
        .w_CACHKINI = .w_PACALDAT
          .DoRTCalc(26,27,.f.)
        .w_FLGEDIT = 'N'
        .w_CAUATT = .w_ATCAUATT
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_CAUATT))
          .link_1_49('Full')
        endif
          .DoRTCalc(30,33,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSAG_MRP.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAG_MRP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATARINV=.w_DATARINV
      .oParentObject.w_ORERINV=.w_ORERINV
      .oParentObject.w_MINRINV=.w_MINRINV
      .oParentObject.w_TIPGEN=.w_TIPGEN
      .oParentObject.w_TIPOATT=.w_TIPOATT
      .oParentObject.w_CAITER=.w_CAITER
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_ATOGGETT=.w_ATOGGETT
      .oParentObject.w_FL_ATTCOMPL=.w_FL_ATTCOMPL
      .oParentObject.w_ATNOTPIA=.w_ATNOTPIA
      .oParentObject.w_FLGNOT=.w_FLGNOT
      .oParentObject.w_ATSERIAL=.w_ATSERIAL
      .oParentObject.w_ATCAUATT=.w_ATCAUATT
      .oParentObject.w_ATOGGETTOR=.w_ATOGGETTOR
      .oParentObject.w_DATINIOR=.w_DATINIOR
      .oParentObject.w_OREINIOR=.w_OREINIOR
      .oParentObject.w_MININIOR=.w_MININIOR
      .oParentObject.w_CACHKINI=.w_CACHKINI
      .oParentObject.w_CACHKFOR=.w_CACHKFOR
      .oParentObject.w_ONLYFIRST=.w_ONLYFIRST
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,11,.t.)
        if .o_FLGNOT<>.w_FLGNOT
            .w_ATNOTPIA = iif(.w_FLGNOT='N',space(0),.w_ATNOTPIA)
        endif
        if .o_TIPGEN<>.w_TIPGEN
            .w_FLGNOT = iif(.w_TIPGEN='T','S','U')
        endif
        if  .o_DATARINV<>.w_DATARINV
          .WriteTo_GSAG_MRP()
        endif
        if .o_FL_ATTCOMPL<>.w_FL_ATTCOMPL
          .Calculate_CTUYDULNVV()
        endif
        .DoRTCalc(14,19,.t.)
            .w_GIOINIOR = iif(!EMPTY(.w_DATINIOR), g_GIORNO[DOW(.w_DATINIOR)] , SPACE(3))
        if .o_DATARINV<>.w_DATARINV
            .w_GIORINV = iif(!EMPTY(.w_DATARINV), g_GIORNO[DOW(.w_DATARINV)] , SPACE(3))
        endif
        .DoRTCalc(22,25,.t.)
        if .o_CACHKINI<>.w_CACHKINI.or. .o_TIPGEN<>.w_TIPGEN
            .w_CACHKFOR = iif( IsAlt(), 'S' , ' ')
        endif
        .DoRTCalc(27,28,.t.)
            .w_CAUATT = .w_ATCAUATT
          .link_1_49('Full')
        .DoRTCalc(30,31,.t.)
        if .o_ORERINV<>.w_ORERINV.or. .o_TIPGEN<>.w_TIPGEN
            .w_ONLYFIRST = IIF(Empty(.w_ORERINV) ,'N','T')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_ATSERIAL<>.o_ATSERIAL
          .Save_GSAG_MRP(.t.)
          .GSAG_MRP.NewDocument()
          .GSAG_MRP.ChangeRow('1',1,.w_ATSERIAL,"PASERIAL")
          if not(.GSAG_MRP.bLoaded)
            .GSAG_MRP.SetKey(.w_ATSERIAL,"PASERIAL")
          endif
        endif
      endwith
      this.DoRTCalc(33,34,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_DNTIPTJHRD()
    with this
          * --- Azzera le note
          .w_ATNOTPIA = ' '
    endwith
  endproc
  proc Calculate_CTUYDULNVV()
    with this
          * --- Controllo su check Completa attivit� selezionata
          GSAG_BCC(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_WJSMTHQBNK()
    with this
          * --- Rivalorizza w_ATOGGETT
          .w_ATOGGETT = .w_ATOGGETTOR
    endwith
  endproc
  proc Calculate_XXOQVDUNVF()
    with this
          * --- Verifica congruit� Iter
          GSAG_BCC(this;
              ,'I';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCACHKINI_1_43.enabled = this.oPgFrm.Page1.oPag.oCACHKINI_1_43.mCond()
    this.oPgFrm.Page1.oPag.oCACHKFOR_1_44.enabled = this.oPgFrm.Page1.oPag.oCACHKFOR_1_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAITER_1_11.visible=!this.oPgFrm.Page1.oPag.oCAITER_1_11.mHide()
    this.oPgFrm.Page1.oPag.oFLGNOT_1_18.visible=!this.oPgFrm.Page1.oPag.oFLGNOT_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCACHKINI_1_43.visible=!this.oPgFrm.Page1.oPag.oCACHKINI_1_43.mHide()
    this.oPgFrm.Page1.oPag.oCACHKFOR_1_44.visible=!this.oPgFrm.Page1.oPag.oCACHKFOR_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oITDECR_1_47.visible=!this.oPgFrm.Page1.oPag.oITDECR_1_47.mHide()
    this.oPgFrm.Page1.oPag.oFLGEDIT_1_48.visible=!this.oPgFrm.Page1.oPag.oFLGEDIT_1_48.mHide()
    this.oPgFrm.Page1.oPag.oONLYFIRST_1_52.visible=!this.oPgFrm.Page1.oPag.oONLYFIRST_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Azzera")
          .Calculate_DNTIPTJHRD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_WJSMTHQBNK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CAITER Changed")
          .Calculate_XXOQVDUNVF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_krv
    if CEVENT='w_FLGEDIT Changed' and This.w_FLGEDIT='S'
       =opengest('M','GSAG_AAT','ATSERIAL',This.w_ATSERIAL)
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES,PACHKCPL,PACHKATT,PACALDAT";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKFES,PACHKCPL,PACHKATT,PACALDAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
      this.w_PACHKCPL = NVL(_Link_.PACHKCPL,space(1))
      this.w_PACHKATT = NVL(_Link_.PACHKATT,0)
      this.w_PACALDAT = NVL(_Link_.PACALDAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKFES = space(1)
      this.w_PACHKCPL = space(1)
      this.w_PACHKATT = 0
      this.w_PACALDAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOATT
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_TIPOATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLRINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_TIPOATT))
          select CACODICE,CADESCRI,CARAGGST,CAFLRINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_TIPOATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLRINV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_TIPOATT)+"%");

            select CACODICE,CADESCRI,CARAGGST,CAFLRINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TIPOATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oTIPOATT_1_10'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'GSAGRAAT.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLRINV";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST,CAFLRINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLRINV";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_TIPOATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_TIPOATT)
            select CACODICE,CADESCRI,CARAGGST,CAFLRINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOATT = NVL(_Link_.CACODICE,space(20))
      this.w_ATOGGETT = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(1))
      this.w_FRINV = NVL(_Link_.CAFLRINV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOATT = space(20)
      endif
      this.w_ATOGGETT = space(254)
      this.w_CARAGGST = space(1)
      this.w_FRINV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CARAGGST#'Z' and .w_FRINV='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo attivit� senza gestione rinvio")
        endif
        this.w_TIPOATT = space(20)
        this.w_ATOGGETT = space(254)
        this.w_CARAGGST = space(1)
        this.w_FRINV = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAITER
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_lTable = "PRO_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2], .t., this.PRO_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAITER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIP',True,'PRO_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IPCODICE like "+cp_ToStrODBC(trim(this.w_CAITER)+"%");

          i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IPCODICE',trim(this.w_CAITER))
          select IPCODICE,IPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAITER)==trim(_Link_.IPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IPDESCRI like "+cp_ToStrODBC(trim(this.w_CAITER)+"%");

            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IPDESCRI like "+cp_ToStr(trim(this.w_CAITER)+"%");

            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAITER) and !this.bDontReportError
            deferred_cp_zoom('PRO_ITER','*','IPCODICE',cp_AbsName(oSource.parent,'oCAITER_1_11'),i_cWhere,'GSAG_MIP',"Elenco raggruppamenti di attivit� ",'GSAG_MIP.PRO_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IPCODICE',oSource.xKey(1))
            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAITER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IPCODICE="+cp_ToStrODBC(this.w_CAITER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IPCODICE',this.w_CAITER)
            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAITER = NVL(_Link_.IPCODICE,space(10))
      this.w_ITDECR = NVL(_Link_.IPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CAITER = space(10)
      endif
      this.w_ITDECR = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])+'\'+cp_ToStr(_Link_.IPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRO_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAITER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUATT
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CASTAATT";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CAUATT)
            select CACODICE,CASTAATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUATT = NVL(_Link_.CACODICE,space(20))
      this.w_STATUS = NVL(_Link_.CASTAATT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUATT = space(20)
      endif
      this.w_STATUS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATARINV_1_4.value==this.w_DATARINV)
      this.oPgFrm.Page1.oPag.oDATARINV_1_4.value=this.w_DATARINV
    endif
    if not(this.oPgFrm.Page1.oPag.oORERINV_1_6.value==this.w_ORERINV)
      this.oPgFrm.Page1.oPag.oORERINV_1_6.value=this.w_ORERINV
    endif
    if not(this.oPgFrm.Page1.oPag.oMINRINV_1_7.value==this.w_MINRINV)
      this.oPgFrm.Page1.oPag.oMINRINV_1_7.value=this.w_MINRINV
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPGEN_1_9.RadioValue()==this.w_TIPGEN)
      this.oPgFrm.Page1.oPag.oTIPGEN_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOATT_1_10.value==this.w_TIPOATT)
      this.oPgFrm.Page1.oPag.oTIPOATT_1_10.value=this.w_TIPOATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCAITER_1_11.value==this.w_CAITER)
      this.oPgFrm.Page1.oPag.oCAITER_1_11.value=this.w_CAITER
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETT_1_14.value==this.w_ATOGGETT)
      this.oPgFrm.Page1.oPag.oATOGGETT_1_14.value=this.w_ATOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oFL_ATTCOMPL_1_16.RadioValue()==this.w_FL_ATTCOMPL)
      this.oPgFrm.Page1.oPag.oFL_ATTCOMPL_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATNOTPIA_1_17.value==this.w_ATNOTPIA)
      this.oPgFrm.Page1.oPag.oATNOTPIA_1_17.value=this.w_ATNOTPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGNOT_1_18.RadioValue()==this.w_FLGNOT)
      this.oPgFrm.Page1.oPag.oFLGNOT_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAUATT_1_28.value==this.w_ATCAUATT)
      this.oPgFrm.Page1.oPag.oATCAUATT_1_28.value=this.w_ATCAUATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETTOR_1_29.value==this.w_ATOGGETTOR)
      this.oPgFrm.Page1.oPag.oATOGGETTOR_1_29.value=this.w_ATOGGETTOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINIOR_1_31.value==this.w_DATINIOR)
      this.oPgFrm.Page1.oPag.oDATINIOR_1_31.value=this.w_DATINIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINIOR_1_33.value==this.w_OREINIOR)
      this.oPgFrm.Page1.oPag.oOREINIOR_1_33.value=this.w_OREINIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oMININIOR_1_34.value==this.w_MININIOR)
      this.oPgFrm.Page1.oPag.oMININIOR_1_34.value=this.w_MININIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINIOR_1_37.value==this.w_GIOINIOR)
      this.oPgFrm.Page1.oPag.oGIOINIOR_1_37.value=this.w_GIOINIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIORINV_1_38.value==this.w_GIORINV)
      this.oPgFrm.Page1.oPag.oGIORINV_1_38.value=this.w_GIORINV
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKINI_1_43.RadioValue()==this.w_CACHKINI)
      this.oPgFrm.Page1.oPag.oCACHKINI_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKFOR_1_44.RadioValue()==this.w_CACHKFOR)
      this.oPgFrm.Page1.oPag.oCACHKFOR_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oITDECR_1_47.value==this.w_ITDECR)
      this.oPgFrm.Page1.oPag.oITDECR_1_47.value=this.w_ITDECR
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGEDIT_1_48.RadioValue()==this.w_FLGEDIT)
      this.oPgFrm.Page1.oPag.oFLGEDIT_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oONLYFIRST_1_52.RadioValue()==this.w_ONLYFIRST)
      this.oPgFrm.Page1.oPag.oONLYFIRST_1_52.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATARINV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATARINV_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATARINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORERINV) < 24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORERINV_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINRINV) < 60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINRINV_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_TIPGEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPGEN_1_9.SetFocus()
            i_bnoObbl = !empty(.w_TIPGEN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TIPOATT)) or not(.w_CARAGGST#'Z' and .w_FRINV='S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOATT_1_10.SetFocus()
            i_bnoObbl = !empty(.w_TIPOATT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo attivit� senza gestione rinvio")
          case   (empty(.w_CAITER))  and not(Not Isalt() or .w_TIPGEN='T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAITER_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CAITER)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAG_MRP.CheckForm()
      if i_bres
        i_bres=  .GSAG_MRP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsag_krv
      IF i_bRes=.t.
          if cp_CharToDatetime(DTOC(.w_DATARINV)+' '+.w_ORERINV+':'+.w_MINRINV+':00') < cp_CharToDatetime(DTOC(.w_DATINIOR)+' '+.w_OREINIOR+':'+.w_MININIOR+':00')
            i_bRes=.f.
            ah_ErrorMsg("Attenzione: data rinvio non consentita.")
          endif
          If i_bRes=.t. And .w_DATARINV<i_DATSYS AND .w_STATUS$'T-D-I'
           i_bRes=ah_Yesno("Inserita data antecedente alla data odierna! Continuare?")
          EndIf
          If i_bRes=.t. And Not Empty(.w_PACHKATT) And .w_DATARINV>i_DATSYS+.w_PACHKATT AND .w_STATUS$'T-D-I'
           i_bRes=Ah_yesno( "Inserita data superiore alla data odierna di oltre %1 giorni Continuare?", '', alltrim(str(.w_PACHKATT)))
          EndIf
          if i_bRes AND .w_FL_ATTCOMPL='N' AND .w_PACHKCPL='S'
            i_bRes = ah_YesNo("Si � deciso di non completare l'attivit� selezionata. Procedi ugualmente?")
          endif
          IF i_bRes=.t.
            .w_CONFERMA='S'
          ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATARINV = this.w_DATARINV
    this.o_ORERINV = this.w_ORERINV
    this.o_TIPGEN = this.w_TIPGEN
    this.o_FL_ATTCOMPL = this.w_FL_ATTCOMPL
    this.o_FLGNOT = this.w_FLGNOT
    this.o_ATSERIAL = this.w_ATSERIAL
    this.o_CACHKINI = this.w_CACHKINI
    * --- GSAG_MRP : Depends On
    this.GSAG_MRP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsag_krvPag1 as StdContainer
  Width  = 580
  height = 535
  stdWidth  = 580
  stdheight = 535
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATARINV_1_4 as StdField with uid="MYGDNXHRTP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATARINV", cQueryName = "DATARINV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di rinvio dell'attivit� selezionata (data di inizio della nuova attivit�)",;
    HelpContextID = 125591180,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=127, Top=112

  func oDATARINV_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_PACHKFES='N' OR Chkfestivi(.w_DATARINV, .w_PACHKFES ))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORERINV_1_6 as StdField with uid="VGJIHEDZAE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ORERINV", cQueryName = "ORERINV",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora rinvio",;
    HelpContextID = 67338266,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=262, Top=112, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORERINV_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORERINV) < 24)
    endwith
    return bRes
  endfunc

  add object oMINRINV_1_7 as StdField with uid="NXNGNCXJWL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MINRINV", cQueryName = "MINRINV",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti rinvio",;
    HelpContextID = 67303738,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=304, Top=112, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINRINV_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINRINV) < 60)
    endwith
    return bRes
  endfunc

  add object oTIPGEN_1_9 as StdCheck with uid="VPSHJLJWSG",rtseq=6,rtrep=.f.,left=336, top=113, caption="Aggiungi attivit� collegate",;
    HelpContextID = 196224822,;
    cFormVar="w_TIPGEN", bObbl = .t. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPGEN_1_9.RadioValue()
    return(iif(this.value =1,'I',;
    'T'))
  endfunc
  func oTIPGEN_1_9.GetRadio()
    this.Parent.oContained.w_TIPGEN = this.RadioValue()
    return .t.
  endfunc

  func oTIPGEN_1_9.SetRadio()
    this.Parent.oContained.w_TIPGEN=trim(this.Parent.oContained.w_TIPGEN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGEN=='I',1,;
      0)
  endfunc

  add object oTIPOATT_1_10 as StdField with uid="UZLDVXBIDH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TIPOATT", cQueryName = "TIPOATT",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo attivit� senza gestione rinvio",;
    ToolTipText = "Tipo della nuova attivit�",;
    HelpContextID = 24782646,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=91, Top=138, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_TIPOATT"

  func oTIPOATT_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOATT_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOATT_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oTIPOATT_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'GSAGRAAT.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oTIPOATT_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_TIPOATT
     i_obj.ecpSave()
  endproc

  add object oCAITER_1_11 as StdField with uid="YIPVHGWTAM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CAITER", cQueryName = "CAITER",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Raggruppamento attivit�",;
    HelpContextID = 264154662,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=91, Top=165, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRO_ITER", cZoomOnZoom="GSAG_MIP", oKey_1_1="IPCODICE", oKey_1_2="this.w_CAITER"

  func oCAITER_1_11.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_TIPGEN='T')
    endwith
  endfunc

  func oCAITER_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAITER_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAITER_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRO_ITER','*','IPCODICE',cp_AbsName(this.parent,'oCAITER_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIP',"Elenco raggruppamenti di attivit� ",'GSAG_MIP.PRO_ITER_VZM',this.parent.oContained
  endproc
  proc oCAITER_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IPCODICE=this.parent.oContained.w_CAITER
     i_obj.ecpSave()
  endproc

  add object oATOGGETT_1_14 as StdField with uid="ICTNRQCNPT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ATOGGETT", cQueryName = "ATOGGETT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto della nuova attivit�",;
    HelpContextID = 47325530,;
   bGlobalFont=.t.,;
    Height=21, Width=292, Left=261, Top=138, InputMask=replicate('X',254)

  add object oFL_ATTCOMPL_1_16 as StdCheck with uid="CMBGGUKYCE",rtseq=11,rtrep=.f.,left=336, top=55, caption="Completa l'attivit� selezionata",;
    ToolTipText = "Se attivo, verr� completata l'attivit� selezionata",;
    HelpContextID = 44183157,;
    cFormVar="w_FL_ATTCOMPL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFL_ATTCOMPL_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFL_ATTCOMPL_1_16.GetRadio()
    this.Parent.oContained.w_FL_ATTCOMPL = this.RadioValue()
    return .t.
  endfunc

  func oFL_ATTCOMPL_1_16.SetRadio()
    this.Parent.oContained.w_FL_ATTCOMPL=trim(this.Parent.oContained.w_FL_ATTCOMPL)
    this.value = ;
      iif(this.Parent.oContained.w_FL_ATTCOMPL=='S',1,;
      0)
  endfunc

  add object oATNOTPIA_1_17 as StdMemo with uid="WWHEBRUBRE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ATNOTPIA", cQueryName = "ATNOTPIA",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note della nuova attivit�",;
    HelpContextID = 22408889,;
   bGlobalFont=.t.,;
    Height=52, Width=554, Left=13, Top=428


  add object oFLGNOT_1_18 as StdCombo with uid="DWYCYPLEOV",rtseq=13,rtrep=.f.,left=54,top=491,width=371,height=21;
    , HelpContextID = 39360854;
    , cFormVar="w_FLGNOT",RowSource=""+"Solo sulla attivit� rinviata,"+"Non inserire le note ,"+"Su tutte le attivit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLGNOT_1_18.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLGNOT_1_18.GetRadio()
    this.Parent.oContained.w_FLGNOT = this.RadioValue()
    return .t.
  endfunc

  func oFLGNOT_1_18.SetRadio()
    this.Parent.oContained.w_FLGNOT=trim(this.Parent.oContained.w_FLGNOT)
    this.value = ;
      iif(this.Parent.oContained.w_FLGNOT=='U',1,;
      iif(this.Parent.oContained.w_FLGNOT=='N',2,;
      iif(this.Parent.oContained.w_FLGNOT=='T',3,;
      0)))
  endfunc

  func oFLGNOT_1_18.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='T')
    endwith
  endfunc


  add object oBtn_1_19 as StdButton with uid="ERHOXVEEGI",left=467, top=485, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 115980058;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="TKCMXZXILU",left=519, top=485, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108691386;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="OKDHYSKAYN",left=13, top=485, width=23,height=23,;
    CpPicture="bmp\elimina2.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per azzerare le note";
    , HelpContextID = 116008714;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .NotifyEvent('Azzera')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oLinkPC_1_23 as stdDynamicChildContainer with uid="MCDMBCFWHW",left=27, top=244, width=527, height=161, bOnScreen=.t.;


  add object oATCAUATT_1_28 as StdField with uid="LBGXFCJQQZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ATCAUATT", cQueryName = "ATCAUATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 262889818,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=91, Top=32, InputMask=replicate('X',20)

  add object oATOGGETTOR_1_29 as StdField with uid="ZBFVVQRHJA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ATOGGETTOR", cQueryName = "ATOGGETTOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 47347786,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=262, Top=32, InputMask=replicate('X',254)

  add object oDATINIOR_1_31 as StdField with uid="ZJMFUWULJM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATINIOR", cQueryName = "DATINIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 121921160,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=127, Top=55

  add object oOREINIOR_1_33 as StdField with uid="LWBQPTRXKS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_OREINIOR", cQueryName = "OREINIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 121864248,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=262, Top=55, InputMask=replicate('X',2)

  add object oMININIOR_1_34 as StdField with uid="HOGLWIPZWG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MININIOR", cQueryName = "MININIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 121898776,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=304, Top=55, InputMask=replicate('X',2)

  add object oGIOINIOR_1_37 as StdField with uid="LKYXKXYVUO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_GIOINIOR", cQueryName = "GIOINIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 121902776,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=91, Top=55, InputMask=replicate('X',3)

  add object oGIORINV_1_38 as StdField with uid="DPWOUXIATY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_GIORINV", cQueryName = "GIORINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 67299738,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=91, Top=112, InputMask=replicate('X',3)


  add object oCACHKINI_1_43 as StdCombo with uid="JDBQLUXVZW",rtseq=25,rtrep=.f.,left=91,top=194,width=133,height=21;
    , ToolTipText = "Metodo di calcolo della data inizio attivit�";
    , HelpContextID = 118640239;
    , cFormVar="w_CACHKINI",RowSource=""+"Da tipo attivit�,"+"Con conferma,"+"Automatica,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCACHKINI_1_43.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'A',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oCACHKINI_1_43.GetRadio()
    this.Parent.oContained.w_CACHKINI = this.RadioValue()
    return .t.
  endfunc

  func oCACHKINI_1_43.SetRadio()
    this.Parent.oContained.w_CACHKINI=trim(this.Parent.oContained.w_CACHKINI)
    this.value = ;
      iif(this.Parent.oContained.w_CACHKINI=='T',1,;
      iif(this.Parent.oContained.w_CACHKINI=='C',2,;
      iif(this.Parent.oContained.w_CACHKINI=='A',3,;
      iif(this.Parent.oContained.w_CACHKINI=='N',4,;
      0))))
  endfunc

  func oCACHKINI_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACHKFES<>'N')
    endwith
   endif
  endfunc

  func oCACHKINI_1_43.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='T')
    endwith
  endfunc

  add object oCACHKFOR_1_44 as StdCheck with uid="SZVHUJOINL",rtseq=26,rtrep=.f.,left=260, top=193, caption="Sospensioni forensi",;
    ToolTipText = "Se attivo nella determinazione della data inizio delle attivit� tiene conto anche della sospensione forense",;
    HelpContextID = 68308600,;
    cFormVar="w_CACHKFOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCACHKFOR_1_44.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCACHKFOR_1_44.GetRadio()
    this.Parent.oContained.w_CACHKFOR = this.RadioValue()
    return .t.
  endfunc

  func oCACHKFOR_1_44.SetRadio()
    this.Parent.oContained.w_CACHKFOR=trim(this.Parent.oContained.w_CACHKFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CACHKFOR=='S',1,;
      0)
  endfunc

  func oCACHKFOR_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACHKFES<>'N' And .w_PACHKFES<>'T')
    endwith
   endif
  endfunc

  func oCACHKFOR_1_44.mHide()
    with this.Parent.oContained
      return (not IsAlt() Or .w_CACHKINI='N' Or .w_CACHKINI='T' or .w_TIPGEN='T')
    endwith
  endfunc

  add object oITDECR_1_47 as StdField with uid="AVUZHNWPNK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ITDECR", cQueryName = "ITDECR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 261058950,;
   bGlobalFont=.t.,;
    Height=21, Width=292, Left=261, Top=164, InputMask=replicate('X',60)

  func oITDECR_1_47.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='T')
    endwith
  endfunc

  add object oFLGEDIT_1_48 as StdCheck with uid="DPGZVNZQJC",rtseq=28,rtrep=.f.,left=336, top=76, caption="Modifica attivit�",;
    ToolTipText = "Se attivo apre attivit� in modifica",;
    HelpContextID = 111122774,;
    cFormVar="w_FLGEDIT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGEDIT_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGEDIT_1_48.GetRadio()
    this.Parent.oContained.w_FLGEDIT = this.RadioValue()
    return .t.
  endfunc

  func oFLGEDIT_1_48.SetRadio()
    this.Parent.oContained.w_FLGEDIT=trim(this.Parent.oContained.w_FLGEDIT)
    this.value = ;
      iif(this.Parent.oContained.w_FLGEDIT=='S',1,;
      0)
  endfunc

  func oFLGEDIT_1_48.mHide()
    with this.Parent.oContained
      return (.w_FL_ATTCOMPL<>'S')
    endwith
  endfunc


  add object oONLYFIRST_1_52 as StdCombo with uid="HOAEJUOZCZ",rtseq=32,rtrep=.f.,left=91,top=217,width=133,height=21;
    , ToolTipText = "Applica ora impostata alle attivit� indicate del raggruppamento";
    , HelpContextID = 114553209;
    , cFormVar="w_ONLYFIRST",RowSource=""+"Tutte,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oONLYFIRST_1_52.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oONLYFIRST_1_52.GetRadio()
    this.Parent.oContained.w_ONLYFIRST = this.RadioValue()
    return .t.
  endfunc

  func oONLYFIRST_1_52.SetRadio()
    this.Parent.oContained.w_ONLYFIRST=trim(this.Parent.oContained.w_ONLYFIRST)
    this.value = ;
      iif(this.Parent.oContained.w_ONLYFIRST=='T',1,;
      iif(this.Parent.oContained.w_ONLYFIRST=='N',2,;
      0))
  endfunc

  func oONLYFIRST_1_52.mHide()
    with this.Parent.oContained
      return (!Isalt() or Empty(.w_ORERINV) or .w_TIPGEN<>'I')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="CDWWSMMEYG",Visible=.t., Left=6, Top=114,;
    Alignment=1, Width=82, Height=18,;
    Caption="Rinvio al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BYZPFCFPKB",Visible=.t., Left=209, Top=114,;
    Alignment=1, Width=50, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="FXUDOJAZEE",Visible=.t., Left=294, Top=114,;
    Alignment=1, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="JTCICOWDFA",Visible=.t., Left=42, Top=142,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CXGZVKWBXN",Visible=.t., Left=13, Top=408,;
    Alignment=0, Width=107, Height=18,;
    Caption="Note"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="DDTKAFHBPY",Visible=.t., Left=35, Top=10,;
    Alignment=0, Width=143, Height=18,;
    Caption="Attivit� selezionata"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ORKJUUCBVM",Visible=.t., Left=38, Top=32,;
    Alignment=1, Width=50, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="LMGOPCHWVW",Visible=.t., Left=38, Top=57,;
    Alignment=1, Width=50, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="JQFSIFZRPU",Visible=.t., Left=204, Top=57,;
    Alignment=1, Width=55, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="RSSICMDKLX",Visible=.t., Left=290, Top=57,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="ASITDWTOLS",Visible=.t., Left=7, Top=195,;
    Alignment=1, Width=81, Height=18,;
    Caption="Calcolo data:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='T')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="NJXJDSARLR",Visible=.t., Left=9, Top=167,;
    Alignment=1, Width=79, Height=18,;
    Caption="Raggruppam.:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_TIPGEN='T')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="IJDBSTNCIJ",Visible=.t., Left=11, Top=217,;
    Alignment=1, Width=77, Height=18,;
    Caption="Applica ora a :"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (!Isalt() or Empty(.w_ORERINV) OR .w_TIPGEN<>'I')
    endwith
  endfunc

  add object oBox_1_26 as StdBox with uid="MXBVHLEODS",left=32, top=26, width=540,height=77
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_krv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
