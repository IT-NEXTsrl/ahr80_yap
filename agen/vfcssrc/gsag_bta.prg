* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bta                                                        *
*              Duplica tipi attivita                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-10                                                      *
* Last revis.: 2011-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bta",oParentObject,m.pAzione)
return(i_retval)

define class tgsag_bta as StdBatch
  * --- Local variables
  pAzione = space(1)
  * --- WorkFile variables
  CAUMATTI_idx=0
  TMP_SERV_idx=0
  CAU_ATTI_idx=0
  TMP_TARI_idx=0
  DEF_PART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                                        Duplicazione Tipi attivit�
    * --- Tabelle Gestite:
    *     CAUMATTI
    *     CAU_ATTI
    * --- pAzione = 'C'  : controllo di esistenza codice tipo attivit�
    *                    = 'D'  : duplicazione tipo attivit�
    do case
      case this.pAzione = "D"
        * --- DUPLICAZIONE
        * --- begin transaction
        cp_BeginTrs()
        * --- Try
        local bErr_035AE530
        bErr_035AE530=bTrsErr
        this.Try_035AE530()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Duplicazione terminata con errori",,"")
        endif
        bTrsErr=bTrsErr or bErr_035AE530
        * --- End
        * --- Drop temporary table TMP_SERV
        i_nIdx=cp_GetTableDefIdx('TMP_SERV')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_SERV')
        endif
        * --- Drop temporary table TMP_TARI
        i_nIdx=cp_GetTableDefIdx('TMP_TARI')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_TARI')
        endif
      case this.pAzione = "C"
        * --- CONTROLLO ESISTENZA CODICE TIPO ATTIVITA'
        if NOT EMPTY(this.oParentObject.w_CODTIPATT)
          * --- Select from CAUMATTI
          i_nConn=i_TableProp[this.CAUMATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CACODICE  from "+i_cTable+" CAUMATTI ";
                +" where CACODICE="+cp_ToStrODBC(this.oParentObject.w_CODTIPATT)+"";
                 ,"_Curs_CAUMATTI")
          else
            select CACODICE from (i_cTable);
             where CACODICE=this.oParentObject.w_CODTIPATT;
              into cursor _Curs_CAUMATTI
          endif
          if used('_Curs_CAUMATTI')
            select _Curs_CAUMATTI
            locate for 1=1
            do while not(eof())
            ah_ErrorMsg("Attenzione: codice tipo attivit� gi� esistente!",,"")
            this.oParentObject.w_CODTIPATT = space(20)
              select _Curs_CAUMATTI
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc
  proc Try_035AE530()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Create temporary table TMP_SERV
    i_nIdx=cp_AddTableDef('TMP_SERV') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CAUMATTI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where CACODICE="+cp_ToStrODBC(this.oParentObject.w_TIPATORIG)+"";
          )
    this.TMP_SERV_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_SERV
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_SERV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SERV_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_SERV_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CACODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODTIPATT),'TMP_SERV','CACODICE');
      +",CADESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESTIPATT),'TMP_SERV','CADESCRI');
          +i_ccchkf ;
             )
    else
      update (i_cTable) set;
          CACODICE = this.oParentObject.w_CODTIPATT;
          ,CADESCRI = this.oParentObject.w_DESTIPATT;
          &i_ccchkf. ;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore scrittura TMP Prestazioni'
      return
    endif
    * --- Insert into CAUMATTI
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_SERV_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CAUMATTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento Prestazioni'
      return
    endif
    * --- Se attivo il check Duplica prestazioni
    if this.oParentObject.w_DUPLPRES="S"
      * --- Create temporary table TMP_TARI
      i_nIdx=cp_AddTableDef('TMP_TARI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.CAU_ATTI_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
            +" where CACODICE="+cp_ToStrODBC(this.oParentObject.w_TIPATORIG)+"";
            )
      this.TMP_TARI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Write into TMP_TARI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMP_TARI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_TARI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TARI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CACODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODTIPATT),'TMP_TARI','CACODICE');
            +i_ccchkf ;
               )
      else
        update (i_cTable) set;
            CACODICE = this.oParentObject.w_CODTIPATT;
            &i_ccchkf. ;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore scrittura TMP Tariffario'
        return
      endif
      * --- Insert into CAU_ATTI
      i_nConn=i_TableProp[this.CAU_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_TARI_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CAU_ATTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserimento Tariffario'
        return
      endif
    endif
    * --- Create temporary table TMP_TARI
    i_nIdx=cp_AddTableDef('TMP_TARI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DEF_PART_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DEF_PART_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where DFCODICE="+cp_ToStrODBC(this.oParentObject.w_TIPATORIG)+"";
          )
    this.TMP_TARI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_TARI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_TARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_TARI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TARI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DFCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODTIPATT),'TMP_TARI','DFCODICE');
          +i_ccchkf ;
             )
    else
      update (i_cTable) set;
          DFCODICE = this.oParentObject.w_CODTIPATT;
          &i_ccchkf. ;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore scrittura TMP Tariffario'
      return
    endif
    * --- Insert into DEF_PART
    i_nConn=i_TableProp[this.DEF_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DEF_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_TARI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DEF_PART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento Tariffario'
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='*TMP_SERV'
    this.cWorkTables[3]='CAU_ATTI'
    this.cWorkTables[4]='*TMP_TARI'
    this.cWorkTables[5]='DEF_PART'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_CAUMATTI')
      use in _Curs_CAUMATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
