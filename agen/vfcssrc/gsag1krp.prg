* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag1krp                                                        *
*              Anteprima parcella                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-07-18                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag1krp",oParentObject))

* --- Class definition
define class tgsag1krp as StdForm
  Top    = 4
  Left   = 5

  * --- Standard Properties
  Width  = 832
  Height = 576
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-07"
  HelpContextID=149411991
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  DIPENDEN_IDX = 0
  KEY_ARTI_IDX = 0
  METCALSP_IDX = 0
  TIP_DOCU_IDX = 0
  PAR_ALTE_IDX = 0
  VOCIIVA_IDX = 0
  TAB_RITE_IDX = 0
  cPrg = "gsag1krp"
  cComment = "Anteprima parcella"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_STATO = space(1)
  w_CODPRA = space(15)
  w_CODRES = space(5)
  w_CodUte = 0
  w_CODSER = space(20)
  w_TIPPRA = space(1)
  w_TIPANA = space(1)
  w_ATSERIAL = space(20)
  w_FLESPA = space(1)
  w_OQRY = space(254)
  w_TIPOPRE = space(1)
  w_PrestRowOrd = 0
  w_ROWNUM = 0
  w_TIPCAU = space(1)
  w_CODTRI = space(5)
  w_READTRI = space(5)
  w_PERRIT = 0
  w_PERIMP = 0
  w_CASPRE = space(5)
  w_SPEGEN = space(5)
  w_PERIVAS = 0
  w_PERIVAC = 0
  w_READPARALT = space(10)
  w_CODIVA = space(5)
  w_MCALSI = space(5)
  w_FLSPTR = space(1)
  w_MCALST = space(5)
  w_DESIMB = space(40)
  w_DESTRA = space(40)
  w_FLSPIM = space(1)
  w_MSIMPFISI = 0
  w_MSIMPFIST = 0
  w_PERIVA = 0
  w_TOTDIR = 0
  o_TOTDIR = 0
  w_TOTONO = 0
  o_TOTONO = 0
  w_TOTGEN = 0
  w_TOTSPE = 0
  w_TOTACC = 0
  w_TOTCOM = 0
  w_TOTCPA = 0
  w_TOTIVA = 0
  w_TOTANT = 0
  o_TOTANT = 0
  w_TOTALE = 0
  w_DESDOC = space(35)
  w_TOTRIT = 0
  w_TOTDOV = 0
  w_MVTIPDOC = space(5)
  o_MVTIPDOC = space(5)
  w_CCFLRITE = space(1)
  o_CCFLRITE = space(1)
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_AGZRP_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag1krpPag1","gsag1krp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTOTDIR_1_39
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGZRP_ZOOM = this.oPgFrm.Pages(1).oPag.AGZRP_ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGZRP_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='METCALSP'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='PAR_ALTE'
    this.cWorkTables[7]='VOCIIVA'
    this.cWorkTables[8]='TAB_RITE'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_STATO=space(1)
      .w_CODPRA=space(15)
      .w_CODRES=space(5)
      .w_CodUte=0
      .w_CODSER=space(20)
      .w_TIPPRA=space(1)
      .w_TIPANA=space(1)
      .w_ATSERIAL=space(20)
      .w_FLESPA=space(1)
      .w_OQRY=space(254)
      .w_TIPOPRE=space(1)
      .w_PrestRowOrd=0
      .w_ROWNUM=0
      .w_TIPCAU=space(1)
      .w_CODTRI=space(5)
      .w_READTRI=space(5)
      .w_PERRIT=0
      .w_PERIMP=0
      .w_CASPRE=space(5)
      .w_SPEGEN=space(5)
      .w_PERIVAS=0
      .w_PERIVAC=0
      .w_READPARALT=space(10)
      .w_CODIVA=space(5)
      .w_MCALSI=space(5)
      .w_FLSPTR=space(1)
      .w_MCALST=space(5)
      .w_DESIMB=space(40)
      .w_DESTRA=space(40)
      .w_FLSPIM=space(1)
      .w_MSIMPFISI=0
      .w_MSIMPFIST=0
      .w_PERIVA=0
      .w_TOTDIR=0
      .w_TOTONO=0
      .w_TOTGEN=0
      .w_TOTSPE=0
      .w_TOTACC=0
      .w_TOTCOM=0
      .w_TOTCPA=0
      .w_TOTIVA=0
      .w_TOTANT=0
      .w_TOTALE=0
      .w_DESDOC=space(35)
      .w_TOTRIT=0
      .w_TOTDOV=0
      .w_MVTIPDOC=space(5)
      .w_CCFLRITE=space(1)
      .w_SELEZI=space(1)
      .w_DATA_INI=oParentObject.w_DATA_INI
      .w_DATA_FIN=oParentObject.w_DATA_FIN
      .w_STATO=oParentObject.w_STATO
      .w_CODRES=oParentObject.w_CODRES
      .w_CODSER=oParentObject.w_CODSER
      .w_ATSERIAL=oParentObject.w_ATSERIAL
      .w_FLESPA=oParentObject.w_FLESPA
      .w_OQRY=oParentObject.w_OQRY
      .w_TIPOPRE=oParentObject.w_TIPOPRE
      .w_PrestRowOrd=oParentObject.w_PrestRowOrd
      .w_ROWNUM=oParentObject.w_ROWNUM
      .oPgFrm.Page1.oPag.AGZRP_ZOOM.Calculate()
          .DoRTCalc(1,2,.f.)
        .w_STATO = 'P'
          .DoRTCalc(4,7,.f.)
        .w_TIPPRA = 'T'
        .w_TIPANA = 'C'
        .w_ATSERIAL = .w_AGZRP_ZOOM.GetVar('ATSERIAL')
        .w_FLESPA = 'N'
        .w_OQRY = 'N'
          .DoRTCalc(13,15,.f.)
        .w_TIPCAU = 'F'
        .w_CODTRI = IIF(.w_CCFLRITE='S', READALTE('I'), space(5))
        .w_READTRI = i_codazi
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_READTRI))
          .link_1_19('Full')
        endif
        .DoRTCalc(19,21,.f.)
        if not(empty(.w_CASPRE))
          .link_1_22('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_SPEGEN))
          .link_1_23('Full')
        endif
          .DoRTCalc(23,24,.f.)
        .w_READPARALT = i_CODAZI
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_READPARALT))
          .link_1_26('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODIVA))
          .link_1_27('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_MCALSI))
          .link_1_28('Full')
        endif
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_MCALST))
          .link_1_30('Full')
        endif
          .DoRTCalc(30,37,.f.)
        .w_TOTGEN = (.w_TOTDIR+.w_TOTONO-.w_TOTACC) * .w_MSIMPFISI / 100
          .DoRTCalc(39,40,.f.)
        .w_TOTCOM = .w_TOTSPE+.w_TOTDIR+.w_TOTONO+.w_TOTGEN-.w_TOTACC
        .w_TOTCPA = (.w_TOTCOM) * .w_MSIMPFIST / 100
        .w_TOTIVA = Cp_Round(((.w_TOTSPE+.w_TOTDIR+.w_TOTONO-.w_TOTACC) * .w_PERIVA / 100) +(.w_TOTCPA * .w_PERIVAC / 100)+(.w_TOTGEN* .w_PERIVAS/ 100),g_perpvl)
          .DoRTCalc(44,44,.f.)
        .w_TOTALE = .w_TOTCOM+.w_TOTCPA+.w_TOTIVA+.w_TOTANT
          .DoRTCalc(46,46,.f.)
        .w_TOTRIT = cp_ROUND(.w_TOTCOM*((.w_PERIMP/100)*(.w_PERRIT/100)),2)
        .w_TOTDOV = .w_TOTALE-.w_TOTRIT
        .w_MVTIPDOC = TIPOPREF(.w_TIPCAU)
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_MVTIPDOC))
          .link_1_61('Full')
        endif
        .w_CCFLRITE = 'N'
        .w_SELEZI = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_68.enabled = this.oPgFrm.Page1.oPag.oBtn_1_68.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATA_INI=.w_DATA_INI
      .oParentObject.w_DATA_FIN=.w_DATA_FIN
      .oParentObject.w_STATO=.w_STATO
      .oParentObject.w_CODRES=.w_CODRES
      .oParentObject.w_CODSER=.w_CODSER
      .oParentObject.w_ATSERIAL=.w_ATSERIAL
      .oParentObject.w_FLESPA=.w_FLESPA
      .oParentObject.w_OQRY=.w_OQRY
      .oParentObject.w_TIPOPRE=.w_TIPOPRE
      .oParentObject.w_PrestRowOrd=.w_PrestRowOrd
      .oParentObject.w_ROWNUM=.w_ROWNUM
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AGZRP_ZOOM.Calculate()
        .DoRTCalc(1,9,.t.)
            .w_ATSERIAL = .w_AGZRP_ZOOM.GetVar('ATSERIAL')
        .DoRTCalc(11,16,.t.)
        if .o_CCFLRITE<>.w_CCFLRITE
            .w_CODTRI = IIF(.w_CCFLRITE='S', READALTE('I'), space(5))
        endif
        if .o_CCFLRITE<>.w_CCFLRITE
            .w_READTRI = i_codazi
          .link_1_19('Full')
        endif
        .DoRTCalc(19,20,.t.)
          .link_1_22('Full')
          .link_1_23('Full')
        .DoRTCalc(23,24,.t.)
          .link_1_26('Full')
          .link_1_27('Full')
          .link_1_28('Full')
        .DoRTCalc(28,28,.t.)
          .link_1_30('Full')
        .DoRTCalc(30,37,.t.)
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_TOTANT<>.w_TOTANT.or. .o_TOTDIR<>.w_TOTDIR.or. .o_TOTONO<>.w_TOTONO
            .w_TOTGEN = (.w_TOTDIR+.w_TOTONO-.w_TOTACC) * .w_MSIMPFISI / 100
        endif
        .DoRTCalc(39,40,.t.)
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_TOTANT<>.w_TOTANT.or. .o_TOTDIR<>.w_TOTDIR.or. .o_TOTONO<>.w_TOTONO
            .w_TOTCOM = .w_TOTSPE+.w_TOTDIR+.w_TOTONO+.w_TOTGEN-.w_TOTACC
        endif
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_TOTANT<>.w_TOTANT.or. .o_TOTDIR<>.w_TOTDIR.or. .o_TOTONO<>.w_TOTONO
            .w_TOTCPA = (.w_TOTCOM) * .w_MSIMPFIST / 100
        endif
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_TOTANT<>.w_TOTANT.or. .o_TOTDIR<>.w_TOTDIR.or. .o_TOTONO<>.w_TOTONO
            .w_TOTIVA = Cp_Round(((.w_TOTSPE+.w_TOTDIR+.w_TOTONO-.w_TOTACC) * .w_PERIVA / 100) +(.w_TOTCPA * .w_PERIVAC / 100)+(.w_TOTGEN* .w_PERIVAS/ 100),g_perpvl)
        endif
        .DoRTCalc(44,44,.t.)
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_TOTANT<>.w_TOTANT.or. .o_TOTDIR<>.w_TOTDIR.or. .o_TOTONO<>.w_TOTONO
            .w_TOTALE = .w_TOTCOM+.w_TOTCPA+.w_TOTIVA+.w_TOTANT
        endif
        .DoRTCalc(46,46,.t.)
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_TOTANT<>.w_TOTANT.or. .o_TOTDIR<>.w_TOTDIR.or. .o_TOTONO<>.w_TOTONO.or. .o_CCFLRITE<>.w_CCFLRITE
            .w_TOTRIT = cp_ROUND(.w_TOTCOM*((.w_PERIMP/100)*(.w_PERRIT/100)),2)
        endif
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_TOTANT<>.w_TOTANT.or. .o_TOTDIR<>.w_TOTDIR.or. .o_TOTONO<>.w_TOTONO.or. .o_CCFLRITE<>.w_CCFLRITE
            .w_TOTDOV = .w_TOTALE-.w_TOTRIT
        endif
          .link_1_61('Full')
        if .o_SELEZI<>.w_SELEZI
          .Calculate_JROJMCSSXL()
        endif
        if .o_SELEZI<>.w_SELEZI
          .Calculate_LAJUSRRILS()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(50,51,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGZRP_ZOOM.Calculate()
    endwith
  return

  proc Calculate_EPXVRUBISQ()
    with this
          * --- Azzera gli importi quando si deseleziona dal tasto destro
          .w_TOTDIR = 0
          .w_TOTONO = 0
          .w_TOTSPE = 0
          .w_TOTGEN = 0
          .w_TOTACC = 0
          .w_TOTCOM = 0
          .w_TOTCPA = 0
          .w_TOTIVA = 0
          .w_TOTANT = 0
          .w_TOTALE = 0
          .w_TOTRIT = 0
          .w_TOTDOV = 0
          .w_MVTIPDOC = ''
          .link_1_61('Full')
    endwith
  endproc
  proc Calculate_JROJMCSSXL()
    with this
          * --- w_selezi changed
          GSAG_BCK(this;
              ,'L';
             )
    endwith
  endproc
  proc Calculate_LAJUSRRILS()
    with this
          * --- Azzera gli importi quando si deseleziona
          .w_TOTDIR = IIF(.w_SELEZI='D',0,.w_TOTDIR)
          .w_TOTONO = IIF(.w_SELEZI='D',0,.w_TOTONO)
          .w_TOTSPE = IIF(.w_SELEZI='D',0,.w_TOTSPE)
          .w_TOTGEN = IIF(.w_SELEZI='D',0,.w_TOTGEN)
          .w_TOTACC = IIF(.w_SELEZI='D',0,.w_TOTACC)
          .w_TOTCOM = IIF(.w_SELEZI='D',0,.w_TOTCOM)
          .w_TOTCPA = IIF(.w_SELEZI='D',0,.w_TOTCPA)
          .w_TOTIVA = IIF(.w_SELEZI='D',0,.w_TOTIVA)
          .w_TOTANT = IIF(.w_SELEZI='D',0,.w_TOTANT)
          .w_TOTALE = IIF(.w_SELEZI='D',0,.w_TOTALE)
          .w_TOTRIT = IIF(.w_SELEZI='D',0,.w_TOTRIT)
          .w_TOTDOV = IIF(.w_SELEZI='D',0,.w_TOTDOV)
          .w_MVTIPDOC = IIF(.w_SELEZI='D','',.w_MVTIPDOC)
          .link_1_61('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag1krp
    IF CEVENT='Blank'
       This.w_AGZRP_ZOOM.ccpQueryName="QUERY\GSAGZZRP"
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGZRP_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("AGZRP_ZOOM rowuncheckall")
          .Calculate_EPXVRUBISQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag1krp
    IF cEvent='Blank'
       DO GSAG_BCK with THIS,'Z'
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READTRI
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
    i_lTable = "TAB_RITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2], .t., this.TAB_RITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRTIPRIT,TRCODTRI,TRCODAZI,TRPERRIT,TRPERIMP";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODAZI="+cp_ToStrODBC(this.w_READTRI);
                   +" and TRTIPRIT="+cp_ToStrODBC('V');
                   +" and TRCODTRI="+cp_ToStrODBC(this.w_CODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRTIPRIT','V';
                       ,'TRCODTRI',this.w_CODTRI;
                       ,'TRCODAZI',this.w_READTRI)
            select TRTIPRIT,TRCODTRI,TRCODAZI,TRPERRIT,TRPERIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READTRI = NVL(_Link_.TRCODAZI,space(5))
      this.w_PERRIT = NVL(_Link_.TRPERRIT,0)
      this.w_PERIMP = NVL(_Link_.TRPERIMP,0)
    else
      if i_cCtrl<>'Load'
        this.w_READTRI = space(5)
      endif
      this.w_PERRIT = 0
      this.w_PERIMP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])+'\'+cp_ToStr(_Link_.TRTIPRIT,1)+'\'+cp_ToStr(_Link_.TRCODTRI,1)+'\'+cp_ToStr(_Link_.TRCODAZI,1)
      cp_ShowWarn(i_cKey,this.TAB_RITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CASPRE
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CASPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CASPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CASPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CASPRE)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CASPRE = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVAC = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_CASPRE = space(5)
      endif
      this.w_PERIVAC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CASPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SPEGEN
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SPEGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SPEGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_SPEGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_SPEGEN)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SPEGEN = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVAS = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_SPEGEN = space(5)
      endif
      this.w_PERIVAS = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SPEGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READPARALT
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPARALT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPARALT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACODIVA,PACASPRE,PASPEGEN";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READPARALT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READPARALT)
            select PACODAZI,PACODIVA,PACASPRE,PASPEGEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPARALT = NVL(_Link_.PACODAZI,space(10))
      this.w_CODIVA = NVL(_Link_.PACODIVA,space(5))
      this.w_CASPRE = NVL(_Link_.PACASPRE,space(5))
      this.w_SPEGEN = NVL(_Link_.PASPEGEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READPARALT = space(10)
      endif
      this.w_CODIVA = space(5)
      this.w_CASPRE = space(5)
      this.w_SPEGEN = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPARALT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCALSI
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCALSI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCALSI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI,MSIMPFIS";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCALSI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCALSI)
            select MSCODICE,MSDESCRI,MSIMPFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCALSI = NVL(_Link_.MSCODICE,space(5))
      this.w_DESIMB = NVL(_Link_.MSDESCRI,space(40))
      this.w_MSIMPFISI = NVL(_Link_.MSIMPFIS,0)
    else
      if i_cCtrl<>'Load'
        this.w_MCALSI = space(5)
      endif
      this.w_DESIMB = space(40)
      this.w_MSIMPFISI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCALSI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCALST
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCALST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCALST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI,MSIMPFIS";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCALST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCALST)
            select MSCODICE,MSDESCRI,MSIMPFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCALST = NVL(_Link_.MSCODICE,space(5))
      this.w_DESTRA = NVL(_Link_.MSDESCRI,space(40))
      this.w_MSIMPFIST = NVL(_Link_.MSIMPFIS,0)
    else
      if i_cCtrl<>'Load'
        this.w_MCALST = space(5)
      endif
      this.w_DESTRA = space(40)
      this.w_MSIMPFIST = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCALST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTIPDOC
  func Link_1_61(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDMCALST,TDMCALSI,TDFLSPTR,TDFLSPIM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC)
            select TDTIPDOC,TDDESDOC,TDMCALST,TDMCALSI,TDFLSPTR,TDFLSPIM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_MCALST = NVL(_Link_.TDMCALST,space(5))
      this.w_MCALSI = NVL(_Link_.TDMCALSI,space(5))
      this.w_FLSPTR = NVL(_Link_.TDFLSPTR,space(1))
      this.w_FLSPIM = NVL(_Link_.TDFLSPIM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_MCALST = space(5)
      this.w_MCALSI = space(5)
      this.w_FLSPTR = space(1)
      this.w_FLSPIM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTOTDIR_1_39.value==this.w_TOTDIR)
      this.oPgFrm.Page1.oPag.oTOTDIR_1_39.value=this.w_TOTDIR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTONO_1_40.value==this.w_TOTONO)
      this.oPgFrm.Page1.oPag.oTOTONO_1_40.value=this.w_TOTONO
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTGEN_1_41.value==this.w_TOTGEN)
      this.oPgFrm.Page1.oPag.oTOTGEN_1_41.value=this.w_TOTGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSPE_1_43.value==this.w_TOTSPE)
      this.oPgFrm.Page1.oPag.oTOTSPE_1_43.value=this.w_TOTSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTACC_1_44.value==this.w_TOTACC)
      this.oPgFrm.Page1.oPag.oTOTACC_1_44.value=this.w_TOTACC
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCOM_1_46.value==this.w_TOTCOM)
      this.oPgFrm.Page1.oPag.oTOTCOM_1_46.value=this.w_TOTCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCPA_1_47.value==this.w_TOTCPA)
      this.oPgFrm.Page1.oPag.oTOTCPA_1_47.value=this.w_TOTCPA
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIVA_1_48.value==this.w_TOTIVA)
      this.oPgFrm.Page1.oPag.oTOTIVA_1_48.value=this.w_TOTIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTANT_1_51.value==this.w_TOTANT)
      this.oPgFrm.Page1.oPag.oTOTANT_1_51.value=this.w_TOTANT
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_1_53.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_1_53.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRIT_1_55.value==this.w_TOTRIT)
      this.oPgFrm.Page1.oPag.oTOTRIT_1_55.value=this.w_TOTRIT
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDOV_1_57.value==this.w_TOTDOV)
      this.oPgFrm.Page1.oPag.oTOTDOV_1_57.value=this.w_TOTDOV
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLRITE_1_62.RadioValue()==this.w_CCFLRITE)
      this.oPgFrm.Page1.oPag.oCCFLRITE_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_64.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_64.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TOTDIR = this.w_TOTDIR
    this.o_TOTONO = this.w_TOTONO
    this.o_TOTANT = this.w_TOTANT
    this.o_MVTIPDOC = this.w_MVTIPDOC
    this.o_CCFLRITE = this.w_CCFLRITE
    this.o_SELEZI = this.w_SELEZI
    return

enddefine

* --- Define pages as container
define class tgsag1krpPag1 as StdContainer
  Width  = 828
  height = 576
  stdWidth  = 828
  stdheight = 576
  resizeXpos=350
  resizeYpos=500
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object AGZRP_ZOOM as cp_szoombox with uid="KTXNPRKKOW",left=-2, top=6, width=581,height=566,;
    caption='AGZRP_ZOOM',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",bQueryOnDblClick=.t.,bRetriveAllRows=.f.,cTable="OFF_ATTI",cZoomFile="GSAG_ZRP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="GSAG1KRP",bNoZoomGridShape=.f.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 222303813

  add object oTOTDIR_1_39 as StdField with uid="SHRAABLLDM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_TOTDIR", cQueryName = "TOTDIR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale diritti",;
    HelpContextID = 4100810,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=18, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTONO_1_40 as StdField with uid="EUJMGFWEOZ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_TOTONO", cQueryName = "TOTONO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale onorari",;
    HelpContextID = 48468682,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=48, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTGEN_1_41 as StdField with uid="AZKIITWQVM",rtseq=38,rtrep=.f.,;
    cFormVar = "w_TOTGEN", cQueryName = "TOTGEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale spese generali",;
    HelpContextID = 75207370,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=76, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTSPE_1_43 as StdField with uid="XKZZOGNGAU",rtseq=39,rtrep=.f.,;
    cFormVar = "w_TOTSPE", cQueryName = "TOTSPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale spese imponobili",;
    HelpContextID = 213881546,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=106, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTACC_1_44 as StdField with uid="WFBEZYZFRV",rtseq=40,rtrep=.f.,;
    cFormVar = "w_TOTACC", cQueryName = "TOTACC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale acconti fatturati",;
    HelpContextID = 262247114,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=138, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTCOM_1_46 as StdField with uid="YLBGEXJCKY",rtseq=41,rtrep=.f.,;
    cFormVar = "w_TOTCOM", cQueryName = "TOTCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale competenze",;
    HelpContextID = 81760970,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=168, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTCPA_1_47 as StdField with uid="WSWIJAXWZM",rtseq=42,rtrep=.f.,;
    cFormVar = "w_TOTCPA", cQueryName = "TOTCPA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale CPA",;
    HelpContextID = 13603530,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=198, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTIVA_1_48 as StdField with uid="LIZNTBRNEX",rtseq=43,rtrep=.f.,;
    cFormVar = "w_TOTIVA", cQueryName = "TOTIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale Iva",;
    HelpContextID = 6918858,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=228, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTANT_1_51 as StdField with uid="FRHCVGEELM",rtseq=44,rtrep=.f.,;
    cFormVar = "w_TOTANT", cQueryName = "TOTANT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale anticipazioni",;
    HelpContextID = 233935562,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=258, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTALE_1_53 as StdField with uid="OWZMKNEAXB",rtseq=45,rtrep=.f.,;
    cFormVar = "w_TOTALE", cQueryName = "TOTALE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 219255498,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=286, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTRIT_1_55 as StdField with uid="PLXJIUUZWH",rtseq=47,rtrep=.f.,;
    cFormVar = "w_TOTRIT", cQueryName = "TOTRIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale ritenuta d'acconto",;
    HelpContextID = 238064330,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=346, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTDOV_1_57 as StdField with uid="EYQFWHLJIA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_TOTDOV", cQueryName = "TOTDOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo dovuto",;
    HelpContextID = 199135946,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=686, Top=372, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oCCFLRITE_1_62 as StdCheck with uid="EEXYYJENRL",rtseq=50,rtrep=.f.,left=686, top=319, caption="IRPEF/IRES",;
    ToolTipText = "Se attivo, il cliente gestisce le ritenute attive. Pu� valere solo IRPEF/IRES.",;
    HelpContextID = 123240555,;
    cFormVar="w_CCFLRITE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLRITE_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLRITE_1_62.GetRadio()
    this.Parent.oContained.w_CCFLRITE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLRITE_1_62.SetRadio()
    this.Parent.oContained.w_CCFLRITE=trim(this.Parent.oContained.w_CCFLRITE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLRITE=='S',1,;
      0)
  endfunc

  add object oSELEZI_1_64 as StdRadio with uid="RDFUYVXOXW",rtseq=51,rtrep=.f.,left=583, top=484, width=129,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_64.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 137239770
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 137239770
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_64.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_64.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_64.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_66 as StdButton with uid="ZPXFKKEARK",left=727, top=526, width=48,height=45,;
    CpPicture="BMP\calcola.ico", caption="", nPag=1;
    , ToolTipText = "Premere per calcolare il riepilogo";
    , HelpContextID = 62783962;
    , Caption='C\<alcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_66.Click()
      with this.Parent.oContained
        GSAG_BCK(this.Parent.oContained,"Z")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_68 as StdButton with uid="VWJPMFDMOM",left=778, top=526, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare generazione";
    , HelpContextID = 64738554;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_68.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_37 as StdString with uid="VIOXXDVEBU",Visible=.t., Left=587, Top=289,;
    Alignment=1, Width=97, Height=18,;
    Caption="totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="EONBVFFTNW",Visible=.t., Left=587, Top=373,;
    Alignment=1, Width=97, Height=18,;
    Caption="= importo dovuto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="QTJUZUOAAU",Visible=.t., Left=578, Top=350,;
    Alignment=0, Width=106, Height=18,;
    Caption="- ritenuta d'acconto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="OUATPPVACR",Visible=.t., Left=581, Top=139,;
    Alignment=1, Width=103, Height=18,;
    Caption="- acconti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="KEPSEAVQKX",Visible=.t., Left=648, Top=231,;
    Alignment=1, Width=36, Height=18,;
    Caption="+ Iva: "  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="YEBMUEZIRX",Visible=.t., Left=648, Top=202,;
    Alignment=1, Width=36, Height=18,;
    Caption="+ cpa: "  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="JOPXAGLJWW",Visible=.t., Left=599, Top=259,;
    Alignment=1, Width=85, Height=18,;
    Caption="+ anticipazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="ZTBEJDFGIF",Visible=.t., Left=576, Top=79,;
    Alignment=1, Width=108, Height=18,;
    Caption="+ spese generali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="HQHQZNFNYK",Visible=.t., Left=576, Top=109,;
    Alignment=1, Width=108, Height=18,;
    Caption="+ spese imponibili:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="YCAZTNVKBO",Visible=.t., Left=599, Top=49,;
    Alignment=1, Width=85, Height=18,;
    Caption="+ totale onorari:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="ORQRIKRGZE",Visible=.t., Left=612, Top=19,;
    Alignment=1, Width=72, Height=18,;
    Caption="+ totale diritti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="TIRPCXFLCX",Visible=.t., Left=587, Top=170,;
    Alignment=1, Width=97, Height=18,;
    Caption="competenze: "  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag1krp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
